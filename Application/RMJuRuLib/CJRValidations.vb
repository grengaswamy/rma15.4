Option Strict Off
Option Explicit On
Imports System.Xml

Public Class CJRValidations
    '---------------------------------------------------------------------------------------
    ' Module    : CJRValidations
    ' DateTime  : 9/7/2005 14:26
    ' Author    : Venkata Mandayam
    ' Purpose   : This Object performs different kinds of Jurisdictional rule validations for
    '             its client. The client program instantiates this class and calls certain methods
    '
    '---------------------------------------------------------------------------------------
    Private Const sClassName As String = "CJRValidations"
    Private objValidationErrors As New CJRValidationErrors
    Private Const CONDITION_TAG As String = "condition"
    Private Const GROUP_TAG As String = "group"
    Private sCondition As String
    Private sMandatoryErrorDesc As String
    Private objRiskMasterFields As New CJRCodes
    Private objErrorMessages As New CJRCodes
    Private objFieldFormat As New CJRCodes
    Private objDNStatus As New CJRCodes
    Private objOperators As New CJRCodes
    Private bIsFieldMandatory As Boolean
    Dim objColEDIStdRecordDNs As CJREDIStdRecordDNs
    '---------------------------------------------------------------------------------------
    ' Procedure : ValidateEDIRecord
    ' DateTime  : 9/7/2005 16:11
    ' Author    : Venkata Mandayam
    ' Purpose   : This function accepts only a pre-defined objEDIRecord.
    '---------------------------------------------------------------------------------------
    'Nanda - Nowhere this method is used.
    '    Public Function ValidateEDIRecord(ByRef objUser As Riskmaster.Security.UserLogin, ByRef objEdiRecord As Object) As CJRValidationErrors 'Replace this late binding object with a certain Object Type if available objEdiRecord As Object cEdiRecords

    '        Const sFunctionName As String = "ValidateEDIRecord"

    '        Dim objRecordDN As CJREDIStdRecordDN
    '        Dim objEdiStdRecordTypes As CJREDIStdRecordTypes
    '        Dim objValidationError As CJRValidationError
    '        Dim iEdiRecTypCount As Short
    '        Dim iEdiRecTypCounter As Short
    '        Dim lRecordTypeID As Integer
    '        Dim i As Integer
    '        Dim bDNFound As Boolean
    '        Dim sConditionsXML As String

    '        Try

    '            'UPGRADE_NOTE: Object ValidateEDIRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '            ValidateEDIRecord = Nothing

    '            'UPGRADE_NOTE: Object objRiskMasterFields may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '            objRiskMasterFields = Nothing
    '            'UPGRADE_NOTE: Object objErrorMessages may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '            objErrorMessages = Nothing
    '            'UPGRADE_NOTE: Object objFieldFormat may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '            objFieldFormat = Nothing
    '            'UPGRADE_NOTE: Object objDNStatus may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '            objDNStatus = Nothing
    '            'UPGRADE_NOTE: Object objOperators may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '            objOperators = Nothing
    '            'UPGRADE_NOTE: Object objValidationErrors may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '            objValidationErrors = Nothing


    '            'objRiskMasterFields.LoadData objUser, "EDI_RM_FIELDS"
    '            objRiskMasterFields.LoadDataNoConnInFun(objUser, "EDI_RM_FIELDS")
    '            'objErrorMessages.LoadData objUser, "IAIABC_ERROR"
    '            objErrorMessages.LoadDataNoConnInFun(objUser, "IAIABC_ERROR")
    '            sMandatoryErrorDesc = objErrorMessages.GetCodeDesc("001") 'GetCodeDesc_SQL(objuser, GetCodeIDWithShort_SQL(objuser, "001", "IAIABC_ERROR"))
    '            'objFieldFormat.LoadData objUser, "EDI_FIELD_FORMAT"
    '            objFieldFormat.LoadDataNoConnInFun(objUser, "EDI_FIELD_FORMAT")
    '            'objDNStatus.LoadData objUser, "IAIABC_DN_STATUS"
    '            objDNStatus.LoadDataNoConnInFun(objUser, "IAIABC_DN_STATUS")
    '            'objOperators.LoadData objUser, "EDI_CONDITIONS"
    '            objOperators.LoadDataNoConnInFun(objUser, "EDI_CONDITIONS")

    '            'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.RecordType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '            'UPGRADE_WARNING: Use of Null/IsNull() detected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="2EED02CB-5C0E-4DC1-AE94-4FAA3A30F51A"'
    '            If IsDbNull(objEdiRecord.RecordType) Or (objEdiRecord.RecordType = 0) Then
    '                'RecordType not provided so load all record Types for that release
    '                objEdiStdRecordTypes = New CJREDIStdRecordTypes
    '                'If objEdiStdRecordTypes.LoadData(objUser, objEdiRecord.Jurisdiction, objEdiRecord.JurisReleaseRowId, 1) <> -1 Then
    '                'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.JurisReleaseRowId. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Jurisdiction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                If objEdiStdRecordTypes.LoadDataNoConnInFun(objUser, objEdiRecord.Jurisdiction, objEdiRecord.JurisReleaseRowId, 1) <> -1 Then
    '                    objValidationError.ErrSource = "Juris Rules: Retrieving EDI Record Types for Validation"
    '                    objValidationError.ErrDescription = "Error Retrieving EDI Record Types for the Release"
    '                    objValidationError.ErrField = "NA"
    '                    objValidationError.ErrRiskMasterField = "NA"
    '                    objValidationErrors.AddErrorObject(objValidationError)
    '                    Exit Function
    '                End If

    '            End If

    '            If Not objEdiStdRecordTypes Is Nothing Then
    '                iEdiRecTypCount = objEdiStdRecordTypes.Count
    '            Else
    '                iEdiRecTypCount = 1
    '            End If

    '            For iEdiRecTypCounter = 1 To iEdiRecTypCount

    '                If Not objEdiStdRecordTypes Is Nothing Then
    '                    lRecordTypeID = objEdiStdRecordTypes.Item(iEdiRecTypCounter).RecordTypeID
    '                Else
    '                    'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.RecordType. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                    lRecordTypeID = objEdiRecord.RecordType
    '                End If

    '                objColEDIStdRecordDNs = New CJREDIStdRecordDNs
    '                'If objColEDIStdRecordDNs.LoadData(objUser, objEdiRecord.Jurisdiction, lRecordTypeID, 1, "") <> -1 Then
    '                'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Jurisdiction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                If objColEDIStdRecordDNs.LoadDataNoConnInFun(objUser, objEdiRecord.Jurisdiction, lRecordTypeID, 1, "") <> -1 Then
    '                    objValidationError.ErrSource = "Juris Rules: Retrieving EDI DN fields for Validation"
    '                    objValidationError.ErrDescription = "Error Retrieving EDI DN fields"
    '                    objValidationError.ErrField = "NA"
    '                    objValidationError.ErrRiskMasterField = "NA"
    '                    objValidationErrors.AddErrorObject(objValidationError)
    '                    Exit Function
    '                End If

    '                'Successfully loaded the DN Records Object with its properties
    '                'Loop through all the fields and validate them

    '                For Each objRecordDN In objColEDIStdRecordDNs

    '                    bDNFound = False

    '                    'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Count. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                    For i = 1 To objEdiRecord.Count

    '                        'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                        If (objEdiRecord.Item(i).DN_ID = objRecordDN.TableRowID) And objEdiRecord.Item(i).Validate Then

    '                            bDNFound = True
    '                            'Load Conditions XML
    '                            sConditionsXML = LoadConditionsXML(objUser, objEdiRecord, i)
    '                            Select Case objRecordDN.FieldStatus

    '                                Case objDNStatus.GetCodeId("M") 'Mandatory Field
    '                                    'If it is a Mandatory field first check if the field is empty
    '                                    'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                                    objValidationError = ValidateMandatoryField(objUser, objEdiRecord.Item(i))
    '                                    objValidationErrors.AddErrorObject(objValidationError)

    '                                    If objValidationError Is Nothing Then
    '                                        'Field has some value so perform other validations
    '                                        'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                                        objValidationError = ValidateFieldFormat(objEdiRecord.Item(i).DNNumber, objEdiRecord.Item(i).DNValue, CInt(objRecordDN.FieldFormat))
    '                                        objValidationErrors.AddErrorObject(objValidationError)

    '                                        If objValidationError Is Nothing Then ValidateOtherConditions(objUser, objRecordDN, i, sConditionsXML)
    '                                        objValidationErrors.AddErrorObject(objValidationError)

    '                                    End If

    '                                    'UPGRADE_NOTE: Object objValidationError may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
    '                                    objValidationError = Nothing

    '                                Case objDNStatus.GetCodeId("C") 'Conditional Field

    '                                    If IsConditionalFieldMandatory(objUser, objRecordDN, sConditionsXML) Then
    '                                        'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                                        objValidationError = ValidateMandatoryField(objUser, objEdiRecord.Item(i))
    '                                        objValidationErrors.AddErrorObject(objValidationError)


    '                                        If objValidationError Is Nothing Then
    '                                            'Field has some value so perform other validations
    '                                            'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                                            objValidationError = ValidateFieldFormat(objEdiRecord.Item(i).DNNumber, objEdiRecord.Item(i).DNValue, CInt(objRecordDN.FieldFormat))
    '                                            objValidationErrors.AddErrorObject(objValidationError)

    '                                            If objValidationError Is Nothing Then ValidateOtherConditions(objUser, objRecordDN, i, sConditionsXML)
    '                                            objValidationErrors.AddErrorObject(objValidationError)
    '                                        End If

    '                                    Else

    '                                        'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                                        If Not objEdiRecord.Item(i).DNValue = "" Then
    '                                            'Field has some value so perform other validations
    '                                            'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                                            objValidationError = ValidateFieldFormat(objEdiRecord.Item(i).DNNumber, objEdiRecord.Item(i).DNValue, CInt(objRecordDN.FieldFormat))
    '                                            objValidationErrors.AddErrorObject(objValidationError)

    '                                            If objValidationError Is Nothing Then ValidateOtherConditions(objUser, objRecordDN, i, sConditionsXML)
    '                                            objValidationErrors.AddErrorObject(objValidationError)
    '                                        End If

    '                                    End If

    '                                Case objDNStatus.GetCodeId("O") 'Optional Field

    '                                    'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                                    If Not objEdiRecord.Item(i).DNValue = "" Then

    '                                        If IsConditionalFieldMandatory(objUser, objRecordDN, sConditionsXML) Then
    '                                            'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                                            objValidationError = ValidateMandatoryField(objUser, objEdiRecord.Item(i))
    '                                            objValidationErrors.AddErrorObject(objValidationError)
    '                                        End If

    '                                        If objValidationError Is Nothing Then
    '                                            'Field has some value so perform other validations
    '                                            'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '                                            objValidationError = ValidateFieldFormat(objEdiRecord.Item(i).DNNumber, objEdiRecord.Item(i).DNValue, CInt(objRecordDN.FieldFormat))
    '                                            objValidationErrors.AddErrorObject(objValidationError)

    '                                            If objValidationError Is Nothing Then ValidateOtherConditions(objUser, objRecordDN, i, sConditionsXML)
    '                                            objValidationErrors.AddErrorObject(objValidationError)
    '                                        End If

    '                                    End If

    '                            End Select

    '                        End If

    '                    Next

    '                    'VRM - 03/15/2006 - This If Block was created to make sure that all the DN Numbers
    '                    'Associated with the record type have been sent for validation.
    '                    'But for release 2, the caller of this validation is not sending all DN Numbers for
    '                    'validation because certain DNs don't make it to the filing attributing to
    '                    'various conditions. So this block has been put to rest :(.
    '                    ''''''        If Not bDNFound Then
    '                    ''''''            Set objValidationError = New CJRValidationError
    '                    ''''''            If objRecordDN.FieldStatus = objDNStatus.GetCodeId("M") Then
    '                    ''''''                objValidationError.ErrDescription = "Mandatory Field not populated in EDI record"
    '                    ''''''            ElseIf objRecordDN.FieldStatus = objDNStatus.GetCodeId("C") Then
    '                    ''''''                objValidationError.ErrDescription = "Conditional Field not populated in EDI record"
    '                    ''''''            End If
    '                    ''''''            objValidationError.ErrField = objRecordDN.DNNumberText
    '                    ''''''            objValidationError.ErrRiskMasterField = objRiskMasterFields.GetCodeDesc(Mid$(objRecordDN.DNNumberText, 3))
    '                    ''''''            objValidationErrors.AddErrorObject objValidationError
    '                    ''''''        End If

    '                Next objRecordDN
    '            Next

    '        Catch ex As Exception
    '            With Err
    '                g_lErrNum = Err.Number
    '                g_sErrSrc = .Source
    '                g_sErrDescription = Err.Description
    '            End With
    '            ValidateEDIRecord = objValidationErrors

    'hCustomError:

    '            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
    '            g_lErrLine = Erl()
    '            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
    '            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

    '        Finally

    '            If Not objValidationErrors Is Nothing Then
    '                'Create User Diary
    '                'objValidationErrors.CreateUserDiaryWithErrors objuser, objEdiRecord.AdjusterID, "EDI Error Diary", "", "CLAIM", objEdiRecord.ClaimId, "Claim Number : " & objEdiRecord.ClaimNumber
    '                ValidateEDIRecord = objValidationErrors
    '            End If

    '        End Try

    '    End Function

    Public Function ValidateMandatoryField(ByRef objUser As Riskmaster.Security.UserLogin, ByRef objEdiField As Object) As CJRValidationError 'objRecordDN as object cEDIRecord

        Dim objValidationError As CJRValidationError
        Dim i As Short
        Dim lMandatoryErrorCodeId As Integer

        'UPGRADE_WARNING: Couldn't resolve default property of object objEdiField.DNValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        If (Trim(objEdiField.DNValue)) = "" Then 'Or (Trim$(objEdiField.DNValue) = SameCharString("0", Len(Trim$(objEdiField.DNValue))))

            objValidationError = New CJRValidationError

            objValidationError.ErrSource = "Juris Rules: Validating Mandatory field"
            objValidationError.ErrDescription = sMandatoryErrorDesc
            'UPGRADE_WARNING: Couldn't resolve default property of object objEdiField.DNNumber. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            objValidationError.ErrField = objEdiField.DNNumber
            'UPGRADE_WARNING: Couldn't resolve default property of object objEdiField.DNNumber. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            objValidationError.ErrRiskMasterField = objRiskMasterFields.GetCodeDesc(Mid(objEdiField.DNNumber, 3))

        End If

        ValidateMandatoryField = objValidationError



    End Function
    'Nanda - Nowhere this method is used.
    'Public Function IsConditionalFieldMandatory(ByRef objUser As Riskmaster.Security.UserLogin, ByRef objEdiRecordDN As CJREDIStdRecordDN, ByRef sConditionsXML As String) As Boolean 'objRecordDN as object cEDIRecord

    '    Const sFunctionName As String = "IsConditionalFieldMandatory"
    '    Dim objConditions As CJREDIStdConditional
    '    Dim sCondition As String
    '    Dim obj As New MSScriptControl.ScriptControl
    '    Dim objValidationError As New CJRValidationError
    '    Dim XMLNodesList As XmlNodeList
    '    Dim XMLElement As XmlElement
    '    Dim objXML As New XmlDocument

    '    Try
    '        'Get the conditions XML for the field.

    '        bIsFieldMandatory = False

    '        If sConditionsXML = "" Then Exit Function

    '        objXML.loadXML(sConditionsXML)

    '        XMLNodesList = objXML.getElementsByTagName(GROUP_TAG)

    '        For Each XMLElement In XMLNodesList

    '            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(mandatoryoptional). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '            If (XMLElement.getAttribute("mandatoryoptional") = "1") Then

    '                bIsFieldMandatory = True
    '                sCondition = ApplyConditionsGroup(XMLElement.InnerXml.ToString(), objEdiRecordDN)

    '            End If
    '        Next XMLElement

    '    Catch ex As Exception
    '        With Err
    '            g_lErrNum = Err.Number
    '            g_sErrSrc = .Source
    '            g_sErrDescription = Err.Description
    '        End With

    '        objValidationError.ErrSource = "Juris Rules: Validating conditional field"
    '        objValidationError.ErrDescription = g_sErrDescription
    '        objValidationError.ErrField = objEdiRecordDN.DNNumberText
    '        objValidationError.ErrRiskMasterField = objRiskMasterFields.GetCodeDesc(Mid(objEdiRecordDN.DNNumberText, 3))

    '        IsConditionalFieldMandatory = False

    '        g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
    '        g_lErrLine = Erl()
    '        LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
    '        If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


    '    Finally
    '        IsConditionalFieldMandatory = bIsFieldMandatory

    '    End Try

    'End Function

    Private Function ApplyConditionsGroup(ByRef sConditionsXML As String, ByRef objRecordDN As Object) As String

        Dim XMLElement As XmlElement
        Dim i As Short
        Dim objXML As New XmlDocument
        Dim sCondition As String

        objXML.LoadXml(sConditionsXML)

        For i = 0 To objXML.ChildNodes.Count - 1
            XMLElement = objXML.ChildNodes(i)
            Call ApplyConditions(XMLElement, sCondition, objRecordDN)
        Next

        ApplyConditionsGroup = sCondition



    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : ApplyConditions
    ' DateTime  : 10/20/2005 19:08
    ' Author    : Venkata Mandayam
    ' Purpose   : This is a recursive Subroutine. It parses the XML and creates a condition string
    '             Once the condition string is formed, it executes the condition.
    '---------------------------------------------------------------------------------------
    '
    Private Sub ApplyConditions(ByRef XMLElement As XmlElement, ByRef sCondition As String, ByRef objRecordDN As Object)

        Dim XMLChildElement As XmlElement
        'Dim TreeNode As Node
        Dim sRelation As String
        Dim sField As String
        Dim i As Short
        Dim sParentKey As String
        Dim sConditionsCol As Collection
        Dim lErrorCode As Integer
        Dim objValidationError As CJRValidationError
        Dim sQualifiedDNValue As String
        Dim sQualifiedValueToCompare As String
        Dim sCurrenctCondition As String
        Dim sValueToCompare As String
        Dim sDNNumber As String


        Try
            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sRelation = UCase(XMLElement.GetAttribute("relation"))

            If Trim(sRelation) <> "" Then
                'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sRelation = " " & UCase(XMLElement.GetAttribute("relation")) & " "
            End If

            Select Case XMLElement.BaseURI
                Case GROUP_TAG

                    If sCondition <> "" Then
                        sCondition = sCondition & sRelation & "("
                    Else
                        sCondition = "("
                    End If

                Case CONDITION_TAG

                    'Lets do a little format check here because there is no point in doing a wrong comparison if the
                    'Field format itself is wrong.
                    objValidationError = New CJRValidationError
                    'UPGRADE_NOTE: Object objValidationError may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    objValidationError = Nothing

                    'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(fixedvalueyn). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If XMLElement.GetAttribute("fixedvalueyn") = 1 Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sValueToCompare = XMLElement.GetAttribute("fixedvalue")
                        'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        objValidationError = ValidateFieldFormat(GetDNNumberFromDNID(objColEDIStdRecordDNs, XMLElement.GetAttribute("dnrowid")), sValueToCompare, GetDNFormatFromDNID(objColEDIStdRecordDNs, XMLElement.GetAttribute("dnrowid")))
                        'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sDNNumber = XMLElement.GetAttribute("dnrowid")
                    Else
                        'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sValueToCompare = XMLElement.GetAttribute("anotherdnvalue")
                        'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        objValidationError = ValidateFieldFormat(GetDNNumberFromDNID(objColEDIStdRecordDNs, XMLElement.GetAttribute("anotherdnrowid")), sValueToCompare, GetDNFormatFromDNID(objColEDIStdRecordDNs, XMLElement.GetAttribute("anotherdnrowid")))
                        'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        sDNNumber = XMLElement.GetAttribute("anotherdnrowid")
                    End If

                    If Not objValidationError Is Nothing Then
                        sCondition = sCondition & sRelation & "(1 = 2)"
                        objValidationErrors.AddErrorObject(objValidationError)
                        Exit Sub
                    End If
                    'UPGRADE_NOTE: Object objValidationError may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    objValidationError = Nothing

                    'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(dnrowvalue). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    objValidationError = ValidateFieldFormat(GetDNNumberFromDNID(objColEDIStdRecordDNs, XMLElement.GetAttribute("dnrowid")), XMLElement.GetAttribute("dnrowvalue"), GetDNFormatFromDNID(objColEDIStdRecordDNs, XMLElement.GetAttribute("dnrowid")))
                    If Not objValidationError Is Nothing Then
                        sCondition = sCondition & sRelation & "(1 = 2)"
                        objValidationErrors.AddErrorObject(objValidationError)
                        Exit Sub
                    End If
                    'UPGRADE_NOTE: Object objValidationError may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    objValidationError = Nothing

                    sCurrenctCondition = "("

                    Select Case XMLElement.GetAttribute("operator")
                        Case objOperators.GetCodeId("C")
                            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(dnrowvalue). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            sCurrenctCondition = sCurrenctCondition & "Instr(" & Chr(34) & XMLElement.GetAttribute("dnrowvalue") & Chr(34) & "," & Chr(34) & sValueToCompare & Chr(34) & ")"
                            'Stop
                        Case objOperators.GetCodeId("NC")
                            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(dnrowvalue). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            sCurrenctCondition = sCurrenctCondition & "NOT Instr(" & Chr(34) & XMLElement.GetAttribute("dnrowvalue") & Chr(34) & "," & Chr(34) & sValueToCompare & Chr(34) & ")"
                        Case objOperators.GetCodeId("S")
                            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(dnrowvalue). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            sCurrenctCondition = sCurrenctCondition & "Left(" & Chr(34) & XMLElement.GetAttribute("dnrowvalue") & Chr(34) & ", Len(" & Chr(34) & sValueToCompare & Chr(34) & "))=" & Chr(34) & sValueToCompare & Chr(34)
                        Case objOperators.GetCodeId("NS")
                            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(dnrowvalue). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            sCurrenctCondition = sCurrenctCondition & "NOT Left(" & Chr(34) & XMLElement.GetAttribute("dnrowvalue") & Chr(34) & ", Len(" & Chr(34) & sValueToCompare & Chr(34) & "))=" & Chr(34) & sValueToCompare & Chr(34)
                        Case objOperators.GetCodeId("E")
                            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(dnrowvalue). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            sCurrenctCondition = sCurrenctCondition & "Right(" & Chr(34) & XMLElement.GetAttribute("dnrowvalue") & Chr(34) & ", Len(" & Chr(34) & sValueToCompare & Chr(34) & "))=" & Chr(34) & sValueToCompare & Chr(34)
                        Case objOperators.GetCodeId("NE")
                            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(dnrowvalue). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            sCurrenctCondition = sCurrenctCondition & "NOT Right(" & Chr(34) & XMLElement.GetAttribute("dnrowvalue") & Chr(34) & ", Len(" & Chr(34) & sValueToCompare & Chr(34) & "))=" & Chr(34) & sValueToCompare & Chr(34)
                        Case objOperators.GetCodeId("MIL")
                            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(dnrowvalue). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            sCurrenctCondition = sCurrenctCondition & "Len(" & Chr(34) & XMLElement.GetAttribute("dnrowvalue") & Chr(34) & ")>=" & sValueToCompare
                        Case objOperators.GetCodeId("MAL")
                            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(dnrowvalue). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            sCurrenctCondition = sCurrenctCondition & "Len(" & Chr(34) & XMLElement.GetAttribute("dnrowvalue") & Chr(34) & ")<=" & sValueToCompare
                        Case objOperators.GetCodeId("AS")
                            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(dnrowvalue). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            sCurrenctCondition = sCurrenctCondition & Chr(34) & XMLElement.GetAttribute("dnrowvalue") & Chr(34) & "=" & Chr(34) & SameCharString(Left(XMLElement.GetAttribute("dnrowvalue"), 1), Len(XMLElement.GetAttribute("dnrowvalue"))) & Chr(34)
                        Case objOperators.GetCodeId("NAS")
                            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(dnrowvalue). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            sCurrenctCondition = sCurrenctCondition & "NOT " & Chr(34) & XMLElement.GetAttribute("dnrowvalue") & Chr(34) & "=" & Chr(34) & SameCharString(Left(XMLElement.GetAttribute("dnrowvalue"), 1), Len(XMLElement.GetAttribute("dnrowvalue"))) & Chr(34)
                        Case Else
                            'UPGRADE_WARNING: Couldn't resolve default property of object objRecordDN.FieldFormat. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            'UPGRADE_WARNING: Couldn't resolve default property of object QualifyDNValue(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            sQualifiedDNValue = QualifyDNValue(XMLElement.GetAttribute("dnrowvalue"), objRecordDN.FieldFormat)
                            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(fixedvalueyn). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If XMLElement.GetAttribute("fixedvalueyn") = 1 Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object objRecordDN.FieldFormat. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                'UPGRADE_WARNING: Couldn't resolve default property of object QualifyDNValue(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                sQualifiedValueToCompare = QualifyDNValue(XMLElement.GetAttribute("fixedvalue"), objRecordDN.FieldFormat)
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objRecordDN.FieldFormat. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                'UPGRADE_WARNING: Couldn't resolve default property of object QualifyDNValue(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                sQualifiedValueToCompare = QualifyDNValue(XMLElement.GetAttribute("anotherdnvalue"), objRecordDN.FieldFormat)
                            End If

                            sCurrenctCondition = sCurrenctCondition & sQualifiedDNValue
                            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            sCurrenctCondition = sCurrenctCondition & objOperators.GetShortCodeFromID(XMLElement.GetAttribute("operator"))
                            sCurrenctCondition = sCurrenctCondition & sQualifiedValueToCompare
                    End Select

                    sCurrenctCondition = sCurrenctCondition & ")"

                    'Execute this Condition if it has a resultant action
                    'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    '#Mukesh-- Should be handeled with Try catch 
                    'lErrorCode = ExecuteCondition(sCurrenctCondition, XMLElement.GetAttribute("errormessage"))

                    If lErrorCode > 0 Then
                        'Add error to Validation errors object
                        objValidationError = New CJRValidationError
                        objValidationError.ErrSource = "Juris Rules: Validating Condition"
                        objValidationError.ErrDescription = objErrorMessages.GetCodeShortDescFromID(lErrorCode)
                        'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        objValidationError.ErrFieldValue = XMLElement.GetAttribute("dnrowvalue")
                        'UPGRADE_WARNING: Couldn't resolve default property of object objRecordDN.DNNumberText. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        objValidationError.ErrField = objRecordDN.DNNumberText
                        'UPGRADE_WARNING: Couldn't resolve default property of object objRecordDN.DNNumberText. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        objValidationError.ErrRiskMasterField = objRiskMasterFields.GetCodeDesc(Mid(objRecordDN.DNNumberText, 3))
                        objValidationErrors.AddErrorObject(objValidationError)
                    End If

                    If sCondition <> "" Then
                        sCondition = sCondition & sRelation & sCurrenctCondition
                    Else
                        sCondition = sCurrenctCondition
                    End If

            End Select

            For Each XMLChildElement In XMLElement.ChildNodes
                Call ApplyConditions(XMLChildElement, sCondition, objRecordDN)
            Next XMLChildElement

            If XMLElement.BaseURI = GROUP_TAG Then

                sCondition = sCondition & ")"

                'Execute this Condition if it has a resultant action
                'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                '#Mukesh Should be handled with try catch()
                'lErrorCode = ExecuteCondition(sCondition, XMLElement.GetAttribute("errormessage"))

                If lErrorCode > 0 Then

                    'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(mandatoryoptional). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If XMLElement.GetAttribute("mandatoryoptional") Then
                        'Dont log any error. Just tell the caller that this will be an optional field
                        bIsFieldMandatory = False
                    Else
                        objValidationError = New CJRValidationError
                        objValidationError.ErrSource = "Juris Rules: Validating Condition"
                        objValidationError.ErrDescription = objErrorMessages.GetCodeShortDescFromID(lErrorCode)
                        'UPGRADE_WARNING: Couldn't resolve default property of object objRecordDN.DNNumberText. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        objValidationError.ErrField = objRecordDN.DNNumberText
                        'UPGRADE_WARNING: Couldn't resolve default property of object objRecordDN.DNNumberText. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        objValidationError.ErrRiskMasterField = objRiskMasterFields.GetCodeDesc(Mid(objRecordDN.DNNumberText, 3))
                        objValidationErrors.AddErrorObject(objValidationError)
                    End If
                End If

            End If
        Catch ex As Exception
        Finally
        End Try

    End Sub

    Private Function StuffDNvalues(ByRef ConditionsXML As String, ByRef objEdiRecord As Object) As String

        Dim XMLNodesList As XmlNodeList
        Dim XMLElement As XmlElement
        Dim XMLAttribute As XmlAttribute
        Dim lDNRowId As Integer
        Dim lAnotherDNRowId As Integer
        Dim objEdiField As Object
        Dim objEDIField2 As Object
        Dim sCondition As String
        Dim objConditionsXML As XmlDocument
        Dim i As Short
        Dim j As Short

        Const sFunctionName As String = "StuffDNvalues"

        Try

            objConditionsXML = New XmlDocument
            objConditionsXML.LoadXml(ConditionsXML)

            XMLNodesList = objConditionsXML.GetElementsByTagName(CONDITION_TAG)
            For Each XMLElement In XMLNodesList
                'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                lDNRowId = XMLElement.GetAttribute("dnrowid")

                'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Count. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                For i = 1 To objEdiRecord.Count

                    'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    objEdiField = objEdiRecord.Item(i)
                    'UPGRADE_WARNING: Couldn't resolve default property of object objEdiField.DN_ID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objEdiField.DN_ID = lDNRowId Then

                        XMLAttribute = objConditionsXML.CreateAttribute("dnrowvalue")
                        'UPGRADE_WARNING: Couldn't resolve default property of object objEdiField.DNValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        XMLAttribute.Value = objEdiField.DNValue
                        XMLElement.SetAttributeNode(XMLAttribute)

                        'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(fixedvalueyn). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If XMLElement.GetAttribute("fixedvalueyn") = 0 Then

                            'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(anotherdnrowid). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If XMLElement.GetAttribute("anotherdnrowid") <> 0 Then

                                'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                lAnotherDNRowId = XMLElement.GetAttribute("anotherdnrowid")

                                'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Count. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                For j = 1 To objEdiRecord.Count

                                    'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    objEDIField2 = objEdiRecord.Item(j)
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objEDIField2.DN_ID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If objEDIField2.DN_ID = lDNRowId Then

                                        XMLAttribute = objConditionsXML.CreateAttribute("anotherdnrowvalue")
                                        'UPGRADE_WARNING: Couldn't resolve default property of object objEdiField.DNValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        XMLAttribute.Value = objEdiField.DNValue
                                        XMLElement.SetAttributeNode(XMLAttribute)

                                        Exit For

                                    End If

                                Next

                            End If

                        End If

                        Exit For

                    End If

                Next

            Next XMLElement

            StuffDNvalues = objConditionsXML.InnerXml.ToString()
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            StuffDNvalues = CStr(Err.Number)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function


    Public Function ValidateFieldFormat(ByRef sDNNumber As String, ByRef sDNValue As String, ByRef lFieldFormat As Integer) As CJRValidationError

        Const sFunctionName As String = "ValidateFieldFormat"
        Dim objValidationError As CJRValidationError
        Dim i As Short
        Dim lMandatoryErrorCodeId As Integer
        Dim sErrorMessage As String

        Try

            sErrorMessage = ""
            Select Case lFieldFormat
                Case objFieldFormat.GetCodeId("$9.2")
                    If InStr(sDNValue, "$") Then
                        If Not IsNumeric(Mid(sDNValue, 2)) Then
                            sErrorMessage = objErrorMessages.GetCodeDesc("028")
                        End If
                    Else
                        If Not IsNumeric(sDNValue) Then
                            sErrorMessage = objErrorMessages.GetCodeDesc("028")
                        End If
                    End If

                Case objFieldFormat.GetCodeId("3.2")
                    If InStr(1, sDNValue, ".") Then
                        If Not IsNumeric(sDNValue) Then
                            sErrorMessage = objErrorMessages.GetCodeDesc("028")
                        End If
                    Else
                        sErrorMessage = objErrorMessages.GetCodeDesc("028")
                    End If

                Case objFieldFormat.GetCodeId("DATE")
                    If InStr(sDNValue, "/") > 0 Then
                        sErrorMessage = objErrorMessages.GetCodeDesc("029") & "--CCYYMMDD"
                    Else
                        If Not bIsDate(sDNValue) Then
                            sErrorMessage = objErrorMessages.GetCodeDesc("029") & "--CCYYMMDD"
                        End If
                    End If

                Case objFieldFormat.GetCodeId("HHMM"), objFieldFormat.GetCodeId("TIME")
                    If Len(sDNValue) > Len("HHMM") Or Len(sDNValue) < Len("HHMM") Then
                        sErrorMessage = objErrorMessages.GetCodeDesc("031") & "--HHMM"
                    Else

                        If IsNumeric(sDNValue) Then
                            If Val(Left(sDNValue, 2)) < 0 Or Val(Left(sDNValue, 2)) > 23 Or Val(Right(sDNValue, 2)) < 0 Or Val(Right(sDNValue, 2)) > 59 Then
                                sErrorMessage = objErrorMessages.GetCodeDesc("031") & "--HHMM"
                            End If
                        Else
                            sErrorMessage = objErrorMessages.GetCodeDesc("031") & "--HHMM"
                        End If
                    End If

                Case objFieldFormat.GetCodeId("HHMMSS")
                    If Len(sDNValue) > Len("HHMMSS") Or Len(sDNValue) < Len("HHMMSS") Then
                        sErrorMessage = objErrorMessages.GetCodeDesc("031") & "--HHMMSS"
                    Else

                        If IsNumeric(sDNValue) Then
                            If Val(Left(sDNValue, 2)) < 0 Or Val(Left(sDNValue, 2)) > 23 Or Val(Right(sDNValue, 2)) < 0 Or Val(Right(sDNValue, 2)) > 59 Or Val(Mid(sDNValue, 3, 2)) < 0 Or Val(Mid(sDNValue, 3, 2)) > 59 Then
                                sErrorMessage = objErrorMessages.GetCodeDesc("031")
                            End If
                        Else
                            sErrorMessage = objErrorMessages.GetCodeDesc("031") & "--HHMMSS"
                        End If
                    End If

                Case objFieldFormat.GetCodeId("N")

                    If InStr(1, sDNValue, ".") Then
                        sErrorMessage = objErrorMessages.GetCodeDesc("028")
                    Else
                        If Not IsNumeric(sDNValue) Then
                            sErrorMessage = objErrorMessages.GetCodeDesc("028")
                        End If
                    End If

                Case objFieldFormat.GetCodeId("A-Z/0-9")

                    If Not IsAlphaNumeric(sDNValue) Then
                        sErrorMessage = objErrorMessages.GetCodeDesc("030")
                    End If

                Case objFieldFormat.GetCodeId("A-Z")

                    If Not IsAlpha(sDNValue) Then
                        sErrorMessage = objErrorMessages.GetCodeDesc("058") & "--Must be an Alphabet"
                    End If

            End Select

            If Not sErrorMessage = "" Then
                objValidationError = New CJRValidationError

                objValidationError.ErrSource = "Juris Rules: Validating Field Format"
                objValidationError.ErrDescription = sErrorMessage
                objValidationError.ErrFieldValue = sDNValue
                objValidationError.ErrField = sDNNumber
                objValidationError.ErrRiskMasterField = objRiskMasterFields.GetCodeDesc(Mid(sDNNumber, 3))

            End If

            ValidateFieldFormat = objValidationError

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Sub ValidateOtherConditions(ByRef objUser As Riskmaster.Security.UserLogin, ByRef objRecordDN As Object, ByRef CurrentItem As Integer, ByRef sConditionsXML As String)

        Const sFunctionName As String = "ValidateOtherConditions"
        Dim objConditions As CJREDIStdConditional
        Dim sCondition As String
        ' Dim obj As New MSScriptControl.ScriptControl
        Dim bConditionPass As Boolean
        Dim objValidationError As New CJRValidationError
        Dim XMLNodesList As XmlNodeList
        Dim XMLElement As XmlElement
        Dim objXML As New XmlDocument

        Try

            If sConditionsXML = "" Then Exit Sub

            objXML.LoadXml(sConditionsXML)

            For Each XMLElement In objXML.FirstChild.ChildNodes 'First child is the Outer group. The actual conditions and groups are withing this parent group

                'UPGRADE_WARNING: Couldn't resolve default property of object XMLElement.getAttribute(mandatoryoptional). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If (XMLElement.GetAttribute("mandatoryoptional") <> "1") Then

                    sCondition = ApplyConditionsGroup(XMLElement.InnerXml.ToString(), objRecordDN)

                End If

            Next XMLElement

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            objValidationError.ErrSource = "Juris Rules: Validating conditional field"
            objValidationError.ErrDescription = g_sErrDescription
            'UPGRADE_WARNING: Couldn't resolve default property of object objRecordDN.DNNumberText. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            objValidationError.ErrField = objRecordDN.DNNumberText
            'UPGRADE_WARNING: Couldn't resolve default property of object objRecordDN.DNNumberText. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            objValidationError.ErrRiskMasterField = objRiskMasterFields.GetCodeDesc(Mid(objRecordDN.DNNumberText, 3))

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Sub


    Public Function LoadConditionsXML(ByRef objUser As Riskmaster.Security.UserLogin, ByRef objEdiRecord As Object, ByRef CurrentItem As Integer) As String

        Const sFunctionName As String = "LoadConditionsXML"
        Dim objConditions As CJREDIStdConditional
        Dim sConditionsXML As String

        sConditionsXML = ""

        Try
            'Get the conditions XML for the field.
            objConditions = New CJREDIStdConditional

            'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Jurisdiction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object objEdiRecord.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objConditions.LoadData(objUser, objEdiRecord.Item(CurrentItem).DN_ID, objEdiRecord.Jurisdiction) = -1 Then
                sConditionsXML = StuffDNvalues((objConditions.ConditionsXML), objEdiRecord)
                LoadConditionsXML = sConditionsXML
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally


        End Try

    End Function

    'Public Function ExecuteCondition(ByRef sCondition As String, ByRef sErrorMessage As String) As Integer

    '    Dim obj As New MSScriptControl.ScriptControl
    '    Dim sIfPart As String
    '    Dim sInsideIfPart As String

    '    ExecuteCondition = 0

    '    If (Not sErrorMessage = "") And (Not sErrorMessage = "0") And (Not sErrorMessage = "-1") Then

    '        obj.Language = "VBScript"

    '        If InStr(sCondition, ") THEN (") Then

    '            'This is an If Statement. So Process it a bit differently.
    '            sIfPart = Mid(sCondition, 2, InStr(sCondition, "THEN") - 2)
    '            sInsideIfPart = Mid(sCondition, InStr(sCondition, "THEN") + 4, Len(sCondition))
    '            sInsideIfPart = Left(sInsideIfPart, Len(sInsideIfPart) - 1) ' (sCondition, InStr(sCondition, "THEN") + 1, Len(sCondition))

    '            'UPGRADE_WARNING: Couldn't resolve default property of object obj.Eval(sIfPart). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '            If obj.Eval(sIfPart) Then
    '                If Not obj.Eval(sInsideIfPart) Then
    '                    ExecuteCondition = CInt(sErrorMessage)
    '                End If
    '            End If

    '        Else

    '            If Not obj.Eval(sCondition) Then
    '                ExecuteCondition = CInt(sErrorMessage)
    '            End If

    '        End If


    '    End If



    'End Function

    Public Function QualifyDNValue(ByRef sDNValue As String, ByRef lFieldFormat As Integer) As Object
        'TODO change this to FieldFormat
        Select Case lFieldFormat

            Case objFieldFormat.GetCodeId("$9.2")
                If Trim(sDNValue) <> "" Then
                    If InStr(sDNValue, "$") Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object QualifyDNValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        QualifyDNValue = CDbl(Mid(sDNValue, 2))
                    Else
                        'UPGRADE_WARNING: Couldn't resolve default property of object QualifyDNValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        QualifyDNValue = CDbl(sDNValue)
                    End If
                End If
            Case objFieldFormat.GetCodeId("3.2")
                'UPGRADE_WARNING: Couldn't resolve default property of object QualifyDNValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If Trim(sDNValue) <> "" Then QualifyDNValue = CInt(sDNValue)

            Case objFieldFormat.GetCodeId("N")
                If Not Trim(sDNValue) = "" Then
                    QualifyDNValue = Val(sDNValue)
                End If

            Case Else
                'UPGRADE_WARNING: Couldn't resolve default property of object QualifyDNValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                QualifyDNValue = Chr(34) & sDNValue & Chr(34)

        End Select



    End Function

    Public Function SameCharString(ByRef schar As String, ByRef ilen As Short) As String
        Dim i As Short
        Dim sSameCharString As String

        For i = 1 To ilen
            sSameCharString = sSameCharString & schar
        Next

        SameCharString = sSameCharString


    End Function

    Public Function GetDNNumberFromDNID(ByRef objRecordDNs As CJREDIStdRecordDNs, ByRef lDNID As Integer) As String

        Dim i As Short

        GetDNNumberFromDNID = ""

        For i = 1 To objRecordDNs.Count
            If (objRecordDNs.Item(i).TableRowID = lDNID) Then
                GetDNNumberFromDNID = objRecordDNs.Item(i).DNNumberText
                Exit For
            End If
        Next



    End Function

    Public Function GetDNFormatFromDNID(ByRef objRecordDNs As CJREDIStdRecordDNs, ByRef lDNID As Integer) As Integer

        Dim i As Short

        GetDNFormatFromDNID = 0

        For i = 1 To objRecordDNs.Count
            If (objRecordDNs.Item(i).TableRowID = lDNID) Then
                GetDNFormatFromDNID = CInt(objRecordDNs.Item(i).FieldFormat)
                Exit For
            End If
        Next



    End Function
End Class

