Option Strict Off
Option Explicit On
Public Class CJRValidationError

    Private m_ErrSource As String
    Private m_ErrDescription As String
    Private m_ErrField As String
    Private m_ErrRiskMasterField As String
    Private m_ErrFieldValue As String

    Public Property ErrSource() As String
        Get
            ErrSource = m_ErrSource
        End Get
        Set(ByVal Value As String)
            m_ErrSource = Value
        End Set
    End Property

    Public Property ErrDescription() As String
        Get
            ErrDescription = m_ErrDescription
        End Get
        Set(ByVal Value As String)
            m_ErrDescription = Value
        End Set
    End Property

    Public Property ErrField() As String
        Get
            ErrField = m_ErrField
        End Get
        Set(ByVal Value As String)
            m_ErrField = Value
        End Set
    End Property

    Public Property ErrRiskMasterField() As String
        Get
            ErrRiskMasterField = m_ErrRiskMasterField
        End Get
        Set(ByVal Value As String)
            m_ErrRiskMasterField = Value
        End Set
    End Property

    Public Property ErrFieldValue() As String
        Get
            ErrFieldValue = m_ErrFieldValue
        End Get
        Set(ByVal Value As String)
            m_ErrFieldValue = Value
        End Set
    End Property
End Class

