Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleDeathSuv
    Const sClassName As String = "CJRBenefitRuleDeathSuv"
    Const sTableName As String = "WCP_DETHS_RULE"
    'local variable(s) not publicly exposed
    Const dWeekToMonthConvFactorDefault As Double = 4.34821
    'to support future interface
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_ErrorMaskJR As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_ErrorMaskSAWW As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_JurisDefinedWorkWeek As Integer 'jtodd22 03/08/2005 not part of the rule, but used as part of rule in RMWCCalc.dll
    Private m_JurisRowID As Integer
    Private m_MaxCompRateMonthly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MaxCompRateWeekly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_NoCodeID As Integer
    Private m_PayPeriodCode As Integer
    Private m_PayPeriodLockedInCalcCode As Integer
    Private m_RuleTotalWeeks As Double
    Private m_TableRowID As Integer
    Private m_UseTwoThirdsCode As Integer
    Private m_Warning As String
    Private m_WeekToMonthConvFactor As Double
    Private m_YesCodeID As Integer

    Private m_AWWPercentOneChild As Double
    Private m_AWWPercentSpouseChild As Double
    Private m_AWWPercentSpouseOnly As Double
    Private m_PaymentMaxAmountSpouseChild As Double
    Private m_PaymentMaxAmountSpouseOnly As Double
    Private m_PaymentMinAmountSpouseOnly As Double
    Private m_SpousalRemarrEndBenefitCode As Integer
    Private m_SpousalRemarrPayLumpSumCode As Integer
    Private m_SpousalRemarrWeeksLumpSum As Integer
    Private m_TotalAmountSpouseChild As Double
    Private m_TotalAmountSpouseOnly As Double
    Private m_TotalTimeChildAgeNoSchool As Integer
    Private m_TotalWeeksChild As Integer
    Private m_TotalWeeksSpouse As Integer
    Private m_UseTTDCode As Integer

    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try

            AssignData = 0
            ClearObject()



            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                m_AWWPercentOneChild = objReader.GetInt32("PA_ONE_CHILD_PCENT")
                m_AWWPercentSpouseChild = objReader.GetInt32("PA_SPO_CHD_PCENT")
                m_AWWPercentSpouseOnly = objReader.GetInt32("PA_SPO_ONLY_PCENT")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_PaymentMaxAmountSpouseChild = objReader.GetInt32("PL_MAX_SPO_CHD_AMT")
                m_PaymentMaxAmountSpouseOnly = objReader.GetInt32("PL_MAX_SPO_ONY_AMT")
                m_PaymentMinAmountSpouseOnly = objReader.GetInt32("PL_MIN_SPO_ONY_AMT")
                m_SpousalRemarrEndBenefitCode = objReader.GetInt32("SR_BENFIT_END_CODE")
                m_SpousalRemarrPayLumpSumCode = objReader.GetInt32("SR_PAY_LMP_SM_CODE")
                m_SpousalRemarrWeeksLumpSum = objReader.GetInt32("SR_WEEKS_IN_LMP_SM")
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_TotalAmountSpouseChild = objReader.GetInt32("TT_SPO_CHD_AMT")
                m_TotalAmountSpouseOnly = objReader.GetInt32("TT_SPO_ONLY_AMT")
                m_TotalTimeChildAgeNoSchool = objReader.GetInt32("TT_CHILD_END_AGE")
                m_TotalWeeksChild = objReader.GetInt32("TT_CHILD_WEEKS")
                m_TotalWeeksSpouse = objReader.GetInt32("TT_SPO_WEEKS")
                m_UseTTDCode = objReader.GetInt32("USE_TTD_CODE")
            End While

            'objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            'If Not g_DBObject.DB_EOF(iRdset) Then
            '	m_AWWPercentOneChild = objReader.GetInt32( "PA_ONE_CHILD_PCENT")
            '	m_AWWPercentSpouseChild = objReader.GetInt32( "PA_SPO_CHD_PCENT")
            '	m_AWWPercentSpouseOnly = objReader.GetInt32( "PA_SPO_ONLY_PCENT")
            '	m_EffectiveDateDTG = objReader.GetString( "EFFECTIVE_DATE")
            '	m_JurisRowID = objReader.GetInt32( "JURIS_ROW_ID")
            '	m_PaymentMaxAmountSpouseChild = objReader.GetInt32( "PL_MAX_SPO_CHD_AMT")
            '	m_PaymentMaxAmountSpouseOnly = objReader.GetInt32( "PL_MAX_SPO_ONY_AMT")
            '	m_PaymentMinAmountSpouseOnly = objReader.GetInt32( "PL_MIN_SPO_ONY_AMT")
            '	m_SpousalRemarrEndBenefitCode = objReader.GetInt32( "SR_BENFIT_END_CODE")
            '	m_SpousalRemarrPayLumpSumCode = objReader.GetInt32( "SR_PAY_LMP_SM_CODE")
            '	m_SpousalRemarrWeeksLumpSum = objReader.GetInt32( "SR_WEEKS_IN_LMP_SM")
            '	m_TableRowID = objReader.GetInt32( "TABLE_ROW_ID")
            '	m_TotalAmountSpouseChild = objReader.GetInt32( "TT_SPO_CHD_AMT")
            '	m_TotalAmountSpouseOnly = objReader.GetInt32( "TT_SPO_ONLY_AMT")
            '	m_TotalTimeChildAgeNoSchool = objReader.GetInt32( "TT_CHILD_END_AGE")
            '	m_TotalWeeksChild = objReader.GetInt32( "TT_CHILD_WEEKS")
            '	m_TotalWeeksSpouse = objReader.GetInt32( "TT_SPO_WEEKS")
            '	m_UseTTDCode = objReader.GetInt32( "USE_TTD_CODE")
            'End If

            m_DataHasChanged = False
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID

            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeCloseRecordset(objReader)

            AssignData = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function ClearObject() As Integer
        'jtodd22 support for future interface
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_ErrorMaskJR = 0
        m_ErrorMaskSAWW = 0
        m_JurisDefinedWorkWeek = 0
        m_JurisRowID = 0
        m_MaxCompRateMonthly = 0
        m_MaxCompRateWeekly = 0
        m_NoCodeID = 0
        m_PayPeriodCode = 0
        m_PayPeriodLockedInCalcCode = 0
        m_RuleTotalWeeks = 0
        m_TableRowID = 0
        m_UseTwoThirdsCode = 0
        m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault
        m_YesCodeID = 0

        m_AWWPercentOneChild = 0
        m_AWWPercentSpouseChild = 0
        m_AWWPercentSpouseOnly = 0
        m_PaymentMaxAmountSpouseChild = 0
        m_PaymentMaxAmountSpouseOnly = 0
        m_PaymentMinAmountSpouseOnly = 0
        m_SpousalRemarrEndBenefitCode = 0
        m_SpousalRemarrPayLumpSumCode = 0
        m_SpousalRemarrWeeksLumpSum = 0
        m_TotalAmountSpouseChild = 0
        m_TotalAmountSpouseOnly = 0
        m_TotalTimeChildAgeNoSchool = 0
        m_TotalWeeksChild = 0
        m_TotalWeeksSpouse = 0
        m_UseTTDCode = 0


    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT "
        sSQL = sSQL & "TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",JURIS_ROW_ID,EFFECTIVE_DATE,DELETED_FLAG"
        sSQL = sSQL & ",PL_MAX_SPO_CHD_AMT"
        sSQL = sSQL & ",PL_MAX_SPO_ONY_AMT"
        sSQL = sSQL & ",PL_MIN_SPO_ONY_AMT"
        sSQL = sSQL & ",PA_SPO_CHD_PCENT"
        sSQL = sSQL & ",PA_SPO_ONLY_PCENT"
        sSQL = sSQL & ",PA_ONE_CHILD_PCENT"
        sSQL = sSQL & ",SR_BENFIT_END_CODE"
        sSQL = sSQL & ",SR_PAY_LMP_SM_CODE"
        sSQL = sSQL & ",SR_WEEKS_IN_LMP_SM"
        sSQL = sSQL & ",TT_SPO_WEEKS"
        sSQL = sSQL & ",TT_CHILD_WEEKS"
        sSQL = sSQL & ",TT_CHILD_END_AGE"
        sSQL = sSQL & ",TT_SPO_CHD_AMT"
        sSQL = sSQL & ",TT_SPO_ONLY_AMT"
        sSQL = sSQL & ",USE_TTD_CODE"
        GetSQLFieldList = sSQL


    End Function
    Public Function LoadDataTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataTableRowID"
        Dim sSQL As String
        Try
            LoadDataTableRowID = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataTableRowID = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim sSQL As String
        Dim sSQL2 As String
        Try
            LoadDataByEventDate = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            LoadDataByEventDate = AssignData(objUser, sSQL)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"

        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try

            SaveData = 0

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)

                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("PA_ONE_CHILD_PCENT").Value = m_AWWPercentOneChild
                objWriter.Fields("PA_SPO_CHD_PCENT").Value = m_AWWPercentSpouseChild
                objWriter.Fields("PA_SPO_ONLY_PCENT").Value = m_AWWPercentSpouseOnly
                objWriter.Fields("PL_MAX_SPO_CHD_AMT").Value = m_PaymentMaxAmountSpouseChild
                objWriter.Fields("PL_MAX_SPO_ONY_AMT").Value = m_PaymentMaxAmountSpouseOnly
                objWriter.Fields("PL_MIN_SPO_ONY_AMT").Value = m_PaymentMinAmountSpouseOnly
                objWriter.Fields("SR_BENFIT_END_CODE").Value = m_SpousalRemarrEndBenefitCode
                objWriter.Fields("SR_PAY_LMP_SM_CODE").Value = m_SpousalRemarrPayLumpSumCode
                objWriter.Fields("SR_WEEKS_IN_LMP_SM").Value = m_SpousalRemarrWeeksLumpSum
                objWriter.Fields("TT_SPO_CHD_AMT").Value = m_TotalAmountSpouseChild
                objWriter.Fields("TT_SPO_ONLY_AMT").Value = m_TotalAmountSpouseOnly
                objWriter.Fields("TT_CHILD_END_AGE").Value = m_TotalTimeChildAgeNoSchool
                objWriter.Fields("TT_CHILD_WEEKS").Value = m_TotalWeeksChild
                objWriter.Fields("TT_SPO_WEEKS").Value = m_TotalWeeksSpouse
                objWriter.Fields("USE_TTD_CODE").Value = m_UseTTDCode
                objWriter.Execute()
            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("PA_ONE_CHILD_PCENT", m_AWWPercentOneChild)
                objWriter.Fields.Add("PA_SPO_CHD_PCENT", m_AWWPercentSpouseChild)
                objWriter.Fields.Add("PA_SPO_ONLY_PCENT", m_AWWPercentSpouseOnly)
                objWriter.Fields.Add("PL_MAX_SPO_CHD_AMT", m_PaymentMaxAmountSpouseChild)
                objWriter.Fields.Add("PL_MAX_SPO_ONY_AMT", m_PaymentMaxAmountSpouseOnly)
                objWriter.Fields.Add("PL_MIN_SPO_ONY_AMT", m_PaymentMinAmountSpouseOnly)
                objWriter.Fields.Add("SR_BENFIT_END_CODE", m_SpousalRemarrEndBenefitCode)
                objWriter.Fields.Add("SR_PAY_LMP_SM_CODE", m_SpousalRemarrPayLumpSumCode)
                objWriter.Fields.Add("SR_WEEKS_IN_LMP_SM", m_SpousalRemarrWeeksLumpSum)
                objWriter.Fields.Add("TT_SPO_CHD_AMT", m_TotalAmountSpouseChild)
                objWriter.Fields.Add("TT_SPO_ONLY_AMT", m_TotalAmountSpouseOnly)
                objWriter.Fields.Add("TT_CHILD_END_AGE", m_TotalTimeChildAgeNoSchool)
                objWriter.Fields.Add("TT_CHILD_WEEKS", m_TotalWeeksChild)
                objWriter.Fields.Add("TT_SPO_WEEKS", m_TotalWeeksSpouse)
                objWriter.Fields.Add("USE_TTD_CODE", m_UseTTDCode)
                objWriter.Execute()

            End If
            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)
        End Try


    End Function

    Public Function ValidateData() As Integer
        Dim sFormatted As String

        m_Warning = ""
        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required."
        End If
        ValidateData = 0


    End Function

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property AWWPercentOneChild() As Double
        Get
            AWWPercentOneChild = m_AWWPercentOneChild
        End Get
        Set(ByVal Value As Double)
            m_AWWPercentOneChild = Value
        End Set
    End Property

    Public Property AWWPercentSpouseChild() As Double
        Get
            AWWPercentSpouseChild = m_AWWPercentSpouseChild
        End Get
        Set(ByVal Value As Double)
            m_AWWPercentSpouseChild = Value
        End Set
    End Property

    Public Property AWWPercentSpouseOnly() As Double
        Get
            AWWPercentSpouseOnly = m_AWWPercentSpouseOnly
        End Get
        Set(ByVal Value As Double)
            m_AWWPercentSpouseOnly = Value
        End Set
    End Property


    Public Property PaymentMaxAmountSpouseChild() As Double
        Get
            PaymentMaxAmountSpouseChild = m_PaymentMaxAmountSpouseChild
        End Get
        Set(ByVal Value As Double)
            m_PaymentMaxAmountSpouseChild = Value
        End Set
    End Property

    Public Property MaxCompRateMonthly() As Double
        Get
            MaxCompRateMonthly = m_MaxCompRateMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateMonthly = Value
        End Set
    End Property

    Public Property MaxCompRateWeekly() As Double
        Get
            MaxCompRateWeekly = m_MaxCompRateWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateWeekly = Value
        End Set
    End Property

    Public Property PaymentMaxAmountSpouseOnly() As Double
        Get
            PaymentMaxAmountSpouseOnly = m_PaymentMaxAmountSpouseOnly
        End Get
        Set(ByVal Value As Double)
            m_PaymentMaxAmountSpouseOnly = Value
        End Set
    End Property

    Public Property PaymentMinAmountSpouseOnly() As Double
        Get
            PaymentMinAmountSpouseOnly = m_PaymentMinAmountSpouseOnly
        End Get
        Set(ByVal Value As Double)
            m_PaymentMinAmountSpouseOnly = Value
        End Set
    End Property

    Public Property TotalAmountSpouseChild() As Double
        Get
            TotalAmountSpouseChild = m_TotalAmountSpouseChild
        End Get
        Set(ByVal Value As Double)
            m_TotalAmountSpouseChild = Value
        End Set
    End Property

    Public Property TotalAmountSpouseOnly() As Double
        Get
            TotalAmountSpouseOnly = m_TotalAmountSpouseOnly
        End Get
        Set(ByVal Value As Double)
            m_TotalAmountSpouseOnly = Value
        End Set
    End Property

    Public Property TotalTimeChildAgeNoSchool() As Integer
        Get
            TotalTimeChildAgeNoSchool = m_TotalTimeChildAgeNoSchool
        End Get
        Set(ByVal Value As Integer)
            m_TotalTimeChildAgeNoSchool = Value
        End Set
    End Property

    Public Property TotalWeeksChild() As Integer
        Get
            TotalWeeksChild = m_TotalWeeksChild
        End Get
        Set(ByVal Value As Integer)
            m_TotalWeeksChild = Value
        End Set
    End Property

    Public Property TotalWeeksSpouse() As Integer
        Get
            TotalWeeksSpouse = m_TotalWeeksSpouse
        End Get
        Set(ByVal Value As Integer)
            m_TotalWeeksSpouse = Value
        End Set
    End Property

    Public Property SpousalRemarrEndBenefitCode() As Integer
        Get
            SpousalRemarrEndBenefitCode = m_SpousalRemarrEndBenefitCode
        End Get
        Set(ByVal Value As Integer)
            m_SpousalRemarrEndBenefitCode = Value
        End Set
    End Property

    Public Property SpousalRemarrPayLumpSumCode() As Integer
        Get
            SpousalRemarrPayLumpSumCode = m_SpousalRemarrPayLumpSumCode
        End Get
        Set(ByVal Value As Integer)
            m_SpousalRemarrPayLumpSumCode = Value
        End Set
    End Property

    Public Property SpousalRemarrWeeksLumpSum() As Integer
        Get
            SpousalRemarrWeeksLumpSum = m_SpousalRemarrWeeksLumpSum
        End Get
        Set(ByVal Value As Integer)
            m_SpousalRemarrWeeksLumpSum = Value
        End Set
    End Property

    Public Property UseTTDCode() As Integer
        Get
            UseTTDCode = m_UseTTDCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTTDCode = Value
        End Set
    End Property

    Public Property ErrorMaskJR() As Integer
        Get
            ErrorMaskJR = m_ErrorMaskJR
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskJR = Value
        End Set
    End Property

    Public Property ErrorMaskSAWW() As Integer
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property PayPeriodLockedInCalcCode() As Integer
        Get
            PayPeriodLockedInCalcCode = m_PayPeriodLockedInCalcCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodLockedInCalcCode = Value
        End Set
    End Property

    Public Property RuleTotalWeeks() As Double
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseTwoThirdsCode() As Integer
        Get
            UseTwoThirdsCode = m_UseTwoThirdsCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTwoThirdsCode = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
End Class

