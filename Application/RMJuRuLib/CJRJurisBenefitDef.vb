Option Strict Off
Option Explicit On
Public Class CJRJurisBenefitDef
    Private Const sClassName As String = "CJRJurisBenefitDef"
    Const sTableName As String = "WCP_BENEFIT_LKUP"

    'jtodd22 for primitive interface
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_JurisRowID As Integer
    Private m_TableRowID As Integer
    Private m_Warning As String

    'Class properties, local copy
    Private m_Abbreviation As String
    Private m_AbbreviationRowID As Integer
    Private m_BenefitLookUpID As Integer
    Private m_BenefitDesc As String
    Private m_TabIndex As Integer
    Private m_TabCaption As String
    Private m_UseInCalculator As Integer
    Private m_TypeDescRowID As Integer
    Private m_objUser As Riskmaster.Security.UserLogin

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()

        'UPGRADE_NOTE: Object m_objUser may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        m_objUser = Nothing

        'UPGRADE_NOTE: Object m_objUser may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        m_objUser = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub


    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader

        Try

            AssignData = 0

            ClearObject()
            m_objUser = objUser



            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                'm_EffectiveDateDTG = objReader.GetString( "EFFECTIVE_DATE") & "")
                m_DeletedFlag = objReader.GetInt32("DELETE_FLAG")
                m_TabIndex = objReader.GetInt32("TAB_INDEX")
                m_TabCaption = objReader.GetString("TAB_CAPTION")
                m_UseInCalculator = objReader.GetInt32("USE_IN_CALCULATOR")
                m_BenefitDesc = objReader.GetString("JURIS_BENEFIT_DESC")
                m_BenefitLookUpID = objReader.GetInt32("BENEFIT_LKUP_ID")
                m_Abbreviation = objReader.GetString("ABBREVIATION")
                m_TypeDescRowID = objReader.GetInt32("TYPE_DESC_ROW_ID")
                m_AbbreviationRowID = objReader.GetInt32("ABBRE_TABLE_ROW_ID")
            End If
            If m_UseInCalculator = 0 Then m_UseInCalculator = GetNoCodeID()
            If m_UseInCalculator = -1 Then m_UseInCalculator = GetYesCodeID()

            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Private Function ClearObject() As Integer

        m_Abbreviation = ""
        m_AbbreviationRowID = 0
        m_BenefitLookUpID = 0
        m_BenefitDesc = ""
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_JurisRowID = 0
        m_TabIndex = 0
        m_TabCaption = ""
        m_TableRowID = 0
        m_TypeDescRowID = 0
        m_UseInCalculator = 0



    End Function

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property TypeDescRowID() As Integer
        Get
            TypeDescRowID = m_TypeDescRowID
        End Get
        Set(ByVal Value As Integer)
            m_TypeDescRowID = Value
        End Set
    End Property

    Public Property Abbreviation() As String
        Get
            Abbreviation = m_Abbreviation
        End Get
        Set(ByVal Value As String)
            m_Abbreviation = Value
        End Set
    End Property

    Public Property BenefitDesc() As String
        Get
            BenefitDesc = m_BenefitDesc
        End Get
        Set(ByVal Value As String)
            m_BenefitDesc = Value
        End Set
    End Property

    Public Property BenefitLookUpID() As Integer
        Get
            BenefitLookUpID = m_BenefitLookUpID
        End Get
        Set(ByVal Value As Integer)
            m_BenefitLookUpID = Value
        End Set
    End Property

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property TabIndex() As Integer
        Get
            TabIndex = m_TabIndex
        End Get
        Set(ByVal Value As Integer)
            m_TabIndex = Value
        End Set
    End Property

    Public Property TabCaption() As String
        Get
            TabCaption = m_TabCaption
        End Get
        Set(ByVal Value As String)
            m_TabCaption = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property UseInCalculator() As Integer
        Get
            UseInCalculator = m_UseInCalculator
        End Get
        Set(ByVal Value As Integer)
            m_UseInCalculator = Value
        End Set
    End Property

    Public Property AbbreviationRowID() As Integer
        Get
            AbbreviationRowID = m_AbbreviationRowID
        End Get
        Set(ByVal Value As Integer)
            m_AbbreviationRowID = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property objUser() As Riskmaster.Security.UserLogin
        Get
            objUser = m_objUser
        End Get
        Set(ByVal Value As Riskmaster.Security.UserLogin)
            m_objUser = Value
        End Set
    End Property

    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lBenefitLkupID As Integer) As Integer
        Dim sSQL As String
        Try

            LoadData = 0

            sSQL = GetSQL()
            sSQL = sSQL & "WHERE BENEFIT_LKUP_ID = " & lBenefitLkupID

            LoadData = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadDataByBenefitID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lBenefitID As Integer) As Integer
        Dim sSQL As String

        Try

            LoadDataByBenefitID = 0

            sSQL = GetSQL()
            sSQL = sSQL & " WHERE BENEFIT_LKUP_ID = " & lBenefitID

            LoadDataByBenefitID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByBenefitID = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadDataByBenefitID|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        Try

            SaveData = 0

            sSQL = GetSQL()
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID



            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                'g_dbObject.DB_PutData iRdSet, "EFFECTIVE_DATE", m_EffectiveDateDTG
                objWriter.Fields("DELETE_FLAG").Value = m_DeletedFlag
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("TAB_INDEX").Value = m_TabIndex
                objWriter.Fields("TAB_CAPTION").Value = m_TabCaption
                objWriter.Fields("USE_IN_CALCULATOR").Value = m_UseInCalculator
                objWriter.Fields("JURIS_BENEFIT_DESC").Value = m_BenefitDesc
                objWriter.Fields("ABBREVIATION").Value = m_Abbreviation
                objWriter.Fields("TYPE_DESC_ROW_ID").Value = m_TypeDescRowID
                objWriter.Fields("ABBRE_TABLE_ROW_ID").Value = m_AbbreviationRowID
                objWriter.Execute()
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                lTest = lGetNextUID(sTableName)
                objWriter.Fields.Add("BENEFIT_LKUP_ID", lTest)
                objWriter.Fields.Add("TABLE_ROW_ID", lTest)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DELETE_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("TAB_INDEX", m_TabIndex)
                objWriter.Fields.Add("TAB_CAPTION", m_TabCaption)
                objWriter.Fields.Add("USE_IN_CALCULATOR", m_UseInCalculator)
                objWriter.Fields.Add("JURIS_BENEFIT_DESC", m_BenefitDesc)
                objWriter.Fields.Add("ABBREVIATION", m_Abbreviation)
                objWriter.Fields.Add("TYPE_DESC_ROW_ID", m_TypeDescRowID)
                objWriter.Fields.Add("ABBRE_TABLE_ROW_ID", m_AbbreviationRowID)
                objWriter.Execute()
            End If

            SaveData = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeCloseRecordset(objReader)

            SaveData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            If InStr(1, UCase(g_sErrDescription), UCase("UNIQUE CONSTRAINT")) > 0 Then
                LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                SaveData = 89999
                Exit Function
            End If
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Public Function ValidateData() As Integer
        Const sFunctionName As String = "ValidateData"
        Dim cBenefitTypes As CJRBenefitTypes
        Dim lIndex As Integer
        Dim lReturn As Integer
        Dim lTypeA As Integer
        Dim lTypeB As Integer

        Try

            ValidateData = 0
            m_Warning = ""
            If m_TypeDescRowID < 1 Then
                m_Warning = m_Warning & "A Transaction Type is required." & vbCrLf
            End If
            If m_BenefitDesc = "" Then
                m_Warning = m_Warning & "A Transaction Description Is required." & vbCrLf
            End If
            If m_UseInCalculator < 1 Then
                m_Warning = m_Warning & "A 'Use In Calculator' selection is required." & vbCrLf
            End If

            If m_Abbreviation > "" Then
                If Me.CheckForExistingAbbr(m_objUser) = -1 Then
                    m_Warning = m_Warning & vbCrLf
                    m_Warning = m_Warning & "The Abbreviation is in Use." & vbCrLf & vbCrLf
                    m_Warning = m_Warning & "A jurisdiction can not have two" & vbCrLf
                    m_Warning = m_Warning & "benefits/transaction types with the same Abbreviation." & vbCrLf
                End If
            End If

            If Trim(m_Abbreviation & "") = "" Then
                'jtodd22 02/23/2007 check against transaction type
                cBenefitTypes = New RMJuRuLib.CJRBenefitTypes
                lReturn = cBenefitTypes.LoadData(objUser)
                For lIndex = 1 To cBenefitTypes.Count
                    If InStr(1, UCase(cBenefitTypes.Item(lIndex).TypeDesc), UCase("Reportable Expense"), CompareMethod.Text) > 0 Then
                        lTypeA = cBenefitTypes.Item(lIndex).TableRowID
                        Exit For
                    End If
                Next lIndex
                For lIndex = 1 To cBenefitTypes.Count
                    If InStr(1, UCase(cBenefitTypes.Item(lIndex).TypeDesc), UCase("Penalty"), CompareMethod.Text) > 0 Then
                        lTypeB = cBenefitTypes.Item(lIndex).TableRowID
                        Exit For
                    End If
                Next lIndex

                If lTypeA <> m_TypeDescRowID And lTypeB <> m_TypeDescRowID Then
                    m_Warning = "This type of transaction is required to have an abbreviation."
                End If

            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            ValidateData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            If InStr(1, UCase(g_sErrDescription), UCase("UNIQUE CONSTRAINT")) > 0 Then
                LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                Exit Function
            End If
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Private Function GetSQL() As String
        Dim sSQL As String
        GetSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED, ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD, UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", DELETE_FLAG"
        'sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", ABBREVIATION"
        sSQL = sSQL & ", BENEFIT_LKUP_ID"
        sSQL = sSQL & ", JURIS_BENEFIT_DESC"
        sSQL = sSQL & ", TAB_INDEX"
        sSQL = sSQL & ", TAB_CAPTION"
        sSQL = sSQL & ", USE_IN_CALCULATOR"
        sSQL = sSQL & ", TYPE_DESC_ROW_ID"
        sSQL = sSQL & ", ABBRE_TABLE_ROW_ID"
        sSQL = sSQL & " FROM " & sTableName
        GetSQL = sSQL


    End Function
    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Dim sSQL As String

        Try

            LoadDataByTableRowID = 0

            sSQL = GetSQL()
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataByTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadDataByTableRowID|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function CheckForExistingAbbr(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Dim objReader As DbReader
        Dim lTmp As Object
        Dim sSQL As String
        Try

            CheckForExistingAbbr = 0



            sSQL = GetSQL()
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & m_JurisRowID
            sSQL = sSQL & " AND ABBREVIATION = '" & m_Abbreviation & "'"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                'UPGRADE_WARNING: Couldn't resolve default property of object lTmp. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                lTmp = objReader.GetInt32("TABLE_ROW_ID")
                'UPGRADE_WARNING: Couldn't resolve default property of object lTmp. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If lTmp <> m_TableRowID Then
                    CheckForExistingAbbr = -1
                    Exit Function
                End If

            End While

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            CheckForExistingAbbr = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".CheckForExistingAbbr|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function

    Public Function DeleteRecord(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer

        Try

            Select Case m_Abbreviation
                Case "TPD" 'shared with TTD

                Case Else
            End Select
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            DeleteRecord = g_lErrNum
            'SafeDropRecordset iRdset

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".DeleteRecord|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'SafeDropRecordset iRdset


        End Try

    End Function
End Class

