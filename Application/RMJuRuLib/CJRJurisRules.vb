Option Strict Off
Option Explicit On
Public Class CJRJurisRules
    Const sClassName As String = "CJRJurisRules"

    Public Property BatchMode() As Boolean
        Get
            BatchMode = g_bBatchMode
        End Get
        Set(ByVal Value As Boolean)
            g_bBatchMode = Value
        End Set
    End Property
End Class

