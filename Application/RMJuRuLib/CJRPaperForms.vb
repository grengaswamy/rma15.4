Option Strict Off
Option Explicit On
Public Class CJRPaperForms
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CJRPaperForms"

    'Class properties, local copy
    Private mCol As Collection
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objPaperForm As CJRPaperForm
        Try

            LoadData = 0



            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " FORM_CAT_DESC, FORM_ID, FORM_TITLE, FORM_NAME"
            sSQL = sSQL & " FROM WCP_FORMS, WCP_FORMS_CAT_LKUP"
            sSQL = sSQL & " WHERE WCP_FORMS.FORM_CATEGORY = WCP_FORMS_CAT_LKUP.FORM_CAT"
            sSQL = sSQL & " AND WCP_FORMS.STATE_ROW_ID = WCP_FORMS_CAT_LKUP.STATE_ROW_ID"
            sSQL = sSQL & " AND WCP_FORMS.STATE_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND PRIMARY_FORM_FLAG <> 0"
            sSQL = sSQL & " ORDER BY FORM_CAT_DESC, FORM_ID, FORM_TITLE, FORM_NAME"
            'jlt 07/28/2004 Note the double condition for the join.  It is required or a DISTINCT.

            'objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            'If Not g_DBObject.DB_EOF(iRdSet) Then
            '	While objReader.Read()
            '		objPaperForm = New CJRPaperForm
            '		With objPaperForm
            '			.FormCategoryName = Trim(objReader.GetString( "FORM_CAT_DESC"))
            '			.FormID = objReader.GetInt32( "FORM_ID")
            '			.FormName = Trim(objReader.GetString( "FORM_NAME"))
            '			.FormTitle = Trim(objReader.GetString( "FORM_TITLE"))
            '			Add(.FormName, .FormID, .FormCategoryName, .FormTitle, "k" & CStr(.FormID))
            '		End With
            '		'UPGRADE_NOTE: Object objPaperForm may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            '		objPaperForm = Nothing
            '		
            '	End While
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objPaperForm = New CJRPaperForm
                With objPaperForm
                    .FormCategoryName = Trim(objReader.GetString("FORM_CAT_DESC"))
                    .FormID = objReader.GetInt32("FORM_ID")
                    .FormName = Trim(objReader.GetString("FORM_NAME"))
                    .FormTitle = Trim(objReader.GetString("FORM_TITLE"))
                    Add(.FormName, .FormID, .FormCategoryName, .FormTitle, "k" & CStr(.FormID))
                End With
                'UPGRADE_NOTE: Object objPaperForm may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objPaperForm = Nothing
            End While



            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function Add(ByRef FormName As String, ByRef FormID As Integer, ByRef FormCategoryName As String, ByRef FormTitle As String, Optional ByRef sKey As String = "") As CJRPaperForm
        Const sFunctionName As String = "Add"
        Dim objNewMember As CJRPaperForm
        Try

            objNewMember = New CJRPaperForm


            'set the properties passed into the method
            objNewMember.FormName = FormName
            objNewMember.FormID = FormID
            objNewMember.FormCategoryName = FormCategoryName
            objNewMember.FormTitle = FormTitle
            If Len(sKey) = 0 Then
                mCol.Add(objNewMember)
            Else
                mCol.Add(objNewMember, sKey)
            End If


            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRPaperForm
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

