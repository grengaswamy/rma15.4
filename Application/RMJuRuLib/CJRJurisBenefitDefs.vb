Option Strict Off
Option Explicit On
Public Class CJRJurisBenefitDefs
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRJurisBenefitDefs"
    Private Const sTableName As String = "WCP_BENEFIT_LKUP"
    Dim m_sSQL As String
    Private mCol As Collection

    Public Function Add(ByRef Abbreviation As String, ByRef BenefitDesc As String, ByRef BenefitLookUpID As Integer, ByRef DeleteFlag As Integer, ByRef EffectiveDateDTG As String, ByRef JurisRowID As Integer, ByRef TabCaption As String, ByRef TabIndex As Integer, ByRef TableRowID As Integer, ByRef UseInCalculator As Integer, ByRef TypeDescRowID As Integer, ByRef sKey As String) As CJRJurisBenefitDef

        Dim sJurisdiction As String

        Try
            'create a new object
            Dim objNewMember As CJRJurisBenefitDef
            objNewMember = New CJRJurisBenefitDef

            sJurisdiction = modFunctions.GetStateName_SQL(JurisRowID)

            'set the properties passed into the method
            With objNewMember
                .Abbreviation = Abbreviation
                .BenefitDesc = BenefitDesc
                .BenefitLookUpID = BenefitLookUpID
                .DeletedFlag = DeleteFlag
                .EffectiveDateDTG = EffectiveDateDTG
                .JurisRowID = JurisRowID
                .TabCaption = TabCaption
                .TabIndex = TabIndex
                .TableRowID = TableRowID
                .TypeDescRowID = TypeDescRowID
                .UseInCalculator = UseInCalculator
            End With

            mCol.Add(objNewMember, sKey)

            Add = objNewMember

        Catch ex As Exception
            Dim sErrMessage As String
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                Select Case g_lErrNum
                    Case 457
                        g_sErrDescription = Err.Description & "; (" & sJurisdiction & "); Key = " & sKey & "; SQL = " & m_sSQL
                    Case Else
                        g_sErrDescription = Err.Description
                End Select
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : LoadData
    ' DateTime  : 6/7/2004 16:31
    ' Author    : jlt
    ' Purpose   :
    ' Notes     : 06/07/2004 jlt.  There are three(5) definition types.
    ' Notes     : 06/07/2004 jlt.  1 is not used to avoid confusion with Abs(boolean = -1)
    ' Notes     : 06/07/2004 jlt.  2 = Commutation of Indemnity Benefit
    ' Notes     : 06/07/2004 jlt.  3 = Indemnity Benefit
    ' Notes     : 06/07/2004 jlt.  4 = Penalty of any kind
    ' Notes     : 07/12/2004 jlt.  9 = Use to fetch all Benefit related definitions
    ' Note      : jtodd22 12/01/2007
    '...........: Added trap for database with duplicate records
    '---------------------------------------------------------------------------------------
    '
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef lDefinitionType As Integer, ByRef lUseInCalculator As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim lCurrentTableRowID As Integer
        Dim lOldTableRowID As Integer
        Dim objBenefit As CJRJurisBenefitDef
        Dim sText As String
        Try

            LoadData = 0

            g_lErrNum = 0
            g_sErrDescription = ""
            If InStr(1, "2,3,4,5,6,9,99", CStr(lDefinitionType)) = 0 Then
                g_lErrNum = 70010
                g_sErrDescription = "Invalid Benefit Definition Type, Type = " & lDefinitionType
                g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
                g_lErrLine = Erl()
                LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                Err.Raise(vbObjectError + g_lErrNum, sClassName & "." & sFunctionName, g_sErrDescription)
                Exit Function
            End If

            m_sSQL = GetSQL()
            m_sSQL = m_sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            Select Case lUseInCalculator
                Case -1
                    m_sSQL = m_sSQL & " AND (USE_IN_CALCULATOR = " & GetYesCodeID() & "OR USE_IN_CALCULATOR = -1)"
                Case 0
                    'jtodd22 12/22/2004--do not code, not a valid criteria
                Case Else
                    m_sSQL = m_sSQL & " AND USE_IN_CALCULATOR = " & lUseInCalculator
            End Select
            Select Case lDefinitionType
                Case 0
                    'not used
                Case 1
                    'not used
                Case 2
                    m_sSQL = m_sSQL & " AND TYPE_DESC_ROW_ID = " & lDefinitionType
                Case 3
                    m_sSQL = m_sSQL & " AND TYPE_DESC_ROW_ID = " & lDefinitionType
                Case 4, 5
                    m_sSQL = m_sSQL & " AND TYPE_DESC_ROW_ID = " & lDefinitionType
                Case 99
                    m_sSQL = m_sSQL & " AND TYPE_DESC_ROW_ID IN (2,3,6)"
                Case Else
            End Select
            m_sSQL = m_sSQL & " ORDER BY JURIS_BENEFIT_DESC ASC"

            objReader = DbFactory.GetDbReader(g_ConnectionString, m_sSQL)
            mCol = New Collection

            lOldTableRowID = -1
            While objReader.Read()
                objBenefit = New CJRJurisBenefitDef
                With objBenefit
                    .BenefitLookUpID = objReader.GetInt32("BENEFIT_LKUP_ID")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    'UPGRADE_WARNING: Use of Null/IsNull() detected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="2EED02CB-5C0E-4DC1-AE94-4FAA3A30F51A"'
                    If IsDBNull(.TableRowID) Or .TableRowID = 0 Then
                        .TableRowID = .BenefitLookUpID
                    End If

                    lCurrentTableRowID = .TableRowID

                    If lOldTableRowID = lCurrentTableRowID Then
                        'jtodd22 12/01/2007 this is fatal to the application
                        'as duplicates are an error.
                        'UPGRADE_NOTE: Object objBenefit may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                        objBenefit = Nothing
                        SafeCloseRecordset(objReader)

                        sText = ""
                        sText = sText & "There are duplicate records in the database table " & sTableName & "."
                        sText = sText & "The table's Primary Key is missing or failed."
                        g_sErrProcedure = g_sErrSrc & "|" & "RMJuRuLib." & sClassName & ".LoadData|"
                        LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, sText)
                        g_sErrDescription = sText
                        Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
                    End If
                    .Abbreviation = objReader.GetString("ABBREVIATION")
                    .BenefitDesc = objReader.GetString("JURIS_BENEFIT_DESC")
                    .DeletedFlag = objReader.GetInt32("DELETE_FLAG")
                    '.EffectiveDateDTG = objReader.GetString( "EFFECTIVE_DATE") & "")
                    .JurisRowID = lJurisRowID
                    .TabCaption = objReader.GetString("TAB_CAPTION")
                    .TabIndex = objReader.GetInt32("TAB_INDEX")
                    .TypeDescRowID = objReader.GetInt32("TYPE_DESC_ROW_ID")
                    .UseInCalculator = objReader.GetInt32("USE_IN_CALCULATOR")
                    If .UseInCalculator = 0 Then .UseInCalculator = GetNoCodeID()
                    If .UseInCalculator = -1 Then .UseInCalculator = GetYesCodeID()

                    Add(.Abbreviation, .BenefitDesc, .BenefitLookUpID, .DeletedFlag, .EffectiveDateDTG, .JurisRowID, .TabCaption, .TabIndex, .TableRowID, .UseInCalculator, .TypeDescRowID, "k" & CStr(.TableRowID))
                End With
                lOldTableRowID = lCurrentTableRowID

            End While
            'UPGRADE_NOTE: Object objBenefit may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objBenefit = Nothing
            LoadData = -1
        Catch ex As Exception
            Dim sErrMessage As String
            With Err()
                If g_lErrNum < 1 Then
                    g_lErrNum = Err.Number
                End If
                g_sErrSrc = .Source
                If g_sErrDescription = "" Then
                    g_sErrDescription = Err.Description
                Else
                    g_sErrDescription = g_sErrDescription & "; " & Err.Description
                End If
            End With
            LoadData = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            sErrMessage = g_sErrDescription
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, sErrMessage)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRJurisBenefitDef
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    Private Function GetSQL() As String
        Dim sSQL As String
        GetSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED, ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD, UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", DELETE_FLAG"
        'sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", ABBREVIATION"
        sSQL = sSQL & ", BENEFIT_LKUP_ID"
        sSQL = sSQL & ", JURIS_BENEFIT_DESC"
        sSQL = sSQL & ", TAB_INDEX"
        sSQL = sSQL & ", TAB_CAPTION"
        sSQL = sSQL & ", USE_IN_CALCULATOR"
        sSQL = sSQL & ", TYPE_DESC_ROW_ID"

        sSQL = sSQL & " FROM WCP_BENEFIT_LKUP"
        GetSQL = sSQL


    End Function
End Class

