Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleDeath
    Private Const sClassName As String = "CJRBenefitRuleDeath"
    Private Const sTableName As String = "WCP_DETHB_RULE"
    'local variable(s) not publicly exposed
    Const dWeekToMonthConvFactorDefault As Double = 4.34821
    'to support future interface
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_ErrorMaskJR As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_ErrorMaskSAWW As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_JurisDefinedWorkWeek As Integer 'jtodd22 03/08/2005 not part of the rule, but used as part of rule in RMWCCalc.dll
    Private m_JurisRowID As Integer
    Private m_MaxCompRateMonthly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MaxCompRateWeekly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_NoCodeID As Integer
    Private m_PayPeriodCode As Integer
    Private m_PayPeriodLockedInCalcCode As Integer
    Private m_RuleTotalWeeks As Double
    Private m_TableRowID As Integer
    Private m_UseTwoThirdsCode As Integer
    Private m_Warning As String
    Private m_WeekToMonthConvFactor As Double
    Private m_YesCodeID As Integer

    Private m_FixedAmount As Double
    Private m_MaxAmount As Double
    Private m_PayFixedAmountCode As Integer
    Private m_PayToEstateCode As Integer
    Private m_PayToPersonCode As Integer
    Private m_PayVariableAmountCode As Integer
    Private m_TimesClaimantAWW As Integer
    Private m_TimesSAWW As Integer
    Private m_FixedAmountDownState As Double
    Private m_MaxAmountDownState As Double
    Private m_PayTransportationCode As Integer
    Private m_MaxTransportAmount As Double

    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try

            AssignData = 0
            ClearObject()


            'objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            'If Not g_DBObject.DB_EOF(iRdset) Then
            '	m_DataHasChanged = False
            '	m_DeletedFlag = objReader.GetInt32( "DELETED_FLAG")
            '	m_EffectiveDateDTG = objReader.GetString( "EFFECTIVE_DATE")
            '	m_FixedAmount = objReader.GetDouble( "FIXED_AMOUNT")
            '	m_FixedAmountDownState = objReader.GetDouble( "FIXED_AMOUNT_DOWN")
            '	m_JurisRowID = objReader.GetInt32( "JURIS_ROW_ID")
            '	m_MaxAmount = objReader.GetDouble( "MAX_AMOUNT")
            '	m_MaxAmountDownState = objReader.GetDouble( "MAX_AMOUNT_DOWN")
            '	m_MaxTransportAmount = objReader.GetDouble( "MAX_AMOUNT_TRANS")
            '	m_PayFixedAmountCode = objReader.GetInt32( "PAY_FIX_AMT_CODE")
            '	m_PayTransportationCode = objReader.GetInt32( "PAY_TRANS_AMT_CODE")
            '	m_PayToEstateCode = objReader.GetInt32( "PAY_TO_ESTATE_CODE")
            '	m_PayToPersonCode = objReader.GetInt32( "PAY_TO_PERSON_CODE")
            '	m_PayVariableAmountCode = objReader.GetInt32( "PAY_VAR_AMT_CODE")
            '	m_TableRowID = objReader.GetInt32( "TABLE_ROW_ID")
            '	m_TimesClaimantAWW = objReader.GetInt32( "TIMES_CLMANT_AWW")
            '	m_TimesSAWW = objReader.GetInt32( "TIMES_SAWW")
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_DataHasChanged = False
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_FixedAmount = objReader.GetDouble("FIXED_AMOUNT")
                m_FixedAmountDownState = objReader.GetDouble("FIXED_AMOUNT_DOWN")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_MaxAmount = objReader.GetDouble("MAX_AMOUNT")
                m_MaxAmountDownState = objReader.GetDouble("MAX_AMOUNT_DOWN")
                m_MaxTransportAmount = objReader.GetDouble("MAX_AMOUNT_TRANS")
                m_PayFixedAmountCode = objReader.GetInt32("PAY_FIX_AMT_CODE")
                m_PayTransportationCode = objReader.GetInt32("PAY_TRANS_AMT_CODE")
                m_PayToEstateCode = objReader.GetInt32("PAY_TO_ESTATE_CODE")
                m_PayToPersonCode = objReader.GetInt32("PAY_TO_PERSON_CODE")
                m_PayVariableAmountCode = objReader.GetInt32("PAY_VAR_AMT_CODE")
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_TimesClaimantAWW = objReader.GetInt32("TIMES_CLMANT_AWW")
                m_TimesSAWW = objReader.GetInt32("TIMES_SAWW")
            End If



            m_DataHasChanged = False
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID

            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'UPGRADE_WARNING: Couldn't resolve default property of object Err.numb. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'AssignData = Err.numb
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function ClearObject() As Integer
        'jtodd22 support for future interface
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_ErrorMaskJR = 0
        m_ErrorMaskSAWW = 0
        m_JurisDefinedWorkWeek = 0
        m_JurisRowID = 0
        m_MaxCompRateMonthly = 0
        m_MaxCompRateWeekly = 0
        m_NoCodeID = 0
        m_PayPeriodCode = 0
        m_PayPeriodLockedInCalcCode = 0
        m_RuleTotalWeeks = 0
        m_TableRowID = 0
        m_UseTwoThirdsCode = 0
        m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault
        m_YesCodeID = 0

        m_PayFixedAmountCode = 0
        m_PayToEstateCode = 0
        m_PayToPersonCode = 0
        m_PayVariableAmountCode = 0
        m_FixedAmount = 0
        m_TimesClaimantAWW = 0
        m_TimesSAWW = 0
        m_MaxAmount = 0
        m_FixedAmountDownState = 0
        m_MaxAmountDownState = 0
        m_MaxTransportAmount = 0
        m_PayTransportationCode = 0


    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT "
        sSQL = sSQL & "TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",JURIS_ROW_ID,EFFECTIVE_DATE,DELETED_FLAG"
        sSQL = sSQL & ",FIXED_AMOUNT,MAX_AMOUNT,PAY_FIX_AMT_CODE,PAY_VAR_AMT_CODE"
        sSQL = sSQL & ",TIMES_CLMANT_AWW,TIMES_SAWW"
        sSQL = sSQL & ",PAY_TO_ESTATE_CODE"
        sSQL = sSQL & ",PAY_TO_PERSON_CODE"
        sSQL = sSQL & ",FIXED_AMOUNT_DOWN"
        sSQL = sSQL & ",MAX_AMOUNT_DOWN"
        sSQL = sSQL & ",PAY_TRANS_AMT_CODE"
        sSQL = sSQL & ",MAX_AMOUNT_TRANS"

        GetSQLFieldList = sSQL


    End Function
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim sSQL As String
        Dim sSQL2 As String
        Try
            LoadDataByEventDate = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            LoadDataByEventDate = AssignData(objUser, sSQL)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadDataTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataTableRowID"
        Dim sSQL As String
        Try
            LoadDataTableRowID = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataTableRowID = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try
            SaveData = 0

            sSQL = ""
            sSQL = sSQL & "SELECT *"
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("PAY_FIX_AMT_CODE").Value = m_PayFixedAmountCode
                objWriter.Fields("PAY_TRANS_AMT_CODE").Value = m_PayFixedAmountCode
                objWriter.Fields("PAY_TO_ESTATE_CODE").Value = m_PayToEstateCode
                objWriter.Fields("PAY_TO_PERSON_CODE").Value = m_PayToPersonCode
                objWriter.Fields("PAY_VAR_AMT_CODE").Value = m_PayVariableAmountCode
                objWriter.Fields("FIXED_AMOUNT").Value = m_FixedAmount
                objWriter.Fields("FIXED_AMOUNT_DOWN").Value = m_FixedAmountDownState
                objWriter.Fields("TIMES_CLMANT_AWW").Value = m_TimesClaimantAWW
                objWriter.Fields("TIMES_SAWW").Value = m_TimesSAWW
                objWriter.Fields("MAX_AMOUNT").Value = m_MaxAmount
                objWriter.Fields("MAX_AMOUNT_DOWN").Value = m_MaxAmountDownState
                objWriter.Fields("MAX_AMOUNT_TRANS").Value = MaxTransportAmount

            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("PAY_FIX_AMT_CODE", m_PayFixedAmountCode)
                objWriter.Fields.Add("PAY_TRANS_AMT_CODE", m_PayFixedAmountCode)
                objWriter.Fields.Add("PAY_TO_ESTATE_CODE", m_PayToEstateCode)
                objWriter.Fields.Add("PAY_TO_PERSON_CODE", m_PayToPersonCode)
                objWriter.Fields.Add("PAY_VAR_AMT_CODE", m_PayVariableAmountCode)
                objWriter.Fields.Add("FIXED_AMOUNT", m_FixedAmount)
                objWriter.Fields.Add("FIXED_AMOUNT_DOWN", m_FixedAmountDownState)
                objWriter.Fields.Add("TIMES_CLMANT_AWW", m_TimesClaimantAWW)
                objWriter.Fields.Add("TIMES_SAWW", m_TimesSAWW)
                objWriter.Fields.Add("MAX_AMOUNT", m_MaxAmount)
                objWriter.Fields.Add("MAX_AMOUNT_DOWN", m_MaxAmountDownState)
                objWriter.Fields.Add("MAX_AMOUNT_TRANS", MaxTransportAmount)

            End If


        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
        Finally
            SafeCloseRecordset(objReader)
        End Try
    End Function

    Public Function ValidateData() As Integer

        ValidateData = 0

        m_Warning = ""

        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_PayToEstateCode = 0 And m_PayToPersonCode = 0 Then
            m_Warning = m_Warning & "A 'Pay To' selection is required." & vbCrLf
        End If
        If m_PayToEstateCode = m_PayToPersonCode And m_PayToEstateCode = m_YesCodeID Then
            m_Warning = m_Warning & "Only one 'Pay To' selection may be set to 'Yes'." & vbCrLf
        End If
        If m_PayToEstateCode = m_PayToPersonCode And m_PayToEstateCode = m_NoCodeID Then
            m_Warning = m_Warning & "Only one 'Pay To' selection may be set to 'No'." & vbCrLf
        End If
        If m_PayFixedAmountCode = 0 Then
            m_Warning = m_Warning & "'Pay Fixed Amount(Yes/No)' is a required field." & vbCrLf
        End If
        If m_PayTransportationCode > 0 Then
            If m_PayTransportationCode = m_YesCodeID And m_MaxTransportAmount = 0 Then
                m_Warning = m_Warning & vbCrLf & "Because 'Pay Transportation (Yes/No)' is 'Yes', " & vbCrLf
                m_Warning = m_Warning & "'Maximum Amount' is required." & vbCrLf
            End If
        End If
        If m_PayVariableAmountCode = 0 Then
            m_Warning = m_Warning & "'Pay Variable Amount (Yes/No)' is a required field." & vbCrLf
        End If
        If m_PayFixedAmountCode = 0 And m_PayVariableAmountCode = 0 Then
            If m_PayFixedAmountCode = m_PayVariableAmountCode Then
                m_Warning = m_Warning & "'Pay Fixed Amount(Yes/No)' and 'Pay Variable Amount (Yes/No)' are required fields." & vbCrLf
                m_Warning = m_Warning & "They may not be the same."
            End If
        End If
        If m_PayFixedAmountCode = m_YesCodeID And m_FixedAmount = 0 Then
            m_Warning = m_Warning & vbCrLf & "Because 'Pay Fixed Amount(Yes/No)' is 'Yes', " & vbCrLf
            m_Warning = m_Warning & "'Fixed Amount' is required." & vbCrLf
        End If


    End Function

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property
    Public Property JurisRowID() As Short
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Short)
            m_JurisRowID = Value
        End Set
    End Property
    Public Property DeletedFlag() As Short
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Short)
            m_DeletedFlag = Value
        End Set
    End Property
    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property PayFixedAmountCode() As Integer
        Get
            PayFixedAmountCode = m_PayFixedAmountCode
        End Get
        Set(ByVal Value As Integer)
            m_PayFixedAmountCode = Value
        End Set
    End Property

    Public Property PayVariableAmountCode() As Integer
        Get
            PayVariableAmountCode = m_PayVariableAmountCode
        End Get
        Set(ByVal Value As Integer)
            m_PayVariableAmountCode = Value
        End Set
    End Property

    Public Property FixedAmount() As Double
        Get
            FixedAmount = m_FixedAmount
        End Get
        Set(ByVal Value As Double)
            m_FixedAmount = Value
        End Set
    End Property

    Public Property TimesClaimantAWW() As Integer
        Get
            TimesClaimantAWW = m_TimesClaimantAWW
        End Get
        Set(ByVal Value As Integer)
            m_TimesClaimantAWW = Value
        End Set
    End Property

    Public Property TimesSAWW() As Integer
        Get
            TimesSAWW = m_TimesSAWW
        End Get
        Set(ByVal Value As Integer)
            m_TimesSAWW = Value
        End Set
    End Property

    Public Property MaxAmount() As Double
        Get
            MaxAmount = m_MaxAmount
        End Get
        Set(ByVal Value As Double)
            m_MaxAmount = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property PayToEstateCode() As Integer
        Get
            PayToEstateCode = m_PayToEstateCode
        End Get
        Set(ByVal Value As Integer)
            m_PayToEstateCode = Value
        End Set
    End Property

    Public Property PayToPersonCode() As Integer
        Get
            PayToPersonCode = m_PayToPersonCode
        End Get
        Set(ByVal Value As Integer)
            m_PayToPersonCode = Value
        End Set
    End Property



    Public Property FixedAmountDownState() As Double
        Get

            FixedAmountDownState = m_FixedAmountDownState

        End Get
        Set(ByVal Value As Double)

            m_FixedAmountDownState = Value

        End Set
    End Property


    Public Property MaxAmountDownState() As Double
        Get

            MaxAmountDownState = m_MaxAmountDownState

        End Get
        Set(ByVal Value As Double)

            m_MaxAmountDownState = Value

        End Set
    End Property

    Public Property MaxCompRateMonthly() As Double
        Get
            MaxCompRateMonthly = m_MaxCompRateMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateMonthly = Value
        End Set
    End Property

    Public Property MaxCompRateWeekly() As Double
        Get
            MaxCompRateWeekly = m_MaxCompRateWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateWeekly = Value
        End Set
    End Property


    Public Property PayTransportationCode() As Integer
        Get

            PayTransportationCode = m_PayTransportationCode

        End Get
        Set(ByVal Value As Integer)

            m_PayTransportationCode = Value

        End Set
    End Property


    Public Property MaxTransportAmount() As Double
        Get

            MaxTransportAmount = m_MaxTransportAmount

        End Get
        Set(ByVal Value As Double)

            m_MaxTransportAmount = Value

        End Set
    End Property


    Public Property ErrorMaskJR() As Integer
        Get
            ErrorMaskJR = m_ErrorMaskJR
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskJR = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property PayPeriodLockedInCalcCode() As Integer
        Get
            PayPeriodLockedInCalcCode = m_PayPeriodLockedInCalcCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodLockedInCalcCode = Value
        End Set
    End Property

    Public Property RuleTotalWeeks() As Double
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseTwoThirdsCode() As Integer
        Get
            UseTwoThirdsCode = m_UseTwoThirdsCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTwoThirdsCode = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
End Class

