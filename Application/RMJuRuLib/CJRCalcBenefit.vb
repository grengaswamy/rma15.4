Option Strict Off
Option Explicit On
Public Class CJRCalcBenefit
    Private Const sClassName As String = "CJRCalcBenefit"
    Private Const sTableName As String = "WCP_BEN_SWCH"

    Dim cOffsets As CJRCalcBenefitOffsets

    'Class properties, local copy
    Private m_TableRowID As Integer
    Private m_JurisRowID As Integer
    Private m_ConWaitingDaysCode As Integer
    Private m_CountFromCode As Integer
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_PPDPayPeriodCode As Integer
    Private m_TTDPayPeriodCode As Integer
    Private m_TTDPayPeriodText As String
    Private m_DOINotPaidAsWaitDayCode As Integer
    Private m_WaitDays As Integer
    Private m_WaitDaysIfHospitalized As Integer
    Private m_RetroActiveDays As Integer
    Private m_RetroDaysFromCode As Integer
    Private m_DaysToPaymentDue As Integer
    Private m_DaysToPaymentFromDateCode As Integer
    Private m_JurisWorkWeek As Integer
    Private m_JurisWorkWeekOriginal As Integer 'Not jurisdictional rule, is used in RMWCCalc.dll because m_JurisWorkWeek may get reset
    Private m_OSEmployerFundedPensionCode As Integer
    Private m_OSEmployerProfitSharingCode As Integer
    Private m_OSEmployerDisabilityPlanCode As Integer
    Private m_OSEmployerSicknessAndAccidentCode As Integer
    Private m_OSEmployerFundedBenefitCode As Integer
    Private m_OSAnyPensionCode As Integer
    Private m_OSUnemploymentCompensationCode As Integer
    Private m_OSSocialSecurityRetirementCode As Integer
    Private m_OSSocialSecurityDisabilityCode As Integer
    Private m_OSAnyWageContinuationPlanCode As Integer
    Private m_OSSecondInjurySpecialFundCode As Integer
    Private m_PPDMultiplier As Double
    Private m_TTDMaxAmount As Double
    Private m_TTDMultiplier01 As Double
    Private m_TTDMultiplier02 As Double
    Private m_DataHasChanged As Boolean
    Private m_Warning As String
    Private m_YesCodeID As Integer

    Private m_ExcludeSunday As Integer
    Private m_ExcludeMonday As Integer
    Private m_ExcludeTuesday As Integer
    Private m_ExcludeWednesday As Integer
    Private m_ExcludeThursday As Integer
    Private m_ExcludeFriday As Integer
    Private m_ExcludeSaturday As Integer
    Private m_IncludeScheduledDaysCode As Integer

    Public Sub New()

    End Sub
    Public Sub New(ByVal connectionString As String)
        g_ConnectionString = connectionString
    End Sub

    Private Function GetTTDPayPeriodText() As Integer
        Const sFunctionName As String = "GetTTDPayPeriodText"
        Dim objReader As DbReader
        Dim sSQL As String

        Try

            GetTTDPayPeriodText = 0
            If m_TTDPayPeriodCode > 0 Then
                sSQL = ""
                sSQL = "SELECT * FROM WCP_PAYROL_PERID WHERE TABLE_ROW_ID = " & m_TTDPayPeriodCode

                objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
                If (objReader.Read()) Then
                    m_TTDPayPeriodText = objReader.GetString("SHORT_DESC") & " - " & objReader.GetString("LONG_DESC")
                End If
                SafeCloseRecordset(objReader)
            End If
            GetTTDPayPeriodText = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetTTDPayPeriodText = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            SafeCloseRecordset(objReader)
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function


    Public Function DLLFetchDataByTableRowID(ByRef lParentTableRowID As Integer) As Integer
        Dim objReader As DbReader
        Dim lTest As Integer
        Dim sSQL As String
        Try
            DLLFetchDataByTableRowID = 0
            ClearObject()

            sSQL = GetSelectFieldsSQLParent()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lParentTableRowID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                lTest = LoadData(objReader)
            Else
                lTest = 0
            End If
            SafeCloseRecordset(objReader)
            If lTest <> -1 Then
                Exit Function
            End If
            cOffsets = New CJRCalcBenefitOffsets
            lTest = cOffsets.DLLFetchParentTableRowID(m_TableRowID)
            If lTest <> -1 Then
                cOffsets = Nothing
                Exit Function
            End If
            If cOffsets.Count = 0 Then
                cOffsets = Nothing
                Exit Function
            Else
                LoadOffSets()
            End If
            DLLFetchDataByTableRowID = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            DLLFetchDataByTableRowID = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".DLLFetchDataByTableRowID|"
            g_lErrLine = Erl()
            SafeCloseRecordset(objReader)
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function

    Public Property CountFromCode() As Double
        Get
            CountFromCode = m_CountFromCode
        End Get
        Set(ByVal Value As Double)
            m_CountFromCode = Value
        End Set
    End Property

    Public Property ConWaitingDaysCode() As Double
        Get
            ConWaitingDaysCode = m_ConWaitingDaysCode
        End Get
        Set(ByVal Value As Double)
            m_ConWaitingDaysCode = Value
        End Set
    End Property

    Public Property DaysToPaymentFromDateCode() As Integer
        Get
            DaysToPaymentFromDateCode = m_DaysToPaymentFromDateCode
        End Get
        Set(ByVal Value As Integer)
            m_DaysToPaymentFromDateCode = Value
        End Set
    End Property

    Public Property PPDMultiplier() As Double
        Get
            PPDMultiplier = m_PPDMultiplier
        End Get
        Set(ByVal Value As Double)
            m_PPDMultiplier = Value
        End Set
    End Property

    Public Property TTDMaxAmount() As Double
        Get
            TTDMaxAmount = m_TTDMaxAmount
        End Get
        Set(ByVal Value As Double)
            m_TTDMaxAmount = Value
        End Set
    End Property

    Public Property TTDMultiplier01() As Double
        Get
            TTDMultiplier01 = m_TTDMultiplier01
        End Get
        Set(ByVal Value As Double)
            m_TTDMultiplier01 = Value
        End Set
    End Property

    Public Property TTDMultiplier02() As Double
        Get
            TTDMultiplier02 = m_TTDMultiplier02
        End Get
        Set(ByVal Value As Double)
            m_TTDMultiplier02 = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property RetroDaysFromCode() As Integer
        Get
            RetroDaysFromCode = m_RetroDaysFromCode
        End Get
        Set(ByVal Value As Integer)
            m_RetroDaysFromCode = Value
        End Set
    End Property

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property
    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property
    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property
    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property
    Public Property PPDPayPeriodCode() As Integer
        Get
            PPDPayPeriodCode = m_PPDPayPeriodCode
        End Get
        Set(ByVal Value As Integer)
            m_PPDPayPeriodCode = Value
        End Set
    End Property

    Public Property TTDPayPeriodCode() As Integer
        Get
            TTDPayPeriodCode = m_TTDPayPeriodCode
        End Get
        Set(ByVal Value As Integer)
            m_TTDPayPeriodCode = Value
        End Set
    End Property

    Public Property TTDPayPeriodText() As String
        Get
            TTDPayPeriodText = m_TTDPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_TTDPayPeriodText = Value
        End Set
    End Property

    Public Property DOINotPaidAsWaitDayCode() As Integer
        Get
            DOINotPaidAsWaitDayCode = m_DOINotPaidAsWaitDayCode
        End Get
        Set(ByVal Value As Integer)
            m_DOINotPaidAsWaitDayCode = Value
        End Set
    End Property
    Public Property WaitDays() As Integer
        Get
            WaitDays = m_WaitDays
        End Get
        Set(ByVal Value As Integer)
            m_WaitDays = Value
        End Set
    End Property
    Public Property WaitDaysIfHospitalized() As Integer
        Get
            WaitDaysIfHospitalized = m_WaitDaysIfHospitalized
        End Get
        Set(ByVal Value As Integer)
            m_WaitDaysIfHospitalized = Value
        End Set
    End Property
    Public Property RetroActiveDays() As Integer
        Get
            RetroActiveDays = m_RetroActiveDays
        End Get
        Set(ByVal Value As Integer)
            m_RetroActiveDays = Value
        End Set
    End Property
    Public Property DaysToPaymentDue() As Integer
        Get
            DaysToPaymentDue = m_DaysToPaymentDue
        End Get
        Set(ByVal Value As Integer)
            m_DaysToPaymentDue = Value
        End Set
    End Property
    Public Property JurisWorkWeek() As Integer
        Get
            JurisWorkWeek = m_JurisWorkWeek
        End Get
        Set(ByVal Value As Integer)
            m_JurisWorkWeek = Value
        End Set
    End Property

    Public Property JurisWorkWeekOriginal() As Integer
        Get
            JurisWorkWeekOriginal = m_JurisWorkWeekOriginal
        End Get
        Set(ByVal Value As Integer)
            m_JurisWorkWeekOriginal = Value
        End Set
    End Property

    Public Property OSEmployerFundedPensionCode() As Integer
        Get
            OSEmployerFundedPensionCode = m_OSEmployerFundedPensionCode
        End Get
        Set(ByVal Value As Integer)
            m_OSEmployerFundedPensionCode = Value
        End Set
    End Property
    Public Property OSEmployerProfitSharingCode() As Integer
        Get
            OSEmployerProfitSharingCode = m_OSEmployerProfitSharingCode
        End Get
        Set(ByVal Value As Integer)
            m_OSEmployerProfitSharingCode = Value
        End Set
    End Property
    Public Property OSEmployerDisabilityPlanCode() As Integer
        Get
            OSEmployerDisabilityPlanCode = m_OSEmployerDisabilityPlanCode
        End Get
        Set(ByVal Value As Integer)
            m_OSEmployerDisabilityPlanCode = Value
        End Set
    End Property
    Public Property OSEmployerSicknessAndAccidentCode() As Integer
        Get
            OSEmployerSicknessAndAccidentCode = m_OSEmployerSicknessAndAccidentCode
        End Get
        Set(ByVal Value As Integer)
            m_OSEmployerSicknessAndAccidentCode = Value
        End Set
    End Property
    Public Property OSEmployerFundedBenefitCode() As Integer
        Get
            OSEmployerFundedBenefitCode = m_OSEmployerFundedBenefitCode
        End Get
        Set(ByVal Value As Integer)
            m_OSEmployerFundedBenefitCode = Value
        End Set
    End Property
    Public Property OSAnyPensionCode() As Integer
        Get
            OSAnyPensionCode = m_OSAnyPensionCode
        End Get
        Set(ByVal Value As Integer)
            m_OSAnyPensionCode = Value
        End Set
    End Property
    Public Property OSUnemploymentCompensationCode() As Integer
        Get
            OSUnemploymentCompensationCode = m_OSUnemploymentCompensationCode
        End Get
        Set(ByVal Value As Integer)
            m_OSUnemploymentCompensationCode = Value
        End Set
    End Property
    Public Property OSSocialSecurityRetirementCode() As Integer
        Get
            OSSocialSecurityRetirementCode = m_OSSocialSecurityRetirementCode
        End Get
        Set(ByVal Value As Integer)
            m_OSSocialSecurityRetirementCode = Value
        End Set
    End Property
    Public Property OSSocialSecurityDisabilityCode() As Integer
        Get
            OSSocialSecurityDisabilityCode = m_OSSocialSecurityDisabilityCode
        End Get
        Set(ByVal Value As Integer)
            m_OSSocialSecurityDisabilityCode = Value
        End Set
    End Property
    Public Property OSAnyWageContinuationPlanCode() As Integer
        Get
            OSAnyWageContinuationPlanCode = m_OSAnyWageContinuationPlanCode
        End Get
        Set(ByVal Value As Integer)
            m_OSAnyWageContinuationPlanCode = Value
        End Set
    End Property

    Public Property OSSecondInjurySpecialFundCode() As Integer
        Get
            OSSecondInjurySpecialFundCode = m_OSSecondInjurySpecialFundCode
        End Get
        Set(ByVal Value As Integer)
            m_OSSecondInjurySpecialFundCode = Value
        End Set
    End Property

    Public ReadOnly Property YesCodeID() As Integer
        Get
            YesCodeID = m_YesCodeID
        End Get
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
    Public Property ExcludeSunday() As Integer
        Get
            ExcludeSunday = m_ExcludeSunday
        End Get
        Set(ByVal Value As Integer)
            m_ExcludeSunday = Value
        End Set
    End Property


    Public Property ExcludeMonday() As Integer
        Get
            ExcludeMonday = m_ExcludeMonday
        End Get
        Set(ByVal Value As Integer)
            m_ExcludeMonday = Value
        End Set
    End Property

    Public Property ExcludeTuesday() As Integer
        Get
            ExcludeTuesday = m_ExcludeTuesday
        End Get
        Set(ByVal Value As Integer)
            m_ExcludeTuesday = Value
        End Set
    End Property


    Public Property ExcludeWednesday() As Integer
        Get
            ExcludeWednesday = m_ExcludeWednesday
        End Get
        Set(ByVal Value As Integer)
            m_ExcludeWednesday = Value
        End Set
    End Property

    Public Property ExcludeThursday() As Integer
        Get
            ExcludeThursday = m_ExcludeThursday
        End Get
        Set(ByVal Value As Integer)
            m_ExcludeThursday = Value
        End Set
    End Property

    Public Property ExcludeFriday() As Integer
        Get
            ExcludeFriday = m_ExcludeFriday
        End Get
        Set(ByVal Value As Integer)
            m_ExcludeFriday = Value
        End Set
    End Property

    Public Property ExcludeSaturday() As Integer
        Get
            ExcludeSaturday = m_ExcludeSaturday
        End Get
        Set(ByVal Value As Integer)
            m_ExcludeSaturday = Value
        End Set
    End Property


    Public Property IncludeScheduledDaysCode() As Integer
        Get

            IncludeScheduledDaysCode = m_IncludeScheduledDaysCode

        End Get
        Set(ByVal Value As Integer)

            m_IncludeScheduledDaysCode = Value

        End Set
    End Property
    Public Function ClearObject() As Integer
        m_TableRowID = 0
        m_JurisRowID = 0
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_PPDPayPeriodCode = 0
        m_TTDPayPeriodCode = 0
        m_TTDPayPeriodText = ""
        m_DOINotPaidAsWaitDayCode = 0
        m_WaitDays = 0
        m_WaitDaysIfHospitalized = 0
        m_RetroActiveDays = 0
        m_DaysToPaymentDue = 0
        m_JurisWorkWeek = 0
        m_JurisWorkWeekOriginal = 0
        m_OSEmployerFundedPensionCode = 0
        m_OSEmployerProfitSharingCode = 0
        m_OSEmployerDisabilityPlanCode = 0
        m_OSEmployerSicknessAndAccidentCode = 0
        m_OSEmployerFundedBenefitCode = 0
        m_OSAnyPensionCode = 0
        m_OSUnemploymentCompensationCode = 0
        m_OSSocialSecurityRetirementCode = 0
        m_OSSocialSecurityDisabilityCode = 0
        m_OSAnyWageContinuationPlanCode = 0
        m_OSSecondInjurySpecialFundCode = 0
        TTDMaxAmount = 0
        m_TTDMultiplier01 = 0
        m_RetroDaysFromCode = 0
        m_ConWaitingDaysCode = 0
        m_CountFromCode = 0
        m_YesCodeID = 0


    End Function
    Public Function LoadDataByJurisRowIDEffectiveDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer

        Dim objReader As DbReader
        Dim lTest As Integer
        Dim sSQL As String
        Dim sSQL2 As String
        Try
            LoadDataByJurisRowIDEffectiveDate = 0
            ClearObject()

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM WCP_BEN_SWCH"
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetSelectFieldsSQLParent()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"
            'g_ConnectionString = objUser.objRiskmasterDatabase.ConnectionString 'Nanda
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If Not objReader.Read() Then
                Exit Function
            End If
            lTest = LoadData(objReader)
            SafeCloseRecordset(objReader)
            If lTest <> -1 Then
                Exit Function
            End If
            cOffsets = New CJRCalcBenefitOffsets
            lTest = cOffsets.DLLFetchDataByTableRowIDParent(m_TableRowID)
            If lTest <> -1 Then
                cOffsets = Nothing
                Exit Function
            End If
            If cOffsets.Count = 0 Then
                cOffsets = Nothing
                Exit Function
            Else
                LoadOffSets()
            End If
            GetTTDPayPeriodText()
            SafeCloseRecordset(objReader)
            m_YesCodeID = GetYesCodeID()

            LoadDataByJurisRowIDEffectiveDate = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
                If InStr(1, g_sErrDescription, "ORA-00936") > 0 Then g_sErrDescription = g_sErrDescription & "--" & sSQL
            End With
            LoadDataByJurisRowIDEffectiveDate = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadDataByJurisRowIDEffectiveDate|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

            'UPGRADE_NOTE: Object cOffsets may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            cOffsets = Nothing
        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : LoadDataByTableRowID
    ' DateTime  : 5/20/2004 11:10
    ' Author    : jlt
    ' Purpose   :
    ' Note      : This procedure is called from the add/edit screens of Jurisdictional Rules
    '...........: and from Class CJRCalcBenefits.  Check for db connection before
    '...........: connecting.

    '---------------------------------------------------------------------------------------
    '
    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lParentTableRowID As Integer) As Integer
        Dim objReader As DbReader
        Dim lTest As Integer
        Dim sSQL As String
        Try
            LoadDataByTableRowID = 0
            ClearObject()
            sSQL = GetSelectFieldsSQLParent()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lParentTableRowID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If Not objReader.Read() Then
                SafeCloseRecordset(objReader)
                Exit Function
            End If
            lTest = LoadData(objReader)
            SafeCloseRecordset(objReader)
            If lTest <> -1 Then
                Exit Function
            End If
            cOffsets = New CJRCalcBenefitOffsets
            lTest = cOffsets.DLLFetchParentTableRowID(m_TableRowID)
            If lTest <> -1 Then
                'jtodd22 12/13/2004 if lTest <> -1 there was an error in the cOffsets.DLLFetchParentTableRowID
                'UPGRADE_NOTE: Object cOffsets may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                cOffsets = Nothing
                Exit Function
            End If
            If cOffsets.Count = 0 Then
                'UPGRADE_NOTE: Object cOffsets may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                cOffsets = Nothing
                Exit Function
            Else
                LoadOffSets()
            End If
            SafeCloseRecordset(objReader)
            m_YesCodeID = GetYesCodeID()
            LoadDataByTableRowID = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadDataByTableRowID|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim objTrans As DbTransaction
        Dim lTest As Integer
        Dim sSQL As String
        Try
            SaveData = 0


            sSQL = GetSelectFieldsSQLParent()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            If (objReader.Read()) Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objTrans = objWriter.Connection.BeginTransaction()
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("JURIS_WORK_WEEK").Value = m_JurisWorkWeek
                objWriter.Fields("BT_FIRST_PAY_DUE").Value = m_DaysToPaymentDue
                objWriter.Fields("BT_PPD_MULTIPLIER").Value = m_PPDMultiplier
                objWriter.Fields("BT_RETRO_DAYS").Value = m_RetroActiveDays
                objWriter.Fields("MAX_TTD_AMT").Value = m_TTDMaxAmount
                objWriter.Fields("BT_TTD_MULTIPLIER1").Value = m_TTDMultiplier01
                objWriter.Fields("BT_TTD_MULTIPLIER2").Value = m_TTDMultiplier02
                objWriter.Fields("BT_WAIT_DAYS").Value = m_WaitDays
                objWriter.Fields("BT_WAIT_HOS_DAYS").Value = m_WaitDaysIfHospitalized
                objWriter.Fields("PPD_PAY_CODE").Value = m_PPDPayPeriodCode
                objWriter.Fields("TTD_PAY_CODE").Value = m_TTDPayPeriodCode
                objWriter.Fields("BT_DOI_WAITDY_CODE").Value = m_DOINotPaidAsWaitDayCode
                objWriter.Fields("BT_RETRO_FROM_CODE").Value = m_RetroDaysFromCode
                objWriter.Fields("BT_CONWAITDAY_CODE").Value = m_ConWaitingDaysCode
                objWriter.Fields("BT_COUNT_FROM_CODE").Value = m_CountFromCode
                objWriter.Fields("BT_PAY_FROMEV_CODE").Value = m_DaysToPaymentFromDateCode

                objWriter.Fields("EXCLUDE_SUNDAY").Value = m_ExcludeSunday
                objWriter.Fields("EXCLUDE_MONDAY").Value = m_ExcludeMonday
                objWriter.Fields("EXCLUDE_TUESDAY").Value = m_ExcludeTuesday
                objWriter.Fields("EXCLUDE_WEDNESDAY").Value = m_ExcludeWednesday
                objWriter.Fields("EXCLUDE_THURSDAY").Value = m_ExcludeThursday
                objWriter.Fields("EXCLUDE_FRIDAY").Value = m_ExcludeFriday
                objWriter.Fields("EXCLUDE_SATURDAY").Value = m_ExcludeSaturday
                objWriter.Fields("INCL_SCD_DAYS_CODE").Value = m_IncludeScheduledDaysCode
                objWriter.Execute()
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objTrans = objWriter.Connection.BeginTransaction()
                lTest = lGetNextUID("WCP_BEN_SWCH")
                objWriter.Fields.Add("TABLE_ROW_ID", lTest)
                m_TableRowID = lTest
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("JURIS_WORK_WEEK", m_JurisWorkWeek)
                objWriter.Fields.Add("BT_FIRST_PAY_DUE", m_DaysToPaymentDue)
                objWriter.Fields.Add("BT_PPD_MULTIPLIER", m_PPDMultiplier)
                objWriter.Fields.Add("BT_RETRO_DAYS", m_RetroActiveDays)
                objWriter.Fields.Add("MAX_TTD_AMT", m_TTDMaxAmount)
                objWriter.Fields.Add("BT_TTD_MULTIPLIER1", m_TTDMultiplier01)
                objWriter.Fields.Add("BT_TTD_MULTIPLIER2", m_TTDMultiplier02)
                objWriter.Fields.Add("BT_WAIT_DAYS", m_WaitDays)
                objWriter.Fields.Add("BT_WAIT_HOS_DAYS", m_WaitDaysIfHospitalized)
                objWriter.Fields.Add("PPD_PAY_CODE", m_PPDPayPeriodCode)
                objWriter.Fields.Add("TTD_PAY_CODE", m_TTDPayPeriodCode)
                objWriter.Fields.Add("BT_DOI_WAITDY_CODE", m_DOINotPaidAsWaitDayCode)
                objWriter.Fields.Add("BT_RETRO_FROM_CODE", m_RetroDaysFromCode)
                objWriter.Fields.Add("BT_CONWAITDAY_CODE", m_ConWaitingDaysCode)
                objWriter.Fields.Add("BT_COUNT_FROM_CODE", m_CountFromCode)
                objWriter.Fields.Add("BT_PAY_FROMEV_CODE", m_DaysToPaymentFromDateCode)

                objWriter.Fields.Add("EXCLUDE_SUNDAY", m_ExcludeSunday)
                objWriter.Fields.Add("EXCLUDE_MONDAY", m_ExcludeMonday)
                objWriter.Fields.Add("EXCLUDE_TUESDAY", m_ExcludeTuesday)
                objWriter.Fields.Add("EXCLUDE_WEDNESDAY", m_ExcludeWednesday)
                objWriter.Fields.Add("EXCLUDE_THURSDAY", m_ExcludeThursday)
                objWriter.Fields.Add("EXCLUDE_FRIDAY", m_ExcludeFriday)
                objWriter.Fields.Add("EXCLUDE_SATURDAY", m_ExcludeSaturday)
                objWriter.Fields.Add("INCL_SCD_DAYS_CODE", m_IncludeScheduledDaysCode)
                objWriter.Execute()
            End If

            SafeCloseRecordset(objReader)

            cOffsets = New CJRCalcBenefitOffsets
            lTest = cOffsets.DLLFetchDataByTableRowIDParent(m_TableRowID)
            If lTest <> -1 Then
                Err.Raise(lTest, "cOffsets.DLLFetchDataByTableRowIDParent", Err.Description)
                objTrans.Rollback()
                Exit Function
            End If
            If cOffsets.Count = 0 Then
                cOffsets.Add("", 0, 213, m_TableRowID, 0, "k213")
                cOffsets.Add("", 0, 214, m_TableRowID, 0, "k214")
                cOffsets.Add("", 0, 215, m_TableRowID, 0, "k215")
                cOffsets.Add("", 0, 216, m_TableRowID, 0, "k216")
                cOffsets.Add("", 0, 217, m_TableRowID, 0, "k217")
                cOffsets.Add("", 0, 218, m_TableRowID, 0, "k218")
                cOffsets.Add("", 0, 219, m_TableRowID, 0, "k219")
                cOffsets.Add("", 0, 220, m_TableRowID, 0, "k220")
                cOffsets.Add("", 0, 221, m_TableRowID, 0, "k221")
                cOffsets.Add("", 0, 222, m_TableRowID, 0, "k222")
                cOffsets.Add("", 0, 223, m_TableRowID, 0, "k223")
            End If
            'Note:  find "k" numbers in table WCP_FRIENDLY_NAME, data source is table WCP_BEN_X_SWCH
            '(212,2,-1,'k213','20040514','CSC-jlt','20040514','CSC-jlt','OS Employer Funded Pension:')
            '(213,2,-1,'k214','20040514','CSC-jlt','20040514','CSC-jlt','OS Employer Profit Sharing:')
            '(214,2,-1,'k215','20040514','CSC-jlt','20040514','CSC-jlt','OS Employer Disability Plan:')
            '(215,2,-1,'k216','20040514','CSC-jlt','20040514','CSC-jlt','OS Employer Sickness And Accident:')
            '(216,2,-1,'k217','20040514','CSC-jlt','20040514','CSC-jlt','OS Employer Funded Benefit:')
            '(217,2,-1,'k218','20040514','CSC-jlt','20040514','CSC-jlt','OS Any Pension:')
            '(218,2,-1,'k219','20040514','CSC-jlt','20040514','CSC-jlt','OS Unemployment Compensation:')
            '(219,2,-1,'k220','20040514','CSC-jlt','20040514','CSC-jlt','OS Social Security Retirement:')
            '(220,2,-1,'k221','20040514','CSC-jlt','20040514','CSC-jlt','OS Social Security Disability:')
            '(221,2,-1,'k222','20040514','CSC-jlt','20040514','CSC-jlt','OS Any Wage Continuation Plan:')
            '(223,2,-1,'k223','20040514','CSC-jlt','20040514','CSC-jlt','OS Second Injury/Special Fund:')
            With cOffsets
                .Item("k213").TableRowIDFriendlyName = 213
                .Item("k213").TableRowIDParent = m_TableRowID
                .Item("k213").UseCode = m_OSEmployerFundedPensionCode
                .Item("k213").SaveData(objUser)
                .Item("k214").TableRowIDFriendlyName = 214
                .Item("k214").TableRowIDParent = m_TableRowID
                .Item("k214").UseCode = m_OSEmployerProfitSharingCode
                .Item("k214").SaveData(objUser)
                .Item("k215").TableRowIDFriendlyName = 215
                .Item("k215").TableRowIDParent = m_TableRowID
                .Item("k215").UseCode = m_OSEmployerDisabilityPlanCode
                .Item("k215").SaveData(objUser)
                .Item("k216").TableRowIDFriendlyName = 216
                .Item("k216").TableRowIDParent = m_TableRowID
                .Item("k216").UseCode = m_OSEmployerSicknessAndAccidentCode
                .Item("k216").SaveData(objUser)
                .Item("k217").TableRowIDFriendlyName = 217
                .Item("k217").TableRowIDParent = m_TableRowID
                .Item("k217").UseCode = m_OSEmployerFundedBenefitCode
                .Item("k217").SaveData(objUser)
                .Item("k218").TableRowIDFriendlyName = 218
                .Item("k218").TableRowIDParent = m_TableRowID
                .Item("k218").UseCode = m_OSAnyPensionCode
                .Item("k218").SaveData(objUser)
                .Item("k219").TableRowIDFriendlyName = 219
                .Item("k219").TableRowIDParent = m_TableRowID
                .Item("k219").UseCode = m_OSUnemploymentCompensationCode
                .Item("k219").SaveData(objUser)
                .Item("k220").TableRowIDFriendlyName = 220
                .Item("k220").TableRowIDParent = m_TableRowID
                .Item("k220").UseCode = m_OSSocialSecurityRetirementCode
                .Item("k220").SaveData(objUser)
                .Item("k221").TableRowIDFriendlyName = 221
                .Item("k221").TableRowIDParent = m_TableRowID
                .Item("k221").UseCode = m_OSSocialSecurityDisabilityCode
                .Item("k221").SaveData(objUser)
                .Item("k222").TableRowIDFriendlyName = 222
                .Item("k222").TableRowIDParent = m_TableRowID
                .Item("k222").UseCode = m_OSAnyWageContinuationPlanCode
                .Item("k222").SaveData(objUser)
                .Item("k223").TableRowIDFriendlyName = 223
                .Item("k223").TableRowIDParent = m_TableRowID
                .Item("k223").UseCode = m_OSSecondInjurySpecialFundCode
                .Item("k223").SaveData(objUser)
            End With
            objTrans.Commit()
            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
            objTrans = Nothing
            objTrans.Dispose()
        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : LoadData
    ' DateTime  : 07/04/2004 09:57
    ' Author    : jtodd22
    ' Purpose   : code reduction;  This function is called by two other LoadData.... functions
    '...........: It exists only to reduce the coding.
    '---------------------------------------------------------------------------------------
    '
    Private Function LoadData(ByRef objReader As DbReader) As Integer
        Try
            LoadData = 0
            m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
            m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
            m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
            m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
            m_PPDPayPeriodCode = objReader.GetInt32("PPD_PAY_CODE")
            m_TTDMaxAmount = objReader.GetDouble("MAX_TTD_AMT")
            m_TTDPayPeriodCode = objReader.GetInt32("TTD_PAY_CODE")
            m_DOINotPaidAsWaitDayCode = objReader.GetInt32("BT_DOI_WAITDY_CODE")

            m_JurisWorkWeek = objReader.GetInt32("JURIS_WORK_WEEK")
            m_JurisWorkWeekOriginal = m_JurisWorkWeek

            m_WaitDays = objReader.GetInt32("BT_WAIT_DAYS")
            m_WaitDaysIfHospitalized = objReader.GetInt32("BT_WAIT_HOS_DAYS")
            m_RetroActiveDays = objReader.GetInt32("BT_RETRO_DAYS")
            m_DaysToPaymentDue = objReader.GetInt32("BT_FIRST_PAY_DUE")
            m_DaysToPaymentFromDateCode = objReader.GetInt32("BT_PAY_FROMEV_CODE")
            m_RetroDaysFromCode = objReader.GetInt32("BT_RETRO_FROM_CODE")
            m_PPDMultiplier = System.Math.Round(objReader.GetDouble("BT_PPD_MULTIPLIER"), 4)
            m_TTDMultiplier01 = System.Math.Round(objReader.GetDouble("BT_TTD_MULTIPLIER1"), 4)
            m_TTDMultiplier02 = System.Math.Round(objReader.GetDouble("BT_TTD_MULTIPLIER2"), 4)
            m_ConWaitingDaysCode = objReader.GetInt32("BT_CONWAITDAY_CODE")
            m_CountFromCode = objReader.GetInt32("BT_COUNT_FROM_CODE")

            m_ExcludeSunday = objReader.GetInt32("EXCLUDE_SUNDAY")
            m_ExcludeMonday = objReader.GetInt32("EXCLUDE_MONDAY")
            m_ExcludeTuesday = objReader.GetInt32("EXCLUDE_TUESDAY")
            m_ExcludeWednesday = objReader.GetInt32("EXCLUDE_WEDNESDAY")
            m_ExcludeThursday = objReader.GetInt32("EXCLUDE_THURSDAY")
            m_ExcludeFriday = objReader.GetInt32("EXCLUDE_FRIDAY")
            m_ExcludeSaturday = objReader.GetInt32("EXCLUDE_SATURDAY")
            m_IncludeScheduledDaysCode = objReader.GetInt32("INCL_SCD_DAYS_CODE")

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Private Function GetSelectFieldsSQLParent() As String
        Dim sSQL As String
        GetSelectFieldsSQLParent = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", ADDED_BY_USER,DTTM_RCD_ADDED"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", JURIS_WORK_WEEK"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", BT_FIRST_PAY_DUE"
        sSQL = sSQL & ", BT_PPD_MULTIPLIER"
        sSQL = sSQL & ", MAX_TTD_AMT"
        sSQL = sSQL & ", BT_TTD_MULTIPLIER1"
        sSQL = sSQL & ", BT_TTD_MULTIPLIER2"
        sSQL = sSQL & ", BT_WAIT_DAYS"
        sSQL = sSQL & ", BT_WAIT_HOS_DAYS"
        sSQL = sSQL & ", BT_RETRO_DAYS"
        sSQL = sSQL & ", PPD_PAY_CODE"
        sSQL = sSQL & ", TTD_PAY_CODE"
        sSQL = sSQL & ", BT_DOI_WAITDY_CODE"
        sSQL = sSQL & ", BT_RETRO_FROM_CODE"
        sSQL = sSQL & ", BT_CONWAITDAY_CODE"
        sSQL = sSQL & ", BT_COUNT_FROM_CODE"
        sSQL = sSQL & ", BT_PAY_FROMEV_CODE"
        sSQL = sSQL & ", EXCLUDE_SUNDAY"
        sSQL = sSQL & ", EXCLUDE_MONDAY"
        sSQL = sSQL & ", EXCLUDE_TUESDAY"
        sSQL = sSQL & ", EXCLUDE_WEDNESDAY"
        sSQL = sSQL & ", EXCLUDE_THURSDAY"
        sSQL = sSQL & ", EXCLUDE_FRIDAY"
        sSQL = sSQL & ", EXCLUDE_SATURDAY"
        sSQL = sSQL & ", INCL_SCD_DAYS_CODE"
        GetSelectFieldsSQLParent = sSQL


    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : LoadOffSets
    ' DateTime  : 12/14/2004 15:27
    ' Author    : jtodd22
    ' Purpose   : code reduction
    ' Note      : Beware the database TABLE_ROW_ID must be the CLng value of the Mid(key,2)
    '---------------------------------------------------------------------------------------
    '
    Private Function LoadOffSets() As Integer
        Try
            Dim i As Short

            LoadOffSets = 0
            'Note:  find "k" numbers in table WCP_FRIENDLY_NAME, data source is table WCP_BEN_SWCH_X_SWCH
            '(213,2,-1,'k213','20040514','CSC-jlt','20040514','CSC-jlt','OS Employer Funded Pension:')
            '(214,2,-1,'k214','20040514','CSC-jlt','20040514','CSC-jlt','OS Employer Profit Sharing:')
            '(215,2,-1,'k215','20040514','CSC-jlt','20040514','CSC-jlt','OS Employer Disability Plan:')
            '(216,2,-1,'k216','20040514','CSC-jlt','20040514','CSC-jlt','OS Employer Sickness And Accident:')
            '(217,2,-1,'k217','20040514','CSC-jlt','20040514','CSC-jlt','OS Employer Funded Benefit:')
            '(218,2,-1,'k218','20040514','CSC-jlt','20040514','CSC-jlt','OS Any Pension:')
            '(219,2,-1,'k219','20040514','CSC-jlt','20040514','CSC-jlt','OS Unemployment Compensation:')
            '(220,2,-1,'k220','20040514','CSC-jlt','20040514','CSC-jlt','OS Social Security Retirement:')
            '(221,2,-1,'k221','20040514','CSC-jlt','20040514','CSC-jlt','OS Social Security Disability:')
            '(222,2,-1,'k222','20040514','CSC-jlt','20040514','CSC-jlt','OS Any Wage Continuation Plan:')
            '(223,2,-1,'k223','20040514','CSC-jlt','20040514','CSC-jlt','OS Second Injury/Special Fund:')
            With cOffsets
                For i = 1 To .Count
                    Select Case .Item(i).TableRowID
                        Case 213
                            m_OSEmployerFundedPensionCode = .Item("k213").UseCode
                        Case 214
                            m_OSEmployerProfitSharingCode = .Item("k214").UseCode
                        Case 215
                            m_OSEmployerDisabilityPlanCode = .Item("k215").UseCode
                        Case 216
                            m_OSEmployerSicknessAndAccidentCode = .Item("k216").UseCode
                        Case 217
                            m_OSEmployerFundedBenefitCode = .Item("k217").UseCode
                        Case 218
                            m_OSAnyPensionCode = .Item("k218").UseCode
                        Case 219
                            m_OSUnemploymentCompensationCode = .Item("k219").UseCode
                        Case 220
                            m_OSSocialSecurityRetirementCode = .Item("k220").UseCode
                        Case 221
                            m_OSSocialSecurityDisabilityCode = .Item("k221").UseCode
                        Case 222
                            m_OSAnyWageContinuationPlanCode = .Item("k222").UseCode
                        Case 223
                            m_OSSecondInjurySpecialFundCode = .Item("k223").UseCode
                        Case Else
                            'oh-oh
                    End Select
                Next i
            End With
            LoadOffSets = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadOffSets = Err.Number

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadOffSets|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function
    Public Function ValidateData() As Integer
        Dim iCheck As Short
        Dim iWkDays As Short

        ValidateData = 0
        m_Warning = ""

        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_JurisRowID < 0 Then
            m_Warning = m_Warning & "A Jurisdiction is required." & vbCrLf
        End If
        If m_TTDPayPeriodCode < 1 Then
            m_Warning = m_Warning & "A TTD Pay Period is required." & vbCrLf
        End If
        If Not IsNumeric(m_WaitDays) Then
            m_Warning = m_Warning & "A 'Waiting Period Days' value is required, even if it is '0' (zero)." & vbCrLf
        End If
        If Not IsNumeric(m_RetroActiveDays) Then
            m_Warning = m_Warning & "A 'RetroActive Days' value is required, even if it is '0' (zero)." & vbCrLf
        End If

        If m_JurisWorkWeek < 7 Then
            iCheck = 0
            iWkDays = m_JurisWorkWeek
            iCheck = iCheck + m_ExcludeSunday
            iCheck = iCheck + m_ExcludeMonday
            iCheck = iCheck + m_ExcludeTuesday
            iCheck = iCheck + m_ExcludeWednesday
            iCheck = iCheck + m_ExcludeThursday
            iCheck = iCheck + m_ExcludeFriday
            iCheck = iCheck + m_ExcludeSaturday

            If iCheck + iWkDays < 7 Then
                m_Warning = m_Warning & "By having the 'Work Week' less than seven days" & vbCrLf
                m_Warning = m_Warning & "you must have excluded days." & vbCrLf
            End If
            If iCheck + iWkDays > 7 Then
                m_Warning = m_Warning & "You have too many excluded days for a " & iWkDays & " day 'Work Week'." & vbCrLf
            End If
        End If


    End Function
End Class

