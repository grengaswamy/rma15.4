Option Strict Off
Option Explicit On
Public Class CJRFromDate
    Const sClassName As String = "CJRFormDate"

    'Class properties, local copy
    Private m_TableRowID As Object
    Private m_FromDateText As Object
    Private m_DataHasChanged As Boolean

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            'UPGRADE_WARNING: Couldn't resolve default property of object m_TableRowID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            'UPGRADE_WARNING: Couldn't resolve default property of object m_TableRowID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_TableRowID = Value
        End Set
    End Property

    Public Property FromDateText() As String
        Get
            'UPGRADE_WARNING: Couldn't resolve default property of object m_FromDateText. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            FromDateText = m_FromDateText
        End Get
        Set(ByVal Value As String)
            'UPGRADE_WARNING: Couldn't resolve default property of object m_FromDateText. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_FromDateText = Value
        End Set
    End Property
End Class

