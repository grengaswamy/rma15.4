Option Strict Off
Option Explicit On
Public Class CJRXJurisWorkWeek
    Private Const sClassName As String = "CJRXJurisWorkWeek"
    Private Const sTableName As String = "WCP_BEN_SWCH"

    'Class properties, local copy
    Private m_ExcludeSunday As Integer
    Private m_ExcludeMonday As Integer
    Private m_ExcludeTuesday As Integer
    Private m_ExcludeWednesday As Integer
    Private m_ExcludeThursday As Integer
    Private m_ExcludeFriday As Integer
    Private m_ExcludeSaturday As Integer
    Private m_JurisDefinedWorkWeek As Integer

    Public Function LoadData(ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadData = 0
            m_JurisDefinedWorkWeek = 0

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = ""
            sSQL = sSQL & " SELECT"
            sSQL = sSQL & " JURIS_WORK_WEEK"
            sSQL = sSQL & ",EXCLUDE_SUNDAY"
            sSQL = sSQL & ",EXCLUDE_MONDAY"
            sSQL = sSQL & ",EXCLUDE_TUESDAY"
            sSQL = sSQL & ",EXCLUDE_WEDNESDAY"
            sSQL = sSQL & ",EXCLUDE_THURSDAY"
            sSQL = sSQL & ",EXCLUDE_FRIDAY"
            sSQL = sSQL & ",EXCLUDE_SATURDAY"
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            'objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            'If Not g_DBObject.DB_EOF(iRdset) Then
            '	m_ExcludeSunday = objReader.GetInt32( "EXCLUDE_SUNDAY")
            '	m_ExcludeMonday = objReader.GetInt32( "EXCLUDE_MONDAY")
            '	m_ExcludeTuesday = objReader.GetInt32( "EXCLUDE_TUESDAY")
            '	m_ExcludeWednesday = objReader.GetInt32( "EXCLUDE_WEDNESDAY")
            '	m_ExcludeThursday = objReader.GetInt32( "EXCLUDE_THURSDAY")
            '	m_ExcludeFriday = objReader.GetInt32( "EXCLUDE_FRIDAY")
            '	m_ExcludeSaturday = objReader.GetInt32( "EXCLUDE_SATURDAY")
            '	m_JurisDefinedWorkWeek = objReader.GetInt32( "JURIS_WORK_WEEK")
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_ExcludeSunday = objReader.GetInt32("EXCLUDE_SUNDAY")
                m_ExcludeMonday = objReader.GetInt32("EXCLUDE_MONDAY")
                m_ExcludeTuesday = objReader.GetInt32("EXCLUDE_TUESDAY")
                m_ExcludeWednesday = objReader.GetInt32("EXCLUDE_WEDNESDAY")
                m_ExcludeThursday = objReader.GetInt32("EXCLUDE_THURSDAY")
                m_ExcludeFriday = objReader.GetInt32("EXCLUDE_FRIDAY")
                m_ExcludeSaturday = objReader.GetInt32("EXCLUDE_SATURDAY")
                m_JurisDefinedWorkWeek = objReader.GetInt32("JURIS_WORK_WEEK")
            End If


            If m_JurisDefinedWorkWeek = 0 Then m_JurisDefinedWorkWeek = 7
            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function

    Public Property ExcludeSunday() As Integer
        Get
            ExcludeSunday = m_ExcludeSunday
        End Get
        Set(ByVal Value As Integer)
            m_ExcludeSunday = Value
        End Set
    End Property


    Public Property ExcludeMonday() As Integer
        Get
            ExcludeMonday = m_ExcludeMonday
        End Get
        Set(ByVal Value As Integer)
            m_ExcludeMonday = Value
        End Set
    End Property

    Public Property ExcludeTuesday() As Integer
        Get
            ExcludeTuesday = m_ExcludeTuesday
        End Get
        Set(ByVal Value As Integer)
            m_ExcludeTuesday = Value
        End Set
    End Property


    Public Property ExcludeWednesday() As Integer
        Get
            ExcludeWednesday = m_ExcludeWednesday
        End Get
        Set(ByVal Value As Integer)
            m_ExcludeWednesday = Value
        End Set
    End Property

    Public Property ExcludeThursday() As Integer
        Get
            ExcludeThursday = m_ExcludeThursday
        End Get
        Set(ByVal Value As Integer)
            m_ExcludeThursday = Value
        End Set
    End Property

    Public Property ExcludeFriday() As Integer
        Get
            ExcludeFriday = m_ExcludeFriday
        End Get
        Set(ByVal Value As Integer)
            m_ExcludeFriday = Value
        End Set
    End Property

    Public Property ExcludeSaturday() As Integer
        Get
            ExcludeSaturday = m_ExcludeSaturday
        End Get
        Set(ByVal Value As Integer)
            m_ExcludeSaturday = Value
        End Set
    End Property

    Public Property JurisDefinedWorkWeek() As Integer
        Get
            JurisDefinedWorkWeek = m_JurisDefinedWorkWeek
        End Get
        Set(ByVal Value As Integer)
            m_JurisDefinedWorkWeek = Value
        End Set
    End Property
End Class

