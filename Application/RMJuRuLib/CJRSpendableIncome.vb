Option Strict Off
Option Explicit On
Public Class CJRSpendableIncome
    Private Const sClassName As String = "CJRSpendableIncome"

    'Class properties--local copy
    Private m_TableRowID As Integer
    Private m_JurisRowID As Short
    Private m_EffectiveDateDTG As String
    Private m_DeletedFlag As Short
    Private m_BaseAmount As Double
    Private m_TaxStatusCode As Integer
    Private m_Exemption00 As Double
    Private m_UseDiscount00Code As Integer
    Private m_Exemption01 As Double
    Private m_Exemption02 As Double
    Private m_Exemption03 As Double
    Private m_Exemption04 As Double
    Private m_Exemption05 As Double
    Private m_Exemption06 As Double
    Private m_Exemption07 As Double
    Private m_Exemption08 As Double
    Private m_Exemption09 As Double
    Private m_Exemption10orMore As Double
    Private m_Exemption11 As Double
    Private m_TaxStatusText As String
    Private m_DataHasChanged As Boolean
    Private m_Warning As String

    Private Function AssignData(ByVal objReader As Riskmaster.Db.DbReader) As Integer
        Const sFunctionName As String = "AssignData"

        Try

            AssignData = -1 'jtodd22 let Error Handler reset on error

            m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
            m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
            m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
            m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
            m_BaseAmount = objReader.GetDouble("BASE_AMOUNT")
            m_TaxStatusCode = objReader.GetInt32("TAX_STATUS_CODE")
            m_Exemption00 = objReader.GetDouble("EXEMP_A")
            m_UseDiscount00Code = objReader.GetInt32("USE_DISCNT_A_CODE")
            m_Exemption01 = objReader.GetDouble("EXEMP_B")
            m_Exemption02 = objReader.GetDouble("EXEMP_C")
            m_Exemption03 = objReader.GetDouble("EXEMP_D")
            m_Exemption04 = objReader.GetDouble("EXEMP_E")
            m_Exemption05 = objReader.GetDouble("EXEMP_F")
            m_Exemption06 = objReader.GetDouble("EXEMP_G")
            m_Exemption07 = objReader.GetDouble("EXEMP_H")
            m_Exemption08 = objReader.GetDouble("EXEMP_I")
            m_Exemption09 = objReader.GetDouble("EXEMP_J")
            m_Exemption10orMore = objReader.GetDouble("EXEMP_K")
            m_Exemption11 = objReader.GetDouble("EXEMP_L")
            m_TaxStatusText = objReader.GetString("SHORT_CODE")

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            ''SafeDropRecordset iRdSet
            ''DBDisconnect

        End Try

    End Function
    Public Function ClearObject() As Integer
        m_TableRowID = 0
        m_JurisRowID = 0
        m_EffectiveDateDTG = vbNullString
        m_DeletedFlag = 0
        m_BaseAmount = 0
        m_TaxStatusCode = 0
        m_Exemption00 = 0
        m_UseDiscount00Code = 0
        m_Exemption01 = 0
        m_Exemption02 = 0
        m_Exemption03 = 0
        m_Exemption04 = 0
        m_Exemption05 = 0
        m_Exemption06 = 0
        m_Exemption07 = 0
        m_Exemption08 = 0
        m_Exemption09 = 0
        m_Exemption10orMore = 0
        m_Exemption11 = 0
        m_TaxStatusText = vbNullString
        m_DataHasChanged = False
        m_Warning = ""


    End Function
    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property TaxStatusText() As String
        Get
            TaxStatusText = m_TaxStatusText
        End Get
        Set(ByVal Value As String)
            m_TaxStatusText = Value
        End Set
    End Property

    Public Property Exemption11() As Double
        Get
            Exemption11 = m_Exemption11
        End Get
        Set(ByVal Value As Double)
            m_Exemption11 = Value
        End Set
    End Property

    Public Property Exemption10orMore() As Double
        Get
            Exemption10orMore = m_Exemption10orMore
        End Get
        Set(ByVal Value As Double)
            m_Exemption10orMore = Value
        End Set
    End Property

    Public Property Exemption09() As Double
        Get
            Exemption09 = m_Exemption09
        End Get
        Set(ByVal Value As Double)
            m_Exemption09 = Value
        End Set
    End Property

    Public Property Exemption08() As Double
        Get
            Exemption08 = m_Exemption08
        End Get
        Set(ByVal Value As Double)
            m_Exemption08 = Value
        End Set
    End Property

    Public Property Exemption07() As Double
        Get
            Exemption07 = m_Exemption07
        End Get
        Set(ByVal Value As Double)
            m_Exemption07 = Value
        End Set
    End Property

    Public Property Exemption06() As Double
        Get
            Exemption06 = m_Exemption06
        End Get
        Set(ByVal Value As Double)
            m_Exemption06 = Value
        End Set
    End Property

    Public Property Exemption05() As Double
        Get
            Exemption05 = m_Exemption05
        End Get
        Set(ByVal Value As Double)
            m_Exemption05 = Value
        End Set
    End Property

    Public Property Exemption04() As Double
        Get
            Exemption04 = m_Exemption04
        End Get
        Set(ByVal Value As Double)
            m_Exemption04 = Value
        End Set
    End Property

    Public Property Exemption03() As Double
        Get
            Exemption03 = m_Exemption03
        End Get
        Set(ByVal Value As Double)
            m_Exemption03 = Value
        End Set
    End Property

    Public Property Exemption02() As Double
        Get
            Exemption02 = m_Exemption02
        End Get
        Set(ByVal Value As Double)
            m_Exemption02 = Value
        End Set
    End Property

    Public Property Exemption01() As Double
        Get
            Exemption01 = m_Exemption01
        End Get
        Set(ByVal Value As Double)
            m_Exemption01 = Value
        End Set
    End Property

    Public Property Exemption00() As Double
        Get
            Exemption00 = m_Exemption00
        End Get
        Set(ByVal Value As Double)
            m_Exemption00 = Value
        End Set
    End Property

    Public Property TaxStatusCode() As Integer
        Get
            TaxStatusCode = m_TaxStatusCode
        End Get
        Set(ByVal Value As Integer)
            m_TaxStatusCode = Value
        End Set
    End Property

    Public Property BaseAmount() As Double
        Get
            BaseAmount = m_BaseAmount
        End Get
        Set(ByVal Value As Double)
            m_BaseAmount = Value
        End Set
    End Property

    Public Property DeletedFlag() As Short
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Short)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property
    Public Property JurisRowID() As Short
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Short)
            m_JurisRowID = Value
        End Set
    End Property
    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property UseDiscount00Code() As Integer
        Get
            UseDiscount00Code = m_UseDiscount00Code
        End Get
        Set(ByVal Value As Integer)
            m_UseDiscount00Code = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String

        Try

            LoadData = 0

            ClearObject()
            sSQL = GetFieldSelect()
            sSQL = sSQL & ", CODES.SHORT_CODE"
            sSQL = sSQL & " FROM WCP_SPENDABLE, CODES"
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID
            sSQL = sSQL & " AND CODES.CODE_ID = WCP_SPENDABLE.TAX_STATUS_CODE"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If Not objReader.Read() Then
                Exit Function
            End If

            LoadData = AssignData(objReader)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function
    Public Function LoadDataByClaimData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lFilingStateID As Integer, ByRef sDateOfEventDTG As String, ByRef lTaxStatusCode As Integer, ByRef lAWW As Integer) As Integer
        Const sFunctionName As String = "LoadDataByClaimData"
        Dim objReader As DbReader
        Dim iMaxBaseAmount As Short
        Dim sSQL As String

        Try

            LoadDataByClaimData = 0


            ClearObject()
            sSQL = GetFieldSelect()
            sSQL = sSQL & ", CODES.SHORT_CODE"
            sSQL = sSQL & " FROM WCP_SPENDABLE, CODES"
            sSQL = sSQL & " WHERE CODES.CODE_ID = WCP_SPENDABLE.TAX_STATUS_CODE"
            sSQL = sSQL & " AND WCP_SPENDABLE.JURIS_ROW_ID = " & lFilingStateID
            sSQL = sSQL & " AND WCP_SPENDABLE.EFFECTIVE_DATE = (SELECT MAX(EFFECTIVE_DATE) FROM WCP_SPENDABLE WHERE JURIS_ROW_ID = " & lFilingStateID & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "')"
            sSQL = sSQL & " AND WCP_SPENDABLE.BASE_AMOUNT = " & lAWW
            sSQL = sSQL & " AND WCP_SPENDABLE.TAX_STATUS_CODE = " & lTaxStatusCode
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If Not objReader.Read() Then
                SafeCloseRecordset(objReader)
                sSQL = ""
                sSQL = sSQL & "SELECT MAX(BASE_AMOUNT) AS MAX_AWW"
                sSQL = sSQL & " FROM WCP_SPENDABLE, CODES"
                sSQL = sSQL & " WHERE CODES.CODE_ID = WCP_SPENDABLE.TAX_STATUS_CODE"
                sSQL = sSQL & " AND WCP_SPENDABLE.JURIS_ROW_ID = " & lFilingStateID
                sSQL = sSQL & " AND WCP_SPENDABLE.EFFECTIVE_DATE = (SELECT MAX(EFFECTIVE_DATE) FROM WCP_SPENDABLE WHERE JURIS_ROW_ID = " & lFilingStateID & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "')"
                sSQL = sSQL & " AND WCP_SPENDABLE.TAX_STATUS_CODE = " & lTaxStatusCode
                If modGlobals.g_dbMake <> eDatabaseType.DBMS_IS_ACCESS Then sSQL = Replace(sSQL, " AS ", " ") 'Remove alias As(notneeded except for access)
                objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
                If Not objReader.Read() Then
                    Exit Function
                Else
                    iMaxBaseAmount = objReader.GetDouble("MAX_AWW")
                    sSQL = GetFieldSelect()
                    sSQL = sSQL & ", CODES.SHORT_CODE"
                    sSQL = sSQL & " FROM WCP_SPENDABLE, CODES"
                    sSQL = sSQL & " WHERE CODES.CODE_ID = WCP_SPENDABLE.TAX_STATUS_CODE"
                    sSQL = sSQL & " AND WCP_SPENDABLE.JURIS_ROW_ID = " & lFilingStateID
                    sSQL = sSQL & " AND WCP_SPENDABLE.EFFECTIVE_DATE = (SELECT MAX(EFFECTIVE_DATE) FROM WCP_SPENDABLE WHERE JURIS_ROW_ID = " & lFilingStateID & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "')"
                    sSQL = sSQL & " AND WCP_SPENDABLE.BASE_AMOUNT = " & iMaxBaseAmount
                    sSQL = sSQL & " AND WCP_SPENDABLE.TAX_STATUS_CODE = " & lTaxStatusCode
                    objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
                    If Not objReader.Read() Then
                        Exit Function
                    End If
                End If
            End If

            LoadDataByClaimData = AssignData(objReader)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByClaimData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
            DBDisconnect()

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String

        Try

            SaveData = 0

            'If ModMain.DBConnect(objUser, sClassName) <> -1 Then
            '    g_sErrSrc = "RMJuRuLib|"
            '    g_sErrDescription = "Failed to connect to database."
            '    g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            '    g_lErrLine = Erl()
            '    g_lErrNum = 70001
            '    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            '    Err.Raise(g_lErrNum, sClassName & "." & sFunctionName, g_sErrDescription)
            '    Exit Function
            'End If

            sSQL = GetFieldSelect()
            sSQL = sSQL & " FROM WCP_SPENDABLE"
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID


            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)

                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("DELETED_FLAG").Value = m_DeletedFlag
                objWriter.Fields("BASE_AMOUNT").Value = m_BaseAmount
                objWriter.Fields("TAX_STATUS_CODE").Value = m_TaxStatusCode
                objWriter.Fields("EXEMP_A").Value = m_Exemption00
                objWriter.Fields("USE_DISCNT_A_CODE").Value = m_UseDiscount00Code
                objWriter.Fields("EXEMP_B").Value = m_Exemption01
                objWriter.Fields("EXEMP_C").Value = m_Exemption02
                objWriter.Fields("EXEMP_D").Value = m_Exemption03
                objWriter.Fields("EXEMP_E").Value = m_Exemption04
                objWriter.Fields("EXEMP_F").Value = m_Exemption05
                objWriter.Fields("EXEMP_G").Value = m_Exemption06
                objWriter.Fields("EXEMP_H").Value = m_Exemption07
                objWriter.Fields("EXEMP_I").Value = m_Exemption08
                objWriter.Fields("EXEMP_J").Value = m_Exemption09
                objWriter.Fields("EXEMP_K").Value = m_Exemption10orMore
                objWriter.Fields("EXEMP_L").Value = m_Exemption11
                objWriter.Execute()
            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                lTest = lGetNextUID("WCP_SPENDABLE")
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("DELETED_FLAG", m_DeletedFlag)
                objWriter.Fields.Add("BASE_AMOUNT", m_BaseAmount)
                objWriter.Fields.Add("TAX_STATUS_CODE", m_TaxStatusCode)
                objWriter.Fields.Add("EXEMP_A", m_Exemption00)
                objWriter.Fields.Add("USE_DISCNT_A_CODE", m_UseDiscount00Code)
                objWriter.Fields.Add("EXEMP_B", m_Exemption01)
                objWriter.Fields.Add("EXEMP_C", m_Exemption02)
                objWriter.Fields.Add("EXEMP_D", m_Exemption03)
                objWriter.Fields.Add("EXEMP_E", m_Exemption04)
                objWriter.Fields.Add("EXEMP_F", m_Exemption05)
                objWriter.Fields.Add("EXEMP_G", m_Exemption06)
                objWriter.Fields.Add("EXEMP_H", m_Exemption07)
                objWriter.Fields.Add("EXEMP_I", m_Exemption08)
                objWriter.Fields.Add("EXEMP_J", m_Exemption09)
                objWriter.Fields.Add("EXEMP_K", m_Exemption10orMore)
                objWriter.Fields.Add("EXEMP_L", m_Exemption11)
                objWriter.Execute()
            End If

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
            DBDisconnect()

        End Try

    End Function
    Private Function GetFieldSelect() As String
        Dim sSQL As String
        GetFieldSelect = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " WCP_SPENDABLE.TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ",DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",WCP_SPENDABLE.EFFECTIVE_DATE"
        sSQL = sSQL & ",WCP_SPENDABLE.JURIS_ROW_ID"
        sSQL = sSQL & ",WCP_SPENDABLE.DELETED_FLAG"
        sSQL = sSQL & ",WCP_SPENDABLE.BASE_AMOUNT"
        sSQL = sSQL & ",WCP_SPENDABLE.TAX_STATUS_CODE"
        sSQL = sSQL & ",EXEMP_A"
        sSQL = sSQL & ",USE_DISCNT_A_CODE"
        sSQL = sSQL & ",EXEMP_B"
        sSQL = sSQL & ",EXEMP_C"
        sSQL = sSQL & ",EXEMP_D"
        sSQL = sSQL & ",EXEMP_E"
        sSQL = sSQL & ",EXEMP_F"
        sSQL = sSQL & ",EXEMP_G"
        sSQL = sSQL & ",EXEMP_H"
        sSQL = sSQL & ",EXEMP_I"
        sSQL = sSQL & ",EXEMP_J"
        sSQL = sSQL & ",EXEMP_K"
        sSQL = sSQL & ",EXEMP_L"
        GetFieldSelect = sSQL


    End Function
    Public Function Validate() As Integer

        Validate = 0

        m_Warning = ""

        If Len(m_EffectiveDateDTG) Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If Me.BaseAmount = 0 Then
            m_Warning = m_Warning & "An AWW Amount greater than zero is required." & vbCrLf
        End If
        If m_TaxStatusCode < 1 Then
            m_Warning = m_Warning & "A valid Tax Status is required." & vbCrLf
        End If


    End Function
End Class

