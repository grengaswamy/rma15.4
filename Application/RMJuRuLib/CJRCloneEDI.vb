Option Strict Off
Option Explicit On
Public Class CJRCloneEDI
    Private Const sClassName As String = "CJRCloneEDI"

    Public Function CloneEDI(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lEDIStdReleaseRowID As Integer, ByRef lTargetJurisRowID As Integer, ByRef sEffectiveDateDTG As String) As Integer
        Const sFunctionName As String = "CloneEDI"
        Try

            CloneEDI = modFunctions.CloneEDI(objUser, lEDIStdReleaseRowID, lTargetJurisRowID, sEffectiveDateDTG)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            CloneEDI = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
End Class

