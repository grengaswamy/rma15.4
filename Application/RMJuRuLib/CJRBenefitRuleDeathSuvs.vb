Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleDeathSuvs
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CJRBenefitRuleDeathSuvs"

    'local variable to hold collection
    Private mCol As Collection
    Private Function Add(ByRef AWWPercentOneChild As Double, ByRef AWWPercentSpouseChild As Double, ByRef AWWPercentSpouseOnly As Double, ByRef DataHasChanged As Boolean, ByRef JurisRowID As Integer, ByRef EffectiveDateDTG As String, ByRef PaymentMaxAmountSpouseChild As Double, ByRef PaymentMaxAmountSpouseOnly As Double, ByRef PaymentMinAmountSpouseOnly As Double, ByRef SpousalRemarrEndBenefitCode As Integer, ByRef SpousalRemarrPayLumpSumCode As Integer, ByRef SpousalRemarrWeeksLumpSum As Integer, ByRef TableRowID As Integer, ByRef TotalAmountSpouseChild As Double, ByRef TotalAmountSpouseOnly As Double, ByRef TotalTimeChildAgeNoSchool As Integer, ByRef TotalWeeksChild As Integer, ByRef TotalWeeksSpouse As Integer, ByRef UseTTDCode As Integer, ByRef sKey As String) As CJRBenefitRuleDeathSuv
        Const sFunctionName As String = "Add"
        'create a new object
        Dim objNewMember As CJRBenefitRuleDeathSuv
        Try
            objNewMember = New CJRBenefitRuleDeathSuv


            'set the properties passed into the method
            objNewMember.AWWPercentOneChild = AWWPercentOneChild
            objNewMember.AWWPercentSpouseChild = AWWPercentSpouseChild
            objNewMember.AWWPercentSpouseOnly = AWWPercentSpouseOnly
            objNewMember.DataHasChanged = DataHasChanged
            objNewMember.JurisRowID = JurisRowID
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.PaymentMaxAmountSpouseChild = PaymentMaxAmountSpouseChild
            objNewMember.PaymentMaxAmountSpouseOnly = PaymentMaxAmountSpouseOnly
            objNewMember.PaymentMinAmountSpouseOnly = PaymentMinAmountSpouseOnly
            objNewMember.SpousalRemarrEndBenefitCode = SpousalRemarrEndBenefitCode
            objNewMember.SpousalRemarrPayLumpSumCode = SpousalRemarrPayLumpSumCode
            objNewMember.SpousalRemarrWeeksLumpSum = SpousalRemarrWeeksLumpSum
            objNewMember.TableRowID = TableRowID
            objNewMember.TotalAmountSpouseChild = TotalAmountSpouseChild
            objNewMember.TotalAmountSpouseOnly = TotalAmountSpouseOnly
            objNewMember.TotalTimeChildAgeNoSchool = TotalTimeChildAgeNoSchool
            objNewMember.TotalWeeksChild = TotalWeeksChild
            objNewMember.TotalWeeksSpouse = TotalWeeksSpouse
            objNewMember.UseTTDCode = UseTTDCode
            mCol.Add(objNewMember, sKey)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".ADD|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT "
        sSQL = sSQL & "TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",JURIS_ROW_ID,EFFECTIVE_DATE,DELETED_FLAG"
        sSQL = sSQL & ",PL_MAX_SPO_CHD_AMT"
        sSQL = sSQL & ",PL_MAX_SPO_ONY_AMT"
        sSQL = sSQL & ",PL_MIN_SPO_ONY_AMT"
        sSQL = sSQL & ",PA_SPO_CHD_PCENT"
        sSQL = sSQL & ",PA_SPO_ONLY_PCENT"
        sSQL = sSQL & ",PA_ONE_CHILD_PCENT"
        sSQL = sSQL & ",SR_BENFIT_END_CODE"
        sSQL = sSQL & ",SR_PAY_LMP_SM_CODE"
        sSQL = sSQL & ",SR_WEEKS_IN_LMP_SM"
        sSQL = sSQL & ",TT_SPO_WEEKS"
        sSQL = sSQL & ",TT_CHILD_WEEKS"
        sSQL = sSQL & ",TT_CHILD_END_AGE"
        sSQL = sSQL & ",TT_SPO_CHD_AMT"
        sSQL = sSQL & ",TT_SPO_ONLY_AMT"
        sSQL = sSQL & ",USE_TTD_CODE"
        sSQL = sSQL & " FROM WCP_DETHS_RULE"
        GetSQLFieldList = sSQL


    End Function
    Public Function LoadData(ByRef objuser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim objRecord As CJRBenefitRuleDeathSuv
        Dim sSQL As String
        Try

            LoadData = 0

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID


            'objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRBenefitRuleDeathSuv
                With objRecord
                    .AWWPercentOneChild = objReader.GetInt32("PA_ONE_CHILD_PCENT")
                    .AWWPercentSpouseChild = objReader.GetInt32("PA_SPO_CHD_PCENT")
                    .AWWPercentSpouseOnly = objReader.GetInt32("PA_SPO_ONLY_PCENT")
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .PaymentMaxAmountSpouseChild = objReader.GetInt32("PL_MAX_SPO_CHD_AMT")
                    .PaymentMaxAmountSpouseOnly = objReader.GetInt32("PL_MAX_SPO_ONY_AMT")
                    .PaymentMinAmountSpouseOnly = objReader.GetInt32("PL_MIN_SPO_ONY_AMT")
                    .SpousalRemarrEndBenefitCode = objReader.GetInt32("SR_BENFIT_END_CODE")
                    .SpousalRemarrPayLumpSumCode = objReader.GetInt32("SR_PAY_LMP_SM_CODE")
                    .SpousalRemarrWeeksLumpSum = objReader.GetInt32("SR_WEEKS_IN_LMP_SM")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .TotalAmountSpouseChild = objReader.GetInt32("TT_SPO_CHD_AMT")
                    .TotalAmountSpouseOnly = objReader.GetInt32("TT_SPO_ONLY_AMT")
                    .TotalTimeChildAgeNoSchool = objReader.GetInt32("TT_CHILD_END_AGE")
                    .TotalWeeksChild = objReader.GetInt32("TT_CHILD_WEEKS")
                    .TotalWeeksSpouse = objReader.GetInt32("TT_SPO_WEEKS")
                    .UseTTDCode = objReader.GetInt32("USE_TTD_CODE")
                    Add(.AWWPercentOneChild, .AWWPercentSpouseChild, .AWWPercentSpouseOnly, .DataHasChanged, .JurisRowID, .EffectiveDateDTG, .PaymentMaxAmountSpouseChild, .PaymentMaxAmountSpouseOnly, .PaymentMinAmountSpouseOnly, .SpousalRemarrEndBenefitCode, .SpousalRemarrPayLumpSumCode, .SpousalRemarrWeeksLumpSum, .TableRowID, .TotalAmountSpouseChild, .TotalAmountSpouseOnly, .TotalTimeChildAgeNoSchool, .TotalWeeksChild, .TotalWeeksSpouse, .UseTTDCode, "k" & CStr(.TableRowID))

                End With
            End While


            ''If Not g_DBObject.DB_EOF(iRdset) Then
            ''    While objReader.Read()
            ''        objRecord = New CJRBenefitRuleDeathSuv
            ''        With objRecord
            ''            .AWWPercentOneChild = objReader.GetInt32( "PA_ONE_CHILD_PCENT")
            ''            .AWWPercentSpouseChild = objReader.GetInt32( "PA_SPO_CHD_PCENT")
            ''            .AWWPercentSpouseOnly = objReader.GetInt32( "PA_SPO_ONLY_PCENT")
            ''            .EffectiveDateDTG = objReader.GetString( "EFFECTIVE_DATE")
            ''            .JurisRowID = objReader.GetInt32( "JURIS_ROW_ID")
            ''            .PaymentMaxAmountSpouseChild = objReader.GetInt32( "PL_MAX_SPO_CHD_AMT")
            ''            .PaymentMaxAmountSpouseOnly = objReader.GetInt32( "PL_MAX_SPO_ONY_AMT")
            ''            .PaymentMinAmountSpouseOnly = objReader.GetInt32( "PL_MIN_SPO_ONY_AMT")
            ''            .SpousalRemarrEndBenefitCode = objReader.GetInt32( "SR_BENFIT_END_CODE")
            ''            .SpousalRemarrPayLumpSumCode = objReader.GetInt32( "SR_PAY_LMP_SM_CODE")
            ''            .SpousalRemarrWeeksLumpSum = objReader.GetInt32( "SR_WEEKS_IN_LMP_SM")
            ''            .TableRowID = objReader.GetInt32( "TABLE_ROW_ID")
            ''            .TotalAmountSpouseChild = objReader.GetInt32( "TT_SPO_CHD_AMT")
            ''            .TotalAmountSpouseOnly = objReader.GetInt32( "TT_SPO_ONLY_AMT")
            ''            .TotalTimeChildAgeNoSchool = objReader.GetInt32( "TT_CHILD_END_AGE")
            ''            .TotalWeeksChild = objReader.GetInt32( "TT_CHILD_WEEKS")
            ''            .TotalWeeksSpouse = objReader.GetInt32( "TT_SPO_WEEKS")
            ''            .UseTTDCode = objReader.GetInt32( "USE_TTD_CODE")
            ''            Add(.AWWPercentOneChild, .AWWPercentSpouseChild, .AWWPercentSpouseOnly, .DataHasChanged, .JurisRowID, .EffectiveDateDTG, .PaymentMaxAmountSpouseChild, .PaymentMaxAmountSpouseOnly, .PaymentMinAmountSpouseOnly, .SpousalRemarrEndBenefitCode, .SpousalRemarrPayLumpSumCode, .SpousalRemarrWeeksLumpSum, .TableRowID, .TotalAmountSpouseChild, .TotalAmountSpouseOnly, .TotalTimeChildAgeNoSchool, .TotalWeeksChild, .TotalWeeksSpouse, .UseTTDCode, "k" & CStr(.TableRowID))
            ''            
            ''        End With
            ''    End While
            ''End If

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeCloseRecordset(objReader)
            LoadData = Err.Number

            LoadData = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRuleDeathSuv
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property

    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function

    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

