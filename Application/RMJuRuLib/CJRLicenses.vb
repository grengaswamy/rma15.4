Option Strict Off
Option Explicit On
Public Class CJRLicenses
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRLicenses"

    'Class properties--local copy
    Private mCol As Collection
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objLicense As CJRLicense
        Try

            LoadData = 0


            sSQL = GetBaseSQL()
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                objLicense = New CJRLicense
                With objLicense
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .EffectiveDateDTG = Trim(objReader.GetString("EFFECTIVE_DATE"))
                    .InJurisResidRequidCode = objReader.GetInt32("RESE_IN_JURIS_CODE")
                    .ReciprocalJurisCode = objReader.GetInt32("RECIP_JURIS_CODE")
                    .DataHasChanged = False
                    Add(.DataHasChanged, .ReciprocalJurisCode, .InJurisResidRequidCode, .EffectiveDateDTG, .DeletedFlag, .JurisRowID, .TableRowID, "k" & CStr(.TableRowID))
                End With
                'UPGRADE_NOTE: Object objLicense may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objLicense = Nothing

            End While
            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = g_lErrNum
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function


    Public Function Add(ByRef DataHasChanged As Boolean, ByRef ReciprocalJurisCode As Integer, ByRef InJurisResidRequidCode As Integer, ByRef EffectiveDateDTG As String, ByRef DeletedFlag As Short, ByRef JurisRowID As Short, ByRef TableRowID As Integer, Optional ByRef sKey As String = "") As CJRLicense
        'create a new object
        Dim objNewMember As CJRLicense
        Try
            objNewMember = New CJRLicense


            'set the properties passed into the method
            objNewMember.DataHasChanged = DataHasChanged
            objNewMember.ReciprocalJurisCode = ReciprocalJurisCode
            objNewMember.InJurisResidRequidCode = InJurisResidRequidCode
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.DeletedFlag = DeletedFlag
            objNewMember.JurisRowID = JurisRowID
            objNewMember.TableRowID = TableRowID
            If Len(sKey) = 0 Then
                mCol.Add(objNewMember)
            Else
                mCol.Add(objNewMember, sKey)
            End If


            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRLicense
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub

    Private Function GetBaseSQL() As String
        Dim sSQL As String
        GetBaseSQL = ""
        sSQL = ""
        sSQL = sSQL & "SELECT "
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ",DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",EFFECTIVE_DATE,JURIS_ROW_ID,DELETED_FLAG,RESE_IN_JURIS_CODE,RECIP_JURIS_CODE"
        sSQL = sSQL & " FROM WCP_LICENSE_RULES"
        GetBaseSQL = sSQL


    End Function
End Class

