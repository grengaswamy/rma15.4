Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleTTDWAs
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRBenefitRuleTTDWA"
    Private Const sTableName As String = "WCP_RULE_TTD_WA"

    'Class properties, local copy
    Private mCol As Collection
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",JURIS_ROW_ID,DELETED_FLAG,EFFECTIVE_DATE"
        sSQL = sSQL & ",MARTIAL_STTUS_CODE"
        sSQL = sSQL & ",EXEMP_NUMB"
        sSQL = sSQL & ",PERCENT_BENEFIT"
        sSQL = sSQL & ",MIN_BENEFIT_AMT"
        sSQL = sSQL & ",MAX_SAWW_PERCENT"
        GetSQLFieldList = sSQL


    End Function
    Public Function LoadData(ByRef objuser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objTTDRule As CJRBenefitRuleTTDWA
        Try

            LoadData = 0

            sSQL = ""
            sSQL = sSQL & GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objTTDRule = New CJRBenefitRuleTTDWA
                With objTTDRule
                    .BenefitPercentage = objReader.GetDouble("PERCENT_BENEFIT")
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .Exemptions = objReader.GetInt32("EXEMP_NUMB")
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .TaxStatusCode = objReader.GetInt32("MARTIAL_STTUS_CODE")
                    .MaxSAWWPercentage = objReader.GetDouble("MAX_SAWW_PERCENT")
                    .MinCompRateMonth = objReader.GetDouble("MIN_BENEFIT_AMT")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    Add(.BenefitPercentage, .DeletedFlag, .Exemptions, .EffectiveDateDTG, .JurisRowID, .TaxStatusCode, .MaxSAWWPercentage, .MinCompRateMonth, .TableRowID, "k" & .TableRowID)
                End With
                'UPGRADE_NOTE: Object objTTDRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objTTDRule = Nothing

            End While

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function Add(ByRef BenefitPercentage As Double, ByRef DeletedFlag As Integer, ByRef Exemptions As Integer, ByRef EffectiveDateDTG As String, ByRef JurisRowID As Integer, ByRef TaxStatusCode As Integer, ByRef MaxSAWWPercentage As Double, ByRef MinCompRateMonth As Double, ByRef TableRowID As Integer, ByRef sKey As String) As CJRBenefitRuleTTDWA

        Try
            'create a new object
            Dim objNewMember As CJRBenefitRuleTTDWA
            objNewMember = New CJRBenefitRuleTTDWA

            'set the properties passed into the method
            objNewMember.BenefitPercentage = BenefitPercentage
            objNewMember.DeletedFlag = DeletedFlag
            objNewMember.Exemptions = Exemptions
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            'objNewMember.JurisDefinedWorkWeek = JurisDefinedWorkWeek
            objNewMember.JurisRowID = JurisRowID
            objNewMember.TaxStatusCode = TaxStatusCode
            objNewMember.MaxSAWWPercentage = MaxSAWWPercentage
            objNewMember.MinCompRateMonth = MinCompRateMonth
            objNewMember.TableRowID = TableRowID
            mCol.Add(objNewMember, sKey)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function

    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRuleTTDWA
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

