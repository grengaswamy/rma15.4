Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleLifeTimeImpairs
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRBenefitRuleLifeTimeImpairs"
    Const sTableName As String = "WCP_RULE_LTI"

    'local variable to hold collection
    Private mCol As Collection
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", ANNUAL_INCREASE"
        sSQL = sSQL & ", MAX_AMOUNT"
        sSQL = sSQL & ", MAX_SAWW_PERCENT"
        sSQL = sSQL & ", MIN_AMOUNT"
        sSQL = sSQL & ", MIN_SAWW_PERCENT"
        sSQL = sSQL & ", STANDARD_PERCENT"
        sSQL = sSQL & ", AL_RQ_MMIDATE_CODE"
        GetSQLFieldList = sSQL


    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim objRule As CJRBenefitRuleLifeTimeImpair
        Dim sSQL As String
        Dim TableRowID As Integer
        Try

            LoadData = 0

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRule = New CJRBenefitRuleLifeTimeImpair
                With objRule
                    .AllwaysRequireMMIDate = objReader.GetInt32("AL_RQ_MMIDATE_CODE")
                    .AnnualIncrease = objReader.GetDouble("ANNUAL_INCREASE")
                    .DataHasChanged = False
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .MaxAmount = objReader.GetDouble("MAX_AMOUNT")
                    .MaxSawwPercent = objReader.GetDouble("MAX_SAWW_PERCENT")
                    .MinAmount = objReader.GetDouble("MIN_AMOUNT")
                    .MinSawwPercent = objReader.GetDouble("MIN_SAWW_PERCENT")
                    .StandardPercent = objReader.GetDouble("STANDARD_PERCENT")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    Add2(.AllwaysRequireMMIDate, .AnnualIncrease, .DataHasChanged, .DeletedFlag, .EffectiveDateDTG, .JurisRowID, .MaxAmount, .MaxSawwPercent, .MinAmount, .MinSawwPercent, .StandardPercent, .TableRowID, "k" & CStr(.TableRowID))
                End With

            End While

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function Add(ByRef AnnualIncrease As Double, ByRef DataHasChanged As Boolean, ByRef DeletedFlag As Integer, ByRef EffectiveDateDTG As String, ByRef JurisRowID As Integer, ByRef MaxAmount As Double, ByRef MaxSawwPercent As Double, ByRef MinAmount As Double, ByRef MinSawwPercent As Double, ByRef StandardPercent As Double, ByRef TableRowID As Integer, ByRef sKey As String) As CJRBenefitRuleLifeTimeImpair
        'create a new object
        Dim objNewMember As CJRBenefitRuleLifeTimeImpair
        objNewMember = New CJRBenefitRuleLifeTimeImpair
        objNewMember.AnnualIncrease = AnnualIncrease
        objNewMember.DataHasChanged = DataHasChanged
        objNewMember.DeletedFlag = DeletedFlag
        objNewMember.EffectiveDateDTG = EffectiveDateDTG
        objNewMember.JurisRowID = JurisRowID
        objNewMember.MaxAmount = MaxAmount
        objNewMember.MaxSawwPercent = MaxSawwPercent
        objNewMember.MinAmount = MinAmount
        objNewMember.MinSawwPercent = MinSawwPercent
        objNewMember.StandardPercent = StandardPercent
        objNewMember.TableRowID = TableRowID
        mCol.Add(objNewMember, sKey)

        'set the properties passed into the method
        'return the object created
        Add = objNewMember
        'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objNewMember = Nothing



    End Function

    Public Function Add2(ByRef AllwaysRequireMMIDate As Integer, ByRef AnnualIncrease As Double, ByRef DataHasChanged As Boolean, ByRef DeletedFlag As Integer, ByRef EffectiveDateDTG As String, ByRef JurisRowID As Integer, ByRef MaxAmount As Double, ByRef MaxSawwPercent As Double, ByRef MinAmount As Double, ByRef MinSawwPercent As Double, ByRef StandardPercent As Double, ByRef TableRowID As Integer, ByRef sKey As String) As CJRBenefitRuleLifeTimeImpair
        'create a new object
        Dim objNewMember As CJRBenefitRuleLifeTimeImpair
        objNewMember = New CJRBenefitRuleLifeTimeImpair
        objNewMember.AllwaysRequireMMIDate = AllwaysRequireMMIDate
        objNewMember.AnnualIncrease = AnnualIncrease
        objNewMember.DataHasChanged = DataHasChanged
        objNewMember.DeletedFlag = DeletedFlag
        objNewMember.EffectiveDateDTG = EffectiveDateDTG
        objNewMember.JurisRowID = JurisRowID
        objNewMember.MaxAmount = MaxAmount
        objNewMember.MaxSawwPercent = MaxSawwPercent
        objNewMember.MinAmount = MinAmount
        objNewMember.MinSawwPercent = MinSawwPercent
        objNewMember.StandardPercent = StandardPercent
        objNewMember.TableRowID = TableRowID
        mCol.Add(objNewMember, sKey)

        'set the properties passed into the method
        'return the object created
        Add2 = objNewMember
        'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objNewMember = Nothing



    End Function

    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRuleLifeTimeImpair
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

