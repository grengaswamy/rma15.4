Option Strict Off
Option Explicit On
Public Class CJREDIStdRecordDNs
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CJREDIStdRecordDNs"
    Const sTableName2 As String = "EDI_STDRECDIF_LKUP"
    Const sTableName3 As String = "WCP_STDRECDIF_LKUP"

    Private mCol As Collection
    Private Function NoApplicationErrors(ByRef lRecordTypeID As Integer, ByRef lReleaseRowID As Integer, ByRef sFunction As String) As Integer

        NoApplicationErrors = 0

        If lRecordTypeID < 1 Then
            g_sErrDescription = "Application Error; Record ID must be greater than 0 (zero)."
            LogError(Err.Source & "|" & sClassName & "|" & sFunction, Erl(), 70010, Err.Source, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(70010, sClassName & "." & sFunction, g_sErrDescription)
            Exit Function
        End If
        If lReleaseRowID < 1 Then
            g_sErrDescription = "Application Error; Release ID must be greater than 0 (zero)."
            LogError(Err.Source & "|" & sClassName & "|" & sFunction, Erl(), 70010, Err.Source, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(70010, sClassName & "." & sFunction, g_sErrDescription)
            Exit Function
        End If

        NoApplicationErrors = -1



    End Function
    Private Function AssignData(ByRef sSQL As String, ByRef lJurisRowID As Integer) As Integer
        Const sFunctionName As String = "AssignData"
        Dim bCreate As Boolean
        Dim objReader As DbReader
        Dim objRecord As CJREDIStdRecordDN
        Try

            AssignData = 0

            bCreate = False

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            If objReader.Read = False And lJurisRowID > 0 Then
                bCreate = True
                Exit Function
            End If

            While objReader.Read()
                objRecord = New CJREDIStdRecordDN
                With objRecord
                    .DataHasChanged = False
                    .DNNumberText = Trim(objReader.GetString("DN_NUMBER_TXT"))
                    .EffectiveDateDTG = ""
                    .FieldFormat = objReader.GetString("FIELD_FORMAT")
                    .FieldLength = objReader.GetInt32("FIELD_LENGTH")
                    .FieldName = objReader.GetString("FIELD_NAME")
                    .FieldSeq = objReader.GetInt32("FIELD_SEQ")
                    .FieldStart = objReader.GetInt32("FIELD_START")
                    .FieldStatus = objReader.GetInt32("FIELD_STATUS")
                    .JurisRowID = 0
                    .RecordName = objReader.GetString("RECORD_DESC")
                    If lJurisRowID > 0 Then
                        .RecordTypeID = objReader.GetInt32("PT_TABLE_ROW_ID")
                    Else
                        .RecordTypeID = objReader.GetInt32("RECORD_TYPE_ID")
                    End If

                    .ReleaseName = objReader.GetString("RELEASE_NAME")
                    .ReleaseRowID = objReader.GetInt32("RELEASE_ROW_ID")
                    .RiskMasterDesc = objReader.GetString("RISKMASTER_DESC")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    Add(.DataHasChanged, .DNNumberText, .EffectiveDateDTG, .FieldFormat, .FieldLength, .FieldName, .FieldSeq, .FieldStart, .FieldStatus, .JurisRowID, .RecordName, .RecordTypeID, .ReleaseName, .ReleaseRowID, .RiskMasterDesc, .TableRowID, "k" & .TableRowID)
                End With
                'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRecord = Nothing

            End While

            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function GetFieldSelect(ByRef sTableName As String) As String
        Dim sSQL As String
        GetFieldSelect = ""
        sSQL = ""
        sSQL = sSQL & "SELECT "
        sSQL = sSQL & sTableName & ".TABLE_ROW_ID"
        sSQL = sSQL & "," & sTableName & ".DTTM_RCD_ADDED"
        sSQL = sSQL & "," & sTableName & ".ADDED_BY_USER"
        sSQL = sSQL & "," & sTableName & ".DTTM_RCD_LAST_UPD"
        sSQL = sSQL & "," & sTableName & ".UPDATED_BY_USER"
        sSQL = sSQL & "," & sTableName & ".DN_NUMBER_TXT"
        sSQL = sSQL & "," & sTableName & ".FIELD_FORMAT"
        sSQL = sSQL & "," & sTableName & ".FIELD_LENGTH"
        sSQL = sSQL & "," & sTableName & ".FIELD_SEQ"
        sSQL = sSQL & "," & sTableName & ".FIELD_START"
        sSQL = sSQL & "," & sTableName & ".FIELD_STATUS"
        sSQL = sSQL & "," & sTableName & ".FIELD_NAME"
        sSQL = sSQL & "," & sTableName & ".RECORD_TYPE_ID"
        sSQL = sSQL & "," & sTableName & ".RISKMASTER_DESC"

        GetFieldSelect = sSQL


    End Function
    Private Function GetSQLFieldListForCloning() As String
        Dim sSQL As String
        GetSQLFieldListForCloning = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED"
        sSQL = sSQL & ", ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD"
        sSQL = sSQL & ", UPDATED_BY_USER"
        sSQL = sSQL & ", DN_NUMBER_TXT"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", FIELD_FORMAT"
        sSQL = sSQL & ", FIELD_LENGTH"
        sSQL = sSQL & ", FIELD_SEQ"
        sSQL = sSQL & ", FIELD_START"
        sSQL = sSQL & ", FIELD_STATUS"
        sSQL = sSQL & ", FIELD_NAME"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", RECORD_TYPE_ID"
        sSQL = sSQL & ", RELEASE_ROW_ID"
        sSQL = sSQL & ", RISKMASTER_DESC"
        GetSQLFieldListForCloning = sSQL



    End Function
    Private Function LoadDataSQL(ByVal lJurisRowID As Integer, ByVal lRecordTypeID As Integer, ByVal lReleaseRowID As Integer, ByVal sEffectiveDateDTG As String) As String
        Dim sSQL As String

        LoadDataSQL = ""

        If lJurisRowID > 0 Then
            sSQL = ""
            sSQL = sSQL & GetFieldSelect("WCP_STDRECDIF_LKUP")
            sSQL = sSQL & ", WCP_STDFILING_LKUP.RELEASE_NAME"
            sSQL = sSQL & ", WCP_STDRECTYP_LKUP.RECORD_DESC"
            sSQL = sSQL & ", WCP_STDRECTYP_LKUP.RELEASE_ROW_ID"
            sSQL = sSQL & ", WCP_STDRECDIF_LKUP.PT_TABLE_ROW_ID"
            sSQL = sSQL & " FROM WCP_STDRECDIF_LKUP, WCP_STDRECTYP_LKUP, WCP_STDFILING_LKUP"
            sSQL = sSQL & " WHERE WCP_STDRECDIF_LKUP.PT_TABLE_ROW_ID = WCP_STDRECTYP_LKUP.TABLE_ROW_ID"
            sSQL = sSQL & " AND WCP_STDRECDIF_LKUP.PT_TABLE_ROW_ID = " & lRecordTypeID
            'sSQL = sSQL & " AND WCP_STDRECTYP_LKUP.PT_TABLE_ROW_ID = " & lReleaseRowID
            sSQL = sSQL & " AND WCP_STDFILING_LKUP.TABLE_ROW_ID = WCP_STDRECTYP_LKUP.PT_TABLE_ROW_ID"
            sSQL = sSQL & " ORDER BY DN_NUMBER_TXT ASC"
        Else
            sSQL = ""
            sSQL = sSQL & GetFieldSelect("EDI_STDRECDIF_LKUP")
            sSQL = sSQL & ", EDI_STDFILING_LKUP.RELEASE_NAME"
            sSQL = sSQL & ", EDI_STDRECTYP_LKUP.RECORD_DESC"
            sSQL = sSQL & ", EDI_STDRECTYP_LKUP.RELEASE_ROW_ID"
            sSQL = sSQL & "  FROM EDI_STDRECDIF_LKUP, EDI_STDRECTYP_LKUP, EDI_STDFILING_LKUP"
            sSQL = sSQL & " WHERE EDI_STDRECDIF_LKUP.RECORD_TYPE_ID = EDI_STDRECTYP_LKUP.TABLE_ROW_ID"
            sSQL = sSQL & " AND EDI_STDFILING_LKUP.TABLE_ROW_ID = EDI_STDRECTYP_LKUP.RELEASE_ROW_ID"
            sSQL = sSQL & " AND EDI_STDRECDIF_LKUP.RECORD_TYPE_ID = " & lRecordTypeID
            sSQL = sSQL & " AND EDI_STDRECDIF_LKUP.RELEASE_ROW_ID = " & lReleaseRowID
            sSQL = sSQL & " ORDER BY DN_NUMBER_TXT ASC"
        End If

        LoadDataSQL = sSQL


    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : CheckForExistingJurisData
    ' DateTime  : 3/17/2005 11:55
    ' Author    : jtodd22
    ' Purpose   : To check if the Jurisdiction, release and effective date combination exists
    '---------------------------------------------------------------------------------------
    '
    Public Function CheckForExistingJurisData(ByRef objUser As Riskmaster.Security.UserLogin, ByVal lJurisRowID As Integer, ByVal lReleaseRowID As Integer, ByVal sEffectiveDateDTG As String) As Integer
        Const sFunctionName As String = "CheckForExistingJurisData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objRecord As CJREDIStdRecordDN
        Try

            CheckForExistingJurisData = 0

            g_sErrDescription = ""
            sEffectiveDateDTG = Trim(sEffectiveDateDTG)

            If sEffectiveDateDTG = "" Then
                g_sErrDescription = "An Effective Date is Required."
                If Not g_bBatchMode Then Err.Raise(70001, sClassName & "." & sFunctionName, g_sErrDescription)
                LogError(Err.Source & "|" & sClassName & "|" & sFunctionName, Erl(), 70001, Err.Source, g_sErrDescription)
                Exit Function
            End If

            If lJurisRowID < 0 Then
            End If
            If lReleaseRowID < 0 Then
            End If


            sSQL = GetSQLFieldListForCloning()
            sSQL = sSQL & " FROM WCP_STDRECDIF_LKUP"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = '" & sEffectiveDateDTG & "'"
            sSQL = sSQL & " AND RELEASE_ROW_ID = " & lReleaseRowID
            sSQL = sSQL & " ORDER BY FIELD_SEQ"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                objRecord = New CJREDIStdRecordDN
                With objRecord
                    .DataHasChanged = False
                    .DNNumberText = Trim(objReader.GetString("DN_NUMBER_TXT"))
                    .EffectiveDateDTG = sEffectiveDateDTG
                    .FieldFormat = objReader.GetString("FIELD_FORMAT")
                    .FieldLength = objReader.GetInt32("FIELD_LENGTH")
                    .FieldName = objReader.GetString("FIELD_NAME")
                    .FieldSeq = objReader.GetInt32("FIELD_SEQ")
                    .FieldStart = objReader.GetInt32("FIELD_START")
                    .FieldStatus = objReader.GetInt32("FIELD_STATUS")
                    .JurisRowID = lJurisRowID
                    .RecordTypeID = objReader.GetInt32("RECORD_TYPE_ID")
                    .ReleaseName = ""
                    .ReleaseRowID = objReader.GetInt32("RELEASE_ROW_ID")
                    .RiskMasterDesc = objReader.GetString("RISKMASTER_DESC")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    Add(.DataHasChanged, .DNNumberText, .EffectiveDateDTG, .FieldFormat, .FieldLength, .FieldName, .FieldSeq, .FieldStart, .FieldStatus, .JurisRowID, .RecordName, .RecordTypeID, .ReleaseName, .ReleaseRowID, .RiskMasterDesc, .TableRowID, "k" & .TableRowID)
                End With
                'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRecord = Nothing
            End While

            CheckForExistingJurisData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            CheckForExistingJurisData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : LoadData
    ' DateTime  : 3/17/2005 11:21
    ' Author    : jtodd22
    ' Purpose   : To fetch data for the Industry Standard or the Jurisdiction
    ' Note      : The switch is the value of lJurisRowID.  If lJurisRowID is zero then fetch the
    ' ..........: Industry Standard data.  If lJurisRowis > 0 then fetch the Jurisdictional data.
    '---------------------------------------------------------------------------------------
    '
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByVal lJurisRowID As Integer, ByVal lRecordTypeID As Integer, ByVal lReleaseRowID As Integer, ByVal sEffectiveDateDTG As String) As Integer
        Const sFunctionName As String = "LoadData"
        Dim bCreate As Boolean
        Dim sSQL As String
        Try

            LoadData = 0

            g_sErrDescription = ""

            If NoApplicationErrors(lRecordTypeID, lReleaseRowID, sFunctionName) = 0 Then Exit Function

            sSQL = LoadDataSQL(lJurisRowID, lRecordTypeID, lReleaseRowID, sEffectiveDateDTG)



            LoadData = AssignData(sSQL, lJurisRowID)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            LoadData = Err.Number

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function
    Public Function LoadDataNoConnInFun(ByRef objUser As Riskmaster.Security.UserLogin, ByVal lJurisRowID As Integer, ByVal lRecordTypeID As Integer, ByVal lReleaseRowID As Integer, ByVal sEffectiveDateDTG As String) As Integer
        Const sFunctionName As String = "LoadDataNoConnInFun"
        Dim bCreate As Boolean
        Dim sSQL As String
        Try

            LoadDataNoConnInFun = 0

            g_sErrDescription = ""

            If NoApplicationErrors(lRecordTypeID, lReleaseRowID, sFunctionName) = 0 Then Exit Function

            sSQL = LoadDataSQL(lJurisRowID, lRecordTypeID, lReleaseRowID, sEffectiveDateDTG)

            LoadDataNoConnInFun = AssignData(sSQL, lJurisRowID)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            LoadDataNoConnInFun = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : LoadDataForCloning
    ' DateTime  : 3/17/2005 08:08
    ' Author    : jtodd22
    ' Purpose   : To fetch data for cloning a EDI std to a Jurisdiction
    ' Note      : As part of fetching the basic data we set the Effective Date and the Jurisdiction
    ' Note      : The data must come from the Industry Standard
    '---------------------------------------------------------------------------------------
    '
    Public Function LoadDataForCloning(ByRef objUser As Riskmaster.Security.UserLogin, ByVal lJurisRowID As Integer, ByVal lReleaseRowID As Integer, ByVal sEffectiveDateDTG As String) As Integer
        Const sFunctionName As String = "LoadDataForCloning"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objRecord As CJREDIStdRecordDN
        Try

            LoadDataForCloning = 0

            g_sErrDescription = ""
            sEffectiveDateDTG = Trim(sEffectiveDateDTG)

            If sEffectiveDateDTG = "" Then
                g_sErrDescription = "An Effective Date is Required."
                LogError(Err.Source & "|" & sClassName & "|" & sFunctionName, Erl(), 70001, Err.Source, g_sErrDescription)
                If Not g_bBatchMode Then Err.Raise(70001, sClassName & "." & sFunctionName, g_sErrDescription)
                Exit Function
            End If

            If lJurisRowID < 0 Then
            End If
            If lReleaseRowID < 0 Then
            End If



            sSQL = GetSQLFieldListForCloning()
            sSQL = sSQL & " FROM EDI_STDRECDIF_LKUP"
            sSQL = sSQL & " WHERE RELEASE_ROW_ID = " & lReleaseRowID
            sSQL = sSQL & " ORDER BY FIELD_SEQ"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                objRecord = New CJREDIStdRecordDN
                With objRecord
                    .DataHasChanged = False
                    .DNNumberText = Trim(objReader.GetString("DN_NUMBER_TXT"))
                    .EffectiveDateDTG = sEffectiveDateDTG
                    .FieldFormat = objReader.GetString("FIELD_FORMAT")
                    .FieldLength = objReader.GetInt32("FIELD_LENGTH")
                    .FieldName = objReader.GetString("FIELD_NAME")
                    .FieldSeq = objReader.GetInt32("FIELD_SEQ")
                    .FieldStart = objReader.GetInt32("FIELD_START")
                    .FieldStatus = objReader.GetInt32("FIELD_STATUS")
                    .JurisRowID = lJurisRowID
                    .RecordTypeID = objReader.GetInt32("RECORD_TYPE_ID")
                    .ReleaseName = ""
                    .ReleaseRowID = objReader.GetInt32("RELEASE_ROW_ID")
                    .RiskMasterDesc = objReader.GetString("RISKMASTER_DESC")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    Add(.DataHasChanged, .DNNumberText, .EffectiveDateDTG, .FieldFormat, .FieldLength, .FieldName, .FieldSeq, .FieldStart, .FieldStatus, .JurisRowID, .RecordName, .RecordTypeID, .ReleaseName, .ReleaseRowID, .RiskMasterDesc, .TableRowID, "k" & .TableRowID)
                End With
                'UPGRADE_NOTE: Object objRecord may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRecord = Nothing

            End While
            LoadDataForCloning = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            LoadDataForCloning = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)


        End Try

    End Function


    Public Function Add(ByRef DataHasChanged As Boolean, ByRef DNNumberText As String, ByRef EffectiveDateDTG As String, ByRef FieldFormat As String, ByRef FieldLength As Integer, ByRef FieldName As String, ByRef FieldSeq As Integer, ByRef FieldStart As Integer, ByRef FieldStatus As Integer, ByRef JurisRowID As Integer, ByRef RecordName As String, ByRef RecordTypeID As Integer, ByRef ReleaseName As String, ByRef ReleaseRowID As Integer, ByRef RiskMasterDesc As String, ByRef TableRowID As Integer, ByRef sKey As String) As CJREDIStdRecordDN
        Const sFunctionName As String = "Add"
        Dim objNewMember As CJREDIStdRecordDN
        Try
            objNewMember = New CJREDIStdRecordDN

            'set the properties passed into the method
            objNewMember.DataHasChanged = DataHasChanged
            objNewMember.DNNumberText = DNNumberText
            objNewMember.ReleaseRowID = ReleaseRowID
            objNewMember.EffectiveDateDTG = EffectiveDateDTG
            objNewMember.FieldFormat = FieldFormat
            objNewMember.FieldLength = FieldLength
            objNewMember.FieldName = FieldName
            objNewMember.FieldSeq = FieldSeq
            objNewMember.FieldStart = FieldStart
            objNewMember.FieldStatus = FieldStatus
            objNewMember.JurisRowID = JurisRowID
            objNewMember.RecordName = RecordName
            objNewMember.RecordTypeID = RecordTypeID
            objNewMember.ReleaseName = ReleaseName
            objNewMember.ReleaseRowID = ReleaseRowID
            objNewMember.RiskMasterDesc = RiskMasterDesc
            objNewMember.TableRowID = TableRowID
            mCol.Add(objNewMember, sKey)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = g_sErrDescription & Err.Description
            End With
            'UPGRADE_WARNING: Couldn't resolve default property of object Add. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'Add = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            If Not g_bBatchMode Then Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Function

    Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJREDIStdRecordDN
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

