Option Strict Off
Option Explicit On
Public Class CJRSpendableIncomeCalcFed
    Const sClassName As String = "CJRSpendableIncomeCalcFederal"
    Const sTableName As String = "WCP_WITHHOL_FD"
    'Class properties--local copy
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Short
    Private m_EffectiveDateDTG As String
    Private m_FedExemptionValueAmount As Double
    Private m_FICAPercentage As Double
    Private m_FedMinAmount As Double
    Private m_FedMaxAmount As Double
    Private m_FedMultipler As Double
    Private m_FedOffsetAmount As Double
    Private m_JurisRowID As Short
    Private m_TableRowID As Integer
    Private m_TaxStatusCode As Integer
    Private m_TaxExemptions As Integer 'not jurisdictional rule
    Private m_Warning As String


    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try

            AssignData = 0
            ClearObject()


            'objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            'If Not g_DBObject.DB_EOF(iRdset) Then
            '	m_DeletedFlag = objReader.GetInt32( "DELETED_FLAG")
            '	m_EffectiveDateDTG = objReader.GetString( "EFFECTIVE_DATE")
            '	m_FedExemptionValueAmount = objReader.GetDouble( "EXEMP_VALUE_AMT")
            '	m_FedMaxAmount = objReader.GetDouble( "FED_MAX_AMT")
            '	m_FedMinAmount = objReader.GetDouble( "FED_MIN_AMT")
            '	m_FedMultipler = objReader.GetDouble( "FED_MULTIPLER")
            '	m_FedOffsetAmount = objReader.GetDouble( "FED_OFFSET_AMT")
            '	m_FICAPercentage = objReader.GetDouble( "FICA_PERCENT")
            '	m_JurisRowID = objReader.GetInt32( "JURIS_ROW_ID")
            '	m_TableRowID = objReader.GetInt32( "TABLE_ROW_ID")
            '	m_TaxStatusCode = objReader.GetInt32( "TAX_STATUS_CODE")
            '      End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_FedExemptionValueAmount = objReader.GetDouble("EXEMP_VALUE_AMT")
                m_FedMaxAmount = objReader.GetDouble("FED_MAX_AMT")
                m_FedMinAmount = objReader.GetDouble("FED_MIN_AMT")
                m_FedMultipler = objReader.GetDouble("FED_MULTIPLER")
                m_FedOffsetAmount = objReader.GetDouble("FED_OFFSET_AMT")
                m_FICAPercentage = objReader.GetDouble("FICA_PERCENT")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_TaxStatusCode = objReader.GetInt32("TAX_STATUS_CODE")
            End If
            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function ClearObject() As Integer
        m_TableRowID = 0
        m_JurisRowID = 0
        m_EffectiveDateDTG = vbNullString
        m_DeletedFlag = 0
        m_TaxStatusCode = 0
        m_FedExemptionValueAmount = 0
        m_FICAPercentage = 0
        m_FedMinAmount = 0
        m_FedMaxAmount = 0
        m_FedMultipler = 0
        m_FedOffsetAmount = 0

        'jtodd22 08/11/2005 no-no---m_TaxExemptions = 0


    End Function
    Private Function GetSQLFieldSelect() As String
        Dim sSQL As String
        GetSQLFieldSelect = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ",DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",EFFECTIVE_DATE"
        sSQL = sSQL & ",JURIS_ROW_ID"
        sSQL = sSQL & ",DELETED_FLAG"
        sSQL = sSQL & ",TAX_STATUS_CODE"
        sSQL = sSQL & ",EXEMP_VALUE_AMT"
        sSQL = sSQL & ",FICA_PERCENT"
        sSQL = sSQL & ",FED_MIN_AMT"
        sSQL = sSQL & ",FED_MAX_AMT"
        sSQL = sSQL & ",FED_MULTIPLER"
        sSQL = sSQL & ",FED_OFFSET_AMT"
        GetSQLFieldSelect = sSQL


    End Function
    Public Function GetFederalWithHoldingTotal(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String, ByRef dAWW As Double, ByRef lTaxStatusCode As Integer, ByRef lExemptions As Integer) As Double
        Const sFunctionName As String = "GetFederalWithHoldingTotal"
        Dim lReturn As Integer
        Dim dEarningsSubject As Double
        Dim dFICA As Double
        Dim dWithHolding As Double
        Try
            GetFederalWithHoldingTotal = -1

            lReturn = Me.LoadDataByClaimData(objUser, lJurisRowID, sDateOfEventDTG, dAWW, lTaxStatusCode, lExemptions)
            If lReturn <> -1 Then Exit Function

            dFICA = dAWW * m_FICAPercentage
            dEarningsSubject = dAWW - (lExemptions * m_FedExemptionValueAmount)

            If m_FedMaxAmount + 0 = 0 Then
                GetFederalWithHoldingTotal = 0
            Else
                GetFederalWithHoldingTotal = (dEarningsSubject - m_FedMaxAmount) * m_FedMultipler + m_FedOffsetAmount
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number : g_sErrSrc = .Source : g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|modFunction." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataByTableRowID"
        Dim objReader As DbReader
        Dim sSQL As String
        Try
            LoadDataByTableRowID = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL = GetSQLFieldSelect()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataByTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadDataByClaimData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String, ByRef dAWW As Double, ByRef lTaxStatusCode As Integer, ByRef lTaxExemptions As Integer) As Integer
        Const sFunctionName As String = "LoadDataByClaimData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sTmp As String
        Try
            LoadDataByClaimData = 0

            If Trim(sTableName & "") = "" Then Exit Function

            m_TaxExemptions = lTaxExemptions

            sTmp = ""
            sTmp = sTmp & "(SELECT MAX(EFFECTIVE_DATE) FROM WCP_WITHHOLDIG"
            sTmp = sTmp & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sTmp = sTmp & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"
            sTmp = sTmp & " AND TAX_STATUS_CODE = " & lTaxStatusCode
            sTmp = sTmp & " AND " & dAWW & " BETWEEN  FED_MAX_AMT AND FED_MIN_AMT)"

            sSQL = GetSQLFieldSelect()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"
            sSQL = sSQL & " AND TAX_STATUS_CODE = " & lTaxStatusCode
            sSQL = sSQL & " AND " & dAWW & " BETWEEN FED_MAX_AMT AND FED_MIN_AMT"

            LoadDataByClaimData = AssignData(objUser, sSQL)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByClaimData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        Try

            SaveData = 0



            sSQL = ""
            sSQL = sSQL & "SELECT *"
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                'existing record -- do an edit
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("DELETED_FLAG").Value = 0
                objWriter.Fields("TAX_STATUS_CODE").Value = m_TaxStatusCode
                objWriter.Fields("EXEMP_VALUE_AMT").Value = m_FedExemptionValueAmount
                objWriter.Fields("FICA_PERCENT").Value = m_FICAPercentage
                objWriter.Fields("FED_MAX_AMT").Value = m_FedMaxAmount
                objWriter.Fields("FED_MIN_AMT").Value = m_FedMinAmount
                objWriter.Fields("FED_MULTIPLER").Value = m_FedMultipler
                objWriter.Fields("FED_OFFSET_AMT").Value = m_FedOffsetAmount
                objWriter.Execute()
            Else
                'new record
                SafeCloseRecordset(objReader)
                objWriter = DbFactory.GetDbWriter(objReader, False)
                m_TableRowID = lGetNextUID(sTableName)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))

                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("DELETED_FLAG", 0)
                objWriter.Fields.Add("TAX_STATUS_CODE", m_TaxStatusCode)
                objWriter.Fields.Add("EXEMP_VALUE_AMT", m_FedExemptionValueAmount)
                objWriter.Fields.Add("FICA_PERCENT", m_FICAPercentage)
                objWriter.Fields.Add("FED_MAX_AMT", m_FedMaxAmount)
                objWriter.Fields.Add("FED_MIN_AMT", m_FedMinAmount)
                objWriter.Fields.Add("FED_MULTIPLER", m_FedMultipler)
                objWriter.Fields.Add("FED_OFFSET_AMT", m_FedOffsetAmount)
                objWriter.Execute()
            End If

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Function ValidateData() As Integer
        ValidateData = 0
        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_TaxStatusCode < 1 Then
            m_Warning = m_Warning & "A Tax Status is required." & vbCrLf
        End If


    End Function
    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property FICAPercentage() As Double
        Get
            FICAPercentage = m_FICAPercentage
        End Get
        Set(ByVal Value As Double)
            m_FICAPercentage = Value
        End Set
    End Property
    Public Property FedMinAmount() As Double
        Get
            FedMinAmount = m_FedMinAmount
        End Get
        Set(ByVal Value As Double)
            m_FedMinAmount = Value
        End Set
    End Property
    Public Property FedMaxAmount() As Double
        Get
            FedMaxAmount = m_FedMaxAmount
        End Get
        Set(ByVal Value As Double)
            m_FedMaxAmount = Value
        End Set
    End Property

    Public Property FedOffsetAmount() As Double
        Get
            FedOffsetAmount = m_FedOffsetAmount
        End Get
        Set(ByVal Value As Double)
            m_FedOffsetAmount = Value
        End Set
    End Property
    Public Property FedMultipler() As Double
        Get
            FedMultipler = m_FedMultipler
        End Get
        Set(ByVal Value As Double)
            m_FedMultipler = Value
        End Set
    End Property
    Public Property FedExemptionValueAmount() As Double
        Get
            FedExemptionValueAmount = m_FedExemptionValueAmount
        End Get
        Set(ByVal Value As Double)
            m_FedExemptionValueAmount = Value
        End Set
    End Property

    Public Property TaxStatusCode() As Integer
        Get
            TaxStatusCode = m_TaxStatusCode
        End Get
        Set(ByVal Value As Integer)
            m_TaxStatusCode = Value
        End Set
    End Property



    Public Property Warning() As String
        Get

            Warning = m_Warning

        End Get
        Set(ByVal Value As String)

            m_Warning = Value

        End Set
    End Property
End Class

