Option Strict Off
Option Explicit On
Public Class CJRCommTypes
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRCommTypes"
    Private Const sTableName As String = "WCP_COMM_TYPES"
    'Class properties--local copy
    Private mCol As Collection

    Private Function Add(ByRef BenefitAbbr As String, ByRef DataHasChanged As Boolean, ByRef DeletedFlag As Short, ByRef ShortCode As String, ByRef TypeDesc As String, ByRef TableRowID As Integer, ByRef sKey As String) As CJRCommType
        Try
            Dim objNewMember As CJRCommType
            objNewMember = New CJRCommType

            'set the properties passed into the method
            With objNewMember
                .BenefitAbbr = BenefitAbbr
                .DataHasChanged = DataHasChanged
                .DeletedFlag = DeletedFlag
                .ShortCode = ShortCode
                .TypeDesc = TypeDesc
                .TableRowID = TableRowID
            End With
            mCol.Add(objNewMember, sKey)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT "
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED, ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD, UPDATED_BY_USER"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", BENEFIT_ABBR"
        sSQL = sSQL & ", SHORT_CODE"
        sSQL = sSQL & ", TYPE_DESC"
        GetSQLFieldList = sSQL


    End Function

    Public Function LoadDataByBenefit(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sBenefitAbbr As String) As Integer
        Const sFunctionName As String = "LoadDataByBenefit"
        Dim objReader As DbReader
        Dim objRecord As CJRCommType
        Dim sSQL As String
        Try

            LoadDataByBenefit = 0



            sSQL = ""
            sSQL = sSQL & GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE BENEFIT_ABBR = '" & sBenefitAbbr & "'"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objRecord = New CJRCommType
                With objRecord
                    .BenefitAbbr = objReader.GetString("BENEFIT_ABBR")
                    .DataHasChanged = False
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .ShortCode = objReader.GetString("SHORT_CODE")
                    .TypeDesc = objReader.GetString("TYPE_DESC")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    Add(.BenefitAbbr, .DataHasChanged, .DeletedFlag, .ShortCode, .TypeDesc, .TableRowID, "k" & CStr(.TableRowID))

                End With
            End While

            LoadDataByBenefit = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByBenefit = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRCommType
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

