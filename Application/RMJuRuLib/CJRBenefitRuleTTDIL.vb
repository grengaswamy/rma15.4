Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleTTDIL
    Const sClassName As String = "CJRBenefitRuleTTDIL"
    Const sTableName As String = "WCP_RULE_TTD_IL"
    'local variable(s) not publicly exposed
    Const dWeekToMonthConvFactorDefault As Double = 4.34821

    Dim m_sDateOfEventDTG As String 'jtodd22 not a Class Property

    Public objJurisWorkWeek As New CJRXJurisWorkWeek

    'to support future interface
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_ErrorMaskJR As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_ErrorMaskSAWW As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_JurisDefinedWorkWeek As Integer 'jtodd22 03/08/2005 not part of the rule, but used as part of rule in RMWCCalc.dll
    Private m_JurisRowID As Integer
    Private m_MaxCompRateMonthly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MaxCompRateWeekly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_NoCodeID As Integer
    Private m_PayPeriodCode As Integer
    Private m_PayPeriodLockedInCalcCode As Integer
    Private m_RuleTotalWeeks As Double
    Private m_TableRowID As Integer
    Private m_Warning As String
    Private m_UseTwoThirdsCode As Integer
    Private m_WeekToMonthConvFactor As Double
    Private m_YesCodeID As Integer

    'local variable(s) to hold property value(s)
    Private m_MaxPercentOfSAWW As Double
    Private m_PayConCurrentPPD As Integer
    Private m_PrimeRate As Double
    Private m_TotalAmount As Double
    Private m_UseSAWWMaximumCode As Integer
    'jtodd22 08/01/2006--To support Illinois Minimum
    Private m_MarriedValue As Double
    Private m_SingleValue As Double
    Private m_Child1 As Double
    Private m_Child2 As Double
    Private m_Child3 As Double
    Private m_Child4 As Double
    Private m_ClaimantsAWWCode As Integer
    Private m_UseSAWWMinimumCode As Integer
    'New for 01 Febuary 2006 changes
    Private m_ChildSpouse0 As Double
    Private m_ChildSpouse1 As Double
    Private m_ChildSpouse2 As Double
    Private m_ChildSpouse3 As Double
    Private m_ChildSpouse4P As Double
    Private m_ClaimantsAWWBCode As Integer
    Private m_UseSAWWMinimumBCode As Integer

    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Dim lReturn As Integer

        Try

            AssignData = 0
            ClearObject()


            m_YesCodeID = GetYesCodeID() 'jtodd22 requires a DB connection

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_DataHasChanged = False
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_MaxCompRateWeekly = objReader.GetDouble("MAX_COMP_RATE")
                m_MaxPercentOfSAWW = objReader.GetDouble("MAX_PERCENT_OFSAWW")
                m_PayConCurrentPPD = objReader.GetInt32("PAY_CONC_PPD_CODE")
                m_PrimeRate = objReader.GetDouble("PRIME_RATE")
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_TotalAmount = objReader.GetDouble("TOTAL_AMT")
                m_RuleTotalWeeks = objReader.GetDouble("TOTAL_WEEKS")
                m_UseSAWWMaximumCode = objReader.GetInt32("USE_SAWW_MAX_CODE")

                'jtodd22 08/01/2006 --group the minimum fields
                m_Child1 = objReader.GetDouble("CHILD_A")
                m_Child2 = objReader.GetDouble("CHILD_B")
                m_Child3 = objReader.GetDouble("CHILD_C")
                m_Child4 = objReader.GetDouble("CHILD_D")
                m_ClaimantsAWWCode = objReader.GetInt32("CLAIMANT_AWW_CODE")
                m_MarriedValue = objReader.GetDouble("MARRIED_VALUE")
                m_SingleValue = objReader.GetDouble("SINGLE_VALUE")
                m_UseSAWWMinimumCode = objReader.GetInt32("USE_SAWW_MIN_CODE")
                m_ChildSpouse0 = objReader.GetDouble("CHILD_SPOUSE_A_AMT")
                m_ChildSpouse1 = objReader.GetDouble("CHILD_SPOUSE_B_AMT")
                m_ChildSpouse2 = objReader.GetDouble("CHILD_SPOUSE_C_AMT")
                m_ChildSpouse3 = objReader.GetDouble("CHILD_SPOUSE_D_AMT")
                m_ChildSpouse4P = objReader.GetDouble("CHILD_SPOUSE_E_AMT")
                m_ClaimantsAWWBCode = objReader.GetInt32("CLAIMANT_AWWB_CODE")
                m_UseSAWWMinimumBCode = objReader.GetInt32("USE_SAWW_MINB_CODE")

            End If

            SafeCloseRecordset(objReader)

            m_DataHasChanged = False
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID

            lReturn = Me.objJurisWorkWeek.LoadData(m_JurisRowID, m_EffectiveDateDTG)
            If lReturn = -1 Then
                m_JurisDefinedWorkWeek = Me.objJurisWorkWeek.JurisDefinedWorkWeek
                AssignData = -1
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function ClearObject() As Integer
        'jtodd22 support for future interface
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_ErrorMaskJR = 0
        m_ErrorMaskSAWW = 0
        m_JurisDefinedWorkWeek = 0
        m_JurisRowID = 0
        m_MaxCompRateMonthly = 0
        m_MaxCompRateWeekly = 0
        m_NoCodeID = 0
        m_PayPeriodCode = 0
        m_PayPeriodLockedInCalcCode = 0
        m_RuleTotalWeeks = 0
        m_TableRowID = 0
        m_UseTwoThirdsCode = 0
        m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault
        m_YesCodeID = 0

        m_Child1 = 0
        m_Child2 = 0
        m_Child3 = 0
        m_Child4 = 0
        m_ClaimantsAWWCode = 0
        m_MarriedValue = 0
        m_MaxCompRateWeekly = 0
        m_MaxPercentOfSAWW = 0
        m_PayConCurrentPPD = 0
        m_SingleValue = 0
        m_TotalAmount = 0
        m_UseSAWWMaximumCode = 0
        m_UseSAWWMinimumCode = 0
        m_ChildSpouse0 = 0
        m_ChildSpouse1 = 0
        m_ChildSpouse2 = 0
        m_ChildSpouse3 = 0
        m_ChildSpouse4P = 0


    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ",DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ",JURIS_ROW_ID,DELETED_FLAG,EFFECTIVE_DATE"
        sSQL = sSQL & ",MAX_COMP_RATE"
        sSQL = sSQL & ",MAX_PERCENT_OFSAWW"
        sSQL = sSQL & ",PAY_CONC_PPD_CODE"
        sSQL = sSQL & ",PRIME_RATE"
        sSQL = sSQL & ",TOTAL_WEEKS"
        sSQL = sSQL & ",TOTAL_AMT"
        sSQL = sSQL & ",USE_SAWW_MAX_CODE"
        'jtodd22 08/01/2006 --group the minimum fields
        sSQL = sSQL & ",CHILD_A"
        sSQL = sSQL & ",CHILD_B"
        sSQL = sSQL & ",CHILD_C"
        sSQL = sSQL & ",CHILD_D"
        sSQL = sSQL & ",CLAIMANT_AWW_CODE"
        sSQL = sSQL & ",MARRIED_VALUE"
        sSQL = sSQL & ",SINGLE_VALUE"
        sSQL = sSQL & ",USE_SAWW_MIN_CODE"
        sSQL = sSQL & ",CHILD_SPOUSE_A_AMT"
        sSQL = sSQL & ",CHILD_SPOUSE_B_AMT"
        sSQL = sSQL & ",CHILD_SPOUSE_C_AMT"
        sSQL = sSQL & ",CHILD_SPOUSE_D_AMT"
        sSQL = sSQL & ",CHILD_SPOUSE_E_AMT"
        sSQL = sSQL & ",CLAIMANT_AWWB_CODE"
        sSQL = sSQL & ",USE_SAWW_MINB_CODE"

        GetSQLFieldList = sSQL


    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : DeleteRecord
    ' DateTime  : 4/18/2006 07:43
    ' Author    : jtodd22
    ' Purpose   :
    ' Note      : lReturnCode is a number defining why the record was not deleted
    '---------------------------------------------------------------------------------------
    '
    Public Function DeleteRecord(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sEffectiveDateDBFormat As String, ByRef lReturnCode As Integer) As Integer
        Const sFunctionName As String = "DeleteRecord"

        Try

            DeleteRecord = modFunctions.DeleteRecordTemporary(objUser, lJurisRowID, sEffectiveDateDBFormat, lReturnCode, sTableName)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            DeleteRecord = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataByTableRowID"
        Dim sSQL As String
        Try
            LoadDataByTableRowID = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataByTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim lReturn As Integer
        Dim sSQL As String
        Dim sSQL2 As String
        Try
            LoadDataByEventDate = 0

            If Trim(sTableName & "") = "" Then Exit Function

            m_sDateOfEventDTG = sDateOfEventDTG

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            lReturn = AssignData(objUser, sSQL)
            If lReturn <> -1 Then Exit Function

            Dim objSAWW As CJRSAWW
            If m_UseSAWWMaximumCode = m_YesCodeID Or m_UseSAWWMinimumCode = m_YesCodeID Then
                objSAWW = New CJRSAWW
                lReturn = objSAWW.LoadDataByEventDate(objUser, m_JurisRowID, m_sDateOfEventDTG)
                Select Case lReturn
                    Case -1 'Normal, expected
                        If objSAWW.TableRowID = 0 Then
                            m_ErrorMaskSAWW = modGlobals.DisBenCalcMask.dbcNoJurisdictionalRule
                            'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                            objSAWW = Nothing
                            Exit Function
                        End If
                    Case Else 'error
                        'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                        objSAWW = Nothing
                        'error should/will bubble up
                        Exit Function
                End Select
                If m_UseSAWWMaximumCode = m_YesCodeID Then
                    If objSAWW.PercentMax > 0 Then
                        m_MaxCompRateWeekly = objSAWW.SAWWAmount * objSAWW.PercentMax / 100
                    End If
                    If objSAWW.MaxAmountAsPublished > 0 Then
                        m_MaxCompRateWeekly = objSAWW.MaxAmountAsPublished
                    End If
                End If
                ''        If m_UseSAWWMinimumCode = m_YesCodeID Then
                ''            m_FloorAmount = objSAWW.SAWWAmount * objSAWW.PercentMin
                ''            If objSAWW.MinAmountAsPublished > 0 Then
                ''                m_FloorAmount = objSAWW.MinAmountAsPublished
                ''            End If
                ''        End If
                'UPGRADE_NOTE: Object objSAWW may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objSAWW = Nothing
            End If
            LoadDataByEventDate = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try

            SaveData = 0



            sSQL = ""
            sSQL = sSQL & "SELECT "
            sSQL = sSQL & " *"
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)

                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("MAX_COMP_RATE").Value = m_MaxCompRateWeekly
                objWriter.Fields("MAX_PERCENT_OFSAWW").Value = m_MaxPercentOfSAWW
                objWriter.Fields("PAY_CONC_PPD_CODE").Value = m_PayConCurrentPPD
                objWriter.Fields("PRIME_RATE").Value = m_PrimeRate
                objWriter.Fields("TOTAL_AMT").Value = m_TotalAmount
                objWriter.Fields("TOTAL_WEEKS").Value = m_RuleTotalWeeks
                objWriter.Fields("USE_SAWW_MAX_CODE").Value = m_UseSAWWMaximumCode
                'jtodd22 08/01/2006 -group the Minimum fields
                objWriter.Fields("CHILD_A").Value = m_Child1
                objWriter.Fields("CHILD_B").Value = m_Child2
                objWriter.Fields("CHILD_C").Value = m_Child3
                objWriter.Fields("CHILD_D").Value = m_Child4
                objWriter.Fields("CLAIMANT_AWW_CODE").Value = m_ClaimantsAWWCode
                objWriter.Fields("MARRIED_VALUE").Value = m_MarriedValue
                objWriter.Fields("SINGLE_VALUE").Value = m_SingleValue
                objWriter.Fields("USE_SAWW_MIN_CODE").Value = m_UseSAWWMinimumCode
                objWriter.Fields("CHILD_SPOUSE_A_AMT").Value = m_ChildSpouse0
                objWriter.Fields("CHILD_SPOUSE_B_AMT").Value = m_ChildSpouse1
                objWriter.Fields("CHILD_SPOUSE_C_AMT").Value = m_ChildSpouse2
                objWriter.Fields("CHILD_SPOUSE_D_AMT").Value = m_ChildSpouse3
                objWriter.Fields("CHILD_SPOUSE_E_AMT").Value = m_ChildSpouse4P
                objWriter.Fields("CLAIMANT_AWWB_CODE").Value = m_ClaimantsAWWBCode
                objWriter.Fields("USE_SAWW_MINB_CODE").Value = m_UseSAWWMinimumBCode
            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("MAX_COMP_RATE", m_MaxCompRateWeekly)
                objWriter.Fields.Add("MAX_PERCENT_OFSAWW", m_MaxPercentOfSAWW)
                objWriter.Fields.Add("PAY_CONC_PPD_CODE", m_PayConCurrentPPD)
                objWriter.Fields.Add("PRIME_RATE", m_PrimeRate)
                objWriter.Fields.Add("TOTAL_AMT", m_TotalAmount)
                objWriter.Fields.Add("TOTAL_WEEKS", m_RuleTotalWeeks)
                objWriter.Fields.Add("USE_SAWW_MAX_CODE", m_UseSAWWMaximumCode)
                'jtodd22 08/01/2006 -group the Minimum fields
                objWriter.Fields.Add("CHILD_A", m_Child1)
                objWriter.Fields.Add("CHILD_B", m_Child2)
                objWriter.Fields.Add("CHILD_C", m_Child3)
                objWriter.Fields.Add("CHILD_D", m_Child4)
                objWriter.Fields.Add("CLAIMANT_AWW_CODE", m_ClaimantsAWWCode)
                objWriter.Fields.Add("MARRIED_VALUE", m_MarriedValue)
                objWriter.Fields.Add("SINGLE_VALUE", m_SingleValue)
                objWriter.Fields.Add("USE_SAWW_MIN_CODE", m_UseSAWWMinimumCode)
                objWriter.Fields.Add("CHILD_SPOUSE_A_AMT", m_ChildSpouse0)
                objWriter.Fields.Add("CHILD_SPOUSE_B_AMT", m_ChildSpouse1)
                objWriter.Fields.Add("CHILD_SPOUSE_C_AMT", m_ChildSpouse2)
                objWriter.Fields.Add("CHILD_SPOUSE_D_AMT", m_ChildSpouse3)
                objWriter.Fields.Add("CHILD_SPOUSE_E_AMT", m_ChildSpouse4P)
                objWriter.Fields.Add("CLAIMANT_AWWB_CODE", m_ClaimantsAWWBCode)
                objWriter.Fields.Add("USE_SAWW_MINB_CODE", m_UseSAWWMinimumBCode)
            End If
            objWriter.Execute()
            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function ValidateData() As Integer

        ValidateData = 0
        m_Warning = ""
        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If Not IsNumeric(m_PrimeRate) Then
            m_Warning = m_Warning & "A value greater than 0 (zero) is required for 'Prime Rate'." & vbCrLf
        Else
            If m_PrimeRate = 0 Then
                m_Warning = m_Warning & "A value greater than 0 (zero) is required for 'Prime Rate'." & vbCrLf
            End If
        End If
        If m_UseSAWWMaximumCode < 1 Then
            m_Warning = m_Warning & "A 'Yes' or 'No' is required for 'Use Maximum From SAWW Table'." & vbCrLf
        End If
        If m_EffectiveDateDTG < "20060201" Then
            If m_UseSAWWMinimumCode < 1 Then
                m_Warning = m_Warning & "A 'Yes' or 'No' is required for 'Use Minimum From SAWW Table'." & vbCrLf
            End If
            If m_ClaimantsAWWCode < 1 Then
                m_Warning = m_Warning & "A 'Yes' or 'No' is required for 'Use Claimant's AWW'." & vbCrLf
            End If
        Else
            If m_UseSAWWMinimumBCode < 1 Then
                m_Warning = m_Warning & "A 'Yes' or 'No' is required for 'Use Minimum From SAWW Table'." & vbCrLf
            End If
            If m_ClaimantsAWWBCode < 1 Then
                m_Warning = m_Warning & "A 'Yes' or 'No' is required for 'Use Claimant's AWW'." & vbCrLf
            End If
        End If


    End Function

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property ErrorMaskJR() As Integer
        Get
            ErrorMaskJR = m_ErrorMaskJR
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskJR = Value
        End Set
    End Property

    Public Property ErrorMaskSAWW() As Integer
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property

    Public Property PayConCurrentPPD() As Integer
        Get
            PayConCurrentPPD = m_PayConCurrentPPD
        End Get
        Set(ByVal Value As Integer)
            m_PayConCurrentPPD = Value
        End Set
    End Property

    Public Property TotalAmount() As Double
        Get
            TotalAmount = m_TotalAmount
        End Get
        Set(ByVal Value As Double)
            m_TotalAmount = Value
        End Set
    End Property

    Public Property UseSAWWMaximumCode() As Integer
        Get
            UseSAWWMaximumCode = m_UseSAWWMaximumCode
        End Get
        Set(ByVal Value As Integer)
            m_UseSAWWMaximumCode = Value
        End Set
    End Property

    Public Property MaxCompRateMonthly() As Double
        Get
            MaxCompRateMonthly = m_MaxCompRateMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateMonthly = Value
        End Set
    End Property

    Public Property MaxCompRateWeekly() As Double
        Get
            MaxCompRateWeekly = m_MaxCompRateWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateWeekly = Value
        End Set
    End Property

    Public Property MaxPercentOfSAWW() As Double
        Get
            MaxPercentOfSAWW = m_MaxPercentOfSAWW
        End Get
        Set(ByVal Value As Double)
            m_MaxPercentOfSAWW = Value
        End Set
    End Property


    Public Property PrimeRate() As Double
        Get
            PrimeRate = m_PrimeRate
        End Get
        Set(ByVal Value As Double)
            m_PrimeRate = Value
        End Set
    End Property


    Public Property JurisDefinedWorkWeek() As Integer
        Get
            JurisDefinedWorkWeek = m_JurisDefinedWorkWeek
        End Get
        Set(ByVal Value As Integer)
            m_JurisDefinedWorkWeek = Value
        End Set
    End Property

    Public Property MarriedValue() As Double
        Get
            MarriedValue = m_MarriedValue
        End Get
        Set(ByVal Value As Double)
            m_MarriedValue = Value
        End Set
    End Property

    Public Property SingleValue() As Double
        Get
            SingleValue = m_SingleValue
        End Get
        Set(ByVal Value As Double)
            m_SingleValue = Value
        End Set
    End Property

    Public Property Child1() As Double
        Get
            Child1 = m_Child1
        End Get
        Set(ByVal Value As Double)
            m_Child1 = Value
        End Set
    End Property

    Public Property Child2() As Double
        Get
            Child2 = m_Child2
        End Get
        Set(ByVal Value As Double)
            m_Child2 = Value
        End Set
    End Property

    Public Property Child3() As Double
        Get
            Child3 = m_Child3
        End Get
        Set(ByVal Value As Double)
            m_Child3 = Value
        End Set
    End Property

    Public Property Child4() As Double
        Get
            Child4 = m_Child4
        End Get
        Set(ByVal Value As Double)
            m_Child4 = Value
        End Set
    End Property
    Public Property ClaimantsAWWCode() As Integer
        Get
            ClaimantsAWWCode = m_ClaimantsAWWCode
        End Get
        Set(ByVal Value As Integer)
            m_ClaimantsAWWCode = Value
        End Set
    End Property

    Public Property UseSAWWMinimumCode() As Integer
        Get
            UseSAWWMinimumCode = m_UseSAWWMinimumCode
        End Get
        Set(ByVal Value As Integer)
            m_UseSAWWMinimumCode = Value
        End Set
    End Property


    Public Property ChildSpouse0() As Double
        Get
            ChildSpouse0 = m_ChildSpouse0
        End Get
        Set(ByVal Value As Double)
            m_ChildSpouse0 = Value
        End Set
    End Property


    Public Property ChildSpouse1() As Double
        Get
            ChildSpouse1 = m_ChildSpouse1
        End Get
        Set(ByVal Value As Double)
            m_ChildSpouse1 = Value
        End Set
    End Property


    Public Property ChildSpouse2() As Double
        Get
            ChildSpouse2 = m_ChildSpouse2
        End Get
        Set(ByVal Value As Double)
            m_ChildSpouse2 = Value
        End Set
    End Property


    Public Property ChildSpouse3() As Double
        Get
            ChildSpouse3 = m_ChildSpouse3
        End Get
        Set(ByVal Value As Double)
            m_ChildSpouse3 = Value
        End Set
    End Property


    Public Property ChildSpouse4P() As Double
        Get
            ChildSpouse4P = m_ChildSpouse4P
        End Get
        Set(ByVal Value As Double)
            m_ChildSpouse4P = Value
        End Set
    End Property


    Public Property ClaimantsAWWBCode() As Integer
        Get
            ClaimantsAWWBCode = m_ClaimantsAWWBCode
        End Get
        Set(ByVal Value As Integer)
            m_ClaimantsAWWBCode = Value
        End Set
    End Property


    Public Property PayPeriodLockedInCalcCode() As Integer
        Get
            PayPeriodLockedInCalcCode = m_PayPeriodLockedInCalcCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodLockedInCalcCode = Value
        End Set
    End Property

    Public Property RuleTotalWeeks() As Double
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseTwoThirdsCode() As Integer
        Get
            UseTwoThirdsCode = m_UseTwoThirdsCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTwoThirdsCode = Value
        End Set
    End Property


    Public Property UseSAWWMinimumBCode() As Integer
        Get
            UseSAWWMinimumBCode = m_UseSAWWMinimumBCode
        End Get
        Set(ByVal Value As Integer)
            m_UseSAWWMinimumBCode = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        If Not Me.objJurisWorkWeek Is Nothing Then
            'UPGRADE_NOTE: Object Me.objJurisWorkWeek may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            Me.objJurisWorkWeek = Nothing
        End If


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

