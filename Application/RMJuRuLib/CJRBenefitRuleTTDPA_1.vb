Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleTTDPA_1
    Const sClassName As String = "CJRBenefitRuleTTDPA"
    Const sTableName As String = "WCP_RULE_TTD_PA"
    'local variable(s) not publicly exposed
    Const dWeekToMonthConvFactorDefault As Double = 4.34821

    Public objJurisWorkWeek As New CJRXJurisWorkWeek

    Dim m_sDateOfEventDTG As String 'jtodd22 not a Class Property

    'to support future interface
    Private m_DataHasChanged As Boolean
    Private m_DeletedFlag As Integer
    Private m_EffectiveDateDTG As String
    Private m_ErrorMaskJR As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_ErrorMaskSAWW As Integer 'jtodd22 not a jurisdictional rule parameter, for error handling
    Private m_JurisDefinedWorkWeek As Integer 'jtodd22 03/08/2005 not part of the rule, but used as part of rule in RMWCCalc.dll
    Private m_JurisRowID As Integer
    Private m_MaxCompRateMonthly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_MaxCompRateWeekly As Double 'jtodd22 not a jurisdictional rule parameter, calculated value
    Private m_NoCodeID As Integer
    Private m_PayPeriodCode As Integer
    Private m_PayPeriodLockedInCalcCode As Integer
    Private m_RuleTotalWeeks As Double
    Private m_TableRowID As Integer
    Private m_UseTwoThirdsCode As Integer
    Private m_Warning As String
    Private m_WeekToMonthConvFactor As Double
    Private m_YesCodeID As Integer

    Private m_PercentToInvokeMinimum As Double
    Private m_PrimeRateMaxAmount As Double
    Private m_PrimeRateMaxWeeks As Integer
    Private m_PrimeRate As Double

    Private m_PayConCurrentPPD As Integer
    Private m_AWWHighAmount As Double
    Private m_HighRate As Double
    Private m_HighMaxRateAmount As Double

    Private m_TotalAmount As Double

    Private m_ClaimantsAWW As Double 'not jurisdictional rule
    Private m_MinCompRate As Double 'not jurisdictional rule


    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Dim lReturn As Integer

        Try

            AssignData = 0
            ClearObject()

            m_YesCodeID = GetYesCodeID()

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_PrimeRate = objReader.GetDouble("PRIME_RATE")
                m_PrimeRateMaxWeeks = objReader.GetInt32("PR_RAT_MAX_WEEKS")
                m_PrimeRateMaxAmount = objReader.GetDouble("PR_RAT_MAX_BENFIT")
                m_PayConCurrentPPD = objReader.GetDouble("PAY_CONC_PPD_CODE")
                m_AWWHighAmount = objReader.GetDouble("AWW_HIGH_AMOUNT")
                m_HighRate = objReader.GetDouble("HIGH_RATE")
                m_HighMaxRateAmount = objReader.GetDouble("HIGH_RATE_MAX_AMNT")
                m_TotalAmount = objReader.GetDouble("TOTAL_AMT")
                m_RuleTotalWeeks = objReader.GetDouble("TOTAL_WEEKS")
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
            End If

            SafeCloseRecordset(objReader)

            m_DataHasChanged = False
            m_NoCodeID = modFunctions.GetNoCodeID
            m_YesCodeID = modFunctions.GetYesCodeID

            lReturn = Me.objJurisWorkWeek.LoadData(m_JurisRowID, m_EffectiveDateDTG)
            If lReturn = -1 Then
                m_JurisDefinedWorkWeek = Me.objJurisWorkWeek.JurisDefinedWorkWeek
                AssignData = -1
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            AssignData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function ClearObject() As Integer
        'jtodd22 support for future interface
        m_DataHasChanged = False
        m_DeletedFlag = 0
        m_EffectiveDateDTG = ""
        m_ErrorMaskJR = 0
        m_ErrorMaskSAWW = 0
        m_JurisDefinedWorkWeek = 0
        m_JurisRowID = 0
        m_MaxCompRateMonthly = 0
        m_MaxCompRateWeekly = 0
        m_NoCodeID = 0
        m_PayPeriodCode = 0
        m_PayPeriodLockedInCalcCode = 0
        m_RuleTotalWeeks = 0
        m_TableRowID = 0
        m_UseTwoThirdsCode = 0
        m_WeekToMonthConvFactor = dWeekToMonthConvFactorDefault
        m_YesCodeID = 0

        m_MinCompRate = 0
        m_PayConCurrentPPD = 0
        m_PrimeRate = 0
        m_PrimeRateMaxWeeks = 0
        m_PrimeRateMaxAmount = 0
        m_AWWHighAmount = 0
        m_HighRate = 0
        m_HighMaxRateAmount = 0
        m_TotalAmount = 0


    End Function
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        '                  '123456789012345678
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED, ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD, UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", PRIME_RATE"
        sSQL = sSQL & ", PR_RAT_MAX_WEEKS"
        sSQL = sSQL & ", PR_RAT_MAX_BENFIT"
        sSQL = sSQL & ", PAY_CONC_PPD_CODE"
        sSQL = sSQL & ", AWW_HIGH_AMOUNT"
        sSQL = sSQL & ", HIGH_RATE"
        sSQL = sSQL & ", HIGH_RATE_MAX_AMNT"
        sSQL = sSQL & ", TOTAL_WEEKS"
        sSQL = sSQL & ", TOTAL_AMT"

        GetSQLFieldList = sSQL


    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : DeleteRecord
    ' DateTime  : 4/18/2006 07:43
    ' Author    : jtodd22
    ' Purpose   :
    ' Note      : lReturnCode is a number defining why the record was not deleted
    '---------------------------------------------------------------------------------------
    '
    Public Function DeleteRecord(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sEffectiveDateDBFormat As String, ByRef lReturnCode As Integer) As Integer
        Const sFunctionName As String = "DeleteRecord"

        Try

            DeleteRecord = modFunctions.DeleteRecordTemporary(objUser, lJurisRowID, sEffectiveDateDBFormat, lReturnCode, sTableName)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            DeleteRecord = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataByTableRowID"
        Dim sSQL As String
        Try
            LoadDataByTableRowID = 0

            If Trim(sTableName & "") = "" Then Exit Function

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataByTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String, ByRef dAWW As Double) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim lReturn As Integer
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadDataByEventDate = 0

            If Trim(sTableName & "") = "" Then Exit Function

            m_ClaimantsAWW = dAWW
            m_sDateOfEventDTG = sDateOfEventDTG

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            lReturn = AssignData(objUser, sSQL)
            If lReturn <> -1 Then Exit Function


            LoadDataByEventDate = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sSQL As String
        Try

            SaveData = 0



            sSQL = ""
            sSQL = sSQL & "SELECT "
            sSQL = sSQL & " * FROM " & sTableName
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = g_UserLoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("PRIME_RATE").Value = m_PrimeRate
                objWriter.Fields("PR_RAT_MAX_WEEKS").Value = m_PrimeRateMaxWeeks
                objWriter.Fields("PR_RAT_MAX_BENFIT").Value = m_PrimeRateMaxAmount

                objWriter.Fields("PAY_CONC_PPD_CODE").Value = m_PayConCurrentPPD
                objWriter.Fields("AWW_HIGH_AMOUNT").Value = m_AWWHighAmount
                objWriter.Fields("HIGH_RATE").Value = m_HighRate
                objWriter.Fields("HIGH_RATE_MAX_AMNT").Value = m_HighMaxRateAmount
                objWriter.Fields("TOTAL_AMT").Value = m_TotalAmount
                objWriter.Fields("TOTAL_WEEKS").Value = m_RuleTotalWeeks
            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("TABLE_ROW_ID", m_TableRowID)
                objWriter.Fields.Add("ADDED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", g_UserLoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("PRIME_RATE", m_PrimeRate)
                objWriter.Fields.Add("PR_RAT_MAX_WEEKS", m_PrimeRateMaxWeeks)
                objWriter.Fields.Add("PR_RAT_MAX_BENFIT", m_PrimeRateMaxAmount)
                objWriter.Fields.Add("PAY_CONC_PPD_CODE", m_PayConCurrentPPD)
                objWriter.Fields.Add("AWW_HIGH_AMOUNT", m_AWWHighAmount)
                objWriter.Fields.Add("HIGH_RATE", m_HighRate)
                objWriter.Fields.Add("HIGH_RATE_MAX_AMNT", m_HighMaxRateAmount)
                objWriter.Fields.Add("TOTAL_AMT", m_TotalAmount)
                objWriter.Fields.Add("TOTAL_WEEKS", m_RuleTotalWeeks)
            End If
            objWriter.Execute()
            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SaveData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".SaveData|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function ValidateData() As Integer

        ValidateData = 0
        m_Warning = ""
        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If


    End Function

    Public Property DeletedFlag() As Integer
        Get
            DeletedFlag = m_DeletedFlag
        End Get
        Set(ByVal Value As Integer)
            m_DeletedFlag = Value
        End Set
    End Property

    Public Property PrimeRateMaxAmount() As Double
        Get
            PrimeRateMaxAmount = m_PrimeRateMaxAmount
        End Get
        Set(ByVal Value As Double)
            m_PrimeRateMaxAmount = Value
        End Set
    End Property

    Public Property PrimeRateMaxWeeks() As Integer
        Get
            PrimeRateMaxWeeks = m_PrimeRateMaxWeeks
        End Get
        Set(ByVal Value As Integer)
            m_PrimeRateMaxWeeks = Value
        End Set
    End Property

    Public Property PrimeRate() As Double
        Get
            PrimeRate = m_PrimeRate
        End Get
        Set(ByVal Value As Double)
            m_PrimeRate = Value
        End Set
    End Property

    Public Property MaxCompRateMonthly() As Double
        Get
            MaxCompRateMonthly = m_MaxCompRateMonthly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateMonthly = Value
        End Set
    End Property

    Public Property MaxCompRateWeekly() As Double
        Get
            MaxCompRateWeekly = m_MaxCompRateWeekly
        End Get
        Set(ByVal Value As Double)
            m_MaxCompRateWeekly = Value
        End Set
    End Property

    Public Property MinCompRate() As Double
        Get
            MinCompRate = m_MinCompRate
        End Get
        Set(ByVal Value As Double)
            m_MinCompRate = Value
        End Set
    End Property

    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property

    Public Property JurisDefinedWorkWeek() As Integer
        Get
            JurisDefinedWorkWeek = m_JurisDefinedWorkWeek
        End Get
        Set(ByVal Value As Integer)
            m_JurisDefinedWorkWeek = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property

    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property ErrorMaskJR() As Integer
        Get
            ErrorMaskJR = m_ErrorMaskJR
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskJR = Value
        End Set
    End Property

    Public Property ErrorMaskSAWW() As Integer
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property

    Public Property TotalAmount() As Double
        Get
            TotalAmount = m_TotalAmount
        End Get
        Set(ByVal Value As Double)
            m_TotalAmount = Value
        End Set
    End Property

    Public Property PayConCurrentPPD() As Integer
        Get
            PayConCurrentPPD = m_PayConCurrentPPD
        End Get
        Set(ByVal Value As Integer)
            m_PayConCurrentPPD = Value
        End Set
    End Property

    Public Property AWWHighAmount() As Double
        Get
            AWWHighAmount = m_AWWHighAmount
        End Get
        Set(ByVal Value As Double)
            m_AWWHighAmount = Value
        End Set
    End Property

    Public Property HighRate() As Double
        Get
            HighRate = m_HighRate
        End Get
        Set(ByVal Value As Double)
            m_HighRate = Value
        End Set
    End Property

    Public Property HighMaxRateAmount() As Double
        Get
            HighMaxRateAmount = m_HighMaxRateAmount
        End Get
        Set(ByVal Value As Double)
            m_HighMaxRateAmount = Value
        End Set
    End Property


    Public Property PayPeriodLockedInCalcCode() As Integer
        Get
            PayPeriodLockedInCalcCode = m_PayPeriodLockedInCalcCode
        End Get
        Set(ByVal Value As Integer)
            m_PayPeriodLockedInCalcCode = Value
        End Set
    End Property

    Public Property RuleTotalWeeks() As Double
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseTwoThirdsCode() As Integer
        Get
            UseTwoThirdsCode = m_UseTwoThirdsCode
        End Get
        Set(ByVal Value As Integer)
            m_UseTwoThirdsCode = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property

    Private Function GetMinCompRate(ByRef objUser As Riskmaster.Security.UserLogin) As Double
        Try

            GetMinCompRate = 0
            m_MinCompRate = 0
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".GetMinCompRate|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Private Function GetJurisDefinedWorkWeek(ByRef lJurisRowID As Integer, ByRef sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "GetJurisDefinedWorkWeek"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            GetJurisDefinedWorkWeek = 0
            m_JurisDefinedWorkWeek = 0

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM WCP_BEN_SWCH"
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = ""
            sSQL = sSQL & " SELECT JURIS_WORK_WEEK, EFFECTIVE_DATE"
            sSQL = sSQL & " FROM WCP_BEN_SWCH"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_JurisDefinedWorkWeek = objReader.GetInt32("JURIS_WORK_WEEK")
            End If
            If m_JurisDefinedWorkWeek = 0 Then m_JurisDefinedWorkWeek = 7
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetJurisDefinedWorkWeek = Err.Number
            SafeCloseRecordset(objReader)
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        If Not Me.objJurisWorkWeek Is Nothing Then
            'UPGRADE_NOTE: Object Me.objJurisWorkWeek may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            Me.objJurisWorkWeek = Nothing
        End If



    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

