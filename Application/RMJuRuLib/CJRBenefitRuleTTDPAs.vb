Option Strict Off
Option Explicit On
Public Class CJRBenefitRuleTTDPAs
    Implements System.Collections.IEnumerable
    Private Const sClassName As String = "CJRBenefitRuleTTDPAs"
    Private Const sTableName As String = "WCP_RULE_TTD_PA"

    'Class properties, local copy
    Private mCol As Collection
    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        '                  '123456789012345678
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED, ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD, UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID"
        sSQL = sSQL & ", DELETED_FLAG"
        sSQL = sSQL & ", EFFECTIVE_DATE"
        sSQL = sSQL & ", PRIME_RATE"
        sSQL = sSQL & ", PR_RAT_MAX_WEEKS"
        sSQL = sSQL & ", PR_RAT_MAX_BENFIT"
        sSQL = sSQL & ", PAY_CONC_PPD_CODE"
        sSQL = sSQL & ", AWW_HIGH_AMOUNT"
        sSQL = sSQL & ", HIGH_RATE"
        sSQL = sSQL & ", HIGH_RATE_MAX_AMNT"
        sSQL = sSQL & ", TOTAL_WEEKS"
        sSQL = sSQL & ", TOTAL_AMT"

        GetSQLFieldList = sSQL


    End Function
    Public Function LoadData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef lJurisRowID As Integer, ByRef sBeginDateDTG As String) As Integer
        Const sFunctionName As String = "LoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim objTTDRule As CJRBenefitRuleTTDPA
        Try

            LoadData = 0


            sSQL = ""
            sSQL = sSQL & GetSQLFieldList()
            sSQL = sSQL & " FROM " & sTableName
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                objTTDRule = New CJRBenefitRuleTTDPA
                With objTTDRule
                    .AWWHighAmount = objReader.GetDouble("AWW_HIGH_AMOUNT")
                    .DeletedFlag = objReader.GetInt32("DELETED_FLAG")
                    .EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                    .HighRate = objReader.GetDouble("HIGH_RATE")
                    .HighMaxRateAmount = objReader.GetDouble("HIGH_RATE_MAX_AMNT")
                    .JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                    .PayConCurrentPPD = objReader.GetDouble("PAY_CONC_PPD_CODE")
                    .PrimeRate = objReader.GetDouble("PRIME_RATE")
                    .PrimeRateMaxWeeks = objReader.GetInt32("PR_RAT_MAX_WEEKS")
                    .PrimeRateMaxAmount = objReader.GetDouble("PR_RAT_MAX_BENFIT")
                    .TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                    .TotalAmount = objReader.GetDouble("TOTAL_AMT")
                    .RuleTotalWeeks = objReader.GetDouble("TOTAL_WEEKS")
                    Add(.AWWHighAmount, .DeletedFlag, .EffectiveDateDTG, .HighRate, .HighMaxRateAmount, .JurisRowID, .PayConCurrentPPD, .PrimeRate, .PrimeRateMaxAmount, .PrimeRateMaxWeeks, .TableRowID, .TotalAmount, .RuleTotalWeeks, "k" & .TableRowID)
                End With
                'UPGRADE_NOTE: Object objTTDRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objTTDRule = Nothing

            End While

            LoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number
            SafeCloseRecordset(objReader)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Private Function Add(ByRef AWWHighAmount As Double, ByRef DeletedFlag As Integer, ByRef EffectiveDateDTG As String, ByRef HighMaxRateAmount As Double, ByRef HighRate As Double, ByRef JurisRowID As Integer, ByRef PayConCurrentPPD As Integer, ByRef PrimeRate As Double, ByRef PrimeRateMaxAmount As Double, ByRef PrimeRateMaxWeeks As Double, ByRef TableRowID As Integer, ByRef TotalAmount As Double, ByRef RuleTotalWeeks As Integer, ByRef sKey As String) As CJRBenefitRuleTTDPA

        Try
            'create a new object
            Dim objNewMember As CJRBenefitRuleTTDPA
            objNewMember = New CJRBenefitRuleTTDPA

            'set the properties passed into the method
            With objNewMember
                .AWWHighAmount = AWWHighAmount
                .EffectiveDateDTG = EffectiveDateDTG
                .HighMaxRateAmount = HighMaxRateAmount
                .HighRate = HighRate
                .JurisRowID = JurisRowID
                .PayConCurrentPPD = PayConCurrentPPD
                .PrimeRate = PrimeRate
                .PrimeRateMaxAmount = PrimeRateMaxAmount
                .PrimeRateMaxWeeks = PrimeRateMaxWeeks
                .TableRowID = TableRowID
                .TotalAmount = TotalAmount
                .RuleTotalWeeks = RuleTotalWeeks
            End With
            mCol.Add(objNewMember, sKey)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function

    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CJRBenefitRuleTTDPA
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property
    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function
    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub
    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub
    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

