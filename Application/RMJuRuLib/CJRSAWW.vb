Option Strict Off
Option Explicit On
Public Class CJRSAWW
    Const sClassName As String = "CJRSAWW"
    Const sTableName As String = "WCP_SAWW_LKUP"

    'Class properties, local copy
    Private m_TableRowID As Integer
    Private m_JurisRowID As Integer
    Private m_EffectiveDateDTG As String
    Private m_SAWWAmount As Decimal
    Private m_PercentAnnualChange As Double
    Private m_PercentMax As Double
    Private m_MaxAmountAsPublished As Double
    Private m_PercentMin As Double
    Private m_MinAmountAsPublished As Double
    Private m_DataHasChanged As Boolean
    Private m_Warning As String

    Private Function AssignData(ByRef objUser As Riskmaster.Security.UserLogin, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim objReader As DbReader
        Try

            AssignData = 0



            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_TableRowID = objReader.GetInt32("TABLE_ROW_ID")
                m_JurisRowID = objReader.GetInt32("JURIS_ROW_ID")
                m_EffectiveDateDTG = objReader.GetString("EFFECTIVE_DATE")
                'm_DeletedFlag = objReader.GetInt32( "DELETED_FLAG"))
                m_SAWWAmount = objReader.GetDouble("SAWW_AMOUNT")
                m_PercentAnnualChange = objReader.GetDouble("PERCENT_ANN_CHGE")
                m_PercentMax = objReader.GetDouble("MAX_PERCENTAGE")
                m_MaxAmountAsPublished = objReader.GetDouble("MAX_AMT_PUBLISHED")
                m_PercentMin = objReader.GetDouble("MIN_PERCENTAGE")
                m_MinAmountAsPublished = objReader.GetDouble("MIN_AMT_PUBLISHED")
            End If

            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeCloseRecordset(objReader)

            AssignData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function
    Public Property DataHasChanged() As Boolean
        Get
            DataHasChanged = m_DataHasChanged
        End Get
        Set(ByVal Value As Boolean)
            m_DataHasChanged = Value
        End Set
    End Property

    Public Property MinAmountAsPublished() As Double
        Get
            MinAmountAsPublished = m_MinAmountAsPublished
        End Get
        Set(ByVal Value As Double)
            m_MinAmountAsPublished = Value
        End Set
    End Property

    Public Property PercentMin() As Double
        Get
            PercentMin = m_PercentMin
        End Get
        Set(ByVal Value As Double)
            m_PercentMin = Value
        End Set
    End Property

    Public Property MaxAmountAsPublished() As Double
        Get
            MaxAmountAsPublished = m_MaxAmountAsPublished
        End Get
        Set(ByVal Value As Double)
            m_MaxAmountAsPublished = Value
        End Set
    End Property

    Public Property PercentMax() As Double
        Get
            PercentMax = m_PercentMax
        End Get
        Set(ByVal Value As Double)
            m_PercentMax = Value
        End Set
    End Property

    Public Property PercentAnnualChange() As Double
        Get
            PercentAnnualChange = m_PercentAnnualChange
        End Get
        Set(ByVal Value As Double)
            m_PercentAnnualChange = Value
        End Set
    End Property
    Public Property SAWWAmount() As Decimal
        Get
            SAWWAmount = m_SAWWAmount
        End Get
        Set(ByVal Value As Decimal)
            m_SAWWAmount = Value
        End Set
    End Property
    Public Property EffectiveDateDTG() As String
        Get
            EffectiveDateDTG = m_EffectiveDateDTG
        End Get
        Set(ByVal Value As String)
            m_EffectiveDateDTG = Value
        End Set
    End Property
    Public Property JurisRowID() As Integer
        Get
            JurisRowID = m_JurisRowID
        End Get
        Set(ByVal Value As Integer)
            m_JurisRowID = Value
        End Set
    End Property
    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property Warning() As String
        Get

            Warning = m_Warning

        End Get
        Set(ByVal Value As String)

            m_Warning = Value

        End Set
    End Property
    Public Function LoadDataByEventDate(ByRef objUser As Riskmaster.Security.UserLogin, ByVal lJurisRowID As Integer, ByVal sDateOfEventDTG As String) As Integer
        Const sFunctionName As String = "LoadDataByEventDate"
        Dim sSQL As String
        Dim sSQL2 As String
        Try

            LoadDataByEventDate = 0
            ClearObject()

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(EFFECTIVE_DATE)"
            sSQL2 = sSQL2 & " FROM " & sTableName
            sSQL2 = sSQL2 & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL2 = sSQL2 & " AND EFFECTIVE_DATE <= '" & sDateOfEventDTG & "'"

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM WCP_SAWW_LKUP"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & lJurisRowID
            sSQL = sSQL & " AND EFFECTIVE_DATE = (" & sSQL2 & ")"

            LoadDataByEventDate = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByEventDate = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadDataByTableRowID(ByRef objUser As Riskmaster.Security.UserLogin, ByVal lTableRowID As Integer) As Integer
        Const sFunctionName As String = "LoadDataByTableRowID"
        Dim sSQL As String
        Try

            LoadDataByTableRowID = 0
            ClearObject()

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM WCP_SAWW_LKUP"
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & lTableRowID

            LoadDataByTableRowID = AssignData(objUser, sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadDataByTableRowID = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".LoadDataByTableRowID|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
    Private Function ClearObject() As Object



    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : SaveData
    ' DateTime  : 07/03/2004 15:26
    ' Author    : jlt--Jim Todd
    ' Purpose   :
    ' Notes     : Do not pass the TABLE_ROW_ID as a parameter, it is part of the object

    '---------------------------------------------------------------------------------------
    '
    Public Function SaveData(ByRef objUser As Riskmaster.Security.UserLogin) As Integer
        Const sFunctionName As String = "SaveData"
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim lTest As Integer
        Dim sSQL As String
        Try

            SaveData = 0

            'If ModMain.DBConnect(objUser, sClassName) <> -1 Then
            '    Err.Raise(70001, sClassName & "." & sFunctionName, "Failed to connect to database.")
            '    Exit Function
            'End If

            If m_TableRowID < 1 Then m_TableRowID = 0
            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " *"
            sSQL = sSQL & " FROM WCP_SAWW_LKUP"
            sSQL = sSQL & " WHERE TABLE_ROW_ID = " & m_TableRowID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)


            If (objReader.Read()) Then

                objWriter = DbFactory.GetDbWriter(objReader, True)
                lTest = lGetNextUID(sTableName)

                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = System.DateTime.Now().ToString("yyyyMMddHHmmss")
                objWriter.Fields("UPDATED_BY_USER").Value = objUser.LoginName
                objWriter.Fields("EFFECTIVE_DATE").Value = m_EffectiveDateDTG
                objWriter.Fields("JURIS_ROW_ID").Value = m_JurisRowID
                objWriter.Fields("SAWW_AMOUNT").Value = System.Math.Round(m_SAWWAmount, 2)
                objWriter.Fields("PERCENT_ANN_CHGE").Value = System.Math.Round(m_PercentAnnualChange, 2)
                objWriter.Fields("MAX_PERCENTAGE").Value = m_PercentMax
                objWriter.Fields("MAX_AMT_PUBLISHED").Value = System.Math.Round(m_MaxAmountAsPublished, 2)
                objWriter.Fields("MIN_PERCENTAGE").Value = m_PercentMin
                objWriter.Fields("MIN_AMT_PUBLISHED").Value = System.Math.Round(m_MinAmountAsPublished, 2)
                objWriter.Execute()
            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                lTest = lGetNextUID(sTableName)
                objWriter.Fields.Add("TABLE_ROW_ID", lTest)
                objWriter.Fields.Add("ADDED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", System.DateTime.Now().ToString("yyyyMMddHHmmss"))
                objWriter.Fields.Add("UPDATED_BY_USER", objUser.LoginName)
                objWriter.Fields.Add("EFFECTIVE_DATE", m_EffectiveDateDTG)
                objWriter.Fields.Add("JURIS_ROW_ID", m_JurisRowID)
                objWriter.Fields.Add("SAWW_AMOUNT", System.Math.Round(m_SAWWAmount, 2))
                objWriter.Fields.Add("PERCENT_ANN_CHGE", System.Math.Round(m_PercentAnnualChange, 2))
                objWriter.Fields.Add("MAX_PERCENTAGE", m_PercentMax)
                objWriter.Fields.Add("MAX_AMT_PUBLISHED", System.Math.Round(m_MaxAmountAsPublished, 2))
                objWriter.Fields.Add("MIN_PERCENTAGE", m_PercentMin)
                objWriter.Fields.Add("MIN_AMT_PUBLISHED", System.Math.Round(m_MinAmountAsPublished, 2))
                objWriter.Execute()
            End If

            SaveData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            SafeCloseRecordset(objReader)

            SaveData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            If InStr(1, UCase(g_sErrDescription), UCase("UNIQUE CONSTRAINT")) > 0 Then
                LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                SaveData = 89999
                Exit Function
            End If
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Private Function GetSQLFieldList() As String
        Dim sSQL As String
        GetSQLFieldList = ""
        sSQL = ""
        sSQL = sSQL & "SELECT"
        sSQL = sSQL & " TABLE_ROW_ID"
        sSQL = sSQL & ", DTTM_RCD_ADDED,ADDED_BY_USER"
        sSQL = sSQL & ", DTTM_RCD_LAST_UPD,UPDATED_BY_USER"
        sSQL = sSQL & ", JURIS_ROW_ID, EFFECTIVE_DATE, DELETED_FLAG"
        sSQL = sSQL & ", SAWW_AMOUNT"
        sSQL = sSQL & ", PERCENT_ANN_CHGE"
        sSQL = sSQL & ", MAX_PERCENTAGE, MAX_AMT_PUBLISHED, MIN_PERCENTAGE, MIN_AMT_PUBLISHED"

        GetSQLFieldList = sSQL


    End Function
    Public Function Validate() As Integer

        Validate = 0

        m_Warning = ""

        If Len(m_EffectiveDateDTG) <> 8 Then
            m_Warning = m_Warning & "A valid Effective Date is required." & vbCrLf
        End If
        If m_SAWWAmount < 1 Then
            m_Warning = m_Warning & "An SAWW Amount is required." & vbCrLf
        End If
        If m_MaxAmountAsPublished > 0 And m_PercentMax > 0 Then
            m_Warning = m_Warning & "For Maximum Rates, only one of the two choices can have a value greater than 0 (zero)." & vbCrLf
        End If
        If m_MinAmountAsPublished > 0 And m_PercentMin > 0 Then
            m_Warning = m_Warning & "For Minimum Rates, only one of the two choices can have a value greater than 0 (zero)." & vbCrLf
        End If



    End Function
End Class

