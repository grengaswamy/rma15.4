﻿
using System;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Collections;

namespace Riskmaster.Application.ISOClaimSubHistory
{

    /**************************************************************
         * $File		: ISOClaimSubHistory.cs
         * $Revision	: 1.0.0.0
         * $Date		: 15/02/2010
         * $Author		: Rahul Aggarwal
         * $Comment		: Has functions to display the Submission History for a ISO Claim (Safeway)
    **************************************************************/

    /// <summary>
    /// ISOClaimSubHistory class
	/// </summary>
	public class ISOClaimSubHistory
	{
		#region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
        //dvatsa
        private int m_iClientId = 0;
		#endregion

		#region Constructor
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
        /// 
        public ISOClaimSubHistory(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)//dvatsa
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;//dvatsa-cloud
		}
		#endregion

		#region Public Methods

		/// <summary>
        /// Gets the ISO Claim Submission History.
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml structure document</param>
		/// <returns>Xml Document with Data</returns>
        public XmlDocument GetISOClaimSubHistory(XmlDocument p_objXmlDoc)
		{
			XmlElement objElm = null;
			XmlElement objNewElm = null;
			XmlElement objRowNode = null;
			LocalCache objCache = null;

            string sSQL = string.Empty;
            int iClaimId = 0;
            string sCode = string.Empty;
            string sDesc = string.Empty;
			try
			{
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword,m_iClientId);//dvatsa-cloud
                m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;

                objCache = new LocalCache(m_sConnectionString,m_iClientId);

                //Claim Submission History
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ISOClaimSubHistory");
				iClaimId=Conversion.ConvertStrToInteger(objElm.SelectSingleNode("//ClaimId").InnerText);

                sSQL = "SELECT * FROM ISO_CLAIM_SUB_HIST WHERE CLAIM_ID = " + iClaimId +
                        " ORDER BY ISO_CLAIM_SUB_ROW_ID DESC " ;
                
                using (DbReader objReader =DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {

                    while (objReader.Read())
                    {
                        objRowNode = objElm.OwnerDocument.CreateElement("Row");

                        // Node for Date
                        objNewElm = objRowNode.OwnerDocument.CreateElement("Date");
                        objNewElm.InnerText = Conversion.GetDBDateFormat(objReader.GetString("DATE_SUB_TYPE_CHGD"), "d");
                        objRowNode.AppendChild(objNewElm);
                        
                        objCache.GetCodeInfo(objReader.GetInt("SUB_TYPE"),ref sCode,ref sDesc);

                        // Node for Type
                        objNewElm = objRowNode.OwnerDocument.CreateElement("Type");
                        objNewElm.InnerText = sCode;
                        objRowNode.AppendChild(objNewElm);

                        // Node for Description
                        objNewElm = objRowNode.OwnerDocument.CreateElement("Description");
                        objNewElm.InnerText = sDesc;
                        objRowNode.AppendChild(objNewElm);

                        objElm.AppendChild(objRowNode);
                    }

                }

                //Submission Type
                objRowNode = objElm.OwnerDocument.CreateElement("SubType");
                sSQL = "SELECT * FROM CLAIMANT WHERE INDSYS_EXTRT_DATE IS NOT NULL AND CLAIM_ID = " + iClaimId;
                using (DbReader objReaderClaimant = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    if (objReaderClaimant.Read())
                        objRowNode.SetAttribute("value", "R");
                    else
                        objRowNode.SetAttribute("value", "I");
                }
                objElm.AppendChild(objRowNode);

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ISOClaimSubHistory/SubmitToISOFlag");
                
                //Value of Submit To ISO Checkbox
                sSQL = "SELECT SUBMIT_TO_ISO_FLAG FROM CLAIM WHERE CLAIM_ID = " + iClaimId;
                using (DbReader objReaderISOClaim = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    if (objReaderISOClaim.Read())
                    {
                        objElm.SetAttribute("value", objReaderISOClaim.GetBoolean("SUBMIT_TO_ISO_FLAG").ToString());
                    }
                }
            }
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("ISOClaimSubHistory.GetISOClaimSubHistory.Error",m_iClientId), p_objEx);//dvatsa-cloud
			}
			finally
			{
				objElm = null;
				objNewElm = null;
				objRowNode = null;
                if (objCache != null)
                    objCache.Dispose();
                if (m_objDataModelFactory != null)
                    m_objDataModelFactory.Dispose();
				
			}

			return p_objXmlDoc;
		}


        /// <summary>
        /// Save ISO Claim Submission Settings
        /// </summary>
        /// <param name="p_objXmlDoc"></param>
        /// <returns></returns>
        public XmlDocument SaveISOClaimSubSettings(XmlDocument p_objXmlDoc)
        {
            XmlElement objElm = null;
            DbConnection objConn = null;

            string sSQL = string.Empty;
            int iClaimId = 0;
            int iSubmitToISOFlag= 0;
            string sSubType = string.Empty;
            int iCount = 0;

            try
            {
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword,m_iClientId);//dvatsa-cloud
                m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;

                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ISOClaimSubHistory");
                iClaimId = Conversion.ConvertStrToInteger(objElm.SelectSingleNode("//ClaimId").InnerText);

                if (objElm.SelectSingleNode("//SubmitToISOFlag").InnerText == "True")
                {
                    iSubmitToISOFlag= -1;
                }
                    
                //Mits id: 34975 - Design change - Govind - Start

                sSQL = "SELECT COUNT(*) FROM CLAIMANT WHERE CLAIM_ID = " + iClaimId;
                iCount = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, sSQL), m_iClientId);
                if (iCount > 0)
                {

                //To Update Submit To ISO Flag
                sSQL = "UPDATE CLAIM SET SUBMIT_TO_ISO_FLAG = " + iSubmitToISOFlag; 
                sSQL = sSQL +  " WHERE CLAIM_ID = " + iClaimId ;
                objConn.ExecuteNonQuery(sSQL);

                //To retrive old status for Submission Type
                sSQL = "SELECT * FROM CLAIMANT WHERE INDSYS_EXTRT_DATE IS NOT NULL AND CLAIM_ID = " + iClaimId;
                using (DbReader objReaderClaimant = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    if (objReaderClaimant.Read())
                        sSubType = "R";
                    else
                        sSubType = "I";
                }

                //To Update Submission Type
                if (objElm.SelectSingleNode("//SubType").InnerText == "I" && sSubType != "I") 
                {
                    sSQL = "UPDATE CLAIMANT SET INDSYS_EXTRT_DATE = NULL " +
                           " WHERE CLAIM_ID = " + iClaimId;     //Reset as Initial submit
                }
                else if (objElm.SelectSingleNode("//SubType").InnerText == "R" && sSubType != "R") 
                {
                    sSQL = "UPDATE CLAIMANT SET INDSYS_EXTRT_DATE = '" + Conversion.ToDbDateTime(DateTime.Now.AddDays(-1)) + "'" +
                         " WHERE CLAIM_ID = " + iClaimId;       //Set as Replacement
                }
                objConn.ExecuteNonQuery(sSQL);
                }
                else
                {

                    XmlElement ClaimantEle = p_objXmlDoc.CreateElement("Claimant");
                    ClaimantEle.InnerText = "Required Claimant";
                    p_objXmlDoc.DocumentElement.AppendChild(ClaimantEle);


                }

                objConn.Close();
                //Mits id: 34975 - Design change - Govind - End
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ISOClaimSubHistory.SaveISOClaimSubSettings.Error",m_iClientId), p_objEx);//dvatsa-cloud
            }
            finally
            {
                objElm = null;
                if (m_objDataModelFactory != null)
                    m_objDataModelFactory.Dispose();
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }

            return p_objXmlDoc;
        }

		#endregion
	}
}
