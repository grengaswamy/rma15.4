using System;
using System.Xml ;
using Riskmaster.Common ;
using Riskmaster.DataModel ;
using Riskmaster.ExceptionTypes ;

namespace Riskmaster.Application.ScriptEditor
{
	/**************************************************************
	 * $File		: Common.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 12/20/2005
	 * $Author		: Vaibhav Kaushik
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/
	public class Common
	{
		public Common()
		{			
		}


		#region Variable Declaration 
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private static string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private static string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private static string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private static string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private static DataModelFactory m_objDataModelFactory = null ;

		private static bool m_bIsInitialized = false ;

        private static int m_iClientId = 0;//psharma206
      
         
		#endregion 

		#region Variable Properties 

		internal static string UserName 
		{
			get
			{
				return( m_sUserName ) ;
			}
			set
			{
				m_sUserName = value ;
			}
		}


		internal static string Password 
		{
			get
			{
				return( m_sPassword ) ;
			}
			set
			{
				m_sPassword = value ;
			}
		}


		internal static string DsnName 
		{
			get
			{
				return( m_sDsnName ) ;
			}
			set
			{
				m_sDsnName = value ;
			}
		}


		internal static string ConnectionString 
		{
			get
			{
				return( m_sConnectionString ) ;
			}
			set
			{
				m_sConnectionString = value ;
			}
		}


		internal static DataModelFactory DataModelFactory 
		{
			get
			{
				return( m_objDataModelFactory ) ;
			}
			set
			{
				m_objDataModelFactory = value ;
			}
		}


		internal static bool IsInitialized 
		{
			get
			{
				return( m_bIsInitialized ) ;
			}	
			set
			{
				m_bIsInitialized = value ;
			}
		}

		
		#endregion 

		#region Initialization 

        internal static void Initialized(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)//psharma206
		{
			if( !IsInitialized )
            {
                m_iClientId = p_iClientId;//psharma206
				UserName = p_sUserName;
				Password = p_sPassword;
				DsnName = p_sDsnName;
				m_objDataModelFactory = new DataModel.DataModelFactory( m_sDsnName , m_sUserName , m_sPassword,m_iClientId);	//psharma206
				ConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;				
				m_bIsInitialized = true ;
			}
		}
		#endregion 

		#region Common XML functions
		/// <summary>
		/// Initialize the XML document
		/// </summary>
		/// <param name="objXmlDocument">XML document</param>
		/// <param name="p_objRootNode">Root Node</param>
		/// <param name="p_objMessagesNode">Messages Node</param>
		/// <param name="p_sRootNodeName">Root Node Name</param>
		internal static void StartDocument( ref XmlDocument objXmlDocument , ref XmlElement p_objRootNode, string p_sRootNodeName )
		{
			try
			{
				objXmlDocument = new XmlDocument();
				p_objRootNode = objXmlDocument.CreateElement( p_sRootNodeName );
				objXmlDocument.AppendChild( p_objRootNode );				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("TODO", m_iClientId) , p_objEx );				
			}
		}

		/// <summary>
		/// Create Element
		/// </summary>
		/// <param name="p_objParentNode">Parent Node</param>
		/// <param name="p_sNodeName">Node Name</param>		
		internal static void CreateElement( XmlElement p_objParentNode , string p_sNodeName )
		{
			XmlElement objChildNode = null ;
			try
			{
				objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
				p_objParentNode.AppendChild( objChildNode );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("TODO", m_iClientId), p_objEx);
			}
			finally
			{
				objChildNode = null ;
			}

		}	
	
		/// <summary>
		/// Create Element
		/// </summary>
		/// <param name="p_objParentNode">Parent Node</param>
		/// <param name="p_sNodeName">Child Node Name</param>
		/// <param name="p_objChildNode">Child Node</param>
		internal static void CreateElement( XmlElement p_objParentNode , string p_sNodeName , ref XmlElement p_objChildNode )
		{
			try
			{
				p_objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
				p_objParentNode.AppendChild( p_objChildNode );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("TODO", m_iClientId), p_objEx);
			}

		}
		
		/// <summary>
		/// Create Element and set inner text.
		/// </summary>
		/// <param name="p_objParentNode">Parent node</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <param name="p_sText">Text</param>		
		internal static void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string p_sText )
		{
			try
			{
				XmlElement objChildNode = null ;
				Common.CreateAndSetElement( p_objParentNode , p_sNodeName , p_sText , ref objChildNode );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("TODO", m_iClientId), p_objEx);
			}
		}
		/// <summary>
		/// Create Element and set inner text.
		/// </summary>
		/// <param name="p_objParentNode">Parent node</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <param name="p_sText">Text</param>
		/// <param name="p_objChildNode">Child Node</param>
		internal static void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string p_sText , ref XmlElement p_objChildNode )
		{
			try
			{
				CreateElement( p_objParentNode , p_sNodeName , ref p_objChildNode );
				p_objChildNode.InnerText = p_sText ;			
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("TODO", m_iClientId), p_objEx);
			}
		}
		

		
		#endregion 

		
	}
}
