﻿

using System;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Settings;
using System.Xml.Linq;
using System.Linq;

namespace Riskmaster.Application.MailMerge
{
    public class AutoMailMerge
    {
        private static Dictionary<string, Dictionary<string, string>> xFilters = null;

        private Dictionary<string, Dictionary<string, string>> Filters
        {
            get 
            {
                if (xFilters == null)
                {
                    xFilters = new Dictionary<string, Dictionary<string, string>>();
                }

                return xFilters;
            }
        }

        ///// <summary>
        ///// Info setting Object
        ///// </summary>
        private InfoSetting objInfoSet;

        /// <summary>
        /// Database type
        /// </summary>
        private int m_iDbType = 0;

        /// <summary>
        /// Info definition Object
        /// </summary>
        private InfoDefinition objInfoDef;

        /// <summary>
        /// Private variable for connection string
        /// </summary>
        string m_sConnStr = "";

        /// <summary>
        /// Dsn Id
        /// </summary>
        int m_iDsnId = 0;
        private int m_iClientId = 0; //sonali

        UserLogin objUserLogin = null;//vkumar258
        
        /// <summary>
        /// Default constructor with connection string
        /// </summary>
        /// <param name="p_sUser">User Name</param>
        /// <param name="p_sPwd">Password</param>
        /// <param name="p_sDsn">Dsn Name</param>
        public AutoMailMerge(string p_sUser, string p_sPwd, string p_sDsn,int p_iClientId)//sonali
        {
            objInfoSet = new InfoSetting();
            objInfoDef = new InfoDefinition();
            m_iClientId = p_iClientId;
            objUserLogin = new UserLogin(p_sUser, p_sPwd, p_sDsn,m_iClientId);
            m_sConnStr = objUserLogin.objRiskmasterDatabase.ConnectionString;
            m_iDbType = (int)objUserLogin.objRiskmasterDatabase.DbType;
            m_iDsnId = objUserLogin.objRiskmasterDatabase.DataSourceId;
          
            //objUserLogin = null;
        }

      

        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="p_sConStr">Connection String</param>
        public AutoMailMerge(string p_sConStr, int p_iClientId)
        {
            m_sConnStr = p_sConStr;
            m_iClientId = p_iClientId;
        }

        /// <summary>
        /// Gets the xml structure with data for the first step of the wizard
        /// </summary>
        /// <param name="p_objXmlDoc">Input xml document structure</param>
        /// <returns>Xml document with data and its structure for UI</returns>
        public XmlDocument GetMergeDefList(XmlDocument p_objXmlDoc)
        {
            //XmlDocument objDOM = null;
            XmlElement objDefElm = null;
            XmlElement objElm = null;
            XmlElement objAvlFltrs = null;
            XmlElement objSelFltrs = null;
            XmlElement objSelTempFltrs = null;
            XmlElement objFltrElm = null;
            XmlElement objTempFltrElm = null;
            XmlElement objNewElm = null;
            XmlElement objNewTempElm = null;
            XmlElement objTemplateElm = null;
            DataSet objDs = null;
            DataSet objDsGroups = null;
            int iDefId = 0;
            int iCatId = 0;
            int iTempId = 0;
            string sSecDsn = "";


            XmlElement objElementTemp = null;
            string sTempId = string.Empty;
            string sTempName = string.Empty;

            try
            {
                sSecDsn = SecurityDatabase.GetSecurityDsn(m_iClientId);//sonali
                objDefElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//DefId");
                if (objDefElm != null)
                    iDefId = Conversion.ConvertStrToInteger(objDefElm.InnerText);

                if (iDefId > 0)
                    LoadInfoSet(p_objXmlDoc, iDefId); 

                if (!(objInfoSet.Index > 0))
                    LoadInfoDef(p_objXmlDoc, -1);

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AutoMailMergeTemplate/level[@value='" + objInfoSet.Index + "']");

                if (objElm != null)
                {
                    objElementTemp = (XmlElement)p_objXmlDoc.SelectSingleNode("//AutoMailMergeTemplate");
                    objElementTemp.SetAttribute("selectedid", objElm.GetAttribute("value"));
                    iTempId = Conversion.ConvertStrToInteger(objElm.GetAttribute("value"));
                    objElm.SetAttribute("selected", "1");
                }

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AutoMailMergeSelFormTemplate");
                if (objElm != null)
                {
                    objElm.SetAttribute("selectedid", Conversion.ConvertObjToStr(objInfoSet.TemplateID));
                    objElm.SetAttribute("selectedname", Conversion.ConvertObjToStr(objInfoSet.TemplateName));
                }


                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AutoMailMergeName");
                if (objElm != null)
                    objElm.InnerText = objInfoSet.Name;

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AutoMailMergeDate");
                if (objElm != null)
                    if (objInfoSet.ProcessDate != null)
                    {
                        objElm.InnerText = FormatDate(objInfoSet.ProcessDate, false);
                    }
                    else
                    {
                        objElm.InnerText = string.Empty;
                    }

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AutoMailMergeName");
                if (objElm != null)
                    objElm.InnerText = objInfoSet.Name;

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//PrinterName");
                if (objElm != null)
                    objElm.InnerText = objInfoSet.PrinterName;

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//PaperBin");
                if (objElm != null)
                    objElm.InnerText = objInfoSet.PaperBin;

                objTemplateElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//TemplateTypeID");
                if (objTemplateElm != null)
                    iCatId = Conversion.ConvertStrToInteger(objTemplateElm.InnerText);
                if (iCatId > 0 || iDefId > 0)
                {
                    GetAvailableTemplateList(p_objXmlDoc);
                }

                objAvlFltrs = (XmlElement)p_objXmlDoc.SelectSingleNode("//Filters/AvlFilters");

                //Populate the available filters
                for (int iCtr = 0; iCtr <= objInfoDef.NumFilters - 1; iCtr++)
                {
                    objNewElm = p_objXmlDoc.CreateElement("level");
                    objNewElm.SetAttribute("templateid", objInfoDef.FilterDef[iCtr].TemplateId.ToString());
                    objNewElm.SetAttribute("id", objInfoDef.FilterDef[iCtr].ID.ToString());
                    objNewElm.SetAttribute("name", objInfoDef.FilterDef[iCtr].Name);
                    objNewElm.SetAttribute("SQLFill", objInfoDef.FilterDef[iCtr].SQLFill);
                    objNewElm.SetAttribute("FilterType", objInfoDef.FilterDef[iCtr].FilterType.ToString());
                    if (objInfoDef.FilterDef[iCtr].DefValue != null)
                        objNewElm.SetAttribute("DefValue", objInfoDef.FilterDef[iCtr].DefValue.ToString());
                    else
                        objNewElm.SetAttribute("DefValue", string.Empty);
                    //rupal:start, R8 Auto Diary Setup Enh
                    if (!string.IsNullOrEmpty(objInfoDef.FilterDef[iCtr].Database))
                        objNewElm.SetAttribute("Database", objInfoDef.FilterDef[iCtr].Database.ToString());
                    else
                        objNewElm.SetAttribute("Database", string.Empty);
                    //rupal:end
                    objAvlFltrs.AppendChild(objNewElm);
                }

                if (iDefId > 0)
                {
                    objSelFltrs = (XmlElement)p_objXmlDoc.SelectSingleNode("//Filters/SelectedFilters");
                    for (int iCtr = 0; iCtr <= objInfoSet.NumFilters - 1; iCtr++)
                    {
                        //Populate the available filters
                        //tanwar2 - corrected XPath - Jan 4th, 2012
                        //objFltrElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//Filters/AvlFilters//level[@id='" + objInfoSet.FilterSet[iCtr].Number + "']"); 
                        objFltrElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//Filters/AvlFilters/level[@id='" + objInfoSet.FilterSet[iCtr].Number + "']");
                        if (objFltrElm != null)
                        {
                            objNewElm = p_objXmlDoc.CreateElement("level");
                            objNewElm.SetAttribute("id", objFltrElm.GetAttribute("id"));
                            objNewElm.SetAttribute("templateid", objInfoSet.Index.ToString());
                            objNewElm.SetAttribute("name", objFltrElm.GetAttribute("name"));
                            objNewElm.SetAttribute("number", objInfoSet.FilterSet[iCtr].Number.ToString());
                            objNewElm.SetAttribute("data", objInfoSet.FilterSet[iCtr].Data.ToString());
                            objNewElm.SetAttribute("SQLFill", objFltrElm.GetAttribute("SQLFill"));
                            objNewElm.SetAttribute("FilterType", objFltrElm.GetAttribute("FilterType"));
                            objNewElm.SetAttribute("DefValue", objFltrElm.GetAttribute("DefValue"));
                            //rupal:start, R8 Auto Diary Setup Enh 
                            if (objFltrElm.HasAttribute("Database"))
                                objNewElm.SetAttribute("Database", objFltrElm.GetAttribute("Database"));
                            else
                                objNewElm.SetAttribute("Database", string.Empty);
                            //rupal:end
                            objSelFltrs.AppendChild(objNewElm);
                        }
                    }
                }

                if (iDefId > 0)
                {
                    objSelTempFltrs = (XmlElement)p_objXmlDoc.SelectSingleNode("//SelectedTemplateList");
                    for (int iCtr = 0; iCtr <= objInfoSet.TempNumFilters - 1; iCtr++)
                    {
                        objTempFltrElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AvailableTemplateList/Template[@TemplateId='" + objInfoSet.TemplateID  + "']");
                        if (objFltrElm != null)
                        {
                            objNewTempElm = p_objXmlDoc.CreateElement("Template");
                            objNewTempElm.SetAttribute("Category", objTempFltrElm.GetAttribute("Category"));
                            objNewTempElm.SetAttribute("TemplateDesc", objInfoSet.TemplateName);
                            objNewTempElm.SetAttribute("TemplateId", objInfoSet.TemplateID.ToString());
                            objNewTempElm.SetAttribute("TableQualify", objTempFltrElm.GetAttribute("TableQualify"));
                            objNewTempElm.SetAttribute("SendMailFlag", objTempFltrElm.GetAttribute("SendMailFlag"));
                            objNewTempElm.SetAttribute("MailRecipient", objTempFltrElm.GetAttribute("MailRecipient"));
                            objNewTempElm.SetAttribute("DisplayCategory", objTempFltrElm.GetAttribute("DisplayCategory"));
                            objNewTempElm.SetAttribute("SelectedFilterID", objInfoSet.TemplateFilterSet[iCtr].Number.ToString());
                            objNewTempElm.SetAttribute("data", objInfoSet.TemplateFilterSet[iCtr].Data.ToString());
                            objSelTempFltrs.AppendChild(objNewTempElm);
                        }
                    }
                }

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//PersonInvolvedValues");
                if (objElm != null)
                    objElm.InnerText = objInfoSet.PersonInvolvedValue;

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ExpertWitnessValues");
                if (objElm != null)
                    objElm.InnerText = objInfoSet.ExpertWitnessValue;

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ClaimantValues");
                if (objElm != null)
                    objElm.InnerText = objInfoSet.ClaimantValue;

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//DefendantValues");
                if (objElm != null)
                    objElm.InnerText = objInfoSet.DefendantValue;

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AdjusterValue");
                if (objElm != null)
                    objElm.InnerText = objInfoSet.AdjusterValue;

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//CaseManagerValue");
                if (objElm != null)
                    objElm.InnerText = objInfoSet.CasemanagerValue;

                //var ExtractFilterXMl = (XmlElement)p_objXmlDoc.SelectSingleNode("//Filters");
                //xElementFilters = XElement.Parse(ExtractFilterXMl.OuterXml);
                //asharma326 MITS 35669 
                XmlNodeList xmlNodeList = p_objXmlDoc.SelectNodes("//Filters/*/level");
                Dictionary<string, string> temp = new Dictionary<string, string>();
                //Bharani - MITS : 35879 - Commenting the below Lines and adding code below that 
                //foreach (XmlNode node in xmlNodeList)
                //{
                //    //if (xFilters == null)
                //    //{
                //    //    xFilters = new Dictionary<string, Dictionary<string, string>>();
                //    //}

                //    if (!Filters.ContainsKey(node.Attributes["templateid"].Value))
                //    {
                //        if (!temp.ContainsKey(node.Attributes["id"].Value))
                //        {
                //            temp.Add(node.Attributes["id"].Value, node.Attributes["SQLFill"].Value);
                //        }
                //    }
                    
                //    node.Attributes["SQLFill"].Value = string.Empty;
                //}

                //objElm = p_objXmlDoc.SelectSingleNode("//Filters/*/level") as XmlElement; // ("//Filters/*/level");

                //if (objElm != null && objElm.HasAttribute("templateid") && !Filters.ContainsKey(objElm.Attributes["templateid"].Value))
                //{
                //    Filters.Add(objElm.Attributes["templateid"].Value, temp);
                //}

                //Bharani - MITS : 35879 - New Code Start

                string sPrevious = string.Empty;
                bool bLastItemRequired = false;

                foreach (XmlNode node in xmlNodeList)
                {
                    if (!Filters.ContainsKey(node.Attributes["templateid"].Value))
                    {
                        if (!temp.ContainsKey(node.Attributes["id"].Value))
                        {
                            temp.Add(node.Attributes["id"].Value, node.Attributes["SQLFill"].Value);
                            bLastItemRequired = true;
                        }
                        else
                        {
                            if (node.NextSibling != null)
                            {
                                bLastItemRequired = false;
                                Dictionary<string, string> temp2 = new Dictionary<string, string>();
                                foreach (KeyValuePair<string, string> kv in temp)
                                {
                                    temp2.Add(kv.Key, kv.Value);
                                }
                                Filters.Add(sPrevious, temp2);
                                temp.Clear();
                                temp.Add(node.Attributes["id"].Value, node.Attributes["SQLFill"].Value);
                            }
                            else
                            {
                                bLastItemRequired = false;
                                Dictionary<string, string> temp2 = new Dictionary<string, string>();
                                foreach (KeyValuePair<string, string> kv in temp)
                                {
                                    temp2.Add(kv.Key, kv.Value);
                                }
                                Filters.Add(sPrevious, temp2);
                            }
                        }
                        sPrevious = node.Attributes["templateid"].Value;
                        node.Attributes["SQLFill"].Value = string.Empty;
                    }
                }

                objElm = p_objXmlDoc.SelectSingleNode("//Filters/*/level") as XmlElement; // ("//Filters/*/level");
                
                if (objElm != null && objElm.HasAttribute("templateid"))
                {
                    if (bLastItemRequired == true)
                    {
                        Dictionary<string, string> temp4 = new Dictionary<string, string>();
                        foreach (KeyValuePair<string, string> kv in temp)
                        {
                            temp4.Add(kv.Key, kv.Value);
                        }
                        Filters.Add(sPrevious, temp4);
                    }
                }

                //Bharani - MITS : 35879 - New Code End

                return p_objXmlDoc;
              }

            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AutoMailMerge.Get.Error",m_iClientId), p_objEx);//sonali
            }
            finally
            {
                objDefElm = null;
                objElm = null;
                objAvlFltrs = null;
                objSelFltrs = null;
                objFltrElm = null;
                objNewElm = null;
                if (objDs != null)
                {
                    objDs.Dispose();
                    objDs = null;
                }
                if (objDsGroups != null)
                {
                    objDsGroups.Dispose();
                    objDsGroups = null;
                }
              }
        }

        /// <summary>
        /// Load the Info Definition data
        /// </summary>
        /// <param name="p_objXml">Input xml document with data</param>
        /// <param name="p_ID">row id</param>
        /// <returns>xml document with data</returns>
        public XmlDocument LoadInfoDef(XmlDocument p_objXml, int p_ID)
        {
            int iPtr = 0;
            int iInfoNum = 0;
            int iFilterNum = -1;
            int iNumFilters = 0;
            int iTmplateNo = 0;
            int iDbType = 0;
            bool bEnd = false;
            //Start-Mridul Bansal. 01/19/10. MITS#18229.
            //bool bUseEnhPol=false;
            //bool bUseEnhPolforGL = false;
            //bool bUseEnhPolforPC = false;
            //bool bUseEnhPolforWC = false;
            //const int LOB_FOR_GC = 241;
            //const int LOB_FOR_WC = 243;
            //const int LOB_FOR_VA = 242;
            //const int LOB_FOR_PC = 845;
            ColLobSettings objLobSettings = null;
            //End-Mridul Bansal. 01/19/10. MITS#18229.
            string sFileName = "";
            string sKeyWord = "";
            string sValue = "";
            string sTmp = "";
            RMConfigurator objConfig = null;
            StreamReader objSRdr = null;
            XmlElement objTmplate = null;
            XmlElement objNewTmplate = null;
            XmlElement objMailTemp = null;
            XmlElement objAvlFltrs = null;
            Riskmaster.Settings.SysSettings objSys = null;

            try
            {
                objInfoDef.NumFilters = 0;
                objSys = new Riskmaster.Settings.SysSettings(m_sConnStr,m_iClientId);//sonali
                //Start-Mridul Bansal. 01/19/10. MITS#18229
                //bUseEnhPol = Conversion.ConvertLongToBool(objSys.UseEnhPolFlag);
                objLobSettings = new ColLobSettings(m_sConnStr, m_iClientId);
                //bUseEnhPolforGL = objLobSettings[LOB_FOR_GC].UseEnhPolFlag == -1 ? true : false;
                //bUseEnhPolforPC = objLobSettings[LOB_FOR_PC].UseEnhPolFlag == -1 ? true : false;
                //bUseEnhPolforWC = objLobSettings[LOB_FOR_WC].UseEnhPolFlag == -1 ? true : false;
                //End-Mridul Bansal. 01/19/10. MITS#18229
                objSys = null;
                string DefFileName = RMConfigurationManager.GetNameValueSectionSettings("RMUtilities",m_sConnStr,m_iClientId)["AutoMailInfoDevFile"];
                sFileName = String.Format(@"{0}\{1}", RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, "MailMerge"), DefFileName);
                //Geeta 03/22/07 : Modified for Mits number 7073
                //objInfoDef.FilterDef = new FilterDefinition[110];
                //rupal:for R8 Auto Diary Enhancement
                objInfoDef.FilterDef = new FilterDefinition[182];

                objTmplate = (XmlElement)p_objXml.SelectSingleNode("//AutoMailMergeTemplate");
                objAvlFltrs = (XmlElement)p_objXml.SelectSingleNode("//Filters/AvlFilters");

                objMailTemp = p_objXml.CreateElement("MailMergeTemplate");
                p_objXml.DocumentElement.AppendChild(objMailTemp);

                if (File.Exists(sFileName))
                {
                    using (objSRdr = new StreamReader(sFileName))
                    {
                        string sLine = "";
                        while ((sLine = objSRdr.ReadLine()) != null)
                        {
                            sLine = sLine.Trim();
                            if (sLine != "")
                            {
                                if (sLine.Substring(0, 1) == "[")
                                {
                                    sKeyWord = sLine.Substring(1, sLine.Length - 2);
                                    if (sKeyWord.ToUpper() == "FILTER")
                                    {
                                        iFilterNum += 1;
                                        iNumFilters += 1;
                                        objInfoDef.NumFilters++;
                                    }
                                    else
                                    {
                                        if (bEnd)
                                            break;
                                        if (p_ID > 0)
                                        {
                                            iFilterNum = -1;
                                            objInfoDef.NumFilters = 0;
                                        }
                                        iNumFilters = -1;
                                        iInfoNum += 1;
                                        objInfoDef.Name = sKeyWord;
                                        if (iNumFilters == -1)
                                        {
                                            //if (sLine.ToUpper() == "[POLICY REVIEW]")
                                            //{

                                            //    //Start-Mridul Bansal. 01/19/10. MITS#18229
                                            //    //if(!bUseEnhPol)
                                            //    if (!bUseEnhPolforAL || !bUseEnhPolforGL || !bUseEnhPolforPC || !bUseEnhPolforWC)
                                            //    //End-Mridul Bansal. 01/19/10. MITS#18229
                                            //    {

                                            //        objNewTmplate = p_objXml.CreateElement("level");
                                            //        objNewTmplate.SetAttribute("name", sKeyWord);
                                            //        //rupal:start, r8 auto diary enh
                                            //        //following line assumes that the templates in def file are
                                            //        //in proper sequence, i.e if 12 templates are defined, their templated id would be
                                            //        //1 to 12 in sequence. Actully this is not an ideal situation, because if we remove any template
                                            //        //from def file in future, we will not change the template id of other filters to follow the strict  sequence
                                            //        //for e.g if  any template with id 9 has become obsolete and we want to remove it from the def file,
                                            //        // we will just remove the template but not change/shift the id of other templates yo follow the sequence
                                            //        //therefore following line is incorrect as it assumes that all id in def file are in sequnce with increament value as 1
                                            //        //we actually need to put the def_inex of the template here, so we will do it in switch case where we find the def_indx

                                            //        //iTmplateNo++;
                                            //        //objNewTmplate.SetAttribute("value",iTmplateNo.ToString());    
                                            //        //objTmplate.AppendChild(objNewTmplate);
                                            //        //rupal:end

                                            //    }
                                            //}
                                            //else if (sLine.ToUpper() == "[POLICY MANAGEMENT REVIEW]")
                                            //{

                                            //    //Start-Mridul Bansal. 01/19/10. MITS#18229
                                            //    //if(bUseEnhPol)
                                            //    if (bUseEnhPolforAL || bUseEnhPolforGL || bUseEnhPolforPC || bUseEnhPolforWC)
                                            //    //End-Mridul Bansal. 01/19/10. MITS#18229
                                            //    {

                                            //        objNewTmplate = p_objXml.CreateElement("level");
                                            //        objNewTmplate.SetAttribute("name", sKeyWord);
                                            //        //rupal:start,r8 nauto diary enh
                                            //        //iTmplateNo++;
                                            //        //objNewTmplate.SetAttribute("value",iTmplateNo.ToString());
                                            //        //objTmplate.AppendChild(objNewTmplate);
                                            //        //rupal:end, r8 auto diary enh
                                            //    }
                                            //}


                                            objNewTmplate = p_objXml.CreateElement("level");
                                            objNewTmplate.SetAttribute("name", sKeyWord);
                                            //rupal:start,r8 nauto diary enh
                                            //iTmplateNo++;
                                            //objNewTmplate.SetAttribute("value",iTmplateNo.ToString());
                                            //objTmplate.AppendChild(objNewTmplate);
                                            //rupal:end, r8 auto diary enh

                                        }
                                    }
                                }
                                else
                                {
                                    
                                    iPtr = sLine.IndexOf("=");
                                    if (sLine.Substring(0, 2) == "//" || iPtr == 0)
                                            iPtr = 1;
                                        sKeyWord = sLine.Substring(0, iPtr);
                                        sValue = sLine.Substring(iPtr + 1, sLine.Length - iPtr - 1).Trim();
                                    switch (sKeyWord.ToUpper())
                                    {
                                        //rupal:start,r8 auto diary setup enh
                                        case "DATABASE":
                                            objInfoDef.FilterDef[iFilterNum].Database = sValue;
                                            break;
                                        //rupal:end
                                        case "NAME":
                                            objInfoDef.FilterDef[iFilterNum].Name = sValue;
                                            break;
                                        case "ID":
                                            if (iNumFilters == -1)
                                            {
                                                objInfoDef.ID = Conversion.ConvertStrToInteger(sValue);
                                                if (p_ID == Conversion.ConvertStrToInteger(sValue))
                                                    bEnd = true;
                                                //rupal:start, r8 auto diary enh                                                
                                                //adding child here solves two problem,
                                                //1. we will add correct def_index of the template
                                                //2.in cae of edit mode in auto diary setup, we will add only the template that was
                                                //selected for edit
                                                if (p_ID > 0)
                                                {
                                                    if (bEnd == true)
                                                    {
                                                        objNewTmplate.SetAttribute("value", sValue);
                                                        objTmplate.AppendChild(objNewTmplate);
                                                    }
                                                }
                                                else
                                                {

                                                    objNewTmplate.SetAttribute("value", sValue);
                                                    objTmplate.AppendChild(objNewTmplate);
                                                }
                                                //rupal:end, r8 auto diary enh
                                            }
                                            else
                                            {
                                                objInfoDef.FilterDef[iFilterNum].ID = Conversion.ConvertStrToInteger(sValue);
                                                objInfoDef.FilterDef[iFilterNum].TemplateId = objInfoDef.ID;
                                            }
                                            break;
                                        case "ATTACH_TABLE":
                                            objInfoDef.AttachTable = sKeyWord;
                                            break;
                                        case "ATTACH_COLUMN":
                                            objInfoDef.AttachCol = sKeyWord;
                                            break;
                                        case "ATTACH_DESC":
                                            objInfoDef.AttachDesc = sKeyWord;
                                            break;
                                        case "SQLFILL":
                                            objInfoDef.FilterDef[iFilterNum].SQLFill = sValue;
                                            objInfoDef.tmpSQL = sValue;
                                            break;
                                        case "SQLFROM":
                                            objInfoDef.FilterDef[iFilterNum].SQLFrom = sValue;
                                            objInfoDef.tmpSQL = sValue;
                                            break;
                                        case "SQLWHERE":
                                        case "TMPSQL":
                                        //TMPSQL SQLFILL and SQL have the same code
                                        case "SQL":
                                            if (sValue.Substring(0, 1) == "[")
                                            {
                                                iPtr = sValue.IndexOf("]");
                                                sTmp = sValue.Substring(2, iPtr - 2);
                                                sValue = sValue.Substring(0, iPtr + 1);
                                                switch (sTmp.ToUpper())
                                                {
                                                    case "SQLSERVER":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_SQLSRVR;
                                                        break;
                                                    case "SYBASE":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_SYBASE;
                                                        break;
                                                    case "INFORMIX":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_INFORMIX;
                                                        break;
                                                    case "ORACLE":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_ORACLE;
                                                        break;
                                                    case "DB2":
                                                        iDbType = (int)eDatabaseType.DBMS_IS_DB2;
                                                        break;
                                                    default:
                                                        iDbType = (int)eDatabaseType.DBMS_IS_ACCESS;
                                                        break;
                                                }
                                                if (iDbType == m_iDbType)
                                                {
                                                    objInfoDef.tmpSQL = sValue;
                                                    if (iFilterNum >= 0)
                                                        objInfoDef.FilterDef[iFilterNum].SQLWhere = sValue;
                                                }
                                            }
                                            else
                                            {
                                                objInfoDef.tmpSQL = sValue;
                                                if (iFilterNum >= 0)
                                                    objInfoDef.FilterDef[iFilterNum].SQLWhere = sValue;
                                            }
                                            break;
                                        case "TYPE":
                                            switch (sValue.ToUpper())
                                            {
                                                case "CHECKBOX":
                                                    objInfoDef.FilterDef[iFilterNum].FilterType = 1;
                                                    break;
                                                case "LIST":
                                                    objInfoDef.FilterDef[iFilterNum].FilterType = 2;
                                                    break;
                                                case "TOPX":
                                                    objInfoDef.FilterDef[iFilterNum].FilterType = 3;
                                                    break;
                                                case "COMBO":
                                                    objInfoDef.FilterDef[iFilterNum].FilterType = 4;
                                                    break;
                                                case "DURATION":
                                                    objInfoDef.FilterDef[iFilterNum].FilterType = 5;
                                                    break;
                                                case "NO_OPTION":
                                                    objInfoDef.FilterDef[iFilterNum].FilterType = 6;
                                                    break;
                                                default:
                                                    objInfoDef.FilterDef[iFilterNum].FilterType = 0;
                                                    break;
                                            }
                                            break;
                                        case "MIN":
                                            // MIN and DEFAULT have the same logic
                                            switch (sValue)
                                            {
                                                case "CURRENT_YEAR":
                                                    objInfoDef.FilterDef[iFilterNum].FilterMin = System.DateTime.Now.Year;
                                                    break;
                                                default:
                                                    objInfoDef.FilterDef[iFilterNum].FilterMin = Conversion.ConvertStrToLong(sValue);
                                                    break;
                                            }
                                            break;
                                        case "DEFAULT":
                                            objInfoDef.FilterDef[iFilterNum].DefValue = sValue;
                                            break;
                                        case "MAX":
                                            objInfoDef.FilterDef[iFilterNum].FilterMax = Conversion.ConvertStrToLong(sValue);
                                            break;
                                        case "TABLE":
                                            objInfoDef.FilterDef[iFilterNum].table = sValue;
                                            break;

                                    }
                                }
                            }
                        }
                    }
                    if (p_ID > objInfoDef.ID)
                        objInfoDef.Name = "";
                }
                return p_objXml;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AutoMailMerge.LoadInfoDef.Err",m_iClientId), p_objEx);//sonali
            }
            finally
            {
                if (objSRdr != null)
                    objSRdr.Dispose();
                objTmplate = null;
                objNewTmplate = null;
                objMailTemp = null;
                objAvlFltrs = null;
                objSys = null;
            }
        }

        /// Name			: LoadInfoSet
        /// Author			: Pankaj Chowdhury
        /// Date Created	: 16 May 2005		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Gets the InfoSet data
        /// </summary>
        /// <param name="p_objXml">Input xml document with data</param>
        /// <param name="p_ID">Auto Row Id</param>
        private void LoadInfoSet(XmlDocument p_objXml, int p_ID)
        {
            int iFilter = 0;
            int iTempFilter = 0;
            DbReader objRdr = null;
            try
            {
                objRdr = DbFactory.GetDbReader(m_sConnStr, "SELECT * FROM MERGE_AUTOSET WHERE AUTO_ROW_ID = " + p_ID);
                if (objRdr != null)
                {
                    if (objRdr.Read())
                    {
                        AutoMailMerge objAuto = new AutoMailMerge(m_sConnStr,m_iClientId);
                        LoadInfoDef(p_objXml, objRdr.GetInt32("DEF_INDEX"));
                        objInfoSet.Name = objRdr.GetString("AUTO_NAME");
                        p_objXml.SelectSingleNode("//AutoMailMergeName").InnerText = objRdr.GetString("AUTO_NAME");
                        objInfoSet.Index = objRdr.GetInt32("DEF_INDEX");
                        objInfoSet.DefName = objRdr.GetString("DEF_NAME");
                        objInfoSet.ProcessDate = objRdr.GetString("PROCESS_DATE");
                        objInfoSet.TemplateID = objRdr.GetInt32("LETTER_ID");
                        objInfoSet.TemplateName = objRdr.GetString("LETTER_NAME");
                        objInfoSet.PrinterName = objRdr.GetString("PRINTER_NAME");
                        objInfoSet.PaperBin = objRdr.GetString("LETTER_PAPER_BIN");
                    }
                }

                objRdr.Close();

                //Get Filters
                objInfoSet.FilterSet = new FilterSetting[100];
                objRdr = DbFactory.GetDbReader(m_sConnStr, "SELECT * FROM MERGE_AUTO_FILTER WHERE AUTO_ID = " + p_ID);
                if (objRdr != null)
                {
                    while (objRdr.Read())
                    {
                        objInfoSet.FilterSet[iFilter].Number = objRdr.GetInt32("FILTER_INDEX");
                        objInfoSet.FilterSet[iFilter].Data = objRdr.GetString("FILTER_DATA");
                        iFilter += 1;
                    }
                }
                objRdr.Dispose();
                objInfoSet.NumFilters = iFilter;

                //Get Filters
                objInfoSet.TemplateFilterSet = new TemplateFilterSetting[100];
                objRdr = DbFactory.GetDbReader(m_sConnStr, "SELECT * FROM MERGE_TBR_FILTER WHERE AUTO_ID = " + p_ID);
                if (objRdr != null)
                {
                    while (objRdr.Read())
                    {
                        objInfoSet.TemplateFilterSet[iTempFilter].Number = objRdr.GetInt32("CATEGORY_ID");
                        objInfoSet.TemplateFilterSet[iTempFilter].Data = objRdr.GetString("TBR_FILTER_DATA");
                        iTempFilter += 1;
                    }
                }
                objRdr.Dispose();
                objInfoSet.TempNumFilters = iTempFilter;

                objRdr = DbFactory.GetDbReader(m_sConnStr, "SELECT * FROM MERGE_ENTITY_ROLE WHERE AUTO_ID = " + p_ID);
                if (objRdr != null)
                {
                    if (objRdr.Read())
                    {
                        objInfoSet.PersonInvolvedValue = objRdr.GetString("PERSON_INV_ROLE");
                        objInfoSet.AdjusterValue = objRdr.GetString("ADJUSTER_ROLE");
                        objInfoSet.CasemanagerValue = objRdr.GetString("CASEMANAGER_ROLE");
                        objInfoSet.DefendantValue = objRdr.GetString("DEFENDANT_TYPE");
                        objInfoSet.ClaimantValue = objRdr.GetString("CLAIMANT_TYPE");
                        objInfoSet.ExpertWitnessValue = objRdr.GetString("EXPERTWITNESS_TYPE");
                    }
                }

                objRdr.Close();
              
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AutoMailMerge.LoadInfoSet.Err",m_iClientId), p_objEx);//sonali
            }
            finally
            {
                if (objRdr != null)
                {
                    objRdr.Dispose();
                    objRdr = null;
                }
            }
        }

        internal string FormatDate(string p_sDate, bool p_bIsSave)
        {
            if (p_sDate.Trim() == "" || p_sDate == "0")
                return "";
            if (p_bIsSave)
            {
                return Conversion.GetUIDate(p_sDate, objUserLogin.objUser.NlsCode.ToString(), m_iClientId);//vkumar258 ML changes
            }
            else
            {
                //return Conversion.ToDate(Conversion.GetDate(p_sDate)).ToShortDateString();
                return Conversion.GetUIDate(p_sDate, objUserLogin.objUser.NlsCode.ToString(), m_iClientId);//vkumar258 ML changes
            }
        }

        public XmlDocument GetAvailableTemplateList(XmlDocument p_objXml)
        {
            XmlElement objElemTemp = null;
            XmlElement objAvalMailTemplate = null;
            objAvalMailTemplate = (XmlElement)p_objXml.SelectSingleNode("//AvailableTemplateList");
            string sSQL = "";
            string sMergeSQL = "";
            DbReader objReader = null;
            DbReader objmergeReader = null;
            try
            {
                sSQL = "SELECT FORM_ID,FORM_NAME,FORM_DESC,CAT_ID,SEND_MAIL_FLAG,DESIGNATED_RECIPIENT,TABLE_QUALIFY FROM MERGE_FORM "
                    + " WHERE CAT_ID= 1";
                sSQL = sSQL + " ORDER BY FORM_NAME";
                using (objReader = DbFactory.GetDbReader(m_sConnStr, sSQL))
                {
                    while (objReader.Read())
                    {
                        objElemTemp = p_objXml.CreateElement("Template");
                        objElemTemp.SetAttribute("Category", Conversion.ConvertObjToStr(objReader.GetValue("CAT_ID")));
                        objElemTemp.SetAttribute("TemplateDesc", Conversion.ConvertObjToStr(objReader.GetValue("FORM_NAME")));
                        objElemTemp.SetAttribute("TemplateId", Conversion.ConvertObjToStr(objReader.GetValue("FORM_ID")));
                        objElemTemp.SetAttribute("TableQualify", Conversion.ConvertObjToStr(objReader.GetValue("TABLE_QUALIFY")));

                        objElemTemp.SetAttribute("SendMailFlag", Conversion.ConvertObjToStr(objReader.GetValue("SEND_MAIL_FLAG")));
                        objElemTemp.SetAttribute("MailRecipient", Conversion.ConvertObjToStr(objReader.GetValue("DESIGNATED_RECIPIENT")));

                        string FormID = string.Empty;
                        FormID = Conversion.ConvertObjToStr(objReader.GetValue("FORM_ID"));
                        sMergeSQL = "SELECT FIELD_ID,FIELD_TABLE, DISPLAY_CAT from MERGE_DICTIONARY where FIELD_ID IN (select FIELD_ID from MERGE_FORM_DEF where FORM_ID =  " + FormID + ')';

                        string CommaSeparateField = string.Empty;
                        using (objmergeReader = DbFactory.GetDbReader(m_sConnStr, sMergeSQL))
                        {
                            while (objmergeReader.Read())
                            {
                                string DisplayCategory = Conversion.ConvertObjToStr(objmergeReader.GetValue("DISPLAY_CAT"));
                                if (CommaSeparateField == "")
                                {
                                    CommaSeparateField = DisplayCategory;
                                }
                                else
                                {
                                    CommaSeparateField = CommaSeparateField + "," + DisplayCategory;
                                }
                            }

                            objElemTemp.SetAttribute("DisplayCategory", CommaSeparateField);
                        }

                        objAvalMailTemplate.AppendChild(objElemTemp);
                    }
                }

                objmergeReader.Close();
                objReader.Close();
                return p_objXml;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("AutoMailMerge.GetAvailableTemplateList.Error",m_iClientId), p_objException);//sonali
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                p_objXml = null;
                objElemTemp = null;
                objAvalMailTemplate = null;

            }
        }

        /// Name			: GetFilterData
        /// Author			: Nitika Gupta
        /// Date Created	: 08/10/2012	
        /// <summary>
        /// Gets the xml structure with data for the first step of the wizard
        /// </summary>
        /// <param name="p_objXmlDoc">Input xml document structure</param>
        /// <returns>Xml document with data and its structure for UI</returns>
        public XmlDocument GetFilterData(XmlDocument p_objXmlDoc)
        {
            string sId = "";
            string sSQL = "";
            string sFilterType = "";
            string sDefVal = "";
            string sData = "";
            DbReader objRdr = null;
            XmlElement objElm = null;
            DataTable objTbl = null;
            XmlElement objSelLst = null;
            XmlElement objAvlLst = null;
            XmlElement objNewElm = null;
            var SelectedNodeSql=string.Empty;
            string templateId = string.Empty;
            
            try
            {
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//id");
                if (objElm != null)
                    sId = objElm.InnerText;

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//templateid");
                if (objElm != null)
                    templateId = objElm.InnerText;

                //LINQ to find selected node from loaded XMl
                if (xFilters != null)
                {
                    //var SelectedNode =(xElementFilters.Elements("AvlFilters").Where(n=>n.Attribute("id").Value==sId).Select(n=>n.Value)).FirstOrDefault();
                    //SelectedNodeSql = (from node in xElementFilters.Descendants("level")
                    //                   where node.Attribute("id").Value == sId
                    //                   //where (node.Elements("level")).Where(n=>n.Attribute("id").Value==sId)
                    //                   select node.Attribute("SQLFill").Value).FirstOrDefault();
                    if (xFilters.ContainsKey(templateId) && xFilters[templateId].ContainsKey(sId))
                    {
                        SelectedNodeSql = xFilters[templateId][sId];
                    }
                }
                objAvlLst = (XmlElement)p_objXmlDoc.SelectSingleNode("//AvlBodyParts");

                objSelLst = (XmlElement)p_objXmlDoc.SelectSingleNode("//SelBodyParts");

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SQLFill");
                if (objElm != null)
                {
                    //sSQL = objElm.InnerText;
                    sSQL = SelectedNodeSql;
                }
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//FilterType");
                if (objElm != null)
                    sFilterType = objElm.InnerText;
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//DefValue");
                if (objElm != null)
                    sDefVal = objElm.InnerText;
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//data");
                if (objElm != null)
                    sData = objElm.InnerText;

                objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                objTbl = objRdr.GetSchemaTable();
                while (objRdr.Read())
                {
                    string sName = "";
                    for (int iCtr = 0; iCtr < objRdr.FieldCount - 1; iCtr++)
                    {
                        sName += objRdr.GetString(iCtr);
                    }

                    if (sData.IndexOf("," + objRdr[objRdr.FieldCount - 1].ToString() + ",") != -1)
                    {
                        objNewElm = p_objXmlDoc.CreateElement("option");
                        objNewElm.SetAttribute("value", objRdr[objRdr.FieldCount - 1].ToString());

                        objNewElm.InnerText = sName;
                        objSelLst.AppendChild(objNewElm);
                    }
                    else
                    {
                        objNewElm = p_objXmlDoc.CreateElement("option");
                        objNewElm.SetAttribute("value", objRdr[objRdr.FieldCount - 1].ToString());

                        objNewElm.InnerText = sName;
                        objAvlLst.AppendChild(objNewElm);
                    }
                }
                return p_objXmlDoc;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AutoMailMerge.Get.Error",m_iClientId), p_objEx);//sonali
            }
            finally
            {
                objElm = null;
                if (objRdr != null)
                {
                    objRdr.Dispose();
                    objRdr = null;
                }
                if (objTbl != null)
                {
                    objTbl.Dispose();
                }
            }
        }

        public XmlDocument GetPeopleInvolved(XmlDocument p_objXml)
        {
            int iMax_Codes_Per_Page = 0;
            int iPageCount = 0;
            string sPagecount = null;
            XmlElement objElemTemp = null;
            XmlElement objPerInvList = null;
            XmlElement objPagecount = null;
            iMax_Codes_Per_Page = GetMaxRecordsonPage();
            objPerInvList = (XmlElement)p_objXml.SelectSingleNode("//GetPersonInvolvedList");
            objPagecount = (XmlElement)p_objXml.SelectSingleNode("//PageCount");
            string sSQL = string.Empty;
            int iCount = 1;
            DbReader objReader = null;
            try
            {
                sSQL = "SELECT GT.TABLE_NAME, G.TABLE_ID FROM GLOSSARY_TEXT GT, GLOSSARY G WHERE GT.TABLE_ID=G.TABLE_ID AND G.GLOSSARY_TYPE_CODE=7  ORDER BY GT.TABLE_NAME";
                using (objReader = DbFactory.GetDbReader(m_sConnStr, sSQL))
                {
                    while (objReader.Read())
                    {
                        objElemTemp = p_objXml.CreateElement("PersonList");
                        objElemTemp.SetAttribute("ID", Conversion.ConvertObjToStr(objReader.GetValue("TABLE_ID")));
                        objElemTemp.SetAttribute("Name", Conversion.ConvertObjToStr(objReader.GetValue("TABLE_NAME")));
                        objPerInvList.AppendChild(objElemTemp);
                    }
                }

                using (DbReader objCount = DbFactory.GetDbReader(m_sConnStr, "SELECT COUNT(GT.TABLE_NAME) COUNTI FROM GLOSSARY_TEXT GT, GLOSSARY G WHERE GT.TABLE_ID=G.TABLE_ID AND G.GLOSSARY_TYPE_CODE=7"))
                {
                    while (objCount.Read())
                    {
                        iCount = Conversion.ConvertObjToInt(objCount.GetValue("COUNTI"), m_iClientId);
                        iPageCount = iCount / iMax_Codes_Per_Page;
                        if ((iCount % iMax_Codes_Per_Page) != 0)
                        {
                            iPageCount += 1;
                        }
                        sPagecount = iPageCount.ToString();
                        objPagecount.SetAttribute("PageCount", sPagecount);
                    }
                }

                objReader.Close();
                return p_objXml;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("AutoMailMerge.GetAvailableTemplateList.Error",m_iClientId), p_objException);//sonali
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                p_objXml = null;
                objElemTemp = null;
                objPerInvList = null;

            }
        }

        private int GetMaxRecordsonPage()
        {
            StringBuilder sbSQL = null;
            int iReturn = 0;
            bool bIsSuccess = false;

            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE");
                sbSQL.Append(" WHERE PARM_NAME = 'CODES_USER_LIMIT' ");

                using (DbReader objReader = DbFactory.GetDbReader(m_sConnStr, sbSQL.ToString()))
                {
                    if (objReader.Read())
                    {
                        iReturn = Conversion.CastToType<int>(Convert.ToString(objReader.GetValue("STR_PARM_VALUE")), out bIsSuccess);
                    }
                    else
                    {
                        iReturn = 100;
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                sbSQL = null;

            }
            return iReturn;
        }

        public XmlDocument GetAdditionalFilterData(XmlDocument p_objXml)
        {
            string sTemplateID = string.Empty;
            string sFormTemplateID = string.Empty;
            string sFilterName = string.Empty;
            string sFilterID = string.Empty;
            string sMode = string.Empty;
            string sData = string.Empty;
            string sSQL = string.Empty;
            DbReader objReader = null;
            XmlElement objElm = null;
            XmlElement objSelLst = null;
            XmlElement objAvlLst = null;
            XmlElement objElemTemp = null;
            XmlElement objElemSelTemp = null;
            
            try
            {
                objAvlLst = (XmlElement)p_objXml.SelectSingleNode("//AvlBodyParts");

                objSelLst = (XmlElement)p_objXml.SelectSingleNode("//SelBodyParts");

                objElm = (XmlElement)p_objXml.SelectSingleNode("//templateid");
                if (objElm != null)
                    sTemplateID = objElm.InnerText;
                objElm = (XmlElement)p_objXml.SelectSingleNode("//selectedformtemplateid");
                if (objElm != null)
                    sFormTemplateID = objElm.InnerText;
                objElm = (XmlElement)p_objXml.SelectSingleNode("//filtername");
                if (objElm != null)
                    sFilterName = objElm.InnerText;
                objElm = (XmlElement)p_objXml.SelectSingleNode("//filterID");
                if (objElm != null)
                    sFilterID = objElm.InnerText;
                objElm = (XmlElement)p_objXml.SelectSingleNode("//mode");
                if (objElm != null)
                    sMode = objElm.InnerText;
                objElm = (XmlElement)p_objXml.SelectSingleNode("//data");
                if (objElm != null)
                    sData = objElm.InnerText;

                
                if (sFilterID == "1")
                {
                    sSQL = "select CODE_ID, SHORT_CODE, CODE_DESC from CODES_TEXT where CODE_ID IN (select CODE_ID FROM CODES WHERE TABLE_ID = (select TABLE_ID from GLOSSARY where SYSTEM_TABLE_NAME = 'LIT_TYPE'))";

                }
                else if (sFilterID == "2")
                {
                    sSQL = "select CODE_ID, SHORT_CODE, CODE_DESC from CODES_TEXT where CODE_ID IN (select CODE_ID FROM CODES WHERE TABLE_ID = (select TABLE_ID from GLOSSARY where SYSTEM_TABLE_NAME = 'LITIGATION_STATUS'))";

                }
                else if (sFilterID == "3")
                {
                    sSQL = "select CODE_ID, SHORT_CODE, CODE_DESC from CODES_TEXT where CODE_ID IN (select CODE_ID FROM CODES WHERE TABLE_ID = (select TABLE_ID from GLOSSARY where SYSTEM_TABLE_NAME = 'SUBRO_TYPE'))";

                }
                else if (sFilterID == "4")
                {
                    sSQL = "select CODE_ID, SHORT_CODE, CODE_DESC from CODES_TEXT where CODE_ID IN (select CODE_ID FROM CODES WHERE TABLE_ID = (select TABLE_ID from GLOSSARY where SYSTEM_TABLE_NAME = 'SUBRO_STATUS'))";
                }
                else if (sFilterID == "5")
                {
                    sSQL = "select CODE_ID, SHORT_CODE, CODE_DESC from CODES_TEXT where CODE_ID IN (select CODE_ID FROM CODES WHERE TABLE_ID = (select TABLE_ID from GLOSSARY where SYSTEM_TABLE_NAME = 'SUBRO_STATUS_DESC'))";
                }
                else if (sFilterID == "6")
                {
                    sSQL = "select CODE_ID, SHORT_CODE, CODE_DESC from CODES_TEXT where CODE_ID IN (select CODE_ID FROM CODES WHERE TABLE_ID = (select TABLE_ID from GLOSSARY where SYSTEM_TABLE_NAME = 'LIABILITY_TYPE'))";
                }
                else if (sFilterID == "7")
                {
                    sSQL = "select CODE_ID, SHORT_CODE, CODE_DESC from CODES_TEXT where CODE_ID IN (select CODE_ID FROM CODES WHERE TABLE_ID = (select TABLE_ID from GLOSSARY where SYSTEM_TABLE_NAME = 'TR_PLAN_STATUS'))";
                }
                else if (sFilterID == "8")
                {
                    sSQL = "select CODE_ID, SHORT_CODE, CODE_DESC from CODES_TEXT where CODE_ID IN (select CODE_ID FROM CODES WHERE TABLE_ID = (select TABLE_ID from GLOSSARY where SYSTEM_TABLE_NAME = 'PROPERTY_TYPE'))";
                }
                else if (sFilterID == "9")
                {
                    sSQL = "select CODE_ID, SHORT_CODE, CODE_DESC from CODES_TEXT where CODE_ID IN (select CODE_ID FROM CODES WHERE TABLE_ID = (select TABLE_ID from GLOSSARY where SYSTEM_TABLE_NAME = 'CLAIMANT_TYPE'))";
                }
                else if (sFilterID == "10")
                {
                    sSQL = "select CODE_ID, SHORT_CODE, CODE_DESC from CODES_TEXT where CODE_ID IN (select CODE_ID FROM CODES WHERE TABLE_ID = (select TABLE_ID from GLOSSARY where SYSTEM_TABLE_NAME = 'DEFENDANT_TYPE'))";
                }

                using (objReader = DbFactory.GetDbReader(m_sConnStr, sSQL))
                {
                    while (objReader.Read())
                    {
                        objElemTemp = p_objXml.CreateElement("Option");
                        objElemTemp.SetAttribute("CategoryID", Conversion.ConvertObjToStr(objReader.GetValue("CODE_ID")));
                        objElemTemp.SetAttribute("Description", Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")));
                        objElemTemp.SetAttribute("ShortCode", Conversion.ConvertObjToStr(objReader.GetValue("SHORT_CODE")));
                        objAvlLst.AppendChild(objElemTemp);
                    }
                }

                if (!string.IsNullOrEmpty(sData))
                {
                    string sSQLCode = string.Empty;
                    DbReader objmgReader = null;

                    string[] sDisplayCategoryArray = sData.Split(',');

                    string CommaSeparateField = string.Empty;
                    foreach (string sDisplayCategory in sDisplayCategoryArray)
                    {
                        if (sDisplayCategory != "")
                        {
                            if (CommaSeparateField == "")
                            {
                                CommaSeparateField = sDisplayCategory;
                            }
                            else
                            {
                                CommaSeparateField = CommaSeparateField + "," + sDisplayCategory;
                            }
                        }
                    }

                    sSQLCode = "select CODE_ID, SHORT_CODE, CODE_DESC from CODES_TEXT where CODE_ID IN (" + CommaSeparateField + ")";
                    using (objmgReader = DbFactory.GetDbReader(m_sConnStr, sSQLCode))
                    {

                        while (objmgReader.Read())
                        {
                            objElemSelTemp = p_objXml.CreateElement("Option");
                            objElemSelTemp.SetAttribute("CategoryID", Conversion.ConvertObjToStr(objmgReader.GetValue("CODE_ID")));
                            objElemSelTemp.SetAttribute("Description", Conversion.ConvertObjToStr(objmgReader.GetValue("CODE_DESC")));
                            objElemSelTemp.SetAttribute("ShortCode", Conversion.ConvertObjToStr(objmgReader.GetValue("SHORT_CODE")));
                            objSelLst.AppendChild(objElemSelTemp);
                        }
                    }

                    objmgReader.Close();
                }
                    
                objReader.Close();
                return p_objXml;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("GetAdditionalFilterData.Get.Error",m_iClientId), p_objEx);//sonali
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                p_objXml = null;
                objElemTemp = null;
                objAvlLst = null;
                objSelLst = null;
                objElemSelTemp = null;

            }

        }

        public XmlDocument GetEntityRoleList(XmlDocument p_objXml)
        {
            string sClaimantType = string.Empty;
            string sExpertType = string.Empty;
            string sDefendantType = string.Empty;
            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;
            string sExpertShortCode = string.Empty;
            string sExpertCodeDesc = string.Empty;
            string sDefShortCode = string.Empty;
            string sDefCodeDesc = string.Empty;
            LocalCache ObjCache = null;
            XmlElement objClaimantList = null;
            XmlElement objXMLClaimant = null;
            XmlElement objExpertList = null;
            XmlElement objXMLExpert = null;
            XmlElement objDefendantList = null;
            XmlElement objXMLDefendant = null;
            XmlElement objElemTemp = null;
            XmlElement objExpertElemTemp = null;
            XmlElement objDefElemTemp = null;
            XmlElement objElm = null;
            ObjCache = new LocalCache(m_sConnStr,m_iClientId);

            
           
           
            try
            {
                objXMLClaimant = (XmlElement)p_objXml.SelectSingleNode("//ClaimantList");

                if (objXMLClaimant == null)
                {
                    objClaimantList = p_objXml.CreateElement("ClaimantList");
                    p_objXml.DocumentElement.AppendChild(objClaimantList);
                }

                objXMLExpert = (XmlElement)p_objXml.SelectSingleNode("//ExpertWitnessList");

                if (objXMLExpert == null)
                {
                    objExpertList = p_objXml.CreateElement("ExpertWitnessList");
                    p_objXml.DocumentElement.AppendChild(objExpertList);
                }

                objXMLDefendant = (XmlElement)p_objXml.SelectSingleNode("//DefendantList");

                if (objXMLDefendant == null)
                {
                    objDefendantList = p_objXml.CreateElement("DefendantList");
                    p_objXml.DocumentElement.AppendChild(objDefendantList);
                }

                objElm = (XmlElement)p_objXml.SelectSingleNode("//ClaimantValues");
                if (objElm != null)
                    sClaimantType  = objElm.InnerText;

                objElm = (XmlElement)p_objXml.SelectSingleNode("//ExpertWitnessValues");
                if (objElm != null)
                    sExpertType = objElm.InnerText;

                objElm = (XmlElement)p_objXml.SelectSingleNode("//DefendantValues");
                if (objElm != null)
                    sDefendantType = objElm.InnerText;

                string[] ClaimantList = sClaimantType.Split(',');

                foreach (string sDisplayCategory in ClaimantList)
                {
                    if (!string.IsNullOrEmpty(sDisplayCategory))
                    {
                        int iCodeID = Convert.ToInt32(sDisplayCategory);
                        ObjCache.GetCodeInfo(iCodeID, ref sShortCode, ref sCodeDesc);
                        objElemTemp = p_objXml.CreateElement("Option");
                        objElemTemp.SetAttribute("ID", sDisplayCategory);
                        objElemTemp.SetAttribute("Description", sCodeDesc);
                        objElemTemp.SetAttribute("ShortCode", sShortCode);
                        objClaimantList.AppendChild(objElemTemp);
                    }
                }

                string[] ExpertList = sExpertType.Split(',');

                foreach (string sExpertCategory in ExpertList)
                {
                    if (!string.IsNullOrEmpty(sExpertCategory))
                    {
                        int iCodeID = Convert.ToInt32(sExpertCategory);
                        ObjCache.GetCodeInfo(iCodeID, ref sExpertShortCode, ref sExpertCodeDesc);
                        objExpertElemTemp = p_objXml.CreateElement("Option");
                        objExpertElemTemp.SetAttribute("ID", sExpertCategory);
                        objExpertElemTemp.SetAttribute("Description", sExpertCodeDesc);
                        objExpertElemTemp.SetAttribute("ShortCode", sExpertShortCode);
                        objExpertList.AppendChild(objExpertElemTemp);
                    }
                }

                string[] DefendantList = sDefendantType.Split(',');

                foreach (string sDefendantCategory in DefendantList)
                {
                    if (!string.IsNullOrEmpty(sDefendantCategory))
                    {
                        int iCodeID = Convert.ToInt32(sDefendantCategory);
                        ObjCache.GetCodeInfo(iCodeID, ref sDefShortCode, ref sDefCodeDesc);
                        objDefElemTemp = p_objXml.CreateElement("Option");
                        objDefElemTemp.SetAttribute("ID", sDefendantCategory);
                        objDefElemTemp.SetAttribute("Description", sDefCodeDesc);
                        objDefElemTemp.SetAttribute("ShortCode", sDefShortCode);
                        objDefendantList.AppendChild(objDefElemTemp);
                    }
                }
               
                   return p_objXml;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("GetEntityRoleList.Get.Error",m_iClientId), p_objEx);//sonali
            }
            finally
            {
               
                p_objXml = null;
                objElemTemp = null;
                objDefElemTemp = null;
                objExpertElemTemp = null;
                objClaimantList = null;
                objExpertList = null;
                objDefendantList = null;
                objXMLDefendant = null;
                objXMLExpert = null;
                objXMLClaimant = null;
            }

        }

        public XmlDocument GetPeopleInvolvedList(XmlDocument p_objXml)
        {
            XmlElement objElemTemp = null;
            XmlElement objPerInvList = null;
            XmlElement objSelPerInvList = null;
            string sPersonType = string.Empty;
            string sSQL = string.Empty;
            DbReader objReader = null;
            
            objPerInvList = (XmlElement)p_objXml.SelectSingleNode("//PersonInvolvedValues");
            objSelPerInvList = (XmlElement)p_objXml.SelectSingleNode("//SelectedPersonInvoluedList");
            
            if (objPerInvList != null)
            {
                sPersonType = objPerInvList.InnerText;
            }

            try
            {
                sSQL = "SELECT TABLE_NAME,TABLE_ID FROM GLOSSARY_TEXT WHERE TABLE_ID IN (" + sPersonType + ")";
                using (objReader = DbFactory.GetDbReader(m_sConnStr, sSQL))
                {
                    while (objReader.Read())
                    {
                        objElemTemp = p_objXml.CreateElement("PersonList");
                        objElemTemp.SetAttribute("ID", Conversion.ConvertObjToStr(objReader.GetValue("TABLE_ID")));
                        objElemTemp.SetAttribute("Name", Conversion.ConvertObjToStr(objReader.GetValue("TABLE_NAME")));
                        objSelPerInvList.AppendChild(objElemTemp);
                    }
                }

                objReader.Close();
                return p_objXml;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("AutoMailMerge.GetPeopleInvolvedList.Error",m_iClientId), p_objException);//sonali
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                p_objXml = null;
                objElemTemp = null;
                objPerInvList = null;
                objSelPerInvList = null;
            }
        }

        public XmlDocument Save(XmlDocument p_objXml)
        {
            int iRowId = 0;
            string sProcDt = string.Empty;
            string sAutoName = string.Empty;
            string sDefName = string.Empty;
            int iDefIndex = 0;
            int iTemplateID = 0;
            string sTemplatename = string.Empty;
            string sPrintername = string.Empty;
            int iPaperBin = 0;
            string sCategoryName = string.Empty;
            string sPersonInvolved = string.Empty;
            string sDefendantType = string.Empty;
            string sExpertWitnessType = string.Empty;
            string sAdjusterType = string.Empty;
            string sCasemanagerType = string.Empty;

            XmlElement objElm = null;
            DbConnection objConn = null;
            DbCommand objCommand = null;
            DbTransaction objTran = null;
            XmlNodeList objNodLst = null;
            XmlNodeList objTempNodLst = null;
            

            try
            {
                objElm = (XmlElement)p_objXml.SelectSingleNode("//DefId");
                if (objElm != null)
                    iRowId = Conversion.ConvertStrToInteger(objElm.InnerText);

                objElm = (XmlElement)p_objXml.SelectSingleNode("//AutoMailMergeDate");
                if (objElm != null)
                    sProcDt = Conversion.GetDate(objElm.InnerText);

                objElm = (XmlElement)p_objXml.SelectSingleNode("//AutoMailMergeName");
                if (objElm != null)
                    sAutoName = objElm.InnerText;

                objElm = (XmlElement)p_objXml.SelectSingleNode("//AutoMailMergeTemplate");
                if (objElm != null)
                {

                    sDefName = objElm.GetAttribute("selectedname");
                    iDefIndex = Conversion.ConvertStrToInteger(objElm.Attributes["selectedid"].Value);
                }

                objElm = (XmlElement)p_objXml.SelectSingleNode("//AutoMailMergeSelFormTemplate");
                if (objElm != null)
                {
                    sTemplatename = objElm.GetAttribute("selectedname");
                    iTemplateID = Conversion.ConvertStrToInteger(objElm.Attributes["selectedid"].Value);
                }

                objElm = (XmlElement)p_objXml.SelectSingleNode("//PrinterName");
                if (objElm != null)
                    sPrintername = objElm.InnerText;

                objElm = (XmlElement)p_objXml.SelectSingleNode("//PaperBin");
                if (objElm != null)
                    iPaperBin = Conversion.ConvertStrToInteger(objElm.InnerText);

                objElm = (XmlElement)p_objXml.SelectSingleNode("//DefendantValues");
                if (objElm != null)
                {
                    sDefendantType = objElm.InnerText;
                }

                objElm = (XmlElement)p_objXml.SelectSingleNode("//ClaimantValues");
                if (objElm != null)
                {
                    sCategoryName = objElm.InnerText;
                }

                objElm = (XmlElement)p_objXml.SelectSingleNode("//ExpertWitnessValues");
                if (objElm != null)
                {
                    sExpertWitnessType = objElm.InnerText;
                }

                objElm = (XmlElement)p_objXml.SelectSingleNode("//PersonInvolvedValues");
                if (objElm != null)
                {
                    sPersonInvolved = objElm.InnerText;
                }

                objElm = (XmlElement)p_objXml.SelectSingleNode("//AdjusterValue");
                if (objElm != null)
                {
                    sAdjusterType = objElm.InnerText;
                }

                objElm = (XmlElement)p_objXml.SelectSingleNode("//CaseManagerValue");
                if (objElm != null)
                {
                    sCasemanagerType = objElm.InnerText;
                }

                if (iRowId == 0)
                {
                    iRowId = Utilities.GetNextUID(m_sConnStr, "MERGE_AUTOSET", m_iClientId);
                }

                objConn = DbFactory.GetDbConnection(m_sConnStr);
                objConn.Open();
                objCommand = objConn.CreateCommand();
                objTran = objConn.BeginTransaction();
                objCommand.Transaction = objTran;

                objCommand.CommandText = "DELETE FROM MERGE_AUTOSET WHERE AUTO_ROW_ID = " + iRowId;
                objCommand.ExecuteNonQuery();

                objCommand.CommandText = "INSERT INTO MERGE_AUTOSET(AUTO_ROW_ID, PROCESS_DATE, AUTO_NAME, DEF_INDEX, DEF_NAME," +
                    "LETTER_ID, LETTER_NAME,PRINTER_NAME,LETTER_PAPER_BIN) VALUES(" +
                    iRowId + ",'" + sProcDt + "','" + sAutoName + "'," + iDefIndex + ",'" + sDefName + "'," + iTemplateID + ",'" + sTemplatename + "','" + sPrintername + "'," +
                    iPaperBin + ")";
              
                objCommand.ExecuteNonQuery();

                objNodLst = p_objXml.SelectNodes("//Filters/SelectedFilters/level");
                objCommand.CommandText = "DELETE FROM MERGE_AUTO_FILTER WHERE AUTO_ID = " + iRowId;
                objCommand.ExecuteNonQuery();
                foreach (XmlNode objNod in objNodLst)
                {
                    objCommand.CommandText = "INSERT INTO MERGE_AUTO_FILTER(AUTO_ID,FILTER_INDEX,FILTER_DATA) VALUES(" +
                        iRowId + "," + ((XmlElement)objNod).GetAttribute("id") + ",'" + ((XmlElement)objNod).GetAttribute("data") + "')";
                    objCommand.ExecuteNonQuery();
                }

                objTempNodLst = p_objXml.SelectNodes("//SelectedTemplateList/Template");
                objCommand.CommandText = "DELETE FROM MERGE_TBR_FILTER WHERE AUTO_ID = " + iRowId;
                objCommand.ExecuteNonQuery();
                foreach (XmlNode objTempNod in objTempNodLst)
                {
                    objCommand.CommandText = "INSERT INTO MERGE_TBR_FILTER(AUTO_ID,CATEGORY_ID,TBR_FILTER_DATA) VALUES(" +
                        iRowId + "," + ((XmlElement)objTempNod).GetAttribute("SelectedFilterID") + ",'" + ((XmlElement)objTempNod).GetAttribute("data") + "')";
                    objCommand.ExecuteNonQuery();
                }


                objCommand.CommandText = "DELETE FROM MERGE_ENTITY_ROLE WHERE AUTO_ID = " + iRowId;
                objCommand.ExecuteNonQuery();

                objCommand.CommandText = "INSERT INTO MERGE_ENTITY_ROLE(AUTO_ID, PERSON_INV_ROLE, DEFENDANT_TYPE, CLAIMANT_TYPE, EXPERTWITNESS_TYPE, ADJUSTER_ROLE," +
                    "CASEMANAGER_ROLE) VALUES(" +
                    iRowId + ",'" + sPersonInvolved + "','" + sDefendantType + "','" + sCategoryName + "','" + sExpertWitnessType + "','" + sAdjusterType + "','" + sCasemanagerType + "')";

                objCommand.ExecuteNonQuery();

                objTran.Commit();

                return p_objXml;
            }
            catch (Exception p_objEx)
            {
                objTran.Rollback();
                throw new RMAppException(Globalization.GetString("AutoMailMerge.Save.Err",m_iClientId), p_objEx);//sonali
            }
            finally
            {
                if (objTran != null)
                {
                    objTran.Dispose();
                    objTran = null;
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                objNodLst = null;
                objCommand = null;
                objElm = null;
                objTempNodLst = null;
            }
        }


        public XmlDocument Get(XmlDocument p_objXml)
        {
            XmlElement objElemTemp = null;
            XmlElement objAutomailMergeList = null;
            string sSQL = string.Empty;
            DbReader objReader = null;

            objAutomailMergeList = (XmlElement)p_objXml.SelectSingleNode("//AutoMailMergeList");

            try
            {
                sSQL = "SELECT AUTO_ROW_ID,AUTO_NAME FROM MERGE_AUTOSET";
                using (objReader = DbFactory.GetDbReader(m_sConnStr, sSQL))
                {
                    while (objReader.Read())
                    {
                        objElemTemp = p_objXml.CreateElement("List");
                        objElemTemp.SetAttribute("RowId", Conversion.ConvertObjToStr(objReader.GetValue("AUTO_ROW_ID")));
                        objElemTemp.SetAttribute("AutoName", Conversion.ConvertObjToStr(objReader.GetValue("AUTO_NAME")));
                        objAutomailMergeList.AppendChild(objElemTemp);
                    }
                }

                objReader.Close();
                return p_objXml;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("AutoMailMerge.Get.Error",m_iClientId), p_objException);//sonali
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                p_objXml = null;
                objElemTemp = null;
                objAutomailMergeList = null;
            }
        }


        /// ************************************************************
        /// <summary>
        /// Deletes a particular Auto Diary Definition
        /// </summary>
        /// <param name="p_objXml">Input xml document</param>
        /// <returns>Returns the list</returns>
        public void Delete(XmlDocument p_objXml)
        {
            int iAutoId = 0;
            DbConnection objConn = null;
            DbTransaction objTran = null;
            DbCommand objCommand = null;
            XmlElement objElm = null;
            try
            {
                objElm = (XmlElement)p_objXml.SelectSingleNode("//AutoMailMergeId");
                if (objElm != null)
                    iAutoId = Conversion.ConvertStrToInteger(objElm.InnerText);
                if (iAutoId > 0)
                {
                    objConn = DbFactory.GetDbConnection(m_sConnStr);
                    objConn.Open();
                    objCommand = objConn.CreateCommand();
                    objTran = objConn.BeginTransaction();
                    objCommand.Transaction = objTran;

                    objCommand.CommandText = "DELETE FROM MERGE_ENTITY_ROLE WHERE AUTO_ID = " + iAutoId;
                    objCommand.ExecuteNonQuery();

                    objCommand.CommandText = "DELETE FROM MERGE_TBR_FILTER WHERE AUTO_ID = " + iAutoId;
                    objCommand.ExecuteNonQuery();

                    objCommand.CommandText = "DELETE FROM MERGE_AUTO_FILTER WHERE AUTO_ID = " + iAutoId;
                    objCommand.ExecuteNonQuery();

                    objCommand.CommandText = "DELETE FROM MERGE_AUTOSET WHERE AUTO_ROW_ID = " + iAutoId;
                    objCommand.ExecuteNonQuery();

                    objTran.Commit();
                }

            }
            catch (Exception p_objEx)
            {
                if (objTran != null)
                    objTran.Rollback();
                throw new RMAppException(Globalization.GetString("AutoMailMerge.Delete.Err",m_iClientId), p_objEx);//sonali
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                if (objTran != null)
                {
                    objTran.Dispose();
                    objTran = null;
                }
                objCommand = null;
            }
        }


        public XmlDocument GetDefinitionList(XmlDocument p_objXmlDoc)
        {
            //XmlDocument objDOM = null;
            XmlElement objElm = null;
            XmlElement objAvlFltrs = null;
            XmlElement objSelFltrs = null;
            XmlElement objSelTempFltrs = null;
            XmlElement objFltrElm = null;
            XmlElement objTempFltrElm = null;

            XmlElement objNewElm = null;
            XmlElement objNewTempElm = null;
            XmlElement objTemplateElm = null;
            XmlElement objTemplateFormIDElm = null;
            DataSet objDs = null;
            DataSet objDsGroups = null;
            int iDefId = 0;
            int iCatId = 0;
            int iTempId = 0;
            int iTemplateFormID = 0;
            string[] arrStrng = null;
            string sSecDsn = string.Empty;


            XmlElement objElementTemp = null;
            string sTempId = string.Empty;
            string sTempName = string.Empty;

            try
            {

                if (!(objInfoSet.Index > 0))
                    LoadInfoDef(p_objXmlDoc, -1);

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AutoMailMergeTemplate/level[@value='" + objInfoSet.Index + "']");

                if (objElm != null)
                {
                    objElementTemp = (XmlElement)p_objXmlDoc.SelectSingleNode("//AutoMailMergeTemplate");
                    objElementTemp.SetAttribute("selectedid", objElm.GetAttribute("value"));
                    iTempId = Conversion.ConvertStrToInteger(objElm.GetAttribute("value"));
                    objElm.SetAttribute("selected", "1");
                }

                objAvlFltrs = (XmlElement)p_objXmlDoc.SelectSingleNode("//Filters/AvlFilters");

                //Populate the available filters
                for (int iCtr = 0; iCtr <= objInfoDef.NumFilters - 1; iCtr++)
                {
                    objNewElm = p_objXmlDoc.CreateElement("level");
                    objNewElm.SetAttribute("templateid", objInfoDef.FilterDef[iCtr].TemplateId.ToString());
                    objNewElm.SetAttribute("id", objInfoDef.FilterDef[iCtr].ID.ToString());
                    objNewElm.SetAttribute("name", objInfoDef.FilterDef[iCtr].Name);
                    objNewElm.SetAttribute("SQLFill", objInfoDef.FilterDef[iCtr].SQLFill);
                    objNewElm.SetAttribute("FilterType", objInfoDef.FilterDef[iCtr].FilterType.ToString());
                    if (objInfoDef.FilterDef[iCtr].DefValue != null)
                        objNewElm.SetAttribute("DefValue", objInfoDef.FilterDef[iCtr].DefValue.ToString());
                    else
                        objNewElm.SetAttribute("DefValue", string.Empty);
                    //rupal:start, R8 Auto Diary Setup Enh
                    if (!string.IsNullOrEmpty(objInfoDef.FilterDef[iCtr].Database))
                        objNewElm.SetAttribute("Database", objInfoDef.FilterDef[iCtr].Database.ToString());
                    else
                        objNewElm.SetAttribute("Database", string.Empty);
                    //rupal:end
                    objAvlFltrs.AppendChild(objNewElm);
                }
                return p_objXmlDoc;
            }

            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AutoMailMerge.Get.Error",m_iClientId), p_objEx);//sonali
            }
            finally
            {
                objElm = null;
                objAvlFltrs = null;
                objSelFltrs = null;
                objFltrElm = null;
                objNewElm = null;
                if (objDs != null)
                {
                    objDs.Dispose();
                    objDs = null;
                }
                if (objDsGroups != null)
                {
                    objDsGroups.Dispose();
                    objDsGroups = null;
                }
            }
        }
    }
}
