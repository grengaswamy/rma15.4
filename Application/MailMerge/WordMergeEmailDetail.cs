﻿

using System;
using System.Text;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Xml;

namespace Riskmaster.Application.MailMerge
{
    /// <summary>
    ///Author  :   Sonam Pahariya
    ///Dated   :   10/01/2012
    ///Purpose :   This class Creates, Edits, Fetches email word merge email details.
    //////************************************************************
    /// Amendment History
    ///************************************************************
    /// Date Amended   *   Amendment   *    Author
    ///************************************************************
    /// </summary>
    public class WordMergeEmailDetail
    {
        #region Variable Declaration
        /// <summary>
        /// Private variable to store connection string
        /// </summary>
        private string m_sConnectionString = "";
        private int m_iClientId = 0;
        #endregion

        #region Constructors
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
        public WordMergeEmailDetail(string p_sConnectionString, int p_iClientId)
		{
			m_sConnectionString = p_sConnectionString;
            m_iClientId = p_iClientId;
		}
		#endregion

        public XmlDocument GetWordMergeEmailDetailList(XmlDocument p_objXmlIn)
        {
            XmlDocument objDom = null;
            StringBuilder sbSQL = new StringBuilder();
            DbReader objReader = null;
            XmlElement objLstRow = null;
            XmlNode objNode = null;
            XmlNodeList objNodeLst = null;
            XmlElement objRowTxt = null;
            XmlElement objElm = null;
            LocalCache objCache = null;
            string sDisplayText = string.Empty; //Pradyumna 11/19/2014 MITS 36930
            try
            {
                objCache = new LocalCache(m_sConnectionString,m_iClientId);
                objNode = p_objXmlIn.SelectSingleNode("//*[name()='MergeEmailDetailList']");
                objNodeLst = objNode.ChildNodes.Item(0).ChildNodes;
                // Pradyumna 11/05/2014 MITS 36930 - Modified Query
                //sbSQL = sbSQL.Append("SELECT EMAIL_ROW_ID, MERGE_FORM.FORM_NAME, EMAIL_FROM, SUBJ_TEXT1, SUBJ_VAR_CODE1, SUBJ_TEXT2, SUBJ_VAR_CODE2, BODY_TEXT1, BODY_VAR_CODE1, BODY_TEXT2, BODY_VAR_CODE2, BODY_TEXT3, MERGE_FORM_EMAIL_DETAIL.FORM_ID ");
                sbSQL = sbSQL.Append("SELECT EMAIL_ROW_ID, MERGE_FORM.FORM_NAME, EMAIL_FROM, EMAIL_SUBJECT_TEXT, EMAIL_BODY_TEXT, MERGE_FORM_EMAIL_DETAIL.FORM_ID ");
                sbSQL = sbSQL.Append(" FROM MERGE_FORM_EMAIL_DETAIL, MERGE_FORM ");
                sbSQL = sbSQL.Append(" WHERE MERGE_FORM.FORM_ID = MERGE_FORM_EMAIL_DETAIL.FORM_ID AND MERGE_FORM.SEND_MAIL_FLAG = '-1' ");

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        objLstRow = p_objXmlIn.CreateElement("listrow");
                        for (int i = 0; i < objNodeLst.Count; i++)
                        {
                            objElm = (XmlElement)objNodeLst[i];
                            objRowTxt = p_objXmlIn.CreateElement(objNodeLst[i].Name);
                            // Pradyumna 11/05/2014 MITS 36930 - Start
                            //if (string.Compare(objNodeLst[i].Name, "SubVar1", true) == 0 || string.Compare(objNodeLst[i].Name, "SubVar2", true) == 0 || string.Compare(objNodeLst[i].Name, "BodyVar1", true) == 0 || string.Compare(objNodeLst[i].Name, "BodyVar2", true) == 0)
                            //    objRowTxt.InnerText = objCache.GetCodeDesc(Conversion.ConvertObjToInt(objReader.GetValue(i)));
                            //else
                            //    objRowTxt.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(i));
                            //objRowTxt.InnerText = Convert.ToString(objReader.GetValue(i));
                            
                            sDisplayText = Convert.ToString(objReader.GetValue(i));

                            if (string.Compare(objNodeLst[i].Name, "SubText", true) == 0 || string.Compare(objNodeLst[i].Name, "BodyText", true) == 0)
                            {
                                if (!String.IsNullOrEmpty(sDisplayText) && CommonFunctions.HTMLCustomDecode(sDisplayText).Length > 250)
                                    sDisplayText = sDisplayText.Substring(0, 250) + "...";
                            }
                            objRowTxt.InnerText = sDisplayText;
                            // Pradyumna 11/05/2014 MITS 36930 - End
                            if (string.Compare(objNodeLst[i].Name, "FormName", true) == 0)
                            {
                                objRowTxt.SetAttribute("FormId", Conversion.ConvertObjToStr(objReader.GetValue("FORM_ID")));
                            }
                            objLstRow.AppendChild(objRowTxt);
                        }
                        objNode.AppendChild((XmlNode)objLstRow);
                    }
                }
                return p_objXmlIn;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WordMergeEmailDetail.GetWordMergeEmailDetailList.Error",m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objDom = null;
            }
        }

        public XmlDocument GetMergeEmailDetail(XmlDocument p_objXmlDocument)
        {
            XmlDocument objDom = null;
            XmlElement objElement = null;
            string sSQL = string.Empty;
            string sValue = string.Empty;
            StringBuilder sbSQL = new StringBuilder();
            DbReader objRead = null;
            XmlDocument objDocEmailVar = null;
            XmlNode objNodeEmailVar = null;
            XmlNode objNode = null;

            try
            {

                objDom = new XmlDocument();

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Template']");

                //query modified by Swati for AIC Gap 8 MITS # 36930
                sbSQL = sbSQL.Append("SELECT MERGE_FORM.FORM_ID, MERGE_FORM.FORM_NAME FROM MERGE_FORM ");
                //sbSQL = sbSQL.Append(" WHERE MERGE_FORM.SEND_MAIL_FLAG = '-1' AND MERGE_FORM.DESIGNATED_RECIPIENT != 0");    
                sbSQL = sbSQL.Append(" WHERE MERGE_FORM.SEND_MAIL_FLAG = '-1' AND EXISTS (SELECT * FROM FORM_RECIPIENT WHERE MERGE_FORM.FORM_ID = FORM_RECIPIENT.FORM_ID)");
                                
                objRead = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objRead != null)
                {
                    while (objRead.Read())
                    {
                        objElement = p_objXmlDocument.CreateElement("Templates");
                        objElement.SetAttribute("tempid", Conversion.ConvertObjToStr(objRead.GetValue("FORM_ID")));
                        objElement.SetAttribute("tempname", Conversion.ConvertObjToStr(objRead.GetValue("FORM_NAME")));
                        objNode.AppendChild(objElement);
                    }
                }
                objRead.Close();

                objDocEmailVar = GetEmailVariable();
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='SubVar1']");
                if (objNode != null)
                {
                    objNodeEmailVar = p_objXmlDocument.ImportNode(objDocEmailVar.DocumentElement, true);
                    objNode.AppendChild(objNodeEmailVar);
                }
                // Pradyumna 11/05/2014 MITS 36930 - Commented - Start
                //objNode = p_objXmlDocument.SelectSingleNode("//control[@name='SubVar2']");
                //if (objNode != null)
                //{
                //    objNodeEmailVar = p_objXmlDocument.ImportNode(objDocEmailVar.DocumentElement, true);
                //    objNode.AppendChild(objNodeEmailVar);
                //}
                // Pradyumna 11/05/2014 MITS 36930 - End
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='BodyVar1']");
                if (objNode != null)
                {
                    objNodeEmailVar = p_objXmlDocument.ImportNode(objDocEmailVar.DocumentElement, true);
                    objNode.AppendChild(objNodeEmailVar);
                }
                // Pradyumna 11/05/2014 MITS 36930 - Commented - Start
                //objNode = p_objXmlDocument.SelectSingleNode("//control[@name='BodyVar2']");
                //if (objNode != null)
                //{
                //    objNodeEmailVar = p_objXmlDocument.ImportNode(objDocEmailVar.DocumentElement, true);
                //    objNode.AppendChild(objNodeEmailVar);
                //}
                // Pradyumna 11/05/2014 MITS 36930 - End

                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='MergeEmailDetailID']");
                if (objElement != null)
                {
                    if (objElement.InnerText != null && objElement.InnerText.Trim() != "")
                    {
                        sbSQL = new StringBuilder();
                        // Modified by Pradyumna 10/13/2014 MITS 36930
                        // sbSQL = sbSQL.Append("SELECT EMAIL_ROW_ID, MERGE_FORM_EMAIL_DETAIL.FORM_ID, EMAIL_FROM, SUBJ_TEXT1, SUBJ_VAR_CODE1, SUBJ_TEXT2, SUBJ_VAR_CODE2, BODY_TEXT1, BODY_VAR_CODE1, BODY_TEXT2, BODY_VAR_CODE2, BODY_TEXT3 ");
                        sbSQL = sbSQL.Append("SELECT EMAIL_ROW_ID, MERGE_FORM_EMAIL_DETAIL.SUBJ_TEXT1, MERGE_FORM_EMAIL_DETAIL.BODY_TEXT1, MERGE_FORM_EMAIL_DETAIL.FORM_ID, EMAIL_FROM, EMAIL_SUBJECT_TEXT, EMAIL_BODY_TEXT");
                        sbSQL = sbSQL.Append(" FROM MERGE_FORM_EMAIL_DETAIL, MERGE_FORM ");
                        sbSQL = sbSQL.Append(" WHERE EMAIL_ROW_ID = " + objElement.InnerText.Trim() + " AND MERGE_FORM_EMAIL_DETAIL.FORM_ID = MERGE_FORM.FORM_ID AND MERGE_FORM.SEND_MAIL_FLAG = '-1' ");
                        objRead = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                        while (objRead.Read())
                        {
                            objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Template']");
                            foreach (XmlElement xmlTempElem in objElement.ChildNodes)
                            {
                                sValue = Conversion.ConvertObjToStr(objRead.GetValue("FORM_ID"));
                                if (string.Compare(xmlTempElem.GetAttribute("tempid").ToString(), sValue, true) == 0)
                                {
                                    xmlTempElem.SetAttribute("Selected", "true");
                                }
                            }
                            objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='EmailFrom']");
                            objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("EMAIL_FROM"));

                            objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SubText1']");
                            objElement.InnerText = CommonFunctions.HTMLCustomDecode(Convert.ToString(objRead.GetValue("EMAIL_SUBJECT_TEXT")));

                            // Pradyumna 10/10/2014 MITS 36930 - Commented
                            //objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SubVar1']//Variable");
                            //foreach (XmlElement xmlTempElem in objElement.ChildNodes)
                            //{
                            //    sValue = Conversion.ConvertObjToStr(objRead.GetValue("SUBJ_VAR_CODE1"));
                            //    // Pradyumna 10/10/2014 MITS 36930 - Replace attribute Name from varid to varcode
                            //    if (string.Compare(xmlTempElem.GetAttribute("varcode").ToString(), sValue, true) == 0)
                            //    {
                            //        xmlTempElem.SetAttribute("Selected", "true");
                            //    }
                            //}

                            //objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SubText2']");
                            //objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("SUBJ_TEXT2"));

                            //objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SubVar2']//Variable");
                            //foreach (XmlElement xmlTempElem in objElement.ChildNodes)
                            //{
                            //    sValue = Conversion.ConvertObjToStr(objRead.GetValue("SUBJ_VAR_CODE2"));
                            //    if (string.Compare(xmlTempElem.GetAttribute("varid").ToString(), sValue, true) == 0)
                            //    {
                            //        xmlTempElem.SetAttribute("Selected", "true");
                            //    }
                            //}

                            objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='BodyText1']");
                            objElement.InnerText = CommonFunctions.HTMLCustomDecode(Convert.ToString(objRead.GetValue("EMAIL_BODY_TEXT")));

                            // Pradyumna 10/10/2014 MITS 36930 - Commented
                            //objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='BodyVar1']//Variable");
                            //foreach (XmlElement xmlTempElem in objElement.ChildNodes)
                            //{
                            //    sValue = Conversion.ConvertObjToStr(objRead.GetValue("BODY_VAR_CODE1"));
                            //    // Pradyumna 10/10/2014 MITS 36930 - Replace attribute Name from varid to varcode
                            //    if (string.Compare(xmlTempElem.GetAttribute("varcode").ToString(), sValue, true) == 0)
                            //    {
                            //        xmlTempElem.SetAttribute("Selected", "true");
                            //    }
                            //}

                            //objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='BodyText2']");
                            //objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("BODY_TEXT2"));

                            //objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='BodyVar2']//Variable");
                            //foreach (XmlElement xmlTempElem in objElement.ChildNodes)
                            //{
                            //    sValue = Conversion.ConvertObjToStr(objRead.GetValue("BODY_VAR_CODE2"));
                            //    if (string.Compare(xmlTempElem.GetAttribute("varid").ToString(), sValue, true) == 0)
                            //    {
                            //        xmlTempElem.SetAttribute("Selected", "true");
                            //    }
                            //}

                            //objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='BodyText3']");
                            //objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("BODY_TEXT3"));
                        }
                        objRead.Close();
                    }
                }


                return p_objXmlDocument;
            }
            catch (XmlException p_objException)
            {
                throw new RMAppException(Globalization.GetString("WordMergeEmailDetail.GetMergeEmailDetail.XmlError",m_iClientId), p_objException);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WordMergeEmailDetail.GetMergeEmailDetail.Error",m_iClientId), p_objException);
            }
            finally
            {
                if (objRead != null)
                {
                    objRead.Close();
                    objRead.Dispose();
                }
                objDom = null;
            }
        }

        private XmlDocument GetEmailVariable()
        {
            XmlDocument objDOM = null;
            XmlElement objElemVar = null;
            XmlElement objElemTemp = null;
            string sSQL = string.Empty;
            string sTableId = string.Empty;
            DbReader objReader = null;
            try
            {
                objDOM = new XmlDocument();
                objElemVar = objDOM.CreateElement("Variable");
                objDOM.AppendChild(objElemVar);
                // Pradyumna 10/10/2014 MITS 36930 - Modified query
                //sSQL = "SELECT C.CODE_ID, CT.CODE_DESC FROM CODES C, CODES_TEXT CT WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'MERGE_FORM_EMAIL_DICTIONARY') ORDER BY CT.CODE_DESC";
                sSQL = "SELECT C.SHORT_CODE, CT.CODE_DESC FROM CODES C, CODES_TEXT CT WHERE C.CODE_ID = CT.CODE_ID AND C.TABLE_ID = (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'MERGE_FORM_EMAIL_DICTIONARY') ORDER BY CT.CODE_DESC";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                while (objReader.Read())
                {
                    objElemTemp = objDOM.CreateElement("Variables");
                    //Pradyumna 10/10/2014 MITS 36930 - Replace varid with varcode and value from Code_ID to SHORT_CODE
                    objElemTemp.SetAttribute("varcode", Conversion.ConvertObjToStr(objReader.GetValue("SHORT_CODE")));
                    objElemTemp.SetAttribute("vardesc", Conversion.ConvertObjToStr(objReader.GetValue("CODE_DESC")));
                    objDOM.FirstChild.AppendChild(objElemTemp);
                }
                objReader.Close();

                return objDOM;
            }            
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WordMergeEmailDetail.GetEmailVariable.Error",m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objDOM = null;
            }
        }


        public XmlDocument SaveMergeEmailDetail(XmlDocument p_objXmlDocument)
        {
            XmlElement objElm = null;
            int iNextUID = 0;
            string sRowId = string.Empty;
            string sTemplateID = string.Empty;
            string sEmailFrom = string.Empty;
            // Pradyumna 11/06/2014 MITS 36930 - Start
            string sSubText = string.Empty;
            string sBodyText = string.Empty;
            //string sSubText1 = string.Empty;
            //string sSubVar1 = string.Empty;
            //string sSubText2 = string.Empty;
            //string sSubVar2 = string.Empty;
            //string sBodyText1 = string.Empty;
            //string sBodyVar1 = string.Empty;
            //string sBodyText2 = string.Empty;
            //string sBodyVar2 = string.Empty;
            //string sBodyText3 = string.Empty;
            // Pradyumna 11/06/2014 MITS 36930 - End
            string sSQL = string.Empty;
            StringBuilder sbSql = new StringBuilder();
            DbConnection objConn = null;

            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='MergeEmailDetailID']");
                if (objElm != null)
                    sRowId = objElm.InnerText.Trim();

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Template']");
                if (objElm != null)
                    sTemplateID = objElm.InnerText.Trim();

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='EmailFrom']");
                if (objElm != null)
                    sEmailFrom = objElm.InnerText.Trim();
                // Pradyumna 11/06/2014 MITS 36930 - Start
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SubText1']");
                if (objElm != null)
                    sSubText = CommonFunctions.HTMLCustomEncode(objElm.InnerText.Trim());

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='BodyText1']");
                if (objElm != null)
                    sBodyText = CommonFunctions.HTMLCustomEncode(objElm.InnerText.Trim());

                //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SubText1']");
                //if (objElm != null)
                //    sSubText1 = objElm.InnerText;

                //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SubVar1']");
                //if (objElm != null)
                //    sSubVar1 = objElm.InnerText;

                //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SubText2']");
                //if (objElm != null)
                //    sSubText2 = objElm.InnerText;

                //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SubVar2']");
                //if (objElm != null)
                //    sSubVar2 = objElm.InnerText;

                //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='BodyText1']");
                //if (objElm != null)
                //    sBodyText1 = objElm.InnerText;

                //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='BodyVar1']");
                //if (objElm != null)
                //    sBodyVar1 = objElm.InnerText;

                //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='BodyText2']");
                //if (objElm != null)
                //    sBodyText2 = objElm.InnerText;

                //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='BodyVar2']");
                //if (objElm != null)
                //    sBodyVar2 = objElm.InnerText;

                //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='BodyText3']");
                //if (objElm != null)
                //    sBodyText3 = objElm.InnerText;

                if (sRowId != "")
                {
                    //UPDATE MERGE_EMAIL_DETAIL SET EMAIL_FROM, SUBJ_TEXT1, SUBJ_VAR_CODE1, SUBJ_TEXT2, SUBJ_VAR_CODE2, BODY_TEXT1, BODY_VAR_CODE1, BODY_TEXT2, BODY_VAR_CODE2, BODY_TEXT3
                    sbSql = sbSql.Append("UPDATE MERGE_FORM_EMAIL_DETAIL SET FORM_ID = " + sTemplateID + ", ");
                    sbSql = sbSql.Append("EMAIL_FROM = '" + sEmailFrom + "', EMAIL_SUBJECT_TEXT = '" + sSubText + "', EMAIL_BODY_TEXT = '" + sBodyText + "' ");
                    //sbSql = sbSql.Append("EMAIL_FROM = '" + sEmailFrom + "', SUBJ_TEXT1 = '" + sSubText1 + "', ");
                    //sbSql = sbSql.Append("SUBJ_VAR_CODE1 = '" + sSubVar1 + "', SUBJ_TEXT2 = '" + sSubText2 + "', ");
                    //sbSql = sbSql.Append("SUBJ_VAR_CODE2 = '" + sSubVar2 + "', BODY_TEXT1 = '" + sBodyText1 + "', ");
                    //sbSql = sbSql.Append("BODY_VAR_CODE1 = '" + sBodyVar1 + "', BODY_TEXT2 = '" + sBodyText2 + "', ");
                    //sbSql = sbSql.Append("BODY_VAR_CODE2 = '" + sBodyVar2 + "', BODY_TEXT3 = '" + sBodyText3 + "' ");
                    sbSql = sbSql.Append("WHERE EMAIL_ROW_ID = " + sRowId);

                }
                else
                {

                    iNextUID = Utilities.GetNextUID(m_sConnectionString, "MERGE_FORM_EMAIL_DETAIL",m_iClientId);
                    sbSql = sbSql.Append("INSERT INTO MERGE_FORM_EMAIL_DETAIL ");
                    //sbSql = sbSql.Append("(EMAIL_ROW_ID, FORM_ID, EMAIL_FROM, SUBJ_TEXT1, SUBJ_VAR_CODE1, SUBJ_TEXT2, SUBJ_VAR_CODE2, BODY_TEXT1, BODY_VAR_CODE1, BODY_TEXT2, BODY_VAR_CODE2, BODY_TEXT3)");
                    sbSql = sbSql.Append("(EMAIL_ROW_ID, FORM_ID, EMAIL_FROM, EMAIL_SUBJECT_TEXT, EMAIL_BODY_TEXT)");
                    sbSql = sbSql.Append(" VALUES(");
                    sbSql = sbSql.Append(iNextUID.ToString() + "," + sTemplateID);
                    sbSql = sbSql.Append(", '" + sEmailFrom + "', '" + sSubText + "', '" + sBodyText + "')");
                    //sbSql = sbSql.Append(", '" + sEmailFrom + "', '" + sSubText1 + "'");
                    //sbSql = sbSql.Append(", '" + sSubVar1 + "', '" + sSubText2 + "'");
                    //sbSql = sbSql.Append(", '" + sSubVar2 + "', '" + sBodyText1 + "'");
                    //sbSql = sbSql.Append(", '" + sBodyVar1 + "', '" + sBodyText2 + "'");
                    //sbSql = sbSql.Append(", '" + sBodyVar2 + "', '" + sBodyText3 + "')");
                }
                // Pradyumna 11/06/2014 MITS 36930 - End
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sbSql.ToString());

                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WordMergeEmailDetail.SaveMergeEmailDetail.Error",m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                sbSql = null;
            }
        }

        public XmlDocument DeleteWordMergeEmailDetail(XmlDocument p_objXmlDocument)
        {
            string sRowId = string.Empty;
            XmlElement objElm = null;
            DbConnection objConn = null;
            string sSQL = string.Empty;
            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='MergeEmailDetailID']");
                if (objElm != null)
                    sRowId = objElm.InnerText;

                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();

                if (sRowId != "")
                {
                    sSQL = "DELETE FROM MERGE_FORM_EMAIL_DETAIL WHERE EMAIL_ROW_ID =" + sRowId;
                }
                objConn.ExecuteNonQuery(sSQL);
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WordMergeEmailDetail.DeleteWordMergeEmailDetail.Error",m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }

    }
}
