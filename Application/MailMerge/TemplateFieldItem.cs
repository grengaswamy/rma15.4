﻿
using System;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Common;
using Riskmaster.Db;
namespace Riskmaster.Application.MailMerge
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   29th November 2004
	///Purpose :   This class Creates, Edits, Fetches template field information.
	/// </summary>
	internal class TemplateFieldItem
	{
		#region Variable Declaration
        private int m_iClientId = 0;
		/// <summary>
		/// Field id
		/// </summary>
		private long m_lFieldId=0; 
		/// <summary>
		/// Field name
		/// </summary>
		private string m_sFieldName=""; 
		/// <summary>
		/// Field description
		/// </summary>
		private string m_sFieldDesc=""; 
		/// <summary>
		/// Display category
		/// </summary>
		private string m_sDisplayCat=""; 
		/// <summary>
		/// Field table
		/// </summary>
		private string m_sFieldTable=""; 
		/// <summary>
		/// Pattern
		/// </summary>
		private string m_sOptMask=""; 
		/// <summary>
		/// Code table
		/// </summary>
		private string m_sCodeTable=""; 
		/// <summary>
		/// Field type
		/// </summary>
		private long m_lFieldType=0; 
		/// <summary>
		/// Supplemental field or not
		/// </summary>
		private bool m_bIsSupp=false; 
		/// <summary>
		/// Long code flag
		/// </summary>
		private bool m_bLongCodeFlag=false; 

		private string m_sParam=""; // Used for res totals and pay totals where a parameter is dynamically allocated   JP 09.08.2006

		#endregion

		#region Property Declaration
		/// <summary>
		/// Field id
		/// </summary>
		public long FieldId
		{
			get
			{
				return m_lFieldId;
			}
			set
			{
				m_lFieldId=value;
			}
		}
		/// <summary>
		/// Long code flag
		/// </summary>
		public bool LongCodeFlag
		{
			get
			{
				return m_bLongCodeFlag;
			}
			set
			{
				m_bLongCodeFlag=value;
			}
		}
		/// <summary>
		/// Supplemental field or not
		/// </summary>
		public bool  IsSupp
		{
			get
			{
				return m_bIsSupp;
			}
			set
			{
				m_bIsSupp=value;
			}
		}
		/// <summary>
		/// Field type
		/// </summary>
		public long  FieldType
		{
			get
			{
				return m_lFieldType;
			}
			set
			{
				m_lFieldType=value;
			}
		}
		/// <summary>
		/// Code table
		/// </summary>
		public string CodeTable
		{
			get
			{
				return m_sCodeTable;
			}
			set
			{
				m_sCodeTable=value;
			}
		}
		/// <summary>
		/// Pattern
		/// </summary>
		public string OptMask
		{
			get
			{
				return m_sOptMask;
			}
			set
			{
				m_sOptMask=value;
			}
		}
		/// <summary>
		/// Field table
		/// </summary>
		public string FieldTable
		{
			get
			{
				return m_sFieldTable;
			}
			set
			{
				m_sFieldTable=value;
			}
		}
		/// <summary>
		/// Display category
		/// </summary>
		public string DisplayCat
		{
			get
			{
				return m_sDisplayCat;
			}
			set
			{
				m_sDisplayCat=value;
			}
		}
		/// <summary>
		/// Field description
		/// </summary>
		public string FieldDesc
		{
			get
			{
				return m_sFieldDesc;
			}
			set
			{
				m_sFieldDesc=value;
			}
		}
		/// <summary>
		/// Field name
		/// </summary>
		public string FieldName
		{
			get
			{
				return m_sFieldName;
			}
			set
			{
				m_sFieldName=value;
			}
		}
		#endregion

		#region TemplateFieldItem class constructor
		/// <summary>
		/// TemplateFieldItem class constructor
		/// </summary>
        public TemplateFieldItem(int p_iClientId) { m_iClientId = p_iClientId; }
		#endregion

		#region Functions used while setting property value
		public string Param
		{
			get
			{
				return m_sParam;
			}
			set
			{
				m_sParam=value;
			}
		}
		/// Name		: ExtendFieldName
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Extends field name based on field type ,long code flag and supplemental field flag
		/// </summary>
		public void ExtendFieldName() 
		{ 
			string sTmp=""; 
			bool bSuppFieldFlag=false; 
			long lFieldType=0; 
			bool lLongCodeFlag=false; 
			try
			{

				bSuppFieldFlag = this.IsSupp; 
				lFieldType = this.FieldType; 
				lLongCodeFlag = this.LongCodeFlag; 
				if (!bSuppFieldFlag) 
				{ 
					if ((lFieldType == 3) & !lLongCodeFlag)
					{
						sTmp=" (Code)";
					}
					if ((lFieldType == 3) & lLongCodeFlag)
					{
						sTmp=" (Desc)";
					}
					if ((lFieldType == 6) & !lLongCodeFlag)
					{
						sTmp=" (Short)";
					}
					if ((lFieldType == 6) & lLongCodeFlag)
					{
						sTmp=" (Long)";
					}
				} 
				else 
				{ 
					if ((lFieldType == 6) & !lLongCodeFlag)
					{
						sTmp=" (Code)";
					}
					if ((lFieldType == 6) & lLongCodeFlag)
					{
						sTmp=" (Desc)";
					}
					if ((lFieldType == 9) & !lLongCodeFlag)
					{
						sTmp=" (Short)";
					}
					if ((lFieldType == 9) & lLongCodeFlag)
					{
						sTmp=" (Long)";
					}
				} 
				if (sTmp != "") 
				{ 
					this.FieldName = this.FieldName + sTmp; 
				} 
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateFieldItem.ExtendFieldName.Error",m_iClientId),p_objException);
			}
			finally
			{
				sTmp=null;
			}
		}
		#endregion
	}

}
