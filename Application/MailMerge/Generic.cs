using System;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using System.IO;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Threading;
using System.Text.RegularExpressions;
using Riskmaster.Settings;//Deb MITS 25775
//using MergeLib;



namespace Riskmaster.Application.MailMerge
{
	#region VehicleTempTable Structure  
	/// <summary>
	/// This structure stores information related to Vehicle merge
	/// </summary>
	public struct VehicleTempTable
	{
		/// <summary>
		/// Table name
		/// </summary>
		public string TableName;
		/// <summary>
		/// Display fields
		/// </summary>
		public string DisplayFields;
		/// <summary>
		/// Tables
		/// </summary>
		public string FromTables;
		/// <summary>
		/// Where clause
		/// </summary>
		public string QueryWhere;
		/// <summary>
		/// From clause
		/// </summary>
		public string ChainFrom;
		/// <summary>
		/// Where clause
		/// </summary>
		public string ChainWhere;
	}
	#endregion
    #region PropertyTempTable Structure
    //Anu Tennyson for MITS 18291 : BEGINS 10/29/2009
    /// <summary>
    /// This structure stores information related to Property merge
    /// </summary>
    public struct PropertyTempTable
    {
        /// <summary>
        /// Table name
        /// </summary>
        public string TableName;
        /// <summary>
        /// Display fields
        /// </summary>
        public string DisplayFields;
        /// <summary>
        /// Tables
        /// </summary>
        public string FromTables;
        /// <summary>
        /// Where clause
        /// </summary>
        public string QueryWhere;
        /// <summary>
        /// From clause
        /// </summary>
        public string ChainFrom;
        /// <summary>
        /// Where clause
        /// </summary>
        public string ChainWhere;
    }
    //Anu Tennyson for MITS 18291 : ENDS
	#endregion
	/////<summary>
	///Author  :   Parag Sarin
	///Dated   :   29th November 2004
	///Purpose :   This class contains generic functions to be used in mail merge module.
	/// </summary>
	public class Generic
	{
		#region Variable Declaration
		/// <summary>
		/// DB type i.e. ORACLE,SQL SERVER
		/// </summary>
		private long m_lDbMake=0;
        private int m_iClientId = 0;
		/// <summary>
		/// Manager class object 
		/// </summary>
		private IManager m_objManager=null;
		/// <summary>
		/// Connection string
		/// </summary>
		private string  m_sConnectionString="";
        SysSettings objSysSettingsGlobal = null;//Deb MITS 25775
        private string m_sTablesNameUniquePart = "";    //tkatsarski: 03/13/15 - RMA-1768
        #endregion

		#region Property Declaration
		/// <summary>
		/// Manager class object
		/// </summary>
		public IManager Manager
		{
			set
			{
				m_objManager=value;
			}
			get
			{
				return m_objManager;
			}
		}
		/// <summary>
		/// DB type i.e. ORACLE,SQL SERVER
		/// </summary>
		public long DbMake
		{
			set
			{
				m_lDbMake=value;
			}
			get
			{
				return m_lDbMake;
			}
		}
        //tkatsarski: 03/13/15 Start changes for RMA-1768
        /// <summary>
        /// Stores an unique postfix for the SafeSQL temporary tables
        /// </summary>
        public string TablesNameUniquePart
        {
            set
            {
                m_sTablesNameUniquePart = value;
            }
            get
            {
                return m_sTablesNameUniquePart;
            }
        }
        //tkatsarski: 03/13/15 End changes for RMA-1768
        public SysSettings SysSettingsGlobal { get { return objSysSettingsGlobal; } set { objSysSettingsGlobal = value; } }//Deb MITS 25775
		#endregion

		#region Generic class constructor
		/// <summary>
		/// Generic class constructor
		/// </summary>
        public Generic(int iClientId = 0) { m_iClientId = iClientId; }
		#endregion

		#region Function to set properties
		/// Name		: Setproperty
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function sets the properties of Generic class
		/// </summary>
		/// <param name="p_lDbMake">DB type i.e ORACLE,SQL SERVER</param>
		/// <param name="p_sConnectionString">Connection string</param>
		internal void Setproperty(long p_lDbMake,string p_sConnectionString)
		{
			m_lDbMake=p_lDbMake;
			m_sConnectionString=p_sConnectionString;
            objSysSettingsGlobal = new SysSettings(m_sConnectionString,m_iClientId);//Deb MITS 25775
		}
		#endregion

		#region Common Functions used to merge data
        // dbisht6 for RMA-8466 this function will remove the nullable constraint of temp table generated during mail merge process for ORAACLE database only
        //this funtion should be used only for ORACLE database as oracle datatype does not allow " " value in the column having not null property 
        //and we can not replace the blank value with any default value because we can not define default values for varchar2 , datetime etc datatypes
        // SQL server accepts blank value for not null column but ORACLE does not hence we will only drop the nullable constraint for ORACLE database
        internal void DropTempTableNULLConstraintsOracle(string tablename)
        {
            //String sSqlAlter = "ALTER TABLE "+tablename +"_"+ m_manager.UID.ToString() + m_sTablesNameUniquePart+" MODIFY ";
            DbConnection m_objMergeConn = null;

            int i = 0;
            DataSet objDataSet = null;
            m_objMergeConn = DbFactory.GetDbConnection(m_sConnectionString);
            m_objMergeConn.Open();
            if (tablename.Contains("["))
            {
                tablename = tablename.Replace("[", "");
            }
            if (tablename.Contains("]"))
            {
                tablename = tablename.Replace("]", "");

            }
            String sSqlRead = @"SELECT COLUMN_NAME FROM USER_TAB_COLUMNS WHERE TABLE_NAME ='" + tablename + "_" + Manager.UID.ToString() + m_sTablesNameUniquePart + "' AND NULLABLE='N'";
            try
            {
                objDataSet = DbFactory.GetDataSet(m_sConnectionString, sSqlRead, m_iClientId);

                foreach (DataRow objRow in objDataSet.Tables[0].Rows)
                {
                    if (!String.IsNullOrEmpty(objRow[i].ToString()))
                    {
                        m_objMergeConn.ExecuteNonQuery("ALTER TABLE " + tablename + "_" + Manager.UID.ToString() + m_sTablesNameUniquePart + " MODIFY (" + objRow[i].ToString() + " null )");
                    }

                }
                m_objMergeConn.Close();
                m_objMergeConn.Dispose();
                objDataSet.Clear();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objDataSet.Clear();
                objDataSet = null;
                m_objMergeConn.Close();
                m_objMergeConn.Dispose();
            }

        }

		/// Name		: FindBitFields
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function finds bit type fields in a table
		/// </summary>
		/// <param name="p_sTable">Table name from which to find bit type field</param>
		/// <param name="p_sBitFieldNames">Field names</param>
		/// <param name="p_sBitFieldValues">Field values</param>
		internal void FindBitFields(string p_sTable,ref string p_sBitFieldNames,ref string p_sBitFieldValues)
		{
			DbReader objReader=null;
			string sType="";
			string sSQL="";
			try
			{
				p_sBitFieldNames = "";
				p_sBitFieldValues = "";
				sSQL="SELECT * FROM "+p_sTable;
                
                if (DbFactory.GetDatabaseType(m_sConnectionString) == eDatabaseType.DBMS_IS_ORACLE)
                {
                    DropTempTableNULLConstraintsOracle(p_sTable);
                }
                
				objReader=DbFactory.GetDbReader(m_sConnectionString,SafeSQL(sSQL));
                DataTable dt = objReader.GetSchemaTable();
				for (int i=0;i<objReader.FieldCount;i++)
				{
					sType=objReader.GetDataTypeName(i);
					if (sType.ToUpper().Equals("BIT"))
					{
						p_sBitFieldNames = p_sBitFieldNames + "," + objReader.GetName(i);
						p_sBitFieldValues = p_sBitFieldValues + "," +"0";
					}
                   
                    else if (sType.ToUpper().Equals("INT") || sType.ToUpper().Equals("FLOAT") || sType.ToUpper().Equals("INT16") || (sType.ToUpper().Equals("INT32")) || (sType.ToUpper().Equals("INT64"))
                               || (sType.ToUpper().Equals("DECIMAL")) || (sType.ToUpper().Equals("DOUBLE")))
                    {
                        p_sBitFieldNames = p_sBitFieldNames + "," + objReader.GetName(i);
                        p_sBitFieldValues = p_sBitFieldValues + "," + "0";
                    }
                    else
                    {
                        p_sBitFieldNames = p_sBitFieldNames + "," + objReader.GetName(i);
                        p_sBitFieldValues = p_sBitFieldValues + "," + "''";
                    }
                   
				}
				objReader.Close();
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Generic.FindBitFields.Error", m_iClientId),p_objException);
			}
			finally
			{
				if (objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				objReader=null;
				sType=null;
				sSQL=null;
			}

		}
        ////asharma326 MITS:29423 Starts
        internal void FindBitFields(string p_sTable, ref string p_sBitFieldNames, ref string p_sBitFieldValues, bool p_flag)
        {
            DbReader objReader = null;
            string sType = "";
            string sSQL = "";
            try
            {
                p_sBitFieldNames = "";
                p_sBitFieldValues = "";
                sSQL = "SELECT * FROM " + p_sTable;
                //dbisht6 JIRA RMA-8466
                if (DbFactory.GetDatabaseType(m_sConnectionString) == eDatabaseType.DBMS_IS_ORACLE)
                {
                    DropTempTableNULLConstraintsOracle(p_sTable);
                }
                //dbisht6 end
                
                objReader = DbFactory.GetDbReader(m_sConnectionString, SafeSQL(sSQL));
                for (int i = 0; i < objReader.FieldCount; i++)
                {
                    sType = objReader.GetDataTypeName(i);
                    if (sType.ToUpper().Equals("BIT"))
                    {
                        p_sBitFieldNames = p_sBitFieldNames + "," + objReader.GetName(i);
                        p_sBitFieldValues = p_sBitFieldValues + "," + "0";
                    }
                    else
                    {
                        //dbisht6 jira RMA-8466
                        if (sType.ToUpper().Equals("INT") || sType.ToUpper().Equals("FLOAT")||sType.ToUpper().Equals("INT16") || (sType.ToUpper().Equals("INT32")) || (sType.ToUpper().Equals("INT64"))
                            || (sType.ToUpper().Equals("DECIMAL")) || (sType.ToUpper().Equals("DOUBLE")))
                        {
                            p_sBitFieldNames = p_sBitFieldNames + "," + objReader.GetName(i);
                            p_sBitFieldValues = p_sBitFieldValues + "," + "0";
                        }
                        //dbisht6 end
                        else
                        {
                            p_sBitFieldNames = p_sBitFieldNames + "," + objReader.GetName(i);
                            p_sBitFieldValues = p_sBitFieldValues + "," + "''";
                        }
                    }
                }
                objReader.Close();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Generic.FindBitFields.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objReader = null;
                sType = null;
                sSQL = null;
            }

        }
        ////asharma326 MITS:29423 ENDS


        //Created by - pmittal5 Mits 18599 12/01/09 - All fields in the table are added in Insert query, since some fields cannot be set to Null while using Insert query
        internal void FindFields(string p_sTable, ref string p_sNames, ref string p_sValues)
        {
            DbReader objReader = null;
            string sType = string.Empty;
            string sSQL = string.Empty;
            try
            {

                sSQL = "SELECT * FROM " + p_sTable;
                objReader = DbFactory.GetDbReader(m_sConnectionString, SafeSQL(sSQL));
                for (int i = 1; i < objReader.FieldCount; i++)  //ID fields are skipped
                { 
                    sType = objReader.GetDataTypeName(i);
                    switch(m_lDbMake)
                    {
                        case (int) eDatabaseType.DBMS_IS_SQLSRVR:
                            if (sType.ToUpper().IndexOf("CHAR") != -1 || sType.ToUpper().IndexOf("TEXT") != -1)
                            {
                                p_sValues += ",''";
                                p_sNames += "," + objReader.GetName(i);
                            }
                            else
                            {
                                p_sValues += ",0";
                                p_sNames += "," + objReader.GetName(i);
                            }
                            break;
                        case (int) eDatabaseType.DBMS_IS_ORACLE:
                            if (sType.ToUpper().IndexOf("CHAR") != -1 || sType.ToUpper().IndexOf("LOB") != -1)
                            {
                                p_sValues += ",''";
                                p_sNames += "," + objReader.GetName(i);
                            }
                            else
                            {
                                p_sValues += ",0";
                                p_sNames += "," + objReader.GetName(i);
                            }
                            break;
                    }
                }
                objReader.Close();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Generic.FindFields.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objReader = null;
                sType = null;
                sSQL = null;
            }

        }
		/// Name		: MergeDupCleanup
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes the temporary merge tables
		/// </summary>
		/// <param name="p_objMergeCmd">Command object</param>
		internal void MergeDupCleanup(DbCommand p_objMergeCmd)
		{
			try
			{
				DropMTable (p_objMergeCmd,"[MRG_TMP]");
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Generic.MergeDupCleanup.Error", m_iClientId), p_objException);
			}
		}
		/// Name		: MergeDupTable
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Merges the data one table to another
		/// </summary>
		/// <param name="p_objMergeCmd">Command object</param>
		/// <param name="p_sTable">Table for which to merge data</param>
		internal void MergeDupTable(DbCommand p_objMergeCmd,string p_sTable)
		{
			string sSQL="";
			try
			{
				DropMTable (p_objMergeCmd,"[MRG_TMP]");					
				sSQL="SELECT * [INTO MRG_TMP] FROM [" +p_sTable + "]";
				p_objMergeCmd.CommandType=CommandType.Text;
				p_objMergeCmd.CommandText=SafeSQL(sSQL);
				p_objMergeCmd.ExecuteNonQuery();
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Generic.MergeDupTable.Error", m_iClientId), p_objException);
			}
			finally
			{
				sSQL=null;
			}
			
		}
		/// Name		: AddUnique
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Add a value in an array if it doesn't exists previously
		/// </summary>
		/// <param name="p_obj">Array</param>
		/// <param name="p_sValue">value to insert</param>
		internal static void AddUnique(ArrayList p_arrlstTemp,string p_sValue)
		{
			for (int i=0;i<p_arrlstTemp.Count;i++)
			{
				if (p_arrlstTemp[i].ToString()==p_sValue)
				{
					return;
				}
			}
			p_arrlstTemp.Add(p_sValue);

		}
		/// Name		: GetDataset
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function creates a DATASET
		/// </summary>
		/// <param name="p_objMergeConn">Connection object</param>
		/// <param name="p_sSQL">Sql query</param>
		/// <param name="p_objds">Dataset object</param>
		internal void GetDataset(DbConnection p_objMergeConn,string p_sSQL,ref DataSet p_objds)
		{
			DbDataAdapter objAdapter=null;
			try
			{
				objAdapter=DbFactory.GetDataAdapter(p_objMergeConn,SafeSQL(p_sSQL));
				p_objds=new DataSet();
				objAdapter.Fill(p_objds);
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Generic.GetDataset.Error", m_iClientId), p_objException);
			}
			finally
			{
				objAdapter=null;
			}
		}
		/// Name		: DeleteOracleTables
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function deletes ORACLE tables
		/// </summary>
		/// <param name="p_sSql">Sql query</param>
		/// <param name="p_sTableName">Table name</param>
		internal void DeleteOracleTables(string p_sSql,string p_sTableName)
		{
			DbConnection objConn=null;
			try
			{
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				objConn.ExecuteNonQuery (p_sSql);
				objConn.Close();
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				//Logger to be used
			}
			finally
			{
				if (objConn != null)
				{
					objConn.Close();
					objConn.Dispose();
				}
			}
		}
		/// Name		: GetSingleLong
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns long value returned from an sql query
		/// </summary>
		/// <param name="p_sFieldName">Field name</param>
		/// <param name="p_sTableName">Table name</param>
		/// <param name="p_sCriteria">Criteria</param>
		/// <param name="p_sConnectString">Connection string</param>
		/// <returns>returned long value from sql query</returns>
		internal static long GetSingleLong(string p_sFieldName, string p_sTableName, string p_sCriteria,string p_sConnectString, int p_iClientId)
		{
			long lGetSingleLong=0;
			string sSql="";
			DbReader objReader=null;
			try
			{	
				if (p_sCriteria.Trim().Length==0) 
				{
					p_sCriteria="";
				}	
				else
				{
					p_sCriteria=" WHERE " + p_sCriteria;
				}
				sSql="SELECT " + p_sFieldName + " FROM " + p_sTableName+p_sCriteria;
				objReader=DbFactory.GetDbReader(p_sConnectString,sSql);

				if (objReader != null)
					if(objReader.Read())
					{
						if(objReader.GetValue(0) is System.DBNull)
						{
							lGetSingleLong=0;
						}
						else
						{
							lGetSingleLong=Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue(0)));
						}
						
					}
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Generic.GetSingleLong.Error", p_iClientId), p_objException);
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				sSql=null;
			}
			return (lGetSingleLong);
		}
		/// Name		: SafeSQL
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function creates a query based on the DB type
		/// </summary>
		/// <param name="p_sSQL">Sql query</param>
		/// <returns>Changed Query</returns>
		internal string SafeSQL(string p_sSQL)
		{
			int iPos=0; 
			int iPosNext=0; 
			string sTemp=""; 
			string sTempName=""; 
			string sSQLTemp=""; 
			sTemp=p_sSQL;
			string sTempOra="";
			try
			{
                //tkatsarski: 03/13/15 Start changes for RMA-1768: m_sTablesNameUniquePart stores unique string which is concatenated to the temporary tables names
                //If multiple mail merges are ran at the same time, the created temporary tables should be with different names, otherwise an exception will be thrown.
                iPos = sTemp.IndexOf("[INTO ");
				if (iPos!=-1)
				{
					iPosNext=sTemp.IndexOf("]",iPos+6);
					sTempName=sTemp.Substring(iPos + 6, iPosNext - iPos - 6).Trim();
					switch(m_lDbMake)
					{
						case (int) eDatabaseType.DBMS_IS_ACCESS:
							sTemp = sTemp.Substring(0,iPos - 1) + " INTO " + sTempName + "_"
                                + Manager.UID + m_sTablesNameUniquePart + sTemp.Substring(iPosNext + 1);
							break;
						case (int) eDatabaseType.DBMS_IS_SQLSRVR:
                            sTemp = sTemp.Substring(0, iPos - 1) + " INTO ##" + sTempName + "_" + Manager.UID.ToString() + m_sTablesNameUniquePart + " " 
								+ sTemp.Substring(iPosNext + 1);
							break;
						case (int) eDatabaseType.DBMS_IS_SYBASE:
                            sTemp = sTemp.Substring(iPos - 1) + " INTO ##" + sTempName + "_" + Manager.UID.ToString() + m_sTablesNameUniquePart + " "
								+ sTemp.Substring(iPosNext + 1);
							break;
						case (int) eDatabaseType.DBMS_IS_ORACLE:
                            sTempOra = "DROP TABLE " + sTempName + "_" + Manager.UID.ToString() + m_sTablesNameUniquePart;
                            DeleteOracleTables(sTempOra, sTempName + "_" + Manager.UID.ToString() + m_sTablesNameUniquePart);
                            sTemp = "CREATE TABLE " + sTempName + "_" + Manager.UID.ToString() + m_sTablesNameUniquePart
                                + " NOLOGGING  AS " + sTemp.Substring(0, iPos - 1) + sTemp.Substring(iPosNext + 1);
							break;
						case (int) eDatabaseType.DBMS_IS_DB2:
							sSQLTemp = sTemp.Substring(0, iPos - 1)
								+ " "+sTemp.Substring(iPosNext + 1) ;
							sSQLTemp=sSQLTemp.Replace("|", " ");
							DoDB2Process(sSQLTemp,sTempName,ref sTemp,iPos,iPosNext);
							break;
						case (int) eDatabaseType.DBMS_IS_INFORMIX:
							sTemp = sTemp.Substring(0,iPos - 1) + " " + sTemp.Substring(0,iPosNext+1)  
								+ " INTO TEMP "+sTempName;
							break;
					}
				}
				iPos=sTemp.IndexOf("[");
				while(iPos!=-1)
				{
					iPosNext=sTemp.IndexOf("]");
					sTempName=sTemp.Substring(iPos + 1, iPosNext - iPos - 1).Trim();
					switch(m_lDbMake)
					{
						case (int) eDatabaseType.DBMS_IS_ACCESS:
                            sTempName = sTempName + "_" + Manager.UID.ToString() + m_sTablesNameUniquePart;
							break;
						case (int) eDatabaseType.DBMS_IS_SQLSRVR:
                            sTempName = " ##" + sTempName + "_" + Manager.UID.ToString() + m_sTablesNameUniquePart;
							break;
						case (int) eDatabaseType.DBMS_IS_SYBASE:
                            sTempName = " ##" + sTempName + "_" + Manager.UID.ToString() + m_sTablesNameUniquePart;
							break;
						case (int) eDatabaseType.DBMS_IS_ORACLE:
                            sTempName = sTempName + "_" + Manager.UID.ToString() + m_sTablesNameUniquePart;
							break;
						case (int) eDatabaseType.DBMS_IS_DB2:
							sTempName=sTempName+ "_" + Manager.UID.ToString();
							sTempName = sTempName.Substring(0,18);
							break;
						case (int) eDatabaseType.DBMS_IS_INFORMIX:
							break;
					}
					sTemp=sTemp.Substring(0,iPos)+sTempName+sTemp.Substring(iPosNext + 1);
					iPos=sTemp.IndexOf("[");
				}
                //tkatsarski: 03/13/15 End changes for RMA-1768
				if (m_lDbMake==(int)eDatabaseType.DBMS_IS_ACCESS)
				{
					sTemp=sTemp.Replace("|", "AS");
				}
				else
				{
					sTemp=sTemp.Replace("| ", "");
				}
                //Deb MITS 25775
                //int indexTaxID = sTemp.IndexOf("ENTITY.TAX_ID");
                //if (objSysSettingsGlobal.MaskSSN && indexTaxID > -1)
                //{
                //    if (sTemp.Substring(indexTaxID - 1, 1) == "," || sTemp.Substring(indexTaxID - 1, 1) == " ")
                //    {
                //        sTemp = sTemp.Replace("ENTITY.TAX_ID", " CASE WHEN ENTITY.TAX_ID IS NOT NULL THEN  '###-##-####' END ");
                //    }
                //    else
                //    {
                //        indexTaxID = indexTaxID - 1;
                //        string sFld = sTemp.Substring(indexTaxID, 1);
                //        do
                //        {
                //            string sTmp = sTemp.Substring(indexTaxID - 1, 1);
                //            if (sTmp == "," || sTmp == " ")
                //                break;
                //            sFld = sTmp + sFld;
                //            indexTaxID = indexTaxID - 1;
                //        } while (indexTaxID > 0);
                //        sTemp = sTemp.Replace(sFld + "ENTITY.TAX_ID", " CASE WHEN ENTITY.TAX_ID IS NOT NULL THEN  '###-##-####' END ");
                //    }
                //}
                ////Deb MITS 25775
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Generic.SafeSQL.Error", m_iClientId), p_objException);
			}
			finally
			{
				sTempName=null; 
				sSQLTemp=null; 
				sTempOra=null;
			}
			return sTemp;
		}
		/// Name		: doDB2Process
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function does the DB2 related process
		/// </summary>
		/// <param name="p_sSql">SQL query</param>
		/// <param name="p_sTempName">Temporary table name</param>
		/// <param name="p_sTemp">Temporary table name</param>
		/// <param name="p_iPos">Start position</param>
		/// <param name="p_iPosNext">Next position</param>
		/// <returns>string</returns>
		internal string DoDB2Process(string p_sSql,string p_sTempName,ref string p_sTemp,int p_iPos,int p_iPosNext)
		{
			DbReader objReader=null;
			DataTable objTable=null;
			DbConnection objConn=null;
			string sType="";
			string sCols="";
			string sTable="";
			string sSQL="";
			try
			{
				p_sSql=p_sSql +" AND 0=1";
				objReader = DbFactory.GetDbReader(m_sConnectionString,p_sSql);
				objTable=objReader.GetSchemaTable();
				objTable=objReader.GetSchemaTable();
				for (int i=0;i<objReader.FieldCount;i++)
				{
					sType=objReader.GetDataTypeName(i);
					if (sCols.Length > 0)
					{
						sCols=sCols+",";
					}
					switch (sType.ToUpper())
					{
						case("SQL_C_CHAR"):
							sCols = sCols+" VARCHAR(" +objTable.Rows[0]["ColumnSize"].ToString()+")";
							break;
						case("SQL_C_BIT"):
							sCols = sCols+" INTEGER ";
							break;
						case("SQL_C_SHORT"):
							sCols = sCols+" INTEGER ";
							break;
						case("SQL_C_LONG"):
							sCols = sCols+" INTEGER ";
							break;
						case("SQL_C_SLONG"):
							sCols = sCols+" INTEGER ";
							break;
						case("SQL_C_FLOAT"):
							sCols = sCols+" FLOAT ";
							break;
						case("SQL_C_DOUBLE"):
							sCols = sCols+" FLOAT ";
							break;
					}
				}
				p_sTempName=p_sTempName+"_"+Manager.UID.ToString();
				sTable=p_sTempName.Substring(0,18);
				sSQL="CREATE TABLE "+sTable+"("+sCols+")";
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();
				objConn.ExecuteNonQuery (sSQL);
				p_sTemp="INSERT INTO "+sTable+" "+p_sTemp.Substring(0,p_iPos - 1)
					+" "+p_sTemp.Substring(0,p_iPosNext +1);
				objConn.ExecuteNonQuery (p_sTemp);
				objConn.Close();
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Generic.DoDB2Process.Error", m_iClientId), p_objException);
			}
			finally
			{
				if (objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn!=null)
				{
					objConn.Close();
					objConn.Dispose();
				}
                if (objTable != null)
                {
                    objTable.Dispose();
                }
                
				sType=null;
				sTable=null;
				sSQL=null;
			}
			return sCols;
		}
		/// Name		: dropMTable
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/////<summary>
		/// This function deletes a table
		/// </summary>
		/// <param name="p_objMergeCmd">Command object</param>
		/// <param name="p_sTableName">Table name to be deleted</param>
		internal void DropMTable(DbCommand p_objMergeCmd,string p_sTableName)
		{
			string sSql="";
			try
			{
				sSql = "DROP TABLE "+p_sTableName;
				if ((m_lDbMake==(int)eDatabaseType.DBMS_IS_SQLSRVR) || (m_lDbMake==(int)eDatabaseType.DBMS_IS_ORACLE))
				{
					try
					{
						p_objMergeCmd.CommandText=SafeSQL(sSql);
						p_objMergeCmd.ExecuteNonQuery();
					}
					catch(Exception p_objException)
					{
						//Logger to be used
					}
				}
				else
				{
					p_objMergeCmd.CommandText=SafeSQL(sSql);
					p_objMergeCmd.ExecuteNonQuery();
				}
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Generic.DropMTable.Error", m_iClientId), p_objException);
			}
			finally
			{
				sSql=null;
			}
			
		}
		/// Name		: GetMergeCount
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns number of records in a table
		/// </summary>
		/// <param name="p_sTable">Table name</param>
		/// <returns>Number of records in a table</returns>
		internal long GetMergeCount(string p_sTable)
		{
			string sSQL="";
			long lCount=0;
			DbReader objReader=null;
			try
			{
				sSQL="SELECT COUNT(*) FROM "+p_sTable;
				objReader=DbFactory.GetDbReader(m_sConnectionString,SafeSQL(sSQL));
				if (objReader.Read())
				{
					lCount=Conversion.ConvertStrToLong(
						Conversion.ConvertObjToStr(objReader.GetValue(0))); 
				}
				else
				{
					lCount=0;
				}
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Generic.GetMergeCount.Error", m_iClientId), p_objException);
			}
			finally
			{
				if (objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				sSQL=null;
			}
			return lCount;
		}
		/// Name		: IndexOf
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the position/index of value in Array.
		/// </summary>
		/// <param name="p_obj">Array</param>
		/// <param name="p_sValue">Value to look for</param>
		/// <returns>Index/Position of value</returns>
		internal static int IndexOf(ArrayList p_arrlstTemp,string p_sValue)
		{
			for (int i=0;i<p_arrlstTemp.Count;i++)
			{
				if (Conversion.ConvertObjToStr(p_arrlstTemp[i])==p_sValue)
				{
					return i;
				}
			}
			return -1;
		}

        /// Name		: ChainIt
        /// Date Created: 07/06/2004		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Creates a sql query from passed parameters
        /// </summary>
        /// <param name="p_sFrom">From clause</param>
        /// <param name="p_sWhere">Where clause</param>
        /// <param name="p_sTemp1">First temporary table</param>
        /// <param name="p_sTemp2">Second temporary table</param>
        /// <param name="p_sField">Field name</param>
        internal static void ChainItWithOr(ref string p_sFrom, ref string p_sWhere, string p_sTemp1,
            string p_sTemp2, string p_sField)
        {
            p_sFrom = p_sFrom + "," + p_sTemp2;
            if (p_sWhere != "")
            {
                p_sWhere = p_sWhere + " OR ";
            }
            p_sWhere = p_sWhere + p_sTemp1 + "." + p_sField + " = " + p_sTemp2 + "."
                + p_sField;


        }

		/// Name		: ChainIt
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creates a sql query from passed parameters
		/// </summary>
		/// <param name="p_sFrom">From clause</param>
		/// <param name="p_sWhere">Where clause</param>
		/// <param name="p_sTemp1">First temporary table</param>
		/// <param name="p_sTemp2">Second temporary table</param>
		/// <param name="p_sField">Field name</param>
		internal static void ChainIt(ref string p_sFrom,ref string p_sWhere,string p_sTemp1,
			string p_sTemp2,string p_sField) 
		{
			p_sFrom=p_sFrom+","+p_sTemp2;
			if (p_sWhere!="")
			{
				p_sWhere = p_sWhere + " AND ";
			}
			p_sWhere = p_sWhere + p_sTemp1 + "." + p_sField + " = " + p_sTemp2 + "."
				+p_sField;
			

		}

        /// Name		: ChainIt
        /// Author		: CSINGH7
        /// Date Created: 8/4/2009		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>               Claim Comment Enhancement
        /// Creates a sql query from passed parameters
        /// </summary>
        /// <param name="p_sFrom">From clause</param>
        /// <param name="p_sWhere">Where clause</param>
        /// <param name="p_sTemp1">First temporary table</param>
        /// <param name="p_sTemp2">Second temporary table</param>
        /// <param name="p_sField">Field name</param>
        internal static void ChainIt(ref string p_sFrom, ref string p_sWhere, string p_sTemp1,
            string p_sTemp2, string p_sField1, string p_sField2)
        {
            p_sFrom = p_sFrom + "," + p_sTemp2;
            if (p_sWhere != "")
            {
                p_sWhere = p_sWhere + " AND ";
            }
            p_sWhere = p_sWhere + p_sTemp1 + "." + p_sField1 + " = " + p_sTemp2 + "."
                + p_sField2;


        }


		/// Name		: GetSingleString
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creates a dynamic query and executes it against the database and returns result as a String value
		/// </summary>
		/// <param name="p_sFieldName">Field Name</param>
		/// <param name="p_sTableName">Table Name</param>
		/// <param name="p_sCriteria">Criteria i.e. Where clause</param>
		/// <param name="p_sConnectString">Connection string</param>
		/// <returns>string</returns>
		internal static string GetSingleString(string p_sFieldName, string p_sTableName, string p_sCriteria,string p_sConnectString, int p_iClientId)
		{
			string sGetSingleLong="";
			string sSql="";
			DbReader objReader=null;
			try
			{	
				p_sCriteria=p_sCriteria.Trim();
				if (p_sCriteria.Length>0) 
				{
					p_sCriteria=" WHERE " + p_sCriteria;
				}
				sSql="SELECT " + p_sFieldName + " FROM " + p_sTableName+p_sCriteria;
				objReader=DbFactory.GetDbReader(p_sConnectString,sSql);

				if (objReader != null)
					if(objReader.Read())
					{
						if(objReader.GetValue(0) is System.DBNull)
						{
							sGetSingleLong="";
						}
						else
						{
							sGetSingleLong=Conversion.ConvertObjToStr(objReader.GetValue(0));
						}
					}
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Generic.GetSingleString.Error", p_iClientId), p_objException);
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return (sGetSingleLong);
		}
		
		/// Name		: GetCodeIDWithShort
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function returns code id 
		/// </summary>
		/// <param name="p_sCode">Code id</param>
		/// <param name="p_lTableId">Table id</param>
		/// <param name="p_sConnectionString">Connection string</param>
		/// <returns>string</returns>
		internal static string GetCodeIDWithShort(string p_sCode,long p_lTableId,string p_sConnectionString, int p_iClientId) 
		{
            return GetSingleString("CODE_ID", "CODES", "TABLE_ID=" + p_lTableId.ToString() + " AND SHORT_CODE=" + Utilities.FormatSqlFieldValue(p_sCode), p_sConnectionString, p_iClientId);
		}
		/// Name		: Append
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Appends a space before a string value
		/// </summary>
		/// <param name="p_sValue">value</param>
		/// <returns>Resultant string value</returns>
		internal static string Append(string p_sValue)
		{
			return (p_sValue.Trim()!=""?" ,"+p_sValue:" "+p_sValue);
		}
		/// Name		: RTFToDoc
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function will generate doc file 
		/// </summary>
		/// <param name="p_sValues">Comma seperated string containing record id's for which
		/// doc file will be generated</param>
		internal static void RTFToDoc(string p_sFile)
		{
//			bool bRequestInitialOwnership = true;
//			bool bMutexWasCreated;
			Object objSaveChanges=false; //CHANGED TO FALSE FROM TRUE BY JASBINDER
			Object objfileName=p_sFile;
			object objReadOnly = false;
			object objIsVisible = true;
			
		
		//	Document objDoc=null;
		//	Mutex objMutex =null;
		//	object objFormat=WdSaveFormat.wdFormatRTF ;
			// Here is the way to handle parameters you don't care about in .NET
		//	object objMissing = System.Reflection.Missing.Value;
		//	ApplicationClass objWordApp=null;
			try
			{
//				Object objReplacedfileName=p_sFile.Replace(".rtf",".doc");
//				objMutex = new Mutex(bRequestInitialOwnership, 
//					"MailMergeMutex", 
//					out bMutexWasCreated);
//
//				if (!(bRequestInitialOwnership && bMutexWasCreated))
//				{
//					objMutex.WaitOne();
//				}
//				objWordApp = new ApplicationClass();
//				
//				//Make word visible, so you can see what's happening
//				objWordApp.Visible = false;
//				objDoc = objWordApp.Documents.Open2000(ref objfileName, ref objMissing,ref objReadOnly, ref objMissing, ref                  objMissing, ref objMissing, ref objMissing, ref objMissing, ref objMissing, ref objMissing, ref objMissing, ref objIsVisible);
//				objDoc.Activate();
//				objWordApp.ActiveDocument.SaveAs2000(ref objReplacedfileName,ref objFormat ,ref objMissing,ref objMissing,ref objMissing,ref objMissing,ref objMissing,ref objMissing,ref objMissing,ref objMissing,ref objMissing);
//				objDoc.Close(ref objSaveChanges,ref objMissing,ref objMissing);//added by jasbinder
//				objWordApp.Quit(ref objSaveChanges,ref objMissing,ref objMissing);



			}
			catch (Exception p_objException)
			{
				//objMutex.ReleaseMutex();
                // npadhy - This function is not being used any where. So client id is not passed here.
                throw new RMAppException(Globalization.GetString("Generic.RTFtoDoc.Error", 0), p_objException);
			}
			finally
			{			
				
			//	objMutex.Close();
				objSaveChanges=null;
				objfileName=null;
				objReadOnly =null;
				objIsVisible = null;
//				objDoc=null;
//				objWordApp=null;
//				objMutex =null;
//				objFormat=null;
//				objMissing = null;
			}
		}
		/// Name		: DocToRTF
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function converts Doc file to rtf file
		/// </summary>
		/// <param name="p_sFile">File name</param>
		internal static void DocToRTF(string p_sFile)
		{
			bool bRequestInitialOwnership = true;
			bool bMutexWasCreated;
			Object objSaveChanges=true;
			Object objfileName=p_sFile;
			object objReadOnly = false;
			object objIsVisible = true;
//			Mutex objMutex =null;
//			object objFormat=WdSaveFormat.wdFormatRTF ;
//			// Here is the way to handle parameters you don't care about in .NET
//			object objMissing = System.Reflection.Missing.Value;
//			ApplicationClass objWordApp=null;
//			Document objDoc=null;
//			try
//			{
//				Object objReplacedfileName=p_sFile.Replace(".doc", ".rtf");
			//	objMutex = new Mutex(bRequestInitialOwnership, 
//					"MailMergeMutex", 
//					out bMutexWasCreated);

//				if (!(bRequestInitialOwnership && bMutexWasCreated))
//				{
//					objMutex.WaitOne();
//				}
//				objWordApp = new ApplicationClass();
//				objWordApp.Visible = false;
//				objWordApp.Activate();
//				objDoc = objWordApp.Documents.Open2000(ref objfileName, ref objMissing,ref objReadOnly, ref objMissing, ref                  objMissing, ref objMissing, ref objMissing, ref objMissing, ref objMissing, ref objMissing, ref objMissing, ref objIsVisible);
//				objDoc.Activate();
//				objWordApp.ActiveDocument.SaveAs2000(ref objReplacedfileName,ref objFormat ,ref objMissing,ref objMissing,ref objMissing,ref objMissing,ref objMissing,ref objMissing,ref objMissing,ref objMissing,ref objMissing);
//				objWordApp.Quit(ref objSaveChanges,ref objMissing,ref objMissing);
//				objMutex.ReleaseMutex();
	//		}
//			catch (Exception p_objException)
//			{
//		//		objMutex.ReleaseMutex();
//				throw new RMAppException(Globalization.GetString("Generic.DocToRTF.Error"),p_objException);
//			}
//			finally
//			{
//				objSaveChanges=null;
//				objfileName=null;
//			//	objWordApp=null;
//				objReadOnly =null;
//				objIsVisible = null;
////				objDoc=null;
////				objMutex =null;
////				objFormat=null;
////				objMissing = null;
//			}
		}
		/// Name		: WordSafeFieldName
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Converts a field to a word safe field name
		/// </summary>
		/// <param name="p_sValue">Field name</param>
		/// <returns>Converted field name</returns>
		public static string WordSafeFieldName(string p_sValue, int p_iClientId)
		{
			try
			{
				Regex objReg=new Regex("[ ,]",RegexOptions.Multiline);
				p_sValue=objReg.Replace(p_sValue,"_");
				objReg=new Regex("[^A-Za-z0-9_]",RegexOptions.Multiline);
				p_sValue=objReg.Replace(p_sValue,"");
				if (p_sValue.Length > 1)
				{
					try
					{
						Convert.ToInt32(p_sValue.Substring(0,1));
						p_sValue="M_"+p_sValue;
					}
					catch(Exception p_objException)
					{
						//Logger to be used
					}
				}
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Generic.WordSafeFieldName.Error", p_iClientId), p_objException);
			}
			if (p_sValue!=null)
			{
				p_sValue=p_sValue.ToUpper();
			}
			else
			{
				p_sValue="";
			}
			return p_sValue;
		}  
		#endregion

		#region Function returns the Table name and primary key of passed table
		/// Name		: MapTableName
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the Table name and primary key of passed table
		/// </summary>
		/// <param name="p_MapTableName">Table name</param>
		/// <param name="p_sRequestPixmlformname">Person involved</param>
		/// <param name="p_sRequestAt">Admin tracking</param>
		/// <returns>Xml string containing table name and primary key of passed table</returns>
		public static string MapTableName(string p_MapTableName,string p_sRequestPixmlformname,
			string p_sRequestAt)
		{
			StringBuilder sbXml=new StringBuilder("<TableName>");
			if (p_sRequestAt==null)
			{
				p_sRequestAt="";
			}
			if (p_sRequestPixmlformname==null)
			{
				p_sRequestPixmlformname="";
			}
			if ((p_MapTableName==null) || (p_MapTableName == "")) 
			{ 
				return ""; 
			}
			else
			{
				p_MapTableName=p_MapTableName.ToUpper();
			}
			if (p_MapTableName == "CLAIM") 
			{ 
				sbXml.Append("<MapTableName>claim</MapTableName>");
				sbXml.Append("<PrimaryId>claimid</PrimaryId>");
			} 
			else if (p_MapTableName == "FUNDS") 
			{ 
				sbXml.Append("<MapTableName>funds</MapTableName>");
				sbXml.Append("<PrimaryId>transid</PrimaryId>");
			} 
			else if (p_MapTableName == "FUNDS_DEPOSIT") 
			{ 
				sbXml.Append("<MapTableName>deposit</MapTableName>");
				sbXml.Append("<PrimaryId>depositid</PrimaryId>");
			} 
			else if (p_MapTableName == "CLAIM_ADJUSTER") 
			{ 
				sbXml.Append("<MapTableName>adjuster</MapTableName>");
				sbXml.Append("<PrimaryId>adjrowid</PrimaryId>");
			} 
			else if (p_MapTableName == "ADJUST_DATED_TEXT") 
			{ 
				sbXml.Append("<MapTableName>adjusterdatedtext</MapTableName>");
				sbXml.Append("<PrimaryId>adjdttextrowid</PrimaryId>");
			} 
			else if (p_MapTableName == "CLAIMANT") 
			{ 
				sbXml.Append("<MapTableName>claimant</MapTableName>");
				sbXml.Append("<PrimaryId>clmntrowid</PrimaryId>");
			} 
			else if (p_MapTableName == "DEFENDANT") 
			{ 
				sbXml.Append("<MapTableName>defendant</MapTableName>");
				sbXml.Append("<PrimaryId>defendantrowid</PrimaryId>");
			} 
			else if (p_MapTableName == "EXPERT") 
			{ 
				sbXml.Append("<MapTableName>expert</MapTableName>");
				sbXml.Append("<PrimaryId>expertrowid</PrimaryId>");
			} 
			else if (p_MapTableName == "CLAIM_X_LITIGATION") 
			{ 
				sbXml.Append("<MapTableName>litigation</MapTableName>");
				sbXml.Append("<PrimaryId>litigationrowid</PrimaryId>");
			} 
			else if (p_MapTableName == "UNIT_X_CLAIM") 
			{ 
				sbXml.Append("<MapTableName>unit</MapTableName>");
				sbXml.Append("<PrimaryId>unitrowid</PrimaryId>");
			} 
			else if (p_MapTableName == "VEHICLE") 
			{ 
				sbXml.Append("<MapTableName>vehicle</MapTableName>");
				sbXml.Append("<PrimaryId>unitid</PrimaryId>");
			} 
			else if (p_MapTableName == "EVENT") 
			{ 
				sbXml.Append("<MapTableName>event</MapTableName>");
				sbXml.Append("<PrimaryId>eventid</PrimaryId>");
			} 
			else if (p_MapTableName == "ENTITY") 
			{ 
				sbXml.Append("<MapTableName>entity</MapTableName>");
				sbXml.Append("<PrimaryId>entityid</PrimaryId>");
			} 
			else if (p_MapTableName == "EMPLOYEE") 
			{ 
				sbXml.Append("<MapTableName>employee</MapTableName>");
				sbXml.Append("<PrimaryId>employeeeid</PrimaryId>");
			} 
			else if (p_MapTableName == "POLICY") 
			{ 
				sbXml.Append("<MapTableName>policy</MapTableName>");
				sbXml.Append("<PrimaryId>policyid</PrimaryId>");
			} 
			else if (p_MapTableName == "PATIENT") 
			{ 
				sbXml.Append("<MapTableName>patient</MapTableName>");
				sbXml.Append("<PrimaryId>patientid</PrimaryId>");
			} 
			else if (p_MapTableName == "PHYSICIAN") 
			{ 
				sbXml.Append("<MapTableName>physician</MapTableName>");
				sbXml.Append("<PrimaryId>physeid</PrimaryId>");
			} 
			else if (p_MapTableName == "MED_STAFF") 
			{ 
				sbXml.Append("<MapTableName>staff</MapTableName>");
				sbXml.Append("<PrimaryId>staffeid</PrimaryId>");
			} 
			else if (p_MapTableName == "EV_X_CONCOM_PROD") 
			{ 
				sbXml.Append("<MapTableName>concomitant</MapTableName>");
				sbXml.Append("<PrimaryId>evconcomrowid</PrimaryId>");
			} 
			else if (p_MapTableName == "EMP_X_DEPENDENT") 
			{ 
				sbXml.Append("<MapTableName>dependent</MapTableName>");
				sbXml.Append("<PrimaryId>empdeprowid</PrimaryId>");
			} 
			else if (p_MapTableName == "PI_X_DEPENDENT") 
			{ 
				sbXml.Append("<MapTableName>pidependent</MapTableName>");
				sbXml.Append("<PrimaryId>pideprowid</PrimaryId>");
			} 
			else if (p_MapTableName == "EVENT_X_DATED_TEXT") 
			{ 
				sbXml.Append("<MapTableName>eventdatedtext</MapTableName>");
				sbXml.Append("<PrimaryId>evdtrowid</PrimaryId>");
			} 
			else if (p_MapTableName == "FALL_INDICATOR") 
			{ 
				sbXml.Append("<MapTableName>fallinfo</MapTableName>");
				sbXml.Append("<PrimaryId>eventid</PrimaryId>");
			} 
			else if (p_MapTableName == "PI_X_WORK_LOSS") 
			{ 
				sbXml.Append("<MapTableName>piworkloss</MapTableName>");
				sbXml.Append("<PrimaryId>piwlrowid</PrimaryId>");
			} 
			else if (p_MapTableName == "EVENT_X_MEDWATCH") 
			{ 
				sbXml.Append("<MapTableName>medwatch</MapTableName>");
				sbXml.Append("<PrimaryId>eventid</PrimaryId>");
			} 
			else if (p_MapTableName == "EVENT_X_MEDW_TEST") 
			{ 
				sbXml.Append("<MapTableName>medwatchtest</MapTableName>");
				sbXml.Append("<PrimaryId>evmwtestrowid</PrimaryId>");
			} 
			else if (p_MapTableName == "EVENT_X_OSHA") 
			{ 
				sbXml.Append("<MapTableName>osha</MapTableName>");
				sbXml.Append("<PrimaryId>eventid</PrimaryId>");
			} 
			else if (p_MapTableName == "POLICY_X_CVG_TYPE") 
			{ 
				sbXml.Append("<MapTableName>policycoverage</MapTableName>");
				sbXml.Append("<PrimaryId>polcvgrowid</PrimaryId>");
			} 
			else if (p_MapTableName == "PI_X_RESTRICT") 
			{ 
				sbXml.Append("<MapTableName>pirestriction</MapTableName>");
				sbXml.Append("<PrimaryId>pirestrictrowid</PrimaryId>");
			} 
			else if (p_MapTableName == "EMPLOYEE_INVOLVED") 
			{ 
				sbXml.Append("<MapTableName>piemployee</MapTableName>");
				sbXml.Append("<PrimaryId>pirowid</PrimaryId>");
			} 
			else if (p_MapTableName == "OTHER_INVOLVED") 
			{ 
				sbXml.Append("<MapTableName>piother</MapTableName>");
				sbXml.Append("<PrimaryId>pirowid</PrimaryId>");
			} 
			else if (p_MapTableName == "PATIENT_INVOLVED") 
			{ 
				sbXml.Append("<MapTableName>pipatient</MapTableName>");
				sbXml.Append("<PrimaryId>pirowid</PrimaryId>");
			} 
            //Aman Driver Enh
            else if (p_MapTableName == "DRIVER_INVOLVED")
            {
                sbXml.Append("<MapTableName>pidriver</MapTableName>");
                sbXml.Append("<PrimaryId>pirowid</PrimaryId>");
            }
            //Aman Driver Enh
			else if (p_MapTableName == "WITNESS_INVOLVED") 
			{ 
				sbXml.Append("<MapTableName>piwitness</MapTableName>");
				sbXml.Append("<PrimaryId>pirowid</PrimaryId>");
			} 
			else if (p_MapTableName == "admtable") 
			{ 
				sbXml.Append("<MapTableName>funds</MapTableName>");
				sbXml.Append("<PrimaryId>transid</PrimaryId>");
			} 
			else if (p_MapTableName == "PERSON_INVOLVED") 
			{ 
				sbXml.Append("<MapTableName>"+p_sRequestPixmlformname+"</MapTableName>");
				sbXml.Append("<PrimaryId>pirowid</PrimaryId>");
			} 
			else if (p_sRequestAt == "1") 
			{ 
				sbXml.Append("<MapTableName>"+p_MapTableName+"</MapTableName>");
				sbXml.Append("<PrimaryId>"+p_MapTableName + "_id"+"</PrimaryId>");
			}
            //Aman Driver Enh
            else if (p_MapTableName == "DRIVER")
            {
                sbXml.Append("<MapTableName>driver</MapTableName>");
                sbXml.Append("<PrimaryId>driverrowid</PrimaryId>");
            }
            //Aman Driver Enh
			sbXml.Append("</TableName>");
			return sbXml.ToString();
		}
		#endregion

		#region Function returns the table name of the passed form name
		/// Name		: GetMergeCount
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns true if case mgmt is installed
		/// </summary>
		/// /// <returns>true/false</returns>
		internal bool IsCaseMgtInstalled()
		{
			string sSQL="";
			bool bInstalled=false;
			DbReader objReader=null;
			try
			{
				sSQL="SELECT CLAIM_ID FROM CASE_MANAGEMENT WHERE CASEMGT_ROW_ID = -1";
				objReader=DbFactory.GetDbReader(m_sConnectionString,SafeSQL(sSQL));
				try
				{
					if (objReader.Read()){}
					if (!objReader.IsClosed)
						objReader.Close();
					objReader=null;
					sSQL="SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'CASE_MGT_FLAG' AND PARM_CATEGORY = 0";
					objReader=DbFactory.GetDbReader(m_sConnectionString,SafeSQL(sSQL));
					if (objReader.Read())
					{
						if (Conversion.ConvertObjToStr(objReader.GetValue(0)).Equals("-1"))
						{
							bInstalled=true;
						}
					}
					else
					{
						bInstalled=false;
					}
					if (!objReader.IsClosed)
						objReader.Close();
					objReader=null;
				}
				catch(Exception ex)
				{
					bInstalled=false;
				}
				
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Generic.IsCaseMgtInstalled.Error", m_iClientId), p_objException);
			}
			finally
			{
				if (objReader!=null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				sSQL=null;
			}
			return bInstalled ;
		}
        
        //BOB Enhancement Unit Stat : Start
        /// <summary>
        /// Checks whether Unit stat setting is ON
        /// </summary>
        /// <returns>true if Unit Stat is ON</returns>
        internal bool IsUnitStatInstalled()
        {
            string sSQL = "";
            bool bInstalled = false;
            DbReader objReader = null;
            try
            {                
                try
                {
                    sSQL = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'UNIT_STAT'";
                    objReader = DbFactory.GetDbReader(m_sConnectionString, SafeSQL(sSQL));
                    if (objReader.Read())
                    {
                        if (Conversion.ConvertObjToStr(objReader.GetValue(0)).Equals("-1"))
                        {
                            bInstalled = true;
                        }
                    }
                    else
                    {
                        bInstalled = false;
                    }
                    if (!objReader.IsClosed)
                        objReader.Close();
                    objReader = null;
                }
                catch (Exception ex)
                {
                    bInstalled = false;
                }

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Generic.IsUnitStattInstalled.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                sSQL = null;
            }
            return bInstalled;
        }








        //BOB Enhancement Unit Stat : End

		/// Name		: MapFormName
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns the table name 
		/// </summary>
		/// <param name="p_sFormName">Form name</param>
		/// <param name="p_sRequestAt">Admin tracking</param>
		/// <returns>Returns the table name</returns>
		public static string MapFormName(string p_sFormName,string p_sRequestAt)
		{
			if (p_sRequestAt==null)
			{
				p_sRequestAt="";
			}
			if ((p_sFormName==null) || (p_sFormName == "")) 
			{ 
				return ""; 
			}
			p_sFormName = p_sFormName.Trim().ToLower(); 
			// Ayush: Row modified for Property Claims mail merge, 12/16/2009 Start: 
			//  if (p_sFormName == "claimgc" || p_sFormName == "claimva" || p_sFormName == "claimwc" || p_sFormName == "claimdi"  || p_sFormName == //"claim")  

            if (p_sFormName == "claimgc" || p_sFormName == "claimva" || p_sFormName == "claimwc" || p_sFormName == "claimdi" || p_sFormName == "claimpc"  || p_sFormName == "claim") 
			{ 
				return "CLAIM"; 
			} 
			// Ayush: Row modified for Property Claims mail merge, 12/16/2009 End: 
            else if (p_sFormName == "piemployee" || p_sFormName == "pimedstaff" || p_sFormName == "piother" || p_sFormName == "pidriver" || p_sFormName == "pipatient" | p_sFormName == "piqpatient" | p_sFormName == "piphysician" | p_sFormName == "piwitness") 
			{ 
				return "PERSON_INVOLVED"; 
			} 
			else if (p_sFormName == "funds") 
			{ 
				return "FUNDS"; 
			} 
			else if (p_sFormName == "deposit") 
			{ 
				return "FUNDS_DEPOSIT"; 
			} 
			else if (p_sFormName == "adjuster") 
			{ 
				return "CLAIM_ADJUSTER"; 
			} 
			else if (p_sFormName == "adjusterdatedtext") 
			{ 
				return "ADJUST_DATED_TEXT"; 
			} 
			else if (p_sFormName == "claimant") 
			{ 
				return "CLAIMANT"; 
			} 
			else if (p_sFormName == "defendant") 
			{ 
				return "DEFENDANT"; 
			} 
			else if (p_sFormName == "expert") 
			{ 
				return "EXPERT"; 
			} 
			else if (p_sFormName == "litigation") 
			{ 
				return "CLAIM_X_LITIGATION"; 
			} 
			else if (p_sFormName == "unit") 
			{ 
				return "UNIT_X_CLAIM"; 
			} 
			else if (p_sFormName == "vehicle") 
			{ 
				return "VEHICLE"; 
			} 
			else if (p_sFormName == "event") 
			{ 
				return "EVENT"; 
			} 
			else if (p_sFormName == "entity" || p_sFormName == "people" || p_sFormName == "entitymaint") 
			{ 
				return "ENTITY"; 
			} 
			else if (p_sFormName == "employee") 
			{ 
				return "EMPLOYEE"; 
			} 
			else if (p_sFormName == "policy") 
			{ 
				return "POLICY"; 
			} 
			else if (p_sFormName == "patient") 
			{ 
				return "PATIENT"; 
			}
            //Aman Driver Enh
            else if (p_sFormName == "driver")
            {
                return "DRIVER";
            }
            //Aman Driver Enh
			else if (p_sFormName == "physician") 
			{ 
				return "PHYSICIAN"; 
			} 
			else if (p_sFormName == "medicalstaff" || p_sFormName == "staff") 
			{ 
				return "MED_STAFF"; 
			} 
			else if (p_sFormName == "concomitant") 
			{ 
				return "EV_X_CONCOM_PROD"; 
			} 
			else if (p_sFormName == "dependent") 
			{ 
				return "EMP_X_DEPENDENT"; 
			} 
			else if (p_sFormName == "pidependent") 
			{ 
				return "PI_X_DEPENDENT"; 
			} 
			else if (p_sFormName == "eventdatedtext") 
			{ 
				return "EVENT_X_DATED_TEXT"; 
			} 
			else if (p_sFormName == "fallinfo") 
			{ 
				return "FALL_INDICATOR"; 
			} 
			else if (p_sFormName == "piworkloss") 
			{ 
				return "PI_X_WORK_LOSS"; 
			} 
			else if (p_sFormName == "medwatch") 
			{ 
				return "EVENT_X_MEDWATCH"; 
			} 
			else if (p_sFormName == "medwatchtest") 
			{ 
				return "EVENT_X_MEDW_TEST"; 
			} 
			else if (p_sFormName == "osha") 
			{ 
				return "EVENT_X_OSHA"; 
			} 
			else if (p_sFormName == "policycoverage") 
			{ 
				return "POLICY_X_CVG_TYPE"; 
			} 
			else if (p_sFormName == "pirestriction") 
			{ 
				return "PI_X_RESTRICT"; 
			} 
			else if (p_sFormName == "admtable") 
			{ 
				return "";
			} 
			else if (p_sRequestAt == "1") 
			{ 
				return p_sFormName; 
			} 
			return "";
		}
		#endregion

		#region Function to generate a unique file name
		/// Name		: GetServerUniqueFileName
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns a unique file name
        /// Change to use the similar approach in RMWorld
		/// </summary>
		/// <returns>unique file name</returns>
		public static string  GetServerUniqueFileName(string sPrefix, string sExtesion, int p_iClientId)
		{
			try
			{
                //Try to avoid of returning a duplicate file name
                Random rand = new Random();
                string sFileName = string.Empty;
                string sRand = string.Empty;
                for (int i = 0; i < 10; i++)
                {
                    sRand = rand.Next().ToString();
                    sRand = sRand.Substring(0, 1);
                    sFileName += sRand;
                }

                sFileName = sPrefix + sFileName + "." + sExtesion;
                return sFileName;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Generic.GetServerUniqueFileName.Error", p_iClientId), p_objException);
			}
		}
		#endregion
	}
}
