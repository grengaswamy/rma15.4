﻿
using System;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using System.Collections;
using System.Text;
namespace Riskmaster.Application.MailMerge
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   29th November 2004
	///Purpose :   This class Fetches template field information.
	/// </summary>
	public class TemplateField
	{
		#region Variable Declaration
        private int m_iClientId = 0;
		/// <summary>
		/// Field id
		/// </summary>
		private long m_lFieldId=0; 
		/// <summary>
		/// Category id
		/// </summary>
		private long m_lCatId=0; 
		/// <summary>
		/// Field name
		/// </summary>
		private string m_sFieldName=""; 
		/// <summary>
		/// Field description
		/// </summary>
		private string m_sFieldDesc=""; 
		/// <summary>
		/// Field table
		/// </summary>
		private string m_sFieldTable=""; 
		/// <summary>
		/// Pattern
		/// </summary>
		private string m_sOptMask=""; 
		/// <summary>
		/// Display category
		/// </summary>
		private string m_sDisplayCat=""; 
		/// <summary>
		/// Code Table
		/// </summary>
		private string m_sCodeTable=""; 
		/// <summary>
		/// Field type
		/// </summary>
		private long m_lFieldType=0; 
		/// <summary>
		/// Param
		/// </summary>
		private string m_sParam=""; 
		/// <summary>
		/// Raw value
		/// </summary>
		private string m_sRawValue=""; 
		/// <summary>
		/// Field value
		/// </summary>
		private string m_sValue=""; 
		/// <summary>
		/// Word field name
		/// </summary>
		private string m_sWordFieldName=""; 
		/// <summary>
		/// Long code flag
		/// </summary>
		private bool m_bLongCodeFlag=false; 
		/// <summary>
		/// Original parameter
		/// </summary>
		private string m_sRawParam="";
		/// <summary>
		/// Form id
		/// </summary>
		private long m_lFormId=0; 
		/// <summary>
		/// Item type
		/// </summary>
		private long m_lItemType=0; 
		/// <summary>
		/// Sequence name
		/// </summary>
		private long m_lSeqNum=0; 
		/// <summary>
		/// Supplemental field or not
		/// </summary>
		private long m_lSuppFieldFlag=0;
		/// <summary>
		/// Connection string
		/// </summary>
		public string m_sConnectionString="";
		#endregion

		#region TemplateField Constructor
		/// <summary>
		/// TemplateField class constructor
		/// </summary>
		/// 
        public TemplateField(int p_iClientId) { m_iClientId = p_iClientId; }
		#endregion

		#region TemplateField destructor
		/// <summary>
		/// TemplateField class destructor
		/// </summary>
		/// 
		~TemplateField()
		{
			m_sFieldDesc=null; 
			m_sFieldTable=null;
			m_sOptMask=null;
			m_sDisplayCat=null;
			m_sCodeTable=null;
			m_sParam=null;
			m_sRawValue=null; 
			m_sValue=null;
			m_sWordFieldName=null;
			m_sRawParam=null;
			m_sConnectionString=null;
		}
		#endregion

		#region Property Declaration
		/// <summary>
		/// Item type 
		/// </summary>
		public long ItemType 
		{
			get
			{
				return m_lItemType;
			}
			set
			{
				m_lItemType=value;
			}
		}
		/// <summary>
		/// Sequence number of field
		/// </summary>
		public long SeqNum 
		{
			get
			{
				return m_lSeqNum;
			}
			set
			{
				m_lSeqNum=value;
			}
		}
		/// <summary>
		/// Supplement field or not
		/// </summary>
		public long SuppFieldFlag 
		{
			get
			{
				return m_lSuppFieldFlag;
			}
			set
			{
				m_lSuppFieldFlag=value;
			}
		}
		/// <summary>
		/// Field type
		/// </summary>
		public long FieldType
		{
			get
			{
				return m_lFieldType;
			}
			set
			{
				m_lFieldType=value;
			}
		}
		/// <summary>
		/// Value of field
		/// </summary>
		public string Value
		{
			get
			{
				return m_sValue;
			}
			set
			{
				m_sRawValue=value;
				m_sValue=value;
				m_sValue=FormatValue();
			}
		}
		/// <summary>
		/// Parameter
		/// </summary>
		public string Param
		{
			get
			{
				return m_sParam;
			}
			set
			{
				long lTmp=0;
				if (value=="")
				{
					m_sParam="";
					return;
				}
				m_sRawParam=value;
				try
				{
					//explicitly done to check for non numeric string value
					//Logger to be used
					lTmp= Convert.ToInt64(value);
				}
				catch(Exception p_objException)
				{
					//explicitly done to check for non numeric string value
					//Logger to be used
					lTmp = -1;
				}
				if (value=="<ALL>")
				{
					m_sParam=value;
				}
				else if (lTmp==-1)
				{
					
					lTmp = Generic.GetSingleLong("CODE_ID", "CODES_TEXT", " CODE_DESC=" + Utilities.FormatSqlFieldValue(value),m_sConnectionString, m_iClientId);
				}
				else if (lTmp == 0)
				{
					m_sParam = "";
				}
				else
				{
					m_sParam=value;
				}
			}
		}
		/// <summary>
		/// Code table
		/// </summary>
		public string CodeTable 
		{
			get
			{
				return m_sCodeTable;
			}
		}
		/// <summary>
		/// Display Category
		/// </summary>
		public string DisplayCat 
		{
			get
			{
				return m_sDisplayCat;
			}
		}
		/// <summary>
		/// Pattern
		/// </summary>
		public string OptMask 
		{
			get
			{
				return m_sOptMask;
			}
		}
		/// <summary>
		/// Field table
		/// </summary>
		public string FieldTable 
		{
			get
			{
				return m_sFieldTable;
			}
		}
		/// <summary>
		/// Raw/original value before being passed to formatvalue function
		/// </summary>
		public string RawValue 
		{
			get
			{
				return m_sRawValue;
			}
		}
		/// <summary>
		/// Field description
		/// </summary>
		public string FieldDesc 
		{
			get
			{
				return m_sFieldDesc;
			}
		}
		/// <summary>
		/// Word safe field name
		/// </summary>
		public string WordFieldName 
		{
			get
			{
				return m_sWordFieldName;
			}
			set
			{
				m_sWordFieldName=value;
			}
		}
		/// <summary>
		/// Field name
		/// </summary>
		public string FieldName 
		{
			get
			{
				return m_sFieldName;
			}
		}
		/// <summary>
		/// Category id
		/// </summary>
		public long CatId 
		{
			get
			{
				return m_lCatId;
			}
		}
		/// <summary>
		/// Form id
		/// </summary>
		public long FormId 
		{
			get
			{
				return m_lFormId;
			}
			set
			{
				m_lFormId=value;
			}
		}
		/// <summary>
		/// Long code 
		/// </summary>
		public bool LongCodeFlag 
		{
			get
			{
				return m_bLongCodeFlag;
			}
			set
			{
				m_bLongCodeFlag=value;
			}
		}
		/// <summary>
		/// Field id
		/// </summary>
		public long FieldId 
		{
			get
			{
				return m_lFieldId;
			}
			set
			{
				
				bool bSuppFlag=false;
				bool bNewFlag=false;
				try
				{
					if (value > 65536)
					{
						m_lFieldId=value%65536;
						if ((value/65536) >= 1000)
						{
							bSuppFlag=true;
						}
					}
					else
					{
						m_lFieldId=value;
					}
					//to do
					bNewFlag=IsNew(m_lFieldId, m_sParam);
					if (bNewFlag && bSuppFlag)
					{
						LoadSuppNew (this.m_lFieldId);
					}
					else if ((bNewFlag) && (!bSuppFlag))
					{
						LoadStdNew (this.m_lFieldId);
					}
					else if ((!bNewFlag) && (bSuppFlag))
					{
						LoadSuppExisting(this.m_lFieldId);
					}
					else if ((!bNewFlag) && (!bSuppFlag))
					{
						LoadStdExisting(this.m_lFieldId);
					}
					FixCodeFields();
					FixAggregateFields();
					//to do
				}
				catch (RMAppException p_objException)
				{
					throw p_objException;
				}
				catch (Exception p_objException)
				{
					throw new RMAppException(Globalization.GetString("TemplateField.SetFieldId.Error",m_iClientId),p_objException);
				}
			}	
		}
		#endregion

		#region Common Function used to set property(s) value
		/// Name		: FixAggregateFields
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Concatenates a value with field description
		/// </summary>
		private void FixAggregateFields() 
		{ 
			string sTmp="";
			LocalCache objCache=null;
			try
			{
                objCache = new LocalCache(m_sConnectionString, m_iClientId);
				if (Param != "") 
				{ 
					if (Param == "<ALL>") 
					{ 
						sTmp = " (ALL)"; 
					} 
					else 
					{ 
						sTmp = " (" + objCache.GetCodeDesc(Conversion.ConvertStrToInteger(Param))+")";
							
					} 
					if (this.FieldDesc.IndexOf(sTmp) == -1) 
					{ 
						m_sFieldDesc = this.FieldDesc + sTmp;
                        WordFieldName = Generic.WordSafeFieldName(m_sFieldDesc, m_iClientId); 
					} 
				}
				
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateField.FixAggregateFields.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objCache!=null)
				{
					objCache.Dispose();
					objCache=null;
				}
				sTmp=null;
			}
		}
		/// Name		: FixCodeFields
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Concatenates a value with field description
		/// </summary>
		private void FixCodeFields() 
		{ 
			string sTmp=""; 
			try
			{
                if (!Conversion.ConvertLongToBool(SuppFieldFlag, m_iClientId)) 
				{ 
					if ((FieldType == 3) & !LongCodeFlag)
					{
						sTmp=" (Code)";
					}
					if ((FieldType == 3) & LongCodeFlag)
					{
						sTmp=" (Desc)";
					}
					if ((FieldType == 6) & !LongCodeFlag)
					{
						sTmp=" (Short)";
					}
					if ((FieldType == 6) & LongCodeFlag)
					{
						sTmp=" (Long)";
					}
				} 
				else 
				{ 
					if ((FieldType == 6) & !LongCodeFlag)
					{
						sTmp=" (Code)";
					}
					if ((FieldType == 6) & LongCodeFlag)
					{
						sTmp=" (Desc)";
					}
					if ((FieldType == 9) & !LongCodeFlag)
					{
						sTmp=" (Short)";
					}
					if ((FieldType == 9) & LongCodeFlag)
					{
						sTmp=" (Long)";
					}
				} 
				if (sTmp != "") 
				{ 
					m_sFieldDesc = FieldDesc + sTmp;
                    m_sWordFieldName = Generic.WordSafeFieldName(m_sFieldDesc, m_iClientId); 
				}
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateField.FixCodeFields.Error",m_iClientId),p_objException);
			}
			finally
			{
				sTmp=null;
			}
		}
		/// Name		: LoadStdExisting
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Loads Standard Data for a field
		/// </summary>
		/// <param name="p_lFieldId">Field data</param>
		private void LoadStdExisting(long p_lFieldId)
		{
			string sSql="";
			//Load all Standard Data for a Previously Used Field
			DbReader objReader = null;
			try
			{
				sSql = "SELECT MERGE_FORM_DEF.FIELD_ID,MERGE_DICTIONARY.FIELD_TYPE, MERGE_FORM.CAT_ID, MERGE_DICTIONARY.FIELD_NAME, MERGE_DICTIONARY.FIELD_DESC, MERGE_DICTIONARY.FIELD_TABLE, MERGE_DICTIONARY.OPT_MASK, MERGE_DICTIONARY.DISPLAY_CAT, MERGE_DICTIONARY.CODE_TABLE, MERGE_FORM_DEF.ITEM_TYPE, MERGE_FORM_DEF.SEQ_NUM, MERGE_FORM_DEF.SUPP_FIELD_FLAG, MERGE_FORM_DEF.MERGE_PARAM FROM MERGE_FORM_DEF, MERGE_FORM, MERGE_DICTIONARY WHERE MERGE_FORM_DEF.FIELD_ID=MERGE_DICTIONARY.FIELD_ID  AND MERGE_FORM_DEF.MERGE_PARAM =" + Utilities.FormatSqlFieldValue(m_sParam) + " AND MERGE_FORM_DEF.FORM_ID=MERGE_FORM.FORM_ID AND MERGE_FORM_DEF.FORM_ID=" + this.FormId + " AND MERGE_FORM_DEF.FIELD_ID=" + p_lFieldId.ToString();
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
				if (objReader.Read())
				{
					m_lFieldId = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("FIELD_ID"))); 
					m_lCatId = 	Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("CAT_ID"))); 
					m_sFieldName = Conversion.ConvertObjToStr(Conversion.ConvertObjToStr(objReader.GetValue( "FIELD_NAME"))); 
					m_sFieldDesc = Conversion.ConvertObjToStr(Conversion.ConvertObjToStr(objReader.GetValue( "FIELD_DESC"))); 
					m_sFieldTable = Conversion.ConvertObjToStr(Conversion.ConvertObjToStr(objReader.GetValue( "FIELD_TABLE"))); 
					m_sOptMask = Conversion.ConvertObjToStr(Conversion.ConvertObjToStr(objReader.GetValue( "OPT_MASK"))); 
					m_sDisplayCat =Conversion.ConvertObjToStr(Conversion.ConvertObjToStr(objReader.GetValue( "DISPLAY_CAT"))); 
					m_sCodeTable = Conversion.ConvertObjToStr(Conversion.ConvertObjToStr(objReader.GetValue( "CODE_TABLE"))); 
					m_lItemType = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("ITEM_TYPE"))); 
					m_lSeqNum = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("SEQ_NUM"))); 
					m_lSuppFieldFlag = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("SUPP_FIELD_FLAG"))); 
					m_lFieldType = 	Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("FIELD_TYPE"))); 
					m_sParam = Conversion.ConvertObjToStr(Conversion.ConvertObjToStr(objReader.GetValue( "MERGE_PARAM")));
					m_sFieldDesc = AppendAggregateSuffixIfExists();
				}
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateField.LoadStdExisting.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				sSql=null;
			}
		}
		/// Name		: LoadSuppNew
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Loads new supplement field
		/// </summary>
		/// <param name="p_lFieldId">Field id</param>
		private void LoadSuppNew(long p_lFieldId)
		{
			string sSql="";
			//Load all Standard Data for a Previously Used Field
			DbReader objReader = null;
			try
			{
				//sSql = "SELECT MERGE_DICTIONARY.FIELD_TYPE, MERGE_DICTIONARY.FIELD_NAME, MERGE_DICTIONARY.FIELD_DESC, MERGE_DICTIONARY.FIELD_TABLE, MERGE_DICTIONARY.OPT_MASK, MERGE_DICTIONARY.DISPLAY_CAT, MERGE_DICTIONARY.CODE_TABLE, MERGE_DICTIONARY.FIELD_TABLE FROM MERGE_DICTIONARY WHERE MERGE_DICTIONARY.FIELD_ID=" + p_lFieldId.ToString();
				sSql = "SELECT FIELD_ID,SYS_FIELD_NAME,USER_PROMPT,FIELD_TYPE, PATTERN,SUPP_TABLE_NAME,CODE_FILE_ID FROM";
				sSql+= " SUPP_DICTIONARY WHERE FIELD_ID = " + p_lFieldId.ToString();
				//sSql+=  " AND FIELD_TYPE NOT IN (7,20,14,15,16)";
                sSql += " AND FIELD_TYPE NOT IN (7,20,14,15)"; //MITS hlv 26255 6/27/12
				sSql+=  " ORDER BY USER_PROMPT";
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
				if (objReader.Read())
				{
					m_lFieldId = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("FIELD_ID"))); 
					m_sFieldName = Conversion.ConvertObjToStr(objReader.GetValue("SYS_FIELD_NAME")); 
					m_sFieldDesc = Conversion.ConvertObjToStr(objReader.GetValue( "USER_PROMPT")); 
					m_sFieldTable = Conversion.ConvertObjToStr(objReader.GetValue( "SUPP_TABLE_NAME")); 
					m_sOptMask = Conversion.ConvertObjToStr(objReader.GetValue( "PATTERN")); 
					m_sCodeTable = Conversion.ConvertObjToStr(objReader.GetValue( "CODE_FILE_ID")); 
					m_lFieldType=Conversion.ConvertStrToLong(
						Conversion.ConvertObjToStr(objReader.GetValue( "FIELD_TYPE"))); 
					m_lSuppFieldFlag = 	1;
				}
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateField.LoadSuppNew.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				sSql=null;
			}
		}
		/// Name		: LoadStdNew
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Loads new standard data for a field
		/// </summary>
		/// <param name="p_lFieldId">Field id</param>
		private void LoadStdNew(long p_lFieldId)
		{
			string sSql="";
			//Load all Standard Data for a Previously Used Field
			DbReader objReader = null;
			try
			{
                sSql = " SELECT MERGE_DICTIONARY.FIELD_TYPE, MERGE_DICTIONARY.FIELD_NAME,MERGE_DICTIONARY.CAT_ID, ";//Added Cat_Id for R7
				sSql = sSql + " MERGE_DICTIONARY.FIELD_DESC, MERGE_DICTIONARY.FIELD_TABLE,";
				sSql = sSql + " MERGE_DICTIONARY.OPT_MASK, MERGE_DICTIONARY.DISPLAY_CAT, MERGE_DICTIONARY.CODE_TABLE,";
				sSql = sSql + " MERGE_DICTIONARY.FIELD_TABLE FROM MERGE_DICTIONARY WHERE MERGE_DICTIONARY.FIELD_ID=" + p_lFieldId.ToString();
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
				if (objReader.Read())
				{
					m_sFieldName = Conversion.ConvertObjToStr(objReader.GetValue( "FIELD_NAME")); 
					m_sFieldDesc = Conversion.ConvertObjToStr(objReader.GetValue( "FIELD_DESC")); 
					m_sFieldTable = Conversion.ConvertObjToStr(objReader.GetValue( "FIELD_TABLE")); 
					m_sOptMask = Conversion.ConvertObjToStr(objReader.GetValue( "OPT_MASK")); 
					m_sDisplayCat = Conversion.ConvertObjToStr(objReader.GetValue( "DISPLAY_CAT")); 
					m_sCodeTable = Conversion.ConvertObjToStr(objReader.GetValue( "CODE_TABLE")); 
					m_lFieldType = 	Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("FIELD_TYPE")));
                    //Added Rakhi for R7:Add Emp Data Elements
                    m_lCatId = 	Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("CAT_ID")));
                    //Added Rakhi for R7:Add Emp Data Elements
				}
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateField.LoadStdNew.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				sSql=null;
			}
		}
		/// Name		: LoadSuppExisting
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Loads existing supplement field data
		/// </summary>
		/// <param name="p_lFieldId">Field id</param>
		private void LoadSuppExisting(long p_lFieldId)
		{
			string sSql="";
			//Load all Standard Data for a Previously Used Field
			DbReader objReader = null;
			try
			{
				sSql = "SELECT SEQ_NUM, MERGE_PARAM FROM MERGE_FORM_DEF WHERE FIELD_ID = " + p_lFieldId.ToString() + " AND FORM_ID=" + this.FormId.ToString();
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
				if (objReader.Read())
				{
					m_sParam = Conversion.ConvertObjToStr(objReader.GetValue( "MERGE_PARAM")); 
					m_lSeqNum = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("SEQ_NUM"))); 
					objReader.Close();
				}
				sSql = "SELECT FIELD_ID,SYS_FIELD_NAME,USER_PROMPT,FIELD_TYPE, PATTERN,SUPP_TABLE_NAME,CODE_FILE_ID FROM";
				sSql = sSql + " SUPP_DICTIONARY WHERE FIELD_ID = " + p_lFieldId.ToString();
				sSql = sSql + " AND FIELD_TYPE NOT IN (7,20,14,15,16)";
				sSql = sSql + " ORDER BY USER_PROMPT";
				objReader = DbFactory.GetDbReader(m_sConnectionString,sSql);
				if (objReader.Read())
				{
					m_lFieldId = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("FIELD_ID"))); 
					m_sFieldName = Conversion.ConvertObjToStr(objReader.GetValue("SYS_FIELD_NAME")); 
					m_sFieldDesc = Conversion.ConvertObjToStr(objReader.GetValue( "USER_PROMPT")); 
					m_sFieldTable = Conversion.ConvertObjToStr(objReader.GetValue( "SUPP_TABLE_NAME")); 
					m_sOptMask = Conversion.ConvertObjToStr(objReader.GetValue( "PATTERN")); 
					m_sCodeTable = Conversion.ConvertObjToStr(objReader.GetValue( "CODE_FILE_ID")); 
					m_lSuppFieldFlag = 	1;
					m_lFieldType = 	Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("FIELD_TYPE"))); 
				}
				
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateField.LoadSuppExisting.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				sSql=null;
			}
		}
		/// Name		: AppendAggregateSuffixIfExists
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Concatenates a value with field description
		/// </summary>
		private string AppendAggregateSuffixIfExists() 
		{ 
			long lTmp=0; 
			string sValue="";
			LocalCache objCache=null;
			try
			{
                objCache = new LocalCache(m_sConnectionString, m_iClientId);
				if (this.Param == "") 
				{ 
					return this.FieldDesc; 
				} 
				lTmp = Conversion.ConvertStrToLong(this.Param);
				if (lTmp == 0)
				{
					sValue=" (ALL)";
				}
				else
				{
					sValue=" ("+objCache.GetCodeDesc(Convert.ToInt32(lTmp))+")";
					
				}
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateField.AppendAggregateSuffixIfExists.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objCache!=null)
				{
					objCache.Dispose();
					objCache=null;
				}
				sValue=null;
			}
			return this.FieldDesc+ sValue;
		}
		/// Name		: FormatValue
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Format a field value based on field type
		/// </summary>
		/// <returns>Formatted value as string</returns>
		private string FormatValue()
		{
			long lTmp=0;
			string sFname="";
			string sLname="";
			string sAbbrevation="";
			string sStateName="";
			string sTmp="";
            StringBuilder sbMEValue = new StringBuilder(); //MITS 26255 hlv 8/2/12

			LocalCache objCache=null;
             Riskmaster.Settings.SysSettings objsys = null;
			try
			{
                if (Conversion.ConvertLongToBool(this.SuppFieldFlag, m_iClientId) == true)
				{
					lTmp=this.m_lFieldType * -1;
				}
				else
				{
					lTmp=this.m_lFieldType * 1;
				}
				switch (lTmp)
				{
					case 5:
                        // akaushik5 Added for RMA-6830 Starts
                    case -31:
                        // akaushik5 Added for RMA-6830 Ends
						if (m_sRawValue.ToLower()=="false")
						{
							sTmp="No";
						}
						else if (m_sRawValue.ToLower()=="true")
						{
							sTmp="Yes";
						}
						else if (m_sRawValue !="0")
						{
							sTmp="Yes";
						}
						else
						{
							sTmp="No";
						}
						if (m_sRawValue =="")
						{
							sTmp="No";
						}
						break;
					case 3:
					case -6:
						if (!LongCodeFlag)
						{
                            objCache = new LocalCache(m_sConnectionString, m_iClientId);
							sTmp = objCache.GetShortCode(Conversion.ConvertStrToInteger(m_sRawValue));
							//Shruti, 27th oct 2006, MITS - 8147
							if(sTmp == "\" \"")
							{
								sTmp = "";
							}
							objCache.Dispose();
						}
						else
						{
                            objCache = new LocalCache(m_sConnectionString, m_iClientId);
							sTmp = objCache.GetCodeDesc(Conversion.ConvertStrToInteger(m_sRawValue));
							objCache.Dispose();
						}
						break;
					case 4:
					case -8:
                        objCache = new LocalCache(m_sConnectionString, m_iClientId);
						objCache.GetEntityInfo(Conversion.ConvertStrToLong(m_sRawValue),ref sFname,ref sLname);
                        //rsharma220 MITS 33163
						sTmp=sFname.Trim().Equals("")?sLname:sLname+", "+sFname;
						objCache.Dispose();
						break;
					case 6:
					case -9:
                        objCache = new LocalCache(m_sConnectionString, m_iClientId);
                        //Start averma62 - MITS 27337
                        if (m_sRawValue.Contains(","))
                        {
                            string[] sArr = m_sRawValue.Split(',');
                            if (sArr != null)
                            {
                                for (int i = 0; i < sArr.Length; i++)
                                {
                                    objCache.GetStateInfo(Conversion.ConvertStrToInteger(sArr[i].Trim()), ref sAbbrevation, ref sStateName);
                                    if (!LongCodeFlag)
                                    {
                                        if (!string.IsNullOrEmpty(sTmp)) sTmp = sTmp + ", " + sAbbrevation;
                                        else
                                        {
                                            sTmp = sAbbrevation;
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(sTmp)) sTmp = sTmp + ", " + sStateName;
                                        else
                                        {
                                            sTmp = sStateName;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            //End averma62 - MITS 27337
                            objCache.GetStateInfo(Conversion.ConvertStrToInteger(m_sRawValue), ref sAbbrevation, ref sStateName);

                            if (!LongCodeFlag)
                            {
                                sTmp = sAbbrevation;
                            }
                            else
                            {
                                sTmp = sStateName;
                            }
                        }//averma62 - MITS 27337

						objCache.Dispose();
						break;
                    case -2:
					case 2:
                     objsys=  new Settings.SysSettings(m_sConnectionString,m_iClientId);
                        try
                        {
                            if (objsys.UseMultiCurrency != 0)
                            {
                                string[] sArr = m_sRawValue.Split('_');
                                if (sArr != null)
                                {
                                    if (sArr.Length > 1)
                                    {
                                        int iD = int.Parse(sArr[1].Trim());
                                        if (iD > 0)
                                            sTmp = CommonFunctions.ConvertByCurrencyCode(int.Parse(sArr[1].Trim()), Conversion.ConvertStrToDouble(sArr[0].Trim()), m_sConnectionString, m_iClientId);
                                        else
                                            sTmp = CommonFunctions.ConvertByCurrencyCode(objsys.BaseCurrencyType, Conversion.ConvertStrToDouble(sArr[0].Trim()), m_sConnectionString, m_iClientId);
                                    }
                                    else
                                    {
                                        sTmp = CommonFunctions.ConvertByCurrencyCode(objsys.BaseCurrencyType, Conversion.ConvertStrToDouble(sArr[0].Trim()), m_sConnectionString, m_iClientId);
                                    }
                                }
                                else
                                {
                                    sTmp = CommonFunctions.ConvertByCurrencyCode(objsys.BaseCurrencyType, Conversion.ConvertStrToDouble(sArr[0].Trim()), m_sConnectionString, m_iClientId);
                                }
                            }
                            else
                            {
                                sTmp = CommonFunctions.ConvertByCurrencyCode(objsys.BaseCurrencyType, Conversion.ConvertStrToDouble(m_sRawValue), m_sConnectionString, m_iClientId);
                                //Base Currency at that time 
                            }
                        }
                        catch (Exception p_objException)
                        {
                            //explicitly done the check
                            //Logger to be used
                            sTmp = CommonFunctions.ConvertByCurrencyCode(objsys.BaseCurrencyType, Conversion.ConvertStrToDouble(m_sRawValue), m_sConnectionString, m_iClientId);
                        }
                        objsys = null;
						break;
					case -1:
					case 11:
						sTmp = Riskmaster.Common.Conversion.ConvertObjToStr(m_sRawValue);
						break;
					case 1:
					case 0:
					case -10:
					case -12:
					case -13:
						sTmp = Riskmaster.Common.Conversion.ConvertObjToStr(m_sRawValue);
						break;
					case 7:
					case -3:
						sTmp = Riskmaster.Common.Conversion.GetDBDateFormat(m_sRawValue,"d");
						break;
					case 9:
                        objCache = new LocalCache(m_sConnectionString,m_iClientId);
						sTmp = objCache.GetTableName(Conversion.ConvertStrToInteger(m_sRawValue));
                        objCache.Dispose();
						break;
					case 10:
					case -5:
					case -11:
						sTmp = m_sRawValue.Replace(Convert.ToChar(10).ToString() ,string.Empty).Replace(Convert.ToChar(13).ToString() ,string.Empty).Replace(Convert.ToChar(34).ToString() ,string.Empty);
						break;
					case -4:
					case 12:
						sTmp =Riskmaster.Common.Conversion.GetDBTimeFormat(m_sRawValue,"t");
						break;
					case 13:
						sTmp = Riskmaster.Common.Conversion.GetDBDTTMFormat(m_sRawValue,"d","t"); 
						break;
                    case 99://Added by amitosh for mits 27378
                        objsys = new Settings.SysSettings(m_sConnectionString,m_iClientId);
                        if (!string.IsNullOrEmpty(m_sRawValue) && objsys.MaskSSN)
                            sTmp = "###-##-####";
                        else
                            sTmp = m_sRawValue;//Added by Deb
                        break;
                    case -16: //MITS 26255 hlv 8/2/12 added for Multi-Entity
						objCache=new LocalCache(m_sConnectionString,m_iClientId);

                        string[] tmpME = m_sRawValue.Split(',');
                        sbMEValue.Remove(0, sbMEValue.Length);
                        for (int i = 0; i < tmpME.Length; i++)
                        {
                            objCache.GetEntityInfo(Conversion.ConvertStrToLong(tmpME[i]), ref sFname, ref sLname);
                            sbMEValue.Append(sFname.Trim().Equals("") ? sLname + "; " : sFname + ", " + sLname + "; ");
                        }

                        if (sbMEValue.Length > 0) { sbMEValue.Remove(sbMEValue.Length - 2, 2); }
                        sTmp = sbMEValue.ToString();
                        //objCache.GetEntityInfo(Conversion.ConvertStrToLong(m_sRawValue), ref sFname, ref sLname);
						//sTmp=sFname.Trim().Equals("")?sLname:sFname+", "+sLname;
						objCache.Dispose();
                        break;
					default:
						sTmp =m_sRawValue;
						break;
									
				}
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateField.FormatValue.Error",m_iClientId),p_objException);
			}
			finally
			{
				if (objCache!=null)
				{
					objCache.Dispose();
					objCache=null;
				}
				sFname=null;
				sLname=null;
				sAbbrevation=null;
				sStateName=null;
                if (objsys != null)
                    objsys = null;
			}
			return sTmp;
			
		}
		/// Name		: IsNew
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function checks if field is new or not
		/// </summary>
		/// <param name="p_lFieldId">Field id</param>
		/// <param name="p_sParam">param</param>
		/// <returns>True id field is new else False</returns>
		private bool IsNew(long p_lFieldId,string p_sParam)
		{
			long lReturn=0;
			try
			{
				
				lReturn=Generic.GetSingleLong("-1", "MERGE_FORM_DEF", "FIELD_ID=" + p_lFieldId.ToString() + " AND MERGE_PARAM=" + Utilities.FormatSqlFieldValue(p_sParam) + " AND FORM_ID=" + this.FormId.ToString(),m_sConnectionString, m_iClientId);
                return !Conversion.ConvertLongToBool(lReturn, m_iClientId);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("TemplateField.IsNew.Error",m_iClientId),p_objException);
			}
		}
		#endregion
	}
}
