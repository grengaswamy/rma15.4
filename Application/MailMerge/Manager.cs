using System;

namespace Riskmaster.Application.MailMerge
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   29th November 2004
	///Purpose :   This class is used to manage the manager information.
	/// </summary>
	public abstract class IManager
	{
		#region Abstract properties declaration
		/// <summary>
		/// Password
		/// </summary>
		public abstract string Pwd{get;set;}
		/// <summary>
		/// User id
		/// </summary>
		public abstract string UID{get;set;}
		/// <summary>
		/// Path for storing templates
		/// </summary>
		public abstract string TemplatePath{get;set;}
		/// <summary>
		/// Connection string to database
		/// </summary>
		public abstract string DSN{get;set;}
		/// <summary>
		/// Document storage path
		/// </summary>
		public abstract string DocStorageDSN{get;set;}
		/// <summary>
		/// Document storage path i.e filesystem/database
		/// </summary>
		public abstract long DocPathType{get;set;}
		#endregion
	}
}
