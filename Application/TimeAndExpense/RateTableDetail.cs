using System;

namespace Riskmaster.Application.TimeExpanse
{
	/// <summary>
	/// Author  :   Anurag Agarwal
	/// Dated   :   19 Nov 2004 
	/// Purpose :   Represents a Rate Table Detail Item.  
	/// </summary>
	internal class RateTableDetail
	{
	 
		#region Constructors

		/// <summary>
		/// Default Constructor
		/// </summary>
		public RateTableDetail()
		{
		}

		#endregion

		#region Variable Declaration

		/// <summary>
		/// Private variable for Rate Table Detail row Id	
		/// </summary>
		private long m_lId = 0;

		/// <summary>
		/// Private variable for Rate Table Id	
		/// </summary>
		private long m_lTableId = 0;

		/// <summary>
		/// Private variable for Trans Type	
		/// </summary>
		private string m_sTransType = string.Empty;

		///<summary>
		/// Private variable for Trans Type	
		/// </summary>
		private long m_lTransTypeCode = 0;

		/// <summary>
		/// Private variable for Unit
		/// </summary>
		private string m_sUnit = string.Empty;

		/// <summary>
		/// Private variable for Rate	
		/// </summary>
		private double m_dRate = 0.0;

		/// <summary>
		/// Private variable for User
		/// </summary>
		private string m_sUser = string.Empty;

		/// <summary>
		/// Private variable for Date Time
		/// </summary>
		private string m_sDateTime = string.Empty;

		#endregion

		#region Properties
		
		/// <summary>
		/// Read/Write property for Date Time.
		/// </summary>
		internal string DateTime  
		{
			get
			{
				return m_sDateTime;
			}
			set
			{
				m_sDateTime = value ;
			}
		}

		/// <summary>
		/// Read/Write property for User.
		/// </summary>
		internal string User  
		{
			get
			{
				return m_sUser;
			}
			set
			{
				m_sUser = value ;
			}
		}

		/// <summary>
		/// Read/Write property for rate.
		/// </summary>
		internal double Rate  
		{
			get
			{
				return m_dRate;
			}
			set
			{
				m_dRate = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Unit.
		/// </summary>
		internal string Unit  
		{
			get
			{
				return m_sUnit;
			}
			set
			{
				m_sUnit = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Trans Code.
		/// </summary>
		internal long TransTypeCode  
		{
			get
			{
				return m_lTransTypeCode;
			}
			set
			{
				m_lTransTypeCode = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Trans Type.
		/// </summary>
		internal string TransType  
		{
			get
			{
				return m_sTransType;
			}
			set
			{
				m_sTransType = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Table ID.
		/// </summary>
		internal long TableId
		{
			get
			{
				return m_lTableId;
			}
			set
			{
				m_lTableId = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Rate Table Detail ID.
		/// </summary>
		internal long Id
		{
			get
			{
				return m_lId ;
			}
			set
			{
				m_lId = value ;
			}
		}

		#endregion

	}
}
