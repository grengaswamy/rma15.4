using System;
using System.Collections ;

namespace Riskmaster.Application.TimeExpanse
{
	/// <summary>
	/// Summary description for RateTableDetails.
	/// </summary>
	internal class RateTableDetails : CollectionBase
	{
		public RateTableDetails()
		{
			
		}

		/// <summary>
		/// Add an item in the list.
		/// </summary>
		/// <param name="p_objRateTableDetail">Financial Summary Item</param>
		/// <returns>Position on which the item is inserted.</returns>
		internal int Add(RateTableDetail p_objRateTableDetail)
		{
			return List.Add( p_objRateTableDetail );			
		}		


		/// <summary>
		/// Remove the item form the list.
		/// </summary>
		/// <param name="p_objRateTableDetail">Financial Summary Item</param>
		internal void Remove( RateTableDetail p_objRateTableDetail )
		{
			List.Remove( p_objRateTableDetail );
		} 

		/// <summary>
		/// Check the existence of the item in the list.
		/// </summary>
		/// <param name="p_objRateTableDetail">Financial Summary Item</param>
		/// <returns>Return the status of the item. "True" if item is in the list.</returns>
		internal bool Contains( RateTableDetail p_objRateTableDetail )
		{
			return List.Contains( p_objRateTableDetail );
		}
		/// <summary>
		/// Get the index of the item in the list.
		/// </summary>
		/// <param name="p_objRateTableDetail">Financial Summary Item</param>
		/// <returns>Return the integer index.</returns>
		internal int IndexOf( RateTableDetail p_objRateTableDetail )
		{
			return List.IndexOf( p_objRateTableDetail );
		}

		/// <summary>
		/// Indexing for RateTableDetail
		/// </summary>
		internal RateTableDetail this[int p_iIndex]
		{
			get { return ( RateTableDetail )List[p_iIndex]; }
			set { List[p_iIndex] = value; }
		}

	}
}
