using System;

namespace Riskmaster.Application.TimeExpanse
{
	/// <summary>
	/// Author  :   Anurag Agarwal
	/// Dated   :   19 Nov 2004 
	/// Purpose :   Represents a InvoiceDetail Item.  
	/// </summary>
	internal class InvoiceDetail
	{
		
		#region Constructors

		/// <summary>
		/// Default Constructor
		/// </summary>
		public InvoiceDetail()
		{
		}

		#endregion

		#region Variable Declaration

		/// <summary>
		/// Private variable for InvDetail row Id	
		/// </summary>
		private long m_lId = 0;

		/// <summary>
		/// Private variable for Acc Rec Detail Id	
		/// </summary>
		private long m_lARId = 0;

		/// <summary>
		/// Private variable for Trans Type	
		/// </summary>
		private string m_sTransType = string.Empty;

		/// <summary>
		/// Private variable for Reserve Type	
		/// </summary>
		private string m_sReserveType = string.Empty;

		/// <summary>
		/// Private variable for Reserve Type Code	
		/// </summary>
		private int m_iReserveTypeCode = 0;

		/// <summary>
		/// Private variable for Invoice Number	
		/// </summary>
		private string m_sInvoiceNumber = string.Empty;

		/// <summary>
		/// Private variable for Item Amount	
		/// </summary>
		private string m_sItemAmount = string.Empty;

		/// <summary>
		/// Private variable for From Date
		/// </summary>
		private string m_sFromDate = string.Empty;

		/// <summary>
		/// Private variable for To Date
		/// </summary>
		private string m_sToDate = string.Empty;

		/// <summary>
		/// Private variable for Reason
		/// </summary>
		private string m_sReason = string.Empty;

		/// <summary>
		/// Private variable for Post Table	
		/// </summary>
		private bool m_bPostable = false;
		
		/// <summary>
		/// Private variable for Quantity	
		/// </summary>
		private double m_dQuantity = 0.0;

		/// <summary>
		/// Private variable for Unit
		/// </summary>
		private string m_sUnit = string.Empty;

		/// <summary>
		/// Private variable for Rate	
		/// </summary>
		private double m_dRate = 0.0;

		/// <summary>
		/// Private variable for Total	
		/// </summary>
		private double m_dTotal = 0.0;

		/// <summary>
		/// Private variable for Invoice Total	
		/// </summary>
		private double m_dInvoiceTotal = 0.0;

		/// <summary>
		/// Private variable for Override	
		/// </summary>
		private bool m_bOverride = false;

		/// <summary>
		/// Private variable for IsBillable	
		/// </summary>
		private bool m_bIsBillable = false;

		/// <summary>
		/// Private variable for Non Bill Reason Code
		/// </summary>
		private string m_sNonBillReasonCode = string.Empty;

		/// <summary>
		/// Private variable for Non Bill Reason
		/// </summary>
		private long m_lNonBillReason = 0;

		/// <summary>
		/// Private variable for comment
		/// </summary>
		private string m_sComment = string.Empty;

		/// <summary>
		/// Private variable for PreComment
		/// </summary>
		private string m_sPreComment = string.Empty;

		/// <summary>
		/// Private variable for Trans Code
		/// </summary>
		private int m_iTransCode = 0;

		/// <summary>
		/// Private variable for Bill Amount	
		/// </summary>
		private double m_dBillAmount = 0.0;

        //abansal23: MITS 18850 - Invoice amount and Inv By field is vacant 11/30/2009 Starts
        /// <summary>
        /// Private variable for Update  By User
        /// </summary>
        private string m_UpdatedByUser = string.Empty;
        //abansal23: MITS 18850 - Invoice amount and Inv By field is vacant 11/30/2009 Ends

		#endregion

		#region Properties
		
		/// <summary>
		/// Read/Write property for Bill Amount.
		/// </summary>
		internal double BillAmount  
		{
			get
			{
				return m_dBillAmount;
			}
			set
			{
				m_dBillAmount = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Reserve Type Code.
		/// </summary>
		internal int ReserveTypeCode  
		{
			get
			{
				return m_iReserveTypeCode;
			}
			set
			{
				m_iReserveTypeCode = value ;
			}
		}	

		/// <summary>
		/// Read/Write property for Trans Code.
		/// </summary>
		internal int TransCode  
		{
			get
			{
				return m_iTransCode;
			}
			set
			{
				m_iTransCode = value ;
			}
		}

		/// <summary>
		/// Read/Write property for NonBill Reason.
		/// </summary>
		internal long NonBillReason  
		{
			get
			{
				return m_lNonBillReason;
			}
			set
			{
				m_lNonBillReason = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Pre Comment.
		/// </summary>
		internal string PreComment  
		{
			get
			{
				return m_sPreComment;
			}
			set
			{
				m_sPreComment = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Comment.
		/// </summary>
		internal string Comment  
		{
			get
			{
				return m_sComment;
			}
			set
			{
				m_sComment = value ;
			}
		}

		/// <summary>
		/// Read/Write property for NonBill Reason Code.
		/// </summary>
		internal string NonBillReasonCode  
		{
			get
			{
				return m_sNonBillReasonCode;
			}
			set
			{
				m_sNonBillReasonCode = value ;
			}
		}

		/// <summary>
		/// Read/Write property for IsBillable.
		/// </summary>
		internal bool IsBillable  
		{
			get
			{
				return m_bIsBillable;
			}
			set
			{
				m_bIsBillable = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Override.
		/// </summary>
		internal bool Override  
		{
			get
			{
				return m_bOverride;
			}
			set
			{
				m_bOverride = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Invoice Total.
		/// </summary>
		internal double InvoiceTotal  
		{
			get
			{
				return m_dInvoiceTotal;
			}
			set
			{
				m_dInvoiceTotal = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Total.
		/// </summary>
		internal double Total  
		{
			get
			{
				return m_dTotal;
			}
			set
			{
				m_dTotal = value ;
			}
		}

		/// <summary>
		/// Read/Write property for rate.
		/// </summary>
		internal double Rate  
		{
			get
			{
				return m_dRate;
			}
			set
			{
				m_dRate = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Unit.
		/// </summary>
		internal string Unit  
		{
			get
			{
				return m_sUnit;
			}
			set
			{
				m_sUnit = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Quantity.
		/// </summary>
		internal double Quantity  
		{
			get
			{
				return m_dQuantity;
			}
			set
			{
				m_dQuantity = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Post Table.
		/// </summary>
		internal bool Postable  
		{
			get
			{
				return m_bPostable ;
			}
			set
			{
				m_bPostable = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Reason.
		/// </summary>
		internal string Reason  
		{
			get
			{
				return m_sReason;
			}
			set
			{
				m_sReason = value ;
			}
		}

		/// <summary>
		/// Read/Write property for To Date.
		/// </summary>
		internal string ToDate  
		{
			get
			{
				return m_sToDate;
			}
			set
			{
				m_sToDate = value ;
			}
		}

		/// <summary>
		/// Read/Write property for From Date.
		/// </summary>
		internal string FromDate  
		{
			get
			{
				return m_sFromDate;
			}
			set
			{
				m_sFromDate = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Item Amount.
		/// </summary>
		internal string ItemAmount  
		{
			get
			{
				return m_sItemAmount;
			}
			set
			{
				m_sItemAmount = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Invoice Number.
		/// </summary>
		internal string InvoiceNumber  
		{
			get
			{
				return m_sInvoiceNumber;
			}
			set
			{
				m_sInvoiceNumber = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Reserve Type.
		/// </summary>
		internal string ReserveType  
		{
			get
			{
				return m_sReserveType;
			}
			set
			{
				m_sReserveType = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Trans Type.
		/// </summary>
		internal string TransType  
		{
			get
			{
				return m_sTransType;
			}
			set
			{
				m_sTransType = value ;
			}
		}

		/// <summary>
		/// Read/Write property for ACC_Rec_Detail ID.
		/// </summary>
		internal long ARId
		{
			get
			{
				return m_lARId ;
			}
			set
			{
				m_lARId = value ;
			}
		}

		/// <summary>
		/// Read/Write property for Invoice Detail ID.
		/// </summary>
		internal long Id
		{
			get
			{
				return m_lId ;
			}
			set
			{
				m_lId = value ;
			}
		}

        //abansal23: MITS 18850 - Invoice amount and Inv By field is vacant 11/30/2009 Starts
        /// <summary>
        /// Read/Write property for Update By User
        /// </summary>
        internal string UpdatedByUser
        {
            get
            {
                return m_UpdatedByUser;
            }
            set
            {
                m_UpdatedByUser = value;
            }
        }
        //abansal23: MITS 18850 - Invoice amount and Inv By field is vacant 11/30/2009 Ends

		#endregion

	}
}
