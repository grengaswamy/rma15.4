﻿
using System;
using System.Collections;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

using Riskmaster.Application.FundManagement;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Settings;
using Riskmaster.Application.VSSInterface;

namespace Riskmaster.Application.TimeExpanse
{
    /// <summary>
    ///Author  :   Anurag Agarwal
    ///Dated   :   23 November 2004
    ///Purpose :   This class Creates, Edits, Fetches & Voids the Invoice & RateTable information 
    /// </summary>
    public class InvoiceDriver : IDisposable
    {
        #region "Constructor & Destructor"

        /// <summary>
        ///	Constructor, initializes the variables to the default value
        /// </summary>
        /// <param name="p_sUserName">UserName</param>
        /// <param name="p_sPassword">Password</param>
        /// <param name="p_sDsnName">DsnName</param>
        /// <param name="p_iUserId">User Id</param>
        /// <param name="p_iUserGroupId">User Group Id</param>		
        public InvoiceDriver(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iUserId, int p_iUserGroupId, int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iUserId = p_iUserId;
            m_iGroupId = p_iUserGroupId;
            m_iClientId = p_iClientId; //Ash - cloud
            this.Initialize();            
        }
        /// <summary>
        /// Destructor
        /// </summary>
        ~InvoiceDriver()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (m_objDataModelFactory != null)
            {
                m_objDataModelFactory.Dispose();
            }
            m_objXmlDocument = null;
        }

        #endregion

        #region "Member Variables"
        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = String.Empty;

        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = String.Empty;

        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = String.Empty;

        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;

        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database.
        /// </summary>
        private string m_sConnectionString = String.Empty;

        /// <summary>
        ///Represents the login name of the Logged in User
        /// </summary>
        private string m_sLoginName = String.Empty;

        /// <summary>
        ///Date time set at the time of Create Invoice
        /// </summary>
        private string m_sInvoiceDateTime = String.Empty;

        /// <summary>
        /// Represents the User Id.
        /// </summary>
        private int m_iUserId = 0;

        /// <summary>
        /// The Group id 
        /// </summary>
        private int m_iGroupId = 0;


        //abansal23 MITS 18880 : Transaction Types display in oracle db ON 12/9/2009 Starts
        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database.
        /// </summary>
        private static string m_sDBType = string.Empty;
        //abansal23 MITS 18880 : Transaction Types display in oracle db ON 12/9/2009 Ends


        /// <summary>
        /// Represents the Id variant.
        /// </summary>
        private long m_lId = 0;

        /// <summary>
        /// Represents the Rate Code.
        /// </summary>
        private int m_iRateCode = 0;

        /// <summary>
        /// Represents the Claim ID.
        /// </summary>
        private int m_iClaimId = 0;

        /// <summary>
        /// Represents the date stamped at the time of creation of invoice
        /// </summary>
        private string m_sDate = string.Empty;

        /// <summary>
        /// Represents the datetime stamped at the time of creation of invoice
        /// </summary>
        private string m_sDateTime = string.Empty;

        /// <summary>
        /// Boolean which indicates whether AllowClosed claims or not
        /// </summary>
        private bool m_bAllowClosedClaim = false;

        /// <summary>
        /// XML Document
        /// </summary>
        private XmlDocument m_objXmlDocument = null;

        #region "Struct ClientDetails"
        /// Name		: ClientDetails
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Structure to hold the client details 
        /// </summary>
        struct ClientDetails
        {
            public string Name;
            public string Address1;
            public string Address2;
            public string Address3;
            public string Address4;
            public string City;
            public string State;
            public string ZipCode;
            public string PhoneNumber;
            public string FaxNumber;
        }
        #endregion

        /// <summary>
        /// Structure for holding the client details
        /// </summary>
        private ClientDetails m_stClientDetails;

        /// <summary>
        /// Boolean for Checking whether operation should be continue in case of error
        /// </summary>
        private bool m_bIsContinue = false;

        /// <summary>
        /// Invoice Ids as comma seperated
        /// </summary>
        private string m_sInvoiceIds = string.Empty;

        /// <summary>
        /// Field for Post Id
        /// </summary>
        private int iPostId = 0;

        /// <summary>
        /// Field for Message Settings
        /// </summary>
        //Ash - cloud, config settings moved to DB
        //Hashtable m_MessageSettings = RMConfigurationSettings.GetRMMessages();
        private Hashtable m_MessageSettings = null;

        /// <summary>
        /// ClientID for multi-tenancy, cloud
        /// </summary>
        private int m_iClientId = 0;

        #endregion

        #region "Constants declaration"

        /// <summary>
        /// Type of field is Radio
        /// </summary>
        public const string RADIO_FLDTYPE = "radio";

        /// <summary>
        /// Type of control is Text Box
        /// </summary>
        public const string TEXT_CONTROL = "text";

        /// <summary>
        /// Type of field is boolean
        /// </summary>
        public const string BOOL_FLDTYPE = "checkbox";

        /// <summary>
        /// Type of field is date
        /// </summary>
        public const string DATE_FLDTYPE = "date";

        /// <summary>
        /// Type of field is Combo Box
        /// </summary>
        public const string COMBOBOX_FLDTYPE = "combobox";

        /// <summary>
        /// Type of object is Invoice Detail
        /// </summary>
        public const string INVOICE_DETAIL = "invDetail";

        /// <summary>
        /// Type of Object is rate table
        /// </summary>
        public const string RATE_TABLE = "ratetable";

        /// <summary>
        /// Type of field is Text And list
        /// </summary>
        public const string TEXTANDLIST = "textandlist";

        /// <summary>
        /// Type of field is 
        /// </summary>
        public const string EIN_FLDTYPE = "ein";

        /// <summary>
        /// Type of field is Memo
        /// </summary>
        public const string MEMO_FLDTYPE = "memo";

        /// <summary>
        /// Type of field is TextMl
        /// </summary>
        public const string TEXTML_FLDTYPE = "textml";

        /// <summary>
        /// Const for ATT ID
        /// </summary>
        public const string ATT_ID = "id";

        /// <summary>
        /// Type of field is SSN
        /// </summary>
        public const string SSN_FLDTYPE = "ssn";

        /// <summary>
        /// Type of field is ZIP
        /// </summary>
        public const string ZIP_FLDTYPE = "zip";

        /// <summary>
        /// Type of field is Tax ID
        /// </summary>
        public const string TAXID_FLDTYPE = "taxid";

        /// <summary>
        /// Const for Id Control
        /// </summary>
        public const string ID_CONTROL = "id";

        /// <summary>
        /// Const for Text label
        /// </summary>
        public const string TEXT_LABEL = "textlable";

        /// <summary>
        /// Type of field is Phone
        /// </summary>
        public const string PHONE_FLDTYPE = "phone";

        /// <summary>
        /// Type of field is Time
        /// </summary>
        public const string TIME_FLDTYPE = "time";

        /// <summary>
        /// Type of field is Date Button Script
        /// </summary>
        public const string DATEBSCRIPT_FLDTYPE = "datebuttonscript";

        #endregion

        #region Property declaration

        /// <summary>
        /// Gets & sets the user id
        /// </summary>
        public int UserId { get { return m_iUserId; } set { m_iUserId = value; } }

        /// <summary>
        /// Set the Login Name
        /// </summary>
        public string LoginName { get { return m_sLoginName; } set { m_sLoginName = value; } }

        /// <summary>
        /// Set the Group Id Property
        /// </summary>
        public int GroupId { get { return m_iGroupId; } set { m_iGroupId = value; } }

        #endregion

        #region "Public Functions"

        #region "GetXmlforInvoiceDetail(string p_sInputXml, long p_lInvoiceDetailId, long p_lRateCode, int p_iIsAllocated)"
        /// Name		: GetXmlforInvoiceDetail
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 03/03/2005     *   Return type from String to XmlDocument *	Mohit Yadav
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Gets the InvoiceDetail in XML format
        /// </summary>
        /// <param name="p_sInputXml">Input XML</param>
        /// <param name="p_lInvoiceDetailId">Invoice Detail Id</param>
        /// <param name="p_lRateCode">Rate Code</param>
        /// <param name="p_iIsAllocated">Value for Allocated</param>
        /// <returns>XML for InvoiceDetail</returns>
        public XmlDocument GetXmlforInvoiceDetail(string p_sInputXml, long p_lInvoiceDetailId, long p_lRateCode, int p_iIsAllocated)

            //**************************************************************
        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
        //
        //To return XML rather then String
        //Initially:- public string GetXmlforInvoiceDetail(string p_sInputXml, long p_lInvoiceDetailId, long p_lRateCode, int p_iIsAllocated)
        //Finally:- public XMLDocument GetXmlforInvoiceDetail(string p_sInputXml, long p_lInvoiceDetailId, long p_lRateCode, int p_iIsAllocated)
        //
        //*************************************************************
        {
            string strTemp = String.Empty;

            XmlDocument objXMLDoc = null;
            XmlDocument objInputXMLDoc = null;
            XmlElement objXMLElement = null;
            XmlElement objInvDetailXMLEle = null;
            XmlElement objTrackIdXMLEle = null;

            try
            {
                objInputXMLDoc = new XmlDocument();
                objInputXMLDoc.LoadXml(p_sInputXml);
                m_objXmlDocument = objInputXMLDoc;

                objXMLDoc = new XmlDocument();

                objInvDetailXMLEle = (XmlElement)m_objXmlDocument.SelectSingleNode("//group/control[@name='invDetail']");
                objTrackIdXMLEle = (XmlElement)m_objXmlDocument.SelectSingleNode("//group/control[@name='invDetail']/option[@trackid=" + p_lInvoiceDetailId.ToString() + "]");

                objXMLElement = objXMLDoc.CreateElement("invdet");

                objXMLDoc.AppendChild(objXMLElement);

                objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("maxtrackid", "value", objInvDetailXMLEle.GetAttribute("maxtrackid")), false));
                objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("ratecode", "value", p_lRateCode.ToString()), false));
                objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("isallocated", "value", p_iIsAllocated.ToString()), false));

                if (objTrackIdXMLEle != null)
                {
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("trackid", "value", objTrackIdXMLEle.GetAttribute("trackid")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("reason", "value", objTrackIdXMLEle.GetAttribute("reason")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("id", "value", objTrackIdXMLEle.GetAttribute("id")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("transtype", "value", objTrackIdXMLEle.GetAttribute("transtype")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("transtypecode", "value", objTrackIdXMLEle.GetAttribute("transtypecode")), false));
                    strTemp = objTrackIdXMLEle.GetAttribute("invamt");
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("invamt", "value", (strTemp.IndexOf('$') == 0 ? strTemp.Substring(1) : strTemp).Replace(",", string.Empty)), false));
                    strTemp = objTrackIdXMLEle.GetAttribute("billamt");
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("billamt", "value", (strTemp.IndexOf('$') == 0 ? strTemp.Substring(1) : strTemp).Replace(",", string.Empty)), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("billable", "value", objTrackIdXMLEle.GetAttribute("billable")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("reservetype", "value", objTrackIdXMLEle.GetAttribute("reservetype")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("reservetypecode", "value", objTrackIdXMLEle.GetAttribute("reservetypecode")), false));
                    strTemp = objTrackIdXMLEle.GetAttribute("invtot");
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("invtot", "value", (strTemp.IndexOf('$') == 0 ? strTemp.Substring(1) : strTemp).Replace(",", string.Empty)), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("invnum", "value", objTrackIdXMLEle.GetAttribute("invnum")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("fromdate", "value", objTrackIdXMLEle.GetAttribute("fromdate")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("todate", "value", objTrackIdXMLEle.GetAttribute("todate")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("unit", "value", objTrackIdXMLEle.GetAttribute("unit")), false));
                    strTemp = objTrackIdXMLEle.GetAttribute("rate");
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("rate", "value", (strTemp.IndexOf('$') == 0 ? strTemp.Substring(1) : strTemp).Replace(",", string.Empty)), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("override", "value", objTrackIdXMLEle.GetAttribute("override")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("nonbillreason", "value", objTrackIdXMLEle.GetAttribute("nonbillreason")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("nonbillreasoncode", "value", objTrackIdXMLEle.GetAttribute("nonbillreasoncode")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("comment", "value", objTrackIdXMLEle.GetAttribute("comment")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("precomment", "value", objTrackIdXMLEle.GetAttribute("precomment")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("qty", "value", objTrackIdXMLEle.GetAttribute("qty")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("pid", "value", objTrackIdXMLEle.GetAttribute("pid")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("postable", "value", objTrackIdXMLEle.GetAttribute("postable")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("status", "value", objTrackIdXMLEle.GetAttribute("status")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("invtotavail", "value", ""), false));
                }

                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //
                //To return XML rather then String
                //Initially:- return objXMLDoc.OuterXml;
                //Finally:- return objXMLDoc;
                //
                //*************************************************************

                return objXMLDoc;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetXmlforInvoiceDetail.ErrorGet", m_iClientId), p_objException);
            }
            finally
            {
                objXMLDoc = null;
                objInputXMLDoc = null;
                objXMLElement = null;
                objTrackIdXMLEle = null;
                objInvDetailXMLEle = null;
            }
        }
        #endregion

        #region "GetXmlforRatetabledetail(string p_sInputXml, long p_lRateId)"
        /// Name		: GetXmlforRatetabledetail
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment							  *    Author
        /// 03/03/2005     *   Return type from String to XmlDocument *	Mohit Yadav
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Gets the RatetableDetail as XML
        /// </summary>
        /// <param name="p_sInputXml">Input Empty XML</param>
        /// <param name="p_lRateId">Rate Id</param>
        /// <returns>XML for RateTableDetail</returns>
        public XmlDocument GetXmlforRatetabledetail(string p_sInputXml, long p_lRateId)

            //**************************************************************
        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
        //
        //To return XML rather then String
        //Initially:- public string GetXmlforRatetabledetail(string p_sInputXml, long p_lRateId)
        //Finally:- public XmlDocument GetXmlforRatetabledetail(string p_sInputXml, long p_lRateId)
        //
        //*************************************************************
        {
            string strTemp = String.Empty;

            XmlDocument objXMLDoc = null;
            XmlDocument objInputXMLDoc = null;
            XmlElement objXMLElement = null;
            XmlElement objInvDetailXMLEle = null;
            XmlElement objTrackIdXMLEle = null;

            try
            {
                objInputXMLDoc = new XmlDocument();
                objInputXMLDoc.LoadXml(p_sInputXml);
                m_objXmlDocument = objInputXMLDoc;

                objXMLDoc = new XmlDocument();

                objInvDetailXMLEle = (XmlElement)m_objXmlDocument.SelectSingleNode("//group/control[@name='ratetable']");
                objTrackIdXMLEle = (XmlElement)m_objXmlDocument.SelectSingleNode("//group/control/option[@trackid=" + p_lRateId.ToString() + "]");

                objXMLElement = objXMLDoc.CreateElement("ratetable");

                objXMLDoc.AppendChild(objXMLElement);

                objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("maxtrackid", "value", objInvDetailXMLEle.GetAttribute("maxtrackid")), false));

                if (objTrackIdXMLEle != null)
                {
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("trackid", "value", p_lRateId.ToString()), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("transtype", "value", objTrackIdXMLEle.GetAttribute("transtype")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("transtypecode", "value", objTrackIdXMLEle.GetAttribute("transtypecode")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("unit", "value", objTrackIdXMLEle.GetAttribute("unit")), false));
                    strTemp = objTrackIdXMLEle.GetAttribute("rate");
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("rate", "value", (strTemp.IndexOf('$') == 0 ? strTemp.Substring(1) : strTemp).Replace(",", string.Empty)), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("user", "value", objTrackIdXMLEle.GetAttribute("user")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("datetime", "value", objTrackIdXMLEle.GetAttribute("datetime")), false));
                    objXMLElement.AppendChild(objXMLDoc.ImportNode(GetNewEleWithAttr("status", "value", objTrackIdXMLEle.GetAttribute("status")), false));
                }

                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //
                //To return XML rather then String
                //Initially:- return objXMLDoc.OuterXml;
                //Finally:- return objXMLDoc;
                //
                //*************************************************************

                return objXMLDoc;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetXmlforRatetabledetail.ErrorGet", m_iClientId), p_objException);
            }
            finally
            {
                objXMLDoc = null;
                objInputXMLDoc = null;
                objXMLElement = null;
                objTrackIdXMLEle = null;
                objInvDetailXMLEle = null;
            }
        }
        #endregion

        #region "SaveInvoiceXml(string p_sInputXML, bool p_bAllowClosedClaim)"
        /// Name		: SaveInvoiceXml
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 04/03/2005     * Return type from String to XmlDocument	* Mohit Yadav
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Saves the Invoice. 
        /// </summary>
        /// <param name="p_sInputXML">XML containing the Invoice Details</param>
        /// <param name="p_bAllowClosedClaim">Flag for AllowingClosedClaims</param>
        /// <returns>XML containing success/failure messages</returns>
        public XmlDocument SaveInvoiceXml(string p_sInputXML, bool p_bAllowClosedClaim)
        {
            int iInvoiceId = 0;
            string sSQL = string.Empty;
            DbReader objReader = null;
            XmlNodeList objXMLNodeList = null;
            DbTransaction objTrans = null;
            AcctRec objAcctRec = null;
            Funds objFunds = null;
            //rupal
            FundsTransSplit objFundsTransSplit = null;

            try
            {
                if (m_objDataModelFactory == null)
                {
                    this.Initialize();
                }

                m_objXmlDocument.LoadXml(p_sInputXML);

                if (m_objXmlDocument != null)
                {
                    m_bAllowClosedClaim = p_bAllowClosedClaim;
                    objXMLNodeList = m_objXmlDocument.GetElementsByTagName("control");

                    objAcctRec = (AcctRec)m_objDataModelFactory.GetDataModelObject("AcctRec", false);
                    objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);

                    string sUpdateMode = m_objXmlDocument.SelectSingleNode("//control[@name='mode']").Attributes.GetNamedItem("value").Value;
                    CreateUpdateInvoice(objXMLNodeList, sUpdateMode, ref objAcctRec, ref objFunds);
                    iInvoiceId = objAcctRec.ArRowId;

                    //Delete FundsTransSplit records first. In the early version of RMX Before R5), the 
                    //SplitRowId value for AcctRecDetail object is not set correctly. We need to delete 
                    //those records and recreated again.
                    int iSplitRowId = 0;
                    bool bSplitDeleted = false;
                    bool bInvoiceDetailDeleted = false;

                    foreach (AcctRecDetail oAcctRecDetail in objAcctRec.AcctRecDetailList)
                    {
                        iSplitRowId = oAcctRecDetail.SplitRowId;
                        if (iSplitRowId == 0)
                        {
                            foreach (FundsTransSplit oFundsTransSplit in objFunds.TransSplitList)
                            {
                                objFunds.TransSplitList.Remove(oFundsTransSplit.SplitRowId);
                                bSplitDeleted = true;
                            }
                            break;
                        }
                    }

                    //Delete those removed records from the database
                    foreach (AcctRecDetail oAcctRecDetail in objAcctRec.AcctRecDetailList)
                    {
                        int iAcctRecDetailId = oAcctRecDetail.ArDetailRowId;
                        XmlNode oDetailNode = m_objXmlDocument.SelectSingleNode("//control/option[@id='" + iAcctRecDetailId + "']");
                        if (oDetailNode == null)
                        {
                            iSplitRowId = oAcctRecDetail.SplitRowId;
                            if (iSplitRowId != 0)
                            {
                                objFunds.TransSplitList.Remove(iSplitRowId);
                                bSplitDeleted = true;
                            }
                            objAcctRec.AcctRecDetailList.Remove(iAcctRecDetailId);
                            bInvoiceDetailDeleted = true;
                        }
                    }

                    if (bSplitDeleted)
                    {
                        objFunds.Save();
                    }

                    if (bInvoiceDetailDeleted)
                    {
                        objAcctRec.Save();
                    }

                    string sComment = String.Empty;
                    //Claim objClaim = null;    Commented by csingh7 for Mits 17482 , Claim Comment Enhancement
                    Comment objComment = null;
                    int iCommentId;

                    if (iInvoiceId != 0)
                    {
                        objXMLNodeList = null;
                        objXMLNodeList = m_objXmlDocument.SelectNodes("//control/option");
                        objComment=(Comment)m_objDataModelFactory.GetDataModelObject("Comment", false);

                        foreach (XmlElement objXMLElement in objXMLNodeList)
                        {
                           
                            UpdateInvoiceDetails(objXMLElement, ref objAcctRec, ref objFunds);

                            sComment = objXMLElement.GetAttribute("comment");
                            if (!string.IsNullOrEmpty(sComment))
                            {
                                //if (objClaim == null)     Commented by csingh7 for Mits 17482 , Claim Comment Enhancement
                                //{
                                //    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                                //    objClaim.MoveTo(m_iClaimId);
                                //}

                                sSQL = "SELECT COMMENT_ID FROM COMMENTS_TEXT WHERE ATTACH_RECORDID = " + m_iClaimId;
                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                                if (objReader != null)
                                {
                                    if (objReader.Read())
                                    {
                                        iCommentId = Conversion.ConvertObjToInt(objReader.GetValue("COMMENT_ID"), m_iClientId);
                                        objComment.MoveTo(iCommentId);
                                        objComment.AttachTable = "CLAIM";
                                        objComment.Comments += "\n\r" + "\n\r" + sComment;
                                        objComment.HTMLComments += "<br/><br/>" + sComment;
                                    }
                                    else
                                    {                                       
                                        objComment.AttachRecordId = m_iClaimId;
                                        objComment.AttachTable = "CLAIM";
                                        objComment.Comments = "\n\r" + "\n\r" + sComment;
                                        objComment.HTMLComments = "<br/><br/>" + sComment;
                                    }
                                    objReader.Close();
                                }
                                else
                                {
                                    objComment.AttachRecordId = m_iClaimId;
                                    objComment.Comments = "\n\r" + "\n\r" + sComment;
                                    objComment.HTMLComments = "<br/><br/>" + sComment;
                                }
                                //objClaim.Comments += "\n\r" + "\n\r" + sComment;
                                //objClaim.HTMLComments += "<br/><br/>" + sComment;
                                objComment.Save();
                                //Added by csingh7 for Mits 17482 , Claim Comment Enhancement
                            }
                        }

                        //rupal:start......................................
                        //while doing changes for multicurrency i found that fundstransplit was not adde in fundstranssplit for saving
                        //because of this, funds amount was being changed everytime with single fundstrassplit which actually should be sum of all fundstranssplit
                        //so I am saving the fund record again after all individual splits are saved so that funds have the fundstranssplit available on chilepresave() event
                        int iTransId = objFunds.TransId;                        
                        objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
                        objFunds.MoveTo(iTransId);
                        if (objFunds.TransSplitList.Count > 0)
                        {
                            objFundsTransSplit = (FundsTransSplit)objFunds.TransSplitList[0];
                        }
                        if (objFundsTransSplit != null)
                        {
                            objFundsTransSplit.Save();
                        }
                        //rupal:end                              

                        //if (objClaim != null) Commented by csingh7 for Mits 17482 , Claim Comment Enhancement
                        //{
                        //    objClaim.Save();
                        //}
                    }
                }

                return m_objXmlDocument;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                if (objTrans != null)
                    objTrans.Rollback();
                throw new RMAppException(Globalization.GetString("InvoiceDriver.SaveInvoiceXml.ErrorSave", m_iClientId), p_objException);
            }
            finally
            {
                if (objTrans != null)
                    objTrans.Dispose();
                objXMLNodeList = null;
                //rupal:multicurrency
                if (objFundsTransSplit != null)
                    objFundsTransSplit.Dispose();
            }
        }
        #endregion

        #region "SaveRateTableXml(string p_sInputXML)"

        // Anjaneya MITS 12347
        /// <summary>
        /// Updates the claim table for Rate Table Id so that it comes auto selected when user moves to T&E Summary screen
        /// </summary>
        /// <param name="p_iRateTableId"></param>
        /// <param name="p_sClaimNumber"></param>
        public void UpdateRateTableForClaim(int p_iRateTableId, string p_sClaimNumber)
        {
            string sSQL = String.Empty;

            try
            {
                sSQL = "UPDATE CLAIM SET RATE_TABLE_ID = " + p_iRateTableId + " WHERE CLAIM_ID = '" + p_sClaimNumber + "'";
                m_objDataModelFactory.Context.DbConn.ExecuteNonQuery(sSQL);

            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.SaveRateTableXml.ErrorSave", m_iClientId), p_objException);
            }
        }

        /// Name		: SaveRateTableXml
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 03/04/2005     * Return type from String to XmlDocument * Mohit Yadav
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Saves a RateTable into database.
        /// </summary>
        /// <param name="p_sInputXML">Input XML containing the Ratetable details.</param>
        /// <returns>XML containing success/failure messages</returns>
        public XmlDocument SaveRateTableXml(string p_sInputXML)

            //**************************************************************
        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
        //
        //To return XML rather then String
        //Initially:- public string SaveRateTableXml(string p_sInputXML)
        //Finally:- public XmlDocument SaveRateTableXml(string p_sInputXML)
        //
        //*************************************************************
        {
            int iRateTableId = 0;
            XmlNodeList objXMLNodeList = null;
            DbTransaction objTrans = null;

            //MGaba2:MITS 16561:Start
            bool bDeleteDetail = true;
            string sSql = String.Empty;
            string sToBeDeleted = String.Empty;
            DbReader oDbReader = null;
            DbCommand objCommand = null;
            //MGaba2:MITS 16561:End

            try
            {
                if (m_objDataModelFactory == null)
                {
                    this.Initialize();
                }

                m_objXmlDocument.LoadXml(p_sInputXML);

                if (m_objXmlDocument != null)
                {
                    objXMLNodeList = m_objXmlDocument.GetElementsByTagName("control");                    
                    
                    objTrans = m_objDataModelFactory.Context.DbConn.BeginTransaction();
                    objCommand = m_objDataModelFactory.Context.DbConn.CreateCommand();
                    //MGaba2:MITS 16561:Start
                    objCommand.Transaction = objTrans;
                    objCommand.CommandType = CommandType.Text;
                    //MGaba2:MITS 16561:End
                    m_objDataModelFactory.Context.DbTrans = objTrans;

                    if (m_objXmlDocument.SelectSingleNode("//control[@name='mode']").Attributes.GetNamedItem("value").Value == "edit")
                    {//MGaba2:MITS 13140:In case of edit,table id need to be send to save table with that previous id only
                        iRateTableId = Convert.ToInt32(m_objXmlDocument.SelectSingleNode("//control[@name='tableid']").Attributes.GetNamedItem("value").Value);
                        SaveRateTable(objXMLNodeList, "edit", out iRateTableId);                        
                    }
                    else if (m_objXmlDocument.SelectSingleNode("//control[@name='mode']").Attributes.GetNamedItem("value").Value == "new")
                        SaveRateTable(objXMLNodeList, "new", out iRateTableId);

                    if (iRateTableId != 0)
                    {
                        objXMLNodeList = null;
                        objXMLNodeList = m_objXmlDocument.SelectNodes("//control/option");

                        //MGaba2:MITS 16561:Deleting those details which are not coming in the xml-in:start
                        int iCustDetailRowId = 0;
                        CustRateDetail objCustRateDetail = (CustRateDetail)m_objDataModelFactory.GetDataModelObject("CustRateDetail", false);
                        objCommand.CommandText = "SELECT CUST_RATE_DETAIL_ROW_ID FROM CUST_RATE_DETAIL WHERE RATE_TABLE_ID=" + iRateTableId;
                        oDbReader = objCommand.ExecuteReader();

                        if (oDbReader != null)
                        {
                            while (oDbReader.Read())
                            {
                                bDeleteDetail = true;
                                iCustDetailRowId = Conversion.ConvertObjToInt(oDbReader.GetValue("CUST_RATE_DETAIL_ROW_ID"), m_iClientId);
                                foreach (XmlElement objXMLElement in objXMLNodeList)
                                {
                                    if (Conversion.ConvertStrToInteger(objXMLElement.GetAttribute("trackid")) == iCustDetailRowId)
                                    {
                                        bDeleteDetail = false;
                                        break;
                                    }
                                }
                                if (bDeleteDetail == true)
                                {
                                    if (string.IsNullOrEmpty(sToBeDeleted))
                                    {
                                        sToBeDeleted = Convert.ToString(iCustDetailRowId);
                                    }
                                    else
                                    {
                                        sToBeDeleted = sToBeDeleted + "," + Convert.ToString(iCustDetailRowId);
                                    }
                                }
                            }
                        }
                        oDbReader.Close();

                        if (sToBeDeleted != string.Empty)
                        {
                            string[] arrRowIds = sToBeDeleted.Split(',');
                            for (int i = 0; i < arrRowIds.Length; i++)
                            {
                                objCustRateDetail.MoveTo(Convert.ToInt32(arrRowIds[i]));
                                objCustRateDetail.Delete();
                            }
                        }
                        //MGaba2:MITS 16561:End  

                        foreach (XmlElement objXMLElement in objXMLNodeList)
                        {
                            if (objXMLElement.GetAttribute("status") == "N" || (objXMLElement.GetAttribute("status") == "E" && objXMLElement.GetAttribute("user") == string.Empty))
                                UpdateRateTableDetails("new", objXMLElement, iRateTableId);
                            else if (objXMLElement.GetAttribute("status") == "E" && objXMLElement.GetAttribute("user") != string.Empty)
                                UpdateRateTableDetails("edit", objXMLElement, iRateTableId);
                            else if (objXMLElement.GetAttribute("status") == "U")
                                UpdateRateTableDetails("unchanged", objXMLElement, iRateTableId);
                        }

                                         
                       

                    }

                    objTrans.Commit();
                }

                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //
                //To return XML rather then String
                //Initially:- return m_objXmlDocument.OuterXml;
                //Finally:- return m_objXmlDocument;
                //
                //*************************************************************
                return m_objXmlDocument;
            }
            catch (Exception p_objException)
            {
                if (objTrans != null)
                    objTrans.Rollback();
                throw new RMAppException(Globalization.GetString("InvoiceDriver.SaveRateTableXml.ErrorSave", m_iClientId), p_objException);
            }
            finally
            {
                objTrans.Dispose();
                objXMLNodeList = null;
            }
        }
        #endregion

        #region "DelRateTable(int p_lTableId)"
        /// Name		: DelRateTable
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Deletes the entries of Rate Table
        /// </summary>
        /// <param name="p_lTableId">Table ID</param>
        /// <returns>Boolean for success/Failure</returns>
        public bool DelRateTable(int p_lTableId)
        {
            CustRate objCustRate = null;
            CustRateDetail objCustRateDetail = null; //MGaba2:MITS 13140

			try
			{
				if( m_objDataModelFactory == null )
					this.Initialize();
	
				objCustRate = (CustRate)m_objDataModelFactory.GetDataModelObject("CustRate",false);
				objCustRate.MoveTo(p_lTableId);  				
				objCustRate.Delete();
                //MGaba2:MITS 13140:while deleting a table,its details should also be deleted from cust_rate_detail:Start
                objCustRateDetail = (CustRateDetail)m_objDataModelFactory.GetDataModelObject("CustRateDetail", false);
                objCustRateDetail.KeyFieldName = "RATE_TABLE_ID";

                try
                {
                    objCustRateDetail.MoveTo(p_lTableId);
                    objCustRateDetail.Delete();
                }
                catch (Exception p_objExc)
                {
                    throw new RMAppException(Globalization.GetString("InvoiceDriver.DelRateTable.ErrorDelete", m_iClientId), p_objExc);
                }
                //MGaba2:MITS 13140:End
				return true;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("InvoiceDriver.DelRateTable.ErrorDelete", m_iClientId),p_objException);
			}
			finally
			{
				if(objCustRate != null)
					objCustRate.Dispose();
			}
		}
		#endregion

        //Function added by Nadim for r4ps2 build stability
        /// <summary>
        /// 
        /// </summary>
        /// <param name="iRatetable"></param>
        /// <param name="bFirstTime"></param>
        /// <returns></returns>
        /// //rupal:r8 multicurrency
        //public XmlDocument GetXmlDataAndDate(int iRatetable, bool bFirstTime)   //pmittal5 Mits 14066 01/06/09
        public XmlDocument GetXmlDataAndDate(int iRatetable, bool bFirstTime,int p_iClaimId)   //pmittal5 Mits 14066 01/06/09
        {
            string sSQL = string.Empty;
            XmlDocument objReturnXMLDoc = null;
            XmlElement objXMLElement = null;
            XmlElement objXMLParentEle = null;
            DbReader objReader = null;

            string sShortCodeDstrbn = "", sDescDstrbn = "";
            int iDefaultDistributionType = 0, iAccountId = 0, iSubAccountId = 0, iEFTAccount = 0, iEFTDitributionCodeId = 0;
            Account objAccount = null;
            BankAccSub objBankAccSub = null; ;

            
               // objReader.Close();
                //rupal:start, r8 multicurrency
                LocalCache objCache = null;
                SysSettings objSysSettings = null;
                try
                {
                    objCache = new LocalCache(m_sConnectionString, m_iClientId);
                    objSysSettings = new SysSettings(m_sConnectionString, m_iClientId); //Ash - cloud
                    objXMLElement = objReturnXMLDoc.CreateElement("UseMultiCurrency");
                    int iClaimCurrencyCode = 0;
                    string sShortCode = string.Empty;
                    string sDesc = string.Empty;

                    objReturnXMLDoc = new XmlDocument();
                    objXMLParentEle = objReturnXMLDoc.CreateElement("Document");
                    objReturnXMLDoc.AppendChild(objXMLParentEle);
                    objXMLElement = objReturnXMLDoc.CreateElement("TandEVerificationDates");

                    sSQL = String.Format("SELECT EFFECTIVE_DATE,EXPIRATION_DATE FROM CUST_RATE WHERE RATE_TABLE_ID={0}", iRatetable);

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            objXMLElement.SetAttribute("efdate", Conversion.GetDate(objReader["EFFECTIVE_DATE"].ToString()));
                            objXMLElement.SetAttribute("exdate", Conversion.GetDate(objReader["EXPIRATION_DATE"].ToString()));
                            objXMLParentEle.AppendChild(objXMLElement);
                        }
                        objReader.Close();
                    }

                    //pmittal5 Mits 14066 01/05/09 - For Auto Populating Current adjuster in "Work Done By"
                    if (bFirstTime)
                    {
                        // akaushik5 Changed for MITS 38324 Starts
                        //User objUser = new User(UserId);
                        User objUser = new User(UserId, this.m_iClientId);
                        // akaushik5 Changed for MITS 38324 Ends

                        objXMLElement = objReturnXMLDoc.CreateElement("CurrentAdjuster");
                        //gagnihotri MITS 18854 12/02/2009
                        if (!objSysSettings.UseEntityRole)
                        {
                            sSQL = "SELECT ENTITY_ID,FIRST_NAME,LAST_NAME FROM ENTITY WHERE FIRST_NAME = '" + objUser.FirstName +
                                   "' AND LAST_NAME = '" + objUser.LastName + "' AND ENTITY_TABLE_ID = " + GetTableId("ADJUSTERS") + " AND DELETED_FLAG = 0";
                        }
                        else
                        {
                            sSQL = "SELECT ENTITY_ID,FIRST_NAME,LAST_NAME FROM ENTITY WHERE RM_USER_ID = " + UserId + " AND DELETED_FLAG = 0";
                        }
                        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        if (objReader != null)
                        {
                            if (objReader.Read())
                            {
                                objXMLElement.SetAttribute("Id", Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_ID"), m_iClientId).ToString());
                                objXMLElement.SetAttribute("FirstName", Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")));
                                objXMLElement.SetAttribute("LastName", Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")));
                                objXMLParentEle.AppendChild(objXMLElement);
                            }
                        }
                        objUser = null;
                    /*
                    if (objSysSettings.UseMultiCurrency != 0)
                    {                        
                        iClaimCurrencyCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimId, m_sConnectionString);                        
                    }
                    else
                    {
                        iClaimCurrencyCode = CommonFunctions.GetBaseCurrencyCode(m_sConnectionString);                        
                    }
                     * */
                    iClaimCurrencyCode = CommonFunctions.GetBaseCurrencyCode(m_sConnectionString); 
                    objXMLElement.SetAttribute("value", objSysSettings.UseMultiCurrency.ToString());
                    objXMLParentEle.AppendChild(objXMLElement);

                    objXMLElement = objReturnXMLDoc.CreateElement("CurrencyType");
                    objCache.GetCodeInfo(iClaimCurrencyCode, ref sShortCode, ref sDesc);
                    objXMLElement.SetAttribute("value", sShortCode);
                    objXMLElement.SetAttribute("codeid", iClaimCurrencyCode.ToString());
                    objXMLElement.SetAttribute("culture", sDesc);
                    objXMLParentEle.AppendChild(objXMLElement);

                    //RMA-15315:aaggarwal29 start
                    sSQL = "SELECT TANDEACCT_ID,USE_SUB_ACCOUNT FROM SYS_PARMS";
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            if (Conversion.ConvertStrToInteger(objReader["USE_SUB_ACCOUNT"].ToString()) == 0)
                            {
                                iAccountId = Conversion.ConvertStrToInteger(objReader["TANDEACCT_ID"].ToString());
                                iSubAccountId = 0;
                            }
                            else
                                iSubAccountId = Conversion.ConvertStrToInteger(objReader["TANDEACCT_ID"].ToString());

                        }
                        objReader.Close();
                    }


                    if (iAccountId != 0)
                    {
                        objAccount = (Account)m_objDataModelFactory.GetDataModelObject("Account", false);
                        objAccount.MoveTo(iAccountId);
                    }
                    if (iSubAccountId != 0)
                    {
                        objBankAccSub = (BankAccSub)m_objDataModelFactory.GetDataModelObject("BankAccSub", false);
                        objBankAccSub.MoveTo(iSubAccountId);
                        iAccountId = objBankAccSub.AccountId;
                        objAccount = (Account)m_objDataModelFactory.GetDataModelObject("Account", false);
                        if (iAccountId != 0)
                            objAccount.MoveTo(iAccountId);

                    }


                    if (iAccountId == 0 && iSubAccountId == 0)
                    {
                        throw new RMAppException(GetMyError(Globalization.GetString("InvoiceDriver.CreateInvoice.ErrorNotSet", m_iClientId), 4, string.Empty, m_iClaimId, 1));
                    }

                    iEFTAccount = objAccount.EFTAccount == true ? 1 : 0;
                    iEFTDitributionCodeId = objCache.GetCodeId("EFT", "DISTRIBUTION_TYPE");
                    objXMLElement = objReturnXMLDoc.CreateElement("IsEFTAccount");
                    objXMLElement.SetAttribute("value", iEFTAccount.ToString());
                    objXMLParentEle.AppendChild(objXMLElement);

                    objXMLElement = objReturnXMLDoc.CreateElement("EFTDistributionCodeId");
                    objXMLElement.SetAttribute("value", iEFTDitributionCodeId.ToString());
                    objXMLParentEle.AppendChild(objXMLElement);



                    Object objDistributionType;

                    if (objAccount.EFTAccount)
                    {
                        iDefaultDistributionType = iEFTDitributionCodeId;
                    }
                    else
                    {
                        objDistributionType = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT DEF_DSTRBN_TYPE_CODE FROM CHECK_OPTIONS");
                        if (objDistributionType != null)
                        {
                            iDefaultDistributionType = Conversion.ConvertObjToInt(objDistributionType, m_iClientId);
                        }
                    }

                    objXMLElement = objReturnXMLDoc.CreateElement("DefaultDistributionType");
                    objXMLElement.SetAttribute("codeid", iDefaultDistributionType.ToString());
                    objCache.GetCodeInfo(iDefaultDistributionType, ref sShortCodeDstrbn, ref sDescDstrbn);
                    objXMLElement.SetAttribute("value", sShortCodeDstrbn + " " + sDescDstrbn);
                    objXMLParentEle.AppendChild(objXMLElement);

                    objXMLElement = objReturnXMLDoc.CreateElement("ManualDistributionCodeId");
                    objXMLElement.SetAttribute("value", objCache.GetCodeId("MAL", "DISTRIBUTION_TYPE").ToString());
                    objXMLParentEle.AppendChild(objXMLElement);
                    //RMA-15315:aaggarwal29 end
                }
            }
                catch (Exception ex) { throw ex; }
                finally
                {
                    if (objCache != null)
                        objCache = null;
                    if (objSysSettings != null)
                        objSysSettings = null;
                    if (objAccount != null)
                        objAccount = null;
                    if (objBankAccSub != null)
                        objBankAccSub = null;
                }

                //rupal:end
            
            //End - pmittal5
            return objReturnXMLDoc;
        }
        //Function added by Nadim for r4ps2 build stability-end

        #region "GetXmlData(string p_sFrmName, string p_sInputXml,int p_iTableId1, int p_iTableId2, int p_iTableId3)"
        /// Name		: GetXMLdata
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 08/03/2005     * Return type from String to XmlDocument  * Mohit Yadav
        /// 12/06/2007     * Changed the method signature(MITS-10682)* Arnab Mukherjee
        /// ***************************************************************************
        /// <summary>
        /// Returns the data XML for the Formname passed.
        /// </summary>
        /// <param name="p_sFrmName">Name of the form</param>
        /// <param name="p_sInputXml">Input empty XML</param>
        /// <param name="p_lTableId1">ID variant, its value depend on the basis of formname passed</param>
        /// <param name="p_lTableId2">ID variant, its value depend on the basis of formname passed</param>
        /// <param name="p_lTableId3">ID variant, its value depend on the basis of formname passed</param>
        /// <param name="p_lTableId3">Lookup string that is needed for the search criteria</param>
        /// <returns></returns>
        // public XmlDocument GetXmlData(string p_sFrmName, string p_sInputXml,int p_iTableId1, int p_iTableId2, int p_iTableId3, int iCustomerId)
        //
        //public XmlDocument GetXmlData(string p_sFrmName, string p_sInputXml, int p_iTableId1, int p_iTableId2, int p_iTableId3, string p_sLookUpText, int iCustomerId,int p_iPmtCurrCodeId)//rupal
        // akaushik5 Added ClaimNumber for RMA-19193 
        public XmlDocument GetXmlData(string p_sFrmName, string p_sInputXml, int p_iTableId1, int p_iTableId2, int p_iTableId3, string p_sLookUpText, int iCustomerId,string claimNumber)
            //**************************************************************
        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
        //
        //To return XML rather then String
        //Initially:- public string GetXmlData(string p_sFrmName, string p_sInputXml,int p_iTableId1, int p_iTableId2, int p_iTableId3)
        //Finally:- public XmlDocument GetXmlData(string p_sFrmName, string p_sInputXml,int p_iTableId1, int p_iTableId2, int p_iTableId3)
        //
        //*************************************************************

            //**************************************************************
        //Changes made by Arnab Mukherjee for BusinessAdaptor of Time And Expense.(MITS-10682)
        //
        //Added one more input parameter to the method signature
        //Initially:- public XmlDocument GetXmlData(string p_sFrmName, string p_sInputXml,int p_iTableId1, int p_iTableId2, int p_iTableId3, int iCustomerId)
        //Finally:- public XmlDocument GetXmlData(string p_sFrmName, string p_sInputXml,int p_iTableId1, int p_iTableId2, int p_iTableId3, string p_sLookUpText, int iCustomerId)
        //
        //*************************************************************
        {
            XmlDocument objReturnXMLDoc = null;

            //**************************************************************
            //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
            //
            //To return XML rather then String
            //Initially:- string sReturnXML = string.Empty;
            //Finally:- XmlDocument objReturnXMLDoc = null;
            //
            //*************************************************************
            //rupal:multicurrency
            Funds objFunds=null;
            try
            {
                objReturnXMLDoc = new XmlDocument();
                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense.
                //*************************************************************

                m_objXmlDocument.LoadXml(p_sInputXml);
                m_lId = p_iTableId2;

                switch (p_sFrmName.ToLower())
                {
                    case "invoice":
                        objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
                        try
                        {
                            objReturnXMLDoc = FillXmlForInvoice(GetInvoice(p_iTableId1, p_iTableId2, ref objFunds), p_iTableId3, objFunds);

                            //**************************************************************
                            //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                            //
                            //To return XML rather then String
                            //Initially:- sReturnXML = FillXmlForInvoice(GetInvoice(p_iTableId1, p_iTableId2), p_iTableId3);
                            //Finally:- objReturnXMLDoc = FillXmlForInvoice(GetInvoice(p_iTableId1, p_iTableId2), p_iTableId3);
                            //
                            //*******************************   ******************************
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        { objFunds = null; }

                        break;
                    case "ratetable":
                        objReturnXMLDoc = FillXmlForRateTable(GetRateTable(p_iTableId1));

                        //**************************************************************
                        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                        //
                        //To return XML rather then String
                        //Initially:- sReturnXML = FillXmlForRateTable(GetRateTable(p_iTableId1));
                        //Finally:- objReturnXMLDoc = FillXmlForRateTable(GetRateTable(p_iTableId1));
                        //
                        //*************************************************************

                        break;
                    case "ratetables":
                        objReturnXMLDoc = GetRateTablesXML();

                        //**************************************************************
                        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                        //
                        //To return XML rather then String
                        //Initially:- sReturnXML = GetRateTablesXML();
                        //Finally:- objReturnXMLDoc = GetRateTablesXML();
                        //
                        //*************************************************************

                        break;
                    case "rates":
                        objReturnXMLDoc = GetRateTablesXMLs(iCustomerId);
                        break;
                    case "ratesandunits":
                        //Arnab: MITS-10682 - Modified this case as method signature is changed
                        //objReturnXMLDoc = FillRatesAndUnits(p_iTableId1, p_iTableId2);
                        // akaushik5 Changed for RMA-19193 Starts
                        //objReturnXMLDoc = FillRatesAndUnits(p_iTableId1, p_iTableId2, p_sLookUpText);
                        objReturnXMLDoc = FillRatesAndUnits(p_iTableId1, p_iTableId2, p_sLookUpText,claimNumber);
                        // akaushik5 Changed for RMA-19193 Ends

                        //**************************************************************
                        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                        //
                        //To return XML rather then String
                        //Initially:- sReturnXML = FillRatesAndUnits(p_iTableId1, p_iTableId2);
                        //Finally:- objReturnXMLDoc = FillRatesAndUnits(p_iTableId1, p_iTableId2);
                        //
                        //*************************************************************

                        break;
                    case "tandesummery":
                        objReturnXMLDoc = GetTandESummaryXml(p_iTableId1);

                        //**************************************************************
                        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                        //
                        //To return XML rather then String
                        //Initially:- sReturnXML = GetTandESummeryXml(p_iTableId1);
                        //Finally:- objReturnXMLDoc = GetTandESummeryXml(p_iTableId1);
                        //
                        //*************************************************************

                        break;
                    case "tandebillexpsummery":
                        objReturnXMLDoc = GetBillExpSummeryXml(p_iTableId1);

                        //**************************************************************
                        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                        //
                        //To return XML rather then String
                        //Initially:- sReturnXML = GetBillExpSummeryXml(p_iTableId1);
                        //Finally:- objReturnXMLDoc = GetBillExpSummeryXml(p_iTableId1);
                        //
                        //*************************************************************

                        break;
                    case "tandedetails":
                        objReturnXMLDoc = GetTandEDetailsXml(p_iTableId1);

                        //**************************************************************
                        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                        //
                        //To return XML rather then String
                        //Initially:- sReturnXML = GetTandEDetailsXml(p_iTableId1);
                        //Finally:- objReturnXMLDoc = GetTandEDetailsXml(p_iTableId1);
                        //
                        //*************************************************************

                        break;
                }
            }
            catch (Exception p_objException)
            {
                //MITS 15546 - throw actual error rather then a generic error
                //Initially: throw new RMAppException(Globalization.GetString("InvoiceDriver.GetXmlData.ErrorGet"),p_objException);
                //  Finally: throw new RMAppException(p_objException.Message);
                throw new RMAppException(p_objException.Message);
            }
            //rupal:start,multicurrency
            finally
            {
                if (objFunds != null)
                    objFunds.Dispose();
            }
            //rupal:end
            return objReturnXMLDoc;

            //**************************************************************
            //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
            //
            //To return XML rather then String
            //Initially:- return sReturnXML;
            //Finally:- return objReturnXMLDoc;
            //
            //*************************************************************
        }
        #endregion

        #region "PrintHistory(int p_iClaimId)"
        /// Name		: PrintHistory
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 08/03/2005     * Return type from String to XmlDocument * Mohit Yadav
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// fetches the data in XML format for printing History for the claim Id passed
        /// </summary>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <returns>Returns XML containing required data to print or error messages if fails to get the data</returns>
        public XmlDocument PrintHistory(int p_iClaimId)

            //**************************************************************
        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
        //
        //To return XML rather then String
        //Initially:- public string PrintHistory(int p_iClaimId)
        //Finally:- public XmlDocument PrintHistory(int p_iClaimId)
        //
        //*************************************************************
        {
            int iInvoiceId = 0;
            int iInvTransId = 0;

            double dInvAmt = 0.0;
            double dInvTotal = 0.0;
            double dTotalBilled = 0.0;
            double dBillAmount = 0.0;

            string sReturnXML = string.Empty;
            string sSQL = string.Empty;

            XmlDocument objXMLDocument = null;
            XmlElement objXMLChildElement = null;
            XmlElement objXMLElement = null;
            DbReader objReader = null;
            Entity objEntity = null;
            DbReader objTempReader = null;
            LocalCache objLocalCache = null;
            XmlElement objCopyRightData = null;

            try
            {
                objXMLDocument = new XmlDocument();
                objCopyRightData = objXMLDocument.CreateElement("CopyRightData");
                this.GetCopyRightElement(ref objCopyRightData);
                objXMLDocument.AppendChild(objCopyRightData);
                if (p_iClaimId == 0)
                    return objXMLDocument;
                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //
                //To return XML rather then String
                //Initially:- return string.Empty;
                //Finally:- return objXMLDocument;
                //
                //*************************************************************

                objXMLDocument.LoadXml("<print></print>");
                objCopyRightData = objXMLDocument.CreateElement("CopyRightData");
                this.GetCopyRightElement(ref objCopyRightData);
                objXMLDocument.SelectSingleNode("print").AppendChild(objCopyRightData);
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("print");
                objXMLChildElement = (XmlElement)objXMLDocument.SelectSingleNode("print");

                GetClientDetails();

                objXMLElement.SetAttribute("title", "Time and Expense History");
                objXMLElement.SetAttribute("name", m_stClientDetails.Name);
                objXMLElement.SetAttribute("tdate", Conversion.ToDbDate(DateTime.Now));
                objXMLElement.SetAttribute("city1", m_stClientDetails.City + " Office");
                objXMLElement.SetAttribute("phone1", "Telephone " + m_stClientDetails.PhoneNumber);
                objXMLElement.SetAttribute("fax1", "FAX " + m_stClientDetails.FaxNumber);

                sSQL = "SELECT AR_ROW_ID, CUSTOMER_EID, INV_NUMBER, INV_DATE, WORK_PERF_BY_EID, INV_TOTAL, " +
                       "INV_TRANS_ID FROM ACCT_REC WHERE CLAIM_ID=" + p_iClaimId + " AND POSTED <> 0 AND " +
                       "VOID_FLAG = 0 ORDER BY INV_DATE DESC, POSTED_ID, AR_ROW_ID";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

                objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(GetNewElement("option"), false);

                        iInvoiceId = Conversion.ConvertObjToInt(objReader["AR_ROW_ID"], m_iClientId);
                        iInvTransId = Conversion.ConvertObjToInt(objReader["INV_TRANS_ID"], m_iClientId);

                        objXMLChildElement.SetAttribute("vendor", objEntity.GetLastFirstName(Conversion.ConvertObjToInt(objReader["WORK_PERF_BY_EID"], m_iClientId)));
                        objXMLChildElement.SetAttribute("invdate", Conversion.GetDate(objReader["INV_DATE"].ToString()).ToString());
                        objXMLChildElement.SetAttribute("invnumber", objReader["INV_NUMBER"].ToString());
                        dInvAmt = objReader.GetDouble("INV_TOTAL");
                        dInvTotal = dInvTotal + dInvAmt;
                        //rupal:multicurrency
                        //objXMLChildElement.SetAttribute("invamnt", string.Format("{0:C}", dInvAmt));
                        objXMLChildElement.SetAttribute("invamnt", CommonFunctions.ConvertCurrency(0, dInvAmt, CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));                        

                        sSQL = "SELECT SUM(TOTAL) FROM ACCT_REC_DETAIL WHERE AR_ROW_ID=" + iInvoiceId + " AND BILLABLE<>0";
                        objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        if (objTempReader != null)
                        {
                            if (objTempReader.Read())
                            {
                                dBillAmount = objTempReader.GetDouble(0);
                            }
                            objTempReader.Close();
                        }
                        //rupal:multicurrency
                        //objXMLChildElement.SetAttribute("billedamnt", string.Format("{0:C}", dBillAmount));
                        objXMLChildElement.SetAttribute("billedamnt", CommonFunctions.ConvertCurrency(0, dBillAmount, CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                        
                        dTotalBilled = dTotalBilled + dBillAmount;

                        string sCode = string.Empty;
                        string sCheckNumber = string.Empty;
                        string sCheckdate = string.Empty;

                        sSQL = "SELECT STATUS_CODE,TRANS_NUMBER,DATE_OF_CHECK FROM FUNDS WHERE TRANS_ID=" + iInvTransId;
                        objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        if (objTempReader != null)
                        {
                            if (objTempReader.Read())
                            {
                                sCode = objLocalCache.GetShortCode(Conversion.ConvertStrToInteger(objTempReader["STATUS_CODE"].ToString()));
                                sCheckNumber = objTempReader["TRANS_NUMBER"].ToString();
                                sCheckdate = Conversion.ToDate(objTempReader["DATE_OF_CHECK"].ToString()).ToString();
                            }
                            objTempReader.Close();
                        }

                        objXMLChildElement.SetAttribute("checkstatus", sCode);
                        objXMLChildElement.SetAttribute("checknumber", sCheckNumber);
                        objXMLChildElement.SetAttribute("checkdate", sCheckdate);

                        objXMLElement.AppendChild(objXMLChildElement);
                    }
                } // if

                
                //rupal:start,multicurrency
                //objXMLElement.SetAttribute("billtotal", dTotalBilled.ToString());
                objXMLElement.SetAttribute("billtotal", CommonFunctions.ConvertCurrency(0, dTotalBilled, CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                //objXMLElement.SetAttribute("invtotal", dInvTotal.ToString());
                objXMLElement.SetAttribute("invtotal", CommonFunctions.ConvertCurrency(0, dInvTotal, CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                //rupal:end

                return objXMLDocument;

                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //
                //To return XML rather then String
                //Initially:- return objXMLDocument.OuterXml;
                //Finally:- return objXMLDocument;
                //
                //*************************************************************
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.PrintHistory.ErrorPrint", m_iClientId), p_objException);
            }
            finally
            {
                objXMLDocument = null;
                objXMLChildElement = null;
                objXMLElement = null;
                if (objReader != null)
                    objReader.Dispose();
                if (objTempReader != null)
                    objTempReader.Dispose();
                if (objEntity != null)
                    objEntity.Dispose();
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                objCopyRightData = null;
            }
        }
        #endregion

        #region "PrintReport(int p_iInvoiceId)"
        /// Name		: PrintReport
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 08/03/2005     * Return type from String to XmlDocument * Mohit Yadav
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// fetches the data in XML format for printing History for the claim Id passed
        /// </summary>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <returns>Returns XML containing required data to Print or Error Messages if fails to get the data</returns>
        public XmlDocument PrintReport(int p_iInvoiceId)

            //**************************************************************
        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
        //
        //To return XML rather then String
        //Initially:- public string PrintReport(int p_iInvoiceId)
        //Finally:- public XmlDocument PrintReport(int p_iInvoiceId)
        //
        //*************************************************************
        {
            int iPostedId = 0;
            string sSQL = string.Empty;

            XmlDocument objXMLDocument = null;
            XmlElement objXMLChildElement = null;
            XmlElement objXMLElement = null;
            DbReader objReader = null;
            LocalCache objLocalCache = null;

            try
            {
                objXMLDocument = new XmlDocument();

                if (p_iInvoiceId == 0)
                    return objXMLDocument;

                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //
                //To return XML rather then String
                //Initially:- return string.Empty;
                //Finally:- return objXMLDocument;
                //
                //*************************************************************

                objXMLDocument.LoadXml("<print></print>");

                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("print");
                objXMLChildElement = (XmlElement)objXMLDocument.SelectSingleNode("print");

                sSQL = "SELECT POSTED_ID FROM ACCT_REC WHERE AR_ROW_ID = " + p_iInvoiceId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iPostedId = objReader.GetInt32("POSTED_ID");
                    }
                    objReader.Close();
                }

                if (iPostedId == 0)
                    return objXMLDocument;

                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //
                //To return XML rather then String
                //Initially:- return string.Empty;
                //Finally:- return objXMLDocument;
                //
                //*************************************************************

                GetClientDetails();

                objXMLElement.SetAttribute("title", "Time and Expense Report");
                objXMLElement.SetAttribute("name", m_stClientDetails.Name);
                objXMLElement.SetAttribute("tdate", Conversion.ToDbDate(DateTime.Now));
                objXMLElement.SetAttribute("city1", m_stClientDetails.City + " Office");
                objXMLElement.SetAttribute("phone1", "Telephone " + m_stClientDetails.PhoneNumber);
                objXMLElement.SetAttribute("fax1", "FAX " + m_stClientDetails.FaxNumber);

                sSQL = "SELECT FUNDS.TRANS_DATE FROM ACCT_REC, FUNDS WHERE " +
                       "ACCT_REC.POSTED_ID = " + iPostedId + " AND ACCT_REC.TRANS_ID = FUNDS.TRANS_ID";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                        objXMLElement.SetAttribute("DateBilled", Conversion.GetDate(objReader["TRANS_DATE"].ToString()).ToString());
                    else
                        objXMLElement.SetAttribute("DateBilled", string.Empty);
                    objReader.Close();
                }

                sSQL = "SELECT CLAIM.CLAIM_NUMBER, CLAIM.COMMENTS FROM CLAIM,ACCT_REC " +
                       "WHERE ACCT_REC.CLAIM_ID=CLAIM.CLAIM_ID AND ACCT_REC.AR_ROW_ID = " + p_iInvoiceId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                        objXMLElement.SetAttribute("ClaimNumber", objReader["CLAIM_NUMBER"].ToString());
                    else
                        objXMLElement.SetAttribute("ClaimNumber", string.Empty);
                    objReader.Close();
                }

                sSQL = "SELECT FIRST_NAME, LAST_NAME FROM CLAIMANT, ENTITY,ACCT_REC " +
                       "WHERE CLAIMANT.CLAIM_ID=ACCT_REC.CLAIM_ID AND ACCT_REC.AR_ROW_ID = " + p_iInvoiceId +
                       "AND CLAIMANT.PRIMARY_CLMNT_FLAG <> 0 AND CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                        objXMLElement.SetAttribute("Claimant", objReader["LAST_NAME"].ToString() + (objReader["FIRST_NAME"].ToString() == "" ? "" : ("," + objReader["FIRST_NAME"].ToString())));
                    else
                        objXMLElement.SetAttribute("Claimant", string.Empty);
                    objReader.Close();
                }

                sSQL = "SELECT INV_DATE, TRANS_TYPE_CODE, LOCAL_COMMENT, QUANTITY, UNIT, RATE, TOTAL, " +
                       "ACCT_REC.AR_ROW_ID ROW_ID FROM ACCT_REC, ACCT_REC_DETAIL WHERE POSTED_ID " +
                       "= " + iPostedId + " AND ACCT_REC.VOID_FLAG=0 AND ACCT_REC_DETAIL.AR_ROW_ID " +
                       "= ACCT_REC.AR_ROW_ID ORDER BY INV_DATE, ACCT_REC.AR_ROW_ID";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    objLocalCache = new Common.LocalCache(m_sConnectionString, m_iClientId);
                    while (objReader.Read())
                    {
                        objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(GetNewElement("option"), false);
                        objXMLChildElement.SetAttribute("invdate", Conversion.ToDate(objReader["INV_DATE"].ToString()).ToString());
                        objXMLChildElement.SetAttribute("typecode", objLocalCache.GetShortCode(Conversion.ConvertStrToInteger(objReader["TRANS_TYPE_CODE"].ToString())));
                        objXMLChildElement.SetAttribute("comment", objReader["LOCAL_COMMENT"].ToString().Length < 47 ? objReader["LOCAL_COMMENT"].ToString() : (objReader["LOCAL_COMMENT"].ToString().Substring(0, 46) + "..."));//MITS 16171
                        objXMLChildElement.SetAttribute("qty", objReader["QUANTITY"].ToString());
                        objXMLChildElement.SetAttribute("unit", objReader["UNIT"].ToString());
                        //rupal:start,multicurrency
                        //objXMLChildElement.SetAttribute("rate", Conversion.ConvertStrToDouble(objReader["RATE"].ToString()).ToString());
                        //objXMLChildElement.SetAttribute("total", Conversion.ConvertStrToDouble(objReader["TOTAL"].ToString()).ToString());
                        objXMLChildElement.SetAttribute("rate", CommonFunctions.ConvertCurrency(0, objReader.GetDouble("RATE"), CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                        objXMLChildElement.SetAttribute("total", CommonFunctions.ConvertCurrency(0, objReader.GetDouble("TOTAL"), CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                        //rupal:end,multicurrency
                        objXMLElement.AppendChild(objXMLChildElement);
                    }
                    objReader.Close();
                    if (objLocalCache != null)
                        objLocalCache.Dispose();
                }

                return objXMLDocument;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.PrintReport.ErrorPrint", m_iClientId), p_objException);
            }
            finally
            {
                objXMLDocument = null;
                objXMLChildElement = null;
                objXMLElement = null;
                if (objReader != null)
                    objReader.Dispose();
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                }
            }
        }
        #endregion

        #region "VoidIt(string p_sInvoiceIds, bool p_bIsContinue, string p_sStatus)"
        /// Name		: VoidIt
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 08/03/2005     * Return type from String to XmlDocument * Mohit Yadav
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Voids the Invoices. 
        /// </summary>
        /// <param name="p_sInvoiceIds">InvoiceIds Comma seperated</param>
        /// <param name="p_bIsContinue">In case of any error continue or not</param>
        /// <param name="p_sStatus">Status</param>
        /// <returns>XML containing the Success/failure messages.</returns>
        public XmlDocument VoidIt(string p_sInvoiceIds, bool p_bIsContinue, string p_sStatus)

            //**************************************************************
        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
        //
        //To return XML rather then String
        //Initially:- public string VoidIt(string p_sInvoiceIds, bool p_bIsContinue, string p_sStatus)
        //Finally:- public XmlDocument VoidIt(string p_sInvoiceIds, bool p_bIsContinue, string p_sStatus)
        //
        //*************************************************************
        {
            bool IsError = false;

            int iReturn = 0;

            string[] arrInvoiceId = { };

            XmlDocument objXMLDoc = null;
            XmlDocument objReturnXMLDoc = null;

            //**************************************************************
            //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
            //
            //To return XML rather then String
            //Initially:- string sReturnXML = string.Empty
            //Finally:- XmlDocument objReturnXMLDoc = null;
            //
            //*************************************************************

            try
            {
                objXMLDoc = new XmlDocument();

                arrInvoiceId = p_sInvoiceIds.Split(',');
                m_bIsContinue = p_bIsContinue;
                m_sInvoiceIds = p_sInvoiceIds;

                objReturnXMLDoc = new XmlDocument();

                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //*************************************************************

                for (int i = 0; i < arrInvoiceId.Length; i++)
                {
                    VoidBill(Conversion.ConvertStrToInteger(arrInvoiceId[i].ToString()), p_sStatus, out iReturn);
                    if (iReturn != 1 && iReturn != 2)
                    {
                        objReturnXMLDoc = m_objXmlDocument;
                        IsError = true;
                        break;

                        //**************************************************************
                        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                        //
                        //To return XML rather then String
                        //Initially:- sReturnXML = m_objXmlDocument.OuterXml;
                        //Finally:- objReturnXMLDoc = m_objXmlDocument;
                        //
                        //*************************************************************
                    }
                    else if (iReturn == 2)
                    {
                        objReturnXMLDoc = m_objXmlDocument;
                        IsError = true;

                        //**************************************************************
                        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                        //
                        //To return XML rather then String
                        //Initially:- sReturnXML = m_objXmlDocument.OuterXml;
                        //Finally:- objReturnXMLDoc = m_objXmlDocument;
                        //
                        //*************************************************************
                    }
                }

                if (IsError == false)
                    objReturnXMLDoc = objXMLDoc;

                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //
                //To return XML rather then String
                //Initially:- sReturnXML = objXMLDoc.OuterXml;
                //Finally:- objReturnXMLDoc = objXMLDoc;
                //
                //*************************************************************

            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.VoidIt.ErrorVoid", m_iClientId), p_objException);
            }
            finally
            {
                objXMLDoc = null;
            }

            return objReturnXMLDoc;

            //**************************************************************
            //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
            //
            //To return XML rather then String
            //Initially:- return sReturnXML;
            //Finally:- return objReturnXMLDoc;
            //
            //*************************************************************
        }
        #endregion

        #region "BillIt(string p_sInvoiceIds, bool p_bIsContinue, bool p_bAllowClosedClaim)"
        /// Name		: BillIt
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 07/03/2005     * Return type from String to XmlDocument * Mohit Yadav
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Bill the invoices.
        /// </summary>
        /// <param name="p_sInvoiceIds">InvoiceIds Comma seperated</param>
        /// <param name="p_bIsContinue">In case of any error continue or not</param>
        /// <param name="p_bAllowClosedClaim">Flag for allowing closed claims</param>
        /// <returns>XML containing the Success/failure messages.</returns>
        public XmlDocument BillIt(string p_sInvoiceIds, bool p_bIsContinue, bool p_bAllowClosedClaim)

            //**************************************************************
        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
        //
        //To return XML rather then String
        //Initially:- public string BillIt(string p_sInvoiceIds, bool p_bIsContinue, bool p_bAllowClosedClaim)
        //Finally:- public XmlDocument BillIt(string p_sInvoiceIds, bool p_bIsContinue, bool p_bAllowClosedClaim)
        //
        //*************************************************************
        {
            XmlDocument objXMLDoc = null;
            string[] arrInvoiceId = { };
            bool IsError = false;
            int iResult = 0;

            //**************************************************************
            //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
            //
            //Initially:- string sReturnXML = string.Empty;
            //Finally:- XmlDocument objReturnXMLDoc = null;
            //
            //*************************************************************
            XmlDocument objReturnXMLDoc = null;

            try
            {
                objXMLDoc = new XmlDocument();

                arrInvoiceId = p_sInvoiceIds.Split(',');
                m_bIsContinue = p_bIsContinue;
                m_bAllowClosedClaim = p_bAllowClosedClaim;
                m_sInvoiceIds = p_sInvoiceIds;

                objReturnXMLDoc = new XmlDocument();

                for (int i = 0; i < arrInvoiceId.Length; i++)
                {
                    iResult = SetBill(Conversion.ConvertStrToInteger(arrInvoiceId[i].ToString()));
                    if (iResult != 1 && iResult != 2)
                    {
                        //**************************************************************
                        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                        //
                        //Initially:- sReturnXML = m_objXmlDocument.OuterXml;
                        //Finally:- objReturnXMLDoc = m_objXmlDocument;
                        //
                        //*************************************************************

                        objReturnXMLDoc = m_objXmlDocument;
                        IsError = true;
                        break;
                    }
                    else if (iResult == 2)
                    {
                        //**************************************************************
                        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                        //
                        //Initially:- sReturnXML = m_objXmlDocument.OuterXml;
                        //Finally:- objReturnXMLDoc = m_objXmlDocument;
                        //
                        //*************************************************************

                        objReturnXMLDoc = m_objXmlDocument;
                        IsError = true;
                    }
                }

                if (IsError == false)
                    objReturnXMLDoc = objXMLDoc;

                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //	
                //Initially:- sReturnXML = objXMLDoc.OuterXml;
                //Finally:- objReturnXMLDoc = objXMLDoc;
                //
                //*************************************************************
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            } // catch
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.BillIt.ErrorSet", m_iClientId), p_objException);
            }
            finally
            {
                objXMLDoc = null;
            }

            return objReturnXMLDoc;

            //**************************************************************
            //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
            //	
            //Initially:- return sReturnXML;
            //Finally:- return objReturnXMLDoc;
            //
            //*************************************************************

        }
        #endregion

        #endregion

        #region "Private Functions"

        #region "Initialize"
        /// Name		: Initialize
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Initialize the DataModal factory class object and Connection string;
        /// </summary>
        private void Initialize()
        {
            //abansal23 MITS 18880 : Transaction Types display in oracle db ON 12/9/2009 Starts
            DbConnection objConn = null;
            try
            {
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;                
                m_objXmlDocument = new XmlDocument();
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                m_sDBType = objConn.DatabaseType.ToString().ToUpper();
                                
                m_MessageSettings = RMConfigurationSettings.GetRMMessages(m_sConnectionString, m_iClientId); // Ash - cloud, get config setting from db
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.Initialize.ErrorInit", m_iClientId), p_objEx);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                objConn = null;
            }
            //abansal23 MITS 18880 : Transaction Types display in oracle db ON 12/9/2009 Ends
        }
        #endregion

        #region "SetBill(int p_iInvoiceId)"
        /// Name		: SetBill
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Saves the Invoice
        /// </summary>
        /// <param name="p_iInvoiceId">Invoice ID</param>
        /// <returns>0 for failure and 1 for success</returns>
        private int SetBill(int p_iInvoiceId)
        {
            int iClientEid = 0;
            int iCustomerEid = 0;
            int iTransId = 0;
            int iUseSubAcct = 0;
            int iReturnValue = 0;
            int iTransTypeCode = 0;
            int iReserveTypeCode = 0;
            int iFundsTransTypeCode = 0;
            int iFundsReserveTypeCode = 0;
            int iDistributionCodeId = 0;
            string sSQL = String.Empty;
            string sClaimStatusCode = String.Empty;

            FundManager objFundManager = null;
            Funds objFunds = null;
            DbReader objReader = null;
            Invoice objInvoice = null;
            InvoiceDetails objInvoiceDetails = null;
            LocalCache objLocalCache = null;
            AcctRec objAcctRec = null;
            FundsTransSplit objFundsTransSplit = null;
            DbConnection objConnection = null;
            DbCommand objcommand = null;
            bool isAmountCorrect = false;//abansal23: MITS 18850 - Invoice amount and Inv By field is vacant 11/30/2009 
            double dAmount = 0.0;//abansal23: MITS 18850 - Invoice amount and Inv By field is vacant 11/30/2009 
            //rupal:start,multicurrency
             Funds objFundsOld = null;
            //rupal:end

            try
            {
                //rupal:start, multicurrency
                objFundsOld = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
                //objInvoice = GetInvoice(p_iInvoiceId);
                //objInvoiceDetails = GetInvoiceDetails(p_iInvoiceId);
                objInvoice = GetInvoice(p_iInvoiceId, ref objFundsOld);
                objInvoiceDetails = GetInvoiceDetails(p_iInvoiceId, objFundsOld);
                //rupal:end

                //pmittal5 MITS 12357 09/19/08 - Extracting Rate Table Customer Id
                //sSQL = "SELECT TRANS_ID FROM ACCT_REC WHERE AR_ROW_ID=" + p_iInvoiceId;
                sSQL = "SELECT TRANS_ID, CUSTOMER_EID FROM ACCT_REC WHERE AR_ROW_ID=" + p_iInvoiceId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iTransId = Conversion.ConvertStrToInteger(objReader["TRANS_ID"].ToString());
                        iCustomerEid = Conversion.ConvertObjToInt(objReader.GetValue("CUSTOMER_EID"), m_iClientId);  //pmittal5 MITS 12357 09/19/08

                        if (iTransId != 0)
                        {
                            objInvoice = null;
                            objInvoiceDetails = null;
                            return 1;
                        }
                    }
                    objReader.Close();
                }
//aaggarwal29: RMA-16240,15315 start
                sSQL = "SELECT DSTRBN_TYPE_CODE FROM FUNDS,ACCT_REC WHERE FUNDS.TRANS_ID = ACCT_REC.INV_TRANS_ID AND  ACCT_REC.AR_ROW_ID= " + p_iInvoiceId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iDistributionCodeId = Conversion.ConvertStrToInteger(objReader["DSTRBN_TYPE_CODE"].ToString());
                    }
                    objReader.Close();
                }
//aaggarwal29: RMA-16240,15315 end
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                objFundManager = new FundManager(m_sDsnName, m_sUserName, m_sPassword, m_iUserId, m_iGroupId, m_iClientId);

                sSQL = "SELECT CLAIM_STATUS_CODE,PAYMNT_FROZEN_FLAG FROM CLAIM WHERE CLAIM_ID=" + objInvoice.ClaimId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        sClaimStatusCode = objLocalCache.GetShortCode(objLocalCache.GetRelatedCodeId(Conversion.ConvertStrToInteger(objReader["CLAIM_STATUS_CODE"].ToString())));
                        if (sClaimStatusCode != "O" && m_bAllowClosedClaim == false)
                        {
                            throw new RMAppException(GetMyError(Globalization.GetString("InvoiceDriver.SetBill.ErrorClaim", m_iClientId), 1, string.Empty, objInvoice.ClaimId, 1));
                        }
                        if (Conversion.ConvertStrToInteger(objReader["PAYMNT_FROZEN_FLAG"].ToString()) == 1)
                        {
                            throw new RMAppException(GetMyError(Globalization.GetString("InvoiceDriver.SetBill.ErrorPayment", m_iClientId), 1, string.Empty, objInvoice.ClaimId, 1));
                        }
                    }
                    objReader.Close();
                }

                objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);

                //pmittal5 MITS 12357 09/19/08 - Set the Payee to the Client level of Rate Table selected
                StringBuilder sSql = new StringBuilder();
                sSql.AppendLine("SELECT DISTINCT CLIENT_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + iCustomerEid + " OR FACILITY_EID = " + iCustomerEid);
                sSql.AppendLine(" OR LOCATION_EID = " + iCustomerEid + " OR DIVISION_EID = " + iCustomerEid + " OR REGION_EID = " + iCustomerEid);
                sSql.AppendLine(" OR OPERATION_EID = " + iCustomerEid + " OR COMPANY_EID = " + iCustomerEid + " OR CLIENT_EID = " + iCustomerEid);

                objConnection = DbFactory.GetDbConnection(m_sConnectionString);
                objConnection.Open();

                objcommand = objConnection.CreateCommand();
                objcommand.CommandText = sSql.ToString();
                iClientEid = Conversion.ConvertObjToInt(objcommand.ExecuteScalar(), m_iClientId);
                objConnection.Close();
                sSql = null;

                //sSQL = "SELECT ENTITY_ID,FIRST_NAME,LAST_NAME,ADDR1,ADDR2,CITY,STATE_ID," +
                //    "ZIP_CODE FROM ENTITY WHERE ENTITY_TABLE_ID=1005";
                sSQL = "SELECT ENTITY_ID,FIRST_NAME,LAST_NAME,ADDR1,ADDR2,ADDR3,ADDR4,CITY,STATE_ID,ZIP_CODE FROM ENTITY WHERE ENTITY_ID = " + iClientEid;
                //End - pmittal5

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        objFunds.Addr1 = objReader["ADDR1"].ToString();
                        objFunds.Addr2 = objReader["ADDR2"].ToString();
                        objFunds.Addr3 = objReader["ADDR3"].ToString();
                        objFunds.Addr4 = objReader["ADDR4"].ToString();
                        objFunds.City = objReader["CITY"].ToString();
                        objFunds.PayeeEid = Conversion.ConvertStrToInteger(objReader["ENTITY_ID"].ToString());
                        objFunds.FirstName = objReader["FIRST_NAME"].ToString();
                        objFunds.LastName = objReader["LAST_NAME"].ToString();
                        objFunds.StateId = Conversion.ConvertStrToInteger(objReader["STATE_ID"].ToString());
                        objFunds.ZipCode = objReader["ZIP_CODE"].ToString();
                        objFunds.PaymentFlag = false;
                    }
                    objReader.Close();
                }

                sSQL = "SELECT RATE_LEVEL,TANDEACCT_ID,USE_SUB_ACCOUNT FROM SYS_PARMS";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        if (Conversion.ConvertStrToInteger(objReader["USE_SUB_ACCOUNT"].ToString()) == 0)
                            iUseSubAcct = 0;
                        else
                            iUseSubAcct = 1;
                    }
                    objReader.Close();
                }

                if (iUseSubAcct == 0)
                {
                    sSQL = "SELECT BANK_ACC_ID FROM POLICY WHERE POLICY_ID " +
                           "=(SELECT PRIMARY_POLICY_ID FROM CLAIM WHERE CLAIM_ID=" + objInvoice.ClaimId + ")";
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            objFunds.AccountId = Conversion.ConvertStrToInteger(objReader["BANK_ACC_ID"].ToString());
                        }
                        objReader.Close();
                    }

                    if (objFunds.AccountId == 0)
                    {
                        sSQL = "SELECT ACCOUNT_ID FROM  ACCOUNT WHERE OWNER_EID " +
                               "=(SELECT CUSTOMER_EID FROM ACCT_REC WHERE AR_ROW_ID=" + p_iInvoiceId + ")";
                        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        if (objReader != null)
                        {
                            if (objReader.Read())
                            {
                                objFunds.AccountId = Conversion.ConvertStrToInteger(objReader["ACCOUNT_ID"].ToString());
                                objFunds.SubAccountId = 0;
                            }
                            objReader.Close();
                        }
                    }
                }
                else
                {
                    sSQL = "SELECT BANK_ACC_ID,SUB_ACC_ROW_ID FROM POLICY WHERE POLICY_ID " +
                           "=(SELECT PRIMARY_POLICY_ID FROM CLAIM WHERE CLAIM_ID=" + objInvoice.ClaimId + ")";
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            objFunds.AccountId = Conversion.ConvertStrToInteger(objReader["BANK_ACC_ID"].ToString());
                            objFunds.SubAccountId = Conversion.ConvertStrToInteger(objReader["SUB_ACC_ROW_ID"].ToString());
                        }
                        objReader.Close();
                    }

                    if (objFunds.AccountId == 0)
                    {
                        sSQL = "SELECT ACCOUNT_ID,SUB_ROW_ID FROM  BANK_ACC_SUB WHERE OWNER_EID " +
                               "=(SELECT CUSTOMER_EID FROM ACCT_REC WHERE AR_ROW_ID=" + p_iInvoiceId + ")";
                        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        if (objReader != null)
                        {
                            if (objReader.Read())
                            {
                                objFunds.AccountId = Conversion.ConvertStrToInteger(objReader["ACCOUNT_ID"].ToString());
                                objFunds.SubAccountId = Conversion.ConvertStrToInteger(objReader["SUB_ROW_ID"].ToString());
                            }
                            objReader.Close();
                        }
                    }
                }

                sSQL = "SELECT UNIT_ID FROM VEHICLE_X_ACC_DATE WHERE CLAIM_ID=" + objInvoice.ClaimId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        objFunds.UnitId = Conversion.ConvertStrToInteger(objReader["UNIT_ID"].ToString());
                    }
                    objReader.Close();
                }

                sSQL = "SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID=" + objInvoice.ClaimId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        objFunds.ClaimantEid = Conversion.ConvertStrToInteger(objReader["CLAIMANT_EID"].ToString());
                    }
                    objReader.Close();
                }

                objFunds.ClaimId = objInvoice.ClaimId;
                objFunds.ClaimNumber = objInvoice.ClaimNumber;
                objFunds.CheckMemo = "Check No." + objInvoice.ClaimNumber;
                objFunds.DstrbnType = iDistributionCodeId;//RMA-16240,15315
                if (iPostId == 0)
                    iPostId = objInvoice.Id;

                double dTotalBillAmount = 0.0;
                //Commented by pmittal5 and copied inside the loop
                //objFundsTransSplit = ( FundsTransSplit ) m_objDataModelFactory.GetDataModelObject("FundsTransSplit",false);

                //rupal:start,multicurrency
                if (objFunds.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                {
                    #region commented
                    /*
                    //payment currency amount
                    objFunds.PmtCurrencyAmount = dTotalBillAmount;
                    //base currency amount
                    objFunds.Amount = objFunds.PmtCurrencyAmount * objFundsOld.PmtToBaseCurRate;
                    //claim currency amount
                    objFunds.ClaimCurrencyAmount = objFunds.Amount * objFundsOld.BaseToClaimCurRate;

                    objFunds.ClaimCurrencyType = objFundsOld.ClaimCurrencyType;
                    objFunds.PmtCurrencyType = objFundsOld.PmtCurrencyType;
                    objFunds.PmtToBaseCurRate = objFundsOld.PmtToBaseCurRate;
                    objFunds.BaseToPmtCurRate = objFundsOld.BaseToPmtCurRate;
                    objFunds.PmtToClaimCurRate = objFundsOld.PmtToClaimCurRate;
                    objFunds.BaseToClaimCurRate = objFundsOld.BaseToClaimCurRate;
                    */
                    #endregion

                    objFunds.ClaimCurrencyType = objFunds.Context.DbConn.ExecuteInt("SELECT CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID=" + objInvoice.ClaimId);
                    objFunds.PmtCurrencyType = objFunds.Context.InternalSettings.SysSettings.BaseCurrencyType;

                    double dExhRatePmtToClaim = CommonFunctions.ExchangeRateSrc2Dest(objFunds.PmtCurrencyType, objFunds.ClaimCurrencyType, m_sConnectionString, m_iClientId);
                    double dExhRatePmtToBase = CommonFunctions.ExchangeRateSrc2Dest(objFunds.PmtCurrencyType, objFunds.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                    double dExhRateBaseToClaim = CommonFunctions.ExchangeRateSrc2Dest(objFunds.Context.InternalSettings.SysSettings.BaseCurrencyType, objFunds.ClaimCurrencyType, m_sConnectionString, m_iClientId);
                    double dExhRateBaseToPmt = CommonFunctions.ExchangeRateSrc2Dest(objFunds.Context.InternalSettings.SysSettings.BaseCurrencyType, objFunds.PmtCurrencyType, m_sConnectionString, m_iClientId);
                    objFunds.PmtToClaimCurRate = dExhRatePmtToClaim;
                    objFunds.PmtToBaseCurRate = dExhRatePmtToBase;
                    objFunds.BaseToPmtCurRate = dExhRateBaseToPmt;
                    objFunds.BaseToClaimCurRate = dExhRateBaseToClaim;

                }
                else
                {
                    #region commented
                    /*
                    objFunds.Amount = dTotalBillAmount;
                    objFunds.PmtCurrencyAmount = objFunds.Amount;
                    objFunds.ClaimCurrencyAmount = objFunds.Amount;

                    objFunds.ClaimCurrencyType = objFundsOld.ClaimCurrencyType;
                    objFunds.PmtCurrencyType = objFundsOld.PmtCurrencyType;
                    objFunds.PmtToBaseCurRate = objFundsOld.PmtToBaseCurRate;
                    objFunds.BaseToPmtCurRate = objFundsOld.BaseToPmtCurRate;
                    objFunds.PmtToClaimCurRate = objFundsOld.PmtToClaimCurRate;
                    objFunds.BaseToClaimCurRate = objFundsOld.BaseToClaimCurRate;
                     */
                    #endregion
                    objFunds.ClaimCurrencyType = objFunds.Context.InternalSettings.SysSettings.BaseCurrencyType;
                    objFunds.BaseToClaimCurRate = 1.0;
                    objFunds.BaseToPmtCurRate = 1.0;
                    objFunds.PmtToClaimCurRate = 1.0;
                    objFunds.PmtToBaseCurRate = 1.0;
                    objFunds.Amount = dTotalBillAmount;
                    objFunds.PmtCurrencyAmount = objFunds.Amount;
                    objFunds.ClaimCurrencyAmount = objFunds.Amount;
                }
                //rupal:end


                foreach (InvoiceDetail objInvoiceDetail in objInvoiceDetails)
                {
                    objFundsTransSplit = (FundsTransSplit)m_objDataModelFactory.GetDataModelObject("FundsTransSplit", false);

                    iTransTypeCode = objInvoiceDetail.TransCode;
                    iReserveTypeCode = objInvoiceDetail.ReserveTypeCode;
                    iFundsTransTypeCode = iTransTypeCode;
                    iFundsReserveTypeCode = iReserveTypeCode;
                    //rupal:start,multicurrency
                    if (objInvoiceDetail.IsBillable != false)
                    {
                        if (objFunds.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                        {
                            #region commented
                            /*
                            //payment currency amounts
                            //since we have modified objInvoiceDetail to contain payment currency amount, we will store objInvoiceDetail objects amount feilds in payment currency feilds of funds
                            objFundsTransSplit.PmtCurrencyAmount = objInvoiceDetail.BillAmount;                            
                            dTotalBillAmount = dTotalBillAmount + objInvoiceDetail.BillAmount; //this is in payment currency
                            //abansal23: MITS 18850 - Invoice amount and Inv By field is vacant 11/30/2009 Starts
                            isAmountCorrect = Double.TryParse(objInvoiceDetail.ItemAmount, out dAmount);
                            objFundsTransSplit.PmtCurrencyInvAmount = isAmountCorrect ? (dAmount * objFundsOld.PmtToBaseCurRate) : 0.0;
                            //abansal23: MITS 18850 - Invoice amount and Inv By field is vacant 11/30/2009 Ends

                            //base currency amounts
                            objFundsTransSplit.Amount = (objFundsTransSplit.PmtCurrencyAmount * objFundsOld.PmtToBaseCurRate);                            
                            objFundsTransSplit.InvoiceAmount = (objFundsTransSplit.PmtCurrencyInvAmount * objFundsOld.PmtToBaseCurRate);
                            objFundsTransSplit.SumAmount = objFundsTransSplit.PmtCurrencyAmount * objFundsOld.PmtToBaseCurRate;//SumAmount contains base currency value

                            //claim currency amounts
                            objFundsTransSplit.ClaimCurrencyAmount = (objFundsTransSplit.Amount * objFundsOld.BaseToClaimCurRate);
                            objFundsTransSplit.ClaimCurrencyInvAmount = (objFundsTransSplit.InvoiceAmount * objFundsOld.BaseToClaimCurRate);
                             */
                            #endregion

                            //base currency amounts
                            
                            objFundsTransSplit.Amount = objInvoiceDetail.BillAmount; //(objFundsTransSplit.PmtCurrencyAmount * objFundsOld.PmtToBaseCurRate);
                            
                            dTotalBillAmount = dTotalBillAmount + objInvoiceDetail.BillAmount;                            
                            
                            objFundsTransSplit.SumAmount = objFundsTransSplit.PmtCurrencyAmount;
                            
                            //payment currency
                            objFundsTransSplit.PmtCurrencyAmount = objFundsTransSplit.Amount * objFunds.BaseToPmtCurRate;
                                                       

                            //claim currency amounts
                            objFundsTransSplit.ClaimCurrencyAmount = (objFundsTransSplit.Amount * objFunds.BaseToClaimCurRate);
                            
                            
                        }
                        else
                        {
                            objFundsTransSplit.Amount = objInvoiceDetail.BillAmount;
                            objFundsTransSplit.SumAmount = objInvoiceDetail.BillAmount;
                            dTotalBillAmount = dTotalBillAmount + objInvoiceDetail.BillAmount;
                            

                        }
                    }
                    //rupal:end
                    //rsushilaggar MITS 31422 date 02/09/2013
                    if (objFunds.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                    {
                        isAmountCorrect = Double.TryParse(objInvoiceDetail.ItemAmount, out dAmount);
                        objFundsTransSplit.InvoiceAmount = isAmountCorrect ? dAmount : 0.0;
                        objFundsTransSplit.PmtCurrencyInvAmount = objFundsTransSplit.InvoiceAmount * objFunds.BaseToPmtCurRate;
                        objFundsTransSplit.ClaimCurrencyInvAmount = (objFundsTransSplit.InvoiceAmount * objFunds.BaseToClaimCurRate);
                    }
                    else
                    {
                        //abansal23: MITS 18850 - Invoice amount and Inv By field is vacant 11/30/2009 Starts
                        isAmountCorrect = Double.TryParse(objInvoiceDetail.ItemAmount, out dAmount);
                        objFundsTransSplit.InvoiceAmount = isAmountCorrect ? dAmount : 0.0;
                        //abansal23: MITS 18850 - Invoice amount and Inv By field is vacant 11/30/2009 Ends
                    }
                    //End rsushilaggar
                    //MITS 16137 - start - mpalinski
                    if (objInvoiceDetail.Postable != true)
                    {
                        //MGaba2:MITS 18971:this query will fail in oracle
                        //sSQL = "SELECT CODE_ID FROM CODES AS C, GLOSSARY AS G " +
                        sSQL = "SELECT CODE_ID FROM CODES C, GLOSSARY G " +
                               "WHERE C.TABLE_ID = G.TABLE_ID " +
                               "AND C.SHORT_CODE = 'NONPOST' " +
                               "AND G.SYSTEM_TABLE_NAME = 'TRANS_TYPES'";
                        using (DbReader objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {
                            if (objDbReader.Read())
                            {
                                iFundsTransTypeCode = Conversion.ConvertObjToInt(objDbReader.GetValue("CODE_ID"), m_iClientId);
                            }
                        }
                        //MGaba2:MITS 18971:this query will fail in oracle
                        //sSQL = "SELECT CODE_ID FROM CODES AS C, GLOSSARY AS G " +
                        sSQL = "SELECT CODE_ID FROM CODES C, GLOSSARY G " +
                               "WHERE C.TABLE_ID = G.TABLE_ID " +
                               "AND C.SHORT_CODE = 'NONPOST' " +
                               "AND G.SYSTEM_TABLE_NAME = 'RESERVE_TYPE'";
                        using (DbReader objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {
                            if (objDbReader.Read())
                            {
                                iFundsReserveTypeCode = Conversion.ConvertObjToInt(objDbReader.GetValue("CODE_ID"), m_iClientId);
                            }
                        }
                    } // if
                    //MITS 16137 - end - mpalinski

                    objFundsTransSplit.FromDate = objInvoiceDetail.FromDate;
                    objFundsTransSplit.GlAccountCode = 0;
                    //rupal:multicurrency, this code moved above
                    //abansal23: MITS 18850 - Invoice amount and Inv By field is vacant 11/30/2009 Starts
                    //isAmountCorrect = Double.TryParse(objInvoiceDetail.ItemAmount,out dAmount);
                    //objFundsTransSplit.InvoiceAmount = isAmountCorrect ? dAmount : 0.0;
                    //abansal23: MITS 18850 - Invoice amount and Inv By field is vacant 11/30/2009 Ends
                    objFundsTransSplit.InvoiceNumber = objInvoiceDetail.InvoiceNumber;
                    objFundsTransSplit.ToDate = objInvoiceDetail.ToDate;
                    objFundsTransSplit.TransTypeCode = iFundsTransTypeCode;
                    objFundsTransSplit.ReserveTypeCode = iFundsReserveTypeCode;
                    objFundsTransSplit.AddedByUser = objInvoiceDetail.UpdatedByUser;
                    objFundsTransSplit.InvoicedBy = objInvoiceDetail.UpdatedByUser;//abansal23: MITS 18850 - Invoice amount and Inv By field is vacant 11/30/2009

                    objFunds.TransSplitList.Add(objFundsTransSplit);
                    objFundsTransSplit.Dispose();
                }
                
                objFunds.PaymentFlag = true;
                objFunds.CollectionFlag = false;
                objFunds.PayeeTypeCode = objLocalCache.GetCodeId("O", objLocalCache.GetTableName(GetTableId("PAYEE_TYPE")));
                objFunds.TransDate = Conversion.ToDbDate(DateTime.Now);
                objFunds.DateOfCheck = Conversion.ToDbDate(DateTime.Now);

               

                if (objFundManager.ValidateData(objFunds) == false)
                {
                    if (objFundManager.ErrorCount > 0)
                    {
                        if (objFundManager.Error(0) == "IRA")
                            throw new RMAppException(GetMyError(Globalization.GetString("InvoiceDriver.SetBill.ErrorIRA", m_iClientId), -1, string.Empty, objInvoice.ClaimId, 0));
                        else
                        {
                            m_objXmlDocument = new XmlDocument();
                            m_objXmlDocument = this.GetXMLForTransaction(objFundManager, objFunds);
                            //return iReturnValue;
                            //Commented for Bug No. 002002
                            throw new RMAppException(GetMyError(objFundManager.Error(0),1,string.Empty,objInvoice.ClaimId,1));
                        }
                    }
                    else if (objFundManager.ConfirmationsCount > 0 && m_bIsContinue == false)
                    {
                        throw new RMAppException(GetMyError(objFundManager.Confirmation(0), 2, m_sInvoiceIds, objInvoice.ClaimId, 1));
                    }
                    else
                        iReturnValue = 1;
                }
                else if (objFundManager.WarningsCount > 0)
                {
                    throw new RMAppException(GetMyError(objFundManager.Warning(0), 4, string.Empty, objInvoice.ClaimId, 1));
                }
                else
                    iReturnValue = 1;

                //pmittal5 MITS 12357 09/09/08
                //objFunds.StatusCode =  objLocalCache.GetCodeId("O",objLocalCache.GetTableName(GetTableId("CHECK_STATUS")));
                objFunds.StatusCode = objLocalCache.GetCodeId("P", objLocalCache.GetTableName(GetTableId("CHECK_STATUS")));

                objFundManager.Save(objFunds);

                iTransId = objFunds.TransId;

                objAcctRec = (AcctRec)m_objDataModelFactory.GetDataModelObject("AcctRec", false);
                objAcctRec.MoveTo(objInvoice.Id);
                objAcctRec.TransId = iTransId;
                objAcctRec.UpdatedByUser = m_sLoginName;
                objAcctRec.PostedId = iPostId;
                objAcctRec.Posted = true;
                objAcctRec.Save();

                return iReturnValue;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.SetBill.ErrorSet", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
                objInvoice = null;
                objInvoiceDetails = null;
                if (objFunds != null)
                    objFunds.Dispose();
                if (objFundManager != null)
                    objFundManager.Dispose();
                if (objAcctRec != null)
                    objAcctRec.Dispose();
                if (objFundsTransSplit != null)
                    objFundsTransSplit.Dispose();
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                //rupal:multicurrency
                if (objFundsOld != null)
                    objFundsOld.Dispose();
                //rupal:end
            }
        }
        #endregion

        #region "VoidBill(int p_iInvoiceId,string p_sStatus)"
        /// Name		: VoidBill
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Voids the invoices.
        /// </summary>
        /// <param name="p_iInvoiceId">Invoice Id</param>
        /// <param name="p_sStatus">Status</param>
        private void VoidBill(int p_iInvoiceId, string p_sStatus, out int p_iReturn)
        {
            p_iReturn = 0;
            int iTransId = 0;
            int iInvTransId = 0;
            int iClaimId = 0;
            int iRollUpId = 0;
            int iRollUpCount = 0;

            string sSQL = String.Empty;

            FundManager objFundManager = null;
            Funds objFunds = null;
            DbReader objReader = null;
            AcctRec objAcctRec = null;
            Funds objFundsTable = null;
            DbTransaction objDbTrans = null;

            try
            {
                sSQL = "SELECT TRANS_ID,INV_TRANS_ID,CLAIM_ID FROM ACCT_REC WHERE AR_ROW_ID=" + p_iInvoiceId + " AND VOID_FLAG=0";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iTransId = Conversion.ConvertStrToInteger(objReader["TRANS_ID"].ToString());
                        iInvTransId = Conversion.ConvertStrToInteger(objReader["INV_TRANS_ID"].ToString());
                        iClaimId = Conversion.ConvertStrToInteger(objReader["CLAIM_ID"].ToString());
                    }
                    else
                    {
                        objReader.Close();
                        p_iReturn = 1;
                    }
                    objReader.Close();
                }

                objFundManager = new FundManager(m_sDsnName, m_sUserName, m_sPassword, m_iUserId, m_iGroupId, m_iClientId);

                //MITS 15952/16469 - added P status (same as B), and non-allocated status (null/blank)
                if (p_sStatus == "B" || p_sStatus == "P" || String.IsNullOrEmpty(p_sStatus))
                {
                    objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);

                    //***************************
                    //Commeted by Anurag
                    //objFunds.MoveTo(iTransId);
                    objFunds.TransId = iTransId;
                    objFunds.ClaimId = iClaimId;
                    //***************************
                    objFunds.VoidDate = Conversion.ToDbDate(DateTime.Now);
                    objFunds.VoidFlag = true;

                    sSQL = "SELECT UNIT_ID FROM VEHICLE_X_ACC_DATE WHERE CLAIM_ID=" + iClaimId;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            objFunds.UnitId = Conversion.ConvertStrToInteger(objReader["UNIT_ID"].ToString());
                        }
                        objReader.Close();
                    }

                    sSQL = "SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID=" + iClaimId;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            objFunds.ClaimantEid = Conversion.ConvertStrToInteger(objReader["CLAIMANT_EID"].ToString());
                        }
                        objReader.Close();
                    }

                    if (objFundManager.ValidateData(objFunds) == false)
                    {
                        p_iReturn = 0;
                        if (objFundManager.ErrorCount > 0)
                        {
                            throw new RMAppException(GetMyError(objFundManager.Error(0), 1, string.Empty, iClaimId, 0));
                        }
                        else if (objFundManager.ConfirmationsCount > 0 && m_bIsContinue == false)
                        {
                            throw new RMAppException(GetMyError(Globalization.GetString("InvoiceDriver.VoidBill.ErrorChecks", m_iClientId), 2, m_sInvoiceIds, iClaimId, 0));
                        }
                    }
                }

                objFundsTable = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
                objFundsTable.MoveTo(iInvTransId);
                iRollUpId = objFundsTable.RollupId;

                if (iRollUpId != 0)
                {
                    sSQL = "SELECT COUNT(*) cnt FROM FUNDS WHERE ROLLUP_ID=" + iRollUpId;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            iRollUpCount = Conversion.ConvertStrToInteger(objReader["cnt"].ToString());
                        }
                        objReader.Close();
                    }
                }

                if (iRollUpCount > 1 && m_bIsContinue == false)
                {
                    p_iReturn = 0;
                    throw new RMAppException(GetMyError(Globalization.GetString("InvoiceDriver.VoidBill.ErrorChecks", m_iClientId), 2, m_sInvoiceIds, iClaimId, 0));
                }

                objDbTrans = m_objDataModelFactory.Context.DbConn.BeginTransaction();
                m_objDataModelFactory.Context.DbTrans = objDbTrans;

                //Not required to do through Data Model
                objAcctRec = (AcctRec)m_objDataModelFactory.GetDataModelObject("AcctRec", false);
                objAcctRec.MoveTo(p_iInvoiceId);
                objAcctRec.VoidFlag = true;
                //objAcctRec.UpdatedByUser = m_sLoginName;
                //objAcctRec.DttmRcdLastUpd = Conversion.ToDbDateTime(DateTime.Now);
                objAcctRec.Save();

                if (iRollUpCount > 1)
                {
                    sSQL = "UPDATE FUNDS SET VOID_FLAG=1,VOID_DATE='" + Conversion.ToDbDate(DateTime.Now) + 
                           "',UPDATED_BY_USER='" + m_sLoginName + 
                           "',DTTM_RCD_LAST_UPD='" + Conversion.ToDbDateTime(DateTime.Now) + 
                           "', WHERE ROLLUP_ID=" + iRollUpId;
                    objDbTrans.Connection.ExecuteNonQuery(sSQL);
                }
                else
                {
                    objFundsTable = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
                    objFundsTable.MoveTo(iInvTransId);
                    objFundsTable.VoidFlag = true;
                    objFundsTable.VoidDate = Conversion.ToDbDate(DateTime.Now);
                    //objFundsTable.UpdatedByUser = m_sLoginName;
                    //objFundsTable.DttmRcdLastUpd = Conversion.ToDbDateTime(DateTime.Now);
                    
                    //rupal:start,MITS 27410 : for a non-allocated transaction iInvTransId which is assigned to objFundsTable.TransId will always be zero so adding the condition
                    if (objFundsTable.TransId > 0)
                    {
                        objFundsTable.Save();
                        //Start:Export payment void data to VSS
                        if (objFundsTable.VoidFlag)
                         {
                            Claim objClaim = null;
                            objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(objFunds.ClaimId);
                            //if (objClaim.VssClaimInd)
                            //Use Utility Settings Instead iof claim settings
                            if(objClaim.Context.InternalSettings.SysSettings.EnableVSS.Equals(-1))
                            {
                                VssExportAsynCall objVss = new VssExportAsynCall(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                                objVss.AsynchVssReserveExport(objFundsTable.ClaimId, objFundsTable.ClaimantEid, 0, objFundsTable.TransId, 0, "","","","", "PaymentVoid");
                                objVss = null;
                            }
                            objClaim.Dispose();
                        }
                    //End:Export payment void data to VSS
                    }
                    //rupal:end
                }

                objDbTrans.Commit();

                //MITS 15952/16469 - added P status (same as B), and non-allocated status (null/blank)
                if (p_sStatus == "B" || p_sStatus == "P" || String.IsNullOrEmpty(p_sStatus))
                {
                    //MITS 15952 The transaction has been committed so should be set to null.
                    m_objDataModelFactory.Context.DbTrans = null;
                    //rupal:start
                    //while doing changes for multicurrency, i found that a non billed transaction have always TransId=0 which can not be marked as void!
                    //so adding a condition for non-zero transid
                    if (objFunds.TransId > 0)
                    {
                        objFundManager.Save(objFunds);
                    }

                } // if

                p_iReturn = 1;
            }
            catch (Exception p_objException)
            {
                if (objDbTrans != null)
                    objDbTrans.Rollback();
                throw new RMAppException(Globalization.GetString("InvoiceDriver.VoidBill.ErrorVoid", m_iClientId), p_objException);
            }
            finally
            {
                objReader.Dispose();
                if (objDbTrans != null)
                    objDbTrans.Dispose();
                if (objFunds != null)
                    objFunds.Dispose();
                if (objFundManager != null)
                    objFundManager.Dispose();
                if (objAcctRec != null)
                    objAcctRec.Dispose();
                if (objFundsTable != null)
                    objFundsTable.Dispose();
            }

        }
        #endregion

        #region "GetInvoice(int p_iRowId, int p_iClaimId)"

        /// Name		: GetInvoice
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// This Function loads the Invoice class object with Invoice details fetched on the 
        /// basis of RowId passed.
        /// </summary>
        /// <param name="p_lRowId">AR ROW ID</param>
        /// <param name="p_lClaimId">Claim ID</param>
        /// <returns>Object of Class Invoice</returns>
        /// private Invoice GetInvoice(int p_iRowId, int p_iClaimId) //rupal:changed signatoure for multicurrency
        private Invoice GetInvoice(int p_iRowId, int p_iClaimId, ref Funds objFunds)
        {
            AcctRec objAcctRec = null;
            Claim objClaim = null;
            Invoice objInvoice = null;
            DbReader objReader = null;
            string sSql = string.Empty;
           

            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                objInvoice = new Invoice();

                objAcctRec = (AcctRec)m_objDataModelFactory.GetDataModelObject("AcctRec", false);
                if (p_iRowId != 0)
                {
                    //Getting the Acct_Ret details based on ARRow Id and entity Id.
                    objAcctRec.MoveTo(p_iRowId);

                    //Setting the corresponding invoice object values from fetched data
                    objInvoice.Id = objAcctRec.ArRowId;
                    objInvoice.InvoiceNumber = objAcctRec.InvNumber;
                    objInvoice.InvoiceDate = objAcctRec.InvDate;
                    
                     //rupal:start,multicurrency
                    objInvoice.InvoiceAmount = objAcctRec.InvTotal.ToString();
                    /*
                    if (objAcctRec.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                    {
                        objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
                        objFunds.MoveTo(objAcctRec.InvTransId);
                        objInvoice.InvoiceAmount = (objAcctRec.InvTotal * objFunds.BaseToPmtCurRate).ToString();
                    }                    
                    else
                    {
                        objInvoice.InvoiceAmount = objAcctRec.InvTotal.ToString();
                    }
                     * */
                    //rupal:end
                    objInvoice.VendorId = objAcctRec.WorkPerfByEid;
                    objInvoice.VendorName = objAcctRec.WorkPerfByEntity.LastName + "," + objAcctRec.WorkPerfByEntity.FirstName;
                    objInvoice.InvTransId = objAcctRec.InvTransId;
                    objInvoice.Posted = objAcctRec.Posted;
                    objInvoice.PostedId = objAcctRec.PostedId;
                    objInvoice.Allocated = objAcctRec.Allocated;                    

                    sSql = "SELECT SUM(TOTAL) FROM ACCT_REC_DETAIL WHERE AR_ROW_ID=" + p_iRowId + " AND BILLABLE<>0";
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            //rupal:start,multicurrency
                            objInvoice.BillAmount = objReader[0].ToString();
                            /*
                            //objAcctRec contains all amount in base currency so we need to convert them to payment curency so that we can display in payment currency
                            if (objFunds.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                            {
                                objInvoice.BillAmount = (objReader.GetDouble(0) * objFunds.BaseToPmtCurRate).ToString();
                            }
                            else
                            {
                                objInvoice.BillAmount = objReader[0].ToString();
                            }
                            //rupal:end
                             * */
                        }
                        objReader.Close();
                    }

                    sSql = "SELECT SUM(LINE_ITEM_AMOUNT) FROM ACCT_REC_DETAIL WHERE AR_ROW_ID=" + p_iRowId;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                             //rupal:start,multicurrency
                            objInvoice.ItemAmount = objReader[0].ToString();
                            /*
                            //objAcctRec contains all amount in base currency so we need to convert them to payment curency so that we can display in payment currency
                            if (objFunds.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                            {
                                objInvoice.ItemAmount = (objReader.GetDouble(0) * objFunds.BaseToPmtCurRate).ToString();
                            }
                            else
                            {
                                objInvoice.ItemAmount = objReader[0].ToString();
                            }
                             * */
                        }
                        objReader.Close();
                    }

                    //Getting the claim data
                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(objAcctRec.ClaimId);

                    objInvoice.ClaimId = objClaim.ClaimId;
                    objInvoice.ClaimNumber = objClaim.ClaimNumber;
                }
                else
                {
                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(p_iClaimId);

                    objInvoice.ClaimId = objClaim.ClaimId;
                    objInvoice.ClaimNumber = objClaim.ClaimNumber;
                    objInvoice.Allocated = true;
                }

                return objInvoice;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetInvoice.ErrorGet", m_iClientId), p_objException);
            }
            finally
            {
                if (objAcctRec != null)
                    objAcctRec.Dispose();
                if (objClaim != null)
                    objClaim.Dispose();
                objInvoice = null;
                if (objReader != null)
                    objReader.Dispose();
            }
        }

        /// <summary>
        /// Overloaded version of function GetInvoice(long p_lRowId, long p_lClaimId)
        /// with passing p_lClaimId as 0
        /// </summary>
        /// <param name="p_lRowId">AR ROW ID</param>
        /// <returns>Object of Class Invoice</returns>
        
        // private Invoice GetInvoice(int p_iRowId,ref Funds objFunds) //rupal:changes signature for multicurrency
        private Invoice GetInvoice(int p_iRowId,ref Funds objFunds)
        {
            //return GetInvoice(p_iRowId, 0); //rupal:multicurrency            
            return GetInvoice(p_iRowId, 0, ref objFunds);
        }
        #endregion

        #region "GetInvoiceDetail"
        /// Name		: GetInvoiceDetail
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// This Function loads the InvoiceDetail class object with Invoice details fetched on the 
        /// basis of Invoice Detail Id passed.
        /// </summary>
        /// <param name="p_iInvDetId">Invoice Detail Id</param>
        /// <returns>Object of Class InvoiceDetail</returns>
        private InvoiceDetail GetInvoiceDetail(int p_iInvDetId)
        {
            Code objCode = null;
            AcctRecDetail objAcctRecDetail = null;
            AcctRec objAcctRec = null;
            InvoiceDetail objInvoiceDetail = null;
            LocalCache objChache = null;
            int iTableID = 0;

            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                objInvoiceDetail = new InvoiceDetail();

                objAcctRecDetail = (AcctRecDetail)m_objDataModelFactory.GetDataModelObject("AcctRecDetail", false);
                objAcctRecDetail.MoveTo(p_iInvDetId);

                objAcctRec = (AcctRec)m_objDataModelFactory.GetDataModelObject("AcctRec", false);
                objAcctRec.MoveTo(objAcctRecDetail.ArRowId);

                objInvoiceDetail.Id = objAcctRecDetail.ArDetailRowId;
                objInvoiceDetail.ARId = objAcctRecDetail.ArRowId;
                objInvoiceDetail.Comment = objAcctRecDetail.LocalComment;
                objInvoiceDetail.FromDate = objAcctRecDetail.FromDate;
                objInvoiceDetail.ToDate = objAcctRecDetail.ToDate;
                objInvoiceDetail.InvoiceTotal = Conversion.ConvertStrToDouble(string.Format("{0:C}", objAcctRec.InvTotal));
                objInvoiceDetail.IsBillable = objAcctRecDetail.Billable;
                objInvoiceDetail.ItemAmount = string.Format("{0:C}", objAcctRecDetail.LineItemAmount);
                objInvoiceDetail.NonBillReason = objAcctRecDetail.NonBillReason;
                objInvoiceDetail.NonBillReasonCode = GetCode(objAcctRecDetail.NonBillReason);
                objInvoiceDetail.Override = objAcctRecDetail.Override;
                objInvoiceDetail.Postable = objAcctRecDetail.Postable;
                objInvoiceDetail.PreComment = objAcctRecDetail.LocalComment;
                objInvoiceDetail.Quantity = objAcctRecDetail.Quantity;
                objInvoiceDetail.Rate = Conversion.ConvertStrToDouble(string.Format("{0:C}", objAcctRecDetail.Rate));
                objInvoiceDetail.Reason = objAcctRecDetail.Reason;
                objInvoiceDetail.ReserveTypeCode = objAcctRecDetail.ReserveTypeCode;
                objInvoiceDetail.ReserveType = GetCode(objAcctRecDetail.ReserveTypeCode);
                objInvoiceDetail.TransCode = objAcctRecDetail.TransTypeCode;
                objInvoiceDetail.BillAmount = Conversion.ConvertStrToDouble(string.Format("{0:C}", objAcctRecDetail.Total));

                //Getting the Table ID on the basis of table name
                objChache = new Common.LocalCache(m_sConnectionString, m_iClientId);
                iTableID = objChache.GetTableId("TRANS_TYPES");
                if (objChache != null) objChache.Dispose();

                objCode = (Code)m_objDataModelFactory.GetDataModelObject("Codes", false);
                objCode.MoveTo(objInvoiceDetail.TransCode);

                foreach (CodeTextItem objCodeTextItem in objCode.CodeTextList)
                {
                    if (objCode.TableId == iTableID && objCode.CodeId == objCodeTextItem.CodeId)
                        objInvoiceDetail.TransType = objCodeTextItem.CodeDesc;
                }

                return objInvoiceDetail;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetInvoiceDetail.ErrorGet", m_iClientId), p_objException);
            }
            finally
            {
                if (objCode != null)
                    objCode.Dispose();
                if (objAcctRecDetail != null)
                    objAcctRecDetail.Dispose();
                if (objAcctRec != null)
                    objAcctRec.Dispose();
                objInvoiceDetail = null;
                if (objChache != null)
                    objChache.Dispose();
            }
        }
        #endregion

        #region "GetInvoiceDetails(int p_iInvoiceId)"
        /// Name		: GetInvoiceDetails
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// This Function makes the collection of InvoiceDetail class object with all Invoice 
        /// details fetched on the basis of Invoice Id passed.
        /// </summary>
        /// <param name="p_lInvoiceId">Invoice Id</param>
        /// <returns>Object of collection of InvoiceDetail</returns>

        //private InvoiceDetails GetInvoiceDetails(int p_iInvoiceId)
        //rupal:changed signature for multicurrency
        private InvoiceDetails GetInvoiceDetails(int p_iInvoiceId, Funds objFunds)
        {
            Code objCode = null;
            InvoiceDetail objInvoiceDetail = null;
            InvoiceDetails objInvoiceDetails = null;
            AcctRec objAcctRec = null;
            LocalCache objChache = null;

            int iTableID = 0;

            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                objInvoiceDetails = new InvoiceDetails();

                objAcctRec = (AcctRec)m_objDataModelFactory.GetDataModelObject("AcctRec", false);
                objAcctRec.MoveTo(p_iInvoiceId);

                foreach (AcctRecDetail objAcctRecDetail in objAcctRec.AcctRecDetailList)
                {
                    if (objAcctRecDetail.ArRowId == p_iInvoiceId)
                    {
                        objInvoiceDetail = new InvoiceDetail();

                        objInvoiceDetail.Id = objAcctRecDetail.ArDetailRowId;
                        objInvoiceDetail.ARId = objAcctRecDetail.ArRowId;
                        objInvoiceDetail.Comment = objAcctRecDetail.LocalComment;
                        objInvoiceDetail.FromDate = objAcctRecDetail.FromDate;
                        objInvoiceDetail.ToDate = objAcctRecDetail.ToDate;

                        //*************************************************************
                        //Mohit Yadav changed the code from (1) to (2)
                        //1 --> objInvoiceDetail.InvoiceTotal = Conversion.ConvertStrToDouble(string.Format("{0:C}",objAcctRecDetail.Total));
                        //2 --> objInvoiceDetail.InvoiceTotal = Conversion.ConvertStrToDouble((objAcctRecDetail.Total).ToString());
                        //*************************************************************

                        
                        objInvoiceDetail.InvoiceTotal = Conversion.ConvertStrToDouble((objAcctRecDetail.Total).ToString());
                        //*************************************************************
                        objInvoiceDetail.IsBillable = objAcctRecDetail.Billable;

                        //*************************************************************
                        //Mohit Yadav changed the code from (1) to (2)
                        //1 --> objInvoiceDetail.ItemAmount = string.Format("{0:C}",objAcctRecDetail.LineItemAmount);
                        
                        objInvoiceDetail.ItemAmount = objAcctRecDetail.LineItemAmount.ToString();
                        //*************************************************************

                        objInvoiceDetail.NonBillReason = objAcctRecDetail.NonBillReason;
                        objInvoiceDetail.NonBillReasonCode = GetCode(objAcctRecDetail.NonBillReason);
                        objInvoiceDetail.Override = objAcctRecDetail.Override;
                        objInvoiceDetail.Postable = objAcctRecDetail.Postable;
                        objInvoiceDetail.PreComment = objAcctRecDetail.LocalComment;
                        objInvoiceDetail.Quantity = objAcctRecDetail.Quantity;
                        objInvoiceDetail.Unit = objAcctRecDetail.Unit;
                        //*************************************************************
                        //Mohit Yadav changed the code from (1) to (2)
                        //1 --> objInvoiceDetail.Rate = Conversion.ConvertStrToDouble(string.Format("{0:C}",objAcctRecDetail.Rate));
                        //2 --> objInvoiceDetail.Rate = Conversion.ConvertStrToDouble((objAcctRecDetail.Rate).ToString());
                        //*************************************************************
                        
                        objInvoiceDetail.Rate = Conversion.ConvertStrToDouble((objAcctRecDetail.Rate).ToString());
                        //*************************************************************

                        objInvoiceDetail.Reason = objAcctRecDetail.Reason;
                        objInvoiceDetail.ReserveTypeCode = objAcctRecDetail.ReserveTypeCode;
                        objInvoiceDetail.ReserveType = GetCode(objAcctRecDetail.ReserveTypeCode);
                        objInvoiceDetail.TransCode = objAcctRecDetail.TransTypeCode;
                        
                        objInvoiceDetail.BillAmount = Conversion.ConvertStrToDouble((objAcctRecDetail.Total).ToString());
                        objInvoiceDetail.InvoiceNumber = objAcctRec.InvNumber;//MITS 16517
                        objInvoiceDetail.UpdatedByUser = objAcctRec.UpdatedByUser;//abansal23: MITS 18850 - Invoice amount and Inv By field is vacant 11/30/2009

                        /*
                        //rupal:start,multicurrency
                        //objAcctRecDetail have all amount feilds in base currency but we want to display feilds in payment currency
                        if (objFunds.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                        {
                            objInvoiceDetail.InvoiceTotal = (objAcctRecDetail.Total * objFunds.BaseToPmtCurRate);
                            objInvoiceDetail.ItemAmount = (objAcctRecDetail.LineItemAmount * objFunds.BaseToPmtCurRate).ToString();
                            objInvoiceDetail.Rate = (objAcctRecDetail.Rate * objFunds.BaseToPmtCurRate);
                            objInvoiceDetail.BillAmount = (objAcctRecDetail.Total * objFunds.BaseToPmtCurRate);
                        }
                        //rupal:end
                         */ 

                        //Getting the Table ID on the basis of table name
                        objChache = new Common.LocalCache(m_sConnectionString, m_iClientId);
                        iTableID = objChache.GetTableId("TRANS_TYPES");
                        if (objChache != null) objChache.Dispose();

                        objCode = (Code)m_objDataModelFactory.GetDataModelObject("Code", false);
                        objCode.MoveTo(objInvoiceDetail.TransCode);

                        foreach (CodeTextItem objCodeTextItem in objCode.CodeTextList)
                        {
                            if (objCode.TableId == iTableID && objCode.CodeId == objCodeTextItem.CodeId)
                                objInvoiceDetail.TransType = objCodeTextItem.CodeDesc;
                        }

                        objInvoiceDetails.Add(objInvoiceDetail);
                    }
                }
                return objInvoiceDetails;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetInvoiceDetails.ErrorGet", m_iClientId), p_objException);
            }
            finally
            {
                if (objCode != null)
                    objCode.Dispose();
                if (objAcctRec != null)
                    objAcctRec.Dispose();
                objInvoiceDetail = null;
                objInvoiceDetails = null;
                if (objChache != null)
                    objChache.Dispose();
            }
        }
        #endregion

        #region "GetRateTable(int p_iTableId)"
        /// Name		: GetRateTable
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// This Function loads the Rate Table class object with rate Table details 
        /// fetched on the basis of TableID passed.
        /// </summary>
        /// <param name="p_iTableId">Table Id</param>
        /// <returns>Object of Class RateTable</returns>
        private RateTable GetRateTable(int p_iTableId)
        {
            CustRate objCustRate = null;
            RateTable objRateTable = null;
            string sSql = string.Empty;

            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                objRateTable = new RateTable();

                objCustRate = (CustRate)m_objDataModelFactory.GetDataModelObject("CustRate", false);
                if (p_iTableId != 0)
                {
                    //Getting the Acct_Ret details based on ARRow Id and entity Id.
                    objCustRate.MoveTo(p_iTableId);

                    objRateTable.CustomerId = objCustRate.CustomerEid;
                    objRateTable.CustomerName = objCustRate.CustomerEntity.LastName + (objCustRate.CustomerEntity.FirstName == "" ? "" : ("," + objCustRate.CustomerEntity.FirstName));
                    objRateTable.Id = objCustRate.RateTableId;
                    objRateTable.TableName = objCustRate.TableName;
                    objRateTable.EffectiveDate = objCustRate.EffectiveDate;
                    objRateTable.ExpirationDate = objCustRate.ExpirationDate;
                }
                return objRateTable;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetRateTable.ErrorGet", m_iClientId), p_objException);
            }
            finally
            {
                if (objCustRate != null)
                    objCustRate.Dispose();
                objRateTable = null;
            }
        }
        #endregion

        #region "GetRateTableDetails(long p_lTableId)"
        /// Name		: GetRateTableDetails
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// This Function loads the collection of Rate Table Detail class object. 
        /// Details fetched on the basis of TableID passed.
        /// </summary>
        /// <param name="p_lTableId">Table Id</param>
        /// <returns>Object of Class RateTableDetails</returns>
        private RateTableDetails GetRateTableDetails(long p_lTableId)
        {
            //int iRecordCounter = 1;
            string sSql = String.Empty;

            DbReader objReader = null;
            RateTableDetail objRateTableDetail = null;
            RateTableDetails objRateTableDetails = null;

            try
            {
                objRateTableDetails = new RateTableDetails();

                sSql = "SELECT * FROM CUST_RATE_DETAIL WHERE RATE_TABLE_ID=" + p_lTableId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);

				if (objReader != null)
				{
					while(objReader.Read())
					{
						objRateTableDetail = new RateTableDetail();
                        //MGaba2:MITS 13140: While retreiving details of table,need to have its row id,which will be used while updating
						//objRateTableDetail.Id = iRecordCounter;                        
                        objRateTableDetail.Id = Conversion.ConvertStrToInteger(objReader["CUST_RATE_DETAIL_ROW_ID"].ToString());
						objRateTableDetail.Rate = Conversion.ConvertStrToDouble(objReader["RATE"].ToString());
						objRateTableDetail.TableId = Conversion.ConvertStrToLong(objReader["RATE_TABLE_ID"].ToString());
						objRateTableDetail.TransTypeCode = Conversion.ConvertStrToLong(objReader["TRANS_TYPE_CODE"].ToString());
						objRateTableDetail.TransType = GetCode(Conversion.ConvertStrToInteger(objReader["TRANS_TYPE_CODE"].ToString()));
						objRateTableDetail.Unit = objReader["UNIT"].ToString();
						objRateTableDetail.User = objReader["ADDED_BY_USER"].ToString();
                        //MGaba2:MITS 13140:while updation,in where clause,this time is required
                        objRateTableDetail.DateTime = objReader["DTTM_RCD_LAST_UPD"].ToString();

                        objRateTableDetails.Add(objRateTableDetail);
                    }
                    objReader.Close();
                }

                return objRateTableDetails;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetRateTableDetail.ErrorGet", m_iClientId), p_objException);
            }
            finally
            {
                objRateTableDetail = null;
                objRateTableDetails = null;
                if (objReader != null)
                    objReader.Dispose();
            }
        }
        #endregion

        #region "GetCode(int p_lCode)"
        /// Name		: GetCode
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Fetches the ShortCode and CodeDesc for the codeId passed and return a string
        /// after concanate the strings.
        /// </summary>
        /// <param name="p_lCode">Code ID</param>
        /// <returns>String containing CodeDesc and ShortCode</returns>
        private string GetCode(int p_lCode)
        {
            string sShortCode = String.Empty;
            string sShortDesc = String.Empty;
            string sCode = String.Empty;

            Common.LocalCache objChache = null;

            try
            {
                objChache = new Common.LocalCache(m_sConnectionString, m_iClientId);
                if (objChache != null)
                {
                    sShortCode = objChache.GetShortCode(p_lCode);
                    sShortDesc = objChache.GetCodeDesc(p_lCode);
                    sCode = String.Format("{0} {1}", sShortCode, sShortDesc);
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetCode.ErrorGet", m_iClientId), p_objException);
            }
            finally
            {
                if (objChache != null)
                    objChache.Dispose();
            }

            return sCode.Trim();
        }
        #endregion

        #region "FillXmlForRateTable(RateTable p_objRateTable)"
        /// Name		: FillXmlForRateTable
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 08/03/2005     * Return type from String to XmlDocument * Mohit Yadav
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// This Function loads the XML for Rate Table on the basis of the XML passed
        /// </summary>
        /// <param name="p_objRateTable">RateTable object</param>
        /// <returns></returns>
        private XmlDocument FillXmlForRateTable(RateTable p_objRateTable)

            //**************************************************************
        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
        //
        //To return XML rather then String
        //Initially:- private string FillXmlForRateTable(RateTable p_objRateTable)
        //Finally:- private XmlDocument FillXmlForRateTable(RateTable p_objRateTable)
        //
        //*************************************************************
        {
            XmlDocument objXML = null;
            XmlNodeList objNodeList = null;

            string sNodename = string.Empty;

            try
            {
                objXML = m_objXmlDocument;
                objNodeList = objXML.GetElementsByTagName("control");
                m_lId = GetRateLevel();

                foreach (XmlElement objXMLElement in objNodeList)
                {
                    sNodename = objXMLElement.GetAttribute("name");
                    switch (sNodename)
                    {
                        case "rtname":
                            SetValue(objXMLElement, "value", p_objRateTable.TableName, null);
                            if (p_objRateTable.TableName != string.Empty)
                                objXMLElement.SetAttribute("lock", "true");
                            break;
                        case "customer":
                            SetValue(objXMLElement, p_objRateTable.CustomerName, p_objRateTable.CustomerId.ToString(), null);
                            break;
                        case "efdate":
                            //**********************************************************
                            //Changes made by Mohit Yadav to get date in correct format
                            //***********************************************************
                            objXMLElement.SetAttribute("value", Conversion.ToDbDate(Conversion.ToDate(p_objRateTable.EffectiveDate)).ToString());

                            //objXMLElement.SetAttribute("value",Conversion.ToDate(p_objRateTable.EffectiveDate).ToString());
                            //SetValue(objXMLElement,"value",Conversion.ToDate(p_objRateTable.EffectiveDate).ToString(),null);
                            //SetValue(objXMLElement,p_objRateTable.EffectiveDate, string.Empty,null);
                            //***********************************************************
                            break;
                        case "exdate":
                            //**********************************************************
                            //Changes made by Mohit Yadav to get date in correct format
                            //***********************************************************
                            objXMLElement.SetAttribute("value", Conversion.ToDbDate(Conversion.ToDate(p_objRateTable.ExpirationDate)).ToString());
                            //SetValue(objXMLElement,p_objRateTable.ExpirationDate, string.Empty,null);  
                            //**********************************************************
                            break;
                        case "tableid":
                            SetValue(objXMLElement, "value", p_objRateTable.Id.ToString(), null);
                            break;
                        case "ratetable":
                            SetValue(objXMLElement, p_objRateTable.Id.ToString(), string.Empty, GetRateTableDetails(p_objRateTable.Id));
                            break;
                        case "mode":
                            if (p_objRateTable.Id == 0)
                                SetValue(objXMLElement, "value", "new", null);
                            else
                                SetValue(objXMLElement, "value", "edit", null);
                            break;
                    }
                }
                return objXML;

                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //
                //To return XML rather then String
                //Initially:- return objXML.OuterXml;
                //Finally:- return objXML;
                //
                //*************************************************************
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.FillXmlForRateTable.ErrorFill", m_iClientId), p_objException);
            }
            finally
            {
                objXML = null;
                objNodeList = null;
            }
        }
        #endregion

        #region "FillXmlForInvoice(Invoice p_objInvoice, int p_iInvoiceId)"
        
        /// Name		: FillXmlForInvoice
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 08/03/2005     * Return type from String to XmlDocument * Mohit Yadav
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Fecthes the invoice details for the invoice Id passed anf fills into the XML
        /// </summary>
        /// <param name="p_objInvoice">Invoice class Object</param>
        /// <param name="p_iInvoiceId">InvoiceID</param>
        /// <returns>Invoice XML</returns>
        //private XmlDocument FillXmlForInvoice(Invoice p_objInvoice, int p_iInvoiceId)
        //rupal:changed signature for multicurrency
        private XmlDocument FillXmlForInvoice(Invoice p_objInvoice, int p_iInvoiceId,Funds objFunds)

            //**************************************************************
        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
        //
        //To return XML rather then String
        //Initially:- private string FillXmlForInvoice(Invoice p_objInvoice, int p_iInvoiceId)
        //Finally:- private XmlDocument FillXmlForInvoice(Invoice p_objInvoice, int p_iInvoiceId)
        //
        //*************************************************************
        {
            string sEffecDate = string.Empty;
            string sExpDate = string.Empty;
            string sNodename = string.Empty;

            XmlDocument objXML = null;
            XmlNodeList objNodeList = null;
            CustRate objCustRate = null;
            //rupal:multicurrency
            SysSettings objSysSettings = null;
            LocalCache objCache = null;
            // akaushik5 Added for RMA-15315 Starts
            Account objAccount = default(Account);
            BankAccSub objBankAccSub = default(BankAccSub);
            // akaushik5 Added for RMA-15315 Ends
            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                objXML = m_objXmlDocument;
                objNodeList = objXML.GetElementsByTagName("control");

                objCustRate = (CustRate)m_objDataModelFactory.GetDataModelObject("CustRate", false);
                objCustRate.MoveTo(p_iInvoiceId);
                sEffecDate = Conversion.GetDate(objCustRate.EffectiveDate);
                sExpDate = Conversion.GetDate(objCustRate.ExpirationDate);
                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                foreach (XmlElement objXMLElement in objNodeList)
                {
                    sNodename = objXMLElement.GetAttribute("name");
                    switch (sNodename)
                    {
                        case "optallocate":
                            if (p_objInvoice.Allocated == false)
                                SetValue(objXMLElement, "value", "0", null);
                            else
                                SetValue(objXMLElement, "value", "1", null);
                            if (p_objInvoice.Id != 0)
                                SetValue(objXMLElement, "Disabled", "1", null);
                            break;
                        case "clmNumber":
                            SetValue(objXMLElement, "value", p_objInvoice.ClaimNumber, null);
                            break;
                        case "vendor":
                            if (p_objInvoice.Id != 0)
                                objXMLElement.SetAttribute("lock", "true");
                            SetValue(objXMLElement, p_objInvoice.VendorName, p_objInvoice.VendorId.ToString(), null);
                            break;
                        case "effsdate":
                            SetValue(objXMLElement, "value", sEffecDate, null);
                            break;
                        //Added by Shivendu for MITS 11868.In few cases effective date comes in this node
                        case "effsdate1":
                            SetValue(objXMLElement, "value", sEffecDate, null);
                            break;
                        case "effedate":
                            SetValue(objXMLElement, "value", sExpDate, null);
                            break;
                        case "invNumber":
                            if (p_objInvoice.Allocated == false)
                                objXMLElement.SetAttribute("visible", "0");
                            SetValue(objXMLElement, "value", p_objInvoice.InvoiceNumber, null);
                            break;
                        case "invAmt":
                            if (p_objInvoice.Allocated == false)
                                objXMLElement.SetAttribute("visible", "0");
                            if (p_objInvoice.Id != 0)
                                SetValue(objXMLElement, "lock", "true", null);
                            //*******************************************
                            //code changed
                            //*******************************************
                            //SetValue(objXMLElement,"value",FormatLastTwoDigits(p_objInvoice.InvoiceAmount),null);
                            //*******************************************
                                
                                SetValue(objXMLElement, "value", p_objInvoice.InvoiceAmount , null);
                            
                            break;
                        case "invDate":
                            if (p_objInvoice.Allocated == false)
                                objXMLElement.SetAttribute("title", "Date");
                            //*******************************************
                            //code changed
                            //*******************************************
                            //SetValue(objXMLElement,p_objInvoice.InvoiceDate,string.Empty,null);
                            //*******************************************
                            //SetValue(objXMLElement,"value",p_objInvoice.InvoiceDate,null);
                            objXMLElement.SetAttribute("value", Conversion.ToDbDate(Conversion.ToDate(p_objInvoice.InvoiceDate)).ToString());
                            break;
                        case "invDetail":
                            SetValue(objXMLElement, p_objInvoice.Id.ToString(), p_objInvoice.ClaimNumber, GetInvoiceDetails(p_objInvoice.Id, objFunds));
                            break;
                        case "totinvoice":
                            if (p_objInvoice.Allocated == false)
                                objXMLElement.SetAttribute("visible", "0");
                            SetValue(objXMLElement, "Invoiced : " + FormatLastTwoDigits(p_objInvoice.InvoiceAmount), string.Empty, null);
                            break;
                        case "totavailable":
                            if (p_objInvoice.Allocated == false)
                                objXMLElement.SetAttribute("visible", "0");
                            SetValue(objXMLElement, "value", Convert.ToString((Conversion.ConvertStrToDouble(p_objInvoice.InvoiceAmount) - Conversion.ConvertStrToDouble(p_objInvoice.ItemAmount))), null);
                            break;
                        case "totbill":
                            SetValue(objXMLElement, "Billed : " + FormatLastTwoDigits(p_objInvoice.BillAmount), string.Empty, null);
                            break;
                        case "mode":
                            if (p_objInvoice.Id == 0)
                                SetValue(objXMLElement, "value", "new", null);
                            else
                                SetValue(objXMLElement, "value", "edit", null);
                            break;
                        case "claimid":
                            SetValue(objXMLElement, "value", p_objInvoice.ClaimId.ToString(), null);
                            break;
                        case "claimnum":
                            SetValue(objXMLElement, "value", p_objInvoice.ClaimNumber, null);
                            break;
                        case "h_pid":
                            SetValue(objXMLElement, "value", p_objInvoice.Id.ToString(), null);
                            break;
                        case "h_Invoiced":
                            if (p_objInvoice.Id != 0)
                                SetValue(objXMLElement, "value", FormatLastTwoDigits(p_objInvoice.InvoiceAmount), null);
                            break;
                        case "h_Billed":
                            SetValue(objXMLElement, "value", FormatLastTwoDigits(p_objInvoice.BillAmount), null);
                            break;
                        case "h_ratecode":
                            SetValue(objXMLElement, "value", p_iInvoiceId.ToString(), null);
                            break;
                        case "h_allocated":
                            if (p_objInvoice.Allocated == true)
                                SetValue(objXMLElement, "value", "1", null);
                            else
                                SetValue(objXMLElement, "value", "0", null);
                            break;
                        //rupal:start,multicurrency
                        case "hUseMultiCurrency":
                            objSysSettings = new SysSettings(m_sConnectionString,m_iClientId); //Ash - cloud
                            SetValue(objXMLElement, "value", objSysSettings.UseMultiCurrency.ToString(), null);
                            break;
                        case "currencytypetext":
                            if (p_objInvoice.ClaimId != 0)
                            {
                                
                                try
                                {
                                   
                                    //int iPmtCurrencyCode = CommonFunctions.GetPaymentCurrencyCode(Convert.ToInt32(p_objInvoice.InvTransId), m_sConnectionString);                                    
                                    int iPmtCurrencyCode = CommonFunctions.GetBaseCurrencyCode(m_sConnectionString);
                                    string sShortCode = string.Empty;
                                    string sDesc = string.Empty;
                                    objCache.GetCodeInfo(iPmtCurrencyCode, ref sShortCode, ref sDesc);
                                    SetValue(objXMLElement, "value", sShortCode, null);
                                    SetValue(objXMLElement, "codeid", iPmtCurrencyCode.ToString(), null);
                                    SetValue(objXMLElement, "culture", sDesc, null);
                                }
                                catch(Exception ex)
                                { throw ex; }
                                
                            }
                            break;
                        //RMA-15315:aaggarwal29 start
                        case "DistributionTypeTandE":
                            if (p_objInvoice.InvTransId != 0)
                            {
                                int iTransId = Convert.ToInt32(p_objInvoice.InvTransId);
                                string sShortCode = "", sDesc = "";
                                if (objFunds != null && objFunds.TransId != iTransId)
                                    objFunds.MoveTo(iTransId);

                                objCache.GetCodeInfo(objFunds.DstrbnType, ref sShortCode, ref sDesc);
                                SetValue(objXMLElement, "value", sShortCode + " " + sDesc, null);
                                SetValue(objXMLElement, "codeid", objFunds.DstrbnType.ToString(), null);
                            }
                            break;
                        //RMA-15315:aaggarwal29 end
                        //rupal:end
                    }
                }
                // akaushik5 Added for RMA-15315 Starts
                string sSQL = "SELECT TANDEACCT_ID,USE_SUB_ACCOUNT FROM SYS_PARMS";
                int iAccountId = default(int);
                int iSubAccountId = default(int);

                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            if (Conversion.ConvertStrToInteger(objReader["USE_SUB_ACCOUNT"].ToString()) == 0)
                            {
                                iAccountId = Conversion.ConvertStrToInteger(objReader["TANDEACCT_ID"].ToString());
                                iSubAccountId = 0;
                            }
                            else
                            {
                                iSubAccountId = Conversion.ConvertStrToInteger(objReader["TANDEACCT_ID"].ToString());
                            }
                        }
                        objReader.Close();
                    }
                }

                if (iAccountId != 0)
                {
                    objAccount = (Account)m_objDataModelFactory.GetDataModelObject("Account", false);
                    objAccount.MoveTo(iAccountId);
                }
                if (iSubAccountId != 0)
                {
                    objBankAccSub = (BankAccSub)m_objDataModelFactory.GetDataModelObject("BankAccSub", false);
                    objBankAccSub.MoveTo(iSubAccountId);
                    iAccountId = objBankAccSub.AccountId;
                    objAccount = (Account)m_objDataModelFactory.GetDataModelObject("Account", false);
                    if (iAccountId != 0)
                        objAccount.MoveTo(iAccountId);

                }

                if (iAccountId == 0 && iSubAccountId == 0)
                {
                    throw new RMAppException(GetMyError(Globalization.GetString("InvoiceDriver.CreateInvoice.ErrorNotSet", m_iClientId), 4, string.Empty, m_iClaimId, 1));
                }

                XmlNode xmlNode = objXML.DocumentElement;
                int iEFTAccount = objAccount.EFTAccount == true ? 1 : 0;
                int iEFTDitributionCodeId = objCache.GetCodeId("EFT", "DISTRIBUTION_TYPE");
                XmlElement objXMLEle = objXML.CreateElement("IsEFTAccount");
                objXMLEle.SetAttribute("value", iEFTAccount.ToString());
                xmlNode.AppendChild(objXMLEle);

                objXMLEle = objXML.CreateElement("EFTDistributionCodeId");
                objXMLEle.SetAttribute("value", iEFTDitributionCodeId.ToString());
                xmlNode.AppendChild(objXMLEle);

                Object objDistributionType;
                int iDefaultDistributionType;
                if (objAccount.EFTAccount)
                {
                    iDefaultDistributionType = iEFTDitributionCodeId;
                }
                else
                {
                    objDistributionType = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT DEF_DSTRBN_TYPE_CODE FROM CHECK_OPTIONS");
                    if (objDistributionType != null)
                    {
                        iDefaultDistributionType = Conversion.ConvertObjToInt(objDistributionType, m_iClientId);
                    }
                }

                objXMLEle = objXML.CreateElement("ManualDistributionCodeId");
                objXMLEle.SetAttribute("value", objCache.GetCodeId("MAL", "DISTRIBUTION_TYPE").ToString());
                xmlNode.AppendChild(objXMLEle);
                // akaushik5 Added for RMA-15315 Ends
                return objXML;

                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //
                //To return XML rather then String
                //Initially:- return objXML.OuterXml;
                //Finally:- return objXML;
                //
                //*************************************************************
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.FillXmlForInvoice.ErrorFill", m_iClientId), p_objException);
            }
            finally
            {
                objXML = null;
                objNodeList = null;
                if (objCustRate != null)
                    objCustRate.Dispose();
                //rupal:multicurrency
                if (objSysSettings != null)
                    objSysSettings = null;
                //rupal:multicurrency
                if (objCache != null)
                    objCache = null;
                // akaushik5 Added for RMA-15315 Starts
                if (!object.ReferenceEquals(objAccount, default(Account)))
                {
                    objAccount.Dispose();
                }

                if (!object.ReferenceEquals(objBankAccSub, default(BankAccSub)))
                {
                    objBankAccSub.Dispose();
                }
                // akaushik5 Added for RMA-15315 Ends
            }
        }
        #endregion

        #region "GetRateTablesXML()"
        /// Name		: GetRateTablesXML
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 08/03/2005     * Return type from String to XmlDocument * Mohit Yadav
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// This Function returns the details of Rate Tables in XML format
        /// These details are fetched from the global XMLDocument 
        /// </summary>
        /// <returns>XML for Rate Tables</returns>
        private XmlDocument GetRateTablesXML()

            //**************************************************************
        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
        //
        //To return XML rather then String
        //Initially:- private string GetRateTablesXML()
        //Finally:- private XmlDocument GetRateTablesXML()
        //
        //*************************************************************
        {
            int iRecordCounter = 0;

            string sSQL = String.Empty;

            XmlDocument objXMLDoc = null;
            XmlElement objXMLElement = null;
            XmlNode objXMLNode = null;
            DbReader objReader = null;

            try
            {
                sSQL = "SELECT RATE_TABLE_ID,TABLE_NAME,CUSTOMER_EID,CUST_RATE.EFFECTIVE_DATE,CUST_RATE.EXPIRATION_DATE,FIRST_NAME," +
                       "LAST_NAME FROM CUST_RATE,ENTITY WHERE CUSTOMER_EID=ENTITY_ID ORDER BY TABLE_NAME";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                objXMLDoc = m_objXmlDocument;
                objXMLNode = objXMLDoc.SelectSingleNode("TandE");

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        objXMLElement = (XmlElement)objXMLDoc.ImportNode(GetNewElement("rateTableData"), false);

                        if ((iRecordCounter % 2) == 0)
                            objXMLElement.SetAttribute("bgcolor", "data1");
                        else
                            objXMLElement.SetAttribute("bgcolor", "data2");

                        objXMLElement.SetAttribute("id", objReader["RATE_TABLE_ID"].ToString());
                        objXMLElement.SetAttribute("name", objReader["TABLE_NAME"].ToString());
                        objXMLElement.SetAttribute("custid", objReader["CUSTOMER_EID"].ToString());
                        objXMLElement.SetAttribute("efdate", Conversion.GetDate(objReader["EFFECTIVE_DATE"].ToString()));
                        objXMLElement.SetAttribute("exdate", Conversion.GetDate(objReader["EXPIRATION_DATE"].ToString()));
                        objXMLElement.SetAttribute("custname", objReader["LAST_NAME"].ToString() + (objReader["FIRST_NAME"].ToString() == "" ? "" : ("," + objReader["FIRST_NAME"].ToString())));

                        objXMLNode.AppendChild(objXMLElement);
                        iRecordCounter = iRecordCounter + 1;
                    }
                    objReader.Close();
                }
                return objXMLDoc;

                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //
                //To return XML rather then String
                //Initially:- return objXMLDoc.OuterXml;
                //Finally:- return objXMLDoc;
                //
                //*************************************************************
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetRateTablesXML.ErrorGet", m_iClientId), p_objException);
            }
            finally
            {
                objXMLElement = null;
                objXMLNode = null;
                objXMLDoc = null;
                m_objXmlDocument = null;

                if (objReader != null)
                    objReader.Dispose();
            }
        }
        #endregion

        #region "GetRateTablesXMLs()"
        /// Name		: GetRateTablesXML
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 08/03/2005     * Return type from String to XmlDocument * Mohit Yadav
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// This Function returns the details of Rate Tables in XML format
        /// These details are fetched from the global XMLDocument 
        /// </summary>
        /// <returns>XML for Rate Tables</returns>
        private XmlDocument GetRateTablesXMLs(int iCustomerId)

            //**************************************************************
        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
        //
        //To return XML rather then String
        //Initially:- private string GetRateTablesXML()
        //Finally:- private XmlDocument GetRateTablesXML()
        //
        //*************************************************************
        {
            int iRecordCounter = 0;

            string sSQL = String.Empty;

            XmlDocument objXMLDoc = null;
            XmlElement objXMLElement = null;
            XmlNode objXMLNode = null;
            DbReader objReader = null;
            LocalCache objLocal = new LocalCache(m_sConnectionString, m_iClientId);

            try
            {
                sSQL = "SELECT RATE_TABLE_ID,TABLE_NAME,CUSTOMER_EID,CUST_RATE.EFFECTIVE_DATE,CUST_RATE.EXPIRATION_DATE,FIRST_NAME," +
                       "LAST_NAME FROM CUST_RATE,ENTITY WHERE CUSTOMER_EID=ENTITY_ID AND CUSTOMER_EID=" + iCustomerId +
                       " ORDER BY TABLE_NAME";//MITS 16127
                //" AND EXPIRATION_DATE >= '" + DateTime.Today.ToString("yyyyMMdd")+ "' ORDER BY TABLE_NAME";

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                string sName = objLocal.GetEntityLastFirstName(iCustomerId);
                objXMLDoc = m_objXmlDocument;
                objXMLNode = objXMLDoc.SelectSingleNode("TandE");

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        objXMLElement = (XmlElement)objXMLDoc.ImportNode(GetNewElement("rateTableData"), false);

                        if ((iRecordCounter % 2) == 0)
                        {
                            objXMLElement.SetAttribute("bgcolor", "data1");
                        }
                        else
                        {
                            objXMLElement.SetAttribute("bgcolor", "data2");
                        }

                        objXMLElement.SetAttribute("id", objReader["RATE_TABLE_ID"].ToString());
                        objXMLElement.SetAttribute("name", objReader["TABLE_NAME"].ToString());
                        objXMLElement.SetAttribute("custid", objReader["CUSTOMER_EID"].ToString());
                        objXMLElement.SetAttribute("efdate", Conversion.GetDate(objReader["EFFECTIVE_DATE"].ToString()));
                        objXMLElement.SetAttribute("exdate", Conversion.GetDate(objReader["EXPIRATION_DATE"].ToString()));
                        objXMLElement.SetAttribute("custname", objReader["LAST_NAME"].ToString() + (objReader["FIRST_NAME"].ToString() == "" ? "" : ("," + objReader["FIRST_NAME"].ToString())));

                        objXMLNode.AppendChild(objXMLElement);
                        iRecordCounter = iRecordCounter + 1;
                    }

                    if (iRecordCounter == 0)
                    {
                        throw new RMAppException("No Rate Tables are valid for " + sName);//fixed misspelling of valid [MJP 4-18-09]
                    }

                    objReader.Close();
                }
                else
                {
                    throw new RMAppException("No Rate Tables are valid for " + sName);//fixed misspelling of valid [MJP 4-18-09]
                }

                return objXMLDoc;

                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //
                //To return XML rather then String
                //Initially:- return objXMLDoc.OuterXml;
                //Finally:- return objXMLDoc;
                //
                //*************************************************************
            }
            catch (Exception p_objException)
            {
                //MITS 15546 - throw actual error rather then a generic error
                //Initially: throw new RMAppException(Globalization.GetString("InvoiceDriver.GetRateTablesXML.ErrorGet"), p_objException);
                //  Finally: throw new RMAppException(p_objException.Message);
                throw new RMAppException(p_objException.Message);
            }
            finally
            {
                objXMLElement = null;
                objXMLNode = null;
                objXMLDoc = null;
                m_objXmlDocument = null;

                if (objReader != null)
                    objReader.Dispose();

                if (objLocal != null)
                    objLocal.Dispose();
            }
        }
        #endregion

        #region "SaveRateTable(XmlNodeList p_objXmlNodeList, string p_sUpdateMode,out int p_iRateTableId)"
        /// Name		: SaveRatetable
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Saves the rate table into the database. 
        /// </summary>
        /// <param name="p_objXmlNodeList">Contains the ratetable details.</param>
        /// <param name="p_supdateMode">Mode os save, either new or edit an existing one.</param>
        /// <param name="p_iRateTableId">rate tabke ID</param>
        /// <returns></returns>
        private bool SaveRateTable(XmlNodeList p_objXmlNodeList, string p_sUpdateMode, out int p_iRateTableId)
        {
            p_iRateTableId = 0;
            int iRateTableId = 0;
            int iCustomerId = 0;

            string sRateTableName = string.Empty;
            string sEfecDate = string.Empty;
            string sExpDate = string.Empty;

            CustRate objCustRate = null;
            CustRateDetail objCustRateDetail = null;

            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                foreach (XmlElement objXMLElement in p_objXmlNodeList)
                {
                    switch (objXMLElement.GetAttribute("name"))
                    {
                        case "rtname":
                            sRateTableName = objXMLElement.GetAttribute("value");
                            break;
                        case "tableid":
                            if (p_sUpdateMode == "edit")
                                iRateTableId = Conversion.ConvertStrToInteger(objXMLElement.GetAttribute("value"));
                            else if (p_sUpdateMode == "new")
                                iRateTableId = 0;
                            break;
                        case "customer":
                            iCustomerId = Conversion.ConvertStrToInteger(objXMLElement.GetAttribute("id"));
                            break;
                        case "efdate":
                            //**************************************************************************
                            //Mohit Yadav changed the code from 1 to 2 to get the date value
                            //1 --> sEfecDate = Conversion.GetDate(objXMLElement.InnerText); 
                            //2 --> sEfecDate = Conversion.GetDate(objXMLElement.GetAttribute("value"));
                            //**************************************************************************
                            sEfecDate = Conversion.GetDate(objXMLElement.GetAttribute("value"));
                            //**************************************************************************
                            break;
                        case "exdate":
                            //**************************************************************************
                            //Mohit Yadav changed the code from 1 to 2 to get the date value
                            //1 --> sExpDate  = Conversion.GetDate(objXMLElement.InnerText);
                            //2 --> sExpDate  = Conversion.GetDate(objXMLElement.GetAttribute("value"));
                            //**************************************************************************
                            sExpDate = Conversion.GetDate(objXMLElement.GetAttribute("value"));
                            //**************************************************************************
                            break;
                    }
                }
                //rsushilaggar MITS 31418 Date 02/09/2013
                //if (ChkDupValue("CUST_RATE", "TABLE_NAME", "CUSTOMER_EID", sRateTableName, iCustomerId) == 1 && p_sUpdateMode == "new")
                if (ChkDupValue("CUST_RATE", "TABLE_NAME", "CUSTOMER_EID", sRateTableName, iCustomerId, iRateTableId) == 1)
                {
                    throw new RMAppException(GetMyError(Globalization.GetString("InvoiceDriver.SaveRateTable.ErrorDuplicate", m_iClientId), 1, string.Empty, 0, 0));
                }

                objCustRate = (CustRate)m_objDataModelFactory.GetDataModelObject("CustRate", false);
                //*****************************************
                //Mohit Yadav
                //MGaba2:MITS 13140:While editing,we will be using the same table id to save
                objCustRate.RateTableId = iRateTableId;
                //*****************************************

                objCustRate.TableName = sRateTableName;
                objCustRate.CustomerEid = iCustomerId;
                objCustRate.EffectiveDate = sEfecDate;
                objCustRate.ExpirationDate = sExpDate;

                if (p_sUpdateMode == "new")
                {
                    objCustRate.AddedByUser = m_sLoginName;
                    objCustRate.DttmRcdAdded = Conversion.ToDbDateTime(DateTime.Now).ToString();
                    //MGaba2:MITS 13140: While updating,in where clause,this field is used 
                    objCustRate.DttmRcdLastUpd  = Conversion.ToDbDateTime(DateTime.Now).ToString(); 
                }
                else if (p_sUpdateMode == "edit")
                {   //MGaba2:MITS 13140: While updating,in where clause,DttmRcdLastUpd field is used 
                    CustRate objTempCustRate = (CustRate)m_objDataModelFactory.GetDataModelObject("CustRate", false);
                    objTempCustRate.MoveTo(objCustRate.RateTableId);                    
                    objCustRate.UpdatedByUser = m_sLoginName;
                    objCustRate.DttmRcdLastUpd = objTempCustRate.DttmRcdLastUpd.ToString();
                    //MGaba2
                    //objCustRate.DttmRcdAdded = objTempCustRate.DttmRcdAdded.ToString();
                }

                objCustRate.Save();
                p_iRateTableId = objCustRate.RateTableId;
                return true;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.SaveRateTable.ErrorSave", m_iClientId), p_objException);
            }
            finally
            {
                if (objCustRate != null)
                    objCustRate.Dispose();
                if (objCustRateDetail != null)
                    objCustRateDetail.Dispose();
            }
        }
        #endregion

        #region "GetBillExpSummeryXml(int p_iClaimId)"
        /// Name		: GetBillExpSummeryXml
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 09/03/2005     * Return type from String to XmlDocument * Mohit Yadav
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Fetches the BillExp Summery as XML.
        /// </summary>
        /// <param name="p_iClaimId">ClaimId</param>
        /// <returns>XML of BillExpSummary</returns>
        private XmlDocument GetBillExpSummeryXml(int p_iClaimId)

            //**************************************************************
        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
        //
        //To return XML rather then String
        //Initially:- private string GetBillExpSummeryXml(int p_iClaimId)
        //Finally:- private XmlDocument GetBillExpSummeryXml(int p_iClaimId)
        //
        //*************************************************************
        {
            int iInvoiceId = 0;
            int iInvTransId = 0;
            int iVandorId = 0;
            int iPayeeId = 0;
            int iCustEId = 0;
            int iRecordCounter = 0;

            double dBillAmount = 0.0;
            double dTotalBilled = 0.0;
            double dTotalNoPost = 0.0;
            double dTotalNoPostNoBill = 0.0;
            double dInvoiceAmount = 0.0;
            double dInvoiveTotal = 0.0;

            string sSQL = string.Empty;
            string sSQLTemp = string.Empty;
            string sVendorFullName = string.Empty;
            string sInvoiceNum = string.Empty;
            string sInvoiceDate = string.Empty;
            string sShortCode = string.Empty;
            string sTransNumber = string.Empty;
            string sDateOfCheck = string.Empty;

            XmlDocument objXMLDoc = null;
            XmlElement objXMLElement = null;
            XmlElement objXMLChildEle = null;
            DbReader objReader = null;
            DbReader objTempReader = null;
            Entity objEntity = null;
            LocalCache objLocalCache = null;
            
            try
            {
                objXMLDoc = m_objXmlDocument;
                objXMLElement = (XmlElement)objXMLDoc.SelectSingleNode("TandE");

                sSQL = "SELECT AR_ROW_ID, CUSTOMER_EID, INV_NUMBER, INV_DATE, WORK_PERF_BY_EID, INV_TOTAL," +
                       "INV_TRANS_ID FROM ACCT_REC WHERE CLAIM_ID=" + p_iClaimId + " AND POSTED <> 0 AND " +
                       "VOID_FLAG = 0 ORDER BY INV_DATE DESC, POSTED_ID, AR_ROW_ID";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);

                if (objReader != null)
                {
                    iRecordCounter = 1;

                    while (objReader.Read())
                    {
                        objXMLChildEle = (XmlElement)objXMLDoc.ImportNode(GetNewElement("TandEData"), false);

                        iInvoiceId = Conversion.ConvertStrToInteger(objReader["AR_ROW_ID"].ToString());
                        iInvTransId = Conversion.ConvertStrToInteger(objReader["INV_TRANS_ID"].ToString());
                        iVandorId = Conversion.ConvertStrToInteger(objReader["WORK_PERF_BY_EID"].ToString());
                        iPayeeId = Conversion.ConvertStrToInteger(objReader["WORK_PERF_BY_EID"].ToString());
                        iCustEId = Conversion.ConvertStrToInteger(objReader["CUSTOMER_EID"].ToString());

                        //objEntity = new Entity(); 
                        sVendorFullName = objEntity.GetLastFirstName(iVandorId);
                        sInvoiceNum = objReader["INV_NUMBER"].ToString();
                        sInvoiceDate = Conversion.GetDate(objReader["INV_DATE"].ToString());
                        dInvoiceAmount = Conversion.ConvertStrToDouble(objReader["INV_TOTAL"].ToString());

                        dInvoiveTotal = dInvoiveTotal + dInvoiceAmount;

                        sSQL = "SELECT SUM(TOTAL) FROM ACCT_REC_DETAIL WHERE AR_ROW_ID=" + iInvoiceId;
                        sSQLTemp = sSQL;
                        sSQL = sSQL + " AND BILLABLE<>0";
                        objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        if (objTempReader != null)
                        {
                            if (objTempReader.Read())
                            {
                                dBillAmount = Conversion.ConvertStrToDouble(objTempReader[0].ToString());
                            }
                            dTotalBilled = dTotalBilled + dBillAmount;
                            objTempReader.Close();
                        }

                        sSQL = sSQLTemp + " AND POSTABLE=0";
                        objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        if (objTempReader != null)
                        {
                            if (objTempReader.Read())
                            {
                                dTotalNoPost = dTotalNoPost + Conversion.ConvertStrToDouble(objTempReader[0].ToString());
                            }
                            objTempReader.Close();
                        }

                        sSQL = sSQLTemp + " AND POSTABLE=0 AND BILLABLE=0";
                        objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        if (objTempReader != null)
                        {
                            if (objTempReader.Read())
                            {
                                dTotalNoPostNoBill = dTotalNoPostNoBill + Conversion.ConvertStrToDouble(objTempReader[0].ToString());
                            }
                            objTempReader.Close();
                        }

                        //rsushilaggar MITS 31427 Date 02/09/2013
                        sShortCode = string.Empty;
                        sTransNumber = string.Empty;
                        sDateOfCheck = string.Empty;

                        sSQL = "SELECT STATUS_CODE,TRANS_NUMBER,DATE_OF_CHECK FROM FUNDS WHERE TRANS_ID=" + iInvTransId;
                        objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        if (objTempReader != null)
                        {
                            if (objTempReader.Read())
                            {
                                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                                sShortCode = objLocalCache.GetShortCode(Conversion.ConvertStrToInteger(objTempReader["STATUS_CODE"].ToString()));
                                sTransNumber = objTempReader["TRANS_NUMBER"].ToString();
                                sDateOfCheck = Conversion.GetDate(objTempReader["DATE_OF_CHECK"].ToString());
                            }
                            objTempReader.Close();
                        }

                        if ((iRecordCounter % 2) == 0)
                            objXMLChildEle.SetAttribute("bgcolor", "data1");
                        else
                            objXMLChildEle.SetAttribute("bgcolor", "data2");

                        objXMLChildEle.SetAttribute("id", iInvoiceId.ToString());
                        objXMLChildEle.SetAttribute("vendor", sVendorFullName);
                        objXMLChildEle.SetAttribute("invoicenumber", sInvoiceNum);
                        objXMLChildEle.SetAttribute("invoicedate", sInvoiceDate);
                        //rupal:start,multicurrency
                        
                        //objXMLChildEle.SetAttribute("invoiceamt", string.Format("{0:C}", dInvoiceAmount));
                        objXMLChildEle.SetAttribute("invoiceamt", CommonFunctions.ConvertCurrency(0, dInvoiceAmount, CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                        //objXMLChildEle.SetAttribute("billamt", string.Format("{0:C}", dBillAmount));
                        objXMLChildEle.SetAttribute("billamt", CommonFunctions.ConvertCurrency(0, dBillAmount, CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                        //rupal:end
                        objXMLChildEle.SetAttribute("status", sShortCode);
                        objXMLChildEle.SetAttribute("chknum", sTransNumber);
                        objXMLChildEle.SetAttribute("transid", iInvTransId.ToString());
                        objXMLChildEle.SetAttribute("payeeid", iPayeeId.ToString());
                        objXMLChildEle.SetAttribute("chkdate", sDateOfCheck);

                        objXMLElement.AppendChild(objXMLChildEle);
                        iRecordCounter++;
                    }
                    objReader.Close();
                }

                string sRateTableId = string.Empty;
                string sTablename = string.Empty;
                string sClaimNumber = string.Empty;

                sSQL = "SELECT CLAIM.RATE_TABLE_ID,CLAIM.CLAIM_NUMBER, CUST_RATE.TABLE_NAME, CUST_RATE.EFFECTIVE_DATE," +
                       " CUST_RATE.EXPIRATION_DATE FROM CLAIM, CUST_RATE WHERE CLAIM_ID=" + p_iClaimId +
                       " AND CLAIM.RATE_TABLE_ID = CUST_RATE.RATE_TABLE_ID";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        sRateTableId = objReader["RATE_TABLE_ID"].ToString();
                        sTablename = objReader["TABLE_NAME"].ToString();
                        sClaimNumber = objReader["CLAIM_NUMBER"].ToString();
                    }
                    objReader.Close();
                }

                //rupal:start, multicurrency               
                int iBaseCurrType = CommonFunctions.GetBaseCurrencyCode(m_sConnectionString);                
                objXMLChildEle = (XmlElement)objXMLDoc.SelectSingleNode("TandE");
                //objXMLChildEle.SetAttribute("invtotal", string.Format("{0:C}", dInvoiveTotal));
                objXMLChildEle.SetAttribute("invtotal", CommonFunctions.ConvertCurrency(0, dInvoiveTotal, CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                //objXMLChildEle.SetAttribute("billtotal", string.Format("{0:C}", dTotalBilled));
                objXMLChildEle.SetAttribute("billtotal", CommonFunctions.ConvertCurrency(0, dTotalBilled, CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                objXMLChildEle.SetAttribute("ratetableid", sRateTableId);
                objXMLChildEle.SetAttribute("ratetable", sTablename);
                objXMLChildEle.SetAttribute("customerid", iCustEId.ToString());
                objXMLChildEle.SetAttribute("claimnumber", sClaimNumber);
                objXMLChildEle.SetAttribute("claimid", p_iClaimId.ToString());
                //objXMLChildEle.SetAttribute("nopost", string.Format("{0:C}", dTotalNoPost));
                objXMLChildEle.SetAttribute("nopost", CommonFunctions.ConvertCurrency(0, dTotalNoPost, CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                //objXMLChildEle.SetAttribute("nopostnobill", string.Format("{0:C}", dTotalNoPostNoBill));
                objXMLChildEle.SetAttribute("nopostnobill", CommonFunctions.ConvertCurrency(0, dTotalNoPostNoBill, CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                //rupal:end
                return objXMLDoc;

                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //
                //To return XML rather then String
                //Initially:- return objXMLDoc.OuterXml;
                //Finally:- return objXMLDoc;
                //
                //*************************************************************
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetBillExpSummeryXml.ErrorGet", m_iClientId), p_objException);
            }
            finally
            {
                objXMLElement = null;
                objXMLChildEle = null;
                objXMLDoc = null;
                if (objReader != null)
                    objReader.Dispose();
                if (objTempReader != null)
                    objTempReader.Dispose();
                if (objEntity != null)
                    objEntity.Dispose();
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }
        }
        #endregion

        #region "GetTandESummaryXml(int p_iClaimId)"
        /// Name		: GetTandESummaryXml
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 09/03/2005     * Return type from String to XmlDocument * Mohit Yadav
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Fetches Time and Expense SummaryXml
        /// </summary>
        /// <param name="p_iClaimId">Claim ID</param>
        /// <returns>XML for TandESummeryXml</returns>
        private XmlDocument GetTandESummaryXml(int p_iClaimId)

            //**************************************************************
        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
        //
        //To return XML rather then String
        //Initially:- private string GetTandESummaryXml(int p_iClaimId)
        //Finally:- private XmlDocument GetTandESummaryXml(int p_iClaimId)
        //
        //*************************************************************
        {
            bool bUseSubAccount = false;

            int iInvoiceId = 0;
            int iInvTransId = 0;
            int iVandorId = 0;
            int iPayeeId = 0;
            int iCustEId = 0;
            int iRateLevel = 0;
            int iTandEAcctId = 0;
            int iDepartmentEId = 0;
            int iRecordCounter = 0;
            int iNextBchNum = 0;            

            long lNextChkNum = 0;  //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length

            double dInvoiceAmount = 0.0;
            double dInvoiveTotal = 0.0;
            double dBillAmount = 0.0;
            double dTotalBilled = 0.0;

            string sVendorFullName = string.Empty;
            string sInvoiceNum = string.Empty;
            string sInvoiceDate = string.Empty;
            string sSQL = string.Empty;
            string sSQLTemp = string.Empty;
            string sShortCode = string.Empty;
            string sTransNumber = string.Empty;
            string sDateOfCheck = string.Empty;
            string sRateTableId = string.Empty;
            string sTablename = string.Empty;
            string sClaimNumber = string.Empty;

            XmlDocument objXMLDoc = null;
            XmlElement objXMLElement = null;
            XmlElement objXMLChildEle = null;
            DbReader objReader = null;
            DbReader objTempReader = null;
            Entity objEntity = null;
            LocalCache objLocalCache = null;

            try
            {
                objXMLDoc = m_objXmlDocument;
                objXMLElement = (XmlElement)objXMLDoc.SelectSingleNode("TandE");

                objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);

                sSQL = "SELECT AR_ROW_ID, ALLOCATED, CUSTOMER_EID, INV_NUMBER, INV_TOTAL, WORK_PERF_BY_EID, " +
                       "INV_DATE, INV_TRANS_ID, ADDED_BY_USER FROM ACCT_REC WHERE CLAIM_ID=" + p_iClaimId +
                       " AND POSTED=0 AND VOID_FLAG=0 ORDER BY INV_DATE DESC";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    iRecordCounter = 1;

                    while (objReader.Read())
                    {
                        objXMLChildEle = (XmlElement)objXMLDoc.ImportNode(GetNewElement("TandEData"), false);

                        if ((iRecordCounter % 2) == 0)
                            objXMLChildEle.SetAttribute("bgcolor", "data1");
                        else
                            objXMLChildEle.SetAttribute("bgcolor", "data2");
                        iInvoiceId = Conversion.ConvertStrToInteger(objReader["AR_ROW_ID"].ToString());
                        iInvTransId = Conversion.ConvertStrToInteger(objReader["INV_TRANS_ID"].ToString());
                        iVandorId = Conversion.ConvertStrToInteger(objReader["WORK_PERF_BY_EID"].ToString());
                        iPayeeId = Conversion.ConvertStrToInteger(objReader["WORK_PERF_BY_EID"].ToString());

                        sVendorFullName = objEntity.GetLastFirstName(iVandorId);
                        sInvoiceNum = objReader["INV_NUMBER"].ToString();
                        sInvoiceDate = Conversion.GetDate(objReader["INV_DATE"].ToString());
                        //*******************************************************************
                        //Mohit Yadav changed the code from (1) to (2)
                        //1 --> dInvoiceAmount = Conversion.ConvertStrToDouble(string.Format("{0:C}",Conversion.ConvertStrToInteger(objReader["INV_TOTAL"].ToString())));
                        //2 --> dInvoiceAmount = Convert.ToDouble(Conversion.ConvertStrToInteger(objReader["INV_TOTAL"].ToString()));
                        //Michael Palinski changed the code from (2) to (3)
                        //3 --> dInvoiceAmount = Conversion.ConvertObjToDouble(objReader["INV_TOTAL"], m_iClientId.ToString());
                        //*******************************************************************
                        dInvoiceAmount = Conversion.ConvertObjToDouble(objReader["INV_TOTAL"].ToString(), m_iClientId);
                        //*******************************************************************

                        dInvoiveTotal = dInvoiveTotal + dInvoiceAmount;

                        sSQL = "SELECT SUM(TOTAL) FROM ACCT_REC_DETAIL WHERE AR_ROW_ID=" + iInvoiceId + " AND BILLABLE<>0";
                        objTempReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        if (objTempReader != null)
                        {
                            if (objTempReader.Read())
                            {
                                //*******************************************************************
                                //Mohit Yadav changed the code from (1) to (2)
                                //1 --> dBillAmount = Conversion.ConvertStrToDouble(string.Format("{0:C}",Conversion.ConvertStrToInteger(objTempReader[0].ToString())));
                                //2 --> dBillAmount = Convert.ToDouble(Conversion.ConvertStrToDouble(objTempReader[0].ToString()));
                                //*******************************************************************
                                dBillAmount = Convert.ToDouble(Conversion.ConvertStrToDouble(objTempReader[0].ToString()));
                                //*******************************************************************
                            }
                            dTotalBilled = dTotalBilled + dBillAmount;
                            objTempReader.Close();
                        }

                        Funds objFunds = null;
                        objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
                        objFunds.MoveTo(iInvTransId);
                        objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                        sShortCode = objLocalCache.GetShortCode(objFunds.StatusCode);

                        objXMLChildEle.SetAttribute("id", iInvoiceId.ToString());
                        objXMLChildEle.SetAttribute("vendor", sVendorFullName);
                        objXMLChildEle.SetAttribute("invoicenumber", sInvoiceNum);
                        objXMLChildEle.SetAttribute("invoicedate", sInvoiceDate);
                        //rupal:start,multicurrency
                        //CommonFunctions.ConvertCurrency(p_iClaimId, dInvoiceAmount, eNavType, m_sConnectionString));
                        //objXMLChildEle.SetAttribute("invoiceamt", string.Format("{0:C}", dInvoiceAmount));
                        objXMLChildEle.SetAttribute("invoiceamt", CommonFunctions.ConvertCurrency(0, dInvoiceAmount, CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                        //objXMLChildEle.SetAttribute("billamt", string.Format("{0:C}", dBillAmount));
                        objXMLChildEle.SetAttribute("billamt", CommonFunctions.ConvertCurrency(0, dBillAmount, CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                        //rupal:end
                        objXMLChildEle.SetAttribute("status", sShortCode);
                        objXMLChildEle.SetAttribute("transid", iInvTransId.ToString());
                        objXMLChildEle.SetAttribute("payeeid", iPayeeId.ToString());

                        objXMLElement.AppendChild(objXMLChildEle);
                        iRecordCounter++;
                    }
                    objReader.Close();
                }

                /*This Block is commented by Shivendu for MITS 12347.Has been moved below.
                sSQL = "SELECT CLAIM.RATE_TABLE_ID,CLAIM.CLAIM_NUMBER, CUST_RATE.TABLE_NAME, CUST_RATE.EFFECTIVE_DATE," +
                    " CUST_RATE.EXPIRATION_DATE FROM CLAIM, CUST_RATE WHERE CLAIM_ID=" + p_iClaimId +
                    " AND CLAIM.RATE_TABLE_ID = CUST_RATE.RATE_TABLE_ID";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        sRateTableId = objReader["RATE_TABLE_ID"].ToString();
                        sTablename = objReader["TABLE_NAME"].ToString();
                        //sClaimNumber = objReader["CLAIM_NUMBER"].ToString();
                    }
                    objReader.Close();
                }*/

                sSQL = "SELECT * FROM CLAIM WHERE CLAIM_ID=" + p_iClaimId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        sClaimNumber = objReader["CLAIM_NUMBER"].ToString();
                    }
                    objReader.Close();
                }

                sSQL = "SELECT DEPT_EID FROM EVENT WHERE EVENT_ID IN(SELECT EVENT_ID FROM CLAIM WHERE CLAIM_ID=" + p_iClaimId + ")";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iDepartmentEId = Conversion.ConvertStrToInteger(objReader["DEPT_EID"].ToString());
                    }
                    objReader.Close();
                }

                sSQL = "SELECT RATE_LEVEL,TANDEACCT_ID,USE_SUB_ACCOUNT FROM SYS_PARMS";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iRateLevel = Conversion.ConvertStrToInteger(objReader["RATE_LEVEL"].ToString());
                        iTandEAcctId = Conversion.ConvertStrToInteger(objReader["TANDEACCT_ID"].ToString());
                        if (objReader["USE_SUB_ACCOUNT"].ToString() == "0")
                            bUseSubAccount = false;
                        else
                            bUseSubAccount = true;

                    }
                    objReader.Close();
                }

                iCustEId = GetParentOrg(iRateLevel, iDepartmentEId);
                //Start by Shivendu for MITS 12347
                sSQL = "SELECT CLAIM.RATE_TABLE_ID,CLAIM.CLAIM_NUMBER, CUST_RATE.TABLE_NAME, CUST_RATE.EFFECTIVE_DATE," +
                       " CUST_RATE.EXPIRATION_DATE FROM CLAIM, CUST_RATE WHERE CLAIM_ID=" + p_iClaimId +
                       " AND CLAIM.RATE_TABLE_ID = CUST_RATE.RATE_TABLE_ID" + " AND CUST_RATE.CUSTOMER_EID=" + iCustEId + 
                       " AND CUST_RATE.EXPIRATION_DATE >= '" + DateTime.Today.ToString("yyyyMMdd") + 
                       "' AND CUST_RATE.EFFECTIVE_DATE <= '" + DateTime.Today.ToString("yyyyMMdd") + "'";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        sRateTableId = objReader["RATE_TABLE_ID"].ToString();
                        sTablename = objReader["TABLE_NAME"].ToString();
                        //sClaimNumber = objReader["CLAIM_NUMBER"].ToString();
                    }
                    objReader.Close();
                }
                //End by Shivendu for MITS 12347

                if (bUseSubAccount == false)
                {
                    Account objAccount = null;
                    objAccount = (Account)m_objDataModelFactory.GetDataModelObject("Account", false);

                    try
                    {
                        objAccount.MoveTo(iTandEAcctId);
                        iNextBchNum = objAccount.NextBatchNumber;
                        lNextChkNum = objAccount.NextCheckNumber;
                    }
                    catch
                    {
                        iNextBchNum = 0;
                        lNextChkNum = 0;
                    }
                }

                objXMLChildEle = (XmlElement)objXMLDoc.SelectSingleNode("TandE");
                
                //rupal:start,multicurrency
                //objXMLChildEle.SetAttribute("invtotal", string.Format("{0:C}", dInvoiveTotal));
                objXMLChildEle.SetAttribute("invtotal", CommonFunctions.ConvertCurrency(0, dInvoiveTotal, CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                //objXMLChildEle.SetAttribute("billtotal", string.Format("{0:C}", dTotalBilled));
                objXMLChildEle.SetAttribute("billtotal", CommonFunctions.ConvertCurrency(0, dTotalBilled, CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                //rupal:end
                objXMLChildEle.SetAttribute("ratetableid", sRateTableId);
                objXMLChildEle.SetAttribute("ratetable", sTablename);
                objXMLChildEle.SetAttribute("customerid", iCustEId.ToString());
                objXMLChildEle.SetAttribute("claimnumber", sClaimNumber);
                objXMLChildEle.SetAttribute("claimid", p_iClaimId.ToString());
                objXMLChildEle.SetAttribute("nextcheck", iNextBchNum.ToString());
                objXMLChildEle.SetAttribute("nextbetch", lNextChkNum.ToString());
                objXMLChildEle.SetAttribute("account", iTandEAcctId.ToString());

                return objXMLDoc;

                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //
                //To return XML rather then String
                //Initially:- return objXMLDoc.OuterXml;
                //Finally:- return objXMLDoc;
                //
                //*************************************************************
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetTandESummeryXml.ErrorGet", m_iClientId), p_objException);
            }
            finally
            {
                objXMLElement = null;
                objXMLChildEle = null;
                objXMLDoc = null;
                if (objReader != null)
                    objReader.Dispose();
                if (objTempReader != null)
                    objReader.Dispose();
                if (objEntity != null)
                    objEntity.Dispose();
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }
        }
        #endregion

        #region "GetTandEDetailsXml(int p_iInvoiceId)"
        /// Name		: GetTandEDetailsXml
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// 09/03/2005     * Return type from String to XmlDocument * Mohit Yadav
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// fetches Time and Expense Detail XML.
        /// </summary>
        /// <param name="p_iInvoiceId">Invoice Id</param>
        /// <returns>XML for TandEDetail</returns>
        private XmlDocument GetTandEDetailsXml(int p_iInvoiceId)

            //**************************************************************
        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
        //
        //To return XML rather then String
        //Initially:- private string GetTandEDetailsXml(int p_iInvoiceId)
        //Finally:- private XmlDocument GetTandEDetailsXml(int p_iInvoiceId)
        //
        //*************************************************************
        {
            int iRecordCounter = 0;

            string sSQL = String.Empty;
            string sToDate = String.Empty;//MITS 16061
            string sFromDate = String.Empty;//MITS 16061
            string sFromToDate = String.Empty;//MITS 16061

            XmlDocument objXMLDoc = null;
            XmlElement objXMLElement = null;
            XmlElement objXMLChildEle = null;
            DbReader objReader = null;
            Entity objEntity = null;

            try
            {
                objXMLDoc = m_objXmlDocument;
                objXMLElement = (XmlElement)objXMLDoc.SelectSingleNode("TandE");

                sSQL = "SELECT ACCT_REC_DETAIL.BILLABLE,ACCT_REC_DETAIL.RATE,ACCT_REC_DETAIL.QUANTITY," +
                       "ACCT_REC_DETAIL.UNIT,ACCT_REC_DETAIL.LINE_ITEM_AMOUNT,ACCT_REC_DETAIL.OVERRIDE," +
                       "ACCT_REC_DETAIL.FROM_DATE,ACCT_REC_DETAIL.TO_DATE,ACCT_REC_DETAIL.TRANS_TYPE_CODE," +
                       "ACCT_REC_DETAIL.DTTM_RCD_ADDED,ACCT_REC_DETAIL.DTTM_RCD_LAST_UPD," +
                       "ACCT_REC_DETAIL.AR_DETAIL_ROW_ID,ACCT_REC_DETAIL.NON_BILL_REASON," +
                       "ACCT_REC_DETAIL.ADDED_BY_USER,ACCT_REC_DETAIL.UPDATED_BY_USER," +
                       "ACCT_REC_DETAIL.POSTABLE,ACCT_REC_DETAIL.TOTAL,ACCT_REC.CUSTOMER_EID," +
                       "ACCT_REC.WORK_PERF_BY_EID,ACCT_REC.INV_DATE FROM (ACCT_REC LEFT JOIN " +
                       "ACCT_REC_DETAIL ON ACCT_REC.AR_ROW_ID=ACCT_REC_DETAIL.AR_ROW_ID)" +
                       " WHERE ACCT_REC.AR_ROW_ID=" + p_iInvoiceId + " AND ACCT_REC.VOID_FLAG=0" +
                       " ORDER BY ACCT_REC_DETAIL.DTTM_RCD_ADDED DESC,ACCT_REC_DETAIL.DTTM_RCD_LAST_UPD DESC";

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);

                if (objReader != null)
                {
                    iRecordCounter = 1;

                    while (objReader.Read())
                    {
                        objXMLChildEle = (XmlElement)objXMLDoc.ImportNode(GetNewElement("TandEData"), false);

                        if ((iRecordCounter % 2) == 0)
                        {
                            objXMLChildEle.SetAttribute("bgcolor", "data1");
                        } // if
                        else
                        {
                            objXMLChildEle.SetAttribute("bgcolor", "data2");
                        } // else

                        objXMLChildEle.SetAttribute("vendor", objEntity.GetLastFirstName(Conversion.ConvertStrToInteger(objReader["WORK_PERF_BY_EID"].ToString())));
                        objXMLChildEle.SetAttribute("billable", objReader["BILLABLE"].ToString());
                        objXMLChildEle.SetAttribute("postable", objReader["POSTABLE"].ToString());
                        //rupal:start,multicurrency
                        
                        //objXMLChildEle.SetAttribute("rate", String.Format("{0:C}", Conversion.ConvertStrToDouble(objReader["RATE"].ToString()))); //MJP 4-9-09 (changed from ConvertStrToInteger to ConvertStrToDouble because values of $xx.50 converted to $0.00)
                        //objXMLChildEle.SetAttribute("total", String.Format("{0:C}", Conversion.ConvertStrToDouble(objReader["TOTAL"].ToString()))); //MJP 4-9-09 (changed from ConvertStrToInteger to ConvertStrToDouble because values of $xx.50 converted to $0.00)
                        //objXMLChildEle.SetAttribute("itemamt", String.Format("{0:C}", Conversion.ConvertStrToDouble(objReader["LINE_ITEM_AMOUNT"].ToString()))); //MJP 3-2-09 (changed from ConvertStrToInteger to ConvertStrToDouble because values of $xx.50 converted to $0.00)
                        objXMLChildEle.SetAttribute("rate", CommonFunctions.ConvertCurrency(0, objReader.GetDouble("RATE"), CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                        objXMLChildEle.SetAttribute("total", CommonFunctions.ConvertCurrency(0, objReader.GetDouble("TOTAL"), CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                        objXMLChildEle.SetAttribute("itemamt", CommonFunctions.ConvertCurrency(0, objReader.GetDouble("LINE_ITEM_AMOUNT"), CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                        
                        //rupal:end
                        
                        objXMLChildEle.SetAttribute("qty", objReader["QUANTITY"].ToString());
                        objXMLChildEle.SetAttribute("unit", objReader["UNIT"].ToString());
                        
                        //objXMLChildEle.SetAttribute("fromtodate", Conversion.ToDbDate(objReader.GetDateTime("FROM_DATE")));
                        //Conversion.GetDate(objReader["FROM_DATE"].ToString());

                        //MITS 16061 - start - mpalinski
                        sFromDate = Conversion.GetDate(objReader["FROM_DATE"].ToString());
                        sToDate = Conversion.GetDate(objReader["TO_DATE"].ToString());

                        if (!String.IsNullOrEmpty(sFromDate))
                        { 
                            DateTime dateTO = Conversion.ToDate(sFromDate);
                            sFromDate = String.Format("{0:MM/dd/yyyy}", dateTO);
                        }

                        if (!String.IsNullOrEmpty(sToDate))
                        {
                            DateTime dateFROM = Conversion.ToDate(sToDate);
                            sToDate = String.Format("{0:MM/dd/yyyy}", dateFROM);
                        }

                        sFromToDate = String.Format("{0} - {1}", sFromDate, sToDate);
                        objXMLChildEle.SetAttribute("fromtodate", sFromToDate);
                        //MITS 16061 - end - mpalinski

                        objXMLChildEle.SetAttribute("override", objReader["OVERRIDE"].ToString());
                        objXMLChildEle.SetAttribute("transtype", GetCode(objReader.GetInt32("TRANS_TYPE_CODE")));
                        //objXMLChildEle.SetAttribute("invdate", Conversion.ToDbDate(objReader.GetDateTime("INV_DATE")));
                        objXMLChildEle.SetAttribute("invdate", Conversion.GetDate(objReader["INV_DATE"].ToString()));
                        objXMLChildEle.SetAttribute("nobillreason", GetCode(objReader.GetInt32("NON_BILL_REASON")));

                        objXMLElement.AppendChild(objXMLChildEle);
                        iRecordCounter++;
                    }
                    objReader.Close();
                }

                sSQL = "SELECT CLAIM.CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID " +
                       "IN (SELECT CLAIM_ID FROM ACCT_REC WHERE AR_ROW_ID=" + p_iInvoiceId + ")";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                objXMLChildEle = (XmlElement)objXMLDoc.SelectSingleNode("TandE");

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        objXMLChildEle.SetAttribute("claimnumber", objReader["CLAIM_NUMBER"].ToString());
                    }
                    objReader.Close();
                }

                return objXMLDoc;

                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //
                //To return XML rather then String
                //Initially:- return objXMLDoc.OuterXml;
                //Finally:- return objXMLDoc;
                //
                //*************************************************************
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetTandEDetailsXml.ErrorGet", m_iClientId), p_objException);
            }
            finally
            {
                objXMLElement = null;
                objXMLChildEle = null;
                objXMLDoc = null;
                if (objReader != null)
                    objReader.Dispose();
                if (objEntity != null)
                    objEntity.Dispose();
            }
        }
        #endregion

        //#region "FillRatesAndUnits(int p_iRateTableId, int p_iAllocated)"
        #region "FillRatesAndUnits(int p_iRateTableId, int p_iAllocated, string p_sLookUpText)"
        /// Name		: FillRatesAndUnits
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ***************************************************************************
        /// Amendment History
        /// ***************************************************************************
        /// Date Amended   *          Amendment                      *    Author
        /// 09/03/2005     * Return type from String to XmlDocument  * Mohit Yadav
        /// 11/30/2007     * Modified method signature(MITS-10682)   * Arnab Mukherjee
        ///                *                                         *
        /// ***************************************************************************
        /// <summary>
        /// Fetches Rates and units for a Rate table
        /// </summary>
        /// <param name="p_iRateTableId">Rate Table ID</param>
        /// <param name="p_iAllocated">Is Allocated</param>
        /// <param name="p_sLookUpText">LookUp Text for CodeID search</param>
        /// <returns>XML for Rates and Units</returns>        
        //private XmlDocument FillRatesAndUnits(int p_iRateTableId, int p_iAllocated)   //Modified signature(MITS-10682)
        //private XmlDocument FillRatesAndUnits(int p_iRateTableId, int p_iAllocated, string p_sLookUpText,int iPmtCurrCodeId)//rupal
        // akaushik5 Added ClaimNumber for RMA-19193 
        private XmlDocument FillRatesAndUnits(int p_iRateTableId, int p_iAllocated, string p_sLookUpText, string claimNumber)
        //**************************************************************************************
        //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
        //
        //To return XML rather then String
        //Initially:- private string FillRatesAndUnits(int p_iRateTableId, int p_iAllocated)
        //Finally:- private XmlDocument FillRatesAndUnits(int p_iRateTableId, int p_iAllocated)
        //
        //**************************************************************************************

        //**********************************************************************************************************
        //Changes made by Arnab Mukherjee for BusinessAdaptor of Time And Expense. 
        //
        //Modified method signature to append the LookUpText in to the search criteria
        //Initially:- private XmlDocument FillRatesAndUnits(int p_iRateTableId, int p_iAllocated)
        //Finally:- private XmlDocument FillRatesAndUnits(int p_iRateTableId, int p_iAllocated, string p_sLookUpText)
        //Desc: If user has put any code in the textbox, it will return the list whose code id starts with LookUpText
        //***********************************************************************************************************
        {
            int iTableId = 0;
            int iUseAllTransTypes = 0;
            int iCodeId = 0;
            int iRelatedCode = 0;

            double dRate = 0.0;
            //rupal:multicurrency
            double dRateInPmtCurr = 0.0;

            string sSQL = string.Empty;
            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;
            string sShortCodeAndDesc = string.Empty;
            string sUnit = string.Empty;

            int iCaseSen = 0;//abansal23 MITS 18880 : Transaction Types display in oracle db ON 12/9/2009

            XmlDocument objXMLDoc = null;
            XmlElement objXMLElement = null;
            XmlElement objXMLChildEle = null;
            DbReader objReader = null;
            LocalCache objLocalCache = null;
            //abansal23 MITS 18880 : Transaction Types display in oracle db ON 12/9/2009 Starts
            SysSettings objSysSettings = new SysSettings(m_sConnectionString, m_iClientId); //Ash - cloud

            iCaseSen = objSysSettings.OracleCaseIns;

            if ((m_sDBType == Constants.DB_ORACLE) && (iCaseSen == 1 || iCaseSen == -1))
                iCaseSen = 1;
            else
                iCaseSen = 0;
            //abansal23 MITS 18880 : Transaction Types display in oracle db ON 12/9/2009 Ends

            try
            {
                objXMLDoc = m_objXmlDocument;
                objXMLElement = (XmlElement)objXMLDoc.SelectSingleNode("//control/rateandunit");

                //Arnab: MITS-10682 - Put if condition for Lookup Text and appended 'LIKE ()'operator in 
                //SQL Query to search data on that basis

                p_sLookUpText = p_sLookUpText.Trim();   //Condition for whether user has put space in the search criteria

                //Geeta 08/06/08  Mits 12319
                iTableId = GetTableId("TRANS_TYPES");

                sSQL = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'USE_ALL_TRANS_TYPES'";
                using (DbReader objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    while (objDbReader.Read())
                    {
                        iUseAllTransTypes = Conversion.ConvertObjToInt(objDbReader.GetValue("STR_PARM_VALUE"), m_iClientId);
                    }
                }
                //Start rsushilaggar MITS 31419 Date 02/09/2013
                UserLogin objUserLogin = new UserLogin(m_iClientId);//rkaur27
                int iUserLangCode = objUserLogin.objUser.NlsCode;
                int iBaseLangCode = Convert.ToInt32(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);

                if (iUseAllTransTypes == -1)
                {
                    if (String.IsNullOrEmpty(p_sLookUpText))
                    {
                        //sSQL = "Select * from (SELECT CODES.CODE_ID, CODES.SHORT_CODE AS SHORTCODE, CODE_DESC, UNIT, RATE FROM CUST_RATE_DETAIL," +
                        //       "CODES, CODES_TEXT WHERE CUST_RATE_DETAIL.TRANS_TYPE_CODE = CODES.CODE_ID AND " +
                        //       "CODES.CODE_ID = CODES_TEXT.CODE_ID AND CUST_RATE_DETAIL.RATE_TABLE_ID = " + p_iRateTableId +
                        //       " UNION SELECT CODES.CODE_ID, CODES.SHORT_CODE,CODE_DESC,'',0.0 FROM CODES, CODES_TEXT WHERE " +
                        //       "CODES.CODE_ID = CODES_TEXT.CODE_ID AND TABLE_ID = " + iTableId +
                        //       " AND CODES.CODE_ID NOT IN(SELECT CODES.CODE_ID FROM CUST_RATE_DETAIL WHERE " +
                        //       " CUST_RATE_DETAIL.TRANS_TYPE_CODE = CODES.CODE_ID " +
                        //       " AND CUST_RATE_DETAIL.RATE_TABLE_ID = " + p_iRateTableId + ")) TransType ";//abansal23 MITS 19169:When Trans Type is Clicked for the data it gives Error
                            //abansal23 MITS 18880 : Transaction Types display in oracle db ON 12/9/2009
                               //" ORDER BY RATE DESC,CODES.SHORT_CODE";//MITS 12319 changed CODES.SHORT_CODE to 2  //abansal23 MITS 18880 : Transaction Types display in oracle db
                        sSQL = "Select * from ( SELECT C.CODE_ID, C.SHORT_CODE AS SHORTCODE, CT.CODE_DESC ";
                        if (iUserLangCode != 0)
                            sSQL = sSQL + " ,UCT.CODE_DESC USER_CODE_DESC";

                        sSQL = sSQL + " ,UNIT, RATE  FROM CUST_RATE_DETAIL CRD " +
                                " INNER JOIN CODES C ON CRD.TRANS_TYPE_CODE = C.CODE_ID " +
                                " INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND CT.LANGUAGE_CODE = " + iBaseLangCode;
                        if (iUserLangCode != 0)
                        sSQL = sSQL + " LEFT OUTER JOIN CODES_TEXT UCT ON C.CODE_ID = UCT.CODE_ID AND UCT.LANGUAGE_CODE = " + iUserLangCode;
                        if (!string.IsNullOrEmpty(claimNumber))
                        {
                            sSQL += " JOIN SYS_LOB_RESERVES SLB on C.RELATED_CODE_ID = SLB.RESERVE_TYPE_CODE JOIN CLAIM CL ON CL.LINE_OF_BUS_CODE = SLB.LINE_OF_BUS_CODE";
                        }

                        sSQL += string.Format(" WHERE CRD.RATE_TABLE_ID = {0} {1} ", p_iRateTableId, !string.IsNullOrEmpty(claimNumber) ? string.Format("AND CL.CLAIM_NUMBER = '{0}'", claimNumber) : string.Empty);
                        sSQL += " UNION SELECT C.CODE_ID, C.SHORT_CODE,CT.CODE_DESC ";
                        
                        if (iUserLangCode != 0)
                        sSQL = sSQL + ",UCT.CODE_DESC USER_CODE_DESC ";
                        
                        sSQL = sSQL + " ,'',0.0 FROM CODES C  INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND CT.LANGUAGE_CODE = " + iBaseLangCode;
                        if (iUserLangCode != 0)
                        sSQL = sSQL + " LEFT OUTER JOIN CODES_TEXT UCT ON C.CODE_ID = UCT.CODE_ID AND UCT.LANGUAGE_CODE = " + iUserLangCode;
                        if (!string.IsNullOrEmpty(claimNumber))
                        {
                            sSQL += " JOIN SYS_LOB_RESERVES SLB on C.RELATED_CODE_ID = SLB.RESERVE_TYPE_CODE JOIN CLAIM CL ON CL.LINE_OF_BUS_CODE = SLB.LINE_OF_BUS_CODE";
                        }
                        sSQL = sSQL + " WHERE TABLE_ID = " + iTableId + " AND C.CODE_ID NOT IN " +
                                " ( SELECT C.CODE_ID FROM CUST_RATE_DETAIL WHERE  CUST_RATE_DETAIL.TRANS_TYPE_CODE = C.CODE_ID  ";
                        sSQL += string.Format(" AND CUST_RATE_DETAIL.RATE_TABLE_ID = {0}) {1}) TransType ", p_iRateTableId, !string.IsNullOrEmpty(claimNumber) ? string.Format("AND CL.CLAIM_NUMBER = '{0}'", claimNumber) : string.Empty);

                    }
                    else
                    {
                        //sSQL = "Select * from (SELECT CODES.CODE_ID, CODES.SHORT_CODE AS SHORTCODE, CODE_DESC, UNIT, RATE FROM CUST_RATE_DETAIL," +
                        //       "CODES, CODES_TEXT WHERE CUST_RATE_DETAIL.TRANS_TYPE_CODE = CODES.CODE_ID AND " +
                        //       "CODES.CODE_ID = CODES_TEXT.CODE_ID AND CUST_RATE_DETAIL.RATE_TABLE_ID = " + p_iRateTableId +
                        //       " AND UPPER(CODES.SHORT_CODE) like UPPER('" + p_sLookUpText + "%') UNION SELECT CODES.CODE_ID, " +
                        //       "CODES.SHORT_CODE,CODE_DESC,'',0.0 FROM CODES, CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID " +
                        //       "AND TABLE_ID=" + iTableId + " AND CODES.CODE_ID NOT IN(SELECT CODES.CODE_ID FROM CUST_RATE_DETAIL " +
                        //       "WHERE CUST_RATE_DETAIL.TRANS_TYPE_CODE = CODES.CODE_ID  AND CUST_RATE_DETAIL.RATE_TABLE_ID = " +
                        //       p_iRateTableId + ") AND UPPER(CODES.SHORT_CODE) like UPPER('" + p_sLookUpText + "%')) TransType ";//abansal23 MITS 19169:When Trans Type is Clicked for the data it gives Error
                            //abansal23 MITS 18880 : Transaction Types display in oracle db ON 12/9/2009
                               //" ORDER BY RATE DESC,CODES.SHORT_CODE";//MITS 12319 changed CODES.SHORT_CODE to 2  //abansal23 MITS 18880 : Transaction Types display in oracle db
                        sSQL = "Select * from ( SELECT C.CODE_ID, C.SHORT_CODE AS SHORTCODE, CT.CODE_DESC ";
                        if (iUserLangCode != 0)
                            sSQL = sSQL + " ,UCT.CODE_DESC USER_CODE_DESC";

                        sSQL = sSQL + " ,UNIT, RATE  FROM CUST_RATE_DETAIL CRD " +
                                " INNER JOIN CODES C ON CRD.TRANS_TYPE_CODE = C.CODE_ID " +
                                " INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND CT.LANGUAGE_CODE = " + iBaseLangCode;
                        if (iUserLangCode != 0)
                            sSQL = sSQL + " LEFT OUTER JOIN CODES_TEXT UCT ON C.CODE_ID = UCT.CODE_ID AND UCT.LANGUAGE_CODE = " + iUserLangCode;
                        if (!string.IsNullOrEmpty(claimNumber))
                        {
                            sSQL += " JOIN SYS_LOB_RESERVES SLB on C.RELATED_CODE_ID = SLB.RESERVE_TYPE_CODE JOIN CLAIM CL ON CL.LINE_OF_BUS_CODE = SLB.LINE_OF_BUS_CODE";
                        }

                        sSQL += string.Format(" WHERE CRD.RATE_TABLE_ID = {0} AND UPPER(C.SHORT_CODE) like UPPER('{1}%') {2} ", p_iRateTableId, p_sLookUpText, !string.IsNullOrEmpty(claimNumber) ? string.Format("AND CL.CLAIM_NUMBER = '{0}'", claimNumber) : string.Empty);
                        sSQL+=" UNION SELECT C.CODE_ID, C.SHORT_CODE,CT.CODE_DESC ";

                        if (iUserLangCode != 0)
                            sSQL = sSQL + ",UCT.CODE_DESC USER_CODE_DESC ";

                        sSQL = sSQL + " ,'',0.0 FROM CODES C  INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND CT.LANGUAGE_CODE = " + iBaseLangCode;
                        if (iUserLangCode != 0)
                            sSQL = sSQL + " LEFT OUTER JOIN CODES_TEXT UCT ON C.CODE_ID = UCT.CODE_ID AND UCT.LANGUAGE_CODE = " + iUserLangCode;
                        if (!string.IsNullOrEmpty(claimNumber))
                        {
                            sSQL += " JOIN SYS_LOB_RESERVES SLB on C.RELATED_CODE_ID = SLB.RESERVE_TYPE_CODE JOIN CLAIM CL ON CL.LINE_OF_BUS_CODE = SLB.LINE_OF_BUS_CODE";
                        }

                        sSQL = sSQL +" WHERE TABLE_ID = " + iTableId + " AND C.CODE_ID NOT IN " +
                                " ( SELECT C.CODE_ID FROM CUST_RATE_DETAIL WHERE  CUST_RATE_DETAIL.TRANS_TYPE_CODE = C.CODE_ID  " ;
                        sSQL += string.Format(" AND CUST_RATE_DETAIL.RATE_TABLE_ID = {0}) AND UPPER(C.SHORT_CODE) like UPPER('{1}%') {2}) TransType ", p_iRateTableId, p_sLookUpText, !string.IsNullOrEmpty(claimNumber) ? string.Format("AND CL.CLAIM_NUMBER = '{0}'", claimNumber) : string.Empty);
                    }
                }//Mits 12319 End
                else
                {
                    if (String.IsNullOrEmpty(p_sLookUpText))
                    {
                        //sSQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE AS SHORTCODE, CODE_DESC, UNIT, RATE FROM CUST_RATE_DETAIL," +
                        //        "CODES, CODES_TEXT WHERE CUST_RATE_DETAIL.TRANS_TYPE_CODE = CODES.CODE_ID AND " +
                        //        "CODES.CODE_ID = CODES_TEXT.CODE_ID AND CUST_RATE_DETAIL.RATE_TABLE_ID = " + p_iRateTableId + " ";//abansal23 MITS 18880 : Transaction Types display in oracle db ON 12/9/2009
                                //" ORDER BY RATE DESC,CODES.SHORT_CODE";//abansal23 MITS 18880 : Transaction Types display in oracle db
                        sSQL = "SELECT C.CODE_ID, C.SHORT_CODE AS SHORTCODE, CT.CODE_DESC ";
                        if (iUserLangCode != 0)
                            sSQL = sSQL + " ,UCT.CODE_DESC USER_CODE_DESC";

                        sSQL = sSQL + " ,UNIT, RATE  FROM CUST_RATE_DETAIL CRD " +
                                " INNER JOIN CODES C ON CRD.TRANS_TYPE_CODE = C.CODE_ID " +
                                " INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND CT.LANGUAGE_CODE = " + iBaseLangCode;
                        if (iUserLangCode != 0)
                        sSQL = sSQL + " LEFT OUTER JOIN CODES_TEXT UCT ON C.CODE_ID = UCT.CODE_ID AND UCT.LANGUAGE_CODE = " + iUserLangCode;
                        if (!string.IsNullOrEmpty(claimNumber))
                        {
                            sSQL += " JOIN SYS_LOB_RESERVES SLB on C.RELATED_CODE_ID = SLB.RESERVE_TYPE_CODE JOIN CLAIM CL ON CL.LINE_OF_BUS_CODE = SLB.LINE_OF_BUS_CODE";
                        }

                        //sSQL = sSQL + " WHERE CRD.RATE_TABLE_ID =  " + p_iRateTableId;
                        sSQL += string.Format(" WHERE CRD.RATE_TABLE_ID =  {0} {1}", p_iRateTableId, !string.IsNullOrEmpty(claimNumber) ? string.Format("AND CL.CLAIM_NUMBER = '{0}'", claimNumber) : string.Empty);
                    }
                    else
                    {
                        //sSQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE AS SHORTCODE, CODE_DESC, UNIT, RATE FROM CUST_RATE_DETAIL," +
                        //        "CODES, CODES_TEXT WHERE CUST_RATE_DETAIL.TRANS_TYPE_CODE = CODES.CODE_ID AND " +
                        //        "CODES.CODE_ID = CODES_TEXT.CODE_ID AND CUST_RATE_DETAIL.RATE_TABLE_ID = " + p_iRateTableId +
                        //        "AND UPPER(CODES.SHORT_CODE) like UPPER('" + p_sLookUpText + "%') ";//abansal23 MITS 18880 : Transaction Types display in oracle db ON 12/9/2009
                        //ORDER BY RATE DESC,CODES.SHORT_CODE";//abansal23 MITS 18880 : Transaction Types display in oracle db
                        sSQL = "SELECT C.CODE_ID, C.SHORT_CODE AS SHORTCODE, CT.CODE_DESC ";
                        if (iUserLangCode != 0)
                            sSQL = sSQL + " ,UCT.CODE_DESC USER_CODE_DESC";

                        sSQL = sSQL + " ,UNIT, RATE  FROM CUST_RATE_DETAIL CRD " +
                                " INNER JOIN CODES C ON CRD.TRANS_TYPE_CODE = C.CODE_ID " +
                                " INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND CT.LANGUAGE_CODE = " + iBaseLangCode;
                        if (iUserLangCode != 0)
                            sSQL = sSQL + " LEFT OUTER JOIN CODES_TEXT UCT ON C.CODE_ID = UCT.CODE_ID AND UCT.LANGUAGE_CODE = " + iUserLangCode;

                        if (!string.IsNullOrEmpty(claimNumber))
                        {
                            sSQL += " JOIN SYS_LOB_RESERVES SLB on C.RELATED_CODE_ID = SLB.RESERVE_TYPE_CODE JOIN CLAIM CL ON CL.LINE_OF_BUS_CODE = SLB.LINE_OF_BUS_CODE";
                        }

                        //sSQL = sSQL + " WHERE CRD.RATE_TABLE_ID =  " + p_iRateTableId + " AND UPPER(C.SHORT_CODE) like UPPER('" + p_sLookUpText + "%') ";
                        sSQL += string.Format(" WHERE CRD.RATE_TABLE_ID =  {0} AND UPPER(C.SHORT_CODE) like UPPER('{1}%') {2}", p_iRateTableId, p_sLookUpText, !string.IsNullOrEmpty(claimNumber) ? string.Format("AND CL.CLAIM_NUMBER = '{0}'", claimNumber) : string.Empty);

                    }
                }
                //End rsushilaggar MITS 31419 Date 02/09/2013
                //MITS-10682 End
                //abansal23 MITS 18880 : Transaction Types display in oracle db ON 12/9/2009 Starts
                if (iCaseSen == 1)
                    sSQL = sSQL + "ORDER BY RATE DESC,UPPER(SHORTCODE)";
                else
                    sSQL = sSQL + "ORDER BY RATE DESC,SHORTCODE";
                //abansal23 MITS 18880 : Transaction Types display in oracle db ON 12/9/2009 Ends

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        objXMLChildEle = (XmlElement)objXMLDoc.ImportNode(GetNewElement("option"), false);

                        iCodeId = Conversion.ConvertStrToInteger(objReader["CODE_ID"].ToString());
                        sShortCode = objReader["SHORTCODE"].ToString(); //abansal23 MITS 18880 : Transaction Types display in oracle db ON 12/10/2009 

                        if(iUserLangCode != 0 && objReader["USER_CODE_DESC"] != null && objReader["USER_CODE_DESC"] != "")
                            sCodeDesc = objReader["USER_CODE_DESC"].ToString();
                        else
                            sCodeDesc = objReader["CODE_DESC"].ToString();

                        iRelatedCode = objLocalCache.GetRelatedCodeId(iCodeId);
                        sShortCodeAndDesc = GetCode(iRelatedCode);
                        sUnit = objReader["UNIT"].ToString();
                        dRate = Conversion.ConvertStrToDouble(objReader["RATE"].ToString());
                        //rupal:multicurrency
                        //dRate = Math.Round(dRate, 2);
                        objXMLChildEle.SetAttribute("id1", iCodeId.ToString());
                        objXMLChildEle.SetAttribute("id2", iRelatedCode.ToString());
                        objXMLChildEle.SetAttribute("code1", sShortCode);
                        objXMLChildEle.SetAttribute("desc1", sCodeDesc);
                        objXMLChildEle.SetAttribute("codedesc", sShortCodeAndDesc);
                        objXMLChildEle.SetAttribute("unit", sUnit);
                        objXMLChildEle.SetAttribute("rate", dRate.ToString());
                        
                        //rupal:multicurrency
                       /*
                        if (objSysSettings.UseMultiCurrency != 0)
                        {
                            //rate is saved in base currency, so while creating TandE entry, we need to convert base rate into payment currency rate
                                int iBaseCurr = objSysSettings.BaseCurrencyType;
                                double dExchangeRateBaseToPmt = CommonFunctions.ExchangeRateSrc2Dest(iBaseCurr, iPmtCurrCodeId, m_sConnectionString);
                                
                                dRateInPmtCurr = Math.Round((dRate * dExchangeRateBaseToPmt), 2);
                                objXMLChildEle.SetAttribute("rateInPmtCurr", dRateInPmtCurr.ToString());
                                
                            
                        }
                        else
                        {
                            dRateInPmtCurr = dRate;
                            objXMLChildEle.SetAttribute("rateInPmtCurr", dRate.ToString());
                        }
                        */
                        objXMLChildEle.SetAttribute("rate_Formatted", CommonFunctions.ConvertCurrency(0, dRate, CommonFunctions.NavFormType.None, m_sConnectionString, m_iClientId));
                        //objXMLChildEle.SetAttribute("rateInPmtCurr_formatted", CommonFunctions.ConvertByCurrencyCode(iPmtCurrCodeId, dRateInPmtCurr, m_sConnectionString));
                        //rupal:end
                        objXMLElement.AppendChild(objXMLChildEle);
                    }
                    objReader.Close();
                }

                //MITS 10688 Client asked to remove these regular transaction type code from the list
                /*
                //Arnab: MITS-10682 - Put if condition for Lookup Text and appended 'LIKE ()'operator in 
                //SQL Query to search data on that basis                
                if (p_sLookUpText == "")
                {
                    sSQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODE_DESC FROM CODES, CODES_TEXT " +
                            "WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND " +
                            "CODES.TABLE_ID = " + objLocalCache.GetTableId("TRANS_TYPES") + " AND CODES.CODE_ID " +
                            "NOT IN (SELECT TRANS_TYPE_CODE FROM CUST_RATE_DETAIL WHERE " +
                            "RATE_TABLE_ID = " + p_iRateTableId + ") ORDER BY CODES.SHORT_CODE";
                }
                else
                {
                    sSQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODE_DESC FROM CODES, CODES_TEXT " +
                            "WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND " +
                            "CODES.TABLE_ID = " + objLocalCache.GetTableId("TRANS_TYPES") + 
                            "AND UPPER(CODES.SHORT_CODE) like UPPER('" + p_sLookUpText +
                            "%') AND CODES.CODE_ID " +
                            "NOT IN (SELECT TRANS_TYPE_CODE FROM CUST_RATE_DETAIL WHERE " +
                            "RATE_TABLE_ID = " + p_iRateTableId + ") ORDER BY CODES.SHORT_CODE";
                }
                //MITS-10682 End

                objReader=DbFactory.GetDbReader(m_sConnectionString,sSQL);					

                if(objReader != null)
                {
                    while(objReader.Read())
                    {
                        objXMLChildEle = (XmlElement)objXMLDoc.ImportNode(GetNewElement("option"),false); 
						 					 
                        iCodeId = Conversion.ConvertStrToInteger(objReader["CODE_ID"].ToString());
                        sShortCode = objReader["SHORT_CODE"].ToString();
                        sCodeDesc = objReader["CODE_DESC"].ToString();
                        iRelatedCode = objLocalCache.GetRelatedCodeId(iCodeId);
                        sShortCodeAndDesc = GetCode(iRelatedCode); 

                        objXMLChildEle.SetAttribute("id1", iCodeId.ToString());
                        objXMLChildEle.SetAttribute("id2", iRelatedCode.ToString());
                        objXMLChildEle.SetAttribute("code1", sShortCode);
                        objXMLChildEle.SetAttribute("desc1", sCodeDesc);
                        objXMLChildEle.SetAttribute("codedesc", sShortCodeAndDesc);

                        objXMLElement.AppendChild(objXMLChildEle);
                    }
                    objReader.Close();
                }
                //Arnab:MITS-10682 - Appended child node to send the search criteria back
                */
                objXMLChildEle = (XmlElement)objXMLDoc.SelectSingleNode("//rateandunit");
                objXMLChildEle.SetAttribute("lookuptext", p_sLookUpText.ToString());
                //MITS-10682 - End
                return objXMLDoc;

                //**************************************************************
                //Changes made by Mohit Yadav for BusinessAdaptor of Time And Expense. 
                //
                //To return XML rather then String
                //Initially:- return objXMLDoc.OuterXml;
                //Finally:- return objXMLDoc;
                //
                //*************************************************************
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.FillRatesAndUnits.ErrorFill", m_iClientId), p_objException);
            }
            finally
            {
                objXMLElement = null;
                objXMLChildEle = null;
                objXMLDoc = null;
                if (objReader != null)
                    objReader.Dispose();
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }
        }
        #endregion

        #region "GetRateLevel()"
        /// Name		: GetRateLevel
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// This Function returns the current rate level from the database
        /// </summary>
        /// <returns>Ratelevel value</returns>
        private long GetRateLevel()
        {
            long lRateLevel = 0;
            string sSql = string.Empty;
            DbReader objReader = null;

            try
            {
                sSql = "SELECT RATE_LEVEL FROM SYS_PARMS";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        switch (Conversion.ConvertStrToLong(objReader["RATE_LEVEL"].ToString()))
                        {
                            case 1005:
                                lRateLevel = 1;
                                break;
                            case 1006:
                                lRateLevel = 2;
                                break;
                            case 1007:
                                lRateLevel = 3;
                                break;
                            case 1008:
                                lRateLevel = 4;
                                break;
                            case 1009:
                                lRateLevel = 5;
                                break;
                            case 1010:
                                lRateLevel = 6;
                                break;
                            case 1011:
                                lRateLevel = 7;
                                break;
                            case 1012:
                                lRateLevel = 8;
                                break;
                        }
                    }
                    objReader.Close();
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetRateLevel.ErrorGet", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
            }

            return lRateLevel;
        }
        #endregion

        #region "UpdateRateTableDetails(string p_sUpdateMode, XmlElement p_objXMLElement, int p_iRateTableId)"
        /// Name		: UpdateRateTableDetails
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Updates the Rate Table Data into the database depending upon the mode passed and Rate TableId passed
        /// </summary>
        /// <param name="p_sUpdateMode">Mode of updation (New/Edit/Unchanged)</param>
        /// <param name="p_objXMLElement">Rate table data</param>
        /// <param name="p_iRateTableId">Rate table Id</param>
        /// <returns>Returns boolean for success/failure</returns>
        private void UpdateRateTableDetails(string p_sUpdateMode, XmlElement p_objXMLElement, int p_iRateTableId)
        {
            int iTransTypeCode = 0;
            int iCustRowId = 0;//MGaba2 :MITS 13140

            double dRate = 0.0;

            string sUnit = string.Empty;
            string sUser = string.Empty;
            string sDateTime = string.Empty;

            CustRateDetail objCustRateDetail = null;

            try
            {
				if( m_objDataModelFactory == null )
					this.Initialize();
				//***************************************************************
				//Mohit Yadav
				//if(p_sUpdateMode!="new")
				//{
				//	objCustRateDetail = (CustRateDetail)m_objDataModelFactory.GetDataModelObject("CustRateDetail",false);
				//	objCustRateDetail.KeyFieldName="RATE_TABLE_ID";
				//	objCustRateDetail.MoveTo(p_iRateTableId);
				//	iCustRowId = objCustRateDetail.CustRateDetailRowId;
				//	objCustRateDetail.Delete();	
				//	objCustRateDetail = null;
				//}
				//else
				//{
				//	objCustRateDetail = (CustRateDetail)m_objDataModelFactory.GetDataModelObject("CustRateDetail",false);
				//	objCustRateDetail.MoveLast();
				//	iCustRowId = objCustRateDetail.CustRateDetailRowId;
				//	objCustRateDetail = null;
				//}
				//Mohit Yadav
				//***************************************************************
				dRate = Conversion.ConvertStrToDouble(p_objXMLElement.GetAttribute("rate")); 
				sUnit = p_objXMLElement.GetAttribute("unit");
				iTransTypeCode = Conversion.ConvertStrToInteger(p_objXMLElement.GetAttribute("transtypecode"));
				sUser = p_objXMLElement.GetAttribute("user");
				sDateTime = p_objXMLElement.GetAttribute("datetime");
                //MGaba2:MITS 13140:need row id of cust_rate_detail table while updation
                iCustRowId = Conversion.ConvertStrToInteger(p_objXMLElement.GetAttribute("trackid"));
                objCustRateDetail = (CustRateDetail)m_objDataModelFactory.GetDataModelObject("CustRateDetail", false);

				objCustRateDetail.RateTableId = p_iRateTableId;
                //MGaba2:MITS 13140:Uncommented the following code::need row id while updation
                if (p_sUpdateMode != "new")
                {
                    objCustRateDetail.CustRateDetailRowId = iCustRowId;
                }
                else
                {
                    objCustRateDetail.CustRateDetailRowId = 0;
                }
				objCustRateDetail.TransTypeCode = iTransTypeCode;
				objCustRateDetail.Unit = sUnit;
				objCustRateDetail.Rate = dRate;
 
				switch(p_sUpdateMode)
				{
					case "new":
						objCustRateDetail.AddedByUser = m_sLoginName;
                        //MGaba2:MITS 13140
                        objCustRateDetail.UpdatedByUser = m_sLoginName;						
                        //objCustRateDetail.DttmRcdAdded = Conversion.ToDbDateTime(DateTime.Now);
                        objCustRateDetail.DttmRcdAdded = Conversion.ToDbDateTime(DateTime.Now);
						//objCustRateDetail.DttmRcdLastUpd = null;
                        objCustRateDetail.DttmRcdLastUpd = Conversion.ToDbDateTime(DateTime.Now); 
						break;
					case "edit":
						objCustRateDetail.AddedByUser = sUser; 
						objCustRateDetail.UpdatedByUser = m_sLoginName;
						objCustRateDetail.DttmRcdAdded = sDateTime;
                        //MGaba2:MITS 13140:While updation this value is used for filtering the record
						//objCustRateDetail.DttmRcdLastUpd = Conversion.ToDbDateTime(DateTime.Now);
                        objCustRateDetail.DttmRcdLastUpd = sDateTime;
						break;
					case "unchanged":
						objCustRateDetail.AddedByUser = sUser;
						objCustRateDetail.UpdatedByUser = null;
						objCustRateDetail.DttmRcdAdded = sDateTime;
                        //MGaba2:MITS 13140:While updation this value is used for filtering the record
                        objCustRateDetail.DttmRcdLastUpd = sDateTime;
                        break;
                }

                objCustRateDetail.Save();
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.UpdateRateTableDetails.ErrorUpdate", m_iClientId), p_objException);
            }
            finally
            {
                if (objCustRateDetail != null)
                    objCustRateDetail.Dispose();
            }
        }
        #endregion

        #region "UpdateInvoiceDetails(string p_sUpdateMode, XmlElement p_objXMLElement, int p_iInvoiceId, int p_iTransId)"
        /// Name		: UpdateInvoiceDetails
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Updates the Invoice details into the database depending upon the mode passed and Invoice id passed
        /// </summary>
        /// <param name="p_sUpdateMode">Mode of updation</param>
        /// <param name="p_objXMLElement">Invoice Data</param>
        /// <param name="p_iInvoiceId">Invoice Id</param>
        /// <param name="p_iTransId">Trans id</param>
        /// <returns></returns>
        
        private void UpdateInvoiceDetails(XmlElement p_objXMLElement, ref AcctRec objAcctRec, ref Funds objFunds)        
        {
            bool bIsPostable = false;
            bool bIsOverride = false;
            bool bIsBillable = false;

            int iSplitId = 0;
            int iInvDetailId = 0;
            int iTransTypeCode = 0;
            int iReserveTypeCode = 0;
            int iNonBillReasonCode = 0;
            int iFundsTransTypeCode = 0;
            int iFundsReserveTypeCode = 0;

            double dRate = 0;
            double dInvoiceAmt = 0;
            double dBilledAmount = 0;
            double dQuantity = 0;  //MITS 28067 mcapps2 Quantity should not be an Int it as we need to allow decimals

            //string sInvoiceDate = Conversion.ToDbDate(DateTime.Now);//MITS 16050
            string sInvoiceDate = objAcctRec.InvDate;//MITS 16050
            string sComment = String.Empty;
            string sUnit = String.Empty;
            string sInvNumber = String.Empty;//MITS 16053 - mpalinski
            string sFromDate = String.Empty;
            string sToDate = String.Empty;
            string sReason = String.Empty;
            string sSQL = String.Empty;

            AcctRecDetail objAcctRecDetail = null;
            FundsTransSplit objFundsTransSplit = null;
            Claim objClaim = null;
            

            try
            {
                iInvDetailId = Conversion.ConvertStrToInteger(p_objXMLElement.GetAttribute("id"));
                objAcctRecDetail = objAcctRec.AcctRecDetailList[iInvDetailId];
                if (objAcctRecDetail == null)
                {
                    objAcctRecDetail = (AcctRecDetail)m_objDataModelFactory.GetDataModelObject("AcctRecDetail", false);
                }

                //will be used only in case of mode != "new"
                dInvoiceAmt = Conversion.ConvertStrToDouble(p_objXMLElement.GetAttribute("invamt"));
                dBilledAmount = Conversion.ConvertStrToDouble(p_objXMLElement.GetAttribute("billamt"));
                sFromDate = Conversion.GetDate(p_objXMLElement.GetAttribute("fromdateFormatted"));
                sToDate = Conversion.GetDate(p_objXMLElement.GetAttribute("todateFormatted"));

                sInvNumber = p_objXMLElement.GetAttribute("invnum");//MITS 16053 - mpalinski
                iTransTypeCode = Conversion.ConvertStrToInteger(p_objXMLElement.GetAttribute("transtypecode"));
                iReserveTypeCode = Conversion.ConvertStrToInteger(p_objXMLElement.GetAttribute("reservetypecode"));
                bIsPostable = p_objXMLElement.GetAttribute("postable") == "1" ? true : false;
                bIsOverride = p_objXMLElement.GetAttribute("override") == "1" ? true : false;
                bIsBillable = p_objXMLElement.GetAttribute("billable") == "1" ? true : false;
                iNonBillReasonCode = Conversion.ConvertStrToInteger(p_objXMLElement.GetAttribute("nonbillreasoncode"));

                sComment = p_objXMLElement.GetAttribute("comment");
                StringBuilder sbLocalComments = new StringBuilder();
                string sPreComments = p_objXMLElement.GetAttribute("precomment");

                if (!string.IsNullOrEmpty(sComment) || !string.IsNullOrEmpty(sPreComments))
                {
                    if (!string.IsNullOrEmpty(sPreComments))
                        sbLocalComments.Append(sPreComments);

                    if (!string.IsNullOrEmpty(sComment))
                    {
                        if (sbLocalComments.Length > 0)
                            sbLocalComments.Append("; ");
                        sbLocalComments.Append(sComment);
                    }
                }

                dQuantity = Conversion.ConvertStrToDouble(p_objXMLElement.GetAttribute("qty")); //MITS 28067  mcapps2
                sReason = p_objXMLElement.GetAttribute("reason");
                dRate = Conversion.ConvertStrToDouble(p_objXMLElement.GetAttribute("rate"));
                sUnit = p_objXMLElement.GetAttribute("unit");
               
                if (objFunds.TransId != 0)
                {
                    iSplitId = objAcctRecDetail.SplitRowId;
                    objFundsTransSplit = objFunds.TransSplitList[iSplitId];

                    if (objFundsTransSplit == null)
                    {
                        objFundsTransSplit = (FundsTransSplit)m_objDataModelFactory.GetDataModelObject("FundsTransSplit", false);
                    }

                    //MITS 16137 - start - mpalinski
                    iFundsReserveTypeCode = iReserveTypeCode;
                    iFundsTransTypeCode = iTransTypeCode;

                    if (!bIsPostable)
                    {
                        //MGaba2:MITS 18971:Non postable transactions not getting saved in Oracle
                        //sSQL = "SELECT CODE_ID FROM CODES AS C, GLOSSARY AS G " +
                        sSQL = "SELECT CODE_ID FROM CODES C, GLOSSARY G " +
                               "WHERE C.TABLE_ID = G.TABLE_ID " +
                               "AND C.SHORT_CODE = 'NONPOST' " +
                               "AND G.SYSTEM_TABLE_NAME = 'TRANS_TYPES'";
                        using (DbReader objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {
                            if (objDbReader.Read())
                            {
                                iFundsTransTypeCode = Conversion.ConvertObjToInt(objDbReader.GetValue("CODE_ID"), m_iClientId);
                            }
                        }

                       //MGaba2:MITS 18971:Non postable transactions not getting saved in Oracle
                       // sSQL = "SELECT CODE_ID FROM CODES AS C, GLOSSARY AS G " +
                        sSQL = "SELECT CODE_ID FROM CODES C, GLOSSARY G " +
                               "WHERE C.TABLE_ID = G.TABLE_ID " +
                               "AND C.SHORT_CODE = 'NONPOST' " +
                               "AND G.SYSTEM_TABLE_NAME = 'RESERVE_TYPE'";
                        using (DbReader objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {
                            if (objDbReader.Read())
                            {
                                iFundsReserveTypeCode = Conversion.ConvertObjToInt(objDbReader.GetValue("CODE_ID"), m_iClientId);
                            }
                        }
                    } // if
                    //MITS 16137 - end - mpalinski

                    
                    objFundsTransSplit.SumAmount = dInvoiceAmt;
                    objFundsTransSplit.TransId = objFunds.TransId;
                    objFundsTransSplit.TransTypeCode = iFundsTransTypeCode;
                    objFundsTransSplit.ReserveTypeCode = iFundsReserveTypeCode;                    
                    objFundsTransSplit.GlAccountCode = 0;
                    objFundsTransSplit.FromDate = sFromDate;
                    objFundsTransSplit.ToDate = sToDate;
                    objFundsTransSplit.InvoicedBy = m_sLoginName;                    
                    objFundsTransSplit.InvoiceNumber = sInvNumber;//MITS 16053 - mpalinski
                    objFundsTransSplit.PoNumber = null;
                    objFundsTransSplit.Crc = 0;

                    //rupal:start,multicurrency                   
                    if (objAcctRecDetail.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                    {
                       /*
                        //payment currency amounts
                        objFundsTransSplit.PmtCurrencyAmount = Math.Round(dInvoiceAmt, 2);
                        objFundsTransSplit.PmtCurrencyInvAmount = Math.Round(dInvoiceAmt, 2);
                        //base currency amounts
                        objFundsTransSplit.Amount = Math.Round((objFundsTransSplit.PmtCurrencyAmount * objFunds.PmtToBaseCurRate), 2);
                        objFundsTransSplit.InvoiceAmount = Math.Round((objFundsTransSplit.PmtCurrencyInvAmount * objFunds.PmtToBaseCurRate), 2);
                        objFundsTransSplit.SumAmount = Math.Round((objFundsTransSplit.SumAmount * objFunds.PmtToBaseCurRate), 2);
                        //claim currency amounts
                        //double dExhRateBaseToClaim = CommonFunctions.ExchangeRateSrc2Dest(objFunds.Context.InternalSettings.SysSettings.BaseCurrencyType, objFunds.ClaimCurrencyType, m_sConnectionString);
                        objFundsTransSplit.ClaimCurrencyAmount = Math.Round((objFundsTransSplit.Amount * objFunds.BaseToClaimCurRate), 2);
                        objFundsTransSplit.ClaimCurrencyInvAmount = Math.Round((objFundsTransSplit.InvoiceAmount * objFunds.BaseToClaimCurRate), 2);
                        */

                        //base currency amounts
                        objFundsTransSplit.Amount = dInvoiceAmt;
                        objFundsTransSplit.InvoiceAmount = dInvoiceAmt;
                        objFundsTransSplit.SumAmount = (objFundsTransSplit.SumAmount * objFunds.PmtToBaseCurRate);
                        
                        //payment currency amounts
                        objFundsTransSplit.PmtCurrencyAmount = (objFundsTransSplit.Amount * objFunds.BaseToPmtCurRate);
                        objFundsTransSplit.PmtCurrencyInvAmount = (objFundsTransSplit.InvoiceAmount * objFunds.BaseToPmtCurRate);
                        
                        //claim currency amounts
                        //double dExhRateBaseToClaim = CommonFunctions.ExchangeRateSrc2Dest(objFunds.Context.InternalSettings.SysSettings.BaseCurrencyType, objFunds.ClaimCurrencyType, m_sConnectionString);
                        objFundsTransSplit.ClaimCurrencyAmount = (objFundsTransSplit.Amount * objFunds.BaseToClaimCurRate);
                        objFundsTransSplit.ClaimCurrencyInvAmount = (objFundsTransSplit.InvoiceAmount * objFunds.BaseToClaimCurRate);
                        
                    }
                    else
                    {
                        objFundsTransSplit.InvoiceAmount = dInvoiceAmt;
                        objFundsTransSplit.Amount = dInvoiceAmt;
                        objFundsTransSplit.PmtCurrencyAmount = objFundsTransSplit.Amount;
                        objFundsTransSplit.PmtCurrencyInvAmount = objFundsTransSplit.InvoiceAmount;
                        objFundsTransSplit.ClaimCurrencyAmount = objFundsTransSplit.Amount;
                        objFundsTransSplit.ClaimCurrencyInvAmount = objFundsTransSplit.InvoiceAmount;
                    }
                    //rupal:end

                    if (iSplitId == 0)
                    {
                        objFundsTransSplit.AddedByUser = m_sLoginName;
                    }
                    else
                    {
                        objFundsTransSplit.UpdatedByUser = m_sLoginName;
                    }

                    objFundsTransSplit.InvoiceDate = sInvoiceDate;
                    
                    objFundsTransSplit.Save();
                }

                objAcctRecDetail.ArRowId = objAcctRec.ArRowId;
                objAcctRecDetail.RateTableId = m_iRateCode;

                if (objFundsTransSplit != null)
                {
                    objAcctRecDetail.SplitRowId = objFundsTransSplit.SplitRowId;
                }

                objAcctRecDetail.ReserveTypeCode = iReserveTypeCode;
                objAcctRecDetail.TransTypeCode = iTransTypeCode;
                objAcctRecDetail.LineItemAmount = dInvoiceAmt;
                objAcctRecDetail.FromDate = sFromDate;
                objAcctRecDetail.ToDate = sToDate;
                objAcctRecDetail.Unit = sUnit;
                objAcctRecDetail.Quantity = dQuantity; //MITS 28067 mcapps2
                objAcctRecDetail.Rate = dRate;
                objAcctRecDetail.Total = dBilledAmount;
                objAcctRecDetail.Override = bIsOverride;
                objAcctRecDetail.Billable = bIsBillable;
                objAcctRecDetail.NonBillReason = iNonBillReasonCode;
                objAcctRecDetail.Postable = bIsPostable;
                objAcctRecDetail.Reason = sReason;
                /*
                //rupal:start,multicurrency
                //all amount feilds in objAcctRecDetail will be in base currency
                if (objAcctRecDetail.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                {
                    objAcctRecDetail.LineItemAmount = Math.Round((dInvoiceAmt * objFunds.PmtToBaseCurRate));
                    objAcctRecDetail.Rate = Math.Round((dRate * objFunds.PmtToBaseCurRate), 2);
                    objAcctRecDetail.Total = Math.Round((dBilledAmount * objFunds.PmtToBaseCurRate), 2);
                }
                //rupal:end
                 * */
                if (iInvDetailId == 0)
                {
                    objAcctRecDetail.AddedByUser = m_sLoginName;
                }
                else
                {
                    objAcctRecDetail.UpdatedByUser = m_sLoginName;
                }
                objAcctRecDetail.LocalComment = sbLocalComments.ToString();
                objAcctRecDetail.Save();
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.UpdateInvoiceDetails.ErrorUpdate", m_iClientId), p_objException);
            }
            finally
            {
                if (objAcctRecDetail != null)
                    objAcctRecDetail.Dispose();
                if (objFundsTransSplit != null)
                    objFundsTransSplit.Dispose();
                if (objClaim != null)
                    objClaim.Dispose();
            }
        }
        #endregion

        #region "GetParentOrg(int p_iLevel, long p_lDeptEntityId)"
        /// Name		: GetParentOrg
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Returns a Entity table Id for the Level passed and on the basis of Dept Entity passed
        /// </summary>
        /// <param name="p_iLevel">Level of Org Hierarchy</param>
        /// <param name="p_lDeptEntityId">Department Entity Id</param>
        /// <returns></returns>
        private int GetParentOrg(int p_iLevel, long p_lDeptEntityId)
        {
            DbReader objReader = null;
            DbReader objReaderEntity = null;

            string sSqlOrgHierarchy = string.Empty;
            string sSqlEntity = string.Empty;

            int iEntityTableId = 0;
            int iReturnDeptId = 0;

            try
            {
                sSqlOrgHierarchy = "SELECT DISTINCT * FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=" + p_lDeptEntityId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSqlOrgHierarchy);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        for (int i = 0; i < objReader.FieldCount; i++)
                        {
                            iReturnDeptId = Conversion.ConvertStrToInteger(objReader[i].ToString());
                            sSqlEntity = "SELECT ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY_ID=" + iReturnDeptId;
                            objReaderEntity = DbFactory.GetDbReader(m_sConnectionString, sSqlEntity);

                            if (objReaderEntity != null)
                            {
                                if (objReaderEntity.Read())
                                    iEntityTableId = Conversion.ConvertStrToInteger(objReaderEntity["ENTITY_TABLE_ID"].ToString());
                                objReaderEntity.Close();
                                objReaderEntity = null;
                            }

                            if (p_iLevel == iEntityTableId)
                            {
                                break;
                            }
                        }
                    }
                }
                objReader.Dispose();

                return iReturnDeptId;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetParentOrg.ErrorGet", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
                if (objReaderEntity != null)
                    objReaderEntity.Dispose();
            }
        }
        #endregion

        #region "CreateInvoice(XmlNodeList p_objXMLNodeList, string p_sUpdateMode,out int p_iInvoiceId,out int p_iTransId)"
        /// Name		: CreateInvoice
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Creates a Invoice into the database
        /// </summary>
        /// <param name="p_objXMLNodeList">Invoice data</param>
        /// <param name="p_sUpdateMode">Mode of Updation for Invoice creation</param>
        /// <param name="objAcctRec">AcctRec object</param>
        /// <param name="objFunds">Funds object</param>
        /// <returns>Boolean for success/failure</returns>
        private bool CreateUpdateInvoice(XmlNodeList p_objXMLNodeList, string p_sUpdateMode, ref AcctRec objAcctRec, ref Funds objFunds)
        {
            string sInvNumber = String.Empty;
            string sClaimNumber = String.Empty;
            string sRateCode = String.Empty;
            string sInvoiceDate = String.Empty;
            string sInvDate = String.Empty;
            string sCtlNumber = String.Empty;
            string sSQL = String.Empty;
            int iVendorId = 0;
            int iInvoiceId = 0;
            int iTableLevel = 0;
            int iAllocated = 0;
            int iTransId = 0;
            int iPayeeTransCode = 0;
            int iDeptEntityId = 0;
            //rupal:multicurrency
            int iPmtCurrencyCode = 0;
            int iDistributionCodeId = 0;
            double dInvoiceAmount = 0.0;

            DbReader objReader = null;
            Entity objEntity = null;
            BankAccSub objBankAccSub = null;
            Claim objClaim = null;
            LocalCache objLocalCache = null;

            try
            {
                foreach (XmlElement objXMLElement in p_objXMLNodeList)
                {
                    switch (objXMLElement.GetAttribute("name"))
                    {
                        case "invNumber":
                            sInvNumber = objXMLElement.GetAttribute("value");
                            break;
                        case "claimid":
                            m_iClaimId = Conversion.ConvertStrToInteger(objXMLElement.GetAttribute("value"));
                            break;
                        case "invAmt":
                            dInvoiceAmount = Conversion.ConvertStrToDouble(objXMLElement.GetAttribute("value"));
                            break;
                        case "invDate":
                            if (objXMLElement.GetAttribute("value") == string.Empty)
                                sInvDate = Conversion.ToDbDate(DateTime.Now);
                            else
                                sInvDate = objXMLElement.GetAttribute("value");
                            break;
                        case "vendor":
                            iVendorId = Conversion.ConvertStrToInteger(objXMLElement.GetAttribute("id"));
                            break;
                        case "h_pid":
                            if (p_sUpdateMode == "edit")
                                iInvoiceId = Conversion.ConvertStrToInteger(objXMLElement.GetAttribute("value"));
                            break;
                        case "h_allocated":
                            iAllocated = Conversion.ConvertStrToInteger(objXMLElement.GetAttribute("value"));
                            break;
                        case "clmNumber":
                            sClaimNumber = objXMLElement.GetAttribute("value");
                            break;
                        case "user":
                            m_sLoginName = objXMLElement.GetAttribute("value");
                            break;
                        case "h_ratecode":
                            sRateCode = objXMLElement.GetAttribute("value");
                            break;
                        case "currencytypetext":
                            iPmtCurrencyCode = Conversion.ConvertStrToInteger(objXMLElement.GetAttribute("codeid"));
                            break;
                        //RMA-15315:aaggarwal29 start
                        case "DistributionTypeTandE":
                            iDistributionCodeId = Conversion.ConvertStrToInteger(objXMLElement.GetAttribute("codeid"));
                            break;
                        //RMA-15315:aaggarwal29 end
                    }
                }

                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

                if (iInvoiceId != 0)
                {
                    objAcctRec.MoveTo(iInvoiceId);
                    iTransId = objAcctRec.InvTransId;
                    objFunds.MoveTo(iTransId);
                }

                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(m_iClaimId);

                if (objLocalCache.GetShortCode(objLocalCache.GetRelatedCodeId(objClaim.ClaimStatusCode)) != "O" && m_bAllowClosedClaim == false)
                {
                    throw new RMAppException(GetMyError(Globalization.GetString("InvoiceDriver.CreateInvoice.ErrorClaimStatus", m_iClientId), 1, string.Empty, 0, 0));
                }

                if (objClaim.PaymntFrozenFlag == true)
                {
                    throw new RMAppException(GetMyError(Globalization.GetString("InvoiceDriver.CreateInvoice.ErrorPayment", m_iClientId), 1, string.Empty, 0, 0));
                }

                m_sDate = Conversion.ToDbDate(DateTime.Now);
                m_sDateTime = Conversion.ToDbDateTime(DateTime.Now);
                iPayeeTransCode = objLocalCache.GetCodeId("R", objLocalCache.GetTableName(GetTableId("PAYEE_TYPE")));

                sSQL = "SELECT DEPT_EID FROM EVENT WHERE EVENT_ID IN(SELECT EVENT_ID FROM CLAIM WHERE CLAIM_ID=" + m_iClaimId + ")";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iDeptEntityId = Conversion.ConvertStrToInteger(objReader["DEPT_EID"].ToString());
                    }
                    objReader.Close();
                }

                if (iAllocated == 1)
                {
                    objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                    objEntity.MoveTo(iVendorId);

                    objFunds.PayeeEid = iVendorId;
                    objFunds.PayeeTypeCode = objLocalCache.GetCodeId("O", objLocalCache.GetTableName(GetTableId("PAYEE_TYPE")));
                    objFunds.FirstName = objEntity.FirstName;
                    objFunds.LastName = objEntity.LastName;
                    objFunds.Addr1 = objEntity.Addr1;
                    objFunds.Addr2 = objEntity.Addr2;
                    objFunds.Addr3 = objEntity.Addr3;
                    objFunds.Addr4 = objEntity.Addr4;
                    objFunds.City = objEntity.City;
                    objFunds.ZipCode = objEntity.ZipCode;
                    objFunds.StateId = objEntity.StateId;

                    sSQL = "SELECT RATE_LEVEL,TANDEACCT_ID,USE_SUB_ACCOUNT FROM SYS_PARMS";
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            if (Conversion.ConvertStrToInteger(objReader["USE_SUB_ACCOUNT"].ToString()) == 0)
                            {
                                objFunds.AccountId = Conversion.ConvertStrToInteger(objReader["TANDEACCT_ID"].ToString());
                                objFunds.SubAccountId = 0;
                            }
                            else
                                objFunds.SubAccountId = Conversion.ConvertStrToInteger(objReader["TANDEACCT_ID"].ToString());
                            iTableLevel = Conversion.ConvertStrToInteger(objReader["RATE_LEVEL"].ToString());
                        }
                        objReader.Close();
                    }

                    if (objFunds.SubAccountId != 0)
                    {
                        objBankAccSub = (BankAccSub)m_objDataModelFactory.GetDataModelObject("BankAccSub", false);
                        objBankAccSub.MoveTo(objFunds.SubAccountId);
                        objFunds.AccountId = objBankAccSub.AccountId;
                    }

                    if (objFunds.AccountId == 0 && objFunds.SubAccountId == 0)
                    {
                        throw new RMAppException(GetMyError(Globalization.GetString("InvoiceDriver.CreateInvoice.ErrorNotSet", m_iClientId), 4, string.Empty, m_iClaimId, 1));
                    }

                    iDeptEntityId = GetParentOrg(iTableLevel, iDeptEntityId);
                    objFunds.StatusCode = objLocalCache.GetCodeId("R", objLocalCache.GetTableName(GetTableId("CHECK_STATUS")));
                    //MITS 16168 - start - MJP
                    objFunds.ClaimId = m_iClaimId; //nsachdeva2 - 03/29/2012 - MITS: 27833 - need to set claim id for BES otherwise funds record will not be available.
                    //objFunds.ClaimId = 0; //a claim number cannot be entered into the db unless it's billed!
                    //objFunds.ClaimNumber = sClaimNumber;
                    //MITS 16168 - end - MJP
                    objFunds.CheckMemo = "Claim No. " + sClaimNumber;
                    objFunds.AutoCheckDetail = null;
                    objFunds.RollupId = 0;
                    objFunds.VoidDate = null;
                    objFunds.AutoCheckFlag = false;
                    objFunds.ClaimantEid = 0;
                    objFunds.CountryCode = 0;
                    objFunds.PrecheckFlag = false;
                    objFunds.UnitId = 0;
                    objFunds.CtlNumber = string.Empty;
                    objFunds.VoidFlag = false;
                    objFunds.DateOfCheck = m_sDate;
                    objFunds.TransDate = m_sDate;
                    objFunds.Amount = dInvoiceAmount;
                    objFunds.Filed1099Flag = false;
                    objFunds.ClearedFlag = false;
                    objFunds.PaymentFlag = true;
                    objFunds.CollectionFlag = false;
                    objFunds.BatchNumber = 0;
                    objFunds.EnclosureFlag = false;
                    objFunds.ApproveUser = null;
                    objFunds.DttmApproval = null;
                    objFunds.SettlementFlag = false;
                    objFunds.VoucherFlag = false;
                    objFunds.NumOfPaidDays = 0;
                    objFunds.WeeksPaidCode = 0;
                    objFunds.PmtCurrencyType = iPmtCurrencyCode;//rupal:multicurrency
                    
                    //rupal:start,multicurrency
                    if (objFunds.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                    {
                        //objFunds.Amount = dInvoiceAmount;
                        objFunds.ClaimCurrencyType = objFunds.Context.DbConn.ExecuteInt("SELECT CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID=" + m_iClaimId);

                        double dExhRatePmtToClaim = CommonFunctions.ExchangeRateSrc2Dest(objFunds.PmtCurrencyType, objFunds.ClaimCurrencyType, m_sConnectionString, m_iClientId);
                        double dExhRatePmtToBase = CommonFunctions.ExchangeRateSrc2Dest(objFunds.PmtCurrencyType, objFunds.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                        double dExhRateBaseToClaim = CommonFunctions.ExchangeRateSrc2Dest(objFunds.Context.InternalSettings.SysSettings.BaseCurrencyType, objFunds.ClaimCurrencyType, m_sConnectionString, m_iClientId);
                        double dExhRateBaseToPmt = CommonFunctions.ExchangeRateSrc2Dest(objFunds.Context.InternalSettings.SysSettings.BaseCurrencyType, objFunds.PmtCurrencyType, m_sConnectionString, m_iClientId);
                        objFunds.PmtToClaimCurRate = dExhRatePmtToClaim;
                        objFunds.PmtToBaseCurRate = dExhRatePmtToBase;
                        objFunds.BaseToPmtCurRate = dExhRateBaseToPmt;
                        objFunds.BaseToClaimCurRate = dExhRateBaseToClaim;
                        /*
                        //payment currency amounts
                        objFunds.PmtCurrencyAmount = Math.Round(dInvoiceAmount, 2);
                        //base currency amount
                        objFunds.Amount = Math.Round((objFunds.PmtCurrencyAmount * dExhRatePmtToBase), 2);
                        //claim currency amount
                        objFunds.ClaimCurrencyAmount = Math.Round((objFunds.Amount * dExhRateBaseToClaim), 2);
                         * */
                      
                        //base currency amount
                        objFunds.Amount = dInvoiceAmount;
                        //payment currency amounts
                        objFunds.PmtCurrencyAmount = objFunds.Amount * dExhRateBaseToPmt;
                        //claim currency amount
                        objFunds.ClaimCurrencyAmount = objFunds.Amount * dExhRateBaseToClaim;
                       
                    }
                    else
                    {
                        objFunds.ClaimCurrencyType = objFunds.PmtCurrencyType;
                        objFunds.BaseToClaimCurRate = 1.0;
                        objFunds.BaseToPmtCurRate = 1.0;
                        objFunds.PmtToClaimCurRate = 1.0;
                        objFunds.PmtToBaseCurRate = 1.0;
                        objFunds.PmtCurrencyAmount = objFunds.Amount;
                        objFunds.ClaimCurrencyAmount = objFunds.Amount;
                    }
                    //rupal:end

                    if (iTransId == 0)
                    {
                        objFunds.AddedByUser = m_sLoginName;
                    }
                    else
                    {
                        objFunds.UpdatedByUser = m_sLoginName;
                    }
                    objFunds.DstrbnType = iDistributionCodeId; //RMA-15315:aaggarwal29
                    objFunds.Save();
                    iTransId = objFunds.TransId;
                    //nsachdeva2 - MITS: 27833 - 03/22/2012
                    sSQL = "UPDATE FUNDS SET CLAIM_ID = 0 WHERE TRANS_ID = " + iTransId; //a claim number cannot be entered into the db unless it's billed! - done for MITS:16168
                    m_objDataModelFactory.Context.DbConn.ExecuteNonQuery(sSQL);
                    objFunds.ClaimId = 0;
                    //End MITS: 27833
                }

                objAcctRec.CustomerEid = iDeptEntityId;
                objAcctRec.ClaimId = m_iClaimId;
                objAcctRec.TransId = 0;
                objAcctRec.InvTransId = iTransId;

                if (iAllocated == 0)
                    objAcctRec.Allocated = false;
                else
                    objAcctRec.Allocated = true;

                objAcctRec.InvNumber = sInvNumber;
                objAcctRec.InvTotal = dInvoiceAmount;
                objAcctRec.InvDate = sInvDate;
                objAcctRec.WorkPerfByEid = iVendorId;
                objAcctRec.Posted = false;

                /*
                //rupal:start,multicurrency
                //all amount feilds in objAcctRec will be in base currency
                if (objFunds.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                {
                    objAcctRec.InvTotal = Math.Round((dInvoiceAmount * objFunds.PmtToBaseCurRate), 2);
                }
                //rupal:end
                 * */

                if (iInvoiceId == 0)
                {
                    objAcctRec.AddedByUser = m_sLoginName;
                }
                else
                {
                    objAcctRec.UpdatedByUser = m_sLoginName;
                }

                objAcctRec.PostedId = 0;
                objAcctRec.VoidFlag = false;
                objAcctRec.Save();

                //only save claim if rate table changed.
                m_iRateCode = Conversion.ConvertStrToInteger(sRateCode);

                if (objClaim.RateTableId != m_iRateCode)
                {
                    objClaim.RateTableId = Conversion.ConvertStrToInteger(sRateCode);
                    objClaim.Save();
                }

                objClaim = null;

                return true;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.CreateInvoice.ErrorCreation", m_iClientId), p_objException);
            }
            finally
            {
                if (objEntity != null)
                    objEntity.Dispose();
                if (objBankAccSub != null)
                    objBankAccSub.Dispose();
                if (objClaim != null)
                    objClaim.Dispose();
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                if (objReader != null)
                    objReader.Dispose();
            }
        }

        #endregion

        #region "SetValue(XmlElement p_objXMLElement, string p_sName, string p_sValue, object p_objVariant)"
        /// Name		: SetValue
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Sets the value of the nodes in a XMLElement passed as input
        /// </summary>
        /// <param name="p_objXMLElement">Containing the node whose value to be set</param>
        /// <param name="p_sName">Name of node</param>
        /// <param name="p_sValue">value of node</param>
        /// <param name="p_objVariant">Variant object which will depend on parameter passd</param>
        private void SetValue(XmlElement p_objXMLElement, string p_sName, string p_sValue, object p_objVariant)
        {
            bool bAddBR = false;

            int iRecordCounter = 0;

            string sNodeType = string.Empty;

            InvoiceDetails objInvoiceDetails = null;
            RateTableDetails objRateTableDetails = null;
            XmlElement objXMLElement = null;
            XmlElement objOptionXMLEle = null;

            try
            {
                sNodeType = p_objXMLElement.GetAttribute("type");

                switch (sNodeType)
                {
                    case RADIO_FLDTYPE:
                        p_objXMLElement.SetAttribute(p_sName, p_sValue);
                        break;
                    case TEXT_CONTROL:
                        p_objXMLElement.SetAttribute(p_sName, p_sValue);
                        break;
                    case TEXT_LABEL:
                        p_objXMLElement.InnerText = p_sName;
                        break;
                    case TEXTANDLIST:
                        p_objXMLElement.SetAttribute("value", p_sName);
                        p_objXMLElement.SetAttribute("id", p_sValue);
                        p_objXMLElement.SetAttribute("mid", m_lId.ToString());
                        break;
                    case BOOL_FLDTYPE:
                        p_objXMLElement.SetAttribute(p_sName, p_sValue);
                        break;
                    case PHONE_FLDTYPE:
                        p_objXMLElement.InnerText = Conversion.ToPhoneNumber(p_sName);
                        break;
                    case SSN_FLDTYPE:
                        p_objXMLElement.InnerText = p_sName;
                        if (Conversion.IsNumeric(p_sName) && p_sName.Length == 9)
                            p_objXMLElement.InnerText = p_sName.Substring(0, 3) + "-" + p_sName.Substring(3, 2) + "-" + p_sName.Substring(5);
                        break;
                    case TAXID_FLDTYPE:
                        p_objXMLElement.InnerText = p_sName;
                        if (Conversion.IsNumeric(p_sName) && p_sName.Length == 9)
                            p_objXMLElement.InnerText = p_sName.Substring(0, 2) + "-" + p_sName.Substring(2);
                        break;
                    case ZIP_FLDTYPE:
                        p_objXMLElement.InnerText = p_sName;
                        if (Conversion.IsNumeric(p_sName) && p_sName.Length >= 5)
                        {
                            p_objXMLElement.InnerText = p_sName.Substring(0, 5);
                            if (p_sName.Length > 5)
                                p_objXMLElement.InnerText = p_objXMLElement.InnerText + "-" + p_sName.Substring(5);
                        }
                        break;
                    case DATE_FLDTYPE:
                    case DATEBSCRIPT_FLDTYPE:
                        p_objXMLElement.InnerText = string.Empty;
                        if (p_sName != string.Empty && p_sName.Length == 8)
                            p_objXMLElement.InnerText = Conversion.GetDate(p_sName);
                        break;
                    case TIME_FLDTYPE:
                        p_objXMLElement.InnerText = string.Empty;
                        if (p_sName != string.Empty && p_sName.Length == 6)
                            p_objXMLElement.InnerText = Conversion.GetTime(p_sName);
                        break;
                    case INVOICE_DETAIL:
                        objInvoiceDetails = (InvoiceDetails)p_objVariant;
                        iRecordCounter = 1;
                        foreach (InvoiceDetail objInvoiceDetail in objInvoiceDetails)
                        {
                            objXMLElement = (XmlElement)p_objXMLElement.OwnerDocument.ImportNode(GetNewElement("option"), false);

                            if ((iRecordCounter % 2) == 0)
                                objXMLElement.SetAttribute("bgcolor", "data1");
                            else
                                objXMLElement.SetAttribute("bgcolor", "data2");

                            objXMLElement.SetAttribute("claimnum", p_sValue);
                            objXMLElement.SetAttribute("trackid", iRecordCounter.ToString());
                            objXMLElement.SetAttribute("id", objInvoiceDetail.Id.ToString());
                            objXMLElement.SetAttribute("transtype", objInvoiceDetail.TransType);
                            objXMLElement.SetAttribute("transtypecode", objInvoiceDetail.TransCode.ToString());
                            objXMLElement.SetAttribute("invamt", FormatLastTwoDigits(objInvoiceDetail.ItemAmount.ToString()));
                            objXMLElement.SetAttribute("billamt", FormatLastTwoDigits(objInvoiceDetail.BillAmount.ToString()));
                            objXMLElement.SetAttribute("billable", objInvoiceDetail.IsBillable == true ? "1" : "0");
                            objXMLElement.SetAttribute("reason", objInvoiceDetail.Reason);
                            objXMLElement.SetAttribute("reservetype", objInvoiceDetail.ReserveType);
                            objXMLElement.SetAttribute("reservetypecode", objInvoiceDetail.ReserveTypeCode.ToString());
                            objXMLElement.SetAttribute("invtot", FormatLastTwoDigits(objInvoiceDetail.ItemAmount.ToString()));

                            objXMLElement.SetAttribute("invnum", string.Empty);

                            if (objInvoiceDetail.FromDate != string.Empty && objInvoiceDetail.FromDate.Length == 8)
                                objXMLElement.SetAttribute("fromdate", Conversion.GetDate(objInvoiceDetail.FromDate));
                            else
                                objXMLElement.SetAttribute("fromdate", string.Empty);

                            //Arnab: MITS-9742
                            if (objInvoiceDetail.FromDate != string.Empty && objInvoiceDetail.FromDate.Length == 8)
                                objXMLElement.SetAttribute("fromdateFormatted", Conversion.GetDBDateFormat(objInvoiceDetail.FromDate, "MM/dd/yyyy"));
                            else
                                objXMLElement.SetAttribute("fromdateFormatted", string.Empty);

                            if (objInvoiceDetail.FromDate != string.Empty && objInvoiceDetail.FromDate.Length == 8)
                                objXMLElement.SetAttribute("todateFormatted", Conversion.GetDBDateFormat(objInvoiceDetail.ToDate, "MM/dd/yyyy"));
                            else
                                objXMLElement.SetAttribute("todateFormatted", string.Empty);
                            //Arnab: MITS-9742 end

                            if (objInvoiceDetail.ToDate != string.Empty && objInvoiceDetail.ToDate.Length == 8)
                                objXMLElement.SetAttribute("todate", Conversion.GetDate(objInvoiceDetail.ToDate));
                            else
                                objXMLElement.SetAttribute("todate", string.Empty);

                            objXMLElement.SetAttribute("unit", objInvoiceDetail.Unit);
                            objXMLElement.SetAttribute("rate", FormatLastTwoDigits(objInvoiceDetail.Rate.ToString()));
                            objXMLElement.SetAttribute("override", objInvoiceDetail.Override == true ? "1" : "0");
                            objXMLElement.SetAttribute("nonbillreason", objInvoiceDetail.NonBillReasonCode);
                            objXMLElement.SetAttribute("nonbillreasoncode", objInvoiceDetail.NonBillReason.ToString());
                            objXMLElement.SetAttribute("comment", string.Empty);
                            objXMLElement.SetAttribute("precomment", objInvoiceDetail.PreComment);
                            objXMLElement.SetAttribute("qty", objInvoiceDetail.Quantity.ToString());
                            objXMLElement.SetAttribute("pid", objInvoiceDetail.ARId.ToString());
                            objXMLElement.SetAttribute("postable", objInvoiceDetail.Postable == true ? "1" : "0");
							objXMLElement.SetAttribute("status","E");
							objXMLElement.SetAttribute("invtotavail","");
							
							p_objXMLElement.AppendChild(objXMLElement);
	  
							iRecordCounter = iRecordCounter + 1;
						}
						p_objXMLElement.SetAttribute("maxtrackid",iRecordCounter.ToString()); 
						objInvoiceDetails = null;
						break;
					case RATE_TABLE:
						objRateTableDetails = (RateTableDetails) p_objVariant;
						iRecordCounter = 1;

						foreach(RateTableDetail objRateTableDetail in objRateTableDetails)
						{
							objXMLElement = (XmlElement)p_objXMLElement.OwnerDocument.ImportNode(GetNewElement("option"),false);

							if((iRecordCounter % 2) == 0)
								objXMLElement.SetAttribute("bgcolor", "data1");
							else
								objXMLElement.SetAttribute("bgcolor", "data2");

                            //MGaba2:MITS 13140:While updation this value is used for filtering the record
                            //objXMLElement.SetAttribute("trackid", iRecordCounter.ToString());
                            objXMLElement.SetAttribute("trackid", objRateTableDetail.Id.ToString());
                            objXMLElement.SetAttribute("transtype", objRateTableDetail.TransType);
                            objXMLElement.SetAttribute("transtypecode", objRateTableDetail.TransTypeCode.ToString());
                            objXMLElement.SetAttribute("unit", objRateTableDetail.Unit);
                            //********************************************************************
                            //Mohit Yadav need to changed the format from integer value to double
                            //objXMLElement.SetAttribute("rate",FormatLastTwoDigits(objRateTableDetail.Rate.ToString()));
                            //
                            //********************************************************************
                            objXMLElement.SetAttribute("rate", FormatLastTwoDigits(objRateTableDetail.Rate.ToString()));
                            //********************************************************************
                            objXMLElement.SetAttribute("user", objRateTableDetail.User);
                            objXMLElement.SetAttribute("datetime", objRateTableDetail.DateTime);
                            objXMLElement.SetAttribute("status", "U");

                            p_objXMLElement.AppendChild(objXMLElement);

                            iRecordCounter = iRecordCounter + 1;
                        }
                        p_objXMLElement.SetAttribute("maxtrackid", iRecordCounter.ToString());
                        objRateTableDetails = null;
                        break;
                    case COMBOBOX_FLDTYPE:
                        objXMLElement.SetAttribute(ATT_ID, p_sName);
                        objOptionXMLEle = (XmlElement)objXMLElement.SelectSingleNode("option[@value='" + p_sName + "']");
                        if (objOptionXMLEle != null)
                            objOptionXMLEle.SetAttribute("selected", "1");
                        break;
                    case MEMO_FLDTYPE:
                    case TEXTML_FLDTYPE:
                        if (bAddBR)
                        {
                            string[] arrTemp = { };
                            int i;
                            XmlElement objTempXMLElement = null;
                            arrTemp = p_sName.Split('\n');

                            for (i = 0; i < arrTemp.Length; i++)
                            {
                                objTempXMLElement = objXMLElement.OwnerDocument.CreateElement("BR");
                                objTempXMLElement.InnerText = arrTemp[i];
                                objXMLElement.AppendChild(objTempXMLElement);
                            }
                        }
                        break;
                    case EIN_FLDTYPE:
                        if (Conversion.IsNumeric(p_sName) && p_sName.Length == 9)
                            objXMLElement.InnerText = Conversion.ConvertStrToLong(p_sName).ToString("00-0000000");
                        else
                            objXMLElement.InnerText = p_sName;
                        break;
                    case ID_CONTROL:
                        p_objXMLElement.SetAttribute(p_sName, p_sValue);
                        break;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.SetValue.ErrorSet", m_iClientId), p_objException);
            }
            finally
            {
                objInvoiceDetails = null;
                objRateTableDetails = null;
                objXMLElement = null;
                objOptionXMLEle = null;
            }
        }
        #endregion

        #region "GetNewElement(string p_sNodeName)"
        /// Name		: GetNewElement
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Creats a new XMLElement with the nodename passed as parameter
        /// </summary>
        /// <param name="p_sNodeName">Node Name</param>
        /// <returns>new XMLElement</returns>
        private XmlElement GetNewElement(string p_sNodeName)
        {
            XmlDocument objXDoc;
            XmlElement objXMLElement;

            try
            {
                objXDoc = new XmlDocument();
                objXMLElement = objXDoc.CreateElement(p_sNodeName);
                return objXMLElement;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetNewElement.XMLError", m_iClientId), p_objException);
            }
            finally
            {
                objXDoc = null;
                objXMLElement = null;
            }
        }
        #endregion

        #region "GetNewEleWithAttr(string p_sNewNodeName, string p_sNodeName, string p_sNodeValue)"
        /// Name		: GetNewEleWithAttr
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Creats a new XMLElement with the nodename passed as parameter. 
        /// Also sets the value of passed attribute to the new XMLElement
        /// </summary>
        /// <param name="p_sNewNodeName">New Node to be Created</param>
        /// <param name="p_sNodeName">Node whose value to be set</param>
        /// <param name="p_sNodeValue">Value for the node</param>
        /// <returns>New XML Element with attribute set</returns>
        private XmlElement GetNewEleWithAttr(string p_sNewNodeName, string p_sNodeName, string p_sNodeValue)
        {
            XmlDocument objXDoc;
            XmlElement objXMLElement;

            try
            {
                objXDoc = new XmlDocument();
                objXMLElement = objXDoc.CreateElement(p_sNewNodeName);
                objXMLElement.SetAttribute(p_sNodeName, p_sNodeValue);
                return objXMLElement;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetNewEleWithAttr.XMLError", m_iClientId), p_objException);
            }
            finally
            {
                objXDoc = null;
                objXMLElement = null;
            }
        }
        #endregion

        #region "GetMyError(string p_sErrMsg,long p_lErrNum,string p_sValue,long p_lClaimId,long p_lAction)"
        /// Name		: GetMyError
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Made a Error XML and returns with the Error Details passed
        /// </summary>
        /// <param name="p_sErrMsg">Error Message</param>
        /// <param name="p_lErrNum">Error Number</param>
        /// <param name="p_sValue">value</param>
        /// <param name="p_lClaimId">Claim id</param>
        /// <param name="p_lAction">Error Action</param>
        /// <returns>Error XML</returns>
        private string GetMyError(string p_sErrMsg, long p_lErrNum, string p_sValue, long p_lClaimId, long p_lAction)
        {
            string sXMLString = string.Empty;

            try
            {
                //sXMLString = "<invdet><error active='1' id='" + p_lErrNum + "' val='" + p_sValue + "' action='" + p_lAction + "' claimid='" + p_lClaimId + "' >" + p_sErrMsg + @"</error></invdet>";
                sXMLString = p_sErrMsg;
                return sXMLString;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetMyError.XMLError", m_iClientId), p_objException);
            }
        }
        #endregion

        #region "GetTableId(string p_sTableName)"
        /// Name		: GetTableId
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// GetTableId returns the TABLE_ID for a particular SYSTEM_TABLE_NAME in the GLOSSARY table.
        /// </summary>
        /// <param name="p_sTableName">SYSTEM_TABLE_NAME for which the TABLE_ID is required</param>
        /// <returns>Table Id</returns>
        private int GetTableId(string p_sTableName)
        {
            DbReader objReader = null;
            DbConnection objConnection = null;

            int iTableId = 0;

            string sSQL = String.Empty;
            string sInsertSql = String.Empty;

            try
            {
                if (String.IsNullOrEmpty(p_sTableName))
                {
                    return (0);
                } // if

                sSQL = " SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='" + p_sTableName + "'";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                    if (objReader.Read())
                        iTableId = Common.Conversion.ConvertObjToInt(objReader.GetValue("TABLE_ID"), m_iClientId);
                objReader.Close();

                sSQL = "SELECT * FROM GLOSSARY_TEXT WHERE TABLE_ID = '" + iTableId + "'";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read() == false)
                    {
                        objConnection = DbFactory.GetDbConnection(this.m_sConnectionString);
                        objConnection.Open();
                        sInsertSql = "INSERT INTO GLOSSARY_TEXT(TABLE_ID,TABLE_NAME,LANGUAobjReader.Close();GE_CODE) VALUES" +
                            "(" + iTableId + ",'" + p_sTableName + "',1033)";
                        objConnection.ExecuteNonQuery(sInsertSql);
                        objConnection.Close();
                    }
                    objReader.Close();
                    objReader.Dispose();
                }

                return iTableId;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetTableId.ErrorGet", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
                if (objConnection != null)
                    objConnection.Dispose();
            }
        }
        #endregion

        #region "ChkDupValue(string p_sTableName,string p_sColumnName1, string p_sColumnName2, string p_svalue,long p_lId)"
        /// Name		: ChkDupValue
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Checks the Duplicate table Name value in rate table.
        /// </summary>
        /// <param name="p_sTableName">Table Name</param>
        /// <param name="p_sColumnName1">Column Name</param>
        /// <param name="p_sColumnName2">Column Name</param>
        /// <param name="p_svalue">value</param>
        /// <param name="p_lId">Table Id</param>
        /// <returns>integer value for found or not found</returns>
        private int ChkDupValue(string p_sTableName, string p_sColumnName1, string p_sColumnName2, string p_svalue, long p_lId, int p_iTableID)
        {
            string sSQL = string.Empty;
            DbReader objReader = null;
            //Start by Shivendu for MITS 11866
            DbConnection objConn = null;
            DbCommand objCommand = null;
            DbParameter objParam = null;
            //End by Shivendu for MITS 11866

            try
            {
                //Start by Shivendu for MITS 11866
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                objCommand = objConn.CreateCommand();
                objParam = objCommand.CreateParameter();
                objParam.Direction = ParameterDirection.Input;
                objParam.ParameterName = "RATETABLENAME";
                objParam.SourceColumn = "TABLE_NAME";
                //End by Shivendu for MITS 11866

                objParam.Value = p_svalue;
                sSQL = "SELECT * FROM " + p_sTableName + " WHERE ";
                if (p_svalue != string.Empty && p_lId != 0)
                    sSQL = sSQL + p_sColumnName1 + "=~RATETABLENAME~ AND " + p_sColumnName2 + "=" + p_lId;//Modified by Shivendu for MITS 11866
                else if (p_svalue == string.Empty && p_lId != 0)
                    sSQL = sSQL + p_sColumnName2 + "=" + p_lId;
                else if (p_svalue != string.Empty && p_lId == 0)
                    sSQL = sSQL + p_sColumnName1 + "= ~RATETABLENAME~";//Modified by Shivendu for MITS 11866

                //Start by Shivendu for MITS 11866
                objCommand.Parameters.Add(objParam);
                objCommand.CommandText = sSQL;
                objReader = objCommand.ExecuteReader();
                //End by Shivendu for MITS 11866

                if (objReader != null)
                    if (objReader.Read())
                    {
                        int iTableID = objReader.GetInt32("RATE_TABLE_ID");
                        if(p_iTableID == 0  || p_iTableID != iTableID)
                            return 1;
                        else
                            return 0;
                    }
                    else
                        return 0;
                else
                    return 0;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.ChkDupValue.ErrorChk", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                if (objConn != null)
                    objConn.Dispose();
                objCommand = null;
                objParam = null;
            }
        }
        #endregion

        #region "GetClientDetails()"
        /// Name		: GetClientDetails
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Fetches the client details and stores into a Structure
        /// </summary>
        private void GetClientDetails()
        {
            string sSQL = string.Empty;
            DbReader objReader = null;

            try
            {
                sSQL = "SELECT * FROM SYS_PARMS";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        m_stClientDetails.Name = objReader["CLIENT_NAME"].ToString();
                        m_stClientDetails.Address1 = objReader["ADDR1"].ToString();
                        m_stClientDetails.Address2 = objReader["ADDR2"].ToString();
                        m_stClientDetails.Address3 = objReader["ADDR3"].ToString();
                        m_stClientDetails.Address4 = objReader["ADDR4"].ToString();
                        m_stClientDetails.City = objReader["CITY"].ToString();
                        m_stClientDetails.State = objReader["STATE"].ToString();
                        m_stClientDetails.ZipCode = objReader["ZIP_CODE"].ToString();
                        m_stClientDetails.PhoneNumber = objReader["PHONE_NUMBER"].ToString();
                        m_stClientDetails.FaxNumber = objReader["FAX_NUMBER"].ToString();
                    }
                    objReader.Close();
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetClientDetails.XMLGet", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
            }
        }
        #endregion

        #region "FormatLastTwoDigits(string p_sValue)"
        /// Name		: FormatLastTwoDigits
        /// Author		: Anurag Agarwal
        /// Date Created	: 3 Dec 2004		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Fetches the client details and stores into a Structure
        /// </summary>
        /// <param name="p_sValue">Input to be formatted</param>
        /// <returns>Formatted Value</returns>
        private string FormatLastTwoDigits(string p_sValue)
        {
            string sReturn = string.Empty;

            try
            {
                //********************************************************************************
                //Mohit Yadav changed the format for the return data from 1 to 2
                //1 --> sReturn = string.Format("{0:C}",Conversion.ConvertStrToInteger(p_sValue));
                //2 --> Convert.ToDouble(p_sValue).ToString()
                //********************************************************************************
                sReturn = Convert.ToString(Conversion.ConvertStrToDouble(p_sValue));
                //********************************************************************************
                sReturn = sReturn.Replace(",", "");
            }
            catch
            {
                sReturn = p_sValue;
            }
            return sReturn;
        }
        #endregion

        #region "GetXMLForTransaction()"
        /// <summary>
        /// Build the XML For funds object.
        /// </summary>
        private XmlDocument GetXMLForTransaction(FundManager p_objFundManager, Funds p_objFunds)
        {
            XmlDocument objXmlDoc = null;
            XmlElement objRootNode = null;
            XmlElement objErrorsNode = null;

            int iIndex = 0;
            string sInsufAmntXml = string.Empty;

            try
            {
                #region Root Node
                // Add Root Node tag "TandE" 
                objXmlDoc = new XmlDocument();
                objRootNode = objXmlDoc.CreateElement("TandE");
                objXmlDoc.AppendChild(objRootNode);
                #endregion

                #region Errors Node
                // Add "Errors" node
                if (p_objFundManager.ErrorCount > 0)
                {
                    XmlElement objChildNode = null;

                    objErrorsNode = objRootNode.OwnerDocument.CreateElement("Errors");
                    objRootNode.AppendChild(objErrorsNode);

                    objErrorsNode.SetAttribute("Count", p_objFundManager.ErrorCount.ToString());
                    for (iIndex = 0; iIndex < p_objFundManager.ErrorCount; iIndex++)
                    {
                        objChildNode = objErrorsNode.OwnerDocument.CreateElement("Error");
                        objChildNode.InnerText = p_objFundManager.Error(iIndex).ToString();
                        objErrorsNode.AppendChild(objChildNode);
                    }
                    objChildNode = null;
                }
                #endregion

                sInsufAmntXml = p_objFundManager.InsufAmntXml;
                sInsufAmntXml = sInsufAmntXml.Replace('~', '<');
                sInsufAmntXml = sInsufAmntXml.Replace('|', '>');

                XmlElement objXmlElem = null;
                objXmlElem = objRootNode.OwnerDocument.CreateElement("InsufAmntXml");
                objXmlElem.InnerXml = sInsufAmntXml;
                objRootNode.AppendChild(objXmlElem);
                objXmlElem = null;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("InvoiceDriver.GetXMLForTransaction.ErrorOutXML", m_iClientId), p_objEx);
            }
            finally
            {
                objRootNode = null;
                objErrorsNode = null;
            }
            return objXmlDoc;
        }

        #endregion

        /// <summary>
        /// This function fills in the copy right information.
        /// </summary>
        /// <param name="p_objCopyRightData">Element to be filled.</param>
        private void GetCopyRightElement(ref XmlElement p_objCopyRightData)
        {
            p_objCopyRightData.SetAttribute("CopyRightProp", m_MessageSettings["DEF_RMPROPRIETARY"].ToString());
            p_objCopyRightData.SetAttribute("ProductName", m_MessageSettings["DEF_NAME"].ToString());
            p_objCopyRightData.SetAttribute("CopyRightDef", m_MessageSettings["DEF_RMCOPYRIGHT"].ToString());
            p_objCopyRightData.SetAttribute("CopyRightsReserved", m_MessageSettings["DEF_RMRIGHTSRESERVED"].ToString());
        }

        #endregion
    }
}
