﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Application.ImagRightWrapper.ImageRightService;
using System.Xml;
using Riskmaster.Common;
using System.IO;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using System.Collections.Specialized;

namespace Riskmaster.Application.ImagRightWrapper
{
    /*
     * ImageRight has the following hierarchy vis a vis the documents
     * Drawer -> File -> Folder -> Documents
     */

    /// <summary>
    /// This class is a wrapper for Image Right web service implementation.
    /// Use this class to interact with ImageRight web service.
    /// Remember: Call Dispose!
    /// </summary>
    public class ImageRight : IDisposable
    {
        #region private variables
        string m_sServiceUrl;
        string m_sConnectionName;
        string m_sSession;
        string m_sWsUserName;
        string m_sWsPassword;
        int m_iClientId;

        string m_sConnectionString;

        IRWebService40 m_IRServeiceProxy = null;
        #endregion
        #region Constructor
        /// <summary>
        /// Constructor - Initializes web service
        /// </summary>
        /// <param name="p_sConnectionString">The connectin string to RISKMASTER database</param>
        public ImageRight(string p_sConnectionString, int p_iClientId)
        {
            m_sConnectionString = p_sConnectionString;

            m_iClientId = p_iClientId;
            
            //Get Settings from configuration
            InitializeWebServiceConfigValues();

            if (m_sServiceUrl == string.Empty)
                throw new RMAppException(Globalization.GetString("ImageRightFileHandling.IncompleteSetup", m_iClientId));
            //Initialize ImageRight web service
            m_IRServeiceProxy = new IRWebService40()
            {
                Url = m_sServiceUrl
            };
        }

        #endregion

        #region Public Methods
        #region Not Used
        #region Gel All folder Type in a Drawer
        /// <summary>
        /// Gets All Folders within a drawer
        /// </summary>
        /// <param name="p_lDrawerId">Drawer id from which folders are to be retrieved</param>
        /// <param name="p_bGetChildFolder">If true - recursively retrieves all child folders</param>
        /// <returns></returns>
        public XmlDocument GetFoldersForDrawer(long p_lDrawerId, bool p_bGetChildFolder)
        {
            bool bAuthenticated = false;
            Drawer drawer = null;
            XmlDocument xDoc = null;
            XmlNode xRootNode = null;
            XmlNode xDocNode = null;
            try
            {
                bAuthenticated = Authenticate();
                if (!bAuthenticated)
                {
                    //TODO: throw RMAppException instead
                    throw new Exception(Globalization.GetString("ImageRight.AuthenticationFailedError", m_iClientId));
                }

                xDoc = new XmlDocument();
                xRootNode = xDoc.CreateNode(XmlNodeType.Element, "Folders", string.Empty);
                xDocNode = xDoc.CreateNode(XmlNodeType.Element, "Folder", string.Empty);

                var allFolderTypes = new List<FolderType>();

                drawer = m_IRServeiceProxy.GetDrawerByRef(ref m_sSession, new DrawerRef { RefId = p_lDrawerId });
                var fileTemplates =
                    drawer.FileTypes.Select(fileType => m_IRServeiceProxy.GetFileTemplate(ref m_sSession, fileType.Id.RefId)).ToList();
                
                foreach (var fileTemplate in fileTemplates)
                {
                    allFolderTypes.AddRange(GetAllFolderTypes(fileTemplate.FolderTypes, p_bGetChildFolder));
                }
                foreach (var folderType in allFolderTypes)
                {
                    CreateAndAppendNode("Id", folderType.Id.RefId.ToString(), xDocNode, xDoc);
                    CreateAndAppendNode("FolderName", folderType.Id.RefId.ToString(), xDocNode, xDoc);
                    CreateAndAppendNode("DateCreated", DateToString(folderType.FolderTypeInfo.DateCreated), xDocNode, xDoc);

                    xRootNode.AppendChild(xDocNode);
                }
                xDoc.AppendChild(xRootNode);
            }
            catch
            {
               
            }
            return xDoc;
        }
        
        #endregion

        #region Get All Document Typess in a File
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_lFileTypeId"></param>
        /// <param name="p_bGetDocumentsFromChildFolder"></param>
        /// <returns></returns>
        public XmlDocument GetDocumentTypesForFileType(long p_lFileTypeId, bool p_bGetDocumentsFromChildFolder)
        {
            bool bAuthenticated = false;
            XmlDocument xDoc = null;
            XmlNode xRootNode = null;
            XmlNode xDocNode = null;
            try
            {
                bAuthenticated = Authenticate();
                if (!bAuthenticated)
                {
                    //TODO: throw RMAppException instead
                    throw new Exception(Globalization.GetString("ImageRight.AuthenticationFailedError", m_iClientId));
                }
                xDoc = new XmlDocument();
                xRootNode = xDoc.CreateNode(XmlNodeType.Element, "Documents", string.Empty);
                xDocNode = xDoc.CreateNode(XmlNodeType.Element, "Doc", string.Empty);
                
                var fileTemplate = m_IRServeiceProxy.GetFileTemplate(ref m_sSession, p_lFileTypeId);
                var allDocumentTypes = new List<DocumentType>();
                if (fileTemplate.DocumentTypes != null && fileTemplate.DocumentTypes.Any())
                {
                    allDocumentTypes.AddRange(fileTemplate.DocumentTypes);
                }
                allDocumentTypes.AddRange(GetAllDocumentTypes(fileTemplate.FolderTypes, p_bGetDocumentsFromChildFolder));
                foreach (var documentType in allDocumentTypes)
                {
                    CreateAndAppendNode("Id", documentType.Id.RefId.ToString(), xDocNode, xDoc);
                    CreateAndAppendNode("Name", documentType.DocumentTypeInfo.Name, xDocNode, xDoc);
                    CreateAndAppendNode("DateCreated", DateToString(documentType.DocumentTypeInfo.DateCreated), xDocNode, xDoc);
                }
            }
            catch
            {
                
            }

            return xDoc;
        }

        
        #endregion

        #region Get All Documents Types in a Folder
        public XmlDocument GetDocumentTypesForFileType(long p_lFileTypeId, long p_lFolderTypeId, bool p_bGetDocumentsFromChildFolder)
        {
            bool bAuthenticated = false;
            XmlDocument xDoc = null;
            XmlNode xRootNode = null;
            XmlNode xDocNode = null;

            try
            {
                bAuthenticated = Authenticate();

                if (!bAuthenticated)
                {
                    //TODO: throw RMAppException instead
                    throw new Exception(Globalization.GetString("ImageRight.AuthenticationFailedError", m_iClientId));
                }
                
                xDoc = new XmlDocument();
                xRootNode = xDoc.CreateNode(XmlNodeType.Element, "Documents", string.Empty);
                xDocNode = xDoc.CreateNode(XmlNodeType.Element, "Doc", string.Empty);

                var fileTemplate = m_IRServeiceProxy.GetFileTemplate(ref m_sSession, p_lFileTypeId);
                var allDocumentTypes = new List<DocumentType>();
                allDocumentTypes.AddRange(GetAllDocumentTypes(fileTemplate.FolderTypes, p_lFolderTypeId, p_bGetDocumentsFromChildFolder));
                foreach (var documentType in allDocumentTypes)
                {
                    CreateAndAppendNode("Id", documentType.Id.RefId.ToString(), xDocNode, xDoc);
                    CreateAndAppendNode("Name", documentType.DocumentTypeInfo.Name, xDocNode, xDoc);
                    CreateAndAppendNode("DateCreated", DateToString(documentType.DocumentTypeInfo.DateCreated), xDocNode, xDoc);
                }
            }
            catch
            {
                
            }

            return xDoc;
        }
        #endregion
        #endregion
        #region Get All Documents in a File
        /// <summary>
        /// Gets documents from ImageRight corresponding to claim.
        /// Catch: The RISKMASTER claim number and ImageRight File Number should be same.
        /// </summary>
        /// <param name="p_sClaimId">The claim id of the claim for which documents are to be retrieved</param>
        /// <returns>Xml Document containing details of the ImageRight documents</returns>
        public XmlDocument GetDocumentsForClaim(string p_sClaimId)
        {
            string sClaimNumber = DbFactory.ExecuteAsType<string>(m_sConnectionString, "SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID=" + p_sClaimId);
            return GetAllDocumentsInFile(sClaimNumber);
        }

        /// <summary>
        /// Gets documents from ImageRight for the file number specified
        /// </summary>
        /// <param name="p_sFileNumber">ImageRight File Number</param>
        /// <returns>Xml Document containing details of the ImageRight documents</returns>
        public XmlDocument GetAllDocumentsInFile(string p_sFileNumber)
        {
            bool bAuthenticated = false;
            Document[] documentArray = null;
            XmlDocument xDoc = null;
            XmlNode xRootNode = null;
            XmlNode xDocNode = null;

            long lFileTypeId = 0L;
            string sFileType = string.Empty;
            LocalCache oLocalCache = null;
            int iClaimId = 0;
            try
            {
                bAuthenticated = Authenticate();
                if (!bAuthenticated)
                {
                    //TODO: throw RMAppException instead
                    throw new Exception(Globalization.GetString("ImageRight.AuthenticationFailedError", m_iClientId));
                }

                iClaimId = DbFactory.ExecuteAsType<int>(m_sConnectionString, "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + p_sFileNumber + "'");

                oLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                int iCodeId = DbFactory.ExecuteAsType<int>(m_sConnectionString, "SELECT IR_FILE_TYPE_CODE FROM CLAIM_SUPP WHERE CLAIM_ID=" + iClaimId);
                if (iCodeId > 0)
                {
                    sFileType = oLocalCache.GetCodeDesc(iCodeId);
                }
                if (string.IsNullOrEmpty(sFileType))
                {
                    sFileType = GetImageRightConfigValue("IR_FileType");
                }

                lFileTypeId = GetFileTypeRefId(sFileType);

                xDoc = new XmlDocument();
                xRootNode = xDoc.CreateNode(XmlNodeType.Element, "Documents", string.Empty);
                documentArray = m_IRServeiceProxy.FindDocuments(ref m_sSession, -1, new FileTypeRef { RefId = lFileTypeId }, p_sFileNumber, string.Empty, string.Empty, string.Empty, false, false);

                foreach (Document oDoc in documentArray)
                {
                    xDocNode = xDoc.CreateNode(XmlNodeType.Element, "Doc", string.Empty);
                    CreateAndAppendNode("ID", oDoc.Id.RefId.ToString(), xDocNode, xDoc);
                    CreateAndAppendNode("Description", oDoc.Description, xDocNode, xDoc);
                    CreateAndAppendNode("CreateDate", DateToString(oDoc.DateCreated), xDocNode, xDoc);
                    CreateAndAppendNode("Pages", oDoc.PageCount.ToString(), xDocNode, xDoc);
                    CreateAndAppendNode("DocType", oDoc.ObjType.Name, xDocNode, xDoc);
                    xRootNode.AppendChild(xDocNode);
                }

                xDoc.AppendChild(xRootNode);

            }
            catch (Exception ex)
            {
                Log.Write("Error In ImageRight.GetAllDocumentsInFile: " + ex.ToString(),m_iClientId);
                throw new Exception(ex.Message);
            }
            finally
            {
                if (oLocalCache!=null)
                {
                    oLocalCache.Dispose();
                }
            }

            return xDoc;
        }
        #endregion

        #region Get Document Content
        /// <summary>
        /// Gets content of document 
        /// </summary>
        /// <param name="p_iDocId">The ImageRight Document Id</param>
        /// <param name="oMemoryStream">stream of bytes</param>
        public void GetDocumentContent(int p_iDocId, out MemoryStream oMemoryStream)
        {
            oMemoryStream = null;
            bool bAllPages = false;
            bool bNoPages = false;
            try
            {
                byte[] fileContent = GetDocumentContent(p_iDocId, out bAllPages, out bNoPages);
                oMemoryStream = new MemoryStream(fileContent);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        /// <summary>
        /// Gets content of document
        /// </summary>
        /// <param name="p_iDocId">The ImageRight Document Id</param>
        /// <returns>Byte Array</returns>
        public byte[] GetDocumentContent(int p_iDocId, out bool p_bAllpages, out bool p_bNoPages)
        {
            Document oDoc = null;
            byte[] fileContent = null;
            bool bAuthenticated = false;
            List<PageRef> pageRefs = new List<PageRef>();
            p_bAllpages = true;
            p_bNoPages = true;
            ImageList imgList = null;
            try
            {
                bAuthenticated = Authenticate();
                if (!bAuthenticated)
                {
                    //TODO: throw RMAppException instead
                    throw new Exception(Globalization.GetString("ImageRight.AuthenticationFailedError", m_iClientId));
                }

                oDoc = m_IRServeiceProxy.GetDocumentByRef(ref m_sSession, new DocumentRef { RefId = p_iDocId }, true, false);
                if (oDoc.Content!=null)
                {
                    foreach (Page oPage in oDoc.Pages)
                    {
                        imgList = null;
                        imgList = m_IRServeiceProxy.GetImage(ref m_sSession, new PageRef { RefId = oPage.Id.RefId }, oPage.Version, OutputTypes.PDF);
                        pageRefs.Add(oPage.Id);
                        if (imgList.Images != null)
                        {
                            pageRefs.Add(oPage.Id);
                            p_bNoPages = false;
                        }
                        else
                        {
                            p_bAllpages = false;
                        }
                    }
                }
                else
                {
                    p_bNoPages = true;
                    p_bAllpages = false;
                }

                if (!p_bNoPages)
                {
                    fileContent = m_IRServeiceProxy.GetMultiPageImageFileUsingPages(ref m_sSession, pageRefs.ToArray(), OutputTypes.PDF);
                }
            }
            catch
            {
                throw;
            }

            return fileContent;
        }
        #endregion

        #endregion

        #region private Methods

        #region Authentication
        /// <summary>
        /// Authenticates the given userName and password - for access to ImageRight webservice
        /// </summary>
        /// <returns>true if authenticated. False if authentication fails</returns>
        private bool Authenticate()
        {
            bool bAuthenticated = false;

            try
            {
                m_sSession = m_IRServeiceProxy.UserLogin(m_sWsUserName, m_sWsPassword, m_sConnectionName);
                if (!string.IsNullOrEmpty(m_sSession))
                {
                    bAuthenticated = true;
                }
            }
            catch
            {
                bAuthenticated = false;
                throw;
            }

            return bAuthenticated;
        }
        #endregion

        #region Not Used
        /// <summary>
        /// Gets all folder typess within the current folder
        /// </summary>
        /// <param name="p_folderTypes">ImageRight folder types from which documents are to be retrieved</param>
        /// <param name="p_bGetChildFolder">If true loops through all the folders (recursively) within the current folder
        /// If false - only returns the folders within current.</param>
        /// <returns>Image Right folders</returns>
        private IEnumerable<FolderType> GetAllFolderTypes(FolderType[] p_folderTypes, bool p_bGetChildFolder)
        {
            var allFolderTypes = new List<FolderType>();
            allFolderTypes.AddRange(p_folderTypes);
            if (p_bGetChildFolder)
            {
                foreach (var folderType in p_folderTypes)
                {
                    allFolderTypes.AddRange(GetAllFolderTypes(folderType.FolderTypes, p_bGetChildFolder));
                }
            }
            return allFolderTypes;
        }
        /// <summary>
        /// Retrieves all document types which are within the folder provided.
        /// </summary>
        /// <param name="p_folderTypes">ImageRight folder types from which documents are to be retrieved</param>
        /// <param name="p_bGetDocumentsFromChildFolder">If true loops through all the folders (recursively) within the current folder 
        /// and returns the docuents from within
        /// If false - only returns the documents from the current folder.</param>
        /// <returns>ImageRight Documents</returns>
        private IEnumerable<DocumentType> GetAllDocumentTypes(FolderType[] p_folderTypes, bool p_bGetDocumentsFromChildFolder)
        {
            var allDocumentTypes = new List<DocumentType>();
            allDocumentTypes.AddRange(p_folderTypes.SelectMany(folderType => folderType.DocumentTypes));
            if (p_bGetDocumentsFromChildFolder)
            {
                //Call the method recursively - by passing the childfolder
                foreach (var folderType in p_folderTypes)
                {
                    allDocumentTypes.AddRange(GetAllDocumentTypes(folderType.FolderTypes, p_bGetDocumentsFromChildFolder));
                }
            }
            return allDocumentTypes;
        }

        /// <summary>
        /// Retrieves all document types which are within the folder provided.
        /// </summary>
        /// <param name="p_folderTypes">ImageRight folder types from which documents are to be retrieved</param>
        /// <param name="p_lFolderTypeId">The folder id from which the documents are to be retrieved.</param>
        /// <param name="p_bGetDocumentsFromChildFolder">If true loops through all the folders (recursively) within the current folder 
        /// and returns the docuents from within
        /// If false - only returns the documents from the current folder.</param>
        /// <returns>ImageRight Documents</returns>
        private IEnumerable<DocumentType> GetAllDocumentTypes(FolderType[] p_folderTypes, long p_lFolderTypeId, bool p_bGetDocumentsFromChildFolder)
        {
            var allDocumentTypes = new List<DocumentType>();
            allDocumentTypes.AddRange(p_folderTypes.Where(folderType => folderType.Id.RefId == p_lFolderTypeId).SelectMany(
                    folderType => folderType.DocumentTypes));
            if (p_bGetDocumentsFromChildFolder)
            {
                //Get all child folders within the given folder id.
                foreach (var folderType in p_folderTypes.Where(folderType => folderType.Id.RefId == p_lFolderTypeId))
                {
                    allDocumentTypes.AddRange(GetAllDocumentTypes(folderType.FolderTypes, p_bGetDocumentsFromChildFolder));
                }
            }
            
            return allDocumentTypes;
        }
        #endregion

        private long GetFileTypeRefId(string p_sFileTypeName)
        {
            ObjectTypeData oFileType = null;
            try
            {
                oFileType = m_IRServeiceProxy.GetFileType(ref m_sSession, p_sFileTypeName);
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(),m_iClientId);
                throw new RMAppException(ex.Message);
            }

            return oFileType.Id.RefId;
        }
        
        /// <summary>
        /// Appends the Xml Node (p_xBaseNode) with a child node.
        /// </summary>
        /// <param name="p_sName">Name of new node</param>
        /// <param name="p_sValue">Value(Inner Text of the new node)</param>
        /// <param name="p_xBaseNode">Node to which the new node is to be appended</param>
        /// <param name="p_xDoc">XmlDocument to which the node belongs</param>
        private void CreateAndAppendNode(string p_sName, string p_sValue, XmlNode p_xBaseNode, XmlDocument p_xDoc)
        {
            XmlNode xNode = p_xDoc.CreateNode(XmlNodeType.Element, p_sName, string.Empty);
            xNode.InnerText = p_sValue;
            p_xBaseNode.AppendChild(xNode);
        }

        /// <summary>
        /// Converts Date provided to string - Format being MM/dd/yyyy
        /// </summary>
        /// <param name="p_Date">DateTime</param>
        /// <returns>Date in MM/dd/yyyy format</returns>
        private string DateToString(DateTime p_Date)
        {
            return Conversion.GetDBDateFormat(Conversion.ToDbDateTime(p_Date), "MM/dd/yyyy");
        }

        /// <summary>
        /// Reads value from configuration
        /// </summary>
        private void InitializeWebServiceConfigValues()
        {
            m_sConnectionName = GetImageRightConfigValue("ws_Connection");
            m_sWsUserName = GetImageRightConfigValue("ws_UserName");
            m_sWsPassword = GetImageRightConfigValue("ws_Password");
            m_sServiceUrl = GetImageRightConfigValue("ws_Url");
        }

        /// <summary>
        /// Reads value from configuration file
        /// </summary>
        /// <param name="p_sKey">Key for which value is to be retrieved</param>
        /// <returns></returns>
        private string GetImageRightConfigValue(string p_sKey)
        {
            string sValue = string.Empty;
            NameValueCollection nvCol = RMConfigurationManager.GetNameValueSectionSettings("ImageRight", m_sConnectionString, m_iClientId);
            if (nvCol != null)
            {
                sValue = nvCol[p_sKey];
            }

            return sValue;
        }
        #endregion

        #region Interface Method
        /// <summary>
        /// 1. Logs off ImageRight active session.
        /// 2. Closes Connection to web service
        /// </summary>
        public void Dispose()
        {
            if (m_IRServeiceProxy != null)
            {
                try
                {
                    if (!string.IsNullOrEmpty(m_sSession))
                    {
                        m_IRServeiceProxy.UserLogoff(m_sSession);
                    }
                }
                catch
                {
                }
                finally
                {
                    m_IRServeiceProxy.Dispose();
                    m_IRServeiceProxy = null;
                }
                
                
            }
        }
        #endregion
    }
}
