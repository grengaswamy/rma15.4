﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Collections.Specialized;
using Riskmaster.DataModel;
using System.Xml;
using System.IO;
using Riskmaster.Settings;

namespace Riskmaster.Application.ImagRightWrapper
{
    public class FUPGenerator
    {
        UserLogin m_oUserLogin;
        string m_sConnectionString;
        string[][] arrFileLayout;

        string m_sUserData1, m_sUserData2, m_sUserData3, m_sUserData4, m_sUserData5;
        int m_iCurrentRowNumber;
        //int m_iFileMark1, m_iFileMark2, m_iFileMark3;
        List<int> m_listUnmark;
        List<int> m_listFileMark;
        int m_iClientId;

        public FUPGenerator(UserLogin p_oUserLogin, int p_iClientId)
        {
            m_oUserLogin = p_oUserLogin;
            m_sConnectionString = p_oUserLogin.objRiskmasterDatabase.ConnectionString;

            m_sUserData1 = string.Empty;
            m_sUserData2 = string.Empty;
            m_sUserData3 = string.Empty;
            m_sUserData4 = string.Empty;
            m_sUserData5 = string.Empty;

            m_iCurrentRowNumber = 0;

            //m_iFileMark1 = 0;
            //m_iFileMark2 = 0;
            //m_iFileMark3 = 0;
            m_listFileMark = new List<int>();
            m_listUnmark = new List<int>();

            m_iClientId = p_iClientId;
        }
        public XmlDocument GenerateFUP(XmlDocument p_objXmlIn)
        {
            int iClaimId = 0;
            bool bSuccess = false;
            if (p_objXmlIn.SelectSingleNode("//ClaimId")!=null)
            {
                iClaimId = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("//ClaimId").InnerText, out bSuccess);
            }

            if (iClaimId>0)
            {
                GenerateFUP(iClaimId, null);
            }
            else
            {
                throw new RMAppException(Globalization.GetString("FUPGenerator.MissingOrInvalidIdError", m_iClientId));
            }

            return p_objXmlIn;
        }

        /// <summary>
        /// Generates FUP
        /// </summary>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <param name="p_objClaim">Claim Object. Pass this object if calling from Claim scripts. Else pass null.</param>
        public void GenerateFUP(int p_iClaimId, Claim p_objClaim)
        {
            StringBuilder sbFUPContent = new StringBuilder();
            int iSeqNum = 0;
            m_iCurrentRowNumber = 1;
            int iNumberOfRows = 0;
            string sErrorMsg = string.Empty;
            string sFUPFileContent = string.Empty;

            //Get FUP File Layout
            GetFileLayout();

            //Get UserData values
            DocumentMap oDocumentMap = new DocumentMap(m_oUserLogin, m_iClientId);
            oDocumentMap.GetUserData(p_iClaimId, p_objClaim, FUPFileMappings.UserDataType.FUP, ref m_sUserData1, ref m_sUserData2, ref m_sUserData3, ref m_sUserData4, ref m_sUserData5, ref sErrorMsg);
            if (!string.IsNullOrEmpty(sErrorMsg))
            {
                throw new RMAppException(sErrorMsg);
            }

            //Get FileMark values
            GetFileMarks(p_iClaimId, p_objClaim, ref sErrorMsg);
            if (!string.IsNullOrEmpty(sErrorMsg))
            {
                throw new RMAppException(sErrorMsg);
            }

            //Get Number Of Rows
            iNumberOfRows = GetNumberOfFUPRows(new string[] { m_sUserData1, m_sUserData2, m_sUserData3, m_sUserData4, m_sUserData5 });

            //Get FUP File Content
            while (m_iCurrentRowNumber <= iNumberOfRows)
            {
                sbFUPContent.Append(GetNextValue(p_iClaimId, ref iSeqNum, ref m_iCurrentRowNumber));
            }

            //Handle Multiple Claimants
            sFUPFileContent = UpdateUserKey(p_objClaim, sbFUPContent.ToString(), p_iClaimId);

            //Save Content to File
            ExportToFUPFile(sFUPFileContent);
        }

        private string GetNextValue(int p_iClaimId, ref int p_iSeqNumber, ref int p_iRowNum)
        {
            bool bSuccess = false;
            string sFUPName = string.Empty;
            int iFUPLength = 0;
            string sFUPValue = string.Empty;
            bool bIsLasValue = false;

            sFUPName = arrFileLayout[p_iSeqNumber][0];
            iFUPLength = Conversion.CastToType<int>(arrFileLayout[p_iSeqNumber][1], out bSuccess);

            //Get Value of FUP Element
            sFUPValue = GetFUPValue(sFUPName, p_iClaimId, ref bIsLasValue);

            //If length is greater than allowed length - then remove extra characters
            if (sFUPValue.Length > iFUPLength)
            {
                sFUPValue = sFUPValue.Remove(iFUPLength);
            }

            //Use full length by adding extra space
            sFUPValue = sFUPValue.PadRight(iFUPLength);

            //If last value - increase row number and change line
            if (bIsLasValue)
            {
                sFUPValue = sFUPValue + Environment.NewLine;
                ++p_iRowNum;

                //Reset sequence number for next line
                p_iSeqNumber = 0;
            }
            else
            {
                //Increase sequence number
                ++p_iSeqNumber;
            }

            
            return (sFUPValue);
        }

        private int GetNumberOfFUPRows(string[] p_sUserDataArray)
        {
            int iNumberOfRows = 0;
            int iNumberOfUD_n = 0;

            foreach (string sUserData in p_sUserDataArray)
            {
                iNumberOfUD_n = sUserData.Split(new string[] { "~|~" }, StringSplitOptions.None).Length;
                if (iNumberOfRows < iNumberOfUD_n)
                {
                    iNumberOfRows = iNumberOfUD_n;
                }
            }

            //Each row can have 3 file marks and unmarks
            if (m_listFileMark.Count > m_listUnmark.Count)
            {
                if (iNumberOfRows < Math.Ceiling((decimal)m_listFileMark.Count/3))
                {
                    //Math.Ceiling retruns decimal to handle numbers like NaN
                    iNumberOfRows = (int)Math.Ceiling((decimal)m_listFileMark.Count / 3);
                }
            }
            else
            {
                if (iNumberOfRows < Math.Ceiling((decimal)m_listUnmark.Count / 3))
                {
                    //Math.Ceiling retruns decimal to handle numbers like NaN
                    iNumberOfRows = (int)Math.Ceiling((decimal)m_listUnmark.Count / 3);
                }
            }

            return iNumberOfRows;
        }

        private string GetFUPValue(string p_sFUPName, int p_iClaimId, ref bool p_bIsLastValue)
        {
            LocalCache oLocalCache = null;
            string sReturnValue = string.Empty;
            p_bIsLastValue = false;
            int iCodeId = 0;
            switch (p_sFUPName.ToUpper())
            {
                case "DRAWER":
                    //sReturnValue = GetImageRightConfigValue("IR_Drawer");
                    oLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                    iCodeId = DbFactory.ExecuteAsType<int>(m_sConnectionString, "SELECT IR_DRAWER_CODE FROM CLAIM_SUPP WHERE CLAIM_ID=" + p_iClaimId);
                    if (iCodeId>0)
                    {
                        sReturnValue = oLocalCache.GetCodeDesc(iCodeId);
                        oLocalCache.Dispose();
                    }
                    if (string.IsNullOrEmpty(sReturnValue))
                    {
                        sReturnValue = GetImageRightConfigValue("IR_Drawer");
                    }
                    break;
                case "FILE NUMBER":
                    sReturnValue = DbFactory.ExecuteAsType<string>(m_sConnectionString, "SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID=" + p_iClaimId);
                    break;
                case "FILE NAME":
                    //sReturnValue = DbFactory.ExecuteAsType<string>(m_sConnectionString, 
                    //    "SELECT E.LAST_NAME FROM CLAIMANT C INNER JOIN ENTITY E ON C.CLAIMANT_EID = E.ENTITY_ID WHERE C.PRIMARY_CLMNT_FLAG=-1 AND C.CLAIM_ID=" 
                    //    + p_iClaimId);
                    //Modified by taleeb : baesd on web.config setting.
                    sReturnValue = GetFileName(p_iClaimId);
                    break;
                case "USERDATA1":
                    sReturnValue = GetUserData(m_sUserData1, m_iCurrentRowNumber);
                    break;
                case "USERDATA2":
                    sReturnValue = GetUserData(m_sUserData2, m_iCurrentRowNumber);
                    break;
                case "USERDATA3":
                    sReturnValue = GetUserData(m_sUserData3, m_iCurrentRowNumber);
                    break;
                case "USERDATA4":
                    sReturnValue = GetUserData(m_sUserData4, m_iCurrentRowNumber);
                    break;
                case "USERDATA5":
                    sReturnValue = GetUserData(m_sUserData5, m_iCurrentRowNumber);
                    break;
                case "FILE MARK 1":
                case "FILE MARK 2":
                case "FILE MARK 3":
                    if (m_listFileMark.Count>0)
                    {
                        sReturnValue = Convert.ToString(m_listFileMark.ElementAt<int>(0));
                        m_listFileMark.RemoveAt(0);
                    }
                    break;
                case "FILE TYPE":
                case "NEW FILE TYPE":
                    oLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                    iCodeId = DbFactory.ExecuteAsType<int>(m_sConnectionString, "SELECT IR_FILE_TYPE_CODE FROM CLAIM_SUPP WHERE CLAIM_ID=" + p_iClaimId);
                    if (iCodeId>0)
                    {
                        if (string.Compare(GetImageRightConfigValue("UseShortCodeInFUPFileType"),"true", true) == 0)
                        {
                            sReturnValue = oLocalCache.GetShortCode(iCodeId);
                        }
                        else
                        {
                            sReturnValue = oLocalCache.GetCodeDesc(iCodeId);
                        }
                        oLocalCache.Dispose();
                    }
                    //if (string.IsNullOrEmpty(sReturnValue))
                    //{
                    //    string sFileType = GetImageRightConfigValue("IR_FileType");
                    //}
                    
                    break;
                case "UNMARK1":
                case "UNMARK2":
                    if (m_listUnmark.Count > 0)
                    {
                        sReturnValue = Convert.ToString(m_listUnmark.ElementAt<int>(0));
                        m_listUnmark.RemoveAt(0);
                    }
                    break;
                case "UNMARK3":
                    if (m_listUnmark.Count > 0)
                    {
                        sReturnValue = Convert.ToString(m_listUnmark.ElementAt<int>(0));
                        m_listUnmark.RemoveAt(0);
                    }
                    p_bIsLastValue = true;
                    break;
                case "USER ID":
                case "USERKEY1":
                case "NEW FILE NUMBER":
                case "NEW DRAWER":
                case "STATUS":
                case "STATUS DATE":
                case "LONG USER ID":
                default:
                    sReturnValue = string.Empty;
                    break;
            }
            return sReturnValue;
        }

        private void GetFileLayout()
        {
            FUPFileMappings oFupFileMappings = new FUPFileMappings(m_oUserLogin, m_iClientId);
            XmlDocument xFileLayout = oFupFileMappings.GetFupFileLayout();
            int iLength = 0;
            bool bSuccess = false;
            int iSeqNum = 0;
            
            int iNumberOfRows = xFileLayout.SelectNodes("//FieldName").Count;
            arrFileLayout = new string[iNumberOfRows][];

            foreach (XmlNode xNode in xFileLayout.SelectNodes("//FieldName"))
            {
                if (!string.IsNullOrEmpty(xNode.InnerText) && xNode.Attributes["Length"] != null)
                {
                    iLength = Conversion.CastToType<int>(xNode.Attributes["Length"].Value, out bSuccess);
                    arrFileLayout[iSeqNum] = new string[2];
                    arrFileLayout[iSeqNum][0] = xNode.InnerText;
                    arrFileLayout[iSeqNum][1] = iLength.ToString();

                    iSeqNum++;
                }
            }
        }

        /// <summary>
        ///  
        /// </summary>
        /// <param name="p_sFieldName"></param>
        /// <param name="p_bIgnoreCase"></param>
        /// <param name="p_iOffset">Offset is the start position(starting from 0).</param>
        /// <param name="p_iLength">Length is the length of this field</param>
        /// <returns></returns>
        private bool GetOffsetAndLength(string p_sFieldName, bool p_bIgnoreCase, out int p_iOffset, out int p_iLength)
        {
            p_iOffset = 0;
            p_iLength = 0;
            bool bFound = false;
            bool bSuccess = false;
            for (int i = 0; i < arrFileLayout.GetUpperBound(0); i++)
            {
                p_iLength = Conversion.CastToType<int>(arrFileLayout[i][1], out bSuccess);
                if (String.Compare(p_sFieldName,arrFileLayout[i][0],p_bIgnoreCase) == 0)
                {
                    bFound = true;
                    break;
                }
                else
                {
                    p_iOffset += p_iLength;
                }
            }

            return bFound;
        }

        private string GetUserData(string p_sUserData, int p_iRowNum)
        {
            string sValue = string.Empty;
            if (p_sUserData.Split(new string[] { "~|~" }, StringSplitOptions.None).Length >= p_iRowNum)
            {
                sValue = p_sUserData.Split(new string[] { "~|~" }, StringSplitOptions.None)[p_iRowNum - 1];
            }

            return sValue;
        }

        #region commented code
        //private void GetFileMarks(int p_iClaimId, out int p_iFileMark1, out int p_iFileMark2, out int p_iFileMark3, ref string p_sErrorMsg)
        //{
        //    FUPFileMappings oFupFileMapping = null;
        //    DataModelFactory oFactory = null;
        //    Claim oClaim = null;
        //    string sSql = string.Empty;

        //    string sIRMarkId = string.Empty;
        //    string sTableName = string.Empty;
        //    string sFieldName = string.Empty;
        //    string sOperator = string.Empty;
        //    string sValue = string.Empty;
        //    string sSysTableName = string.Empty;

        //    int iCodeId = 0;
        //    int iMarkId = 0;
        //    bool bIsCount = false;
        //    bool bSuccess = false;
        //    bool bAllFileMarkAssigned = false;

        //    //Using Nullable int to determine if FileMark is assigned or not
        //    int? iFileMark1 = null;
        //    int? iFileMark2 = null;
        //    int? iFileMark3 = null;
            
        //    XmlDocument xFileMark = null;
        //    try
        //    {
        //        oFupFileMapping = new FUPFileMappings(m_oUserLogin);
        //        oFactory = new DataModelFactory(m_oUserLogin);
        //        oClaim = oFactory.GetDataModelObject("Claim", false) as Claim;
        //        oClaim.MoveTo(p_iClaimId);

        //        //Get All File marks mapping
        //        xFileMark = oFupFileMapping.GetFileMarks(string.Empty);
                
        //        //Iterate through all mapping node to get file marks
        //        foreach (XmlNode xMap in xFileMark.SelectNodes("//Map"))
        //        {
        //            //1. Read Values from XmlNode
        //            if (xMap.SelectSingleNode("IRMarkId")!=null)
        //            {
        //                sIRMarkId = xMap.SelectSingleNode("IRMarkId").InnerText;
        //                iMarkId = Conversion.CastToType<int>(sIRMarkId, out bSuccess);
        //            }
        //            if (xMap.SelectSingleNode("SysTableName") != null)
        //            {
        //                sSysTableName = xMap.SelectSingleNode("SysTableName").InnerText;
        //            }
        //            if (xMap.SelectSingleNode("Count") != null)
        //            {
        //                bIsCount = ((string.Compare(xMap.SelectSingleNode("Count").InnerText, "True", true) == 0) ? true : false);
        //            }
        //            if (xMap.SelectSingleNode("TableName") != null)
        //            {
        //                sTableName = xMap.SelectSingleNode("TableName").InnerText;
        //            }
        //            if (xMap.SelectSingleNode("FieldName") != null)
        //            {
        //                sFieldName = xMap.SelectSingleNode("FieldName").InnerText;
        //            }
        //            if (xMap.SelectSingleNode("Operator") != null)
        //            {
        //                sOperator = xMap.SelectSingleNode("Operator").InnerText;
        //            }
        //            if (xMap.SelectSingleNode("Value") != null)
        //            {
        //                sValue = xMap.SelectSingleNode("Value").InnerText;
        //                if (xMap.Attributes["codeid"] != null)
        //                {
        //                    iCodeId = Conversion.CastToType<int>(xMap.Attributes["codeid"].Value, out bSuccess);
        //                }
        //                else
        //                {
        //                    iCodeId = 0;
        //                }
        //            }

        //            //check if all file marks are assigned
        //            if (bAllFileMarkAssigned)
        //            {
        //                m_listUnmark.Add(iMarkId);
        //                continue;
        //            }
        //            //Check if Condition Matches
        //            if (IsMatch(bIsCount, sTableName, sFieldName, sSysTableName, sOperator, sValue, iCodeId, oClaim))
        //            {
        //                AssignFileMark(ref iFileMark1, ref iFileMark2, ref iFileMark3, iMarkId);
        //            }
        //            else
        //            {
        //                m_listUnmark.Add(iMarkId);
        //            }

        //            //If All File Marks are assigned
        //            if (iFileMark1!=null && iFileMark2!=null && iFileMark3!=null)
        //            {
        //                bAllFileMarkAssigned = true;
        //            }
        //        }

        //        //Assign File Mark values
        //        p_iFileMark1 = iFileMark1 != null ? iFileMark1.Value : 0;
        //        p_iFileMark2 = iFileMark2 != null ? iFileMark2.Value : 0;
        //        p_iFileMark3 = iFileMark3 != null ? iFileMark3.Value : 0;

        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (oClaim!=null)
        //        {
        //            oClaim.Dispose();
        //        }
        //        if (oFactory!=null)
        //        {
        //            oFactory.Dispose();
        //        }
        //    }
        //}
        #endregion

        private void GetFileMarks(int p_iClaimId, Claim p_objClaim, ref string p_sErrorMsg)
        {
            FUPFileMappings oFupFileMapping = null;
            DataModelFactory oFactory = null;
            Claim oClaim = null;
            string sSql = string.Empty;

            string sIRMarkId = string.Empty;
            string sTableName = string.Empty;
            string sFieldName = string.Empty;
            string sOperator = string.Empty;
            string sValue = string.Empty;
            string sSysTableName = string.Empty;

            int iCodeId = 0;
            int iMarkId = 0;
            bool bIsCount = false;
            bool bSuccess = false;

            XmlDocument xFileMark = null;
            try
            {
                if (p_objClaim == null)
                {
                    oFactory = new DataModelFactory(m_oUserLogin, m_iClientId);
                    oClaim = oFactory.GetDataModelObject("Claim", false) as Claim;
                    oClaim.MoveTo(p_iClaimId);
                }
                else
                {
                    oClaim = p_objClaim;
                }

                oFupFileMapping = new FUPFileMappings(m_oUserLogin, m_iClientId);

                //Get All File marks mapping
                xFileMark = oFupFileMapping.GetFileMarks(string.Empty);

                //Iterate through all mapping node to get file marks
                foreach (XmlNode xMap in xFileMark.SelectNodes("//Map"))
                {
                    //1. Read Values from XmlNode
                    if (xMap.SelectSingleNode("IRMarkId") != null)
                    {
                        sIRMarkId = xMap.SelectSingleNode("IRMarkId").InnerText;
                        iMarkId = Conversion.CastToType<int>(sIRMarkId, out bSuccess);
                    }
                    if (xMap.SelectSingleNode("SysTableName") != null)
                    {
                        sSysTableName = xMap.SelectSingleNode("SysTableName").InnerText;
                    }
                    if (xMap.SelectSingleNode("Count") != null)
                    {
                        bIsCount = ((string.Compare(xMap.SelectSingleNode("Count").InnerText, "True", true) == 0) ? true : false);
                    }
                    if (xMap.SelectSingleNode("TableName") != null)
                    {
                        sTableName = xMap.SelectSingleNode("TableName").InnerText;
                    }
                    if (xMap.SelectSingleNode("FieldName") != null)
                    {
                        sFieldName = xMap.SelectSingleNode("FieldName").InnerText;
                    }
                    if (xMap.SelectSingleNode("Operator") != null)
                    {
                        sOperator = xMap.SelectSingleNode("Operator").InnerText;
                    }
                    if (xMap.SelectSingleNode("Value") != null)
                    {
                        sValue = xMap.SelectSingleNode("Value").InnerText;
                        if (xMap.Attributes["codeid"] != null)
                        {
                            iCodeId = Conversion.CastToType<int>(xMap.Attributes["codeid"].Value, out bSuccess);
                        }
                        else
                        {
                            iCodeId = -1;
                        }
                    }

                    //Check if Condition Matches
                    if (IsMatch(bIsCount, sTableName, sFieldName, sSysTableName, sOperator, sValue, iCodeId, oClaim))
                    {
                        if (!m_listFileMark.Contains(iMarkId))
                        {
                            m_listFileMark.Add(iMarkId);    
                        }
                        
                        //If this id is set for unmark - then remove it
                        if (m_listUnmark.Contains(iMarkId))
                        {
                            m_listUnmark.Remove(iMarkId);
                        }
                    }
                    else
                    {
                        //if the id is not set for mark - add for unmark
                        if (!m_listFileMark.Contains(iMarkId))
                        {
                            if (!m_listUnmark.Contains(iMarkId))
                            {
                                m_listUnmark.Add(iMarkId);    
                            }
                            
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (oClaim != null)
                {
                    oClaim.Dispose();
                }
                if (oFactory != null)
                {
                    oFactory.Dispose();
                }
            }
        }

        private void AssignFileMark(ref int? p_iFileMark1, ref int? p_iFileMark2, ref int? p_iFileMark3, int p_iFileMarkValue)
        {
            if (p_iFileMark1 == null)
            {
                p_iFileMark1 = p_iFileMarkValue;
            }
            else if (p_iFileMark2 == null)
            {
                p_iFileMark2 = p_iFileMarkValue;
            }
            else
            {
                p_iFileMark3 = p_iFileMarkValue;
            }
        }

        private bool IsMatch(bool p_bIsCount, string p_sTableName, string p_sFieldName, string p_sSysFieldName, string p_sOperator, string p_sValue, int p_iCodeId, Claim p_oClaim)
        {
            bool bIsMatch = false;

            switch (p_sTableName.ToUpper())
            {
                case "CLAIM":
                    bIsMatch = IsMatchClaim(p_bIsCount, p_sFieldName, p_sSysFieldName, p_sOperator, p_sValue, p_iCodeId, p_oClaim);
                    break;
                case "CLAIMANT":
                    bIsMatch = IsMatchClaimant(p_bIsCount, p_sFieldName, p_sSysFieldName, p_sOperator, p_sValue, p_iCodeId, p_oClaim);
                    break;
                case "LITIGATION":
                    bIsMatch = IsMatchLitigation(p_bIsCount, p_sFieldName, p_sSysFieldName, p_sOperator, p_sValue, p_iCodeId, p_oClaim);
                    break;
                case "SUBROGATION":
                    bIsMatch = IsMatchSubrogation(p_bIsCount, p_sFieldName, p_sSysFieldName, p_sOperator, p_sValue, p_iCodeId, p_oClaim);
                    break;
                case "ARBITRATION":
                    bIsMatch = IsMatchArbitration(p_bIsCount, p_sFieldName, p_sSysFieldName, p_sOperator, p_sValue, p_iCodeId, p_oClaim);
                    break;
                case "CLAIM SUPP":
                    bIsMatch = IsMatchClaimSupp(p_bIsCount, p_sFieldName, p_sSysFieldName, p_sOperator, p_sValue, p_iCodeId, p_oClaim);
                    break;
                case "CLAIMANT SUPP":
                    bIsMatch = IsMatchClaimantSupp(p_bIsCount, p_sFieldName, p_sSysFieldName, p_sOperator, p_sValue, p_iCodeId, p_oClaim);
                    break;
                case "LITIGATION SUPP":
                    bIsMatch = IsMatchLitigationSupp(p_bIsCount, p_sFieldName, p_sSysFieldName, p_sOperator, p_sValue, p_iCodeId, p_oClaim);
                    break;
                default:
                    bIsMatch = false;
                    break;
            }

            return bIsMatch;
        }

        private bool IsMatchClaim(bool p_bIsCount, string p_sFieldName, string p_sSysFieldName, string p_sOperator, string p_sValue, int p_iCodeId, Claim p_oClaim)
        {
            bool bSuccess = false;
            bool bIsMatch = false;
            string sCalculatedValue = string.Empty;
            int iCalculatedValue = 0;
            int iValue = 0;

            if (p_bIsCount)
            {
                iCalculatedValue = 1;
                iValue = Conversion.CastToType<int>(p_sValue, out bSuccess);
                bIsMatch = CompareValues(p_sOperator, iValue, iCalculatedValue);
            }
            else if (p_iCodeId >= 0)
            {
                switch (p_sFieldName.ToUpper())
                {
                    case "CLAIM STATUS":
                        bIsMatch = CompareValues(p_sOperator, p_oClaim.ClaimStatusCode, p_iCodeId);
                        break;
                    case "CLAIM TYPE":
                        bIsMatch = CompareValues(p_sOperator, p_oClaim.ClaimTypeCode, p_iCodeId);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                //Placeholder for future codes
            }

            return bIsMatch;
        }

        private bool IsMatchClaimant(bool p_bIsCount, string p_sFieldName, string p_sSysFieldName, string p_sOperator, string p_sValue, int p_iCodeId, Claim p_oClaim)
        {
            bool bSuccess = false;
            bool bIsMatch = false;
            int iCalculatedValue = 0;
            int iValue = 0;
            
            if (p_bIsCount)
            {
                iCalculatedValue = p_oClaim.ClaimantList.Count;
                iValue = Conversion.CastToType<int>(p_sValue, out bSuccess);
                bIsMatch = CompareValues(p_sOperator, iValue, iCalculatedValue);
            }
            else if (p_iCodeId >= 0)
            {
                foreach (Claimant oClaimant in p_oClaim.ClaimantList)
                {
                    switch (p_sFieldName.ToUpper())
                    {
                        case "CLAIMANT TYPE":
                            bIsMatch = CompareValues(p_sOperator, p_oClaim.PrimaryClaimant.ClaimantTypeCode, p_iCodeId);
                            break;
                        case "NAME TYPE":
                            bIsMatch = CompareValues(p_sOperator, p_oClaim.PrimaryClaimant.ClaimantEntity.NameType, p_iCodeId);
                            break;
                        default:
                            break;
                    }

                    if (bIsMatch)
                    {
                        break;
                    }
                }
            }

            return bIsMatch;
        }

        private bool IsMatchLitigation(bool p_bIsCount, string p_sFieldName, string p_sSysFieldName, string p_sOperator, string p_sValue, int p_iCodeId, Claim oClaim)
        {
            bool bSuccess = false;
            bool bIsMatch = false;
            int iCalculatedValue = 0;
            int iValue = 0;

            if (p_bIsCount)
            {
                iCalculatedValue = oClaim.LitigationList.Count;
                iValue = Conversion.CastToType<int>(p_sValue, out bSuccess);
                bIsMatch = CompareValues(p_sOperator, iValue, iCalculatedValue);
            }
            else if (p_iCodeId >= 0)
            {
                foreach (ClaimXLitigation oClmXLitig in oClaim.LitigationList)
                {
                    switch (p_sFieldName.ToUpper())
                    {
                        case "LITIGATION TYPE":
                            bIsMatch = CompareValues(p_sOperator, oClmXLitig.LitTypeCode, p_iCodeId);
                            break;
                        case "LITIGATION STATUS":
                            bIsMatch = CompareValues(p_sOperator, oClmXLitig.LitStatusCode, p_iCodeId);
                            break;
                        default:
                            break;
                    }

                    if (bIsMatch)
                    {
                        break;
                    }
                }
                
            }

            return bIsMatch;
        }

        private bool IsMatchSubrogation(bool p_bIsCount, string p_sFieldName, string p_sSysFieldName, string p_sOperator, string p_sValue, int p_iCodeId, Claim oClaim)
        {
            bool bSuccess = false;
            bool bIsMatch = false;
            int iCalculatedValue = 0;
            int iValue = 0;

            if (p_bIsCount)
            {
                iCalculatedValue = oClaim.SubrogationList.Count;
                iValue = Conversion.CastToType<int>(p_sValue, out bSuccess);
                bIsMatch = CompareValues(p_sOperator, iValue, iCalculatedValue);
            }
            else if (p_iCodeId >= 0)
            {
                foreach (ClaimXSubrogation oClmXSubrogation in oClaim.SubrogationList)
                {
                    switch (p_sFieldName.ToUpper())
                    {
                        case "SUBROGATION TYPE":
                            bIsMatch = CompareValues(p_sOperator, oClmXSubrogation.SubTypeCode, p_iCodeId);
                            break;
                        case "SUBROGATION STATUS":
                            bIsMatch = CompareValues(p_sOperator, oClmXSubrogation.SubStatusCode, p_iCodeId);
                            break;
                        default:
                            break;
                    }

                    if (bIsMatch)
                    {
                        break;
                    }
                }

            }

            return bIsMatch;
        }

        private bool IsMatchArbitration(bool p_bIsCount, string p_sFieldName, string p_sSysFieldName, string p_sOperator, string p_sValue, int p_iCodeId, Claim oClaim)
        {
            bool bSuccess = false;
            bool bIsMatch = false;
            int iCalculatedValue = 0;
            int iValue = 0;

            if (p_bIsCount)
            {
                iCalculatedValue = oClaim.ArbitrationList.Count;
                iValue = Conversion.CastToType<int>(p_sValue, out bSuccess);
                bIsMatch = CompareValues(p_sOperator, iValue, iCalculatedValue);
            }
            else if (p_iCodeId >= 0)
            {
                foreach (ClaimXArbitration oClmXArbitration in oClaim.ArbitrationList)
                {
                    switch (p_sFieldName.ToUpper())
                    {
                        case "ARBITRATION TYPE":
                            bIsMatch = CompareValues(p_sOperator, oClmXArbitration.ArbTypeCode, p_iCodeId);
                            break;
                        case "ARBITRATION STATUS":
                            bIsMatch = CompareValues(p_sOperator, oClmXArbitration.ArbStatusCode, p_iCodeId);
                            break;
                        case "ARBITRATION PARTY":
                            bIsMatch = CompareValues(p_sOperator, oClmXArbitration.ArbPartyCode, p_iCodeId);
                            break;
                        default:
                            break;
                    }

                    if (bIsMatch)
                    {
                        break;
                    }
                }

            }

            return bIsMatch;
        }

        private bool IsMatchClaimSupp(bool p_bIsCount, string p_sFieldName, string p_sSysFieldName, string p_sOperator, string p_sValue, int p_iCodeId, Claim oClaim)
        {
            bool bSuccess = false;
            bool bIsMatch = false;
            int iCalculatedValue = 0;
            int iValue = 0;

            if (p_bIsCount)
            {
                iCalculatedValue = 1;
                iValue = Conversion.CastToType<int>(p_sValue, out bSuccess);
                bIsMatch = CompareValues(p_sOperator, iValue, iCalculatedValue);
            }
            else if (p_iCodeId > 0)
            {
                bIsMatch = GetAndCompareCodeId(p_sFieldName, "CLAIM_SUPP", oClaim.KeyFieldName, oClaim.KeyFieldValue, p_iCodeId, p_sOperator);
            }
            else
            {
                bIsMatch = GetAndCompareValues(p_sFieldName, "CLAIM_SUPP", oClaim.KeyFieldName, oClaim.KeyFieldValue, p_sValue, p_sOperator);
            }

            return bIsMatch;
        }

        private bool IsMatchClaimantSupp(bool p_bIsCount, string p_sFieldName, string p_sSysFieldName, string p_sOperator, string p_sValue, int p_iCodeId, Claim oClaim)
        {
            bool bSuccess = false;
            bool bIsMatch = false;
            int iCalculatedValue = 0;
            int iValue = 0;

            if (p_bIsCount)
            {
                iCalculatedValue = oClaim.ClaimantList.Count;
                iValue = Conversion.CastToType<int>(p_sValue, out bSuccess);
                bIsMatch = CompareValues(p_sOperator, iValue, iCalculatedValue);
            }
            else if (p_iCodeId > 0)
            {
                foreach (Claimant oClaimant in oClaim.ClaimantList)
                {
                    bIsMatch = GetAndCompareCodeId(p_sFieldName, "CLAIMANT_SUPP", oClaimant.KeyFieldName, oClaimant.KeyFieldValue, p_iCodeId, p_sOperator);
                    if (bIsMatch)
                    {
                        break;
                    }
                }
            }
            else
            {
                foreach (Claimant oClaimant in oClaim.ClaimantList)
                {
                    bIsMatch = GetAndCompareValues(p_sFieldName, "CLAIMANT_SUPP", oClaimant.KeyFieldName, oClaimant.KeyFieldValue, p_sValue, p_sOperator);
                    if (bIsMatch)
                    {
                        break;
                    }
                }
            }

            return bIsMatch;
        }

        private bool IsMatchLitigationSupp(bool p_bIsCount, string p_sFieldName, string p_sSysFieldName, string p_sOperator, string p_sValue, int p_iCodeId, Claim oClaim)
        {
            bool bSuccess = false;
            bool bIsMatch = false;
            int iCalculatedValue = 0;
            int iValue = 0;

            if (p_bIsCount)
            {
                iCalculatedValue = oClaim.SubrogationList.Count;
                iValue = Conversion.CastToType<int>(p_sValue, out bSuccess);
                bIsMatch = CompareValues(p_sOperator, iValue, iCalculatedValue);
            }
            else if (p_iCodeId > 0)
            {
                foreach (ClaimXLitigation oLitigation in oClaim.LitigationList)
                {
                    bIsMatch = GetAndCompareCodeId(p_sFieldName, "LITIGATION_SUPP", oLitigation.KeyFieldName, oLitigation.KeyFieldValue, p_iCodeId, p_sOperator);
                    if (bIsMatch)
                    {
                        break;
                    }
                }
            }
            else
            {
                foreach (ClaimXLitigation oLitigation in oClaim.LitigationList)
                {
                    bIsMatch = GetAndCompareValues(p_sFieldName, "LITIGATION_SUPP", oLitigation.KeyFieldName, oLitigation.KeyFieldValue, p_sValue, p_sOperator);
                    if (bIsMatch)
                    {
                        break;
                    }
                }
            }

            return bIsMatch;
        }

        private bool CompareValues(string p_sOperator, int p_iValue1, int p_iValue2)
        {
            bool bTrue = false;

            switch (p_sOperator.ToUpper())
            {
                case "=":
                    bTrue = (p_iValue1 == p_iValue2);
                    break;
                case ">":
                    bTrue = (p_iValue1 > p_iValue2);
                    break;
                case "<":
                    bTrue = (p_iValue1 < p_iValue2);
                    break;
                case ">=":
                    bTrue = (p_iValue1 >= p_iValue2);
                    break;
                case "<=":
                    bTrue = (p_iValue1 <= p_iValue2);
                    break;
                case "<>":
                    bTrue = (p_iValue1 != p_iValue2);
                    break;
                case "IS NULL":
                    bTrue = (p_iValue1 == 0);
                    break;
                case "IS NOT NULL":
                    bTrue = (p_iValue1 > 0);
                    break;
                default:
                    bTrue = false;
                    break;
            }

            return bTrue;
        }

        private bool CompareValues(string p_sOperator, string p_sValue1, string p_sValue2)
        {
            bool bTrue = false;

            switch (p_sOperator.ToUpper())
            {
                case "=":
                    bTrue = (string.Compare(p_sValue1, p_sValue2) == 0);
                    break;
                case "<>":
                    bTrue = (string.Compare(p_sValue1, p_sValue2) != 0);
                    break;
                case "IS NULL":
                    bTrue = string.IsNullOrEmpty(p_sValue1);
                    break;
                case "IS NOT NULL":
                    bTrue = (!string.IsNullOrEmpty(p_sValue1));
                    break;
                default:
                    bTrue = false;
                    break;
            }

            return bTrue;
        }

        private bool GetAndCompareCodeId(string p_sColumnName, string p_sTableName, string p_sPrimaryKey, int p_KeyId, int p_iCodeId, string p_sOperator)
        {
            bool bTrue = false;
            int iValue = DbFactory.ExecuteAsType<int>(m_sConnectionString, 
                string.Format("SELECT {0} FROM {1} WHERE {2}={3}", new object[]{p_sColumnName, p_sTableName, p_sPrimaryKey, p_KeyId}));

            bTrue = CompareValues(p_sOperator, iValue, p_iCodeId);

            return bTrue;
        }

        private bool GetAndCompareValues(string p_sColumnName, string p_sTableName, string p_sPrimaryKey, int p_KeyId, string p_sValue, string p_sOperator)
        {
            bool bTrue = false;
            string sValue1 = DbFactory.ExecuteAsType<string>(m_sConnectionString,
                string.Format("SELECT {0} FROM {1} WHERE {2}={3}", new object[] { p_sColumnName, p_sTableName, p_sPrimaryKey, p_KeyId }));

            bTrue = CompareValues(p_sOperator, sValue1, p_sValue);

            return bTrue;
        }

        private void ExportToFUPFile(string p_sFUPFileContent)
        {
            string sTempPath = string.Empty;
            string sExportPath = string.Empty;
            string sFileName = string.Empty;
            string sFilePath = string.Empty;
            try
            {
                sTempPath = RMConfigurator.TempPath;
                sFileName = string.Format("FUP_{0}.fup", DateTime.Now.ToString("yyyyMMddHHmmssfff"));
                sFilePath = Path.Combine(sTempPath, sFileName);
                sExportPath = GetImageRightConfigValue("FUPFilePath");

                if (string.IsNullOrEmpty(sExportPath))
                {
                    throw new RMAppException(Globalization.GetString("FUPGenerator.MissingValueInConfigError", m_iClientId));
                }

                //Write to temporary File
                File.WriteAllText(sFilePath, p_sFUPFileContent);
                //Move the file to destination
                File.Move(sFilePath, Path.Combine(sExportPath, sFileName));
                //Delete the file at temporary location
                File.Delete(sFilePath);

            }
            catch (Exception ex)
            {
                Log.Write("Error In ExportToFupFile: " + ex.ToString(),m_iClientId);
                throw new RMAppException(ex.Message);
            }
        }

        private string UpdateUserKey(Claim p_objClaim, string p_sFUPFileContent, int p_iClaimId)
        {
            int iClmntRowId = Int32.MaxValue;
            string sSql = string.Empty;
            int iOffset = 0;
            int iLength = 0;
            string sFieldName = string.Empty;
            string sValue = string.Empty;
            string sFormattedValue = string.Empty;
            DocumentMap oDocumentMap = null;
            bool bExists = false;
            StringBuilder sbFUPContentToReturn = null;
            Claimant oClaimant = null;
            List<Claimant> lstClaimant = null;
            DataModelFactory oFactory = null;
            try
            {
                sbFUPContentToReturn = new StringBuilder();

                oDocumentMap = new DocumentMap(m_oUserLogin, m_iClientId);
                ClaimantTypeToUse oClaimantTypeToUse = oDocumentMap.GetClaimantTypeToUse();

                if (p_objClaim == null)
                {
                    oFactory = new DataModelFactory(m_oUserLogin, m_iClientId);
                    p_objClaim = oFactory.GetDataModelObject("Claim", false) as Claim;
                    p_objClaim.MoveTo(p_iClaimId);
                }

                lstClaimant = new List<Claimant>();

                if (oClaimantTypeToUse == ClaimantTypeToUse.UsePrimary || oClaimantTypeToUse == ClaimantTypeToUse.UseFirst)
                {
                    oClaimant = p_objClaim.PrimaryClaimant;
                    if (oClaimant != null)
                    {
                        lstClaimant.Add(oClaimant);
                    }
                }
                if (lstClaimant.Count == 0)
                {
                    foreach (Claimant oClmnt in p_objClaim.ClaimantList)
                    {
                        if (oClmnt.ClaimantRowId < iClmntRowId)
                        {
                            iClmntRowId = oClmnt.ClaimantRowId;
                        }

                        //if UseAll then add to ClaimantList
                        if (oClaimantTypeToUse == ClaimantTypeToUse.UseAll)
                        {
                            lstClaimant.Add(oClmnt);
                        }

                    }
                    //If Use first or Use primary and first - iClmtRowId now contains the lowest row id of all claimants.
                    //In case of UsePrimaryOrFirst - the control will reach here only when there is no primary claimant
                    if (iClmntRowId < Int32.MaxValue && (oClaimantTypeToUse == ClaimantTypeToUse.UseFirst || oClaimantTypeToUse == ClaimantTypeToUse.UsePrimaryOrFirst))
                    {
                        lstClaimant.Add(p_objClaim.ClaimantList[iClmntRowId]);
                    }
                }

                bExists = GetOffsetAndLength("USERKEY1", true, out iOffset, out iLength);
                if (bExists)
                {
                    foreach (Claimant oClmnt in lstClaimant)
                    {
                        sValue = (oClmnt.ClaimantEntity.FirstName + " " + oClmnt.ClaimantEntity.LastName).Trim().PadRight(iLength);
                        if (sValue.Length > iLength)
                        {
                            sValue.Remove(iLength);
                        }
                        sbFUPContentToReturn.Append(p_sFUPFileContent.Remove(iOffset, iLength).Insert(iOffset, sValue));
                    }
                }

                if (sbFUPContentToReturn.Length == 0)
                {
                    sbFUPContentToReturn.Append(p_sFUPFileContent);
                }

            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                throw;
            }
            finally
            {
                if (oFactory!=null)
                {
                    oFactory.Dispose();
                }
            }

            return Convert.ToString(sbFUPContentToReturn);
        }

        private string GetFileName(int p_iClaimId)
        {
            int iInsuredEid = 0;
            string sSql = string.Empty;
            string sFirstName = string.Empty;
            string sLastName = string.Empty;
            string sReturnValue = string.Empty;
            string sFileNameConfig = GetImageRightConfigValue("FUPFileName");
            try
            {
                if (string.Compare(sFileNameConfig, "[FirstInsured]", true) == 0)
                {
                    iInsuredEid = DbFactory.ExecuteAsType<int>(m_sConnectionString, "SELECT PXI.INSURED_EID FROM POLICY_X_INSURED PXI INNER JOIN CLAIM_X_POLICY CXP ON PXI.POLICY_ID = CXP.POLICY_ID WHERE CLAIM_ID="
                    + p_iClaimId + " ORDER BY PXI.INSURED_EID ASC");


                    if (iInsuredEid > 0)
                    {
                        sSql = "SELECT E.FIRST_NAME, E.LAST_NAME FROM ENTITY E WHERE E.ENTITY_ID =" + iInsuredEid;
                        using (DbReader oDBReader = DbFactory.ExecuteReader(m_sConnectionString, sSql))
                        {
                            if (oDBReader.Read())
                            {
                                if (oDBReader["FIRST_NAME"] != null && oDBReader["FIRST_NAME"] != DBNull.Value)
                                {
                                    sFirstName = oDBReader["FIRST_NAME"].ToString();
                                }
                                if (oDBReader["LAST_NAME"] != null && oDBReader["LAST_NAME"] != DBNull.Value)
                                {
                                    sFirstName = oDBReader["LAST_NAME"].ToString();
                                }
                            }
                        }
                    }


                    sReturnValue = (sFirstName + " " + sLastName).Trim();
                }
                else
                {
                    sReturnValue = DbFactory.ExecuteAsType<string>(m_sConnectionString,
                        "SELECT E.LAST_NAME FROM CLAIMANT C INNER JOIN ENTITY E ON C.CLAIMANT_EID = E.ENTITY_ID WHERE C.PRIMARY_CLMNT_FLAG=-1 AND C.CLAIM_ID="
                        + p_iClaimId);
                }


            }
            catch
            {
                throw;
            }
            return sReturnValue;
        }

        private string GetImageRightConfigValue(string p_sKey)
        {
            string sValue = string.Empty;
            NameValueCollection nvCol = RMConfigurationManager.GetNameValueSectionSettings("ImageRight", m_sConnectionString, m_iClientId);
            if (nvCol != null)
            {
                sValue = nvCol[p_sKey];
            }

            return sValue;
        }
    }
}
