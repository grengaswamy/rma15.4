﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Collections.Specialized;

namespace Riskmaster.Application.ImagRightWrapper
{
    public class FUPFileMappings
    {
        public enum UserDataType
        {
            ColdImport,
            FUP
        };

        UserLogin m_oUserLogin;
        string m_sConnectionString;
        int m_iClientId;
        public FUPFileMappings(UserLogin p_oUserLogin, int p_iClientId)
        {
            m_oUserLogin = p_oUserLogin;
            m_sConnectionString = p_oUserLogin.objRiskmasterDatabase.ConnectionString;
            m_iClientId = p_iClientId;
        }       
        public XmlDocument GetFupFileLayout()
        {
            XmlDocument xFileLayout = null;
            XmlNode xRootNode = null;
            XmlNode xNode = null;
            string sSql = string.Empty;
            string sFieldName = string.Empty;
            string sStartPosition = string.Empty;
            string sLength = string.Empty;
            string sFileLayoutId = string.Empty;
            try
            {
                sSql = "SELECT * FROM FUP_FILE_LAYOUT ORDER BY FUP_FILE_LAYOUT_ID ASC";
                xFileLayout = new XmlDocument();
                xRootNode = xFileLayout.CreateNode(XmlNodeType.Element, "FileLayout", string.Empty);

                using (DbReader oReader = DbFactory.ExecuteReader(m_sConnectionString, sSql))
                {
                    if (oReader!=null)
                    {
                        while (oReader.Read())
                        {
                            if (oReader["FUP_FILE_LAYOUT_ID"] != null && oReader["FUP_FILE_LAYOUT_ID"] != DBNull.Value)
                            {
                                sFileLayoutId = Convert.ToString(oReader["FUP_FILE_LAYOUT_ID"]);
                            }
                            if (oReader["FIELD_NAME"] != null && oReader["FIELD_NAME"] != DBNull.Value)
                            {
                                sFieldName = Convert.ToString(oReader["FIELD_NAME"]);
                            }
                            if (oReader["START_POSITION"] != null && oReader["START_POSITION"] != DBNull.Value)
                            {
                                sStartPosition = Convert.ToString(oReader["START_POSITION"]);
                            }
                            if (oReader["LENGTH"] != null && oReader["LENGTH"] != DBNull.Value)
                            {
                                sLength = Convert.ToString(oReader["LENGTH"]);
                            }

                            xNode = xFileLayout.CreateNode(XmlNodeType.Element, "FieldName", string.Empty);
                            ((XmlElement)xNode).SetAttribute("StartPosition", sStartPosition);
                            ((XmlElement)xNode).SetAttribute("Length", sLength);
                            ((XmlElement)xNode).SetAttribute("FileLayoutId", sFileLayoutId);
                            xNode.InnerText = sFieldName;

                            xRootNode.AppendChild(xNode);
                        }
                        xFileLayout.AppendChild(xRootNode);
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Write("Error in GetFupFileLayout:" + ex.ToString(), Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                throw new RMAppException(string.Format(Globalization.GetString("FUPFileMappings.GetFUPFileLayoutError", m_iClientId), ex.Message));
            }
            return xFileLayout;
        }

        public XmlDocument SaveFUPFileLayout(XmlDocument p_objXmlIn)
        {
            XmlNode xEditNode = null;
            int iStartValue = 1;
            int iLength = 0;
            bool bSuccess = false;

            string sFupFileLayoutId = string.Empty;
            int iInitStartPosition = 1;
            int iInitLength = 0;

            DbWriter oWriter = DbFactory.GetDbWriter(m_sConnectionString);

            XmlDocument xFupFileLayoutDoc = GetFupFileLayout();
            foreach (XmlNode xFupFileNode in xFupFileLayoutDoc.SelectNodes("//FieldName"))
            {
                if (xFupFileNode.Attributes["FileLayoutId"] != null)
                {
                    sFupFileLayoutId = xFupFileNode.Attributes["FileLayoutId"].Value;

                    if (xFupFileNode.Attributes["StartPosition"] != null)
                    {
                        iInitStartPosition = Conversion.CastToType<int>(xFupFileNode.Attributes["StartPosition"].Value, out bSuccess);
                    }
                    if (xFupFileNode.Attributes["Length"] != null)
                    {
                        iInitLength = Conversion.CastToType<int>(xFupFileNode.Attributes["Length"].Value, out bSuccess);
                    }

                    xEditNode = p_objXmlIn.SelectSingleNode(string.Format("//FieldName[@FileLayoutId='{0}']", sFupFileLayoutId));
                    if (xEditNode != null && xEditNode.Attributes["Length"] != null)
                    {
                        iLength = Conversion.CastToType<int>(xEditNode.Attributes["Length"].Value, out bSuccess);
                    }
                    else if (xFupFileNode.Attributes["Length"] != null)
                    {
                        iLength = Conversion.CastToType<int>(xFupFileNode.Attributes["Length"].Value, out bSuccess);
                    }
                }

                if (iInitStartPosition != iStartValue || iInitLength != iLength)
                {
                    oWriter.Tables.Add("FUP_FILE_LAYOUT");
                    oWriter.Fields.Add("START_POSITION", iStartValue.ToString());
                    oWriter.Fields.Add("LENGTH", iLength.ToString());

                    oWriter.Where.Add("FUP_FILE_LAYOUT_ID = " + sFupFileLayoutId);

                    oWriter.Execute();
                    oWriter.Reset(true);
                }

                //GetValue of next "StartPosition"
                iStartValue += iLength;
            }

            return xFupFileLayoutDoc;
        }

        public XmlDocument GetUserDataMapping(string p_sUserDataId, string p_sUserDataType)
        {
            XmlDocument xDocUserData = null;
            XmlNode xRootNode = null;
            XmlNode xNode = null;
            string sSql = string.Empty;
            
            try
            {
                sSql = "SELECT * FROM IR_USER_DATA_MAPPING WHERE USER_DATA_TYPE=" + p_sUserDataType;

                if (!string.IsNullOrEmpty(p_sUserDataId))
                {
                    sSql = sSql + " AND IR_USER_DATA_MAPPING_ID= " + p_sUserDataId;
                }
                sSql = sSql + " ORDER BY IR_USER_DATA_MAPPING_ID ASC ";

                xDocUserData = new XmlDocument();
                xRootNode = xDocUserData.CreateNode(XmlNodeType.Element, "UserDataMapping", string.Empty);

                using (DbReader oReader = DbFactory.ExecuteReader(m_sConnectionString, sSql))
                {
                    if (oReader != null)
                    {
                        while (oReader.Read())
                        {
                            xNode = xDocUserData.CreateNode(XmlNodeType.Element, "Map", string.Empty);
                            if (oReader["IR_USER_DATA_MAPPING_ID"] != null && oReader["IR_USER_DATA_MAPPING_ID"] != DBNull.Value)
                            {
                                CreateAndAppendNode("UserDataMapId", Convert.ToString(oReader["IR_USER_DATA_MAPPING_ID"]), xNode, xDocUserData);
                            }
                            if (oReader["USER_DATA"] != null && oReader["USER_DATA"] != DBNull.Value)
                            {
                                CreateAndAppendNode("UserData", Convert.ToString(oReader["USER_DATA"]), xNode, xDocUserData);
                            }
                            if (oReader["IR_NAME"] != null && oReader["IR_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("ImgRightName", Convert.ToString(oReader["IR_NAME"]), xNode, xDocUserData);
                            }
                            if (oReader["TABLE_NAME"] != null && oReader["TABLE_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("TableName", Convert.ToString(oReader["TABLE_NAME"]), xNode, xDocUserData);
                            }
                            if (oReader["FIELD_NAME"] != null && oReader["FIELD_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("FieldName", Convert.ToString(oReader["FIELD_NAME"]), xNode, xDocUserData);
                            }
                            xRootNode.AppendChild(xNode);
                        }
                        xDocUserData.AppendChild(xRootNode);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write("Error in GetUserDataMapping:" + ex.ToString(), Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                throw new RMAppException(string.Format(Globalization.GetString("FUPFileMappings.GetUserDataMappingError", m_iClientId), ex.Message));
            }

            return xDocUserData;
        }

        public XmlDocument SaveUserDataMapping(XmlDocument p_objXmlIn)
        {
            XmlNode xUserDataMapId = null;
            XmlNode xUserData = null;
            XmlNode xImgRightName = null;
            XmlNode xTableName = null;
            XmlNode xFieldName = null;
            XmlNode xType = null;
            DbWriter oDbWriter = null;
            DataModelFactory oFactory = null;

            bool bSuccess = false;
            UserDataType oType = UserDataType.ColdImport;
            try
            {
                xUserData = p_objXmlIn.SelectSingleNode("//UserData");
                xUserDataMapId = p_objXmlIn.SelectSingleNode("//UserDataMapId");
                xImgRightName = p_objXmlIn.SelectSingleNode("//ImgRightName");
                xTableName = p_objXmlIn.SelectSingleNode("//TableName");
                xFieldName = p_objXmlIn.SelectSingleNode("//FieldName");
                xType = p_objXmlIn.SelectSingleNode("//UserDataType");

                if (xUserData != null && xUserDataMapId != null)
                {
                    if (xType!=null)
                    {
                        oType = (UserDataType)Conversion.CastToType<int>(xType.InnerText, out bSuccess);
                    }
                    
                    //For FUP multiple values for same userdata is allowed
                    if (oType == UserDataType.ColdImport)
                    {
                        if (!ValidateUserData(xUserDataMapId.InnerText, xUserData.InnerText))
                        {
                            throw new RMAppException(Globalization.GetString("FUPFileMappings.UserDataExists", m_iClientId));
                        }
                    }
                    
                }

                oDbWriter = DbFactory.GetDbWriter(m_sConnectionString);
                oDbWriter.Tables.Add("IR_USER_DATA_MAPPING");

                if (xUserData != null)
                {
                    oDbWriter.Fields.Add("USER_DATA", xUserData.InnerText);
                }
                if (xImgRightName != null)
                {
                    oDbWriter.Fields.Add("IR_NAME", xImgRightName.InnerText);
                }
                if (xTableName != null)
                {
                    oDbWriter.Fields.Add("TABLE_NAME", xTableName.InnerText);
                }
                if (xFieldName != null)
                {
                    oDbWriter.Fields.Add("FIELD_NAME", xFieldName.InnerText);
                }
                if (xType!=null)
                {
                    oDbWriter.Fields.Add("USER_DATA_TYPE", xType.InnerText);
                }

                if (xUserDataMapId != null)
                {
                    if (string.IsNullOrEmpty(xUserDataMapId.InnerText) || string.Compare(xUserDataMapId.InnerText, "0") == 0)
                    {
                        oFactory = new DataModelFactory(m_oUserLogin, m_iClientId);
                        oDbWriter.Fields.Add("IR_USER_DATA_MAPPING_ID", oFactory.Context.GetNextUID("IR_USER_DATA_MAPPING"));
                    }
                    else
                    {
                        oDbWriter.Where.Add("IR_USER_DATA_MAPPING_ID = " + xUserDataMapId.InnerText);
                    }
                }

                oDbWriter.Execute();
            }
            catch (Exception ex)
            {
                Log.Write("Error in SaveUserDataMapping:" + ex.ToString(), Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                throw new RMAppException(string.Format(Globalization.GetString("FUPFileMappings.SaveUserDataMappingError", m_iClientId), ex.Message));
            }
            finally
            {
                if (oFactory!=null)
                {
                    oFactory.Dispose();
                }
            }

            return p_objXmlIn;
        }

        public void DeleteUserDataMapping(string p_sUserDataMapId)
        {
            int iId = 0;
            bool bSuccess = false;
            try
            {
                iId = Conversion.CastToType<int>(p_sUserDataMapId, out bSuccess);
                if (iId>0)
                {
                    DbFactory.ExecuteNonQuery(m_sConnectionString, "DELETE FROM IR_USER_DATA_MAPPING WHERE IR_USER_DATA_MAPPING_ID="+iId);
                }
            }
            catch (Exception ex)
            {
                Log.Write("Error in DeleteUserDataMapping:" + ex.ToString(), Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                throw new RMAppException(string.Format(Globalization.GetString("FUPFileMappings.DeleteUserDataMappingError", m_iClientId), ex.Message));
            }
        }

        private bool ValidateUserData(string p_sUserDataMapId, string p_sUserData)
        {
            bool bValid = false;
            bool bSuccess = false;

            string sSql = string.Format("SELECT IR_USER_DATA_MAPPING_ID FROM IR_USER_DATA_MAPPING WHERE USER_DATA LIKE '{0}' AND USER_DATA_TYPE = {1}"
                , p_sUserData, (int)UserDataType.ColdImport);
            int iUserDataMapId = DbFactory.ExecuteAsType<int>(m_sConnectionString, sSql);
            int iUserDataMapIdFromXml = Conversion.CastToType<int>(p_sUserDataMapId, out bSuccess);
            if (iUserDataMapId==0)
            {
                bValid = true;
            }
            else if (iUserDataMapId == iUserDataMapIdFromXml)
            {
                bValid = true;
            }

            return bValid;
        }

        public XmlDocument GetSuppValue(string p_sSuppTableName)
        {
            XmlDocument xSuppFields = null;
            XmlNode xRootNode = null;
            XmlNode xNode = null;
            string sSql = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(p_sSuppTableName))
                {
                    throw new RMAppException(Globalization.GetString("FUPFileMappings.SuppTableNameError", m_iClientId));
                }

                sSql = string.Format("SELECT * FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME LIKE '{0}' AND FIELD_TYPE IN (0,1,2,3,4,5,6,8,9,11)", p_sSuppTableName);

                xSuppFields = new XmlDocument();
                xRootNode = xSuppFields.CreateNode(XmlNodeType.Element, "SuppFields", string.Empty);

                using (DbReader oReader = DbFactory.ExecuteReader(m_sConnectionString, sSql))
                {
                    if (oReader != null)
                    {
                        while (oReader.Read())
                        {
                            xNode = xSuppFields.CreateNode(XmlNodeType.Element, "Supp", string.Empty);
                            if (oReader["SYS_FIELD_NAME"] != null && oReader["SYS_FIELD_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("FieldName", Convert.ToString(oReader["SYS_FIELD_NAME"]), xNode, xSuppFields);
                            }
                            xRootNode.AppendChild(xNode);
                        }
                        xSuppFields.AppendChild(xRootNode);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write("Error in GetSuppValue:" + ex.ToString(), Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                throw new RMAppException(string.Format(Globalization.GetString("FUPFileMappings.GetSuppValueError", m_iClientId), ex.Message));
            }

            return xSuppFields;
        }

        public XmlDocument GetSuppType(string p_sSuppTableName, string p_sSysFieldName)
        {
            XmlDocument xSuppTable = null;
            XmlNode xRootNode = null;
            string sSql = string.Empty;

            int iTableId = 0;
            string sTableName = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(p_sSuppTableName))
                {
                    throw new RMAppException(Globalization.GetString("FUPFileMappings.SuppTableNameError", m_iClientId));
                }

                sSql = string.Format("SELECT CODE_FILE_ID FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME LIKE '{0}' AND SYS_FIELD_NAME LIKE '{1}'", p_sSuppTableName, p_sSysFieldName);
                iTableId = DbFactory.ExecuteAsType<int>(m_sConnectionString, sSql);

                if (iTableId>0)
                {
                    sTableName = DbFactory.ExecuteAsType<string>(m_sConnectionString, "SELECT SYSTEM_TABLE_NAME FROM GLOSSARY WHERE TABLE_ID=" + iTableId);
                }

                xSuppTable = new XmlDocument();
                xRootNode = xSuppTable.CreateNode(XmlNodeType.Element, "SuppFields", string.Empty);
                CreateAndAppendNode("TableName", sTableName, xRootNode, xSuppTable);
                CreateAndAppendNode("TableId", iTableId.ToString(), xRootNode, xSuppTable);
                xSuppTable.AppendChild(xRootNode);
                
            }
            catch (Exception ex)
            {
                Log.Write("Error in GetSuppVlaue:" + ex.ToString(), Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                throw new RMAppException(string.Format(Globalization.GetString("FUPFileMappings.GetSuppValueError", m_iClientId), ex.Message));
            }

            return xSuppTable;
        }

        public XmlDocument GetFileMarks(string p_sFileMarkMappingId)
        {
            
            XmlDocument xDocUserData = null;
            XmlNode xRootNode = null;
            XmlNode xNode = null;
            string sSql = string.Empty;
            string sSysTableName = string.Empty;
            int iCodeId = 0;
            bool bSuccess = false;
            LocalCache oLocalCache = null;
            int iLangCode = 0;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            try
            {
                oLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                iLangCode = Conversion.CastToType<int>(oLocalCache.LanguageCode, out bSuccess);

                sSql = "SELECT * FROM IR_FILEMARK_MAPPING ";

                if (!string.IsNullOrEmpty(p_sFileMarkMappingId))
                {
                    sSql = sSql + " WHERE IR_FILEMARK_MAPPING_ID= " + p_sFileMarkMappingId;
                }
                sSql = sSql + " ORDER BY IR_FILEMARK_MAPPING_ID ASC ";

                xDocUserData = new XmlDocument();
                xRootNode = xDocUserData.CreateNode(XmlNodeType.Element, "FileMarkMapping", string.Empty);

                using (DbReader oReader = DbFactory.ExecuteReader(m_sConnectionString, sSql))
                {
                    if (oReader != null)
                    {
                        while (oReader.Read())
                        {
                            xNode = xDocUserData.CreateNode(XmlNodeType.Element, "Map", string.Empty);
                            if (oReader["IR_FILEMARK_MAPPING_ID"] != null && oReader["IR_FILEMARK_MAPPING_ID"] != DBNull.Value)
                            {
                                CreateAndAppendNode("IRFileMarkMappingId", Convert.ToString(oReader["IR_FILEMARK_MAPPING_ID"]), xNode, xDocUserData);
                            }
                            if (oReader["IR_MARK_ID"] != null && oReader["IR_MARK_ID"] != DBNull.Value)
                            {
                                CreateAndAppendNode("IRMarkId", Convert.ToString(oReader["IR_MARK_ID"]), xNode, xDocUserData);
                            }
                            if (oReader["IR_FILE_MARK_NAME"] != null && oReader["IR_FILE_MARK_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("IRFileMarkName", Convert.ToString(oReader["IR_FILE_MARK_NAME"]), xNode, xDocUserData);
                            }
                            if (oReader["SYS_TABLE_NAME"] != null && oReader["SYS_TABLE_NAME"] != DBNull.Value)
                            {
                                sSysTableName = Convert.ToString(oReader["SYS_TABLE_NAME"]);
                                CreateAndAppendNode("SysTableName", sSysTableName, xNode, xDocUserData);
                            }

                            if (oReader["COUNT"] != null && oReader["COUNT"] != DBNull.Value)
                            {
                                if (Convert.ToInt32(oReader["COUNT"]) == -1)
                                {
                                    CreateAndAppendNode("Count", "True", xNode, xDocUserData);
                                }
                                else
                                {
                                    CreateAndAppendNode("Count", "False", xNode, xDocUserData);
                                }
                            }
                            else
                            {
                                CreateAndAppendNode("Count", "False", xNode, xDocUserData);
                            }

                            if (oReader["TABLE_NAME"] != null && oReader["TABLE_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("TableName", Convert.ToString(oReader["TABLE_NAME"]), xNode, xDocUserData);
                            }
                            if (oReader["FIELD_NAME"] != null && oReader["FIELD_NAME"] != DBNull.Value)
                            {
                                CreateAndAppendNode("FieldName", Convert.ToString(oReader["FIELD_NAME"]), xNode, xDocUserData);
                            }
                            if (oReader["OPERATOR"] != null && oReader["OPERATOR"] != DBNull.Value)
                            {
                                CreateAndAppendNode("Operator", Convert.ToString(oReader["OPERATOR"]), xNode, xDocUserData);
                            }
                            if (oReader["VALUE"] != null && oReader["VALUE"] != DBNull.Value)
                            {
                                if (string.IsNullOrEmpty(sSysTableName) || string.Compare(sSysTableName, "0") == 0)
                                {
                                    CreateAndAppendNode("Value", Convert.ToString(oReader["VALUE"]), xNode, xDocUserData);
                                }
                                else
                                {
                                    iCodeId = Conversion.CastToType<int>(Convert.ToString(oReader["VALUE"]), out bSuccess);
                                    oLocalCache.GetCodeInfo(iCodeId, ref sShortCode, ref sDesc, iLangCode);
                                    CreateAndAppendNode("Value", string.Format("{0} {1}", sShortCode, sDesc), xNode, xDocUserData);
                                    ((XmlElement)xNode).SetAttribute("codeid", iCodeId.ToString());
                                }
                            }
                            xRootNode.AppendChild(xNode);
                        }
                        xDocUserData.AppendChild(xRootNode);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write("Error in GetFileMarks:" + ex.ToString(), Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                throw new RMAppException(string.Format(Globalization.GetString("FUPFileMappings.GetFileMarksError", m_iClientId), ex.Message));
            }
            finally
            {
                if (oLocalCache!=null)
                {
                    oLocalCache.Dispose();
                }
            }
            return xDocUserData;
        }

        public XmlDocument SaveFileMarks(XmlDocument p_objXmlIn)
        {
            XmlNode xFileMarkMappingId = null;
            XmlNode xIRMarkId = null;
            XmlNode xFileMarkName = null;
            XmlNode xTableName = null;
            XmlNode xFieldName = null;
            XmlNode xCount = null;
            XmlNode xOperator = null;
            XmlNode xValue = null;
            XmlNode xSysTablename = null;
            DbWriter oDbWriter = null;
            DataModelFactory oFactory = null;
            try
            {
                xFileMarkMappingId = p_objXmlIn.SelectSingleNode("//IRFileMarkMappingId");
                xIRMarkId = p_objXmlIn.SelectSingleNode("//IRMarkId");
                xFileMarkName = p_objXmlIn.SelectSingleNode("//IRFileMarkName");
                xTableName = p_objXmlIn.SelectSingleNode("//TableName");
                xFieldName = p_objXmlIn.SelectSingleNode("//FieldName");
                xCount = p_objXmlIn.SelectSingleNode("//Count");
                xOperator = p_objXmlIn.SelectSingleNode("//Operator");
                xValue = p_objXmlIn.SelectSingleNode("//Value");
                xSysTablename = p_objXmlIn.SelectSingleNode("//SysTableName");

                oDbWriter = DbFactory.GetDbWriter(m_sConnectionString);
                oDbWriter.Tables.Add("IR_FILEMARK_MAPPING");

                if (xIRMarkId != null)
                {
                    oDbWriter.Fields.Add("IR_MARK_ID", xIRMarkId.InnerText);
                }
                if (xFileMarkName != null)
                {
                    oDbWriter.Fields.Add("IR_FILE_MARK_NAME", xFileMarkName.InnerText);
                }
                if (xTableName != null)
                {
                    oDbWriter.Fields.Add("TABLE_NAME", xTableName.InnerText);
                }
                if (xFieldName != null)
                {
                    oDbWriter.Fields.Add("FIELD_NAME", xFieldName.InnerText);
                }
                if (xCount != null)
                {
                    if (string.Compare(xCount.InnerText, "True",true)==0)
                    {
                        oDbWriter.Fields.Add("COUNT", -1);
                    }
                    else
                    {
                        oDbWriter.Fields.Add("COUNT", 0);
                    }
                }
                if (xOperator != null)
                {
                    oDbWriter.Fields.Add("OPERATOR", xOperator.InnerText);
                }
                if (xValue != null)
                {
                    oDbWriter.Fields.Add("VALUE", xValue.InnerText);
                }
                if (xSysTablename != null)
                {
                    oDbWriter.Fields.Add("SYS_TABLE_NAME", xSysTablename.InnerText);
                }

                if (xFileMarkMappingId != null)
                {
                    if (string.IsNullOrEmpty(xFileMarkMappingId.InnerText) || string.Compare(xFileMarkMappingId.InnerText, "0") == 0)
                    {
                        oFactory = new DataModelFactory(m_oUserLogin, m_iClientId);
                        oDbWriter.Fields.Add("IR_FILEMARK_MAPPING_ID", oFactory.Context.GetNextUID("IR_FILEMARK_MAPPING"));
                    }
                    else
                    {
                        oDbWriter.Where.Add("IR_FILEMARK_MAPPING_ID = " + xFileMarkMappingId.InnerText);
                    }
                }

                oDbWriter.Execute();
            }
            catch (Exception ex)
            {
                Log.Write("Error in SaveFileMarks:" + ex.ToString(), Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                throw new RMAppException(string.Format(Globalization.GetString("FUPFileMappings.SaveFileMarksError", m_iClientId), ex.Message));
            }
            finally
            {
                if (oFactory != null)
                {
                    oFactory.Dispose();
                }
            }

            return p_objXmlIn;
        }

        /// <summary>
        /// Function to delete the FileMarkMapping
        /// </summary> 
        /// <createdby>Varun - vchouhan6</createdby>
        /// <param name="sFileMarkMappingId"></param>
        /// <returns></returns>
        public bool DeleteFileMarkMapping(string sFileMarkMappingId)
        {
            int iFileMarkMapId = 0;
            bool bSuccess = false;
            try
            {
                iFileMarkMapId = Conversion.CastToType<int>(sFileMarkMappingId, out bSuccess);
                if (iFileMarkMapId > 0)
                {
                    DbFactory.ExecuteNonQuery(m_sConnectionString, "DELETE FROM IR_FILEMARK_MAPPING WHERE IR_FILEMARK_MAPPING_ID=" + iFileMarkMapId);
                    bSuccess = true;
                }
            }
            catch (Exception ex)
            {
                Log.Write("Error in DeleteFileMarkMapping:" + ex.ToString(), Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                throw new RMAppException(string.Format(Globalization.GetString("FUPFileMappings.DeleteFileMarkMappingError", m_iClientId), ex.Message));
            }
            return bSuccess;
        }

        private void CreateAndAppendNode(string p_sName, string p_sValue, XmlNode p_xBaseNode, XmlDocument p_xDoc)
        {
            XmlNode xNode = p_xDoc.CreateNode(XmlNodeType.Element, p_sName, string.Empty);
            xNode.InnerText = p_sValue;
            p_xBaseNode.AppendChild(xNode);
        }
    }
}
