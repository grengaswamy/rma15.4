﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Common;
using System.Collections.Specialized;
using Riskmaster.ExceptionTypes;
using System.IO;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.Application.FileStorage;
using System.Text.RegularExpressions;

namespace Riskmaster.Application.ImagRightWrapper
{
    public class ImageRightFileHandling
    {
        private UserLogin m_oUserLogin;
        private string m_sConnectionString;
        int m_iClientId;
        const int MAX_PATH_Length = 260;
        const int MAX_FILE_Length = 255;

        public ImageRightFileHandling(UserLogin p_oUserLoging, int p_iClientId)
        {
            m_oUserLogin = p_oUserLoging;
            m_sConnectionString = p_oUserLoging.objRiskmasterDatabase.ConnectionString;
            m_iClientId = p_iClientId;
        }

        public virtual bool SaveToImageRight(string p_sDocIds, string p_sClaimId)
        {
            bool bSuccess = false;
            int iDocId = 0;
            string sClaimNumber = string.Empty;
            int iClaimId = 0;
            try
            {
                sClaimNumber = DbFactory.ExecuteAsType<string>(m_sConnectionString, "SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID='" + p_sClaimId + "'");
                iClaimId = Conversion.CastToType<int>(p_sClaimId, out bSuccess);
                foreach (string sDocId in p_sDocIds.Split(','))
                {
                    iDocId = Conversion.CastToType<int>(sDocId, out bSuccess);
                    if (bSuccess && iDocId > 0)
                    {
                        bSuccess = SaveToImageRight(iDocId, sClaimNumber, iClaimId);
                        if (!bSuccess)
                        {
                            break;
                        }
                    }
                }
            }
            catch (RMAppException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log.Write("Error In SaveToImageRight(string, string): " + ex.ToString(), Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                bSuccess = false;
                throw;
            }
            return bSuccess;
        }

        public bool SaveToImageRight(int p_iDocId, string p_sClaimNumber, int p_iClaimId)
        {
            bool bSuccess = false;
            string sSql = string.Empty;
            string sDocName = string.Empty;
            string sFileName = string.Empty;
            string sFilePath = string.Empty;
            string sDocCategory = string.Empty;
            string sDocClass = string.Empty;
            string sDocType = string.Empty;
            string sTempPath = string.Empty;
            FileStorageManager oFilesStorageManager = null;
            try
            {
                sSql = "SELECT DOCUMENT_NAME,DOCUMENT_FILENAME,DOCUMENT_FILEPATH,"
                    + " KEYWORDS,CREATE_DATE,NOTES,FOLDER_ID,USER_ID, DOCUMENT_CLASS, "
                    + " DOCUMENT_CATEGORY, DOCUMENT_SUBJECT, DOCUMENT_TYPE, DOC_INTERNAL_TYPE, UPLOAD_STATUS "
                    + "	FROM DOCUMENT "
                    + " WHERE DOCUMENT_ID=" + p_iDocId;

                using (DbReader oDbReader = DbFactory.ExecuteReader(m_sConnectionString, sSql))
                {
                    if (oDbReader.Read())
                    {
                        if (oDbReader["DOCUMENT_NAME"] != null && oDbReader["DOCUMENT_NAME"] != DBNull.Value)
                        {
                            sDocName = oDbReader["DOCUMENT_NAME"].ToString();
                        }
                        if (oDbReader["DOCUMENT_FILENAME"] != null && oDbReader["DOCUMENT_FILENAME"] != DBNull.Value)
                        {
                            sFileName = oDbReader["DOCUMENT_FILENAME"].ToString();
                        }
                        if (oDbReader["DOCUMENT_CATEGORY"] != null && oDbReader["DOCUMENT_CATEGORY"] != DBNull.Value)
                        {
                            sDocCategory = oDbReader["DOCUMENT_CATEGORY"].ToString();
                        }
                        if (oDbReader["DOCUMENT_TYPE"] != null && oDbReader["DOCUMENT_TYPE"] != DBNull.Value)
                        {
                            sDocType = oDbReader["DOCUMENT_TYPE"].ToString();
                        }
                        if (oDbReader["DOCUMENT_CLASS"] != null && oDbReader["DOCUMENT_CLASS"] != DBNull.Value)
                        {
                            sDocClass = oDbReader["DOCUMENT_CLASS"].ToString();
                        }
                    }
                }

                if (((StorageType)m_oUserLogin.objRiskmasterDatabase.DocPathType) == StorageType.DatabaseStorage )
                {
                    //sFileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + sFileName;
                    sTempPath = RMConfigurator.TempPath;
                    oFilesStorageManager = new FileStorageManager(StorageType.DatabaseStorage, m_oUserLogin.objRiskmasterDatabase.GlobalDocPath, m_iClientId);
                    oFilesStorageManager.RetrieveFile(sFileName, Path.Combine(sTempPath, DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_" + sFileName), 
                        m_oUserLogin.LoginName, out sFilePath);

                    sFileName = Path.GetFileName(sFilePath);
                    sFilePath = sTempPath;
                }
                else
                {
                    if (!string.IsNullOrEmpty(m_oUserLogin.DocumentPath))
                    {
                        sFilePath = m_oUserLogin.DocumentPath;
                    }
                    else
                    {
                        sFilePath = m_oUserLogin.objRiskmasterDatabase.GlobalDocPath;
                    }
                }

                if (string.IsNullOrEmpty(sFilePath))
                {
                    throw new IOException(string.Format(Globalization.GetString("ImageRightFileHandling.DocumentAccessError", m_iClientId), p_iDocId));
                }
                if (string.IsNullOrEmpty(sFileName))
                {
                    throw new IOException(string.Format(Globalization.GetString("ImageRightFileHandling.DocumentAccessError", m_iClientId), p_iDocId));
                }

                bSuccess = SaveToImageRight(sDocName, sFilePath, sFileName, sDocClass, sDocCategory, sDocType, p_sClaimNumber, p_iClaimId);

                if (bSuccess)
                {
                    sSql = "UPDATE DOCUMENT SET IR_TRANSFER_STATUS = -1 WHERE DOCUMENT_ID=" + p_iDocId;
                    DbFactory.ExecuteNonQuery(m_sConnectionString, sSql);

                    //Delete temporary file
                    if (((StorageType)m_oUserLogin.objRiskmasterDatabase.DocPathType) == StorageType.DatabaseStorage)
                    {
                        try
                        {
                            File.Delete(Path.Combine(sFilePath, sFileName));
                        }
                        catch (Exception ex)
                        {
                            Log.Write("Error Deleting Temp Files In SaveToImageRight(int, string, int): " + ex.ToString(), Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                        }
                    }
                }
            }
            catch (RMAppException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log.Write("Error In SaveToImageRight(int, string, int): " + ex.ToString(), Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                bSuccess = false;
                throw;
            }
            return bSuccess;
        }

        public bool SaveToImageRight(string p_sDocName, string p_sFilePath, string p_sFileName, string p_sDocClass, string p_sDocCategory, string p_sDocType, 
            string p_sClaimNumber, int p_iClaimId)
        {
            string sIRpath = string.Empty;
            string sFileExtension = string.Empty;
            string sDestinationFileName = string.Empty;
            string sFinalFile = string.Empty;
            bool bSuccess = false;
            string sError = string.Empty;

            Regex oRegEx = null;
            try
            {
                sIRpath = GetImageRightConfigValue("IR_ImportDir");

                if (string.IsNullOrEmpty(sIRpath))
                {
                    throw new RMAppException(Globalization.GetString("ImageRightFileHandling.MissingEntryErrorInConfig", m_iClientId));
                }
                if (!Directory.Exists(sIRpath))
                {
                    throw new IOException(Globalization.GetString("ImageRightFileHandling.DirectoryPathNotFoundError", m_iClientId));
                }

                sFileExtension = Path.GetExtension(p_sFileName);
                sFinalFile = Path.Combine(p_sFilePath, p_sFileName);
                
                sDestinationFileName = Path.Combine(sIRpath, RenameFileForExportToIR(p_sDocClass, p_sDocCategory, p_sDocType, p_sClaimNumber, sFileExtension, p_iClaimId, p_sDocName));

                //Convert to PDF will take care of the following
                //1. Check if conversion is required
                //2. Convert
                //3. If Converted update the sFinalFile and sDestinationFileName (extension of both will be changed to .pdf)
                //bool bConverted = ConvertToPDF(ref sFinalFile, ref sDestinationFileName);

                bSuccess = MoveFile(sFinalFile, sDestinationFileName);
                //if (bConverted)
                //{
                //    try
                //    {
                //        File.Delete(sFinalFile);
                //    }
                //    catch (Exception ex)
                //    {
                //        Log.Write("Error Deleting Temp Files In SaveToImageRight(int, string, int): " + ex.ToString(), Log.LOG_CATEGORY_DEFAULT);
                //    }
                //}
            }
            catch (RMAppException ex)
            {
                Log.Write("Error In SaveToImageRight(string, string, string, string, string, string): " + ex.ToString(), Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw;
            }
            catch (Exception ex)
            {
                Log.Write("Error In SaveToImageRight(string, string, string, string, string, string): " + ex.ToString(), Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                bSuccess = false;
                throw;
            }
            return bSuccess;
        }

        public bool PrintToImageRight(string p_sFile)
        {
            string sIRPrintpath = string.Empty;
            NameValueCollection nvImageRightConfig = null;
            string sFileName = string.Empty;
            string sDestinationFileName = string.Empty;
            bool bSuccess = false;
            try
            {
                nvImageRightConfig = RMConfigurationManager.GetNameValueSectionSettings("ImageRight", m_sConnectionString, m_iClientId);
                sIRPrintpath = nvImageRightConfig["IR_PrintDir"];

                if (string.IsNullOrEmpty(sIRPrintpath))
                {
                    throw new RMAppException(Globalization.GetString("ImageRightFileHandling.MissingEntryErrorInConfig", m_iClientId));
                }
                if (!Directory.Exists(sIRPrintpath))
                {
                    throw new IOException(Globalization.GetString("ImageRightFileHandling.DirectoryPathNotFoundError", m_iClientId));
                }

                sFileName = Path.GetFileName(p_sFile);
                //sDestinationFileName = Path.Combine(sIRPrintpath, RenameFileForExportToIR(sFileName));
                bSuccess = MoveFile(p_sFile, sDestinationFileName);
            }
            catch (Exception ex)
            {
                Log.Write("Error In PrintToImageRight: " + ex.ToString(), Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                bSuccess = false;
                throw;
            }
            return bSuccess;
        }

        /// <summary>
        /// Drawer_ClaimNumber_Folder_DocType___UD1_UD2_UD3_UD4_UD5__Desc_______Cnvrt__DateTime
        /// </summary>
        /// <param name="p_sFileName"></param>
        /// <returns></returns>
        public virtual string RenameFileForExportToIR(string p_sDocClass, string p_sDocCategory, string p_sDocType, string p_sClaimNumber, string p_sExt, int p_iClaimId, string p_sDocName)
        {
            DocumentMap oDocMap = null;
            string sIRFolderType = string.Empty, sIRDocType = string.Empty, sDescription = string.Empty;
            bool bConversion = false;
            string sDrawerName = string.Empty;
            string sFileName = string.Empty;
            string sDateTime = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string sUD1 = string.Empty;
            string sUD2 = string.Empty;
            string sUD3 = string.Empty;
            string sUD4 = string.Empty;
            string sUD5 = string.Empty;
            string sErrorMsg = string.Empty;
            string sConvertToTiff = string.Empty;
            string sConvertExtension = string.Empty;
            LocalCache oLocalCache = null;
            string sInvalidPathChars = string.Empty;
            Regex oRegEx = null;

            try
            {
                oDocMap = new DocumentMap(m_oUserLogin, m_iClientId);
                oDocMap.Get(p_sDocClass ,p_sDocType, p_sDocCategory, p_sExt, p_sDocName, ref sIRFolderType, ref sIRDocType, ref sDescription);
                oDocMap.GetUserData(p_iClaimId, null, FUPFileMappings.UserDataType.ColdImport, ref sUD1, ref sUD2, ref sUD3, ref sUD4, ref sUD5, ref sErrorMsg);

                //sDrawerName = GetImageRightConfigValue("IR_Drawer");
                oLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                int iCodeId = DbFactory.ExecuteAsType<int>(m_sConnectionString, "SELECT IR_DRAWER_CODE FROM CLAIM_SUPP WHERE CLAIM_ID=" + p_iClaimId);
                if (iCodeId > 0)
                {
                    sDrawerName = oLocalCache.GetCodeDesc(iCodeId);
                }
                if (string.IsNullOrEmpty(sDrawerName))
                {
                    sDrawerName = GetImageRightConfigValue("IR_Drawer");
                }
                sConvertToTiff = GetImageRightConfigValue("ConvertToTiff");
                sConvertExtension = GetImageRightConfigValue("ConvertExtension");

                switch (sConvertToTiff)
                {
                    case "1":
                        bConversion = true;
                        break;
                    case "2":
                        foreach (string sExtensions in sConvertExtension.Split(','))
                        {
                            if (string.Compare(sExtensions.Trim(), p_sExt, true) == 0)
                            {
                                bConversion = true;
                                break;
                            }
                        }
                        break;
                    case "3":
                        bConversion = true;
                        foreach (string sExtensions in sConvertExtension.Split(','))
                        {
                            if (string.Compare(sExtensions.Trim(), p_sExt, true) == 0)
                            {
                                bConversion = false;
                                break;
                            }
                        }
                        break;
                    default:
                        bConversion = false;
                        break;
                }

                sFileName = string.Format("{0}_{1}_{2}_{3}___{4}_{5}_{6}_{7}_{8}__{9}_______{10}__{11}{12}",
                    new object[] { sDrawerName, p_sClaimNumber, sIRFolderType, sIRDocType,
                        sUD1,sUD2,sUD3,sUD4,sUD5, sDescription, (bConversion? "C" : string.Empty), sDateTime, p_sExt});
                //If any invalid character is present - replace it will empty string
                 sInvalidPathChars = new string(Path.GetInvalidPathChars()) + new string(Path.GetInvalidFileNameChars());
                oRegEx = new Regex(string.Format("[{0}]", Regex.Escape(sInvalidPathChars)));
                sFileName = oRegEx.Replace(sFileName, string.Empty);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (oLocalCache!=null)
                {
                    oLocalCache.Dispose();
                }
            }
            return sFileName;
        }

        //private bool ConvertToPDF(ref string p_sFile, ref string p_sFinalDestinationFile)
        //{
        //    OpenOfficeToPdf oPDFConverter = null;
        //    string[] objApplicableFileExt = new string[] { ".doc", ".docx", ".xls", ".xlsx", ".xlsb", ".xlsm", ".ppt", ".pptx" };
        //    bool bReturnValue = false;
        //    if (objApplicableFileExt.Contains( Path.GetExtension(p_sFile)))
        //    {
        //        try
        //        {
        //            oPDFConverter = new OpenOfficeToPdf();
        //            oPDFConverter.ConvertToPdf(p_sFile, Path.ChangeExtension(p_sFile, ".pdf"));

        //            //if control reaches here - this implies conversion is successful.
        //            //Change extension of the file name.
        //            p_sFile = Path.ChangeExtension(p_sFile, ".pdf");
        //            p_sFinalDestinationFile = Path.ChangeExtension(p_sFinalDestinationFile, ".pdf");
        //            bReturnValue = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            bReturnValue = false;
        //            Log.Write("Failed to convert to PDF. " + ex.ToString(), Log.LOG_CATEGORY_DEFAULT);
        //        }
        //    }

        //    return bReturnValue;
        //}

        public string GetImageRightConfigValue(string p_sKey)
        {
            string sValue = string.Empty;
            NameValueCollection nvCol = RMConfigurationManager.GetNameValueSectionSettings("ImageRight", m_sConnectionString, m_iClientId);
             if (nvCol != null)
             {
                 sValue = nvCol[p_sKey];
             }

             return sValue;
        }

        public virtual bool MoveFile(string P_sSourcePath, string p_sDestinationPath)
        {
            if (p_sDestinationPath.Length > MAX_PATH_Length || Path.GetFileName(p_sDestinationPath).Length > MAX_FILE_Length)
            {
                throw new RMAppException(Globalization.GetString("ImageRightFileHandling.NameTooLong",m_iClientId));
            }
            File.Copy(P_sSourcePath, p_sDestinationPath);
            return true;
        }
    }
}
