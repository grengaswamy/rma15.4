﻿
using System;
using System.Collections; 
using System.Text;
using System.Text.RegularExpressions;
using System.Data; 
using System.Xml;
using Microsoft.VisualBasic;

using C1.Win.C1Spell;
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Settings;
using Riskmaster.Db;
using Riskmaster.DataModel;
using System.IO;

namespace Riskmaster.Application.Editor
{
	
	
    /// <summary>
    /// A class representing the Editor/Memo Field. It exposes various methods for Spell Checking
	/// and saving the fields into database
    /// </summary>
	public class Editor
	{
		
		#region security codes

		private const int RMB_ClaimNotes_GC = 170;
		private const int RMB_ClaimNotes_WC = 3020;
		private const int RMB_ClaimNotes_VA = 6020;
		private const int RMB_ClaimNotes_DI = 60020;
        //Start-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims
        private const int RMB_ClaimNotes_PC = 40020;
        //End-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims
		private const int RMB_EventNotes_Event = 11020;
        private int m_iClientId = 0; //sonali - cloud

		#endregion

		#region Variable Declarations	

		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
        
		internal static UserLogin m_objUserLogin=null;
        /// <summary>
        /// MGaba2: MITS 12510-Private variable to store connection string
        /// </summary>
        private string m_sConnectionString = string.Empty;

        private C1Spell m_objC1Spell = null;

        private string m_sComments = "";
        public string Comments { get { return m_sComments; } set { m_sComments = value; } }

        private string m_sHTMLComments = "";
        public string HTMLComments { get { return m_sHTMLComments; } set { m_sHTMLComments = value; } }
		#endregion

		#region Constructor
		/// Name		: Editor
		/// Author		: Navneet Sota
		/// Date Created: 23/08/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>
		/// Constructor for the class
		/// </summary>
		/// <param name="p_sConnectionstring"> The Connection string for database</param>
        public Editor(UserLogin p_objUserLogin, int p_iClientId) //sonali - cloud
		{
			m_objUserLogin = p_objUserLogin;
            m_objC1Spell = InitiateSpell();
            m_iClientId = p_iClientId;
            
            
		}
		
         /// <summary>
        ///	MGaba2: MITS 12510-Constructor, initializes the user login and Connection String
        /// </summary>
        public Editor(UserLogin p_objUserLogin, string p_sConnectionString, int p_iClientId) // sonali-cloud
        {
            m_objUserLogin = p_objUserLogin;
            m_sConnectionString = p_sConnectionString;
            m_objC1Spell = InitiateSpell();
            m_iClientId = p_iClientId;
        }
		#endregion

        #region SpellChecker
        /// <summary>
        /// Creates an instance of the SpellChecker object
        /// </summary>
        /// <returns>an instance of the SpellChecker object</returns>
        private static C1Spell InitiateSpell()
        {
            C1Spell objC1Spell = new C1Spell();

            string sPath = Path.Combine(RMConfigurator.UserDataPath, "rmxdictionary.txt");
            objC1Spell.MainDictFile = AppDomain.CurrentDomain.BaseDirectory + @"bin\C1SP_AE.dct";

            // ABhateja, 06.27.2007 -START-
            // MITS 9910,create rmxdictionary.txt if it does not exist.
            if (!File.Exists(sPath))
                File.Create(sPath);

            DictionaryItem y = new DictionaryItem();
            y.Checked = true;
            y.Item = sPath;
            objC1Spell.CustomDictFiles = new string[] { y.Item };
            objC1Spell.WhichCustomDict = 1;
            objC1Spell.IsOptionSettingFixed = false;

            return objC1Spell;
        } 
        #endregion

		#region Public Methods

        #region public GetExistingComments (p_sInputText)

        /// Name		: GetExistingComments
        /// Author		: Navneet Sota
        /// Date Created: 27/08/2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for retreiving the Existing comments for specific RecordId which is
        /// stored in a separate table.
        /// </summary>
        /// <returns>XML having existing Comments.</returns>
        /// <param name="p_sFormName">Form Name for which Comments screen is to be opened</param>
        /// <param name="p_iRecordId">RecordId corresponding to which comments needs to be fetched.</param>		
        /// <param name="p_iParentSecurityId">ParentSecurityId</param>
        /// <param name="p_sUserName">The Login Name</param>
        public XmlDocument GetExistingComments(string p_sFormName, int p_iCommentId, int p_iRecordId, int p_iParentSecurityId, string p_sUserName)
        {
            string sLookUpType = string.Empty;
            string sData = string.Empty;
            string sTemp = string.Empty;
            string sCommentId = string.Empty;

            bool bFreeze = false;
            bool bStamp = false;
            bool bNoEditDateTimeStamp = false;
            bool bDateTimeStampEvtLocDesc = false;
            XmlDocument objXMLDOM = null;
            XmlElement objXmlElem = null;
            XmlNode objNode = null;
            int iAdjRowID = 0;
            Riskmaster.DataModel.DataModelFactory objDmf = null;
            DataModelFactory objDmf1 = null;
            Claim objClaim = null;
            string sComments = null, sHTMLComments = null;
            string sSQL = string.Empty;
            //Raman MITS 19155 : Comments added by one user overwrite other users changes if both users had open the same comments window
            string sDttmRcdLastUpd = string.Empty;
            GetSecurityPermissions(p_sFormName);

            try 
            {
                if (p_sFormName == "" || p_iRecordId == 0)
                    throw new RMAppException(Globalization.GetString("Editor.GetExistingComments.InvalidParameters",m_iClientId));//sonali

                objDmf = new DataModelFactory(m_objUserLogin, m_iClientId);
                bFreeze = objDmf.Context.InternalSettings.SysSettings.FreezeTextFlag;
                bStamp = objDmf.Context.InternalSettings.SysSettings.DateStampFlag;
                bNoEditDateTimeStamp = Conversion.ConvertStrToBool(objDmf.Context.InternalSettings.SysSettings.FreezeDateFlag.ToString());
                bDateTimeStampEvtLocDesc = Conversion.ConvertStrToBool(objDmf.Context.InternalSettings.SysSettings.DateEventDesc.ToString());

                switch (p_sFormName.ToLower())
                {
                    case "claim":
                    //Start-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims
                    case "claimpc":
                    //End-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims
                    case "claimwc":
                    case "claimva":
                    case "claimgc":
                    case "claimdi":
                        sLookUpType = "Claim";                        
                        break;
                    case "event":
                        sLookUpType = "Event";
                        break;
                    //nsachdeva2 - MITS: 27077 - 6/5/2012
                    case "litigation":
                        sLookUpType = "Litigation";
                        break;
                    //End MITS: 27077
                    default:
                        break;
                }
                //Raman MITS 19155 : Comments added by one user overwrite other users changes if both users had open the same comments window

                if(sLookUpType =="Claim")
                    sSQL = "SELECT COMMENT_ID,COMMENTS,HTMLCOMMENTS, DTTM_RCD_LAST_UPD FROM COMMENTS_TEXT WHERE ATTACH_TABLE like 'CLAIM' AND ATTACH_RECORDID =" + p_iRecordId.ToString();
                else if (sLookUpType == "Event")
                    sSQL = "SELECT COMMENT_ID,COMMENTS,HTMLCOMMENTS , DTTM_RCD_LAST_UPD FROM COMMENTS_TEXT WHERE ATTACH_TABLE like 'EVENT' AND ATTACH_RECORDID =" + p_iRecordId.ToString();
                //nsachdeva2 - MITS: 27077 - 6/5/2012
                else if (sLookUpType == "Litigation")
                    sSQL = "SELECT COMMENT_ID,COMMENTS,HTMLCOMMENTS , DTTM_RCD_LAST_UPD FROM COMMENTS_TEXT WHERE ATTACH_TABLE like 'CLAIM_X_LITIGATION' AND ATTACH_RECORDID =" + p_iRecordId.ToString();
                //End MITS: 27077
                else
                    throw new RMAppException(Globalization.GetString("Editor.GetExistingComments.GenericError",m_iClientId));//sonali

                using (DbReader objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            sComments = Conversion.ConvertObjToStr(objDbReader.GetValue("COMMENTS"));
                            sHTMLComments = Conversion.ConvertObjToStr(objDbReader.GetValue("HTMLCOMMENTS"));
                            sCommentId = Conversion.ConvertObjToStr(objDbReader.GetValue("COMMENT_ID"));
                            //Raman MITS 19155 : Comments added by one user overwrite other users changes if both users had open the same comments window
                            sDttmRcdLastUpd = Conversion.ConvertObjToStr(objDbReader.GetValue("DTTM_RCD_LAST_UPD"));
                        }
                    }
                }
                  
                objXMLDOM = new XmlDocument();
                objXMLDOM.LoadXml("<Editor/>");

                objNode = objXMLDOM.DocumentElement.SelectSingleNode("//Editor");

                objXmlElem = objXMLDOM.CreateElement("RecordDetails");
                objXmlElem.SetAttribute("FormName", p_sFormName);
                objXmlElem.SetAttribute("RecordId", p_iRecordId.ToString());
                objXmlElem.SetAttribute("ParentSecurityId", p_iParentSecurityId.ToString());
                objXmlElem.SetAttribute("UserName", p_sUserName);

                //raman has added code to fetch UseAdjusterDatedText flag

                bool bUseAdjFlag = false;
                switch (p_sFormName.ToLower())
                {
                    case "claim":
                    //Start-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims
                    case "claimpc":
                    //End-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims
                    case "claimwc":
                    case "claimva":
                    case "claimgc":
                    case "claimdi":
                        objDmf1 = new DataModelFactory(m_objUserLogin, m_iClientId);
                        ColLobSettings objLobSettings = new ColLobSettings(objDmf1.Context.DbConn.ConnectionString,m_iClientId);//psharma206
                        int iLOB = 0;
                        objClaim = (Claim)objDmf1.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(p_iRecordId);
                        iLOB = objClaim.LineOfBusCode;

                        sSQL = "SELECT ADJ_ROW_ID FROM CLAIM_ADJUSTER WHERE CURRENT_ADJ_FLAG <> 0 AND CLAIM_ID = " + p_iRecordId;
                        using (DbReader objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {
                            if (objDbReader.Read())
                            {
                                iAdjRowID = Riskmaster.Common.Conversion.ConvertObjToInt(objDbReader.GetValue("ADJ_ROW_ID"), m_iClientId);
                            }
                        }
                        bUseAdjFlag = objLobSettings[iLOB].UseAdjText;
                        break;
                    default:
                        break;
                }
                objXmlElem.SetAttribute("UseAdjTxt", bUseAdjFlag.ToString());
                objNode.AppendChild(objXmlElem);

                objXmlElem = objXMLDOM.CreateElement("Comments");
                objXmlElem.SetAttribute("bgcolor", "WhiteSmoke");
                string sHeaderText = FetchFormTitle(objClaim);
                if (sLookUpType.ToLower() == "claim")
                {
                    objXmlElem.SetAttribute("Header", "Claim");
                }
                else
                {
                    objXmlElem.SetAttribute("Header", "");
                }
                objXmlElem.SetAttribute("HeaderText", sHeaderText);
                objXmlElem.SetAttribute("FreezeTextFlag", bFreeze.ToString().ToLower());
                objXmlElem.SetAttribute("DateStampFlag", bStamp.ToString().ToLower());
                objXmlElem.SetAttribute("FreezeDateFlag", bNoEditDateTimeStamp.ToString().ToLower());
                objXmlElem.SetAttribute("DateTimeStampEvtLocDesc", bDateTimeStampEvtLocDesc.ToString().ToLower());
                objXmlElem.SetAttribute("AdjusterRowID", iAdjRowID.ToString());

                objXmlElem.InnerText = sComments;   // from session - work "in progress"
                objNode.AppendChild(objXmlElem);

                //Get the HTML or Plain text comments
                objXmlElem = objXMLDOM.CreateElement("HTMLComments");
                if ( string.IsNullOrEmpty(sHTMLComments) )
                {
                    if ( !string.IsNullOrEmpty(sComments))
                        objXmlElem.InnerText = sComments;
                }
                else
                    objXmlElem.InnerText = sHTMLComments;
                
                objNode.AppendChild(objXmlElem);

                //Raman MITS 19155 : Comments added by one user overwrite other users changes if both users had open the same comments window
                objXmlElem = objXMLDOM.CreateElement("DttmRcdLastUpd");
                objXmlElem.InnerText = sDttmRcdLastUpd;
                objNode.AppendChild(objXmlElem);

                objXmlElem = objXMLDOM.CreateElement("CommentId");
                objXmlElem.InnerText = sCommentId;
                objNode.AppendChild(objXmlElem);
                //else
                //    throw new RMAppException(Globalization.GetString("Editor.GetExistingComments.NoDataErr"));
            }//End of Main Try Block
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Editor.GetExistingComments.GenericError", m_iClientId), objException);
            }
            finally
            {
                objDmf.Dispose();
                if (objDmf1 != null)
                    objDmf1.Dispose();
                if (objClaim != null)
                    objClaim.Dispose();

            }
            //Return the result to calling function
            return objXMLDOM;
        }//end function
        #endregion

        #region public GetExistingComments (p_sInputText)

        /// Name		: GetExistingComments
        /// Author		: Navneet Sota
        /// Date Created: 27/08/2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for retreiving the Existing comments for specific RecordId when Comments/HTMLComment
        /// stored in the same table.
        /// </summary>
        /// <returns>XML having existing Comments.</returns>
        /// <param name="p_sFormName">Form Name for which Comments screen is to be opened</param>
        /// <param name="p_iRecordId">RecordId corresponding to which comments needs to be fetched.</param>		
        /// <param name="p_iParentSecurityId">ParentSecurityId</param>
        /// <param name="p_sUserName">The Login Name</param>
        public XmlDocument GetExistingComments(string p_sFormName, int p_iRecordId, int p_iParentSecurityId, string p_sUserName,string p_sComments,string p_sHTMLComments)
        {
            string sLookUpType = string.Empty;
            string sData = string.Empty;
            string sTemp = string.Empty;

            bool bFreeze = false;
            bool bStamp = false;
            bool bNoEditDateTimeStamp = false;
            bool bDateTimeStampEvtLocDesc = false;
            XmlDocument objXMLDOM = null;
            XmlElement objXmlElem = null;
            XmlNode objNode = null;
            int iAdjRowID = 0;
            int iUseEnhPol = 0;   // csingh7 for MITS 20092 ,19776
            Riskmaster.DataModel.DataModelFactory objDmf = null;
            Riskmaster.DataModel.DataObject objDataObject = null;
            DataModelFactory objDmf1 = null;
            Claim objClaim = null;
            Object oProperty = null;
            string sComments = null, sHTMLComments = null;

            string sSQL = string.Empty;

            GetSecurityPermissions(p_sFormName);

            try
            {
                if (p_sFormName == "" || p_iRecordId == 0)
                    throw new RMAppException(Globalization.GetString("Editor.GetExistingComments.InvalidParameters", m_iClientId));

                objDmf = new DataModelFactory(m_objUserLogin, m_iClientId);
                bFreeze = objDmf.Context.InternalSettings.SysSettings.FreezeTextFlag;
                bStamp = objDmf.Context.InternalSettings.SysSettings.DateStampFlag;
                bNoEditDateTimeStamp = Conversion.ConvertStrToBool(objDmf.Context.InternalSettings.SysSettings.FreezeDateFlag.ToString());
                bDateTimeStampEvtLocDesc = Conversion.ConvertStrToBool(objDmf.Context.InternalSettings.SysSettings.DateEventDesc.ToString());
                iUseEnhPol = objDmf.Context.InternalSettings.SysSettings.UseEnhPolFlag;  // csingh7 for MITS 20092 , 19776
                switch (p_sFormName.ToLower())
                {
                    case "claim":
                    //Start-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims
                    case "claimpc":
                    //End-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims
                    case "claimwc":
                    case "claimva":
                    case "claimgc":
                    case "claimdi":
                        sLookUpType = "Claim";
                        break;
                    case "event":
                        sLookUpType = "Event";
                        break;
                    case "funds":
                        sLookUpType = "Funds";
                        break;
                    case "piemployee":
                        sLookUpType = "PiEmployee";
                        break;
                    case "piother":
                        sLookUpType = "PiOther";
                        break;
                    case "piwitness":
                        sLookUpType = "PiWitness";
                        break;
                    case "litigation":
                        sLookUpType = "ClaimXLitigation";
                        break;
                    case "subrogation":
                        sLookUpType = "ClaimXSubrogation";
                        break;
                    case "demandoffer":
                        sLookUpType = "DemandOffer";
                        break;
                    case "claimant":
                        sLookUpType = "Claimant";
                        break;
                    case "defendant":
                        sLookUpType = "Defendant";
                        break;
                    case "policy":
                        sLookUpType = "Policy";
                        break;
                    case "policyenh":       //csingh7 MITS 20092
                    case "policyenhal": //csingh7 - Vaco Merge : Implementing Comments for ENhanced Policy ( all 4 LOBs )   
                    case "policyenhgl": 
                    case "policyenhpc":
                    case "policyenhwc":
                        sLookUpType = "PolicyEnh";
                        break;
                    case "people":
                    case "employee":
                    case "entitymaint":
                        sLookUpType = "Entity";
                        break;
                    case "plan":
                        sLookUpType = "DisabilityPlan";
                        break;
                    //Geeta : FMLA Enhancement
                    case "leaveplan":
                        sLookUpType = "LeavePlan";
                        break;
                    case "lookupclaimpolicy":       //csingh7 for MITS 20092
                        if (iUseEnhPol == -1) 
                            sLookUpType = "PolicyEnh";
                        else
                            sLookUpType = "Policy";
                        break;
                    case "supervisorapproval":   //csingh7 for MITS 20092
                        sLookUpType = "Funds";
                        break;
                    //Start averma62  MITS 28988
                    case "casemanagement":  
                        sLookUpType = "CaseManagement";
                        break;
                    //End averma62  MITS 28988
                    //Amandeep MITS 29298 --start
                    case "arbitration":
                        sLookUpType = "ClaimXArbitration";
                        break;
                    //Amandeep MITS 29298 --end
                    case "piinjury":
                        sLookUpType = "PiEmployee";
                        break;
                    //Aman Driver Enh MITS 29866
                    case "pidriver":
                        sLookUpType = "PiDriver";
                        break;
                    default:
                        break;
                }
                
                if (!string.IsNullOrEmpty(p_sComments))
                {
                    sComments = p_sComments;
                }
                if (!string.IsNullOrEmpty(p_sHTMLComments))
                {
                    sHTMLComments = p_sHTMLComments;
                }
                if (string.IsNullOrEmpty(sComments))
                {
                    objDataObject = (Riskmaster.DataModel.DataObject)objDmf.GetDataModelObject(sLookUpType, false);
                    objDataObject.MoveTo(p_iRecordId);
                }

                objXMLDOM = new XmlDocument();
                objXMLDOM.LoadXml("<Editor/>");

                objNode = objXMLDOM.DocumentElement.SelectSingleNode("//Editor");

                objXmlElem = objXMLDOM.CreateElement("RecordDetails");
                objXmlElem.SetAttribute("FormName", p_sFormName);
                objXmlElem.SetAttribute("RecordId", p_iRecordId.ToString());
                objXmlElem.SetAttribute("ParentSecurityId", p_iParentSecurityId.ToString());
                objXmlElem.SetAttribute("UserName", p_sUserName);

                //raman has added code to fetch UseAdjusterDatedText flag
                bool bUseAdjFlag = false;
                switch (p_sFormName.ToLower())
                {
                    case "claim":
                    //Start-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims
                    case "claimpc":
                    //End-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims
                    case "claimwc":
                    case "claimva":
                    case "claimgc":
                    case "claimdi":
                        objDmf1 = new DataModelFactory(m_objUserLogin, m_iClientId);
                        ColLobSettings objLobSettings = new ColLobSettings(objDmf1.Context.DbConn.ConnectionString,m_iClientId);//psharma206
                        int iLOB = 0;
                        objClaim = (Claim)objDmf1.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(p_iRecordId);
                        iLOB = objClaim.LineOfBusCode;

                        sSQL = "SELECT ADJ_ROW_ID FROM CLAIM_ADJUSTER WHERE CURRENT_ADJ_FLAG <> 0 AND CLAIM_ID = " + p_iRecordId;
                        using (DbReader objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {
                            if (objDbReader.Read())
                            {
                                iAdjRowID = Riskmaster.Common.Conversion.ConvertObjToInt(objDbReader.GetValue("ADJ_ROW_ID"), m_iClientId);
                            }
                        }
                        bUseAdjFlag = objLobSettings[iLOB].UseAdjText;
                        break;
                    default:
                        break;
                }
                objXmlElem.SetAttribute("UseAdjTxt", bUseAdjFlag.ToString());
                objNode.AppendChild(objXmlElem);

                objXmlElem = objXMLDOM.CreateElement("Comments");
                objXmlElem.SetAttribute("bgcolor", "WhiteSmoke");
                string sHeaderText = FetchFormTitle(objClaim);
                if (sLookUpType.ToLower() == "claim")
                {
                    objXmlElem.SetAttribute("Header", "Claim");
                }
                else
                {
                    objXmlElem.SetAttribute("Header", "");
                }
                objXmlElem.SetAttribute("HeaderText", sHeaderText);
                objXmlElem.SetAttribute("FreezeTextFlag", bFreeze.ToString().ToLower());
                objXmlElem.SetAttribute("DateStampFlag", bStamp.ToString().ToLower());
                objXmlElem.SetAttribute("FreezeDateFlag", bNoEditDateTimeStamp.ToString().ToLower());
                objXmlElem.SetAttribute("DateTimeStampEvtLocDesc", bDateTimeStampEvtLocDesc.ToString().ToLower());
                objXmlElem.SetAttribute("AdjusterRowID", iAdjRowID.ToString());
                if (string.IsNullOrEmpty(sComments))
                {
				    oProperty = objDataObject.GetProperty("Comments");
            	    if (oProperty != null)
                	    sComments = oProperty.ToString();
			    }
                objXmlElem.InnerText = sComments;
                objNode.AppendChild(objXmlElem);

			    if (string.IsNullOrEmpty(sHTMLComments))
                {
				    oProperty = objDataObject.GetProperty("HTMLComments");
            	    if (oProperty != null)
                	    sHTMLComments = oProperty.ToString();
			    }
                //Get the HTML or Plain text comments. If HTMLComments is blank, try to get Comments
                objXmlElem = objXMLDOM.CreateElement("HTMLComments");
                if (string.IsNullOrEmpty(sHTMLComments))
                {
                    if (!string.IsNullOrEmpty(sComments))
                        objXmlElem.InnerText = sComments;
                }
                else
                {
                    objXmlElem.InnerText = sHTMLComments;
                }

            	objNode.AppendChild(objXmlElem);
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Editor.GetExistingComments.GenericError", m_iClientId), objException);
            }
            finally
            {
                objDmf.Dispose();
                if (objDmf1 != null)
                    objDmf1.Dispose();
                if (objClaim != null)
                    objClaim.Dispose();

            }

            return objXMLDOM;
        }
        #endregion

		#region public GetMemoAttributes (p_sInputText)

		/// Name		: GetMemoAttributes
		/// Author		: Navneet Sota
		/// Date Created: 27/08/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// This method is basically used for retreiving the Existing comments for specific RecordId.
		/// </summary>
		/// <returns>XML having existing Comments.</returns>
		/// <param name="p_sUserName">The Login Name</param>
		public XmlDocument GetMemoAttributes(string p_sUserName,string p_sSupplementalField)
		{
			bool bFreeze = false;
			bool bStamp = false;
			bool bNoEditDateTimeStamp = false;
			bool bDateTimeStampEvtLocDesc = false;
			bool bFreezeEventDesc = false;
			bool bFreezeEventLocDesc = false;
            bool bFreezePayOrder = false;//jramkumar for MITS 32095

			XmlDocument objXMLDOM = null;
			XmlElement objXmlElem = null;
			XmlNode objNode = null;
			
			Riskmaster.DataModel.DataModelFactory objDmf = null;

			try //Main Try Block
			{
				objDmf = new DataModelFactory(m_objUserLogin,m_iClientId); //sonali-cloud
                if(string.Compare(p_sSupplementalField,"supplementalfield",true)== 0)
                {
                    bFreeze = objDmf.Context.InternalSettings.SysSettings.FreezeSuppText;
                }
                else
                {
                    bFreeze = objDmf.Context.InternalSettings.SysSettings.FreezeTextFlag;
                }
				bStamp = objDmf.Context.InternalSettings.SysSettings.DateStampFlag;
				bFreezeEventDesc = Conversion.ConvertStrToBool(objDmf.Context.InternalSettings.SysSettings.FreezeEventDesc.ToString());
				bFreezeEventLocDesc = Conversion.ConvertStrToBool(objDmf.Context.InternalSettings.SysSettings.FreezeLocDesc.ToString());
				bNoEditDateTimeStamp = Conversion.ConvertStrToBool(objDmf.Context.InternalSettings.SysSettings.FreezeDateFlag.ToString());
				bDateTimeStampEvtLocDesc=Conversion.ConvertStrToBool(objDmf.Context.InternalSettings.SysSettings.DateEventDesc.ToString());
                bFreezePayOrder = objDmf.Context.InternalSettings.SysSettings.FreezePayOrderText;//jramkumar for MITS 32095
				objXMLDOM = new XmlDocument();
				objXMLDOM.LoadXml("<Editor/>");
			
				objNode = objXMLDOM.DocumentElement.SelectSingleNode("//Editor");

				objXmlElem = objXMLDOM.CreateElement("RecordDetails");
				objXmlElem.SetAttribute("UserName", p_sUserName);

				objNode.AppendChild(objXmlElem);

				objXmlElem = objXMLDOM.CreateElement("Comments");
				objXmlElem.SetAttribute("FreezeTextFlag",bFreeze.ToString().ToLower());
				objXmlElem.SetAttribute("DateStampFlag",bStamp.ToString().ToLower());
				objXmlElem.SetAttribute("FreezeDateFlag",bNoEditDateTimeStamp.ToString().ToLower());
                objXmlElem.SetAttribute("DateTimeStampHTMLText", objDmf.Context.InternalSettings.SysSettings.DateStampHTMLText.ToString().ToLower());//asharma326 JIRA 6422 for HTML text
				objXmlElem.SetAttribute("DateTimeStampEvtLocDesc",bDateTimeStampEvtLocDesc.ToString().ToLower());	
				objXmlElem.SetAttribute("FreezeEventDesc", bFreezeEventDesc.ToString().ToLower());
				objXmlElem.SetAttribute("FreezeEventLocDesc", bFreezeEventLocDesc.ToString().ToLower());
                objXmlElem.SetAttribute("FreezePayOrder", bFreezePayOrder.ToString().ToLower());//jramkumar for MITS 32095

				objNode.AppendChild(objXmlElem);

				objXmlElem = objXMLDOM.CreateElement("HTMLComments");
				objNode.AppendChild(objXmlElem);

			}//End of Main Try Block
			catch (Exception objException)
			{
				throw new RMAppException(Globalization.GetString("Editor.GetMemoAttributes.GenericError", m_iClientId) ,objException);
			}
			finally
			{
				objDmf.Dispose();
			}

			//Return the result to calling function
			return objXMLDOM;
		}//end function
		#endregion

		/// <summary>
		/// Adds the specified word to the dictionary
		/// </summary>
		/// <param name="passedWord"></param>
		/// <returns></returns>
		public bool AddToDictionary(string passedWord)
		{
			bool retVal = false;
			try
			{
				if (m_objC1Spell.WhichCustomDict !=1)
				{
					throw new RMAppException(Globalization.GetString("Editor.GetExistingComments.CustomDicNotSet", m_iClientId));
				}
				retVal = m_objC1Spell.AddToCustomFile(passedWord);					
			}
			catch(Exception ex)
			{
				throw;	
			}				
			
			return retVal;			
		}
		#region public GetSpellCheckDone (p_sInputText)

		/// Name		: GetSpellCheckDone
		/// Author		: Navneet Sota
		/// Date Created: 23/08/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// This method is basically used for checking the spelling of the passed in string.
		/// </summary>
		/// <returns>XML having all the bad words along with corresponding suggestions.</returns>
		/// <param name="p_sInputText">The input string to be checked for spellings</param>
		/// <param name="p_sHTMLText">The Original Text in HTML format</param>
		public string GetSpellCheckDone(string p_sInputText, string p_sHTMLText)
		{
			//String Variables
			string sdelimStr = "";
			//Array arrInputString = null;
			char [] delimiter = null;
			
			//Integer Variables
			int iCnt = 0; // Variable used in For Loops
			int iTemp = 0;// Variable used in ForEach Loops
			int iWordIndex = 0;		//bad word index
			string sWordChecked = string.Empty;

			//If it's in a year format, don't treat it as bad spelt word
			Regex oDateRegex = new Regex(@"\d{1,2}/\d{1,2}/\d{2,4}");
			Regex oWordStartRegex = new Regex(@"^\W+");
			Regex oWordEndRegex = new Regex(@"\W+$");

			//XML Object variables
			XmlDocument objXmlDoc = null;
			XmlElement objXmlElem = null;
			XmlNode objNode = null;

			XmlNode objSuggestionNode = null;
			XmlElement objXMLSuggestionElem = null;

			BadWordDialogEnum objEnum;
			try //Main Try Block
			{

				delimiter = sdelimStr.ToCharArray();										
				objEnum = new BadWordDialogEnum();
				//Start XML tree
				objXmlDoc = new XmlDocument();
				objXmlDoc.LoadXml("<SpellCheck><OriginalPlainText/><OriginalHTMLText/><BadWord/></SpellCheck>");
				//Special characters				
				p_sHTMLText = ReplaceSpecialCharacter(p_sHTMLText);
				//Set the Passed in Input String as Original Text.
				objNode = objXmlDoc.DocumentElement.SelectSingleNode("//OriginalPlainText");
				objNode.InnerText = ReplaceSpecialCharacter(p_sInputText);

				objNode = objXmlDoc.DocumentElement.SelectSingleNode("//OriginalHTMLText");
				objNode.InnerText = p_sHTMLText;

				string[] arrInputString = p_sInputText.Split(delimiter);

                // ABhateja, 06.28.2007
                // MITS 9929, Remove the specified char from the SeparateChars array.
                m_objC1Spell.RemoveCharFromSeparateArr('-');

                for(iCnt=0; iCnt<=arrInputString.Length-1; iCnt++)
				{
					//remove (,",',& from the begin/end of the word
					sWordChecked = arrInputString.GetValue(iCnt).ToString();
					sWordChecked = oWordStartRegex.Replace(sWordChecked, "");
					sWordChecked = oWordEndRegex.Replace(sWordChecked, "");

                    sdelimStr = m_objC1Spell.CheckHTMLString(sWordChecked, objEnum);

					//If it's a bad spelt word and it's a date format
					if( (m_objC1Spell.BadWordCount > 0) && (oDateRegex.IsMatch(sdelimStr) == false))
					{
						objNode = objXmlDoc.DocumentElement.SelectSingleNode("//BadWord");
						objXmlElem = objXmlDoc.CreateElement("word_" + iWordIndex);
						sdelimStr = ReplaceSpecialCharacter(sdelimStr);
						objXmlElem.SetAttribute("value", sdelimStr);
						objXmlElem.SetAttribute("index", iWordIndex.ToString());
						
						objNode.AppendChild(objXmlElem);
						iTemp = 0;
						foreach(string sItem in m_objC1Spell.Suggestions)
						{
							iTemp++;
							objSuggestionNode = objNode.SelectSingleNode("//word_" + iWordIndex);
							objXMLSuggestionElem = objXmlDoc.CreateElement("Suggestion");
							objXMLSuggestionElem.InnerText = ReplaceSpecialCharacter(sItem);
							objSuggestionNode.AppendChild(objXMLSuggestionElem);
						}
						iWordIndex ++;
					}	
				}


			}//End of Main Try Block
			catch (Exception objException)
			{
				throw new RMAppException(Globalization.GetString("Editor.GetSpellCheckDone.GenericError", m_iClientId) ,objException);
			}
			
			//Return the result to calling function
			return objXmlDoc.OuterXml;
		}//end function
		#endregion
		
		/// <summary>
		/// returns html eqv of special characters in a string
		/// </summary>
		/// <param name="inputText">passed string</param>
		/// <returns>encoded string</returns>
		private string ReplaceSpecialCharacter(string inputText)
		{
			//please do corresponding changes in spellcjheck.js file
			// change encode and decode special characters function there
			// as well We need to cater to other characters as well
			//3/23/06
			
			return inputText
							.Replace("\n","&#10;")
							.Replace("\r","&#13;")
							.Replace("'","&#39;");
		}

		/// <summary>
		/// Getting security Permissions
		/// </summary>
		/// <param name="p_FormName">Form Name</param>
		/// <returns></returns>
		private void GetSecurityPermissions(string p_FormName)
		{
			switch (p_FormName.ToLower())
			{
				case "claimgc":
					if(!m_objUserLogin.IsAllowedEx(RMB_ClaimNotes_GC))
						throw new PermissionViolationException(RMPermissions.RMO_ClaimNotes_Access_GC,RMB_ClaimNotes_GC);
					break;
				case "claimwc":
					if(!m_objUserLogin.IsAllowedEx(RMB_ClaimNotes_WC))
						throw new PermissionViolationException(RMPermissions.RMO_ClaimNotes_Access_WC,RMB_ClaimNotes_WC);
					break;
				case "claimva":
					if(!m_objUserLogin.IsAllowedEx(RMB_ClaimNotes_VA))
						throw new PermissionViolationException(RMPermissions.RMO_ClaimNotes_Access_VA,RMB_ClaimNotes_VA);
					break;					
				case "claimdi":
					if(!m_objUserLogin.IsAllowedEx(RMB_ClaimNotes_DI))
						throw new PermissionViolationException(RMPermissions.RMO_ClaimNotes_Access_DI,RMB_ClaimNotes_DI);
					break;
                //Start-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims
                case "claimpc":
                    if (!m_objUserLogin.IsAllowedEx(RMB_ClaimNotes_PC))
                    {
                        throw new PermissionViolationException(RMPermissions.RMO_ClaimNotes_Access_PC, RMB_ClaimNotes_PC);
                    }
                    break;
                //End-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims                        
				case "event":
					if(!m_objUserLogin.IsAllowedEx(RMB_EventNotes_Event))
						throw new PermissionViolationException(RMPermissions.RMO_EventNotes_Access_Event,RMB_EventNotes_Event);
					break;
				default:
					break;
			}

		}

		/// <summary>
		/// Decode the fckeditor returned text
		/// </summary>
		/// <param name="sMemo"></param>
		/// <returns></returns>
		private string DecodeFckEditorText(string sMemo)
		{
			sMemo = sMemo.Replace("&amp;", "&") ;
			sMemo = sMemo.Replace("&quot;", "\"") ;
			sMemo = sMemo.Replace("&lt;", "<") ;
			sMemo = sMemo.Replace("&gt;", ">") ;
	
			return sMemo;
		}

        #region public Save (p_sInputText)

        /// Name		: SaveComments 
        /// Author		: Charanpreet
        /// Date Created: 27/08/2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for saving Comments for specific RecordId for Claim/Event.
        /// </summary>
        /// <returns>true/false</returns>
        /// <param name="p_objXmlNode">XML containg all the required information</param>
        //nsachdeva2 - 02/22/2012 - MITS: 27610
        //public bool Save(XmlNode p_objXmlNode)
        public bool Save(XmlNode p_objXmlNode, string p_sFormName)
        {
            string sFormName = "";
            string sLookUpType = string.Empty;
            string sData = string.Empty;
            string sDataHTML = string.Empty;
            string sPlainData = string.Empty;
            string sHTMLData = string.Empty;
            string sTemp = string.Empty;
            //Shruti for MITS 10364
            string sMoved = string.Empty;

            int iRecordId = 0;
            bool bReturnValue = false;

            //Shruti for 10364
            Riskmaster.DataModel.DataModelFactory objDmf = null;
            Riskmaster.DataModel.DataObject objDataObject = null;
            GetSecurityPermissions(p_sFormName);    //nsachdeva2 - 02/22/2012 - MITS: 27610
            try //Main Try Block
            {
                sFormName = p_objXmlNode.SelectSingleNode("//FormName").InnerText;
                //Shruti for 10364
                sMoved = p_objXmlNode.SelectSingleNode("//Moved").InnerText;
                iRecordId = Conversion.ConvertStrToInteger(p_objXmlNode.SelectSingleNode("//RecordId").InnerText);

                if (sFormName == "" || iRecordId == 0)
                    throw new RMAppException(Globalization.GetString("Editor.SaveComments.InvalidParameters", m_iClientId));

                // objDmf = new DataModelFactory(m_objUserLogin);
                switch (sFormName.ToLower())
                {
                    case "claim":
                    //Start-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims
                    case "claimpc":
                    //End-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims                        
                    case "claimwc":
                    case "claimva":
                    case "claimgc":
                    case "claimdi":
                        sLookUpType = "Claim";
                        //TO DO Complete this part, as per notes.asp
                        break;
                    case "event":
                        sLookUpType = "Event";
                        break;
                    //nsachdeva2 - MITS: 27077 - 6/5/2012
                    case "litigation":
                        sLookUpType = "Litigation";
                        break;
                        //End MITS: 27077
                    default:
                        break;
                }
                //*****Step (1)******
                //Get Existing Comments
                sData = p_objXmlNode.SelectSingleNode("//Comments").InnerText;
                sDataHTML = p_objXmlNode.SelectSingleNode("//HTMLComments").InnerText;

                //Add DateTime Stamp
                //XHU MITS 6753 The comments saved to comments column should not include tag <br/>
                if (sData.Length > 0)
                    sData += Environment.NewLine + Environment.NewLine;
                sData += p_objXmlNode.SelectSingleNode("//DateTimeStamp").InnerText;
                if (sDataHTML.Length > 0)
                    sDataHTML += "<br/><br/>";

                sDataHTML += p_objXmlNode.SelectSingleNode("//DateTimeStamp").InnerText;

                //*****Step (2)******
                //Append newly added Plain text comments
                sPlainData = sData;
                sPlainData += p_objXmlNode.SelectSingleNode("//PlainText").InnerText;

                //Append newly added HTML comments
                sHTMLData = sDataHTML;
                sHTMLData += p_objXmlNode.SelectSingleNode("//EditorTextHtml").InnerText;

                //Raman MITS 19155 : Comments added by one user overwrite other users changes if both users had open the same comments window
                string sDttmRcdLastUpd = p_objXmlNode.SelectSingleNode("//DttmRcdLastUpd").InnerText;

                //Set the Plain Text Comments.
                sPlainData = DecodeFckEditorText(sPlainData);

                objDmf = new DataModelFactory(m_objUserLogin,m_iClientId);

                Comment objComment = null;
                int iCommentId = 0;

                objComment = (Comment)objDmf.GetDataModelObject("Comment", false);
                iCommentId = Conversion.ConvertStrToInteger(p_objXmlNode.SelectSingleNode("//CommentId").InnerText);
                if (iCommentId > 0)
                {
                    objComment.MoveTo(iCommentId);
                    //Raman MITS 19155 : Comments added by one user overwrite other users changes if both users had open the same comments window
                    if (String.Compare(objComment.DttmRcdLastUpd, sDttmRcdLastUpd) != 0)
                    {
                        throw new Riskmaster.ExceptionTypes.DataModelException(String.Format(Globalization.GetString("DataModelObject.PopulateObject.Exception.RecordAlreadyChanged", m_iClientId), "Comments" + "." + "RecordId", iRecordId.ToString()));
                    }
                }
                else
                    iCommentId = 0;

                objComment.Comments = sPlainData;
                objComment.HTMLComments = sHTMLData;
                objComment.AttachRecordId = iRecordId;
                if (sLookUpType == "Claim")
                    objComment.AttachTable = "CLAIM";
                else if (sLookUpType == "Event")
                    objComment.AttachTable = "EVENT";
                //nsachdeva2 - MITS: 27077 - 6/5/2012
                else if (sLookUpType == "Litigation")
                    objComment.AttachTable = "CLAIM_X_LITIGATION";
                    //End MITS: 27077
                else
                    throw new RMAppException(Globalization.GetString("Editor.SaveComments.GenericError", m_iClientId));
                objComment.Save();
                bReturnValue = true;
                //else
                //{
                //    // Save data to session - FDA will pick this up when the parent form is saved
                //    busAdaptor.setCachedObject(sFormName.ToLower() + "|" + iRecordId.ToString() + "|Comments", sPlainData);
                //    busAdaptor.setCachedObject(sFormName.ToLower() + "|" + iRecordId.ToString() + "|HTMLComments", sHTMLData);
                //    bReturnValue = true;
                //}

            }//End of Main Try Block
            catch (DataModelException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Editor.SaveComments.GenericError", m_iClientId), objException);
            }
            finally
            {
                if (objDmf != null)
                    objDmf.Dispose();
                if (objDataObject != null)
                    objDataObject.Dispose();
            }

            //Return the result to calling function
            return bReturnValue;
        }//end function
        #endregion

        #region public SaveComments (p_sInputText)

		/// Name		: SaveComments 
		/// Author		: Navneet Sota
		/// Date Created: 27/08/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// This method is basically used for saving Comments for specific RecordId which is
		/// Not a Claim/Event.
		/// </summary>
		/// <returns>true/false</returns>
		/// <param name="p_objXmlNode">XML containg all the required information</param>
        public bool SaveComments(XmlNode p_objXmlNode)
		{
			string sFormName = "";
			string sLookUpType = string.Empty;
			string sData =  string.Empty;
			string sDataHTML =  string.Empty;
			string sPlainData =  string.Empty;
			string sHTMLData =  string.Empty;
			string sTemp = string.Empty;
            //Shruti for MITS 10364
            string sMoved = string.Empty;

			int iRecordId = 0;
			bool bReturnValue = false;
            
			//Shruti for 10364
			Riskmaster.DataModel.DataModelFactory objDmf = null;
			Riskmaster.DataModel.DataObject objDataObject = null;			

			try //Main Try Block
			{
				sFormName = p_objXmlNode.SelectSingleNode("//FormName").InnerText;
                //Shruti for 10364
                sMoved = p_objXmlNode.SelectSingleNode("//Moved").InnerText;
				iRecordId = Conversion.ConvertStrToInteger(p_objXmlNode.SelectSingleNode("//RecordId").InnerText);

				if(sFormName == "" || iRecordId == 0)
                    throw new RMAppException(Globalization.GetString("Editor.SaveComments.InvalidParameters", m_iClientId)); // sonali - cloud

				// objDmf = new DataModelFactory(m_objUserLogin);
				switch (sFormName.ToLower())
				{
					case "claim":
                    //Start-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims
                    case "claimpc":
                    //End-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims                        
					case "claimwc":
					case "claimva":
					case "claimgc":
					case "claimdi":
						sLookUpType = "Claim";
						break;
					case "event":
						sLookUpType = "Event";
						break;
					case "funds":
                    case "supervisorapproval"://saurabh singh MITS 26643 Date 11/17/2011
						sLookUpType = "Funds";
						break;
					case "piemployee":
						sLookUpType = "PiEmployee";
						break;
					case "piother":
						sLookUpType = "PiOther";
						break;
					case "piwitness":
						sLookUpType = "PiWitness";
						break;
                    case "litigation":
                        sLookUpType = "ClaimXLitigation";
                        break;
                    case "subrogation":
                        sLookUpType = "ClaimXSubrogation";
                        break;
                    case "demandoffer":
                        sLookUpType = "DemandOffer";
                        break;
					case "claimant":
						sLookUpType = "Claimant";
						break;
					case "defendant":
						sLookUpType = "Defendant";
						break;
					case "policy":
						sLookUpType = "Policy";
						break;
                    case "policyenh":       //csingh7 MITS 20092
                    case "policyenhal": //csingh7 - Vaco Merge : Implementing Comments for ENhanced Policy ( all 4 LOBs ) 
                    case "policyenhgl": 
                    case "policyenhpc": 
                    case "policyenhwc": 
                        sLookUpType = "PolicyEnh";
                        break;
					case "people":
					case "employee":
					case "entitymaint":
						sLookUpType = "Entity";
						break;
					case "plan":
						sLookUpType = "DisabilityPlan";
						break;
					//Geeta : FMLA Enhancement
					case "leaveplan":
						sLookUpType = "LeavePlan";
						break;
                    //Start averma62  MITS 28988
                    case "casemanagement":   
                        sLookUpType = "CaseManagement";
                        break;
                    //End averma62  MITS 28988
                    //Amandeep MITS 29298 --start
                    case "arbitration":
                        sLookUpType = "ClaimXArbitration";
                        break;
                    //Amandeep MITS 29298 --end
                    case "piinjury":
                        sLookUpType = "PiEmployee";
                        break;
                    //Aman Driver Enh MITS 29866
                    case "pidriver":
                        sLookUpType = "PiDriver";
                        break;
                    //Aman Driver Enh MITS 29866
					default:
						break;
				}

				sData = p_objXmlNode.SelectSingleNode("//Comments").InnerText;
				sDataHTML = p_objXmlNode.SelectSingleNode("//HTMLComments").InnerText;
                

				//Add DateTime Stamp
				//XHU MITS 6753 The comments saved to comments column should not include tag <br/>
				if( sData.Length > 0 )
					sData += Environment.NewLine + Environment.NewLine;
				sData += p_objXmlNode.SelectSingleNode("//DateTimeStamp").InnerText;
				if( sDataHTML.Length > 0 )
					sDataHTML += "<br/><br/>";
                //gagnihotri Fix was made for FCKeditor
                ////gagnihotri MITS 12278 Altered, to differentiate it from RMWorld <br/>
                //sDataHTML = sDataHTML.Replace("<br/>", "<br />");
				sDataHTML += p_objXmlNode.SelectSingleNode("//DateTimeStamp").InnerText;

				//*****Step (2)******
				//Append newly added Plain text comments
				sPlainData = sData;
				sPlainData += p_objXmlNode.SelectSingleNode("//PlainText").InnerText;
                //Manish Jain MITS 27260
                Comments = sPlainData;
				//Append newly added HTML comments
				sHTMLData = sDataHTML;
				sHTMLData += p_objXmlNode.SelectSingleNode("//EditorTextHtml").InnerText;
                //Manish Jain MITS 27260
                HTMLComments = sHTMLData;
				//Set the Plain Text Comments.
				sPlainData = DecodeFckEditorText(sPlainData);

                /* //Shruti for MITS 10364
                if ((sMoved == "true") || sLookUpType == "Claim" || sLookUpType == "Event")
                {*/
                    objDmf = new DataModelFactory(m_objUserLogin, m_iClientId);

                        objDataObject = (Riskmaster.DataModel.DataObject)objDmf.GetDataModelObject(sLookUpType, false);
                        objDataObject.MoveTo(iRecordId);

                        objDataObject.SetProperty("Comments", sPlainData);
                        objDataObject.SetProperty("HTMLComments", sHTMLData);

                        objDataObject.Save();
                        //gagnihotri MITS 12977
                        bReturnValue = true;
                /*}
                else
                {
                    m_sComments = sPlainData;
                    m_sHTMLComments = sHTMLData;
                    bReturnValue = true;
                }*/

			}//End of Main Try Block
			catch (Exception objException)
			{
                throw new RMAppException(Globalization.GetString("Editor.SaveComments.GenericError", m_iClientId), objException); // sonali - cloud
			}
			finally
			{
				if(objDmf != null)
                    objDmf.Dispose();
				if(objDataObject != null)
                    objDataObject.Dispose();
			}

			//Return the result to calling function
			return bReturnValue;
		}//end function
		#endregion



		// Start - Naresh MITS 7723 08/11/2006
		/// <summary>
		/// Fetches the Form Title of the Claim Screen
		/// </summary>
		/// <param name="objClaim"> The Claim Object corresponding to which the Screen Name has to be found</param>
		/// <returns> The Screen Name</returns>
		private string FetchFormTitle(Claim objClaim)
		{
			string sCaption="";
			int captionLevel=0;
			string sTmp="";
			LobSettings objLobSettings = null;
			try
			{
				objLobSettings = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode];

				Event objEvent = (objClaim.Parent as Event);
				//Handle User Specific Caption Level
				if(!objClaim.IsNew || objClaim.PrimaryClaimant.ClaimantRowId!=0)
				{
					if(objLobSettings.CaptionLevel!=0)
						captionLevel = objLobSettings.CaptionLevel;
					else
						captionLevel = 1006;
					if(captionLevel <1006 || captionLevel >1012)
						captionLevel = 0;

					if(captionLevel !=1012)
						sCaption = objClaim.Context.InternalSettings.CacheFunctions.GetOrgParent(objEvent.DeptEid,captionLevel,0,ref captionLevel);//captionLevel discarded
					else
					{
						objClaim.Context.InternalSettings.CacheFunctions.GetOrgInfo(objEvent.DeptEid,ref sCaption,ref sTmp);
						sCaption += sTmp; 
					}
					sCaption = sCaption.Substring(0, Math.Min(sCaption.Length, 25));
			
					if(objClaim.EventId!=0 && objClaim.PrimaryClaimant!=null && objClaim.PrimaryClaimant.ClaimantEntity!=null)
						sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * " +	
							objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName() + "]";
					else
						sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * - ]";
				}
				//Pass this subtitle value to view (ref'ed from @valuepath).
				if(objClaim.IsNew)
					sCaption="";
			} // try

			catch
			{
				sCaption = "";
			} // catch
			finally
			{
				objLobSettings = null;
				
			} // finally

			
			return sCaption;
		}


		// End - Naresh MITS 7723 08/11/2006
		#endregion		


        #region public GetComments (p_objXmlIn)
        /// <summary>
        /// Author - Sumit Kumar
        /// Date - 10/12/2009
        /// This function will fetch the comments from the POLICY_X_TRANS_ENH table
        /// MITS - 18251
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <returns></returns>
        public XmlDocument GetComments(XmlDocument p_objXmlIn)
        {
            XmlDocument objXMLDOM = null;
            XmlElement objXmlElem = null;
            XmlNode objNode = null;

            string sSQL = string.Empty;
            string sComments = string.Empty;
            int iTransID = 0;
            //DbReader objDbReader = null;

            try //Main Try Block
            {
                if (p_objXmlIn.SelectSingleNode("//TransactionID") != null)
                {
                    iTransID = Common.Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//TransactionID").InnerText);               
                }

                sSQL = "SELECT COMMENTS FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID =" + iTransID;
                using (DbReader objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    if (objDbReader.Read())
                    {
                        sComments = Conversion.ConvertObjToStr(objDbReader.GetValue("COMMENTS"));
                    }
                    //objDbReader.Close();
                }

                objXMLDOM = new XmlDocument();
                objXMLDOM.LoadXml("<Editor><ExistingComments/></Editor>");

                objNode = objXMLDOM.DocumentElement.SelectSingleNode("//ExistingComments");
                objXmlElem = objXMLDOM.CreateElement("Comments");
                objXmlElem.InnerText = sComments;
                objNode.AppendChild(objXmlElem);


            }//End of Main Try Block
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Editor.GetComments.Error",m_iClientId), objException);//sonali
            }
            finally
            {
                objXmlElem = null;
                objNode = null;
                //if (objDbReader != null)
                //{
                //    objDbReader.Close();
                //    objDbReader.Dispose();
                //}
            }

            //Return the result to calling function
            return objXMLDOM;
        }//end function
        #endregion
		
	}// End class
} // End namespace
