﻿///********************************************************************
/// Amendment History
///********************************************************************
/// Date Amended   *            Amendment               *    Author
/// 02/10/2014         MITS 34260 - duplicate claim         Ngupta36
///********************************************************************
using System;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using System.Data;
using System.Text;
using Riskmaster.Settings;
//smahajan6 - MITS #18230 - 11/18/2009 :Start
using System.Collections;
//smahajan6 - MITS #18230 - 11/18/2009 :End
//dvatsa- JIRA 11108(start)
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using Newtonsoft.Json.Linq;
//dvatsa- JIRA 11108(end)
namespace Riskmaster.Application.LookupData
{
	/**************************************************************
		 * $File		: Lookup.cs
		 * $Revision	: 1.0.0.0
		 * $Date		: 08/22/2005
		 * $Author		: Mihika Agrawal
		 * $Comment		: Has functions to display the Look up Data for a particular form
		 * $Source		:  	
		**************************************************************/

	/// <summary>
	/// Lookup class
	/// </summary>
	public class Lookup:IDisposable
	{
	
		#region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
        /// <summary>
        /// Sumit 01/11/2010 MITS# 18230
        /// Private variable to store the conversion status by CastToType method
        /// </summary>
        private Boolean bIsSucess = false;
		private bool m_bIsADMTable = false;
		private ADMTable m_objADM = null;
        //Raman: modified for R5
        string m_sParentID = string.Empty;
        public static string sParentID = string.Empty; //dvatsa-11108
        //10/09/2009 Raman Bhatia: Creating Lookup for Reserve History in Reserve Worksheet
        string m_sParentsysformname = string.Empty;

        UserLogin m_objUserLogin = null;

        private int m_iClientId = 0;//rkaur27
        //dvatsa -JIRA 11108(start)
        private int m_iLangCode = 0;
        public static int iRowCount = 0;
        //public string strHyperLinkCellTemplate = "<div class=\"ngCellText\" ng-class=\"col.colIndex()\" title=\"{{COL_FIELD}}\" ><a onclick=\"OpenRecord(\'{{row.getProperty(\'pid\')}}\',\'{{row.getProperty(\'pid\')}}\')\" href=\"#\">{{row.getProperty(col.field)}}</a></div>";
        //public string strHyperLinkCellTemplate = "<div ng-if=\"row.getProperty(col.field)== \'\'\" class=\"ngCellText\" ng-class=\"col.colIndex()\" title=\"{{COL_FIELD}}\" ><a onclick=\"OpenRecord(\'{{row.getProperty(\'pid\')}}\',\'{{row.getProperty(\'pid\')}}\')\" href=\"#\">_</a></div> <div ng-if=\"row.getProperty(col.field)!= \'\'\" class=\"ngCellText\" ng-class=\"col.colIndex()\" title=\"{{COL_FIELD}}\" ><a onclick=\"OpenRecord(\'{{row.getProperty(\'pid\')}}\',\'{{row.getProperty(\'pid\')}}\')\" href=\"#\">{{row.getProperty(col.field)}}</a></div>"; //rma 18249
        public string strHyperLinkCellTemplate = "<div ng-switch=\"row.getProperty(col.field)\"><div ng-switch-when=\'\'\" class=\"ngCellText\" ng-class=\"col.colIndex()\" title=\"{{COL_FIELD}}\" ><a onclick=\"OpenRecord(\'{{row.getProperty(\'pid\')}}\',\'{{row.getProperty(\'pid\')}}\')\" href=\"#\">_</a></div> <div ng-switch-default><div class=\"ngCellText\" ng-class=\"col.colIndex()\" title=\"{{COL_FIELD}}\" ><a onclick=\"OpenRecord(\'{{row.getProperty(\'pid\')}}\',\'{{row.getProperty(\'pid\')}}\')\" href=\"#\">{{row.getProperty(col.field)}}</a></div></div></div>";//rma 18249
        
        public string sCenterAlignHeaderCellTemplate = "<div class=\"ngHeaderSortColumn {{col.headerClass}}\" ng-style=\"{'cursor': col.cursor}\" ng-class=\"{ 'ngSorted': !noSortVisible }\">" +
                           "<div ng-dblclick=\"onDblClickRow(col)\" ng-click=\"col.sort($event)\"ng-class=\"'colt' + col.index\" class=\"ngHeaderTextAlignCenter\">{{col.displayName}}</div><div class=\"ngSortButtonDown\" ng-show=\"col.showSortButtonDown()\"></div>" +
                           "<div class=\"ngSortButtonUp\" ng-show=\"col.showSortButtonUp()\"></div><div class=\"ngSortPriority\">{{col.sortPriority}}</div><div ng-class=\"{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }\" ng-click=\"togglePin(col)\" ng-show=\"col.pinnable\"></div></div>" +
                           "<input type=\"text\" ng-click=\"stopClickProp($event)\" placeholder=\"Filter...\" ng-model=\"col.filterText\" ng-style=\"{ 'width' : col.width - 14 + 'px' }\" style=\"position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;\"/>" +
                           "<div ng-show=\"col.resizable\" class=\"ngHeaderGrip\" ng-click=\"col.gripClick($event)\" ng-mousedown=\"col.gripOnMouseDown($event)\"></div>";
        public string sRightAlignHeaderCellTemplate = "<div class=\"ngHeaderSortColumn {{col.headerClass}}\" ng-style=\"{'cursor': col.cursor}\" ng-class=\"{ 'ngSorted': !noSortVisible }\">" +
                           "<div ng-dblclick=\"onDblClickRow(col)\" ng-click=\"col.sort($event)\"ng-class=\"'colt' + col.index\" class=\"ngHeaderTextAlignRight\">{{col.displayName}}</div><div class=\"ngSortButtonDown\" ng-show=\"col.showSortButtonDown()\"></div>" +
                           "<div class=\"ngSortButtonUp\" ng-show=\"col.showSortButtonUp()\"></div><div class=\"ngSortPriority\">{{col.sortPriority}}</div><div ng-class=\"{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }\" ng-click=\"togglePin(col)\" ng-show=\"col.pinnable\"></div></div>" +
                           "<input type=\"text\" ng-click=\"stopClickProp($event)\" placeholder=\"Filter...\" ng-model=\"col.filterText\" ng-style=\"{ 'width' : col.width - 14 + 'px' }\" style=\"position: absolute; top: 30px; bottom: 30px; left: 0; bottom:0;\"/>" +
                           "<div ng-show=\"col.resizable\" class=\"ngHeaderGrip\" ng-click=\"col.gripClick($event)\" ng-mousedown=\"col.gripOnMouseDown($event)\"></div>";
        public string sCenterAlignCellTemplate = "<div class=\"ngCellTextAlignCenter\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{COL_FIELD }}</span></div>";
        public string sRightAlignCellTemplate = "<div class=\"ngCellTextAlignRight\" ng-class=\"col.colIndex()\"><span ng-cell-text>{{COL_FIELD }}</span></div>";
       //dvatsa- JIRA 11108(end)
		#endregion

		#region Constants Declaration
		/// <summary>
		/// Default Page Size
		/// </summary>
		private const int DEFAULT_PAGE_SIZE = 100 ;
        //smahajan6 11/26/09 MITS:18230 :Start
        private const string ARR_DELIMITER = "^*^*^";
        //smahajan6 11/26/09 MITS:18230 :End
		
		#endregion

		#region Constructor
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="objUser">UserLogin</param>
		public Lookup(UserLogin objUser, int p_iClientId)
		{
            m_objUserLogin = objUser;
			m_sUserName = objUser.LoginName;
			m_sPassword = objUser.Password;
			m_sDsnName = objUser.objRiskmasterDatabase.DataSourceName;
            m_iClientId = p_iClientId;//rkaur27
			Initialize(objUser);
		}
        private bool _isDisposed = false;
		public void Dispose()
		{
            if (!_isDisposed)
            {
                _isDisposed = true;

                if (m_objDataModelFactory != null)
                {
                    m_objDataModelFactory.Dispose();
                    m_objDataModelFactory = null;
                }
                if (m_objADM != null)
                {
                    m_objADM.Dispose();
                    m_objADM = null;
                }
                GC.SuppressFinalize(this);
            }
		}
		#endregion

		#region Public Methods
		
		/// Name		: GetLookUpData
		/// Author		: Mihika Agrawal
		/// Date Created: 08/22/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the Look up Data for a particular form.
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml structure document</param>
		/// <returns>Xml Document with Data</returns>
		public XmlDocument GetLookUpData(XmlDocument p_objXmlDoc)
		{
			XmlDocument objXmlDoc = new XmlDocument();
			XmlElement objData = null;
			XmlElement objHead = null;
			XmlElement objNode = null;
			XmlElement objRows = null;
			XmlElement objCol = null;
			XmlElement objHiddenInputs = null;
			DbReader objReader = null;
            bool bIsMobileAdjuster = false;
            bool bIsMobilityAdjuster = false;
			string sFormName = string.Empty;
			string sSQL = string.Empty;
			string sDBType = string.Empty;
            
			//MITS 11688 Raman Bhatia
            //Default Page Size set in utilities is not working for lookups
            SysSettings objSysSettings = null;//Deb MITS 25775
            int iPageSize = DEFAULT_PAGE_SIZE;
            iPageSize = GetRecordsPerPage();
            
            int iRecordCount = 0;
			if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("recordcount") != null)
				iRecordCount = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("recordcount").InnerText);

			int iStartAt = 0;
			
			int iPageCount = 0;
			if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("pagecount") != null)
				iPageCount = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("pagecount").InnerText);

			int iPageNumber = 0;
			if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("pagenumber") != null)
				iPageNumber = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("pagenumber").InnerText);
			if (iPageNumber == 0)
				iPageNumber = 1;
			
			int iTmp = 0;

			string sPrimaryField = string.Empty;
			string sSecondaryField = string.Empty;
			string sThirdField = string.Empty;
			string sForthField = string.Empty;
			string sPrimaryFormField = string.Empty;
			string sSecondaryFormField = string.Empty;
			string sThirdFormField = string.Empty;
            //Added:Yukti, DT:05/21/2014, MITS 35772
             string sSQLUnit = string.Empty;
            string sUnitNumber = string.Empty;

            //smahajan6 - MITS #18230 - 11/18/2009 :Start 
            //For Hidden Columns - Array of Columns to Hide
            ArrayList arrlstHiddenFields = new ArrayList();
            //smahajan6 - MITS #18230 - 11/18/2009 :End
			string sFpid = string.Empty;
			if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("fpid") != null)
				sFpid = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("fpid").InnerText;

			string sFpidName = string.Empty;
			if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("fpidname") != null)
				sFpidName = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("fpidname").InnerText;
			
			string sSid = string.Empty;
			if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("sid") != null)
				sSid = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("sid").InnerText;

			string sPsid = string.Empty;
			if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("psid") != null)
				sPsid = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("psid").InnerText;

			string sSysEx = string.Empty;
			if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("sys_ex") != null)
				sSysEx = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("sys_ex").InnerText;

            string sClaimid = string.Empty;
            if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("claimid") != null)
                sClaimid = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("claimid").InnerText;
			
			try
			{
				//BSB Moved this call into the Ctor.
				//this.Initialize();
				sFormName = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("form").InnerText;
                sFormName = sFormName.ToLower();

                //Raman: modified for R5
                if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("ParentID") != null)
                {
                    m_sParentID = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("ParentID").InnerText;
                }

                //10/09/2009 Raman Bhatia: Creating Lookup for Reserve History in Reserve Worksheet
                if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("parentsysformname") != null)
                {
                    m_sParentsysformname = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("parentsysformname").InnerText;
                }

                //smahajan6 - MITS #18230 - 11/18/2009 : Start
                //Additional Argument for Hidden Columns - To Get the List of Columns to Hide
                sSQL = GetSQLFromFormName(sFormName, p_objXmlDoc, ref sPrimaryField, ref sSecondaryField, ref sThirdField, ref sForthField, ref sPrimaryFormField, ref sSecondaryFormField, ref sThirdFormField, ref arrlstHiddenFields);
                //sSQL =  GetSQLFromFormName(sFormName, p_objXmlDoc, ref sPrimaryField, ref sSecondaryField, ref sThirdField, ref sForthField, ref sPrimaryFormField, ref sSecondaryFormField, ref sThirdFormField);
                //smahajan6 - MITS #18230 - 11/18/2009 : End
				if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
				{
					if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml!="")
					{
						sSQL = sSQL + " ORDER BY " +p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml + " " +p_objXmlDoc.SelectSingleNode("//OrderMode").InnerXml;
					}
				}
				if(m_bIsADMTable)
				{
					ChangeSQLAdminTrck(ref sSQL, sFormName);
				}

				sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString();

				if(sDBType == Constants.DB_ACCESS)
					sSQL = sSQL.Replace("|", " AS ");
				else
                    // npadhy In case of Oracle || is used for Concatenation and | is used as Alias
                    // The Code below preserves the || operator for concatenation
					sSQL = sSQL.Replace("||", "^@").Replace("|" , " ").Replace("^@","||");

				//MGaba2:MITS 20724:Crashing when Lookup fields are not defined for Admin Tracking Table in Oracle
				//if (iRecordCount == 0)
                if (iRecordCount == 0 && sSQL != "")
					iRecordCount = GetRecordCount(sSQL);
                //MITS 11688 Raman Bhatia
                //Default Page Size set in utilities is not working for lookups
				//iPageCount = (int)Math.Ceiling((double)iRecordCount/DEFAULT_PAGE_SIZE);
                iPageCount = (int)Math.Ceiling((double)iRecordCount / iPageSize);
				if (iPageNumber == 1)
					iStartAt = 1;
				else
					//iStartAt = ((iPageNumber - 1) * DEFAULT_PAGE_SIZE) + 1;
                    iStartAt = ((iPageNumber - 1) * iPageSize) + 1;

				objData = objXmlDoc.CreateElement("Data");
				objXmlDoc.AppendChild(objData);
				objData.SetAttribute("form", sFormName);

				if (sSecondaryFormField == "hidden")
				{
					objData.SetAttribute("fpid2", sPrimaryFormField);
					objData.SetAttribute("fpid3", sThirdFormField);
				}
				else
					objData.SetAttribute("fpid2", sSecondaryFormField);

				objData.SetAttribute("psid", sPsid);
				objData.SetAttribute("sys_ex", sSysEx); 
				objData.SetAttribute("sys_ex_name", GetSysEx(p_objXmlDoc));
				objData.SetAttribute("recordcount", iRecordCount.ToString());
				objData.SetAttribute("pagecount", iPageCount.ToString());
				objData.SetAttribute("thispage", iPageNumber.ToString());
				objData.SetAttribute("fpid", sFpid);
                objData.SetAttribute("pagesize", Convert.ToString(iPageSize));

				objData.SetAttribute("sid", sSid); //TODO

				objHead = objXmlDoc.CreateElement("Header");
				objData.AppendChild(objHead);

				int iColCnt = 0;
				string sColName = string.Empty;
				string sUnAliasedFieldName = "";
                string sDbType = string.Empty;
//				Log.Write(sSQL);
				//MGaba2:MITS 20724:Crashing when Lookup fields are not defined for Admin Tracking Table in Oracle
                if (sSQL != "")
                {
                    //Commented by Amitosh for MITS 27378
                    //Deb MITS 25775
                    //objSysSettings = new SysSettings(m_sConnectionString);
                    //if (objSysSettings.MaskSSN)
                    //{
                    //    if (sSQL.IndexOf("ENTITY.TAX_ID") > -1)
                    //    {
                    //        sSQL = sSQL.Replace("ENTITY.TAX_ID", "'###-##-####' TAX_ID");
                    //    }
                    //    else if (sSQL.IndexOf("TAX_ID") > -1)
                    //    {
                    //        sSQL = sSQL.Replace("TAX_ID", "'###-##-####' TAX_ID");
                    //    }
                    //}
                    //Deb MITS 25775
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    iColCnt = objReader.FieldCount;
                    for (int i = 0; i < iColCnt; i++)
                    {
                        sColName = objReader.GetName(i);
                        sDbType = objReader.GetDataTypeName(i);

                        //smahajan6 - MITS #18230 - 11/18/2009 : Start
                        //Additional Check for Hidden Columns - Prevent Display of Hidden Columns 
                        if (sColName != sPrimaryField && sColName != sSecondaryField && !arrlstHiddenFields.Contains(sColName))
                        //if (sColName != sPrimaryField && sColName != sSecondaryField)
                        //smahajan6 - MITS #18230 - 11/18/2009 : End
                        {
                            if ((sSecondaryFormField == "hidden" && sColName != sThirdField) ||
                                sSecondaryFormField != "hidden")
                            {
                                objNode = objXmlDoc.CreateElement("ColHead");

                                //MGaba2:MITS 15638:Sorting on Dated Text not working:Start
                                //Parijat: Mits 11491 :disabling sorting for columns where data types cannot be compared or sorted
                                if (objReader.GetDataTypeName(i) == "text" || objReader.GetDataTypeName(i) == "ntext" || objReader.GetDataTypeName(i) == "image" || objReader.GetDataTypeName(i) == "Clob")
                                {
                                    objNode.SetAttribute("nosort", "on"); // TODO
                                }
                                //MGaba2:MITS 15638:Sorting on Dated Text not working:End
                                //MITS 20135 Need to escape Admin tracking field caption before added to attribute
                                if (this.m_bIsADMTable)
                                {
                                    if (!IsSortableSuppFieldType(m_objADM[sColName]))
                                        objNode.SetAttribute("nosort", "on"); // TODO
                                    objNode.SetAttribute("title", System.Security.SecurityElement.Escape(m_objADM[sColName].Caption)); // TODO
                                }
                                else
                                {
                                    objNode.SetAttribute("title", FormatFieldName(sColName)); // TODO
                                }
                                //rupal
                                if (sColName.ToUpper() == "VIN-ADDRESS" && string.Equals(sFormName, "otherunitloss",StringComparison.InvariantCultureIgnoreCase))
                                    sColName = "REFERENCE_NUMBER";
                                //rupal
                                //dvatsa- JIRA 11108 - added check for Adjuster_Type column as it doesn't need to be bind in grid (start)
                                if (sColName != "ADJUSTER_TYPE") 
                                {
                                    objNode.SetAttribute("DbFieldName", sColName);
                                    objNode.SetAttribute("DbFieldType", sDbType);
                                    objHead.AppendChild(objNode);
                                }
                                //dvatsa- JIRA 11108 - added check for Adjuster_Type column as it doesn't need to be bind in grid (end)
                            }
                        }
                    }

                    objRows = objXmlDoc.CreateElement("Rows");
                    objData.AppendChild(objRows);

                    // move to the current page
                    int iCount = 1;
                    if (iStartAt > 1)
                    {
                        while (iCount < iStartAt)
                        {
                            iCount++;
                            objReader.Read();
                        }
                    }
                    objNode = null;

                    string sValue = string.Empty;
                    string sTmpStr = string.Empty;
                    string[] arrPID = { "" };
                    iCount = 0;
                    if (p_objXmlDoc.SelectSingleNode("//caller") != null)
                    {
                        bIsMobileAdjuster = true;
                        bIsMobilityAdjuster = true;
                    }
                    //MITS 11688 Raman Bhatia
                    //Default Page Size set in utilities is not working for lookups
                    //while (objReader.Read() && iCount < DEFAULT_PAGE_SIZE)
                    while (objReader.Read() && (iCount < iPageSize || bIsMobileAdjuster || bIsMobilityAdjuster))
                    {
                        iCount++;
                        objNode = objXmlDoc.CreateElement("Row");
                        sValue = objReader.GetValue(sPrimaryField).ToString();
                        objNode.SetAttribute("pid", sValue);
                        //Added:Yukti, DT:05/22/2014, MITS 35772
                        if (sFormName == "claimant")
                        {
                           
                            if (sDBType == Constants.DB_SQLSRVR)
                            {
                                sSQLUnit = "SELECT CASE WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='P' THEN (SELECT 'PIN:' + PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                        + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='V' THEN (SELECT 'VIN:' + VIN FROM VEHICLE WHERE UNIT_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                        + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='S' THEN (SELECT 'SITE:' + SITE_NUMBER FROM SITE_UNIT WHERE SITE_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                        + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='SU' THEN (SELECT 'STAT:' + ISNULL(ENTITY.LAST_NAME, '') + ISNULL(ENTITY.FIRST_NAME, '') FROM ENTITY WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM OTHER_UNIT WHERE OTHER_UNIT_ID = CLAIMANT_X_UNIT.UNIT_ID))"
                                        + " END AS UNIT_NUMBER FROM CLAIMANT_X_UNIT WHERE CLAIMANT_ROW_ID = " + sValue + " ";
                            }
                            else if (sDBType == Constants.DB_ORACLE)
                            {
                                sSQLUnit = "SELECT CASE WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='P' THEN (SELECT 'PIN:' || PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                        + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='V' THEN (SELECT 'VIN:' || VIN FROM VEHICLE WHERE UNIT_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                        + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='S' THEN (SELECT 'SITE:' || SITE_NUMBER FROM SITE_UNIT WHERE SITE_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                        + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='SU' THEN (SELECT 'STAT:' || NVL(ENTITY.LAST_NAME, '') || NVL(ENTITY.FIRST_NAME, '') FROM ENTITY WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM OTHER_UNIT WHERE OTHER_UNIT_ID = CLAIMANT_X_UNIT.UNIT_ID))"
                                        + " END AS UNIT_NUMBER FROM CLAIMANT_X_UNIT WHERE CLAIMANT_ROW_ID = " + sValue + " ";
                            }

                            using (DbReader objReaderUnit = DbFactory.GetDbReader(m_sConnectionString, sSQLUnit))
                            {
                                while (objReaderUnit.Read())
                                {
                                    if(sUnitNumber != null && sUnitNumber != "")
                                    {
                                        sUnitNumber = sUnitNumber + "," + Convert.ToString(objReaderUnit.GetValue("UNIT_NUMBER"));
                                    }
                                    else
                                    {
                                        sUnitNumber = Convert.ToString(objReaderUnit.GetValue("UNIT_NUMBER"));
                                    }
                                }
                            }
                            objNode.SetAttribute("pid5", sUnitNumber);

                        }
                        //Ended:Yukti, DT:05/22/2014,MITS 35772
                        if (sSecondaryField != "")
                            sValue = objReader.GetValue(sSecondaryField).ToString();
                        else
                            sValue = "";

                        if (sSecondaryFormField == "origvalues")
                        {
                            sTmpStr = sValue;
                            if (sThirdField != string.Empty)
                            {
                                sValue = objReader.GetValue(sThirdField).ToString();
                                sTmpStr = sTmpStr + "|" + sValue;
                            }
                            if (sForthField != string.Empty)
                            {
                                sValue = objReader.GetValue(sForthField).ToString();
                                sTmpStr = sTmpStr + "|" + sValue;
                            }
                            objNode.SetAttribute("pid2", sTmpStr);
                        }
                        else if (sSecondaryFormField == "hidden")
                        {
                            sValue = objReader.GetValue(sSecondaryField).ToString();
                            objNode.SetAttribute("pid2", sValue);

                            if (sThirdFormField == "staterowid")
                            {
                                sValue = objReader.GetValue(sThirdField).ToString();
                                objNode.SetAttribute("pid3", sValue);
                            }

                        }
                        else
                            objNode.SetAttribute("pid2", sValue);

                        //smahajan6 - MITS #18230 - 11/18/2009 : Start
                        //For Hidden Columns - Fetch Value for Hidden Columns
                        if (arrlstHiddenFields.Count > 0)
                        {
                            sValue = "";
                            sTmpStr = "";
                            for (int iCnt = 0; iCnt < arrlstHiddenFields.Count; iCnt++)
                            {
                                sTmpStr = objReader.GetValue(arrlstHiddenFields[iCnt].ToString()).ToString();
                                sValue += sTmpStr + ARR_DELIMITER;
                            }
                            objNode.SetAttribute("pid4", sValue);
                        }
                        //smahajan6 - MITS #18230 - 11/18/2009 : End
                        if (sFpid != "")
                        {
                            arrPID = new string[1];
                            if (sFpid.IndexOf(",") > 0)
                                arrPID = sFpid.Split(',');
                            else
                                arrPID[0] = sFpid;

                            //Changed by Gagan for MITS 9633 : Start
                            if (sFpidName == "")
                            {
                                for (iTmp = 0; iTmp < arrPID.Length; iTmp++)
                                {
                                    if (p_objXmlDoc.SelectSingleNode("//" + arrPID[iTmp]) != null)
                                    {
                                        if (p_objXmlDoc.SelectSingleNode("//" + arrPID[iTmp]).Attributes.GetNamedItem("value") != null)
                                        {
                                            sFpidName = sFpidName + arrPID[iTmp] + "=" + p_objXmlDoc.SelectSingleNode("//" + arrPID[iTmp]).Attributes.GetNamedItem("value").InnerText + "&";
                                        }
                                    }
                                    else if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem(arrPID[iTmp]) != null)
                                    {
                                        sFpidName = sFpidName + arrPID[iTmp] + "=" + p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem(arrPID[iTmp]).InnerText + "&";
                                    }
                                }
                            }
                            else
                            {
                                iTmp = 1;
                            }
                            //Changed by Gagan for MITS 9633 : End

                        }

                        if (iTmp > 0)
                        {
                            //Removed extra & sign at the end of fpidname
                            if (sFpidName.EndsWith("&"))
                                sFpidName = sFpidName.Substring(0, sFpidName.Length - 1);
                            if (sFpidName.EndsWith("="))
                                sFpidName = "";
                            objData.SetAttribute("fpidname", sFpidName);
                        }
                        else
                            objData.SetAttribute("fpid", sPrimaryFormField);

                        objRows.AppendChild(objNode);

                        for (int i = 0; i < iColCnt; i++)
                        {
                            sColName = objReader.GetName(i);
                            //rupal
                          if (sColName.ToUpper() == "VIN-ADDRESS" && string.Equals(sFormName, "otherunitloss", StringComparison.InvariantCultureIgnoreCase))
                                sColName = "REFERENCE_NUMBER";
                            //rupal
                            //smahajan6 - MITS #18230 - 11/18/2009 : Start
                            //Additional Check for Hidden Columns - Prevent Display of Hidden Columns
                            if (sColName != sPrimaryField && sColName != sSecondaryField && !arrlstHiddenFields.Contains(sColName))
                            //if (sColName != sPrimaryField && sColName != sSecondaryField)
                            //smahajan6 - MITS #18230 - 11/18/2009 : End
                            {
                                if ((sSecondaryFormField == "hidden" && sColName != sThirdField)
                                    || (sSecondaryFormField != "hidden"))
                                {
                                    //commented by Shivendu for R5
                                    //objCol = objXmlDoc.CreateElement("Col");
                                    //objNode.AppendChild(objCol);
                                    //objCol.SetAttribute("name", sColName);

                                    //Added by Shivendu for R5
                                    objCol = objXmlDoc.CreateElement(sColName);
                                    objNode.AppendChild(objCol);

                                    //MITS 11203 by Gagan : Start
                                    sValue = FormatField(objReader.GetValue(i).ToString(), sColName, objReader.GetFieldType(i).ToString());
                                    if (sFormName == "adjusterdatedtext" && sColName == "DATED_TEXT")
                                    {
                                        objCol.InnerText = sValue;
                                    }
                                    else if ((sColName == "FIRST_NAME") || (sColName == "LAST_NAME")) //MITS 24823 mcapps2 - We do not want to truncate the name, it is used in Funds
                                    {
                                        objCol.InnerText = sValue;
                                    }
                                    //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                                    else if ((string.Equals(sFormName, "adjuster", StringComparison.OrdinalIgnoreCase) || string.Equals(sFormName, "claimadjuster", StringComparison.OrdinalIgnoreCase)) && string.Equals(sColName, "ASSIGNMENT_LEVEL", StringComparison.OrdinalIgnoreCase))
                                    {
                                        string strTableName = "ASSIGNMENT_LEVEL";
                                        using (LocalCache objCache = new LocalCache(m_sConnectionString,m_iClientId))
                                        {
                                            switch (sValue.ToUpper().Trim())
                                            {
                                                case "CT":      //For Claimant
                                                    sValue = objCache.GetCodeDesc(objCache.GetCodeId("CT", strTableName));
                                                    break;
                                                case "RE":      //For Reserve
                                                    sValue = objCache.GetCodeDesc(objCache.GetCodeId("RE", strTableName));

                                                    objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                                                    if (objSysSettings.MultiCovgPerClm == -1)       //For Carrier Claim
                                                        sValue = string.Concat(objCache.GetCodeDesc(objCache.GetCodeId("CT", strTableName)),"/",sValue);
                                                    break;
                                                case "UN":      //For Unit
                                                    sValue = objCache.GetCodeDesc(objCache.GetCodeId("UN", strTableName));
                                                    break;
                                                default:        //For Claim
                                                    sValue = objCache.GetCodeDesc(objCache.GetCodeId("CL", strTableName));
                                                    break;
                                            }
                                        }
                                        objCol.InnerText = sValue;
                                    }
                                    else if ((string.Equals(sFormName, "adjuster", StringComparison.OrdinalIgnoreCase) || string.Equals(sFormName, "claimadjuster", StringComparison.OrdinalIgnoreCase)) && string.Equals(sColName, "ASSIGNMENT_NAME", StringComparison.OrdinalIgnoreCase))
                                    {
                                        sValue = GetAssignmentName(objReader.GetValue("ASSIGNMENT_LEVEL").ToString(), sValue);
                                        objCol.InnerText = sValue;
                                    }
                                    //Ankit End
                                    else if (string.Equals(sColName, "TAX_ID", StringComparison.InvariantCultureIgnoreCase)) //Added by Amitosh for MITS 27378  
                                    {
                                        objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                                        if (objSysSettings.MaskSSN)
                                        {
                                            if (string.IsNullOrEmpty(sValue))
                                            {
                                                objCol.InnerText = "";
                                            }
                                            else
                                            {
                                                objCol.InnerText = "###-##-####";
                                            }
                                        }
                                        else
                                            objCol.InnerText = sValue;
                                    }//end Amitosh
                                    //Added:Yukti, DT:05/22/2014, MITS 35772
                                    else if(string.Equals(sFormName, "claimant", StringComparison.InvariantCultureIgnoreCase) && string.Equals(sColName, "UNIT"))
                                    {
                                        if (sUnitNumber.Contains(","))
                                        {
                                            objCol.InnerText = sValue + "...";
                                        }
                                        else
                                        {
                                            objCol.InnerText = sValue;
                                        }
                                    }
                                    //JIRA RMA-15935 ajohari2 START
                                    else if (string.Equals(sFormName, "casemanagementlist", StringComparison.InvariantCultureIgnoreCase) && (sColName.ToUpper().Equals("DATE_RECORD_ADDED") || sColName.ToUpper().Equals("DATE_RECORD_LAST_UPDATED")))
                                    {
                                        objCol.InnerText = Convert.ToDateTime(Conversion.GetDBDateFormat(sValue, "d")).ToShortDateString();
                                    }
                                    //JIRA RMA-15935 ajohari2 START
                                    else
                                    {
                                        if (sValue.Length == 0)
                                            objCol.InnerText = "";
                                        else if (sValue.Length > 255)
                                            objCol.InnerText = sValue.Substring(0, 255);
                                        else
                                            objCol.InnerText = sValue;
                                    }
                                    //MITS 11203 by Gagan : End
                                }
                            }
                        } // end-for
                        sUnitNumber = "";
                    }// end-while

                    objReader.Close();

                    // Create hiddeninputs tag
                    string[] arrFpidName = new string[1];
                    if (sFpidName.IndexOf("&") > 0)
                        arrFpidName = sFpidName.Split('&');
                    else
                        arrFpidName[0] = sFpidName;

                    for (int i = 0; i < arrFpidName.Length; i++)
                    {
                        sValue = arrFpidName[i];
                        if (sValue.IndexOf("=") > 0)
                        {
                            objHiddenInputs = objXmlDoc.CreateElement("hiddeninputs");
                            objData.AppendChild(objHiddenInputs);
                            objHiddenInputs.SetAttribute("name", sValue.Substring(0, sValue.IndexOf("=")));
                            objHiddenInputs.SetAttribute("value", sValue.Substring(sValue.IndexOf("=") + 1));
                        }
                    }

                    if (iPageCount > 1)
                    {
                        if (iPageNumber == 1)
                            objData.RemoveAttribute("previouspage");
                        else
                        {
                            iTmp = iPageNumber - 1;
                            objData.SetAttribute("previouspage", iTmp.ToString());
                        }

                        if (iPageNumber > 1)
                            objData.SetAttribute("firstpage", "1");
                        else
                            objData.RemoveAttribute("firstpage");

                        if (iPageNumber == iPageCount)
                            objData.RemoveAttribute("nextpage");
                        else
                        {
                            iTmp = iPageNumber + 1;
                            objData.SetAttribute("nextpage", iTmp.ToString());
                        }

                        objData.SetAttribute("thispage", iPageNumber.ToString());
                        objData.SetAttribute("pagecount", iPageCount.ToString());

                        if (iPageNumber < iPageCount)
                            objData.SetAttribute("lastpage", iPageCount.ToString());
                        else
                            objData.RemoveAttribute("lastpage");
                    }
                }//End if : sSql is not blank
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("Lookup.GetLookUpData.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objCol = null;
				objData = null;
				objHead = null;
				objHiddenInputs = null;
				objNode = null;
				//Modified by Shivendu to dispose
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                    
                }
				if(m_objDataModelFactory !=null)
				{
					m_objDataModelFactory.UnInitialize();
					m_objDataModelFactory.Dispose();
				}
                objSysSettings = null;//Deb MITS 25775
			}
			return objXmlDoc;
		}
		#endregion

		#region Private Methods

        //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
        private string GetAssignmentName(string sAssginmentLvl, string sValue)
        {
            string strReturnVal = sValue;
            string strFirstName = string.Empty;
            string strLastName = string.Empty;
            string[] arrLevel = new string[1];            
            arrLevel = strReturnVal.Split(' ');

            using (LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId))
            {
                switch (sAssginmentLvl.ToUpper().Trim())
                {
                    case "CT":      //For Claimant
                        objCache.GetClaimantInfo(Convert.ToInt64(arrLevel[1].Trim()), ref strFirstName, ref strLastName);
                        strReturnVal = string.Concat(strFirstName, " ", strLastName).Trim();
                        break;
                    case "RE":      //For Reserve
                        strReturnVal = GetReserveAssignName(Convert.ToInt32(arrLevel[1]));
                        break;
                    case "UN":      //For Unit
                        if (string.Equals(arrLevel[0].ToUpper(), Constants.WPA_DIARY_ENTRY_ATTACH_TABLE.PROPERTYLOSS.ToString(), StringComparison.OrdinalIgnoreCase))
                            strReturnVal = GetPropertyLossAssignName(Convert.ToInt32(arrLevel[1]));
                        else if (string.Equals(arrLevel[0].ToUpper(), Constants.WPA_DIARY_ENTRY_ATTACH_TABLE.UNIT.ToString(), StringComparison.OrdinalIgnoreCase))
                            strReturnVal = GetVehicleAssignName(Convert.ToInt32(arrLevel[1]));
                        else if (string.Equals(arrLevel[0].ToUpper(), Constants.WPA_DIARY_ENTRY_ATTACH_TABLE.SITELOSS.ToString(), StringComparison.OrdinalIgnoreCase))
                            strReturnVal = GetSiteLossAssignName(Convert.ToInt32(arrLevel[1]));
                        else if (string.Equals(arrLevel[0].ToUpper(), Constants.WPA_DIARY_ENTRY_ATTACH_TABLE.OTHERUNIT.ToString(), StringComparison.OrdinalIgnoreCase))
                            strReturnVal = string.Concat(Constants.WPA_DIARY_ENTRY_ATTACH_TABLE.OTHERUNIT.ToString(), " ", objCache.GetEntityLastFirstName(Convert.ToInt32(arrLevel[1])));
                        break;
                    default:        //For Claim
                        strReturnVal = objCache.GetCodeDesc(objCache.GetCodeId("CL", "ASSIGNMENT_LEVEL")).ToUpper();
                        break;
                }
            }

            return strReturnVal;
        }

        private string GetReserveAssignName(int iRcRowID)
        {
            SysSettings objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
            UserLogin objUserLogin = new UserLogin(m_iClientId); 
            string sReserveAssignName = string.Empty;
            StringBuilder sSQL = new StringBuilder();
            string sFullName = string.Empty;
            string sCodeDesc = string.Empty;
            bool isBaseLangCode = false;
            int iUserLangCode = objUserLogin.objUser.NlsCode;
            int iBaseLangCode = Convert.ToInt32(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
            
            if (int.Equals(iUserLangCode, iBaseLangCode) || int.Equals(iUserLangCode, 0))
                isBaseLangCode = true;

            sSQL.Append("SELECT CT.CODE_DESC, ");
            if (isBaseLangCode)
                sSQL.Append(" '' USER_CODE_DESC ");
            else
                sSQL.Append(" UCT.CODE_DESC USER_CODE_DESC ");

            if (objSysSettings.MultiCovgPerClm == -1)       //For Carrier Claim
                sSQL.Append(", E.FIRST_NAME, E.LAST_NAME");

            sSQL.Append(" FROM RESERVE_CURRENT RC ");
            sSQL.Append(" INNER JOIN CODES_TEXT CT ON CT.CODE_ID = RC.RESERVE_TYPE_CODE  AND CT.LANGUAGE_CODE = " + iBaseLangCode + " ");
            if (objSysSettings.MultiCovgPerClm == -1)       //For Carrier Claim
                sSQL.Append(" INNER JOIN ENTITY E ON E.ENTITY_ID = RC.CLAIMANT_EID ");

            if (!isBaseLangCode)
                sSQL.Append(" LEFT OUTER JOIN CODES_TEXT UCT On C.CODE_ID = UCT.CODE_ID AND UCT.LANGUAGE_CODE = " + iUserLangCode + " ");
            sSQL.Append(" WHERE RC_ROW_ID = " + iRcRowID);

            using (DbReader objReader = DbFactory.ExecuteReader(m_sConnectionString, sSQL.ToString()))
            {
                if (objReader.Read())
                {
                    if (string.IsNullOrEmpty(Convert.ToString(objReader.GetValue("USER_CODE_DESC")).Trim()))
                        sCodeDesc = Convert.ToString(objReader.GetValue("CODE_DESC")).Trim();
                    else
                        sCodeDesc = Convert.ToString(objReader.GetValue("USER_CODE_DESC")).Trim();
                    if (objSysSettings.MultiCovgPerClm == -1)       //For Carrier Claim
                    {
                        if (objReader.GetValue("FIRST_NAME") != null)
                            sFullName = Convert.ToString(objReader.GetValue("FIRST_NAME")).Trim();
                        if (objReader.GetValue("LAST_NAME") != null)
                            sFullName = string.Concat(sFullName.Trim(), " ", Convert.ToString(objReader.GetValue("LAST_NAME")).Trim()).Trim();
                    }
                }
            }
            if (objSysSettings.MultiCovgPerClm == -1)       //For Carrier Claim
            {
                if (!string.IsNullOrEmpty(sFullName))
                    sReserveAssignName = string.Concat(sFullName, "/");
                sReserveAssignName = string.Concat(sReserveAssignName, sCodeDesc);
            }
            else
                sReserveAssignName = sCodeDesc;

            return sReserveAssignName;
        }

        private string GetPropertyLossAssignName(int iRowID)
        {
            string sPropertyAssignName = Constants.WPA_DIARY_ENTRY_ATTACH_TABLE.PROPERTYLOSS.ToString();
            StringBuilder sSQL = new StringBuilder();

            sSQL.Append("SELECT PIN FROM PROPERTY_UNIT ");
            sSQL.Append(" WHERE PROPERTY_ID = ( ");
            sSQL.Append(" SELECT PROPERTY_ID FROM CLAIM_X_PROPERTYLOSS ");
            sSQL.Append(" WHERE ROW_ID = " + iRowID);
            sSQL.Append(" ) ");


            using (DbReader objReader = DbFactory.ExecuteReader(m_sConnectionString, sSQL.ToString()))
            {
                if (objReader.Read())
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(objReader.GetValue("PIN")).Trim()))
                        sPropertyAssignName = string.Concat(sPropertyAssignName, " ", objReader.GetValue("PIN")).Trim();
                }
            }

            return sPropertyAssignName;
        }

        private string GetVehicleAssignName(int iRowID)
        {
            string sVehicleAssignName = Constants.WPA_DIARY_ENTRY_ATTACH_TABLE.UNIT.ToString();
            StringBuilder sSQL = new StringBuilder();

            sSQL.Append("SELECT VIN FROM VEHICLE ");
            sSQL.Append(" WHERE UNIT_ID = ( ");
            sSQL.Append(" SELECT UNIT_ID FROM UNIT_X_CLAIM ");
            sSQL.Append(" WHERE UNIT_ROW_ID = " + iRowID);
            sSQL.Append(" ) ");


            using (DbReader objReader = DbFactory.ExecuteReader(m_sConnectionString, sSQL.ToString()))
            {
                if (objReader.Read())
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(objReader.GetValue("VIN")).Trim()))
                        sVehicleAssignName = string.Concat(sVehicleAssignName, " ", objReader.GetValue("VIN")).Trim();
                }
            }

            return sVehicleAssignName;
        }

        private string GetSiteLossAssignName(int iRowID)
        {
            string sSiteAssignName = Constants.WPA_DIARY_ENTRY_ATTACH_TABLE.SITELOSS.ToString();
            StringBuilder sSQL = new StringBuilder();

            sSQL.Append("SELECT SITE_NUMBER FROM SITE_UNIT ");
            sSQL.Append(" WHERE SITE_ID = ( ");
            sSQL.Append(" SELECT SITE_ID FROM CLAIM_X_SITELOSS ");
            sSQL.Append(" WHERE ROW_ID = " + iRowID);
            sSQL.Append(" ) ");


            using (DbReader objReader = DbFactory.ExecuteReader(m_sConnectionString, sSQL.ToString()))
            {
                if (objReader.Read())
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(objReader.GetValue("SITE_NUMBER")).Trim()))
                        sSiteAssignName = string.Concat(sSiteAssignName, " ", objReader.GetValue("SITE_NUMBER")).Trim();
                }
            }

            return sSiteAssignName;
        }
        //Ankit End
		private bool IsSortableSuppFieldType(SupplementalField objSupp)
		{
			if(objSupp ==null)
				return false;
			if(objSupp.IsMultiValue)
				return false;
			if((objSupp.FieldType== SupplementalFieldTypes.SuppTypeClaimLookup) ||
				(objSupp.FieldType== SupplementalFieldTypes.SuppTypeEntity) ||
				(objSupp.FieldType== SupplementalFieldTypes.SuppTypeEventLookup) ||
				(objSupp.FieldType== SupplementalFieldTypes.SuppTypeFreeText) ||
				(objSupp.FieldType== SupplementalFieldTypes.SuppTypeMemo) ||
				(objSupp.FieldType== SupplementalFieldTypes.SuppTypeText) ||
				(objSupp.FieldType== SupplementalFieldTypes.SuppTypeVehicleLookup))
				return false;

			return true;
		}
		/// <summary>
		/// GetSQLFromFormName returns the SQL query corresponding to the form from which
		/// Look Up Data has been evoked.
		/// </summary>
		/// <param name="p_sFormName">form name from which the screen is called</param>
		/// <param name="p_objXmlDoc">XML document</param>
		/// <param name="p_sPrimaryField">Primary Field</param>
		/// <param name="p_sSecondaryField">Secondary Field</param>
		/// <param name="p_sThirdField">Third Field</param>
		/// <param name="p_sForthField">Forth Field</param>
		/// <param name="p_sPrimaryFormField">Primary Form Field</param>
		/// <param name="p_sSecondaryFormField">Secondary Form Field</param>
		/// <param name="p_sThirdFormField">Third Form Field</param>
		/// <param name="p_arrlstHiddenFields">Hidden Fields</param>
		/// <returns>SQL query</returns>
		//smahajan6 - MITS #18230 - 11/18/2009 :Start - Added Additional Argument , p_arrlstHiddenFields
		private string GetSQLFromFormName(string p_sFormName, XmlDocument p_objXmlDoc, ref string p_sPrimaryField, 
			ref string p_sSecondaryField, ref string p_sThirdField, ref string p_sForthField, 
			ref string p_sPrimaryFormField, ref string p_sSecondaryFormField, ref string p_sThirdFormField,ref ArrayList p_arrlstHiddenFields)
		//private string GetSQLFromFormName(string p_sFormName, XmlDocument p_objXmlDoc, ref string p_sPrimaryField, 
		//	ref string p_sSecondaryField, ref string p_sThirdField, ref string p_sForthField, 
		//	ref string p_sPrimaryFormField, ref string p_sSecondaryFormField, ref string p_sThirdFormField)
		//smahajan6 - MITS #18230 - 11/18/2009 :End
		{
            StringBuilder strSql = null;
			string sSQL = string.Empty;
			int iAccountId = 0;
            //Sumit - Start(05/20/2010) - MITS# 20484
            int iPolicyId=0;
            string sExposureIds = string.Empty;
            DbReader objReader = null;
            //Sumit - End
            string sId = String.Empty;
            LocalCache objLocalCache = null;
            int iTableId = 0;
            int iCodeId = 0;
            string sDBType = string.Empty;
           
			try
			{
                Riskmaster.Settings.SysSettings objSettings = new Riskmaster.Settings.SysSettings(m_sConnectionString, m_iClientId);
                sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString();//skhare7 Point Policy interface
          
				switch (p_sFormName)
				{
					case "event":
						sSQL = "SELECT EVENT.EVENT_ID, EVENT.EVENT_NUMBER, EVENT.DATE_OF_EVENT, EV_STATUS.CODE_DESC | EVENT_STATUS, EV_INDICATOR.CODE_DESC | EV_INDICATOR_CODE ,DEPT.LAST_NAME | DEPARTMENT ";
						sSQL = sSQL + " FROM EVENT, ENTITY | DEPT, CODES_TEXT | EV_STATUS, CODES_TEXT | EV_INDICATOR ";
						sSQL = sSQL + " WHERE EVENT.DEPT_EID = DEPT.ENTITY_ID AND EVENT.EVENT_ID <> 0 AND EVENT.EVENT_NUMBER IS NOT NULL ";
						sSQL = sSQL + " AND EVENT.EVENT_STATUS_CODE=EV_STATUS.CODE_ID AND EVENT.EVENT_IND_CODE=EV_INDICATOR.CODE_ID ";
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY EVENT.EVENT_NUMBER ";
							}
						}
						else
							sSQL = sSQL + " ORDER BY EVENT.EVENT_NUMBER ";
						p_sPrimaryField = "EVENT_ID";
						p_sPrimaryFormField = "eventid";
						break;
					case "eventdatedtext":
                        //Raman : modified for R5
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT EV_DT_ROW_ID, ENTERED_BY_USER, DATE_ENTERED, TIME_ENTERED, DATED_TEXT FROM EVENT_X_DATED_TEXT WHERE EVENT_ID = "
                                + p_objXmlDoc.SelectSingleNode("//eventid").Attributes.GetNamedItem("value").InnerText;
                        }
                        else
                        {
                            sSQL = "SELECT EV_DT_ROW_ID, ENTERED_BY_USER, DATE_ENTERED, TIME_ENTERED, DATED_TEXT FROM EVENT_X_DATED_TEXT WHERE EVENT_ID = "
                                + m_sParentID;
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY DATE_ENTERED";
							}
						}
						else
							sSQL = sSQL + " ORDER BY DATE_ENTERED";
						p_sPrimaryField="EV_DT_ROW_ID";
						p_sPrimaryFormField="evdtrowid";
						break;
					case "claimgc":
						sSQL="SELECT CLAIM_ID, CLAIM_NUMBER, DATE_OF_CLAIM, CODES1.CODE_DESC | CLAIM_TYPE, CODES2.CODE_DESC | CLAIM_STATUS FROM CLAIM, CODES_TEXT | CODES1, CODES_TEXT | CODES2 WHERE CLAIM.LINE_OF_BUS_CODE=241 AND CLAIM_TYPE_CODE=CODES1.CODE_ID AND CLAIM_STATUS_CODE=CODES2.CODE_ID";
						p_sPrimaryField="CLAIM_ID";
						p_sPrimaryFormField="claimid";
						break;
					case "claimva":
						sSQL="SELECT CLAIM_ID, CLAIM_NUMBER, DATE_OF_CLAIM, CODES1.CODE_DESC | CLAIM_TYPE, CODES2.CODE_DESC | CLAIM_STATUS FROM CLAIM, CODES_TEXT | CODES1, CODES_TEXT | CODES2 WHERE CLAIM.LINE_OF_BUS_CODE=242 AND CLAIM_TYPE_CODE=CODES1.CODE_ID AND CLAIM_STATUS_CODE=CODES2.CODE_ID";
						p_sPrimaryField="CLAIM_ID";
						p_sPrimaryFormField="claimid";
						break;
					case "claimwc":
						sSQL="SELECT CLAIM_ID, CLAIM_NUMBER, DATE_OF_CLAIM, CODES1.CODE_DESC | CLAIM_TYPE, CODES2.CODE_DESC | CLAIM_STATUS FROM CLAIM, CODES_TEXT | CODES1, CODES_TEXT | CODES2 WHERE CLAIM.LINE_OF_BUS_CODE=243 AND CLAIM_TYPE_CODE=CODES1.CODE_ID AND CLAIM_STATUS_CODE=CODES2.CODE_ID";
						p_sPrimaryField="CLAIM_ID";
						p_sPrimaryFormField="claimid";
						break;
					case "claimdi":
						sSQL="SELECT CLAIM_ID, CLAIM_NUMBER, DATE_OF_CLAIM, CODES1.CODE_DESC | CLAIM_TYPE, CODES2.CODE_DESC | CLAIM_STATUS FROM CLAIM, CODES_TEXT | CODES1, CODES_TEXT | CODES2 WHERE CLAIM.LINE_OF_BUS_CODE=844 AND CLAIM_TYPE_CODE=CODES1.CODE_ID AND CLAIM_STATUS_CODE=CODES2.CODE_ID";
						p_sPrimaryField="CLAIM_ID";
						p_sPrimaryFormField="claimid";
						break;
                    //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                    case "adjuster":
                    case "claimadjuster":
                        string sPrimaryCol = "CA.ADJ_ROW_ID";
                        if (string.Equals(p_sFormName, "claimadjuster", StringComparison.OrdinalIgnoreCase) && m_sParentID == string.Empty)
                        {
                            m_sParentID = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("claimid").InnerText;
                            sPrimaryCol = string.Concat("CA.ADJUSTER_EID", " ADJ_ROW_ID");
                        }
                        //Ankit End
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT ADJ_ROW_ID, LAST_NAME, FIRST_NAME, CURRENT_ADJ_FLAG | CURRENT_FLAG,CLAIM_ADJUSTER.ADJUSTER_TYPE FROM CLAIM_ADJUSTER, ENTITY WHERE CLAIM_ID = "
                                + p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText
                                + " AND ENTITY.ENTITY_ID = ADJUSTER_EID";
                        }
                        else
                        {
                            //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                            string sConcat = "+";
                            if (sDBType == Constants.DB_ORACLE)
                                sConcat = "||";

                            sSQL = "SELECT " + sPrimaryCol + ", E.LAST_NAME, E.FIRST_NAME, ";
                            if (!string.Equals(p_sFormName, "claimadjuster", StringComparison.OrdinalIgnoreCase))
                                sSQL += " CT.SHORT_CODE | ASSIGNMENT_LEVEL, (CAA.ATTACH_TABLE " + sConcat + " ' ' " + sConcat + " CAST(CAA.ATTACH_RECORDID AS VARCHAR(50))) | ASSIGNMENT_NAME, ";
                            sSQL += " CA.CURRENT_ADJ_FLAG | CURRENT_FLAG ,CA.ADJUSTER_TYPE";
                            sSQL += " FROM CLAIM_ADJUSTER CA ";
                            sSQL += " INNER JOIN ENTITY E ON E.ENTITY_ID = CA.ADJUSTER_EID ";
                            if (!string.Equals(p_sFormName, "claimadjuster", StringComparison.OrdinalIgnoreCase))
                            {
                                sSQL += " LEFT OUTER JOIN CLAIM_ADJ_ASSIGNMENT CAA ON CAA.ADJ_ROW_ID = CA.ADJ_ROW_ID ";
                                sSQL += " LEFT OUTER JOIN CODES_TEXT CT ON CT.CODE_ID = CAA.ASSIGNMENT_LEVEL_CODE_ID ";
                            }
                            sSQL += " WHERE CLAIM_ID = " + m_sParentID;
                            //Ankit End
                        }
						p_sPrimaryField="ADJ_ROW_ID";
						p_sPrimaryFormField="adjrowid";
						break;
					case "adjusterdatedtext":
                        //Added for REM-Nadim(5/4/2009)
                        XmlDocument objXml=null;
                        string sText = string.Empty;
                        string[] sArrCodeFilter; 
                        string sTempCode=string.Empty;
                         objXml = new XmlDocument();
                         string strContent = RMSessionManager.GetCustomContent(m_iClientId);
                             if (! string.IsNullOrEmpty(strContent))
                             {
                                 objXml.LoadXml(strContent);
                                 sText = objXml.SelectSingleNode("//RMAdminSettings/SpecialSettings/text").InnerText;
                             }
                         
                      
                         if (sText != "")
                         {
                             sArrCodeFilter = sText.Split(',');
                             if (sArrCodeFilter.Length > 0)
                             {
                                 for (int iCodeCount = 0; iCodeCount < sArrCodeFilter.Length; iCodeCount++)
                                 {
                                     sTempCode += "'" + sArrCodeFilter[iCodeCount].Trim().ToUpper() + "',";
                                 }
                                 sText = sTempCode.Remove(sTempCode.Length - 1, 1);
                             }

                             if (m_sParentID == String.Empty)
                             {//Parijat:18962
                                 sSQL = "SELECT ADJ_DTTEXT_ROW_ID, DATE_ENTERED,CODE_DESC TEXT_TYPE, DATED_TEXT FROM ADJUST_DATED_TEXT,CODES_TEXT WHERE ADJ_ROW_ID =" + p_objXmlDoc.SelectSingleNode("//adjrowid").Attributes.GetNamedItem("value").InnerText + " AND CODES_TEXT.CODE_ID IN (SELECT CODE_ID FROM CODES_TEXT WHERE SHORT_CODE IN(" + sText + ")) AND TEXT_TYPE_CODE=CODES_TEXT.CODE_ID";
                                 
                             }
                             else
                             {//Parijat:18962
                                 sSQL = "SELECT ADJ_DTTEXT_ROW_ID, DATE_ENTERED,CODE_DESC TEXT_TYPE, DATED_TEXT FROM ADJUST_DATED_TEXT,CODES_TEXT WHERE ADJ_ROW_ID =" + m_sParentID + " AND CODES_TEXT.CODE_ID IN (SELECT CODE_ID FROM CODES_TEXT WHERE SHORT_CODE IN(" + sText + ")) AND TEXT_TYPE_CODE=CODES_TEXT.CODE_ID";
                                 
                             }


                         }
                         else
                         {
                             if (m_sParentID == String.Empty)
                             {//Parijat:18962
                                 sSQL = "SELECT ADJ_DTTEXT_ROW_ID, DATE_ENTERED,CODE_DESC TEXT_TYPE, DATED_TEXT FROM ADJUST_DATED_TEXT,CODES_TEXT WHERE ADJ_ROW_ID = "
                                 + p_objXmlDoc.SelectSingleNode("//adjrowid").Attributes.GetNamedItem("value").InnerText + " AND TEXT_TYPE_CODE=CODES_TEXT.CODE_ID";
                             }
                             else
                             {//Parijat:18962
                                 sSQL = "SELECT ADJ_DTTEXT_ROW_ID, DATE_ENTERED,CODE_DESC TEXT_TYPE, DATED_TEXT FROM ADJUST_DATED_TEXT,CODES_TEXT WHERE ADJ_ROW_ID = "
                                 + m_sParentID + " AND TEXT_TYPE_CODE=CODES_TEXT.CODE_ID";
                             }
                         }
                         //Added for REM(5/4/2009)
                        //if (m_sParentID == String.Empty)
                        //{
                        //    sSQL = "SELECT ADJ_DTTEXT_ROW_ID, DATE_ENTERED, DATED_TEXT FROM ADJUST_DATED_TEXT WHERE ADJ_ROW_ID = "
                        //    + p_objXmlDoc.SelectSingleNode("//adjrowid").Attributes.GetNamedItem("value").InnerText;
                        //}
                        //else
                        //{
                        //    sSQL = "SELECT ADJ_DTTEXT_ROW_ID, DATE_ENTERED, DATED_TEXT FROM ADJUST_DATED_TEXT WHERE ADJ_ROW_ID = "
                        //    + m_sParentID;
                        //}
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY DATE_ENTERED";
							}
						} 
						else
							sSQL = sSQL + " ORDER BY DATE_ENTERED";
						p_sPrimaryField="ADJ_DTTEXT_ROW_ID";
						p_sPrimaryFormField="adjdttextrowid";
						break;
					case "litigation":
                        //pmittal5 Mits 17628 10/13/09 - Modified SQL queries to display Attorney Firm and Attorney Last Name also along with Docket Number
                        
                        /*nnorouzi MITS 19485 (revising MITS 17628) Jan. 18, 2010
                         * need to use Outer Join so that if Attorney or Attorney_Firm are not there 
                         * then still include the litigation record in results 
                         * this way number of records in navTree and lookup result match together
                         * and also clients will know if there are missing data or broken links in database.
                        */

                        if (m_sParentID != String.Empty)
                        {
                            sId = m_sParentID;
                        }
                        else if (p_objXmlDoc.SelectSingleNode("//claimid") != null)
                        {
                            sId = p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText;
                        }
                        else if (p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"] != null)
                        {
                            sId = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("claimid").InnerText;
                        }

                        strSql = new StringBuilder();

                        if (sId != string.Empty)
                        {
                            strSql.Append(string.Format("SELECT {0} FROM {1} LEFT OUTER JOIN {2} ON {3} LEFT OUTER JOIN {4} ON {5} WHERE {6}"
                                , "LITIGATION_ROW_ID, DOCKET_NUMBER, ATTORNEY_FIRM.LAST_NAME | ATTORNEY_FIRM, ATTORNEY.LAST_NAME | ATTORNEY, SUIT_DATE, COURT_DATE"
                                , "CLAIM_X_LITIGATION"
                                , "ENTITY | ATTORNEY"
                                , "ATTORNEY.ENTITY_ID = CLAIM_X_LITIGATION.CO_ATTORNEY_EID"
                                , "ENTITY | ATTORNEY_FIRM "
                                , "ATTORNEY_FIRM.ENTITY_ID = ATTORNEY.PARENT_EID"
                                , "CLAIM_ID = ")
                            );

                            strSql.Append(sId);

                            //End - pmittal5 Mits 17628
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                            {
                                if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                                {
                                    strSql.Append(" ORDER BY LITIGATION_ROW_ID");
                                }
                            }
                            else
                                strSql.Append(" ORDER BY LITIGATION_ROW_ID");

                            p_sPrimaryField = "LITIGATION_ROW_ID";
                            p_sPrimaryFormField = "litigationrowid";
                        }
						break;
                    case "subrogation":

                        if (m_sParentID != String.Empty)
                        {
                            sId = m_sParentID;
                        }
                        else if (p_objXmlDoc.SelectSingleNode("//claimid") != null)
                        {
                            sId = p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText;
                        }
                        else if (p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"] != null)
                        {
                            sId = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("claimid").InnerText;
                        }

                        strSql = new StringBuilder();

                        if (sId != string.Empty)
                        {
                            strSql.Append(string.Format("SELECT {0} FROM {1} LEFT OUTER JOIN {2} ON {3} LEFT OUTER JOIN {4} ON {5} LEFT OUTER JOIN {6} ON {7} WHERE {8}"
                                , "SUBROGATION_ROW_ID, TYPE.CODE_DESC | TYPE, SPECIALIST.LAST_NAME | SPECIALIST, STATUS.CODE_DESC | STATUS, STATUS_DATE"
                                , "CLAIM_X_SUBRO"
                                , "CODES_TEXT | TYPE"
                                , "TYPE.CODE_ID = CLAIM_X_SUBRO.SUB_TYPE_CODE"
                                , "CODES_TEXT | STATUS"
                                , "STATUS.CODE_ID = CLAIM_X_SUBRO.SUB_STATUS_CODE"
                                , "ENTITY | SPECIALIST"
                                , "SPECIALIST.ENTITY_ID = CLAIM_X_SUBRO.SUB_SPECIALIST_EID"
                                , "CLAIM_ID = ")
                            );

                            strSql.Append(sId);

                            //End - pmittal5 Mits 17628
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                            {
                                if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                                {
                                    strSql.Append(" ORDER BY SUBROGATION_ROW_ID");
                                }
                            }
                            else
                                strSql.Append(" ORDER BY SUBROGATION_ROW_ID");

                            p_sPrimaryField = "SUBROGATION_ROW_ID";
                            p_sPrimaryFormField = "subrogationrowid";
                        }
                        break;
                    //rupal:start for R8 enhancement of Arbitration
                    case "arbitration":

                        if (m_sParentID != String.Empty)
                        {
                            sId = m_sParentID;
                        }
                        else if (p_objXmlDoc.SelectSingleNode("//claimid") != null)
                        {
                            sId = p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText;
                        }
                        else if (p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"] != null)
                        {
                            sId = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("claimid").InnerText;
                        }

                        strSql = new StringBuilder();

                        if (sId != string.Empty)
                        {
                            strSql.Append(string.Format("SELECT {0} FROM {1} LEFT OUTER JOIN {2} ON {3} LEFT OUTER JOIN {4} ON {5} LEFT OUTER JOIN {6} ON {7} WHERE {8}"
                                , "ARBITRATION_ROW_ID, TYPE.CODE_DESC | TYPE, OWNER.LAST_NAME | ADVERSE_PARTY , FILED_DATE | DATE_FILED, STATUS.CODE_DESC | STATUS"
                                , "CLAIM_X_ARB"
                                , "CODES_TEXT | TYPE"
                                , "TYPE.CODE_ID = CLAIM_X_ARB.ARB_TYPE_CODE"
                                , "ENTITY | OWNER"
                                , "OWNER.ENTITY_ID = CLAIM_X_ARB.ADVERSE_NAME_EID"
                                , "CODES_TEXT | STATUS"
                                , "STATUS.CODE_ID = CLAIM_X_ARB.ARB_STATUS_CODE"
                                , "CLAIM_ID = ")
                            );

                            strSql.Append(sId);


                            if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                            {
                                if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                                {
                                    strSql.Append(" ORDER BY ARBITRATION_ROW_ID");
                                }
                            }
                            else
                                strSql.Append(" ORDER BY ARBITRATION_ROW_ID");

                            p_sPrimaryField = "ARBITRATION_ROW_ID";
                            p_sPrimaryFormField = "arbitrationrowid";
                        }
                        break;
                    //rupal:end
                    case "propertyloss":
                        //Added by Amitosh for R8 enhancement of PropertyLoss
                        if (m_sParentID != String.Empty)
                        {
                            sId = m_sParentID;
                        }
                        else if (p_objXmlDoc.SelectSingleNode("//claimid") != null)
                        {
                            sId = p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText;
                        }
                        else if (p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"] != null)
                        {
                            sId = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("claimid").InnerText;
                        }

                        strSql = new StringBuilder();
                        //skhare7 Point Policy interface changes for PIN
                        if (sId != string.Empty)
                        {

                            if (!objSettings.UsePolicyInterface)
                            {
                                strSql.Append(string.Format("SELECT {0} FROM {1} LEFT OUTER JOIN {2} ON {3} LEFT OUTER JOIN {4} ON {5} INNER JOIN {6} ON {7} WHERE {8}"
                                 , "ROW_ID,PROPERTY_UNIT.PIN, TYPE.CODE_DESC | TYPE, ESTDAMAGE | ESTIMATED_DAMAGE, OWNER.LAST_NAME | OWNER, DATEREPORTED,(CASE WHEN INSURED=0 THEN 'NO' WHEN INSURED=-1 THEN 'YES' WHEN INSURED=-2 THEN 'NA' ELSE 'NO' END) |INSURED "
                                 , "CLAIM_X_PROPERTYLOSS"
                                 , "CODES_TEXT | TYPE"
                                 , "TYPE.CODE_ID = CLAIM_X_PROPERTYLOSS.PROPERTYTYPE"
                                 , "ENTITY | OWNER"
                                 , "OWNER.ENTITY_ID = CLAIM_X_PROPERTYLOSS.OWNER"
                                 , "PROPERTY_UNIT"
                                 , " PROPERTY_UNIT.PROPERTY_ID=CLAIM_X_PROPERTYLOSS.PROPERTY_ID"
                                 , "CLAIM_ID = ")
                                    );
                                strSql.Append(sId);
                            }
                            else
                            {

                                if (sDBType == Constants.DB_SQLSRVR)
                                {
                                    strSql.Append(string.Format("SELECT {0} FROM {1} LEFT OUTER JOIN {2} ON {3} LEFT OUTER JOIN {4} ON {5} INNER JOIN {6} ON {7} LEFT OUTER JOIN {8} ON {9} LEFT OUTER JOIN {10} ON {11}  WHERE {12} AND {13} "
                                     , "CLAIM_X_PROPERTYLOSS.ROW_ID,PROPERTY_UNIT.PIN, TYPE.CODE_DESC | TYPE, ESTDAMAGE | ESTIMATED_DAMAGE, OWNER.LAST_NAME | OWNER, DATEREPORTED,(CASE WHEN INSURED=0 THEN 'NO' WHEN INSURED=-1 THEN 'YES' WHEN INSURED=-2 THEN 'NA' ELSE 'NO' END) |INSURED,(CASE WHEN POINT_UNIT_DATA.STAT_UNIT_NUMBER IS NULL THEN INTEGRAL_UNIT_DATA.UNIT_NUMBER ELSE (POINT_UNIT_DATA.STAT_UNIT_NUMBER+' / '+POINT_UNIT_DATA.UNIT_RISK_LOC+' / '+POINT_UNIT_DATA.UNIT_RISK_SUB_LOC) END) |UNITNO "

                                     , "CLAIM_X_PROPERTYLOSS"
                                     , "CODES_TEXT | TYPE"
                                     , "TYPE.CODE_ID = CLAIM_X_PROPERTYLOSS.PROPERTYTYPE"
                                     , "ENTITY | OWNER"
                                     , "OWNER.ENTITY_ID = CLAIM_X_PROPERTYLOSS.OWNER"
                                     , "PROPERTY_UNIT"
                                     , " PROPERTY_UNIT.PROPERTY_ID=CLAIM_X_PROPERTYLOSS.PROPERTY_ID"
                                     , "POINT_UNIT_DATA"
                                     , "PROPERTY_UNIT.PROPERTY_ID=POINT_UNIT_DATA.UNIT_ID AND POINT_UNIT_DATA.UNIT_TYPE='P'"
                                     , "INTEGRAL_UNIT_DATA"
                                     , "PROPERTY_UNIT.PROPERTY_ID=INTEGRAL_UNIT_DATA.UNIT_ID AND INTEGRAL_UNIT_DATA.UNIT_TYPE='P'"//vkumar258 - added to get integral data
                                     , "CLAIM_X_PROPERTYLOSS.INSURED <> -2 "
                                     , "CLAIM_ID = ")
                                     );

                                    strSql.Append(sId);
                                }
                                else if(sDBType == Constants.DB_ORACLE)
                                {

                                    strSql.Append(string.Format("SELECT {0} FROM {1} LEFT OUTER JOIN {2} ON {3} LEFT OUTER JOIN {4} ON {5} INNER JOIN {6} ON {7} LEFT OUTER JOIN {8} ON {9} LEFT OUTER JOIN {10} ON {11}  WHERE {12} AND {13} "
                                        , "CLAIM_X_PROPERTYLOSS.ROW_ID,PROPERTY_UNIT.PIN, TYPE.CODE_DESC | TYPE, ESTDAMAGE | ESTIMATED_DAMAGE, OWNER.LAST_NAME | OWNER, DATEREPORTED,(CASE WHEN INSURED=0 THEN 'NO' WHEN INSURED=-1 THEN 'YES' WHEN INSURED=-2 THEN 'NA' ELSE 'NO' END) |INSURED,(CASE WHEN POINT_UNIT_DATA.STAT_UNIT_NUMBER IS NULL THEN INTEGRAL_UNIT_DATA.UNIT_NUMBER ELSE (POINT_UNIT_DATA.STAT_UNIT_NUMBER||' / '||POINT_UNIT_DATA.UNIT_RISK_LOC||' / '||POINT_UNIT_DATA.UNIT_RISK_SUB_LOC) END)|UNITNO "

                                        , "CLAIM_X_PROPERTYLOSS"
                                        , "CODES_TEXT | TYPE"
                                        , "TYPE.CODE_ID = CLAIM_X_PROPERTYLOSS.PROPERTYTYPE"
                                        , "ENTITY | OWNER"
                                        , "OWNER.ENTITY_ID = CLAIM_X_PROPERTYLOSS.OWNER"
                                        , "PROPERTY_UNIT"
                                        , " PROPERTY_UNIT.PROPERTY_ID=CLAIM_X_PROPERTYLOSS.PROPERTY_ID"
                                        , "POINT_UNIT_DATA"
                                        , "PROPERTY_UNIT.PROPERTY_ID=POINT_UNIT_DATA.UNIT_ID AND POINT_UNIT_DATA.UNIT_TYPE='P'"
                                        , "INTEGRAL_UNIT_DATA"
                                        , "PROPERTY_UNIT.PROPERTY_ID=INTEGRAL_UNIT_DATA.UNIT_ID AND INTEGRAL_UNIT_DATA.UNIT_TYPE='P'"//vkumar258 - added to get integral data
                                        , "CLAIM_X_PROPERTYLOSS.INSURED <> -2 "
                                        , "CLAIM_ID = ")
                                        );

                                    strSql.Append(sId);
                                
                                }
                            }

                        
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                            {
                                if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                                {
                                    strSql.Append(" ORDER BY ROW_ID");
                                }
                            }
                            else
                                strSql.Append(" ORDER BY ROW_ID");

                            p_sPrimaryField = "ROW_ID";
                            p_sPrimaryFormField = "plrowid";
                        }
                        break;
                        //end Amitosh
                    //rupal:start,policy system interface
                    case "siteloss":
                        
                        if (m_sParentID != String.Empty)
                        {
                            sId = m_sParentID;
                        }
                        else if (p_objXmlDoc.SelectSingleNode("//claimid") != null)
                        {
                            sId = p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText;
                        }
                        else if (p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"] != null)
                        {
                            sId = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("claimid").InnerText;
                        }

                        strSql = new StringBuilder();

                        if (sId != string.Empty)
                        {
                            if (!objSettings.UsePolicyInterface)
                            {
                                strSql.Append(string.Format("SELECT {0} FROM {1} INNER JOIN {2} ON {3} WHERE {4}"
                                    , "CLAIM_X_SITELOSS.ROW_ID,SITE_NUMBER , NAME ,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END)|INSURED  "
                                    , "CLAIM_X_SITELOSS"
                                    , "SITE_UNIT"
                                    , "CLAIM_X_SITELOSS.SITE_ID = SITE_UNIT.SITE_ID"
                                    , "CLAIM_ID = ")
                                );
                            }
                            else
                            {
                                if (sDBType == Constants.DB_SQLSRVR)
                                {
                                    strSql.Append(string.Format("SELECT {0} FROM {1} INNER JOIN {2} ON {3} LEFT OUTER JOIN {4} ON {5} LEFT OUTER JOIN {6} ON {7} WHERE {8} AND {9}"
                                        , "CLAIM_X_SITELOSS.ROW_ID,SITE_NUMBER , NAME ,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END)|INSURED ,(CASE WHEN POINT_UNIT_DATA.STAT_UNIT_NUMBER IS NULL THEN INTEGRAL_UNIT_DATA.UNIT_NUMBER ELSE (POINT_UNIT_DATA.STAT_UNIT_NUMBER+' / '+POINT_UNIT_DATA.UNIT_RISK_LOC+' / '+POINT_UNIT_DATA.UNIT_RISK_SUB_LOC) END)|UNITNO "
                                         , "CLAIM_X_SITELOSS"
                                         , "SITE_UNIT"
                                         , "CLAIM_X_SITELOSS.SITE_ID = SITE_UNIT.SITE_ID"
                                         , "POINT_UNIT_DATA"
                                         , "SITE_UNIT.SITE_ID=POINT_UNIT_DATA.UNIT_ID AND  POINT_UNIT_DATA.UNIT_TYPE ='S'"
                                          , "INTEGRAL_UNIT_DATA"
                                         , "SITE_UNIT.SITE_ID=INTEGRAL_UNIT_DATA.UNIT_ID AND  INTEGRAL_UNIT_DATA.UNIT_TYPE ='S'"//vkumar258 - added to get integral data
                                         , "CLAIM_X_SITELOSS.ISINSURED <> -2"
                                         , "CLAIM_ID = ")
                                     );
                                   
                                }
                                else if (sDBType == Constants.DB_ORACLE)
                                {
                                    strSql.Append(string.Format("SELECT {0} FROM {1} INNER JOIN {2} ON {3} LEFT OUTER JOIN {4} ON {5} LEFT OUTER JOIN {6} ON {7} WHERE {8} AND {9}"
                                              , "CLAIM_X_SITELOSS.ROW_ID,SITE_NUMBER , NAME ,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END)|INSURED ,(CASE WHEN POINT_UNIT_DATA.STAT_UNIT_NUMBER IS NULL THEN INTEGRAL_UNIT_DATA.UNIT_NUMBER ELSE (POINT_UNIT_DATA.STAT_UNIT_NUMBER||' / '||POINT_UNIT_DATA.UNIT_RISK_LOC||' / '||POINT_UNIT_DATA.UNIT_RISK_SUB_LOC ) END)|UNITNO"
                                             , "CLAIM_X_SITELOSS"
                                             , "SITE_UNIT"
                                             , "CLAIM_X_SITELOSS.SITE_ID = SITE_UNIT.SITE_ID"
                                             , "POINT_UNIT_DATA"
                                             , "SITE_UNIT.SITE_ID=POINT_UNIT_DATA.UNIT_ID AND  POINT_UNIT_DATA.UNIT_TYPE ='S'"
                                             , "INTEGRAL_UNIT_DATA"
                                             , "SITE_UNIT.SITE_ID=INTEGRAL_UNIT_DATA.UNIT_ID AND  INTEGRAL_UNIT_DATA.UNIT_TYPE ='S'"//vkumar258 - added to get integral data
                                             , "CLAIM_X_SITELOSS.ISINSURED <> -2"
                                             ," CLAIM_ID = ")
                                         );
                                
                                }
                            
                            }
                            strSql.Append(sId);
                                


                            if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                            {
                                if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                                {
                                    strSql.Append(" ORDER BY ROW_ID");
                                }
                            }
                            else
                                strSql.Append(" ORDER BY ROW_ID");

                            p_sPrimaryField = "ROW_ID";
                            p_sPrimaryFormField = "slrowid";
                        }
                        break;
                    case "otherunitloss":
                        if (m_sParentID != String.Empty)
                        {
                            sId = m_sParentID;
                        }
                        else if (p_objXmlDoc.SelectSingleNode("//claimid") != null)
                        {
                            sId = p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText;
                        }
                        else if (p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"] != null)
                        {
                            sId = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("claimid").InnerText;
                        }

                        strSql = new StringBuilder();

                        if (sId != string.Empty)
                        {
                            if (!objSettings.UsePolicyInterface)
                            {
                                strSql.Append(string.Format("SELECT {0} FROM {1} INNER JOIN {2} ON {3} INNER JOIN {4} ON {5} WHERE {6}"
                                    //dbisht6 merging sangeet's change for mits 35655
                                    , "E.ENTITY_ID, E.LAST_NAME, E.ABBREVIATION, E.TAX_ID, E.PHONE1 | PHONE_NUMBER,O.UNIT_TYPE ,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END)|INSURED"
                                    //dbisht6 end
                                    , "CLAIM_X_OTHERUNIT CO"
                                    , "OTHER_UNIT O"
                                    , "CO.OTHER_UNIT_ID = O.OTHER_UNIT_ID"
                                    , "ENTITY E"
                                    , "E.ENTITY_ID = O.ENTITY_ID"

                                    , " ISINSURED <>-2 AND CO.CLAIM_ID = ") //aaggarwal29 : Added for MITS 37196, No Need to display units with insured flag = -2 
                                );
                            }
                            else
                            {
                                if (sDBType == Constants.DB_SQLSRVR)


                                    strSql.Append(string.Format("SELECT {0} FROM {1} INNER JOIN {2} ON {3} INNER JOIN {4} ON {5} LEFT OUTER JOIN {6} ON {7} LEFT OUTER JOIN {8} ON {9} WHERE {10}"
                                        //dbisht6 merging sangeet's change for mits 35655
                                        , "E.ENTITY_ID, E.LAST_NAME,E.REFERENCE_NUMBER|'VIN-ADDRESS' ,E.ABBREVIATION, E.TAX_ID, E.PHONE1 | PHONE_NUMBER,O.UNIT_TYPE ,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END)|INSURED ,(CASE WHEN POINT_UNIT_DATA.STAT_UNIT_NUMBER IS NULL THEN INTEGRAL_UNIT_DATA.UNIT_NUMBER ELSE (POINT_UNIT_DATA.STAT_UNIT_NUMBER+' / '+POINT_UNIT_DATA.UNIT_RISK_LOC+' / '+POINT_UNIT_DATA.UNIT_RISK_SUB_LOC) END)|UNITNO "
                                        //dbisht6 end    
                                        , "CLAIM_X_OTHERUNIT CO"
                                            , "OTHER_UNIT O"
                                            , "CO.OTHER_UNIT_ID = O.OTHER_UNIT_ID"
                                            , "ENTITY E"
                                            , "E.ENTITY_ID = O.ENTITY_ID"
                                             , "POINT_UNIT_DATA"
                                     , "O.OTHER_UNIT_ID=POINT_UNIT_DATA.UNIT_ID AND POINT_UNIT_DATA.UNIT_TYPE='SU'"
                                     , "INTEGRAL_UNIT_DATA"
                                            , "O.OTHER_UNIT_ID=INTEGRAL_UNIT_DATA.UNIT_ID AND  INTEGRAL_UNIT_DATA.UNIT_TYPE='SU'"//vkumar258 - added to get integral data
                                            , " ISINSURED <>-2 AND CO.CLAIM_ID = ") //aaggarwal29 : Added for MITS 37196, No Need to display units with insured flag = -2 
                                        );
                                else if (sDBType == Constants.DB_ORACLE)
                                {
                                    strSql.Append(string.Format("SELECT {0} FROM {1} INNER JOIN {2} ON {3} INNER JOIN {4} ON {5} LEFT OUTER JOIN {6} ON {7} LEFT OUTER JOIN {8} ON {9} WHERE {10}"
                                        // RMA-10039: Ash, Incorrect handling for POINT, Integral, Staging   
                                        , "E.ENTITY_ID, E.LAST_NAME,E.REFERENCE_NUMBER|\"VIN-ADDRESS\" , E.ABBREVIATION, E.TAX_ID, E.PHONE1 | PHONE_NUMBER,O.UNIT_TYPE ,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END)|INSURED , (CASE WHEN POINT_UNIT_DATA.STAT_UNIT_NUMBER IS NULL THEN INTEGRAL_UNIT_DATA.UNIT_NUMBER ELSE (POINT_UNIT_DATA.STAT_UNIT_NUMBER||' / '||POINT_UNIT_DATA.UNIT_RISK_LOC||' / '||POINT_UNIT_DATA.UNIT_RISK_SUB_LOC) END)|UNITNO"      //Ankit Start : Worked for MITS - 35655 (OtherUnit Issue for Oracle)
                                            , "CLAIM_X_OTHERUNIT CO"
                                            , "OTHER_UNIT O"
                                            , "CO.OTHER_UNIT_ID = O.OTHER_UNIT_ID"
                                            , "ENTITY E"
                                            , "E.ENTITY_ID = O.ENTITY_ID"
                                            , "POINT_UNIT_DATA"
                                     , "O.OTHER_UNIT_ID=POINT_UNIT_DATA.UNIT_ID AND POINT_UNIT_DATA.UNIT_TYPE='SU'"
                                      , "INTEGRAL_UNIT_DATA"
                                     , "O.OTHER_UNIT_ID=INTEGRAL_UNIT_DATA.UNIT_ID AND  INTEGRAL_UNIT_DATA.UNIT_TYPE='SU'"//vkumar258 - added to get integral data
                                            , " ISINSURED <>-2 AND CO.CLAIM_ID = ") //aaggarwal29 : Added for MITS 37196, No Need to display units with insured flag = -2 
                                        );
                                
                                
                                }
                            
                            }

                            strSql.Append(sId);


                            if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                            {
                                if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                                {
                                    strSql.Append(" ORDER BY CO.ROW_ID");
                                }
                            }
                            else
                                strSql.Append(" ORDER BY CO.ROW_ID");

                            p_sPrimaryField = "ENTITY_ID";
                            p_sPrimaryFormField = "entityid";
                        }
                        break;
                    //rupal:end
                   

                    case "demandoffer":

                        string sParentClassName = string.Empty;
                        XmlAttribute xAttParentSysName = p_objXmlDoc.SelectSingleNode("//Data").Attributes["parentsysformname"];

                        if (m_sParentID != String.Empty)
                        {
                            sId = m_sParentID;
                        }

                        if (xAttParentSysName != null)
                        {
                            //Aman MITS 27036---Start
                            if (xAttParentSysName.Value == "subrogation")
                            {
                                sParentClassName = typeof(ClaimXSubrogation).Name;
                            }
                            else if (xAttParentSysName.Value == "litigation")
                            {
                                sParentClassName = typeof(ClaimXLitigation).Name;
                            }
                            //Added by Amitosh for R8 enhancement of PropertyLoss and VehicleLoss
                            else if (xAttParentSysName.Value == "propertyloss")
                            {
                                sParentClassName = typeof(ClaimXPropertyLoss).Name;
                            }
                            else if (xAttParentSysName.Value == "unit")
                            {
                                sParentClassName = typeof(UnitXClaim).Name; 
                            }//end Amitosh
                            //added by swati for claimant demand offer mits # 35363
                            else if (xAttParentSysName.Value == "claimant")
                            {
                                sParentClassName = typeof(Claimant).Name;
                            }
                            //change end here by swati
                            //Neha start
                            else if (string.Compare(xAttParentSysName.Value, "PiEmployee", StringComparison.OrdinalIgnoreCase) == 0)
                            {
                                sParentClassName = typeof(PiEmployee).Name;
                            }
                            else if (string.Compare(xAttParentSysName.Value, "PiInjury", StringComparison.OrdinalIgnoreCase) == 0)
                            {
                                sParentClassName = typeof(PiEmployee).Name;
                            }//Aman MITS 27036---End
                            //Neha End
                        }

                        strSql = new StringBuilder();

                        if (sId != string.Empty && sParentClassName != string.Empty)
                        {
                            strSql.Append(string.Format("SELECT {0} FROM {1} LEFT OUTER JOIN {2} ON {3} LEFT OUTER JOIN {4} ON {5} LEFT OUTER JOIN {6} ON {7} WHERE PARENT_ID = {8} AND PARENT_NAME = '{9}'"
                                , "DEMAND_OFFER_ROW_ID, DEMAND_OFFER_DATE, ACTIVITY.CODE_DESC | ACTIVITY, RESULT.CODE_DESC | RESULT, DEMAND_OFFER_AMOUNT | AMOUNT, DECISION.CODE_DESC | DECISION"
                                , "DEMAND_OFFER"
                                , "CODES_TEXT | ACTIVITY"
                                , "ACTIVITY.CODE_ID = DEMAND_OFFER.ACTIVITY_CODE"
                                , "CODES_TEXT | RESULT"
                                , "RESULT.CODE_ID = DEMAND_OFFER.RESULT_CODE"
								, "CODES_TEXT | DECISION"
								, "DECISION.CODE_ID = DEMAND_OFFER.DECISION_CODE"
                                , sId
                                , sParentClassName)
                            );
                       //spahariya MITS 30620 - added Amount and decision in the list - End 11/29/2012
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                            {
                                if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                                {
                                    strSql.Append(" ORDER BY DEMAND_OFFER_ROW_ID");
                                }
                            }
                            else
                                strSql.Append(" ORDER BY DEMAND_OFFER_ROW_ID");

                            p_sPrimaryField = "DEMAND_OFFER_ROW_ID";
                            p_sPrimaryFormField = "demandofferrowid";
                        }
                        break;
					case "expert":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT EXPERT_ROW_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME, ENTITY.ABBREVIATION FROM EXPERT, ENTITY WHERE ENTITY.ENTITY_ID=EXPERT.EXPERT_EID AND LITIGATION_ROW_ID = "
                            + p_objXmlDoc.SelectSingleNode("//litigationrowid").Attributes.GetNamedItem("value").InnerText;
                        }
                        else
                        {
                            sSQL = "SELECT EXPERT_ROW_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME, ENTITY.ABBREVIATION FROM EXPERT, ENTITY WHERE ENTITY.ENTITY_ID=EXPERT.EXPERT_EID AND LITIGATION_ROW_ID = "
                            + m_sParentID;
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY EXPERT_ROW_ID";
							}
						}
						else
							sSQL = sSQL + " ORDER BY EXPERT_ROW_ID";
						p_sPrimaryField="EXPERT_ROW_ID";
						p_sPrimaryFormField="expertrowid";
						break;
					case "claimant":
                        if (m_sParentID == String.Empty)
                        {
                            string sClaimID = string.Empty;
                            //avipinsrivas Start : Worked for Jira-340
                            //if (p_objXmlDoc.SelectSingleNode("//claimid") != null)
                            //{
                            //    //Added:Yukti, MITS 35772, DT:05/28/2014
                            //    //Amitosh
                            //    //sSQL = "SELECT DISTINCT CLAIMANT_ROW_ID, CLAIMANT.CLAIMANT_EID, STATES.STATE_ROW_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME, ENTITY.MIDDLE_NAME, ENTITY.TAX_ID, ENTITY.ADDR1, ENTITY.ADDR2, ENTITY.CITY, STATES.STATE_ID, STATES.STATE_NAME ,ENTITY.ZIP_CODE, PRIMARY_CLMNT_FLAG,CODE_TEXT.B FROM CLAIMANT, ENTITY, CODES_TEXT, STATES WHERE ENTITY.ENTITY_ID=CLAIMANT.CLAIMANT_EID AND CLAIMANT.CLAIM_ID = "
                            //    //+ p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText
                            //    //+ " AND ENTITY.STATE_ID=STATES.STATE_ROW_ID ";

                            //    //sSQL = "SELECT DISTINCT CLAIMANT_ROW_ID, CLAIMANT.CLAIMANT_EID, STATES.STATE_ROW_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME, ENTITY.MIDDLE_NAME, ENTITY.TAX_ID, ENTITY.ADDR1, ENTITY.ADDR2, ENTITY.CITY, STATES.STATE_ID, STATES.STATE_NAME ,ENTITY.ZIP_CODE, PRIMARY_CLMNT_FLAG, CODES_TEXT.CODE_DESC FROM CLAIMANT  LEFT OUTER JOIN ENTITY_X_BANKING ON CLAIMANT.CLAIMANT_EID = ENTITY_X_BANKING.ENTITY_ID LEFT OUTER JOIN CODES_TEXT ON ENTITY_X_BANKING.BANK_STATUS_CODE = CODES_TEXT.CODE_ID,ENTITY, STATES WHERE ENTITY.ENTITY_ID=CLAIMANT.CLAIMANT_EID AND CLAIMANT.CLAIM_ID ="
                            //    //    + p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText
                            //    //                                + " AND ENTITY.STATE_ID=STATES.STATE_ROW_ID  ";

                            //    sSQL = "SELECT DISTINCT CLAIMANT_ROW_ID, CLAIMANT.CLAIMANT_EID, STATES.STATE_ROW_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME, ENTITY.MIDDLE_NAME, ENTITY.TAX_ID, ENTITY.ADDR1, ENTITY.ADDR2, ENTITY.CITY, STATES.STATE_NAME ,ENTITY.ZIP_CODE, PRIMARY_CLMNT_FLAG, CODES_TEXT.CODE_DESC FROM CLAIMANT  LEFT OUTER JOIN ENTITY_X_BANKING ON CLAIMANT.CLAIMANT_EID = ENTITY_X_BANKING.ENTITY_ID LEFT OUTER JOIN CODES_TEXT ON ENTITY_X_BANKING.BANK_STATUS_CODE = CODES_TEXT.CODE_ID,ENTITY, STATES WHERE ENTITY.ENTITY_ID=CLAIMANT.CLAIMANT_EID AND CLAIMANT.CLAIM_ID ="
                            //        + p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText
                            //                                    + " AND ENTITY.STATE_ID=STATES.STATE_ROW_ID  ";
                            //}
                            //else if (p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"] != null)
                            //{
                            //    //sSQL = "SELECT DISTINCT CLAIMANT_ROW_ID, CLAIMANT.CLAIMANT_EID, STATES.STATE_ROW_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME, ENTITY.MIDDLE_NAME, ENTITY.TAX_ID, ENTITY.ADDR1, ENTITY.ADDR2, ENTITY.CITY, STATES.STATE_ID, STATES.STATE_NAME ,ENTITY.ZIP_CODE, PRIMARY_CLMNT_FLAG FROM CLAIMANT, ENTITY, CODES_TEXT, STATES WHERE ENTITY.ENTITY_ID=CLAIMANT.CLAIMANT_EID AND CLAIMANT.CLAIM_ID = "
                            //    //+ p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("claimid").InnerText
                            //    //+ " AND ENTITY.STATE_ID=STATES.STATE_ROW_ID ";


                            //    //sSQL = "SELECT DISTINCT CLAIMANT_ROW_ID, CLAIMANT.CLAIMANT_EID, STATES.STATE_ROW_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME, ENTITY.MIDDLE_NAME, ENTITY.TAX_ID, ENTITY.ADDR1, ENTITY.ADDR2, ENTITY.CITY, STATES.STATE_ID, STATES.STATE_NAME ,ENTITY.ZIP_CODE, PRIMARY_CLMNT_FLAG, CODES_TEXT.CODE_DESC FROM CLAIMANT  LEFT OUTER JOIN ENTITY_X_BANKING ON CLAIMANT.CLAIMANT_EID = ENTITY_X_BANKING.ENTITY_ID LEFT OUTER JOIN CODES_TEXT ON ENTITY_X_BANKING.BANK_STATUS_CODE = CODES_TEXT.CODE_ID,ENTITY, STATES WHERE ENTITY.ENTITY_ID=CLAIMANT.CLAIMANT_EID AND CLAIMANT.CLAIM_ID ="
                            //    //                                + p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("claimid").InnerText
                            //    //                                + " AND ENTITY.STATE_ID=STATES.STATE_ROW_ID ";

                            //    sSQL = "SELECT DISTINCT CLAIMANT_ROW_ID, CLAIMANT.CLAIMANT_EID, STATES.STATE_ROW_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME, ENTITY.MIDDLE_NAME, ENTITY.TAX_ID, ENTITY.ADDR1, ENTITY.ADDR2, ENTITY.CITY, STATES.STATE_NAME ,ENTITY.ZIP_CODE, PRIMARY_CLMNT_FLAG, CODES_TEXT.CODE_DESC FROM CLAIMANT  LEFT OUTER JOIN ENTITY_X_BANKING ON CLAIMANT.CLAIMANT_EID = ENTITY_X_BANKING.ENTITY_ID LEFT OUTER JOIN CODES_TEXT ON ENTITY_X_BANKING.BANK_STATUS_CODE = CODES_TEXT.CODE_ID,ENTITY, STATES WHERE ENTITY.ENTITY_ID=CLAIMANT.CLAIMANT_EID AND CLAIMANT.CLAIM_ID ="
                            //    + p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("claimid").InnerText
                            //    + " AND ENTITY.STATE_ID=STATES.STATE_ROW_ID ";
                            //                                  }

                            if (p_objXmlDoc.SelectSingleNode("//claimid") != null)
                                sClaimID = p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText;
                            else if (p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"] != null)
                                sClaimID = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("claimid").InnerText;

                            if (!string.IsNullOrEmpty(sClaimID))
                            {
                                sSQL = " SELECT DISTINCT CLAIMANT_ROW_ID, CLAIMANT.CLAIMANT_EID, STATES.STATE_ROW_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME, ";
                                // akaushik Changed for MITS 37743 Starts
                                //sSQL = string.Concat(sSQL, " ENTITY.MIDDLE_NAME, ENTITY.TAX_ID, ENTITY.ADDR1, ENTITY.ADDR2, ENTITY.CITY, STATES.STATE_NAME, ");
                                sSQL = string.Concat(sSQL, " ENTITY.MIDDLE_NAME, ENTITY.TAX_ID, ENTITY.ADDR1, ENTITY.ADDR2, ENTITY.ADDR3, ENTITY.ADDR4, ENTITY.CITY, STATES.STATE_ID, STATES.STATE_NAME, ");
                                // akaushik Changed for MITS 37743 Ends
                                sSQL = string.Concat(sSQL, " ENTITY.ZIP_CODE, PRIMARY_CLMNT_FLAG, CODES_TEXT.CODE_DESC, ");
                                sSQL = string.Concat(sSQL, " CTS.CODE_DESC STATUS_CODE, ");//JIRA RMA-9685 ajohari2
                                sSQL = string.Concat(sSQL, " CODES.SHORT_CODE COUNTRY_ID,CT.CODE_DESC COUNTRY_NAME ,CODES.CODE_ID COUNTRYID ");
                                sSQL = string.Concat(sSQL, " FROM CLAIMANT ");
                                sSQL = string.Concat(sSQL, " INNER JOIN ENTITY ON ENTITY.ENTITY_ID=CLAIMANT.CLAIMANT_EID ");
                                sSQL = string.Concat(sSQL, " LEFT OUTER JOIN STATES ON ENTITY.STATE_ID=STATES.STATE_ROW_ID ");//Change inner join to Left outer as inner join is not working properly in oracle
                                sSQL = string.Concat(sSQL, " LEFT OUTER JOIN ENTITY_X_BANKING ON CLAIMANT.CLAIMANT_EID = ENTITY_X_BANKING.ENTITY_ID ");
                                sSQL = string.Concat(sSQL, " LEFT OUTER JOIN CODES_TEXT ON ENTITY_X_BANKING.BANK_STATUS_CODE = CODES_TEXT.CODE_ID ");
                                sSQL = string.Concat(sSQL, " LEFT OUTER JOIN CODES_TEXT CTS ON CLAIMANT.CLAIMANT_STATUS_CODE = CTS.CODE_ID ");//JIRA RMA-9685 ajohari2 
                                //rkatyal4: 12024 start
                                //sSQL = string.Concat(sSQL, " RIGHT JOIN CODES ON CODES.CODE_ID = ENTITY.COUNTRY_CODE ");// converting left to right join as we want all data from entity table
                                //sSQL = string.Concat(sSQL, " RIGHT JOIN CODES_TEXT CT ON CT.CODE_ID =ENTITY.COUNTRY_CODE ");// converting left to right join as we want all data from entity table
                                sSQL = string.Concat(sSQL, " LEFT OUTER JOIN CODES ON CODES.CODE_ID = ENTITY.COUNTRY_CODE ");
                                sSQL = string.Concat(sSQL, " LEFT OUTER JOIN CODES_TEXT CT ON CT.CODE_ID =ENTITY.COUNTRY_CODE ");                                
                                //rkatyal: 12024 end
                                sSQL = string.Concat(sSQL, " WHERE CLAIMANT.CLAIM_ID = ", sClaimID);
                            }
                            //avipinsrivas End
                        }
                        else
                        {
                            //Added:Yukti,DT:05/20/2014, MITS 35772
                            //sSQL = "SELECT DISTINCT CLAIMANT_ROW_ID, CLAIMANT.CLAIMANT_EID, STATES.STATE_ROW_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME, ENTITY.MIDDLE_NAME, ENTITY.TAX_ID, ENTITY.ADDR1, ENTITY.ADDR2, ENTITY.CITY, STATES.STATE_ID, STATES.STATE_NAME ,ENTITY.ZIP_CODE, PRIMARY_CLMNT_FLAG FROM CLAIMANT, ENTITY, CODES_TEXT, STATES WHERE ENTITY.ENTITY_ID=CLAIMANT.CLAIMANT_EID AND CLAIMANT.CLAIM_ID = "
                            //+ m_sParentID
                            //+ " AND ENTITY.STATE_ID=STATES.STATE_ROW_ID ";

                            //sSQL = "SELECT DISTINCT CLAIMANT_ROW_ID, CLAIMANT.CLAIMANT_EID, STATES.STATE_ROW_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME, ENTITY.MIDDLE_NAME, ENTITY.TAX_ID, ENTITY.ADDR1, ENTITY.ADDR2, ENTITY.CITY, STATES.STATE_ID, STATES.STATE_NAME ,ENTITY.ZIP_CODE, PRIMARY_CLMNT_FLAG, CODES_TEXT.CODE_DESC FROM CLAIMANT  LEFT OUTER JOIN ENTITY_X_BANKING ON CLAIMANT.CLAIMANT_EID = ENTITY_X_BANKING.ENTITY_ID LEFT OUTER JOIN CODES_TEXT ON ENTITY_X_BANKING.BANK_STATUS_CODE = CODES_TEXT.CODE_ID,ENTITY, STATES WHERE ENTITY.ENTITY_ID=CLAIMANT.CLAIMANT_EID AND CLAIMANT.CLAIM_ID ="
                            //               + m_sParentID
                            //                                    + " AND ENTITY.STATE_ID=STATES.STATE_ROW_ID ";
                            if (sDBType == Constants.DB_SQLSRVR)
                            {
                                sSQL = "SELECT CLAIMANT.CLAIMANT_ROW_ID, CLAIMANT.CLAIMANT_EID, STATES.STATE_ROW_ID, ENTITY.LAST_NAME,"
                                    + " ENTITY.FIRST_NAME, ENTITY.MIDDLE_NAME, ENTITY.TAX_ID, ENTITY.ADDR1, ENTITY.ADDR2,ENTITY.ADDR3, ENTITY.ADDR4, ENTITY.CITY,STATES.STATE_ID, "
                                    + " STATES.STATE_NAME ,ENTITY.ZIP_CODE,  CODES_TEXT.CODE_DESC ,CODES.SHORT_CODE COUNTRY_ID,CT.CODE_DESC COUNTRY_NAME , CODES.CODE_ID COUNTRYID,"
                                    + " CASE WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='P' THEN (SELECT 'PIN:' + PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                    + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='V' THEN (SELECT 'VIN:' + VIN FROM VEHICLE WHERE UNIT_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                    + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='S' THEN (SELECT 'SITE:' + SITE_NUMBER FROM SITE_UNIT WHERE SITE_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                    + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='SU' THEN (SELECT 'STAT:' + ISNULL(ENTITY.LAST_NAME, '') + ISNULL(ENTITY.LAST_NAME, '') FROM ENTITY WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM OTHER_UNIT WHERE OTHER_UNIT_ID = CLAIMANT_X_UNIT.UNIT_ID))"
                                    + " END AS UNIT, PRIMARY_CLMNT_FLAG, CTS.CODE_DESC STATUS_CODE FROM CLAIMANT"//JIRA RMA-9685 ajohari2  
                                    + " LEFT OUTER JOIN ENTITY_X_BANKING ON CLAIMANT.CLAIMANT_EID = ENTITY_X_BANKING.ENTITY_ID"
                                    + " LEFT OUTER JOIN CODES_TEXT ON ENTITY_X_BANKING.BANK_STATUS_CODE = CODES_TEXT.CODE_ID"
                                    + " LEFT OUTER JOIN CODES_TEXT CTS ON CLAIMANT.CLAIMANT_STATUS_CODE = CTS.CODE_ID"//JIRA RMA-9685 ajohari2  
                                    + " LEFT OUTER JOIN (SELECT CLMNT_UNIT_ROW_ID, CLAIMANT_ROW_ID, UNIT_ID, UNIT_TYPE FROM CLAIMANT_X_UNIT CXU WHERE CXU.CLMNT_UNIT_ROW_ID IN (SELECT MIN(CLMNT_UNIT_ROW_ID) FROM CLAIMANT_X_UNIT CXU1 WHERE CXU.CLAIMANT_ROW_ID = CXU1.CLAIMANT_ROW_ID)) CLAIMANT_X_UNIT"
                                    //rkatyal4 start : Worked for JIRA - 12024
                                    //+ " ON CLAIMANT.CLAIMANT_ROW_ID =CLAIMANT_X_UNIT.CLAIMANT_ROW_ID,ENTITY, STATES ,CODES_TEXT CT,CODES"
                                    //+ " WHERE ENTITY.ENTITY_ID=CLAIMANT.CLAIMANT_EID AND CLAIMANT.CLAIM_ID = " + m_sParentID + " AND ENTITY.STATE_ID=STATES.STATE_ROW_ID AND CODES.CODE_ID =ENTITY.COUNTRY_CODE AND CT.CODE_ID =ENTITY.COUNTRY_CODE";
                                    + " ON CLAIMANT.CLAIMANT_ROW_ID =CLAIMANT_X_UNIT.CLAIMANT_ROW_ID"
                                    + " INNER JOIN ENTITY ON ENTITY.ENTITY_ID=CLAIMANT.CLAIMANT_EID "
                                    + " LEFT OUTER JOIN STATES ON ENTITY.STATE_ID=STATES.STATE_ROW_ID "
                                    + " LEFT OUTER JOIN CODES_TEXT CT ON CT.CODE_ID =ENTITY.COUNTRY_CODE "
                                    + " LEFT OUTER JOIN CODES ON CODES.CODE_ID =ENTITY.COUNTRY_CODE "
                                    + " WHERE CLAIMANT.CLAIM_ID = " + m_sParentID + "";
                                    //rkatyal4 end
                            }
                            else if (sDBType == Constants.DB_ORACLE)
                            {
                                sSQL = "SELECT CLAIMANT.CLAIMANT_ROW_ID, CLAIMANT.CLAIMANT_EID, STATES.STATE_ROW_ID, ENTITY.LAST_NAME,"
                                    + " ENTITY.FIRST_NAME, ENTITY.MIDDLE_NAME, ENTITY.TAX_ID, ENTITY.ADDR1, ENTITY.ADDR2,ENTITY.ADDR3, ENTITY.ADDR4, ENTITY.CITY,"
                                    + " STATES.STATE_ID,STATES.STATE_NAME ,ENTITY.ZIP_CODE, CODES_TEXT.CODE_DESC ,CODES.SHORT_CODE COUNTRY_ID,CT.CODE_DESC COUNTRY_NAME , CODES.CODE_ID COUNTRYID,"
                                    + " CASE WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='P' THEN (SELECT 'PIN:' || PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                    + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='V' THEN (SELECT 'VIN:' || VIN FROM VEHICLE WHERE UNIT_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                    + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='S' THEN (SELECT 'SITE:' || SITE_NUMBER FROM SITE_UNIT WHERE SITE_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                    + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='SU' THEN (SELECT 'STAT:' || NVL(ENTITY.LAST_NAME, '') || NVL(ENTITY.FIRST_NAME, '') FROM ENTITY WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM OTHER_UNIT WHERE OTHER_UNIT_ID = CLAIMANT_X_UNIT.UNIT_ID))"
                                    + " END AS UNIT, PRIMARY_CLMNT_FLAG, CTS.CODE_DESC STATUS_CODE FROM CLAIMANT"//JIRA RMA-9685 ajohari2  
                                    + " LEFT OUTER JOIN ENTITY_X_BANKING ON CLAIMANT.CLAIMANT_EID = ENTITY_X_BANKING.ENTITY_ID"
                                    + " LEFT OUTER JOIN CODES_TEXT ON ENTITY_X_BANKING.BANK_STATUS_CODE = CODES_TEXT.CODE_ID"
                                    + " LEFT OUTER JOIN CODES_TEXT CTS ON CLAIMANT.CLAIMANT_STATUS_CODE = CTS.CODE_ID"//JIRA RMA-9685 ajohari2 
                                    + " LEFT OUTER JOIN (SELECT CLMNT_UNIT_ROW_ID, CLAIMANT_ROW_ID, UNIT_ID, UNIT_TYPE FROM CLAIMANT_X_UNIT CXU WHERE CXU.CLMNT_UNIT_ROW_ID IN (SELECT MIN(CLMNT_UNIT_ROW_ID) FROM CLAIMANT_X_UNIT CXU1 WHERE CXU.CLAIMANT_ROW_ID = CXU1.CLAIMANT_ROW_ID)) CLAIMANT_X_UNIT"
                                    //rkatyal4 start : Worked for JIRA - 12024
                                    //+ " ON CLAIMANT.CLAIMANT_ROW_ID =CLAIMANT_X_UNIT.CLAIMANT_ROW_ID,ENTITY, STATES ,CODES_TEXT CT,CODES"
                                    //+ " WHERE ENTITY.ENTITY_ID=CLAIMANT.CLAIMANT_EID AND CLAIMANT.CLAIM_ID = " + m_sParentID + " AND ENTITY.STATE_ID=STATES.STATE_ROW_ID AND CODES.CODE_ID =ENTITY.COUNTRY_CODE AND CT.CODE_ID =ENTITY.COUNTRY_CODE";
                                    + " ON CLAIMANT.CLAIMANT_ROW_ID =CLAIMANT_X_UNIT.CLAIMANT_ROW_ID"
                                    + " INNER JOIN ENTITY ON ENTITY.ENTITY_ID=CLAIMANT.CLAIMANT_EID "
                                    + " LEFT OUTER JOIN STATES ON ENTITY.STATE_ID=STATES.STATE_ROW_ID "
                                    + " LEFT OUTER JOIN CODES_TEXT CT ON CT.CODE_ID =ENTITY.COUNTRY_CODE "
                                    + " LEFT OUTER JOIN CODES ON CODES.CODE_ID =ENTITY.COUNTRY_CODE "
                                    + " WHERE CLAIMANT.CLAIM_ID = " + m_sParentID + "";
                                    //rkatyal4: 12024 end
                            }
                            //Ended:Yukti,Dt:05/20/2014

                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY ENTITY.LAST_NAME";
							}
						}	
						else
							sSQL = sSQL + " ORDER BY ENTITY.LAST_NAME";
						p_sPrimaryField="CLAIMANT_ROW_ID";
						p_sPrimaryFormField="claimantrowid";
						p_sSecondaryField="CLAIMANT_EID";
						p_sSecondaryFormField="hidden";
						p_sThirdField= "STATE_ROW_ID";
						p_sThirdFormField = "staterowid";
                        p_arrlstHiddenFields.Insert(0,"CODE_DESC");//Added by Amitosh For EFT Payment
						break;
					case "defendant":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT DEFENDANT_ROW_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME FROM DEFENDANT, ENTITY WHERE ENTITY.ENTITY_ID=DEFENDANT.DEFENDANT_EID AND DEFENDANT.CLAIM_ID = "
                                + p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText;
                        }
                        else
                        {
                            sSQL = "SELECT DEFENDANT_ROW_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME FROM DEFENDANT, ENTITY WHERE ENTITY.ENTITY_ID=DEFENDANT.DEFENDANT_EID AND DEFENDANT.CLAIM_ID = "
                                + m_sParentID;
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY ENTITY.LAST_NAME";
							}
						}
						else
							sSQL = sSQL + " ORDER BY ENTITY.LAST_NAME";
						p_sPrimaryField="DEFENDANT_ROW_ID";
						p_sPrimaryFormField="defendantrowid";
						break;
                    //Start - Averma62 - MITS 28452 VSS Integration
                    case "vssassignment":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT ROW_ID, ASSIGNMENT_NAME,ACCOUNT_NUMBER,ASSIGNMENT_DUE_DATE FROM CLAIM_X_VSSASSIGNMENT WHERE CLAIM_X_VSSASSIGNMENT.CLAIM_ID = "
                                + p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText;
                        }
                        else
                        {
                            sSQL = "SELECT ROW_ID, ASSIGNMENT_NAME,ACCOUNT_NUMBER,ASSIGNMENT_DUE_DATE FROM CLAIM_X_VSSASSIGNMENT WHERE CLAIM_ID = "
                                + m_sParentID;
                        }
                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY ASSIGNMENT_NAME";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY ASSIGNMENT_NAME";
                        p_sPrimaryField = "ROW_ID";
                        p_sPrimaryFormField = "vssassignmentrowid";
                        break;
                    //End - Averma62 - MITS 28452 VSS Integration
					case "unit":
                        //smahajan6 - MITS #18644 - 11/18/2009 : Start
                        if (p_objXmlDoc.SelectSingleNode("//claimpolicyidenh") != null)
                        {
                            strSql = new StringBuilder();
                            if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_ORACLE)
                            {
                                strSql.Append(String.Format(@"SELECT V.UNIT_ID,V.VIN AS VEHICLE_ID,V.VEHICLE_MAKE,V.VEHICLE_YEAR,V.LICENSE_NUMBER,
                                (NVL(V.VEHICLE_MODEL,'') || '{0}' || NVL(V.UNIT_TYPE_CODE,0) || '{0}' || NVL(UNIT_TYPE.SHORT_CODE,'') || ' ' || 
                                 NVL(UNIT_TYPE.CODE_DESC,'') || '{0}' || NVL(V.HOME_DEPT_EID,0) || '{0}' || NVL(HOME_DEPT.ABBREVIATION,'') || ' ' ||
                                 NVL(HOME_DEPT.LAST_NAME,'') || '{0}' || NVL(S.STATE_ROW_ID,0) || '{0}' || NVL(S.STATE_ID,'') || ' ' || NVL(S.STATE_NAME,''))) 
                                 AS HIDDEN_FIELDS", ARR_DELIMITER));
                            }
                            else
                            {
                                strSql.Append(String.Format(@"SELECT V.UNIT_ID,V.VIN AS VEHICLE_ID,V.VEHICLE_MAKE,V.VEHICLE_YEAR,V.LICENSE_NUMBER,
                                (CONVERT(VARCHAR(50),COALESCE(V.VEHICLE_MODEL,'')) + '{0}' + CONVERT(VARCHAR(10),COALESCE(V.UNIT_TYPE_CODE,0)) + '{0}' +
                                 CONVERT(VARCHAR(25),COALESCE(UNIT_TYPE.SHORT_CODE,'')) + ' ' +CONVERT(VARCHAR(50),COALESCE(UNIT_TYPE.CODE_DESC,'')) + '{0}' +
                                 CONVERT(VARCHAR(10),COALESCE(V.HOME_DEPT_EID,0)) + '{0}' + CONVERT(VARCHAR(25),COALESCE(HOME_DEPT.ABBREVIATION,'')) + ' ' +
                                 CONVERT(VARCHAR(50),COALESCE(HOME_DEPT.LAST_NAME,'')) + '{0}' + CONVERT(VARCHAR(10),COALESCE(S.STATE_ROW_ID,0)) + '{0}' +
                                 CONVERT(VARCHAR(4),COALESCE(S.STATE_ID,'')) + ' ' + CONVERT(VARCHAR(25),COALESCE(S.STATE_NAME,''))) AS HIDDEN_FIELDS", ARR_DELIMITER));
                            }
                            strSql.Append(" FROM VEHICLE V, POLICY_X_UAR PXU, ENTITY HOME_DEPT, CODES_TEXT UNIT_TYPE, STATES S");
                            strSql.Append(" WHERE V.DELETED_FLAG <> -1 ");
                            strSql.Append(" AND V.STATE_ROW_ID = S.STATE_ROW_ID AND V.UNIT_TYPE_CODE = UNIT_TYPE.CODE_ID");
                            strSql.Append(" AND V.HOME_DEPT_EID = HOME_DEPT.ENTITY_ID AND V.UNIT_ID = PXU.UAR_ID AND PXU.POLICY_ID = ");
                            strSql.Append(Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//claimpolicyidenh").Attributes.GetNamedItem("value").InnerText));
                            //MITS: 20484 - start
                            if (p_objXmlDoc.SelectSingleNode("//claimpolicyidenh") != null)
                            {
                                iPolicyId = Conversion.CastToType<int>(p_objXmlDoc.SelectSingleNode("//claimpolicyidenh").Attributes.GetNamedItem("value").InnerText, out bIsSucess);
                                sSQL = "SELECT EXPOSURE_ID FROM POLICY_X_EXP_ENH WHERE TRANSACTION_ID=(SELECT MAX(TRANSACTION_ID)FROM POLICY_X_EXP_ENH WHERE POLICY_ID=" + iPolicyId + ")";
                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                                while (objReader.Read())
                                {
                                    if (String.IsNullOrEmpty(sExposureIds))
                                    {
                                        sExposureIds = objReader.GetInt32("EXPOSURE_ID").ToString();
                                    }
                                    else
                                    {
                                        sExposureIds += ',' + objReader.GetInt32("EXPOSURE_ID").ToString();
                                    }
                                }
                                objReader.Close();
                                objReader.Dispose();
                                if (!String.IsNullOrEmpty(sExposureIds))
                                {
                                    strSql.Append(" AND PXU.EXPOSURE_ID IN (" + sExposureIds + ")");
                                }
                            }
                            //MITS: 20484 - end
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                            {
                                if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                                {
                                    strSql.Append(" ORDER BY V.UNIT_ID");
                                }
                            }
                            else
                            {
                                strSql.Append(" ORDER BY V.UNIT_ID");
                            }

                            p_sPrimaryField = "UNIT_ID";
                            p_sPrimaryFormField = "unitid";
                            //Hidden Fields
                            p_arrlstHiddenFields.Insert(0, "HIDDEN_FIELDS");
                        }
                        else
                        //smahajan6 - MITS #18644 - 11/18/2009 : End
                        {
                            if (objSettings.UsePolicyInterface)
                            {
                                if (m_sParentID == String.Empty)
                                {
                                    if (p_objXmlDoc.SelectSingleNode("//caller") == null)
                                    {
                                        if (p_objXmlDoc.SelectSingleNode("//claimid") != null)
                                        {//skhare7 Added insured column for point policy interface
                                            if (sDBType == Constants.DB_SQLSRVR)
                                            {
                                                sSQL = "SELECT UNIT_ROW_ID, VEHICLE.VEH_DESC, VEHICLE.VIN,VEHICLE.VEHICLE_MAKE, VEHICLE.VEHICLE_MODEL,  VEHICLE.VEHICLE_YEAR,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END) INSURED,POINT_UNIT_DATA.STAT_UNIT_NUMBER+' / '+POINT_UNIT_DATA.UNIT_RISK_LOC+' / '+POINT_UNIT_DATA.UNIT_RISK_SUB_LOC UNITNO "
                                               + " FROM UNIT_X_CLAIM, VEHICLE LEFT OUTER JOIN   POINT_UNIT_DATA ON  POINT_UNIT_DATA.UNIT_ID=VEHICLE.UNIT_ID AND  POINT_UNIT_DATA.UNIT_TYPE='V'  WHERE UNIT_X_CLAIM.UNIT_ID=VEHICLE.UNIT_ID AND UNIT_X_CLAIM.ISINSURED <> -2 AND  UNIT_X_CLAIM.CLAIM_ID = "
                                                    + p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText;
                                            }
                                            else if (sDBType == Constants.DB_ORACLE)
                                            {
                                                sSQL = "SELECT UNIT_ROW_ID, VEHICLE.VEH_DESC,VEHICLE.VIN, VEHICLE.VEHICLE_MAKE,VEHICLE.VEHICLE_MODEL,  VEHICLE.VEHICLE_YEAR,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END) INSURED,POINT_UNIT_DATA.STAT_UNIT_NUMBER||' / '||POINT_UNIT_DATA.UNIT_RISK_LOC||' / '||POINT_UNIT_DATA.UNIT_RISK_SUB_LOC UNITNO  "
                                                + " FROM UNIT_X_CLAIM, VEHICLE LEFT OUTER JOIN   POINT_UNIT_DATA ON  POINT_UNIT_DATA.UNIT_ID=VEHICLE.UNIT_ID AND  POINT_UNIT_DATA.UNIT_TYPE='V' WHERE UNIT_X_CLAIM.UNIT_ID=VEHICLE.UNIT_ID AND UNIT_X_CLAIM.ISINSURED <> -2 AND UNIT_X_CLAIM.CLAIM_ID = "
                                                 + p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText;
                                            }
                                        }
                                        else if (p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"] != null)
                                        {

                                            if (sDBType == Constants.DB_SQLSRVR)
                                            {
                                                sSQL = "SELECT UNIT_ROW_ID, VEHICLE.VEH_DESC, VEHICLE.VIN,VEHICLE.VEHICLE_MAKE,VEHICLE.VEHICLE_MODEL,  VEHICLE.VEHICLE_YEAR,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END) INSURED,POINT_UNIT_DATA.STAT_UNIT_NUMBER+' / '+POINT_UNIT_DATA.UNIT_RISK_LOC+' / '+POINT_UNIT_DATA.UNIT_RISK_SUB_LOC UNITNO  "
                                                + " FROM UNIT_X_CLAIM, VEHICLE LEFT OUTER JOIN   POINT_UNIT_DATA ON  POINT_UNIT_DATA.UNIT_ID=VEHICLE.UNIT_ID AND  POINT_UNIT_DATA.UNIT_TYPE='V' WHERE UNIT_X_CLAIM.UNIT_ID=VEHICLE.UNIT_ID AND UNIT_X_CLAIM.ISINSURED <> -2 AND  UNIT_X_CLAIM.CLAIM_ID = "
                                                + p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"].Value;
                                            }
                                            else if (sDBType == Constants.DB_ORACLE)
                                            {
                                                sSQL = "SELECT UNIT_ROW_ID, VEHICLE.VEH_DESC,VEHICLE.VIN,VEHICLE.VEHICLE_MAKE, VEHICLE.VEHICLE_MODEL,  VEHICLE.VEHICLE_YEAR,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END) INSURED"
                                                + " ,POINT_UNIT_DATA.STAT_UNIT_NUMBER||' / '||POINT_UNIT_DATA.UNIT_RISK_LOC||' / '||POINT_UNIT_DATA.UNIT_RISK_SUB_LOC UNITNO   FROM UNIT_X_CLAIM, VEHICLE LEFT OUTER JOIN   POINT_UNIT_DATA ON  POINT_UNIT_DATA.UNIT_ID=VEHICLE.UNIT_ID AND  POINT_UNIT_DATA.UNIT_TYPE='V' WHERE UNIT_X_CLAIM.UNIT_ID=VEHICLE.UNIT_ID AND UNIT_X_CLAIM.ISINSURED <> -2 AND UNIT_X_CLAIM.CLAIM_ID = "
                                                    + p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"].Value;
                                            
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (sDBType == Constants.DB_SQLSRVR)
                                        {
                                            sSQL = "SELECT UNIT_ROW_ID, VEHICLE.VEH_DESC, VEHICLE.VIN,VEHICLE.VEHICLE_MAKE,VEHICLE.VEHICLE_MODEL,  VEHICLE.VEHICLE_YEAR ,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END) INSURED,"
                                            + " POINT_UNIT_DATA.STAT_UNIT_NUMBER+' / '+POINT_UNIT_DATA.UNIT_RISK_LOC+' / '+POINT_UNIT_DATA.UNIT_RISK_SUB_LOC UNITNO  FROM UNIT_X_CLAIM, CLAIM,VEHICLE  LEFT OUTER JOIN   POINT_UNIT_DATA ON  POINT_UNIT_DATA.UNIT_ID=VEHICLE.UNIT_ID AND  POINT_UNIT_DATA.UNIT_TYPE='V' WHERE UNIT_X_CLAIM.UNIT_ID=VEHICLE.UNIT_ID AND UNIT_X_CLAIM.CLAIM_ID = CLAIM.CLAIM_ID AND UNIT_X_CLAIM.ISINSURED <> -2  AND"
                                                + " CLAIM.CLAIM_NUMBER = '" + p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimnumber"].Value + "'";
                                        }
                                        else if (sDBType == Constants.DB_ORACLE)
                                        {

                                            sSQL = "SELECT UNIT_ROW_ID, VEHICLE.VEH_DESC, VEHICLE.VIN, VEHICLE.VEHICLE_MAKE, VEHICLE.VEHICLE_MODEL, VEHICLE.VEHICLE_YEAR ,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END) INSURED,"
                                           + " POINT_UNIT_DATA.STAT_UNIT_NUMBER||' / '||POINT_UNIT_DATA.UNIT_RISK_LOC||' / '||POINT_UNIT_DATA.UNIT_RISK_SUB_LOC UNITNO  FROM UNIT_X_CLAIM, CLAIM,VEHICLE LEFT OUTER JOIN   POINT_UNIT_DATA ON  POINT_UNIT_DATA.UNIT_ID=VEHICLE.UNIT_ID AND  POINT_UNIT_DATA.UNIT_TYPE='V' WHERE UNIT_X_CLAIM.UNIT_ID=VEHICLE.UNIT_ID  AND UNIT_X_CLAIM.CLAIM_ID = CLAIM.CLAIM_ID AND UNIT_X_CLAIM.ISINSURED <> -2  AND"
                                                  + " CLAIM.CLAIM_NUMBER = '" + p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimnumber"].Value + "'";
                                        
                                        }
                                    }
                                }
                                else
                                {
                                    if (sDBType == Constants.DB_SQLSRVR)//vkumar258 - added to get integral data
                                    {
                                        sSQL = "SELECT UNIT_ROW_ID, VEHICLE.VEH_DESC, VEHICLE.VIN,VEHICLE.VEHICLE_MAKE,VEHICLE.VEHICLE_MODEL,  VEHICLE.VEHICLE_YEAR,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END) INSURED,"
                                            +"(CASE WHEN POINT_UNIT_DATA.STAT_UNIT_NUMBER IS NULL THEN INTEGRAL_UNIT_DATA.UNIT_NUMBER ELSE (POINT_UNIT_DATA.STAT_UNIT_NUMBER+' / '+POINT_UNIT_DATA.UNIT_RISK_LOC+' / '+POINT_UNIT_DATA.UNIT_RISK_SUB_LOC) END) UNITNO FROM UNIT_X_CLAIM, VEHICLE LEFT OUTER JOIN POINT_UNIT_DATA ON  POINT_UNIT_DATA.UNIT_ID=VEHICLE.UNIT_ID AND  POINT_UNIT_DATA.UNIT_TYPE='V' LEFT OUTER JOIN INTEGRAL_UNIT_DATA ON  INTEGRAL_UNIT_DATA.UNIT_ID=VEHICLE.UNIT_ID AND  INTEGRAL_UNIT_DATA.UNIT_TYPE='V' WHERE UNIT_X_CLAIM.UNIT_ID=VEHICLE.UNIT_ID AND UNIT_X_CLAIM.ISINSURED <> -2 AND UNIT_X_CLAIM.CLAIM_ID ="
                                            + m_sParentID;
                                    }
                                    else if (sDBType == Constants.DB_ORACLE)
                                    {
                                        sSQL = "SELECT UNIT_ROW_ID, VEHICLE.VEH_DESC,VEHICLE.VIN,VEHICLE.VEHICLE_MAKE, VEHICLE.VEHICLE_MODEL,  VEHICLE.VEHICLE_YEAR,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END) INSURED,"
                                       + " (CASE WHEN POINT_UNIT_DATA.STAT_UNIT_NUMBER IS NULL THEN INTEGRAL_UNIT_DATA.UNIT_NUMBER ELSE (POINT_UNIT_DATA.STAT_UNIT_NUMBER||' / '||POINT_UNIT_DATA.UNIT_RISK_LOC||' / '||POINT_UNIT_DATA.UNIT_RISK_SUB_LOC) END) UNITNO   FROM UNIT_X_CLAIM, VEHICLE LEFT OUTER JOIN   POINT_UNIT_DATA ON  POINT_UNIT_DATA.UNIT_ID=VEHICLE.UNIT_ID AND  POINT_UNIT_DATA.UNIT_TYPE='V' LEFT OUTER JOIN INTEGRAL_UNIT_DATA ON  INTEGRAL_UNIT_DATA.UNIT_ID=VEHICLE.UNIT_ID AND  INTEGRAL_UNIT_DATA.UNIT_TYPE='V' WHERE UNIT_X_CLAIM.UNIT_ID=VEHICLE.UNIT_ID AND UNIT_X_CLAIM.ISINSURED <> -2 AND UNIT_X_CLAIM.CLAIM_ID = "
                                            + m_sParentID;
                                    
                                    }
                                }
                            }
                            else
                            {

                                if (m_sParentID == String.Empty)
                                {
                                    if (p_objXmlDoc.SelectSingleNode("//caller") == null)
                                    {
                                        if (p_objXmlDoc.SelectSingleNode("//claimid") != null)
                                        {//skhare7 Added insured column for point policy interface
                                            sSQL = "SELECT UNIT_ROW_ID, VEHICLE.VIN, VEHICLE.VEHICLE_MAKE, VEHICLE.VEHICLE_MODEL, VEHICLE.VEHICLE_YEAR,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END) INSURED   FROM UNIT_X_CLAIM, VEHICLE WHERE UNIT_X_CLAIM.UNIT_ID=VEHICLE.UNIT_ID AND UNIT_X_CLAIM.CLAIM_ID = "
                                           + p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText;
                                        }
                                        else if (p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"] != null)
                                        {
                                            sSQL = "SELECT UNIT_ROW_ID, VEHICLE.VIN, VEHICLE.VEHICLE_MAKE, VEHICLE.VEHICLE_MODEL, VEHICLE.VEHICLE_YEAR,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END) INSURED FROM UNIT_X_CLAIM, VEHICLE WHERE UNIT_X_CLAIM.UNIT_ID=VEHICLE.UNIT_ID AND UNIT_X_CLAIM.CLAIM_ID = "
                                            + p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"].Value;
                                        }
                                    }
                                    else
                                    {
                                        sSQL = "SELECT UNIT_ROW_ID, VEHICLE.VIN,  VEHICLE.VEHICLE_MAKE,VEHICLE.VEHICLE_MODEL, VEHICLE.VEHICLE_YEAR ,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END) INSURED FROM UNIT_X_CLAIM, VEHICLE, CLAIM WHERE UNIT_X_CLAIM.UNIT_ID=VEHICLE.UNIT_ID AND UNIT_X_CLAIM.CLAIM_ID = CLAIM.CLAIM_ID AND"
                                            + " CLAIM.CLAIM_NUMBER = '" + p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimnumber"].Value + "'";
                                    }
                                }
                                else//sbhatnagar21
                                {
                                    sSQL = "SELECT UNIT_ROW_ID, VEHICLE.VIN, VEHICLE.VEHICLE_MAKE,VEHICLE.VEHICLE_MODEL, VEHICLE.VEHICLE_YEAR,(CASE WHEN ISINSURED=0 THEN 'NO' WHEN ISINSURED=-1 THEN 'YES' WHEN ISINSURED=-2 THEN 'NA' ELSE 'NO' END) INSURED  FROM UNIT_X_CLAIM, VEHICLE WHERE UNIT_X_CLAIM.UNIT_ID=VEHICLE.UNIT_ID AND UNIT_X_CLAIM.CLAIM_ID = "
                                    + m_sParentID;
                                }
                            
                            
                            }
                                if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                                {
                                    if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                                    {
                                        sSQL = sSQL + " ORDER BY UNIT_X_CLAIM.UNIT_ID";
                                    }
                                }
                                else
                                    sSQL = sSQL + " ORDER BY UNIT_X_CLAIM.UNIT_ID";
                                p_sPrimaryField = "UNIT_ROW_ID";
                                p_sPrimaryFormField = "unitrowid";
                            }
                        
						break;
					case "policy":
                        // MITS 16114: ybhaskar : Changes made in query for Policy_status_code
                        sSQL = "SELECT POLICY_ID, POLICY_NUMBER, POLICY_NAME, INSURER.LAST_NAME | INSURER_EID, CODES_TEXT.CODE_DESC STATUS, POLICY.EFFECTIVE_DATE, BROKER.LAST_NAME | BROKER_EID FROM POLICY, ENTITY | INSURER, ENTITY | BROKER, CODES_TEXT WHERE POLICY.INSURER_EID=INSURER.ENTITY_ID AND POLICY.BROKER_EID=BROKER.ENTITY_ID AND POLICY_STATUS_CODE = CODES_TEXT.CODE_ID ";
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY POLICY.POLICY_ID";
							}
						}	
						else
							sSQL = sSQL + " ORDER BY POLICY.POLICY_ID";
						p_sPrimaryField="POLICY_ID";
						p_sPrimaryFormField="policyid";
						break;

                        //Divya - 04/16/2007 MITS 9229 Lookup was throwing error  -START
                        
                        case "policyenh":
                        //pmahli MITS 9229 reopened -Start
                        //commented sSQL as left outer join written was not compatible with ORACLE 
                        //sSQL = "SELECT POLICY_ID, POLICY_NAME, INSURER.LAST_NAME | INSURER_EID, POLICY_STATUS.CODE_DESC | POL_STATUS_CODE, ISSUE_DATE, BROKER.LAST_NAME | BROKER_EID FROM POLICY_ENH, ENTITY | INSURER, ENTITY | BROKER , CODES_TEXT  | POLICY_STATUS WHERE POLICY_ENH.INSURER_EID *=INSURER.ENTITY_ID AND POLICY_ENH.BROKER_EID *=BROKER.ENTITY_ID AND POLICY_ENH.POLICY_STATUS_CODE =POLICY_STATUS.CODE_ID ";
                        sSQL = "SELECT POLICY_ID,POLICY_NAME,INSURER.LAST_NAME | INSURER_EID,POLICY_STATUS.CODE_DESC | POL_STATUS_CODE,ISSUE_DATE,BROKER.LAST_NAME | BROKER_EID FROM POLICY_ENH Left outer join ENTITY | INSURER on POLICY_ENH.INSURER_EID =INSURER.ENTITY_ID left outer join ENTITY | BROKER on POLICY_ENH.BROKER_EID =BROKER.ENTITY_ID inner join CODES_TEXT | POLICY_STATUS on POLICY_ENH.POLICY_STATUS_CODE =POLICY_STATUS.CODE_ID";
                        //pmahli MITS 9229 -End
                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY POLICY_ENH.POLICY_ID";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY POLICY_ENH.POLICY_ID";
                        p_sPrimaryField = "POLICY_ID";
                        p_sPrimaryFormField = "policyid";
                        break;

                    //Divya - 04/16/2007 MITS 9229 Lookup was throwing error  - END

                        //Mridul. 12/07/09. MITS#18229.
                         #region Enhanced Policy on LOB basis
                        //Divya - 04/16/2007 MITS 9229 Lookup was throwing error  -START
                        case "policyenhal":
                        //pmahli MITS 9229 reopened -Start
                        //commented sSQL as left outer join written was not compatible with ORACLE 
                        //sSQL = "SELECT POLICY_ID, POLICY_NAME, INSURER.LAST_NAME | INSURER_EID, POLICY_STATUS.CODE_DESC | POL_STATUS_CODE, ISSUE_DATE, BROKER.LAST_NAME | BROKER_EID FROM POLICY_ENH, ENTITY | INSURER, ENTITY | BROKER , CODES_TEXT  | POLICY_STATUS WHERE POLICY_ENH.INSURER_EID *=INSURER.ENTITY_ID AND POLICY_ENH.BROKER_EID *=BROKER.ENTITY_ID AND POLICY_ENH.POLICY_STATUS_CODE =POLICY_STATUS.CODE_ID ";
                        using (LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId))
                        {
                            sSQL = string.Format("SELECT POLICY_ID,POLICY_NAME,INSURER.LAST_NAME | INSURER_EID,POLICY_STATUS.CODE_DESC | POL_STATUS_CODE,ISSUE_DATE,BROKER.LAST_NAME | BROKER_EID FROM POLICY_ENH LEFT OUTER JOIN ENTITY | INSURER ON POLICY_ENH.INSURER_EID =INSURER.ENTITY_ID LEFT OUTER JOIN ENTITY | BROKER ON POLICY_ENH.BROKER_EID =BROKER.ENTITY_ID INNER JOIN CODES_TEXT | POLICY_STATUS ON POLICY_ENH.POLICY_STATUS_CODE = POLICY_STATUS.CODE_ID AND POLICY_ENH.POLICY_TYPE = {0}", objCache.GetCodeId("AL", "POLICY_LOB").ToString());
                        }
                        //pmahli MITS 9229 -End
                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY POLICY_ENH.POLICY_ID";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY POLICY_ENH.POLICY_ID";
                        p_sPrimaryField = "POLICY_ID";
                        p_sPrimaryFormField = "policyid";
                        break;

                        case "policyenhgl":
                        //pmahli MITS 9229 reopened -Start
                        //commented sSQL as left outer join written was not compatible with ORACLE 
                        //sSQL = "SELECT POLICY_ID, POLICY_NAME, INSURER.LAST_NAME | INSURER_EID, POLICY_STATUS.CODE_DESC | POL_STATUS_CODE, ISSUE_DATE, BROKER.LAST_NAME | BROKER_EID FROM POLICY_ENH, ENTITY | INSURER, ENTITY | BROKER , CODES_TEXT  | POLICY_STATUS WHERE POLICY_ENH.INSURER_EID *=INSURER.ENTITY_ID AND POLICY_ENH.BROKER_EID *=BROKER.ENTITY_ID AND POLICY_ENH.POLICY_STATUS_CODE =POLICY_STATUS.CODE_ID ";
                        using (LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId))
                        {
                            sSQL = string.Format("SELECT POLICY_ID,POLICY_NAME,INSURER.LAST_NAME | INSURER_EID,POLICY_STATUS.CODE_DESC | POL_STATUS_CODE,ISSUE_DATE,BROKER.LAST_NAME | BROKER_EID FROM POLICY_ENH LEFT OUTER JOIN ENTITY | INSURER ON POLICY_ENH.INSURER_EID =INSURER.ENTITY_ID LEFT OUTER JOIN ENTITY | BROKER ON POLICY_ENH.BROKER_EID =BROKER.ENTITY_ID INNER JOIN CODES_TEXT | POLICY_STATUS ON POLICY_ENH.POLICY_STATUS_CODE = POLICY_STATUS.CODE_ID AND POLICY_ENH.POLICY_TYPE = {0}", objCache.GetCodeId("GL", "POLICY_LOB").ToString());
                        }
                        //pmahli MITS 9229 -End
                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY POLICY_ENH.POLICY_ID";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY POLICY_ENH.POLICY_ID";
                        p_sPrimaryField = "POLICY_ID";
                        p_sPrimaryFormField = "policyid";
                        break;

                        case "policyenhpc":
                        //pmahli MITS 9229 reopened -Start
                        //commented sSQL as left outer join written was not compatible with ORACLE 
                        //sSQL = "SELECT POLICY_ID, POLICY_NAME, INSURER.LAST_NAME | INSURER_EID, POLICY_STATUS.CODE_DESC | POL_STATUS_CODE, ISSUE_DATE, BROKER.LAST_NAME | BROKER_EID FROM POLICY_ENH, ENTITY | INSURER, ENTITY | BROKER , CODES_TEXT  | POLICY_STATUS WHERE POLICY_ENH.INSURER_EID *=INSURER.ENTITY_ID AND POLICY_ENH.BROKER_EID *=BROKER.ENTITY_ID AND POLICY_ENH.POLICY_STATUS_CODE =POLICY_STATUS.CODE_ID ";
                        using (LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId))
                        {
                            sSQL = string.Format("SELECT POLICY_ID,POLICY_NAME,INSURER.LAST_NAME | INSURER_EID,POLICY_STATUS.CODE_DESC | POL_STATUS_CODE,ISSUE_DATE,BROKER.LAST_NAME | BROKER_EID FROM POLICY_ENH LEFT OUTER JOIN ENTITY | INSURER ON POLICY_ENH.INSURER_EID =INSURER.ENTITY_ID LEFT OUTER JOIN ENTITY | BROKER ON POLICY_ENH.BROKER_EID =BROKER.ENTITY_ID INNER JOIN CODES_TEXT | POLICY_STATUS ON POLICY_ENH.POLICY_STATUS_CODE = POLICY_STATUS.CODE_ID AND POLICY_ENH.POLICY_TYPE = {0}", objCache.GetCodeId("PC", "POLICY_LOB").ToString());
                        }
                        //pmahli MITS 9229 -End
                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY POLICY_ENH.POLICY_ID";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY POLICY_ENH.POLICY_ID";
                        p_sPrimaryField = "POLICY_ID";
                        p_sPrimaryFormField = "policyid";
                        break;

                        case "policyenhwc":
                        //pmahli MITS 9229 reopened -Start
                        //commented sSQL as left outer join written was not compatible with ORACLE 
                        //sSQL = "SELECT POLICY_ID, POLICY_NAME, INSURER.LAST_NAME | INSURER_EID, POLICY_STATUS.CODE_DESC | POL_STATUS_CODE, ISSUE_DATE, BROKER.LAST_NAME | BROKER_EID FROM POLICY_ENH, ENTITY | INSURER, ENTITY | BROKER , CODES_TEXT  | POLICY_STATUS WHERE POLICY_ENH.INSURER_EID *=INSURER.ENTITY_ID AND POLICY_ENH.BROKER_EID *=BROKER.ENTITY_ID AND POLICY_ENH.POLICY_STATUS_CODE =POLICY_STATUS.CODE_ID ";
                        using (LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId))
                        {
                            sSQL = string.Format("SELECT POLICY_ID,POLICY_NAME,INSURER.LAST_NAME | INSURER_EID,POLICY_STATUS.CODE_DESC | POL_STATUS_CODE,ISSUE_DATE,BROKER.LAST_NAME | BROKER_EID FROM POLICY_ENH LEFT OUTER JOIN ENTITY | INSURER ON POLICY_ENH.INSURER_EID =INSURER.ENTITY_ID LEFT OUTER JOIN ENTITY | BROKER ON POLICY_ENH.BROKER_EID =BROKER.ENTITY_ID INNER JOIN CODES_TEXT | POLICY_STATUS ON POLICY_ENH.POLICY_STATUS_CODE = POLICY_STATUS.CODE_ID AND POLICY_ENH.POLICY_TYPE = {0}", objCache.GetCodeId("WC", "POLICY_LOB").ToString());
                        }
                        //pmahli MITS 9229 -End
                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY POLICY_ENH.POLICY_ID";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY POLICY_ENH.POLICY_ID";
                        p_sPrimaryField = "POLICY_ID";
                        p_sPrimaryFormField = "policyid";
                        break;
                        //Divya - 04/16/2007 MITS 9229 Lookup was throwing error  - END
                        #endregion Enhanced Policy on LOB basis

					case "policycoverage":
                        if (objSettings.MultiCovgPerClm == 0)
                        {
                            if (m_sParentID == String.Empty)
                            {

                                sSQL = "SELECT POLCVG_ROW_ID, POLICY_LIMIT, CODES_TEXT.CODE_DESC | COVERAGE_TYPE FROM POLICY_X_CVG_TYPE, CODES_TEXT WHERE POLICY_ID="
                                + p_objXmlDoc.SelectSingleNode("//policyid").Attributes.GetNamedItem("value").InnerText
                                + " AND POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE=CODES_TEXT.CODE_ID ";
                            }
                            else
                            {
                                sSQL = "SELECT POLCVG_ROW_ID, POLICY_LIMIT, CODES_TEXT.CODE_DESC | COVERAGE_TYPE FROM POLICY_X_CVG_TYPE, CODES_TEXT WHERE POLICY_ID="
                                + m_sParentID
                                + " AND POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE=CODES_TEXT.CODE_ID ";
                            }
                        }
                        else if (objSettings.MultiCovgPerClm == -1)
                        {
                            if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_ORACLE)
                          //dbisht6 start for omig issue
                            {
                                
                                sSQL = "  SELECT POLCVG_ROW_ID,CODES_TEXT.SHORT_CODE || ' ' ||  CODES_TEXT.CODE_DESC | COVERAGE_TYPE, COVERAGE_TEXT";
                            }
                                //dbisht6 end
                            else if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_SQLSRVR)
                                //dbisht6 start of omig issue
                                sSQL = "  SELECT POLCVG_ROW_ID,CODES_TEXT.SHORT_CODE + ' ' + CODES_TEXT.CODE_DESC | COVERAGE_TYPE, COVERAGE_TEXT";
                            // dbisht6 end
                            if (m_sParentID == String.Empty)
                            {

                                sSQL = sSQL + " ,POLICY_LIMIT,FULL_TERM_PREMIUM |PREMIUM, EXPOSURE,SELF_INSURE_DEDUCT | DEDUCTIBLE  FROM POLICY_X_CVG_TYPE, CODES_TEXT WHERE POLICY_UNIT_ROW_ID="
                                + p_objXmlDoc.SelectSingleNode("//PolicyUnitRowId").Attributes.GetNamedItem("value").InnerText
                                + " AND POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE=CODES_TEXT.CODE_ID ";
                            }
                            else
                            {
                                sSQL = sSQL + ",POLICY_LIMIT, FULL_TERM_PREMIUM |PREMIUM, EXPOSURE,SELF_INSURE_DEDUCT | DEDUCTIBLE  FROM POLICY_X_CVG_TYPE, CODES_TEXT WHERE POLICY_UNIT_ROW_ID="
                                + m_sParentID
                                + " AND POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE=CODES_TEXT.CODE_ID ";
                            }
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY POLCVG_ROW_ID";
							}
						}
						else
							sSQL = sSQL + " ORDER BY POLCVG_ROW_ID";
						p_sPrimaryField="POLCVG_ROW_ID";
						p_sPrimaryFormField="polcvgrowid";
						break;
					case "policymco":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT POL_X_MCO_ROW_ID, MCO_EID, ENTITY.LAST_NAME | MCO_NAME, MCO_BEGIN_DATE , MCO_END_DATE FROM POLICY_X_MCO, ENTITY WHERE POLICY_ID="
                            + p_objXmlDoc.SelectSingleNode("//policyid").Attributes.GetNamedItem("value").InnerText
                            + " AND MCO_EID=ENTITY.ENTITY_ID ";
                        }
                        else
                        {
                            sSQL = "SELECT POL_X_MCO_ROW_ID, MCO_EID, ENTITY.LAST_NAME | MCO_NAME, MCO_BEGIN_DATE , MCO_END_DATE FROM POLICY_X_MCO, ENTITY WHERE POLICY_ID="
                            + m_sParentID
                            + " AND MCO_EID=ENTITY.ENTITY_ID ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY POL_X_MCO_ROW_ID";
							}
						}
						else
							sSQL = sSQL + " ORDER BY POL_X_MCO_ROW_ID";
						p_sPrimaryField="POL_X_MCO_ROW_ID";
						p_sPrimaryFormField="polxmcorowid";
						p_sSecondaryField="MCO_EID";
						p_sSecondaryFormField="origvalues";
						p_sThirdField="MCO_BEGIN_DATE";
						p_sForthField="MCO_END_DATE";
						break;
                    // Shruti 6/1/2007 - Lookup for the Enh Policy MCO screen. - starts
                    case "policymcoenh":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT POL_X_MCO_ENH_ROW_ID, MCO_EID, ENTITY.LAST_NAME | MCO_NAME, MCO_BEGIN_DATE , MCO_END_DATE FROM POLICY_X_MCO_ENH, ENTITY WHERE POLICY_ID="
                            + p_objXmlDoc.SelectSingleNode("//policyid").Attributes.GetNamedItem("value").InnerText
                            + " AND MCO_EID=ENTITY.ENTITY_ID ";
                        }
                        else
                        {
                            sSQL = "SELECT POL_X_MCO_ENH_ROW_ID, MCO_EID, ENTITY.LAST_NAME | MCO_NAME, MCO_BEGIN_DATE , MCO_END_DATE FROM POLICY_X_MCO_ENH, ENTITY WHERE POLICY_ID="
                            + m_sParentID
                            + " AND MCO_EID=ENTITY.ENTITY_ID ";
                        }
                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY POL_X_MCO_ENH_ROW_ID";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY POL_X_MCO_ENH_ROW_ID";
                        p_sPrimaryField = "POL_X_MCO_ENH_ROW_ID";
                        p_sPrimaryFormField = "polxmcoenhrowid";
                        p_sSecondaryField = "MCO_EID";
                        p_sSecondaryFormField = "origvalues";
                        p_sThirdField = "MCO_BEGIN_DATE";
                        p_sForthField = "MCO_END_DATE";
                        break;
                    // Shruti 6/1/2007 - Lookup for the Enh Policy MCO screen. - ends
					// Mihika 01/12/2007 MITS 8656 - Lookup for the 'Insurer' screen.
					case "policyinsurer":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT IN_ROW_ID, INSURER_CODE, ENTITY.LAST_NAME | INSURER, RES_PERCENTAGE FROM POLICY_X_INSURER, ENTITY WHERE POLICY_ID ="
                            + p_objXmlDoc.SelectSingleNode("//policyid").Attributes.GetNamedItem("value").InnerText
                             //MITS 16126 :Umesh
                            //+ " AND INSURER_CODE=ENTITY.ENTITY_ID ORDER BY IN_ROW_ID ";
                            + " AND INSURER_CODE=ENTITY.ENTITY_ID ";
                        }
                        else
                        {
                            sSQL = "SELECT IN_ROW_ID, INSURER_CODE, ENTITY.LAST_NAME | INSURER, RES_PERCENTAGE FROM POLICY_X_INSURER, ENTITY WHERE POLICY_ID ="
                            + m_sParentID
                             //MITS 16126 :Umesh
                            //+ " AND INSURER_CODE=ENTITY.ENTITY_ID ORDER BY IN_ROW_ID ";
                            + " AND INSURER_CODE=ENTITY.ENTITY_ID ";
                        }

                        //MITS 16126 :Umesh
                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY IN_ROW_ID";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY IN_ROW_ID";
                        //MITS 16126 End
						p_sPrimaryField ="IN_ROW_ID";
						p_sPrimaryFormField="inrowid";
						p_sSecondaryField = "INSURER_CODE";
						p_sSecondaryFormField = "insentityid";
						break;
                        // npadhy MITS 15614 Start Added the Lookup Code for Policy Reinsurer
                    case "policyreinsurer":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT POL_X_INS_REINS_ROW_ID, REINSURER_EID, ENTITY.LAST_NAME | REINSURER, PREMIUM_AMOUNT, INSTAL_PAYMENT_AMT Installment_payment, SCHEDULE.CODE_DESC Installment_Schedule FROM POLICY_X_INS_REINS, ENTITY , CODES_TEXT | SCHEDULE WHERE POL_X_INS_ROW_ID ="
                            + p_objXmlDoc.SelectSingleNode("//inrowid").Attributes.GetNamedItem("value").InnerText
                            + " AND REINSURER_EID=ENTITY.ENTITY_ID AND SCHEDULE.CODE_ID = INSTAL_SCHEDULE_CODE";
                        }
                        else
                        {
                            sSQL = "SELECT POL_X_INS_REINS_ROW_ID, REINSURER_EID, ENTITY.LAST_NAME | REINSURER, PREMIUM_AMOUNT,INSTAL_PAYMENT_AMT Installment_payment, SCHEDULE.CODE_DESC Installment_Schedule  FROM POLICY_X_INS_REINS, ENTITY WHERE POL_X_INS_ROW_ID ="
                            + m_sParentID
                            + " AND REINSURER_EID=ENTITY.ENTITY_ID AND SCHEDULE.CODE_ID = INSTAL_SCHEDULE_CODE";
                        }

                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY POL_X_INS_REINS_ROW_ID";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY POL_X_INS_REINS_ROW_ID";
                        p_sPrimaryField = "POL_X_INS_REINS_ROW_ID";
                        p_sPrimaryFormField = "reinsurerrowid";
                        p_sSecondaryField = "REINSURER_EID";
                        p_sSecondaryFormField = "reinsentityid";
                        // npadhy MITS 15614 End
                        break;
					case "vehicle":
                        //bkumar33
						//sSQL="SELECT UNIT_ID, VIN, VEHICLE_MAKE, VEHICLE_YEAR, LICENSE_NUMBER FROM VEHICLE WHERE DELETED_FLAG=0";
                        sSQL = "SELECT UNIT_ID, VIN as VEHICLE_ID, VEHICLE_MAKE, VEHICLE_YEAR, LICENSE_NUMBER FROM VEHICLE WHERE DELETED_FLAG=0";
                        //end
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY UNIT_ID";
							}
						}		
						else
							sSQL = sSQL + " ORDER BY UNIT_ID";
						p_sPrimaryField="UNIT_ID";
						p_sPrimaryFormField="unitid";
						break;
					case "vehicleinspections":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT UNIT_INSP_ROW_ID, UNIT_ID, INSPECTION_DATE, INSPECTION_RESULT FROM VEHICLE_X_INSPCT WHERE UNIT_ID="
                            + p_objXmlDoc.SelectSingleNode("//unitid").Attributes.GetNamedItem("value").InnerText;
                        }
                        else
                        {
                            sSQL = "SELECT UNIT_INSP_ROW_ID, UNIT_ID, INSPECTION_DATE, INSPECTION_RESULT FROM VEHICLE_X_INSPCT WHERE UNIT_ID="
                            + m_sParentID;
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY UNIT_INSP_ROW_ID";
							}
						}
						else
							sSQL = sSQL + " ORDER BY UNIT_INSP_ROW_ID";
                        //Raman: 02/22/2008: Hot Fix for R3Patchset 4
                        //Inspection Lookup wasn't working as system was trying to fetch unit id instead of inspection id
                        //p_sPrimaryField="UNIT_ID";
                        //p_sPrimaryFormField="unitid";
                        //p_sSecondaryField="UNIT_INSP_ROW_ID";
                        //p_sSecondaryFormField="unitinsprowid";
                        p_sPrimaryField = "UNIT_INSP_ROW_ID";
                        p_sPrimaryFormField = "unitinsprowid";
                        p_sSecondaryField = "UNIT_ID";
                        p_sSecondaryFormField = "unitid";
						break;
					case "people":
                        if (m_sParentID == String.Empty)
                        {
                            //avipinsrivas start : Worked for Jira-340
                            sSQL = "SELECT E.ENTITY_ID, E.LAST_NAME, E.FIRST_NAME, E.ALSO_KNOWN_AS, E.TAX_ID, E.PHONE1 FROM ENTITY E WHERE";
                            //if (objSettings.UseEntityRole)
                            //{
                            //    sSQL = string.Concat(sSQL, " LEFT OUTER JOIN ENTITY_X_ROLES EXR ON EXR.ENTITY_ID = E.ENTITY_ID ");
                            //    sSQL = string.Concat(sSQL, " LEFT OUTER JOIN GLOSSARY G ON G.TABLE_ID = EXR.ENTITY_TABLE_ID ");
                            //    sSQL = string.Concat(sSQL, " WHERE G.GLOSSARY_TYPE_CODE = 7 ");
                            //    sSQL = string.Concat(sSQL, " AND G.SYSTEM_TABLE_NAME NOT IN ('EMPLOYEES', 'PHYSICIANS', 'MEDICAL_STAFF', 'PATIENTS', 'WITNESS') ");
                            //}
                            //else
                            if (!objSettings.UseEntityRole)
                            {
                                sSQL = string.Concat(sSQL, "  E.ENTITY_TABLE_ID= " + p_objXmlDoc.SelectSingleNode("//entitytableid").Attributes.GetNamedItem("value").InnerText);
                                sSQL = string.Concat(sSQL, " AND ");
                            }
                            sSQL = string.Concat(sSQL, "  E.DELETED_FLAG <> -1 ");
                            sSQL = string.Concat(sSQL, " GROUP BY E.ENTITY_ID, E.LAST_NAME, E.FIRST_NAME, E.ALSO_KNOWN_AS, E.TAX_ID, E.PHONE1 ");       //avipinsrivas Start : Worked for JIRA - 5407
                            //avipinsrivas End
                        }
                        else
                        {
                            if (!objSettings.UseEntityRole)
                            {
                                sSQL = "SELECT E.ENTITY_ID, E.LAST_NAME, E.FIRST_NAME, E.ALSO_KNOWN_AS, E.TAX_ID, E.PHONE1 FROM ENTITY E WHERE E.ENTITY_TABLE_ID="
                                + m_sParentID
                                    //shruti, 20th dec 2006, MITS 8176 starts
                                + " AND E.DELETED_FLAG <> -1";
                                //shruti, 20th dec 2006, MITS 8176 ends
                            }
                            else
                            {
                                sSQL = "SELECT E.ENTITY_ID, E.LAST_NAME, E.FIRST_NAME, E.ALSO_KNOWN_AS, E.TAX_ID, E.PHONE1 FROM ENTITY E WHERE E.DELETED_FLAG <> -1 ";
                            }
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
                                sSQL = sSQL + " ORDER BY E.ENTITY_ID";
							}
						}	
						else
                            sSQL = sSQL + " ORDER BY E.ENTITY_ID";
						p_sPrimaryField="ENTITY_ID";
						p_sPrimaryFormField="entityid";
						break;
					case "employee":
						sSQL="SELECT EMPLOYEE_EID, EMPLOYEE_NUMBER, LAST_NAME, FIRST_NAME, ALSO_KNOWN_AS,";
                        if (m_objUserLogin.IsAllowedEx(RMPermissions.RMO_EMPLOYEE, RMPermissions.RMO_EMPLOYEE_VIEW_SSN))
                            sSQL += "TAX_ID, ";
                        sSQL += "PHONE1 FROM EMPLOYEE, ENTITY WHERE EMPLOYEE_EID=ENTITY.ENTITY_ID ";
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY EMPLOYEE_EID";
							}
						}	
						else
							sSQL = sSQL + " ORDER BY EMPLOYEE_EID";
						p_sPrimaryField="EMPLOYEE_EID";
						p_sPrimaryFormField="employeeeid";
						break;
					case "violation":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT EMP_X_VIOLATION.EMPLOYEE_EID, VIOLATION_ID, VIOLATION_DATE, VIOLATION.CODE_DESC | Violation FROM EMP_X_VIOLATION, CODES_TEXT | Violation WHERE EMP_X_VIOLATION.EMPLOYEE_EID="
                            + p_objXmlDoc.SelectSingleNode("//employeeeid").Attributes.GetNamedItem("value").InnerText
                            + " AND VIOLATION.CODE_ID=VIOLATION_CODE ";
                        }
                        else
                        {
                            sSQL = "SELECT EMP_X_VIOLATION.EMPLOYEE_EID, VIOLATION_ID, VIOLATION_DATE, VIOLATION.CODE_DESC | Violation FROM EMP_X_VIOLATION, CODES_TEXT | Violation WHERE EMP_X_VIOLATION.EMPLOYEE_EID="
                            + m_sParentID
                            + " AND VIOLATION.CODE_ID=VIOLATION_CODE ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY VIOLATION_DATE";
							}
						}
						else
							sSQL = sSQL + " ORDER BY VIOLATION_DATE";
                        //Raman: 02/22/2008: Hot Fix for R3Patchset 4
                        //Violation Lookup wasn't working as system was trying to fetch employee id instead of violation id
                        //p_sPrimaryField="EMPLOYEE_EID";
                        //p_sPrimaryFormField="employeeeid";
                        //p_sSecondaryField="VIOLATION_ID";
                        //p_sSecondaryFormField="violationid";
                        p_sPrimaryField = "VIOLATION_ID";
                        p_sPrimaryFormField = "violationid";
                        p_sSecondaryField = "EMPLOYEE_EID";
                        p_sSecondaryFormField = "employeeeid";
						break;
					case "dependent":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT EMP_X_DEPENDENT.EMPLOYEE_EID, EMP_DEP_ROW_ID, DEPENDENT.LAST_NAME, DEPENDENT.FIRST_NAME, HEALTH_PLAN_FLAG, RELATION.CODE_DESC | Relation FROM EMP_X_DEPENDENT, ENTITY | DEPENDENT, CODES_TEXT | RELATION WHERE EMP_X_DEPENDENT.EMPLOYEE_EID="
                            + p_objXmlDoc.SelectSingleNode("//employeeeid").Attributes.GetNamedItem("value").InnerText
                            + " AND DEPENDENT.ENTITY_ID=DEPENDENT_EID AND RELATION.CODE_ID=RELATION_CODE ";
                        }
                        else
                        {
                            sSQL = "SELECT EMP_X_DEPENDENT.EMPLOYEE_EID, EMP_DEP_ROW_ID, DEPENDENT.LAST_NAME, DEPENDENT.FIRST_NAME, HEALTH_PLAN_FLAG, RELATION.CODE_DESC | Relation FROM EMP_X_DEPENDENT, ENTITY | DEPENDENT, CODES_TEXT | RELATION WHERE EMP_X_DEPENDENT.EMPLOYEE_EID="
                            + m_sParentID
                            + " AND DEPENDENT.ENTITY_ID=DEPENDENT_EID AND RELATION.CODE_ID=RELATION_CODE ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY EMP_DEP_ROW_ID";
							}
						}	
						else
							sSQL = sSQL + " ORDER BY EMP_DEP_ROW_ID";
                        //Raman: 02/22/2008: Hot Fix for R3Patchset 4
                        //Dependent Lookup wasn't working as system was trying to fetch employee id instead of dependent row id
                        //p_sPrimaryField="EMPLOYEE_EID";
                        //p_sPrimaryFormField="employeeeid";
                        //p_sSecondaryField="EMP_DEP_ROW_ID";
                        //p_sSecondaryFormField="empdeprowid";
                        p_sPrimaryField = "EMP_DEP_ROW_ID";
                        p_sPrimaryFormField = "empdeprowid";
                        p_sSecondaryField = "EMPLOYEE_EID";
                        p_sSecondaryFormField = "employeeeid";
						break;
					case "entitymaint":
                        if (m_sParentID == String.Empty)
                        {
                            //avipinsrivas start : Worked for Jira-340
                            sSQL = "SELECT E.ENTITY_ID, E.LAST_NAME, E.ABBREVIATION, E.TAX_ID, E.PHONE1 | PHONE_NUMBER FROM ENTITY E ";
                            //RMA -17617 , As ENTITY_X_ROLES table is no longer valid
                            //if (objSettings.UseEntityRole)
                            //{
                            //    sSQL = string.Concat(sSQL, " LEFT OUTER JOIN ENTITY_X_ROLES EXR ON EXR.ENTITY_ID = E.ENTITY_ID ");
                            //    sSQL = string.Concat(sSQL, " LEFT OUTER JOIN GLOSSARY G ON G.TABLE_ID = EXR.ENTITY_TABLE_ID ");
                            //    //avipinsrivas start : Worked for Jira-7767
                            //    //sSQL = string.Concat(sSQL, " WHERE G.GLOSSARY_TYPE_CODE = 4 ");
                            //    sSQL = string.Concat(sSQL, " WHERE E.ENTITY_ID > 0 ");
                            //    //avipinsrivas end
                            //}
                            sSQL = string.Concat(sSQL, " WHERE E.DELETED_FLAG <> -1 ");
                            //else
                            if (!objSettings.UseEntityRole)
                               sSQL = string.Concat(sSQL, " AND E.ENTITY_TABLE_ID= " + p_objXmlDoc.SelectSingleNode("//entitytableid").Attributes.GetNamedItem("value").InnerText);
                            //sSQL = string.Concat(sSQL, " AND E.DELETED_FLAG <> -1 ");                            
                            //if (objSettings.UseEntityRole)
                                sSQL = string.Concat(sSQL, " GROUP BY E.ENTITY_ID, E.LAST_NAME, E.ABBREVIATION, E.TAX_ID, E.PHONE1 ");
                            //avipinsrivas End
                        }
                        else
                        {
                            if (!objSettings.UseEntityRole)
                            {
                                sSQL = "SELECT E.ENTITY_ID, E.LAST_NAME, E.ABBREVIATION, E.TAX_ID, E.PHONE1 | PHONE_NUMBER FROM ENTITY E WHERE E.DELETED_FLAG <> -1 AND E.ENTITY_TABLE_ID="
                                + m_sParentID;
                            }
                            else 
                            {
                                sSQL = "SELECT E.ENTITY_ID, E.LAST_NAME, E.ABBREVIATION, E.TAX_ID, E.PHONE1 | PHONE_NUMBER FROM ENTITY E WHERE E.DELETED_FLAG <> -1 ";
                            }
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
                                sSQL = sSQL + " ORDER BY E.ENTITY_ID";
							}
						}
						else
							sSQL = sSQL + " ORDER BY ENTITY_ID";
						p_sPrimaryField="ENTITY_ID";
						p_sPrimaryFormField="entityid";
						break;
					case "patient":
						sSQL="SELECT PATIENT_ID, LAST_NAME, FIRST_NAME, PATIENT_ACCT_NO, MEDICAL_RCD_NO FROM PATIENT, ENTITY WHERE PATIENT_EID=ENTITY.ENTITY_ID ";
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY PATIENT_ID";
							}
						}	
						else
							sSQL = sSQL + " ORDER BY PATIENT_ID";
						p_sPrimaryField="PATIENT_ID";
						p_sPrimaryFormField="patientid";
						break;
					case "patientprocedure":
                        //Raman: 02/22/2008: Hot Fix for R3Patchset 4
                        //Removed "Procedure" from query..Wrong QUERY
                        //sSQL="SELECT PATIENT_ID, PROC_ROW_ID, DATE_OF_PROCEDURE, CODES_TEXT.CODE_DESC | Procedure, COMPLICATION_DATE FROM PATIENT_PROCEDURE, CODES_TEXT WHERE PATIENT_ID=" 
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT PATIENT_ID, PROC_ROW_ID, DATE_OF_PROCEDURE, CODES_TEXT.CODE_DESC | Procedures, COMPLICATION_DATE FROM PATIENT_PROCEDURE, CODES_TEXT WHERE PATIENT_ID="
                            + p_objXmlDoc.SelectSingleNode("//patientid").Attributes.GetNamedItem("value").InnerText
                            + " AND CODES_TEXT.CODE_ID=PROCEDURE_CODE ";
                        }
                        else
                        {
                            sSQL = "SELECT PATIENT_ID, PROC_ROW_ID, DATE_OF_PROCEDURE, CODES_TEXT.CODE_DESC | Procedures, COMPLICATION_DATE FROM PATIENT_PROCEDURE, CODES_TEXT WHERE PATIENT_ID="
                            + m_sParentID
                            + " AND CODES_TEXT.CODE_ID=PROCEDURE_CODE ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY PROC_ROW_ID";
							}
						}	
						else
							sSQL = sSQL + " ORDER BY PROC_ROW_ID";
                        //Raman: 02/22/2008: Hot Fix for R3Patchset 4
                        //Procedure Lookup wasn't working as system was trying to fetch patient id instead of procedure id
                        //p_sPrimaryField="PATIENT_ID";
                        //p_sPrimaryFormField="patientid";
                        //p_sSecondaryField="PROC_ROW_ID";
                        //p_sSecondaryFormField="procrowid";
                        p_sPrimaryField="PROC_ROW_ID";
						p_sPrimaryFormField="procrowid";
						p_sSecondaryField="PATIENT_ID";
						p_sSecondaryFormField="patientid";
						break;
					case "physician":
						sSQL="SELECT PHYS_EID, LAST_NAME, FIRST_NAME, PHYSICIAN_NUMBER, MED_STAFF_NUMBER FROM PHYSICIAN, ENTITY WHERE PHYS_EID=ENTITY.ENTITY_ID ";
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY PHYS_EID";
							}
						}	
						else
							sSQL = sSQL + " ORDER BY PHYS_EID";
						p_sPrimaryField="PHYS_EID";
						p_sPrimaryFormField="physeid";
						break;
					case "physicianprivilege":
                        //Raman: 02/22/2008: Hot Fix for R3Patchset 4
                        //Replaced "physed" in query with "physeid"
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT PRIV_ID, PHYS_EID, CATEGORY.CODE_DESC | Category, TYPE.CODE_DESC | Type, STATUS.CODE_DESC | Status, INT_DATE | Start_Date, END_DATE | End_Date FROM PHYS_PRIVS, CODES_TEXT | CATEGORY, CODES_TEXT | TYPE, CODES_TEXT | STATUS WHERE PHYS_EID="
                                //+ p_objXmlDoc.SelectSingleNode("physed").Attributes.GetNamedItem("value").InnerText
                            + p_objXmlDoc.SelectSingleNode("//physeid").Attributes.GetNamedItem("value").InnerText
                            + " AND CATEGORY.CODE_ID=PHYS_PRIVS.CATEGORY_CODE AND TYPE.CODE_ID=PHYS_PRIVS.TYPE_CODE AND STATUS.CODE_ID=PHYS_PRIVS.STATUS_CODE ";
                        }
                        else
                        {
                            sSQL = "SELECT PRIV_ID, PHYS_EID, CATEGORY.CODE_DESC | Category, TYPE.CODE_DESC | Type, STATUS.CODE_DESC | Status, INT_DATE | Start_Date, END_DATE | End_Date FROM PHYS_PRIVS, CODES_TEXT | CATEGORY, CODES_TEXT | TYPE, CODES_TEXT | STATUS WHERE PHYS_EID="
                                //+ p_objXmlDoc.SelectSingleNode("physed").Attributes.GetNamedItem("value").InnerText
                            + m_sParentID
                            + " AND CATEGORY.CODE_ID=PHYS_PRIVS.CATEGORY_CODE AND TYPE.CODE_ID=PHYS_PRIVS.TYPE_CODE AND STATUS.CODE_ID=PHYS_PRIVS.STATUS_CODE ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY PRIV_ID";
							}
						}	
						else
							sSQL = sSQL + " ORDER BY PRIV_ID";
                        //Raman: 02/22/2008: Hot Fix for R3Patchset 4
                        //Privelige Lookup wasn't working as system was trying to fetch phyician id instead of previlege id
                        //p_sPrimaryField="PHYS_EID";
                        //p_sPrimaryFormField="physeid";
                        //p_sSecondaryField="PRIV_ID";
                        //p_sSecondaryFormField="privid";
                        p_sPrimaryField = "PRIV_ID";
                        p_sPrimaryFormField = "privid";
                        p_sSecondaryField = "PHYS_EID";
                        p_sSecondaryFormField = "physeid";
						break;
					case "physiciancertification":
                        //Raman: 02/22/2008: Hot Fix for R3Patchset 4
                        //Replaced "physed" in query with "physeid"
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT CERT_ID, PHYS_EID, NAME.CODE_DESC | Name, STATUS.CODE_DESC | Status, BOARD.CODE_DESC | Board, INT_DATE | Start_Date, END_DATE | End_Date FROM PHYS_CERTS, CODES_TEXT | NAME, CODES_TEXT | STATUS, CODES_TEXT | BOARD WHERE PHYS_EID="
                                //+ p_objXmlDoc.SelectSingleNode("physed").Attributes.GetNamedItem("value").InnerText
                            + p_objXmlDoc.SelectSingleNode("//physeid").Attributes.GetNamedItem("value").InnerText
                            + " AND NAME.CODE_ID=PHYS_CERTS.NAME_CODE AND STATUS.CODE_ID=PHYS_CERTS.STATUS_CODE AND BOARD.CODE_ID=PHYS_CERTS.BOARD_CODE ";
                        }
                        else
                        {
                            sSQL = "SELECT CERT_ID, PHYS_EID, NAME.CODE_DESC | Name, STATUS.CODE_DESC | Status, BOARD.CODE_DESC | Board, INT_DATE | Start_Date, END_DATE | End_Date FROM PHYS_CERTS, CODES_TEXT | NAME, CODES_TEXT | STATUS, CODES_TEXT | BOARD WHERE PHYS_EID="
                                //+ p_objXmlDoc.SelectSingleNode("physed").Attributes.GetNamedItem("value").InnerText
                            + m_sParentID
                            + " AND NAME.CODE_ID=PHYS_CERTS.NAME_CODE AND STATUS.CODE_ID=PHYS_CERTS.STATUS_CODE AND BOARD.CODE_ID=PHYS_CERTS.BOARD_CODE ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY CERT_ID";
							}
						}		
						else
							sSQL = sSQL + " ORDER BY CERT_ID";
                        //Raman: 02/22/2008: Hot Fix for R3Patchset 4
                        //Cerification Lookup wasn't working as system was trying to fetch phyician id instead of Certification id
                        //p_sPrimaryField="PHYS_EID";
                        //p_sPrimaryFormField="physeid";
                        //p_sSecondaryField="CERT_ID";
                        //p_sSecondaryFormField="certid";
                        p_sPrimaryField = "CERT_ID";
                        p_sPrimaryFormField = "certid";
                        p_sSecondaryField = "PHYS_EID";
                        p_sSecondaryFormField = "physeid";
						break;
					case "physicianeducation":
                        //Raman: 02/22/2008: Hot Fix for R3Patchset 4
                        //Replaced "physed" in query with "physeid"
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT EDUC_ID, PHYS_EID, EDUC.CODE_DESC | Type, INSTITUTION.LAST_NAME | Institution, DEGREE.CODE_DESC | Degree_Type, DEGREE_DATE FROM PHYS_EDUCATION, CODES_TEXT | EDUC, CODES_TEXT | DEGREE, ENTITY | INSTITUTION WHERE PHYS_EID="
                                //+ p_objXmlDoc.SelectSingleNode("physed").Attributes.GetNamedItem("value").InnerText
                            + p_objXmlDoc.SelectSingleNode("//physeid").Attributes.GetNamedItem("value").InnerText
                            + " AND EDUC.CODE_ID=PHYS_EDUCATION.EDUC_TYPE_CODE AND DEGREE.CODE_ID=PHYS_EDUCATION.DEGREE_TYPE AND PHYS_EDUCATION.INSTITUTION_EID=INSTITUTION.ENTITY_ID ";
                        }
                        else
                        {
                            sSQL = "SELECT EDUC_ID, PHYS_EID, EDUC.CODE_DESC | Type, INSTITUTION.LAST_NAME | Institution, DEGREE.CODE_DESC | Degree_Type, DEGREE_DATE FROM PHYS_EDUCATION, CODES_TEXT | EDUC, CODES_TEXT | DEGREE, ENTITY | INSTITUTION WHERE PHYS_EID="
                                //+ p_objXmlDoc.SelectSingleNode("physed").Attributes.GetNamedItem("value").InnerText
                            + m_sParentID
                            + " AND EDUC.CODE_ID=PHYS_EDUCATION.EDUC_TYPE_CODE AND DEGREE.CODE_ID=PHYS_EDUCATION.DEGREE_TYPE AND PHYS_EDUCATION.INSTITUTION_EID=INSTITUTION.ENTITY_ID ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY EDUC_ID";
							}
						}	
						else
							sSQL = sSQL + " ORDER BY EDUC_ID";
                        //Raman: 02/22/2008: Hot Fix for R3Patchset 4
                        //Education Lookup wasn't working as system was trying to fetch phyician id instead of Education id
                        //p_sPrimaryField="PHYS_EID";
                        //p_sPrimaryFormField="physeid";
                        //p_sSecondaryField="EDUC_ID";
                        //p_sSecondaryFormField="educid";
                        p_sPrimaryField = "EDUC_ID";
                        p_sPrimaryFormField = "educid";
                        p_sSecondaryField = "PHYS_EID";
                        p_sSecondaryFormField = "physeid";
						break;
					case "physicianprevhospital":
                        //Raman: 02/22/2008: Hot Fix for R3Patchset 4
                        //Replaced "physed" in query with "physeid"
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT PREV_HOSP_ID, PHYS_EID, STATUS.CODE_DESC | Status, HOSPITAL.LAST_NAME | Hospital, PRIVILEGE.CODE_DESC | Privilege, INT_DATE | Start_Date, END_DATE | End_Date FROM PHYS_PREV_HOSP, CODES_TEXT | STATUS, CODES_TEXT | PRIVILEGE, ENTITY | HOSPITAL WHERE PHYS_EID="
                                //+ p_objXmlDoc.SelectSingleNode("physed").Attributes.GetNamedItem("value").InnerText
                            + p_objXmlDoc.SelectSingleNode("//physeid").Attributes.GetNamedItem("value").InnerText
                            + " AND STATUS.CODE_ID=PHYS_PREV_HOSP.STATUS_CODE AND PRIVILEGE.CODE_ID=PHYS_PREV_HOSP.PRIV_CODE AND PHYS_PREV_HOSP.HOSPITAL_EID=HOSPITAL.ENTITY_ID ";
                        }
                        else
                        {
                            sSQL = "SELECT PREV_HOSP_ID, PHYS_EID, STATUS.CODE_DESC | Status, HOSPITAL.LAST_NAME | Hospital, PRIVILEGE.CODE_DESC | Privilege, INT_DATE | Start_Date, END_DATE | End_Date FROM PHYS_PREV_HOSP, CODES_TEXT | STATUS, CODES_TEXT | PRIVILEGE, ENTITY | HOSPITAL WHERE PHYS_EID="
                                //+ p_objXmlDoc.SelectSingleNode("physed").Attributes.GetNamedItem("value").InnerText
                            + m_sParentID
                            + " AND STATUS.CODE_ID=PHYS_PREV_HOSP.STATUS_CODE AND PRIVILEGE.CODE_ID=PHYS_PREV_HOSP.PRIV_CODE AND PHYS_PREV_HOSP.HOSPITAL_EID=HOSPITAL.ENTITY_ID ";
                        }
                        
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY PREV_HOSP_ID";
							}
						}
						else
							sSQL = sSQL + " ORDER BY PREV_HOSP_ID";
                        //Raman: 02/22/2008: Hot Fix for R3Patchset 4
                        //Previous Hospital Lookup wasn't working as system was trying to fetch phyician id instead of prev hosp id
                        //p_sPrimaryField="PHYS_EID";
                        //p_sPrimaryFormField="physeid";
                        //p_sSecondaryField="PREV_HOSP_ID";
                        //p_sSecondaryFormField="prevhospid";
                        p_sPrimaryField = "PREV_HOSP_ID";
                        p_sPrimaryFormField = "prevhospid";
                        p_sSecondaryField = "PHYS_EID";
                        p_sSecondaryFormField = "physeid";
						break;
					case "staff":
						sSQL="SELECT STAFF_EID, LAST_NAME, FIRST_NAME, MED_STAFF_NUMBER FROM MED_STAFF, ENTITY WHERE STAFF_EID=ENTITY.ENTITY_ID ";
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY STAFF_EID";
							}
						}	
						else
							sSQL = sSQL + " ORDER BY STAFF_EID";
						p_sPrimaryField="STAFF_EID";
						p_sPrimaryFormField="staffeid";
						break;
					case "staffprivilege":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT STAFF_PRIV_ID, STAFF_EID, CATEGORY.CODE_DESC | Category, TYPE.CODE_DESC | Type, STATUS.CODE_DESC | Status, INT_DATE | Start_Date, END_DATE | End_Date FROM STAFF_PRIVS, CODES_TEXT | CATEGORY, CODES_TEXT | TYPE, CODES_TEXT | STATUS WHERE STAFF_EID="
                            + p_objXmlDoc.SelectSingleNode("//staffeid").Attributes.GetNamedItem("value").InnerText
                            + " AND CATEGORY.CODE_ID=STAFF_PRIVS.CATEGORY_CODE AND TYPE.CODE_ID=STAFF_PRIVS.TYPE_CODE AND STATUS.CODE_ID=STAFF_PRIVS.STATUS_CODE ";
                        }
                        else
                        {
                            sSQL = "SELECT STAFF_PRIV_ID, STAFF_EID, CATEGORY.CODE_DESC | Category, TYPE.CODE_DESC | Type, STATUS.CODE_DESC | Status, INT_DATE | Start_Date, END_DATE | End_Date FROM STAFF_PRIVS, CODES_TEXT | CATEGORY, CODES_TEXT | TYPE, CODES_TEXT | STATUS WHERE STAFF_EID="
                            + m_sParentID
                            + " AND CATEGORY.CODE_ID=STAFF_PRIVS.CATEGORY_CODE AND TYPE.CODE_ID=STAFF_PRIVS.TYPE_CODE AND STATUS.CODE_ID=STAFF_PRIVS.STATUS_CODE ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY STAFF_PRIV_ID";
							}
						}	
						else
							sSQL = sSQL + " ORDER BY STAFF_PRIV_ID";
                        //Raman: 02/22/2008: Hot Fix for R3Patchset 4
                        //Privilege Lookup wasn't working as system was trying to fetch staff id instead of privilege id
                        //p_sPrimaryField = "STAFF_EID";
                        //p_sPrimaryFormField = "staffeid";
                        //p_sSecondaryField = "STAFF_PRIV_ID";
                        //p_sSecondaryFormField = "privid";
                        p_sPrimaryField = "STAFF_PRIV_ID";
                        p_sPrimaryFormField = "privid";
                        p_sSecondaryField = "STAFF_EID";
                        p_sSecondaryFormField = "staffeid";
						break;
					case "staffcertification":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT STAFF_CERT_ID, STAFF_EID, NAME.CODE_DESC | Name, INT_DATE | Start_Date, END_DATE | End_Date FROM STAFF_CERTS, CODES_TEXT | NAME WHERE STAFF_EID="
                            + p_objXmlDoc.SelectSingleNode("//staffeid").Attributes.GetNamedItem("value").InnerText
                            + " AND NAME.CODE_ID=STAFF_CERTS.NAME_CODE ";
                        }
                        else
                        {
                            sSQL = "SELECT STAFF_CERT_ID, STAFF_EID, NAME.CODE_DESC | Name, INT_DATE | Start_Date, END_DATE | End_Date FROM STAFF_CERTS, CODES_TEXT | NAME WHERE STAFF_EID="
                            + m_sParentID
                            + " AND NAME.CODE_ID=STAFF_CERTS.NAME_CODE ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY STAFF_CERT_ID";
							}
						}	
						else
							sSQL = sSQL + " ORDER BY STAFF_CERT_ID";
                        //Raman: 02/22/2008: Hot Fix for R3Patchset 4
                        //Certification Lookup wasn't working as system was trying to fetch staff id instead of cert id
                        p_sPrimaryField = "STAFF_CERT_ID";
                        p_sPrimaryFormField = "certid";
                        p_sSecondaryField = "STAFF_EID";
                        p_sSecondaryFormField = "staffeid";
						break;
					case "bankaccount":
						sSQL="SELECT ACCOUNT_ID, ACCOUNT_NUMBER, ACCOUNT_NAME, CODES_TEXT.CODE_DESC | ACCOUNT_TYPE, BANK.LAST_NAME | BANK_NAME, OWNER.LAST_NAME | OWNER_NAME FROM ACCOUNT, ENTITY | BANK, ENTITY | OWNER, CODES_TEXT WHERE BANK_EID=BANK.ENTITY_ID AND OWNER_EID=OWNER.ENTITY_ID AND ACCT_TYPE_CODE=CODES_TEXT.CODE_ID ";
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY ACCOUNT_ID";
							}
						}
						else
							sSQL = sSQL + " ORDER BY ACCOUNT_ID";
						p_sPrimaryField="ACCOUNT_ID";
						p_sPrimaryFormField="accountid";
						break;
					case "bankaccountsub":
                        //sgoel6 MITS 15290 Filter for Account ID added
                        string sAccountId = string.Empty;
                        if (p_objXmlDoc.SelectSingleNode("//accountid").Attributes.GetNamedItem("value") != null)
                            sAccountId = p_objXmlDoc.SelectSingleNode("//accountid").Attributes.GetNamedItem("value").InnerText;
                        
                       //sSQL="SELECT ACCOUNT_ID, SUB_ACC_NUMBER, SUB_ACC_NAME, OWNER.LAST_NAME | OWNER_NAME FROM BANK_ACC_SUB, ENTITY | OWNER WHERE OWNER_EID=OWNER.ENTITY_ID ";
                        //Abhishek Added  SUB_ROW_ID col with the query
                        sSQL = "SELECT ACCOUNT_ID, SUB_ACC_NUMBER, SUB_ACC_NAME, OWNER.LAST_NAME | OWNER_NAME, SUB_ROW_ID FROM BANK_ACC_SUB, ENTITY | OWNER WHERE OWNER_EID=OWNER.ENTITY_ID ";
						 if (sAccountId != "")
                            sSQL = sSQL + " AND BANK_ACC_SUB.ACCOUNT_ID = " + sAccountId + " " ;
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY ACCOUNT_ID";
							}
						}
						else
							sSQL = sSQL + " ORDER BY ACCOUNT_ID";
						p_sPrimaryField="ACCOUNT_ID";
						p_sPrimaryFormField="accountid";
						break;
					case "deposit":
                        iAccountId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//accountid").Attributes.GetNamedItem("value").InnerText);

                        //Changed by gagan for MITS 22749 : Start
                        SysSettings objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                        if (objSysSettings.UseFundsSubAcc)
                        {
                            if (iAccountId > 0)
                                //MGaba2:MITS 9180-04/14/2008-Formatting of Flag fields require "_FLAG" at the end of column header to convert 0/-1 to Yes/No
                                //sSQL="SELECT DEPOSIT_ID, CTL_NUMBER, TRANS_DATE, AMOUNT, CLEARED_FLAG | CLEARED, VOID_FLAG | VOID FROM FUNDS_DEPOSIT WHERE BANK_ACC_ID=" + iAccountId.ToString() ;
                        //Added by Amitosh for Mits 24089 (03/17/2011)       
                                // sSQL = "SELECT ACCOUNT.ACCOUNT_NAME, BANK_ACC_SUB.SUB_ACC_NAME, DEPOSIT_ID, CTL_NUMBER, TRANS_DATE, AMOUNT, CLEARED_FLAG , VOID_FLAG  FROM FUNDS_DEPOSIT, ACCOUNT, BANK_ACC_SUB WHERE FUNDS_DEPOSIT.BANK_ACC_ID=" + iAccountId.ToString() + " AND ACCOUNT.ACCOUNT_ID = FUNDS_DEPOSIT.BANK_ACC_ID AND BANK_ACC_SUB.ACCOUNT_ID =  FUNDS_DEPOSIT.SUB_ACC_ID";
                                 sSQL = " SELECT ACCOUNT.ACCOUNT_NAME, BANK_ACC_SUB.SUB_ACC_NAME, DEPOSIT_ID, CTL_NUMBER, TRANS_DATE, AMOUNT, CLEARED_FLAG , VOID_FLAG FROM FUNDS_DEPOSIT, ACCOUNT, BANK_ACC_SUB WHERE FUNDS_DEPOSIT.BANK_ACC_ID= " + iAccountId.ToString() + " AND ACCOUNT.ACCOUNT_ID = BANK_ACC_SUB.ACCOUNT_ID AND BANK_ACC_SUB.SUB_ROW_ID =  FUNDS_DEPOSIT.SUB_ACC_ID";
                            else
                                //MGaba2:MITS 9180-04/14/2008-Formatting of Flag fields require "_FLAG" at the end of column header to convert 0/-1 to Yes/No
                                //sSQL="SELECT DEPOSIT_ID, CTL_NUMBER, TRANS_DATE, AMOUNT, CLEARED_FLAG | CLEARED, VOID_FLAG | VOID FROM FUNDS_DEPOSIT ";
                                sSQL = "SELECT ACCOUNT.ACCOUNT_NAME, BANK_ACC_SUB.SUB_ACC_NAME,DEPOSIT_ID, CTL_NUMBER, TRANS_DATE, AMOUNT, CLEARED_FLAG , VOID_FLAG  FROM FUNDS_DEPOSIT,ACCOUNT,BANK_ACC_SUB WHERE ACCOUNT.ACCOUNT_ID = FUNDS_DEPOSIT.BANK_ACC_ID AND BANK_ACC_SUB.SUB_ROW_ID =  FUNDS_DEPOSIT.SUB_ACC_ID";//Deb: MITS 31479
                        }
                        else
                        {
                            if (iAccountId > 0)
                                //MGaba2:MITS 9180-04/14/2008-Formatting of Flag fields require "_FLAG" at the end of column header to convert 0/-1 to Yes/No
                                //sSQL="SELECT DEPOSIT_ID, CTL_NUMBER, TRANS_DATE, AMOUNT, CLEARED_FLAG | CLEARED, VOID_FLAG | VOID FROM FUNDS_DEPOSIT WHERE BANK_ACC_ID=" + iAccountId.ToString() ;
                                sSQL = "SELECT ACCOUNT.ACCOUNT_NAME, DEPOSIT_ID, CTL_NUMBER, TRANS_DATE, AMOUNT, CLEARED_FLAG , VOID_FLAG  FROM FUNDS_DEPOSIT,ACCOUNT WHERE BANK_ACC_ID=" + iAccountId.ToString() + " AND ACCOUNT.ACCOUNT_ID = FUNDS_DEPOSIT.BANK_ACC_ID ";
                            else
                                //MGaba2:MITS 9180-04/14/2008-Formatting of Flag fields require "_FLAG" at the end of column header to convert 0/-1 to Yes/No
                                //sSQL="SELECT DEPOSIT_ID, CTL_NUMBER, TRANS_DATE, AMOUNT, CLEARED_FLAG | CLEARED, VOID_FLAG | VOID FROM FUNDS_DEPOSIT ";
                                sSQL = "SELECT ACCOUNT.ACCOUNT_NAME,DEPOSIT_ID, CTL_NUMBER, TRANS_DATE, AMOUNT, CLEARED_FLAG , VOID_FLAG  FROM FUNDS_DEPOSIT,ACCOUNT WHERE ACCOUNT.ACCOUNT_ID = FUNDS_DEPOSIT.BANK_ACC_ID ";
                        }
                            
                        
                        //if (iAccountId > 0) 
                        //    //MGaba2:MITS 9180-04/14/2008-Formatting of Flag fields require "_FLAG" at the end of column header to convert 0/-1 to Yes/No
                        //    //sSQL="SELECT DEPOSIT_ID, CTL_NUMBER, TRANS_DATE, AMOUNT, CLEARED_FLAG | CLEARED, VOID_FLAG | VOID FROM FUNDS_DEPOSIT WHERE BANK_ACC_ID=" + iAccountId.ToString() ;
                        //    sSQL = "SELECT DEPOSIT_ID, CTL_NUMBER, TRANS_DATE, AMOUNT, CLEARED_FLAG , VOID_FLAG  FROM FUNDS_DEPOSIT WHERE BANK_ACC_ID=" + iAccountId.ToString();
                        //else
                        //    //MGaba2:MITS 9180-04/14/2008-Formatting of Flag fields require "_FLAG" at the end of column header to convert 0/-1 to Yes/No
                        //    //sSQL="SELECT DEPOSIT_ID, CTL_NUMBER, TRANS_DATE, AMOUNT, CLEARED_FLAG | CLEARED, VOID_FLAG | VOID FROM FUNDS_DEPOSIT ";
                        //    sSQL = "SELECT DEPOSIT_ID, CTL_NUMBER, TRANS_DATE, AMOUNT, CLEARED_FLAG , VOID_FLAG  FROM FUNDS_DEPOSIT ";

                        //Changed by gagan for MITS 22749 : End
    
                        if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY DEPOSIT_ID";
							}
						}
						else
							sSQL = sSQL + " ORDER BY DEPOSIT_ID";
						p_sPrimaryField="DEPOSIT_ID";
						p_sPrimaryFormField="depositid";
						break;
					case "piprivilege":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT PRIV_ID, PHYS_EID, CATEGORY.CODE_DESC | Category, TYPE.CODE_DESC | Type, STATUS.CODE_DESC | Status, INT_DATE | Start_Date, END_DATE | End_Date FROM PHYS_PRIVS, CODES_TEXT | CATEGORY, CODES_TEXT | TYPE, CODES_TEXT | STATUS WHERE PHYS_EID="
                            + p_objXmlDoc.SelectSingleNode("//physeid").Attributes.GetNamedItem("value").InnerText
                            + " AND CATEGORY.CODE_ID=PHYS_PRIVS.CATEGORY_CODE AND TYPE.CODE_ID=PHYS_PRIVS.TYPE_CODE AND STATUS.CODE_ID=PHYS_PRIVS.STATUS_CODE ";
                        }
                        else
                        {
                            sSQL = "SELECT PRIV_ID, PHYS_EID, CATEGORY.CODE_DESC | Category, TYPE.CODE_DESC | Type, STATUS.CODE_DESC | Status, INT_DATE | Start_Date, END_DATE | End_Date FROM PHYS_PRIVS, CODES_TEXT | CATEGORY, CODES_TEXT | TYPE, CODES_TEXT | STATUS WHERE PHYS_EID="
                            + m_sParentID
                            + " AND CATEGORY.CODE_ID=PHYS_PRIVS.CATEGORY_CODE AND TYPE.CODE_ID=PHYS_PRIVS.TYPE_CODE AND STATUS.CODE_ID=PHYS_PRIVS.STATUS_CODE ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY PRIV_ID";
							}
						}	
						else
							sSQL = sSQL + " ORDER BY PRIV_ID";

                        //abhateja 05.21.2009
                        //Privilege lookup wasn't working as system was trying to fetch pi eid instead of privilege id
                        //p_sPrimaryField = "PHYS_EID";
                        //p_sPrimaryFormField = "pieid";
                        //p_sSecondaryField = "PRIV_ID";
                        //p_sSecondaryFormField = "privid";

                        p_sPrimaryField = "PRIV_ID";
                        p_sPrimaryFormField = "privid";
                        p_sSecondaryField = "PHYS_EID";
                        p_sSecondaryFormField = "pieid";
                        break;
					case "picertification":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT CERT_ID, PHYS_EID, NAME.CODE_DESC | Name, STATUS.CODE_DESC | Status, BOARD.CODE_DESC | Board, INT_DATE | Start_Date, END_DATE | End_Date FROM PHYS_CERTS, CODES_TEXT | NAME, CODES_TEXT | STATUS, CODES_TEXT | BOARD WHERE PHYS_EID="
                            + p_objXmlDoc.SelectSingleNode("//physeid").Attributes.GetNamedItem("value").InnerText
                            + " AND NAME.CODE_ID=PHYS_CERTS.NAME_CODE AND STATUS.CODE_ID=PHYS_CERTS.STATUS_CODE AND BOARD.CODE_ID=PHYS_CERTS.BOARD_CODE ";
                        }
                        else
                        {
                            sSQL = "SELECT CERT_ID, PHYS_EID, NAME.CODE_DESC | Name, STATUS.CODE_DESC | Status, BOARD.CODE_DESC | Board, INT_DATE | Start_Date, END_DATE | End_Date FROM PHYS_CERTS, CODES_TEXT | NAME, CODES_TEXT | STATUS, CODES_TEXT | BOARD WHERE PHYS_EID="
                            + m_sParentID
                            + " AND NAME.CODE_ID=PHYS_CERTS.NAME_CODE AND STATUS.CODE_ID=PHYS_CERTS.STATUS_CODE AND BOARD.CODE_ID=PHYS_CERTS.BOARD_CODE ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY CERT_ID";
							}
						}	
						else
							sSQL = sSQL + " ORDER BY CERT_ID";

                        //abhateja 05.21.2009
                        //Certification lookup wasn't working as system was trying to fetch pi eid instead of certification id
                        //p_sPrimaryField = "PHYS_EID";
                        //p_sPrimaryFormField = "physeid";
                        //p_sSecondaryField = "CERT_ID";
                        //p_sSecondaryFormField = "certid";

                        p_sPrimaryField = "CERT_ID";
                        p_sPrimaryFormField = "certid";
                        p_sSecondaryField = "PHYS_EID";
                        p_sSecondaryFormField = "physeid";
                        
                        break;
					case "pieducation":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT EDUC_ID, PHYS_EID, EDUC.CODE_DESC | Type, INSTITUTION.LAST_NAME | Institution, DEGREE.CODE_DESC | Degree_Type, DEGREE_DATE FROM PHYS_EDUCATION, CODES_TEXT | EDUC, CODES_TEXT | DEGREE, ENTITY | INSTITUTION WHERE PHYS_EID="
                            + p_objXmlDoc.SelectSingleNode("//physeid").Attributes.GetNamedItem("value").InnerText
                            + " AND EDUC.CODE_ID=PHYS_EDUCATION.EDUC_TYPE_CODE AND DEGREE.CODE_ID=PHYS_EDUCATION.DEGREE_TYPE AND PHYS_EDUCATION.INSTITUTION_EID=INSTITUTION.ENTITY_ID ";
                        }
                        else
                        {
                            sSQL = "SELECT EDUC_ID, PHYS_EID, EDUC.CODE_DESC | Type, INSTITUTION.LAST_NAME | Institution, DEGREE.CODE_DESC | Degree_Type, DEGREE_DATE FROM PHYS_EDUCATION, CODES_TEXT | EDUC, CODES_TEXT | DEGREE, ENTITY | INSTITUTION WHERE PHYS_EID="
                            + m_sParentID
                            + " AND EDUC.CODE_ID=PHYS_EDUCATION.EDUC_TYPE_CODE AND DEGREE.CODE_ID=PHYS_EDUCATION.DEGREE_TYPE AND PHYS_EDUCATION.INSTITUTION_EID=INSTITUTION.ENTITY_ID ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY EDUC_ID";
							}
						}
						else
							sSQL = sSQL + " ORDER BY EDUC_ID";

                        //abhateja 05.21.2009
                        //Education lookup wasn't working as system was trying to fetch pi eid instead of education id
                        //p_sPrimaryField = "PHYS_EID";
                        //p_sPrimaryFormField = "physeid";
                        //p_sSecondaryField = "EDUC_ID";
                        //p_sSecondaryFormField = "educid";
                        
                        p_sPrimaryField = "EDUC_ID";
                        p_sPrimaryFormField = "educid";
                        p_sSecondaryField = "PHYS_EID";
                        p_sSecondaryFormField = "physeid";

                        break;
					case "piprevhospital":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT PREV_HOSP_ID, PHYS_EID, STATUS.CODE_DESC | Status, HOSPITAL.LAST_NAME | Hospital, PRIVILEGE.CODE_DESC | Privilege, INT_DATE | Start_Date, END_DATE | End_Date FROM PHYS_PREV_HOSP, CODES_TEXT | STATUS, CODES_TEXT | PRIVILEGE, ENTITY | HOSPITAL WHERE PHYS_EID="
                            + p_objXmlDoc.SelectSingleNode("//physeid").Attributes.GetNamedItem("value").InnerText
                            + " AND STATUS.CODE_ID=PHYS_PREV_HOSP.STATUS_CODE AND PRIVILEGE.CODE_ID=PHYS_PREV_HOSP.PRIV_CODE AND PHYS_PREV_HOSP.HOSPITAL_EID=HOSPITAL.ENTITY_ID ";
                        }
                        else
                        {
                            sSQL = "SELECT PREV_HOSP_ID, PHYS_EID, STATUS.CODE_DESC | Status, HOSPITAL.LAST_NAME | Hospital, PRIVILEGE.CODE_DESC | Privilege, INT_DATE | Start_Date, END_DATE | End_Date FROM PHYS_PREV_HOSP, CODES_TEXT | STATUS, CODES_TEXT | PRIVILEGE, ENTITY | HOSPITAL WHERE PHYS_EID="
                            + m_sParentID
                            + " AND STATUS.CODE_ID=PHYS_PREV_HOSP.STATUS_CODE AND PRIVILEGE.CODE_ID=PHYS_PREV_HOSP.PRIV_CODE AND PHYS_PREV_HOSP.HOSPITAL_EID=HOSPITAL.ENTITY_ID ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY PREV_HOSP_ID";
							}
						}	
						else
							sSQL = sSQL + " ORDER BY PREV_HOSP_ID";

                        //abhateja 05.21.2009
                        //Prev Hospital lookup wasn't working as system was trying to fetch pi eid instead of prev hospital id
                        //p_sPrimaryField = "PHYS_EID";
                        //p_sPrimaryFormField = "physeid";
                        //p_sSecondaryField = "PREV_HOSP_ID";
                        //p_sSecondaryFormField = "prevhospid";

                        p_sPrimaryField = "PREV_HOSP_ID";
                        p_sPrimaryFormField = "prevhospid";
                        p_sSecondaryField = "PHYS_EID";
                        p_sSecondaryFormField = "physeid";
                        break;
					case "pidependent":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT PI_ROW_ID, PI_DEP_ROW_ID, DEPENDENT.LAST_NAME, DEPENDENT.FIRST_NAME, HEALTH_PLAN_FLAG, RELATION.CODE_DESC | Relation FROM PI_X_DEPENDENT, CODES_TEXT | RELATION, ENTITY | DEPENDENT WHERE PI_ROW_ID="
                            + p_objXmlDoc.SelectSingleNode("//pirowid").Attributes.GetNamedItem("value").InnerText
                            + " AND DEPENDENT_EID=DEPENDENT.ENTITY_ID AND RELATION.CODE_ID=RELATION_CODE ";
                        }
                        else
                        {
                            sSQL = "SELECT PI_ROW_ID, PI_DEP_ROW_ID, DEPENDENT.LAST_NAME, DEPENDENT.FIRST_NAME, HEALTH_PLAN_FLAG, RELATION.CODE_DESC | Relation FROM PI_X_DEPENDENT, CODES_TEXT | RELATION, ENTITY | DEPENDENT WHERE PI_ROW_ID="
                            + m_sParentID
                            + " AND DEPENDENT_EID=DEPENDENT.ENTITY_ID AND RELATION.CODE_ID=RELATION_CODE ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY PI_DEP_ROW_ID";
							}
						}
						else
							sSQL = sSQL + " ORDER BY PI_DEP_ROW_ID";
                        //Raman: 02/22/2008: MITS 11659
                        //PI dependent Lookup wasn't working as system was trying to fetch employee id instead of dependent id
                        p_sPrimaryField = "PI_DEP_ROW_ID";
                        p_sPrimaryFormField = "pideprowid";
                        p_sSecondaryField = "PI_ROW_ID";
                        p_sSecondaryFormField = "pirowid";
                        //p_sPrimaryField="PI_ROW_ID";
                        //p_sPrimaryFormField="pirowid";
                        //p_sSecondaryField="PI_DEP_ROW_ID";
                        //p_sSecondaryFormField="pideprowid";
						break;
					case "pirestriction":
                        //Raman: 02/22/2008: MITS 11659
                        //PI restriction Lookup wasn't working as system was trying to fetch employee id instead of restriction id
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT PI_ROW_ID, PI_RESTRICT_ROW_ID, DATE_FIRST_RESTRCT, DATE_LAST_RESTRCT, DURATION, PERCENT_DISABLED | PercentDisabled, POSITION.CODE_DESC | Position FROM PI_X_RESTRICT, CODES_TEXT | POSITION WHERE PI_ROW_ID="
                            + p_objXmlDoc.SelectSingleNode("//pirowid").Attributes.GetNamedItem("value").InnerText
                            + " AND POSITION.CODE_ID=POSITION_CODE ";
                        }
                        else
                        {
                            sSQL = "SELECT PI_ROW_ID, PI_RESTRICT_ROW_ID, DATE_FIRST_RESTRCT, DATE_LAST_RESTRCT, DURATION, PERCENT_DISABLED | PercentDisabled, POSITION.CODE_DESC | Position FROM PI_X_RESTRICT, CODES_TEXT | POSITION WHERE PI_ROW_ID="
                            + m_sParentID
                            + " AND POSITION.CODE_ID=POSITION_CODE ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY PI_RESTRICT_ROW_ID";
							}
						}	
						else
							sSQL = sSQL + " ORDER BY PI_RESTRICT_ROW_ID";
                        //Raman: 02/22/2008: MITS 11659
                        //PI restriction Lookup wasn't working as system was trying to fetch pi row id instead of restriction id
                        //p_sPrimaryField="PI_ROW_ID";
                        //p_sPrimaryFormField="pirowid";
                        //p_sSecondaryField="PI_RESTRICT_ROW_ID";
                        //p_sSecondaryFormField="pirestrictrowid";
                        p_sPrimaryField = "PI_RESTRICT_ROW_ID";
                        p_sPrimaryFormField = "pirestrictrowid";
                        p_sSecondaryField = "PI_ROW_ID";
                        p_sSecondaryFormField = "pirowid";
						break;
					case "piworkloss":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT PI_ROW_ID, PI_WL_ROW_ID, DATE_LAST_WORKED, DATE_RETURNED, DURATION, STATE_DURATION FROM PI_X_WORK_LOSS WHERE PI_ROW_ID="
                            + p_objXmlDoc.SelectSingleNode("//pirowid").Attributes.GetNamedItem("value").InnerText;
                        }
                        else
                        {
                            sSQL = "SELECT PI_ROW_ID, PI_WL_ROW_ID, DATE_LAST_WORKED, DATE_RETURNED, DURATION, STATE_DURATION FROM PI_X_WORK_LOSS WHERE PI_ROW_ID="
                            + m_sParentID;
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY PI_WL_ROW_ID";
							}
						}	
						else
						{
							sSQL = sSQL + " ORDER BY PI_WL_ROW_ID";
						}
                        //Raman: 02/22/2008: MITS 11659
                        //PI Work Loss Lookup wasn't working as system was trying to fetch pi row id instead of work loss id
                        //p_sPrimaryField="PI_ROW_ID";
                        //p_sPrimaryFormField="pirowid";
                        //p_sSecondaryField="PI_WL_ROW_ID";
                        //p_sSecondaryFormField="piwlrowid";
                        p_sPrimaryField = "PI_WL_ROW_ID";
                        p_sPrimaryFormField = "piwlrowid";
                        p_sSecondaryField = "PI_ROW_ID";
                        p_sSecondaryFormField = "pirowid";
						break;
					case "piqprocedure":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT PATIENT_ID, PROC_ROW_ID, DATE_OF_PROCEDURE, PROCEDURE_TYPE.CODE_DESC | Procedure, COMPLICATION_DATE FROM PATIENT_PROCEDURE, CODES_TEXT | PROCEDURE_TYPE WHERE PATIENT_ID="
                            + p_objXmlDoc.SelectSingleNode("//patientid").Attributes.GetNamedItem("value").InnerText
                            + " AND PROCEDURE_TYPE.CODE_ID=PROCEDURE_CODE ";
                        }
                        else
                        {
                            sSQL = "SELECT PATIENT_ID, PROC_ROW_ID, DATE_OF_PROCEDURE, PROCEDURE_TYPE.CODE_DESC | Procedure, COMPLICATION_DATE FROM PATIENT_PROCEDURE, CODES_TEXT | PROCEDURE_TYPE WHERE PATIENT_ID="
                            + m_sParentID
                            + " AND PROCEDURE_TYPE.CODE_ID=PROCEDURE_CODE ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY PROC_ROW_ID";
							}
						}	
						else
						{
							sSQL = sSQL + " ORDER BY PROC_ROW_ID";
						}
                        //Raman: 02/22/2008: MITS 11659
                        //PI proc row Id Lookup wasn't working as system was trying to fetch patient id instead of proc row Id
                        //p_sPrimaryField="PATIENT_ID";
                        //p_sPrimaryFormField="patientid";
                        //p_sSecondaryField="PROC_ROW_ID";
                        //p_sSecondaryFormField="procrowid";
                        p_sPrimaryField = "PROC_ROW_ID";
                        p_sPrimaryFormField = "procrowid";
                        p_sSecondaryField = "PATIENT_ID";
                        p_sSecondaryFormField = "patientid";
						break;
					case "piprocedure":
                        #region Earlier code   
                    //Modified bycsingh7 for MITS 14108
                        sSQL = "SELECT PI_ROW_ID, PI_PROC_ROW_ID, DATE_OF_PROCEDURE, CODES_TEXT.CODE_DESC, COMPLICATION_DATE FROM PI_X_PROCEDURE, CODES_TEXT WHERE PI_ROW_ID="
                            + p_objXmlDoc.SelectSingleNode("//pirowid").Attributes.GetNamedItem("value").InnerText
                            + " AND CODES_TEXT.CODE_ID=PROCEDURE_CODE ";
                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY PI_PROC_ROW_ID";
                            }
                        }
                        else
                        {
                            sSQL = sSQL + " ORDER BY PI_PROC_ROW_ID";
                        }
                        //Raman: 02/22/2008: MITS 11659
                        //PI proc row Id Lookup wasn't working as system was trying to fetch patient id instead of proc row Id
                        //p_sPrimaryField="PI_ROW_ID";
                        //p_sPrimaryFormField="pirowid";
                        //p_sSecondaryField="PI_PROC_ROW_ID";
                        //p_sSecondaryFormField="procrowid";
                        p_sPrimaryField = "PI_PROC_ROW_ID";
                        p_sPrimaryFormField = "procrowid";
                        p_sSecondaryField = "PI_ROW_ID";
                        p_sSecondaryFormField = "pirowid";
#endregion
                        //Commented by csingh7 for MITS 15116
                        //Parijat : Mits 9962 since PATIENT_PROCEDURE is being used to save the procedures instead of PI_X_PROCEDURE
                        //if (m_sParentID == String.Empty)
                        //{
                        //    sSQL = "SELECT PATIENT_ID, PROC_ROW_ID, DATE_OF_PROCEDURE, CODES_TEXT.CODE_DESC , COMPLICATION_DATE FROM PATIENT_PROCEDURE, CODES_TEXT  WHERE PATIENT_ID="
                        //    + p_objXmlDoc.SelectSingleNode("//patientid").Attributes.GetNamedItem("value").InnerText
                        //    + " AND CODES_TEXT.CODE_ID=PROCEDURE_CODE ";
                        //}
                        //else
                        //{
                        //    sSQL = "SELECT PATIENT_ID, PROC_ROW_ID, DATE_OF_PROCEDURE, CODES_TEXT.CODE_DESC , COMPLICATION_DATE FROM PATIENT_PROCEDURE, CODES_TEXT  WHERE PATIENT_ID="
                        //    + m_sParentID
                        //    + " AND CODES_TEXT.CODE_ID=PROCEDURE_CODE ";
                        //}
                        //if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        //{
                        //    if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                        //    {
                        //        sSQL = sSQL + " ORDER BY PROC_ROW_ID";
                        //    }
                        //}
                        //else
                        //{
                        //    sSQL = sSQL + " ORDER BY PROC_ROW_ID";
                        //}
                       
                        //p_sPrimaryField = "PROC_ROW_ID";
                        //p_sPrimaryFormField = "procrowid";
                        //p_sSecondaryField = "PATIENT_ID";
                        //p_sSecondaryFormField = "patientid";
						break;
					case "pimedstaffprivilege":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT STAFF_PRIV_ID, STAFF_EID, CATEGORY.CODE_DESC | Category, TYPE.CODE_DESC | Type, STATUS.CODE_DESC | Status, INT_DATE | Start_Date, END_DATE | End_Date FROM STAFF_PRIVS, CODES_TEXT | CATEGORY, CODES_TEXT | TYPE, CODES_TEXT | STATUS WHERE STAFF_EID="
                            //+ p_objXmlDoc.SelectSingleNode("//pieid").Attributes.GetNamedItem("value").InnerText
                            + p_objXmlDoc.SelectSingleNode("//staffeid").Attributes.GetNamedItem("value").InnerText //csingh7 MITS 14083
                            + " AND CATEGORY.CODE_ID=STAFF_PRIVS.CATEGORY_CODE AND TYPE.CODE_ID=STAFF_PRIVS.TYPE_CODE AND STATUS.CODE_ID=STAFF_PRIVS.STATUS_CODE ";
                        }
                        else
                        {
                            sSQL = "SELECT STAFF_PRIV_ID, STAFF_EID, CATEGORY.CODE_DESC | Category, TYPE.CODE_DESC | Type, STATUS.CODE_DESC | Status, INT_DATE | Start_Date, END_DATE | End_Date FROM STAFF_PRIVS, CODES_TEXT | CATEGORY, CODES_TEXT | TYPE, CODES_TEXT | STATUS WHERE STAFF_EID="
                            + m_sParentID
                            + " AND CATEGORY.CODE_ID=STAFF_PRIVS.CATEGORY_CODE AND TYPE.CODE_ID=STAFF_PRIVS.TYPE_CODE AND STATUS.CODE_ID=STAFF_PRIVS.STATUS_CODE ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY STAFF_PRIV_ID";
							}
						}
						else
						{
							sSQL = sSQL + " ORDER BY STAFF_PRIV_ID";
						}
                        //Raman: 02/22/2008: MITS 11659
                        //PI Staff Priv Id Lookup wasn't working as system was trying to fetch staff id instead of Staff Priv Id
                        //p_sPrimaryField="STAFF_EID";
                        //p_sPrimaryFormField="pieid";
                        //p_sSecondaryField="STAFF_PRIV_ID";
                        //p_sSecondaryFormField="staffprivid";
                        p_sPrimaryField = "STAFF_PRIV_ID";
                        p_sPrimaryFormField = "staffprivid";
                        p_sSecondaryField = "STAFF_EID";
                        p_sSecondaryFormField = "pieid";
						break;
					case "pimedstaffcertification":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT STAFF_CERT_ID, STAFF_EID, NAME.CODE_DESC | Name, INT_DATE | Start_Date, END_DATE | End_Date FROM STAFF_CERTS, CODES_TEXT | NAME WHERE STAFF_EID="
                            //+ p_objXmlDoc.SelectSingleNode("//pieid").Attributes.GetNamedItem("value").InnerText
                            + p_objXmlDoc.SelectSingleNode("//staffeid").Attributes.GetNamedItem("value").InnerText //csingh7 MITS 14083
                            + " AND NAME.CODE_ID=STAFF_CERTS.NAME_CODE ";
                        }
                        else
                        {
                            sSQL = "SELECT STAFF_CERT_ID, STAFF_EID, NAME.CODE_DESC | Name, INT_DATE | Start_Date, END_DATE | End_Date FROM STAFF_CERTS, CODES_TEXT | NAME WHERE STAFF_EID="
                            + m_sParentID
                            + " AND NAME.CODE_ID=STAFF_CERTS.NAME_CODE ";
                        }
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY STAFF_CERT_ID";
							}
						}	
						else
						{
							sSQL = sSQL + " ORDER BY STAFF_CERT_ID";
						}
                        //Raman: 02/22/2008: MITS 11659
                        //PI Staff cert Id Lookup wasn't working as system was trying to fetch staff id instead of Staff cert Id
                        //p_sPrimaryField="STAFF_EID";
                        //p_sPrimaryFormField="pieid";
                        //p_sSecondaryField="STAFF_CERT_ID";
                        //p_sSecondaryFormField="staffcertid";
                        p_sPrimaryField = "STAFF_CERT_ID";
                        p_sPrimaryFormField = "staffcertid";
                        p_sSecondaryField = "STAFF_EID";
                        p_sSecondaryFormField = "pieid";
						break;
					case "autoclaimchecks":
						sSQL = "SELECT FUNDS_AUTO.AUTO_BATCH_ID, FUNDS_AUTO.AUTO_BATCH_ID | BATCH_NUMBER, CLAIM_NUMBER";
                        sSQL = sSQL + ", CTL_NUMBER | CONTROL_NUMBER, AMOUNT, PRINT_DATE, CODES_TEXT.CODE_DESC | PAYEE_TYPE, FIRST_NAME , LAST_NAME, ";
                        if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_ORACLE)
                            sSQL = sSQL + "NVL(ADDR1, '') || ' ' || NVL(ADDR2, '')|| ' ' || NVL(ADDR3, '')|| ' ' || NVL(ADDR4, '') AS Address";
                        else
                            sSQL = sSQL + "COALESCE(ADDR1, '') + ' ' + COALESCE(ADDR2, '') + ' ' + COALESCE(ADDR3, '') + ' ' + COALESCE(ADDR4, '') Address";

                        sSQL = sSQL + ", CITY, ZIP_CODE, STATES.STATE_NAME | STATE, FUNDS_AUTO_BATCH.DISABILITY_FLAG";
						sSQL = sSQL + " FROM FUNDS_AUTO, FUNDS_AUTO_BATCH, CODES_TEXT, STATES";
						sSQL = sSQL + " WHERE FUNDS_AUTO.AUTO_BATCH_ID = FUNDS_AUTO_BATCH.AUTO_BATCH_ID AND PAYEE_TYPE_CODE = CODES_TEXT.CODE_ID";
						sSQL = sSQL + " AND FUNDS_AUTO.STATE_ID = STATES.STATE_ROW_ID" ;
						sSQL = sSQL + " AND FUNDS_AUTO_BATCH.DISABILITY_FLAG <> -1 AND AUTO_TRANS_ID IN (SELECT MIN(AUTO_TRANS_ID) FROM FUNDS_AUTO GROUP BY AUTO_BATCH_ID) ";
						if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY FUNDS_AUTO.AUTO_BATCH_ID";
							}
						}
						else
						{
							sSQL = sSQL + " ORDER BY FUNDS_AUTO.AUTO_BATCH_ID";
						}
						p_sPrimaryField="AUTO_BATCH_ID";
						p_sPrimaryFormField="autobatchid";
						break;
					case "funds":
                        //pmittal5 Mits 18849 12/03/09 - Show all Funds transactions if no Claim Number is selected. 
                        XmlAttribute objClaimid = null;
                        objClaimid =  (XmlAttribute)p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"];
						sSQL = "SELECT TRANS_ID, CTL_NUMBER, CLAIM_NUMBER, TRANS_DATE, FIRST_NAME, LAST_NAME, AMOUNT FROM FUNDS ";
                        //if (p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"] != null)
                        //{
                        //    sSQL = sSQL + "WHERE CLAIM_ID = " + Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"].Value);
                        //}
                        if (objClaimid != null && Conversion.ConvertStrToInteger(objClaimid.Value) != 0)
                        {
                            sSQL = sSQL + "WHERE CLAIM_ID = " + Conversion.ConvertStrToInteger(objClaimid.Value);
                        }
                        //End - pmittal5
                        if(p_objXmlDoc.SelectSingleNode("//OrderBy")!=null)
						{
							if(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml=="")
							{
								sSQL = sSQL + " ORDER BY TRANS_ID";
							}
						}
						else
						{
							sSQL = sSQL + " ORDER BY TRANS_ID";
						}
						p_sPrimaryField="TRANS_ID";
						p_sPrimaryFormField="transid";
						break;
                    //Geeta FMLA Enhancement                    
                    case "leave":
                        //Geeta 03/28/07 : SQL Modified to fix Mits number 8862 and 8863
                        //abansal23 05/01/2009 : Changed for look up for RMX5
                        sSQL = "SELECT LEAVE_ROW_ID,LEAVE_PLAN_NAME,LEAVE_PLAN_CODE,DATE_ELIGIBILITY,HOURS_WORKED,CODES1.CODE_DESC | ELIG_STATUS_CODE FROM CLAIM_LEAVE,LEAVE_PLAN,CODES_TEXT | CODES1 WHERE CLAIM_LEAVE.CLAIM_ID = " + p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"].Value + " AND ELIG_STATUS_CODE = CODES1.CODE_ID" + " AND CLAIM_LEAVE.LP_ROW_ID = LEAVE_PLAN.LP_ROW_ID";
                        p_sPrimaryField = "LEAVE_ROW_ID";
                        p_sPrimaryFormField = "leaverowid";
                        break;
                    //Geeta FMLA Enhancement    
                    case "leaveplan":
                        //Geeta 03/28/07 : SQL Modified to fix Mits number 8862 and 8863
                        sSQL = "SELECT LP_ROW_ID, LEAVE_PLAN_CODE, LEAVE_PLAN_NAME, LEAVE_PLAN_DESC, CODES1.CODE_DESC | LP_STATUS_CODE, EFFECTIVE_DATE FROM LEAVE_PLAN, CODES_TEXT | CODES1 WHERE LP_STATUS_CODE = CODES1.CODE_ID";
                        p_sPrimaryField = "LP_ROW_ID";
                        p_sPrimaryFormField = "lprowid";
                        break;
                    //Raman LTD Enhancement
                    case "plan":
                        sSQL = "SELECT PLAN_ID , PLAN_NUMBER, PLAN_NAME, PLAN_DESCRIPTION, CODES_TEXT.CODE_DESC STATUS , EFFECTIVE_DATE FROM DISABILITY_PLAN , CODES_TEXT WHERE PLAN_STATUS_CODE = CODES_TEXT.CODE_ID";
                        p_sPrimaryField = "PLAN_ID";
                        p_sPrimaryFormField = "planid";
                        break;
                    //Shivendu added for Concomitant in MITS 9677
                    case "concomitant":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT EV_CONCOM_ROW_ID,CONCOM_PRODUCT_ID,from_date,TO_DATE,CONCOM_PRODUCT FROM EV_X_CONCOM_PROD WHERE EVENT_ID = '" + p_objXmlDoc.SelectSingleNode("//eventid").Attributes.GetNamedItem("value").InnerText + "'";                        
                        }
                        else
                        {
                            sSQL = "SELECT EV_CONCOM_ROW_ID,CONCOM_PRODUCT_ID,from_date,TO_DATE,CONCOM_PRODUCT FROM EV_X_CONCOM_PROD WHERE EVENT_ID = '" + m_sParentID + "'";    
                        }
                        p_sPrimaryField = "EV_CONCOM_ROW_ID";
                        p_sPrimaryFormField = "evconcomrowid";
                        break;
                    //Shivendu added for providercontract in MITS 9862
                    case "providercontract":
                        sSQL = "SELECT PRVD_CONT_ROW_ID,START_DATE,END_DATE,AMOUNT,DISCOUNT,COMMENTS";
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = sSQL + " FROM PROVIDER_CONTRACTS WHERE PROVIDER_EID = '" + p_objXmlDoc.SelectSingleNode("//entityid").Attributes.GetNamedItem("value").InnerText + "'";
                        }
                        else
                        {
                            sSQL = sSQL + " FROM PROVIDER_CONTRACTS WHERE PROVIDER_EID = '" + m_sParentID + "'";
                        }
                        p_sPrimaryField = "PRVD_CONT_ROW_ID";
                        p_sPrimaryFormField = "prvdcontrowid";
                        break;    
                        //Parijat: Mits 9937--for medwatchtest look up
                    case "medwatchtest":
                        sSQL = "SELECT EV_MW_TEST_ROW_ID, LAB_TEST, RESULT, TEST_DATE";
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = sSQL + " FROM EVENT_X_MEDW_TEST WHERE EVENT_ID = " + p_objXmlDoc.SelectSingleNode("//eventid").Attributes.GetNamedItem("value").InnerText;
                        }
                        else
                        {
                            sSQL = sSQL + " FROM EVENT_X_MEDW_TEST WHERE EVENT_ID = " + m_sParentID;
                        }
                        
                        p_sPrimaryField = "EV_MW_TEST_ROW_ID";
                        p_sPrimaryFormField = "evmwtestrowid";
                        break; 
                    //Parijat: Mits 9962--for Jurisdiction licence code look up
                    case "jurisdictionlicensecodes":
                        //bkumar33
                        //sSQL = "SELECT ENTITY_X_CODELICEN.TABLE_ROW_ID, STATES.STATE_NAME JURISDICTION , ENTITY_X_CODELICEN.CODELICENSE_NUMBER, ENTITY_X_CODELICEN.EFFECTIVE_DATE ,ENTITY_X_CODELICEN.EXPIRATION_DATE";
                        sSQL = "SELECT ENTITY_X_CODELICEN.TABLE_ROW_ID, STATES.STATE_NAME JURISDICTION , ENTITY_X_CODELICEN.CODELICENSE_NUMBER as Code_or_License_Number, ENTITY_X_CODELICEN.EFFECTIVE_DATE ,ENTITY_X_CODELICEN.EXPIRATION_DATE";
                        //end
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = sSQL + " FROM ENTITY_X_CODELICEN ,STATES WHERE ENTITY_ID = " + p_objXmlDoc.SelectSingleNode("//entityid").Attributes.GetNamedItem("value").InnerText;
                        }
                        else
                        {
                            sSQL = sSQL + " FROM ENTITY_X_CODELICEN ,STATES WHERE ENTITY_ID = " + m_sParentID;
                        }
                        sSQL = sSQL + " AND STATES.STATE_ROW_ID = ENTITY_X_CODELICEN.JURIS_ROW_ID";
                        p_sPrimaryField = "TABLE_ROW_ID";
                        p_sPrimaryFormField = "tablerowid";
                        break; 
                    //Parijat: Mits 9962--for Plan Class look up
                    case "planclasses":
                        sSQL = "SELECT CLASS_ROW_ID , CLASS_NAME , CLASS_DESC FROM DISABILITY_CLASS";
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = sSQL + " WHERE PLAN_ID = " + p_objXmlDoc.SelectSingleNode("//planid").Attributes.GetNamedItem("value").InnerText;
                        }
                        else
                        {
                            sSQL = sSQL + " WHERE PLAN_ID = " + m_sParentID;
                        }
                        p_sPrimaryField = "CLASS_ROW_ID";
                        p_sPrimaryFormField = "classrowid";
                        break;
                    //Shruti, 11658
                    case "entityexposure":
                        sSQL = "SELECT EXPOSURE_ROW_ID, START_DATE, END_DATE, NO_OF_EMPLOYEES, NO_OF_WORK_HOURS, PAYROLL_AMOUNT, ASSET_VALUE, TOTAL_REVENUE, SQUARE_FOOTAGE, VEHICLE_COUNT ";
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = sSQL + " FROM ENTITY_EXPOSURE WHERE ENTITY_ID = '" + p_objXmlDoc.SelectSingleNode("//entityid").Attributes.GetNamedItem("value").InnerText + "'";
                        }
                        else
                        {
                            sSQL = sSQL + " FROM ENTITY_EXPOSURE WHERE ENTITY_ID = '" + m_sParentID + "'";
                        }
                        p_sPrimaryField = "EXPOSURE_ROW_ID";
                        p_sPrimaryFormField = "exposurerowid";
                        break;
                    //Shruti, 11658 ends
                        //mona:marker
                        // npadhy MITS 15832 SQL and Oracle differs in the way they handle NULL, in case of Oracle if Null is concatenated 
                        // with a string then we get string as the Output, but in case of SQL Server Null concatenated with a string is Null
                        // So if we have a First Name as Null, when concatenated with Last Name in below query, we used to get the Null as
                        // Output, so added a check that if the First Name or last name is null, then we replace it with blank

                        // npadhy MITS 15832 A space is added while concatenating the Last Name and First Name as while removing the comma,
                        // we check if the last character is space then prior to this we remove the comma
                    case "cmxcmgrhist":
                        if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_ORACLE)
                            sSQL = "SELECT CMCMH_ROW_ID,CASE_MGR.LAST_NAME || ', ' || CASE_MGR.FIRST_NAME Case_Manager ,REF_TYPE.CODE_DESC Referral_Type,REF_DATE Referral_Date,REF_TO.LAST_NAME || ', ' || REF_TO.FIRST_NAME Referred_To,CASE_STATUS.CODE_DESC Case_Status,DATE_CLOSED,DUE_DATE UR_Due_Date,PRIMARY_CMGR_FLAG Primary_Case_Mgr";
                        else
                            sSQL = "SELECT CMCMH_ROW_ID,COALESCE(CASE_MGR.LAST_NAME,'') + ', ' + COALESCE(CASE_MGR.FIRST_NAME,'')  Case_Manager ,REF_TYPE.CODE_DESC Referral_Type,REF_DATE Referral_Date,COALESCE(REF_TO.LAST_NAME, '') + ', ' + COALESCE(REF_TO.FIRST_NAME,'') Referred_To,CASE_STATUS.CODE_DESC Case_Status,DATE_CLOSED,DUE_DATE UR_Due_Date,PRIMARY_CMGR_FLAG Primary_Case_Mgr";                      
                        //rsushilaggar MITS 32536 Date 05/09/2013
                        //sSQL = sSQL + " FROM CM_X_CMGR_HIST, ENTITY CASE_MGR, CODES_TEXT REF_TYPE, ENTITY REF_TO, CODES_TEXT CASE_STATUS";
                        //sSQL = sSQL + " WHERE CASEMGT_ROW_ID =";
                        //sSQL = sSQL + m_sParentID;
                        //sSQL = sSQL +" AND CASE_MGR_EID=CASE_MGR.ENTITY_ID AND REF_TYPE_CODE=REF_TYPE.CODE_ID AND REF_TO_EID=REF_TO.ENTITY_ID AND CASE_STATUS.CODE_ID=CASE_STATUS_CODE ";  
                        sSQL = sSQL + " FROM CM_X_CMGR_HIST ";
                        sSQL = sSQL + " INNER JOIN ENTITY CASE_MGR ON CASE_MGR_EID=CASE_MGR.ENTITY_ID ";
                        sSQL = sSQL + " INNER JOIN CODES_TEXT REF_TYPE ON REF_TYPE_CODE=REF_TYPE.CODE_ID ";
                        sSQL = sSQL + " LEFT OUTER JOIN  ENTITY REF_TO ON REF_TO_EID=REF_TO.ENTITY_ID ";
                        sSQL = sSQL + " INNER JOIN CODES_TEXT CASE_STATUS ON CASE_STATUS.CODE_ID=CASE_STATUS_CODE ";
                        sSQL = sSQL + " WHERE CASEMGT_ROW_ID = " + m_sParentID;
                        //end rsushilaggar
                        p_sPrimaryField = "CMCMH_ROW_ID";
                        p_sPrimaryFormField = "CmcmhRowId";
                        break;
                    case "cmxaccommodation":
                        // npadhy MITS 15531 Changed the MODE the Alias for 'EDIT' to Accomm_Mode as Mode is a keyword in Oracle
                        // So, The Query was failing in case of oracle.
                        sSQL = "SELECT CM_ACCOMM_ROW_ID,'EDIT' Accomm_Mode ,ACC_TYPE.CODE_DESC Accomm_Type ,ACC_STATUS.CODE_DESC Status,ACC_SCOPE.CODE_DESC Scope,ACCOMM_ACCEPTED Accomm_Accepted_Date,ACCOMM_RESTRICTION Restriction,ACCOMM_TEXT Accommodation,NET_ACCOMM_COST Cost"; 
                            sSQL = sSQL + " FROM CM_X_ACCOMMODATION,CODES_TEXT ACC_TYPE,CODES_TEXT ACC_STATUS,CODES_TEXT ACC_SCOPE";
                            sSQL = sSQL + " WHERE CM_ROW_ID = " + m_sParentID;
                            sSQL = sSQL + " AND ACCOMM_TYPE = ACC_TYPE.CODE_ID AND ACCOMM_STATUS = ACC_STATUS.CODE_ID AND ACCOMM_SCOPE = ACC_SCOPE.CODE_ID";
                        p_sPrimaryField = "CM_ACCOMM_ROW_ID";
                        p_sPrimaryFormField = "CmAccommRowId";
                        break;
                        // ijha MITS 24430 The date format for Accomm accepted was not working so changed Accomm_Accepted to Accomm_Accepted_Date.
                        // npadhy MITS 15534 The Lookup for Case Manage Notes was crashing as there was no Case to handle it.
                        // Added a case to fix the same.
                    case "casemgrnotes":
                        if (m_sParentID == String.Empty)
                        {
                            sSQL = "SELECT CMGR_NOTES_ROW_ID, ENTERED_BY_USER, DATE_ENTERED, TIME_ENTERED, NOTES_TYPE.CODE_DESC NOTE_TYPE, CMGR_NOTES CASE_MANAGER_NOTES'";
                            sSQL += " FROM CASE_MGR_NOTES,  CODES_TEXT NOTES_TYPE ";
                            sSQL += " WHERE CASEMGR_ROW_ID = " + p_objXmlDoc.SelectSingleNode("//casemgrrowid").Attributes.GetNamedItem("value").InnerText;
                            sSQL += " AND CASE_MGR_NOTES.NOTES_TYPE_CODE = NOTES_TYPE.CODE_ID";

                        }
                        else
                        {
                            sSQL = "SELECT CMGR_NOTES_ROW_ID, ENTERED_BY_USER, DATE_ENTERED, TIME_ENTERED, NOTES_TYPE.CODE_DESC NOTE_TYPE, CMGR_NOTES CASE_MANAGER_NOTES";
                            sSQL += " FROM CASE_MGR_NOTES,  CODES_TEXT NOTES_TYPE";
                            sSQL += " WHERE CASEMGR_ROW_ID = " + m_sParentID;
                            sSQL += " AND CASE_MGR_NOTES.NOTES_TYPE_CODE = NOTES_TYPE.CODE_ID";

                        }
                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY DATE_ENTERED";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY DATE_ENTERED";
                        p_sPrimaryField = "CMGR_NOTES_ROW_ID";
                        p_sPrimaryFormField = "cmgrnotesrowid";
                        break;
                        //Neha creating lookup for case management
                    case "casemanagementlist":
                        if (m_sParentID == String.Empty)
                        {
                            //JIRA RMA-15935 ajohari2 START
                            sSQL = "SELECT CM.CASEMGT_ROW_ID, CM.ADDED_BY_USER, CM.DTTM_RCD_ADDED AS DATE_RECORD_ADDED, CM.DTTM_RCD_LAST_UPD AS DATE_RECORD_LAST_UPDATED, CLAIM.CLAIM_NUMBER, CM.PI_ROW_ID";
                            sSQL += " FROM CASE_MANAGEMENT CM";
                            sSQL += " LEFT JOIN CLAIM ON CLAIM.CLAIM_ID = CM.CLAIM_ID ";
                            sSQL += " WHERE CM.PI_ROW_ID = " + p_objXmlDoc.SelectSingleNode("//pirowid").Attributes.GetNamedItem("value").InnerText;
                            

                        }
                        else
                        {
                            //sSQL = "SELECT CASEMGT_ROW_ID, ADDED_BY_USER, DTTM_RCD_ADDED, DTTM_RCD_LAST_UPD, CLAIM_ID, CLAIM_ID AS 'a',PI_ROW_ID ";
                            sSQL = "SELECT CM.CASEMGT_ROW_ID, CM.ADDED_BY_USER, CM.DTTM_RCD_ADDED AS DATE_RECORD_ADDED, CM.DTTM_RCD_LAST_UPD AS DATE_RECORD_LAST_UPDATED, CLAIM.CLAIM_NUMBER, CM.PI_ROW_ID";
                            sSQL += " FROM CASE_MANAGEMENT CM";
                            sSQL += " LEFT JOIN CLAIM ON CLAIM.CLAIM_ID = CM.CLAIM_ID ";
                            sSQL += " WHERE CM.PI_ROW_ID = " + m_sParentID;
                            //JIRA RMA-15935 ajohari2 END

                        }
                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY CASEMGT_ROW_ID";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY CASEMGT_ROW_ID";
                        p_sPrimaryField = "CASEMGT_ROW_ID";
                        p_sPrimaryFormField = "CasemgtRowId";
                       	p_sSecondaryField="PI_ROW_ID";
                        p_sSecondaryFormField="hidden";
					break;
                    //10/09/2009 Raman Bhatia: Creating Lookup for Reserve History in Reserve Worksheet
                    case "reserveworksheet":
                        int iApprovedCodeId = 0, iHistoryCodeId = 0;

                        using (LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId))
                        {
                            iApprovedCodeId = objCache.GetCodeId("AP", "RSW_STATUS");
                            iHistoryCodeId = objCache.GetCodeId("HI", "RSW_STATUS");
                        }//using
                        
                        if (m_objDataModelFactory.Context.DbConn.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                        {
                            sSQL = "SELECT RSW_WORKSHEETS.RSW_ROW_ID , DTTM_APPROVED Date_Time_Approved , CODES.SHORT_CODE || ' ' || CODES_TEXT.CODE_DESC Reserve_Worksheet_Status ,  APPROVED_BY Approved_By ";
                        }
                        else
                        {
                            sSQL = "SELECT RSW_WORKSHEETS.RSW_ROW_ID , DTTM_APPROVED Date_Time_Approved , CODES.SHORT_CODE + ' ' + CODES_TEXT.CODE_DESC Reserve_Worksheet_Status , APPROVED_BY Approved_By ";
                        }
                        
                        if (m_sParentID == String.Empty)
                        {
                            //Not Needed as of now
                        }
                        else
                        {
                            //sSQL = sSQL + ',' + "SUM(RSW_TRANS_AMOUNT.AMOUNT) Amount , SUM(RSW_TRANS_AMOUNT.PAID_TOTAL) Paid_Total";
                                    
                            switch (m_sParentsysformname.ToLower())
                            {
                                case "claim":
                                    sSQL = sSQL + " FROM RSW_WORKSHEETS , CODES_TEXT , CODES WHERE CLAIM_ID = " + m_sParentID;
                                    break;

                                case "claimant":
                                    sSQL = sSQL + " FROM RSW_WORKSHEETS , CODES_TEXT , CODES WHERE CLAIMANT_ROW_ID = " + m_sParentID;
                                    break;

                                case "unit":
                                    sSQL = sSQL + " FROM RSW_WORKSHEETS , CODES_TEXT , CODES WHERE UNIT_ROW_ID = " + m_sParentID;
                                    break;

                                default:
                                    sSQL = sSQL + "FROM RSW_WORKSHEETS , CODES_TEXT , CODES WHERE CLAIM_ID = " + m_sParentID;
                                    break;
                            }
                        }

                        sSQL = sSQL + " AND RSW_STATUS_CODE IN (" + iHistoryCodeId + " , " + iApprovedCodeId + ") AND CODES_TEXT.CODE_ID = RSW_WORKSHEETS.RSW_STATUS_CODE" +
                                " AND CODES.CODE_ID = RSW_WORKSHEETS.RSW_STATUS_CODE  ";
                                
                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY DTTM_APPROVED DESC";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY DTTM_APPROVED DESC";
                        p_sPrimaryField = "RSW_ROW_ID";
                        p_sPrimaryFormField = "rswrowid";
                        break;
                    //Mridul. 10/22/09. MITS#18291
                    case "propertyunit":
                        if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_ORACLE)
                            sSQL = "SELECT P.PROPERTY_ID, P.PIN AS Property__ID, P.DESCRIPTION AS Description, NVL(P.ADDR1, '') || ' ' || NVL(P.ADDR2, '') || ' ' || NVL(P.ADDR3, '') || ' ' || NVL(P.ADDR4, '') AS Address, P.CITY, ST.STATE_ID AS State, P.ZIP_CODE AS Zip_Code FROM PROPERTY_UNIT P, STATES ST WHERE P.STATE_ID = ST.STATE_ROW_ID AND P.DELETED_FLAG <> -1";
                        else
                            sSQL = "SELECT P.PROPERTY_ID, P.PIN Property__ID, P.DESCRIPTION Description, COALESCE(P.ADDR1, '') + ' ' + COALESCE(P.ADDR2, '')+ ' ' + COALESCE(P.ADDR3, '')+ ' ' + COALESCE(P.ADDR4, '') Address, P.CITY, ST.STATE_ID State, P.ZIP_CODE Zip_Code FROM PROPERTY_UNIT P, STATES ST WHERE P.STATE_ID = ST.STATE_ROW_ID AND P.DELETED_FLAG <> -1";
                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY PIN";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY PIN";
                        p_sPrimaryField = "PROPERTY_ID";
                        p_sPrimaryFormField = "PIN";
                        break;
                        //rupal:start, policy system interface
                    case "siteunit":
                        if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_ORACLE)
                            //aahuja21 : MITS 31063 --> Minor change replaced P by S in P.ADDR2
                            sSQL = "SELECT S.SITE_ID, S.SITE_NUMBER AS SITE__ID, S.NAME AS Name, NVL(S.ADDR1, '') || ' ' || NVL(S.ADDR2, '')|| ' ' || NVL(S.ADDR3, '')|| ' ' || NVL(S.ADDR4, '') AS Address, S.CITY, ST.STATE_ID AS State, S.ZIP_CODE AS Zip_Code FROM SITE_UNIT S, STATES ST WHERE S.STATE_ID = ST.STATE_ROW_ID AND S.DELETED_FLAG <> -1";
                        else
                            sSQL = "SELECT S.SITE_ID,  S.SITE_NUMBER SITE__ID,S.NAME Name, COALESCE(S.ADDR1, '') + ' ' + COALESCE(S.ADDR2, '')+ ' ' + COALESCE(S.ADDR3, '')+ ' ' + COALESCE(S.ADDR4, '') Address, S.CITY, ST.STATE_ID State, S.ZIP_CODE Zip_Code FROM SITE_UNIT S, STATES ST WHERE S.STATE_ID = ST.STATE_ROW_ID AND S.DELETED_FLAG <> -1";
                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY SITE_NUMBER";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY SITE_NUMBER";
                        p_sPrimaryField = "SITE_ID";
                        p_sPrimaryFormField = "SITE_NUMBER";
                        break;                    
                        //rupal:end

                    //smahajan6 - MITS #18230 - 11/18/2009 :Start
                    case "claimpc":
                        strSql = new StringBuilder();
                        if (p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"] != null)
                        {
                            if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_ORACLE)
                            {
                                strSql.Append("SELECT P.PROPERTY_ID, P.PIN AS Property__ID, P.DESCRIPTION AS Description,");
                                strSql.Append(" NVL(P.ADDR1, '') || ' ' || NVL(P.ADDR2, '') || ' ' || NVL(P.ADDR3, '') || ' ' || NVL(P.ADDR4, '') AS Address, P.CITY, NVL(S.STATE_ID,'') ||' '|| NVL(S.STATE_NAME,'') AS State,");
                                strSql.Append(" P.ZIP_CODE AS Zip_Code, NVL(S.STATE_ROW_ID,0) AS StateRowID");
                            }
                            else
                            {
                                strSql.Append("SELECT P.PROPERTY_ID, P.PIN Property__ID, P.DESCRIPTION Description,");
                                strSql.Append(" COALESCE(P.ADDR1, '') + ' ' + COALESCE(P.ADDR2, '') + ' ' + COALESCE(P.ADDR3, '') + ' ' + COALESCE(P.ADDR4, '') Address, P.CITY, (COALESCE(S.STATE_ID,'') + ' ' + COALESCE(S.STATE_NAME,'')) State,");
                                strSql.Append(" P.ZIP_CODE Zip_Code, COALESCE(S.STATE_ROW_ID,0) StateRowID");
                            }

                            strSql.Append(" FROM PROPERTY_UNIT P , POLICY_X_UAR PXU, STATES S");
                            strSql.Append(" WHERE  P.DELETED_FLAG <> -1");
                            strSql.Append(" AND P.STATE_ID=S.STATE_ROW_ID");
                            strSql.Append(" AND P.PROPERTY_ID = PXU.UAR_ID");

                            if (p_objXmlDoc.SelectSingleNode("//claimpolicyidenh") != null)
                            {
                                //Sumit - Start(05/20/2010) - MITS# 20484 - Fetch records for exposure Ids of latest transaction Id.
                                iPolicyId= Conversion.CastToType<int>(p_objXmlDoc.SelectSingleNode("//claimpolicyidenh").Attributes.GetNamedItem("value").InnerText, out bIsSucess);
                                sSQL="SELECT EXPOSURE_ID FROM POLICY_X_EXP_ENH WHERE TRANSACTION_ID=(SELECT MAX(TRANSACTION_ID)FROM POLICY_X_EXP_ENH WHERE POLICY_ID=" + iPolicyId + ")";
                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                                while (objReader.Read())
                                {
                                    if(String.IsNullOrEmpty(sExposureIds))
                                    {
                                        sExposureIds = objReader.GetInt32("EXPOSURE_ID").ToString();
                                    }
                                    else
                                    {
                                        sExposureIds += ',' + objReader.GetInt32("EXPOSURE_ID").ToString();
                                    }
                                }
                                objReader.Close();
                                objReader.Dispose();
                                if(!String.IsNullOrEmpty(sExposureIds))
                                {
                                    strSql.Append(" AND PXU.EXPOSURE_ID IN (" + sExposureIds + ")");
                                }
                                //strSql.Append(" AND PXU.POLICY_ID = ");
                                //strSql.Append(Conversion.CastToType<int>(p_objXmlDoc.SelectSingleNode("//claimpolicyidenh").Attributes.GetNamedItem("value").InnerText, out bIsSucess));
                                //Sumit - End
                            }                            
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                            {
                                if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                                {
                                    strSql.Append(" ORDER BY PIN");
                                }
                            }
                            else
                            {
                                strSql.Append(" ORDER BY PIN");
                            }

                            p_sPrimaryField = "PROPERTY_ID";
                            p_sPrimaryFormField = "PIN";
                            p_sSecondaryField = "StateRowID";
                            p_sSecondaryFormField = "StateRowID";
                        }
                        break;
                    //smahajan6 - MITS #18230 - 11/18/2009 :End
                        //Added by Amitosh for R8 enhancement of Salvage

                    case "entityaddress":
                        //tmalhotra2 MITS 30194
                        strSql = new StringBuilder();
                        string currdate = Conversion.ToDbDate(System.DateTime.Today); //tmalhotra3- MITS 35404
                        //mbahl3 mits 31165
                        // RMA-17505 : start
                        string sEntityId = "0";
                        if (p_objXmlDoc.SelectSingleNode("//entityid").Attributes.GetNamedItem("value").InnerText != "")
                        {
                            sEntityId = p_objXmlDoc.SelectSingleNode("//entityid").Attributes.GetNamedItem("value").InnerText;
                        }
                        // RMA-17505 : end
                        if (sDBType == Constants.DB_SQLSRVR)
                        {
                            //zmohammad : Merging into CDR MITs 37124
                            //srajindersin MITS 37124 dt 09/29/2014
                            /* strSql.Append(string.Format("SELECT {0} FROM {1}  WHERE {2} AND {3} AND ENTITY_ID = {4} AND (EXPIRATION_DATE >= {5} OR EXPIRATION_DATE IS NULL OR EXPIRATION_DATE = '' OR PRIMARY_ADD_FLAG = -1)",
                                      "ADDRESS_ID,ADDR1,ADDR2,ADDR3,ADDR4,CITY,COUNTY,STATES.STATE_ID +' ' + STATES.STATE_NAME STATE,CODES_TEXT.SHORT_CODE + ' '+ CODES_TEXT.CODE_DESC COUNTRY,ZIP_CODE,ENTITY_X_ADDRESSES.STATE_ID STATEID , ENTITY_X_ADDRESSES.COUNTRY_CODE COUNTRYCODE",
                                      "ENTITY_X_ADDRESSES,STATES,CODES_TEXT",
                                      "ENTITY_X_ADDRESSES.STATE_ID=STATES.STATE_ROW_ID",
                                      "ENTITY_X_ADDRESSES.COUNTRY_CODE =  CODES_TEXT.CODE_ID ",
                                       p_objXmlDoc.SelectSingleNode("//entityid").Attributes.GetNamedItem("value").InnerText,
                                       "'" + currdate + "'"
                                         )); */
                            // RMA-17505 : start
                            //(Removing Primary_Add_Flag=-1, because now we need to bring records which are not expired)
                            strSql.Append(string.Format("SELECT {0} FROM {1}  WHERE {2} AND {3} AND {4} AND ENTITY_ID = {5} AND (EXPIRATION_DATE >= {6} OR EXPIRATION_DATE IS NULL OR EXPIRATION_DATE = '') AND (EFFECTIVE_DATE <= {6} OR EFFECTIVE_DATE IS NULL OR EFFECTIVE_DATE = '')",
                                      "ADDRESS.ADDRESS_ID,ADDRESS.ADDR1,ADDRESS.ADDR2,ADDRESS.ADDR3,ADDRESS.ADDR4,ADDRESS.CITY,ADDRESS.COUNTY,STATES.STATE_ID +' ' + STATES.STATE_NAME STATE,CODES_TEXT.SHORT_CODE + ' '+ CODES_TEXT.CODE_DESC COUNTRY,ZIP_CODE,ADDRESS.STATE_ID STATEID , ADDRESS.COUNTRY_CODE COUNTRYCODE",
                                      "ENTITY_X_ADDRESSES,ADDRESS,STATES,CODES_TEXT",
                                      "ENTITY_X_ADDRESSES.ADDRESS_ID=ADDRESS.ADDRESS_ID",
                                      "ADDRESS.STATE_ID=STATES.STATE_ROW_ID",
                                      "ADDRESS.COUNTRY_CODE =  CODES_TEXT.CODE_ID ",
                                       sEntityId,
                                       "'" + currdate + "'"
                                         ));
                            // RMA-17505 : end
                            p_sPrimaryField = "ADDRESS_ID";
                            p_sPrimaryFormField = "addrid";
                            p_sSecondaryField = "COUNTRYCODE";
                            p_sSecondaryFormField = "hidden";
                            p_sThirdField = "STATEID";
                            p_sThirdFormField = "staterowid";
                            //mbahl3 mits 31165
                        }
                        else if (sDBType == Constants.DB_ORACLE)
                        {
                            //zmohammad : Merging into CDR MITs 37124
                            //tmalhotra3- MITS 35404 //srajindersin MITS 37124 dt 09/29/2014
                            /* strSql.Append(string.Format("SELECT {0} FROM {1}  WHERE {2} AND {3} AND ENTITY_ID = {4} AND (EXPIRATION_DATE >= {5} OR EXPIRATION_DATE IS NULL OR EXPIRATION_DATE = '' OR PRIMARY_ADD_FLAG = -1)",
                             "ADDRESS_ID,ADDR1,ADDR2,ADDR3,ADDR4,CITY,COUNTY,STATES.STATE_ID || ' ' ||  STATES.STATE_NAME STATE,CODES_TEXT.SHORT_CODE ||  ' ' || CODES_TEXT.CODE_DESC COUNTRY,ZIP_CODE,ENTITY_X_ADDRESSES.STATE_ID STATEID , ENTITY_X_ADDRESSES.COUNTRY_CODE COUNTRYCODE",
                             "ENTITY_X_ADDRESSES,STATES,CODES_TEXT",
                             "ENTITY_X_ADDRESSES.STATE_ID=STATES.STATE_ROW_ID",
                             "ENTITY_X_ADDRESSES.COUNTRY_CODE =  CODES_TEXT.CODE_ID ",
                              p_objXmlDoc.SelectSingleNode("//entityid").Attributes.GetNamedItem("value").InnerText,
                              "'" + currdate + "'"
                                )); */
                            // RMA-17505 : start
                            //(Removing Primary_Add_Flag=-1, because now we need to bring records which are not expired)
                            strSql.Append(string.Format("SELECT {0} FROM {1}  WHERE {2} AND {3} AND {4} AND ENTITY_ID = {5} AND (EXPIRATION_DATE >= {6} OR EXPIRATION_DATE IS NULL OR EXPIRATION_DATE = '') AND (EFFECTIVE_DATE <= {6} OR EFFECTIVE_DATE IS NULL OR EFFECTIVE_DATE = '')",
                                      "ADDRESS.ADDRESS_ID,ADDRESS.ADDR1,ADDRESS.ADDR2,ADDRESS.ADDR3,ADDRESS.ADDR4,ADDRESS.CITY,ADDRESS.COUNTY,STATES.STATE_ID || ' ' ||  STATES.STATE_NAME STATE,CODES_TEXT.SHORT_CODE ||  ' ' || CODES_TEXT.CODE_DESC COUNTRY,ZIP_CODE,ADDRESS.STATE_ID STATEID , ADDRESS.COUNTRY_CODE COUNTRYCODE",
                                      "ENTITY_X_ADDRESSES,ADDRESS,STATES,CODES_TEXT",
                                      "ENTITY_X_ADDRESSES.ADDRESS_ID=ADDRESS.ADDRESS_ID",
                                      "ADDRESS.STATE_ID=STATES.STATE_ROW_ID",
                                      "ADDRESS.COUNTRY_CODE =  CODES_TEXT.CODE_ID ",
                                       sEntityId,
                                       "'" + currdate + "'"
                                         ));
                            // RMA-17505 : end
                            p_sPrimaryField = "ADDRESS_ID";
                            p_sPrimaryFormField = "addrid";
                            p_sSecondaryField = "COUNTRYCODE";
                            p_sSecondaryFormField = "hidden";
                            p_sThirdField = "STATEID";
                            p_sThirdFormField = "staterowid";
                        }
                        //mbahl3 mits 31165
                        //tmalhotra2 end
                        break;
                        //Added by amitosh for R8 enhancement EFT Payments
                    case "bankinginfo":

                         if (m_sParentID != String.Empty)
                        {
                            sId = m_sParentID;
                        }
                        else if (p_objXmlDoc.SelectSingleNode("//entityid") != null)
                        {
                            sId = p_objXmlDoc.SelectSingleNode("//entityid").Attributes.GetNamedItem("value").InnerText;
                        }
                        

                         sSQL = " SELECT BANKING_ROW_ID,BANK_NAME, ROUTING_NUMBER, ACCOUNT_NUMBER,CODES_TEXT.CODE_DESC BANK_STATUS_CODE,EFFECTIVE_DATE,EXPIRATION_DATE FROM  ENTITY_X_BANKING,CODES_TEXT WHERE BANK_STATUS_CODE=CODES_TEXT.CODE_ID And ENTITY_X_BANKING.ENTITY_ID = ";
                        sSQL = sSQL + sId;
                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY BANKING_ROW_ID";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY BANKING_ROW_ID";
                        p_sPrimaryField = "BANKING_ROW_ID";
                        p_sPrimaryFormField = "BankingRowId";
                        break;
                        //end Amitosh

                        //Added by Amitosh for R8 enhancement of liabilityloss
                    case "liabilityloss":
                    
                        if (m_sParentID != String.Empty)
                        {
                            sId = m_sParentID;
                        }
                        else if (p_objXmlDoc.SelectSingleNode("//claimid") != null)
                        {
                            sId = p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText;
                        }
                        else if (p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"] != null)
                        {
                            sId = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("claimid").InnerText;
                        }

                        strSql = new StringBuilder();

                        if (sId != string.Empty)
                        {
                            strSql.Append(string.Format("SELECT {0} FROM {1} LEFT OUTER JOIN {2} ON {3} LEFT OUTER JOIN {4} ON {5} LEFT OUTER JOIN {6} ON {7} WHERE {8}"
                                , "ROW_ID, LIABILITY_TYPE_CODE.CODE_DESC | LIABILITY_TYPE, DAMAGE_TYPE_CODE.CODE_DESC | DAMAGE_TYPE, PARTY_AFFECTED.LAST_NAME | PARTY_AFFECTED"
                                , "CLAIM_X_LIABILITYLOSS"
                                , "CODES_TEXT | LIABILITY_TYPE_CODE"
                                , "LIABILITY_TYPE_CODE.CODE_ID = CLAIM_X_LIABILITYLOSS.LIABILITY_TYPE_CODE"
                                , "CODES_TEXT | DAMAGE_TYPE_CODE"
                                , "DAMAGE_TYPE_CODE.CODE_ID = CLAIM_X_LIABILITYLOSS.DAMAGE_TYPE_CODE"
                                , "ENTITY | PARTY_AFFECTED"
                                , "PARTY_AFFECTED.ENTITY_ID = CLAIM_X_LIABILITYLOSS.PARTY_AFFECTED"
                                , "CLAIM_ID = ")
                            );

                            strSql.Append(sId);


                            if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                            {
                                if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                                {
                                    strSql.Append(" ORDER BY ROW_ID");
                                }
                            }
                            else
                                strSql.Append(" ORDER BY ROW_ID");

                            p_sPrimaryField = "ROW_ID";
                            p_sPrimaryFormField = "llrowid";
                        }
                        break;
                        //end Amitosh
                        //skhare7 R8 Enhancement Combined Payment Setup
                    case "combinedpayment":
                        sSQL = "SELECT  COMBINED_PAY_ROW_ID,TAX_ID  ,FIRST_NAME, LAST_NAME, " ; 

                        if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_ORACLE)
                            sSQL = sSQL + "NVL(ADDR1, '') || ' ' || NVL(ADDR2, '')|| ' ' || NVL(ADDR3, '')|| ' ' || NVL(ADDR4, '') AS Address";
                        else
                            sSQL = sSQL + "COALESCE(ADDR1, '') + ' ' + COALESCE(ADDR2, '') + ' ' + COALESCE(ADDR3, '') + ' ' + COALESCE(ADDR4, '') Address";
                        sSQL = sSQL + ", CITY, ZIP_CODE, STATES.STATE_NAME ,";
                        sSQL = sSQL + " STARTING_ON_DATE,NEXT_SCD_DATE ,STOP_DATE,STOP_FLAG";
                        sSQL = sSQL + " FROM COMBINED_PAYMENTS, STATES";
                        sSQL = sSQL + " WHERE COMBINED_PAYMENTS.STATE_ID = STATES.STATE_ROW_ID";
                    

                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + "  ORDER BY COMBINED_PAY_ROW_ID";
                            }
                        }
                        else
                        {
                            sSQL = sSQL + " ORDER BY COMBINED_PAY_ROW_ID";
                        }
                        p_sPrimaryField = "COMBINED_PAY_ROW_ID";
                        p_sPrimaryFormField = "CombPayRowId";
                        break;
                        //rupal:start, r8 enh
                    case "personinvolved":
                        int iClaimId = 0;
                        if (p_objXmlDoc.SelectSingleNode("//claimid") != null)
                        {
                            iClaimId = Conversion.CastToType<int>(p_objXmlDoc.SelectSingleNode("//claimid").Attributes.GetNamedItem("value").InnerText.Trim(), out bIsSucess);
                        }
                        else if (p_objXmlDoc.SelectSingleNode("//Data").Attributes["claimid"] != null)
                        {
                            iClaimId = Conversion.CastToType<int>(p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("claimid").InnerText.Trim(), out bIsSucess);
                        }
                        ////Ankit Starts : For MITS - 30882 (Prefix/Suffix Issue)
                        //sSQL = " SELECT DISTINCT PI_ROW_ID, PERSON_INVOLVED.PI_EID, STATES.STATE_ROW_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME, " +
                        //        " ENTITY.MIDDLE_NAME, ENTITY.TAX_ID, ENTITY.ADDR1, ENTITY.ADDR2,ENTITY.ADDR3, ENTITY.ADDR4, ENTITY.CITY, STATES.STATE_ID, STATES.STATE_NAME, " +
                        //        " ENTITY.ZIP_CODE, CT.CODE_DESC, CTP.CODE_DESC PREFIX, CTS.CODE_DESC SUFFIX_COMMON, " +
                        //        " ENTITY.COUNTRY_CODE COUNTRYID , C.SHORT_CODE COUNTRY_ID, PT.CODE_DESC  COUNTRY_NAME " +
                        //        //" , PERSON_INVOLVED.PI_ER_ROW_ID " +          //avipinsrivas Start : Worked for Jira-340
                        //        " FROM PERSON_INVOLVED " +
                        //        " LEFT OUTER JOIN ENTITY_X_BANKING ON PERSON_INVOLVED.PI_EID = ENTITY_X_BANKING.ENTITY_ID " +
                        //        " LEFT OUTER JOIN CODES_TEXT CT ON ENTITY_X_BANKING.BANK_STATUS_CODE = CT.CODE_ID " +
                        //        " INNER JOIN ENTITY ON ENTITY.ENTITY_ID=PERSON_INVOLVED.PI_EID " +
                        //        //rkatyal4: 12024 start 
                        //        //" INNER JOIN STATES ON ENTITY.STATE_ID=STATES.STATE_ROW_ID " +
                        //        " LEFT OUTER JOIN STATES ON ENTITY.STATE_ID=STATES.STATE_ROW_ID " +
                        //        " INNER JOIN CLAIM ON PERSON_INVOLVED.EVENT_ID = CLAIM.EVENT_ID " +
                        //        " LEFT OUTER JOIN CODES_TEXT CTP ON CTP.CODE_ID = ENTITY.PREFIX " +
                        //        " LEFT OUTER JOIN CODES_TEXT CTS ON CTS.CODE_ID = ENTITY.SUFFIX_COMMON " +
                        //        //" INNER JOIN  CODES C ON C.CODE_ID = ENTITY.COUNTRY_CODE " +
                        //        //" INNER JOIN  CODES_TEXT PT ON PT.CODE_ID = ENTITY.COUNTRY_CODE " +
                        //        " LEFT OUTER JOIN  CODES C ON C.CODE_ID = ENTITY.COUNTRY_CODE " +
                        //        " LEFT OUTER JOIN  CODES_TEXT PT ON PT.CODE_ID = ENTITY.COUNTRY_CODE " +
                        //        //rkatyal4: 12024 end 
                        //        " WHERE CLAIM.CLAIM_ID = {0} ";

                        //pgupta215 Starts - PI Lookup
                        //sSQL = " SELECT DISTINCT PI_ROW_ID, PERSON_INVOLVED.PI_EID, STATES.STATE_ROW_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME, " +
                        sSQL = " SELECT MAX(PI_ROW_ID) PI_ROW_ID, PERSON_INVOLVED.PI_EID, STATES.STATE_ROW_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME, " +
                                " ENTITY.MIDDLE_NAME, ENTITY.TAX_ID, ENTITY.ADDR1, ENTITY.ADDR2,ENTITY.ADDR3, ENTITY.ADDR4, ENTITY.CITY, STATES.STATE_ID, STATES.STATE_NAME, " +
                                " ENTITY.ZIP_CODE, CT.CODE_DESC, CTP.CODE_DESC PREFIX, CTS.CODE_DESC SUFFIX_COMMON, " +
                                " ENTITY.COUNTRY_CODE COUNTRYID , C.SHORT_CODE COUNTRY_ID, PT.CODE_DESC  COUNTRY_NAME " +
                                " FROM PERSON_INVOLVED " +
                                " LEFT OUTER JOIN ENTITY_X_BANKING ON PERSON_INVOLVED.PI_EID = ENTITY_X_BANKING.ENTITY_ID " +
                                " LEFT OUTER JOIN CODES_TEXT CT ON ENTITY_X_BANKING.BANK_STATUS_CODE = CT.CODE_ID " +
                                " LEFT JOIN ENTITY ON ENTITY.ENTITY_ID=PERSON_INVOLVED.PI_EID " +
                                " LEFT OUTER JOIN STATES ON ENTITY.STATE_ID=STATES.STATE_ROW_ID " +
                                " LEFT JOIN CLAIM ON PERSON_INVOLVED.PARENT_ROW_ID=CLAIM.CLAIM_ID" +
                                " LEFT OUTER JOIN CODES_TEXT CTP ON CTP.CODE_ID = ENTITY.PREFIX " +
                                " LEFT OUTER JOIN CODES_TEXT CTS ON CTS.CODE_ID = ENTITY.SUFFIX_COMMON " +
                                " LEFT OUTER JOIN  CODES C ON C.CODE_ID = ENTITY.COUNTRY_CODE " +
                                " LEFT OUTER JOIN  CODES_TEXT PT ON PT.CODE_ID = ENTITY.COUNTRY_CODE " +
                                " LEFT JOIN EVENT ON PERSON_INVOLVED.EVENT_ID= EVENT.EVENT_ID " +
                                " WHERE CLAIM.CLAIM_ID = {0} AND  UPPER(PERSON_INVOLVED.PARENT_TABLE_NAME)='CLAIM'   " +
                                " OR PARENT_ROW_ID in (SELECT EVENT_ID FROM CLAIM WHERE CLAIM.EVENT_ID=PERSON_INVOLVED.EVENT_ID and CLAIM_ID= {0} AND UPPER(PERSON_INVOLVED.PARENT_TABLE_NAME)='EVENT') " +
                                " OR PARENT_ROW_ID in (SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE POLICY_ID=PERSON_INVOLVED.PARENT_ROW_ID AND CLAIM_ID= {0}  AND UPPER(PERSON_INVOLVED.PARENT_TABLE_NAME)='POLICY')  " +
                                " GROUP BY PERSON_INVOLVED.PI_EID, STATES.STATE_ROW_ID, ENTITY.LAST_NAME, ENTITY.FIRST_NAME,  ENTITY.MIDDLE_NAME, ENTITY.TAX_ID, ENTITY.ADDR1, ENTITY.ADDR2,ENTITY.ADDR3, ENTITY.ADDR4, ENTITY.CITY, STATES.STATE_ID, STATES.STATE_NAME,  ENTITY.ZIP_CODE, CT.CODE_DESC, CTP.CODE_DESC , CTS.CODE_DESC ,  ENTITY.COUNTRY_CODE , C.SHORT_CODE , PT.CODE_DESC ";
                        sSQL = string.Format(sSQL, iClaimId);
                        //pgupta215 End


                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY ENTITY.LAST_NAME ";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY ENTITY.LAST_NAME ";
                        p_sPrimaryField = "PI_ROW_ID";
                        p_sPrimaryFormField = "pirowid";
                        p_sSecondaryField = "PI_EID";
                        p_sSecondaryFormField = "hidden";
                        p_sThirdField = "STATE_ROW_ID";
						p_sThirdFormField = "staterowid";
                        p_arrlstHiddenFields.Insert(0,"CODE_DESC");//Added by Amitosh For EFT Payment

                        //Ankit Starts : For MITS - 30882 (Prefix/Suffix Issue)
                        p_arrlstHiddenFields.Insert(1,"PREFIX");
                        p_arrlstHiddenFields.Insert(2, "SUFFIX_COMMON");
                        //Ankit End
                        //p_arrlstHiddenFields.Insert(3, "PI_ER_ROW_ID");     //avipinsrivas Start : Worked for Jira-340
                        break;
                        //rupal:end

                    //Aman Catastrophe Enhancement MITS -28528  start 
                    case "catastrophe":
                        sSQL = "SELECT CATASTROPHE_ROW_ID, CAT_NUMBER, LOSS_START_DATE, LOSS_END_DATE FROM CATASTROPHE ";
                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY CATASTROPHE.CAT_CODE";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY CATASTROPHE.CAT_CODE";
                        p_sPrimaryField = "CATASTROPHE_ROW_ID";
                        p_sPrimaryFormField = "catastropherowid";
                        break;
                    //Aman Catastrophe Enhancement MITS -28528  END
                    //Aman Driver Enhancement                   
                    case "driver":
                        objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                        iTableId = objLocalCache.GetTableId("DRIVERS");
                        if (p_objXmlDoc.SelectSingleNode("//entitytableid").Attributes.GetNamedItem("value") != null)
                        {
                            iCodeId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//entitytableid").Attributes.GetNamedItem("value").InnerText);
                        }
                        sSQL = "SELECT DRIVER_ROW_ID, LAST_NAME, FIRST_NAME, ALSO_KNOWN_AS, TAX_ID, PHONE1 FROM DRIVER, ENTITY WHERE DRIVER_EID = ENTITY.ENTITY_ID AND DRIVER.DRIVER_TYPE = " + iCodeId;
                        if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                        {
                            if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml == "")
                            {
                                sSQL = sSQL + " ORDER BY DRIVER_ROW_ID";
                            }
                        }
                        else
                            sSQL = sSQL + " ORDER BY DRIVER_ROW_ID";
                        p_sPrimaryField = "DRIVER_ROW_ID";
                        p_sPrimaryFormField = "driverrowid";
                        break;
                    //Aman Driver Enhancement
                    //MITS 34260 start
                    case "claimxpolicy":

                        sSQL = "SELECT POLICY.POLICY_ID ,POLICY_NUMBER , POLICY_NAME , EFFECTIVE_DATE  , EXPIRATION_DATE FROM CLAIM_X_POLICY , POLICY WHERE POLICY.POLICY_ID = CLAIM_X_POLICY.POLICY_ID AND CLAIM_X_POLICY.CLAIM_ID ="+ m_sParentID;
                        p_sPrimaryField = "POLICY_ID";
                        p_sPrimaryFormField = "policyid";
                        break;
                    //MITS 34260 end
					default:
						// BSB 12.2.2005 
						// Enhance to test for and pick up ANY admin tracking table not just "explicit"
						// default ones shipped with the product. (Also fixes handling for the explicit ones.)
						m_objADM = this.m_objDataModelFactory.GetDataModelObject("ADMTable",false) as ADMTable;
						try{m_objADM.TableName = p_sFormName.ToUpper();}
						catch(Exception){}
						if(m_objADM.TableId!=0)//Matching ADMTable found.
						{
							m_bIsADMTable=true;
							sSQL="SELECT " +  m_objADM.KeyName + " {lookupfields} FROM {fromtables} {filter} ";
							if(	(p_objXmlDoc.SelectSingleNode("//OrderBy")==null)||
								(p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml==""))//Uses short circuit...
								sSQL = sSQL + " ORDER BY " + m_objADM.KeyName;
							
							p_sPrimaryField=m_objADM.KeyName;
							p_sPrimaryFormField="sysformid";
						}
						else
							sSQL = "";
						break;
				} // end of switch
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("Lookup.GetSQLFromFormName.Error", m_iClientId), p_objEx);
			}
            if (strSql != null)
            {
                return strSql.ToString();
            }
            else
            {
                return sSQL;
            }
		}

		
		/// <summary>
		/// ChangeSQLAdminTrck changes the SQL qury for the adminstrative tracking tables
		/// </summary>
		/// <param name="p_sSQL">SQL query which is changed</param>
		/// <param name="p_sFormName">name of the form</param>
		private void ChangeSQLAdminTrck(ref string p_sSQL, string p_sFormName)
		{
			string sSqlLookupFields = string.Empty;
			string sLookupFields = string.Empty;
			string sWhere = string.Empty;
			string[] arrLookupFields = {""};
			string sField = string.Empty;
			string sAlias = string.Empty;
			string sFrom = string.Empty;
            string[] arr = { "" };  // Added by csingh7 MITS 18657 

            DbReader objReader = null;

            sSqlLookupFields = "SELECT SYS_FIELD_NAME, FIELD_TYPE";         //Ankit Start : MITS - 32387

            sSqlLookupFields = sSqlLookupFields + " FROM SUPP_DICTIONARY";
            sSqlLookupFields = sSqlLookupFields + " WHERE SUPP_TABLE_NAME = '" + p_sFormName.ToUpper() + "'";
            sSqlLookupFields = sSqlLookupFields + " AND (LOOKUP_FLAG < 0 or LOOKUP_FLAG > 0)";
            //igupta3 MITS:25132 Changes starts
            sSqlLookupFields = sSqlLookupFields + " AND (FIELD_TYPE < 14 or FIELD_TYPE > 17 AND FIELD_TYPE <> 22)"; //added by rsriharsha for avoiding the "FIELD_TYPE=22" from lookup(RMA-13316)
            //igupta3 MITS:25132 Changes ends

            sSqlLookupFields = sSqlLookupFields + " AND (DELETE_FLAG = 0 or DELETE_FLAG IS NULL)";

            try
            {
				objReader = DbFactory.GetDbReader(m_sConnectionString, sSqlLookupFields);
				while(objReader.Read())
				{
                    //Ankit Start : MITS - 32387
                    if (objReader.GetInt16("FIELD_TYPE") == 8)     //8 represents Entity
                        sField = objReader.GetString("SYS_FIELD_NAME") + "_ENTITY";
                    else
                        sField = objReader.GetString("SYS_FIELD_NAME");

                    sLookupFields = sLookupFields + "," + sField;
                    //Ankit End
				}
				objReader.Close();
                //MGaba2:MITS 20724:In case of Oracle,if lookup field doesnt exist for a table,it was crashing
                //In function GetLookUpData,for this column,it was trying to get supplemental field with column name as 'NoLookupFieldDefined'
                if (sLookupFields == "")
                {
                 //   sLookupFields = ", 'No Lookup Field Defined'";
                    p_sSQL = "";
                }
                else                
                {
                    sLookupFields = sLookupFields.ToUpper();
                    //				Log.Write("SLookupFields:" + sLookupFields + "\nSQL:" + p_sSQL);
                    if (sLookupFields.IndexOf("_CODE") > 0 || sLookupFields.IndexOf("_STATE") > 0 || sLookupFields.IndexOf("_ENTITY") > 0)          ////Ankit Start : MITS - 32387
                    {
                        sLookupFields = sLookupFields.Substring(1);
                        arrLookupFields = new string[1];
                        if (sLookupFields.IndexOf(",") > 0)
                            arrLookupFields = sLookupFields.Split(',');
                        else
                            arrLookupFields[0] = sLookupFields;

                        sLookupFields = "";
                        //zmohammad MITS 38496 Start
                        for (int i = 0; i < arrLookupFields.Length; i++)
                        {
                            if (arrLookupFields[i].IndexOf("_CODE") > 0)
                            {
                                sField = arrLookupFields[i];
                                sFrom = sFrom + "," + " CODES_TEXT CT" + i + "";
                                if (sWhere == "")
                                    sWhere += " WHERE ";
                                else
                                    sWhere += " AND ";
                                sWhere += " CT" + i + ".CODE_ID = " + p_sFormName.ToUpper() + "." + sField;
                                sLookupFields = sLookupFields + ",CT" + i + ".CODE_DESC " + sField;
                            }
                            else if (arrLookupFields[i].IndexOf("_STATE") > 0)
                            {
                                sField = arrLookupFields[i];
                                sLookupFields = sLookupFields + ",(SELECT STATES.STATE_ID FROM STATES WHERE STATE_ROW_ID = " + sField + ") " + sField;
                            }
                            //Ankit Start : MITS - 32387
                            else if (arrLookupFields[i].IndexOf("_ENTITY") > 0)
                            {
                                sField = arrLookupFields[i].Replace("_ENTITY", "");

                                if (m_objDataModelFactory.Context.DbConn.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                                    sLookupFields = sLookupFields + ", ISNULL((E" + i + ".LAST_NAME + ',' + ISNULL(E" + i + ".FIRST_NAME,'')),'') " + sField;
                                else if (m_objDataModelFactory.Context.DbConn.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                                    sLookupFields = sLookupFields + ", NVL((E" + i + ".LAST_NAME || ',' || NVL(E" + i + ".FIRST_NAME,'')),'') " + sField;

                                sFrom = sFrom + " LEFT OUTER JOIN ENTITY E" + i + " On E" + i + ".ENTITY_ID = " + p_sFormName.ToUpper() + "." + sField;
                            }
                            //Ankit End
                            else
                                sLookupFields = sLookupFields + "," + arrLookupFields[i];
                        }// end of for
                    }// end - if
                    //}
                    sFrom = p_sFormName.ToUpper() + sFrom;
                    p_sSQL = p_sSQL.Replace("{fromtables}", sFrom);
                    p_sSQL = p_sSQL.Replace("{filter}", sWhere);
                    p_sSQL = p_sSQL.Replace("{lookupfields}", sLookupFields);
                }
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("Lookup.ChangeSQLAdminTrck.Error", m_iClientId), p_objEx);
			}
			finally
			{
				//Modified by Shivendu
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                    
                }
			}
		}

        /// <summary>
        /// GetRecordCount returns the number of records in the database corresponding to a SQL query
        /// </summary>
        /// <param name="p_sSQL">SQL query</param>
        /// <returns>record count</returns>
        private int GetRecordCount(string p_sSQL)
        {
            string sCountSQL = string.Empty;
            string sBeforeFromSQL = string.Empty;
            int iStart = 0;
            int iLength = 0;
            int iRecLen = 0;

            DbReader objReader = null;
            DataSet ds = null;
            try
            {
                ds = DbFactory.GetDataSet(m_sConnectionString, p_sSQL, m_iClientId);
                if (ds.Tables[0] != null)
                {
                    iRecLen = ds.Tables[0].Rows.Count;
                    ds.Dispose();
                    return iRecLen;
                }
                //                sCountSQL = p_sSQL;
                //                sBeforeFromSQL = p_sSQL.Substring(0, p_sSQL.IndexOf(" FROM "));

                //                if (sBeforeFromSQL.IndexOf("(SELECT") >= 0)
                //                {
                //                    iStart = p_sSQL.IndexOf("(SELECT");
                //                    iLength = p_sSQL.IndexOf("]") + 1;
                //                    sCountSQL = "SELECT COUNT(*) " + p_sSQL.Substring(0, iLength);
                //                    iLength = p_sSQL.IndexOf("ORDER BY") - 1;
                //                    sCountSQL = sCountSQL.Substring(0, iLength);
                //                }
                //                else
                //                {
                //                    iStart = p_sSQL.IndexOf("FROM");
                //                    iLength = p_sSQL.IndexOf("ORDER BY") - iStart;
                //                    if (iLength > 0)
                //                        sCountSQL = "SELECT COUNT(*) " + sCountSQL.Substring(iStart, iLength);
                //                    else
                //                        sCountSQL = "SELECT COUNT(*) " + sCountSQL.Substring(iStart);
                //                }
                ////				Log.Write(sCountSQL);
                //                objReader = DbFactory.GetDbReader(m_sConnectionString, sCountSQL);
                //                objReader.Read();
                //                iRecLen = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);

                //                objReader.Close();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Lookup.GetRecordCount.Error", m_iClientId), p_objEx);
            }
            finally
            {
                //Modified by Shivendu
                //if (objReader != null)
                //{
                //    objReader.Close();
                //    objReader.Dispose();

                //}
                if (ds != null)
                    ds.Dispose();
            }
            return iRecLen;
        }


		/// <summary>
		/// GetSysEx returns name-value pairs of the sys_ex parameters
		/// </summary>
		/// <param name="p_objXmlDoc">input XML document</param>
		/// <returns>sys_ex params</returns>
		private string GetSysEx(XmlDocument p_objXmlDoc)
		{
			string sRet = string.Empty;
			string[] arrSys_ex = {""};
			string sValue = string.Empty;
			string sSysEx = string.Empty;
			
			try
			{
				if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("sys_ex") != null)
					sSysEx = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("sys_ex").InnerText;

				if (sSysEx == "")
					return "";
			
				arrSys_ex = new string[1];
				if (sSysEx.IndexOf(",") > 0)
					arrSys_ex = sSysEx.Split(',');
				else
					arrSys_ex[0] = sSysEx;

				for (int i = 0; i < arrSys_ex.Length; i++)
				{
                    if (arrSys_ex[i].StartsWith("/"))
                    {
                        arrSys_ex[i] = arrSys_ex[i].Substring(1);
                    }
                    if (p_objXmlDoc.SelectSingleNode("//" + arrSys_ex[i]) != null)
                    {
                        sValue = p_objXmlDoc.SelectSingleNode("//" + arrSys_ex[i]).Attributes.GetNamedItem("value").InnerText;
                        if (sValue != "")
                            sRet = sRet + "&" + arrSys_ex[i] + "=" + sValue;
                    }
                    else
                    {
                        return ""; 
                    }
				}

				if (sRet.StartsWith("&"))
					sRet = sRet.Substring(1);
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("Lookup.GetSysEx.Error", m_iClientId), p_objEx);
			}
			return sRet;
		}


		/// <summary>
		/// FormatFieldName formats a column name and returns it
		/// </summary>
		/// <param name="p_sColName">Column Name</param>
		/// <returns>formatted column name</returns>
		private string FormatFieldName(string p_sColName)
		{
			try
			{
				p_sColName = p_sColName.Trim().ToUpper();
                if (p_sColName == "RESOLVE_CLAIMANT_NAME")
                    p_sColName = "Claimant Name";
                //Mridul. MITS 18292 11/23/09. Handled specially to avoid displaying "ID" as "Id".
                else if (string.Compare(p_sColName, "Property__ID", StringComparison.OrdinalIgnoreCase) == 0)
                    p_sColName = "Property_ID";
                //rupal:start, policu system interface
                else if (string.Compare(p_sColName, "SITE__ID", StringComparison.OrdinalIgnoreCase) == 0)
                    p_sColName = "Site_ID";
                //rupal:end
                else if (string.Compare(p_sColName, "VEH_DESC", StringComparison.OrdinalIgnoreCase) == 0)
                    p_sColName = "Description";
                else
                {
                    p_sColName = p_sColName.Replace("_SCODE", "");
                    p_sColName = p_sColName.Replace("_EID", "");
                    p_sColName = p_sColName.Replace("_CODE", "");
                    p_sColName = p_sColName.Replace("_TABLE_ID", "");
                    p_sColName = p_sColName.Replace("_FLAG", "");
                    //bkumar33: cosmetic change
                    //p_sColName = p_sColName.Replace("_", " ");
                    //end
                    p_sColName = MixedCase(p_sColName);
                }
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("Lookup.FormatFieldName.Error", m_iClientId), p_objEx);
			}
			return p_sColName;
		}


		/// <summary>
		/// MixedCase converts a string to the mixed case format
		/// </summary>
		/// <param name="p_sFormat">input string</param>
		/// <returns>formatted input string</returns>
		private string MixedCase(string p_sFormat)
		{
			string[] arrColWords = {""};
			string sRet = string.Empty;

			if (p_sFormat.IndexOf("_") > 0)
				arrColWords = p_sFormat.Split('_');
			else
				arrColWords[0] = p_sFormat;

			for (int i = 0; i < arrColWords.Length; i++)
				sRet = sRet + arrColWords[i].Substring(0, 1).ToUpper() + arrColWords[i].Substring(1).ToLower() + " ";

			return sRet.Trim();
		}


		/// <summary>
		/// FormatField formats the input data and returns it
		/// </summary>
		/// <param name="p_sData">Data to be formatted</param>
		/// <param name="p_sColName">Column Name</param>
		/// <param name="p_sColType">Column Type</param>
		/// <returns>Formatted Data</returns>

        private string FormatField(string p_sData, string p_sColName, string p_sColType)
        {
            string sTmp = string.Empty;
            try
            {
                switch (p_sColType)
                {
                    case "System.Int32":
                        if (p_sColName.IndexOf("_FLAG") > 0)
                            if (Conversion.IsNumeric(p_sData) && p_sData != "")
                                if (Conversion.ConvertStrToInteger(p_sData) != 0)
                                    sTmp = "Yes";
                                else
                                    sTmp = "No";
                            else
                                sTmp = "No";
                        else if (p_sColName.ToUpper().IndexOf("PRIMARY_CASE_MGR") == 0) //Akashyap3 : Mits No: 15080, 08-May-09
                        {
                            if (Conversion.IsNumeric(p_sData) && p_sData != "")
                                if (Conversion.ConvertStrToInteger(p_sData) != 0)
                                    sTmp = "Yes";
                                else
                                    sTmp = "No";
                            else
                                sTmp = "No";
                        }

                        else
                            sTmp = p_sData;
                        break;
                    case "System.Int16":
                        if (p_sColName.IndexOf("_FLAG") > 0)
                            if (Conversion.IsNumeric(p_sData) && p_sData != "")
                                if (Conversion.ConvertStrToInteger(p_sData) != 0)
                                    sTmp = "Yes";
                                else
                                    sTmp = "No";
                            else
                                sTmp = "No";
                        else if (p_sColName.ToUpper().IndexOf("PRIMARY_CASE_MGR") == 0) //Akashyap3 : Mits No: 15080, 08-May-09
                        {
                            if (Conversion.IsNumeric(p_sData) && p_sData != "")
                                if (Conversion.ConvertStrToInteger(p_sData) != 0)
                                    sTmp = "Yes";
                                else
                                    sTmp = "No";
                            else
                                sTmp = "No";
                        }

                        else
                            sTmp = p_sData;
                        break;

                    case "System.Int64":
                        if (p_sColName.IndexOf("_FLAG") > 0)
                            if (Conversion.IsNumeric(p_sData) && p_sData != "")
                                if (Conversion.ConvertStrToInteger(p_sData) != 0)
                                    sTmp = "Yes";
                                else
                                    sTmp = "No";
                            else
                                sTmp = "No";
                        else if (p_sColName.ToUpper().IndexOf("PRIMARY_CASE_MGR") == 0) //Akashyap3 : Mits No: 15080, 08-May-09
                        {
                            if (Conversion.IsNumeric(p_sData) && p_sData != "")
                                if (Conversion.ConvertStrToInteger(p_sData) != 0)
                                    sTmp = "Yes";
                                else
                                    sTmp = "No";
                            else
                                sTmp = "No";
                        }

                        else
                            sTmp = p_sData;
                        break;

                    case "System.Double":
                        sTmp = p_sData;
                        break;
                    case "System.Single":
                        sTmp = p_sData;
                        break;
                    case "System.String":
                        if (p_sColName.ToUpper().IndexOf("DATE") >= 0 && p_sData.Length == 8)
                        {
                            if (p_sData != "")
                                sTmp = Conversion.GetDBDateFormat(p_sData, "d");
                        }
                        //10/09/2009 Raman Bhatia: Creating Lookup for Reserve History in Reserve Worksheet
                        //else if (p_sColName.ToUpper().IndexOf("DTTM") >= 0) 
                        else if (p_sColName.ToUpper().IndexOf("DTTM") >= 0 || p_sColName.ToUpper().IndexOf("DATE_TIME") >= 0) 
                        {
                            sTmp = Conversion.GetDBDTTMFormat(p_sData, "d", "t");
                        }
                        else if (p_sColName.ToUpper().IndexOf("TIME") >= 0 && p_sData.Length == 6)
                        {
                            sTmp = Conversion.GetDBTimeFormat(p_sData, "t");
                        }
                        else
                            sTmp = p_sData;
                        if (sTmp.Length > 255)
                            sTmp = sTmp.Substring(0, 255) + "...";
                        if (sTmp=="\" \"")
                        {
                            sTmp = string.Empty;
                        }
                        break;
                    default:
                        sTmp = p_sData;
                        if (sTmp.Length > 255)
                            sTmp = sTmp.Substring(0, 255) + "...";
                        break;
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Lookup.FormatField.Error", m_iClientId), p_objEx);
            }
            return sTmp;
        }

		private void Initialize(UserLogin objUser)
		{
			try
			{
				m_objDataModelFactory = new DataModelFactory(objUser, m_iClientId);	
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;				
			}
			catch(DataModelException p_objEx)
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("Lookup.Initialize.Error", m_iClientId) , p_objEx );				
			}			
		}
        
        /// <summary>
        /// Gets the number records displayed per page 
        /// </summary>
        /// <returns></returns>
        private int GetRecordsPerPage()
        {
            StringBuilder sbSQL = null;
            int iReturn = 0;
            DbReader objReader = null;
            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE");
                sbSQL.Append(" WHERE PARM_NAME = 'SRCH_USER_LIMIT' ");
                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader.Read())
                {
                    iReturn = Conversion.ConvertObjToInt(objReader.GetValue("STR_PARM_VALUE"), m_iClientId);
                    if (iReturn < 10)
                        iReturn = 10;//Record per Page should greater than equal to 10
                }
                else
                {
                    //iReturn=0;
                    iReturn = 10;//Record per Page should greater than equal to 10
                }
                if (!objReader.IsClosed)
                {
                    objReader.Close();
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                sbSQL = null;
                if (!objReader.IsClosed)
                {
                    objReader.Close();
                    //Shivendu to dispose the object
                    objReader.Dispose();
                }
                objReader = null;
            }
            return iReturn;
        }

        /// <summary>
        /// spahariya MITS 30700 to show unit with involved coverages list
        /// </summary>
        /// <param name="p_iClaimID">Claim Id</param>
        /// <param name="sFormName">Form name</param>
        /// <returns></returns>
        public XmlDocument GetUnitCovLst(int p_iClaimID, string sFormName)
        {
            StringBuilder sbSQL = new StringBuilder();
            DbReader objReader = null ;
            DbReader objDBReader = null;
            DataSet objDataSet = null;
            XmlDocument objDOM = null;
            XmlElement objElemTemp = null;
            XmlElement objChildElemTemp = null;
            string sUnitId = string.Empty;
            try
            {
                objDOM = new XmlDocument();
                objElemTemp = objDOM.CreateElement("UnitCoverageLst");
                objDOM.AppendChild(objElemTemp);
                if (string.Compare(sFormName, "unit", true) == 0)
                {
                    objElemTemp = objDOM.CreateElement("UnitCoverage");
                    objChildElemTemp = objDOM.CreateElement("ID");
                    objChildElemTemp.InnerText = "Vin";
                    objElemTemp.AppendChild(objChildElemTemp);
                    objChildElemTemp = objDOM.CreateElement("CoverageType");
                    objChildElemTemp.InnerText = "Involved Coverages";
                    objElemTemp.AppendChild(objChildElemTemp);
                    objDOM.FirstChild.AppendChild(objElemTemp);

                     sbSQL.Append(" SELECT UNIT_ID from UNIT_X_CLAIM WHERE CLAIM_ID = " + p_iClaimID.ToString());
                     using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                     {
                         while (objReader.Read())
                         {
                             sUnitId = Convert.ToString(objReader.GetValue(0));
                             sbSQL.Length = 0;
                             sbSQL.Append("SELECT DISTINCT COV_TYPE.CODE_DESC, V.VIN ");
                             sbSQL.Append(" FROM VEHICLE V,POLICY_X_UNIT PU, CLAIM_X_POLICY CP, POLICY_X_CVG_TYPE PCT, RESERVE_CURRENT RC, COVERAGE_X_LOSS CL, CODES_TEXT COV_TYPE ");
                             sbSQL.Append(" WHERE V.UNIT_ID = PU.UNIT_ID");
                             sbSQL.Append(" AND PU.POLICY_ID = CP.POLICY_ID");
                             sbSQL.Append(" AND PCT.POLICY_UNIT_ROW_ID = PU.POLICY_UNIT_ROW_ID");
                             sbSQL.Append(" AND RC.POLCVG_LOSS_ROW_ID = CL.CVG_LOSS_ROW_ID");
                             sbSQL.Append(" AND Cl.POLCVG_ROW_ID = PCT.POLCVG_ROW_ID");
                             sbSQL.Append(" AND RC.CLAIM_ID = CP.CLAIM_ID");
                             sbSQL.Append(" AND COV_TYPE.CODE_ID = PCT.COVERAGE_TYPE_CODE");
                             sbSQL.Append(" AND PU.UNIT_TYPE = 'V'");
                             sbSQL.Append(" AND CP.CLAIM_ID = " + p_iClaimID.ToString());
                             sbSQL.Append(" AND PU.UNIT_ID = " + sUnitId);
                             sbSQL.Append(" ORDER BY V.VIN");

                             using (objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId))
                             {
                                 if (objDataSet != null && objDataSet.Tables[0].Rows.Count > 0)
                                 {
                                     for (int iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                                     {
                                         objElemTemp = objDOM.CreateElement("UnitCoverage");
                                         objChildElemTemp = objDOM.CreateElement("ID");
                                         objChildElemTemp.InnerText = Convert.ToString(objDataSet.Tables[0].Rows[iCnt][1]);
                                         objElemTemp.AppendChild(objChildElemTemp);
                                         objChildElemTemp = objDOM.CreateElement("CoverageType");
                                         objChildElemTemp.InnerText = Convert.ToString(objDataSet.Tables[0].Rows[iCnt][0]);
                                         objElemTemp.AppendChild(objChildElemTemp);
                                         objDOM.FirstChild.AppendChild(objElemTemp);
                                     }
                                 }
                                 else
                                 {
                                     sbSQL.Length = 0;
                                     sbSQL.Append(" SELECT VIN FROM VEHICLE WHERE UNIT_ID = " + sUnitId);
                                     using (objDBReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                                     {
                                         while (objDBReader.Read())
                                         {
                                             objElemTemp = objDOM.CreateElement("UnitCoverage");
                                             objChildElemTemp = objDOM.CreateElement("ID");
                                             objChildElemTemp.InnerText = Convert.ToString(objDBReader.GetValue(0));
                                             objElemTemp.AppendChild(objChildElemTemp);
                                             objChildElemTemp = objDOM.CreateElement("CoverageType");
                                             objChildElemTemp.InnerText = "";
                                             objElemTemp.AppendChild(objChildElemTemp);
                                             objDOM.FirstChild.AppendChild(objElemTemp);
                                         }
                                     }
                                 }
                             }
                         }
                     }
                }
                else if (string.Compare(sFormName, "propertyloss", true) == 0)
                {
                    objElemTemp = objDOM.CreateElement("UnitCoverage");
                    objChildElemTemp = objDOM.CreateElement("ID");
                    objChildElemTemp.InnerText = "Property ID";
                    objElemTemp.AppendChild(objChildElemTemp);
                    objChildElemTemp = objDOM.CreateElement("CoverageType");
                    objChildElemTemp.InnerText = "Involved Coverages";
                    objElemTemp.AppendChild(objChildElemTemp);
                    objDOM.FirstChild.AppendChild(objElemTemp);


                    sbSQL.Length = 0;
                    sbSQL.Append(" SELECT PROPERTY_ID FROM CLAIM_X_PROPERTYLOSS WHERE CLAIM_ID = " + p_iClaimID.ToString());
                    using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                    {
                        while (objReader.Read())
                        {
                            sUnitId = Convert.ToString(objReader.GetValue(0));
                            sbSQL.Length = 0;
                            sbSQL.Append("SELECT DISTINCT COV_TYPE.CODE_DESC, P.PIN  ");
                            sbSQL.Append(" FROM PROPERTY_UNIT P,POLICY_X_UNIT PU, CLAIM_X_POLICY CP, POLICY_X_CVG_TYPE PCT, RESERVE_CURRENT RC, COVERAGE_X_LOSS CL, CODES_TEXT COV_TYPE");
                            sbSQL.Append(" WHERE P.PROPERTY_ID = PU.UNIT_ID");
                            sbSQL.Append(" AND PU.POLICY_ID = CP.POLICY_ID");
                            sbSQL.Append(" AND PCT.POLICY_UNIT_ROW_ID = PU.POLICY_UNIT_ROW_ID");
                            sbSQL.Append(" AND RC.POLCVG_LOSS_ROW_ID = CL.CVG_LOSS_ROW_ID");
                            sbSQL.Append(" AND Cl.POLCVG_ROW_ID = PCT.POLCVG_ROW_ID");
                            sbSQL.Append(" AND RC.CLAIM_ID = CP.CLAIM_ID");
                            sbSQL.Append(" AND COV_TYPE.CODE_ID = PCT.COVERAGE_TYPE_CODE");
                            sbSQL.Append(" AND PU.UNIT_TYPE = 'P'");
                            sbSQL.Append(" AND CP.CLAIM_ID = " + p_iClaimID.ToString());
                            sbSQL.Append(" AND PU.UNIT_ID = " + sUnitId);
                            sbSQL.Append(" ORDER BY P.PIN");


                            using (objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId))
                            {
                                if (objDataSet != null && objDataSet.Tables[0].Rows.Count > 0)
                                {
                                    for (int iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                                    {
                                        objElemTemp = objDOM.CreateElement("UnitCoverage");
                                        objChildElemTemp = objDOM.CreateElement("ID");
                                        objChildElemTemp.InnerText = Convert.ToString(objDataSet.Tables[0].Rows[iCnt][1]);
                                        objElemTemp.AppendChild(objChildElemTemp);
                                        objChildElemTemp = objDOM.CreateElement("CoverageType");
                                        objChildElemTemp.InnerText = Convert.ToString(objDataSet.Tables[0].Rows[iCnt][0]);
                                        objElemTemp.AppendChild(objChildElemTemp);
                                        objDOM.FirstChild.AppendChild(objElemTemp);
                                    }
                                }
                                else
                                {
                                    sbSQL.Length = 0;
                                    sbSQL.Append(" SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = " + sUnitId);
                                    using (objDBReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                                    {
                                        while (objDBReader.Read())
                                        {
                                            objElemTemp = objDOM.CreateElement("UnitCoverage");
                                            objChildElemTemp = objDOM.CreateElement("ID");
                                            objChildElemTemp.InnerText = Convert.ToString(objDBReader.GetValue(0));
                                            objElemTemp.AppendChild(objChildElemTemp);
                                            objChildElemTemp = objDOM.CreateElement("CoverageType");
                                            objChildElemTemp.InnerText = "";
                                            objElemTemp.AppendChild(objChildElemTemp);
                                            objDOM.FirstChild.AppendChild(objElemTemp);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Lookup.GetUnitCovLst.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objDBReader != null)
                {
                    objDBReader.Close();
                    objDBReader.Dispose();
                }
                if (objDataSet != null)
                {
                    objDataSet = null;
                }
            }
            return objDOM;
        }
        
		#endregion


        //dvatsa- JIRA 11108 (start)
        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
            p_objParentNode.AppendChild(p_objChildNode);
        }

        public XmlDocument GetLookUpData_Async(XmlDocument p_objXmlDoc, UserLogin p_objUserLogin, string p_sPageName, string p_sGridId)
        {
            #region variable declaration
            XmlDocument objXmlDoc = new XmlDocument();
            XmlElement objData = null;
            XmlElement objHead = null;
            XmlElement objNode = null;
            XmlElement objRows = null;
            XmlElement objCol = null;
            XmlElement objHiddenInputs = null;
            XmlElement objUserPref = null;
            XmlElement objLookUpXmlElement = null;
            DbReader objReader = null;
            SysParms objSysSetting = null;
            StringBuilder strJsonResponse = new StringBuilder();
            bool bIsMobileAdjuster = false;
            bool bIsMobilityAdjuster = false;
            int iUserID = 0;
            int iDSNId = 0;
            int iTmp = 0;
            string sFormName = string.Empty;
            string sSQL = string.Empty;
            string sDBType = string.Empty;
            string strJsonUserPref = "";
            string sPrimaryField = string.Empty;
            string sSecondaryField = string.Empty;
            string sThirdField = string.Empty;
            string sForthField = string.Empty;
            string sPrimaryFormField = string.Empty;
            string sSecondaryFormField = string.Empty;
            string sThirdFormField = string.Empty;
            //Added:Yukti, DT:05/21/2014, MITS 35772iPageSize
            string sSQLUnit = string.Empty;
            string sUnitNumber = string.Empty;
            string sValueAdjType = string.Empty;//dvatsa
            string sValueCodeDesc = string.Empty;//dvatsa
            if (p_objUserLogin != null)
            {
                iUserID = p_objUserLogin.UserId;
                iDSNId = p_objUserLogin.DatabaseId;
                if (m_iLangCode == 0)
                    m_iLangCode = p_objUserLogin.objUser.NlsCode;
            }
            #endregion
            //MITS 11688 Raman Bhatia
            //Default Page Size set in utilities is not working for lookups
            SysSettings objSysSettings = null;//Deb MITS 25775
            int iPageSize = DEFAULT_PAGE_SIZE;
            iPageSize = GetRecordsPerPage();

            //  int iRecordCount = 0;
            //  if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("recordcount") != null)
            //    iRecordCount = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("recordcount").InnerText);

            int iStartAt = 0;

            //  int iPageCount = 0;
            //  if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("pagecount") != null)
            //  iPageCount = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("pagecount").InnerText);

            int iPageNumber = 0;
            if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("pagenumber") != null)
                iPageNumber = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("pagenumber").InnerText);
            if (iPageNumber == 0)
                iPageNumber = 1;



            //smahajan6 - MITS #18230 - 11/18/2009 :Start 
            //For Hidden Columns - Array of Columns to Hide
            ArrayList arrlstHiddenFields = new ArrayList();
            //smahajan6 - MITS #18230 - 11/18/2009 :End
            string sFpid = string.Empty;


            if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("fpid") != null)
                sFpid = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("fpid").InnerText;

            string sFpidName = string.Empty;
            if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("fpidname") != null)
                sFpidName = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("fpidname").InnerText;

            string sSid = string.Empty;
            if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("sid") != null)
                sSid = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("sid").InnerText;

            string sPsid = string.Empty;
            if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("psid") != null)
                sPsid = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("psid").InnerText;

            string sSysEx = string.Empty;
            if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("sys_ex") != null)
                sSysEx = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("sys_ex").InnerText;

            string sClaimid = string.Empty;
            if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("claimid") != null)
                sClaimid = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("claimid").InnerText;

            try
            {
                //BSB Moved this call into the Ctor.
                //this.Initialize();
                sFormName = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("form").InnerText;
                sFormName = sFormName.ToLower();

                //Raman: modified for R5
                if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("ParentID") != null)
                {
                    m_sParentID = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("ParentID").InnerText;

                }
                sParentID = m_sParentID;
                //10/09/2009 Raman Bhatia: Creating Lookup for Reserve History in Reserve Worksheet
                if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("parentsysformname") != null)
                {
                    m_sParentsysformname = p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem("parentsysformname").InnerText;
                }
               
                //smahajan6 - MITS #18230 - 11/18/2009 : Start
                //Additional Argument for Hidden Columns - To Get the List of Columns to Hide
                sSQL = GetSQLFromFormName(sFormName, p_objXmlDoc, ref sPrimaryField, ref sSecondaryField, ref sThirdField, ref sForthField, ref sPrimaryFormField, ref sSecondaryFormField, ref sThirdFormField, ref arrlstHiddenFields);
                //sSQL =  GetSQLFromFormName(sFormName, p_objXmlDoc, ref sPrimaryField, ref sSecondaryField, ref sThirdField, ref sForthField, ref sPrimaryFormField, ref sSecondaryFormField, ref sThirdFormField);
                //smahajan6 - MITS #18230 - 11/18/2009 : End
                if (p_objXmlDoc.SelectSingleNode("//OrderBy") != null)
                {
                    if (p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml != "")
                    {
                        sSQL = sSQL + " ORDER BY " + p_objXmlDoc.SelectSingleNode("//OrderBy").InnerXml + " " + p_objXmlDoc.SelectSingleNode("//OrderMode").InnerXml;
                    }
                }
                if (m_bIsADMTable)
                {
                    ChangeSQLAdminTrck(ref sSQL, sFormName);
                }

                sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString();

                if (sDBType == Constants.DB_ACCESS)
                    sSQL = sSQL.Replace("|", " AS ");
                else
                    // npadhy In case of Oracle || is used for Concatenation and | is used as Alias
                    // The Code below preserves the || operator for concatenation
                    sSQL = sSQL.Replace("||", "^@").Replace("|", " ").Replace("^@", "||");

                //MGaba2:MITS 20724:Crashing when Lookup fields are not defined for Admin Tracking Table in Oracle

                //  if (iRecordCount == 0 && sSQL != "")
                //  iRecordCount = GetRecordCount(sSQL);
                //MITS 11688 Raman Bhatia
                //Default Page Size set in utilities is not working for lookups

                //  iPageCount = (int)Math.Ceiling((double)iRecordCount / iPageSize);
                if (iPageNumber == 1)
                    iStartAt = 1;
                else
                    //iStartAt = ((iPageNumber - 1) * DEFAULT_PAGE_SIZE) + 1;
                    iStartAt = ((iPageNumber - 1) * iPageSize) + 1;
                objLookUpXmlElement = objXmlDoc.CreateElement("LookUpNavigation");
                objXmlDoc.AppendChild(objLookUpXmlElement);

                CreateElement(objXmlDoc.DocumentElement, "Data", ref objData);
                objData.SetAttribute("form", sFormName);

                if (sSecondaryFormField == "hidden")
                {
                    objData.SetAttribute("fpid2", sPrimaryFormField);
                    objData.SetAttribute("fpid3", sThirdFormField);
                }
                else
                    objData.SetAttribute("fpid2", sSecondaryFormField);

                objData.SetAttribute("psid", sPsid);
                objData.SetAttribute("sys_ex", sSysEx);

                objData.SetAttribute("sys_ex_name", GetSysEx(p_objXmlDoc));

                //  objData.SetAttribute("recordcount", iRecordCount.ToString()); 

                //  objData.SetAttribute("pagecount", iPageCount.ToString()); 

                objData.SetAttribute("thispage", iPageNumber.ToString());

                objData.SetAttribute("fpid", sFpid);

                objData.SetAttribute("pagesize", Convert.ToString(iPageSize));

                objData.SetAttribute("sid", sSid); //TODO  

                int iColCnt = 0;
                string sColName = string.Empty;
                string sUnAliasedFieldName = "";
                string sDbType = string.Empty;
                //				Log.Write(sSQL);
                //MGaba2:MITS 20724:Crashing when Lookup fields are not defined for Admin Tracking Table in Oracle
                if (sSQL != "")
                {
                    //Commented by Amitosh for MITS 27378
                    //Deb MITS 25775
                    //objSysSettings = new SysSettings(m_sConnectionString);
                    //if (objSysSettings.MaskSSN)
                    //{
                    //    if (sSQL.IndexOf("ENTITY.TAX_ID") > -1)
                    //    {
                    //        sSQL = sSQL.Replace("ENTITY.TAX_ID", "'###-##-####' TAX_ID");
                    //    }
                    //    else if (sSQL.IndexOf("TAX_ID") > -1)
                    //    {
                    //        sSQL = sSQL.Replace("TAX_ID", "'###-##-####' TAX_ID");
                    //    }
                    //}
                    //Deb MITS 25775
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    iColCnt = objReader.FieldCount;
                    // move to the current page
                    iRowCount = 1;
                    if (iStartAt > 1)
                    {
                        while (iRowCount < iStartAt)
                        {
                            iRowCount++;
                            objReader.Read();
                        }
                    }
                    objNode = null;

                    string sValue = string.Empty;
                    string sTmpStr = string.Empty;
                    string[] arrPID = { "" };
                    iRowCount = 0;
                    if (p_objXmlDoc.SelectSingleNode("//caller") != null)
                    {
                        bIsMobileAdjuster = true;
                        bIsMobilityAdjuster = true;
                    }
                    //MITS 11688 Raman Bhatia
                    //Default Page Size set in utilities is not working for lookups
                    //while (objReader.Read() && iCount < DEFAULT_PAGE_SIZE)

                    while (objReader.Read() && (iRowCount < iPageSize || bIsMobileAdjuster || bIsMobilityAdjuster))
                    {
                        iRowCount++;
                        if (iRowCount > 1)
                            strJsonResponse.Append(",");
                        if (iRowCount == 1)
                        {
                            strJsonResponse.Append("[");
                        }
                        strJsonResponse.Append("{");
                        
                        sValue = objReader.GetValue(sPrimaryField).ToString();
                        sValue = RemoveComma(sValue); //dvatsa-added code to handle comma ,refer MITS 15832 
                        strJsonResponse.Append(Conversion.GetJsonNode("pid", sValue, true));
                        sFormName = RemoveComma(sFormName); //dvatsa-added code to handle comma ,refer MITS 15832 
                        strJsonResponse.Append(Conversion.GetJsonNode("formName", sFormName));
                        //Added:Yukti, DT:05/22/2014, MITS 35772
                        if (sFormName == "claimant")
                        {

                            if (sDBType == Constants.DB_SQLSRVR)
                            {
                                sSQLUnit = "SELECT CASE WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='P' THEN (SELECT 'PIN:' + PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                        + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='V' THEN (SELECT 'VIN:' + VIN FROM VEHICLE WHERE UNIT_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                        + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='S' THEN (SELECT 'SITE:' + SITE_NUMBER FROM SITE_UNIT WHERE SITE_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                        + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='SU' THEN (SELECT 'STAT:' + ISNULL(ENTITY.LAST_NAME, '') + ISNULL(ENTITY.FIRST_NAME, '') FROM ENTITY WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM OTHER_UNIT WHERE OTHER_UNIT_ID = CLAIMANT_X_UNIT.UNIT_ID))"
                                        + " END AS UNIT_NUMBER FROM CLAIMANT_X_UNIT WHERE CLAIMANT_ROW_ID = " + sValue + " ";
                            }
                            else if (sDBType == Constants.DB_ORACLE)
                            {
                                sSQLUnit = "SELECT CASE WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='P' THEN (SELECT 'PIN:' || PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                        + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='V' THEN (SELECT 'VIN:' || VIN FROM VEHICLE WHERE UNIT_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                        + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='S' THEN (SELECT 'SITE:' || SITE_NUMBER FROM SITE_UNIT WHERE SITE_ID = CLAIMANT_X_UNIT.UNIT_ID)"
                                        + " WHEN CLAIMANT_X_UNIT.UNIT_TYPE ='SU' THEN (SELECT 'STAT:' || NVL(ENTITY.LAST_NAME, '') || NVL(ENTITY.FIRST_NAME, '') FROM ENTITY WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM OTHER_UNIT WHERE OTHER_UNIT_ID = CLAIMANT_X_UNIT.UNIT_ID))"
                                        + " END AS UNIT_NUMBER FROM CLAIMANT_X_UNIT WHERE CLAIMANT_ROW_ID = " + sValue + " ";
                            }

                            using (DbReader objReaderUnit = DbFactory.GetDbReader(m_sConnectionString, sSQLUnit))
                            {
                                while (objReaderUnit.Read())
                                {
                                    if (sUnitNumber != null && sUnitNumber != "")
                                    {
                                        sUnitNumber = sUnitNumber + "," + Convert.ToString(objReaderUnit.GetValue("UNIT_NUMBER"));
                                    }
                                    else
                                    {
                                        sUnitNumber = Convert.ToString(objReaderUnit.GetValue("UNIT_NUMBER"));
                                    }
                                }
                            }

                            sUnitNumber = RemoveComma(sUnitNumber);   //dvatsa-added code to handle comma ,refer MITS 15832 
                            strJsonResponse.Append(Conversion.GetJsonNode("pid5", sUnitNumber));
                        }
                        //Ended:Yukti, DT:05/22/2014,MITS 35772
                        if (sSecondaryField != "")
                            sValue = objReader.GetValue(sSecondaryField).ToString();
                        else
                            sValue = "";
                        if (sSecondaryFormField == "origvalues")
                        {
                            sTmpStr = sValue;
                            if (sThirdField != string.Empty)
                            {
                                sValue = objReader.GetValue(sThirdField).ToString();
                                sTmpStr = sTmpStr + "|" + sValue;
                            }
                            if (sForthField != string.Empty)
                            {
                                sValue = objReader.GetValue(sForthField).ToString();
                                sTmpStr = sTmpStr + "|" + sValue;
                                sTmpStr = RemoveComma(sTmpStr);  //dvatsa- code for handling comma - refer MITS 15832
                            }

                            sTmpStr = RemoveComma(sTmpStr); //dvatsa-added code to handle comma ,refer MITS 15832 
                            strJsonResponse.Append(Conversion.GetJsonNode("pid2", sTmpStr)); 

                        }
                        else if (sSecondaryFormField == "hidden")
                        {
                            sValue = objReader.GetValue(sSecondaryField).ToString();
                            sValue = RemoveComma(sValue);//dvatsa-added code to handle comma ,refer MITS 15832 
                            strJsonResponse.Append(Conversion.GetJsonNode("pid2", sValue));

                            if (sThirdFormField == "staterowid")
                            {
                                sValue = objReader.GetValue(sThirdField).ToString();
                                sValue = RemoveComma(sValue); //dvatsa-added code to handle comma ,refer MITS 15832 
                                strJsonResponse.Append(Conversion.GetJsonNode("pid3", sValue));
                            }

                        }
                        else
                        {
                            sValue = RemoveComma(sValue); //dvatsa-added code to handle comma ,refer MITS 15832 
                            strJsonResponse.Append(Conversion.GetJsonNode("pid2", sValue));
                        }

                        //smahajan6 - MITS #18230 - 11/18/2009 : Start
                        //For Hidden Columns - Fetch Value for Hidden Columns
                        if (arrlstHiddenFields.Count > 0)
                        {
                            sValue = "";
                            sTmpStr = "";
                            for (int iCnt = 0; iCnt < arrlstHiddenFields.Count; iCnt++)
                            {
                                sTmpStr = objReader.GetValue(arrlstHiddenFields[iCnt].ToString()).ToString();
                                sValue += sTmpStr + ARR_DELIMITER;
                            }
                            sValue = RemoveComma(sValue);//dvatsa-added code to handle comma ,refer MITS 15832 
                            strJsonResponse.Append(Conversion.GetJsonNode("pid4", sValue));


                        }
                        //smahajan6 - MITS #18230 - 11/18/2009 : End
                        if (sFpid != "")
                        {
                            arrPID = new string[1];
                            if (sFpid.IndexOf(",") > 0)
                                arrPID = sFpid.Split(',');
                            else
                                arrPID[0] = sFpid;

                            //Changed by Gagan for MITS 9633 : Start
                            if (sFpidName == "")
                            {
                                for (iTmp = 0; iTmp < arrPID.Length; iTmp++)
                                {
                                    if (p_objXmlDoc.SelectSingleNode("//" + arrPID[iTmp]) != null)
                                    {
                                        if (p_objXmlDoc.SelectSingleNode("//" + arrPID[iTmp]).Attributes.GetNamedItem("value") != null)
                                        {
                                            sFpidName = sFpidName + arrPID[iTmp] + "=" + p_objXmlDoc.SelectSingleNode("//" + arrPID[iTmp]).Attributes.GetNamedItem("value").InnerText + "&";
                                        }
                                    }
                                    else if (p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem(arrPID[iTmp]) != null)
                                    {
                                        sFpidName = sFpidName + arrPID[iTmp] + "=" + p_objXmlDoc.SelectSingleNode("//Data").Attributes.GetNamedItem(arrPID[iTmp]).InnerText + "&";
                                    }
                                }
                            }
                            else
                            {
                                iTmp = 1;
                            }
                            //Changed by Gagan for MITS 9633 : End

                        }

                        if (iTmp > 0)
                        {
                            //Removed extra & sign at the end of fpidname
                            if (sFpidName.EndsWith("&"))
                                sFpidName = sFpidName.Substring(0, sFpidName.Length - 1);
                            if (sFpidName.EndsWith("="))
                                sFpidName = "";
                            sFpidName = RemoveComma(sFpidName); //dvatsa-added code to handle comma ,refer MITS 15832 
                            strJsonResponse.Append(Conversion.GetJsonNode("fpidname", sFpidName));

                        }
                        else
                        {
                            sPrimaryFormField = RemoveComma(sPrimaryFormField); //dvatsa-added code to handle comma ,refer MITS 15832
                            strJsonResponse.Append(Conversion.GetJsonNode("fpid", sPrimaryFormField));

                        }
                        for (int i = 0; i < iColCnt; i++)
                        {

                            sColName = objReader.GetName(i);
                            //rupal
                            if (sColName.ToUpper() == "VIN-ADDRESS" && string.Equals(sFormName, "otherunitloss", StringComparison.InvariantCultureIgnoreCase))
                                sColName = "REFERENCE_NUMBER";
                            //rupal
                            //dvatsa - Code to create node for Adjuster Type in Json(start)
                            if (sColName.ToUpper() == "ADJUSTER_TYPE")
                            {
                                sValueAdjType = Convert.ToString(objReader.GetValue("ADJUSTER_TYPE"));
                                if(!String.IsNullOrEmpty(sValueAdjType))
                                {
                                    string sSQLAdj = "select distinct CT.CODE_DESC from CODES_TEXT CT,CLAIM_ADJUSTER CA where CT.CODE_ID ="+ sValueAdjType;
                                    DbReader objReaderAdjuster = DbFactory.GetDbReader(m_sConnectionString, sSQLAdj);
                                    while (objReaderAdjuster.Read())
                                    {
                                        sValueCodeDesc = objReaderAdjuster.GetValue("CODE_DESC").ToString();
                                    }
                                    if (sValueCodeDesc != null)
                             			strJsonResponse.Append(Conversion.GetJsonNode("code_desc", sValueCodeDesc)); //dvatsa- JIRA 17683
                                 }
                            }
                            //dvatsa - Code to create node for Adjuster Type in Json(end)

                            //smahajan6 - MITS #18230 - 11/18/2009 : Start
                            //Additional Check for Hidden Columns - Prevent Display of Hidden Columns
                            if (sColName != sPrimaryField && sColName != sSecondaryField && !arrlstHiddenFields.Contains(sColName))
                            //if (sColName != sPrimaryField && sColName != sSecondaryField)
                            //smahajan6 - MITS #18230 - 11/18/2009 : End
                            {
                                if ((sSecondaryFormField == "hidden" && sColName != sThirdField)
                                    || (sSecondaryFormField != "hidden"))
                                {
                                    //commented by Shivendu for R5
                                    //objCol = objXmlDoc.CreateElement("Col");
                                    //objNode.AppendChild(objCol);
                                    //objCol.SetAttribute("name", sColName);

                                    //MITS 11203 by Gagan : Start
                                    sValue = FormatField(objReader.GetValue(i).ToString(), sColName, objReader.GetFieldType(i).ToString());
                                    //dvatsa - added for handling sorting in ng-grid -JIRA 11108 (start)
                                    if (sFormName == "adjusterdatedtext" || sFormName == "casemgrnotes" || sFormName == "eventdatedtext")
                                    {
                                        if (sColName.ToUpper() == "DATE_ENTERED") //dvatsa- JIRA 17683
                                          strJsonResponse.Append(Conversion.GetJsonNode("date_entered.DbValue", sValue)); //dvatsa- JIRA 17683
                                    }
                                    else if (sFormName == "litigation")
                                    {
                                        if (sColName.ToUpper() == "SUIT_DATE") //dvatsa- JIRA 17683
                                          strJsonResponse.Append(Conversion.GetJsonNode("suit_date.DbValue", sValue));//dvatsa- JIRA 17683
                                        else if (sColName.ToUpper() == "COURT_DATE")
                                           strJsonResponse.Append(Conversion.GetJsonNode("court_date.DbValue", sValue)); //dvatsa- JIRA 17683
                                    }
                                    else if (sFormName == "demandoffer" && sColName.ToUpper() == "DEMAND_OFFER_DATE") //dvatsa- JIRA 17683
                                    {
                                        strJsonResponse.Append(Conversion.GetJsonNode("demand_offer_date.DbValue", sValue)); //dvatsa- JIRA 17683
                                    }
                                    else if (sFormName == "subrogation" && sColName.ToUpper() == "STATUS_DATE") //dvatsa- JIRA 17683
                                    {
                                       strJsonResponse.Append(Conversion.GetJsonNode("status_date.DbValue", sValue)); //dvatsa- JIRA 17683
                                    }
                                    else if (sFormName == "propertyloss")
                                    {
                                        if (sColName.ToUpper() == "DATEREPORTED") //dvatsa- JIRA 17683
                                        {
                                           strJsonResponse.Append(Conversion.GetJsonNode("datereported.DbValue", sValue)); //dvatsa- JIRA 17683
                                        }
                                        else if (sColName.ToUpper() == "UNITNO")
                                        {
                                            strJsonResponse.Append(Conversion.GetJsonNode("unitno.DbValue", sValue));  //dvatsa- JIRA 17683
                                        } 
                                    }
                                    else if (sFormName == "unit")
                                    {
                                        if (sColName.ToUpper() == "UNITNO") //dvatsa- JIRA 17683
                                          strJsonResponse.Append(Conversion.GetJsonNode("unitno.DbValue", sValue));  //dvatsa- JIRA 17683
                                    }
                                    else if (sFormName == "arbitration" && sColName.ToUpper() == "DATE_FILED") //dvatsa- JIRA 17683
                                    {
                                        strJsonResponse.Append(Conversion.GetJsonNode("date_filed.DbValue", sValue)); //dvatsa- JIRA 17683
                                    }
                                    else if (sFormName == "cmxaccommodation" && sColName.ToUpper() == "ACCOMM_ACCEPTED_DATE")  //dvatsa- JIRA 17683
                                    {
                                        strJsonResponse.Append(Conversion.GetJsonNode("accomm_accepted_date.DbValue", sValue)); //dvatsa- JIRA 17683
                                    }


                                    else if (sFormName == "cmxcmgrhist")
                                    {
                                        if (sColName.ToUpper() == "REFERRAL_DATE")
                                           strJsonResponse.Append(Conversion.GetJsonNode("referral_date.DbValue", sValue));
                                        else if (sColName.ToUpper() == "DATE_CLOSED")
                                           strJsonResponse.Append(Conversion.GetJsonNode("date_closed.DbValue", sValue)); //dvatsa- JIRA 17683
                                        else if (sColName.ToUpper() == "UR_DUE_DATE")
                                            strJsonResponse.Append(Conversion.GetJsonNode("ur_due_date.DbValue", sValue));
                                    }
                                    else if (sFormName == "claimxpolicy")
                                    {
                                        if (sColName.ToUpper() == "EFFECTIVE_DATE")
                                           strJsonResponse.Append(Conversion.GetJsonNode("effective_date.DbValue", sValue)); //dvatsa- JIRA 17683
                                        else if (sColName.ToUpper() == "EXPIRATION_DATE")
                                            strJsonResponse.Append(Conversion.GetJsonNode("expiration_date.DbValue", sValue));
                                    }
                                    else if (sFormName == "medwatchtest")
                                    {
                                        if (sColName.ToUpper() == "TEST_DATE")
                                           strJsonResponse.Append(Conversion.GetJsonNode("test_date.DbValue", sValue)); //dvatsa- JIRA 17683
                                    }
                                    else if (sFormName == "concomitant")
                                    {
                                        if (sColName.ToUpper() == "FROM_DATE")
                                            strJsonResponse.Append(Conversion.GetJsonNode("from_date.DbValue", sValue));
                                        if (sColName.ToUpper() == "TO_DATE")
                                            strJsonResponse.Append(Conversion.GetJsonNode("to_date.DbValue", sValue)); //dvatsa- JIRA 17683
                                    }
                                    else if (sFormName == "vssassignment")
                                    {
                                        if (sColName.ToUpper() == "ASSIGNMENT_DUE_DATE")
                                         strJsonResponse.Append(Conversion.GetJsonNode("assignment_due_date.DbValue", sValue)); //dvatsa- JIRA 17683

                                    }
                                    else if (sFormName == "casemanagementlist")
                                    {
                                        if (sColName.ToUpper() == "DATE_RECORD_ADDED")
                                           strJsonResponse.Append(Conversion.GetJsonNode("date_record_added.DbValue", sValue)); //dvatsa- JIRA 17683
                                        else if (sColName.ToUpper() == "DATE_RECORD_LAST_UPDATED")
                                            strJsonResponse.Append(Conversion.GetJsonNode("date_record_last_updated.DbValue", sValue)); //dvatsa- JIRA 17683
                                    }
                                    //dvatsa - added for handling sorting in ng-grid -JIRA 11108 (end)

                                    if (sFormName == "adjusterdatedtext" && sColName.ToUpper() == "DATED_TEXT")
                                    {

                                        sValue = RemoveComma(sValue); //dvatsa-added code to handle comma ,refer MITS 15832 
              
                                        strJsonResponse.Append(Conversion.GetJsonNode(sColName.ToLower(), sValue));  //dvatsa- JIRA 17683
                                    }
                                    else if ((sColName.ToUpper() == "FIRST_NAME") || (sColName.ToUpper() == "LAST_NAME")) //MITS 24823 mcapps2 - We do not want to truncate the name, it is used in Funds
                                    {
                                        sValue = RemoveComma(sValue); //dvatsa-added code to handle comma ,refer MITS 15832 
                                     
                                        strJsonResponse.Append(Conversion.GetJsonNode(sColName.ToLower(), sValue)); //dvatsa- JIRA 17683

                                    }
                                    //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                                    else if ((string.Equals(sFormName, "adjuster", StringComparison.OrdinalIgnoreCase) || string.Equals(sFormName, "claimadjuster", StringComparison.OrdinalIgnoreCase)) && string.Equals(sColName.ToUpper(), "ASSIGNMENT_LEVEL", StringComparison.OrdinalIgnoreCase))
                                    {
                                        string strTableName = "ASSIGNMENT_LEVEL";
                                        using (LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId))
                                        {
                                            switch (sValue.ToUpper().Trim())
                                            {
                                                case "CT":      //For Claimant
                                                    sValue = objCache.GetCodeDesc(objCache.GetCodeId("CT", strTableName));
                                                    break;
                                                case "RE":      //For Reserve
                                                    sValue = objCache.GetCodeDesc(objCache.GetCodeId("RE", strTableName));
                                                    objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                                                    if (objSysSettings.MultiCovgPerClm == -1)       //For Carrier Claim
                                                        sValue = string.Concat(objCache.GetCodeDesc(objCache.GetCodeId("CT", strTableName)), "/", sValue);
                                                    break;
                                                case "UN":      //For Unit
                                                    sValue = objCache.GetCodeDesc(objCache.GetCodeId("UN", strTableName));
                                                    break;
                                                default:        //For Claim
                                                    sValue = objCache.GetCodeDesc(objCache.GetCodeId("CL", strTableName));
                                                    break;
                                            }
                                        }

                                        sValue = RemoveComma(sValue); //dvatsa-added code to handle comma ,refer MITS 15832
                                    
                                        strJsonResponse.Append(Conversion.GetJsonNode(sColName.ToLower(), sValue));//dvatsa- JIRA 17683
                                     }
                                    else if ((string.Equals(sFormName, "adjuster", StringComparison.OrdinalIgnoreCase) || string.Equals(sFormName, "claimadjuster", StringComparison.OrdinalIgnoreCase)) && string.Equals(sColName.ToUpper(), "ASSIGNMENT_NAME", StringComparison.OrdinalIgnoreCase)) //dvatsa- JIRA 17683
                                    {
                                        sValue = GetAssignmentName(objReader.GetValue("ASSIGNMENT_LEVEL").ToString(), sValue);

                                        sValue = RemoveComma(sValue);//dvatsa-added code to handle comma ,refer MITS 15832 
                                        
                                        strJsonResponse.Append(Conversion.GetJsonNode(sColName.ToLower(), sValue)); //dvatsa- JIRA 17683

                                    }
                                    //Ankit End
                                    else if (string.Equals(sColName.ToUpper(), "TAX_ID", StringComparison.InvariantCultureIgnoreCase)) //Added by Amitosh for MITS 27378   //dvatsa- JIRA 17683
                                    {
                                        objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                                        if (objSysSettings.MaskSSN)
                                        {
                                            if (string.IsNullOrEmpty(sValue))
                                            {
                                               strJsonResponse.Append(Conversion.GetJsonNode(sColName.ToLower(), "")); //dvatsa- JIRA 17683
                                            }
                                            else
                                            {
                                             
                                                strJsonResponse.Append(Conversion.GetJsonNode(sColName.ToLower(), "###-##-####")); //dvatsa- JIRA 17683
                                            }
                                        }
                                        else
                                            sValue = RemoveComma(sValue); //dvatsa-added code to handle comma ,refer MITS 15832 
                                            strJsonResponse.Append(Conversion.GetJsonNode(sColName.ToLower(), sValue)); //dvatsa- JIRA 17683


                                    }//end Amitosh
                                    //Added:Yukti, DT:05/22/2014, MITS 35772
                                    else if (string.Equals(sFormName, "claimant", StringComparison.InvariantCultureIgnoreCase) && string.Equals(sColName.ToUpper(), "UNIT"))
                                    {
                                        if (sUnitNumber.Contains(","))
                                        {
                                            sValue = RemoveComma(sValue); //dvatsa-added code to handle comma ,refer MITS 15832 
                                            strJsonResponse.Append(Conversion.GetJsonNode(sColName.ToLower(), (sValue + "..."))); //dvatsa- JIRA 17683

                                        }
                                        else
                                        {
                                            sValue = RemoveComma(sValue);//dvatsa-added code to handle comma ,refer MITS 15832 
                                            strJsonResponse.Append(Conversion.GetJsonNode(sColName.ToLower(), sValue));//dvatsa- JIRA 17683
                                        }
                                    }

                                    //JIRA RMA-15935 ajohari2 START
                                    else if (string.Equals(sFormName, "casemanagementlist", StringComparison.InvariantCultureIgnoreCase) && (sColName.ToUpper().Equals("DATE_RECORD_ADDED") || sColName.ToUpper().Equals("DATE_RECORD_LAST_UPDATED")))
                                    {
                                       // objCol.InnerText = Convert.ToDateTime(Conversion.GetDBDateFormat(sValue, "d")).ToShortDateString();
                                        strJsonResponse.Append(Conversion.GetJsonNode(sColName.ToLower(),Convert.ToDateTime(Conversion.GetDBDateFormat(sValue, "d")).ToShortDateString()));
                                    }
                                    //JIRA RMA-15935 ajohari2 START
                                    else
                                    {

                                        if (sValue.Length == 0)
                                            strJsonResponse.Append(Conversion.GetJsonNode(sColName.ToLower(), "")); //dvatsa- JIRA 17683

                                        else if (sValue.Length > 255)
                                        {
                                            sValue = RemoveComma(sValue);//dvatsa-added code to handle comma ,refer MITS 15832 
                                            strJsonResponse.Append(Conversion.GetJsonNode(sColName.ToLower(), sValue.Substring(0, 255))); //dvatsa- JIRA 17683
                                        }
                                        else
                                        {
                                            sValue = RemoveComma(sValue); //dvatsa-added code to handle comma ,refer MITS 15832 
                                            strJsonResponse.Append(Conversion.GetJsonNode(sColName.ToLower(), sValue)); //dvatsa- JIRA 17683
                                        }
                                    }
                                    //MITS 11203 by Gagan : End
                                }
                            }
                        } // end-for
                        sUnitNumber = "";
                        strJsonResponse.Append(" }");

                    }// end-while

                    if (iRowCount != 0)//if no records found
                        strJsonResponse.Append("]");
                    else //empty aray
                        strJsonResponse.Append("[]");
                    objReader.Close();

                    objData.InnerText = strJsonResponse.ToString();
                    objLookUpXmlElement.AppendChild(objData);

                    int iMultiCovgPerClm = m_objDataModelFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm;
                    objSysSetting = new SysParms(m_sConnectionString, m_iClientId);


                    #region Prepare Json for UserPref and add to XMl doc
                    strJsonUserPref = "";
                    strJsonUserPref = GetOrCreateUserPreference(iUserID, p_sPageName, iDSNId, p_sGridId, iMultiCovgPerClm);

                    XmlElement objUserPrefXmlElement = objXmlDoc.CreateElement("UserPref");
                    objUserPrefXmlElement.InnerText = strJsonUserPref;
                    objLookUpXmlElement.AppendChild(objUserPrefXmlElement);


                    #endregion

                    #region Prepare Json string for additional data and add to XMl doc
                    XmlElement objAdditionalDataXmlElement = objXmlDoc.CreateElement("AdditionalData");
                    strJsonResponse = new StringBuilder();
                    strJsonResponse.Append("[{");
                    strJsonResponse.Append(Conversion.GetJsonNode("TotalCount", iRowCount.ToString(), true));
                    // Create hiddeninputs tag
                    string[] arrFpidName = new string[1];
                    if (sFpidName.IndexOf("&") > 0)
                        arrFpidName = sFpidName.Split('&');
                    else
                        arrFpidName[0] = sFpidName;

                    for (int i = 0; i < arrFpidName.Length; i++)
                    {
                        sValue = arrFpidName[i];
                        if (sValue.IndexOf("=") > 0)
                        {

                            objHiddenInputs = objXmlDoc.CreateElement("hiddeninputs");
                            objAdditionalDataXmlElement.AppendChild(objHiddenInputs);
                            strJsonResponse.Append(Conversion.GetJsonNode("name", sValue.Substring(0, sValue.IndexOf("="))));
                            strJsonResponse.Append(Conversion.GetJsonNode("value", sValue.Substring(sValue.IndexOf("=") + 1)));
                        }
                    }
                    //rkotak: 8490  starts
                    objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                    strJsonResponse.Append(Conversion.GetJsonNode("carrierclaims", objSysSettings.MultiCovgPerClm.ToString()));
                    //objData.SetAttribute("carrierclaims", objSysSettings.MultiCovgPerClm.ToString());
                    //rkotak: 8490 ends

                    strJsonResponse.Append("}]");
                    objAdditionalDataXmlElement.InnerText = strJsonResponse.ToString();
                    objLookUpXmlElement.AppendChild(objAdditionalDataXmlElement);
                    #endregion


                }//End if : sSql is not blank

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Lookup.GetLookUpData.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objCol = null;
                objData = null;
                objHead = null;
                objHiddenInputs = null;
                objNode = null;
                //Modified by Shivendu to dispose
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();

                }
                if (m_objDataModelFactory != null)
                {
                    m_objDataModelFactory.UnInitialize();
                    m_objDataModelFactory.Dispose();
                }
                objSysSettings = null;//Deb MITS 25775
            }
            return objXmlDoc;
        }

        //dvatsa- Function to handle comma ,refer MITS 15832 (start)
        public string RemoveComma(string sValue)
        {
            try
            {
                string sTrimmedValue = sValue.Trim();
                if (sTrimmedValue == ",")
                    sValue = "";
                else if (sTrimmedValue != sValue)
                {
                    if (sTrimmedValue.StartsWith(","))
                    {
                        sValue = sTrimmedValue.Substring(1);
                    }
                    else if (sTrimmedValue.EndsWith(","))
                    {
                        sValue = sTrimmedValue.Substring(0, sTrimmedValue.Length - 1);
                    }
                }
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }

            finally
            {

            }
            return sValue;
        }
        //dvatsa- Function to handle comma ,refer MITS 15832 (end)

        private string GetOrCreateUserPreference(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId, int iCarrierClaim)
        {
            string sJsonUserPref = string.Empty;
            bool IsUserPrefExist = false;
            try
            {
                Riskmaster.Common.GridCommonFunctions.GetUserHeaderAndPreference(ref sJsonUserPref, p_iUserID, m_iClientId, p_sPageName, p_iDSNId.ToString(), p_sGridId);
                if (String.IsNullOrEmpty(sJsonUserPref))
                    IsUserPrefExist = false;
                else
                    IsUserPrefExist = true;

                if (!IsUserPrefExist)
                {
                    return CreateUserPreference(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);
                }
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("Lookup.GetOrCreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
            }
            return sJsonUserPref;
        }

        private string CreateUserPreference(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            try
            {
               if (!String.IsNullOrEmpty(p_sGridId))
                {
                    if (p_sGridId == "adjuster")
                        sJsonUserPref = CreateUserPrefForAdjuster(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "adjusterdatedtext")
                        sJsonUserPref = CreateUserPrefForAdjusterDatedText(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "claimant")
                        sJsonUserPref = CreateUserPrefForClaimant(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "defendant")
                        sJsonUserPref = CreateUserPrefForDefendant(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "litigation")
                        sJsonUserPref = CreateUserPrefForLitigation(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "expert")
                        sJsonUserPref = CreateUserPrefForExpert(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "demandoffer")
                        sJsonUserPref = CreateUserPrefForDemandOffer(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "subrogation")
                        sJsonUserPref = CreateUserPrefForSubrogation(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "propertyloss")
                        sJsonUserPref = CreateUserPrefForPropertyLoss(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "unit")
                        sJsonUserPref = CreateUserPrefForUnit(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "liabilityloss")
                        sJsonUserPref = CreateUserPrefForLiabilityLoss(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "arbitration")
                        sJsonUserPref = CreateUserPrefForArbitration(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "otherunitloss")
                        sJsonUserPref = CreateUserPrefForOtherunitLoss(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "casemanagementlist")
                        sJsonUserPref = CreateUserPrefForCaseManagementList(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "casemgrnotes")
                        sJsonUserPref = CreateUserPrefForCasemgrnotes(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "cmxaccommodation")
                        sJsonUserPref = CreateUserPrefForCmxAccommodation(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "eventdatedtext")
                        sJsonUserPref = CreateUserPrefForEventDatedText(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "cmxcmgrhist")
                        sJsonUserPref = CreateUserPrefForCmxcmgrhist(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "claimxpolicy")
                        sJsonUserPref = CreateUserPrefForClaimxPolicy(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "siteloss")
                        sJsonUserPref = CreateUserPrefForSiteLoss(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "medwatchtest")
                        sJsonUserPref = CreateUserPrefForMedwatchTest(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "concomitant")
                        sJsonUserPref = CreateUserPrefForConcomitant(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                    else if (p_sGridId == "vssassignment")
                        sJsonUserPref = CreateUserPrefForVssAssignment(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);

                }
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
            }
            return sJsonUserPref;

        }

        private string CreateUserPrefForAdjuster(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    if (sParentID == String.Empty)
                    {
                        lstColDef.Add(new GridColumnDefHelper("last_name", getMLHeaderText("lblLastName", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("first_name", getMLHeaderText("lblFirstName", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("current_flag", getMLHeaderText("lblCurrent", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("code_desc", getMLHeaderText("lblAdjusterType", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: false));
                        lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        if (iRowCount != 0)
                            lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    }
                    else
                    {
                        lstColDef.Add(new GridColumnDefHelper("last_name", getMLHeaderText("lblLastName", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("first_name", getMLHeaderText("lblFirstName", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("assignment_level", getMLHeaderText("lblAssignmentLevel", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("assignment_name", getMLHeaderText("lblAssignmentName", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("current_flag", getMLHeaderText("lblCurrent", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("code_desc", getMLHeaderText("lblAdjusterType", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: false));
                        lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        if (iRowCount != 0)
                            lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    }
                    oGridPref = new GridPreference();
                    oGridPref.colDef = lstColDef;
                    oGridPref.SortColumn = new string[1] { "pid" }; // should match with sql query order by clause for default sorting
                    oGridPref.SortDirection = new string[1] { "asc" };
                    oGridPref.AdditionalUserPref = dicAdditionalUserPref;
                    sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
                }
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForAdjusterDatedText(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    lstColDef.Add(new GridColumnDefHelper("date_entered", getMLHeaderText("lblDateEntered", sPageID), p_bvisible: true, p_sCellTemplate: strHyperLinkCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("date_entered.DbValue", "DATE_ENTERED.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));//rkotak:custom sorting,need to add sorting function in the arguments for this column because this is the default sorting column as well  
                    lstColDef.Add(new GridColumnDefHelper("text_type", getMLHeaderText("lblTextType", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("dated_text", getMLHeaderText("lblDatedText", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                    lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    if (iRowCount != 0)
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));

                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "date_entered.DbValue" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForClaimant(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    if (sParentID == String.Empty)
                    {
                        lstColDef.Add(new GridColumnDefHelper("last_name", getMLHeaderText("lblLastName", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("first_name", getMLHeaderText("lblFirstName", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("middle_name", getMLHeaderText("lblMiddleName", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("tax_id", getMLHeaderText("lblTaxId", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("addr1", getMLHeaderText("lblAddr1", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("addr2", getMLHeaderText("lblAddr2", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("addr3", getMLHeaderText("lblAddr3", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("addr4", getMLHeaderText("lblAddr4", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("city", getMLHeaderText("lblCity", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("state_id", getMLHeaderText("lblStateId", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("state_name", getMLHeaderText("lblStateName", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("zip_code", getMLHeaderText("lblZip", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("country_id", getMLHeaderText("lblCountryId", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("country_name", getMLHeaderText("lblCountryName", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("primary_clmnt_flag", getMLHeaderText("lblPrimaryClmntFlag", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("status_code", getMLHeaderText("lblStatus", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("pid5", getMLHeaderText("lblpid5", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("pid3", getMLHeaderText("lblpid3", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("pid4", getMLHeaderText("lblpid4", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    }
                    else
                    {
                        lstColDef.Add(new GridColumnDefHelper("last_name", getMLHeaderText("lblLastName", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("first_name", getMLHeaderText("lblFirstName", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("middle_name", getMLHeaderText("lblMiddleName", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("tax_id", getMLHeaderText("lblTaxId", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("addr1", getMLHeaderText("lblAddr1", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("addr2", getMLHeaderText("lblAddr2", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("addr3", getMLHeaderText("lblAddr3", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("addr4", getMLHeaderText("lblAddr4", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("city", getMLHeaderText("lblCity", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("state_id", getMLHeaderText("lblStateId", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("state_name", getMLHeaderText("lblStateName", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("zip_code", getMLHeaderText("lblZip", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("country_id", getMLHeaderText("lblCountryId", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("country_name", getMLHeaderText("lblCountryName", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("unit", getMLHeaderText("lblUnit", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("primary_clmnt_flag", getMLHeaderText("lblPrimaryClmntFlag", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("status_code", getMLHeaderText("lblStatus", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("pid5", getMLHeaderText("lblpid5", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("pid3", getMLHeaderText("lblpid3", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("pid4", getMLHeaderText("lblpid4", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    }
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "last_name" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForDefendant(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    lstColDef.Add(new GridColumnDefHelper("last_name", getMLHeaderText("lblLastName", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("first_name", getMLHeaderText("lblFirstName", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    if (iRowCount != 0)
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "last_name" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForLitigation(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    lstColDef.Add(new GridColumnDefHelper("docket_number", getMLHeaderText("lblDocketNumber", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("attorney_firm", getMLHeaderText("lblAttorneyFirm", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("attorney", getMLHeaderText("lblAttorney", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("suit_date", getMLHeaderText("lblSuitDate", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("suit_date.DbValue", "suit_date.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("court_date", getMLHeaderText("lblCourtDate", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("court_date.DbValue", "court_date.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    if (iRowCount != 0)
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "pid" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForExpert(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    lstColDef.Add(new GridColumnDefHelper("last_name", getMLHeaderText("lblLastName", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("first_name", getMLHeaderText("lblFirstName", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("abbreviation", getMLHeaderText("lblAbbreviation", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    if (iRowCount != 0)
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "pid" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForDemandOffer(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    lstColDef.Add(new GridColumnDefHelper("demand_offer_date", getMLHeaderText("lblDemandOfferDate", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("demand_offer_date.DbValue", "demand_offer_date.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("activity", getMLHeaderText("lblActivity", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("result", getMLHeaderText("lblResult", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("amount", getMLHeaderText("lblAmount", sPageID), p_sCellTemplate: sRightAlignCellTemplate, p_sHeaderCellTemplate: sRightAlignHeaderCellTemplate));
                    lstColDef.Add(new GridColumnDefHelper("decision", getMLHeaderText("lblDecision", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    if (iRowCount != 0)
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "pid" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForSubrogation(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    lstColDef.Add(new GridColumnDefHelper("type", getMLHeaderText("lblType", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("specialist", getMLHeaderText("lblSpecialist", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("status", getMLHeaderText("lblStatus", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("status_date", getMLHeaderText("lblStatusDate", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("status_date.DbValue", "status_date.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    if (iRowCount != 0)
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "pid" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForPropertyLoss(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    if (iCarrierClaim == -1)
                    {
                        lstColDef.Add(new GridColumnDefHelper("pin", getMLHeaderText("lblPin", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("type", getMLHeaderText("lblType", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("estimated_damage", getMLHeaderText("lblEstimatedDamage", sPageID), p_sCellTemplate: sRightAlignCellTemplate, p_sHeaderCellTemplate: sRightAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("owner", getMLHeaderText("lblOwner", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("datereported", getMLHeaderText("lblDateReported", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                        lstColDef.Add(new GridColumnDefHelper("datereported.DbValue", "datereported.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("insured", getMLHeaderText("lblInsured", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("unitno", getMLHeaderText("lblUnitNo", sPageID), p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                        lstColDef.Add(new GridColumnDefHelper("unitno.DbValue", "unitno.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        if (iRowCount != 0)
                            lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    }
                    else
                    {
                        lstColDef.Add(new GridColumnDefHelper("pin", getMLHeaderText("lblPin", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("type", getMLHeaderText("lblType", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("estimated_damage", getMLHeaderText("lblEstimatedDamage", sPageID), p_sCellTemplate: sRightAlignCellTemplate, p_sHeaderCellTemplate: sRightAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("owner", getMLHeaderText("lblOwner", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("datereported", getMLHeaderText("lblDateReported", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                        lstColDef.Add(new GridColumnDefHelper("datereported.DbValue", "datereported.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("insured", getMLHeaderText("lblInsured", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        if (iRowCount != 0)
                            lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    }
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "pid" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForUnit(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();
            string strHyperLinkCellTemplate_Unit = "<div class=\"ngCellTextAlignCenter\" ng-class=\"col.colIndex()\" title=\"{{COL_FIELD}}\" ><a onclick=\"OpenRecord(\'{{row.getProperty(\'pid\')}}\',\'{{row.getProperty(\'pid\')}}\')\" href=\"#\">{{row.getProperty(col.field)}}</a></div>";
            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    if (iCarrierClaim == -1)
                    {
                        //dvatsa- changed column alignment to center- JIRA 17805(start)
                        lstColDef.Add(new GridColumnDefHelper("veh_desc", getMLHeaderText("lblVehicleDesc", sPageID),p_sCellTemplate: strHyperLinkCellTemplate_Unit,p_sHeaderCellTemplate:sCenterAlignHeaderCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("vin", getMLHeaderText("lblVin", sPageID),p_sCellTemplate:sCenterAlignCellTemplate,p_sHeaderCellTemplate:sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("vehicle_model", getMLHeaderText("lblVehicleModel", sPageID),p_sCellTemplate:sCenterAlignCellTemplate,p_sHeaderCellTemplate:sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("vehicle_make", getMLHeaderText("lblVehicleMake", sPageID),p_sCellTemplate:sCenterAlignCellTemplate,p_sHeaderCellTemplate:sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("vehicle_year", getMLHeaderText("lblVehicleYear", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate:sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("insured", getMLHeaderText("lblInsured", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("unitno", getMLHeaderText("lblUnitNo", sPageID),p_sCellTemplate:sCenterAlignCellTemplate,p_sHeaderCellTemplate:sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                        lstColDef.Add(new GridColumnDefHelper("unitno.DbValue", "unitno.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        if (iRowCount != 0)
                            lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));

                    }
                    else
                    {
                        lstColDef.Add(new GridColumnDefHelper("vin", getMLHeaderText("lblVin", sPageID), p_sCellTemplate: strHyperLinkCellTemplate_Unit,p_sHeaderCellTemplate:sCenterAlignHeaderCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("vehicle_model", getMLHeaderText("lblVehicleModel", sPageID),p_sCellTemplate:sCenterAlignCellTemplate,p_sHeaderCellTemplate:sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("vehicle_make", getMLHeaderText("lblVehicleMake", sPageID),p_sCellTemplate:sCenterAlignCellTemplate,p_sHeaderCellTemplate:sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("vehicle_year", getMLHeaderText("lblVehicleYear", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("insured", p_sdisplayName: getMLHeaderText("lblInsured", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                        //dvatsa- changed column alignmentto center- JIRA 17805(end)
                        lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        if (iRowCount != 0)
                            lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    }
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "pid" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForLiabilityLoss(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    lstColDef.Add(new GridColumnDefHelper("liability_type", getMLHeaderText("lblLiabilityType", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("damage_type", getMLHeaderText("lblDamageType", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("party_affected", getMLHeaderText("lblPartyAffected", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    if (iRowCount != 0)
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "pid" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForArbitration(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    lstColDef.Add(new GridColumnDefHelper("type", getMLHeaderText("lblType", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("adverse_party", getMLHeaderText("lblAdverseParty", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("date_filed", getMLHeaderText("lblDateFiled", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("date_filed.DbValue", "date_filed.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("status", getMLHeaderText("lblStatus", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    if (iRowCount != 0)
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "pid" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForOtherunitLoss(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();
            try
            {
                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    if (iCarrierClaim == -1)
                    {
                        lstColDef.Add(new GridColumnDefHelper("last_name", getMLHeaderText("lblLastName", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("reference_number", getMLHeaderText("lblVinAddress", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("abbreviation", getMLHeaderText("lblAbbreviation", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("tax_id", getMLHeaderText("lblTaxId", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("phone_number", getMLHeaderText("lblPhoneNumber", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("unit_type", getMLHeaderText("lblUnitType", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("insured", getMLHeaderText("lblInsured", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("unitno", getMLHeaderText("lblUnitNo", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        if (iRowCount != 0)
                            lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    }
                    else
                    {
                        lstColDef.Add(new GridColumnDefHelper("last_name", getMLHeaderText("lblLastName", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("abbreviation", getMLHeaderText("lblAbbreviation", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("tax_id", getMLHeaderText("lblTaxId", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("phone_number", getMLHeaderText("lblPhoneNumber", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("unit_type", getMLHeaderText("lblUnitType", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("insured", getMLHeaderText("lblInsured", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        if (iRowCount != 0)
                            lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    }
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "pid" };
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForCaseManagementList(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    lstColDef.Add(new GridColumnDefHelper("added_by_user", getMLHeaderText("lblAddedByUser", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("date_record_added", getMLHeaderText("lblDateRcdAdded", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("date_record_added.DbValue", "date_record_added.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("date_record_last_updated", getMLHeaderText("lblDateRcdLastUpd", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("date_record_last_updated.DbValue", "date_record_last_updated.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("claim_number", getMLHeaderText("lblClaimId", sPageID)));
                    //lstColDef.Add(new GridColumnDefHelper("a", getMLHeaderText("lblA", sPageID),p_sCellTemplate:sRightAlignCellTemplate,p_sHeaderCellTemplate:sRightAlignHeaderCellTemplate));
                    lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    if (iRowCount != 0)
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "pid" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForCasemgrnotes(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    lstColDef.Add(new GridColumnDefHelper("entered_by_user", getMLHeaderText("lblEnteredByUser", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("date_entered", getMLHeaderText("lblDateEntered", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("date_entered.DbValue", "date_entered.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("time_entered", getMLHeaderText("lblTimeEntered", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("note_type", getMLHeaderText("lblNoteType", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("case_manager_notes", getMLHeaderText("lblCaseMgrNotes", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                    lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    if (iRowCount != 0)
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "date_entered.DbValue" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForCmxAccommodation(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    lstColDef.Add(new GridColumnDefHelper("accomm_mode", getMLHeaderText("lblAccommMode", sPageID), true, strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("accomm_type", getMLHeaderText("lblAccommType", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("status", getMLHeaderText("lblStatus", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("scope", getMLHeaderText("lblScope", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("accomm_accepted_date", getMLHeaderText("lblAccommAcceptedDate", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("accomm_accepted_date.DbValue", "accomm_accepted_date.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("restriction", getMLHeaderText("lblRestriction", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                    lstColDef.Add(new GridColumnDefHelper("accommodation", getMLHeaderText("lblAccomodation", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                    lstColDef.Add(new GridColumnDefHelper("cost", getMLHeaderText("lblCost", sPageID), p_sCellTemplate: sRightAlignCellTemplate, p_sHeaderCellTemplate: sRightAlignHeaderCellTemplate));
                    lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    if (iRowCount != 0)
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "pid" };
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;
                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForEventDatedText(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    lstColDef.Add(new GridColumnDefHelper("entered_by_user", getMLHeaderText("lblEnteredByUser", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("date_entered", getMLHeaderText("lblDateEntered", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("date_entered.DbValue", "date_entered.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("time_entered", getMLHeaderText("lblTimeEntered", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("dated_text", getMLHeaderText("lblDatedText", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                    lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    if (iRowCount != 0)
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "date_entered.DbValue" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForCmxcmgrhist(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    lstColDef.Add(new GridColumnDefHelper("case_manager", getMLHeaderText("lblCaseManager", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("referral_type", getMLHeaderText("lblReferralType", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("referral_date", getMLHeaderText("lblReferralDate", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("referral_date.DbValue", "referral_date.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("referred_to", getMLHeaderText("lblReferredTo", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("case_status", getMLHeaderText("lblCaseStatus", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("date_closed", getMLHeaderText("lblDateClosed", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("date_closed.DbValue", "date_closed.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("ur_due_date", getMLHeaderText("lblUrDueDate", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("ur_due_date.DbValue", "ur_due_date.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("primary_case_mgr", getMLHeaderText("lblPrimaryCaseMgr", sPageID), p_sCellTemplate: sRightAlignCellTemplate, p_sHeaderCellTemplate: sRightAlignHeaderCellTemplate));
                    lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    if (iRowCount != 0)
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "pid" };
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
                
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForClaimxPolicy(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    lstColDef.Add(new GridColumnDefHelper("policy_number", getMLHeaderText("lblPolicyNumber", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("policy_name", getMLHeaderText("lblPolicyName", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("effective_date", getMLHeaderText("lblEffectiveDate", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("effective_date.DbValue", "effective_date.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("expiration_date", getMLHeaderText("lblExpirationDate", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("expiration_date.DbValue", "expiration_date.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    if (iRowCount != 0)
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "pid" };
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForSiteLoss(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    if (iCarrierClaim == -1)
                    {
                        lstColDef.Add(new GridColumnDefHelper("site_number", getMLHeaderText("lblSiteNumber", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("name", getMLHeaderText("lblName", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("insured", getMLHeaderText("lblInsured", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("unitno", getMLHeaderText("lblUnitNo", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        if (iRowCount != 0)
                            lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    }
                    else
                    {
                        lstColDef.Add(new GridColumnDefHelper("site_number", getMLHeaderText("lblSiteNumber", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                        lstColDef.Add(new GridColumnDefHelper("name", getMLHeaderText("lblName", sPageID)));
                        lstColDef.Add(new GridColumnDefHelper("insured", getMLHeaderText("lblInsured", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate));
                        lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                        if (iRowCount != 0)
                            lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    }
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "pid" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForMedwatchTest(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    lstColDef.Add(new GridColumnDefHelper("lab_test", getMLHeaderText("lblLabTest", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("result", getMLHeaderText("lblResult", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("test_date", getMLHeaderText("lblTestDate", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("test_date.DbValue", "test_date.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    if (iRowCount != 0)
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "pid" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForConcomitant(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    lstColDef.Add(new GridColumnDefHelper("concom_product_id", getMLHeaderText("lblConcomProductId", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_sHeaderCellTemplate: sRightAlignHeaderCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("from_date", getMLHeaderText("lblFromDate", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("from_date.DbValue", "from_date.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("to_date", getMLHeaderText("lblToDate", sPageID), p_sCellTemplate: sCenterAlignCellTemplate, p_sHeaderCellTemplate: sCenterAlignHeaderCellTemplate, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("to_date.DbValue", "to_date.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("concom_product", getMLHeaderText("lblConcomProduct", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    if (iRowCount != 0)
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "pid" };
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }
        private string CreateUserPrefForVssAssignment(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {

                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();
                if (!String.IsNullOrEmpty(p_sGridId))
                {
                    lstColDef.Add(new GridColumnDefHelper("assignment_name", getMLHeaderText("lblAssigmentName", sPageID), p_sCellTemplate: strHyperLinkCellTemplate, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("account_number", getMLHeaderText("lblAccountNumber", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("assignment_due_date", getMLHeaderText("lblAssignmentDueDate", sPageID), p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("assignment_due_date.DbValue", "assignment_due_date.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    lstColDef.Add(new GridColumnDefHelper("pid", getMLHeaderText("lblpid", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                    if (iRowCount != 0)
                        lstColDef.Add(new GridColumnDefHelper("pid2", getMLHeaderText("lblpid2", sPageID), p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));
                }
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "assignment_name" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }

        private string getMLHeaderText(string key, string sPageID)
        {
            string sBaseLangCode = "0";
            string sUserLangCode = "0";
            try
            {
                sUserLangCode = m_iLangCode.ToString();
                if (sUserLangCode == "0")
                {
                    //set base lang code as user lang code if it is not defined
                    sUserLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
                }
                return CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString(key, 0, sPageID, m_iClientId), sUserLangCode);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }

        public string RestoreDefaultUserPref(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = "";

            try
            {
                if (Riskmaster.Common.GridCommonFunctions.DeleteUserPreference(p_iUserID, m_iClientId, p_sPageName, p_iDSNId, p_sGridId))
                    sJsonUserPref = CreateUserPreference(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("LookUp.RestoreDefaultUserPref.Error", m_iClientId), objException);
            }
            finally
            {
            }
            return sJsonUserPref;
        }
        //dvatsa- JIRA 11108 (end)







	}
}
