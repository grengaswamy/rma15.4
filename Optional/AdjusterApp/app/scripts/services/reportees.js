/**
 * Created by nsharma202 on 12/18/2014.
 */
app.factory('Peek',['$http','$q','Auth','BASE_URL',function($http,$q,Auth,BASE_URL){
  'use strict';
  var users=[];
  var diaries=[];
  var loadingUsers=true;;
  var loadingDiaries=true;
  var error=false;

  var Peek= {
    requestUsers: function () {
      return $http({
        cache:true,
        method: 'GET',
        url: Auth.getServer() + BASE_URL + '/GetReportees',
        headers: {'session': Auth.getSession(), 'ClientId': Auth.getClientId()}
      });
    },
    requestDiaries:function(user){
      return $http({
        cache:true,
        method: 'GET',
        url: Auth.getServer() + BASE_URL + '/Tasks',
        headers: {'session': Auth.getSession(), 'ClientId': Auth.getClientId(),'user':user}
      });
    }

  }
  return Peek;
}]);
