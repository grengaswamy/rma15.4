﻿'use strict';
app.factory('Diary',['$http','Auth','BASE_URL',
    function($http,Auth,BASE_URL){
  
    var Diary = {
        requestDiary : function(){
            $http({
                method : 'GET',
                url: Auth.getServer()+ BASE_URL+'/Diary',
                headers : {'session': Auth.getSession(),'ClientId':Auth.getClientId()}

            })
            .success(function(data){
                
            })
            .error(function(error){
                console.log("error requesting codes");
                console.log(error);
            });
        },
        postDiary : function(diary){
           return $http({
                method : 'POST',
                url : Auth.getServer()+BASE_URL +'/Tasks',
                data : diary,
                headers : {'session' : Auth.getSession(), 'user': Auth.getUsername() ,'Content-Type' : 'json','ClientId':Auth.getClientId()}
             });
        }
        
    }
    return Diary;
}]);