﻿'use strict';
app.factory('Claim', ['$http', '$q', 'Auth', 'BASE_URL', 'localStorageService',
  function ($http, $q, Auth, BASE_URL, localStorageService) {
    var claims = [];
    var raw = '';
    var claimAttributes = [];
    var columns = [];
    var error = 'false';
    var errorMessage = '';
    var claimPromise = $q.defer();

    if (!localStorageService.get('sessionId')) {
      localStorageService.set('claims', '');
      localStorageService.set('claims.loading', 'true');
    }
    else {
      localStorageService.get('claims.loading');
    }

    var Claim = {
      reset: function () {
        claims = [];
        raw = '';
        claimAttributes = [];
        columns = [];
        error = 'false';
        errorMessage = '';
        claimPromise = $q.defer();
        localStorageService.set('claims', '');
        localStorageService.set('claims.loading', 'true');
      },
      requestClaims: function () {
        return $http({
          method: 'GET',
          url: Auth.getServer() + BASE_URL + '/Claims',
          headers: {'session': Auth.getSession(), 'ClientId': Auth.getClientId()}

        });
      },
      requestClaimDetails: function (id) {
        return $http({
          method: 'GET',
          url: Auth.getServer() + BASE_URL + '/Claims/' + id,
          headers: {'session': Auth.getSession(), 'ClientId': Auth.getClientId()}
        });

      },
      requestClaimPaymentDetails: function (c) {
        return $http({
          method: 'GET',
          url: Auth.getServer() + BASE_URL + '/Payment/' + c["Claim Number"],
          headers: {'session': Auth.getSession(), 'pid': c["pid"], 'ClientId': Auth.getClientId()}
        });

      },
      loadClaims: function () {
        console.log("Loading claims");
        claims = [];
        Claim.requestClaims()
          .success(function (data) {
            if (data.ResultMessage.MsgStatus.MsgStatusCd == 'Error') {
              console.log("error fetching claims list");
              console.log(data);
              error = 'true';
              errorMessage = data.ResultMessage.MsgStatus.ExtendedStatus;
              localStorageService.set('claims.loading', 'false');
              claimPromise.reject(0);
            }
            else {
              error = 'false';
              console.log("Claims success");
              console.log(data);
              raw = data;
              //getting column heads
              angular.forEach(data.ResultMessage.Document.results.columns.column, function (column) {
                claimAttributes.push(column.text);
                columns.push(column);
              });

              //formatting data

              //console.log(raw.ResultMessage.Document.results.data.row);
              if (raw.ResultMessage.Document.results.totalrows > 0) //RMA-17041, skip if 0 rows
              {
                if (Array.isArray(raw.ResultMessage.Document.results.data.row)) {
                    angular.forEach(raw.ResultMessage.Document.results.data.row, function (row) {
                      console.log(row);
                      var temp = {};
                      angular.forEach(row.field, function (v, j) {
                        temp[claimAttributes[j]] = v;
                      });
                      temp['pid'] = row.pid;
                      claims.push(temp);
                    });
                  }
                  else {
                    var temp = {};
                    angular.forEach(raw.ResultMessage.Document.results.data.row.field, function (v, j) {
                      //temp[eventAttributes[j]] = v; //RMA-17041 - typo error
                      temp[claimAttributes[j]] = v; 
                    });
                    temp['pid'] = raw.ResultMessage.Document.results.data.row.pid;
                    claims.push(temp);
                  }
              }

              localStorageService.set('claims.loading', 'false');
              claimPromise.resolve(claims.length);
              console.log("Formatted clams # " + raw.ResultMessage.Document.results.totalrows);
              console.log(claims);
              localStorageService.set('claims', claims);
            }
          })
          .error(function (data, config, status, headers) {
            console.log("error reaching server");
            console.log(data);
            console.log(config);
            console.log(status);
            error = 'true';
            localStorageService.set('claims.loading', 'false');
            errorMessage = "server not reachable";
            claimPromise.reject(0);
          });

      },
      setClaims: function (data) {
        localStorageService.set('claims', data);
        //claims=data;
      },
      getClaims: function () {
        return localStorageService.get('claims') || [];
      },
      find: function (id) {
        claims = localStorageService.get('claims') || [];
        for (var i = 0; i < claims.length; i++) {
          var claim = claims[i];
          if (claim["Claim Number"] == id) {
            return claim;
          }
        }
        return null;
      },
      findEvent: function (id) {
        var result = [];
        claims = localStorageService.get('claims') || [];
        console.log("looking for claim with event id :" + id);
        for (var i = 0; i < claims.length; i++) {
          var claim = claims[i];
          if (claim["Event Number"] == id) {
            result.push(claim);
          }
        }
        console.log("attached claims");
        console.log(result);
        return result;
      },
      getLoading: function () {
        return localStorageService.get('claims.loading');
      },
      getError: function () {
        return error;
      },
      getErrorMessage: function () {
        return errorMessage;
      },
      getPid: function (id) {
        claims = localStorageService.get('claims') || [];
        for (var i = 0; i < claims.length; i++) {
          var claim = claims[i];
          if (claim["Claim Number"] == id) {
            return claim.pid;
          }
        }
        return null;
      },
      getTotalClaims: function () {
        return claimPromise.promise;
      },
      getTotalClaimsStatic: function () {
        if (localStorageService.get('claims'))
          return (localStorageService.get('claims')).length
        else return 0;
      }
    };
    return Claim;
  }]);
