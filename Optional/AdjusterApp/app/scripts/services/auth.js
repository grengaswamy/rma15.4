/**
 * Created by nsharma202 on 9/1/2014.
 */
'use strict';
app.factory('Auth',['$rootScope','$http','localStorageService','$location','SERVER','CLIENT_ID','BASE_URL',
    function($rootScope,$http,localStorageService,$location,SERVER,CLIENT_ID,BASE_URL){

        var user = { UserName: '', Password: '', NewPassword: '', DsnName : ''};
        var userDSNs='';
        if(localStorageService.get('sessionId')){

        }
        else{
            localStorageService.set('isAuthenticated','false');
            localStorageService.set('sessionId','null');
            localStorageService.set('clientId',CLIENT_ID);
            localStorageService.set('server',SERVER);
            localStorageService.set('baseUrl',BASE_URL);
            localStorageService.set('UserName','');
            localStorageService.set('currentDSN','');
            localStorageService.set('pins','');
        }

        var Auth ={
            reset: function(){
                localStorageService.clearAll();
                localStorageService.set('isAuthenticated','false');
                localStorageService.set('sessionId','null');
                localStorageService.set('clientId',CLIENT_ID);
                localStorageService.set('server',SERVER);
                localStorageService.set('baseUrl',BASE_URL);
                localStorageService.set('UserName','');
                localStorageService.set('currentDSN','');
                localStorageService.set('pins','');
                user = { UserName: '', Password: '', NewPassword: '', DsnName : ''};
                userDSNs='';
            },
            login : function(user){
                return   $http({
                    method: 'POST',
                    url : localStorageService.get('server')+ localStorageService.get('baseUrl')+'/Authenticate',
                    data : user,
                    headers: {'Content-Type': 'json'}
                });
            },
            logout : function(session){
                return $http({
                    method : 'POST',
                    url : localStorageService.get('server')+ localStorageService.get('baseUrl')+'/LogOut',
                    data: session,
                    headers: {'Content-Type': 'raw'}
                });
            },
            requestDSNs : function (user){
                return $http({
                    method : 'POST',
                    url : localStorageService.get('server')+ localStorageService.get('baseUrl')+'/GetUserDSNs',
                    data : user,
                    headers : {'Content-Type':'json'}
                });
            },
            getUserSessionID : function(user){
                return $http({
                    method : 'POST',
                    url : localStorageService.get('server')+ localStorageService.get('baseUrl')+'/GetUserSessionID',
                    data : user,
                    headers : { 'Content-Type':'json'}
                });
            },
            requestName : function(user){
                return $http({
                    method : 'POST',
                    url : localStorageService.get('server')+ localStorageService.get('baseUrl')+'/GetUsername',
                    data : user,
                    headers : { 'Content-Type':'json'}
                });
            },
            getFullName: function(){
                return localStorageService.get('fullName');
            },
            setFullName: function(fullName){
                var firstName=fullName.split(" ");
                localStorageService.set('fullName',firstName[0]);
            },
            getClientId: function(){
                return localStorageService.get('clientId');
            },
            setServer: function(userServer){
                localStorageService.set('server', userServer || localStorageService.get('server'));
            },
            getServer:function(){
                return localStorageService.get('server');
            },
            signedIn : function (){
                return localStorageService.get('isAuthenticated');
            },
            setDSNs : function(dsnList){
                localStorageService.set('userDSNs',JSON.stringify(dsnList));
            },
            getUserDSNs : function(){
                return localStorageService.get('userDSNs');
            },
            setCurrentDSN : function(dsn){
                localStorageService.set('currentDSN',dsn);
            },
            getCurrentDSN : function(){
                return localStorageService.get('currentDSN');
            },
            getSession:function(){
                return localStorageService.get('sessionId');
            },
            setIsAuthenticated: function(value){
                localStorageService.set('isAuthenticated',value);
            },
            setSessionID : function (value) {
                localStorageService.set('sessionId',value);
            },
            sessionReady: function(){
                return localStorageService.get('sessionId')!==null;
            },
            setUser : function(u){
                user.UserName= u.UserName;
                localStorageService.set('UserName',user.UserName);
            },
            getUser : function(){
                return user;
            },
            getUsername: function(){
                return localStorageService.get('UserName');
            },
            changePassword: function(user){
                return   $http({
                    method: 'POST',
                    url : localStorageService.get('server')+ localStorageService.get('baseUrl')+'/ChangePassword',
                    data : user,
                    headers: {'Content-Type': 'raw','session':Auth.getSession()}
                });
            }
        };

        $rootScope.signedIn = function (){
            return Auth.signedIn();
        };
        $rootScope.sessionReady = function(){
            return Auth.sessionReady();
        };
        $rootScope.getUsername=function(){
            return Auth.getUsername;
        };

        $rootScope.$on('$locationChangeStart',function(){
            console.log('doing hiding');
            $('.modal-backdrop').remove();
        });
        return Auth;
    }]);


