'use strict';
app.factory('Code',['$http','Auth','BASE_URL','localStorageService',
  function($http,Auth,BASE_URL,localStorageService){

  var codes=[];
    if(!localStorageService.get('sessionId')){
      localStorageService.set('codes.claimStatus','');
      localStorageService.set('codes.unitTypeCode','');
      localStorageService.set('codes.states','');
      localStorageService.set('codes.noteTypeCode','');
      localStorageService.set('codes.wpaActivities','');
      localStorageService.set('codes.personsInvolvedType','');
      localStorageService.set('codes.entity','');
      localStorageService.set('codes.claimType','');
      localStorageService.set('codes.otherPeople','');
      localStorageService.set('codes.documentType','');
    }


  var Code = {
    reset : function(){
      codes=[];
      localStorageService.set('codes.claimStatus','');
      localStorageService.set('codes.unitTypeCode','');
      localStorageService.set('codes.states','');
      localStorageService.set('codes.noteTypeCode','');
      localStorageService.set('codes.wpaActivities','');
      localStorageService.set('codes.personsInvolvedType','');
      localStorageService.set('codes.entity','');
      localStorageService.set('codes.claimType','');
      localStorageService.set('codes.otherPeople','');
      localStorageService.set('codes.documentType','');
    },
    requestCodes : function(){
      $http({
        method : 'GET',
        url: Auth.getServer()+ BASE_URL+'/Codes',
        headers : {'session': Auth.getSession(),'ClientId':Auth.getClientId()},
        cache : true
      })
        .success(function(data){
          console.log('Codes are here vroooom');
          console.log(data);
          saveCodes(data);
          console.log('code objects');
          console.log(Code.getNoteTypeCode());
          console.log(Code.getWpaActivities());
          console.log(Code.getPersonsInvolvedType());
          console.log(Code.getDocumentType());
        })
        .error(function(error){
          console.log('error requesting codes');
          console.log(error);
        });
    },
    getNoteTypeCode : function () { return localStorageService.get('codes.noteTypeCode');},
    getWpaActivities : function() { return localStorageService.get('codes.wpaActivities');},
    getPersonsInvolvedType : function () { return localStorageService.get('codes.personsInvolvedType');},
    getDocumentType: function () { return localStorageService.get('codes.documentType','');}
  };

    var saveCodes = function(data){
      angular.forEach(data,function(code){
        switch(code.TableName)
        {
          case 'CLAIM_STATUS' : localStorageService.set('codes.claimStatus',code); break;
          case 'UNIT_TYPE_CODE' :localStorageService.set('codes.unitTypeCode',code); break;
          case 'STATES' :localStorageService.set('codes.states',code); break;
          case 'NOTE_TYPE_CODE' :localStorageService.set('codes.noteTypeCode',code); break;
          case 'WPA_ACTIVITIES' :localStorageService.set('codes.wpaActivities',code); break;
          case 'PERSON_INV_TYPE' :localStorageService.set('codes.personsInvolvedType',code); break;
          case 'ENTITY' : localStorageService.set('codes.entity',code); break;
          case 'CLAIM_TYPE' : localStorageService.set('codes.claimType',code); break;
          case 'OTHER_PEOPLE' :localStorageService.set('codes.otherPeople',code); break;
          case 'DOCUMENT_TYPE' : localStorageService.set('codes.documentType',code);break;
        }
      });
    };
  return Code;
}]);

