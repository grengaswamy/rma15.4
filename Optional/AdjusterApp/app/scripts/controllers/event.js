﻿/**
 * Created by nsharma202 on 8/28/2014.
 */

app.controller('EventCtrl',['$scope','$route','$rootScope','$routeParams','$q','$location','Auth','Claim','Event','Task','Code','Diary','Data','$timeout',
    function($scope,$route,$rootScope,$routeParams,$q,$location,Auth,Claim,Event,Task,Code,Diary,Data,$timeout){
        'use strict';

        //Getting event related data
        $scope.event=Event.find($routeParams.eventId);
        $scope.eventNo=$routeParams.eventId;
        console.log('event is');
        console.log($scope.event);
        $scope.claims=Claim.findEvent($scope.eventNo);
        $scope.tasks='';
        $scope.tasks=Task.find($scope.eventNo);

        //This will check for pinned items
        var pin={link: $location.path(), name : $scope.eventNo, info : 'Event'};
        $scope.pinned=Data.checkPin(pin);
        $scope.unPin=function(){
            $scope.toggle=true;
            $timeout(toggleValue,5000,true);
            $scope.pinned=!$scope.pinned;
            Data.removePin(pin);
        };
        $scope.pin=function(){
            $scope.toggle=true;
            $timeout(toggleValue,5000,true);
            $scope.pinned=!$scope.pinned;
            Data.addPin(pin);
        };

        //Swipe left functionality
        $scope.left=function(){
            $location.path('/events');
        };

        $rootScope.sending=false;
        $scope.sent=false;
        //$rootScope.server=false;

        //TASK
        $scope.showMyTask = function(){
            $('#myTasks').modal('show');
            $('.modal-backdrop').removeClass('modal-backdrop');
        };
        $scope.toggle='false';
        var toggleValue=function(){
            $scope.toggle=false;
        };
        $rootScope.taskError='false';
        $scope.taskErrorMessage='';
        $rootScope.taskAdded='false';
        var taskAddedToggle=function(){
            $rootScope.taskAdded=false;
        };
        $scope.taskEntered='';
        $scope.task={ name:'', type: '', dueDate :'', dueTime : '', description : '', recordId:'', table:'', activity:''};
        $scope.taskTypes=Code.getWpaActivities();

        //Check for validation
        $scope.submitTask = function() {

            $scope.task_form.submitted=false;
            console.log($scope.task_form.$valid);
            if ($scope.task_form.$valid) {
                $('#myTasks').modal('hide');
                postTask();
                $scope.task_form.$setPristine();
            } else {
                $scope.task_form.submitted = true;
            }
        };
        //Post a task
        var postTask=function(){
            $rootScope.sending=true;
            $rootScope.taskError='false';
            $rootScope.taskAdded='false';
            $scope.task.recordId=$scope.eventNo;
            $scope.task.table='EVENT';
            $scope.task.type=$scope.taskEntered.Id;
            $scope.task.activity=$scope.taskEntered.Id+' | '+$scope.taskEntered.ShortCode+' '+$scope.taskEntered.Desc;
            console.log($scope.task);
            Diary.postDiary($scope.task)
                .success(function(data,status){
                    $rootScope.sending=false;
                    console.log(data.ResultMessage.MsgStatus.MsgStatusCd);
                    if(data.ResultMessage.MsgStatus.MsgStatusCd=='Error'){
                        $rootScope.taskError='true';
                        $scope.taskErrorMessage=data.ResultMessage.MsgStatus.ExtendedStatus;
                        console.log('error posting task');
                        console.log(data);
                        console.log(status);
                    }
                    else {
                        console.log('task added');
                        $rootScope.taskAdded='true';
                        $timeout(taskAddedToggle,5000,true);
                        console.log(data);
                        var temp={ 'taskSubject':'', 'entryNotes':'','workActivity':'','completeDate':'','completeTime':'', 'attachments':'','linked':'','type':''};
                        temp.taskSubject=$scope.task.name;
                        temp.entryNotes=$scope.task.description;
                        temp.completeDate=$scope.task.dueDate;
                        temp.completeTime=$scope.task.dueTime;
                        temp.workActivity=$scope.task.activity;
                        temp.type='E';
                        temp.attachments='Event '+$scope.eventNo;;
                        temp.linked=$routeParams.eventId;
                        $scope.tasks.push(temp);
                        console.log('attached tasks');
                        console.log($scope.tasks);
                        Task.addTask(temp);
                        $scope.task={ name:'', type: '', dueDate :'', dueTime : '', description : '', recordId:'', table:'', activity:''};
                    }
                })
                .error(function(data,status){
                    console.log('error creating task');
                    $rootScope.sending=false;
                    $scope.message='error creating task';
                    $rootScope.taskError=true;
                    $scope.taskErrorMessage='error creating task';
                    console.log(data);
                    console.log(status);
                })

        };

        //Setting geoCodes for googleMaps
        var tempAddress='';
        if($scope.event){
            tempAddress=$scope.event['Loc.: Address 1']+' '+$scope.event['Loc.: Address 2']+' '+$scope.event['Loc.: City']+' '+$scope.event['Loc.: State']+' '+$scope.event['Loc.: Country'];
        }
        console.log(tempAddress);
        $scope.geoCodes='';
        if(typeof(google) == 'undefined'){
            //do nothing we dont have google maps api loaded
        }
        else {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode( { 'address': tempAddress}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log(results[0]);
                    $scope.geoCodes=results[0].geometry.location;

                } else {
                    console.log('Geocode was not successful for the following reason: ' + status);
                    $scope.geoCodes='';
                }
            });
        }
        //geocode ends

    }]);
