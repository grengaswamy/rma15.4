/**
 * Created by nsharma202 on 12/19/2014.
 */
app.controller('PeekDiariesCtrl',['$scope','$routeParams','$location','Peek',function($scope,$routeParams,$location,Peek){
  'use strict';
  $scope.loading=true;
  $scope.error=false;
  $scope.diaries=[];
  $scope.user=$routeParams.user;
  Peek.requestDiaries($routeParams.user)
    .success(function (data) {
      $scope.loading=false;
      if(data.Diaries.length===undefined){
        console.log("error fetching tasks list");
        console.log(data);
        $scope.error=true;
      }
      else{
        $scope.error='false';
        console.log("dairies peeked");
        console.log(data);
        $scope.diaries=data.Diaries;
      }
    })
    .error(function(){
      $scope.loading=false;
      $scope.error=true;
    })
  $scope.left=function(){
    $location.path('/peekUsers');
  };
}]);

