
app.controller('ClaimCtrl',['$scope','$routeParams','$q','$location','Auth','Claim','Event','Task','Code','Note','Diary','Attachment','Data','$timeout','$filter','$rootScope',
    function($scope,$routeParams,$q,$location,Auth,Claim,Event,Task,Code,Note,Diary,Attachment,Data,$timeout,$filter,$rootScope){
    'use strict';
    $scope.claim=Claim.find($routeParams.claimId);
    $scope.claimNo=$routeParams.claimId;
    $scope.pid=Claim.getPid($scope.claimNo);
    console.log($scope.claim);
    $scope.error='false';
    $scope.errorMessage='';
    $scope.status='';
    $scope.message='';
    $scope.loading='true';
    $scope.adjusterInformation=[];
    $scope.claimantsInformation='';
    $scope.claimInfo='';
    $scope.tasks='';
    $scope.eventDetail='';
    $scope.paymentHistory='';
    $scope.payments=[];
    $scope.personsInvolved=[];
    $scope.policyManagementInformation='';
    $scope.personsInvolvedTypeSelected='';
    $scope.notes=[];
    $scope.employeeInformation='';
    $rootScope.sending=false;
    $scope.sent=false;
    $scope.emptyCase=false;
    var raw='';
    $scope.debug=false;
    $scope.activeDiary=false;

    //Pins
    var pin={link: $location.path(), name : $scope.claimNo, info :'Claim'};
    $scope.pinned=Data.checkPin(pin);
    $scope.unPin=function(){
        $scope.toggle=true;
        $timeout(toggleValue,3000,true);
            $scope.pinned=!$scope.pinned;
            Data.removePin(pin);
    };
    $scope.pin=function(){
        $scope.toggle=true;
        $timeout(toggleValue,3000,true);
            console.log(pin);
            $scope.pinned=!$scope.pinned;
            Data.addPin(pin);
    };

    $scope.toggle='false';
    var toggleValue=function(){
            $scope.toggle=false;
    };

    //ng-touch
    $scope.left=function(){
        $location.path('/claims');
    };
    //Request Claim details
    Claim.requestClaimDetails($scope.claimNo)
            .success(function(data){
                $scope.status=data.ResultMessage.MsgStatus.MsgStatusCd;
                if(data.ResultMessage.MsgStatus.MsgStatusCd=='Error'){
                    $scope.error=true;
                    $scope.errorMessage=data.ResultMessage.MsgStatus.ExtendedStatus;
                    console.log('Error loading claim executive summary');
                    console.log(data);
                    $scope.loading='false';
                }
                else{

                    $scope.claim=data;
                    console.log('Claim object');
                    console.log(data);
                    raw=data.ResultMessage.Document.ExecutiveSummaryReport.File.ClaimSummary;

                    if(raw.hasOwnProperty('AdjusterInformation')){
                        var adjusters=raw.AdjusterInformation.Adjusters;
                        if(adjusters.Adjuster.length===undefined){
                            //single adjuster
                            $scope.adjusterInformation.push(adjusters.Adjuster);
                        }
                        else{
                            //multiple adjusters
                            angular.forEach(adjusters.Adjuster, function(adjuster){
                                $scope.adjusterInformation.push(adjuster);
                            });
                        }
                    }

                    if(raw.hasOwnProperty('ClaimantsInformation')){
                        $scope.claimantsInformation=raw.ClaimantsInformation;
                    }
                    if(raw.hasOwnProperty('ClaimInfo')){
                        $scope.claimInfo=raw.ClaimInfo;
                    }
                    if(raw.hasOwnProperty('EmployeeInformation')){
                        $scope.employeeInformation=raw.EmployeeInformation;
                    }
                    if(raw.hasOwnProperty('EventDetail')){
                        $scope.eventDetail=raw.EventDetail;
                    }
                    if(raw.hasOwnProperty('PaymentHistory')){
                        $scope.paymentHistory=raw.PaymentHistory;
                        if(Array.isArray(raw.PaymentHistory.Payment)){
                            angular.forEach(raw.PaymentHistory.Payment,function(pay){
                                $scope.payments.push(pay);
                            });
                        }
                        else{
                            $scope.payments.push(raw.PaymentHistory.Payment);
                        }
                    }
                    if(raw.hasOwnProperty('PolicyManagementInformation')){
                        $scope.policyManagementInformation=raw.PolicyManagementInformation;
                    }
                    if(raw.hasOwnProperty('PersonsInvolved')){
                        var temp=raw.PersonsInvolved;
                        //Sorting PersonsInvolved
                        angular.forEach(temp,function(person){
                            for(var key in person){
                                if(Array.isArray(person[key])){
                                    angular.forEach(person[key],function(p){
                                        $scope.personsInvolved.push(p);
                                    })
                                }
                                else{
                                    $scope.personsInvolved.push(person[key]);
                                }
                                break;
                            }
                        });
                    }

                    if($scope.claimInfo.hasOwnProperty('EnhancedNotes')){
                        if(Array.isArray($scope.claimInfo.EnhancedNotes.Note)){
                            angular.forEach($scope.claimInfo.EnhancedNotes.Note,function(note){
                                $scope.notes.push(note);
                            });
                        }
                        else{
                            $scope.notes.push($scope.claimInfo.EnhancedNotes.Note);
                        }
                    }

                    $scope.tasks=Task.find($scope.claimNo);
                    $scope.emptyCase=!!(($scope.claimantsInformation.length===0) &&  $.isEmptyObject($scope.employeeInformation));
                    $scope.loading='false';
                }

            })
            .error(function(error){
                $scope.error=true;
                $scope.errorMessage=error;
                $scope.serverError='Server not reachable';
                $scope.loading='false';
                $scope.status=error.status;
                console.log('Error loading claim executive summary');
                console.log(error);
            });

    //Payment Filter
        $scope.paymentFilterType='';
        $scope.paymentFilter=function(p){
            console.log('payment filter '+p);
            $scope.paymentFilterType=p;
        };
    //PersonsInvolved Filter
        $scope.personsInvolvedTypes=Code.getPersonsInvolvedType();
        $scope.personFilter=function(p){
        };

    //Add Notes
    $scope.showMyNote = function(){
        $('#myNotes').modal('show');
        $('.modal-backdrop').removeClass('modal-backdrop');
    };
    $rootScope.Error=false;
    $scope.noteErrorMessage='';
        var toggleNoteAdded=function(){
          $rootScope.noteAdded=false;
        };
        var toggleNoteError=function(){
            $rootScope.Error=false;
        };

    $scope.note = {
            'AdjusterName': '',
            'AttachedRecordId': '0',
            'AttachedTable': '',
            'AttachedTo': '',
            'bIsMobileAdjuster': '',
            'bIsMobilityAdjuster': '',
            'ClaimID': '',
            'ClaimNumber': '',
            'ClaimProgressNoteId': '0',
            'CodeId': '',
            'DateCreated': '',
            'DateEntered': '',
            'DateEnteredForSort': '',
            'EnteredBy': '',
            'EnteredByName': '',
            'EventID': '',
            'EventNumber': '',
            'IsNoteEditable': '',
            'NewRecord': '',
            'NoteMemo': '',
            'NoteMemoCareTech': '',
            'NoteMemoTrimmed': '',
            'NoteTypeCode': '',
            'NoteTypeCodeId': '',
            'PIName': '',
            'PolicyID': '',
            'PolicyName': '',
            'PolicyNumber': '',
            'Subject': '',
            'SubjectTrimmed': '',
            'TemplateID': '0',
            'TemplateName': '',
            'TimeCreated': '',
            'TimeCreatedForSort': '',
            'UserTypeCode': '',
            'UserTypeCodeId': ''
        };
    $scope.note.DateEntered=$filter('date')(new Date(), 'dd/MM/yyyy');
    $scope.currentDate=$filter('date')(new Date(), 'dd/MM/yyyy');
    //console.log("current date : "+$scope.currentDate);

    $scope.note.NoteMemo=(new Date()).toUTCString();
//    $scope.note= {claimNumber:'', activityDate:'', noteText:'', noteTypeCode:''};
    $scope.noteTypes= Code.getNoteTypeCode();
        $scope.submitNote = function() {
            $scope.note_form.submitted=false;
            console.log($scope.note_form.$valid);
            if ($scope.note_form.$valid) {
                $('#myNotes').modal('hide');
                postNote();
                $scope.note_form.$setPristine();
            } else {
                $scope.note_form.submitted = true;
            }
        };

    var postNote =function(){
        $rootScope.sending=true;
        $rootScope.Error=false;
        $rootScope.noteAdded=false;

        //adding note fields
        $scope.note.ClaimNumber=$routeParams.claimId;
        $scope.note.NoteTypeCode=$scope.note.NoteTypeCode.Id;
        $scope.note.ClaimID='-1';
        $scope.note.EventID='0';
        $scope.note.NewRecord='true';
        $scope.note.NoteMemoCareTech=$scope.note.NoteMemo;
        $scope.note.TemplateID='0';
        $scope.note.bIsMobileAdjuster=true;
        $scope.note.bIsMobilityAdjuster=true;


        console.log('going to post note');
        Note.postNote($scope.note)
        .success(function(data, status){
                $rootScope.sending=false;
                console.log(data);
                if(data.ClaimProgressNoteId>0){
                    console.log('Note added!');
                    $rootScope.noteAdded=true;
                    $timeout(toggleNoteAdded,3000,true);
                    $scope.note.DateCreated=data.DateCreated;
                    $scope.note.TimeCreated=data.TimeCreated;
                    $scope.note.NoteText=data.NoteMemo;
                    $scope.note.NoteType=getNoteType(data.NoteTypeCode);
                    $scope.notes.push($scope.note);
                    $scope.note = {
                        'AdjusterName': '',
                        'AttachedRecordId': '0',
                        'AttachedTable': '',
                        'AttachedTo': '',
                        'bIsMobileAdjuster': '',
                        'bIsMobilityAdjuster': '',
                        'ClaimID': '',
                        'ClaimNumber': '',
                        'ClaimProgressNoteId': '0',
                        'CodeId': '',
                        'DateCreated': '',
                        'DateEntered': '',
                        'DateEnteredForSort': '',
                        'EnteredBy': '',
                        'EnteredByName': '',
                        'EventID': '',
                        'EventNumber': '',
                        'IsNoteEditable': '',
                        'NewRecord': '',
                        'NoteMemo': '',
                        'NoteMemoCareTech': '',
                        'NoteMemoTrimmed': '',
                        'NoteTypeCode': '',
                        'NoteTypeCodeId': '',
                        'PIName': '',
                        'PolicyID': '',
                        'PolicyName': '',
                        'PolicyNumber': '',
                        'Subject': '',
                        'SubjectTrimmed': '',
                        'TemplateID': '0',
                        'TemplateName': '',
                        'TimeCreated': '',
                        'TimeCreatedForSort': '',
                        'UserTypeCode': '',
                        'UserTypeCodeId': ''
                    };

                }
                else {
                    $rootScope.Error=true;
                    $timeout(toggleNoteError,3000,true);
                    $scope.noteErrorMessage=data.ResultMessage.MsgStatus.ExtendedStatus;
                    console.log('Error posting note');
                    console.log(data);
                    console.log(status);

                }
        })
        .error(function(data, status, headers, config) {
                $rootScope.sending=false;
                $rootScope.Error=true;
                $timeout(toggleNoteError,3000,true);
                $scope.noteErrorMessage='Server not reachable';
                console.log(data);
                console.log(status);
                console.log(config);
        });
    };

    var getNoteType= function(codeId){
      var result='';
      angular.forEach($scope.noteTypes.Codes, function (code){
        if(code.Id==codeId){
          console.log('note short & desc'+code.ShortCode +' '+ code.Desc);
          result=code.ShortCode +' '+ code.Desc;
          return result;
        }
      });
      return result;
    };
    //Note Ends

    //Add Task
    $scope.showMyTask = function(){
        $('#myTasks').modal('show');
        $('.modal-backdrop').removeClass('modal-backdrop');
    };
    $rootScope.taskError='false';
    $scope.taskErrorMessage='';
    $rootScope.taskAdded='false';
        var taskAddedToggle=function(){
            $rootScope.taskAdded=false;
        };
    $scope.taskEntered='';
    $scope.task={ name:'', type: '', dueDate :'', dueTime : '', description : '', recordId:'', table:'', activity:''};
    $scope.taskTypes=Code.getWpaActivities();
        $scope.submitTask = function() {
            $scope.task_form.submitted=false;
            console.log($scope.task_form.$valid);
            if ($scope.task_form.$valid) {
                $('#myTasks').modal('hide');
                postTask();
                $scope.task_form.$setPristine();
            } else {
                $scope.task_form.submitted = true;
            }
        }
    var postTask=function(){
        $rootScope.sending=true;
        $rootScope.taskError='false';
        $rootScope.taskAdded='false';

        $scope.task.recordId=$routeParams.claimId;
        $scope.task.table='CLAIM';
        //console.log($scope.taskEntered);
        $scope.task.type=$scope.taskEntered.Id;
        $scope.task.activity=$scope.taskEntered.Id+' | '+$scope.taskEntered.ShortCode+' '+$scope.taskEntered.Desc;
        console.log($scope.task);
        Diary.postDiary($scope.task)
        .success(function(data,status, headers, config){
                $rootScope.sending=false;
                if(data.ResultMessage.MsgStatus.MsgStatusCd=='Error'){
                    $rootScope.taskError=true;
                    $scope.taskErrorMessage=data.ResultMessage.MsgStatus.ExtendedStatus;
                    console.log('Error posting task');
                    console.log(data);
                    console.log(status);
                }
                else {
                    console.log('Task added');
                    $rootScope.taskAdded=true;
                    $timeout(taskAddedToggle,5000,true);
                    console.log(data);
                    var temp={ 'taskSubject':'', 'entryNotes':'','workActivity':'','completeDate':'','completeTime':'', 'attachments':'','linked':'','type':''};
                    temp.taskSubject=$scope.task.name;
                    temp.entryNotes=$scope.task.description;
                    temp.completeDate=$scope.task.dueDate;
                    temp.completeTime=$scope.task.dueTime;
                    temp.workActivity=$scope.task.activity;
                    temp.type='C';
                    temp.attachments=$routeParams.claimId;
                    temp.linked=$routeParams.claimId;
                    $scope.tasks.push(temp);
                    console.log($scope.tasks)
                    Task.addTask(temp);
                    $scope.task={ name:'', type: '', dueDate :'', dueTime : '', description : '', recordId:'', table:'', activity:''};
                }
            })
        .error(function(data){
            console.log('Error creating task');
            $scope.message='error creating task';
            $rootScope.sending=false;
            $rootScope.taskError=true;
            $scope.taskErrorMessage='server not reachable';
            console.log(data);
            })

    }
    //Task Ends

    $scope.deviceError=false;
    $scope.deviceErrorMessage='';
        var toggleDeviceError=function(){
            $scope.deviceError=false;
        };

    //camera starts
        var rawImage='';
        $scope.click=function(){
            console.log('launching camera');
            navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
                destinationType: Camera.DestinationType.DATA_URL
            });
        };
        $scope.gallery=function(){
            console.log('opening gallery');
            navigator.camera.getPicture(onSuccess,onFail,{ quality : 75,
                destinationType : Camera.DestinationType.DATA_URL,
                sourceType : Camera.PictureSourceType.PHOTOLIBRARY
            });
        };
        var onSuccess=function(imageData) {
            //var image = document.getElementById('myImage');
            //image.src = 'data:image/jpeg;base64,' + imageData;
            $('#myCamera').modal('show');
            $('.modal-backdrop').removeClass('modal-backdrop');
            rawImage=imageData;
        };
        var onFail=function(message) {
            $scope.deviceError=true;
            $scope.deviceErrorMessage=message;
            $timeout(toggleDeviceError,2000,true);
        };
    //camera ends

    //audio recording starts
        var audio='';
        $scope.recordAudio=function(){
                  navigator.device.capture.captureAudio(captureSuccess, captureError, {limit:2});
        };
        var captureSuccess = function(mediaFiles) {
            //var i, path, len;
            //for (i = 0, len = mediaFiles.length; i < len; i += 1) {
            //    path = mediaFiles[i].fullPath;
            //    var audioFile=document.getElementById('myAudioClip');
            //    audioFile.src=path;
            //}
            audio=mediaFiles[0];
            $('#myAudio').modal('show');
            $('.modal-backdrop').removeClass('modal-backdrop');
        };
        var captureError = function(error) {
            $scope.deviceError=true;
            $scope.deviceErrorMessage=error;
            $timeout(toggleDeviceError,2000,true);
        };
    //audio recording ends

    //video recording starts
        var video='';
        $scope.recordVideo=function(){
            navigator.device.capture.captureVideo(captureSuccessVideo, captureError, {limit:1});
        }
        var captureSuccessVideo = function(mediaFiles) {
            //var i, path, len;
            //for (i = 0, len = mediaFiles.length; i < len; i += 1) {
            //    path = mediaFiles[i].fullPath;
            //    var videoFile=document.getElementById('myVideoClip');
            //    videoFile.src=path;
            //}
            video=mediaFiles[0];
            $('#myVideo').modal('show');
            $('.modal-backdrop').removeClass('modal-backdrop');
        };
    //video recording ends

    //Attachments
    $rootScope.attachError=false;
    $rootScope.attachSuccess=false;
        var toggleAttachSuccess=function(){
            $rootScope.attachSuccess=false;
        };
        var toggleAttachError=function(){
            $rootScope.attachError=false;
        };
    $scope.uploading='false';
        var attachmentModal= { FileName:'', Title:'', Subject:'', Notes:'', base64EncodedFileContents:'', Claimnumber:'', typeId:''};
        $scope.attachment= { fileName:'', title:'', subject:'', notes:'', base64EncodedFileContents:'', Claimnumber:'', typeId:'', type:''};
        $scope.attach = function(type) {
            $scope.attachment_form.submitted=false;
            console.log($scope.attachment_form.$valid);
            if ($scope.attachment_form.$valid) {
                postAttachment(type);
                $scope.attachment_form.$setPristine();
            } else {
                $scope.attachment_form.submitted = true;
            }
        };
      var configure=function(){
        attachmentModal.FileName=$scope.attachment.fileName;
        attachmentModal.Title=$scope.attachment.title;
        attachmentModal.Subject=$scope.attachment.subject;
        attachmentModal.Notes=$scope.attachment.notes;
        attachmentModal.base64EncodedFileContents=$scope.attachment.base64EncodedFileContents;
        attachmentModal.Claimnumber=$scope.attachment.Claimnumber;
        attachmentModal.typeId=$scope.attachment.typeId;
      };
    var postAttachment=function(type){
        $rootScope.sending=true;
        $rootScope.attachError=false;
        $rootScope.attachSuccess=false;
        $scope.uploading=true;
        $scope.attachment.fileName=(new Date).toISOString().split(':').join('-');

        if(type=='image'){
            $scope.attachment.base64EncodedFileContents=rawImage;
            $scope.attachment.fileName+='.jpeg';
            if(getTypeIDForAttachment('P')){
                $scope.attachment.typeId=getTypeIDForAttachment('P');
            }
            console.log('type id for P : '+$scope.attachment.typeId);
            $('#myCamera').modal('hide');
        }
        else if(type=='audio'){
            Attachment.readFile(audio.fullPath)
                .then(function(content){
                    $scope.attachment.base64EncodedFileContents=content;
                });
            $scope.attachment.fileName+='.'+audio.fullPath.split('.').pop();
            if(getTypeIDForAttachment('S')){
                $scope.attachment.typeId=getTypeIDForAttachment('S');
            }
            $('#myAudio').modal('hide');
        }
        else{
            Attachment.readFile(video.fullPath)
                .then(function(content){
                    $scope.attachment.base64EncodedFileContents=content;
                });
            $scope.attachment.fileName+='.'+video.fullPath.split('.').pop();
            if(getTypeIDForAttachment('V')){
                $scope.attachment.typeId=getTypeIDForAttachment('V');
            }
            $('#myVideo').modal('hide');
        }

        $scope.attachment.title=$scope.claimNo+' attachment';
        $scope.attachment.Claimnumber=$routeParams.claimId;

        //correcting naming conventions as required by service
        configure();
        Attachment.requestClaimAttachment(attachmentModal)
                .success(function(result){
                    console.log(result);
                    $scope.uploading=false;
                    $rootScope.sending=false;
                    if(result){
                        console.log('uploaded');
                        $rootScope.attachSuccess=true;
                        clear(type);
                        $timeout(toggleAttachSuccess,2000,true);
                    }
                    else{
                        $scope.message='OOps Something went wrong :D';
                        $rootScope.attachError=true;
                        $timeout(toggleAttachError,2000,true);
                    }
                })
                .error(function(data, status, headers, config) {
                    console.log('data');
                    console.log(data);
                    console.log('status');
                    console.log(status);
                    console.log('headers');
                    console.log(headers);
                    console.log('config');
                    console.log(config);
                    $scope.uploading=false;
                    $rootScope.sending=false;
                    $rootScope.attachError=true;
                    $timeout(toggleAttachError,2000,true);
                })
    };

    var getTypeIDForAttachment=function(type){
        var id=null;
        var documentTypes=Code.getDocumentType();
        angular.forEach(documentTypes.Codes,function(doc){
           if(doc.ShortCode==type){
               id=doc.Id;
           }
        });
        return id;
    };
    var clear=function(type){
        if(type=='image'){
            rawImage='';
            var image = document.getElementById('myImage');
            image.src = '';
        }
        else if(type=='audio'){
            audio='';
            var audioFile=document.getElementById('myAudioClip');
            audioFile.src='';
        }
        else if(type=='video'){
            video='';
            var videoFile=document.getElementById('myVideoClip');
            videoFile.src='';
        }
    };
    }]);
