app.controller('HeaderCtrl', ['$scope', '$rootScope', '$location', 'Auth',
  function ($scope, $rootScope, $location, Auth) {
    'use strict';
    $rootScope.goHome = function () {
      if (Auth.getSession()) {
        $location.path('/home');
      }
      console.log("go home called");
    };
    $(function () {
      $('.toggle-nav').click(function () {
        $('body').toggleClass('show-nav');
        return false;
      });
    });
    $(function () {
      $('.view').click(function () {
        $('body').removeClass('show-nav');
      });
    });
  }]);

