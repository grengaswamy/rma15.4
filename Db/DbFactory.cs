//Author: BSB 03.22.2004
//BSB Define a Macro to control whether debug strings are not evaluated for every db call.
#define DO_DBG

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.Common;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;





namespace Riskmaster.Db
{
	 enum DbTrace:int
	{
		None=0,
		Connect=1,
		Execute=2,
		//Additional = 4,
		ConnectionOpenerStack=128
	};

	/// <summary>
	/// Factory is the base gatekeeper to the Riskmaster Database layer object instances.
	/// It is intended to be an "interception" point.  
	/// For example: when a request for a Riskmaster.DbConnection object type comes in, the static methods of 
	/// this class may either simply create and return the object OR they may use configuration information 
	/// and hand back fully configured derived object that is actually a remote proxy.
	/// To this end, most Db object methods are also marked as virtual.
	///  Implementation of the actual derived remotable object versions will be a phase 2 item.
	/// </summary>
	public partial class DbFactory
	{
		#region Instrumentation Logic
        //akaur9 Mobile Adjuster Set the Protect Pool to true to allow GC to run. 
		//private static bool m_bProtectPool = false;
        private static bool m_bProtectPool = true; //mbahl3 Mit:-22878


		/// <summary>
		/// Gets the Connection Pool
		/// </summary>
		public static bool ProtectPool{get{return m_bProtectPool;}}

		private static int m_TraceDetail = 0;
		/// <summary>
		/// Gets the Trace Detail
		/// </summary>
		public static int TraceDetail{get{return m_TraceDetail;}}

		private static string m_sTracePath = string.Empty;
		/// <summary>
		/// Gets the Trace Path
		/// </summary>
		public static string TracePath{get{return m_sTracePath;}}

		//private static NameValueCollection m_ContextSettings;//=null;
		//internal static NameValueCollection  ConfigSettings
		//{
		//    get
		//    {
		//        if( m_ContextSettings !=null )
		//            return m_ContextSettings;
		//        else
		//        {
		//            m_ContextSettings = RMConfigurator.NameValueCollection("DbContextSettings");
		//            return m_ContextSettings;
		//        }
		//    }
		//}
//		static internal void Dbg(string s)
//		{
//			if(m_TraceDetail>0)
//			{
//				m_stm.WriteLine(s);
//				System.Diagnostics.Trace.WriteLine(s);
//			}
//		}
		//BSB Conditional Attribute usage wipes this function definition AND
		// the calling code (including param evals) from source if the DO_DBG token is not defined.
		//[System.Diagnostics.Conditional("DO_DBG")]
		static internal void Dbg(string s,DbTrace traceDetailLevel)
		{
			 if(IsDbgLevel((int)traceDetailLevel))
			 {
				 //Migrate this code to the Enterprise Library logging component
			 }
		}
		static internal bool IsDbgLevel(int traceDetailLevel)
		{
			return ((traceDetailLevel & m_TraceDetail)!=0);
		}
		#endregion
		//**************************************************************************************************
		//DbConnection.
		//DbConnection has the ability to create and hand out all of the other DbXXX types.
		// In an inherited class, these methods will need to be re-implemented to hand out further proxy type objects.
		//**************************************************************************************************
		
		 /// <summary>
		 /// GetDbConnection returns a DbConnection object pointing to the connection string passed in the connectionString parameter..
		 /// </summary>
		 /// <param name="connectionString">The connection string to use for this conneciton.</param>
		 public static DbConnection GetDbConnection(string connectionString)
		{
			return new DbConnection(connectionString); //Local
			// return GetDbConnection_Remote(connectionString)
			// return GetDbConnection_Encrypted(connectionString)

		}
		
		 /// <summary>
		 /// GetDbConnection clones an existing connection object.  This is usefull for providers such as SQL Server 
		 /// where only  a single query may be executed at a time on the connection.  This function can be used to easily create 
		 /// a second connection in order to run some secondary query while still looking at connected results from the first query.
		 /// </summary>
		 /// <param name="srcConnection">The existing connection object from which to make a copy.</param>
		 public static DbConnection GetDbConnection(DbConnection srcConnection)
		{
			return new DbConnection(srcConnection); //Local
			// return GetDbConnection_Remote(srcConnection)
			// return GetDbConnection_Encrypted(srcConnection)
		}

		//**************************************************************************************************
		//DbReader.
		//DbReader is the connected recordset of ADO.Net 
		//**************************************************************************************************
		
		 /// <summary>
		 /// GetDbReader used only inside this class  to create a new DbReader based on the Native Reader and a DbCommand.
		 /// </summary>
		 /// <param name="anyReader">A native reader to wrap.</param>
		 /// <param name="anyCmd">A command to run which generates the results for the reader.</param>
		 internal static DbReader GetDbReader(IDataReader anyReader,DbCommand anyCmd)
		{
			return new DbReader(anyReader,anyCmd); //Local
			// return GetDbReader_Remote(anyReader,anyCmd);
			// return GetDbReader_Encrypted(anyReader,anyCmd);
		}
		
		//Handy flat helper functions.
		//Get a Reader w/o worrying about managing your connection object.
		
		 /// <summary>
		 /// GetDbReader is used to get a connected recordset (reader) in a single call.  The connection used will be closed 
		 /// automatically when the returned reader is closed.
		 /// </summary>
		 /// <param name="connectionString">The connection information used when creating the database connection internally.</param>
		 /// <param name="SQL">The query who's results should be placed into the reader.</param>
		 /// <param name="behaviorFlags">ADO.Net specific flags for the behavior of the reader.</param>
		 /// <param name="optLock">Riskmaster specific enumeration denoting the type of locking to be used.
		 ///  Currently only supports optimistic locking based on a date-time stamp field in the database.  If this
		 ///  option is used, then the stamp field MUST be included as a returned column in the SQL query.</param>
		 public static DbReader GetDbReader(string connectionString, string SQL, CommandBehavior behaviorFlags, eLockType optLock)
		{
			return GetDbReader_Local(connectionString, SQL,behaviorFlags, optLock); //Local
			// return GetDbReader_Remote(connectionString, SQL,behaviorFlags);
			// return GetDbReader_Encrypted(connectionString, SQL,behaviorFlags);
		}
		
		/// <summary>
		/// GetDbReader is used to get a connected recordset (reader) in a single call.  The connection used will be closed 
		/// automatically when the returned reader is closed.
		/// </summary>
		/// <param name="connectionString">The connection information used when creating the database connection internally.</param>
		/// <param name="SQL">The query who's results should be placed into the reader.</param>
		/// <param name="behaviorFlags">ADO.Net specific flags for the behavior of the reader.</param>
		public static DbReader GetDbReader(string connectionString, string SQL, CommandBehavior behaviorFlags)
			{
			return GetDbReader_Local(connectionString, SQL,behaviorFlags, eLockType.None); //Local
			// return GetDbReader_Remote(connectionString, SQL,behaviorFlags);
			// return GetDbReader_Encrypted(connectionString, SQL,behaviorFlags);
		}
		
		
		 /// <summary>
		 /// GetDbReader_Local is the internal implementation supporting the GetDbReader overloads.
		 /// </summary>
		/// <param name="connectionString">The connection information used when creating the database connection internally.</param>
		/// <param name="SQL">The query who's results should be placed into the reader.</param>
		/// <param name="behaviorFlags">ADO.Net specific flags for the behavior of the reader.</param>
		/// <param name="optLock">Riskmaster specific enumeration denoting the type of locking to be used.
		///  Currently only supports optimistic locking based on a date-time stamp field in the database.  If this
		///  option is used, then the stamp field MUST be included as a returned column in the SQL query.</param>
		private static DbReader GetDbReader_Local(string connectionString, string SQL, CommandBehavior behaviorFlags, eLockType optLock)
		{
			DbConnection conn = GetDbConnection(connectionString);
			conn.Open();
			//BSB CommandBehavior.CloseConnection is still leaving open connections
			// when the reader is not explicitly closed even though it has gone out of scope
			// in code.  To handle this situation, let's pass an "exclusive" flag to the 
			// ctor of our command object allowing it's dispose method to KNOW that it has 
			// exclusive control over this particular connection.
			// We can assume this because the connection and commmand are never exposed 
			// from the DbReader. 
			DbCommand cmd = conn.CreateExclusiveCommand();
			cmd.CommandText = SQL;
			return cmd.ExecuteReader(behaviorFlags | CommandBehavior.CloseConnection, optLock);
		}

		
		/// <summary>
		/// GetDbReader is used to get a connected recordset (reader) in a single call.  The connection used will be closed 
		/// automatically when the returned reader is closed.
		/// </summary>
		/// <param name="connectionString">The connection information used when creating the database connection internally.</param>
		/// <param name="SQL">The query who's results should be placed into the reader.</param>
		public static DbReader GetDbReader(string connectionString, string SQL)
		{
			return DbFactory.ExecuteReader(connectionString, SQL);
		}
		
		/// <summary>
		/// GetDbReader is used to get a connected recordset (reader) in a single call.  The connection used will be closed 
		/// automatically when the returned reader is closed.
		/// </summary>
		/// <param name="connectionString">The connection information used when creating the database connection internally.</param>
		/// <param name="SQL">The query who's results should be placed into the reader.</param>
		/// <param name="optLock">Riskmaster specific enum value denoting the type of locking to be used.
		///  Currently only supports optimistic locking based on a date-time stamp field in the database.  If this
		///  option is used, then the stamp field MUST be included as a returned column in the SQL query.</param>
		public static DbReader GetDbReader(string connectionString, string SQL, eLockType optLock)
		{
			return DbFactory.GetDbReader(connectionString,SQL, CommandBehavior.CloseConnection, optLock);
		}

		//**************************************************************************************************
		//DbWriter.
		//DbWriter is the closest DbXXX object to the old Rocket behavior for updating based on an open recordset.
		// Note: the DbWriter also has the ability to write to the Database without requiring initialization from a Reader.
		// Note: YOU must get your DbWriter from a connection object if you want to use the old optimistic locking mechanism.
		//  This is based on the DTTM_... fields in the database.
		//**************************************************************************************************
		
		/// <summary>GetDbWriter returns a "blank" writer object.  A reader is not required to create a writer although it is simpler. 
		/// Ad-hoc fields and values can be added along with a where clause specified directly on the writer.</summary>
		/// <param name="connectionString">The connection information used when creating the database connection internally.</param>
		/// <returns>A "blank" writer object</returns>
		/// <remarks>DbWriter is the closest DbXXX object to the old Rocket behavior for updating based on an open recordset.
		/// Note: the DbWriter also has the ability to write to the Database without requiring initialization from a Reader.
		/// Note: YOU must get your DbWriter from a connection object if you want to use the old optimistic locking mechanism.
		/// This is based on the DTTM_... fields in the database.</remarks>
		public static  DbWriter GetDbWriter(string connectionString)
		{
			return new DbWriter(connectionString);
			// return GetDbReader_Remote(connectionString);
			// return GetDbReader_Encrypted(connectionString);

		}

		/// <summary>Internal constructor Riskmaster.Db.DbWriter.DbWriter provided for use by DbFactory.
		/// This overload allows a DbConnection to be passed in for transaction support. Locking is assumed to be
		/// on RMOptimistic using this constructor since the locking information is also passed in.  </summary>
		/// <param name="objConn">DbConnection presumably with the transaction context desired for this DbWriter if any.</param>
		/// <param name="timeStamp">String specifies the timeStamp to be applied to the record at DbWriter.Execute()</param>
		/// <param name="userStamp">String specifies the user Stamp to be applied to the record at DbWriter.Execute()</param>
		/// <param name="sourceTimeStamp">String specifies the record Stamp to be checked for changes during DbWriter.Execute()</param>
		/// <remarks>Used only when RMOptimistic locking is desired.</remarks>
		public static DbWriter GetDbWriter(DbConnection objConn, string timeStamp, string userStamp, string sourceTimeStamp )
		{
			return new DbWriter(objConn, timeStamp,userStamp,sourceTimeStamp);
		}

		/// <summary>Internal constructor Riskmaster.Db.DbWriter.DbWriter provided for use by DbFactory.
		/// This overload allows a DbConnection to be passed in for transaction support. Locking is assumed to be
		/// off using this constructor since the Lock information is not provided.  </summary>
		/// <param name="objConn">Connection on which to execute the writer commands.</param>
		/// <remarks>Used only when No locking is desired.</remarks>
		public static  DbWriter GetDbWriter(DbConnection objConn)
		{
			return new DbWriter(objConn);
		}

		/// <summary>GetDbWriter returns a writer object that contains the schema and optionally data values gathered from the reader parameter..</summary>
		/// <returns>A DbWriter object</returns>
		/// <remarks>DbWriter is the closest DbXXX object to the old Rocket behavior for updating based on an open recordset.
		/// Note: the DbWriter also has the ability to write to the Database without requiring initialization from a Reader.
		/// Note: YOU must get your DbWriter from a connection object if you want to use the old optimistic locking mechanism.
		/// This is based on the DTTM_... fields in the database.</remarks>
		/// <param name="reader">A populated DbReader from which the DbWriter will gather schema information for the update\add logic.</param>
		 /// <param name="initializeValues">If true, the Writer will populate fields with initial values from the current row of the DbReader.</param>
		 public static  DbWriter GetDbWriter(DbReader reader, bool initializeValues)
		{
			return new DbWriter(reader, initializeValues);
			// return GetDbWriter_Remote(DbReader reader, bool initializeValues);
			// return GetDbWriter_Encrypted(DbReader reader, bool initializeValues);
		}


		//**************************************************************************************************
		//DbDataAdapter.
		//DbDataAdapter is the ADO.Net feed object for creating a disconnected recordset called a DataSet.
		//**************************************************************************************************
		
		 /// <summary>
		 /// Returns a DbDataAdapter ready to run the "fill" command against the database connection
		 /// passed in and using the specified SQL statement.  Also used internally by GetDataSet() 
		 /// DataAdapter is the ADO.Net "feed" object for creating a disconnected recordset called a DataSet.
		 /// </summary>
		 /// <param name="conn">The connection object from which the adapter is to retrieve data.</param>
		 /// <param name="SQL">The select statement specifying what data to retrieve.</param>
		 public static DbDataAdapter GetDataAdapter(DbConnection conn, string SQL)
		{
			return GetDataAdapter_Local(conn, SQL);
			// return GetDataAdapter_Remote(DbConnection conn, string SQL);
			// return GetDataAdapter_Encrypted(DbConnection conn, string SQL);
		}



         //mbahl3 jira RMA-8486
         /// <summary>
         /// Scripting assembly overload for GetDataAdapter
         /// </summary>     
         public static DbDataAdapter GetDataAdapter(DbCommand cmd)
         {
             if (CheckCallingStack("Riskmaster.Scripting"))
             {
                 return GetDataAdapter( cmd,0);
             }
             else
                 throw new ConfigurationErrorsException(string.Format("GetDataAdapter -The current function overload is available to Scripting assembly (or RMXCustomScripts) only"));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
         }
         //mbahl3 jira RMA-8486

		/// <summary>
		/// Returns a DbDataAdapter ready to run the "fill" command against the database connection
		/// passed in and using the specified SQL statement.  
		/// </summary>
		/// <param name="cmd">The fully configured command object from which the adapter is to retrieve data.</param>
		/// <remarks>The cmd object supplied must exist on a valid connection and have it's
		/// CommandText already set.</remarks>
		public static DbDataAdapter GetDataAdapter(DbCommand cmd,int p_iClientId)
		{
            return GetDataAdapter_Local(cmd, p_iClientId);
			// return GetDataAdapter_Remote(cmd);
			// return GetDataAdapter_Encrypted(cmd);
		}
		/// <summary>
		/// Implementation for a local data layer instance of GetDataAdapter()
		/// </summary>
		/// <param name="dbCmd">The command object from which the adapter is to retrieve data.</param>
		private static DbDataAdapter GetDataAdapter_Local(DbCommand dbCmd,int iClientId)
		{
			IDbDataAdapter anyAdapter= null;
            //Deb ML Changes
            string sError = GlobalResource.GetResource("GetDataAdaptor_Local.Exception.InvalidArgument",iClientId);
			if(dbCmd==null)
				//throw new Riskmaster.ExceptionTypes.RMAppException(Common.Globalization.GetString(""));
                throw new Riskmaster.ExceptionTypes.RMAppException(sError);
			
			if (dbCmd.Connection.State != ConnectionState.Open)
				dbCmd.Connection.Open();

			if (dbCmd.Connection.State != ConnectionState.Open)
				//throw new Riskmaster.ExceptionTypes.RMAppException(Common.Globalization.GetString("GetDataAdaptor_Local.Exception.InvalidArgument"));
                throw new Riskmaster.ExceptionTypes.RMAppException(sError);
			
			if (String.IsNullOrEmpty(dbCmd.CommandText))
				//throw new Riskmaster.ExceptionTypes.RMAppException(Common.Globalization.GetString("GetDataAdaptor_Local.Exception.InvalidArgument"));
                throw new Riskmaster.ExceptionTypes.RMAppException(sError);
            //Deb ML Changes
			//Get the Appropriate Adapter
			switch ( dbCmd.Connection.ConnectionType)
			{
				case eConnectionType.ManagedSqlServer:
					anyAdapter = new  SqlDataAdapter(dbCmd.AsNative() as SqlCommand);
					break;
				case eConnectionType.ManagedOracleServer:
					anyAdapter = new  OracleDataAdapter(dbCmd.AsNative() as OracleCommand);
					break;
				case eConnectionType.Odbc:
					anyAdapter = new  OdbcDataAdapter(dbCmd.AsNative() as OdbcCommand);
					break;
				default:
					throw new Exception("Unknown data provider.  Could not create DataAdapter");
			}

			if (anyAdapter !=null)
				return new DbDataAdapter(anyAdapter);
			else
				return null;
		}

		/// <summary>
		/// Implementation for a local data layer instance of GetDataAdapter()
		/// </summary>
		/// <param name="conn">The connection object from which the adapter is to retrieve data.</param>
		/// <param name="selectSQL">The select statement specifying what data to retrieve.</param>
		private static DbDataAdapter GetDataAdapter_Local(DbConnection conn, string selectSQL)
		{
			DbCommand dbCmd ;
			IDbDataAdapter anyAdapter= null;;

			if (conn.State != ConnectionState.Open)
				conn.Open();

			dbCmd =conn.CreateCommand();
			dbCmd.CommandText = selectSQL;

			//Get the Appropriate Adapter
			switch ( conn.ConnectionType)
			{
				case eConnectionType.ManagedSqlServer:
					anyAdapter = new  SqlDataAdapter(dbCmd.AsNative() as SqlCommand);
					break;
				case eConnectionType.ManagedOracleServer:
					anyAdapter = new  OracleDataAdapter(dbCmd.AsNative() as OracleCommand);
					break;
				case eConnectionType.Odbc:
					anyAdapter = new  OdbcDataAdapter(dbCmd.AsNative() as OdbcCommand);
					break;
				default:
					throw new Exception("Unknown data provider.  Could not create DataAdapter");
			}

			if (anyAdapter !=null)
				return new DbDataAdapter(anyAdapter);
			else
				return null;
		}

	
		//**************************************************************************************************
		//DataSet.
		//DataSet is the ADO.Net disconnected recordset.
		//Get a populated DataSet w/o worrying about managing your connection
		//**************************************************************************************************
        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetDataSet
        /// </summary>     
        public static DataSet GetDataSet(string connectionString, string SQL)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                return  GetDataSet( connectionString,SQL,0);
            }
            else
                throw new ConfigurationErrorsException(string.Format("GetDataSet -The current function overload is available to Scripting assembly (or RMXCustomScripts) only"));
           // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
				 /// <summary>GetDataSet connects to the database specified by connectionString, 
		 /// and gathers the results of the SQL statement into the ADO.Net 
		 /// disconnected recordset called a DataSet.</summary>
		 /// <param name="connectionString">The connection information for the database from which to fetch the information.</param>
		 /// <param name="SQL">The SQL statement for which the DataSet shall contain results. </param>
        public static DataSet GetDataSet(string connectionString, string SQL, int p_iClientId)
		{
			Dictionary<string, string> dictParams = new Dictionary<string, string>();
            return ExecuteDataSet(connectionString, SQL, dictParams, p_iClientId);
		}

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetDataSet
        /// </summary>     
        public static DataSet GetDataSet(DbConnection conn, string SQL)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetDataSet(conn, SQL, 0);
            }
            else
                throw new ConfigurationErrorsException(string.Format("GetDataSet -The current function overload is available to Scripting assembly (or RMXCustomScripts) only"));
             // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
        /// <summary>GetDataSet connects to the database specified by connection, 
        /// and gathers the results of the SQL statement into the ADO.Net 
        /// disconnected recordset called a DataSet.</summary>
        /// <param name="connectionString">The connection information for the database from which to fetch the information.</param>
        /// <param name="SQL">The SQL statement for which the DataSet shall contain results. </param>
        /// Start - MITS# 36997 GAP 06 - agupta298 - GAP 6 - RMA 5497
        public static DataSet GetDataSet(DbConnection conn, string SQL, int p_iClientId)
        {
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            return ExecuteDataSet(conn, SQL, dictParams, p_iClientId);
		}
        //End - MITS# 36997 GAP 06 - agupta298 - GAP 6 - RMA 5497

		/// <summary>
		/// Returns the type of Database based on the connection string
		/// </summary>
		/// <param name="connectionString">string containing the database connection information </param>
		/// <returns>enumeration containing the appropriate database type</returns>
		/// <remarks>This method is used to supplant the previous method requiring an open database
		/// connection to determine the database type</remarks>
		 public static eDatabaseType GetDatabaseType(string connectionString)
		 {
			 eDatabaseType eDbType;

			 using (DbConnection dbConn = new DbConnection(connectionString))
			 {
				 eDbType = dbConn.DatabaseType;
			 }//using

			 return eDbType;
		}//method: GetDatabaseType

		/// <summary>
		/// Gets whether or not the specified database connection string
		/// references an Oracle database
		/// </summary>
		/// <param name="connectionString">string containing the database connection information </param>
		/// <returns>boolean indicating whether or not the specified target database is an Oracle database</returns>
		/// <remarks>Currently, the only supported database platforms are Oracle and SQL Server.  Therefore, if the database
		/// has been determined to not be an Oracle database, it automatically implies that the platform is SQL Server.</remarks>
		 public static bool IsOracleDatabase(string connectionString)
		 {
			 bool blnIsOracleDb = false;

			 if (GetDatabaseType(connectionString) == eDatabaseType.DBMS_IS_ORACLE)
			 {
				 blnIsOracleDb = true;
			 }//if

			 return blnIsOracleDb;
		 }//method: IsOracleDatabase

         /// <summary>
         // This function verifies whether assembly name (passed as argument) was called or not, after ProcessRequest() funtion of RiskmasterService assembly
         // For example, When argument passed is "Scripting", then function verifies whether Custom Script is calling assembly or not.
         /// <summary>
        //mbahl3
         public static bool CheckCallingStack(string Key)
         {
             StackTrace stackTrace = new StackTrace();
             StackFrame[] frames = stackTrace.GetFrames();
             string sServiceAssembly = "RiskmasterService";

             var script = frames.FirstOrDefault(s => s.GetMethod().DeclaringType.Assembly.ToString().Contains(Key) || s.GetMethod().DeclaringType.Assembly.ToString().Contains(sServiceAssembly));
             return script.GetMethod().DeclaringType.Assembly.ToString().Contains(Key);
             //mbahl3 used linq instead of foreach

         //    foreach (var stackFrame in frames)
         //    {
         //        string ownerAssembly = stackFrame.GetMethod().DeclaringType.Assembly.ToString();
         //        if (ownerAssembly.Contains(Key))
         //            return true;
         //        else if (ownerAssembly.Contains(sServiceAssembly))
         //            return false;
         //    }
         //    return false;
         }

         //mbahl3 scripts work jira RMA-8486

		/// <summary>Internal Flag for "Pending GC Request".
		/// This flag can be set when the system suspects db connections are leaking.  The 
		/// full GC will be performed the next time a connection open is requested and the flag reset.</summary>
		/// <remarks>Set by DbConnection finalizer when cleaning up a "leaked" connection.  
		/// This will cause other leaked connections to be reclaimed.</remarks>
		private static bool m_PendingGCRequestFlag;// = false; 
		internal static void SetPendingGCRequestFlag(){m_PendingGCRequestFlag = true;}
		internal static void ClearPendingGCRequestFlag(){m_PendingGCRequestFlag = false;}
		internal static bool PendingGCRequestFlag{get{return m_PendingGCRequestFlag;}}
	}

	#region Extension methods for DbFactory
	/// <summary>
	/// Extension methods that conform to the methods that are provided
	/// by the Microsoft Enterprise Library Data Application Block
	/// </summary>
	public partial class DbFactory
	{
		

		#region ExecuteReader methods
		/// <summary>
		/// Creates a DbReader object based on a SQL statement to be executed
		/// </summary>
		/// <param name="connectionString">string containing the specified database connection string</param>
		/// <param name="strSQLStmt">string containing the SQL statement to be executed</param>
		/// <returns>DbReader containing with an open connection handle to the database</returns>
		/// <remarks>NOTE: Since this method returns a DbReader, the DbReader should be promptly closed after completion of the operation.
		/// Perhaps, one of the best ways to do this is to enclose the returned DbReader within a using block
		/// </remarks>
		/// <exception cref="DataException">DataException that occurs with database connection problems etc.</exception>
		public static DbReader ExecuteReader(string connectionString, string strSQLStmt)
		{
			Dictionary<string, string> strDictParams = new Dictionary<string, string>();

			return ExecuteReader(connectionString, strSQLStmt, strDictParams);

		} // method: ExecuteReader

		/// <summary>
		/// Creates a DbReader object based on a specified of string parameters to a SQL query
		/// </summary>
		/// <param name="connectionString">string containing the specified database connection string</param>
		/// <param name="strSQLStmt">string containing the SQL statement to be executed containing parameter placeholders</param>
		/// <param name="objDictParams">StringDictionary containing parameters to pass to the database
		/// with the parameters SQL query</param>
		/// <returns>DbReader containing with an open connection handle to the database</returns>
		/// <remarks>NOTE: Since this method returns a DbReader, the DbReader should be promptly closed after completion of the operation.
		/// Perhaps, one of the best ways to do this is to enclose the returned DbReader within a using block
		/// </remarks>
		/// <example>
		/// <code>
		/// Sample SQL statement with parameters: SELECT MY_COLUMN FROM MY_TABLE WHERE MY_OTHER_COLUMN=~MYPARAMETER~
		/// Sample StringDictionary object: 
		/// Dictionary&lt;string, string&gt; myStrDictionary = new Dictionary&lt;string, string&gt;();
		/// myStrDictionary.Add("myKey", "myValue");
		/// myStrDictionary.Add("myKey2", "myValue2");
		/// DbReader dbRdr = DbFactory.ExecuteReader(myConnString, mySQLStmt, myStrDictionary);
		/// </code>
		/// </example>
		/// <exception cref="DataException">DataException that occurs with database connection problems etc.</exception>
		public static DbReader ExecuteReader<TKey, TValue>(string connectionString, string strSQLStmt, Dictionary<TKey, TValue> objDictParams)
		{
			Db.DbConnection dbConn = null;

			try
			{
				dbConn = DbFactory.GetDbConnection(connectionString);

				//Open the database connection
				dbConn.Open();

				DbCommand dbCmd = dbConn.CreateCommand();

				foreach (TKey dictKey in objDictParams.Keys)
				{
					DbParameter dbParam = dbCmd.CreateParameter();
					dbParam.ParameterName = dictKey.ToString();
					dbParam.Value = objDictParams[dictKey];

					dbCmd.Parameters.Add(dbParam);
				} // foreach

				dbCmd.CommandText = strSQLStmt;

				return dbCmd.ExecuteReader(CommandBehavior.CloseConnection);
			}
			catch (System.Data.DataException ex)
			{

				throw ex;
			}//catch
		} // method: ExecuteReader 

		#endregion

		#region Execute Scalar methods
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TKey"></typeparam>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="connectionString"></param>
		/// <param name="strSQLStmt"></param>
		/// <param name="objDictParams"></param>
		/// <param name="cmdType"></param>
		/// <returns></returns>
		internal static object ExecuteScalar<TKey, TValue>(string connectionString, string strSQLStmt, Dictionary<TKey, TValue> objDictParams, CommandType cmdType)
		{
			object objScalarValue = null;

			using (Db.DbConnection dbConn = DbFactory.GetDbConnection(connectionString))
			{
				//Open the database connection
				dbConn.Open();

				DbCommand dbCmd = dbConn.CreateCommand();

				foreach (TKey dictKey in objDictParams.Keys)
				{
					DbParameter dbParam = dbCmd.CreateParameter();
					dbParam.ParameterName = dictKey.ToString();
					dbParam.Value = objDictParams[dictKey];

					dbCmd.Parameters.Add(dbParam);
				} // foreach

				dbCmd.CommandType = cmdType;
				dbCmd.CommandText = strSQLStmt;

				objScalarValue = dbCmd.ExecuteScalar();
			} // using
			return objScalarValue;
		}//method: ExecuteScalar()


		/// <summary>
		/// Returns a scalar object value from the database by specifyinig a set of string parameters to a SQL query
		/// </summary>
		/// <param name="connectionString">string containing the specified database connection string</param>
		/// <param name="strSQLStmt">string containing the SQL statement to be executed containing parameter placeholders</param>
		/// <param name="objDictParams">StringDictionary containing parameters to pass to the database
		/// with the parameters SQL query</param>
		/// <returns>object containing scalar value returned from the database query</returns>
		/// <example>
		/// <code>
		/// Sample SQL statement with parameters: SELECT MY_COLUMN FROM MY_TABLE WHERE MY_OTHER_COLUMN=~MYPARAMETER~
		/// Sample StringDictionary object: 
		/// Dictionary&lt;string, string&gt; myStrDictionary = new Dictionary&lt;string, string&gt;();
		/// myStrDictionary.Add("myKey", "myValue");
		/// myStrDictionary.Add("myKey2", "myValue2");
		/// object myObject = DbFactory.ExecuteScalar(myConnString, mySQLStmt, myStrDictionary);
		/// </code>
		/// </example>
		/// <remarks>This method returns an object result which requires explicit casting to its 
		/// appropriate data type.  However, there are several things to keep in mind before performing
		/// an explicit cast.
		/// 1.  If a SQL statement does not return any records, the object value will be null.
		/// Therefore, performing a check of (objValue != null) would be appropriate.
		/// 2.  If the SQL statement result is a column value with a NULL value, the object value will actually be DBNull.Value
		/// Therefore, performing a check of (objValue != DBNull.Value) would be appropriate
		/// </remarks>
		/// <seealso cref="ExecuteReader"/>
		public static object ExecuteScalar<TKey, TValue>(string connectionString, string strSQLStmt, Dictionary<TKey, TValue> objDictParams)
		{
			return ExecuteScalar(connectionString, strSQLStmt, objDictParams, CommandType.Text);
		} // method: ExecuteScalar

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TKey"></typeparam>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="connectionString"></param>
		/// <param name="strSQLStmt"></param>
		/// <param name="objDictParams"></param>
		/// <param name="strCmdType"></param>
		/// <returns></returns>
		public static object ExecuteScalar<TKey, TValue>(string connectionString, string strSQLStmt, Dictionary<TKey, TValue> objDictParams, string strCmdType)
		{
			switch (strCmdType)
			{
				case "Text":
					return ExecuteScalar(connectionString, strSQLStmt, objDictParams, CommandType.Text);        
				case "StoredProcedure":
					return ExecuteScalar(connectionString, strSQLStmt, objDictParams, CommandType.StoredProcedure);
				case "TableDirect":
					return ExecuteScalar(connectionString, strSQLStmt, objDictParams, CommandType.TableDirect);
				default:
					return ExecuteScalar(connectionString, strSQLStmt, objDictParams, CommandType.Text);        
			}//switch
			
		} // method: ExecuteScalar

		/// <summary>
		/// Returns a scalar object value from the database through a simple select-based SQL query
		/// </summary>
		/// <param name="connectionString">string containing the specified database connection string</param>
		/// <param name="strSQLStmt">string containing the SQL statement to be executed containing parameter placeholders</param>
		/// <returns>object containing scalar value returned from the database query</returns>
		/// <remarks>This method returns an object result which requires explicit casting to its 
		/// appropriate data type.  However, there are several things to keep in mind before performing
		/// an explicit cast.
		/// 1.  If a SQL statement does not return any records, the object value will be null.
		/// Therefore, performing a check of (objValue != null) would be appropriate.
		/// 2.  If the SQL statement result is a column value with a NULL value, the object value will actually be DBNull.Value
		/// Therefore, performing a check of (objValue != DBNull.Value) would be appropriate
		/// </remarks>
		/// <seealso cref="ExecuteReader"/>
		public static object ExecuteScalar(string connectionString, string strSQLStmt)
		{
			Dictionary<string, string> objDictParams = new Dictionary<string, string>();

			return ExecuteScalar(connectionString, strSQLStmt, objDictParams);
		} // method: ExecuteScalar 

		/// <summary>
		/// Provides a single unified method to execute database queries against a wide variety
		/// of database return types through the use of Generics
		/// </summary>
		/// <typeparam name="T">Generic type parameter indicating the
		/// required output type</typeparam>
		/// <param name="connectionString">string containing the specified database connection string</param>
		/// <param name="strSQLStmt">string containing the SQL statement to be executed</param>
		/// <returns>Generic type which has been cast to its appropriate data type</returns>
		public static T ExecuteAsType<T>(string connectionString, string strSQLStmt) where T : IConvertible
		{
			//Get the type of the Generic type
			Type typToConvert = typeof(T);

			using (DbConnection dbConn = new DbConnection(connectionString))
			{
				//Open the database connectionString
				dbConn.Open();

				//Get the Generic Type name to determine which cast operation to perform
				switch (typToConvert.Name)
				{
					case "Int32":
						return (T)Convert.ChangeType(dbConn.ExecuteInt(strSQLStmt), typeof(T));
					case "String":
						return (T)Convert.ChangeType(dbConn.ExecuteString(strSQLStmt), typeof(T));
					case "Double":
						return (T)Convert.ChangeType(dbConn.ExecuteDouble(strSQLStmt), typeof(T));
					default:
						return (T)Convert.ChangeType(dbConn.ExecuteString(strSQLStmt), typeof(T));
				}//switch 
			}//using
			
		}//method: ExecuteAsType
		#endregion



        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for ExecuteDataSet
        /// </summary>     
        public static DataSet ExecuteDataSet<TKey, TValue>(string connectionString, string strSQLStmt, Dictionary<TKey, TValue> objDictParams)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                return  ExecuteDataSet<TKey, TValue>(connectionString,strSQLStmt, objDictParams,0);
            }
            else
                throw new ConfigurationErrorsException(string.Format("ExecuteDataSet -The current function overload is available to Scripting assembly (or RMXCustomScripts) only"));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
		/// <summary>
		/// Returns a DataSet from the database by specifyinig a set of string parameters to a SQL query
		/// </summary>
		/// <param name="connectionString">string containing the specified database connection string</param>
		/// <param name="strSQLStmt">string containing the SQL statement to be executed containing parameter placeholders</param>
		/// <param name="objDictParams">StringDictionary containing parameters to pass to the database
		/// with the parameters SQL query</param>
		/// <returns>DataSet containing the result set from the database</returns>
		/// <example>
		/// <code>
		/// Sample SQL statement with parameters: SELECT MY_COLUMN FROM MY_TABLE WHERE MY_OTHER_COLUMN=~MYPARAMETER~
		/// Sample StringDictionary object: 
		/// Dictionary&lt;string, string&gt; myStrDictionary = new Dictionary&lt;string, string&gt;();
		/// myStrDictionary.Add("myKey", "myValue");
		/// myStrDictionary.Add("myKey2", "myValue2");
		/// object myObject = DbFactory.ExecuteDataSet(myConnString, mySQLStmt, myStrDictionary);
		/// </code>
		/// </example>
		/// <remarks>This method is particularly useful when doing DataBinding to the UI through use
		/// of an ObjectDataSource or similar DataSource control</remarks>
		/// <seealso cref="ExecuteReader"/>
        public static DataSet ExecuteDataSet<TKey, TValue>(string connectionString, string strSQLStmt, Dictionary<TKey, TValue> objDictParams, int p_iClientId)
		{
			DataSet dstResults = new DataSet();


			using (Db.DbConnection dbConn = DbFactory.GetDbConnection(connectionString))
			{
				//Open the database connection
				dbConn.Open();

				DbCommand dbCmd = dbConn.CreateCommand();

				foreach (TKey dictKey in objDictParams.Keys)
				{
					DbParameter dbParam = dbCmd.CreateParameter();
					dbParam.ParameterName = dictKey.ToString();
					dbParam.Value = objDictParams[dictKey];

					dbCmd.Parameters.Add(dbParam);
				} // foreach

                //aanandpraka2:Start changes for MITS 22728
                if (dbConn.DatabaseType == Db.eDatabaseType.DBMS_IS_ORACLE)
                {
                    ((Oracle.DataAccess.Client.OracleCommand)(dbCmd.m_anyCommand)).BindByName = true;
                }
                //aanandpraka2:End changes for MITS 22728
				dbCmd.CommandText = strSQLStmt;

				//Get the DataAdapter
				DbDataAdapter dbAdapter = GetDataAdapter(dbCmd, p_iClientId);

				//Populate the contents of the DataSet from the DataAdapter
				dbAdapter.Fill(dstResults);

			} // using
			return dstResults;
		} // method: ExecuteDataSet

		/// <summary>
		/// Executes a parameterized query by specifying a set of string parameters to a SQL query
		/// and returns the number of records affected
		/// </summary>
		/// <param name="connectionString">string containing the specified database connection string</param>
		/// <param name="strSQLStmt">string containing the SQL statement to be executed containing parameter placeholders</param>
		/// <param name="objDictParams">StringDictionary containing parameters to pass to the database
		/// with the parameters SQL query</param>
		/// <returns>integer containing the number of records affected by the query</returns>
		/// <example>
		/// <code>
		/// Sample SQL statement with parameters: SELECT MY_COLUMN FROM MY_TABLE WHERE MY_OTHER_COLUMN=~MYPARAMETER~
		/// Sample StringDictionary object: 
		/// Dictionary&lt;string, string&gt; myStrDictionary = new Dictionary&lt;string, string&gt;();
		/// myStrDictionary.Add("myKey", "myValue");
		/// myStrDictionary.Add("myKey2", "myValue2");
		/// int intRecordsAffected = DbFactory.ExecuteNonQuery(myConnString, mySQLStmt, myStrDictionary);
		/// </code> 
		/// </example>
		/// <seealso cref="ExecuteReader"/>
		/// <seealso cref="ExecuteDataSet"/>
		public static int ExecuteNonQuery<TKey, TValue>(string connectionString, string strSQLStmt, Dictionary<TKey, TValue> objDictParams)
		{
			return ExecuteNonQuery(connectionString, strSQLStmt, objDictParams, CommandType.Text);
		} // method: ExecuteNonQuery

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TKey"></typeparam>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="connectionString"></param>
		/// <param name="strSQLStmt"></param>
		/// <param name="objDictParams"></param>
		/// <param name="strCmdType"></param>
		/// <returns></returns>
		public static int ExecuteNonQuery<TKey, TValue>(string connectionString, string strSQLStmt, Dictionary<TKey, TValue> objDictParams, string strCmdType)
		{
			switch (strCmdType)
			{
				case "Text":
					return ExecuteNonQuery(connectionString, strSQLStmt, objDictParams, CommandType.Text);
				case "StoredProcedure":
					return ExecuteNonQuery(connectionString, strSQLStmt, objDictParams, CommandType.StoredProcedure);
				case "TableDirect":
					return ExecuteNonQuery(connectionString, strSQLStmt, objDictParams, CommandType.TableDirect);
				default:
					return ExecuteNonQuery(connectionString, strSQLStmt, objDictParams, CommandType.Text);
			}//switch
		} // method: ExecuteNonQuery


		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TKey"></typeparam>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="connectionString"></param>
		/// <param name="strSQLStmt"></param>
		/// <param name="objDictParams"></param>
		/// <param name="cmdType"></param>
		/// <returns></returns>
		internal static int ExecuteNonQuery<TKey, TValue>(string connectionString, string strSQLStmt, Dictionary<TKey, TValue> objDictParams, CommandType cmdType)
		{
			int intRecordsAffected = 0;

			using (Db.DbConnection dbConn = DbFactory.GetDbConnection(connectionString))
			{
				//Open the database connection
				dbConn.Open();

				DbCommand dbCmd = dbConn.CreateCommand();

				foreach (TKey dictKey in objDictParams.Keys)
				{
					DbParameter dbParam = dbCmd.CreateParameter();
					dbParam.ParameterName = dictKey.ToString();
					dbParam.Value = objDictParams[dictKey];

					dbCmd.Parameters.Add(dbParam);
				} // foreach

				dbCmd.CommandType = cmdType;
				dbCmd.CommandText = strSQLStmt;

				intRecordsAffected = dbCmd.ExecuteNonQuery();

			} // using

			return intRecordsAffected;
		} // method: ExecuteNonQuery


		/// <summary>
		/// 
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="strSQLStmt"></param>
		/// <returns></returns>
		public static int ExecuteNonQuery(string connectionString, string strSQLStmt)
		{
			Dictionary<string, string> objDictParams = new Dictionary<string, string>();

			return ExecuteNonQuery<string, string>(connectionString, strSQLStmt, objDictParams);

		} // method: ExecuteNonQuery

		/// <summary>
		/// Executes a database transaction
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="SQL"></param>
		public static void ExecuteNonQueryTransaction(string connectionString, string SQL)
		{
			using (DbConnection dbConn = DbFactory.GetDbConnection(connectionString))
			{
				DbTransaction dbTrans = null;

				try
				{
					//Open the database connection
					dbConn.Open();

					dbTrans = dbConn.BeginTransaction();

					dbConn.ExecuteNonQuery(SQL, dbTrans);

					dbTrans.Commit();
				} // try
				catch
				{
					if (dbTrans != null)
					{
						dbTrans.Rollback();
					} // if
				} // catch
			}//using
		} // method: ExecuteTransaction

        //PenTesting MITS:26852 - srajindersin - 20th Dec 2011
        #region Transaction Methods for Parameterized Queries

        /// <summary>
        /// Returns a scalar object value from the database by specifyinig a set of string parameters to a SQL query
        /// </summary>
        /// <param name="connectionString">string containing the specified database connection string</param>
        /// <param name="strSQLStmt">string containing the SQL statement to be executed containing parameter placeholders</param>
        /// <param name="objDictParams">StringDictionary containing parameters to pass to the database
        /// with the parameters SQL query</param>
        /// <returns>object containing scalar value returned from the database query</returns>
        /// <example>
        /// <code>
        /// Sample SQL statement with parameters: SELECT MY_COLUMN FROM MY_TABLE WHERE MY_OTHER_COLUMN=~MYPARAMETER~
        /// Sample StringDictionary object: 
        /// Dictionary&lt;string, string&gt; myStrDictionary = new Dictionary&lt;string, string&gt;();
        /// myStrDictionary.Add("myKey", "myValue");
        /// myStrDictionary.Add("myKey2", "myValue2");
        /// object myObject = DbFactory.ExecuteScalar(myConnString, mySQLStmt, myStrDictionary);
        /// </code>
        /// </example>
        /// <remarks>This method returns an object result which requires explicit casting to its 
        /// appropriate data type.  However, there are several things to keep in mind before performing
        /// an explicit cast.
        /// 1.  If a SQL statement does not return any records, the object value will be null.
        /// Therefore, performing a check of (objValue != null) would be appropriate.
        /// 2.  If the SQL statement result is a column value with a NULL value, the object value will actually be DBNull.Value
        /// Therefore, performing a check of (objValue != DBNull.Value) would be appropriate
        /// </remarks>
        /// <seealso cref="ExecuteReader"/>
        public static object ExecuteScalar<TKey, TValue>(DbCommand dbCmd, Dictionary<TKey, TValue> objDictParams)
        {
            object objScalarValue = null;

            foreach (TKey dictKey in objDictParams.Keys)
            {
                DbParameter dbParam = dbCmd.CreateParameter();
                dbParam.ParameterName = dictKey.ToString();
                dbParam.Value = objDictParams[dictKey];

                dbCmd.Parameters.Add(dbParam);
            } // foreach

            objScalarValue = dbCmd.ExecuteScalar();
            return objScalarValue;
        } // method: ExecuteScalar

        /// <summary>
        /// Creates a DbReader object based on a specified of string parameters to a SQL query
        /// </summary>
        /// <param name="connectionString">string containing the specified database connection string</param>
        /// <param name="strSQLStmt">string containing the SQL statement to be executed containing parameter placeholders</param>
        /// <param name="objDictParams">StringDictionary containing parameters to pass to the database
        /// with the parameters SQL query</param>
        /// <returns>DbReader containing with an open connection handle to the database</returns>
        /// <remarks>NOTE: Since this method returns a DbReader, the DbReader should be promptly closed after completion of the operation.
        /// Perhaps, one of the best ways to do this is to enclose the returned DbReader within a using block
        /// </remarks>
        /// <example>
        /// <code>
        /// Sample SQL statement with parameters: SELECT MY_COLUMN FROM MY_TABLE WHERE MY_OTHER_COLUMN=~MYPARAMETER~
        /// Sample StringDictionary object: 
        /// Dictionary&lt;string, string&gt; myStrDictionary = new Dictionary&lt;string, string&gt;();
        /// myStrDictionary.Add("myKey", "myValue");
        /// myStrDictionary.Add("myKey2", "myValue2");
        /// DbReader dbRdr = DbFactory.ExecuteReader(myConnString, mySQLStmt, myStrDictionary);
        /// </code>
        /// </example>
        /// <exception cref="DataException">DataException that occurs with database connection problems etc.</exception>
        public static DbReader ExecuteReader<TKey, TValue>(DbCommand dbCmd, Dictionary<TKey, TValue> objDictParams)
        {
            try
            {
                foreach (TKey dictKey in objDictParams.Keys)
                {
                    DbParameter dbParam = dbCmd.CreateParameter();
                    dbParam.ParameterName = dictKey.ToString();
                    dbParam.Value = objDictParams[dictKey];

                    dbCmd.Parameters.Add(dbParam);
                } // foreach

                return dbCmd.ExecuteReader();
            }
            catch (System.Data.DataException ex)
            {
                throw ex;
            }//catch
        } // method: ExecuteReader 

        /// <summary>
        /// Returns a DataSet from the database by specifyinig a set of string parameters to a SQL query
        /// </summary>
        /// <param name="connectionString">string containing the specified database connection string</param>
        /// <param name="strSQLStmt">string containing the SQL statement to be executed containing parameter placeholders</param>
        /// <param name="objDictParams">StringDictionary containing parameters to pass to the database
        /// with the parameters SQL query</param>
        /// <returns>DataSet containing the result set from the database</returns>
        /// <example>
        /// <code>
        /// Sample SQL statement with parameters: SELECT MY_COLUMN FROM MY_TABLE WHERE MY_OTHER_COLUMN=~MYPARAMETER~
        /// Sample StringDictionary object: 
        /// Dictionary&lt;string, string&gt; myStrDictionary = new Dictionary&lt;string, string&gt;();
        /// myStrDictionary.Add("myKey", "myValue");
        /// myStrDictionary.Add("myKey2", "myValue2");
        /// object myObject = DbFactory.ExecuteDataSet(myConnString, mySQLStmt, myStrDictionary);
        /// </code>
        /// </example>
        /// <remarks>This method is particularly useful when doing DataBinding to the UI through use
        /// of an ObjectDataSource or similar DataSource control</remarks>
        /// <seealso cref="ExecuteReader"/>
        /// //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for ExecuteDataSet
        /// </summary>     
        public static DataSet ExecuteDataSet<TKey, TValue>(DbCommand dbCmd, Dictionary<TKey, TValue> objDictParams)
        {
            if (DbFactory.CheckCallingStack("Riskmaster.Scripting"))
            {
                return ExecuteDataSet<TKey, TValue>( dbCmd,objDictParams,0);
            }
            else
                throw new ConfigurationErrorsException(string.Format("ExecuteDataSet -The current function overload is available to Scripting assembly (or RMXCustomScripts) only"));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        public static DataSet ExecuteDataSet<TKey, TValue>(DbCommand dbCmd, Dictionary<TKey, TValue> objDictParams, int p_iClientId)
        {
            DataSet dstResults = new DataSet();

            foreach (TKey dictKey in objDictParams.Keys)
            {
                DbParameter dbParam = dbCmd.CreateParameter();
                dbParam.ParameterName = dictKey.ToString();
                dbParam.Value = objDictParams[dictKey];

                dbCmd.Parameters.Add(dbParam);
            } // foreach

            //Get the DataAdapter
            DbDataAdapter dbAdapter = GetDataAdapter(dbCmd, p_iClientId);

            //Populate the contents of the DataSet from the DataAdapter
            dbAdapter.Fill(dstResults);

            return dstResults;
        } // method: ExecuteDataSet

        /// <summary>
        /// Executes a parameterized query by specifying a set of string parameters to a SQL query
        /// and returns the number of records affected
        /// </summary>
        /// <param name="connectionString">string containing the specified database connection string</param>
        /// <param name="strSQLStmt">string containing the SQL statement to be executed containing parameter placeholders</param>
        /// <param name="objDictParams">StringDictionary containing parameters to pass to the database
        /// with the parameters SQL query</param>
        /// <returns>integer containing the number of records affected by the query</returns>
        /// <example>
        /// <code>
        /// Sample SQL statement with parameters: SELECT MY_COLUMN FROM MY_TABLE WHERE MY_OTHER_COLUMN=~MYPARAMETER~
        /// Sample StringDictionary object: 
        /// Dictionary&lt;string, string&gt; myStrDictionary = new Dictionary&lt;string, string&gt;();
        /// myStrDictionary.Add("myKey", "myValue");
        /// myStrDictionary.Add("myKey2", "myValue2");
        /// int intRecordsAffected = DbFactory.ExecuteNonQuery(myConnString, mySQLStmt, myStrDictionary);
        /// </code>
        /// </example>
        /// <seealso cref="ExecuteReader"/>
        /// <seealso cref="ExecuteDataSet"/>
        public static int ExecuteNonQuery<TKey, TValue>(DbCommand dbCmd, Dictionary<TKey, TValue> objDictParams)
        {
            int intRecordsAffected = 0;

            foreach (TKey dictKey in objDictParams.Keys)
            {
                DbParameter dbParam = dbCmd.CreateParameter();
                dbParam.ParameterName = dictKey.ToString();
                dbParam.Value = objDictParams[dictKey];

                dbCmd.Parameters.Add(dbParam);
            } // foreach

            intRecordsAffected = dbCmd.ExecuteNonQuery();

            return intRecordsAffected;
        } // method: ExecuteNonQuery


        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for ExecuteDataSet
        /// </summary>     
        public static DataSet ExecuteDataSet<TKey, TValue>(DbConnection dbConn, string strSQLStmt, Dictionary<TKey, TValue> objDictParams)
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                return ExecuteDataSet<TKey, TValue>( dbConn,  strSQLStmt,objDictParams,0);
            }
            else
                throw new ConfigurationErrorsException(string.Format("ExecuteDataSet -The current function overload is available to Scripting assembly (or RMXCustomScripts) only"));
             // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// <summary>
        /// Returns a DataSet from the database by specifying a set of string parameters to a SQL query
        /// </summary>
        /// <param name="dbConn">The database connection.</param>
        /// <param name="strSQLStmt">string containing the SQL statement to be executed containing parameter placeholders</param>
        /// <param name="objDictParams">StringDictionary containing parameters to pass to the database
        /// with the parameters SQL query</param>
        /// <returns>DataSet containing the result set from the database</returns>
        /// <example>
        /// <code>
        /// Sample SQL statement with parameters: SELECT MY_COLUMN FROM MY_TABLE WHERE MY_OTHER_COLUMN=~MYPARAMETER~
        /// Sample StringDictionary object: 
        /// Dictionary&lt;string, string&gt; myStrDictionary = new Dictionary&lt;string, string&gt;();
        /// myStrDictionary.Add("myKey", "myValue");
        /// myStrDictionary.Add("myKey2", "myValue2");
        /// object myObject = DbFactory.ExecuteDataSet(myConnString, mySQLStmt, myStrDictionary);
        /// </code>
        /// </example>
        /// <remarks>This method is particularly useful when doing DataBinding to the UI through use
        /// of an ObjectDataSource or similar DataSource control</remarks>
        /// <seealso cref="ExecuteReader"/>
        /// Start - MITS# 36997 GAP 06 - agupta298 - GAP 6 - RMA 5497
        public static DataSet ExecuteDataSet<TKey, TValue>(DbConnection dbConn, string strSQLStmt, Dictionary<TKey, TValue> objDictParams, int p_iClientId)
        {
            //Open the database connection
            if (dbConn.State != ConnectionState.Open)
                dbConn.Open();

            DbCommand dbCmd = dbConn.CreateCommand();
            dbCmd.CommandText = strSQLStmt;

            if (dbConn.DatabaseType == Db.eDatabaseType.DBMS_IS_ORACLE)
            {
                ((Oracle.DataAccess.Client.OracleCommand)(dbCmd.m_anyCommand)).BindByName = true;
            }

            return ExecuteDataSet(dbCmd,objDictParams,p_iClientId);
        } // method: ExecuteDataSet 
        //End - MITS# 36997 GAP 06 - agupta298 - GAP 6 - RMA 5497
        #endregion
        //END PenTesting MITS:26852 - srajindersin - 20th Dec 2011

    }//class 
    #endregion

    //Deb ML Changes
    /// <summary>
    /// GlobalResource
    /// </summary>
    public class GlobalResource
    {
        private static Dictionary<string, string> resourceGlobalCache = new Dictionary<string, string>();
        private static string sConnString =string.Empty;// ConfigurationManager.ConnectionStrings["ViewDataSource"].ConnectionString;
        private static object synObj = new object();

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetResource
        /// </summary>     
        public static string GetResource(string sKey)
        {
            if (DbFactory.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetResource( sKey,0);
            }
            else
                throw new ConfigurationErrorsException(string.Format("GetResource -The current function overload is available to Scripting assembly (or RMXCustomScripts) only"));
           // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// </summary>
        /// <param name="sKey"></param>
        /// <returns></returns>
        public static string GetResource(string sKey, int p_iClientId)
        {
            string sRet = string.Empty;
            string sKeyValue = string.Empty;
            //todo optimize the code here
            
            try
            {
                if (p_iClientId != 0)
                {
                    string sBaseConnString = ConfigurationManager.ConnectionStrings["rmATenantSecurity"].ConnectionString;
                    Dictionary<string, int> objParams = new Dictionary<string, int>();
                    objParams.Add("CLIENT_ID", p_iClientId);
                    string sSQL = String.Format("SELECT {0} FROM CLIENT INNER JOIN CLIENT_DETAIL ON CLIENT.CLIENT_ID = CLIENT_DETAIL.CLIENT_ID WHERE CLIENT_DETAIL.CLIENT_ID={1}", "ViewDataSource", "~CLIENT_ID~");
                    object objClientConnString = DbFactory.ExecuteScalar(sBaseConnString, sSQL, objParams);
                    sConnString = Convert.ToString(objClientConnString);
                }
                else
                {
                    sConnString = ConfigurationManager.ConnectionStrings["ViewDataSource"].ConnectionString;
                }
                if (resourceGlobalCache.ContainsKey(sKey))
                {
                    sKeyValue = resourceGlobalCache[sKey];
                    sRet = sKeyValue;
                }
                if (string.IsNullOrEmpty(sKeyValue))
                {
                    string sSql = string.Format("SELECT GLOBAL_RESOURCE_ID,LANGUAGE_ID,RESOURCE_VALUE FROM GLOBAL_RESOURCE WHERE RESOURCE_KEY='{0}'", sKey);//rkaur27 : JIRA [RMA - 1542]
                    using (DbReader dbr = DbFactory.ExecuteReader(sConnString, sSql))
                    {
                        while (dbr.Read())
                        {
                            if (string.IsNullOrEmpty(sKeyValue))
                            {
                                sKeyValue = dbr.GetValue("GLOBAL_RESOURCE_ID").ToString() + "|^|" + dbr.GetValue("LANGUAGE_ID").ToString() + "|^|" + dbr.GetValue("RESOURCE_VALUE").ToString();
                            }
                            else
                            {
                                sKeyValue = sKeyValue + "~*~" + dbr.GetValue("GLOBAL_RESOURCE_ID").ToString() + "|^|" + dbr.GetValue("LANGUAGE_ID").ToString() + "|^|" + dbr.GetValue("RESOURCE_VALUE").ToString();
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(sKeyValue))
                    {
                        lock (synObj)
                        {
                            if (!resourceGlobalCache.ContainsKey(sKey))
                                resourceGlobalCache.Add(sKey, sKeyValue);
                        }
                    }
                    sRet = sKeyValue;
                }
            }
            catch (Exception ee)
            {
            }
            // RMA 6444 - In case the Key is not present in DB, and while throwing the error back to CommonWebService, an exception is also sent, then it used to check if the 
            // the Error Description is blank, then it used to display the exception message. But now as we are sending the key, it was failing that condition.
            // So prepending "~~BlankKey" with the Key and if the message in Service ends with this token we are displaying the Exception Message
            return (!string.IsNullOrEmpty(sKeyValue) ? sRet : sKey +"~~BlankKey");
        }
        /// <summary>
        /// ClearResource
        /// </summary>
        /// <param name="sResId">sResId</param>
        public static void ClearResource(string sResId)
        {
            Dictionary<string, string> resCacheByKeyType =null;
            try
            {
                resCacheByKeyType = resourceGlobalCache;
                foreach (KeyValuePair<string, string> sKey in resCacheByKeyType)
                {
                    if (sKey.Value.StartsWith(sResId + "|^|"))
                    {
                        resourceGlobalCache.Remove(sKey.Key);
                    }
                }
            }
            catch (Exception ee)
            {
                resCacheByKeyType = null;
            }
        }
        /// <summary>
        /// Append Error Code to the Resource Value
        /// </summary>
        /// <param name="sKeyID"></param>
        /// <returns></returns>
        //private static string AppendErrorCode(string sKeyID)
        //{
        //    if (string.IsNullOrEmpty(sKeyID))
        //    {
        //        return string.Empty;
        //    }
        //    else
        //    {
        //        sKeyID = "rmA-" + sKeyID;
        //        return sKeyID;
        //    }
        //}
        /// <summary>
        /// Remove Error code from Resource Value
        /// </summary>
        /// <param name="sKeyValue"></param>
        /// <returns></returns>
        //private static string RemoveErrorCode(string sKeyValue)
        //{
        //    if (sKeyValue.StartsWith("rmA-"))
        //    {
        //        string sValue = sKeyValue.Substring(sKeyValue.IndexOf(":") + 1);
        //        return sValue;
        //    }
        //    else
        //    {
        //        return sKeyValue;
        //    }
        //}
    }
    //Deb ML Changes


    //Rakhel ML Changes
    /// <summary>
    /// LocalResource
    /// </summary>
    public class LocalResource
    {
        private static Dictionary<string, string> resourceLocalCache = new Dictionary<string, string>();
        private static string sConnString =string.Empty;// ConfigurationManager.ConnectionStrings["ViewDataSource"].ConnectionString;
        private static object synObj = new object();

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetResource
        /// </summary>     
        public static string GetResource(string sKey,int iResourceType ,string sPageId)
        {
            if (DbFactory.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetResource(sKey, iResourceType, sPageId, 0);
            }
            else
                throw new ConfigurationErrorsException(string.Format("GetResource -The current function overload is available to Scripting assembly (or RMXCustomScripts) only"));
             // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
        /// </summary>
        /// <param name="sKey"></param>
        /// <param name="sResourceType"></param>
        /// <param name="sPageId"></param>
        /// <param name="sLangCode"></param>
        /// <returns></returns>
        public static string GetResource(string sKey,int iResourceType ,string sPageId,int p_iClientId)
        {
            string sRet = string.Empty;
            string sKeyValue = string.Empty;
           
            try
            {
                //todo optimize the code here
                if (p_iClientId != 0)
                {
                    string sBaseConnString = ConfigurationManager.ConnectionStrings["rmATenantSecurity"].ConnectionString;
                    Dictionary<string, int> objParams = new Dictionary<string, int>();
                    objParams.Add("CLIENT_ID", p_iClientId);
                    string sSQL = String.Format("SELECT {0} FROM CLIENT INNER JOIN CLIENT_DETAIL ON CLIENT.CLIENT_ID = CLIENT_DETAIL.CLIENT_ID WHERE CLIENT_DETAIL.CLIENT_ID={1}", "ViewDataSource", "~CLIENT_ID~");
                    object objClientConnString = DbFactory.ExecuteScalar(sBaseConnString, sSQL, objParams);
                    sConnString = Convert.ToString(objClientConnString);
                }
                else
                {
                    sConnString = ConfigurationManager.ConnectionStrings["ViewDataSource"].ConnectionString;
                }
                if (resourceLocalCache.ContainsKey(sKey))
                {
                    sKeyValue = resourceLocalCache[sKey];
                    sRet = sKeyValue;
                }
                if (string.IsNullOrEmpty(sKeyValue))
                {
                    string sSql = string.Format("SELECT LOCAL_RESOURCE_ID,LANGUAGE_ID,RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE RESOURCE_KEY='{0}' AND RESOURCE_TYPE={1} AND PAGE_ID={2}", sKey, iResourceType, sPageId);
                    using (DbReader dbr = DbFactory.ExecuteReader(sConnString, sSql))
                    {
                        while (dbr.Read())
                        {
                            if (string.IsNullOrEmpty(sKeyValue))
                            {
                                sKeyValue = dbr.GetValue("LOCAL_RESOURCE_ID").ToString() + "|^|" + dbr.GetValue("LANGUAGE_ID").ToString() + "|^|" + dbr.GetValue("RESOURCE_VALUE").ToString();
                            }
                            else
                            {
                                sKeyValue = sKeyValue + "~*~" + dbr.GetValue("LOCAL_RESOURCE_ID").ToString() + "|^|" + dbr.GetValue("LANGUAGE_ID").ToString() + "|^|" + dbr.GetValue("RESOURCE_VALUE").ToString();
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(sKeyValue))
                    {
                        lock (synObj)
                        {
                            if (!resourceLocalCache.ContainsKey(sKey))
                                resourceLocalCache.Add(sKey, sKeyValue);
                        }
                    }
                    sRet = sKeyValue;
                }
            }
            catch (Exception ee)
            {
            }
            // RMA 6444 - In case the Key is not present in DB, and while throwing the error back to CommonWebService, an exception is also sent, then it used to check if the 
            // the Error Description is blank, then it used to display the exception message. But now as we are sending the key, it was failing that condition.
            // So prepending "~~BlankKey" with the Key and if the message in Service ends with this token we are displaying the Exception Message
            return (!string.IsNullOrEmpty(sKeyValue) ? sRet : sKey + "~~BlankKey");
        }
        /// <summary>
        /// ClearResource
        /// </summary>
        /// <param name="sResId">sResId</param>
        public static void ClearResource(string sResId)
        {
            Dictionary<string, string> resCacheByKeyType = null;
            try
            {
                resCacheByKeyType = resourceLocalCache;
                foreach (KeyValuePair<string, string> sKey in resCacheByKeyType)
                {
                    if (sKey.Value.StartsWith(sResId + "|^|"))
                    {
                        resourceLocalCache.Remove(sKey.Key);
                    }
                }
            }
            catch (Exception ee)
            {
                resCacheByKeyType = null;
            }
        }

        //rkotak:9681
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sPageName"></param>
        /// <param name="p_iClientId"></param>
        /// <returns></returns>
        public static string GetPageId(string sPageName, int p_iClientId)
        {
            string sPageId = string.Empty;

            try
            {
                if (p_iClientId != 0)
                {
                    string sBaseConnString = ConfigurationManager.ConnectionStrings["rmATenantSecurity"].ConnectionString;
                    Dictionary<string, int> objParams = new Dictionary<string, int>();
                    objParams.Add("CLIENT_ID", p_iClientId);
                    string sSQL = String.Format("SELECT {0} FROM CLIENT INNER JOIN CLIENT_DETAIL ON CLIENT.CLIENT_ID = CLIENT_DETAIL.CLIENT_ID WHERE CLIENT_DETAIL.CLIENT_ID={1}", "ViewDataSource", "~CLIENT_ID~");
                    object objClientConnString = DbFactory.ExecuteScalar(sBaseConnString, sSQL, objParams);
                    sConnString = Convert.ToString(objClientConnString);
                }
                else
                {
                    sConnString = ConfigurationManager.ConnectionStrings["ViewDataSource"].ConnectionString;
                }

                if (!string.IsNullOrEmpty(sPageName))
                {
                    string sSql = string.Format("SELECT PAGE_ID FROM PAGE_INFO WHERE PAGE_NAME='{0}'", sPageName);

                    sPageId = Convert.ToString(DbFactory.ExecuteScalar(sConnString, sSql));

                }
            }
            catch (Exception ee)
            {
            }
            // RMA 6444 - In case the Key is not present in DB, and while throwing the error back to CommonWebService, an exception is also sent, then it used to check if the 
            // the Error Description is blank, then it used to display the exception message. But now as we are sending the key, it was failing that condition.
            // So prepending "~~BlankKey" with the Key and if the message in Service ends with this token we are displaying the Exception Message
            return sPageId;
        }
        //rkotak:9681
       

       
    }
    //Rakhel  ML Changes
}
