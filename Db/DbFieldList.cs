using System;
using System.Collections;

namespace Riskmaster.Db
{
    
     /// <summary>Riskmaster.Db.DbFieldList used by the DbWriter object as a 
     /// strongly typed collection for Field objects.  It is exposed through the DbWriter.Fields property.
     /// The class offers several Add overloads to make populating the collection easier.</summary>
     public class DbFieldList
	{
        private ArrayList m_Fields=new ArrayList(16);
        
         /// <summary>Riskmaster.Db.DbFieldList.this string indexed iterator retrieves\sets 
         /// a DbField object based on it's name.</summary>
         /// <returns>DbField object or throws an exception to indicate that the requested field was not found.</returns>
         public DbField this[string name] {
            get
			{
				foreach(DbField field in m_Fields)
				{
					if(field.Name==name)
						return field;
				}
                throw new InvalidOperationException("Field could not be found.");
            }
            set
			{
				for(int i=0;i<m_Fields.Count;i++)
				{
					if(((DbField)m_Fields[i]).Name==name)
					{
						m_Fields[i]=value;
						return;
					}
				}
				throw new InvalidOperationException("Field could not be found.");
            }
        }

		
		 /// <summary>Riskmaster.Db.DbFieldList.this integer based iterator retrieves\sets 
		 /// a DbField object based on it's position.</summary>
		 /// <returns>DbField object or throws an exception to indicate that the requested field was not found.</returns>
		 public DbField this[int index] 
		{
			get
			{
				return (DbField)m_Fields[index];
			}
			set
			{
				m_Fields[index]=value;
			}
		}

		
         /// <summary>This Riskmaster.Db.DbFieldList.Add overload creates 
         /// a new DbField using the name and string value provided.</summary>
         /// <param name="fieldName">Name of the DbField to be created and added.</param>
         /// <param name="value">String value of the DbField to be created and added.</param>
         public int Add(string fieldName, string value)
		{
			return m_Fields.Add(new DbField(fieldName,value));
		}

		
		 /// <summary>This Riskmaster.Db.DbFieldList.Add overload creates 
		 /// a new DbField using the name and integer value provided.</summary>
		 /// <param name="fieldName">Name of the DbField to be created and added.</param>
		 /// <param name="value">Integer value of the DbField to be created and added.</param>
		 public int Add(string fieldName, int value)
		{
			return m_Fields.Add(new DbField(fieldName,value));
		}

		
		 /// <summary>This Riskmaster.Db.DbFieldList.Add overload creates 
		 /// a new DbField using the name and float value provided.</summary>
		 /// <param name="fieldName">Name of the DbField to be created and added.</param>
		 /// <param name="value">Float value of the DbField to be created and added.</param>
		 public int Add(string fieldName, float value)
		{
			return m_Fields.Add(new DbField(fieldName,value));
		}

         //pmittal5 Mits 13767 12/16/08
         /// <summary>This Riskmaster.Db.DbFieldList.Add overload creates 
         /// a new DbField using the name and integer value provided.</summary>
         /// <param name="fieldName">Name of the DbField to be created and added.</param>
         /// <param name="value">Long value of the DbField to be created and added.</param>
         public int Add(string fieldName, long value)
         {
             return m_Fields.Add(new DbField(fieldName, value));
         }
         //End - pmittal5
		 /// <summary>This Riskmaster.Db.DbFieldList.Add overload creates 
		 /// a new DbField using the name and object value provided.  
		 ///This is the override to use when a stronger typed version is not available.</summary>
		 /// <param name="fieldName">Name of the DbField to be created and added.</param>
		 /// <param name="value">Object value of the DbField to be created and added.</param>
		 public int Add(string fieldName, object value)
		{
			return m_Fields.Add(new DbField(fieldName,value));
		}
         /// <summary>
         /// Add  by kuladeep for Jira-527- Overload function of Add with direction.
         /// </summary>
         /// <param name="fieldName"></param>
         /// <param name="value"></param>
         /// <param name="Direction"></param>
         /// <returns></returns>
         public int Add(string fieldName, object value, System.Data.ParameterDirection Direction)
         {
             return m_Fields.Add(new DbField(fieldName, value, Direction));
         }

        
		 /// <summary>This Riskmaster.Db.DbFieldList.Add overload creates 
		 /// a new DbField using the name provided.  
		 ///This is the override to use when the value is still unknown.</summary>
		 /// <param name="value">Name of the DbField to be created and added.</param>
         public int Add(DbField value)
		{
            return m_Fields.Add(value);
        }
               
        
         /// <summary>This Riskmaster.Db.DbFieldList.Contains overload checks to see 
         /// whether the collection contains a DbField object with a matching name.</summary>
         /// <param name="value">DbField who's name to look for.</param>
         /// <returns>Boolean true if the collection contains a matching field.</returns>
         public bool Contains(DbField value) {
            return m_Fields.Contains(value.Name);
        }

		
		 /// <summary>This Riskmaster.Db.DbFieldList.Contains overload checks to see 
		 /// whether the collection contains a DbField object with a matching name.</summary>
		 /// <param name="fieldName">String  field name to look for.</param>
		 /// <returns>Boolean true if the collection contains a matching field.</returns>
         public bool Contains(string fieldName) 
		{
			return m_Fields.Contains(this[fieldName]);
		}
        
        
         /// <summary>This Riskmaster.Db.DbFieldList.Remove overload 
         /// removes the specified DbField from the collection.</summary>
         /// <param name="value">DbField to be removed.</param>
         public void Remove(DbField value)
		{
            m_Fields.Remove(value);
        }

		
		 /// <summary>This Riskmaster.Db.DbFieldList.Remove overload 
		 /// removes the specified DbField from the collection.</summary>
		 /// <param name="fieldName">String field name to be removed.</param>
		 public void Remove(string fieldName) 
		{
			m_Fields.Remove(this[fieldName]);
		}
        
        
         /// <summary>This Riskmaster.Db.DbFieldList.CopyTo overload populates 
         /// a DbField array starting at a given index with the DbField objects 
         /// of this collection.</summary>
         /// <param name="array">DbField array to populate.</param>
         /// <param name="arrayIndex">Integer index at which to place the first DbField object.</param>
         public void CopyTo(DbField[] array, int arrayIndex)
		{
			m_Fields.CopyTo(array,arrayIndex);
        }

		
		 /// <summary>This Riskmaster.Db.DbFieldList.CopyTo overload populates 
		 /// a DbField array with the DbField objects 
		 /// of this collection.</summary>
		 /// <param name="array">DbField array to populate.</param>
		 public void CopyTo(DbField[] array)
		{
			m_Fields.CopyTo(array,0);
		}

		
         /// <summary>Riskmaster.Db.DbFieldList.Count provides a count of the DbFields currently in the collection.</summary>
         /// <returns>Integer count of the DbFields currently in the collection.</returns>
         public int Count
		{
			get
			{
				return m_Fields.Count;
			}
		}
    }
}
