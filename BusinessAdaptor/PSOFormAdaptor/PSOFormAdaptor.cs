using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.DataModel;
using System.Collections;
using Riskmaster.Application.PSOForm;
using Riskmaster.Application.DocumentManagement;
using Riskmaster.Application.FileStorage;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// This class is written for PSO Form
    /// Author: Anu Tennyson
    /// </summary>
    public class PSOFormAdaptor : BusinessAdaptorBase
    {
        public const string s_FORM_NAME = "PSO Web Form";
        public const string s_ATTACH_TABLE = "Event";
        public const string s_FILE_EXTN = ".pdf";

        #region Constructor
        public PSOFormAdaptor()
        {
        }
        #endregion

        private enum ErrorType : int
        {
            PatientMisMatch = 1,
            PatientCountInsuffienct = 2,
            PatNoError = 3
        }
        /// <summary>
        /// This function populates the screen
        /// </summary>
        /// <param name="objXmlIn">Input XML</param>
        /// <param name="objXmlOut">Output XML</param>
        /// <param name="objErrOut">Error XML</param>
        /// <returns>bool value</returns>
        public bool Get(XmlDocument objXmlIn, ref XmlDocument objXmlOut, ref BusinessAdaptorErrors objErrOut)
        {
            bool bIsSuccess = false;
            bool bDeleteINCMedInfo = false;
            bool bDeleteNMMedInfo = false;
            bool bDeleteUSCMedInfo = false;
            bool bDeleteDeviceInfo = false;
            bool bDataChanged = false;
            bool bPatientMismatch = false;
            int iIndex = 0;
            int iEvtPsoRowId = 0;
            int iPatPsoRowId = 0;
            string sFilePath = string.Empty;
            string sHiddenFields = string.Empty;
            string sDataChanged = string.Empty;
            XmlDocument xmlDom = new XmlDocument();
            XmlElement objGridXMLElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlElement objOptionXmlElement = null;
            XmlElement objGridTempElement = null;
            List<string> lstHiddenControl = new List<string>();
            List<string> lstPanelControl = new List<string>();
            List<string> lstRemoveHiddenControls = new List<string>();
            List<string> lstDeletedINCMedInfoRowId = new List<string>();
            List<string> lstDeletedNMMedInfoRowId = new List<string>();
            List<string> lstDeletedUSCMedInfoRowId = new List<string>();
            List<string> lstDeletedDeviceInfoRowId = new List<string>();
            List<string> lstDeletedLnkEvtRowId = new List<string>();
            Dictionary<string, List<CodeList>> dicCodeList = new Dictionary<string, List<CodeList>>();
            Dictionary<string, List<string>> dicControlList = new Dictionary<string,List<string>>();
            PSOFormManager objPSOFormManager = new PSOFormManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password,base.ClientId);
            EventPSO objEventPSO = (EventPSO)objPSOFormManager.DataModelFactory.GetDataModelObject("EventPSO", false);
            PatientPSO objPatientInfo = (PatientPSO)objPSOFormManager.DataModelFactory.GetDataModelObject("PatientPSO", false);
            Event objEvent = (Event)objPSOFormManager.DataModelFactory.GetDataModelObject("Event", false);

            try
            {
                xmlDom.LoadXml(objXmlIn.SelectSingleNode("//Event/FormValues").InnerText);
                sDataChanged = objXmlIn.SelectSingleNode("//Event/SysPageDataChanged").InnerText;

                if (!string.IsNullOrEmpty(sDataChanged))
                {
                    bDataChanged = true;
                }
                //********************************
                //Populating fields
                int iEvent = 0;
                if ((XmlElement)objXmlIn.SelectSingleNode("/Event/EventPSOData/eventid") != null)
                {
                    iEvent = Conversion.CastToType<int>(objXmlIn.SelectSingleNode("/Event/EventPSOData/eventid").InnerText, out bIsSuccess);
                    if (iEvent != 0)
                    {
                        objEvent.MoveTo(iEvent);
                        PopulateXMLDom(objEvent, ref objXmlIn, objPSOFormManager, lstHiddenControl);
                    }

                }
                //******************************

                bool bCheck = false;
                XmlDocument xmlTempDocument = new XmlDocument();
                XmlDocument objPropStoreTemp = new XmlDocument();
                XmlDocument objSerilzationXML = new XmlDocument();

                if ((XmlElement)objXmlIn.SelectSingleNode("/Event/EventPSO/EvtPsoRowId") != null)
                {
                    iEvtPsoRowId = Conversion.CastToType<int>(objXmlIn.SelectSingleNode("/Event/EventPSO/EvtPsoRowId").InnerText, out bIsSuccess);
                }
                if (iEvtPsoRowId == 0 )
                {
                    xmlTempDocument.LoadXml(objXmlIn.SelectSingleNode("//Event/EventPSO").OuterXml);
                    objEventPSO.PopulateObject(xmlTempDocument);
                }
                else
                {
                    objEventPSO.MoveTo(iEvtPsoRowId);
                    if (bDataChanged)
                    {
                        xmlTempDocument.LoadXml(objXmlIn.SelectSingleNode("//Event/EventPSO").OuterXml);
                        objEventPSO.PopulateObject(xmlTempDocument);
                    }
                }

                objSerilzationXML.LoadXml("<EventPSO></EventPSO>");
                objPropStoreTemp.LoadXml(objEventPSO.SerializeObject(objSerilzationXML));

                XmlElement objTempElement = (XmlElement)objXmlIn.SelectSingleNode("//Event/EventPSO");
                if (objTempElement != null)
                {
                    objTempElement.ParentNode.RemoveChild(objTempElement);
                }
                objTempElement = (XmlElement)objXmlIn.SelectSingleNode("//Event");
                objTempElement.AppendChild(objXmlIn.ImportNode(objPropStoreTemp.SelectSingleNode("//EventPSO"), true));

                xmlTempDocument = new XmlDocument();
                objPropStoreTemp = new XmlDocument();
                objSerilzationXML = new XmlDocument();

                PSOFormAdaptor.ErrorType enumErrorType = ErrorType.PatNoError;
                if ((XmlElement)objXmlIn.SelectSingleNode("/Event/PatientPSO/PatPsoRowId") != null)
                {
                    iPatPsoRowId = Conversion.CastToType<int>(objXmlIn.SelectSingleNode("/Event/PatientPSO/PatPsoRowId").InnerText, out bIsSuccess);
                }

                if (iPatPsoRowId == 0 )
                {
                    string sEventCategory = string.Empty;
                    XmlElement objNodeTemp = null;
                    if (objXmlIn.SelectSingleNode("//Event/PatientPSO/psocdAffectedByEventBirthing") != null)
                    {
                        objNodeTemp = (XmlElement)objXmlIn.SelectSingleNode("//Event/PatientPSO/psocdAffectedByEventBirthing");
                        sEventCategory = Convert.ToString(objPSOFormManager.LocalCache.GetShortCode(Conversion.CastToType<int>(objNodeTemp.GetAttribute("codeid"), out bIsSuccess)));

                    }
                    if (string.Compare(sEventCategory, "A1524", true) != 0)
                    {
                        if (objXmlIn.SelectSingleNode("//Event/PatientPSO/PatInvNeoEntId") != null)
                        {
                            objXmlIn.SelectSingleNode("//Event/PatientPSO/PatInvNeoEntId").InnerText = "0";
                        }
                    }
                    xmlTempDocument.LoadXml(objXmlIn.SelectSingleNode("//Event/PatientPSO").OuterXml);
                    objPatientInfo.PopulateObject(xmlTempDocument);
                    
                    DeletePatient(objXmlIn, iPatPsoRowId, objPatientInfo, ref objErrOut);
                }
                else
                {
                    objPatientInfo.MoveTo(iPatPsoRowId);
                    if (bDataChanged)
                    {
                        xmlTempDocument.LoadXml(objXmlIn.SelectSingleNode("//Event/PatientPSO").OuterXml);
                        objPatientInfo.PopulateObject(xmlTempDocument);
                    }
                    CheckPatientInfo(objXmlIn, objPatientInfo, iPatPsoRowId, out enumErrorType, ref bPatientMismatch);

                    if (bPatientMismatch)
                    {
                        if (enumErrorType == ErrorType.PatientMisMatch)
                        {
                            objErrOut.Add(Globalization.GetString("PSOFormAdaptor.PatientMismatch.Error", base.ClientId), Globalization.GetString("PSOFormAdaptor.PatientMismatch.Error", base.ClientId), BusinessAdaptorErrorType.Warning);
                        }
                        else if (enumErrorType == ErrorType.PatientCountInsuffienct)
                        {
                            DeletePatient(objXmlIn, iPatPsoRowId, objPatientInfo, ref objErrOut);
                        }

                    }
                    
                }

                objSerilzationXML.LoadXml("<PatientPSO></PatientPSO>");
                objPropStoreTemp.LoadXml(objPatientInfo.SerializeObject(objSerilzationXML));

                objTempElement = (XmlElement)objXmlIn.SelectSingleNode("//Event/PatientPSO");
                if (objTempElement != null)
                {
                    objTempElement.ParentNode.RemoveChild(objTempElement);
                }
                objTempElement = (XmlElement)objXmlIn.SelectSingleNode("//Event");
                objTempElement.AppendChild(objXmlIn.ImportNode(objPropStoreTemp.SelectSingleNode("//PatientPSO"), true));


                PopulateDomValue(ref xmlDom, objXmlIn, objPSOFormManager, "//Event/EventPSO", false);
                PopulateDomValue(ref xmlDom, objXmlIn, objPSOFormManager, "//Event/PatientPSO", false);
                PopulateDomValue(ref xmlDom, objXmlIn, objPSOFormManager, "//Event/EventPSOData", false);
                PopulateDomValue(ref xmlDom, objXmlIn, objPSOFormManager, "//Event/PatientPSOData", false);

                HideDisplayXml(ref xmlDom, lstHiddenControl, lstRemoveHiddenControls, objPSOFormManager);

                for (int iCount = 0; iCount < lstRemoveHiddenControls.Count; iCount++)
                {
                    if (lstHiddenControl.Contains(lstRemoveHiddenControls[iCount]))
                    {
                        lstHiddenControl.RemoveAll(sValue => sValue == lstRemoveHiddenControls[iCount]);
                    }
                }

                for (int iCount = 0; iCount < lstHiddenControl.Count; iCount++)
                {
                    if (lstHiddenControl[iCount].Contains("pnl"))
                    {
                        sHiddenFields = sHiddenFields + lstHiddenControl[iCount] + "," + lstHiddenControl[iCount] + "_header" + ",";                        
                        GetPanelControls(lstHiddenControl[iCount], lstPanelControl, xmlDom);
                    }
                    else
                    {
                        sHiddenFields = sHiddenFields + lstHiddenControl[iCount] + "_ctlRow,";
                    }
                }
                sHiddenFields = sHiddenFields.TrimEnd(',');

                objPSOFormManager.CreateAndSetElement(objXmlIn.DocumentElement, "HiddenFieldList", sHiddenFields);

                //Get the required fields starts
                string sRequiredFileds = string.Empty;
                GetRequiredFields(xmlDom, ref sRequiredFileds);
                objPSOFormManager.CreateAndSetElement(objXmlIn.DocumentElement, "RequiredFieldList", sRequiredFileds);
                //Ends

                //Get the controls in the Panel
                string sPanelContControls = string.Empty;
                for (int iCount = 0; iCount < lstPanelControl.Count; iCount++)
                {
                    sPanelContControls = sPanelContControls + lstPanelControl[iCount] + ",";
                }
                sPanelContControls = sPanelContControls.TrimEnd(',');
                objPSOFormManager.CreateAndSetElement(objXmlIn.DocumentElement, "pnlhiddencontrols", sPanelContControls);
                //Ends
                //Get the txtFunctionToCall
                string sTxtFuncToCall = string.Empty;
                if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtGridDataDeleted") != null)
                {
                    sTxtFuncToCall = objXmlIn.SelectSingleNode("//Event/EventPSOData/txtGridDataDeleted").InnerText;
                }
                if (sTxtFuncToCall.Contains("INCMEDGrid"))
                {
                    bDeleteINCMedInfo = true;
                }
                if (sTxtFuncToCall.Contains("NMMEDGrid"))
                {
                    bDeleteNMMedInfo = true;
                }
                if (sTxtFuncToCall.Contains("USCMEDGrid"))
                {
                    bDeleteUSCMedInfo = true;
                }
                if (sTxtFuncToCall.Contains("DevicesGrid"))
                {
                    bDeleteDeviceInfo = true;
                }
                
                //Ends
                //Setting the deleted row id for the grids
                if (bDeleteINCMedInfo)
                {
                    string sINCMedRowId = string.Empty;
                    foreach (MedicationInfo objINCMedInfo in objEventPSO.MedInfoList)
                    {
                        if (objINCMedInfo.MedRowId > 0)
                        {
                            sINCMedRowId = sINCMedRowId + objINCMedInfo.MedRowId + "|";
                        }
                    }
                    sINCMedRowId = sINCMedRowId.TrimEnd('|');
                    if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtINCMedInfoDeletedId") != null)
                    {
                        objXmlIn.SelectSingleNode("//Event/EventPSOData/txtINCMedInfoDeletedId").InnerText = sINCMedRowId;
                    }
                }
                if (bDeleteNMMedInfo)
                {
                    string sNMMedRowId = string.Empty;
                    foreach (MedicationInfo objINCMedInfo in objEventPSO.MedInfoList)
                    {
                        if (objINCMedInfo.MedRowId > 0)
                        {
                            sNMMedRowId = sNMMedRowId + objINCMedInfo.MedRowId + "|";
                        }
                    }
                    sNMMedRowId = sNMMedRowId.TrimEnd('|');
                    if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtNMMedInfoDeletedId") != null)
                    {
                        objXmlIn.SelectSingleNode("//Event/EventPSOData/txtNMMedInfoDeletedId").InnerText = sNMMedRowId;
                    }
                }
                if (bDeleteUSCMedInfo)
                {
                    string sUSCMedRowId = string.Empty;
                    foreach (MedicationInfo objINCMedInfo in objEventPSO.MedInfoList)
                    {
                        if (objINCMedInfo.MedRowId > 0)
                        {
                            sUSCMedRowId = sUSCMedRowId + objINCMedInfo.MedRowId + "|";
                        }
                    }
                    sUSCMedRowId = sUSCMedRowId.TrimEnd('|');
                    if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtUSCMedInfoDeletedId") != null)
                    {
                        objXmlIn.SelectSingleNode("//Event/EventPSOData/txtUSCMedInfoDeletedId").InnerText = sUSCMedRowId;
                    }
                }
                if (bDeleteDeviceInfo)
                {
                    string sDeviceRowId = string.Empty;
                    foreach (DeviceInfo objDeviceInfo in objEventPSO.DeviceInfoList)
                    {
                        if (objDeviceInfo.DeviceRowId > 0)
                        {
                            sDeviceRowId = sDeviceRowId + objDeviceInfo.DeviceRowId + "|";
                        }
                    }
                    sDeviceRowId = sDeviceRowId.TrimEnd('|');
                    if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtDeviceInfoDeletedId") != null)
                    {
                        objXmlIn.SelectSingleNode("//Event/EventPSOData/txtDeviceInfoDeletedId").InnerText = sDeviceRowId;
                    }
                }
                //Ends
                //Get the Medication Grid Selected 
                //This block will handle the deleted row ids of the grids starts
                string sINCMedInfoDeletedId = string.Empty;
                string[] arrINCMEDInfoDeletedId = null;
                if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtINCMedInfoDeletedId") != null)
                {
                    sINCMedInfoDeletedId = objXmlIn.SelectSingleNode("//Event/EventPSOData/txtINCMedInfoDeletedId").InnerText;
                    arrINCMEDInfoDeletedId = sINCMedInfoDeletedId.Split('|');
                }
                for (int iCount = 0; iCount < arrINCMEDInfoDeletedId.Length; iCount++)
                {
                    if (Conversion.CastToType<int>(arrINCMEDInfoDeletedId[iCount], out bIsSuccess) > 0)
                    {
                        lstDeletedINCMedInfoRowId.Add(arrINCMEDInfoDeletedId[iCount]);
                    }
                }
                //Ends

                //***********************************
                //INC Medication Grid--starts
                //*************************************           
                objGridXMLElement = null;
                objListHeadXmlElement = null;
                objGridXMLElement = (XmlElement)objXmlIn.SelectSingleNode("/Event/EventPSO/INCMedInfoList");
                if (objGridXMLElement != null)
                {
                    objGridXMLElement.ParentNode.RemoveChild(objGridXMLElement);
                }
                objGridTempElement = (XmlElement)objXmlIn.SelectSingleNode("/Event/EventPSO");

                objPSOFormManager.CreateElement(objGridTempElement, "INCMedInfoList", ref objGridXMLElement);

                // Create HEADER nodes for Grid.
                objPSOFormManager.CreateElement(objGridXMLElement, "listhead", ref objListHeadXmlElement);

                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "DrugName", "Device Name");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "BrandName", "Brand Type");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "ManufName", "Manufacturer");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "Strength", "Strength or concentration of product");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "Dosage", "Dosage form of product");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "WasMedPresc", "Was Medication/ Substance prescribed?");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "WasMedGiven", "Was Medication/ Substance given?");
                //Populating grid values                
                string sINCMedInfoSessionId = string.Empty;
                objOptionXmlElement = null;
                iIndex = 0;
                Hashtable objINCMedUniqueIds = new Hashtable();

                if (objXmlIn.SelectSingleNode("/Event/EventPSOData/txtINCMedInfoSessionId") != null)
                {
                    sINCMedInfoSessionId = objXmlIn.SelectSingleNode("/Event/EventPSOData/txtINCMedInfoSessionId").InnerText;
                }
                SessionManager objSessionManager = null;
                int iINCMedRowId = -1;
                if (!string.IsNullOrEmpty(sINCMedInfoSessionId))
                {
                    string[] arrINCMEdInfoSessionId = sINCMedInfoSessionId.Split('~');

                    for (int iCount = 0; iCount < arrINCMEdInfoSessionId.Length; iCount++)
                    {
                        if (!string.IsNullOrEmpty(arrINCMEdInfoSessionId[iCount]))
                        {
                            string sSessionRawData = string.Empty;
                            objSessionManager = base.GetSessionObject();
                            sSessionRawData = Riskmaster.Common.Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(arrINCMEdInfoSessionId[iCount])).ToString();

                            XmlDocument objTempXML = new XmlDocument();
                            XmlDocument objPropStore = new XmlDocument();
                            objTempXML.LoadXml(sSessionRawData);
                            objPropStore.LoadXml(objTempXML.SelectSingleNode("MedicationInfo").OuterXml);

                            MedicationInfo objINCMedInfo = (MedicationInfo)objPSOFormManager.DataModelFactory.GetDataModelObject("MedicationInfo", false);
                            XmlNode objDelNode = objPropStore.SelectSingleNode("/MedicationInfo/txtMedInfoSessionId");
                            if (objDelNode != null)
                            {
                                objDelNode.ParentNode.RemoveChild(objDelNode);
                            }

                            int iINCMedInfoRowId = 0;
                            if (objPropStore.SelectSingleNode("/MedicationInfo/MedRowId") != null)
                            {
                                iINCMedInfoRowId = Conversion.CastToType<int>(objPropStore.SelectSingleNode("/MedicationInfo/MedRowId").InnerText, out bIsSuccess);

                            }
                            if (iINCMedInfoRowId == 0)
                            {
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "MedRowId", Convert.ToString(iINCMedRowId));
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "EvtPsoRowId", Convert.ToString(iINCMedRowId));
                                iINCMedRowId = iINCMedRowId - 1;
                                objINCMedInfo.PopulateObject(objPropStore);
                                objEventPSO.MedInfoList.Add(objINCMedInfo, true);
                                objINCMedUniqueIds.Add(objINCMedInfo.MedRowId, arrINCMEdInfoSessionId[iCount]); ;
                            }
                            else
                            {
                                objEventPSO.MedInfoList[iINCMedInfoRowId].PopulateObject(objPropStore);
                                if (!string.IsNullOrEmpty(arrINCMEdInfoSessionId[iCount]))
                                {
                                    objINCMedUniqueIds.Add(objEventPSO.MedInfoList[iINCMedInfoRowId].MedRowId, arrINCMEdInfoSessionId[iCount]);
                                }
                            }
                            objSessionManager = null;
                        }

                    }
                }
                int iINCMedGridCount = 0;
                if (objEventPSO.MedInfoList != null)
                {
                    foreach (MedicationInfo objINCMedInfo in objEventPSO.MedInfoList)
                    {
                        if (lstDeletedINCMedInfoRowId.Contains(Convert.ToString(objINCMedInfo.MedRowId)))
                        {
                            continue;
                        }

                        objPSOFormManager.CreateElement(objGridXMLElement, "option", ref objOptionXmlElement);
                        objOptionXmlElement.SetAttribute("ref", Riskmaster.Application.PSOForm.Constants.INSTANCE_DOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                        string sINCMedShortCode = string.Empty;
                        string sINCMedDesc = string.Empty;
                        string sINCMedType = string.Empty;

                        string sINCMedGivShortCode = string.Empty;
                        string sINCMedGivDesc = string.Empty;
                        string sINCMedGivType = string.Empty;

                        objPSOFormManager.DataModelFactory.Context.LocalCache.GetCodeInfo(objINCMedInfo.MedicationPrescribedCode, ref sINCMedShortCode, ref sINCMedDesc);
                        sINCMedType = sINCMedShortCode + " " + sINCMedDesc;

                        objPSOFormManager.DataModelFactory.Context.LocalCache.GetCodeInfo(objINCMedInfo.MedicationGivenCode, ref sINCMedGivShortCode, ref sINCMedGivDesc);
                        sINCMedGivType = sINCMedGivShortCode + " " + sINCMedGivDesc;

                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "DrugName", objINCMedInfo.DrugName);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "BrandName", objINCMedInfo.BrandName);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "ManufName", objINCMedInfo.ManufacturerName);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "Strength", objINCMedInfo.ProductStrength);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "Dosage", objINCMedInfo.Dosage);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "WasMedPresc", sINCMedType);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "WasMedGiven", sINCMedGivType);

                        string sSessionId = string.Empty;
                        if (objINCMedUniqueIds.ContainsKey(objINCMedInfo.MedRowId))
                        {
                            sSessionId = (string)objINCMedUniqueIds[objINCMedInfo.MedRowId];
                        }
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "SessionId", sSessionId);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "MedRowId", Convert.ToString(objINCMedInfo.MedRowId));
                        iINCMedGridCount++;
                    }

                    objPSOFormManager.CreateElement(objGridXMLElement, "option", ref objOptionXmlElement);
                    objOptionXmlElement.SetAttribute("ref", Riskmaster.Application.PSOForm.Constants.INSTANCE_DOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "DrugName", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "BrandName", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "ManufName", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "Strength", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "Dosage", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "WasMedPresc", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "WasMedGiven", string.Empty);
                }
                if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtINCMedGridCount") != null)
                {
                    objXmlIn.SelectSingleNode("//Event/EventPSOData/txtINCMedGridCount").InnerText = Convert.ToString(iINCMedGridCount);
                }
                //INC Med population Ends
                //***********************************
                //INC Medication Grid--end
                //*************************************
               
                //***********************************
                //NM Medication Grid--starts
                //*************************************  
                //This block will handle the deleted row ids of the grids starts
                string sNMMedInfoDeletedId = string.Empty;
                string[] arrNMMEDInfoDeletedId = null;
                if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtNMMedInfoDeletedId") != null)
                {
                    sNMMedInfoDeletedId = objXmlIn.SelectSingleNode("//Event/EventPSOData/txtNMMedInfoDeletedId").InnerText;
                    arrNMMEDInfoDeletedId = sNMMedInfoDeletedId.Split('|');
                }
                for (int iCount = 0; iCount < arrNMMEDInfoDeletedId.Length; iCount++)
                {
                    if (Conversion.CastToType<int>(arrNMMEDInfoDeletedId[iCount], out bIsSuccess) > 0)
                    {
                        lstDeletedNMMedInfoRowId.Add(arrNMMEDInfoDeletedId[iCount]);
                    }
                }
                //Ends


                objGridXMLElement = null;
                objListHeadXmlElement = null;
                objGridXMLElement = (XmlElement)objXmlIn.SelectSingleNode("/Event/EventPSO/NMMedInfoList");
                if (objGridXMLElement != null)
                {
                    objGridXMLElement.ParentNode.RemoveChild(objGridXMLElement);
                }
                objPSOFormManager.CreateElement(objGridTempElement, "NMMedInfoList", ref objGridXMLElement);

                // Create HEADER nodes for Grid.
                objPSOFormManager.CreateElement(objGridXMLElement, "listhead", ref objListHeadXmlElement);

                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "DrugName", "Device Name");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "BrandName", "Brand Type");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "ManufName", "Manufacturer");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "Strength", "Strength or concentration of product");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "Dosage", "Dosage form of product");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "WasMedPresc", "Was Medication/ Substance prescribed?");
                //Populating grid values                
                string sNMMedInfoSessionId = string.Empty;
                objOptionXmlElement = null;
                iIndex = 0;
                Hashtable objNMMedUniqueIds = new Hashtable();

                if (objXmlIn.SelectSingleNode("/Event/EventPSOData/txtNMMedInfoSessionId") != null)
                {
                    sNMMedInfoSessionId = objXmlIn.SelectSingleNode("/Event/EventPSOData/txtNMMedInfoSessionId").InnerText;
                }
                objSessionManager = null;
                int iNMMedRowId = -1;
                if (!string.IsNullOrEmpty(sNMMedInfoSessionId))
                {
                    string[] arrNMMEdInfoSessionId = sNMMedInfoSessionId.Split('~');

                    for (int iCount = 0; iCount < arrNMMEdInfoSessionId.Length; iCount++)
                    {
                        if (!string.IsNullOrEmpty(arrNMMEdInfoSessionId[iCount]))
                        {
                            string sSessionRawData = string.Empty;
                            objSessionManager = base.GetSessionObject();
                            sSessionRawData = Riskmaster.Common.Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(arrNMMEdInfoSessionId[iCount])).ToString();

                            XmlDocument objTempXML = new XmlDocument();
                            XmlDocument objPropStore = new XmlDocument();
                            objTempXML.LoadXml(sSessionRawData);
                            objPropStore.LoadXml(objTempXML.SelectSingleNode("MedicationInfo").OuterXml);

                            MedicationInfo objNMMedInfo = (MedicationInfo)objPSOFormManager.DataModelFactory.GetDataModelObject("MedicationInfo", false);
                            XmlNode objDelNode = objPropStore.SelectSingleNode("/MedicationInfo/txtMedInfoSessionId");
                            if (objDelNode != null)
                            {
                                objDelNode.ParentNode.RemoveChild(objDelNode);
                            }

                            int iNMMedInfoRowId = 0;
                            if (objPropStore.SelectSingleNode("/MedicationInfo/MedRowId") != null)
                            {
                                iNMMedInfoRowId = Conversion.CastToType<int>(objPropStore.SelectSingleNode("/MedicationInfo/MedRowId").InnerText, out bIsSuccess);

                            }
                            if (iNMMedInfoRowId == 0)
                            {
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "MedRowId", Convert.ToString(iNMMedRowId));
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "EvtPsoRowId", Convert.ToString(iNMMedRowId));
                                iNMMedRowId = iNMMedRowId - 1;
                                objNMMedInfo.PopulateObject(objPropStore);
                                objEventPSO.MedInfoList.Add(objNMMedInfo, true);
                                objNMMedUniqueIds.Add(objNMMedInfo.MedRowId, arrNMMEdInfoSessionId[iCount]); ;
                            }
                            else
                            {
                                objEventPSO.MedInfoList[iNMMedInfoRowId].PopulateObject(objPropStore);
                                if (!string.IsNullOrEmpty(arrNMMEdInfoSessionId[iCount]))
                                {
                                    objNMMedUniqueIds.Add(objEventPSO.MedInfoList[iNMMedInfoRowId].MedRowId, arrNMMEdInfoSessionId[iCount]);
                                }
                            }
                            
                            objSessionManager = null;
                        }

                    }
                }

                int iNMMedGridCount = 0;
                if (objEventPSO.MedInfoList != null)
                {
                    foreach (MedicationInfo objNMMedInfo in objEventPSO.MedInfoList)
                    {
                        if (lstDeletedNMMedInfoRowId.Contains(Convert.ToString(objNMMedInfo.MedRowId)))
                        {
                            continue;
                        }
                        objPSOFormManager.CreateElement(objGridXMLElement, "option", ref objOptionXmlElement);
                        objOptionXmlElement.SetAttribute("ref", Riskmaster.Application.PSOForm.Constants.INSTANCE_DOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                        string sNMMedShortCode = string.Empty;
                        string sNMMedDesc = string.Empty;
                        string sNMMedType = string.Empty;

                        objPSOFormManager.DataModelFactory.Context.LocalCache.GetCodeInfo(objNMMedInfo.MedicationPrescribedCode, ref sNMMedShortCode, ref sNMMedDesc);

                        sNMMedType = sNMMedShortCode + " " + sNMMedDesc;

                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "DrugName", objNMMedInfo.DrugName);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "BrandName", objNMMedInfo.BrandName);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "ManufName", objNMMedInfo.ManufacturerName);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "Strength", objNMMedInfo.ProductStrength);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "Dosage", objNMMedInfo.Dosage);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "WasMedPresc", sNMMedType);

                        string sSessionId = string.Empty;
                        if (objNMMedUniqueIds.ContainsKey(objNMMedInfo.MedRowId))
                        {
                            sSessionId = (string)objNMMedUniqueIds[objNMMedInfo.MedRowId];
                        }
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "SessionId", sSessionId);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "MedRowId", Convert.ToString(objNMMedInfo.MedRowId));
                        iNMMedGridCount++;
                    }

                    objPSOFormManager.CreateElement(objGridXMLElement, "option", ref objOptionXmlElement);
                    objOptionXmlElement.SetAttribute("ref", Riskmaster.Application.PSOForm.Constants.INSTANCE_DOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "DrugName", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "BrandName", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "ManufName", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "Strength", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "Dosage", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "WasMedPresc", string.Empty);
                }
                if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtNMMedGridCount") != null)
                {
                    objXmlIn.SelectSingleNode("//Event/EventPSOData/txtNMMedGridCount").InnerText = Convert.ToString(iNMMedGridCount);
                }
                //NM Med population Ends
                //***********************************
                //NM Medication Grid--end
                //*************************************

                //***********************************
                //USC Medication Grid--starts
                //*************************************       
                //This block will handle the deleted row ids of the grids starts
                string sUSCMedInfoDeletedId = string.Empty;
                string[] arrUSCMEDInfoDeletedId = null;
                if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtUSCMedInfoDeletedId") != null)
                {
                    sUSCMedInfoDeletedId = objXmlIn.SelectSingleNode("//Event/EventPSOData/txtUSCMedInfoDeletedId").InnerText;
                    arrUSCMEDInfoDeletedId = sUSCMedInfoDeletedId.Split('|');
                }
                for (int iCount = 0; iCount < arrUSCMEDInfoDeletedId.Length; iCount++)
                {
                    if (Conversion.CastToType<int>(arrUSCMEDInfoDeletedId[iCount], out bIsSuccess) > 0)
                    {
                        lstDeletedUSCMedInfoRowId.Add(arrUSCMEDInfoDeletedId[iCount]);
                    }
                }
                //Ends
                objGridXMLElement = null;
                objListHeadXmlElement = null;
                objGridXMLElement = (XmlElement)objXmlIn.SelectSingleNode("/Event/EventPSO/USCMedInfoList");
                if (objGridXMLElement != null)
                {
                    objGridXMLElement.ParentNode.RemoveChild(objGridXMLElement);
                }
                objPSOFormManager.CreateElement(objGridTempElement, "USCMedInfoList", ref objGridXMLElement);

                // Create HEADER nodes for Grid.
                objPSOFormManager.CreateElement(objGridXMLElement, "listhead", ref objListHeadXmlElement);
                
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "DrugName", "Device Name");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "BrandName", "Brand Type");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "ManufName", "Manufacturer");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "Strength", "Strength or concentration of product");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "Dosage", "Dosage form of product");
               
                //Populating grid values                
                string sUSCMedInfoSessionId = string.Empty;
                objOptionXmlElement = null;
                iIndex = 0;
                Hashtable objUSCMedUniqueIds = new Hashtable();

                if (objXmlIn.SelectSingleNode("/Event/EventPSOData/txtUSCMedInfoSessionId") != null)
                {
                    sUSCMedInfoSessionId = objXmlIn.SelectSingleNode("/Event/EventPSOData/txtUSCMedInfoSessionId").InnerText;
                }
                objSessionManager = null;
                int iUSCMedRowId = -1;
                if (!string.IsNullOrEmpty(sUSCMedInfoSessionId))
                {
                    string[] arrUSCMEdInfoSessionId = sUSCMedInfoSessionId.Split('~');

                    for (int iCount = 0; iCount < arrUSCMEdInfoSessionId.Length; iCount++)
                    {
                        if (!string.IsNullOrEmpty(arrUSCMEdInfoSessionId[iCount]))
                        {
                            string sSessionRawData = string.Empty;
                            objSessionManager = base.GetSessionObject();
                            sSessionRawData = Riskmaster.Common.Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(arrUSCMEdInfoSessionId[iCount])).ToString();

                            XmlDocument objTempXML = new XmlDocument();
                            XmlDocument objPropStore = new XmlDocument();
                            objTempXML.LoadXml(sSessionRawData);
                            objPropStore.LoadXml(objTempXML.SelectSingleNode("MedicationInfo").OuterXml);

                            MedicationInfo objUSCMedInfo = (MedicationInfo)objPSOFormManager.DataModelFactory.GetDataModelObject("MedicationInfo", false);
                            XmlNode objDelNode = objPropStore.SelectSingleNode("/MedicationInfo/txtMedInfoSessionId");
                            if (objDelNode != null)
                            {
                                objDelNode.ParentNode.RemoveChild(objDelNode);
                            }

                            int iUSCMedInfoRowId = 0;
                            if (objPropStore.SelectSingleNode("/MedicationInfo/MedRowId") != null)
                            {
                                iUSCMedInfoRowId = Conversion.CastToType<int>(objPropStore.SelectSingleNode("/MedicationInfo/MedRowId").InnerText, out bIsSuccess);

                            }
                            if (iUSCMedInfoRowId == 0)
                            {
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "MedRowId", Convert.ToString(iUSCMedRowId));
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "EvtPsoRowId", Convert.ToString(iUSCMedRowId));
                                iUSCMedRowId = iUSCMedRowId - 1;
                                objUSCMedInfo.PopulateObject(objPropStore);
                                objEventPSO.MedInfoList.Add(objUSCMedInfo, true);
                                objUSCMedUniqueIds.Add(objUSCMedInfo.MedRowId, arrUSCMEdInfoSessionId[iCount]); ;
                            }
                            else
                            {
                                objEventPSO.MedInfoList[iUSCMedInfoRowId].PopulateObject(objPropStore);
                                if (!string.IsNullOrEmpty(arrUSCMEdInfoSessionId[iCount]))
                                {
                                    objUSCMedUniqueIds.Add(objEventPSO.MedInfoList[iUSCMedInfoRowId].MedRowId, arrUSCMEdInfoSessionId[iCount]);
                                }
                            }

                            objSessionManager = null;
                        }

                    }
                }

                int iUSCMedGridCount = 0;
                if (objEventPSO.MedInfoList != null)
                {
                    foreach (MedicationInfo objUSCMedInfo in objEventPSO.MedInfoList)
                    {
                        if (lstDeletedUSCMedInfoRowId.Contains(Convert.ToString(objUSCMedInfo.MedRowId)))
                        {
                            continue;
                        }
                        objPSOFormManager.CreateElement(objGridXMLElement, "option", ref objOptionXmlElement);
                        objOptionXmlElement.SetAttribute("ref", Riskmaster.Application.PSOForm.Constants.INSTANCE_DOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "DrugName", objUSCMedInfo.DrugName);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "BrandName", objUSCMedInfo.BrandName);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "ManufName", objUSCMedInfo.ManufacturerName);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "Strength", objUSCMedInfo.ProductStrength);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "Dosage", objUSCMedInfo.Dosage);

                        string sSessionId = string.Empty;
                        if (objUSCMedUniqueIds.ContainsKey(objUSCMedInfo.MedRowId))
                        {
                            sSessionId = (string)objUSCMedUniqueIds[objUSCMedInfo.MedRowId];
                        }
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "SessionId", sSessionId);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "MedRowId", Convert.ToString(objUSCMedInfo.MedRowId));
                        iUSCMedGridCount++;
                    }

                    objPSOFormManager.CreateElement(objGridXMLElement, "option", ref objOptionXmlElement);
                    objOptionXmlElement.SetAttribute("ref", Riskmaster.Application.PSOForm.Constants.INSTANCE_DOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "DrugName", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "BrandName", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "ManufName", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "Strength", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "Dosage", string.Empty);
                }
                if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtUSCMedGridCount") != null)
                {
                    objXmlIn.SelectSingleNode("//Event/EventPSOData/txtUSCMedGridCount").InnerText = Convert.ToString(iUSCMedGridCount);
                }
                //USC Med population Ends
                //***********************************
                //USC Medication Grid--end
                //*************************************

                //***********************************
                //Linked Event Grid--starts
                //*************************************
                //This block will handle the deleted row ids of the grids starts
                string sLnkEvtInfoDeletedId = string.Empty;
                string[] arrLnkEvtInfoDeletedId = null;
                if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtLnkEvtInfoDeletedId") != null)
                {
                    sLnkEvtInfoDeletedId = objXmlIn.SelectSingleNode("//Event/EventPSOData/txtLnkEvtInfoDeletedId").InnerText;
                    arrLnkEvtInfoDeletedId = sLnkEvtInfoDeletedId.Split('|');
                }
                for (int iCount = 0; iCount < arrLnkEvtInfoDeletedId.Length; iCount++)
                {
                    if (Conversion.CastToType<int>(arrLnkEvtInfoDeletedId[iCount], out bIsSuccess) > 0)
                    {
                        lstDeletedLnkEvtRowId.Add(arrLnkEvtInfoDeletedId[iCount]);
                    }
                }
                //Ends
                objGridXMLElement = null;
                objListHeadXmlElement = null;
                objGridXMLElement = (XmlElement)objXmlIn.SelectSingleNode("/Event/EventPSO/LinkedEventInfoList");
                if (objGridXMLElement != null)
                {
                    objGridXMLElement.ParentNode.RemoveChild(objGridXMLElement);
                }
                objPSOFormManager.CreateElement(objGridTempElement, "LinkedEventInfoList", ref objGridXMLElement);

                // Create HEADER nodes for Grid.
                objPSOFormManager.CreateElement(objGridXMLElement, "listhead", ref objListHeadXmlElement);

                //Header values
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "LinkedEventNumber", "Linked Event Number");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "LinkingReason", "Reason For Linking");

                //Linked Event Grid Populate starts
                string sLnkEvtInfoSessionId = string.Empty;
                objOptionXmlElement = null;
                iIndex = 0;
                Hashtable objLnkEvtUniqueIds = new Hashtable();

                if (objXmlIn.SelectSingleNode("/Event/EventPSOData/txtLnkEvtInfoSessionId") != null)
                {
                    sLnkEvtInfoSessionId = objXmlIn.SelectSingleNode("/Event/EventPSOData/txtLnkEvtInfoSessionId").InnerText;
                }
                objSessionManager = null;
                int iLnkEvtRowId = -1;
                if (!string.IsNullOrEmpty(sLnkEvtInfoSessionId))
                {
                    string[] arrLnkEvtInfoSessionId = sLnkEvtInfoSessionId.Split('~');

                    for (int iCount = 0; iCount < arrLnkEvtInfoSessionId.Length; iCount++)
                    {
                        if (!string.IsNullOrEmpty(arrLnkEvtInfoSessionId[iCount]))
                        {
                            string sSessionRawData = string.Empty;
                            objSessionManager = base.GetSessionObject();
                            sSessionRawData = Riskmaster.Common.Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(arrLnkEvtInfoSessionId[iCount])).ToString();

                            XmlDocument objTempXML = new XmlDocument();
                            XmlDocument objPropStore = new XmlDocument();
                            objTempXML.LoadXml(sSessionRawData);
                            objPropStore.LoadXml(objTempXML.SelectSingleNode("LinkedEventInfo").OuterXml);

                            LinkedEventInfo objLnkEvtInfo = (LinkedEventInfo)objPSOFormManager.DataModelFactory.GetDataModelObject("LinkedEventInfo", false);
                            XmlNode objDelNode = objPropStore.SelectSingleNode("/LinkedEventInfo/txtLnkEvtInfoSessionId");

                            if (objDelNode != null)
                            {
                                objDelNode.ParentNode.RemoveChild(objDelNode);
                            }

                            int iLnkEventRowId = 0;
                            if (objPropStore.SelectSingleNode("/LinkedEventInfo/LnkEventRowId") != null)
                            {
                                iLnkEventRowId = Conversion.CastToType<int>(objPropStore.SelectSingleNode("/LinkedEventInfo/LnkEventRowId").InnerText, out bIsSuccess);

                            }
                            if (iLnkEventRowId == 0)
                            {
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "LnkEventRowId", Convert.ToString(iLnkEvtRowId));
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "EvtPsoRowId", Convert.ToString(iLnkEvtRowId));
                                iLnkEvtRowId = iLnkEvtRowId - 1;
                                objLnkEvtInfo.PopulateObject(objPropStore);
                                objEventPSO.LinkedEventInfoList.Add(objLnkEvtInfo, true);
                                objLnkEvtUniqueIds.Add(objLnkEvtInfo.LnkEventRowId, arrLnkEvtInfoSessionId[iCount]);
                            }
                            else
                            {
                                objEventPSO.LinkedEventInfoList[iLnkEventRowId].PopulateObject(objPropStore);
                                if (!string.IsNullOrEmpty(arrLnkEvtInfoSessionId[iCount]))
                                {
                                    objLnkEvtUniqueIds.Add(objEventPSO.LinkedEventInfoList[iLnkEventRowId].LnkEventRowId, arrLnkEvtInfoSessionId[iCount]);
                                }

                            }
                            objSessionManager = null;
                        }

                    }
                }

                int iLnkEvtGridCount = 0;
                if (objEventPSO.LinkedEventInfoList != null)
                {
                    foreach (LinkedEventInfo objLnkEvtInfo in objEventPSO.LinkedEventInfoList)
                    {
                        if (lstDeletedLnkEvtRowId.Contains(Convert.ToString(objLnkEvtInfo.LnkEventRowId)))
                        {
                            continue;
                        }
                        objPSOFormManager.CreateElement(objGridXMLElement, "option", ref objOptionXmlElement);
                        objOptionXmlElement.SetAttribute("ref", Riskmaster.Application.PSOForm.Constants.INSTANCE_DOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                        string sReasonShortCode = string.Empty;
                        string sReasonDesc = string.Empty;
                        string sReasonType = string.Empty;

                        objPSOFormManager.DataModelFactory.Context.LocalCache.GetCodeInfo(objLnkEvtInfo.EventLnkReasonCode, ref sReasonShortCode, ref sReasonDesc);

                        sReasonType = sReasonShortCode + " " + sReasonDesc;

                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "LinkedEventNumber", objLnkEvtInfo.LnkEventNumber);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "LinkingReason", sReasonType);
                        string sSessionId = string.Empty;
                        if (objLnkEvtUniqueIds.ContainsKey(objLnkEvtInfo.LnkEventRowId))
                        {
                            sSessionId = (string)objLnkEvtUniqueIds[objLnkEvtInfo.LnkEventRowId];
                        }
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "SessionId", sSessionId);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "LnkEventRowId", Convert.ToString(objLnkEvtInfo.LnkEventRowId));
                        iLnkEvtGridCount++;
                    }

                    objPSOFormManager.CreateElement(objGridXMLElement, "option", ref objOptionXmlElement);
                    objOptionXmlElement.SetAttribute("ref", Riskmaster.Application.PSOForm.Constants.INSTANCE_DOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "LinkedEventNumber", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "LinkingReason", string.Empty);
                    
                }
                if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtLnkEvtGridCount") != null)
                {
                    objXmlIn.SelectSingleNode("//Event/EventPSOData/txtLnkEvtGridCount").InnerText = Convert.ToString(iLnkEvtGridCount);
                }
                //Linked population Ends
                //***********************************
                //Linked Event Grid--end
                //*************************************

                //***********************************
                //Device Grid--starts
                //*************************************    
                //This block will handle the deleted row ids of the grids starts
                string sDeviceInfoDeletedId = string.Empty;
                string[] arrDeviceInfoDeletedId = null;
                if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtDeviceInfoDeletedId") != null)
                {
                    sDeviceInfoDeletedId = objXmlIn.SelectSingleNode("//Event/EventPSOData/txtDeviceInfoDeletedId").InnerText;
                    arrDeviceInfoDeletedId = sDeviceInfoDeletedId.Split('|');
                }
                for (int iCount = 0; iCount < arrDeviceInfoDeletedId.Length; iCount++)
                {
                    if (Conversion.CastToType<int>(arrDeviceInfoDeletedId[iCount], out bIsSuccess) > 0)
                    {
                        lstDeletedDeviceInfoRowId.Add(arrDeviceInfoDeletedId[iCount]);
                    }
                }
                //Ends

                objGridXMLElement = null;
                objListHeadXmlElement = null;
                objGridXMLElement = (XmlElement)objXmlIn.SelectSingleNode("/Event/EventPSO/DevicesInfoList");
                if (objGridXMLElement != null)
                {
                    objGridXMLElement.ParentNode.RemoveChild(objGridXMLElement);
                }
                objPSOFormManager.CreateElement(objGridTempElement, "DevicesInfoList", ref objGridXMLElement);

                // Create HEADER nodes for Grid.
                objPSOFormManager.CreateElement(objGridXMLElement, "listhead", ref objListHeadXmlElement);


                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "DevType", "Device Type");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "DevName", "Device Name");
                objPSOFormManager.CreateAndSetElement(objListHeadXmlElement, "ManufName", "Manufacturer Name");
                //***********************************
                //Populating grid values
                //*************************************
                //txtDeviceInfoSessionId
                string sDeviceInfoSessionId = string.Empty;
                objOptionXmlElement = null;
                iIndex = 0;
                Hashtable objDevicesUniqueIds = new Hashtable();

                if (objXmlIn.SelectSingleNode("/Event/EventPSOData/txtDeviceInfoSessionId") != null)
                {
                    sDeviceInfoSessionId = objXmlIn.SelectSingleNode("/Event/EventPSOData/txtDeviceInfoSessionId").InnerText;
                }
                objSessionManager = null;
                int iDeviceRowId = -1;
                if (!string.IsNullOrEmpty(sDeviceInfoSessionId))
                {
                    string[] arrDeviceInfoSessionId = sDeviceInfoSessionId.Split('~');

                    for (int iCount = 0; iCount < arrDeviceInfoSessionId.Length; iCount++)
                    {
                        if (!string.IsNullOrEmpty(arrDeviceInfoSessionId[iCount]))
                        {
                            string sSessionRawData = string.Empty;
                            objSessionManager = base.GetSessionObject();
                            sSessionRawData = Riskmaster.Common.Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(arrDeviceInfoSessionId[iCount])).ToString();
                            
                            XmlDocument objTempXML = new XmlDocument();
                            XmlDocument objPropStore = new XmlDocument();
                            objTempXML.LoadXml(sSessionRawData);
                            objPropStore.LoadXml(objTempXML.SelectSingleNode("DeviceInfo").OuterXml);

                            DeviceInfo objDeviceInfo = (DeviceInfo)objPSOFormManager.DataModelFactory.GetDataModelObject("DeviceInfo",false);
                            XmlNode objDelNode = objPropStore.SelectSingleNode("/DeviceInfo/txtDeviceInfoSessionId");
                            if (objDelNode != null)
                            {
                                objDelNode.ParentNode.RemoveChild(objDelNode);
                            }

                            int iDeviceInfoRowId = 0;
                            if (objPropStore.SelectSingleNode("/DeviceInfo/DeviceRowId") != null)
                            {
                                iDeviceInfoRowId = Conversion.CastToType<int>(objPropStore.SelectSingleNode("/DeviceInfo/DeviceRowId").InnerText, out bIsSuccess);

                            }
                            if (iDeviceInfoRowId == 0)
                            {
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "DeviceRowId", Convert.ToString(iDeviceRowId));
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "EvtPsoRowId", Convert.ToString(iDeviceRowId));
                                iDeviceRowId = iDeviceRowId - 1;
                                objDeviceInfo.PopulateObject(objPropStore);
                                objEventPSO.DeviceInfoList.Add(objDeviceInfo, true);
                                objDevicesUniqueIds.Add(objDeviceInfo.DeviceRowId, arrDeviceInfoSessionId[iCount]); ;
                            }
                            else
                            {
                                objEventPSO.DeviceInfoList[iDeviceInfoRowId].PopulateObject(objPropStore);
                                if (!string.IsNullOrEmpty(arrDeviceInfoSessionId[iCount]))
                                {
                                    objDevicesUniqueIds.Add(objEventPSO.DeviceInfoList[iDeviceInfoRowId].DeviceRowId, arrDeviceInfoSessionId[iCount]);
                                }
                            }

                            objSessionManager = null;
                        }

                    }
                }

                int iDeviceGridCount = 0;
                if (objEventPSO.DeviceInfoList != null)
                {
                    foreach (DeviceInfo objDeviceInfo in objEventPSO.DeviceInfoList)
                    {
                        if (lstDeletedDeviceInfoRowId.Contains(Convert.ToString(objDeviceInfo.DeviceRowId)))
                        {
                            continue;
                        }
                        objPSOFormManager.CreateElement(objGridXMLElement, "option", ref objOptionXmlElement);
                        objOptionXmlElement.SetAttribute("ref", Riskmaster.Application.PSOForm.Constants.INSTANCE_DOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                        string sDevShortCode = string.Empty;
                        string sDevDesc = string.Empty;
                        string sDevType = string.Empty;

                        objPSOFormManager.DataModelFactory.Context.LocalCache.GetCodeInfo(objDeviceInfo.DeviceType, ref sDevShortCode, ref sDevDesc);

                        sDevType = sDevShortCode + " " + sDevDesc; 

                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "DevType", sDevType);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "DevName", objDeviceInfo.DeviceBrandName);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "ManufName", objDeviceInfo.DevManfName);
                        string sSessionId = string.Empty;
                        if (objDevicesUniqueIds.ContainsKey(objDeviceInfo.DeviceRowId))
                        {
                            sSessionId = (string)objDevicesUniqueIds[objDeviceInfo.DeviceRowId];
                        }
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "SessionId", sSessionId);
                        objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "DeviceRowId", Convert.ToString(objDeviceInfo.DeviceRowId));
                        iDeviceGridCount++;
                    }

                    objPSOFormManager.CreateElement(objGridXMLElement, "option", ref objOptionXmlElement);
                    objOptionXmlElement.SetAttribute("ref", Riskmaster.Application.PSOForm.Constants.INSTANCE_DOCUMENT_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "DevType", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "DevName", string.Empty);
                    objPSOFormManager.CreateAndSetElement(objOptionXmlElement, "ManufName", string.Empty);
                }
                if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtDeviceGridCount") != null)
                {
                    objXmlIn.SelectSingleNode("//Event/EventPSOData/txtDeviceGridCount").InnerText = Convert.ToString(iDeviceGridCount);
                }
                //Device population Ends
                //***********************************
                //Device Grid--end
                //*************************************

                //***************************TODO
                
                //**********************************

                //objXmlElement = p_objXmlOut.CreateElement("HiddenFieldList");
                //p_objXmlOut.AppendChild(objXmlElement);

                //objXmlElement = p_objXmlOut.CreateElement("HiddenControls");
                //objXmlElement.InnerText = sHiddenFields;
                //p_objXmlOut.FirstChild.AppendChild(objXmlElement);
                //Commented Anu Tennyson : If the code table is to be converted to drop down uncomment this code
                //objPSOFormManager.GetCodes(arrTableNames, ref dicCodeList, ref dicControlList);


                //foreach (string sKeys in dicCodeList.Keys)
                //{
                //    List<string> lstControls = new List<string>();
                //    lstControls = (List<string>)dicControlList[sKeys];

                //    List<CodeList> lstCodeList = new List<CodeList>();
                //    lstCodeList = (List<CodeList>)dicCodeList[sKeys];

                //    for (int iCount = 0; iCount < lstControls.Count; iCount++)
                //    {
                //        string sElementName = lstControls[iCount];
                //        XmlElement objCodeListEle = null;
                //        XmlElement objCodeEle = null;
                //        XmlElement objOptions = null;
                //        objPSOFormManager.CreateElement(objXmlIn.DocumentElement, sElementName, ref objCodeListEle);
                //        objPSOFormManager.CreateElement(objCodeListEle, "CodeList", ref objCodeEle);
                //        objPSOFormManager.CreateElement(objCodeEle, "option", ref objOptions);
                //        objOptions.SetAttribute("value", string.Empty);
                //        XmlCDataSection objCData = null;
                //        for (int iCodeCount = 0; iCodeCount < lstCodeList.Count; iCodeCount++)
                //        {
                //            CodeList objCodeList = new CodeList();
                //            objCodeList = lstCodeList[iCodeCount];
                //            objCodeListEle = null;
                //            //objCodeEle = null;
                //            objOptions = null;
                //            objPSOFormManager.CreateElement(objCodeEle, "option", ref objOptions);
                //            objCData = objXmlIn.CreateCDataSection(objPSOFormManager.LocalCache.GetShortCode(objCodeList.CodeId) + " " + objPSOFormManager.LocalCache.GetCodeDesc(objCodeList.CodeId));
                //            objOptions.AppendChild(objCData);
                //            objOptions.SetAttribute("value", objCodeList.CodeId + "," + objPSOFormManager.LocalCache.GetShortCode(objCodeList.CodeId));
                //        }
                //    }

                //}
                //if (iPatPsoRowId > 0)
                //{
                //    if (bPatientMismatch)
                //    {
                //        if (enumErrorType == ErrorType.PatientMisMatch)
                //        {
                //            objErrOut.Add(Globalization.GetString("PSOFormAdaptor.PatientMismatch.Error"), Globalization.GetString("PSOFormAdaptor.PatientMismatch.Error"), BusinessAdaptorErrorType.Warning);
                //        }
                //        else if (enumErrorType == ErrorType.PatientCountInsuffienct)
                //        {
                //            string sPatientCount = string.Empty;
                //            string sEventReported = string.Empty;
                //            string sEventCategory = string.Empty;
                //            if (objXmlIn.SelectSingleNode("//Event/PatientCount") != null)
                //            {
                //                sPatientCount = objXmlIn.SelectSingleNode("//Event/PatientCount").InnerText;
                //            }
                //            string[] arrEventReported = null;
                //            if (objXmlIn.SelectSingleNode("//Event/EventPSO/psocdWhatReported") != null)
                //            {
                //                sEventReported = objXmlIn.SelectSingleNode("//Event/EventPSO/psocdWhatReported").InnerText;
                //                arrEventReported = sEventReported.Split(' ');
                //            }
                //            string[] arrEventCategory = null;
                //            if (objXmlIn.SelectSingleNode("//Event/PatientPSO/psocdAffectedByEventBirthing") != null)
                //            {
                //                sEventCategory = objXmlIn.SelectSingleNode("//Event/PatientPSO/psocdAffectedByEventBirthing").InnerText;
                //                arrEventCategory = sEventCategory.Split(' ');
                //            }
                            
                //            if (string.IsNullOrEmpty(sPatientCount) || sPatientCount == "0")
                //            {
                //                if (string.Compare(arrEventReported[0], "A3", true) == 0)
                //                {
                //                    objPatientInfo.Delete();
                //                    objErrOut.Add(Globalization.GetString("PSOFormAdaptor.PatientMismatch.Error"), Globalization.GetString("PSOFormAdaptor.NoPatient.Error"), BusinessAdaptorErrorType.Error);
                //                }
                //            }
                //            else if (string.Compare(sPatientCount, "1") == 0)
                //            {
                //                if (string.Compare(arrEventCategory[0], "A1524", true) == 0)
                //                {
                //                    objPatientInfo.Delete();
                //                    objErrOut.Add(Globalization.GetString("PSOFormAdaptor.PatientMismatch.Error"), Globalization.GetString("PSOFormAdaptor.PatientCountInsuff.Error"), BusinessAdaptorErrorType.Error);
                //                }
                //            }
                            
                //        }
                        
                //    }   
                //}
                //Resetting the values
                if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtGridDataDeleted") != null)
                {
                    objXmlIn.SelectSingleNode("//Event/EventPSOData/txtGridDataDeleted").InnerText = string.Empty;
                }
                objXmlOut = objXmlIn;


            }
            catch (RMAppException p_objException)
            {
                objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                objErrOut.Add(p_objException, Globalization.GetString("PSOFormAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objEventPSO != null)
                {
                    objEventPSO.Dispose();
                }
                objGridXMLElement = null;
                objListHeadXmlElement = null;
                objOptionXmlElement = null;
                objGridTempElement = null;
                if (objPSOFormManager != null)
                {
                    objPSOFormManager.Dispose();
                }
                lstHiddenControl = null;
                lstRemoveHiddenControls = null;
                lstDeletedLnkEvtRowId = null;
                dicCodeList = null;
                dicControlList = null;
                if (objPatientInfo != null)
                {
                    objPatientInfo.Dispose();
                }
            }
            return true;
        }
        private void DeletePatient(XmlDocument objXmlIn, int iPatPsoRowId,PatientPSO objPatientInfo, ref BusinessAdaptorErrors objErrOut)
        {
            string sPatientCount = string.Empty;
            string sEventReported = string.Empty;
            string sEventCategory = string.Empty;
            string sEffectByBirthing = string.Empty;
            string[] arrEffectByBirthing = null;
            string sEffectByShortCode = string.Empty;


            try
            {
                if (iPatPsoRowId != 0)
                {
                    sEffectByShortCode = objPatientInfo.Context.LocalCache.GetShortCode(objPatientInfo.psocdAffectedByEventBirthing);
                }
                else if (objXmlIn.SelectSingleNode("//Event/PatientPSO/psocdAffectedByEventBirthing") != null)
                {
                    sEffectByBirthing = objXmlIn.SelectSingleNode("//Event/PatientPSO/psocdAffectedByEventBirthing").InnerText;
                    arrEffectByBirthing = sEventCategory.Split(' ');
                    sEffectByShortCode = arrEffectByBirthing[0];
                }
                if (objXmlIn.SelectSingleNode("//Event/PatientCount") != null)
                {
                    sPatientCount = objXmlIn.SelectSingleNode("//Event/PatientCount").InnerText;
                }
                string[] arrEventReported = null;
                if (objXmlIn.SelectSingleNode("//Event/EventPSO/psocdWhatReported") != null)
                {
                    sEventReported = objXmlIn.SelectSingleNode("//Event/EventPSO/psocdWhatReported").InnerText;
                    arrEventReported = sEventReported.Split(' ');
                }
                

                if (string.IsNullOrEmpty(sPatientCount) || sPatientCount == "0")
                {
                    if (string.Compare(arrEventReported[0], "A3", true) == 0)
                    {
                        if (iPatPsoRowId != 0)
                        {
                            objPatientInfo.Delete();
                        }
                        objErrOut.Add(Globalization.GetString("PSOFormAdaptor.PatientMismatch.Error", base.ClientId), Globalization.GetString("PSOFormAdaptor.NoPatient.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                    }
                }
                else if (string.Compare(sPatientCount, "1") == 0)
                {
                    if (string.Compare(sEffectByShortCode, "A1524", true) == 0)
                    {
                        if (iPatPsoRowId != 0)
                        {
                            objPatientInfo.Delete();
                        }
                        objErrOut.Add(Globalization.GetString("PSOFormAdaptor.PatientMismatch.Error",base.ClientId), Globalization.GetString("PSOFormAdaptor.PatientCountInsuff.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                    }
                }
            }
            catch (RMAppException objEx)
            {
                throw objEx;
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("PSOFormAdaptor.DeletePatient.Error",base.ClientId), objEx);
            }
        }

        private void CheckPatientInfo(XmlDocument objXmlIn, PatientPSO objPatientInfo, int iPatPsoRowId, out ErrorType enumErrorType, ref bool bMistMatch)
        {
            bool bIsSuccess = false;
            string sCurrPatientEID = string.Empty;
            string sCurrPatientNeoEID = string.Empty;
            string sPatientEidAdded = string.Empty;
            string sPatientNeoEidAdded = string.Empty;
            string sEffectByBirthing = string.Empty;
            int iPatientCount = 0;
            int iPrevPatientCount = 0;

            try
            {
                sEffectByBirthing = objPatientInfo.Context.LocalCache.GetShortCode(objPatientInfo.psocdAffectedByEventBirthing);
                if (string.Compare(sEffectByBirthing, "A1524", true) != 0)
                {
                    if (objXmlIn.SelectSingleNode("//Event/PatientPSO/PatInvNeoEntId") != null)
                    {
                        objXmlIn.SelectSingleNode("//Event/PatientPSO/PatInvNeoEntId").InnerText = string.Empty;
                    }
                }
                if (objXmlIn.SelectSingleNode("//Event/PatientCount") != null)
                {
                    iPatientCount = Conversion.CastToType<int>(objXmlIn.SelectSingleNode("//Event/PatientCount").InnerText, out bIsSuccess);
                }
                if (objXmlIn.SelectSingleNode("//Event/PatientPSO/PatInvEntId") != null)
                {
                    sCurrPatientEID = objXmlIn.SelectSingleNode("//Event/PatientPSO/PatInvEntId").InnerText;
                }
                if (objXmlIn.SelectSingleNode("//Event/PatientPSO/PatInvNeoEntId") != null)
                {
                    sCurrPatientNeoEID = objXmlIn.SelectSingleNode("//Event/PatientPSO/PatInvNeoEntId").InnerText;
                }
                sPatientEidAdded = Convert.ToString(objPatientInfo.PatInvEntId);
                sPatientNeoEidAdded = Convert.ToString(objPatientInfo.PatInvNeoEntId);
                if (sCurrPatientEID == "0")
                {
                    sCurrPatientEID = string.Empty;
                }
                if (sCurrPatientNeoEID == "0")
                {
                    sCurrPatientNeoEID = string.Empty;
                }
                if (sPatientEidAdded == "0")
                {
                    sPatientEidAdded = string.Empty;
                }
                if (sPatientNeoEidAdded == "0")
                {
                    sPatientNeoEidAdded = string.Empty;
                }

                if (!string.IsNullOrEmpty(sPatientEidAdded))
                {
                    iPrevPatientCount++;
                }
                if (!string.IsNullOrEmpty(sPatientNeoEidAdded))
                {
                    iPrevPatientCount++;
                }

                if (iPatientCount == 1)
                {
                    if (iPrevPatientCount != iPatientCount)
                    {
                        enumErrorType = ErrorType.PatientCountInsuffienct;
                        bMistMatch = true;
                        return;
                    }
                    if ((!string.IsNullOrEmpty(sCurrPatientEID) && !string.IsNullOrEmpty(sPatientEidAdded)))
                    {
                        if (string.Compare(sCurrPatientEID, sPatientEidAdded, true) != 0)
                        {
                            enumErrorType = ErrorType.PatientMisMatch;
                            bMistMatch = true;
                            return;
                        }
                    }
                }
                else if (iPatientCount >= 2)
                {
                    if ((!string.IsNullOrEmpty(sCurrPatientEID) && !string.IsNullOrEmpty(sPatientEidAdded)) && 
                        (!string.IsNullOrEmpty(sCurrPatientNeoEID) && !string.IsNullOrEmpty(sPatientNeoEidAdded)))
                    {
                        if (string.Compare(sCurrPatientEID, sPatientEidAdded, true) != 0 || string.Compare(sCurrPatientNeoEID, sPatientNeoEidAdded, true) != 0)
                        {
                            enumErrorType = ErrorType.PatientMisMatch;
                            bMistMatch = true;
                            return;
                        }
                    }
                }

                if (string.IsNullOrEmpty(sCurrPatientEID) || string.IsNullOrEmpty(sCurrPatientNeoEID))
                {
                    enumErrorType = ErrorType.PatientCountInsuffienct;
                    bMistMatch = true;
                    return;
                }
                
                enumErrorType = ErrorType.PatNoError;
            }
            catch (RMAppException objEx)
            {
                throw objEx;
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("PSOFormAdaptor.CheckPatientInfo.Error",base.ClientId), objEx);
            }

        }
        

        private void PopulateXMLDom(Event objEvent, ref XmlDocument objXMLDOM, PSOFormManager objPSOFormManager, List<string> lstHiddenControl)
        {
            
            PopulateEventValues(objEvent, ref objXMLDOM, objPSOFormManager, lstHiddenControl);
            PopulateReporterValues(objEvent, ref objXMLDOM, objPSOFormManager, lstHiddenControl);
            PopulatePersonInvolved(objEvent, ref objXMLDOM, objPSOFormManager);
            
        }
        //Event fields to be prepopulated with the mapping
        public string[,] arrEventFields = {
                                                    {"dtEvtDiscoveryDate","DateOfEvent"},
                                                    {"tmDiscoveryTime","TimeOfEvent"},
                                                    {"dtEvtDiscoveryDateNM","DateOfEvent"},
                                                    {"tmDiscoveryTimeNM","TimeOfEvent"},
                                                    {"txtEventDescription","EventDescription"},                                                    
                                                    {"txtLocationDescription","LocationAreaDesc"},                                                    
                                                    {"dtReportDate","DttmRcdAdded"},                                                    
                                                    {"dtReportedDate","DateReported"},

                                                };

        //Event fields to be prepopulated with the mapping
        public string[,] arrReporterFields = {                                                    
                                                    {"txtReporterPhoneNumber","Phone1"},
                                                    {"txtReporterEmail","EmailAddress"},                                                  
                                             };

        public List<string> lstCodeList = new List<string>();
        public List<string> lstDateControlList = new List<string>();
        //Right now there are no multicoe which is prepopulated 
        //In Future if we add some then we can use this.
        public List<string> lstMulCodeList = new List<string>();

        private void PopulateEventValues(Event objEvent, ref XmlDocument objXMLDOM, PSOFormManager objPSOFormManager, List<string> lstHiddenControl)
        {
            PopulateCodeAndMultiCode();
            string sValue = string.Empty;
            try
            {
                for (int iCount = 0; iCount < arrEventFields.Length / 2; iCount++)
                {
                    sValue = objPSOFormManager.GetPropertyValue(objEvent, arrEventFields[iCount, 1]);
                    if (objXMLDOM.SelectSingleNode("/Event/EventPSOData/" + arrEventFields[iCount, 0]) != null)
                    {
                        if (!lstHiddenControl.Contains(arrEventFields[iCount, 0]))
                        {
                            if (lstCodeList.Contains(arrEventFields[iCount, 1]))
                            {
                                XmlElement objTempElement = (XmlElement)objXMLDOM.SelectSingleNode("/Event/EventPSOData/" + arrEventFields[iCount, 0]);
                                objTempElement.SetAttribute("codeid", sValue);
                            }
                            if (lstDateControlList.Contains(arrEventFields[iCount, 1]))
                            {
                                sValue = Conversion.GetDBDateFormat(sValue, "d");
                            }
                            objXMLDOM.SelectSingleNode("/Event/EventPSOData/" + arrEventFields[iCount, 0]).InnerText = sValue;
                        }
                    }
                    else if (objXMLDOM.SelectSingleNode("/Event/EventPSO/" + arrEventFields[iCount, 0]) != null)
                    {
                        if (!lstHiddenControl.Contains(arrEventFields[iCount, 0]))
                        {
                            if (lstCodeList.Contains(arrEventFields[iCount, 1]))
                            {
                                XmlElement objTempElement = (XmlElement)objXMLDOM.SelectSingleNode("/Event/EventPSO/" + arrEventFields[iCount, 0]);
                                objTempElement.SetAttribute("codeid", sValue);
                            }
                            if (lstDateControlList.Contains(arrEventFields[iCount, 1]))
                            {
                                sValue = Conversion.GetDBDateFormat(sValue, "d");
                            }
                            objXMLDOM.SelectSingleNode("/Event/EventPSO/" + arrEventFields[iCount, 0]).InnerText = sValue;
                        }
                    }

                }
            }
            catch (RMAppException objEx)
            {
                throw objEx;
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("PSOFormAdaptor.PopulateEventValues.Error",base.ClientId), objEx);
            }
            finally
            {
                sValue = string.Empty;
            }
        }


        private void PopulateReporterValues(Event objEvent, ref XmlDocument objXMLDOM, PSOFormManager objPSOFormManager, List<string> lstHiddenControl)
        {            
            string sValue = string.Empty;
            string sRepIdentitySC= string.Empty;
            string sRepIdentityDesc = string.Empty;
            string sRepIdentityValue = string.Empty;
            int iRepIdentity = 0;
            try
            {
                if (objEvent.RptdByEid != 0)
                {
                    Entity objEntity = (Entity)objPSOFormManager.DataModelFactory.GetDataModelObject("Entity", false);
                    objEntity.MoveTo(objEvent.RptdByEid);

                    iRepIdentity = objPSOFormManager.LocalCache.GetCodeId("N", "YES_NO");
                    objPSOFormManager.LocalCache.GetCodeInfo(iRepIdentity, ref sRepIdentitySC, ref sRepIdentityDesc);
                    sRepIdentityValue = sRepIdentitySC + " " + sRepIdentityDesc;
                    XmlElement objRepIdentity = (XmlElement)objXMLDOM.SelectSingleNode("/Event/EventPSOData/psocdIdentifyReporter");
                    objRepIdentity.SetAttribute("codeid", iRepIdentity.ToString());
                    objRepIdentity.InnerText = sRepIdentityValue;

                    XmlElement objRepName = (XmlElement)objXMLDOM.SelectSingleNode("/Event/EventPSOData/txtReporterName");
                    objRepName.InnerText = objPSOFormManager.GetPropertyValue(objEntity, "FirstName") + " " + objPSOFormManager.GetPropertyValue(objEntity, "LastName");

                    for (int iCount = 0; iCount < arrReporterFields.Length / 2; iCount++)
                    {
                        sValue = objPSOFormManager.GetPropertyValue(objEntity, arrReporterFields[iCount, 1]);
                        if (objXMLDOM.SelectSingleNode("/Event/EventPSOData/" + arrReporterFields[iCount, 0]) != null)
                        {
                            if (!lstHiddenControl.Contains(arrEventFields[iCount, 0]))
                            {
                                if (lstCodeList.Contains(arrEventFields[iCount, 1]))
                                {
                                    XmlElement objTempElement = (XmlElement)objXMLDOM.SelectSingleNode("/Event/EventPSOData/" + arrEventFields[iCount, 0]);
                                    objTempElement.SetAttribute("codeid", sValue);
                                }
                                objXMLDOM.SelectSingleNode("/Event/EventPSOData/" + arrReporterFields[iCount, 0]).InnerText = sValue;
                            }
                        }
                    }
                }
                else
                {
                    iRepIdentity = objPSOFormManager.LocalCache.GetCodeId("Y", "YES_NO");
                    objPSOFormManager.LocalCache.GetCodeInfo(iRepIdentity, ref sRepIdentitySC, ref sRepIdentityDesc);
                    sRepIdentityValue = sRepIdentitySC + " " + sRepIdentityDesc;
                    XmlElement objRepIdentity = (XmlElement)objXMLDOM.SelectSingleNode("/Event/EventPSOData/psocdIdentifyReporter");
                    objRepIdentity.SetAttribute("codeid", iRepIdentity.ToString());
                    objRepIdentity.InnerText = sRepIdentityValue;
                }
            }
            catch (RMAppException objEx)
            {
                throw objEx;
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("PSOFormAdaptor.PopulateReporterValues.Error",base.ClientId), objEx);
            }
            finally
            {
                
            }
        }

        private void PopulateCodeAndMultiCode()
        {
            lstDateControlList.Add("DttmRcdAdded");
        }

        private void PopulatePersonInvolved(Event objEvent, ref XmlDocument objXMLDOM, PSOFormManager objPSOFormManager)
        {
            bool bIsSuccess = false;
            int iPersonInvolvedCount = 0;
            SortedDictionary<int, PersonInvolved> objPersonInvolvedDic = new SortedDictionary<int, PersonInvolved>();
            PersonInvolved objPersonInvolved = (PersonInvolved)objPSOFormManager.DataModelFactory.GetDataModelObject("PersonInvolved", false);
            PiPatient objPiPatient = (PiPatient)objPSOFormManager.DataModelFactory.GetDataModelObject("PiPatient", false);
            Entity objEntity = (Entity)objPSOFormManager.DataModelFactory.GetDataModelObject("Entity", false);
            string sPatGenderSC = string.Empty;
            string sPatGenderDesc = string.Empty;
            string sPatGenderValue = string.Empty;
            int iPatGender = 0;
            try
            {
                foreach (PersonInvolved objPerInvolved in objEvent.PiList)
                {
                    if (string.Compare(objPSOFormManager.DataModelFactory.Context.LocalCache.GetShortCode(objPerInvolved.PiTypeCode), "P", true) == 0)
                    {
                        iPersonInvolvedCount++;
                        objPersonInvolvedDic.Add(objPerInvolved.PiRowId, objPerInvolved);
                    }
                }
                objPSOFormManager.CreateAndSetElement(objXMLDOM.DocumentElement, "PatientCount", Convert.ToString(iPersonInvolvedCount));

                if (iPersonInvolvedCount != 0 && iPersonInvolvedCount >= 1)
                {
                    objPersonInvolved = objPersonInvolvedDic.Values.First();
                    objPiPatient.MoveTo(objPersonInvolved.PiRowId);

                    XmlElement objPatName = (XmlElement)objXMLDOM.SelectSingleNode("/Event/PatientPSOData/txtPatientName");
                    objPatName.InnerText = objPSOFormManager.GetPropertyValue(objPersonInvolved.PiEntity, "FirstName") + " " + objPSOFormManager.GetPropertyValue(objPersonInvolved.PiEntity, "LastName");

                    XmlElement objPatDOB = (XmlElement)objXMLDOM.SelectSingleNode("/Event/PatientPSOData/txtPatientDOB");
                    objPatDOB.InnerText = objPSOFormManager.GetPropertyValue(objPersonInvolved.PiEntity, "BirthDate");

                    XmlElement objPatMRCNO = (XmlElement)objXMLDOM.SelectSingleNode("/Event/PatientPSOData/txtMedicalRecordNo");
                    objPatMRCNO.InnerText = objPSOFormManager.GetPropertyValue(objPiPatient.Patient, "MedicalRcdNo");

                    XmlElement objPatGender = (XmlElement)objXMLDOM.SelectSingleNode("/Event/PatientPSOData/cdPatientGender");
                    objPatGender.SetAttribute("codeid", objPersonInvolved.PiEntity.SexCode.ToString());
                    iPatGender = objPersonInvolved.PiEntity.SexCode;
                    if (iPatGender != 0)
                    {
                        objPSOFormManager.LocalCache.GetCodeInfo(iPatGender, ref sPatGenderSC, ref sPatGenderDesc);
                        objPatGender.InnerText = sPatGenderSC + " " + sPatGenderDesc;
                    }
                    XmlElement objPatEntId = (XmlElement)objXMLDOM.SelectSingleNode("/Event/PatientPSO/PatInvEntId");
                    objPatEntId.InnerText = Convert.ToString(objPiPatient.PiEid);

                }

                if (iPersonInvolvedCount != 0 && iPersonInvolvedCount >= 2)
                {
                    int iCount = 0;
                    int iPatLbInd = 0;
                    string sPatLbIndSC = string.Empty;
                    string sPatLbIndDesc = string.Empty;
                    string sPSOLbIndSC = string.Empty;
                    string sRMXLbIndSC = string.Empty;
                    IEnumerator IEnum = objPersonInvolvedDic.Values.GetEnumerator();
                    while (IEnum.MoveNext())
                    {
                        iCount++;
                        if (iCount == 2)
                        {
                            objPersonInvolved = (PersonInvolved)IEnum.Current;
                            break;
                        }
                    }
                    objPiPatient.MoveTo(objPersonInvolved.PiRowId);

                    XmlElement objPatName = (XmlElement)objXMLDOM.SelectSingleNode("/Event/PatientPSOData/txtPatientNameNeo");
                    objPatName.InnerText = objPSOFormManager.GetPropertyValue(objPersonInvolved.PiEntity, "FirstName") + " " + objPSOFormManager.GetPropertyValue(objPersonInvolved.PiEntity, "LastName");

                    XmlElement objPatDOB = (XmlElement)objXMLDOM.SelectSingleNode("/Event/PatientPSOData/txtPatientDOBNeo");
                    objPatDOB.InnerText = objPSOFormManager.GetPropertyValue(objPersonInvolved.PiEntity, "BirthDate");

                    XmlElement objPatMRCNO = (XmlElement)objXMLDOM.SelectSingleNode("/Event/PatientPSOData/txtMedicalRecordNoNeo");
                    objPatMRCNO.InnerText = objPSOFormManager.GetPropertyValue(objPiPatient.Patient, "MedicalRcdNo");

                    XmlElement objPatGender = (XmlElement)objXMLDOM.SelectSingleNode("/Event/PatientPSOData/cdPatientGenderNeo");
                    objPatGender.SetAttribute("codeid", objPersonInvolved.PiEntity.SexCode.ToString());
                    iPatGender = objPersonInvolved.PiEntity.SexCode;
                    if (iPatGender != 0)
                    {
                        objPSOFormManager.LocalCache.GetCodeInfo(iPatGender, ref sPatGenderSC, ref sPatGenderDesc);
                        objPatGender.InnerText = sPatGenderSC + " " + sPatGenderDesc;
                    }

                    //XmlElement objPatGest = (XmlElement)objXMLDOM.SelectSingleNode("/Event/PatientPSOData/txtGestation");
                    //objPatGest.InnerText = objPSOFormManager.GetPropertyValue(objPiPatient.Patient, "NbLenOfGest");

                    XmlElement objPatNoLiveBirth = (XmlElement)objXMLDOM.SelectSingleNode("/Event/PatientPSOData/txtLiveBirth");
                    objPatNoLiveBirth.InnerText = objPSOFormManager.GetPropertyValue(objPiPatient.Patient, "NbmLiveBirths");

                    XmlElement objNeoWt = (XmlElement)objXMLDOM.SelectSingleNode("/Event/PatientPSOData/txtNeonateWieght");
                    objNeoWt.InnerText = objPSOFormManager.GetPropertyValue(objPiPatient.Patient, "NbBirthWeight");

                    XmlElement objNeoApgarSc = (XmlElement)objXMLDOM.SelectSingleNode("/Event/PatientPSOData/txtApgarScore");
                    objNeoApgarSc.InnerText = objPSOFormManager.GetPropertyValue(objPiPatient.Patient, "NbApgarScore");

                    XmlElement objPatLbInd = (XmlElement)objXMLDOM.SelectSingleNode("/Event/PatientPSO/psocdLaborinduced");
                    objPatLbInd.SetAttribute("codeid", objPiPatient.Patient.NbmMedIndLabor.ToString());
                    iPatLbInd = objPiPatient.Patient.NbmMedIndLabor;
                    if (iPatLbInd != 0)
                    {
                        objPSOFormManager.LocalCache.GetCodeInfo(iPatLbInd, ref sPatLbIndSC, ref sPatLbIndDesc);
                        objPatLbInd.InnerText = sPatLbIndSC + " " + sPatLbIndDesc;
                    }

                    XmlElement objPatNeoEntId = (XmlElement)objXMLDOM.SelectSingleNode("/Event/PatientPSO/PatInvNeoEntId");
                    objPatNeoEntId.InnerText = Convert.ToString(objPiPatient.PiEid);
                    
                    XmlElement objPatNeoEntIdTemp = (XmlElement)objXMLDOM.SelectSingleNode("/Event/PatientPSOData/txtPatEvtNeoId");
                    objPatNeoEntIdTemp.InnerText = Convert.ToString(objPiPatient.PiEid);
                }

            }
            catch (RMAppException objEx)
            {
                throw objEx;
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("PSOFormAdaptor.PopulatePersonInvolved.Error",base.ClientId), objEx);
            }
            finally
            {
                objPersonInvolvedDic = null;
                if (objPersonInvolved != null)
                {
                    objPersonInvolved.Dispose();
                }
                if (objPiPatient != null)
                {
                    objPiPatient.Dispose();
                }
                if (objEntity != null)
                {
                    objEntity.Dispose();
                }
            }
        }
        private void GetRequiredFields(XmlDocument xmlDom, ref string sRequiredFields)
        {
            string sRequired = string.Empty;
            string sControlId = string.Empty;
            try
            {
                foreach (XmlElement xEle in xmlDom.SelectNodes("//form/group/control"))
                {
                    if (xEle.Attributes["required"] != null)
                    {
                        sRequired = xEle.Attributes["required"].Value.ToString();
                    }
                    if (xEle.Attributes["id"] != null)
                    {
                        sControlId = xEle.Attributes["id"].Value.ToString();
                    }
                    if (string.Compare(sRequired, "yes", true) == 0)
                    {
                        sRequiredFields = sRequiredFields + sControlId + "_Req,";
                    }
                }
                sRequiredFields = sRequiredFields.TrimEnd(',');
            }
            catch (RMAppException objEx)
            {
                throw objEx;
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("PSOFormAdaptor.GetRequiredFields.Error",base.ClientId), objEx);
            }
            finally
            {

            }
        }

        public void GetPanelControls(string sControlName, List<string> lstPanelControls, XmlDocument xmlDom)
        {
            XmlNodeList xmlPanelList = xmlDom.SelectNodes("/form/group[@name='" + sControlName +"']");

            foreach (XmlNode objNode in xmlPanelList)
            {
                foreach (XmlElement objElement in objNode)
                {
                    string sName = objElement.GetAttribute("id");
                    if (!lstPanelControls.Contains(sName))
                    {
                        lstPanelControls.Add(sName);
                    }
                }
            }
        }
        public bool GetDeviceInfo(XmlDocument objXmlIn, ref XmlDocument objXmlOut, ref BusinessAdaptorErrors objErrOut)
        {
            bool bIsPopulated = false;
            bool bIsSuccess = false;
            bool bSaveValue = false;
            string sFilePath = string.Empty;
            string sHiddenFields = string.Empty;
            XmlDocument xmlDom = new XmlDocument();
            XmlNode objDelNode = null;
            List<string> lstHiddenControl = new List<string>();
            List<string> lstRemoveHiddenControls = new List<string>();
            List<string> lstPanelControl = new List<string>();
            PSOFormManager objPSOFormManager = new PSOFormManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password,base.ClientId);
            DeviceInfo objDeviceInfo = (DeviceInfo)objPSOFormManager.DataModelFactory.GetDataModelObject("DeviceInfo", false);
            try
            {
                xmlDom.LoadXml(objXmlIn.SelectSingleNode("//DeviceInfo/FormValues").InnerText);
                objDelNode = objXmlIn.SelectSingleNode("/DeviceInfo/FormValues");
                if (objDelNode != null)
                {
                    objDelNode.ParentNode.RemoveChild(objDelNode);
                }
                objDelNode = null;
                //********************************
                //Populating fields
                //Write logic to prepopulate the data
                string sSessionId = string.Empty;
                string sDeviceRowId = string.Empty;
                int iDeviceRowId = 0;
                XmlDocument objPropStore = new XmlDocument();
                XmlDocument objSerilzationXML = new XmlDocument();

                if ((XmlElement)objXmlIn.SelectSingleNode("DeviceInfo/txtDeviceInfoSessionId") != null)
                {
                    sSessionId = objXmlIn.SelectSingleNode("DeviceInfo/txtDeviceInfoSessionId").InnerText;
                    sDeviceRowId = objXmlIn.SelectSingleNode("DeviceInfo/DeviceRowId").InnerText;
                    if (!string.IsNullOrEmpty(sSessionId))
                    {                        
                        SessionManager objSessionManager = null;
                        string sSessionRawData = string.Empty;                   
                        objSessionManager = base.GetSessionObject();
                        sSessionRawData = Riskmaster.Common.Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(sSessionId)).ToString();                        
                        objPropStore.LoadXml(sSessionRawData);
                        bIsPopulated = true;                        
                    }
                    else if (!string.IsNullOrEmpty(sDeviceRowId))
                    {
                        iDeviceRowId = Conversion.CastToType<int>(sDeviceRowId, out bIsSuccess);
                        objDeviceInfo.MoveTo(iDeviceRowId);
                        bSaveValue = true;

                        objSerilzationXML.LoadXml("<DeviceInfo><Supplementals/></DeviceInfo>");
                        objPropStore = new XmlDocument();
                        objPropStore.LoadXml(objDeviceInfo.SerializeObject(objSerilzationXML));
                        objXmlIn = objPropStore;
                        PopulateDomValue(ref xmlDom, objXmlIn, objPSOFormManager, "//DeviceInfo", true);
                        
                    }
                }

                if (!bSaveValue)
                {
                    if (bIsPopulated)
                    {
                        objDelNode = objPropStore.SelectSingleNode("/DeviceInfo/txtDeviceInfoSessionId");
                        if (objDelNode != null)
                        {
                            objDelNode.ParentNode.RemoveChild(objDelNode);
                        }
                        objDeviceInfo.PopulateObject(objPropStore);
                        PopulateDomValue(ref xmlDom, objPropStore, objPSOFormManager, "//DeviceInfo", true);
                    }
                    else
                    {
                        objDelNode = objXmlIn.SelectSingleNode("/DeviceInfo/txtDeviceInfoSessionId");
                        if (objDelNode != null)
                        {
                            objDelNode.ParentNode.RemoveChild(objDelNode);
                        }
                        objDeviceInfo.PopulateObject(objXmlIn);
                        PopulateDomValue(ref xmlDom, objXmlIn, objPSOFormManager, "//DeviceInfo", true);
                    }

                    objSerilzationXML.LoadXml("<DeviceInfo><Supplementals/></DeviceInfo>");
                    objPropStore = new XmlDocument();
                    objPropStore.LoadXml(objDeviceInfo.SerializeObject(objSerilzationXML));
                }

                //******************************

                HideDisplayXml(ref xmlDom, lstHiddenControl, lstRemoveHiddenControls, objPSOFormManager);
                objXmlIn = objPropStore;

                for (int iCount = 0; iCount < lstRemoveHiddenControls.Count; iCount++)
                {
                    if (lstHiddenControl.Contains(lstRemoveHiddenControls[iCount]))
                    {
                        lstHiddenControl.RemoveAll(sValue => sValue == lstRemoveHiddenControls[iCount]);
                    }
                }

                for (int iCount = 0; iCount < lstHiddenControl.Count; iCount++)
                {
                    if (lstHiddenControl[iCount].Contains("pnl"))
                    {
                        sHiddenFields = sHiddenFields + lstHiddenControl[iCount] + "," + lstHiddenControl[iCount] + "_header" + ",";
                        GetPanelControls(lstHiddenControl[iCount], lstPanelControl, xmlDom);
                    }
                    else
                    {
                        sHiddenFields = sHiddenFields + lstHiddenControl[iCount] + "_ctlRow,";
                    }
                }
                sHiddenFields = sHiddenFields.TrimEnd(',');

                objPSOFormManager.CreateAndSetElement((XmlElement)objXmlIn.SelectSingleNode("DeviceInfo"), "HiddenFieldList", sHiddenFields);
                //Get the required fields starts
                string sRequiredFileds = string.Empty;
                GetRequiredFields(xmlDom, ref sRequiredFileds);
                objPSOFormManager.CreateAndSetElement(objXmlIn.DocumentElement, "RequiredFieldList", sRequiredFileds);
                //Ends
                

                objXmlOut = objXmlIn;
            }
            catch (RMAppException p_objException)
            {
                objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                objErrOut.Add(p_objException, Globalization.GetString("PSOFormAdaptor.GetDeviceInfo.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                xmlDom = null;
                objDelNode = null;
                lstHiddenControl = null;
                lstRemoveHiddenControls = null;
                if (objPSOFormManager != null)
                {
                    objPSOFormManager.Dispose();
                }
                if (objDeviceInfo != null)
                {
                    objDeviceInfo.Dispose();
                }
            }
            return true;
        }

        public bool GetMedInfo(XmlDocument objXmlIn, ref XmlDocument objXmlOut, ref BusinessAdaptorErrors objErrOut)
        {
            bool bIsPopulated = false;
            bool bIsSuccess = false;
            bool bSaveValue = false;
            int iMedRowId = 0;
            string sFilePath = string.Empty;
            string sHiddenFields = string.Empty;
            XmlDocument xmlDom = new XmlDocument();
            XmlElement objGridXMLElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlNode objDelNode = null;
            List<string> lstHiddenControl = new List<string>();
            List<string> lstRemoveHiddenControls = new List<string>();
            PSOFormManager objPSOFormManager = new PSOFormManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password,base.ClientId);
            MedicationInfo objMedInfo = (MedicationInfo)objPSOFormManager.DataModelFactory.GetDataModelObject("MedicationInfo", false);

            try
            {
                xmlDom.LoadXml(objXmlIn.SelectSingleNode("//MedicationInfo/FormValues").InnerText);
                objDelNode = objXmlIn.SelectSingleNode("/MedicationInfo/FormValues");
                if (objDelNode != null)
                {
                    objDelNode.ParentNode.RemoveChild(objDelNode);
                }
                objDelNode = null;
                //********************************
                //Populating fields
                //Write logic to prepoulate the data
                string sSessionId = string.Empty;
                string sLnkEvtRowId = string.Empty;
                XmlDocument objPropStore = new XmlDocument();
                XmlDocument objSerilzationXML = new XmlDocument();
                if ((XmlElement)objXmlIn.SelectSingleNode("MedicationInfo/txtMedInfoSessionId") != null)
                {
                    sSessionId = objXmlIn.SelectSingleNode("MedicationInfo/txtMedInfoSessionId").InnerText;
                    sLnkEvtRowId = objXmlIn.SelectSingleNode("MedicationInfo/MedRowId").InnerText;

                    if (!string.IsNullOrEmpty(sSessionId))
                    {
                        SessionManager objSessionManager = null;
                        string sSessionRawData = string.Empty;
                        objSessionManager = base.GetSessionObject();
                        sSessionRawData = Riskmaster.Common.Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(sSessionId)).ToString();
                        objPropStore.LoadXml(sSessionRawData);
                        bIsPopulated = true;
                    }
                    else if (!string.IsNullOrEmpty(sLnkEvtRowId))
                    {
                        iMedRowId = Conversion.CastToType<int>(sLnkEvtRowId, out bIsSuccess);
                        objMedInfo.MoveTo(iMedRowId);
                        bSaveValue = true;
                        objSerilzationXML.LoadXml("<MedicationInfo><Supplementals/></MedicationInfo>");
                        objPropStore = new XmlDocument();
                        objPropStore.LoadXml(objMedInfo.SerializeObject(objSerilzationXML));
                        objXmlIn = objPropStore;
                        PopulateDomValue(ref xmlDom, objXmlIn, objPSOFormManager, "//MedicationInfo", true);

                    }
                }

                if (bIsPopulated)
                {
                    objDelNode = objPropStore.SelectSingleNode("/MedicationInfo/txtMedInfoSessionId");
                    if (objDelNode != null)
                    {
                        objDelNode.ParentNode.RemoveChild(objDelNode);
                    }
                    objMedInfo.PopulateObject(objPropStore);
                    PopulateDomValue(ref xmlDom, objPropStore, objPSOFormManager, "//MedicationInfo", true);
                }
                else
                {
                    objDelNode = objXmlIn.SelectSingleNode("/MedicationInfo/txtMedInfoSessionId");
                    if (objDelNode != null)
                    {
                        objDelNode.ParentNode.RemoveChild(objDelNode);
                    }
                    objMedInfo.PopulateObject(objXmlIn);
                    PopulateDomValue(ref xmlDom, objXmlIn, objPSOFormManager, "//MedicationInfo", true);
                }
                objSerilzationXML.LoadXml("<MedicationInfo></MedicationInfo>");
                objPropStore = new XmlDocument();
                objPropStore.LoadXml(objMedInfo.SerializeObject(objSerilzationXML));

                //******************************
                objXmlIn = objPropStore;
                //Get the required fields starts
                string sRequiredFileds = string.Empty;
                GetRequiredFields(xmlDom, ref sRequiredFileds);
                objPSOFormManager.CreateAndSetElement(objXmlIn.DocumentElement, "RequiredFieldList", sRequiredFileds);
                //Ends
                objXmlOut = objXmlIn;
            }
            catch (RMAppException p_objException)
            {
                objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                objErrOut.Add(p_objException, Globalization.GetString("PSOFormAdaptor.GetMedInfo.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                xmlDom = null;
                lstHiddenControl = null;
                lstRemoveHiddenControls = null;
                if (objPSOFormManager != null)
                {
                    objPSOFormManager.Dispose();
                }
                if (objMedInfo != null)
                {
                    objMedInfo.Dispose();
                }
            }
            return true;
        }

        public bool GetLinkedEventInfo(XmlDocument objXmlIn, ref XmlDocument objXmlOut, ref BusinessAdaptorErrors objErrOut)
        {
            bool bIsPopulated = false;
            bool bIsSuccess = false;
            bool bSaveValue = false;
            int iLnkEvtRowId = 0;
            string sFilePath = string.Empty;
            string sHiddenFields = string.Empty;
            XmlDocument xmlDom = new XmlDocument();
            XmlElement objGridXMLElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlNode objDelNode = null;
            List<string> lstHiddenControl = new List<string>();
            List<string> lstRemoveHiddenControls = new List<string>();
            List<string> lstPanelControl = new List<string>();
            PSOFormManager objPSOFormManager = new PSOFormManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password,base.ClientId);
            LinkedEventInfo objLnkInfoInfo = (LinkedEventInfo)objPSOFormManager.DataModelFactory.GetDataModelObject("LinkedEventInfo", false);

            try
            {
                xmlDom.LoadXml(objXmlIn.SelectSingleNode("//LinkedEventInfo/FormValues").InnerText);

                objDelNode = objXmlIn.SelectSingleNode("/LinkedEventInfo/FormValues");
                if (objDelNode != null)
                {
                    objDelNode.ParentNode.RemoveChild(objDelNode);
                }
                objDelNode = null;
                //********************************
                //Populating fields
                //Write logic to prepoulate the data
                string sSessionId = string.Empty;
                string sLnkEvtRowId = string.Empty;
                XmlDocument objPropStore = new XmlDocument();
                XmlDocument objSerilzationXML = new XmlDocument();
                if ((XmlElement)objXmlIn.SelectSingleNode("LinkedEventInfo/txtLnkEvtInfoSessionId") != null)
                {
                    sSessionId = objXmlIn.SelectSingleNode("LinkedEventInfo/txtLnkEvtInfoSessionId").InnerText;
                    sLnkEvtRowId = objXmlIn.SelectSingleNode("LinkedEventInfo/LnkEventRowId").InnerText;

                    if (!string.IsNullOrEmpty(sSessionId))
                    {
                        SessionManager objSessionManager = null;
                        string sSessionRawData = string.Empty;
                        objSessionManager = base.GetSessionObject();
                        sSessionRawData = Riskmaster.Common.Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(sSessionId)).ToString();
                        objPropStore.LoadXml(sSessionRawData);
                        bIsPopulated = true;
                    }
                    else if (!string.IsNullOrEmpty(sLnkEvtRowId))
                    {
                        iLnkEvtRowId = Conversion.CastToType<int>(sLnkEvtRowId, out bIsSuccess);
                        objLnkInfoInfo.MoveTo(iLnkEvtRowId);
                        bSaveValue = true;
                        objSerilzationXML.LoadXml("<LinkedEventInfo><Supplementals/></LinkedEventInfo>");
                        objPropStore = new XmlDocument();
                        objPropStore.LoadXml(objLnkInfoInfo.SerializeObject(objSerilzationXML));
                        objXmlIn = objPropStore;
                        PopulateDomValue(ref xmlDom, objXmlIn, objPSOFormManager, "//LinkedEventInfo", true);

                    }
                }

                if (!bSaveValue)
                {
                    if (bIsPopulated)
                    {
                        objDelNode = objPropStore.SelectSingleNode("/LinkedEventInfo/txtLnkEvtInfoSessionId");
                        if (objDelNode != null)
                        {
                            objDelNode.ParentNode.RemoveChild(objDelNode);
                        }
                        objLnkInfoInfo.PopulateObject(objPropStore);
                        PopulateDomValue(ref xmlDom, objPropStore, objPSOFormManager, "//LinkedEventInfo", true);
                    }
                    else
                    {
                        objDelNode = objXmlIn.SelectSingleNode("/LinkedEventInfo/txtLnkEvtInfoSessionId");
                        if (objDelNode != null)
                        {
                            objDelNode.ParentNode.RemoveChild(objDelNode);
                        }
                        objLnkInfoInfo.PopulateObject(objXmlIn);
                        PopulateDomValue(ref xmlDom, objXmlIn, objPSOFormManager, "//LinkedEventInfo", true);
                    }
                }

                objSerilzationXML.LoadXml("<LinkedEventInfo></LinkedEventInfo>");

                objPropStore = new XmlDocument();
                objPropStore.LoadXml(objLnkInfoInfo.SerializeObject(objSerilzationXML));

                //******************************
                HideDisplayXml(ref xmlDom, lstHiddenControl, lstRemoveHiddenControls, objPSOFormManager);
                objXmlIn = objPropStore;

                for (int iCount = 0; iCount < lstRemoveHiddenControls.Count; iCount++)
                {
                    if (lstHiddenControl.Contains(lstRemoveHiddenControls[iCount]))
                    {
                        lstHiddenControl.RemoveAll(sValue => sValue == lstRemoveHiddenControls[iCount]);
                    }
                }

                for (int iCount = 0; iCount < lstHiddenControl.Count; iCount++)
                {
                    if (lstHiddenControl[iCount].Contains("pnl"))
                    {
                        sHiddenFields = sHiddenFields + lstHiddenControl[iCount] + "," + lstHiddenControl[iCount] + "_header" + ",";
                        GetPanelControls(lstHiddenControl[iCount], lstPanelControl, xmlDom);
                    }
                    else
                    {
                        sHiddenFields = sHiddenFields + lstHiddenControl[iCount] + "_ctlRow,";
                    }
                }
                sHiddenFields = sHiddenFields.TrimEnd(',');

                objPSOFormManager.CreateAndSetElement((XmlElement)objXmlIn.SelectSingleNode("LinkedEventInfo"), "HiddenFieldList", sHiddenFields);
                //Get the required fields starts
                string sRequiredFileds = string.Empty;
                GetRequiredFields(xmlDom, ref sRequiredFileds);
                objPSOFormManager.CreateAndSetElement(objXmlIn.DocumentElement, "RequiredFieldList", sRequiredFileds);
                //Ends


                objXmlOut = objXmlIn;
            }
            catch (RMAppException p_objException)
            {
                objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                objErrOut.Add(p_objException, Globalization.GetString("PSOFormAdaptor.GetLinkedEventInfo.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                xmlDom = null;
                objDelNode = null;
                lstHiddenControl = null;
                lstRemoveHiddenControls = null;
                if (objPSOFormManager != null)
                {
                    objPSOFormManager.Dispose();
                }
                if (objLnkInfoInfo != null)
                {
                    objLnkInfoInfo.Dispose();
                }
            }
            return true;
        }

        private void PopulateDomValue(ref XmlDocument xmlDom, XmlDocument objXmlIn, PSOFormManager objPSOFormManager, string sNode, bool bCheck)
        {
            bool bIsSuccses = false;
            XmlNodeList objNodeList = null;
            try
            {
                objNodeList = objXmlIn.SelectNodes(sNode);

                if (bCheck)
                {
                    foreach (XmlNode objNode in objNodeList)
                    {
                        foreach (XmlElement objElement in objNode)
                        {
                            string sControlName = objElement.Name;
                            if (xmlDom.SelectSingleNode("/form/group/control[@property='" + sControlName + "']") != null)
                            {
                                XmlElement objSelectedElement = (XmlElement)xmlDom.SelectSingleNode("/form/group/control[@property='" + sControlName + "']");
                                string sValue = string.Empty;
                                if (objElement.Attributes["codeid"] != null)
                                {
                                    sValue = objElement.GetAttribute("codeid");
                                }
                                else
                                {
                                    sValue = objElement.InnerText;
                                }
                                if (string.Compare(sValue, "0") == 0)
                                {
                                    sValue = string.Empty;
                                }
                                objSelectedElement.SetAttribute("value", sValue);
                            }
                        }
                    }
                }
                else
                {

                    //Remove this Starts 
                    foreach (XmlNode objNode in objNodeList)
                    {
                        foreach (XmlElement objElement in objNode)
                        {
                            string sControlName = objElement.Name;
                            if (xmlDom.SelectSingleNode("/form/group/control[@id='" + sControlName + "']") != null)
                            {
                                XmlElement objSelectedElement = (XmlElement)xmlDom.SelectSingleNode("/form/group/control[@id='" + sControlName + "']");
                                string sValue = string.Empty;
                                if (objElement.Attributes["codeid"] != null)
                                {
                                    sValue = objElement.GetAttribute("codeid");
                                }
                                else
                                {
                                    sValue = objElement.InnerText;
                                }
                                objSelectedElement.SetAttribute("value", sValue);
                            }
                        }
                    }
                    //Remove This Ends
                }
            }
            catch (RMAppException objEx)
            {
                throw objEx;
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("PSOFormAdaptor.PopulateDomValue.Error",base.ClientId), objEx);
            }
            finally
            {
                objNodeList = null;
            }
        }

        private void HideDisplayXml(ref XmlDocument xmlDoM, List<string> lstHiddenControl, List<string> lstRemoveHiddenControls, PSOFormManager objPSOFormManager)
        {
            try
            {
                foreach (XmlElement objGroup in xmlDoM.GetElementsByTagName("group"))
                {
                    if (objGroup.GetAttribute("hidden") == "yes")
                    {
                        if (!lstHiddenControl.Contains(objGroup.Attributes["name"].Value))
                        {
                            lstHiddenControl.Add(objGroup.Attributes["name"].Value);
                        }
                    }
                    //Hide-Display based on the value selected
                    foreach (XmlElement objXmlNode in objGroup.ChildNodes)
                    {
                        if (objXmlNode.Attributes["type"].Value.Trim() == "code" || objXmlNode.Attributes["type"].Value.Trim() == "dropdown" || objXmlNode.Attributes["type"].Value.Trim() == "multicode") //12/11/2009 Saurabh Walia: Last condition added.
                        {
                            if (objXmlNode.Attributes["displaygroupname"] != null && objXmlNode.Attributes["displaygroupid"] != null)
                                HideDisplayXml_Group(ref xmlDoM, objXmlNode, lstHiddenControl, lstRemoveHiddenControls, objPSOFormManager);
                            if (objXmlNode.Attributes["displaycontrol"] != null && objXmlNode.Attributes["displaycontrolid"] != null)
                                HideDisplayXml_Control(ref xmlDoM, objXmlNode, lstHiddenControl, lstRemoveHiddenControls, objPSOFormManager);
                        }

                    }

                }
            }
            catch (RMAppException objEx)
            {
                throw objEx;
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("PSOFormAdaptor.HideDisplayXml.Error",base.ClientId), objEx);
            }
            finally
            {

            }
        }

        private void HideDisplayXml_Group(ref XmlDocument xmlDoM, XmlElement objXmlNode, List<string> lstHiddenControl, List<string> lstRemoveHiddenControls, PSOFormManager objPSOFormManager)
        {
            Boolean bMatch = false;
            
            try
            {
                String[] strarrgroups = objXmlNode.Attributes["displaygroupname"].Value.Trim().Replace("%", "").Split('#');
                String[] strarrgroupids = objPSOFormManager.GetCodeIdList(objXmlNode.Attributes["displaygroupid"].Value.Trim(), objXmlNode.Attributes["codetable"].Value.Trim()).Replace("%", "").Split('#');

                //Saurabh Walia 12/08/2009: Hide every group as a first step.
                for (int k = 0; k < strarrgroups.Length; k++)
                {
                    String[] strControlsToHide = (strarrgroups[k].ToString()).Split(',');

                    for (int m = 0; m < strControlsToHide.Length; m++)
                    {
                        String strControlName = "/form/group[@name='" + strControlsToHide[m].ToString() + "']";
                        XmlElement xmlControl = (XmlElement)xmlDoM.SelectNodes(strControlName).Item(0);
                        if (xmlControl != null)
                        {
                            if (xmlControl.Attributes["hidden"] != null)
                            {
                                xmlControl.SetAttribute("hidden", "yes");
                                string sType = Convert.ToString(xmlControl.Attributes["type"]);
                                if (!lstHiddenControl.Contains(strControlsToHide[m]))
                                {
                                    lstHiddenControl.Add(strControlsToHide[m]);
                                }
                            }
                        }
                    }
                }

                for (int i = 0; i < strarrgroupids.Length; i++)
                {
                    int ireturn = 0;
                    List<string> strvalue = new List<string>();
                    IEnumerator ienumstr = strarrgroupids[i].Split(',').GetEnumerator();
                    while (ienumstr.MoveNext())
                    {
                        strvalue.Add(strarrgroupids[i].Split(',')[ireturn].ToString());
                        ireturn++;
                    }
                    ienumstr = null;
                    if (objXmlNode.Attributes["value"] == null || (objXmlNode.Attributes["value"].Value == null) || (objXmlNode.Attributes["value"].Value == ""))
                        continue;
                    if (!bMatch)
                    {
                        if ((strvalue.Contains(objXmlNode.Attributes["value"].Value.Trim())))
                            bMatch = true;
                    }
                    if ((strvalue.Contains(objXmlNode.Attributes["value"].Value.Trim())) || (strarrgroupids[i] == "*") || (strarrgroupids[i].ToLower() == "else" && !bMatch))
                    {
                        String[] strGroupsToDisplay = (strarrgroups[i].ToString()).Split(',');

                        for (int j = 0; j < strGroupsToDisplay.Length; j++)
                        {
                            if (!String.IsNullOrEmpty(strGroupsToDisplay[j].ToString()))
                            {
                                String strGroupName = "/form/group[@name='" + strGroupsToDisplay[j].ToString() + "']";
                                XmlElement xmlGroup = (XmlElement)xmlDoM.SelectNodes(strGroupName).Item(0);
                                if (xmlGroup != null)
                                {
                                    xmlGroup.SetAttribute("hidden", "");
                                    if (lstHiddenControl.Contains(strGroupsToDisplay[j]))
                                    {
                                        lstRemoveHiddenControls.Add(strGroupsToDisplay[j]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (RMAppException objEx)
            {
                throw objEx;
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("PSOFormAdaptor.HideDisplayXml_Group.Error",base.ClientId), objEx);
            }
            finally
            {

            }
        }

        private void HideDisplayXml_Control(ref XmlDocument xmlDoM, XmlElement objXmlNode, List<string> lstHiddenControl, List<string> lstRemoveHiddenControls, PSOFormManager objPSOFormManager)
        {
            bool bMatch = false;
            Boolean bMultiMatch = false; //For multicode

            try
            {
                String[] strarrcntrls = objXmlNode.Attributes["displaycontrol"].Value.Trim().Replace("%", "").Split('#');
                String[] strarrcntrlids = objPSOFormManager.GetCodeIdList(objXmlNode.Attributes["displaycontrolid"].Value.Trim(), objXmlNode.Attributes["codetable"].Value.Trim()).Replace("%", "").Split('#');

                //Saurabh Walia 12/08/2009: Hide every control as a first step.
                for (int k = 0; k < strarrcntrls.Length; k++)
                {
                    String[] strControlsToHide = (strarrcntrls[k].ToString()).Split(',');

                    for (int m = 0; m < strControlsToHide.Length; m++)
                    {
                        String strControlName = "/form/group/control[@id='" + strControlsToHide[m].ToString() + "']";
                        XmlElement xmlControl = (XmlElement)xmlDoM.SelectNodes(strControlName).Item(0);
                        if (xmlControl != null)
                        {
                            if (xmlControl.Attributes["hidden"] != null)
                            {
                                xmlControl.SetAttribute("hidden", "yes");
                                string sType = Convert.ToString(xmlControl.Attributes["type"]);
                                if (!lstHiddenControl.Contains(strControlsToHide[m]))
                                {
                                    lstHiddenControl.Add(strControlsToHide[m]);
                                }
                            }
                        }
                    }
                }

                for (int i = 0; i < strarrcntrlids.Length; i++)
                {
                    int ireturn = 0;
                    List<string> strvalue = new List<string>();
                    IEnumerator ienumstr = strarrcntrlids[i].Split(',').GetEnumerator();
                    while (ienumstr.MoveNext())
                    {
                        strvalue.Add(strarrcntrlids[i].Split(',')[ireturn].ToString());
                        ireturn++;
                    }

                    //To handle Multicode hide and display
                    if (objXmlNode.Attributes["type"].Value.Trim().ToLower() == "multicode")
                    {
                        ireturn = 0;
                        List<string> strmultivalue = new List<string>();
                        ienumstr = objXmlNode.Attributes["value"].Value.Trim().Split(' ').GetEnumerator();
                        while (ienumstr.MoveNext())
                        {
                            strmultivalue.Add(objXmlNode.Attributes["value"].Value.Trim().Split(' ')[ireturn].ToString());
                            ireturn++;
                        }

                        foreach (string strSingleValue in strmultivalue)
                        {
                            if (strvalue.Contains(strSingleValue))
                            {
                                bMultiMatch = true;
                                break;
                            }
                            else
                            {
                                bMultiMatch = false;
                            }
                        }
                    }
                    //End
                    ienumstr = null;
                    if (objXmlNode.Attributes["value"] == null || (objXmlNode.Attributes["value"].Value == null) || (objXmlNode.Attributes["value"].Value == ""))
                        continue;
                    //Saurabh Walia 12/09/2009: Handling for else case
                    if (!bMatch && objXmlNode.Attributes["type"].Value.Trim().ToLower() != "multicode")
                    {
                        if ((strvalue.Contains(objXmlNode.Attributes["value"].Value.Trim())))
                        {
                            bMatch = true;
                        }
                        else
                        {
                            bMatch = false;
                        }
                    }

                    if ((strvalue.Contains(objXmlNode.Attributes["value"].Value.Trim()) && objXmlNode.Attributes["type"].Value.Trim().ToLower() != "multicode") || (objXmlNode.Attributes["type"].Value.Trim().ToLower() == "multicode" && strarrcntrlids[i] == "*" && !bMultiMatch) || (strarrcntrlids[i].ToLower() == "else" && !bMatch && objXmlNode.Attributes["type"].Value.Trim().ToLower() != "multicode") || (objXmlNode.Attributes["type"].Value.Trim().ToLower() == "multicode" && strarrcntrlids[i].ToLower() == "else" && !bMultiMatch) || (objXmlNode.Attributes["type"].Value.Trim().ToLower() == "multicode" && bMultiMatch)) //Added conditions to handle multicode.
                    {
                        //bMatch = true;
                        String[] strControlsToDisplay = (strarrcntrls[i].ToString()).Split(',');

                        for (int j = 0; j < strControlsToDisplay.Length; j++)
                        {
                            if (!String.IsNullOrEmpty(strControlsToDisplay[j].ToString()))
                            {
                                String strControlName = "/form/group/control[@id='" + strControlsToDisplay[j].ToString() + "']";
                                XmlElement xmlControl = (XmlElement)xmlDoM.SelectNodes(strControlName).Item(0);
                                string sDefaultValuePresent = string.Empty;
                                if (xmlControl != null)
                                {
                                    xmlControl.SetAttribute("hidden", "");
                                    if (lstHiddenControl.Contains(strControlsToDisplay[j]))
                                    {
                                        lstRemoveHiddenControls.Add(strControlsToDisplay[j]);
                                    }
                                    if (xmlControl.Attributes["displaycontrol"] != null && xmlControl.Attributes["displaycontrolid"] != null)
                                        HideDisplayXml_Control(ref xmlDoM, xmlControl, lstHiddenControl, lstRemoveHiddenControls, objPSOFormManager);
                                }
                            }
                        }
                    }
                }
            }
            catch (RMAppException objEx)
            {
                throw objEx;
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("PSOFormAdaptor.HideDisplayXml_Control.Error",base.ClientId), objEx);
            }
            finally
            {

            }
        }

        public bool SetDeviceInfo(XmlDocument objXmlIn, ref XmlDocument objXmlOut, ref BusinessAdaptorErrors objErrOut)
        {
            XmlDocument objReturnDoc = null;
            XmlElement objRoot = null;
            try
            {
                using (PSOFormManager objPSOManager = new PSOFormManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password,base.ClientId))
                {
                    string sUniqueId = Riskmaster.Common.Utilities.GenerateGuid();
                    //Removing nodes from the XML
                    //Anu Tennyson Starts
                    XmlNode objDelNode = objXmlIn.SelectSingleNode("/DeviceInfo/txtDeviceInfoSessionId");
                    if (objDelNode != null)
                    {
                        objDelNode.ParentNode.RemoveChild(objDelNode);
                    }
                    using (SessionManager objSessionManager = base.GetSessionObject())
                    {
                        objSessionManager.SetBinaryItem(sUniqueId, Riskmaster.Common.Utilities.BinarySerialize(objXmlIn.OuterXml),base.ClientId);
                    }
                    objPSOManager.StartDocument(ref objReturnDoc, ref objRoot, "SetDevices");
                    objPSOManager.CreateAndSetElement(objRoot, "SessionId", sUniqueId);
                    objXmlOut = objReturnDoc;
                }
                return true;

            }
            catch (RMAppException p_objException)
            {
                objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                objErrOut.Add(p_objException, Globalization.GetString("PSOFormAdaptor.SetDeviceInfo.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objReturnDoc = null;
                objRoot = null;
            }


        }

        public bool SetMedInfo(XmlDocument objXmlIn, ref XmlDocument objXmlOut, ref BusinessAdaptorErrors objErrOut)
        {
            XmlDocument objReturnDoc = null;
            XmlElement objRoot = null;

            try
            {
                using (PSOFormManager objPSOManager = new PSOFormManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password,base.ClientId))
                {
                    string sUniqueId = Riskmaster.Common.Utilities.GenerateGuid();
                    //Removing nodes from the XML
                    //Anu Tennyson Starts
                    XmlNode objDelNode = objXmlIn.SelectSingleNode("/MedicationInfo/txtMedInfoSessionId");
                    if (objDelNode != null)
                    {
                        objDelNode.ParentNode.RemoveChild(objDelNode);
                    }
                    using (SessionManager objSessionManager = base.GetSessionObject())
                    {
                        objSessionManager.SetBinaryItem(sUniqueId, Riskmaster.Common.Utilities.BinarySerialize(objXmlIn.OuterXml),base.ClientId);
                    }
                    objPSOManager.StartDocument(ref objReturnDoc, ref objRoot, "SetMedInfo");
                    objPSOManager.CreateAndSetElement(objRoot, "SessionId", sUniqueId);
                    objXmlOut = objReturnDoc;
                }
                return true;

            }
            catch (RMAppException p_objException)
            {
                objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                objErrOut.Add(p_objException, Globalization.GetString("PSOFormAdaptor.SetMedInfo.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objReturnDoc = null;
                objRoot = null;
            }


        }

        public bool SetLnkEvtInfo(XmlDocument objXmlIn, ref XmlDocument objXmlOut, ref BusinessAdaptorErrors objErrOut)
        {
            XmlDocument objReturnDoc = null;
            XmlElement objRoot = null;

            try
            {
                using (PSOFormManager objPSOManager = new PSOFormManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password,base.ClientId))
                {
                    string sUniqueId = Riskmaster.Common.Utilities.GenerateGuid();
                    //Removing nodes from the XML
                    //Anu Tennyson Starts
                    XmlNode objDelNode = objXmlIn.SelectSingleNode("/LinkedEventInfo/txtLnkEvtInfoSessionId");
                    if (objDelNode != null)
                    {
                        objDelNode.ParentNode.RemoveChild(objDelNode);
                    }
                    using (SessionManager objSessionManager = base.GetSessionObject())
                    {
                        objSessionManager.SetBinaryItem(sUniqueId, Riskmaster.Common.Utilities.BinarySerialize(objXmlIn.OuterXml),base.ClientId);
                    }
                    objPSOManager.StartDocument(ref objReturnDoc, ref objRoot, "SetLinkedEvtInfo");
                    objPSOManager.CreateAndSetElement(objRoot, "SessionId", sUniqueId);
                    objXmlOut = objReturnDoc;
                }
                return true;

            }
            catch (RMAppException p_objException)
            {
                objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                objErrOut.Add(p_objException, Globalization.GetString("PSOFormAdaptor.SetLnkEvtInfo.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objReturnDoc = null;
                objRoot = null;
            }


        }

        public bool SavePSOData(XmlDocument objXmlIn, ref XmlDocument objXmlOut, ref BusinessAdaptorErrors objErrOut)
        {
            bool bIsSuccess = false;
            PSOFormManager objPSOFormManager = new PSOFormManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password,base.ClientId);
            EventPSO objEventPSO = (EventPSO)objPSOFormManager.DataModelFactory.GetDataModelObject("EventPSO", false);
            PatientPSO objPatientPSO = (PatientPSO)objPSOFormManager.DataModelFactory.GetDataModelObject("PatientPSO", false);
            int iEvtPSORowId = 0;
            int iPatPSORowId = 0;
            List<string> lstDeletedINCMedInfoRowId = new List<string>();
            List<string> lstDeletedNMMedInfoRowId = new List<string>();
            List<string> lstDeletedUSCMedInfoRowId = new List<string>();
            List<string> lstDeletedDeviceInfoRowId = new List<string>();
            List<string> lstDeletedLnkEvtRowId = new List<string>();
            XmlDocument xmlTempDeviceDom = new XmlDocument();
            XmlDocument xmlTempMedDom = new XmlDocument();
            XmlDocument xmlTempLnkEvtDom = new XmlDocument();

            try
            {
                xmlTempDeviceDom.LoadXml(objXmlIn.SelectSingleNode("//Event/DeviceFormValues").InnerText);
                xmlTempMedDom.LoadXml(objXmlIn.SelectSingleNode("//Event/MedicationFormValues").InnerText);
                xmlTempLnkEvtDom.LoadXml(objXmlIn.SelectSingleNode("//Event/LnkEvtFormValues").InnerText);
                
                XmlDocument xmlTempDocument = new XmlDocument();
                xmlTempDocument.LoadXml(objXmlIn.SelectSingleNode("//Event/EventPSO").OuterXml);

                if ((XmlElement)objXmlIn.SelectSingleNode("/Event/EventPSO/EvtPsoRowId") != null)
                {
                    iEvtPSORowId = Conversion.CastToType<int>(objXmlIn.SelectSingleNode("/Event/EventPSO/EvtPsoRowId").InnerText, out bIsSuccess);
                }
                if (iEvtPSORowId != 0)
                {
                    objEventPSO.MoveTo(iEvtPSORowId);
                }

                objEventPSO.PopulateObject(xmlTempDocument);
                //*******************
                xmlTempDocument = new XmlDocument();
                XmlElement objNodeTemp = null;
                string sEventCategory = string.Empty;
                string sEventReported = string.Empty;
                string[] arrEventReported = null;
                if (objXmlIn.SelectSingleNode("//Event/EventPSO/psocdWhatReported") != null)
                {
                    sEventReported = objXmlIn.SelectSingleNode("//Event/EventPSO/psocdWhatReported").InnerText;
                    arrEventReported = sEventReported.Split(' ');
                }
                if (string.Compare(arrEventReported[0], "A3", true) != 0)
                {
                    if (objXmlIn.SelectSingleNode("//Event/PatientPSO/PatInvEntId") != null)
                    {
                        objXmlIn.SelectSingleNode("//Event/PatientPSO/PatInvEntId").InnerText = "0";
                    }
                }
                if (objXmlIn.SelectSingleNode("//Event/PatientPSO/psocdAffectedByEventBirthing") != null)
                {
                    objNodeTemp = (XmlElement)objXmlIn.SelectSingleNode("//Event/PatientPSO/psocdAffectedByEventBirthing");
                    sEventCategory = Convert.ToString(objPSOFormManager.LocalCache.GetShortCode(Conversion.CastToType<int>(objNodeTemp.GetAttribute("codeid"), out bIsSuccess)));

                }
                if (string.Compare(sEventCategory, "A1524", true) == 0)
                {
                    if (objXmlIn.SelectSingleNode("//Event/PatientPSO/PatInvNeoEntId") != null)
                    {
                        objXmlIn.SelectSingleNode("//Event/PatientPSO/PatInvNeoEntId").InnerText = objXmlIn.SelectSingleNode("//Event/PatientPSOData/txtPatEvtNeoId").InnerText;
                    }
                }
                xmlTempDocument.LoadXml(objXmlIn.SelectSingleNode("//Event/PatientPSO").OuterXml);
                if ((XmlElement)objXmlIn.SelectSingleNode("/Event/PatientPSO/PatPsoRowId") != null)
                {
                    iPatPSORowId = Conversion.CastToType<int>(objXmlIn.SelectSingleNode("/Event/PatientPSO/PatPsoRowId").InnerText, out bIsSuccess);
                }

                if (iPatPSORowId != 0)
                {
                    objPatientPSO.MoveTo(iPatPSORowId);
                }

                objPatientPSO.PopulateObject(xmlTempDocument);
                objPatientPSO.DataChanged = true;
                objPatientPSO.Save();
                //*****************
                //This block will handle the deleted row ids of the grids starts
                string sDeviceInfoDeletedId = string.Empty;
                string[] arrDeviceInfoDeletedId = null;
                if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtDeviceInfoDeletedId") != null)
                {
                    sDeviceInfoDeletedId = objXmlIn.SelectSingleNode("//Event/EventPSOData/txtDeviceInfoDeletedId").InnerText;
                    arrDeviceInfoDeletedId = sDeviceInfoDeletedId.Split('|');
                }
                for (int iCount = 0; iCount < arrDeviceInfoDeletedId.Length; iCount++)
                {
                    if (Conversion.CastToType<int>(arrDeviceInfoDeletedId[iCount], out bIsSuccess) > 0)
                    {
                        lstDeletedDeviceInfoRowId.Add(arrDeviceInfoDeletedId[iCount]);
                    }
                }
                //Ends

                string sDeviceInfoSessionId = string.Empty;
                XmlElement objOptionXmlElement = null;
                int iIndex = 0;
                XmlDocument objReturnDoc = null;
                XmlElement objRoot = null;
                Hashtable objDevicesUniqueIds = new Hashtable();

                if (objXmlIn.SelectSingleNode("/Event/EventPSOData/txtDeviceInfoSessionId") != null)
                {
                    sDeviceInfoSessionId = objXmlIn.SelectSingleNode("/Event/EventPSOData/txtDeviceInfoSessionId").InnerText;
                }
                SessionManager objSessionManager = null;
                int iDeviceRowId = -1;
                if (!string.IsNullOrEmpty(sDeviceInfoSessionId))
                {
                    string[] arrDeviceInfoSessionId = sDeviceInfoSessionId.Split('~');

                    for (int iCount = 0; iCount < arrDeviceInfoSessionId.Length; iCount++)
                    {
                        if (!string.IsNullOrEmpty(arrDeviceInfoSessionId[iCount]))
                        {
                            string sSessionRawData = string.Empty;
                            objSessionManager = base.GetSessionObject();
                            sSessionRawData = Riskmaster.Common.Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(arrDeviceInfoSessionId[iCount])).ToString();

                            XmlDocument objTempXML = new XmlDocument();
                            XmlDocument objPropStore = new XmlDocument();
                            objTempXML.LoadXml(sSessionRawData);
                            objPropStore.LoadXml(objTempXML.SelectSingleNode("DeviceInfo").OuterXml);

                            DeviceInfo objDeviceInfo = (DeviceInfo)objPSOFormManager.DataModelFactory.GetDataModelObject("DeviceInfo", false);
                            XmlNode objDelNode = objPropStore.SelectSingleNode("/DeviceInfo/txtDeviceInfoSessionId");
                            if (objDelNode != null)
                            {
                                objDelNode.ParentNode.RemoveChild(objDelNode);
                            }

                            int iDeviceInfoRowId = 0;
                            if (objPropStore.SelectSingleNode("/DeviceInfo/DeviceRowId") != null)
                            {
                                iDeviceInfoRowId = Conversion.CastToType<int>(objPropStore.SelectSingleNode("/DeviceInfo/DeviceRowId").InnerText, out bIsSuccess);

                            }
                            if (iDeviceInfoRowId == 0)
                            {
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "DeviceRowId", Convert.ToString(iDeviceRowId));
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "EvtPsoRowId", Convert.ToString(iDeviceRowId));
                                objDeviceInfo.PopulateObject(objPropStore);
                                objEventPSO.DeviceInfoList.Add(objDeviceInfo, true);
                                objDevicesUniqueIds.Add(objDeviceInfo.DeviceRowId, arrDeviceInfoSessionId[iCount]);
                                iDeviceRowId = iDeviceRowId - 1;
                            }
                            else
                            {
                                objEventPSO.DeviceInfoList[iDeviceInfoRowId].PopulateObject(objPropStore);
                            }
                            objSessionManager = null;
                        }

                    }
                }

                //Block for Deleting the Deleted rows from the collection Starts
                foreach (DeviceInfo objDeviceInfo in objEventPSO.DeviceInfoList)
                {
                    if (arrDeviceInfoDeletedId.Contains(objDeviceInfo.DeviceRowId.ToString()))
                    {
                        if (objDeviceInfo.DeviceRowId > 0)
                        {
                            objEventPSO.DeviceInfoList.Remove(objDeviceInfo.DeviceRowId);
                        }
                    }
                }
                //Ends

                //This block will handle the deleted row ids of the grids starts
                string sINCMedInfoDeletedId = string.Empty;
                string[] arrINCMedInfoDeletedId = null;
                if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtINCMedInfoDeletedId") != null)
                {
                    sINCMedInfoDeletedId = objXmlIn.SelectSingleNode("//Event/EventPSOData/txtINCMedInfoDeletedId").InnerText;
                    arrINCMedInfoDeletedId = sINCMedInfoDeletedId.Split('|');
                }
                for (int iCount = 0; iCount < arrINCMedInfoDeletedId.Length; iCount++)
                {
                    if (Conversion.CastToType<int>(arrINCMedInfoDeletedId[iCount], out bIsSuccess) > 0)
                    {
                        lstDeletedINCMedInfoRowId.Add(arrINCMedInfoDeletedId[iCount]);
                    }
                }
                //Ends

                string sINCMedInfoSessionId = string.Empty;
                objOptionXmlElement = null;
                iIndex = 0;
                Hashtable objINCMedUniqueIds = new Hashtable();

                if (objXmlIn.SelectSingleNode("/Event/EventPSOData/txtINCMedInfoSessionId") != null)
                {
                    sINCMedInfoSessionId = objXmlIn.SelectSingleNode("/Event/EventPSOData/txtINCMedInfoSessionId").InnerText;
                }
                objSessionManager = null;
                int iINCMedRowId = -1;
                if (!string.IsNullOrEmpty(sINCMedInfoSessionId))
                {
                    string[] arrINCMEdInfoSessionId = sINCMedInfoSessionId.Split('~');

                    for (int iCount = 0; iCount < arrINCMEdInfoSessionId.Length; iCount++)
                    {
                        if (!string.IsNullOrEmpty(arrINCMEdInfoSessionId[iCount]))
                        {
                            string sSessionRawData = string.Empty;
                            objSessionManager = base.GetSessionObject();
                            sSessionRawData = Riskmaster.Common.Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(arrINCMEdInfoSessionId[iCount])).ToString();

                            XmlDocument objTempXML = new XmlDocument();
                            XmlDocument objPropStore = new XmlDocument();
                            objTempXML.LoadXml(sSessionRawData);
                            objPropStore.LoadXml(objTempXML.SelectSingleNode("MedicationInfo").OuterXml);

                            MedicationInfo objINCMedInfo = (MedicationInfo)objPSOFormManager.DataModelFactory.GetDataModelObject("MedicationInfo", false);
                            XmlNode objDelNode = objPropStore.SelectSingleNode("/MedicationInfo/txtMedInfoSessionId");
                            if (objDelNode != null)
                            {
                                objDelNode.ParentNode.RemoveChild(objDelNode);
                            }

                            //*************
                            int iMedInfoRowId = 0;
                            if (objPropStore.SelectSingleNode("/MedicationInfo/MedRowId") != null)
                            {
                                iMedInfoRowId = Conversion.CastToType<int>(objPropStore.SelectSingleNode("/MedicationInfo/MedRowId").InnerText, out bIsSuccess);

                            }
                            if (iMedInfoRowId == 0)
                            {
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "MedRowId", Convert.ToString(iINCMedRowId));
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "EvtPsoRowId", Convert.ToString(iINCMedRowId));
                                objINCMedInfo.PopulateObject(objPropStore);
                                objEventPSO.MedInfoList.Add(objINCMedInfo, true);
                                objINCMedUniqueIds.Add(objINCMedInfo.MedRowId, arrINCMEdInfoSessionId[iCount]);
                                iINCMedRowId = iINCMedRowId - 1;
                            }
                            else
                            {
                                objEventPSO.MedInfoList[iMedInfoRowId].PopulateObject(objPropStore);
                            }
                            //**************
                            objSessionManager = null;
                        }

                    }
                }

                //Block for Deleting the Deleted rows from the collection Starts
                foreach (MedicationInfo objMedInfo in objEventPSO.MedInfoList)
                {
                    if (arrINCMedInfoDeletedId.Contains(objMedInfo.MedRowId.ToString()))
                    {
                        if (objMedInfo.MedRowId > 0)
                        {
                            objEventPSO.MedInfoList.Remove(objMedInfo.MedRowId);
                        }
                    }
                }
                //Ends

                //This block will handle the deleted row ids of the grids starts
                string sNMMedInfoDeletedId = string.Empty;
                string[] arrNMMedInfoDeletedId = null;
                if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtNMMedInfoDeletedId") != null)
                {
                    sNMMedInfoDeletedId = objXmlIn.SelectSingleNode("//Event/EventPSOData/txtNMMedInfoDeletedId").InnerText;
                    arrNMMedInfoDeletedId = sNMMedInfoDeletedId.Split('|');
                }
                for (int iCount = 0; iCount < arrNMMedInfoDeletedId.Length; iCount++)
                {
                    if (Conversion.CastToType<int>(arrNMMedInfoDeletedId[iCount], out bIsSuccess) > 0)
                    {
                        lstDeletedNMMedInfoRowId.Add(arrNMMedInfoDeletedId[iCount]);
                    }
                }
                //Ends
                string sNMMedInfoSessionId = string.Empty;
                objOptionXmlElement = null;
                iIndex = 0;
                Hashtable objNMMedUniqueIds = new Hashtable();

                if (objXmlIn.SelectSingleNode("/Event/EventPSOData/txtNMMedInfoSessionId") != null)
                {
                    sNMMedInfoSessionId = objXmlIn.SelectSingleNode("/Event/EventPSOData/txtNMMedInfoSessionId").InnerText;
                }
                objSessionManager = null;
                int iNMMedRowId = -1;
                if (!string.IsNullOrEmpty(sNMMedInfoSessionId))
                {
                    string[] arrNMMEdInfoSessionId = sNMMedInfoSessionId.Split('~');

                    for (int iCount = 0; iCount < arrNMMEdInfoSessionId.Length; iCount++)
                    {
                        if (!string.IsNullOrEmpty(arrNMMEdInfoSessionId[iCount]))
                        {
                            string sSessionRawData = string.Empty;
                            objSessionManager = base.GetSessionObject();
                            sSessionRawData = Riskmaster.Common.Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(arrNMMEdInfoSessionId[iCount])).ToString();

                            XmlDocument objTempXML = new XmlDocument();
                            XmlDocument objPropStore = new XmlDocument();
                            objTempXML.LoadXml(sSessionRawData);
                            objPropStore.LoadXml(objTempXML.SelectSingleNode("MedicationInfo").OuterXml);

                            MedicationInfo objNMMedInfo = (MedicationInfo)objPSOFormManager.DataModelFactory.GetDataModelObject("MedicationInfo", false);
                            XmlNode objDelNode = objPropStore.SelectSingleNode("/MedicationInfo/txtMedInfoSessionId");
                            if (objDelNode != null)
                            {
                                objDelNode.ParentNode.RemoveChild(objDelNode);
                            }

                            //********************
                            //*************
                            int iMedInfoRowId = 0;
                            if (objPropStore.SelectSingleNode("/MedicationInfo/MedRowId") != null)
                            {
                                iMedInfoRowId = Conversion.CastToType<int>(objPropStore.SelectSingleNode("/MedicationInfo/MedRowId").InnerText, out bIsSuccess);

                            }
                            if (iMedInfoRowId == 0)
                            {
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "MedRowId", Convert.ToString(iNMMedRowId));
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "EvtPsoRowId", Convert.ToString(iNMMedRowId));
                                objNMMedInfo.PopulateObject(objPropStore);
                                objEventPSO.MedInfoList.Add(objNMMedInfo, true);
                                objNMMedUniqueIds.Add(objNMMedInfo.MedRowId, arrNMMEdInfoSessionId[iCount]);
                                iNMMedRowId = iNMMedRowId - 1;
                            }
                            else
                            {
                                objEventPSO.MedInfoList[iMedInfoRowId].PopulateObject(objPropStore);
                            }
                            //**************
                            //************************
                            objSessionManager = null;
                        }

                    }
                }
                //Block for Deleting the Deleted rows from the collection Starts
                foreach (MedicationInfo objMedInfo in objEventPSO.MedInfoList)
                {
                    if (arrNMMedInfoDeletedId.Contains(objMedInfo.MedRowId.ToString()))
                    {
                        if (objMedInfo.MedRowId> 0)
                        {
                            objEventPSO.MedInfoList.Remove(objMedInfo.MedRowId);
                        }
                    }
                }
                //Ends

                //This block will handle the deleted row ids of the grids starts
                string sUSCMedInfoDeletedId = string.Empty;
                string[] arrUSCMedInfoDeletedId = null;
                if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtUSCMedInfoDeletedId") != null)
                {
                    sUSCMedInfoDeletedId = objXmlIn.SelectSingleNode("//Event/EventPSOData/txtUSCMedInfoDeletedId").InnerText;
                    arrUSCMedInfoDeletedId = sUSCMedInfoDeletedId.Split('|');
                }
                for (int iCount = 0; iCount < arrUSCMedInfoDeletedId.Length; iCount++)
                {
                    if (Conversion.CastToType<int>(arrUSCMedInfoDeletedId[iCount], out bIsSuccess) > 0)
                    {
                        lstDeletedUSCMedInfoRowId.Add(arrUSCMedInfoDeletedId[iCount]);
                    }
                }
                //Ends
                string sUSCMedInfoSessionId = string.Empty;
                objOptionXmlElement = null;
                iIndex = 0;
                Hashtable objUSCMedUniqueIds = new Hashtable();

                if (objXmlIn.SelectSingleNode("/Event/EventPSOData/txtUSCMedInfoSessionId") != null)
                {
                    sUSCMedInfoSessionId = objXmlIn.SelectSingleNode("/Event/EventPSOData/txtUSCMedInfoSessionId").InnerText;
                }
                objSessionManager = null;
                int iUSCMedRowId = -1;
                if (!string.IsNullOrEmpty(sUSCMedInfoSessionId))
                {
                    string[] arrUSCMEdInfoSessionId = sUSCMedInfoSessionId.Split('~');

                    for (int iCount = 0; iCount < arrUSCMEdInfoSessionId.Length; iCount++)
                    {
                        if (!string.IsNullOrEmpty(arrUSCMEdInfoSessionId[iCount]))
                        {
                            string sSessionRawData = string.Empty;
                            objSessionManager = base.GetSessionObject();
                            sSessionRawData = Riskmaster.Common.Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(arrUSCMEdInfoSessionId[iCount])).ToString();

                            XmlDocument objTempXML = new XmlDocument();
                            XmlDocument objPropStore = new XmlDocument();
                            objTempXML.LoadXml(sSessionRawData);
                            objPropStore.LoadXml(objTempXML.SelectSingleNode("MedicationInfo").OuterXml);

                            MedicationInfo objUSCMedInfo = (MedicationInfo)objPSOFormManager.DataModelFactory.GetDataModelObject("MedicationInfo", false);
                            XmlNode objDelNode = objPropStore.SelectSingleNode("/MedicationInfo/txtMedInfoSessionId");
                            if (objDelNode != null)
                            {
                                objDelNode.ParentNode.RemoveChild(objDelNode);
                            }

                            //***********
                            int iMedInfoRowId = 0;
                            if (objPropStore.SelectSingleNode("/MedicationInfo/MedRowId") != null)
                            {
                                iMedInfoRowId = Conversion.CastToType<int>(objPropStore.SelectSingleNode("/MedicationInfo/MedRowId").InnerText, out bIsSuccess);

                            }
                            if (iMedInfoRowId == 0)
                            {
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "MedRowId", Convert.ToString(iUSCMedRowId));
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "EvtPsoRowId", Convert.ToString(iUSCMedRowId));
                                objUSCMedInfo.PopulateObject(objPropStore);
                                objEventPSO.MedInfoList.Add(objUSCMedInfo, true);
                                objUSCMedUniqueIds.Add(objUSCMedInfo.MedRowId, arrUSCMEdInfoSessionId[iCount]);
                                iUSCMedRowId = iUSCMedRowId - 1;
                            }
                            else
                            {
                                objEventPSO.MedInfoList[iMedInfoRowId].PopulateObject(objPropStore);
                            }
                            //********************
                            objSessionManager = null;
                        }

                    }
                }

                foreach (MedicationInfo objMedInfo in objEventPSO.MedInfoList)
                {
                    if (arrUSCMedInfoDeletedId.Contains(objMedInfo.MedRowId.ToString()))
                    {
                        if (objMedInfo.MedRowId> 0)
                        {
                            objEventPSO.MedInfoList.Remove(objMedInfo.MedRowId);
                        }
                    }
                }
                //Linked Event Grid Populate starts

                //This block will handle the deleted row ids of the grids starts
                string sLnkEvtInfoDeletedId = string.Empty;
                string[] arrLnkEvtInfoDeletedId = null;
                if (objXmlIn.SelectSingleNode("//Event/EventPSOData/txtLnkEvtInfoDeletedId") != null)
                {
                    sLnkEvtInfoDeletedId = objXmlIn.SelectSingleNode("//Event/EventPSOData/txtLnkEvtInfoDeletedId").InnerText;
                    arrLnkEvtInfoDeletedId = sLnkEvtInfoDeletedId.Split('|');
                }
                for (int iCount = 0; iCount < arrLnkEvtInfoDeletedId.Length; iCount++)
                {
                    if (Conversion.CastToType<int>(arrLnkEvtInfoDeletedId[iCount], out bIsSuccess) > 0)
                    {
                        lstDeletedLnkEvtRowId.Add(arrLnkEvtInfoDeletedId[iCount]);
                    }
                }
                //Ends
                string sLnkEvtInfoSessionId = string.Empty;
                objOptionXmlElement = null;
                iIndex = 0;
                Hashtable objLnkEvtUniqueIds = new Hashtable();

                if (objXmlIn.SelectSingleNode("/Event/EventPSOData/txtLnkEvtInfoSessionId") != null)
                {
                    sLnkEvtInfoSessionId = objXmlIn.SelectSingleNode("/Event/EventPSOData/txtLnkEvtInfoSessionId").InnerText;
                }
                objSessionManager = null;
                int iLnkEvtRowId = -1;
                if (!string.IsNullOrEmpty(sLnkEvtInfoSessionId))
                {
                    string[] arrLnkEvtInfoSessionId = sLnkEvtInfoSessionId.Split('~');

                    for (int iCount = 0; iCount < arrLnkEvtInfoSessionId.Length; iCount++)
                    {
                        if (!string.IsNullOrEmpty(arrLnkEvtInfoSessionId[iCount]))
                        {
                            string sSessionRawData = string.Empty;
                            objSessionManager = base.GetSessionObject();
                            sSessionRawData = Riskmaster.Common.Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(arrLnkEvtInfoSessionId[iCount])).ToString();

                            XmlDocument objTempXML = new XmlDocument();
                            XmlDocument objPropStore = new XmlDocument();
                            objTempXML.LoadXml(sSessionRawData);
                            objPropStore.LoadXml(objTempXML.SelectSingleNode("LinkedEventInfo").OuterXml);

                            LinkedEventInfo objLnkEvtInfo = (LinkedEventInfo)objPSOFormManager.DataModelFactory.GetDataModelObject("LinkedEventInfo", false);
                            XmlNode objDelNode = objPropStore.SelectSingleNode("/LinkedEventInfo/txtLnkEvtInfoSessionId");
                            if (objDelNode != null)
                            {
                                objDelNode.ParentNode.RemoveChild(objDelNode);
                            }

                            //******************
                            int iLnkEvtInfoRowId = 0;
                            if (objPropStore.SelectSingleNode("/LinkedEventInfo/LnkEventRowId") != null)
                            {
                                iLnkEvtInfoRowId = Conversion.CastToType<int>(objPropStore.SelectSingleNode("/LinkedEventInfo/LnkEventRowId").InnerText, out bIsSuccess);

                            }
                            if (iLnkEvtInfoRowId == 0)
                            {
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "LnkEventRowId", Convert.ToString(iLnkEvtRowId));
                                objPSOFormManager.CreateAndSetElement(objPropStore.DocumentElement, "EvtPsoRowId", Convert.ToString(iLnkEvtRowId));
                                objLnkEvtInfo.PopulateObject(objPropStore);
                                objEventPSO.LinkedEventInfoList.Add(objLnkEvtInfo, true);
                                objLnkEvtUniqueIds.Add(objLnkEvtInfo.LnkEventRowId, arrLnkEvtInfoSessionId[iCount]);
                                iLnkEvtRowId = iLnkEvtRowId - 1;
                            }
                            else
                            {
                                objEventPSO.LinkedEventInfoList[iLnkEvtInfoRowId].PopulateObject(objPropStore);
                            }
                            //************************
                            objSessionManager = null;
                        }

                    }
                }
                //Block for Deleting the Deleted rows from the collection Starts
                foreach (LinkedEventInfo objLnkEvt in objEventPSO.LinkedEventInfoList)
                {
                    if (arrLnkEvtInfoDeletedId.Contains(objLnkEvt.LnkEventRowId.ToString()))
                    {
                        if (objLnkEvt.LnkEventRowId > 0)
                        {
                            objEventPSO.LinkedEventInfoList.Remove(objLnkEvt.LnkEventRowId);
                        }
                    }
                }
                //Ends
                objEventPSO.DataChanged = true;
                objEventPSO.Save();
                
                CreateAndAttachPDF(objXmlIn, objPSOFormManager, xmlTempDeviceDom, xmlTempLnkEvtDom, xmlTempMedDom, objEventPSO);
                XmlElement objTempElemet = (XmlElement)objXmlIn.SelectSingleNode("/Event/EventPSO/EvtPsoRowId");
                objTempElemet.InnerText = Convert.ToString(objEventPSO.EvtPsoRowId);

                objTempElemet = (XmlElement)objXmlIn.SelectSingleNode("/Event/PatientPSO/PatPsoRowId");
                objTempElemet.InnerText = Convert.ToString(objPatientPSO.PatPsoRowId);

                objXmlOut = objXmlIn;
            }
            catch (RMAppException p_objException)
            {
                objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                objErrOut.Add(p_objException, Globalization.GetString("PSOFormAdaptor.SavePSOData.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }
        private void CreateAndAttachPDF(XmlDocument objXmlIn, PSOFormManager objPSOFormManager, XmlDocument xmlTempDeviceDom, XmlDocument xmlTempLnkEvtDom, XmlDocument xmlTempMedDom, EventPSO objEvtPSO)
        {
            bool bIsSuccess = false;
            List<string> lstHiddenControl = new List<string>();
            List<string> lstRemoveHiddenControls = new List<string>();
            List<string> lstPanelControl = new List<string>();
            XmlDocument xmlDom = new XmlDocument();
            Hashtable hsTitle = new Hashtable();
            Hashtable hsValue = new Hashtable();
            MemoryStream objPDFMemStream = null;
            Event objEvent = (Event)objPSOFormManager.DataModelFactory.GetDataModelObject("Event", false);

            try
            {
                xmlDom.LoadXml(objXmlIn.SelectSingleNode("//Event/FormValues").InnerText);
                int iEvent = 0;
                if ((XmlElement)objXmlIn.SelectSingleNode("/Event/EventPSOData/eventid") != null)
                {
                    iEvent = Conversion.CastToType<int>(objXmlIn.SelectSingleNode("/Event/EventPSOData/eventid").InnerText, out bIsSuccess);
                    if (iEvent != 0)
                    {
                        objEvent.MoveTo(iEvent);
                        PopulateXMLDom(objEvent, ref objXmlIn, objPSOFormManager, lstHiddenControl);
                    }

                }

                PopulateDomValue(ref xmlDom, objXmlIn, objPSOFormManager, "//Event/EventPSO", false);
                PopulateDomValue(ref xmlDom, objXmlIn, objPSOFormManager, "//Event/PatientPSO", false);
                PopulateDomValue(ref xmlDom, objXmlIn, objPSOFormManager, "//Event/EventPSOData", false);
                PopulateDomValue(ref xmlDom, objXmlIn, objPSOFormManager, "//Event/PatientPSOData", false);

                HideDisplayXml(ref xmlDom, lstHiddenControl, lstRemoveHiddenControls, objPSOFormManager);

                for (int iCount = 0; iCount < lstRemoveHiddenControls.Count; iCount++)
                {
                    if (lstHiddenControl.Contains(lstRemoveHiddenControls[iCount]))
                    {
                        lstHiddenControl.RemoveAll(sValue => sValue == lstRemoveHiddenControls[iCount]);
                    }
                }

                for (int iCount = 0; iCount < lstHiddenControl.Count; iCount++)
                {
                    if (lstHiddenControl[iCount].Contains("pnl"))
                    {
                        GetPanelControls(lstHiddenControl[iCount], lstPanelControl, xmlDom);
                    }
                }

                CreateTitleValues(xmlDom, objPSOFormManager, lstHiddenControl, lstPanelControl, hsTitle, hsValue, xmlTempDeviceDom, xmlTempLnkEvtDom, xmlTempMedDom, objEvtPSO);

                if (hsTitle.Count > 0)
                {
                    CreatePDF(hsTitle, hsValue, objEvent, objPSOFormManager, ref objPDFMemStream);
                    AttachPDF(objPDFMemStream, objPSOFormManager, objEvent);
                }

            }
            catch (RMAppException objEx)
            {
                throw objEx;
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("PSOFormAdaptor.CreateAndAttachPDF.Error",base.ClientId), objEx);
            }
            finally
            {

            }
        }

        private void CreateTitleValues(XmlDocument xmlDom, PSOFormManager objPSOFormManager, List<string> lstHiddenControl, List<string> lstPanelControl, Hashtable hsTitle, Hashtable hsValue, XmlDocument xmlTempDeviceDom, XmlDocument xmlTempLnkEvtDom, XmlDocument xmlTempMedDom, EventPSO objEvtPSO)
        {
            int iCount = 0;
            string sValue = string.Empty;
            string sType = string.Empty;
            string sCodeTable = string.Empty;
            string sTitle = string.Empty;
            string sTempValue = string.Empty;
            string sFirstName = string.Empty;
            string sLastName = string.Empty;
            string sControlId = string.Empty;
            XmlDocument xmlTempDoc = null;
            try
            {
                foreach (XmlElement xEle in xmlDom.SelectNodes("//form/group/control"))
                {

                    if (xEle.Attributes["value"] != null)
                    {
                        sValue = xEle.Attributes["value"].Value.ToString();
                    }
                    sType = xEle.Attributes["type"].Value.ToString();
                    if (string.Compare(sType, "grid", true) != 0)
                    {
                        if (string.IsNullOrEmpty(sValue) || sValue == "0")
                        {
                            continue;
                        }
                    }
                    if (string.Compare(sType, "DATE", true) == 0)
                    {
                        if (!sValue.Contains("/"))
                        {
                            sValue = Conversion.GetDBDateFormat(sValue, "MM/dd/yyyy");
                        }
                    }
                    if (string.Compare(sType, "TIME", true) == 0)
                    {
                        sValue = Conversion.GetTimeAMPM(sValue);
                    }
                    sControlId = xEle.Attributes["id"].Value.ToString();
                    if (lstHiddenControl.Contains(sControlId) || lstPanelControl.Contains(sControlId))
                    {
                        continue;
                    }
                    sTitle = xEle.Attributes["title"].Value.ToString();

                    if (xEle.Attributes["codetable"] != null)
                    {
                        sCodeTable = xEle.Attributes["codetable"].Value.ToString();
                    }
                    if (string.Compare(sType, "grid", true) == 0)
                    {
                        switch (sControlId)
                        {
                            case "DevicesGrid":
                                xmlTempDoc = xmlTempDeviceDom;
                                break;
                            case "INCMEDGrid":
                            case "NMMEDGrid":
                            case "USCMEDGrid":
                                xmlTempDoc = xmlTempMedDom;
                                break;
                            case "LinkedEventsGrid":
                                xmlTempDoc = xmlTempLnkEvtDom;
                                break;
                        }
                        GetGridTitleValues(ref hsTitle, ref hsValue, ref iCount, objEvtPSO, sControlId, xmlTempDoc, objPSOFormManager);
                        continue;
                    }
                    GetCodeMultiCodeValue(sType, sCodeTable, objPSOFormManager, ref sValue);
                    iCount++;
                    hsTitle.Add(iCount, sTitle);
                    hsValue.Add(iCount, sValue);
                }
            }
            catch (RMAppException objEx)
            {
                throw objEx;
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("PSOFormAdaptor.CreateTitleValues.Error",base.ClientId), objEx);
            }
            finally
            {

            }
        }

        private void GetGridTitleValues(ref Hashtable hsTitle, ref Hashtable hsValue, ref int iCount, EventPSO objEvtPSO, string sGridName, XmlDocument xmlTempDoc, PSOFormManager objPSOFormManager)
        {
            XmlDocument objPropStoreTemp = new XmlDocument();
            XmlDocument objSerilzationXML = new XmlDocument();
            List<string> lstHiddenControl = new List<string>();
            List<string> lstRemoveHiddenControls = new List<string>();

            if (string.Compare("DevicesGrid", sGridName, true) == 0)
            {
                if (objEvtPSO.DeviceInfoList.Count > 0)
                {
                    iCount++;
                    hsTitle.Add(iCount, "Device Information:");
                    hsValue.Add(iCount, "");
                }
                foreach (DeviceInfo objDeviceInfo in objEvtPSO.DeviceInfoList)
                {
                    objSerilzationXML.LoadXml("<DeviceInfo></DeviceInfo>");
                    objPropStoreTemp.LoadXml(objDeviceInfo.SerializeObject(objSerilzationXML));
                    PopulateDomValue(ref xmlTempDoc, objPropStoreTemp, objPSOFormManager, "//DeviceInfo", true);
                    HideDisplayXml(ref xmlTempDoc, lstHiddenControl, lstRemoveHiddenControls, objPSOFormManager);

                    for (int iGridCount = 0; iGridCount < lstRemoveHiddenControls.Count; iGridCount++)
                    {
                        if (lstHiddenControl.Contains(lstRemoveHiddenControls[iGridCount]))
                        {
                            lstHiddenControl.RemoveAll(sValue => sValue == lstRemoveHiddenControls[iGridCount]);
                        }
                    }
                    

                    GetXMLValue(xmlTempDoc, ref hsTitle, ref hsValue, ref iCount, objPSOFormManager, lstHiddenControl);
                }
            }
            else if (string.Compare("INCMEDGrid", sGridName, true) == 0 || string.Compare("NMMEDGrid", sGridName, true) == 0 || string.Compare("USCMEDGrid", sGridName, true) == 0)
            {
                if (objEvtPSO.MedInfoList.Count > 0)
                {
                    iCount++;
                    hsTitle.Add(iCount, "Medication Information:");
                    hsValue.Add(iCount, "");
                }
                foreach (MedicationInfo objMedicationInfo in objEvtPSO.MedInfoList)
                {
                    objSerilzationXML.LoadXml("<MedicationInfo></MedicationInfo>");
                    objPropStoreTemp.LoadXml(objMedicationInfo.SerializeObject(objSerilzationXML));
                    PopulateDomValue(ref xmlTempDoc, objPropStoreTemp, objPSOFormManager, "//MedicationInfo", true);
                    HideDisplayXml(ref xmlTempDoc, lstHiddenControl, lstRemoveHiddenControls, objPSOFormManager);

                    for (int iGridCount = 0; iGridCount < lstRemoveHiddenControls.Count; iGridCount++)
                    {
                        if (lstHiddenControl.Contains(lstRemoveHiddenControls[iGridCount]))
                        {
                            lstHiddenControl.RemoveAll(sValue => sValue == lstRemoveHiddenControls[iGridCount]);
                        }
                    }
                    

                    GetXMLValue(xmlTempDoc, ref hsTitle, ref hsValue, ref iCount, objPSOFormManager, lstHiddenControl);
                }
            }
            else if (string.Compare("LinkedEventsGrid", sGridName, true) == 0)
            {
                if (objEvtPSO.LinkedEventInfoList.Count > 0)
                {
                    iCount++;
                    hsTitle.Add(iCount, "Linked Event Information:");
                    hsValue.Add(iCount, "");
                }
                foreach (LinkedEventInfo objLnkEvtInfo in objEvtPSO.LinkedEventInfoList)
                {
                    objSerilzationXML.LoadXml("<LinkedEventInfo></LinkedEventInfo>");
                    objPropStoreTemp.LoadXml(objLnkEvtInfo.SerializeObject(objSerilzationXML));
                    PopulateDomValue(ref xmlTempDoc, objPropStoreTemp, objPSOFormManager, "//LinkedEventInfo", true);
                    HideDisplayXml(ref xmlTempDoc, lstHiddenControl, lstRemoveHiddenControls, objPSOFormManager);

                    for (int iGridCount = 0; iGridCount < lstRemoveHiddenControls.Count; iGridCount++)
                    {
                        if (lstHiddenControl.Contains(lstRemoveHiddenControls[iGridCount]))
                        {
                            lstHiddenControl.RemoveAll(sValue => sValue == lstRemoveHiddenControls[iGridCount]);
                        }
                    }
                    

                    GetXMLValue(xmlTempDoc, ref hsTitle, ref hsValue, ref iCount, objPSOFormManager, lstHiddenControl);
                }
            }
        }

        private void GetXMLValue(XmlDocument xmlTempDoc, ref Hashtable hsTitle, ref Hashtable hsValue, ref int iCount, PSOFormManager objPSOFormManager, List<string> lstHiddenControl)
        {
            string sValue = string.Empty;
            string sType = string.Empty;
            string sCodeTable = string.Empty;
            string sTitle = string.Empty;
            string sTempValue = string.Empty;
            string sFirstName = string.Empty;
            string sLastName = string.Empty;
            string sControlId = string.Empty;
            string sControlName = string.Empty;

            foreach (XmlElement xEle in xmlTempDoc.SelectNodes("//form/group/control"))
            {

                if (xEle.Attributes["value"] != null)
                {
                    sValue = xEle.Attributes["value"].Value.ToString();
                }
                sType = xEle.Attributes["type"].Value.ToString();
                if (string.Compare(sType, "DATE", true) == 0)
                {
                    sValue = Conversion.GetDBDateFormat(sValue, "MM/dd/yyyy");
                }
                if (string.Compare(sType, "TIME", true) == 0)
                {
                    sValue = Conversion.GetTimeAMPM(sValue);
                }
                if (string.IsNullOrEmpty(sValue) || sValue == "0")
                {
                    continue;
                }
                sControlId = xEle.Attributes["id"].Value.ToString();
                if (lstHiddenControl.Contains(sControlId))
                {
                    continue;
                }
                
                sTitle = xEle.Attributes["title"].Value.ToString();
                sControlName = xEle.Attributes["id"].Value.ToString();

                if (xEle.Attributes["codetable"] != null)
                {
                    sCodeTable = xEle.Attributes["codetable"].Value.ToString();
                }
                
                GetCodeMultiCodeValue(sType, sCodeTable, objPSOFormManager, ref sValue);

                
                iCount++;
                hsTitle.Add(iCount, sTitle);
                hsValue.Add(iCount, sValue);
            }
        }
        private void GetCodeMultiCodeValue(string sType, string sCodeTable, PSOFormManager objPSOFormManager, ref string sValue)
        {
            string sTempValue = string.Empty;
            string sFirstName = string.Empty;
            string sLastName = string.Empty;
            bool bIsSuccess = false;
            try
            {
                if (string.Compare(sType, "code", true) == 0)
                {
                    switch (sCodeTable)
                    {
                        case "DEPT_EID":
                        case "DEPTINV_EID":
                            sValue = objPSOFormManager.DataModelFactory.Context.LocalCache.GetEntityAbbreviation(Conversion.CastToType<int>(sValue, out bIsSuccess)) + " " + objPSOFormManager.DataModelFactory.Context.LocalCache.GetEntityLastName(Conversion.CastToType<int>(sValue, out bIsSuccess));
                            break;
                        case "STATES":
                            string sStateId = string.Empty;
                            string sStateName = string.Empty;
                            objPSOFormManager.DataModelFactory.Context.LocalCache.GetStateInfo(Conversion.CastToType<int>(sValue, out bIsSuccess), ref sStateId, ref sStateName);
                            sValue = (string)sStateId + " " + (string)sStateName;
                            break;
                        case "DEPARTMENT":
                        case "COMPANY":
                        case "FACILITY_DEPARTMENT":
                        case "LOCATION":
                        case "FACILITY":
                            sValue = objPSOFormManager.DataModelFactory.Context.LocalCache.GetEntityAbbreviation(Conversion.CastToType<int>(sValue, out bIsSuccess)) + " " + objPSOFormManager.DataModelFactory.Context.LocalCache.GetEntityLastName(Conversion.CastToType<int>(sValue, out bIsSuccess));
                            break;
                        default:
                            sValue = objPSOFormManager.DataModelFactory.Context.LocalCache.GetShortCode(Conversion.CastToType<int>(sValue, out bIsSuccess)) + " " + objPSOFormManager.DataModelFactory.Context.LocalCache.GetCodeDesc(Conversion.CastToType<int>(sValue, out bIsSuccess));
                            break;
                    }
                }
                else if (string.Compare(sType, "multicode", true) == 0)
                {
                    sTempValue = sValue;
                    sValue = string.Empty;
                    for (int iValueCount = 0; iValueCount < sTempValue.Split(' ').Length; iValueCount++)
                    {
                        switch (sCodeTable)
                        {
                            case "DEPARTMENT":
                            case "FACILITY":
                                objPSOFormManager.DataModelFactory.Context.LocalCache.GetEntityInfo((Conversion.CastToType<int>(sTempValue.Split(' ')[iValueCount], out bIsSuccess)), ref sFirstName, ref sLastName);
                                sValue = sValue + ", " + objPSOFormManager.DataModelFactory.Context.LocalCache.GetEntityAbbreviation(Conversion.CastToType<int>(sTempValue.Split(' ')[iValueCount], out bIsSuccess)) + " " + sFirstName + " " + sLastName;
                                break;
                            default:
                                sValue = sValue + "," + objPSOFormManager.DataModelFactory.Context.LocalCache.GetShortCode(Conversion.CastToType<int>(sTempValue.Split(' ')[iValueCount], out bIsSuccess)) + " " + objPSOFormManager.DataModelFactory.Context.LocalCache.GetCodeDesc(Conversion.CastToType<int>(sTempValue.Split(' ')[iValueCount], out bIsSuccess));
                                break;
                        }
                    }
                    sValue = sValue.Remove(0, 1);
                }
            }
            catch (RMAppException objEx)
            {
                throw objEx;
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("PSOFormAdaptor.GetCodeMultiCodeValue.Error",base.ClientId), objEx);
            }
            finally
            {

            }
        }

        private void CreatePDF(Hashtable hsTitle, Hashtable hsValue, Event objEvent, PSOFormManager objPSOFormManager, ref MemoryStream objPDFMemStream)
        {
            string sOutputValue = string.Empty;
            CustPdfEvents objPdfEvt = null;

            const string sPDFHeader = "PATIENT SAFETY EVENT REPORT";

            try
            {
                objPDFMemStream = new MemoryStream();
                iTextSharp.text.Document PdfDocument = new iTextSharp.text.Document(PageSize.LETTER);

                PdfWriter writer = PdfWriter.GetInstance(PdfDocument, objPDFMemStream);

                PdfDocument.SetMargins(5, 5, 50, 3);
                BaseFont Normalf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, BaseFont.EMBEDDED);
                Font Font1 = new Font(Normalf, 8f);
                string sYear = System.DateTime.Now.ToString("yyyy");
                HeaderFooter footer = new HeaderFooter(new Phrase(" ", Font1), true);
                footer.Border = Rectangle.NO_BORDER;
                footer.Alignment = HeaderFooter.ALIGN_RIGHT;
                PdfDocument.Footer = footer;

                BaseFont p_bf = BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.WINANSI, BaseFont.EMBEDDED);
                string sPDFTitle = objEvent.EventNumber;

                objPdfEvt = new CustPdfEvents(p_bf, sPDFTitle, sPDFHeader, base.ClientId);
                writer.PageEvent = objPdfEvt;
                PdfDocument.Open();
                PdfContentByte p_cb = writer.DirectContent;

                PdfPTable tb = new PdfPTable(2);
                tb.SplitLate = false;
                tb.TotalWidth = 600;
                BaseFont Normal = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, BaseFont.EMBEDDED);
                Font fTable = new Font(Normal, 8f);

                int iRowsCount = hsTitle.Count;

                int i = 0;
                float fYFactor = 60;
                for (i = 1; i <= hsTitle.Count; i++)
                {
                    //Cell for fields
                    sOutputValue = hsTitle[i].ToString();
                    Phrase ph1 = new Phrase(sOutputValue, fTable);
                    tb.AddCell(ph1);
                    //Cell for Values
                    sOutputValue = hsValue[i].ToString();
                    Phrase ph2 = new Phrase(sOutputValue, fTable);
                    tb.AddCell(ph2);

                    if (ph1 != null)
                        ph1 = null;
                    if (ph2 != null)
                        ph2 = null;
                }
                float fstartpos = tb.TotalHeight + 60 + 10 + 10;
                PdfDocument.Add(tb);

                PdfDocument.Close();
                objPDFMemStream.Close();
                objPDFMemStream.Dispose();

            }
            catch (RMAppException objEx)
            {
                throw objEx;
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("PSOFormAdaptor.CreatePDF.Error",base.ClientId), objEx);
            }
            finally
            {

            }
        }

        private void AttachPDF(MemoryStream objPDFMemStream, PSOFormManager objPSOFormManager, Event objEvent)
        {
            long lDocumentId = 0;
            string sSQL = string.Empty;
            DocumentManager objDocumentManager = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;

            try
            {
                objDocumentManager = new DocumentManager(userLogin,base.ClientId);
                objDocumentManager.ConnectionString = objPSOFormManager.ConnectionString;
                objDocumentManager.SecurityConnectionString = RMConfigurationSettings.GetSecurityDSN(base.ClientId);
                objDocumentManager.UserLoginName = userLogin.LoginName;

                if (userLogin.DocumentPath.Length > 0)
                {
                    objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                }
                else
                {
                    if (userLogin.objRiskmasterDatabase.DocPathType == 0)
                    {
                        objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                    }
                    else
                    {
                        objDocumentManager.DocumentStorageType = StorageType.DatabaseStorage;
                    }
                }

                if (userLogin.DocumentPath.Length > 0)
                {
                    objDocumentManager.DestinationStoragePath = userLogin.DocumentPath;
                }
                else
                {
                    objDocumentManager.DestinationStoragePath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }

                XmlDocument objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("data");
                objDOM.AppendChild(objElemParent);

                objElemChild = objDOM.CreateElement("Document");

                // Document ID
                objElemTemp = objDOM.CreateElement("DocumentId");
                objElemTemp.InnerText = "0";
                objElemChild.AppendChild(objElemTemp);

                // Folder ID
                objElemTemp = objDOM.CreateElement("FolderId");
                objElemTemp.InnerText = "0";
                objElemChild.AppendChild(objElemTemp);

                // Creation date
                objElemTemp = objDOM.CreateElement("CreateDate");
                objElemTemp.InnerText = Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now);
                objElemChild.AppendChild(objElemTemp);

                // Category				
                objElemTemp = objDOM.CreateElement("Category");
                objElemTemp.InnerText = "0";
                objElemChild.AppendChild(objElemTemp);

                // Name of the attachment
                objElemTemp = objDOM.CreateElement("Name");
                objElemTemp.InnerText = s_FORM_NAME;
                objElemChild.AppendChild(objElemTemp);

                // Class of the attachment
                objElemTemp = objDOM.CreateElement("Class");
                objElemTemp.InnerText = "0";
                objElemChild.AppendChild(objElemTemp);

                // Subject of the attachment
                objElemTemp = objDOM.CreateElement("Subject");
                objElemTemp.InnerText = s_FORM_NAME;
                objElemChild.AppendChild(objElemTemp);

                // Type of the attachment
                objElemTemp = objDOM.CreateElement("Type");
                objElemTemp.InnerText = "0";
                objElemChild.AppendChild(objElemTemp);

                // Notes of the attachment
                objElemTemp = objDOM.CreateElement("Notes");
                objElemTemp.InnerText = "";
                objElemChild.AppendChild(objElemTemp);

                // User login name 
                objElemTemp = objDOM.CreateElement("UserLoginName");
                objElemTemp.InnerText = userLogin.LoginName;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("FileName");

                objElemTemp.InnerText = "PSO_" + objEvent.EventNumber + "_" + Conversion.ToDbDateTime(DateTime.Now) + s_FILE_EXTN;
                objElemChild.AppendChild(objElemTemp);

                // File path of the attachment
                objElemTemp = objDOM.CreateElement("FilePath");
                objElemTemp.InnerText = "";
                objElemChild.AppendChild(objElemTemp);

                // Keywords for the attachment
                objElemTemp = objDOM.CreateElement("Keywords");
                objElemChild.AppendChild(objElemTemp);

                // Table to which worksheet needs to be attached - claim

                objElemTemp = objDOM.CreateElement("AttachTable");
                objElemTemp.InnerText = s_ATTACH_TABLE;
                objElemChild.AppendChild(objElemTemp);
                // Id of the record to which worksheet needs to be attached.
                objElemTemp = objDOM.CreateElement("AttachRecordId");
                objElemTemp.InnerText = Convert.ToString(objEvent.EventId);


                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("FormName");
                objElemTemp.InnerText = s_ATTACH_TABLE;

                objElemChild.AppendChild(objElemTemp);

                objDOM.FirstChild.AppendChild(objElemChild);

                StringWriter sw = new StringWriter();
                XmlTextWriter xw = new XmlTextWriter(sw);

                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();

                //added code to check the configuration-shilpi

                objPSOFormManager.SecurityConnectionString = this.securityConnectionString;
                objDocumentManager.AddDocument(objDOM.InnerXml, objPDFMemStream, objPSOFormManager.GetEventFuncId(), out lDocumentId);

                objPDFMemStream.Close();
                objPDFMemStream.Dispose();

                objPSOFormManager.UpdateDocument();



            }
            catch (RMAppException objEx)
            {
                throw objEx;
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("PSOFormAdaptor.AttachPDF.Error",base.ClientId), objEx);
            }
            finally
            {
                if (objPDFMemStream != null)
                {
                    objPDFMemStream.Close();
                    objPDFMemStream.Dispose();
                }
            }
        }

        //Find Global.resx file as well and constant file for the future use
    }
    public class CustPdfEvents : PdfPageEventHelper
    {
        private string sPDFHeader;
        private BaseFont m_bf;
        private string sEvent = string.Empty;
        private int m_iClientId = 0;
        /// Name		    : CustPdfEvents
        /// Author		    : Animesh Sahai
        /// Date Created	: 08/05/2010
        /// <summary>
        ///     MITS 21177: for handling PDF header
        /// </summary>
        public CustPdfEvents()
        {

        }
        /// Name		    : OnStartPage
        /// Author		    : Animesh Sahai
        /// Date Created	: 08/05/2010
        /// <summary>
        ///     MITS 21177: for handling PDF header
        /// </summary>
        /// <params name="objTempImg">Header image object</params>
        /// <params name="m_tempbf">Base font </params>   
        /// <params name="strTempEvent">Event number</params>  
        public CustPdfEvents(BaseFont m_tempbf, string sTempEvent, string sPDFHeader, int p_iClientId)
        {
            m_bf = m_tempbf;
            sEvent = sTempEvent;
            m_iClientId = p_iClientId;
            this.sPDFHeader = sPDFHeader;
        }
        /// Name		    : OnStartPage
        /// Author		    : Animesh Sahai
        /// Date Created	: 08/05/2010
        /// <summary>
        ///     MITS 21177: for handling PDF header
        /// </summary>
        /// <params name="objWriter">Pdf writer object</params>
        /// <params name="objDoc">The TextSharp document.</params>        
        public override void OnStartPage(iTextSharp.text.pdf.PdfWriter objWriter, iTextSharp.text.Document objDoc)
        {
            try
            {
                PdfContentByte p_cb = objWriter.DirectContent;
                BaseFont bf = m_bf;
                PdfTemplate temp = p_cb.CreateTemplate(900, 44);
                temp.SetFontAndSize(bf, 12);
                temp.BeginText();
                string sAppName = sPDFHeader;
                temp.ShowTextAligned(PdfContentByte.ALIGN_LEFT, sAppName, 10, 25, 0);
                if (sEvent != "0")
                {
                    temp.SetFontAndSize(bf, 8);
                    temp.ShowTextAligned(PdfContentByte.ALIGN_LEFT, sEvent, 10, 13, 0);
                }
                temp.EndText();
                p_cb.AddTemplate(temp, 230, objDoc.PageSize.Height - 55);
                base.OnStartPage(objWriter, objDoc);
            }
            catch (RMAppException objEx)
            {
                throw objEx;
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("CustPdfEvents.OnStartPage.Error", m_iClientId), objEx);
            }
            finally
            {

            }
        }


    }
}
