﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.IO;
using Riskmaster.Models;
using System.Xml.XPath;
using Riskmaster.Settings;
using System.Xml.Linq;
using Riskmaster.DataModel;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Globalization;
using Riskmaster.Application.FAS;
using System.Net;
using System.Collections;

/// Name		: Create FAS Claim Data
/// Author		: Anshul Verma
/// Date Created: 24-June-2013
/// ************************************************************
/// <summary>		
/// This method is used to send claim data for FAS.
/// </summary>
///

namespace Riskmaster.BusinessAdaptor.FASDataAdaptor
{
    /// <summary>	
    ///	A class representing the FAS adaptor.
    /// </summary>
    public class FASAdaptor : BusinessAdaptorBase
    {
        private DataModelFactory m_objDataModelFactory = null;
        private List<FASReportable> objFASreporatblelst = new List<FASReportable>();
        private string m_sLoginName = "";
        private string m_sPassword = "";
        private string m_sDSNName = "";
        private string m_sConnectionString = "";        

        
        public FASAdaptor()
        {

        }
        public FASAdaptor(string p_sLoginName, string p_sPassword, string p_sDSNName, string p_sConnectionString, int p_iClientId)
        {
            m_sLoginName = p_sLoginName;
            m_sPassword = p_sPassword;
            m_sDSNName = p_sDSNName;
            m_sConnectionString = p_sConnectionString;
            m_objDataModelFactory = new DataModelFactory(m_sDSNName, m_sLoginName, m_sPassword, p_iClientId);
        }

        /// <summary>
        /// This method is a wrapper to the GetClaimTypes method in Riskmaster.Application.FASClaimData.GetClaimStatus
        /// </summary>
        /// <param name="p_objXmlIn">Input XML</param>
        /// <param name="p_objOutXML">Output XML</param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>
        /// <returns>True/False for Success/Failure of the method</returns>
        public bool GetClaimStatus(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
        {
            FASClaimData objFAS = null;
            try
            {
                objFAS = new FASClaimData(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);
                p_objOutXML = objFAS.GetClaimStatus(p_objXmlIn);

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objFAS = null;
            }
        }


        #region Create FAS Claim Data
        /// Name		: Create FAS Claim Data
        /// Author		: Anshul Verma
        /// Date Created: 12/26/2012
        /// ************************************************************
        /// <summary>		
        /// This method is used to send claim data for FAS.
        /// </summary>
        ///

        public void CreateFASClaimData(string p_sClaimStatus, string p_sClaimType, string p_sFromDateOfClaim, string p_sToDateOfClaim, string p_sFromClaimUpdateDate, string p_sToClaimUpdateDate,ref string p_sOutPut)
        {
            StringBuilder sbSQL = null;
            string sSql = string.Empty;
            StringBuilder objBuilder = null;
            LocalCache objLocalCache = null;
            XElement FASBaseTemplate = FASBaseAcordTemplate();
            string sFileName = Utilities.GenerateGuid() + ".xml";
            string sEnableFAS = string.Empty;
            try
            {
                //To Check whether FAS Setting is Enabled in General System Parameter or Not.
                using (DbReader oReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='ENABLE_FAS'"))
                {
                    if(oReader.Read())
                    {
                        sEnableFAS = Convert.ToString(oReader.GetValue("STR_PARM_VALUE")).ToString();
                    }
                }
                if (sEnableFAS == "-1")
                {
                    sbSQL = new StringBuilder();
                    objBuilder = new StringBuilder();
                    objLocalCache = new LocalCache(m_sConnectionString,base.ClientId);

                    sbSQL.Append("SELECT CLAIM.CLAIM_ID FROM CLAIM,RMA_FAS_CLAIM_EXP WHERE CLAIM.CLAIM_ID= RMA_FAS_CLAIM_EXP.CLAIM_ID AND RMA_FAS_CLAIM_EXP.FAS_CLAIM_FLAG=0");

                    if (!string.IsNullOrEmpty(p_sClaimStatus))
                    {
                        sbSQL.Append(" AND CLAIM.CLAIM_STATUS_CODE IN (" + p_sClaimStatus + ")");
                    }

                    if (!string.IsNullOrEmpty(p_sClaimType))
                    {
                        sbSQL.Append(" AND CLAIM.CLAIM_TYPE_CODE IN (" + p_sClaimType + ")");
                    }
                   
                    //asharma326 MITS 34853 For Claim Dates
                    if (!(string.IsNullOrEmpty(p_sFromDateOfClaim))) 
                    {
                        p_sFromDateOfClaim = DateTime.ParseExact(p_sFromDateOfClaim, "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
                         if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_SQLSRVR)
                         { sbSQL.Append(" AND SUBSTRING(CLAIM.DATE_OF_CLAIM,1,8) >= '"+p_sFromDateOfClaim +"' "); }
                         else
                         { sbSQL.Append(" AND SUBSTR(CLAIM.DATE_OF_CLAIM,1,8) >= '" + p_sFromDateOfClaim + "' "); }
                    }
                    if (!(string.IsNullOrEmpty(p_sToDateOfClaim)))
                    {
                        p_sToDateOfClaim = DateTime.ParseExact(p_sToDateOfClaim, "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
                        if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_SQLSRVR)
                        { sbSQL.Append(" AND SUBSTRING(CLAIM.DATE_OF_CLAIM,1,8) <= '" + p_sToDateOfClaim + "' "); }
                        else
                        { sbSQL.Append(" AND SUBSTR(CLAIM.DATE_OF_CLAIM,1,8) <= '" + p_sToDateOfClaim + "' "); }
                    }

                    if (!(string.IsNullOrEmpty(p_sFromClaimUpdateDate)))
                    {
                        p_sFromClaimUpdateDate = DateTime.ParseExact(p_sFromClaimUpdateDate, "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
                        if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_SQLSRVR)
                        {
                            sbSQL.Append(" AND SUBSTRING(CLAIM.DTTM_RCD_LAST_UPD,1,8) >= '" + p_sFromClaimUpdateDate + "' ");
                        }
                        else { sbSQL.Append(" AND SUBSTR(CLAIM.DTTM_RCD_LAST_UPD,1,8) >= '" + p_sFromClaimUpdateDate + "' "); }
                    }

                    if (!(string.IsNullOrEmpty(p_sToClaimUpdateDate)))
                    {
                        p_sToClaimUpdateDate = DateTime.ParseExact(p_sToClaimUpdateDate, "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
                        if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_SQLSRVR)
                        {
                            sbSQL.Append(" AND SUBSTRING(CLAIM.DTTM_RCD_LAST_UPD,1,8) <= '" + p_sToClaimUpdateDate + "' ");
                        }
                        else { sbSQL.Append(" AND SUBSTR(CLAIM.DTTM_RCD_LAST_UPD,1,8) <= '" + p_sToClaimUpdateDate + "' "); }
                    }

                        //if (!(string.IsNullOrEmpty(p_sFromDateOfClaim) && string.IsNullOrEmpty(p_sToDateOfClaim)))
                        //{
                        //    p_sFromDateOfClaim = DateTime.ParseExact(p_sFromDateOfClaim, "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
                        //    p_sToDateOfClaim = DateTime.ParseExact(p_sToDateOfClaim, "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
                        //    if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_SQLSRVR)
                        //    { sbSQL.Append(" AND SUBSTRING(CLAIM.DATE_OF_CLAIM,1,8) BETWEEN " + p_sFromDateOfClaim + " AND " + p_sToDateOfClaim + " "); }
                        //    else
                        //    { sbSQL.Append(" AND SUBSTR(CLAIM.DATE_OF_CLAIM,1,8) BETWEEN " + p_sFromDateOfClaim + " AND " + p_sToDateOfClaim + " "); }
                        //}
                        //if (!(string.IsNullOrEmpty(p_sFromClaimUpdateDate) && string.IsNullOrEmpty(p_sToClaimUpdateDate)))
                        //{
                        //    p_sFromClaimUpdateDate = DateTime.ParseExact(p_sFromClaimUpdateDate, "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
                        //    p_sToClaimUpdateDate = DateTime.ParseExact(p_sToClaimUpdateDate, "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd");
                        //    if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_SQLSRVR)
                        //    {
                        //        sbSQL.Append(" AND SUBSTRING(CLAIM.DTTM_RCD_LAST_UPD,1,8) BETWEEN " + p_sFromClaimUpdateDate + " AND " + p_sToClaimUpdateDate + " ");
                        //    }
                        //    else { sbSQL.Append(" AND SUBSTR(CLAIM.DTTM_RCD_LAST_UPD,1,8) BETWEEN " + p_sFromClaimUpdateDate + " AND " + p_sToClaimUpdateDate + " "); }
                        //}
                    //System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\test.txt");
                    //file.WriteLine(sbSQL.ToString());

                    //file.Close();

                    using (DbReader oDbReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                    {
                        int iClaimId = 0;
                        int iClaimCount = 0;
                        string sUpdateSQL = string.Empty;
                        string sFtpError = string.Empty;
                        ArrayList iArrayClaimId = new ArrayList();
                        while (oDbReader.Read())
                        {
                            iClaimId = Conversion.ConvertObjToInt(oDbReader.GetValue("CLAIM_ID").ToString(), base.ClientId);
                            iArrayClaimId.Add(iClaimId);
                            FASBaseTemplate.XPathSelectElement("./ClaimsSvcRq").Add(GenerateFASClaimData(iClaimId));
                            iClaimCount = iClaimCount + 1;
                            sUpdateSQL = "UPDATE RMA_FAS_CLAIM_EXP SET FAS_CLAIM_FLAG = -1 WHERE CLAIM_ID=" + iClaimId;

                            DbFactory.ExecuteNonQuery(m_sConnectionString, sUpdateSQL);
                        }
                        objBuilder.Append(FASBaseTemplate.ToString());
                        //asharma326 stopping blank xml pushed to FAS Servers.
                        if (!FASBaseTemplate.ToString().Equals(FASBaseAcordTemplate().ToString()))
                        {
                            if (!WriteFiletoFASFtp(sFileName, objBuilder.ToString(), out sFtpError)) // If there is any error while ftp uplaod change change uploaded flag status to 0 again
                            {
                                p_sOutPut = "You can take the ACORD xml from the userdata foler, because there was some error during the file uplaod " + sFtpError;
                            }
                            else
                            {
                                //p_sOutPut = "ACORD xml generated at " + RMConfigurator.BasePath + "//appfiles//FAS//" + sFileName + "  location. Total number of Claim(s) are " + iClaimCount + "";
                                p_sOutPut = "ACORD xml generated at " + RMConfigurator.BasePath + "//userdata//" + sFileName + "  location. Total number of Claim(s) are " + iClaimCount + "";
                                //no need we can handle with System.DateTime.Now.AddMonths(-1).ToString("yyyyMMddHHmmss")
                                //sSql = "DELETE FROM RMA_FAS_CLAIM_EXP WHERE FAS_CLAIM_FLAG = -1 AND DTTM_RCD_LAST_UPD <= REPLACE(REPLACE(REPLACE(CAST(CONVERT(VARCHAR(19), DATEADD(month, -1, GETDATE()), 120) AS VARCHAR(20)),'-',''),':',''),' ','')";
                                sSql = "DELETE FROM RMA_FAS_CLAIM_EXP WHERE FAS_CLAIM_FLAG = -1 AND DTTM_RCD_LAST_UPD <= " + System.DateTime.Now.AddMonths(-1).ToString("yyyyMMddHHmmss");
                                DbFactory.ExecuteScalar(m_sConnectionString, sSql);
                            }
                        }
                        //asharma326 34773  message incase no xml processed.
                        else
                        {
                            p_sOutPut = "No xml generated, 0 Claim(s) Processed.";
                        }
                        iArrayClaimId = null;
                    }
                }
                else
                {
                    p_sOutPut = "Please Enable FAS.";
                }
            }

            catch (Exception p_objException)
            {
                Log.Write(p_objException.Message + "\n" + p_objException.StackTrace, base.ClientId);//sharishkumar Rmacloud
                throw p_objException;
            }
            finally
            {
                if (sbSQL != null)
                {
                    sbSQL = null;
                }

                if (objLocalCache != null)
                {
                    objLocalCache = null;
                }

                if (FASBaseTemplate != null)
                {
                    FASBaseTemplate = null;
                }

                if (objBuilder != null)
                {
                    objBuilder = null;
                }
            }
        }
        #endregion /// <summary>


        public XElement GenerateFASClaimData(int p_iClaimId)
        {
            Claim objClaim = null;
            Event objEvent = null;
            Entity objEntity = null;
            Policy objPolicy = null;
            DataModelFactory objDMF = null;
            UserLogin objUserLogin = null;
            LocalCache objCache = null;
            XElement oTemplate = FASBaseSkeletonAcordTemplate();
            XElement oElement = null;
            XElement oChildTemplate = null;
            XElement oChildChildTemplate = null;
            XmlDocument obFASClaimTemplate = new XmlDocument();//Load the FAS xml Template
            string sSQL = string.Empty;
            try
            {
                objUserLogin = new UserLogin(m_sLoginName, m_sPassword, m_sDSNName, base.ClientId);
                objDMF = new DataModelFactory(objUserLogin, base.ClientId);
                objCache = new LocalCache(m_sConnectionString,base.ClientId);

                objClaim = (Claim)objDMF.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_iClaimId);

                objEvent = (Event)objDMF.GetDataModelObject("Event", false);
                objEvent.MoveTo(objClaim.EventId);

                objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                #region "Supplemental Fields"
                if (objFASreporatblelst.Count == 0)//asharma326 stop multiple calls for filling list MITS 34772 
                {
                    using (DbReader oDbReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT SUPP_TABLE_NAME,REPLACE(USER_PROMPT,' ','') as USER_PROMPT,SYS_FIELD_NAME,FIELD_TYPE,FAS_REPORTABLE FROM SUPP_DICTIONARY WHERE FAS_REPORTABLE = -1"))
                    {
                        try
                        {
                            while (oDbReader.Read())
                            {
                                objFASreporatblelst.Add(new FASReportable
                                {
                                    SuppTableName = oDbReader.GetValue("SUPP_TABLE_NAME").ToString(),
                                    SysFieldName = oDbReader.GetValue("SYS_FIELD_NAME").ToString(),
                                    SuppFieldType = oDbReader.GetValue("FIELD_TYPE").ToString(),
                                    SuppFASReportable = oDbReader.GetValue("FAS_REPORTABLE").ToString(),
                                    SuppUserPromt = (oDbReader.GetValue("USER_PROMPT").ToString()).Trim()
                                });
                            }
                        }
                        catch (Exception exe)
                        {
                            Log.Write(exe.ToString(), base.ClientId);//sharishkumar Rmacloud
                        }
                    }
                }
                #endregion

                #region Claim
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='ClaimNumber']/OtherId");
                if (oElement != null)
                {
                    oElement.Value = objClaim.ClaimNumber;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/IncidentClaimTypeCd");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetShortCode(objClaim.ClaimTypeCode);
                }


                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/IncidentClaimTypeDesc");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetCodeDesc(objClaim.ClaimTypeCode);
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/ClaimTypeCd");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetShortCode(objClaim.ClaimTypeCode);
                }
                //asharma32 MITS 34791
                //asharma32 MITS 35006  fro current Adjuster info and Event date reported.
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/CurrentAdjusterInfo/PersonNameLong/SurnameLong");
                if (oElement != null)
                {
                    oElement.Value =objClaim.CurrentAdjuster.AdjusterEntity.LastName;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/CurrentAdjusterInfo/PersonNameLong/GivenNameLong");
                if (oElement != null)
                {
                    oElement.Value = objClaim.CurrentAdjuster.AdjusterEntity.FirstName;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/CurrentAdjusterInfo/PersonNameLong/OtherGivenNameLong");
                if (oElement != null)
                {
                    oElement.Value = objClaim.CurrentAdjuster.AdjusterEntity.MiddleName;
                }
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/EventDateReported");
                if (oElement != null)
                {
                    oElement.Value = objEvent.DateReported;
                }
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/EventTimeReported");
                if (oElement != null)
                {
                    oElement.Value = objEvent.TimeReported;
                }
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/ServiceCode");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetShortCode(objClaim.ServiceCode);
                }
                //asharma326 Claim Date reporated
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/ClaimDateReported");
                if (oElement != null)
                {
                    oElement.Value = objClaim.DateRptdToRm;
                }
                //asharma326 Time of cliam
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/TimeOfClaim");
                if (oElement != null)
                {
                    oElement.Value = objClaim.TimeOfClaim;
                }

                //asharma326 Line Of business
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/LineOfBusiness");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetShortCode(objClaim.LineOfBusCode);
                }
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/ClaimStatusCd");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetShortCode(objClaim.ClaimStatusCode);
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/LossDt");
                if (oElement != null)
                {
                    oElement.Value = objEvent.DateOfEvent;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/LossDesc");
                if (oElement != null)
                {
                    oElement.Value = objClaim.LossDescription;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/DateOfClaim");
                if (oElement != null)
                {
                    oElement.Value = objClaim.DateOfClaim;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/Catastrophe/CatastropheNumber");
                if (oElement != null)
                {
                    DbReader objReader = null;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT CAT_NUMBER FROM CATASTROPHE WHERE CAT_CODE = " + objClaim.CatastropheCode);
                    if (objReader.Read())
                    {
                        oElement.Value = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }
                    objReader.Close();
                }
                //asharma326 MITS 34791 
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/Catastrophe/CatastropheTypeCd");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetShortCode( objClaim.CatastropheCode);
                }
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/Catastrophe/CatastropheTypeDesc");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetCodeDesc(objClaim.CatastropheCode);
                }
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/ClaimsReported/ReportedDt");
                if (oElement != null)
                {
                    oElement.Value = objClaim.DateRptdToRm;
                }
                //asharma326 MITS  34791 Loss Info and acord Info xml Start
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/LossInfo/AIACODE12Cd");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetShortCode(objClaim.AIACode12);
                }
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/LossInfo/AIACODE12Desc");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetCodeDesc(objClaim.AIACode12);
                }
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/LossInfo/AIACODE34Cd");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetShortCode(objClaim.AIACode34);
                }
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/LossInfo/AIACODE34Desc");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetCodeDesc(objClaim.AIACode34);
                }
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/LossInfo/AIACODE56Cd");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetShortCode(objClaim.AIACode56);
                }
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/LossInfo/AIACODE56Desc");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetCodeDesc(objClaim.AIACode56);
                }
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/LossInfo/ReportNumber");
                if (oElement != null)
                {
                    oElement.Value = objClaim.ReportNumber;
                }
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/LossInfo/DateOFDiscovery");
                if (oElement != null)
                {
                    oElement.Value = objClaim.DateOfDiscovery;
                }
                //for acord tag
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/AcordInfo");
                if (oElement != null)
                {
                    oElement.ReplaceWith(XElement.Parse("<AcordInfo>" + GetAcordInfo(objClaim) + "</AcordInfo>"));
                }
                //asharma326 MITS  34791 Loss Info and acord Info xml End
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/EventInfo/EventNumber");
                if (oElement != null)
                {
                    oElement.Value = objClaim.EventNumber;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/EventInfo/EventCd");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetShortCode(objEvent.EventTypeCode);
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/EventInfo/EventDt");
                if (oElement != null)
                {
                    oElement.Value = objEvent.DateOfEvent;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/ClosedDt");
                if (oElement != null)
                {
                    oElement.Value = objClaim.DttmClosed;
                }

                //For claim Supplemental
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/ClaimsOccurrence/com.csc_ClaimSupp");
                if (oElement != null)
                {
                    oElement.ReplaceWith(XElement.Parse("<com.csc_ClaimSupp>" + GetFASSuppValues<Claim>(objClaim, "CLAIM_SUPP") + "</com.csc_ClaimSupp>"));
                }
                //end claim supplemental
                #endregion Claim

                #region Event

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_EventNumber");
                if (oElement != null)
                {
                    oElement.Value = objEvent.EventNumber;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/EventInfo/EventCd");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetShortCode(objEvent.EventTypeCode);

                }
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/EventInfo/EventDt");
                if (oElement != null)
                {
                    oElement.Value = objEvent.DateOfEvent;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/EventInfo/com.csc_TimeofEvent");
                if (oElement != null)
                {
                    oElement.Value = objEvent.TimeOfEvent;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/EventInfo/com.csc_DateReported");
                if (oElement != null)
                {
                    oElement.Value = objEvent.DateReported;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/EventInfo/com.csc_TimeReported");
                if (oElement != null)
                {
                    oElement.Value = objEvent.TimeReported;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_InjuryFrom");
                if (oElement != null)
                {
                    oElement.Value = objEvent.InjuryFromDate;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_InjuryTo");
                if (oElement != null)
                {
                    oElement.Value = objEvent.InjuryToDate;
                }

                //asharma326 Location desc. correct mapping.
                //oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_Occurrence/com.csc_CaseVersion/com.csc_EventInfo/com.csc_LocationDesc");
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_LocationDesc");
                if (oElement != null)
                {
                    oElement.Value = objEvent.LocationAreaDesc;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_EventDesc");
                if (oElement != null)
                {
                    oElement.Value = objEvent.EventDescription;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/Addr/Addr1");
                if (oElement != null)
                {
                    oElement.Value = objEvent.Addr1;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/Addr/Addr2");
                if (oElement != null)
                {
                    oElement.Value = objEvent.Addr2;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/Addr/City");
                if (oElement != null)
                {
                    oElement.Value = objEvent.City;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/Addr/StateProvCd");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetStateCode(objEvent.StateId);
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/Addr/PostalCode");
                if (oElement != null)
                {
                    oElement.Value = objEvent.ZipCode;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/Addr/CountryCd");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetShortCode(objEvent.CountryCode);
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_CountryOfInjury");
                if (oElement != null)
                {
                    oElement.Value = objEvent.CountyOfInjury;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_CauseCode");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetShortCode(objEvent.CauseCode);

                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_AuthorityContacted");
                if (oElement != null)
                {
                    DbReader objReader = null;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT LAST_NAME FROM ENTITY WHERE ENTITY_ID = " + objClaim.PoliceAgencyEid);
                    if (objReader.Read())
                    {
                        oElement.Value = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }
                    objReader.Close();
                }
                //Check
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_ClaimantSpouseEmployee");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetShortCode(objEvent.CauseCode);

                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_ReportersNameInfo/PersonNameLong/SurnameLong");
                if (oElement != null)
                {
                    oElement.Value = objEvent.ReporterEntity.LastName;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_ReportersNameInfo/PersonNameLong/GivenNameLong");
                if (oElement != null)
                {
                    oElement.Value = objEvent.ReporterEntity.FirstName;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_ReportersNameInfo/PersonNameLong/OtherGivenNameLong");
                if (oElement != null)
                {
                    oElement.Value = objEvent.ReporterEntity.MiddleName;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_ReportersAddr/Addr1");
                if (oElement != null)
                {
                    oElement.Value = objEvent.ReporterEntity.Addr1;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_ReportersAddr/Addr2");
                if (oElement != null)
                {
                    oElement.Value = objEvent.ReporterEntity.Addr2;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_ReportersAddr/City");
                if (oElement != null)
                {
                    oElement.Value = objEvent.ReporterEntity.City;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_ReportersAddr/StateProvCd");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetStateCode(objEvent.ReporterEntity.StateId);
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_ReportersAddr/PostalCode");
                if (oElement != null)
                {
                    oElement.Value = objEvent.ReporterEntity.ZipCode;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_ReportersAddr/CountryCd");
                if (oElement != null)
                {
                    oElement.Value = objCache.GetShortCode(objEvent.ReporterEntity.CountryCode);
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_ReportersCommunications/PhoneInfo[PhoneTypeCd='Home Phone']/PhoneNumber");
                if (oElement != null)
                {
                    oElement.Value = objEvent.ReporterEntity.Phone2;
                }

                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_ReportersCommunications/PhoneInfo[PhoneTypeCd='Office']/PhoneNumber");
                if (oElement != null)
                {
                    oElement.Value = objEvent.ReporterEntity.Phone1;
                }
                //For Event Supplemental
                oElement = oTemplate.XPathSelectElement("./com.csc_CaseVersion/com.csc_EventInfo/com.csc_EventSupp");
                if (oElement != null)
                {
                    oElement.ReplaceWith(XElement.Parse("<com.csc_EventSupp>" + GetFASSuppValues<Event>(objEvent, "EVENT_SUPP") + "</com.csc_EventSupp>"));
                }
                //end Event supplemental

                #endregion Event

                #region Claimant
                foreach (Claimant objClaimant in objClaim.ClaimantList)
                {
                    try
                    {
                        oChildTemplate = FASClaimsPartyAcordTemplate();
                        objEntity.MoveTo(objClaimant.ClaimantEid);
                        oElement = oChildTemplate.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='ClaimantId']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.ClaimantRowId.ToString();
                        }


                        oElement = oChildTemplate.XPathSelectElement("./ClaimsPartyInfo/ClaimsPartyRoleCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objClaimant.ClaimantTypeCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./ClaimsPartyInfo/ClaimantTypeCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objClaimant.ClaimantTypeCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./ClaimsPartyInfo/ClaimantTypeDesc");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetCodeDesc(objClaimant.ClaimantTypeCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./ClaimsPartyInfo/SOLDate");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.SolDate;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./ClaimsPartyInfo/InjuryDescription");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.InjuryDescription;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./ClaimsPartyInfo/DamageDescription");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.DamageDescription;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/com.csc_EntityType");
                        if (oElement != null)
                        {
                            oElement.Value =objCache.GetTableName(objClaimant.ClaimantEntity.EntityTableId);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/NameInfo/PersonNameLong/SurnameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.ClaimantEntity.LastName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/NameInfo/PersonNameLong/GivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.ClaimantEntity.FirstName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/NameInfo/PersonNameLong/OtherGivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.ClaimantEntity.MiddleName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Addr/Addr1");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.ClaimantEntity.Addr1;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Addr/Addr2");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.ClaimantEntity.Addr2;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Addr/City");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.ClaimantEntity.City;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Addr/StateProvCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetStateCode(objClaimant.ClaimantEntity.StateId);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Addr/PostalCode");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.ClaimantEntity.ZipCode;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Addr/CountryCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objClaimant.ClaimantEntity.CountryCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Communications/CountryCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objClaimant.ClaimantEntity.CountryCode);
                        }


                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Home Phone']/PhoneNumber");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.ClaimantEntity.Phone2;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Office']/PhoneNumber");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.ClaimantEntity.Phone1;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Fax']/PhoneNumber");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.ClaimantEntity.FaxNumber;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Communications/EmailInfo/EmailAddr");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.ClaimantEntity.EmailAddress;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./PersonInfo/BirthDt");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.ClaimantEntity.BirthDate;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./PersonInfo/GenderCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objClaimant.ClaimantEntity.SexCode);
                        }

                        //Start Claimant Attorney
                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/com.csc_EntityType");
                        if (oElement != null)
                        {
                            oElement.Value =objCache.GetTableName(objClaimant.AttorneyEntity.EntityTableId);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/NameInfo/PersonNameLong/SurnameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.AttorneyEntity.LastName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/NameInfo/PersonNameLong/GivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.AttorneyEntity.FirstName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/NameInfo/PersonNameLong/OtherGivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.AttorneyEntity.MiddleName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/Addr/Addr1");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.AttorneyEntity.Addr1;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/Addr/Addr2");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.AttorneyEntity.Addr2;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/Addr/City");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.AttorneyEntity.City;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/Addr/StateProvCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetStateCode(objClaimant.AttorneyEntity.StateId);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/Addr/PostalCode");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.AttorneyEntity.ZipCode;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/Addr/CountryCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objClaimant.AttorneyEntity.CountryCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/Communications/CountryCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objClaimant.AttorneyEntity.CountryCode);
                        }


                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Home Phone']/PhoneNumber");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.AttorneyEntity.Phone2;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Office']/PhoneNumber");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.AttorneyEntity.Phone1;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Fax']/PhoneNumber");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.AttorneyEntity.FaxNumber;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/Communications/EmailInfo/EmailAddr");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimant.AttorneyEntity.EmailAddress;
                        }
                        //for Claimant Firm Name Starts
                        Entity objEntiytAttorney = (Entity)objDMF.GetDataModelObject("Entity", false);
                        objEntiytAttorney.MoveTo(objClaimant.AttorneyEntity.ParentEid);
                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/FirmNameInfo/PersonNameLong/SurnameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objEntiytAttorney.LastName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/FirmNameInfo/PersonNameLong/GivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objEntiytAttorney.FirstName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AttorneyGeneralPartyInfo/FirmNameInfo/PersonNameLong/OtherGivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objEntiytAttorney.MiddleName;
                        }
                        objEntiytAttorney = null;//cleanup

                        //for Claimant firm Name Ends
                        //For claimant Supplemental
                        oElement = oChildTemplate.XPathSelectElement("./com.csc_ClaimsPartySupp");
                        if (oElement != null)
                        {
                            oElement.ReplaceWith(XElement.Parse("<com.csc_ClaimsPartySupp>" + GetFASSuppValues<Claimant>(objClaimant, "CLAIMANT_SUPP") + "</com.csc_ClaimsPartySupp>"));
                            //oElement.Add(XElement.Parse("<com.csc_ClaimsPartySupp>" + GetFASSuppValues<Claimant>(objClaimant, "CLAIMANT_SUPP") + "<com.csc_ClaimsPartySupp>"));
                        }
                        //end claimant supplemental
                        oTemplate.XPathSelectElement("./com.csc_CaseVersion").Add(oChildTemplate); // Add child to the parent template

                        //End Claimant Attorney
                    } //end try
                    catch (Exception ex)
                    {
                        Log.Write(FASClaimsPartyAcordTemplate().ToString() + "\n" + ex.Message + "\n" + ex.StackTrace, base.ClientId);//sharishkumar Rmacloud
                    }
                }

                #endregion Claimant

                #region Claims Payment

                foreach (ReserveCurrent objResCurr in objClaim.ReserveCurrentList)
                {
                    try
                    {
                        oChildTemplate = FASClaimsPaymentAcordTemplate();

                        oElement = oChildTemplate.XPathSelectElement("./ReserveChangeInfo/ReservePositionTypeCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objResCurr.ReserveTypeCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./ReserveChangeInfo/ReserveAmt");
                        if (oElement != null)
                        {
                            oElement.Value = objResCurr.ReserveAmount.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./ItemIdInfo/InsurerId");
                        if (oElement != null)
                        {
                            oElement.Value = objResCurr.UnitId.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_TotalPaid ");
                        if (oElement != null)
                        {
                            oElement.Value = objResCurr.PaidTotal.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_TotalIncurred ");
                        if (oElement != null)
                        {
                            oElement.Value = objResCurr.IncurredAmount.ToString();
                        }

                        oTemplate.XPathSelectElement("./com.csc_CaseVersion").Add(oChildTemplate); // Add child to the parent template
                    } //end Try
                    catch (Exception ex)
                    {
                        Log.Write(FASClaimsPaymentAcordTemplate().ToString() + "\n" + ex.Message + "\n" + ex.StackTrace, base.ClientId);//sharishkumar Rmacloud
                    }
                }

                #endregion Claims Payment

                #region Policy
                foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
                {
                    try
                    {
                        objPolicy = (Policy)objDMF.GetDataModelObject("Policy", false);
                        objPolicy.MoveTo(objClaimXPolicy.PolicyId);

                        oChildTemplate = FASPolicyAcordTemplate();

                        oElement = oChildTemplate.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='PolicyId']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPolicy.PolicyId.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./PolicyName ");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicy.PolicyName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./PolicyNumber ");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicy.PolicyNumber;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./PolicyStatusCd ");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objPolicy.PolicyStatusCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./LOBCd ");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objPolicy.PolicyLOB);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./RenewalDate ");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicy.RenewalDate;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_PolicyIssuedDt ");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicy.IssueDate;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./CancelDeclineDt ");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicy.CancelDate;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./EffectiveDt ");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicy.EffectiveDate;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./ExpirationDt ");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicy.ExpirationDate;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_POLState ");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetStateCode(objPolicy.InsurerEntity.StateId);
                        }
                        //For Policy Supplemental
                        oElement = oTemplate.XPathSelectElement("./com.csc_PolicySupp");
                        if (oElement != null)
                        {
                            oElement.ReplaceWith(XElement.Parse("<com.csc_PolicySupp>" + GetFASSuppValues<Policy>(objPolicy, "POLICY_SUPP") + "</com.csc_PolicySupp>"));
                        }
                        //end Policy supplemental
                        oTemplate.XPathSelectElement("./com.csc_CaseVersion").Add(oChildTemplate); // Add child to the parent template
                    }//End Try
                    catch (Exception ex)
                    {
                        Log.Write(FASPolicyAcordTemplate().ToString() + "\n" + ex.Message + "\n" + ex.StackTrace, base.ClientId);//sharishkumar Rmacloud
                    }
                }
                #endregion Policy

                #region Unit Vehicle
                foreach (UnitXClaim objUnit in objClaim.UnitList)
                {
                    try
                    {
                        oChildTemplate = FASVehicleAcordTemplate();

                        oElement = oChildTemplate.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='VehicleId']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objUnit.Vehicle.UnitId.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Vin");
                        if (oElement != null)
                        {
                            oElement.Value = objUnit.Vin;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./LeasedDt");
                        if (oElement != null)
                        {
                            oElement.Value = objUnit.Vehicle.LeaseExpireDate;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Manufacturer");
                        if (oElement != null)
                        {
                            oElement.Value = objUnit.Vehicle.VehicleMake;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Model");
                        if (oElement != null)
                        {
                            oElement.Value = objUnit.Vehicle.VehicleModel;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./ModelYear");
                        if (oElement != null)
                        {
                            oElement.Value = objUnit.Vehicle.VehicleYear.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./LicensePlateNumber");
                        if (oElement != null)
                        {
                            oElement.Value = objUnit.Vehicle.LicenseNumber;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./UnitTypeCode");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objUnit.UnitTypeCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./UnitTypeCodeDesc");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetCodeDesc(objUnit.UnitTypeCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./InsuranceCoverage");
                        if (oElement != null)
                        {
                            oElement.Value = objUnit.Vehicle.InsuranceCoverage;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_VEHStateCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetStateCode(objUnit.StateRowId);
                        }
                        //oElement = oChildTemplate.XPathSelectElement("./com.csc_VEHStateDesc");
                        //if (oElement != null)
                        //{
                        //    oElement.Value = objCache.GetCodeDesc(objUnit.StateRowId);
                        //}
                        oElement = oChildTemplate.XPathSelectElement("./com.csc_VEHInsured");
                        if (oElement != null)
                        {
                            oElement.Value = Convert.ToString(objUnit.Insured);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DrivableInd");
                        if (oElement != null)
                        {
                            oElement.Value = Convert.ToString(objUnit.Drivable);
                        }
                        oElement = oChildTemplate.XPathSelectElement("./com.csc_VEHNonVehicleDamage");
                        if (oElement != null)
                        {
                            oElement.Value = Convert.ToString(objUnit.NonVehicleDamage);
                        }
                        oElement = oChildTemplate.XPathSelectElement("./com.csc_VEHEstDamage");
                        if (oElement != null)
                        {
                            oElement.Value = Convert.ToString(objUnit.EstDamage);
                        }
                        //For Unit vehicle Supplemental
                        oElement = oChildTemplate.XPathSelectElement("./com.csc_UnitSupp");
                        if (oElement != null)
                        {
                            oElement.ReplaceWith(XElement.Parse("<com.csc_UnitSupp>" + GetFASSuppValues<UnitXClaim>(objUnit, "UNIT_X_CLAIM_SUPP") + "</com.csc_UnitSupp>"));
                        }
                        //end unit Vehicle supplemental
                        oTemplate.XPathSelectElement("./com.csc_CaseVersion").Add(oChildTemplate); // Add child to the parent template
                    }//end try
                    catch (Exception ex)
                    {
                        Log.Write(FASVehicleAcordTemplate().ToString() + "\n" + ex.Message + "\n" + ex.StackTrace, base.ClientId);//sharishkumar Rmacloud
                    }
                }
                #endregion Vehicle

                #region Property

                foreach (ClaimXPropertyLoss objClaimXPropertyLoss in objClaim.PropertyLossList)
                {
                    //Change to property Unit because ClaimXPorperty doesn't contain any data 
                    //ClaimXPorperty  objClaimXPorperty = null;
                    PropertyUnit objClaimXPorperty = null;
                    try
                    {
                        oChildTemplate = FASPropertyInfoAcordTemplate();
                        //objClaimXPorperty = (ClaimXPorperty)objDMF.GetDataModelObject("ClaimXProperty", false);

                        objClaimXPorperty = (PropertyUnit)objDMF.GetDataModelObject("PropertyUnit", false);
                        objClaimXPorperty.MoveTo(objClaimXPropertyLoss.PropertyID);

                        oElement = oChildTemplate.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='PropertyId']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPropertyLoss.PropertyID.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Pin");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPorperty.Pin;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Addr/Addr1");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPorperty.Addr1;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Addr/Addr2");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPorperty.Addr2;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Addr/City");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPorperty.City;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Addr/StateProvCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetStateCode(objClaimXPorperty.StateId);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Addr/PostalCode");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPorperty.ZipCode;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Addr/CountryCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objClaimXPorperty.CountryCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./ClassOfConstruction");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objClaimXPorperty.ClassOfConstruction);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./YearOfConstruction");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPorperty.YearOfConstruction.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./SquareFootage");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPorperty.SquareFootage.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./NoOfStories");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPorperty.NoOfStories.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AvgStoryHeight");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPorperty.AvgStoryHeight.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AppraisedValue");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPorperty.AppraisedValue.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./ReplacementValue");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPorperty.ReplacementValue.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./AppraisedDate");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPorperty.AppraisedDate;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./CategoryCode");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objClaimXPorperty.CategoryCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./CategoryCodeDesc");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetCodeDesc(objClaimXPorperty.CategoryCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_VacantInd");
                        if (oElement != null)
                        {
                            if (!objClaimXPropertyLoss.Vacant)
                            {
                                oElement.Value = "F";
                            }
                            else
                            {
                                oElement.Value = "T";
                            }

                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_OtherInsuranceInd");
                        if (oElement != null)
                        {
                            if (!objClaimXPropertyLoss.OtherInsurance)
                            {
                                oElement.Value = "F";
                            }
                            else
                            {
                                oElement.Value = "T";
                            }
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DamageDesc");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPropertyLoss.Damage;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_TypeOfPropertyCode");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objClaimXPropertyLoss.PropertyType);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_EstimatedDamage ");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPropertyLoss.EstDamage.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_IncendiaryFireInd");
                        if (oElement != null)
                        {
                            if (!objClaimXPropertyLoss.IncendiaryFire)
                            {
                                oElement.Value = "F";
                            }
                            else
                            {
                                oElement.Value = "T";
                            }
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_UnderConstruction");
                        if (oElement != null)
                        {
                            if (!objClaimXPropertyLoss.UnderConstruction)
                            {
                                oElement.Value = "F";
                            }
                            else
                            {
                                oElement.Value = "T";
                            }
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_ScheduleNameCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objClaimXPorperty.AppraisalSourceCode);
                        }
                        oElement = oChildTemplate.XPathSelectElement("./com.csc_ScheduleNameDesc");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetCodeDesc(objClaimXPorperty.AppraisalSourceCode);
                        }
                        
                        //asharma326 MITS 35004
                        oElement = oChildTemplate.XPathSelectElement("./Insured");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPropertyLoss.Insured.ToString();
                        }
                        oElement = oChildTemplate.XPathSelectElement("./Description");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPorperty.Description;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_Amount");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimXPorperty.AppraisedValue.ToString();
                        }

                        //For Property Supplemental
                        oElement = oChildTemplate.XPathSelectElement("./com.csc_PropertySupp");
                        if (oElement != null)
                        {
                            oElement.ReplaceWith(XElement.Parse("<com.csc_PropertySupp>" + GetFASSuppValues<ClaimXPropertyLoss>(objClaimXPropertyLoss, "CLAIM_X_PROPERTYLOSS_SUPP") + "</com.csc_PropertySupp>"));
                        }
                        //end Property supplemental

                        oTemplate.XPathSelectElement("./com.csc_CaseVersion").Add(oChildTemplate); // Add child to the parent template
                    }//end try
                    catch (Exception ex)
                    {
                        Log.Write(FASPropertyInfoAcordTemplate().ToString() + "\n" + ex.Message + "\n" + ex.StackTrace, base.ClientId);//sharishkumar Rmacloud
                    }
                    finally
                    {
                        if (objClaimXPorperty != null)
                        {
                            objClaimXPorperty = null;
                        }
                    }
                }
                #endregion Property

                #region Liability Loss

                foreach (ClaimXLiabilityLoss objLiabilityLoss in objClaim.LiabilityLossList)
                {
                    try
                    {
                        oChildTemplate = FASLiabilityLossAcordTemplate();

                        oElement = oChildTemplate.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='LiabilityLossId']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objLiabilityLoss.RowId.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_LiabilityTypeCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objLiabilityLoss.LiabilityType);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_AffectedParty");
                        if (oElement != null)
                        {
                            oElement.Value = objLiabilityLoss.PartyAffected.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralDamages");
                        if (oElement != null)
                        {
                            oElement.Value = objLiabilityLoss.GeneralDamage;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_SpecialDamages");
                        if (oElement != null)
                        {
                            oElement.Value = objLiabilityLoss.SpecialDamage;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_PolicyNumber");
                        if (oElement != null)
                        {
                            oElement.Value = objLiabilityLoss.PolicyNumber;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Coverage/CoverageDesc"); //asharma326 Change coveragecd to CoverageDesc MITS 35008 
                        if (oElement != null)
                        {
                            oElement.Value = objLiabilityLoss.Coverages;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Coverage/Limit/FormatCurrencyAmt/Amt");
                        if (oElement != null)
                        {
                            oElement.Value = objLiabilityLoss.Limits;
                        }
                        //For Liability Loss Supplemental
                        oElement = oChildTemplate.XPathSelectElement("./com.csc_LiabilityLossSupp");
                        if (oElement != null)
                        {
                            oElement.ReplaceWith(XElement.Parse("<com.csc_LiabilityLossSupp>" + GetFASSuppValues<ClaimXLiabilityLoss>(objLiabilityLoss, "CLAIM_X_LIABILITYLOSS_SUPP") + "</com.csc_LiabilityLossSupp>"));
                        }
                        //end Liability Loss supplemental
                        oTemplate.XPathSelectElement("./com.csc_CaseVersion").Add(oChildTemplate); // Add child to the parent template
                    }//end try
                    catch (Exception ex)
                    {
                        Log.Write(FASLiabilityLossAcordTemplate().ToString() + "\n" + ex.Message + "\n" + ex.StackTrace, base.ClientId);//sharishkumar Rmacloud
                    }
                }
                #endregion Liability Loss

                #region Adjuster Party
                foreach (ClaimAdjuster objClaimAdjuster in objClaim.AdjusterList)
                {
                    try
                    {
                        oChildTemplate = FASAdjusterPartyAcordTemplate();

                        oElement = oChildTemplate.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='AdjusterId']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimAdjuster.AdjRowId.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimAdjuster.AdjRowId.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/com.csc_EntityType");
                        if (oElement != null)
                        {
                            oElement.Value =objCache.GetTableName(objClaimAdjuster.AdjusterEntity.EntityTableId);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/NameInfo/PersonNameLong/SurnameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimAdjuster.AdjusterEntity.LastName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/NameInfo/PersonNameLong/GivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimAdjuster.AdjusterEntity.FirstName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/NameInfo/PersonNameLong/OtherGivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimAdjuster.AdjusterEntity.MiddleName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Addr/Addr1");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimAdjuster.AdjusterEntity.Addr1;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Addr/Addr2");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimAdjuster.AdjusterEntity.Addr2;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Addr/City");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimAdjuster.AdjusterEntity.City;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Addr/StateProvCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetStateCode(objClaimAdjuster.AdjusterEntity.StateId);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Addr/PostalCode");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimAdjuster.AdjusterEntity.ZipCode;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Addr/CountryCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objClaimAdjuster.AdjusterEntity.CountryCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Communications/CountryCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objClaimAdjuster.AdjusterEntity.CountryCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Office']/PhoneNumber");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimAdjuster.AdjusterEntity.Phone1;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./GeneralPartyInfo/com.csc_Title");
                        if (oElement != null)
                        {
                            oElement.Value = objClaimAdjuster.AdjusterEntity.Title;
                        }
                        //For Adjuster Supplemental
                        oElement = oChildTemplate.XPathSelectElement("./com.csc_AdjusterPartySupp");
                        if (oElement != null)
                        {
                            oElement.ReplaceWith(XElement.Parse("<com.csc_AdjusterPartySupp>" + GetFASSuppValues<ClaimAdjuster>(objClaimAdjuster, "CLAIM_ADJ_SUPP") + "</com.csc_AdjusterPartySupp>"));
                        }
                        //end Adjuster supplemental
                        oTemplate.XPathSelectElement("./com.csc_CaseVersion").Add(oChildTemplate); // Add child to the parent template
                    }//end try
                    catch (Exception ex)
                    {
                        Log.Write(FASAdjusterPartyAcordTemplate().ToString() + "\n" + ex.Message + "\n" + ex.StackTrace, base.ClientId);//sharishkumar Rmacloud
                    }
                }
                #endregion Adjuster Party

                #region Litigation Info
                foreach (ClaimXLitigation objLitigation in objClaim.LitigationList)
                {
                    try
                    {
                        oChildTemplate = FASLitigationInfoAcordTemplate();

                        oElement = oChildTemplate.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='LitigationId']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objLitigation.LitigationRowId.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./DocketNum");
                        if (oElement != null)
                        {
                            oElement.Value = objLitigation.DocketNumber;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_SuitDate");
                        if (oElement != null)
                        {
                            oElement.Value = objLitigation.SuitDate;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_CourtDate");
                        if (oElement != null)
                        {
                            oElement.Value = objLitigation.CourtDate;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_VenueState");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetStateCode(objLitigation.VenueStateId);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_LitigationType");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objLitigation.LitTypeCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./LitigationStatusCd ");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objLitigation.LitStatusCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./csc.com_JudgeNameInfo/com.csc_EntityType");
                        if (oElement != null)
                        {
                            oElement.Value =objCache.GetTableName(objLitigation.JudgeEntity.EntityTableId);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./csc.com_JudgeNameInfo/PersonNameLong/SurnameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objLitigation.JudgeEntity.LastName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./csc.com_JudgeNameInfo/PersonNameLong/GivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objLitigation.JudgeEntity.FirstName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./csc.com_JudgeNameInfo/PersonNameLong/OtherGivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objLitigation.JudgeEntity.MiddleName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./csc.com_AttorneyNameInfo/com.csc_EntityType");
                        if (oElement != null)
                        {
                            oElement.Value =objCache.GetTableName(objLitigation.CoAttorneyEntity.EntityTableId);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./csc.com_AttorneyNameInfo/PersonNameLong/SurnameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objLitigation.CoAttorneyEntity.LastName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./csc.com_AttorneyNameInfo/PersonNameLong/GivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objLitigation.CoAttorneyEntity.FirstName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./csc.com_AttorneyNameInfo/PersonNameLong/OtherGivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objLitigation.CoAttorneyEntity.MiddleName;
                        }
                        //asharma326 MITS 35014 Litigation Attorney firm details
                        Entity ObjAtttorneyFirmName = (Entity)objDMF.GetDataModelObject("Entity", false);
                        ObjAtttorneyFirmName.MoveTo(objLitigation.CoAttorneyEntity.ParentEid);
                        oElement = oChildTemplate.XPathSelectElement("./com.csc_AttorneyFirm/PersonNameLong/SurnameLong");
                        if (oElement != null)
                        {
                            oElement.Value = ObjAtttorneyFirmName.LastName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_AttorneyFirm/PersonNameLong/GivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = ObjAtttorneyFirmName.FirstName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_AttorneyFirm/PersonNameLong/OtherGivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = ObjAtttorneyFirmName.MiddleName;
                        }
                        ObjAtttorneyFirmName = null;//CleanUp of Temp Entity object.
                        //oElement = oChildTemplate.XPathSelectElement("./com.csc_AttorneyFirm");
                        //if (oElement != null)
                        //{
                        //    oElement.Value = objLitigation.c.LastName;
                        //}

                        oElement = oChildTemplate.XPathSelectElement("./Addr/Addr1");
                        if (oElement != null)
                        {
                            oElement.Value = objLitigation.CoAttorneyEntity.Addr1;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Addr/Addr2");
                        if (oElement != null)
                        {
                            oElement.Value = objLitigation.CoAttorneyEntity.Addr2;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Addr/City");
                        if (oElement != null)
                        {
                            oElement.Value = objLitigation.CoAttorneyEntity.City;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Addr/StateProvCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetStateCode(objLitigation.CoAttorneyEntity.StateId);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Addr/PostalCode");
                        if (oElement != null)
                        {
                            oElement.Value = objLitigation.CoAttorneyEntity.ZipCode;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Addr/CountryCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objLitigation.CoAttorneyEntity.CountryCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./Communications/CountryCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objLitigation.CoAttorneyEntity.CountryCode);
                        }
                        //Attorney Office Number
                        oElement = oChildTemplate.XPathSelectElement("./Communications/PhoneInfo[PhoneTypeCd='Office']/PhoneNumber");
                        if (oElement != null)
                        {
                            oElement.Value = objLitigation.CoAttorneyEntity.Phone1;
                        }
                        //Attorney Home Phone Number
                        oElement = oChildTemplate.XPathSelectElement("./Communications/PhoneInfo[PhoneTypeCd='Home Phone']/PhoneNumber");
                        if (oElement != null)
                        {
                            oElement.Value = objLitigation.CoAttorneyEntity.Phone2;
                        }
                        //For Litigation Supplemental
                        oElement = oChildTemplate.XPathSelectElement("./com.csc_LitigationSupp");
                        if (oElement != null)
                        {
                            oElement.ReplaceWith(XElement.Parse("<com.csc_LitigationSupp>" + GetFASSuppValues<ClaimXLitigation>(objLitigation, "LITIGATION_SUPP") + "</com.csc_LitigationSupp>"));
                        }
                        //end Litigation supplemental
                        oTemplate.XPathSelectElement("./com.csc_CaseVersion").Add(oChildTemplate);// Add child to the parent template

                    } //end try
                    catch (Exception ex)
                    {
                        Log.Write(FASLitigationInfoAcordTemplate().ToString() + "\n" + ex.Message + "\n" + ex.StackTrace, base.ClientId);//sharishkumar Rmacloud
                    }
                }
                #endregion Litigation Info

                #region Defendant

                foreach (Defendant objDefendant in objClaim.DefendantList)
                {
                    try
                    {
                        oChildTemplate = FASDefendantInfoAcordTemplate();

                        oElement = oChildTemplate.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='DefendantId']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.DefendantRowId.ToString();
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantType");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objDefendant.DfndntTypeCode);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_FirmName");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.DefendantEntity.LastName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_FirmName");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.DefendantEntity.LastName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantNameInfo/com.csc_EntityType");
                        if (oElement != null)
                        {
                            oElement.Value =objCache.GetTableName(objDefendant.DefendantEntity.EntityTableId);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantNameInfo/PersonNameLong/SurnameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.DefendantEntity.LastName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantNameInfo/PersonNameLong/GivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.DefendantEntity.FirstName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantNameInfo/PersonNameLong/OtherGivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.DefendantEntity.MiddleName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantAddr/Addr1");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.DefendantEntity.Addr1;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantAddr/Addr2");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.DefendantEntity.Addr2;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantAddr/City");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.DefendantEntity.City;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantAddr/StateProvCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetStateCode(objDefendant.DefendantEntity.StateId);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantAddr/PostalCode");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.DefendantEntity.ZipCode;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantAddr/CountryCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objDefendant.DefendantEntity.CountryCode);
                        }


                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantCommunications/PhoneInfo[PhoneTypeCd='Office']/PhoneNumber");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.DefendantEntity.Phone1;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantCommunications/PhoneInfo[PhoneTypeCd='Home Phone']/PhoneNumber");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.DefendantEntity.Phone2;
                        }

                        //Start Defendant Attorney
                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendanAttorneytNameInfo/com.csc_EntityType");
                        if (oElement != null)
                        {
                            oElement.Value =objCache.GetTableName(objDefendant.AttorneyEntity.EntityTableId);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendanAttorneytNameInfo/PersonNameLong/SurnameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.AttorneyEntity.LastName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendanAttorneytNameInfo/PersonNameLong/GivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.AttorneyEntity.FirstName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendanAttorneytNameInfo/PersonNameLong/OtherGivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.AttorneyEntity.MiddleName;
                        }
                        //asharma326 MITS 35015 Defendant Attorney FirmName
                        Entity ObjAtttorneyFirmName = (Entity)objDMF.GetDataModelObject("Entity", false);
                        ObjAtttorneyFirmName.MoveTo(objDefendant.AttorneyEntity.ParentEid);
                        
                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendanAttorneyFirm/PersonNameLong/SurnameLong");
                        if (oElement != null)
                        {
                            oElement.Value = ObjAtttorneyFirmName.LastName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendanAttorneyFirm/PersonNameLong/GivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = ObjAtttorneyFirmName.FirstName;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendanAttorneyFirm/PersonNameLong/OtherGivenNameLong");
                        if (oElement != null)
                        {
                            oElement.Value = ObjAtttorneyFirmName.MiddleName;
                        }
                        ObjAtttorneyFirmName = null;//cleanup Of attorney object

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantAttorneyAddr/Addr1");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.AttorneyEntity.Addr1;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantAttorneyAddr/Addr2");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.AttorneyEntity.Addr2;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantAttorneyAddr/City");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.AttorneyEntity.City;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantAttorneyAddr/StateProvCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetStateCode(objDefendant.AttorneyEntity.StateId);
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantAttorneyAddr/PostalCode");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.AttorneyEntity.ZipCode;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantAttorneyAddr/CountryCd");
                        if (oElement != null)
                        {
                            oElement.Value = objCache.GetShortCode(objDefendant.AttorneyEntity.CountryCode);
                        }


                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantAttorneyCommunications/PhoneInfo[PhoneTypeCd='Office']/PhoneNumber");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.AttorneyEntity.Phone1;
                        }

                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantAttorneyCommunications/PhoneInfo[PhoneTypeCd='Home Phone']/PhoneNumber");
                        if (oElement != null)
                        {
                            oElement.Value = objDefendant.AttorneyEntity.Phone2;
                        }
                        //For defendant Supplemental
                        oElement = oChildTemplate.XPathSelectElement("./com.csc_DefendantSupp");
                        if (oElement != null)
                        {
                            oElement.ReplaceWith(XElement.Parse("<com.csc_DefendantSupp>" + GetFASSuppValues<Defendant>(objDefendant, "DEFENDANT_SUPP") + "</com.csc_DefendantSupp>"));
                        }
                        //end defendant supplemental
                        oTemplate.XPathSelectElement("./com.csc_CaseVersion").Add(oChildTemplate); // Add child to the parent template
                        //End Defendant Attorney
                    } //end try
                    catch (Exception ex)
                    {
                        Log.Write(FASDefendantInfoAcordTemplate().ToString() + "\n" + ex.Message + "\n" + ex.StackTrace, base.ClientId);//sharishkumar Rmacloud
                    }
                }
                #endregion Defendant

                #region Expert Witness


                foreach (ClaimXLitigation objLitigation in objClaim.LitigationList)
                {
                    foreach (Expert objExpert in objLitigation.ExpertList)
                    {
                        try
                        {
                            oChildTemplate = FASExpertWitnessAcordTemplate();

                            oElement = oChildTemplate.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='ExpertWitnessId']/OtherId");
                            if (oElement != null)
                            {
                                oElement.Value = objExpert.ExpertRowId.ToString();
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_WitnessTypeCode");
                            if (oElement != null)
                            {
                                oElement.Value = objCache.GetShortCode(objExpert.ExpertType);
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_WitnessTypeCode");
                            if (oElement != null)
                            {
                                oElement.Value = objCache.GetShortCode(objExpert.ExpertType);
                            }

                            oElement = oChildTemplate.XPathSelectElement("./NameInfo/com.csc_EntityType");
                            if (oElement != null)
                            {
                                oElement.Value =objCache.GetTableName(objExpert.ExpertEntity.EntityTableId);
                            }

                            oElement = oChildTemplate.XPathSelectElement("./NameInfo/PersonNameLong/SurnameLong");
                            if (oElement != null)
                            {
                                oElement.Value = objExpert.ExpertEntity.LastName;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./NameInfo/PersonNameLong/GivenNameLong");
                            if (oElement != null)
                            {
                                oElement.Value = objExpert.ExpertEntity.FirstName;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./NameInfo/PersonNameLong/OtherGivenNameLong");
                            if (oElement != null)
                            {
                                oElement.Value = objExpert.ExpertEntity.MiddleName;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./Addr/Addr1");
                            if (oElement != null)
                            {
                                oElement.Value = objExpert.ExpertEntity.Addr1;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./Addr/Addr2");
                            if (oElement != null)
                            {
                                oElement.Value = objExpert.ExpertEntity.Addr2;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./Addr/City");
                            if (oElement != null)
                            {
                                oElement.Value = objExpert.ExpertEntity.City;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./Addr/StateProvCd");
                            if (oElement != null)
                            {
                                oElement.Value = objCache.GetStateCode(objExpert.ExpertEntity.StateId);
                            }

                            oElement = oChildTemplate.XPathSelectElement("./Addr/PostalCode");
                            if (oElement != null)
                            {
                                oElement.Value = objExpert.ExpertEntity.ZipCode;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./Addr/CountryCd");
                            if (oElement != null)
                            {
                                oElement.Value = objCache.GetShortCode(objExpert.ExpertEntity.CountryCode);
                            }

                            oElement = oChildTemplate.XPathSelectElement("./Addr/CountryCd");
                            if (oElement != null)
                            {
                                oElement.Value = objCache.GetShortCode(objExpert.ExpertEntity.CountryCode);
                            }

                            oElement = oChildTemplate.XPathSelectElement("./Communications/PhoneInfo[PhoneTypeCd='Office']/PhoneNumber");
                            if (oElement != null)
                            {
                                oElement.Value = objExpert.ExpertEntity.Phone1;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./Communications/PhoneInfo[PhoneTypeCd='Home Phone']/PhoneNumber");
                            if (oElement != null)
                            {
                                oElement.Value = objExpert.ExpertEntity.Phone2;
                            }
                            //For ExpertWitness Supplemental
                            oElement = oChildTemplate.XPathSelectElement("./com.csc_ExpertSupp");
                            if (oElement != null)
                            {
                                oElement.ReplaceWith(XElement.Parse("<com.csc_ExpertSupp>" + GetFASSuppValues<Expert>(objExpert, "EXPERT_SUPP") + "</com.csc_ExpertSupp>"));
                            }
                            //end ExpertWitness supplemental
                            oTemplate.XPathSelectElement("./com.csc_CaseVersion").Add(oChildTemplate);// Add child to the parent template
                        }//end try
                        catch (Exception ex)
                        {
                            Log.Write(FASExpertWitnessAcordTemplate().ToString() + "\n" + ex.Message + "\n" + ex.StackTrace, base.ClientId);//sharishkumar Rmacloud
                        }
                    }
                    
                }

                #endregion Expert Witness

                #region Employee
                foreach (PersonInvolved objPersonInvolved in objEvent.PiList)
                {
                    try
                    {
                        if (objPersonInvolved.PiTypeCode.Equals(237))
                        {
                            oChildTemplate = FASEmployeeAcordTemplate();

                            oElement = oChildTemplate.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='EmployeeId']/OtherId");
                            if (oElement != null)
                            {
                                oElement.Value = objPersonInvolved.PiEid.ToString();
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/com.csc_EntityType");
                            if (oElement != null)
                            {
                                oElement.Value =objCache.GetTableName(objPersonInvolved.PiEntity.EntityTableId);
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/NameInfo/PersonNameLong/SurnameLong");
                            if (oElement != null)
                            {
                                oElement.Value = objPersonInvolved.PiEntity.LastName;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/NameInfo/PersonNameLong/GivenNameLong");
                            if (oElement != null)
                            {
                                oElement.Value = objPersonInvolved.PiEntity.FirstName;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/NameInfo/PersonNameLong/OtherGivenNameLong");
                            if (oElement != null)
                            {
                                oElement.Value = objPersonInvolved.PiEntity.MiddleName;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/Addr1");
                            if (oElement != null)
                            {
                                oElement.Value = objPersonInvolved.PiEntity.Addr1;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/Addr2");
                            if (oElement != null)
                            {
                                oElement.Value = objPersonInvolved.PiEntity.Addr2;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/City");
                            if (oElement != null)
                            {
                                oElement.Value = objPersonInvolved.PiEntity.City;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/StateProvCd");
                            if (oElement != null)
                            {
                                oElement.Value = objCache.GetStateCode(objPersonInvolved.PiEntity.StateId);
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/PostalCode");
                            if (oElement != null)
                            {
                                oElement.Value = objPersonInvolved.PiEntity.ZipCode;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/CountryCd");
                            if (oElement != null)
                            {
                                oElement.Value = objCache.GetShortCode(objPersonInvolved.PiEntity.CountryCode);
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Communications/CountryCd");
                            if (oElement != null)
                            {
                                oElement.Value = objCache.GetShortCode(objPersonInvolved.PiEntity.CountryCode);
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Office']/PhoneNumber");
                            if (oElement != null)
                            {
                                oElement.Value = objPersonInvolved.PiEntity.Phone1;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Home Phone']/PhoneNumber");
                            if (oElement != null)
                            {
                                oElement.Value = objPersonInvolved.PiEntity.Phone2;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Fax']/PhoneNumber");
                            if (oElement != null)
                            {
                                oElement.Value = objPersonInvolved.PiEntity.FaxNumber;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Communications/EmailInfo/EmailAddr");
                            if (oElement != null)
                            {
                                oElement.Value = objPersonInvolved.PiEntity.EmailAddress;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/PersonInfo/BirthDt");
                            if (oElement != null)
                            {
                                oElement.Value = objPersonInvolved.PiEntity.BirthDate;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/PersonInfo/GenderCd");
                            if (oElement != null)
                            {
                                oElement.Value = objCache.GetShortCode(objPersonInvolved.PiEntity.SexCode);
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/Addr1");
                            if (oElement != null)
                            {
                                oElement.Value = objPersonInvolved.PiEntity.Addr1;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_EmployeeAttorney");
                            if (oElement != null)
                            {
                                oElement.Value = objPersonInvolved.PiEntity.LegalName;
                            }

                            PiEmployee objPIEmployee = null;
                            objPIEmployee = (PiEmployee)objDMF.GetDataModelObject("PiEmployee", false);
                            objPIEmployee.MoveTo(objPersonInvolved.PiRowId);

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_HospitalSDaterange");
                            if (oElement != null)
                            {
                                oElement.Value = objPIEmployee.HrangeStartDate;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_HospitalEDaterange");
                            if (oElement != null)
                            {
                                oElement.Value = objPIEmployee.HrangeEndDate;
                            }

                            oElement = oChildTemplate.XPathSelectElement("./com.csc_EMPOthrTreatment");
                            if (oElement != null)
                            {
                                oElement.Value = objPIEmployee.OtherTreatments; ;
                            }

                            //Get data from employee table

                            sSQL = "SELECT DATE_HIRED,POSITION_CODE, NCCI_CLASS_CODE,MARITAL_STAT_CODE,TERM_DATE,JOB_CLASS_CODE FROM EMPLOYEE WHERE EMPLOYEE_EID=" + objPersonInvolved.PiEid;

                            using (DbReader oDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString()))
                            {
                                while (oDbReader.Read())
                                {
                                    try
                                    {
                                        oElement = oChildTemplate.XPathSelectElement("./HiredDt");
                                        if (oElement != null)
                                        {
                                            oElement.Value = Conversion.ConvertObjToStr(oDbReader.GetValue("DATE_HIRED").ToString());
                                        }

                                        oElement = oChildTemplate.XPathSelectElement("./NCCIIDNumber");
                                        if (oElement != null)
                                        {
                                            oElement.Value = Conversion.ConvertObjToInt(oDbReader.GetValue("NCCI_CLASS_CODE").ToString(), base.ClientId).ToString();
                                        }

                                        oElement = oChildTemplate.XPathSelectElement("./com.csc_PositionCode");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objCache.GetShortCode(Conversion.ConvertObjToInt(oDbReader.GetValue("POSITION_CODE").ToString(), base.ClientId));
                                        }

                                        oElement = oChildTemplate.XPathSelectElement("./com.csc_MaritalStatus");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objCache.GetShortCode(Conversion.ConvertObjToInt(oDbReader.GetValue("MARITAL_STAT_CODE").ToString(), base.ClientId));
                                        }

                                        oElement = oChildTemplate.XPathSelectElement("./com.csc_TerminatedDt");
                                        if (oElement != null)
                                        {
                                            oElement.Value = Conversion.ConvertObjToStr(oDbReader.GetValue("TERM_DATE").ToString());
                                        }

                                        oElement = oChildTemplate.XPathSelectElement("./com.csc_JobClassification");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objCache.GetShortCode(Conversion.ConvertObjToInt(oDbReader.GetValue("JOB_CLASS_CODE").ToString(), base.ClientId));
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Write(FASEmployeeAcordTemplate().ToString() + "\n" + ex.Message + "\n" + ex.StackTrace, base.ClientId);//sharishkumar Rmacloud
                                    }

                                }
                            }

                            //Start Employee Hospital
                            oChildChildTemplate = FASEmpHospitalAcordTemplate();
                            sSQL = string.Empty;
                            sSQL = "SELECT HOSPITAL_EID FROM PI_X_HOSPITAL WHERE PI_ROW_ID=" + objPersonInvolved.PiRowId;

                            using (DbReader oDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString()))
                            {
                                while (oDbReader.Read())
                                {
                                    try
                                    {
                                        objEntity.MoveTo(Conversion.ConvertObjToInt(oDbReader.GetValue("HOSPITAL_EID").ToString(), base.ClientId));

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/com.csc_EntityType");
                                        if (oElement != null)
                                        {
                                            oElement.Value =objCache.GetTableName(objEntity.EntityTableId);
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/NameInfo/PersonNameLong/SurnameLong");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objEntity.LastName;
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/Addr1");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objEntity.Addr1;
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/Addr2");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objEntity.Addr2;
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/City");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objEntity.City;
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/StateProvCd");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objCache.GetStateCode(objEntity.StateId);
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/PostalCode");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objEntity.ZipCode;
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/CountryCd");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objCache.GetShortCode(objEntity.CountryCode);
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Communications/CountryCd");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objCache.GetShortCode(objEntity.CountryCode);
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Office']/PhoneNumber");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objEntity.Phone1;
                                        }


                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Fax']/PhoneNumber");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objEntity.FaxNumber;
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Communications/EmailInfo/EmailAddr");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objEntity.EmailAddress;
                                        }

                                        oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo").AddAfterSelf(oChildChildTemplate);
                                    }//end try
                                    catch (Exception ex)
                                    {
                                        Log.Write(FASEmpHospitalAcordTemplate().ToString() + "\n" + ex.Message + "\n" + ex.StackTrace, base.ClientId);//sharishkumar Rmacloud
                                    }
                                }
                            }
                            //End Employee Hospital 


                            //Start Employee Physician
                            oChildChildTemplate = FASEmpPhysicianAcordTemplate();
                            sSQL = string.Empty;
                            sSQL = "SELECT PHYSICIAN_EID FROM PI_X_PHYSICIAN WHERE PI_ROW_ID=" + objPersonInvolved.PiRowId;

                            using (DbReader oDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString()))
                            {
                                while (oDbReader.Read())
                                {
                                    try
                                    {
                                        objEntity.MoveTo(Conversion.ConvertObjToInt(oDbReader.GetValue("PHYSICIAN_EID").ToString(), base.ClientId));

                                        oElement = oChildChildTemplate.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='EmpPhysicianId']/OtherId");
                                        if (oElement != null)
                                        {
                                            oElement.Value = oDbReader.GetValue("PHYSICIAN_EID").ToString();
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/com.csc_EntityType");
                                        if (oElement != null)
                                        {
                                            oElement.Value =objCache.GetTableName(objEntity.EntityTableId);
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/NameInfo/PersonNameLong/SurnameLong");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objEntity.LastName;
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/Addr1");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objEntity.Addr1;
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/Addr2");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objEntity.Addr2;
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/City");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objEntity.City;
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/StateProvCd");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objCache.GetStateCode(objEntity.StateId);
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/PostalCode");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objEntity.ZipCode;
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Addr/CountryCd");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objCache.GetShortCode(objEntity.CountryCode);
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Communications/CountryCd");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objCache.GetShortCode(objEntity.CountryCode);
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Office']/PhoneNumber");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objEntity.Phone1;
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Communications/PhoneInfo[PhoneTypeCd='Fax']/PhoneNumber");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objEntity.FaxNumber;
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo/Communications/EmailInfo/EmailAddr");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objEntity.EmailAddress;
                                        }

                                        oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo").AddAfterSelf(oChildChildTemplate);// Add child to the parent template
                                    }//end try
                                    catch (Exception ex)
                                    {
                                        Log.Write(FASEmpPhysicianAcordTemplate().ToString() + "\n" + ex.Message + "\n" + ex.StackTrace, base.ClientId);//sharishkumar Rmacloud
                                    }
                                }
                            }
                            //End Employee Physician


                            //Start Employee Injury

                            oChildChildTemplate = FASEmpInjuryAcordTemplate();
                            sSQL = string.Empty;
                            sSQL = "SELECT INJURY_CODE FROM PI_X_INJURY  WHERE PI_ROW_ID=" + objPersonInvolved.PiRowId;

                            using (DbReader oDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString()))
                            {
                                while (oDbReader.Read())
                                {
                                    try
                                    {
                                        oElement = oChildChildTemplate.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='EmpInjuryId']/OtherId");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objPersonInvolved.PiRowId.ToString();
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_InjuryCode");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objCache.GetShortCode(Conversion.ConvertObjToInt(oDbReader.GetValue("INJURY_CODE").ToString(), base.ClientId));
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_InjuryCodeDesc");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objCache.GetCodeDesc(Conversion.ConvertObjToInt(oDbReader.GetValue("INJURY_CODE").ToString(), base.ClientId));
                                        }

                                        oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo").AddAfterSelf(oChildChildTemplate);// Add child to the parent template
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Write(FASEmpInjuryAcordTemplate().ToString() + "\n" + ex.Message + "\n" + ex.StackTrace, base.ClientId);//sharishkumar Rmacloud
                                    }
                                }
                            }
                            //End Employee Injury

                            //Start Employee Treatment

                            oChildChildTemplate = FASEmpTreatmentAcordTemplate();
                            sSQL = string.Empty;
                            sSQL = "SELECT TREATMENT_CODE FROM PI_X_TREATMENT  WHERE PI_ROW_ID=" + objPersonInvolved.PiRowId;

                            using (DbReader oDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString()))
                            {
                                while (oDbReader.Read())
                                {
                                    try
                                    {
                                        oElement = oChildChildTemplate.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='EmpTreatmentId']/OtherId");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objPersonInvolved.PiRowId.ToString();
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_TreatmentCode");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objCache.GetShortCode(Conversion.ConvertObjToInt(oDbReader.GetValue("TREATMENT_CODE").ToString(), base.ClientId));
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_TreatmentCodeDesc");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objCache.GetCodeDesc(Conversion.ConvertObjToInt(oDbReader.GetValue("TREATMENT_CODE").ToString(), base.ClientId));
                                        }

                                        oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo").AddAfterSelf(oChildChildTemplate);// Add child to the parent template
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Write(FASEmpTreatmentAcordTemplate().ToString() + "\n" + ex.Message + "\n" + ex.StackTrace, base.ClientId);//sharishkumar Rmacloud
                                    }
                                }
                            }
                            //End Employee Treatment

                            //Start Employee Body Part

                            oChildChildTemplate = FASEmpBodyPartAcordTemplate();
                            sSQL = string.Empty;
                            sSQL = "SELECT BODY_PART_CODE FROM PI_X_BODY_PART  WHERE PI_ROW_ID=" + objPersonInvolved.PiRowId;

                            using (DbReader oDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString()))
                            {
                                while (oDbReader.Read())
                                {
                                    try
                                    {
                                        oElement = oChildChildTemplate.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='EmpBodyPartId']/OtherId");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objPersonInvolved.PiRowId.ToString();
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_BodyPartCode");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objCache.GetShortCode(Conversion.ConvertObjToInt(oDbReader.GetValue("BODY_PART_CODE").ToString(), base.ClientId));
                                        }

                                        oElement = oChildChildTemplate.XPathSelectElement("./com.csc_BodyPartCodeDesc");
                                        if (oElement != null)
                                        {
                                            oElement.Value = objCache.GetCodeDesc(Conversion.ConvertObjToInt(oDbReader.GetValue("BODY_PART_CODE").ToString(), base.ClientId));
                                        }

                                        oChildTemplate.XPathSelectElement("./com.csc_GeneralPartyInfo").AddAfterSelf(oChildChildTemplate);
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Write(FASEmpBodyPartAcordTemplate().ToString() + "\n" + ex.Message + "\n" + ex.StackTrace, base.ClientId);//sharishkumar Rmacloud
                                    }
                                }
                            }
                            //End Employee Body Part
                        } //end If
                        //For Employee Supplemental
                        oElement = oChildTemplate.XPathSelectElement("./com.csc_EmployeeSupp");
                        Employee objEmployee = null;
                        objEmployee = (Employee)objDMF.GetDataModelObject("Employee", false);
                        objEmployee.MoveTo(objPersonInvolved.PiEid);

                        if (oElement != null)
                        {
                            oElement.ReplaceWith(XElement.Parse("<com.csc_EmployeeSupp>" + GetFASSuppValues<Employee>(objEmployee, "EMP_SUPP") + "</com.csc_EmployeeSupp>"));
                        }
                        //end Employee supplemental
                        oTemplate.XPathSelectElement("./com.csc_CaseVersion").Add(oChildTemplate);  // Add child to the parent template
                    }//end try
                    catch (Exception ex)
                    {
                        Log.Write(FASEmployeeAcordTemplate().ToString() + "\n" + ex.Message + "\n" + ex.StackTrace, base.ClientId);//sharishkumar Rmacloud
                    }
                } // End Main For Each loop
                #endregion Employee

                return oTemplate;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objClaim != null)
                {
                    objClaim = null;
                }

                if (objEvent != null)
                {
                    objEvent = null;
                }

                if (objEntity != null)
                {
                    objEntity = null;
                }

                if (oElement != null)
                {
                    oElement = null;
                }

                if (objDMF != null)
                {
                    objDMF = null;
                }

                if (objPolicy != null)
                {
                    objPolicy = null;
                }

                if (objUserLogin != null)
                {
                    objUserLogin = null;
                }

                if (objCache != null)
                {
                    objCache = null;
                }
                if (oChildTemplate != null)
                {
                    oChildTemplate = null;
                }

                if (oChildChildTemplate != null)
                {
                    oChildChildTemplate = null;
                }
            }
        }
        //asharma326 MITS 34791
        private string GetAcordInfo(Claim objClaim)
        {
            StringBuilder strReturn = new StringBuilder();
            Dictionary<string, string> dictionaryAcord = new Dictionary<string, string>() 
            {
                {"REMARKS","Remarks"},
                {"AMT_OF_LOSS", "AmountofTotalLoss"},
                {"INJ_EMP_NAME_ADDR", "InjuredEmployeeNameAddr"},
                {"INJ_EMP_PHONE", "InjuredEmployeePhone"},
                {"DESC_PROP_DAMAGED", "DescribePropertyDamage"},
                {"DAMAGED_COMP_NAME", "DamagedPropertyCompanyName"},
                {"DAMAGED_POL_NUM", "DamagedPropertyPolicyNum"},
                {"DAMAGED_NAME_ADDR", "DamagedOwnerNameAddr"},
                {"DAMAGED_PHONE2", "DamagedOwnerResPhone"},
                {"DAMAGED_PHONE1", "DamagedOwnerBusPhone"},
                {"DESC_DAMAGE", "DescribeDamage"},
                {"DAMAGE_EST_AMT", "DamageEstimateAmount"},
                {"WHERE_DAMAGE_SEEN", "WhereCanDamageBeSeen"},
                {"RPT_NUM", "ReportNumber"},
                {"PROP_DAMAGED_VEH", "PropertyDamagedVehicle"},
            };
            try
            {
                var AcorList = objClaim.Acord.Cast<SupplementalField>().ToList();
                var requiredAcordLst = (from l1 in AcorList
                                        join l2 in dictionaryAcord on l1.FieldName.ToUpper() equals l2.Key.ToUpper()
                                        select new { AcordTagName = l2.Value, AccordTagValue = l1.Value }).ToList();
                foreach (var item in requiredAcordLst)
                {
                    strReturn.Append("<" + item.AcordTagName + ">" + item.AccordTagValue + "</" + item.AcordTagName + ">");
                }
            }
            catch (Exception exe)
            {
                Log.Write(exe.Message.ToString(), base.ClientId);//sharishkumar Rmacloud
            }
            return strReturn.ToString();
        }

        private string GetFASSuppValues<T>(T objType, string SuppTableName)
        {
            StringBuilder strReturn = new StringBuilder();
            try
            {
                UserLogin objUserLogin = new UserLogin(m_sLoginName, m_sPassword, m_sDSNName, base.ClientId);
                LocalCache objCache = new LocalCache(m_sConnectionString,base.ClientId);
                DataModelFactory objDMF = new DataModelFactory(objUserLogin, base.ClientId);
                Entity objEntity = null;
                objType = (T)Convert.ChangeType(objType, typeof(T));

                //collections of all supplementals
                var lst2 = ((IEnumerable)objType.GetType().GetProperty("Supplementals").GetValue(objType)).Cast<SupplementalField>().ToList();

                //collection of FAS reportables
                var lst1 = from a in objFASreporatblelst
                           where a.SuppTableName.ToUpper() == SuppTableName.ToUpper()
                           select a;

                //FAS reporatbels get values from all collections 
                var lst3 = (from l2 in lst2
                            join l1 in lst1 on l2.FieldName.ToUpper() equals l1.SysFieldName.ToUpper()
                            select new FASReportable()
                            {
                                SuppFASReportable = l1.SuppFASReportable,
                                SuppFieldType = l1.SuppFieldType,
                                SuppTableName = l1.SuppTableName,
                                SuppUserPromt = l1.SuppUserPromt,
                                SuppMultiValues = Convert.ToString(l2.Values),
                                SuppValue = Convert.ToString(l2.Value),
                                SysFieldName = l1.SysFieldName
                            }).ToList();

                foreach (var item in lst3)
                {
                    switch (item.SuppFieldType)
                    {
                        case "0":
                            //string data
                            //direct
                            strReturn.Append("<" + item.SysFieldName + ">" + item.SuppValue + "</" + item.SysFieldName + ">");
                            //strReturn.Append(Convert.ToString(lstSuppObj.Find(a => string.Equals(a.FieldName, item.SysFieldName)).Value).ToString());
                            //strReturn.Append("</" + item.SuppUserPromt + ">");
                            break;
                        case "1":
                            //Number data
                            //direct
                            strReturn.Append("<" + item.SysFieldName + ">" + item.SuppValue + "</" + item.SysFieldName + ">");
                            //strReturn.Append(Convert.ToString(lstSuppObj.Find(a => string.Equals(a.FieldName, item.SysFieldName)).Value).ToString());
                            //strReturn.Append("</" + item.SysFieldName + ">");
                            break;
                        case "2":
                            //Currency
                            //direct add dollar in front
                            strReturn.Append("<" + item.SysFieldName + ">" + item.SuppValue + "</" + item.SysFieldName + ">");
                            //strReturn.Append(Convert.ToString(lstSuppObj.Find(a => string.Equals(a.FieldName, item.SysFieldName)).Value).ToString());
                            //strReturn.Append("</" + item.SuppUserPromt + ">");
                            break;
                        case "3":
                            //date 
                            //conversion required
                            if (!string.IsNullOrEmpty(item.SuppValue))
                            {
                                strReturn.Append("<" + item.SysFieldName + ">" + Conversion.ToDate(item.SuppValue) + "</" + item.SysFieldName + ">");
                                //strReturn.Append(Conversion.GetTime(Convert.ToString(lstSuppObj.Find(a => string.Equals(a.FieldName, item.SysFieldName)).Value).ToString()));
                                //strReturn.Append("</" + item.SuppUserPromt + ">");
                            }
                            else { strReturn.Append("<" + item.SysFieldName + ">" + " " + "</" + item.SysFieldName + ">"); }

                            break;
                        case "4":
                            //time conversion required
                            if (!string.IsNullOrEmpty(item.SuppValue))
                            {
                                strReturn.Append("<" + item.SysFieldName + ">" + Conversion.ToDate(item.SuppValue).ToShortTimeString() + "</" + item.SysFieldName + ">");
                                //strReturn.Append(Conversion.GetTime(Convert.ToString(lstSuppObj.Find(a => string.Equals(a.FieldName, item.SysFieldName)).Value).ToString()));
                                //strReturn.Append("</" + item.SuppUserPromt + ">");
                            }
                            else { strReturn.Append("<" + item.SysFieldName + ">" + " " + "</" + item.SysFieldName + ">"); }

                            break;
                        case "5":
                            //Direct
                            strReturn.Append("<" + item.SysFieldName + ">" + item.SuppValue + "</" + item.SysFieldName + ">");
                            //strReturn.Append(Convert.ToString(lstSuppObj.Find(a => string.Equals(a.FieldName, item.SysFieldName)).Value).ToString());
                            //strReturn.Append("</" + item.SuppUserPromt + ">");
                            break;

                        case "6":
                            //resolve from codestext
                            if (!string.IsNullOrEmpty(item.SuppValue))
                            {
                                strReturn.Append("<" + item.SysFieldName + ">" + objCache.GetShortCode(Convert.ToInt32(item.SuppValue)) + " " + objCache.GetCodeDesc(Convert.ToInt32(item.SuppValue)) + "</" + item.SysFieldName + ">");
                                //string strTemp = Convert.ToString(lstSuppObj.Find(a => string.Equals(a.FieldName, item.SysFieldName)).Value).ToString();
                                //strReturn.Append("<" + item.SuppUserPromt + ">");
                                //strReturn.Append(objCache.GetShortCode(Convert.ToInt32(strTemp)) + " " + objCache.GetCodeDesc(Convert.ToInt32(strTemp)));
                                //strReturn.Append("</" + item.SuppUserPromt + ">");
                            }
                            else { strReturn.Append("<" + item.SysFieldName + ">" + " " + "</" + item.SysFieldName + ">"); }

                            break;

                        case "7":
                            break;
                        case "8":
                            //resolve from entity
                            if (!string.IsNullOrEmpty(item.SuppValue))
                            {
                                objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                                //objEntity.MoveTo(Convert.ToInt32(Convert.ToString(lstSuppObj.Find(a => string.Equals(a.FieldName, item.SysFieldName)).Value).ToString()));
                                objEntity.MoveTo(Convert.ToInt32(item.SuppValue));
                                strReturn.Append("<" + item.SysFieldName + ">" + objEntity.LastName + ", " + objEntity.FirstName + "</" + item.SysFieldName + ">");
                                //strReturn.Append("<" + item.SuppUserPromt + ">");
                                //strReturn.Append(objEntity.LastName + ", " + objEntity.FirstName);
                                //strReturn.Append("</" + item.SuppUserPromt + ">");
                            }
                            else { strReturn.Append("<" + item.SysFieldName + ">" + " " + "</" + item.SysFieldName + ">"); }

                            break;
                        case "9":
                            //resolve from states
                            if (!string.IsNullOrEmpty(item.SuppValue))
                            {
                                string strStateCode = "";
                                string strStateDesc = "";
                                objCache.GetStateInfo(Convert.ToInt32(item.SuppValue), ref strStateCode, ref strStateDesc);
                                strReturn.Append("<" + item.SysFieldName + ">" + strStateCode + " " + strStateDesc + "</" + item.SysFieldName + ">");
                                //strReturn.Append("<" + item.SuppUserPromt + ">");
                                //strReturn.Append(objCache.GetStateCode(Convert.ToInt32(Convert.ToString(lstSuppObj.Find(a => string.Equals(a.FieldName, item.SysFieldName)).Value).ToString())));
                                //strReturn.Append("</" + item.SuppUserPromt + ">");
                            }
                            else { strReturn.Append("<" + item.SysFieldName + ">" + " " + "</" + item.SysFieldName + ">"); }

                            break;
                        case "10":
                            //Direct 
                            strReturn.Append("<" + item.SysFieldName + ">" + item.SuppValue + "</" + item.SysFieldName + ">");
                            //strReturn.Append("<" + item.SuppUserPromt + ">");
                            //strReturn.Append(Convert.ToString(lstSuppObj.Find(a => string.Equals(a.FieldName, item.SysFieldName)).Value).ToString());
                            //strReturn.Append("</" + item.SuppUserPromt + ">");
                            break;
                        case "11":
                            //Direct 
                            strReturn.Append("<" + item.SysFieldName + ">" + item.SuppValue + "</" + item.SysFieldName + ">");
                            //strReturn.Append("<" + item.SuppUserPromt + ">");
                            //strReturn.Append(Convert.ToString(lstSuppObj.Find(a => string.Equals(a.FieldName, item.SysFieldName)).Value).ToString());
                            //strReturn.Append("</" + item.SuppUserPromt + ">");
                            break;
                        case "12":
                            //Direct
                            strReturn.Append("<" + item.SysFieldName + ">" + item.SuppValue + "</" + item.SysFieldName + ">");
                            //strReturn.Append("<" + item.SuppUserPromt + ">");
                            //strReturn.Append(Convert.ToString(lstSuppObj.Find(a => string.Equals(a.FieldName, item.SysFieldName)).Value).ToString());
                            //strReturn.Append("</" + item.SuppUserPromt + ">");
                            break;
                        case "13":
                            //Direct
                            strReturn.Append("<" + item.SysFieldName + ">" + item.SuppValue + "</" + item.SysFieldName + ">");
                            //strReturn.Append("<" + item.SuppUserPromt + ">");
                            //strReturn.Append(Convert.ToString(lstSuppObj.Find(a => string.Equals(a.FieldName, item.SysFieldName)).Value).ToString());
                            //strReturn.Append("</" + item.SuppUserPromt + ">");
                            break;
                        case "14":
                            //Multi Code Supp
                            //strReturn.Append("<" + item.SuppUserPromt + ">");
                            if (!string.IsNullOrEmpty(item.SuppMultiValues))
                            {
                                string strtemp = "";
                                string[] strarary = item.SuppMultiValues.Split(' ');
                                foreach (string id in strarary)
                                {
                                    strtemp += objCache.GetShortCode(Convert.ToInt32(id)) + " " + objCache.GetCodeDesc(Convert.ToInt32(id)) + ",";
                                }
                                strReturn.Append("<" + item.SysFieldName + ">" + strtemp.Remove(strtemp.Length - 1) + "</" + item.SysFieldName + ">");
                            }
                            else { strReturn.Append("<" + item.SysFieldName + ">" + " " + "</" + item.SysFieldName + ">"); }
                            break;
                        case "15":
                            //Multi states
                            if (!string.IsNullOrEmpty(item.SuppMultiValues))
                            {
                                string strStateCode1 = "";
                                string strStateName = "";
                                string strtemp3 = "";
                                string[] strarary3 = item.SuppMultiValues.Split(' ');
                                foreach (string id in strarary3)
                                {
                                    objCache.GetStateInfo(Convert.ToInt32(id), ref strStateCode1, ref strStateName);
                                    strtemp3 += strStateCode1 + " " + strStateName + ",";
                                }
                                strReturn.Append("<" + item.SysFieldName + ">" + strtemp3.Remove(strtemp3.Length - 1) + "</" + item.SysFieldName + ">");
                            }
                            else { strReturn.Append("<" + item.SysFieldName + ">" + " " + "</" + item.SysFieldName + ">"); }

                            break;
                        case "16":
                            //Multi Entity Supp
                            if (!string.IsNullOrEmpty(item.SuppMultiValues))
                            {
                                string strtemp2 = "";
                                string[] strarary2 = item.SuppMultiValues.Split(' ');
                                foreach (string id in strarary2)
                                {
                                    objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                                    //objEntity.MoveTo(Convert.ToInt32(Convert.ToString(lstSuppObj.Find(a => string.Equals(a.FieldName, item.SysFieldName)).Value).ToString()));
                                    objEntity.MoveTo(Convert.ToInt32(id));
                                    strtemp2 += objEntity.LastName + " " + objEntity.FirstName + ",";
                                }
                                strReturn.Append("<" + item.SysFieldName + ">" + strtemp2.Remove(strtemp2.Length - 1) + "</" + item.SysFieldName + ">");
                            }
                            else { strReturn.Append("<" + item.SysFieldName + ">" + " " + "</" + item.SysFieldName + ">"); }

                            break;
                        case "17":
                            //Grid Values Direct
                            if (!item.SuppValue.Equals("KNALB"))
                            {
                                strReturn.Append("<" + item.SysFieldName + ">" + item.SuppValue + "</" + item.SysFieldName + ">");
                            }
                            else
                                strReturn.Append("<" + item.SysFieldName + ">" + " " + "</" + item.SysFieldName + ">");
                            break;
                        case "31":
                            //Checkbox Conversion required.
                            if (!string.IsNullOrEmpty(item.SuppValue))
                            {
                                strReturn.Append("<" + item.SysFieldName + ">" + Conversion.ConvertStrToBool(item.SuppValue) + "</" + item.SysFieldName + ">");
                                //strReturn.Append("<" + item.SuppUserPromt + ">");
                                //strReturn.Append(Convert.ToString(lstSuppObj.Find(a => string.Equals(a.FieldName, item.SysFieldName)).Value).ToString());
                                //strReturn.Append("</" + item.SuppUserPromt + ">");
                            }
                            else { strReturn.Append("<" + item.SysFieldName + ">" + " " + "</" + item.SysFieldName + ">"); }

                            break;

                    }

                }
            }
            catch (Exception exe)
            {
                Log.Write(exe.Message.ToString(), base.ClientId);//sharishkumar Rmacloud
            }
            return strReturn.ToString();
        }

        //private bool WriteFormattedXmlToFile(string p_sFileName, string sData)
        //{
        //    StreamWriter objWriter = null;
        //    string sFilePath = string.Empty;
        //    try
        //    {
        //        sFilePath = RMConfigurator.BasePath + "//appfiles//FAS//" + p_sFileName;
        //        if (File.Exists(sFilePath))
        //        {
        //            File.Delete(sFilePath);
        //        }

        //        objWriter = new StreamWriter(sFilePath);
        //        objWriter.WriteLine(sData);
        //      return  true;
        //    }
        //    catch (Exception e)
        //    {
        //        return false;
        //    }
        //    finally
        //    {
        //        if (objWriter != null)
        //        {
        //            objWriter.Close();
        //        }
        //    }
        //}

        private bool WriteFiletoFASFtp(string p_sFileName,string sData,out string sFtpError)
        {
            SysSettings objSysSettings=new SysSettings(m_sConnectionString, base.ClientId); //Ash - cloud
            StreamWriter objWriter = null;
            string sFilePath = RMConfigurator.BasePath + "//userdata//" + p_sFileName;

            try
            {
                objWriter = new StreamWriter(sFilePath);
                objWriter.WriteLine(sData);
                objWriter.Close();
                GC.SuppressFinalize(this);

                if (objSysSettings.FASFileLocation.ToString()=="F")
                {
                    // Get the object used to communicate with the server.
                    //asharma326 MITS 34849  
                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://"+objSysSettings.FASServer + "/" + objSysSettings.FASFolder + "/" + p_sFileName);
                    request.Method = WebRequestMethods.Ftp.UploadFile;

                    // This example assumes the FTP site uses anonymous logon.
                    request.Credentials = new NetworkCredential(objSysSettings.FASUserId, objSysSettings.FASPassword);

                    // Copy the contents of the file to the request stream.
                    StreamReader sourceStream = new StreamReader(sFilePath);
                    byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                    sourceStream.Close();
                    request.ContentLength = fileContents.Length;

                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(fileContents, 0, fileContents.Length);
                    requestStream.Close();

                    FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                    Console.WriteLine("Upload File Complete, status {0}", response.StatusDescription);

                    response.Close();
                    Log.Write("Upload file to FTP Succeded at location \n Server : " + objSysSettings.FASServer + " FASFolder: " + objSysSettings.FASFolder, base.ClientId);//sharishkumar Rmacloud
                }
                //Asharma326 MITS 32386 Starts
                else if (objSysSettings.FASFileLocation.ToString() == "S")
                {
                    File.Copy(sFilePath, objSysSettings.FASSharedLocation + "\\" + p_sFileName);
                    Log.Write("Upload file to Shared location Succeded at \n path : " + objSysSettings.FASSharedLocation + "\\" + p_sFileName, base.ClientId);//sharishkumar Rmacloud
                }
                //Asharma326 MITS 32386 Ends
            }
            catch (Exception ex)
            {
                sFtpError ="upload error " + ex.Message;
                Log.Write(ex.Message.ToString(),base.ClientId);//sharishkumar Rmacloud
                return false;
            }
            finally
            {
                if (objSysSettings != null)
                {
                    objSysSettings = null;
                }
                if (objWriter != null)
                {
                    objWriter = null;
                }
            }
            sFtpError = "";
            return true;
        }

        # region AcordTemplates
        private XElement FASBaseAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <ACORD>
	                <SignonRq>
		                <SignonPswd>
			                <CustId>
				                <SPName/>
				                <CustLoginId></CustLoginId>
			                </CustId>
			                <CustPswd>
				                <EncryptionTypeCd/>
				                <Pswd/>
			                </CustPswd>
		                </SignonPswd>
		                <ClientDt/>
		                <CustLangPref/>
		                <ClientApp>
			                <Org/>
			                <Name></Name>
			                <Version/>
                            <com.csc_ClientFile/>
		                </ClientApp>
		                <com.csc_SessKey></com.csc_SessKey>
	                </SignonRq>
                     <ClaimsSvcRq>
                        <RqUID />
                        <TransactionRequestDt />
                      </ClaimsSvcRq>
                </ACORD>");

            return oTemplate;
        }

        private XElement FASBaseSkeletonAcordTemplate()
        {
            //asharma326 added <com.csc_EventSupp> </com.csc_EventSupp> for Event Suppliemntal and <com.csc_ClaimSupp> </com.csc_ClaimSupp> for Claim Supp
            XElement oTemplate = XElement.Parse(@"
               
                        <com.csc_Occurrence>
                          <com.csc_CaseVersion>
                            <com.csc_EventInfo>
                              <com.csc_EventNumber></com.csc_EventNumber>
                              <EventInfo>
                                <EventCd></EventCd>
                                <EventDt />
                                <com.csc_TimeofEvent></com.csc_TimeofEvent>
                                <com.csc_DateReported></com.csc_DateReported>
                                <com.csc_TimeReported></com.csc_TimeReported>
                              </EventInfo>
                              <com.csc_InjuryFrom></com.csc_InjuryFrom>
                              <com.csc_InjuryTo></com.csc_InjuryTo>
                              <com.csc_LocationDesc></com.csc_LocationDesc>
                              <com.csc_EventDesc></com.csc_EventDesc>
                              <Addr>
                                <Addr1 />
                                <Addr2 />
                                <City />
                                <StateProvCd />
                                <PostalCode />
                                <CountryCd />
                              </Addr>
                              <com.csc_CountryOfInjury></com.csc_CountryOfInjury>
                              <com.csc_CauseCode></com.csc_CauseCode>
                              <com.csc_AuthorityContacted></com.csc_AuthorityContacted>
                              <com.csc_ClaimantSpouseEmployee></com.csc_ClaimantSpouseEmployee>
                              <com.csc_ReportersNameInfo>
                                <com.csc_EntityType>REPORTER</com.csc_EntityType>
                                <PersonNameLong>
                                  <SurnameLong />
                                  <GivenNameLong />
                                  <OtherGivenNameLong />
                                </PersonNameLong>
                              </com.csc_ReportersNameInfo>
                              <com.csc_ReportersAddr>
                                <Addr1 />
                                <Addr2 />
                                <City />
                                <StateProvCd />
                                <PostalCode />
                                <CountryCd />
                              </com.csc_ReportersAddr>
                              <com.csc_ReportersCommunications>
                                <PhoneInfo>
                                  <PhoneTypeCd>Office</PhoneTypeCd>
                                  <PhoneNumber />
                                </PhoneInfo>
                                <PhoneInfo>
                                  <PhoneTypeCd>Home Phone</PhoneTypeCd>
                                  <PhoneNumber />
                                </PhoneInfo>
                              </com.csc_ReportersCommunications>
                                <com.csc_EventSupp> </com.csc_EventSupp> 
                            </com.csc_EventInfo>

                            <ClaimsOccurrence>
                              <ItemIdInfo>
                                <OtherIdentifier>
                                  <OtherIdTypeCd>ClaimNumber</OtherIdTypeCd>
                                  <OtherId />
                                </OtherIdentifier>
                              </ItemIdInfo>
                              <IncidentClaimTypeCd />
                              <IncidentClaimTypeDesc />
                              <ClaimTypeCd />
                              <LineOfBusiness/>
                              <ClaimStatusCd />
                              <LossDt />
                              <LossDesc />
                              <DateOfClaim />
                              <ClaimDateReported/>
                              <EventDateReported/>
                              <EventTimeReported/>
                              <CurrentAdjusterInfo>
                                 <PersonNameLong>
                                    <SurnameLong />
                                    <GivenNameLong />
                                    <OtherGivenNameLong />
                                  </PersonNameLong>
                              </CurrentAdjusterInfo>
                              <ServiceCode/>  
                              <TimeOfClaim/>
                              <Catastrophe>
                                <CatastropheTypeCd/>
                                <CatastropheTypeDesc/>
                                <CatastropheNumber />
                              </Catastrophe>
                              <ClaimsReported>
                                <ReportedDt />
                              </ClaimsReported>
                                 <LossInfo>
                                    <AIACODE12Cd/>
                                    <AIACODE12Desc/>
                                    <AIACODE34Cd/>
                                    <AIACODE34Desc/>
                                    <AIACODE56Cd/>
                                    <AIACODE56Desc/>
                                    <ReportNumber/>
                                    <DateOFDiscovery/>
                                  </LossInfo>
                                  <AcordInfo>
                                  </AcordInfo>
                              <EventInfo>
                                <EventNumber />
                                <EventCd></EventCd>
                                <EventDt />
                              </EventInfo>
                              <ClosedDt />
                              <com.csc_ClaimSupp> </com.csc_ClaimSupp>
                            </ClaimsOccurrence>
                          </com.csc_CaseVersion>
                        </com.csc_Occurrence>");

            return oTemplate;
        }

        private XElement FASClaimsPartyAcordTemplate()
        {
            //asharma326 <com.csc_ClaimsPartySupp></com.csc_ClaimsPartySupp> for Claimant supp
            XElement oTemplate = XElement.Parse(@"
                 <ClaimsParty>
                      <ItemIdInfo>
                        <OtherIdentifier>
                          <OtherIdTypeCd>ClaimantId</OtherIdTypeCd>
                          <OtherId />
                        </OtherIdentifier>
                      </ItemIdInfo>
                      <ClaimsPartyInfo>
                        <ClaimsPartyRoleCd></ClaimsPartyRoleCd>
                        <ClaimantTypeCd />
                        <ClaimantTypeDesc />
                        <SOLDate />
                        <InjuryDescription />
                        <DamageDescription />
                      </ClaimsPartyInfo>
                      <GeneralPartyInfo>
                        <com.csc_EntityType/>
                        <NameInfo>
                          <PersonNameLong>
                            <SurnameLong />
                            <GivenNameLong />
                            <OtherGivenNameLong />
                          </PersonNameLong>
                        </NameInfo>
                        <Addr>
                          <Addr1 />
                          <Addr2 />
                          <City />
                          <StateProvCd />
                          <PostalCode />
                          <CountryCd />
                        </Addr>
                        <Communications>
                          <PhoneInfo>
                            <PhoneTypeCd>Office</PhoneTypeCd>
                            <PhoneNumber />
                          </PhoneInfo>
                          <PhoneInfo>
                            <PhoneTypeCd>Home Phone</PhoneTypeCd>
                            <PhoneNumber />
                          </PhoneInfo>
                          <PhoneInfo>
                            <PhoneTypeCd>Fax</PhoneTypeCd>
                            <PhoneNumber />
                          </PhoneInfo>
                          <EmailInfo>
                            <EmailAddr />
                          </EmailInfo>
                        </Communications>
                      </GeneralPartyInfo>
                      <PersonInfo>
                        <BirthDt />
                        <GenderCd />
                      </PersonInfo>
                    <AttorneyGeneralPartyInfo>
                        <com.csc_EntityType/>
                        <NameInfo>
                          <PersonNameLong>
                            <SurnameLong />
                            <GivenNameLong />
                            <OtherGivenNameLong />
                          </PersonNameLong>
                        </NameInfo>
                        <Addr>
                          <Addr1 />
                          <Addr2 />
                          <City />
                          <StateProvCd />
                          <PostalCode />
                          <CountryCd />
                        </Addr>
                        <Communications>
                          <PhoneInfo>
                            <PhoneTypeCd>Office</PhoneTypeCd>
                            <PhoneNumber />
                          </PhoneInfo>
                          <PhoneInfo>
                            <PhoneTypeCd>Home Phone</PhoneTypeCd>
                            <PhoneNumber />
                          </PhoneInfo>
                          <PhoneInfo>
                            <PhoneTypeCd>Fax</PhoneTypeCd>
                            <PhoneNumber />
                          </PhoneInfo>
                          <EmailInfo>
                            <EmailAddr />
                          </EmailInfo>
                        </Communications>
                        <FirmNameInfo>
                          <PersonNameLong>
                            <SurnameLong />
                            <GivenNameLong />
                            <OtherGivenNameLong />
                          </PersonNameLong>
                        </FirmNameInfo>
                      </AttorneyGeneralPartyInfo>
                <com.csc_ClaimsPartySupp></com.csc_ClaimsPartySupp>
        </ClaimsParty>  ");

            return oTemplate;
        }

        private XElement FASPolicyAcordTemplate()
        {
            //asharma326 <com.csc_PolicySupp></com.csc_PolicySupp> for policy Supplimentals
            XElement oTemplate = XElement.Parse(@"
                <Policy>
                <ItemIdInfo>
                        <OtherIdentifier>
                          <OtherIdTypeCd>PolicyId</OtherIdTypeCd>
                          <OtherId />
                        </OtherIdentifier>
                      </ItemIdInfo>
                  <PolicyName></PolicyName>
                  <PolicyNumber></PolicyNumber>
                  <PolicyStatusCd />
                  <LOBCd></LOBCd>
                  <RenewalDate></RenewalDate>
                  <com.csc_PolicyIssuedDt></com.csc_PolicyIssuedDt>
                  <CancelDeclineDt></CancelDeclineDt>
                  <EffectiveDt></EffectiveDt>
                  <ExpirationDt></ExpirationDt>
                  <com.csc_POLState></com.csc_POLState>
                  <com.csc_PolicySupp></com.csc_PolicySupp>
                </Policy>  ");

            return oTemplate;
        }

        private XElement FASClaimsPaymentAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <ClaimsPayment>
                  <ItemIdInfo>
                    <InsurerId></InsurerId>
                  </ItemIdInfo>
                  <ReserveChangeInfo>
                    <ReservePositionTypeCd></ReservePositionTypeCd>
                    <ReserveAmt></ReserveAmt>
                  </ReserveChangeInfo>
                  <com.csc_TotalPaid></com.csc_TotalPaid>
                  <com.csc_TotalIncurred></com.csc_TotalIncurred>
               </ClaimsPayment>  ");

            return oTemplate;
        }

        private XElement FASEmpInjuryAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <com.csc_EmpInjury>
                    <ItemIdInfo>
                        <OtherIdentifier>
                          <OtherIdTypeCd>EmpInjuryId</OtherIdTypeCd>
                          <OtherId />
                        </OtherIdentifier>
                      </ItemIdInfo>
                  <com.csc_InjuryCode />
                  <com.csc_InjuryCodeDesc />
               </com.csc_EmpInjury>");

            return oTemplate;
        }

        private XElement FASEmpTreatmentAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <com.csc_EmpTreatment>
                <ItemIdInfo>
                        <OtherIdentifier>
                          <OtherIdTypeCd>EmpTreatmentId</OtherIdTypeCd>
                          <OtherId />
                        </OtherIdentifier>
                      </ItemIdInfo>
                  <com.csc_TreatmentCode />
                  <com.csc_TreatmentCodeDesc />
               </com.csc_EmpTreatment>");

            return oTemplate;
        }

        private XElement FASEmpBodyPartAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <com.csc_EmpBodyPart>
                    <ItemIdInfo>
                        <OtherIdentifier>
                          <OtherIdTypeCd>EmpBodyPartId</OtherIdTypeCd>
                          <OtherId />
                        </OtherIdentifier>
                      </ItemIdInfo>
                  <com.csc_BodyPartCode />
                  <com.csc_BodyPartCodeDesc />
               </com.csc_EmpBodyPart>");

            return oTemplate;
        }

        private XElement FASEmpPhysicianAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <com.csc_EmpPhysician>
                <ItemIdInfo>
                        <OtherIdentifier>
                          <OtherIdTypeCd>EmpPhysicianId</OtherIdTypeCd>
                          <OtherId />
                        </OtherIdentifier>
                      </ItemIdInfo>
                  <com.csc_GeneralPartyInfo>
                        <com.csc_EntityType/>
                        <NameInfo>
                          <PersonNameLong>
                            <SurnameLong />
                          </PersonNameLong>
                        </NameInfo>
                        <Addr>
                          <Addr1 />
                          <Addr2 />
                          <City />
                          <StateProvCd />
                          <PostalCode />
                          <CountryCd />
                        </Addr>
                        <Communications>
                          <PhoneInfo>
                            <PhoneTypeCd>Office</PhoneTypeCd>
                            <PhoneNumber />
                          </PhoneInfo>
                          <PhoneInfo>
                            <PhoneTypeCd>Fax</PhoneTypeCd>
                            <PhoneNumber />
                          </PhoneInfo>
                          <EmailInfo>
                            <EmailAddr />
                          </EmailInfo>
                        </Communications>
                      </com.csc_GeneralPartyInfo>
               </com.csc_EmpPhysician>");

            return oTemplate;
        }

        private XElement FASEmpHospitalAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <com.csc_EmpHospital>
                    <ItemIdInfo>
                        <OtherIdentifier>
                          <OtherIdTypeCd>EmpHospitalId</OtherIdTypeCd>
                          <OtherId />
                        </OtherIdentifier>
                      </ItemIdInfo>
                  <com.csc_GeneralPartyInfo>
<com.csc_EntityType/>
                        <NameInfo>
                          <PersonNameLong>
                            <SurnameLong />
                          </PersonNameLong>
                        </NameInfo>
                        <Addr>
                          <Addr1 />
                          <Addr2 />
                          <City />
                          <StateProvCd />
                          <PostalCode />
                          <CountryCd />
                        </Addr>
                        <Communications>
                          <PhoneInfo>
                            <PhoneTypeCd>Office</PhoneTypeCd>
                            <PhoneNumber />
                          </PhoneInfo>
                          <PhoneInfo>
                            <PhoneTypeCd>Fax</PhoneTypeCd>
                            <PhoneNumber />
                          </PhoneInfo>
                          <EmailInfo>
                            <EmailAddr />
                          </EmailInfo>
                        </Communications>
                      </com.csc_GeneralPartyInfo>
               </com.csc_EmpHospital>");

            return oTemplate;
        }

        private XElement FASEmployeeAcordTemplate()
        {
            //asharma326 <com.csc_EmployeeSupp></com.csc_EmployeeSupp> for employee supp
            XElement oTemplate = XElement.Parse(@"
                  <EmployeeInfo>
                    <ItemIdInfo>
                        <OtherIdentifier>
                          <OtherIdTypeCd>EmployeeId</OtherIdTypeCd>
                          <OtherId />
                        </OtherIdentifier>
                      </ItemIdInfo>
                      <com.csc_GeneralPartyInfo>
                        <com.csc_EntityType/>
                        <NameInfo>
                          <PersonNameLong>
                            <SurnameLong />
                            <GivenNameLong />
                            <OtherGivenNameLong />
                            <Title />
                          </PersonNameLong>
                        </NameInfo>
                        <Addr>
                          <Addr1 />
                          <Addr2 />
                          <City />
                          <StateProvCd />
                          <PostalCode />
                          <CountryCd />
                        </Addr>
                        <Communications>
                          <PhoneInfo>
                            <PhoneTypeCd>Office</PhoneTypeCd>
                            <PhoneNumber />
                          </PhoneInfo>
                          <PhoneInfo>
                            <PhoneTypeCd>Home Phone</PhoneTypeCd>
                            <PhoneNumber />
                          </PhoneInfo>
                          <PhoneInfo>
                            <PhoneTypeCd>Fax</PhoneTypeCd>
                            <PhoneNumber />
                          </PhoneInfo>
                          <EmailInfo>
                            <EmailAddr />
                          </EmailInfo>
                        </Communications>
                        <PersonInfo>
                          <BirthDt />
                          <GenderCd />
                        </PersonInfo>
                      </com.csc_GeneralPartyInfo>
                      <HiredDt></HiredDt>
                      <NCCIIDNumber></NCCIIDNumber>
                      <com.csc_EmployeeAttorney></com.csc_EmployeeAttorney>
                      <com.csc_TerminatedDt></com.csc_TerminatedDt>
                      <com.csc_PositionCode></com.csc_PositionCode>
                      <com.csc_MaritalStatus></com.csc_MaritalStatus>
                      <com.csc_JobClassification></com.csc_JobClassification>
                      <com.csc_HospitalSDaterange></com.csc_HospitalSDaterange>
                      <com.csc_HospitalEDaterange></com.csc_HospitalEDaterange>
                      <com.csc_EMPOthrTreatment></com.csc_EMPOthrTreatment>
                      <com.csc_EmployeeSupp></com.csc_EmployeeSupp>
                </EmployeeInfo>    ");

            return oTemplate;
        }

        private XElement FASAdjusterPartyAcordTemplate()
        {
            //asharma326 added <com.csc_AdjusterPartySupp></com.csc_AdjusterPartySupp> For Adjsuter Supp
            XElement oTemplate = XElement.Parse(@"
                 <AdjusterParty>
                      <ItemIdInfo>
                        <OtherIdentifier>
                          <OtherIdTypeCd>AdjusterId</OtherIdTypeCd>
                          <OtherId />
                        </OtherIdentifier>
                      </ItemIdInfo>
                      <GeneralPartyInfo>
                       <com.csc_EntityType/>
                        <NameInfo>
                          <PersonNameLong>
                            <SurnameLong />
                            <GivenNameLong />
                            <OtherGivenNameLong />
                          </PersonNameLong>
                        </NameInfo>
                        <Addr>
                          <Addr1 />
                          <Addr2 />
                          <City />
                          <StateProvCd />
                          <PostalCode />
                          <CountryCd />
                        </Addr>
                        <Communications>
                          <PhoneInfo>
                            <PhoneTypeCd>Office</PhoneTypeCd>
                            <PhoneNumber />
                          </PhoneInfo>
                        </Communications>
                        <com.csc_Title />
                      </GeneralPartyInfo>
                    <com.csc_AdjusterPartySupp></com.csc_AdjusterPartySupp>
            </AdjusterParty>   ");

            return oTemplate;
        }

        private XElement FASVehicleAcordTemplate()
        {
            //asharma326 added <com.csc_UnitSupp></com.csc_UnitSupp> for Vehicle supp
            XElement oTemplate = XElement.Parse(@"
                   <PCVEH>
                     <ItemIdInfo>
                        <OtherIdentifier>
                          <OtherIdTypeCd>VehicleId</OtherIdTypeCd>
                          <OtherId />
                        </OtherIdentifier>
                      </ItemIdInfo>
                      <Vin />
                      <LeasedDt />
                      <Manufacturer />
                      <Model />
                      <ModelYear />
                      <LicensePlateNumber />
                      <UnitTypeCode />
                      <UnitTypeCodeDesc />
                      <InsuranceCoverage />
                      <com.csc_PropertyDamagedVehInd></com.csc_PropertyDamagedVehInd>
                      <com.csc_ProbableIncurredAmt></com.csc_ProbableIncurredAmt>
                      <com.csc_DrivableInd></com.csc_DrivableInd>
                      <com.csc_VEHStateCd/>
                      <com.csc_VEHNonVehicleDamage/>
                      <com.csc_VEHEstDamage/>
                      <com.csc_VEHInsured></com.csc_VEHInsured>
                      <com.csc_UnitSupp></com.csc_UnitSupp>
                   </PCVEH>   ");

            return oTemplate;
        }

        private XElement FASPropertyInfoAcordTemplate()
        {
            //asharma326 added <com.csc_PropertySupp></com.csc_PropertySupp> for property Supp
            XElement oTemplate = XElement.Parse(@"
                   <PropertyInfo>
                    <ItemIdInfo>
                        <OtherIdentifier>
                          <OtherIdTypeCd>PropertyId</OtherIdTypeCd>
                          <OtherId />
                        </OtherIdentifier>
                      </ItemIdInfo>
                      <Pin />
                      <Addr>
                        <Addr1 />
                        <Addr2 />
                        <City />
                        <StateProvCd />
                        <PostalCode />
                        <CountryCd />
                      </Addr>
                      <ClassOfConstruction />
                      <YearOfConstruction />
                      <SquareFootage />
                      <NoOfStories />
                      <AvgStoryHeight />
                      <Description/>
                      <Insured/>
                      <AppraisedValue />
                      <ReplacementValue />
                      <AppraisedDate />
                      <CategoryCode />
                      <CategoryCodeDesc />
                      <com.csc_VacantInd></com.csc_VacantInd>
                      <com.csc_OtherInsuranceInd></com.csc_OtherInsuranceInd>
                      <com.csc_DamageDesc></com.csc_DamageDesc>
                      <com.csc_TypeOfPropertyCode></com.csc_TypeOfPropertyCode>
                      <com.csc_EstimatedDamage></com.csc_EstimatedDamage>
                      <com.csc_IncendiaryFireInd></com.csc_IncendiaryFireInd>
                      <com.csc_UnderConstruction></com.csc_UnderConstruction>
                      <com.csc_ScheduleNameCd/>
                      <com.csc_ScheduleNameDesc/>
                      <com.csc_Amount></com.csc_Amount>
                      <com.csc_PropertySupp></com.csc_PropertySupp>
              </PropertyInfo>  ");

            return oTemplate;
        }

        private XElement FASLitigationInfoAcordTemplate()
        {
            //asharma326 added <com.csc_LitigationSupp></com.csc_LitigationSupp> Litigation Supp
            XElement oTemplate = XElement.Parse(@"
                   <LitigationInfo>
                        <ItemIdInfo>
                        <OtherIdentifier>
                          <OtherIdTypeCd>LitigationId</OtherIdTypeCd>
                          <OtherId />
                        </OtherIdentifier>
                      </ItemIdInfo>
                      <DocketNum></DocketNum>
                      <com.csc_SuitDate></com.csc_SuitDate>
                      <com.csc_CourtDate></com.csc_CourtDate>
                      <com.csc_VenueState></com.csc_VenueState>
                      <com.csc_LitigationType></com.csc_LitigationType>
                      <LitigationStatusCd></LitigationStatusCd>
                      <csc.com_JudgeNameInfo>
                        <com.csc_EntityType/>
                        <PersonNameLong>
                          <SurnameLong />
                          <GivenNameLong />
                          <OtherGivenNameLong />
                        </PersonNameLong>
                      </csc.com_JudgeNameInfo>
                      <csc.com_AttorneyNameInfo>
                        <com.csc_EntityType/>
                        <PersonNameLong>
                          <SurnameLong />
                          <GivenNameLong />
                          <OtherGivenNameLong />
                        </PersonNameLong>
                      </csc.com_AttorneyNameInfo>
                      <Addr>
                        <Addr1 />
                        <Addr2 />
                        <City />
                        <StateProvCd />
                        <PostalCode />
                        <CountryCd />
                      </Addr>
                      <com.csc_AttorneyFirm>
                       <PersonNameLong>
                          <SurnameLong />
                          <GivenNameLong />
                          <OtherGivenNameLong />
                        </PersonNameLong>
                     </com.csc_AttorneyFirm>
                      <Communications>
                        <PhoneInfo>
                          <PhoneTypeCd>Office</PhoneTypeCd>
                          <PhoneNumber />
                        </PhoneInfo>
                        <PhoneInfo>
                          <PhoneTypeCd>Home Phone</PhoneTypeCd>
                          <PhoneNumber />
                        </PhoneInfo>
                      </Communications>
                    <com.csc_LitigationSupp></com.csc_LitigationSupp>
                </LitigationInfo>  ");

            return oTemplate;
        }

        private XElement FASDefendantInfoAcordTemplate()
        {
            //asharma326 added <com.csc_DefendantSupp></com.csc_DefendantSupp> for Defendant Supp
            XElement oTemplate = XElement.Parse(@"
                   <com.csc_DefendantInfo>
                    <ItemIdInfo>
                        <OtherIdentifier>
                          <OtherIdTypeCd>DefendantId</OtherIdTypeCd>
                          <OtherId />
                        </OtherIdentifier>
                      </ItemIdInfo>
                          <com.csc_DefendantType></com.csc_DefendantType>
                          <com.csc_FirmName></com.csc_FirmName>
                          <com.csc_DefendantNameInfo>
                            <com.csc_EntityType/>
                            <PersonNameLong>
                              <SurnameLong />
                              <GivenNameLong />
                              <OtherGivenNameLong />
                            </PersonNameLong>
                          </com.csc_DefendantNameInfo>
                          <com.csc_DefendantAddr>
                            <Addr1 />
                            <Addr2 />
                            <City />
                            <StateProvCd />
                            <PostalCode />
                            <CountryCd />
                          </com.csc_DefendantAddr>
                          <com.csc_DefendantCommunications>
                            <PhoneInfo>
                              <PhoneTypeCd>Office</PhoneTypeCd>
                              <PhoneNumber />
                            </PhoneInfo>
                            <PhoneInfo>
                              <PhoneTypeCd>Home Phone</PhoneTypeCd>
                              <PhoneNumber />
                            </PhoneInfo>
                          </com.csc_DefendantCommunications>
                          <com.csc_DefendanAttorneytNameInfo>
                            <com.csc_EntityType/>
                            <PersonNameLong>
                              <SurnameLong />
                              <GivenNameLong />
                              <OtherGivenNameLong />
                            </PersonNameLong>
                          </com.csc_DefendanAttorneytNameInfo>
                      <com.csc_DefendanAttorneyFirm>
                       <PersonNameLong>
                          <SurnameLong />
                          <GivenNameLong />
                          <OtherGivenNameLong />
                        </PersonNameLong>
                     </com.csc_DefendanAttorneyFirm>
                          <com.csc_DefendantAttorneyAddr>
                            <Addr1 />
                            <Addr2 />
                            <City />
                            <StateProvCd />
                            <PostalCode />
                            <CountryCd />
                          </com.csc_DefendantAttorneyAddr>
                          <com.csc_DefendantAttorneyCommunications>
                            <PhoneInfo>
                              <PhoneTypeCd>Office</PhoneTypeCd>
                              <PhoneNumber />
                            </PhoneInfo>
                            <PhoneInfo>
                              <PhoneTypeCd>Home Phone</PhoneTypeCd>
                              <PhoneNumber />
                            </PhoneInfo>
                          </com.csc_DefendantAttorneyCommunications>
                <com.csc_DefendantSupp></com.csc_DefendantSupp>
                </com.csc_DefendantInfo>  ");

            return oTemplate;
        }

        private XElement FASExpertWitnessAcordTemplate()
        {
            //asharma326 added <com.csc_ExpertSupp></com.csc_ExpertSupp> for Expert Supp
            XElement oTemplate = XElement.Parse(@"
                   <com.csc_ExpertWitnessInfo>
                    <ItemIdInfo>
                        <OtherIdentifier>
                          <OtherIdTypeCd>ExpertWitnessId</OtherIdTypeCd>
                          <OtherId />
                        </OtherIdentifier>
                      </ItemIdInfo>
                      <com.csc_WitnessTypeCode></com.csc_WitnessTypeCode>
                      <NameInfo>
                        <com.csc_EntityType/>
                        <PersonNameLong>
                          <SurnameLong />
                          <GivenNameLong />
                          <OtherGivenNameLong />
                        </PersonNameLong>
                      </NameInfo>
                      <Addr>
                        <Addr1 />
                        <Addr2 />
                        <City />
                        <StateProvCd />
                        <PostalCode />
                        <CountryCd />
                      </Addr>
                      <Communications>
                        <PhoneInfo>
                          <PhoneTypeCd>Office</PhoneTypeCd>
                          <PhoneNumber />
                        </PhoneInfo>
                        <PhoneInfo>
                          <PhoneTypeCd>Home Phone</PhoneTypeCd>
                          <PhoneNumber />
                        </PhoneInfo>
                      </Communications>
                   <com.csc_ExpertSupp></com.csc_ExpertSupp>
                 </com.csc_ExpertWitnessInfo>  ");

            return oTemplate;
        }

        private XElement FASLiabilityLossAcordTemplate()
        {
            //asharma326 <com.csc_LiabilityLossSupp></com.csc_LiabilityLossSupp> for Liability loss Supp
            XElement oTemplate = XElement.Parse(@"
                   <LiabilityLossInfo>
                    <ItemIdInfo>
                        <OtherIdentifier>
                          <OtherIdTypeCd>LiabilityLossId</OtherIdTypeCd>
                          <OtherId />
                        </OtherIdentifier>
                      </ItemIdInfo>
                      <com.csc_LiabilityTypeCd></com.csc_LiabilityTypeCd>
                      <com.csc_AffectedParty></com.csc_AffectedParty>
                      <com.csc_GeneralDamages></com.csc_GeneralDamages>
                      <com.csc_SpecialDamages></com.csc_SpecialDamages>
                      <com.csc_PolicyNumber></com.csc_PolicyNumber>
                      <Coverage>
                        <CoverageDesc />
                        <Limit>
                          <FormatCurrencyAmt>
                            <Amt></Amt>
                          </FormatCurrencyAmt>
                        </Limit>
                      </Coverage>
                    <com.csc_LiabilityLossSupp></com.csc_LiabilityLossSupp>
                </LiabilityLossInfo>  ");

            return oTemplate;
        }
        # endregion
    }

    public class FASReportable
    {
        public string SuppTableName { get; set; }
        public string SysFieldName { get; set; }
        public string SuppFieldType { get; set; }
        public string SuppFASReportable { get; set; }
        public string SuppUserPromt { get; set; }
        public string SuppMultiValues { get; set; }
        public string SuppValue { get; set; }
    }
}

