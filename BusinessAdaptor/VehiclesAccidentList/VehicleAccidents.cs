using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Application.VehicleAccidentsList;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using System.IO;
using Riskmaster.Security;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: VehiclesAccidentList.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 20-Sept-2005
	///* $Author	: Rahul Sharma
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is a wrapper of Vehicles Accident Class Application Layer.
	/// </summary>
	public class VehicleAccidentsAdaptor: BusinessAdaptorBase
	{
		#region Constructor
		public VehicleAccidentsAdaptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		#endregion

		#region Public Functions
		public bool GetAccidentsList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.VehicleAccidentsList.VehicleAccidents objVehicleAccidents=null;
			try
			{
				XmlElement objElement=null;		//used for parsing the input xml
				string sReturnXml=string.Empty; //used for holding the output xml string
				string sOutputXml=string.Empty;
				string sUnitId=string.Empty;
                string sLangCode = string.Empty;
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//UnitId");
                sLangCode = p_objXmlIn.SelectSingleNode("//LangCode").InnerText;
				if (objElement==null)
				{
					try
					{
                        p_objErrOut.Add("VehicleAccidentsListParameterMissing", Globalization.GetString("VehicleAccidentsListParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					}
					catch(Exception ex)
					{
						string ss= ex.Message;
					}
					return false;
				}
				sUnitId=objElement.InnerXml;
			
				objVehicleAccidents = new Riskmaster.Application.VehicleAccidentsList.VehicleAccidents(base.ClientId);
                sOutputXml = objVehicleAccidents.GetAccidentsList(sUnitId, base.connectionString, sLangCode);
				p_objXmlOut.LoadXml(sOutputXml);
				//create the output xml by including the Output parameter also
				sReturnXml="<ReturnXml><OutputResult>Success</OutputResult></ReturnXml>";
				//p_objXmlOut.LoadXml(sReturnXml);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("VehicleAccidentsList.GetAccidentsList.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			
		}
		#endregion

	}
}
