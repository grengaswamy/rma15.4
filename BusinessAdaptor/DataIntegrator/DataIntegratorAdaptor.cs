﻿
using System;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.IO;
using Riskmaster.Security;
using System.Collections;
using Riskmaster.Models;
using System.Collections.Generic;
using Riskmaster.Application.DataIntegrator;
using Riskmaster.Application.RMUtilities;
using Riskmaster.Db;
using System.Data.SqlClient;						//kkaur25-UI change for pmt table
using System.Data;		//kkaur25-UI change for pmt table

namespace Riskmaster.BusinessAdaptor
{
    public class DataIntegratorAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        //bool m_bSoapAttachmentCachedToDisk = false;
       
        public DataIntegratorAdaptor()
        {
            //m_bSoapAttachmentCachedToDisk = (null != Riskmaster.Common.RMConfigurator.Value("CommonWebServiceHandler/UseAttachmentServiceExtension"));
        }

        #endregion

        #region Public Methods

        //Developer - Subhendu | Template - DA CLAIM EXPORT CSSTARS | Start
        /// <summary>
		///	This method is a wrapper to Riskmaster.Application.DataIntegrator.Setting.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISettings = new Settings(base.ClientId);
            try
            {
                objDISettings.connectionString = m_connectionString;
                p_objXmlOut = objDISettings.Get(p_objXmlIn);
                return true;
            }
            catch(RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDISettings = null;
            }
        }
        /// <summary>
        /// This method is a wrapper to Riskmaster.Application.DataIntegrator.Setting.GetCEOrg() method.	
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetCEOrg(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISettings = new Settings(base.ClientId);			
            try
            {
                objDISettings.connectionString = m_connectionString;
                p_objXmlOut = objDISettings.GetCEOrg(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDISettings = null;
            }
        }
        /// <summary>
        /// This method is a wrapper to Riskmaster.Application.DataIntegrator.Setting.Save() method.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISettings = new Settings(base.ClientId); //Application layer component			
            try
            {
                objDISettings.connectionString = m_connectionString;
                p_objXmlOut = objDISettings.Save(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDISettings = null;
            }
        }
        /// <summary>
        /// This method is a wrapper to Riskmaster.Application.DataIntegrator.Setting.Delete() method.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISettings = new Settings(base.ClientId);//Application layer component			
            try
            {
                objDISettings.connectionString = m_connectionString;
                p_objXmlOut = objDISettings.Delete(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDISettings = null;
            }
        }
        /// <summary>
        /// This method is a wrapper to Riskmaster.Application.DataIntegrator.Setting.CleanDataGridonCancel() method.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool CleanDataGridonCancel(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISetting = new Settings(base.ClientId);
                                   
            objDISetting.connectionString = m_connectionString;
            objDISetting.CleanDataGridonCancel(p_objXmlIn);
            return true;
        }
        //Developer - Subhendu | Template - DA CLAIM EXPORT CSSTARS | End

        //ihthesham start
        public bool ISOSave(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISettings = new Settings(base.ClientId); //Application layer component			
            try
            {
                objDISettings.connectionString = m_connectionString;
                p_objXmlOut = objDISettings.ISOSave(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDISettings = null;
            }
        }
        //ihtesham end

        
       
        public DataIntegratorModel RetrieveSettings(DataIntegratorModel objDIModel, ref BusinessAdaptorErrors p_objErrOut)
        {
            
            Settings objDISettings = new Settings(base.ClientId);
            Dictionary<string, string> objDIDict = null;
            DataIntegratorModel objReturnDIModel = new DataIntegratorModel();
            bool bIsSuccess = false;
            int iRREIdLob;  //mrishi2 - JIRA 16461 
            
            //Vsoni5 : MITS 22565 : Exception handling done for Error display on UI and error logging in service error log.
            try
            {
                //Vsoni5 -  MITS 22550 - Return Staging connectionString Node name specific to DA1 or DA2 modules 
                objDISettings.DAConnectionString = GetDAConnectionString(objDIModel.ModuleName);   // added by csingh7 : DA phase 2

                //Vsoni5 - MITS 25527, 25528 - Check if AccessStagingDataSource node exist in connectionString.config
                //This validation is just for DA MBR and DA DIS modules.
                if (String.Equals(objDIModel.ModuleName, "MBR") || String.Equals(objDIModel.ModuleName, "DIS"))
                  {
                      ValidateAccessStagingConnection();
                  }
                //END - Vsoni5 - MITS 25527, 25528.

            if (objDIModel.OptionSetID == 0)
            {
                objDISettings.connectionString = m_connectionString;
                objDISettings.moduleName = objDIModel.ModuleName;
              objDIDict = objDISettings.RetrieveDefaultSettings();
            }
            else
            {   //ipuri Mits:30917 start
                string sClaimOut;
                string sSubmissorOut;
                string sBatchID;//---sgupta320:jira-10616 for 2GB 
                //ipuri Mits:30917 end
                objDISettings.connectionString = m_connectionString;
                objDISettings.moduleName = objDIModel.ModuleName;
                objReturnDIModel.OptionSetName = objDISettings.GetOptionSetName(objDIModel.OptionSetID);
                //npradeepshar Merged MITS 21761 03/24/2011
                objReturnDIModel.OptionSetID = objDIModel.OptionSetID;
                //end
                objDIDict = objDISettings.RetrieveOptionSetSettings(userID, objDISettings.GetOptionSetName(objDIModel.OptionSetID));
                //ipuri Mits:30917 start
                objDIDict.TryGetValue("Claim_Match_OtherUserList", out sClaimOut);
                objDIModel.sUserid = sClaimOut;
                objDIDict.TryGetValue("Submissor_OtherUserList", out sSubmissorOut);
                objDIModel.sSubmissorUserid = sSubmissorOut;
                //ipuri Mits:30917 end
                
                //------sgupta320:jira-10616 for 2GB 
                objDIDict.TryGetValue("txtBatchID", out sBatchID);
                objReturnDIModel.BatchID = Convert.ToInt32(sBatchID);
                //-----end----
               
            }

             objReturnDIModel.Parms = objDIDict;
             //Anu Tennyson for PSO Starts
            if (string.Compare(objDIModel.ModuleName, "PSO", true) == 0)
            {
                string sComapnyName = string.Empty;
                int iCompanyId = Conversion.CastToType<int>(objDIDict["COMPANY_ID"], out bIsSuccess);
                if (iCompanyId != 0)
                {
                    sComapnyName = objDISettings.GetCompanyName(iCompanyId);
                }
                objDIDict.Add("COMPANY_NAME", sComapnyName);
            }
            //Anu Tennyson Ends
             // Developer Subhendu : MITS 30839 : Added Carrier or Non Carrier setting : Start
             objReturnDIModel.bIsCarrier = objDISettings.IsCarrier();
             // Developer Subhendu : MITS 30839 : Added Carrier or Non Carrier setting : End
             //Module specific settings
             if (objDIModel.ModuleName == "ISO")
             {
                 //objReturnDIModel.bCarrier = Convert.ToBoolean(objDIDict["bIsCarrier"]);
                 objReturnDIModel.dsClaimTypeGC = objDISettings.GetLOBClaimType(241);
                 objReturnDIModel.dsClaimTypeVA = objDISettings.GetLOBClaimType(242);
                 objReturnDIModel.dsClaimTypeWC = objDISettings.GetLOBClaimType(243);
                 objReturnDIModel.dsClaimTypePC = objDISettings.GetLOBClaimType(845); //Subhendu 11/02/2010 MITS 22785
                 objReturnDIModel.dsClaimantType = objDISettings.GetClaimantTypes();
                 objReturnDIModel.dsISOJuris = objDISettings.GetISOJurisStates();
                 //sagarwal54 08/14/2013 MITS 33168 Start
                 objReturnDIModel.dsLossInjury = objDISettings.GetLossInjury();
                 objReturnDIModel.dsDocumentType = objDISettings.GetDocumentTypes(); 
                 objReturnDIModel.dsEnhancedNoteType = objDISettings.GetEnhancedNoteTypes();
                 objReturnDIModel.dsDiaryType = objDISettings.GetDiaryTypes();
                 //sagarwal54 08/14/2013 MITS 33168 End
                 //ipuri Mits:30917 Start
                 //objDISettings.connectionString = m_connectionString;
                 //objDISettings.moduleName = objDIModel.ModuleName;
                 //SetSecurityInfo(objDIModel.sUserid);
                 if (!string.IsNullOrEmpty(objDIModel.sUserid))
                 {
                     objReturnDIModel.dsUserid = objDISettings.GetUserName(objDIModel.sUserid, m_DSNID, m_securityConnectionString);                  
                 }
                 if (!string.IsNullOrEmpty(objDIModel.sSubmissorUserid))
                 {                     
                     objReturnDIModel.dsSubmissoUserid = objDISettings.GetUserName(objDIModel.sSubmissorUserid, m_DSNID, m_securityConnectionString);
                 }
                 //ipuri Mits:30917 End
                 objDISettings.UpdateClaimantTable();
             }
             //Developer :- Subhendu | Module - DA CLAIM EXPORT CSStars | Retrieve setting Start
             if (objDIModel.ModuleName == "CLAIM_EXPORT_CSStars")
             {
                 if (objDIModel.OptionSetID > 0)
                 {
                     objReturnDIModel.dsClaimTypeGC = objDISettings.GetLOBClaimType(241);
                     objReturnDIModel.dsReserveTypes = objDISettings.GetDDSReserveTypeCodes(241);
                     //Policy Supp - Temporary Hidden - Start
                     //objReturnDIModel.dsPolicySupp = objDISettings.GetPolicySupp();
                     //Policy Supp - Temporary Hidden - Start
                     objReturnDIModel.sClaimTypeGC = objDISettings.GetClaimTypeOnCriteriaEdit(241, objDIModel.OptionSetID);
                     objReturnDIModel.sReserveTypeGC = objDISettings.GetReserveTypeOnCriteriaEdit(241, objDIModel.OptionSetID);
             }
                 else
                 {
                     objReturnDIModel.dsClaimTypeGC = objDISettings.GetLOBClaimType(241);
                     objReturnDIModel.dsReserveTypes = objDISettings.GetDDSReserveTypeCodes(241);
                     //Policy Supp - Temporary Hidden - Start
                     //objReturnDIModel.dsPolicySupp = objDISettings.GetPolicySupp();
                     //Policy Supp - Temporary Hidden - End
                 }
             }
             //Developer :- Subhendu | Module - DA CLAIM EXPORT CSStars | Retrieve setting End

             if (objDIModel.ModuleName == "MBR")
             {
                 objReturnDIModel.dsMedicalReserve = objDISettings.GetMedicalReserve();
                 objReturnDIModel.dsOtherReserve = objDISettings.GetOtherReserves();
                 objReturnDIModel.dsJurisStates = objDISettings.GetStates();
                 //Manika : Vsoni5 : 07/13/2011 : MITS 24924 : Retrieve Entity and People type tables
                 objReturnDIModel.dsEntityCode = objDISettings.GetECTableName();
                 objReturnDIModel.dsPeopleCode = objDISettings.GetPeopleTableName();
                 //Manika -- end
                 // Vsoni5 : 08/12/2011 : MITS 25674
                 objReturnDIModel.dsLOB = objDISettings.GetLineOfBusinessCodes();
                 
                 //Vsoni5 : 08/31/2011 : MITS 24981 : get the last run time date.
                 objReturnDIModel.sLastRuntime = objDISettings.GetLastRunTime(objDIModel.OptionSetID);
                 
                 // vchaturvedi2 MITS : 20850 Get Last Run Export Date 
                 //if (objDIModel.OptionSetID == 0)
                 //{
                 //    ////vsoni5 : MITS 23064 : MBR Criteria Screen is showing Error on Initial load.
                 //    //if (objDIDict.ContainsKey("ExportDateOnUI"))
                 //    //    objDIDict["ExportDateOnUI"] = objDISettings.GetExportDateOnUI();
                 //    //else
                 //    //    objDIDict.Add("ExportDateOnUI", objDISettings.GetExportDateOnUI());
                 //}
             }
                
             if (objDIModel.ModuleName == "MMSEA")
             {

				//mrishi2 - JIRA 16461 - Changes BEGINS
                 string rreidval = objDIModel.sRREIdSV;
                 objReturnDIModel.dsRREId = objDISettings.GetRREId(rreidval);
                 objReturnDIModel.dsClaimTypeGC = objDISettings.GetLOBClaimType(241);
                 objReturnDIModel.dsClaimTypeVA = objDISettings.GetLOBClaimType(242);
                 objReturnDIModel.dsClaimTypeWC = objDISettings.GetLOBClaimType(243);
             }
 				//mrishi2 - JIRA 16461 - Changes ENDS              

             if (objDIModel.ModuleName == "RM1099")
             {
                 objReturnDIModel.dsTransTypes = objDISettings.GetTransactionTypeCodes();
                 objReturnDIModel.dsReserveTypes = objDISettings.GetReserveTypeCodes();
                 objReturnDIModel.dsLOB = objDISettings.GetLineOfBusinessCodes();
                 objReturnDIModel.dsAccount = objDISettings.GetBankAccounts();
                 objReturnDIModel.dsEntity = objDISettings.GetEntities();
             }
             //pmittal5 - Fetch Admin Tracking Tables and Fields for first table selected
             if (objDIModel.ModuleName == "DIS")
             {
                 objReturnDIModel.dsAdminTrackingTables = objDISettings.GetAdminTrackingTables();
                 if (objReturnDIModel.Parms["AdminTrack_Area"] != "") //for existing Optiosets, if an Admin Tracking table is already selected
                     objReturnDIModel.dsAdminTrackTableFields = objDISettings.GetAdminTrackFields(objReturnDIModel.Parms["AdminTrack_Area"]);
                 else //For new optionsets, fields for first Admin Tracking Table are fetched
                     objReturnDIModel.dsAdminTrackTableFields = objDISettings.GetAdminTrackFields(objReturnDIModel.dsAdminTrackingTables.Tables[0].Rows[0][0].ToString());
             }
             //pmittal5 - Fetch Bank Accounts for Payments
             if (objDIModel.ModuleName == "DDS")
             {
                 objReturnDIModel.dsBankAccounts = objDISettings.GetDDSBankAccounts();
                 objReturnDIModel.dsLOB = objDISettings.GetLineOfBusinessCodes();
                // if (objDIModel.LOB != 0)
                    // objReturnDIModel.dsReserveTypes = objDISettings.GetDDSReserveTypeCodes(objDIModel.LOB);
                 //npradeepshar MITS 21553 merged 03/24/2011
                 //rsushilaggar MITS 21553 DATE 20-Sep-2010
                 if (objDIModel.OptionSetID != 0)
                 {
                     objReturnDIModel.dsReserveMappings = objDISettings.GetDDSReserveMappings(objDIModel.OptionSetID);
                     //Vsoni5 : MITS 24657 : Added else statement to retrieve default Reserve Mapping Settings.
                     if(objReturnDIModel.dsReserveMappings.Tables[0].Rows.Count > 0)
                        objReturnDIModel.dsReserveTypes = objDISettings.GetDDSReserveTypeCodes(Convert.ToInt32(objReturnDIModel.dsReserveMappings.Tables[0].Rows[0]["LOB_CODE"]));
                     else
                         objReturnDIModel.dsReserveTypes = objDISettings.GetDDSReserveTypeCodes(Conversion.ConvertObjToInt(objReturnDIModel.dsLOB.Tables[0].Rows[0].ItemArray[0], base.ClientId));
                 }
                 else
                     objReturnDIModel.dsReserveTypes = objDISettings.GetDDSReserveTypeCodes(Conversion.ConvertObjToInt(objReturnDIModel.dsLOB.Tables[0].Rows[0].ItemArray[0], base.ClientId));
             }

                //Start - MITS# 36997 GAP 06 - agupta298 - RMA 5497 
                if (objDIModel.ModuleName == "DCI")
                {
                    using (DbConnection dbconn = DbFactory.GetDbConnection(m_connectionString))
                    {
                        dbconn.Open();
                        objReturnDIModel.dtJurisStates = objDISettings.GetJurisdictionStates(dbconn);
                        objReturnDIModel.dtClaimTypes = objDISettings.GetClaimTypes(dbconn);
                        objReturnDIModel.dtTPInterfaceType = objDISettings.GetTPInterfaceType(dbconn);
                        objReturnDIModel.sLastRuntime = objDISettings.GetLastRunTime(dbconn, objDIModel.OptionSetID);
                    }
                }
                //End - MITS# 36997 GAP 06 - agupta298 - RMA 5497 
            return objReturnDIModel;
        }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, p_objException.Message, BusinessAdaptorErrorType.Message);
                throw p_objException;         
            }
        }

        public bool GetPositivePayAccountList(PPAccountList p_objIn)
        {
            bool bReturnValue = false;
            Settings Accountlist = new Settings(base.ClientId);
            Accountlist.connectionString = m_connectionString;

            try
            {
                bReturnValue = Accountlist.GetAccountNames(ref p_objIn);
                bReturnValue = true;
            }




            catch (Exception e)
            {
                bReturnValue = false;
            }


            return bReturnValue;
        }

        public DataIntegratorModel SaveSettings(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            TaskManager objTaskManager = new TaskManager(userLogin, base.ClientId);//rkaur27
            DataIntegratorModel objoutModel = new DataIntegratorModel();
            string sTmp = string.Empty;

            objDISettings.connectionString = m_connectionString;

            //Vsoni5 -  MITS 22550 - Return Staging connectionString Node name specific to DA1 or DA2 modules
            objDISettings.DAConnectionString = GetDAConnectionString(objDIModel.ModuleName);   // added by csingh7 : DA phase 2

            //Vsoni5 - MITS 25527, 25528 - Check if AccessStagingDataSource node exist in connectionString.config
            //This validation is just for DA MBR and DA DIS modules.
            if(String.Equals(objDIModel.ModuleName, "MBR") || String.Equals(objDIModel.ModuleName, "DIS"))
            {
                ValidateAccessStagingConnection();
            }
            //END - Vsoni5 - MITS 25527, 25528.

            objDISettings.moduleName = objDIModel.ModuleName;

            //ipuri Mits:30917 Start
            if (objDIModel.ModuleName == "ISO")
            {
                objDIModel.Parms.Add("DSNID", m_DSNID.ToString());
            }
            //ipuri Mits:30917 End

            if (objDIModel.ImportFlage == true)
            {//if import then it has to be saved 2 times one to get the optionsetid onec you have the optionsetid save the file name which is
                sTmp = RMConfigurator.TempPath;

                objDIModel.FilePath = sTmp + "\\DataAnalytics\\";

            }
                if (objDIModel.OptionSetID == 0)
                {
                    if (CheckOptionSetName(objDIModel.OptionSetName, objDISettings) == true)
                        objDIModel.OptionSetID = -1;
                    else
                        //------sgupta320: jira-10616 for more than 2GB
                        //objDIModel.OptionSetID = objDISettings.InsertSettings(userID, objDIModel.OptionSetName, objDIModel.Parms);
                        objDIModel.OptionSetID = objDISettings.InsertSettings(userID, objDIModel.OptionSetName, objDIModel.Parms,objDIModel.BatchID);
                        
                    if(objDIModel.OptionSetID != -1 && String.Equals(objDIModel.ModuleName,"DDS"))
                    {
                        objDISettings.InsertReserveMappings(objDIModel.OptionSetID,objDIModel.dsReserveMappings);
                    }

                }
                else
                {
                    //------sgupta320: jira-10616 for more than 2GB
                    //objDIModel.OptionSetID = objDISettings.SaveSettings(userID, objDIModel.OptionSetName, objDIModel.Parms);
                    objDIModel.OptionSetID = objDISettings.SaveSettings(userID, objDIModel.OptionSetName, objDIModel.Parms,objDIModel.BatchID);
                    if (objDIModel.OptionSetID != -1 && String.Equals(objDIModel.ModuleName, "DDS"))
                    {
                        objDISettings.SaveReserveMappings(objDIModel.OptionSetID, objDIModel.dsReserveMappings);
                    }
                }
                objoutModel = objDIModel;

            // Save the settings to the Task Manager database if the Optionset id is not -1 or 0.
                if (objDIModel.OptionSetID > 0)
            {
                XmlDocument objXmlIn = new XmlDocument();
                string sTaskManagerXml = objDIModel.TaskManagerXml;

                if (!string.IsNullOrEmpty(sTaskManagerXml))
                {
                    objXmlIn.LoadXml(sTaskManagerXml);

                    // Pass the OptionsetId to Task Manager.
                    if (objXmlIn.SelectSingleNode("//OptionsetId") != null)
                    {
                        objXmlIn.SelectSingleNode("//OptionsetId").InnerText = objDIModel.OptionSetID.ToString();
                    }
                    
                    string sScheduleType = objXmlIn.SelectSingleNode("//ScheduleTypeId").InnerText;

                    switch (sScheduleType)
                    {
                        case "1":
                            objTaskManager.SaveOneTimeSettings(objXmlIn);
                            break;
                        case "2":
                            objTaskManager.SavePeriodicalSettings(objXmlIn);
                            break;
                        case "3":
                            objTaskManager.SaveWeeklySettings(objXmlIn);
                            break;
                        case "4":
                            objTaskManager.SaveMonthlySettings(objXmlIn);
                            break;
                        case "5":
                            objTaskManager.SaveYearlySettings(objXmlIn);
                            break;

                        default:
                            break;
                    }
                }
            }

                return objoutModel;
        }

        /// <summary>
        /// Vsoni5 - MITS 25527, 25528
        /// Check if AccessStagingDataSource node exist in connectionString.config
        /// If the AccessStagingDataSource is missing, function will through an Exception of RMAppException type
        /// </summary>
        private void ValidateAccessStagingConnection()
        {
 	        string sDAAcessConStringNodeName = "AccessStagingDataSource";
            string sDAAccessConString = String.Empty;
            try
            {
                sDAAccessConString = RMConfigurationManager.GetConfigConnectionString(sDAAcessConStringNodeName, base.ClientId);
            }
            catch
            {
                 throw new RMAppException(String.Format(@"ConnectionString does not exist for '{0}'.", sDAAcessConStringNodeName));
            }
        }
        //smahajan6 - 06/22/2010 - MITS# 21191 : Start
        

        public void UpdateDAStagingDatabase(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            int iOptionSetID = 0;
            bool bUpdateFlag = false;
            string sImportFile = string.Empty;
            string sAccessStagingDataSource = string.Empty;
            string smodName = string.Empty;
            smodName = objDIModel.ModuleName.ToString();
            bUpdateFlag = objDIModel.bUpdateFlag;
            iOptionSetID = objDIModel.OptionSetID;
            sImportFile = objDIModel.sDAStagingDBImportFile;
            sAccessStagingDataSource = RMConfigurationManager.GetConfigConnectionString("AccessStagingDataSource",base.ClientId);

            objDISettings.UpdateDAStagingDatabase(bUpdateFlag, iOptionSetID, sImportFile, sAccessStagingDataSource, objDIModel.Parms, smodName);
        }
        //smahajan6 - 06/22/2010 - MITS# 21191 : End

        //kkaur25 start UI 9907 JIRA for retrieveing file from db and saving to DataAnalytics folder
        public void RetrieveFile(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            string sImportFile = string.Empty;
            string sStagingDataSource = string.Empty;
            string sFileName = string.Empty;
            string sFilePath = string.Empty;
            
            sImportFile = objDIModel.sDAStagingDBImportFile;
            sFileName = Path.GetFileName(sImportFile);
            sFilePath = sImportFile.Substring(0, sImportFile.LastIndexOf("\\"));
            //kkaur25 start-JIRA 10283 -to set staging node of connection string
            if (objDIModel.ModuleName == "DIS")
                sStagingDataSource = RMConfigurationManager.GetConfigConnectionString("RMXStagingDataSource", base.ClientId);
            else
                sStagingDataSource = RMConfigurationManager.GetConfigConnectionString("StagingDataSource", base.ClientId);
            //kkaur25 end  -JIRA 10283 -to set staging node of connection string
            objDISettings.RetrieveFile(sFileName, sFilePath, sStagingDataSource);
        }
        //kkaur25 end UI 9907 JIRA for retrieveing file from db and saving to DataAnalytics folder

        //smahajan6 - 06/28/2010 - MITS# 21261 : Start
        public void PeekDAStagingDatabase(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            string sImportFile = string.Empty;
            string sAccessStagingDataSource = string.Empty;
            string smodName = string.Empty;
            smodName = objDIModel.ModuleName.ToString();
            sImportFile = objDIModel.sDAStagingDBImportFile;
            sAccessStagingDataSource = RMConfigurationManager.GetConfigConnectionString("AccessStagingDataSource", base.ClientId);

            //----- sgupta320: jira-10616 comment this code for more than 2GB Enhancment 
            //objDISettings.PeekDAStagingDatabase(sImportFile, sAccessStagingDataSource, objDIModel.Parms, smodName);

            //-----sgupta320:jira-10616 for more than 2GB Enhancement
            objDISettings.PeekDAStagingDatabase(sImportFile, sAccessStagingDataSource, objDIModel.Parms,smodName,objDIModel.BatchID,objDIModel.OptionSetID,objDIModel.AdminTrackTable,objDIModel.bUpdateFlag);
        }

        public string GetRMConfiguratorDATempPath()
        {
            string sTmp = string.Empty;
            sTmp = RMConfigurator.TempPath + "\\DataAnalytics\\";
            return sTmp;
        }
        //smahajan6 - 06/28/2010 - MITS# 21261 : End

        public bool CheckOptionSetName(string sOptionSetName, Settings objDISettings)
        {

            List<string> OptionSetName;
            //DI_Settings.Settings objDISettings = new DI_Settings.Settings();
            OptionSetName = objDISettings.GetOptionSetNames(userID);
            foreach (string sFind in OptionSetName)
            {
                if (sFind == sOptionSetName)
                    return true;
            }
            return false;
        }

        public bool RetrieveJobFile(JobFile objJobFile, out JobFile objOutFile)
        {
            bool bReturn = false;
            MemoryStream objJobFileStream = null;
            string sFileName = string.Empty;
            Settings objSettings = new Settings(base.ClientId);//dvatsa-cloud
            objOutFile = new JobFile();

            try
            {
                objSettings.TaskManagerConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource", base.ClientId);

                objSettings.RetrieveJobFile(objJobFile.JobId, out objJobFileStream, ref sFileName);

                objOutFile.FileContents = objJobFileStream.ToArray();
                objOutFile.FileName = sFileName;

                bReturn = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            finally
            {
            }
            return bReturn;
        }

        public int SetClaimForReplacement(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;
            int iRecordCount = 0;

            iRecordCount = objDISettings.SetClaimForReplacement(objDIModel);
            return iRecordCount;
        }

        public int ResetClaimForInitialSubmit(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;
            int iRecordCount = 0;

            iRecordCount = objDISettings.ResetClaimForInitialSubmit(objDIModel);
            return iRecordCount;
        }
        // Developer - Subhendu : MITS 30839 : Function UpdateISOCodeTables Not Required any more. : Start
        //public void UpdateISOCodeTables(DataIntegratorModel objDIModel)
        //{
        //    bool bStatus = false;
        //    Settings objDISettings = new Settings(base.ClientId);
        //    objDISettings.connectionString = m_connectionString;
        //    objDISettings.moduleName = objDIModel.ModuleName;

        //    //string sSQL = "CREATE TABLE CL_ISO_POLICY_CODES (LOB INT NOT NULL, POLICY_CODE VARCHAR(5) NOT NULL, DESCRIP VARCHAR(40))";
        //    //Subh
        //    string sSQL = "CREATE TABLE CL_ISO_POLICY_CODES (RECORD_TYPE VARCHAR(5) NOT NULL,POLICY_CODE VARCHAR(5) NOT NULL, DESCRIP VARCHAR(40))";
        //    bStatus = objDISettings.CreateISOCodeTables("CL_ISO_POLICY_CODES", sSQL);

        //    sSQL = "CREATE TABLE CL_ISO_COVERAGE_CODES (RECORD_TYPE VARCHAR(5) NOT NULL, POLICY_CODE VARCHAR(5) NOT NULL, COVERAGE_CODE VARCHAR(5) NOT NULL, DESCRIP VARCHAR(40))";
        //    bStatus = objDISettings.CreateISOCodeTables("CL_ISO_COVERAGE_CODES", sSQL);

        //    sSQL = "CREATE TABLE CL_ISO_LOSS_CODES (RECORD_TYPE VARCHAR(5) NOT NULL, POLICY_CODE VARCHAR(5) NOT NULL, COVERAGE_CODE VARCHAR(5) NOT NULL, LOSS_CODE VARCHAR(5) NOT NULL, DESCRIP VARCHAR(40))";
        //    bStatus = objDISettings.CreateISOCodeTables("CL_ISO_LOSS_CODES", sSQL);
        //    //Subhnendu : ISOCleanUp : Start
        //    objDISettings.UpdatePolicyCodes();
        //    objDISettings.UpdateCoverageCodes();
        //    objDISettings.UpdateLossCodes();
        //    //Subhnendu : ISOCleanUp : Start
        //    objDISettings.CreateMappingTable();
        //}
        //Subhendu : 3/7/2013 : MITS 31855 : Function modified.        

        /// <summary>
        /// Developer - Subhendu : MITS 30839 : Function GetISOLossMappings : Start
        /// </summary>
        /// <param name="objDIModel"></param>
        /// <returns></returns>
        public DataIntegratorModel GetISOLossMappings(DataIntegratorModel objDIModel)
        {
            //bool bStatus = false;
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;

            //string sSQL = "CREATE TABLE CL_ISO_POLICY_CODES (RECORD_TYPE VARCHAR(5) NOT NULL, POLICY_CODE VARCHAR(5) NOT NULL, DESCRIP VARCHAR(40))";
            //bStatus = objDISettings.CreateISOCodeTables("CL_ISO_POLICY_CODES", sSQL);

            //sSQL = "CREATE TABLE CL_ISO_COVERAGE_CODES (RECORD_TYPE VARCHAR(5) NOT NULL, POLICY_CODE VARCHAR(5) NOT NULL, COVERAGE_CODE VARCHAR(5) NOT NULL, DESCRIP VARCHAR(40))";
            //bStatus = objDISettings.CreateISOCodeTables("CL_ISO_COVERAGE_CODES", sSQL);

            //sSQL = "CREATE TABLE CL_ISO_LOSS_CODES (RECORD_TYPE VARCHAR(5) NOT NULL, POLICY_CODE VARCHAR(5) NOT NULL, COVERAGE_CODE VARCHAR(5) NOT NULL, LOSS_CODE VARCHAR(5) NOT NULL, DESCRIP VARCHAR(40))";
            //bStatus = objDISettings.CreateISOCodeTables("CL_ISO_LOSS_CODES", sSQL);

            //if (bStatus == true)
            //{
            //    //Subhnendu : ISOCleanUp : Start
            //    objDISettings.UpdatePolicyCodes();
            //    objDISettings.UpdateCoverageCodes();
            //    objDISettings.UpdateLossCodes();
            //    //Subhnendu : ISOCleanUp : End
            //}

            //objDISettings.CreateMappingTable();

            objDIModel.dsRMClaimType = objDISettings.GetISOLossMappings(objDIModel.LOB);
            return objDIModel;
        }
        // Developer - Subhendu : MITS 30839 : Function GetISOLossMappings : End

        public DataIntegratorModel GetRMClaimTypeMapping(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;
            objDISettings.GetRMClaimTypeMapping(ref objDIModel);
            return objDIModel;
        }

        public DataIntegratorModel GetCoverageTypeMapping(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;
            // Developer - Subhendu : MITS 30839 : Start
            objDIModel.dsISOCoverageType = objDISettings.GetCoverageTypeMapping(objDIModel.LOB, objDIModel.sISOPolicyCode, objDIModel.sISORecordType);
            // Developer - Subhendu : MITS 30839 : End
            return objDIModel;
        }

        public DataIntegratorModel GetLossTypeMapping(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;
            // Developer - Subhendu : MITS 30839 : Start
            objDIModel.dsISOLossType = objDISettings.GetLossTypeMapping(objDIModel.LOB, objDIModel.sISOPolicyCode, objDIModel.sISOCoverageCode, objDIModel.sISORecordType);
            // Developer - Subhendu : MITS 30839 : End
            return objDIModel;
        }

        public DataIntegratorModel SaveRMLossTypeMapping(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;
            objDISettings.SaveRMLossTypeMapping(objDIModel.LOB, objDIModel.RMClaimTypeID, objDIModel.sISOPolicyCode, objDIModel.sISOCoverageCode, objDIModel.sISOLossCode, objDIModel.iPolicyType,objDIModel.iLossTypeCode, objDIModel.iDisabilityCode);
            return objDIModel;
        }

        public DataIntegratorModel GetClaimantTypeMapping(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;
            objDISettings.GetClaimantTypeMapping(ref objDIModel);
            return objDIModel;
        }

        //mihtesham

        public DataIntegratorModel GetPolicyLOB(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;
            objDIModel.dsPolicyLOB = objDISettings.GetPolicyLOB("POLICY_CLAIM_LOB");
            // Developer - Subhendu : MITS 30839 : Start
            //objDIModel.dsLossCode = objDISettings.GetPolicyLOB("LOSS_CODES");
            //objDIModel.dsLossCode = objDISettings.GetPolicyTypeOfLossOrDisabilityCode(objDIModel.iPolicyType, objDIModel.iCovCode, "LOSS_CODE"); - Original
            
            //Developer – abharti5 |MITS 36676 | start
            objDIModel.dsLossCode = objDISettings.GetPolicyTypeOfLossOrDisabilityCode(objDIModel.iPolicyType, objDIModel.iCovCode, "LOSS_CODE", objDIModel.iPolicySystemId);
            //objDIModel.dsDisablityCode = objDISettings.GetPolicyLOB("DISABILITY_TYPE_CODE");
            //objDIModel.dsDisablityCode = objDISettings.GetPolicyTypeOfLossOrDisabilityCode(objDIModel.iPolicyType, objDIModel.iCovCode, "LOSS_CODE"); - Orignial
            objDIModel.dsDisablityCode = objDISettings.GetPolicyTypeOfLossOrDisabilityCode(objDIModel.iPolicyType, objDIModel.iCovCode, "LOSS_CODE", objDIModel.iPolicySystemId);
            //Developer – abharti5 |MITS 36676 | end

            //objDISettings.CreateMappingTable();
            // Developer - Subhendu : MITS 30839 : End
            return objDIModel;
        }

        //Developer – abharti5 |MITS 36676 | start
        public DataIntegratorModel GetPolicySystemNames(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;
            objDIModel.dsPolicySystemNames = objDISettings.GetPolicySystemNames("POLICY_X_WEB");
            objDIModel.bPolicySystemInterface = objDISettings.IsPolicySystemInterface; //sagarwal54 MITS 37006 09/02/2014
            return objDIModel;
        }
        //Developer – abharti5 |MITS 36676 | end

        public DataIntegratorModel GetWCPolicyType(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;
            objDIModel.dsWCPolicyType = objDISettings.GetWCPolicyType("POLICY_CLAIM_LOB");
            return objDIModel;
        }

        public int GetPolicyClaimLOBType(int iPolicyId)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            return objDISettings.GetPolicyClaimLOBType(iPolicyId);
        }

        public DataIntegratorModel GetPolicyClaimType(DataIntegratorModel objDIModel)
        {
            //bool bStatus = false;
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;

            // Developer - Subhendu : MITS 30839 :Start
            //string sSQL = "CREATE TABLE CL_ISO_POLICY_CODES (RECORD_TYPE VARCHAR(5) NOT NULL, RECORD_TYPE VARCHAR(5) NULL, POLICY_CODE VARCHAR(5) NOT NULL, DESCRIP VARCHAR(100))";
            //bStatus = objDISettings.CreateISOCodeTables("CL_ISO_POLICY_CODES", sSQL);

            //sSQL = "CREATE TABLE CL_ISO_COVERAGE_CODES (RECORD_TYPE VARCHAR(5) NOT NULL,RECORD_TYPE VARCHAR(5) NULL, POLICY_CODE VARCHAR(5) NOT NULL, COVERAGE_CODE VARCHAR(5) NOT NULL, DESCRIP VARCHAR(100))";
            //bStatus = objDISettings.CreateISOCodeTables("CL_ISO_COVERAGE_CODES", sSQL);

            //sSQL = "CREATE TABLE CL_ISO_LOSS_CODES (RECORD_TYPE VARCHAR(5) NOT NULL,RECORD_TYPE VARCHAR(5) NULL, POLICY_CODE VARCHAR(5) NOT NULL, COVERAGE_CODE VARCHAR(5) NOT NULL, LOSS_CODE VARCHAR(5) NOT NULL, DESCRIP VARCHAR(100))";
            //bStatus = objDISettings.CreateISOCodeTables("CL_ISO_LOSS_CODES", sSQL);

            //if (bStatus == true)
            //{
            //    //Subhnendu : ISOCleanUp : Start
            //    objDISettings.UpdatePolicyCodes();
            //    objDISettings.UpdateCoverageCodes();
            //    objDISettings.UpdateLossCodes();
            //    //Subhnendu : ISOCleanUp : End
            //}

            //objDISettings.CreateMappingTable();
            // Developer - Subhendu : MITS 30839 :End
            objDIModel.dsPolicyClmtTypeMapping = objDISettings.GetPolicyClaimType(objDIModel.iPolicyType);
            return objDIModel;
        }

        //sagarwal54 MITS 35386 start
        public DataIntegratorModel GetrmAPropTypeAndISOPropType(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;
            objDIModel.dsrmAPropCodes = objDISettings.GetrmAPropTypeAndISOPropType();
            return objDIModel;
        }

        //function to save new property mappings
        public bool ISOSavePropertyMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISettings = new Settings(base.ClientId); //Application layer component			
            try
            {
                objDISettings.connectionString = m_connectionString;
                p_objXmlOut = objDISettings.ISOSavePropertyMapping(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDISettings = null;
            }
        }

        //function to display existing property mapping in grid
        public bool ISOGetPropType(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISettings = new Settings(base.ClientId);
            try
            {
                objDISettings.connectionString = m_connectionString;

               
                p_objXmlOut = objDISettings.ISOGetPropType(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDISettings = null;
            }
        }

        //function to delete existing prop mapping from grid
        public bool ISODeletePropertyMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISettings = new Settings(base.ClientId);
            try
            {
                objDISettings.connectionString = m_connectionString;
                p_objXmlOut = objDISettings.ISODeletePropertyMapping(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDISettings = null;
            }
        }

        //sagarwal54 MITS 35386 end

        //sagarwal54 MITS 35704 start
        public bool ISOGetPartyToClaimTypes(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISettings = new Settings(base.ClientId);
            try
            {
                objDISettings.connectionString = m_connectionString;
                p_objXmlOut = objDISettings.ISOGetPartyToClaimTypes(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDISettings = null;
            }
        }

        //bind drop downs
        public DataIntegratorModel GetAdditionalClaimantRoleCodes(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;
            objDIModel.dsAdditionalClaimantCodes = objDISettings.GetAdditionalClaimantRoleCodes();
            return objDIModel;
        }

        //function to save new Additional Claimant mappings
        public bool ISOSaveAdditionalClaimantMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISettings = new Settings(base.ClientId); //Application layer component			
            try
            {
                objDISettings.connectionString = m_connectionString;
                p_objXmlOut = objDISettings.ISOSaveAdditionalClaimantMapping(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDISettings = null;
            }
        }

        public bool ISODeleteAdditionalClaimantMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISettings = new Settings(base.ClientId);
            try
            {
                objDISettings.connectionString = m_connectionString;
                p_objXmlOut = objDISettings.ISODeleteAdditionalClaimantMapping(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDISettings = null;
            }
        }

        //sagarwal54 MITS 35704 end
        public bool ISOGet(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISettings = new Settings(base.ClientId);          
            XmlNode objNode = null; // mkaran2 : MITS 34134 
            try
            {
                objDISettings.connectionString = m_connectionString;
              
                #region mkaran2 - Added pagination on the ISO mapping setup grid MITS 34134                                         
                objNode = p_objXmlIn.SelectSingleNode("//ISOMappingList//CurrentPage");
                if (objNode != null && objNode.InnerText != "")
                    objDISettings.ISOCurrentPage  = objNode.InnerText;                
                else
                    objDISettings.ISOCurrentPage ="1";               
                

                objNode = p_objXmlIn.SelectSingleNode("//ISOMappingList//hdnPageSize");

                if (objNode != null && objNode.InnerText != "")
                    objDISettings.ISOPageSize = objNode.InnerText;
                else
                    objDISettings.ISOPageSize = "25";

                objNode = null; ;
                #endregion
                p_objXmlOut = objDISettings.ISOGet(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDISettings = null;
            }
        }

        public bool ISOEdit(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISettings = new Settings(base.ClientId);
            try
            {
                objDISettings.connectionString = m_connectionString;
                p_objXmlOut = objDISettings.ISOEdit(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDISettings = null;
            }
        }

        public DataIntegratorModel GetPolicyCoverageType(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;

            //Developer – abharti5 |MITS 36676 | start
            objDIModel.dsPolicyCoverageMapping = objDISettings.GetPolicyCoverageType(objDIModel.iPolicyType, objDIModel.iPolicySystemId);
            //objDIModel.dsPolicyCoverageMapping = objDISettings.GetPolicyCoverageType(objDIModel.iPolicyType); - original
            //Developer – abharti5 |MITS 36676 | end
            
            //objDISettings.CreateMappingTable();
            return objDIModel;
        }

        public bool ISODelete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISettings = new Settings(base.ClientId);//Application layer component			
            try
            {
                objDISettings.connectionString = m_connectionString;
                p_objXmlOut = objDISettings.ISODelete(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDISettings = null;
            }
        }

        //mihtesham end
        public DataIntegratorModel SaveClaimantTypeMapping(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;
            objDISettings.SaveClaimantTypeMapping(objDIModel.iClmtTypeID, objDIModel.iClaimantMapping);
            return objDIModel;
        }
        
        //pmittal5 - Retreive Fields for Admin Tracking Table selected on DIS screen
        public DataIntegratorModel GetAdminTrackFields(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;
            objDIModel.dsAdminTrackTableFields = objDISettings.GetAdminTrackFields(objDIModel.sAdminTrackTableName);
            return objDIModel;
        }

        public DataIntegratorModel GetBankAccountList(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;            
            objDISettings.moduleName = objDIModel.ModuleName;
            objDIModel.dsBankAccounts = objDISettings.GetDDSBankAccounts();
            return objDIModel;
        }

        public DataIntegratorModel GetReserveList4LOB(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            //Vsoni5 -  MITS 22550 - Return Staging connectionString Node name specific to DA1 or DA2 modules
            objDISettings.DAConnectionString = GetDAConnectionString(objDIModel.ModuleName);
            objDISettings.moduleName = objDIModel.ModuleName;
            objDIModel.dsReserveTypes = objDISettings.GetDDSReserveTypeCodes(objDIModel.LOB);
            return objDIModel;
        }


        //Vsoni5 -  MITS 22550 - Return Staging connectionString Node name specific to DA1 or DA2 modules
        private string GetDAConnectionString(string p_sModuleName)
        {
            string sDAConStringNodeName = string.Empty;
            sDAConStringNodeName = GetDAConnectionStringNodeName(p_sModuleName);
            try
            {
                return RMConfigurationManager.GetConfigConnectionString(sDAConStringNodeName, base.ClientId);
            }
            catch
            {
                throw new RMAppException(String.Format(@"ConnectionString does not exist for '{0}'.", sDAConStringNodeName));
            }
        }

        //Vsoni5 -  MITS 22550 - Return Staging connectionString Node name specific to DA1 or DA2 modules
        private string GetDAConnectionStringNodeName(string p_sModuleName)
        {
            switch (p_sModuleName)
            {
                case "DIS":
                case "DDS":
                    return "RMXStagingDataSource";
                default:
                    return "StagingDataSource";
            }
        }

        //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
        public void UploadDAImportFile(DAImportFile p_objInputFile)
        {
           string sFileName = string.Empty;
           string sDAtempFilePath = string.Empty;
           DocumentManagementAdaptor objDocumentManagementAdaptor;
           StreamedUploadDocumentType objStreamedUploadDocumentType;
           BusinessAdaptorErrors objErr = null;
           try
           {
               // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
               // We do not want to save to disk if We are on Cloud rather we need to save into DB
               if (RMConfigurationManager.GetAppSetting("CloudDeployed") == "False" && base.ClientId == 0)
               {
                   sFileName = p_objInputFile.fileName;
                   sDAtempFilePath = p_objInputFile.filePath;
                   
                   objErr = new BusinessAdaptorErrors(base.ClientId);
                   //if(!Directory.Exists(sDAtempFilePath))
                   //{
                   // Directory.CreateDirectory(sDAtempFilePath);
                   //}
                   sDAtempFilePath += "\\" + sFileName;

                   BinaryWriter objBinaryWriter = null;
                   const int BufferLen = 4096;
                   byte[] buffer = new byte[BufferLen];
                   int count = 0;

                   using (FileStream targetStream = new FileStream(sDAtempFilePath, FileMode.Create))
                   {
                       objBinaryWriter = new BinaryWriter(targetStream);
                       Stream sourceStream = new MemoryStream(p_objInputFile.FileContents);
                       buffer = new byte[BufferLen];
                       count = 0;
                       while ((count = sourceStream.Read(buffer, 0, BufferLen)) > 0)
                       {
                           objBinaryWriter.Write(buffer, 0, count);
                           objBinaryWriter.Flush();
                       }
                   }

               }
               else
               {
                   //kkaur25-UI JIRA 9651 start change for pmt table
                   Settings objDISettings = new Settings(base.ClientId);
                   DataSet ObjDS = new DataSet();
                   DataSet ObjDSOra = new DataSet();
                   string sTableName = string.Empty;
                   string sPMT = "select count(*) from PMT_DOCUMENT_STORAGE;";	
		           string Cloud_StagingConn = string.Empty;
                   //kkaur25 start-JIRA 10283 -to set staging node of connection string
                   if (p_objInputFile.ModuleName == "DIS" || p_objInputFile.ModuleName == "DDS")
                   Cloud_StagingConn = RMConfigurationManager.GetConnectionString("RMXStagingDataSource", base.ClientId);
                   else
                   Cloud_StagingConn = RMConfigurationManager.GetConnectionString("StagingDataSource", base.ClientId);
                   //kkaur25 end-JIRA 10283 -to set staging node of connection string
                   sTableName = "PMT_DOCUMENT_STORAGE";
                   GetData(sPMT, ref ObjDS, sTableName);
                  
                     if (ObjDS.Tables.Count > 0)      //kkaur25-UI JIRA 9651 end change for pmt table
                   {
                       // npadhy RMACLOUD-2418 - ISO Compatible with Cloud 
                       // In case of Cloud we need to save the uploaded file into DB
                       objDocumentManagementAdaptor = new DocumentManagementAdaptor();
                       objStreamedUploadDocumentType = new StreamedUploadDocumentType();
                       objStreamedUploadDocumentType.ClientId = base.ClientId;
                       objStreamedUploadDocumentType.FileName = p_objInputFile.fileName;
                       objStreamedUploadDocumentType.filePath = p_objInputFile.filePath;
                       objStreamedUploadDocumentType.FileContents = p_objInputFile.FileContents;
                       objStreamedUploadDocumentType.DocumentType = p_objInputFile.DocumentType;
                       objStreamedUploadDocumentType.ModuleName = p_objInputFile.ModuleName;
                       objStreamedUploadDocumentType.RelatedId = p_objInputFile.OptionsetId;

                       //kkaur25-UI JIRA 9651 start change for checking sequence in oracle
                       if (DbFactory.IsOracleDatabase(m_connectionString))
                       {
                           
                           
                           string sSeq = "SELECT count(*) FROM user_sequences WHERE sequence_name = 'SEQ_DOCUMENT_STORAGE_ROW_ID'";
                           sTableName = "SEQ_DOCUMENT_STORAGE_ROW_ID";
                           int ireturn = objDISettings.GetDataS(Cloud_StagingConn,sSeq, sTableName);
                           if (ireturn<1)
                           {
                               
                           throw new RMAppException("Cannot find " + sTableName);
            
                           }

                           
                       }
                       //kkaur25-UI JIRA 9651 end change for checking sequence in oracle
                       
                         // Call the documentmanagement Adaptor function to Insert file into DB
                        objDocumentManagementAdaptor.UploadFile(objStreamedUploadDocumentType, ref objErr, GetDAConnectionString(p_objInputFile.ModuleName));

                         }
                
               }

           }
           catch (Exception p_objException)
           {
               throw new RMAppException("DataIntegratorAdaptor.UploadImportFile.Error - " + p_objException.Message.ToString());
           }
           finally
           {
           }
        }


       
	
        //kkaur25-UI JIRA 9651 start function for checking pmt table

        public void GetData(string sSql, ref  DataSet dsPSOData, string tablename)
        {
            try
            {
                dsPSOData = new DataSet();
                string m_StagingDBSourceName = "StagingDataSource";
                string Cloud_StagingConn = string.Empty;
                Cloud_StagingConn = RMConfigurationManager.GetConnectionString(m_StagingDBSourceName, base.ClientId);
                DbConnection objConnection = DbFactory.GetDbConnection(Cloud_StagingConn);
                DbDataAdapter objDataAdaptar = DbFactory.GetDataAdapter(objConnection, sSql);
                objDataAdaptar.Fill(dsPSOData);
            }
            catch (Exception e)
            {
                throw new RMAppException("Cannot find " + tablename);
               
            }

        }

        //kkaur25-UI JIRA 9651 end function for checking pmt table

        /// <summary>
        /// Vsoni5 - MITS 23347, 23357, 23358,23364, 23365, 23439
        /// </summary>
        /// <param name="p_objDIModel">Object of DI Model</param>
        /// <param name="p_objException">Refrence to error adaptor object to pass on errors to Service layer</param>
        /// <returns></returns>
        /// 

        public void SaveSettingsCleanup(DataIntegratorModel p_objDIModel, ref BusinessAdaptorErrors p_objException)
        {
            //03/28/2011 npradeepshar MITS 24480 Changed code to log defects in case of error.
            
            try
            {
                Settings objDISettings = new Settings(base.ClientId);
                TaskManager objTaskManager = new TaskManager(userLogin, base.ClientId);//rkaur27
                DataIntegratorModel objoutModel = new DataIntegratorModel();

                string sTmp = string.Empty;

                objDISettings.connectionString = m_connectionString;
                objDISettings.DAConnectionString = GetDAConnectionString(p_objDIModel.ModuleName);   // added by csingh7 : DA phase 2

                objDISettings.moduleName = p_objDIModel.ModuleName;

                // Clean up Reserve Mapping for DDS
                if (String.Equals(objDISettings.moduleName, "DDS"))
                {
                    try
                    {
                        objDISettings.CleanUpReserveMappings(p_objDIModel.OptionSetID, objDISettings.DAConnectionString);
                    }
                    catch (Exception eCleanUpReserveMappings)
                    {
                        p_objException.Add(eCleanUpReserveMappings, eCleanUpReserveMappings.Message, BusinessAdaptorErrorType.Message);
                    }
                }

                //Vsoni5 : MITS 25119 : Modified condition to clean Access Staging database for MBR
                //Clean up Access Staging DB for DIS and DA MBR
                if (String.Equals(objDISettings.moduleName, "DIS") || String.Equals(objDISettings.moduleName, "MBR"))
                { //03/28/2011 npradeepshar MITS 24480 AccessStagingDataSource should be used for DIS only
                    try
                    {
                        string sAccessStagingDataSource = RMConfigurationManager.GetConfigConnectionString("AccessStagingDataSource", base.ClientId);
                        objDISettings.CleanUpMSAccessStaging(p_objDIModel.OptionSetID, sAccessStagingDataSource,p_objDIModel.Parms); //mkaur24 JIRA12427
                    }
                    catch (Exception eCleanUpMSAccessStaging)
                    {
                        p_objException.Add(eCleanUpMSAccessStaging, eCleanUpMSAccessStaging.Message, BusinessAdaptorErrorType.Message);
                    }
                }

                //Clean up TaskManager Entry
                try
                {
                    string sTaskManagerDataSource = RMConfigurationManager.GetConnectionString("TaskManagerDataSource", base.ClientId);
                    objDISettings.CleanUpTaskMgr(p_objDIModel.OptionSetID, sTaskManagerDataSource);
                }
                catch (Exception eCleanUpTaskMgr)
                {
                    p_objException.Add(eCleanUpTaskMgr, eCleanUpTaskMgr.Message, BusinessAdaptorErrorType.Message);
                }

                //Clean up Optionset Entry from DATA_INTEGRATOR table
                try
                {
                    objDISettings.CleanUpOptionset(p_objDIModel.OptionSetID, objDISettings.connectionString);
                }
                catch (Exception eCleanUpOptionset)
                {
                    p_objException.Add(eCleanUpOptionset, eCleanUpOptionset.Message, BusinessAdaptorErrorType.Message);
                }

                // Delete import file from "DataAnalytics" folder
                try
                {
                    CleanUpImportFile(p_objDIModel.OptionSetID, p_objDIModel.FilePath);
                }
                catch (Exception eCleanUpImportFile)
                {
                    p_objException.Add(eCleanUpImportFile, eCleanUpImportFile.Message, BusinessAdaptorErrorType.Message);
                }
            }
            catch (Exception eGeneral)
            {
                p_objException.Add(eGeneral, eGeneral.Message, BusinessAdaptorErrorType.Message);
            }

            if (p_objException.Count > 0)
            {
                throw new RMAppException("DataIntegratorAdaptor.SaveSettingsCleanup.Error");
            }
        }
        
        private void CleanUpImportFile(int iOptionsetId, string sDAFilePath)
        {
            string[] importFiles;
            if (Directory.Exists(sDAFilePath))
            {
                importFiles = Directory.GetFiles(sDAFilePath, "*." + iOptionsetId);
                foreach (string sImportFile in importFiles)
                {
                    File.Delete(sImportFile);
                }
            }
        }
        /// <summary>
        /// Vsoni5 : MITS 25785.
        /// </summary>
        /// <param name="sEntityList"> Comma seperated list of Payer Entity Ids selected on UI</param>
        /// <returns> This function return dataset containing TaxID, Abbr. and Last Name of payer</returns>
        public System.Data.DataSet GetTaxIds(string sEntityList)
        {
            Settings objDISettings = new Settings(base.ClientId);
            return objDISettings.GetTaxIds(m_connectionString, sEntityList);
        }

        //Developer - Mohd Ihtesham | Template - DA ISO OMI Enhancement | MITS 35711 | Start 
        public DataIntegratorModel GetrmASPRoleAndISOSPRole(DataIntegratorModel objDIModel)
        {
            Settings objDISettings = new Settings(base.ClientId);
            objDISettings.connectionString = m_connectionString;
            objDISettings.moduleName = objDIModel.ModuleName;
            objDIModel.dsrmASPRoleCodes = objDISettings.GetrmASPRoleAndISOSPRole();
            return objDIModel;
        }

        //function to save new service provider mappings
        public bool ISOSaveSPRoleMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISettings = new Settings(base.ClientId); //Application layer component			
            try
            {
                objDISettings.connectionString = m_connectionString;
                p_objXmlOut = objDISettings.ISOSaveSPRoleMapping(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDISettings = null;
            }
        }

        //function to display existing service provider mapping in grid
        public bool ISOGetSPRoleType(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISettings = new Settings(base.ClientId);
            try
            {
                objDISettings.connectionString = m_connectionString;


                p_objXmlOut = objDISettings.ISOGetSPRoleType(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDISettings = null;
            }
        }

        //function to delete existing service provider mapping from grid
        public bool ISODeleteSPRoleMapping(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Settings objDISettings = new Settings(base.ClientId);
            try
            {
                objDISettings.connectionString = m_connectionString;
                p_objXmlOut = objDISettings.ISODeleteSPRoleMapping(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDISettings = null;
            }
        }

        //Developer - Mohd Ihtesham | Template - DA ISO OMI Enhancement | MITS 35711 | End


       
        
    }
        
    
        #endregion

    }


