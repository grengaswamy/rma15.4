﻿using System;
using System.Xml;
using System.Collections.Generic;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.BenefitCalculator;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// Adaptor class to transform the Benefit Calculator information
    /// from the Application layer so that it can be consumed by the
    /// WebService and UI
    /// </summary>
    public class BenefitCalculatorAdaptor: BusinessAdaptorBase
    {
        const int DW_FLAGS                      = 0;
        const string DATE_FORMAT                = "yyyyMMdd";
        const string STD_DATE_FORMAT            = "MM/dd/yyyy";
        private string m_strDBConnectionString  = String.Empty;
        private string m_strPassword            = String.Empty;
        private string m_strLoginName           = String.Empty;
        private string m_strDSN                 = String.Empty;

        /// <summary>
        /// DEFAULT Class Constructor
        /// </summary>
        public BenefitCalculatorAdaptor()
        {
            
        }//default constructor

        /// <summary>
        /// OVERLOADED: class constructor
        /// </summary>
        public BenefitCalculatorAdaptor(string strLoginName, string strPassword, string strDSN)
        {
            this.m_strLoginName = strLoginName;
            this.m_strPassword  = strPassword;
            this.m_strDSN       = strDSN;
        }//overloaded constructor

        /// <summary>
        /// Initializes all values needed for the Application layer
        /// </summary>
        private void Initialize()
        {
            m_strDBConnectionString    = base.connectionString;
            m_strPassword              = base.userLogin.Password;
            m_strLoginName             = base.loginName;
            m_strDSN                   = base.userLogin.objRiskmasterDatabase.DataSourceName;
        }//method: Initialize()

        /// <summary>
        ///In order to fetch the details to the screen
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            BenefitCalcData objBenefitCalcData = null;
           
            //Initialize the required values needed to be used by the BusinessAdaptor
            Initialize();

            try
            {
                //Call the function to obtain the Claim ID
                int intClaimID = GetClaimID(p_objXmlIn);

                objBenefitCalcData = PopulateBenefitCalcData(intClaimID);

                p_objXmlOut = GetXmlOutput(p_objXmlIn.OuterXml, objBenefitCalcData);

                //Check if a warning has been returned from the RMWCCalc object
                if (! string.IsNullOrEmpty(objBenefitCalcData.Warning))
                {
                    //.Warning functionality is not passed from the instance.xml & view.xsl to the UI
                    //p_objErrOut.Add("Error from RMWCCalc object", objBenefitCalcData.Warning, BusinessAdaptorErrorType.Warning);

                    //Add the warning message to the BusinessAdaptor Errors collection, pass as an .Error for now
                    p_objErrOut.Add("Warning from RMWCCalc object", objBenefitCalcData.Warning, BusinessAdaptorErrorType.Warning);
                }

                return true;
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                // p_objErrOut.Add(p_objException, Globalization.GetString("BenefitCalcAdaptor.Get.Error"), BusinessAdaptorErrorType.Error);
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objBenefitCalcData = null;
            }
        }

        /// <summary>
        /// saving
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            //Application layer components
            BenefitCalc objBenefitCalc = null;
            BenefitCalcData objBenefitCalcData = null;

            Initialize();

            try
            {
                // TODO
                objBenefitCalcData = PopulateBenefitCalcData(p_objXmlIn.OuterXml);

                int intClaimID = GetClaimID(p_objXmlIn);

                //Create the new Benefit Calculator object
                objBenefitCalc = new BenefitCalc(m_strLoginName, m_strPassword, m_strDSN, intClaimID,base.ClientId);

                //Call the save on the Benefit Calculator Application Library
                objBenefitCalc.Save(ref objBenefitCalcData);

                //Update the Xml Output with the values from the Benefit Calculator object
                p_objXmlOut = GetXmlOutput(p_objXmlIn.OuterXml, objBenefitCalcData);
                return true;
            }//try
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }//catch
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }//catch
            finally
            {
                objBenefitCalc = null;
                objBenefitCalcData = null;
            }//finally
        }

        /// <summary>
        /// Gets the Claim ID from the XML Document
        /// </summary>
        /// <param name="objXmlDocument"></param>
        /// <returns></returns>
        public int GetClaimID(XmlDocument objXmlDocument)
        {
            int intClaimID = 0;
            intClaimID     = Conversion.ConvertStrToInteger(objXmlDocument.SelectSingleNode("//ClaimID").InnerText);
            return intClaimID;
        }//method: GetClaimID()

        /// <summary>
        /// Populates the BenefitCalcData object with data based off of a Claim ID
        /// </summary>
        /// <param name="intClaimID"></param>
        /// <returns></returns>
        public BenefitCalcData PopulateBenefitCalcData(int intClaimID)
        {
            //Application layer components
            BenefitCalc objBenefitCalc         = null;
            BenefitCalcData objBenefitCalcData = null;

            //Create the new Benefit Calculator object
            objBenefitCalc = new BenefitCalc(m_strLoginName, m_strPassword, m_strDSN, intClaimID,base.ClientId);

            //Call the Basic Rate function which returns a Benefit Calculator Data object
            objBenefitCalcData = objBenefitCalc.GetBasicRate();

            //Call the Override Rate function to populate the Override Rate values
            objBenefitCalc.GetOverrideRate(ref objBenefitCalcData);

            return objBenefitCalcData;
        }//method: PopulateBenefitCalcData(int)

        /// <summary>
        /// Populates the BenefitCalcData from the Xml Document
        /// </summary>
        /// <param name="strXmlDoc">string containing the OuterXml from the original XmlDocument</param>
        /// <returns></returns>
        public BenefitCalcData PopulateBenefitCalcData(string strXmlDoc)
        {
            bool blnDateParsed                  = false;
            DateTime dtDateParsed               = DateTime.Now;

            XmlDocument objXmlDocument          = new XmlDocument();
            BenefitCalcData objBenefitCalcData  = new BenefitCalcData();

            //Load the specified Xml
            objXmlDocument.LoadXml(strXmlDoc);

            //Populate nodes with data retrieved from the BenefitCalcData object
            //Parse all integers
            objBenefitCalcData.WaitingPeriodDays              = ParseXmlNode(objXmlDocument.SelectSingleNode("//WaitingPeriod").InnerText);
            objBenefitCalcData.RetroactivePeriodDays          = ParseXmlNode(objXmlDocument.SelectSingleNode("//RetroactivePeriod").InnerText);
            objBenefitCalcData.WaitingPeriodSatisfiedCode     = ParseXmlNode(objXmlDocument.SelectSingleNode("//WaitSatisfied").InnerText);
            objBenefitCalcData.RetroactivePerSatisfiedCode    = ParseXmlNode(objXmlDocument.SelectSingleNode("//RetroSatisfied").InnerText);

            //Parse all doubles
            objBenefitCalcData.CurrentMMIDisability           = ParseXmlNodeDouble(objXmlDocument.SelectSingleNode("//CurrentDisabilityPercent").InnerText);
            objBenefitCalcData.AverageWage                    = ParseXmlNodeDouble(objXmlDocument.SelectSingleNode("//AvgWeeklyWage").InnerText.Replace("$","").Replace(",","").Trim());
            objBenefitCalcData.TTRate                         = ParseXmlNodeDouble(objXmlDocument.SelectSingleNode("//JurisRateTT").InnerText);
            objBenefitCalcData.PPRate                         = ParseXmlNodeDouble(objXmlDocument.SelectSingleNode("//JurisRatePP").InnerText);
            objBenefitCalcData.PTRate                         = ParseXmlNodeDouble(objXmlDocument.SelectSingleNode("//JurisRatePT").InnerText);
            //Update the override rate
            objBenefitCalcData.PPOverrideRate                 = ParseXmlNodeDouble(objXmlDocument.SelectSingleNode("//OverRatePP").InnerText);
            objBenefitCalcData.PTOverrideRate                 = ParseXmlNodeDouble(objXmlDocument.SelectSingleNode("//OverRatePT").InnerText);
            objBenefitCalcData.TTOverrideRate                 = ParseXmlNodeDouble(objXmlDocument.SelectSingleNode("//OverRateTT").InnerText);

            //Assign all string values
            objBenefitCalcData.TTAbbr                         = objXmlDocument.SelectSingleNode("//JurisAbbrTT").InnerText;
            objBenefitCalcData.PPAbbr                         = objXmlDocument.SelectSingleNode("//JurisAbbrPP").InnerText;
            objBenefitCalcData.PTAbbr                         = objXmlDocument.SelectSingleNode("//JurisAbbrPT").InnerText;

            if (! String.IsNullOrEmpty(objXmlDocument.SelectSingleNode("//CurrentMMIDate").InnerText))
            {
                blnDateParsed = ParseDate(objXmlDocument.SelectSingleNode("//CurrentMMIDate").InnerText, DATE_FORMAT, out dtDateParsed);

                if (blnDateParsed)
                {
                    objBenefitCalcData.CurrentMMIDateDBFormat = FormatDateTime(dtDateParsed, STD_DATE_FORMAT);
                }
            }

            if (! String.IsNullOrEmpty(objXmlDocument.SelectSingleNode("//AccidentDate").InnerText))
            {
                blnDateParsed = ParseDate(objXmlDocument.SelectSingleNode("//AccidentDate").InnerText, DATE_FORMAT, out dtDateParsed);

                if (blnDateParsed)
                {
                    objBenefitCalcData.EventDateDBFormat = FormatDateTime(dtDateParsed, STD_DATE_FORMAT);
                }
            }

            if (!String.IsNullOrEmpty(objXmlDocument.SelectSingleNode("//LastWorkWeekDate").InnerText))
            {
                blnDateParsed = ParseDate(objXmlDocument.SelectSingleNode("//LastWorkWeekDate").InnerText, DATE_FORMAT, out dtDateParsed);

                if (blnDateParsed)
                {
                    objBenefitCalcData.OriginalDisabilityDateDBFormat = FormatDateTime(dtDateParsed, STD_DATE_FORMAT);
                }
            }

            if (! String.IsNullOrEmpty(objXmlDocument.SelectSingleNode("//CurrentDisabilityDateDBFormat").InnerText))
            {
                blnDateParsed = ParseDate(objXmlDocument.SelectSingleNode("//CurrentDisabilityDateDBFormat").InnerText, DATE_FORMAT, out dtDateParsed);

                if (blnDateParsed)
                {
                    objBenefitCalcData.CurrentDisabilityDateDBFormat = FormatDateTime(dtDateParsed, STD_DATE_FORMAT);
                }
            }

            if (! String.IsNullOrEmpty(objXmlDocument.SelectSingleNode("//MostRecentRTW").InnerText))
            {
                blnDateParsed = ParseDate(objXmlDocument.SelectSingleNode("//MostRecentRTW").InnerText, DATE_FORMAT, out dtDateParsed);

                if (blnDateParsed)
                {
                    objBenefitCalcData.MostRecentRetToWkDateDBFormat = FormatDateTime(dtDateParsed, STD_DATE_FORMAT);
                }
            }

            //Clean up
            objXmlDocument = null;

            return objBenefitCalcData;
        }//method: PopulatebenefitCalcData(string)

        /// <summary>
        /// Validates the incoming Xml from the Web Service
        /// </summary>
        public XmlDocument ValidateXmlIn()
        {
            XmlDocument objXmlDocument = new XmlDocument();
            XmlElement objRoot, objChildNode;

            List<string> arrElements = new List<string>();
            arrElements.Add("WaitingPeriod");
            arrElements.Add("CurrentMMIDate");
            arrElements.Add("AvgWeeklyWage");
            arrElements.Add("RetroactivePeriod");
            arrElements.Add("CurrentDisabilityPercent");
            arrElements.Add("AccidentDate");
            arrElements.Add("LastWorkWeekDate");
            arrElements.Add("CurrentDisabilityDateDBFormat");
            arrElements.Add("MostRecentRTW");
            arrElements.Add("WaitSatisfied");
            arrElements.Add("RetroSatisfied");
            arrElements.Add("JurisAbbrTT");
            arrElements.Add("JurisAbbrPP");
            arrElements.Add("JurisAbbrPT");
            arrElements.Add("JurisRateTT");
            arrElements.Add("JurisRatePP");
            arrElements.Add("JurisRatePT");
            arrElements.Add("OverRateTT");
            arrElements.Add("OverRatePP");
            arrElements.Add("OverRatePT");
            //arrElements.Add("Warning");

            objRoot = objXmlDocument.CreateElement("BenefitCalculator");
            objXmlDocument.AppendChild(objRoot);

            foreach (string strElementName in arrElements)
            {
                //repeat this code over and over for new elements
                objChildNode = objXmlDocument.CreateElement(strElementName);
                objRoot.AppendChild(objChildNode);
            }//foreach

            return objXmlDocument;
        }//method: ValidateXmlIn()

        /// <summary>
        /// Parses the value of a specified Xml Node and
        /// returns the appropriate value
        /// </summary>
        /// <param name="strNodeValue">string containing the value to be parsed of the Xml Node</param>
        /// <returns>integer value containing the result of the Parsed Xml Node</returns>
        private int ParseXmlNode(string strNodeValue)
        {
            int intReturnValue = 0;
            bool blnParse      = false;

            blnParse = Int32.TryParse(strNodeValue, out intReturnValue);

            return intReturnValue;
        }//method: ParseXmlNode()

        /// <summary>
        /// Parses the value of a specified Xml Node and
        /// returns the appropriate value
        /// </summary>
        /// <param name="strNodeValue">string containing the value to be parsed of the Xml Node</param>
        /// <returns>double value containing the result of the Parsed Xml Node</returns>
        private double ParseXmlNodeDouble(string strNodeValue)
        {
            double dblReturnValue = 0.0;
            bool blnParse         = false;

            blnParse = Double.TryParse(strNodeValue, out dblReturnValue);

            return dblReturnValue;
        }//method: ParseXmlNodeDouble()

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strDateValue"></param>
        /// <param name="strDateFormat"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        private bool ParseDate(string strDateValue, string strDateFormat, out DateTime dtDateParsed)
        {
            bool blnDateParsed = false;

            blnDateParsed = DateTime.TryParseExact(strDateValue, strDateFormat, null, System.Globalization.DateTimeStyles.NoCurrentDateDefault, out dtDateParsed);

            return blnDateParsed;
        }//method: ParseDate()

        /// <summary>
        /// This will format the DateTime to the appropriate Riskmaster standard format
        /// </summary>
        /// <param name="dtDateParsed">datetime to be formatted</param>
        /// <param name="strStandardFormat">date format</param>
        /// <returns></returns>
        private string FormatDateTime(DateTime dtDateParsed, string strStandardFormat)
        {
            return dtDateParsed.ToString(strStandardFormat);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strXmlDoc"></param>
        /// <param name="objBenefitCalcData"></param>
        /// <returns></returns>
        public XmlDocument GetXmlOutput(string strXmlDoc, BenefitCalcData objBenefitCalcData)
        {
            const string CURRENCY = "C";
            bool blnDateParsed    = false;
            DateTime dtDateParsed = DateTime.Now;

            try
            {
                #region Dummy Test region
                //p_objXmlDocument.SelectSingleNode("//WaitingPeriod").InnerText                 = "7";
                //p_objXmlDocument.SelectSingleNode("//CurrentMMIDate").InnerText                = "12/7/2008";
                //p_objXmlDocument.SelectSingleNode("//AvgWeeklyWage").InnerText                 = "2500";
                //p_objXmlDocument.SelectSingleNode("//WaitingPeriodDays").InnerText             = "14";
                //p_objXmlDocument.SelectSingleNode("//CurrentDisabilityPercent").InnerText      = "80";
                //p_objXmlDocument.SelectSingleNode("//AccidentDate").InnerText                  = "12/3/2002";
                //p_objXmlDocument.SelectSingleNode("//LastWorkWeekDate").InnerText              = "4";
                //p_objXmlDocument.SelectSingleNode("//CurrentDisabilityDateDBFormat").InnerText = "12/4/2005";
                //p_objXmlDocument.SelectSingleNode("//EventDateDBFormat").InnerText;
                //p_objXmlDocument.SelectSingleNode("//WaitSatisfied").InnerText                 = "true";
                //p_objXmlDocument.SelectSingleNode("//RetroSatisfied").InnerText;
                #endregion

                XmlDocument objXmlDocument = new XmlDocument();

                objXmlDocument.LoadXml(strXmlDoc);

                //Populate nodes with data retrieved from the BenefitCalcData object
                objXmlDocument.SelectSingleNode("//WaitingPeriod").InnerText = objBenefitCalcData.WaitingPeriodDays.ToString();

                if (!String.IsNullOrEmpty(objBenefitCalcData.CurrentMMIDateDBFormat))
                {
                    blnDateParsed = ParseDate(objBenefitCalcData.CurrentMMIDateDBFormat, DATE_FORMAT, out dtDateParsed);

                    if (blnDateParsed)
                    {
                        objXmlDocument.SelectSingleNode("//CurrentMMIDate").InnerText = FormatDateTime(dtDateParsed, STD_DATE_FORMAT);
                    }
                }

                objXmlDocument.SelectSingleNode("//AvgWeeklyWage").InnerText            = objBenefitCalcData.AverageWage.ToString(CURRENCY);
                objXmlDocument.SelectSingleNode("//RetroactivePeriod").InnerText        = objBenefitCalcData.RetroactivePeriodDays.ToString();
                objXmlDocument.SelectSingleNode("//CurrentDisabilityPercent").InnerText = objBenefitCalcData.CurrentMMIDisability.ToString();

                if (!String.IsNullOrEmpty(objBenefitCalcData.EventDateDBFormat))
                {
                    blnDateParsed = ParseDate(objBenefitCalcData.EventDateDBFormat, DATE_FORMAT, out dtDateParsed);

                    if (blnDateParsed)
                    {
                        objXmlDocument.SelectSingleNode("//AccidentDate").InnerText = FormatDateTime(dtDateParsed, STD_DATE_FORMAT);
                    }
                }

                if (!String.IsNullOrEmpty(objBenefitCalcData.OriginalDisabilityDateDBFormat))
                {
                    blnDateParsed = ParseDate(objBenefitCalcData.OriginalDisabilityDateDBFormat, DATE_FORMAT, out dtDateParsed);

                    if (blnDateParsed)
                    {
                        objXmlDocument.SelectSingleNode("//LastWorkWeekDate").InnerText = FormatDateTime(dtDateParsed, STD_DATE_FORMAT);
                    }
                }

                if (!String.IsNullOrEmpty(objBenefitCalcData.CurrentDisabilityDateDBFormat))
                {
                    blnDateParsed = ParseDate(objBenefitCalcData.CurrentDisabilityDateDBFormat, DATE_FORMAT, out dtDateParsed);

                    if (blnDateParsed)
                    {
                        objXmlDocument.SelectSingleNode("//CurrentDisabilityDateDBFormat").InnerText = FormatDateTime(dtDateParsed, STD_DATE_FORMAT);
                    }
                }

                if (!String.IsNullOrEmpty(objBenefitCalcData.MostRecentRetToWkDateDBFormat))
                {
                    blnDateParsed = ParseDate(objBenefitCalcData.MostRecentRetToWkDateDBFormat, DATE_FORMAT, out dtDateParsed);

                    if (blnDateParsed)
                    {
                        objXmlDocument.SelectSingleNode("//MostRecentRTW").InnerText = FormatDateTime(dtDateParsed, STD_DATE_FORMAT);
                    }
                }

                if (objBenefitCalcData.WaitingPeriodSatisfiedCode == objBenefitCalcData.YesCodeID)
                {
                    objXmlDocument.SelectSingleNode("//WaitSatisfied").InnerText = "true";
                }
                else
                {
                    objXmlDocument.SelectSingleNode("//WaitSatisfied").InnerText = "false";
                }

                if (objBenefitCalcData.RetroactivePerSatisfiedCode == objBenefitCalcData.YesCodeID)
                {
                    objXmlDocument.SelectSingleNode("//RetroSatisfied").InnerText = "true";
                }
                else
                {
                    objXmlDocument.SelectSingleNode("//RetroSatisfied").InnerText = "false";
                }

                //Query the Xml to populate the BenefitCalcData object
                objXmlDocument.SelectSingleNode("//JurisAbbrTT").InnerText = objBenefitCalcData.TTAbbr;
                objXmlDocument.SelectSingleNode("//JurisAbbrPP").InnerText = objBenefitCalcData.PPAbbr;
                objXmlDocument.SelectSingleNode("//JurisAbbrPT").InnerText = objBenefitCalcData.PTAbbr;
                objXmlDocument.SelectSingleNode("//JurisRateTT").InnerText = objBenefitCalcData.TTRate.ToString();
                objXmlDocument.SelectSingleNode("//JurisRatePP").InnerText = objBenefitCalcData.PPRate.ToString();
                objXmlDocument.SelectSingleNode("//JurisRatePT").InnerText = objBenefitCalcData.PTRate.ToString();

                //Update the OverRate in the Xml Document
                objXmlDocument.SelectSingleNode("//OverRatePP").InnerText = objBenefitCalcData.PPOverrideRate.ToString();
                objXmlDocument.SelectSingleNode("//OverRatePT").InnerText = objBenefitCalcData.PTOverrideRate.ToString();
                objXmlDocument.SelectSingleNode("//OverRateTT").InnerText = objBenefitCalcData.TTOverrideRate.ToString();

                return objXmlDocument;
            }//try
            catch (Exception objException)
            {
                throw objException;
            }//catch
        }//method: GetXmlOutput()
    }
}