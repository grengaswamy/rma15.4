using System;
using System.Reflection;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class FormViewAdaptor: BusinessAdaptorBase
	{


		private XmlDocument m_req = null;
		private XmlDocument m_resp = null;
		internal BusinessAdaptorErrors m_err = null;
		#region BSB Standard  XML Parameter Parsing Routines
		static XmlNamespaceManager _nsmgr =null;
		static XmlNameTable _nt = null;
		static FormViewAdaptor()
		{
			_nt = new NameTable();
			_nsmgr = new XmlNamespaceManager(_nt);
			_nsmgr.AddNamespace("m",AppConstants.APP_NAMESPACE);
			_nsmgr.AddNamespace("soap",@"http://schemas.xmlsoap.org/soap/envelope/");
		}
		internal bool HasParam(string paramName){return(SafeParam(paramName) !=null);}
		internal XmlElement SafeParam(string paramName)
		{
			return (SafeXPath(String.Format("ParamList/Param[@name='{0}']",paramName)) as XmlElement);
		}		
		internal string SafeParamText(string paramName)
		{
			try{return (SafeParam(paramName) as XmlElement).InnerText;}
			catch{return "";}																																
		}
		
		internal XmlNode SafeXPath(string xPath)
		{
			try
			{
				// JP TMP 01.18.2006   return m_req.SelectSingleNode(xPath, _nsmgr);
				return m_req.SelectSingleNode(xPath);
			}
			catch
			{
				return null;
			}
		}
		internal bool SafeSaveParamText(string paramName, string paramValue)
		{
			try{ (SafeParam(paramName) as XmlElement).InnerText = paramValue;return true;}
			catch{return false;}																																
		}
		internal bool SafeSaveParamXml(string paramName, string paramValue)
		{
			try{ (SafeParam(paramName) as XmlElement).InnerXml = paramValue.Replace("&","&amp;");return true;}
			catch{return false;}																																
		}
		static internal string CData(string s) {return String.Format("<![CDATA[{0}]]>",s);}
		#endregion

		public FormViewAdaptor()
		{
			m_resp = new XmlDocument(_nt);
            m_err = new BusinessAdaptorErrors(base.ClientId);
		}
		
		/// <summary>
		/// Fetch the view xml for the requested screen name.  In special cases for the claim screen
		/// a decision may be made based on the security id you're coming from or the recordid to pick
		/// the most appropriate line of business claim screen.
		/// </summary>
		/// <param name="req"></param>
		/// <returns></returns>
		public bool FetchView(XmlDocument xmlIn, ref XmlDocument xmlResponse,ref BusinessAdaptorErrors  errOut)
		{
			m_req = xmlIn;
			errOut = m_err;
			
			//Extract Parameters from Message
			string sSysSid = SafeParamText("SysSid");
			int prevSid = Conversion.ConvertStrToInteger(sSysSid);
			
			string sSysViewId = SafeParamText("SysViewId");
			int ViewId = Conversion.ConvertStrToInteger(sSysViewId);
			
			string sSysRecordId = SafeParamText("SysFormId");
			int RecordId = Conversion.ConvertStrToInteger(sSysRecordId);

			FormViewManager mgr = new FormViewManager(base.userLogin, m_err, base.ClientId);

			string sFormName = mgr.FormNameToSpecialized(SafeParamText("SysFormName"),prevSid); 

			this.SafeSaveParamText("SysFormName",sFormName);
			(SafeParam("SysViewStore") as XmlElement).InnerXml = mgr.FetchView(prevSid,ViewId,RecordId,sFormName);

			m_resp = m_req;
			xmlResponse = m_resp;
			
			return (this.m_err.Count ==0); 		

		}
	}

}
