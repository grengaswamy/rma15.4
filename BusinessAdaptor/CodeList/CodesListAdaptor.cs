using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Application.CodesList;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using System.IO;
using Riskmaster.Security;
using Riskmaster.Models;
using System.Collections.Generic;
namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: CodesListAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 13-May-2005
	///* $Author	: Nikhil Kr. Garg
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for CodesList which 
	///	fetch the list of codes for different Entities and Tables.
	///	It caters to two functionalities: "getcode" and "quicklookup"
	/// </summary>
	public class CodesListAdaptor: BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Constructor for the class.
		/// </summary>
		public CodesListAdaptor()
		{
		}
		#endregion

		#region Public Methods

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.CodesList.CodesListManager.GetCodes() method.
		///		Gets the codes list for the entity according to page number.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<CodesList>
		///				<DeptEId>Department Id</DeptEId>
		///				<TableName>Table Name for the Codes</TableName>
		///				<LOB>Line Of Business</LOB>
		///				<TypeLimits>Limits for Claim_Types or Event_Types</TypeLimits>
		///				<FormName>FormName on which the code is required</FormName>
		///				<TriggerDate>Date for filtering</TriggerDate>
		///				<SessionDsnId>DSN Id stored in the session</SessionDsnId>
		///				<SessionLOB>LOB stored in the session</SessionLOB>
		///				<RecordCount>Record Count</RecordCount>
		///				<PageNumber>Current Page Number</PageNumber>
		///			</CodesList>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<codes	recordcount="" pagecount="" tablename="" triggerdate="" FormName="" previouspage="" 
		///				firstpage="" nextpage="" lastpage="" thispage="" pagecount="">
		///			<code id="" shortcode="" parentcode=""></code>
		///		</codes>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool GetCodes(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			CodesListManager objCodeList=null;		//Application layer component
			long lDeptEId;							//Department Id
			string sTableName;						//Table Name for the Codes
			string sLOB;							//Line Of Business
			string sTypeLimits;						//Limits for Claim_Types or Event_Types
			string sFormName;						//FormName on which the code is required
			string sTriggerDate;					//Date for filtering
            string sEventDate = "";                 //needed for trigger date when form is a claim
            string sSessionDsnId;					//DSN Id stored in the session
			string sSessionLOB;						//LOB stored in the session
            string sSessionClaimId;                //ClaimId stored in the session  : Umesh
			long lRecordCount;						//Record Count
			long lPageNumber;						//Current Page Number
            string sFilter = "";                    //Additional Filter for Custom Code Lookup
			XmlElement objElement=null;				//used for parsing the input xml
            //string sTitle = "";                     //pmittal5  MITS:12318  06/13/08      
			try
			{
				//check existence of Department Id which is required
				
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//DeptEId");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				lDeptEId=Conversion.ConvertStrToLong(objElement.InnerText);

				//check existence of Table Name which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TableName");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sTableName=objElement.InnerText;
				
				//check existence of LOB which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//LOB");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sLOB=objElement.InnerText;

				//check existence of Type Limits which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TypeLimits");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sTypeLimits=objElement.InnerText;

				//check existence of Form Name which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FormName");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sFormName=objElement.InnerText;

				//check existence of Trigger Date which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TriggerDate");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sTriggerDate=objElement.InnerText;

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//EventDate");
                if (objElement != null)
                {
                    sEventDate = objElement.InnerText;
                }

				//Get Session Id from Session for the security database
				sSessionDsnId=DSNID.ToString();

				//check existence of Session LOB which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//SessionLOB");
				if (objElement==null)
				{
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sSessionLOB=objElement.InnerText;

                //check existence of Session ClaimId which is required : Umesh
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//SessionClaimId");
                if (objElement == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sSessionClaimId = objElement.InnerText;
				//check existence of Record Count which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//RecordCount");
				if (objElement==null)
				{
					p_objErrOut.Add("CodesListAdaptorParameterMissing",Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				lRecordCount=Conversion.ConvertStrToLong(objElement.InnerText);

				//check existence of Page Number which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//PageNumber");
				if (objElement==null)
				{
					p_objErrOut.Add("CodesListAdaptorParameterMissing",Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				lPageNumber=Conversion.ConvertStrToLong(objElement.InnerText);

                //check for additional filter. MCIC Changes 01/24/08
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//filter");
                if (objElement != null)
                {
                    sFilter = objElement.InnerText;
                }                

                ////pmittal5  MITS:12318  06/13/08  -Passing Title to separate "Search" from other screens in case of Code lookup
                //objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//Title");
                //if (objElement != null)
                //{
                //    sTitle = objElement.InnerText;
                //} 

				objCodeList = new CodesListManager(base.m_connectionString,base.ClientId);

				//get codes list            
                p_objXmlOut = objCodeList.GetCodes(lDeptEId, sTableName.Replace("'", "''"), sLOB, sTypeLimits, sFormName,
                                                 sTriggerDate, sEventDate, sSessionDsnId, sSessionLOB, sSessionClaimId, lRecordCount,
                                                 lPageNumber, sFilter, "1", "Asc");//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17 
                //pmittal5  MITS:12318  06/13/08
                //p_objXmlOut = objCodeList.GetCodes(lDeptEId, sTableName.Replace("'", "''"), sLOB, sTypeLimits, sFormName,
                //                                 sTriggerDate, sEventDate, sSessionDsnId, sSessionLOB, sSessionClaimId, lRecordCount,
                //                                 lPageNumber, sFilter,sTitle);

				
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("CodesListAdaptor.GetCodes.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objCodeList = null;
				objElement=null;
			}
		}
        public bool GetCodesNew(CodeTypeRequest p_objRequest, ref CodeListType objCodes, ref BusinessAdaptorErrors p_objErrOut)
        {
            CodesListManager objCodeList = null;		//Application layer component
            long lDeptEId;							//Department Id
            string sTableName;						//Table Name for the Codes
            string sLOB;							//Line Of Business
            string sTypeLimits;						//Limits for Claim_Types or Event_Types
            string sFormName;						//FormName on which the code is required
            string sTriggerDate;					//Date for filtering
            string sEventDate = "";                 //needed for trigger date when form is a claim
            string sSessionDsnId;					//DSN Id stored in the session
            string sSessionLOB;						//LOB stored in the session
            string sSessionClaimId;                //ClaimId stored in the session  : Umesh
            long lRecordCount;						//Record Count
            long lPageNumber;						//Current Page Number
            string sFilter = "";                    //Additional Filter for Custom Code Lookup
            string sTitle = "";                     //pmittal5  MITS:12318  06/13/08      
            string sInsuredEid = "";                //PJS MITS 15220
            string sEventId = "";
            string sParentCodeID = string.Empty;    //Code id of the parent--stara 05/18/09
            string sPolicyId = string.Empty;
            string sCovTypeCodeId = string.Empty;
            string sLossTypeCodeId = string.Empty;       //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
            string sTransId = string.Empty;
            string sClamantEid = string.Empty;
            //rupal:start for first & final payment
            string sIsFirstFinal = string.Empty;            
            //rupal:end for first & final payment
            string sPayCol = string.Empty;//AA            
            string sCovgSeqNum = string.Empty;//rupal:policy system interface
            string sPolUnitRowId = string.Empty;
            string sRcRowId = string.Empty;
            objCodes = new CodeListType();
            string sPolicyLOB = string.Empty;
            string sJurisdiction = string.Empty;    //added by swati for MITS # 36929
            string sTransSeqNum = string.Empty;
            string sCoverageKey = string.Empty;     //Ankit Start : Worked on MITS - 34297
			
			string sFieldName = string.Empty; //igupta3 Ensuring field is supp or not
            string sSortColumn = string.Empty;//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17 
            string sSortOrder = string.Empty;//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17 
            
            try
            {
                if (p_objRequest.SortColumn == null)//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17 
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sSortColumn = p_objRequest.SortColumn;//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17 
                if (p_objRequest.SortOrder == null)//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17 
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sSortOrder = p_objRequest.SortOrder;//MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17 

                if (p_objRequest.DeptEId == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                lDeptEId = Conversion.ConvertStrToLong(p_objRequest.DeptEId);

                //check existence of Table Name which is required
                if (p_objRequest.TableName == null || p_objRequest.TableName == "")
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sTableName = p_objRequest.TableName;

                //check existence of LOB which is required
                if (p_objRequest.LOB == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sLOB = p_objRequest.LOB;

                //check existence of Type Limits which is required
                if (p_objRequest.TypeLimits == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sTypeLimits = p_objRequest.TypeLimits;

                //check existence of Form Name which is required

                if (p_objRequest.FormName == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sFormName = p_objRequest.FormName;

                //check existence of Trigger Date which is required

                if (p_objRequest.TriggerDate == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sTriggerDate = p_objRequest.TriggerDate;


                if (p_objRequest.EventDate != null && p_objRequest.EventDate != "")
                {
                    sEventDate = p_objRequest.EventDate;
                }

                //Get Session Id from Session for the security database
                sSessionDsnId = DSNID.ToString();

                //check existence of Session LOB which is required

                if (p_objRequest.SessionLOB == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sSessionLOB = p_objRequest.SessionLOB;

                //check existence of Session ClaimId which is required : Umesh

                if (p_objRequest.SessionClaimId == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sSessionClaimId = p_objRequest.SessionClaimId;
                //check existence of Record Count which is required

                if (p_objRequest.RecordCount == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                lRecordCount = Conversion.ConvertStrToLong(p_objRequest.RecordCount);

                //check existence of Page Number which is required

                if (p_objRequest.PageNumber == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }

                lPageNumber = Conversion.ConvertStrToLong(p_objRequest.PageNumber);

                //check for additional filter. MCIC Changes 01/24/08
                if (p_objRequest.Filter != "" || p_objRequest.Filter != null)
                    sFilter = p_objRequest.Filter; //MGaba2:R6:Adding filter property to base

                //if (p_objRequest.Filter == "" || p_objRequest.Filter == null)
                //sFilter = "";
                
                    

                objCodeList = new CodesListManager(base.m_connectionString,base.ClientId);

                //pmittal5  MITS:12318  06/13/08  -Passing Title to separate "Search" from other screens in case of Code lookup
                if (p_objRequest.Title == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sTitle = p_objRequest.Title;

                // PJS MITS 15220 -check existence of Insured Eid which is required
                sParentCodeID = p_objRequest.ParentCodeID;//--stara Mits 16667 05/18/09
               
                if (p_objRequest.InsuredEid == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sInsuredEid = p_objRequest.InsuredEid;

                if (p_objRequest.EventId == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sEventId = p_objRequest.EventId;

                if (p_objRequest.PolicyId != null)
                {
                    sPolicyId = p_objRequest.PolicyId;
                }

                if (p_objRequest.CovTypeCodeId != null)
                {
                    sCovTypeCodeId = p_objRequest.CovTypeCodeId;
                }
                //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
                if (p_objRequest.CovTypeCodeId != null)
                    sLossTypeCodeId = p_objRequest.LossTypeCodeId;
                //Ankit End
                if (p_objRequest.TransId != null)
                {
                    sTransId = p_objRequest.TransId;
                }
                if (p_objRequest.ClaimantEid != null)
                {
                    sClamantEid = p_objRequest.ClaimantEid;
                }

                //added by swati for AIC Gap 7 MITS # 36929
                if (p_objRequest.Jurisdiction != null)
                {
                    sJurisdiction = p_objRequest.Jurisdiction;
                }
                //change end here by swati
                //rupal:start for first & final payment
                if (p_objRequest.IsFFPmt != null)
                {
                    sIsFirstFinal = p_objRequest.IsFFPmt;
                }
                //AA
                if (p_objRequest.sPayCol != null)
                {
                    sPayCol = p_objRequest.sPayCol;
                }
                if (p_objRequest.PolUnitRowId != null)
                {
                    sPolUnitRowId = p_objRequest.PolUnitRowId;
                }
                //rupal:end for first & final payment
                //rupal:policy system interface
                if (p_objRequest.CovgSeqNum != null)
                {
                    sCovgSeqNum = p_objRequest.CovgSeqNum;
                }
                //rupal:end
                if (p_objRequest.RcRowId != null)
                {
                    sRcRowId = p_objRequest.RcRowId;
                }
                if (p_objRequest.TransSeqNum != null)
                {
                    sTransSeqNum = p_objRequest.TransSeqNum;
                }
                //Ankit Start : Worked on MITS - 34297
                if (p_objRequest.CoverageKey != null)
                {
                    sCoverageKey = p_objRequest.CoverageKey;
                }
                //Ankit End
				//igupta3 Ensuring field is supp or not
                if (p_objRequest.FieldName != null)
                {
                    sFieldName = p_objRequest.FieldName;
                }
                //get codes list
                //pmittal5  MITS:12318  06/13/08
                //p_objXmlOut=objCodeList.GetCodes(lDeptEId,sTableName.Replace("'","''"),sLOB,sTypeLimits,sFormName,
                //								 sTriggerDate,sEventDate,sSessionDsnId,sSessionLOB,sSessionClaimId,lRecordCount,
                //								 lPageNumber,sFilter);

                //For ExtraFilter : MITS 16650 Start
                objCodeList.userid = base.userID;
                objCodeList.OrgSec = base.userLogin.objRiskmasterDatabase.OrgSecFlag; 
                //MITS 16650 End
				
			    // ijha : Mobile adjuater 
			    if (p_objRequest.bIsMobileAdjuster == true)
                {
                    objCodeList.bIsMobileAdjuster = true;
                }
                // End
                if (p_objRequest.PolicyLOB != null)
                    sPolicyLOB = p_objRequest.PolicyLOB;
                //Aman ML Change
                objCodeList.DBID = userLogin.DatabaseId.ToString();
                //objCodeList.USERID = userLogin.UserId;
                objCodeList.userid = base.userID;
                //Aman ML Change
                //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
                //sJurisdiction added by swati for AIC Gap 7 MITS # 36929
                objCodes = objCodeList.GetCodesNew(lDeptEId, sTableName.Replace("'", "''"), sLOB, sJurisdiction, sTypeLimits, sFormName,
                //objCodes = objCodeList.GetCodesNew(lDeptEId, sTableName.Replace("'", "''"), sLOB, sTypeLimits, sFormName,
                                                 sTriggerDate, sEventDate, sSessionDsnId, sSessionLOB, sSessionClaimId, lRecordCount,
                                                 lPageNumber, sFilter, sTitle, sInsuredEid, sEventId, sParentCodeID, p_objRequest.ShowCheckBox, sPolicyId, sCovTypeCodeId,
                                                 sTransId, sClamantEid, sIsFirstFinal, sPolUnitRowId, sRcRowId, sPolicyLOB, sCovgSeqNum, p_objRequest.RsvStatusParent, 
                                                 sTransSeqNum, sSortColumn, sSortOrder, userLogin.objUser.NlsCode, sFieldName, sCoverageKey, sLossTypeCodeId,sPayCol); //MITS # 29790 - 20120921 - bsharma33 - BR Id # 5.1.17 //Mona:Adding codefilter like showing codes whith parent "Open":R6
                //rsushilaggar Handled  paging for checkbox functionality  MITS 21600
                //rupal:sIsFirstFinal feild added for first & final payment                

                return true;
            }
            catch (InvalidValueException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CodesListAdaptor.GetCodes.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objCodeList = null;
            }
        }
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.CodesList.CodesListManager.QuickLookup() method.
		///		Gets the codes list for the quick lookup window by searching on the text
		///		typed by the user.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<CodesList>
		///				<Index>Department Id</Index>
		///				<LookupString>String to search</LookupString>
		///				<LookupType>Type/Code table to search</LookupType>
		///				<LOB>Line Of Business</LOB>	
		///				<SessionDsnId>DSN Id stored in the session</SessionDsnId>
		///				<TypeLimits>Limits for Claim_Types or Event_Types</TypeLimits>
		///			</CodesList>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<codes>
		///			<code>
		///				<id></id>
		///				<text></text>
		///			</code>
		///		</codes>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool QuickLookup(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			CodesListManager objCodeList=null;		//Application layer component
			string sIndex;							//Department Id
			string sLookupString;					//String to search
			string sLookupType;						//Type/Code table to search
			string sLOB;							//Line Of Business
			string sTypeLimits;						//Limits for Claim_Types or Event_Types
			string sSessionDsnId;					//DSN Id stored in the session
            bool bDescriptionSearch;                //whether we want search in short code + desciption
			XmlElement objElement=null;				//used for parsing the input xml
            string sFilter = "";                    //Additional Filter for Custom Lookup for MCIC
            //PJS MITS 11571,03-11-2008 
            string sSessionLOB;
            long lDeptEId;
            string sTableName;
            string sFormName;						//FormName on which the code is required
            string sTriggerDate;					//Date for filtering
            string sEventDate = "";                 //needed for trigger date when form is a claim
            //PJS MITS 11571
            string sTitle = "";                     //pmittal5  MITS:12318  06/13/08
            string sSessionClaimId="";                 //zalam 08/27/2008 Mits:-11708
            string sOrgLevel = "";                  //pmittal5  MITS:12369  08/28/08
			try
			{
				//check existence of Index which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Index");
				if (objElement==null)
				{
					p_objErrOut.Add("CodesListAdaptorParameterMissing",Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				sIndex=objElement.InnerText;
				
				//check existence of Lookup String which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//LookupString");
				if (objElement==null)
				{
					p_objErrOut.Add("CodesListAdaptorParameterMissing",Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				sLookupString=objElement.InnerText;

				//check existence of Lookup Type which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//LookupType");
				if (objElement==null)
				{
					p_objErrOut.Add("CodesListAdaptorParameterMissing",Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				sLookupType=objElement.InnerText;

				//check existence of LOB which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//LOB");
				if (objElement==null)
				{
					p_objErrOut.Add("CodesListAdaptorParameterMissing",Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				sLOB=objElement.InnerText;
				
				
				sSessionDsnId=userLogin.objRiskmasterDatabase.DataSourceId.ToString();
				

				//check existence of Type Limits which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TypeLimits");
				if (objElement==null)
				{
					p_objErrOut.Add("CodesListAdaptorParameterMissing",Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				sTypeLimits=objElement.InnerText;
                //Umesh : MITS 11246
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//bDescriptionSearch");
                if (objElement == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                bDescriptionSearch = Conversion.ConvertStrToBool(objElement.InnerText);
                //check for additional filter. MCIC Changes 01/24/08
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//filter");
                if (objElement != null)
                {
                    sFilter = objElement.InnerText;
                }    

                //PJS MITS 11571-03-11-2008 : Additional Parameters passed for filtering in Quick lookup

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//SessionLOB");
                if (objElement == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sSessionLOB = objElement.InnerText;

                //check existence of Department Id which is required

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//DeptEId");
                if (objElement == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                lDeptEId = Conversion.ConvertStrToLong(objElement.InnerText);


                //check existence of Form Name which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//FormName");
                if (objElement == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sFormName = objElement.InnerText;

                //check existence of Trigger Date which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//TriggerDate");
                if (objElement == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sTriggerDate = objElement.InnerText;

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//EventDate");
                if (objElement != null)
                {
                    sEventDate = objElement.InnerText;
                }

                //PJS 11571,03-11-2008 -till here

                ////pmittal5  MITS:12318  06/13/08  -Passing Title to separate "Search" from other screens in case of Code lookup
                //objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//Title");
                //if (objElement != null)
                //{
                //    sTitle = objElement.InnerText;
                //}

                ////zalam 08/27/2008 Mits:-11708 Start
                //objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//SessionClaimId");
                //if (objElement == null)
                //{
                //    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing"), BusinessAdaptorErrorType.Error);
                //    return false;
                //}
                //sSessionClaimId = objElement.InnerText;
                //zalam 08/27/2008 Mits:-11708 End

                ////pmittal5  MITS:12369  08/28/08
                //objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//orgLevel");
                //if (objElement == null)
                //{
                //    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing"), BusinessAdaptorErrorType.Error);
                //    return false;
                //}
                //sOrgLevel = objElement.InnerText;
                ////End-pmittal5

				objCodeList = new CodesListManager(base.m_connectionString,base.ClientId);

				//get quick look up list
				//PJS 11571,03-11-2008- Passed more parameter to the function
                //pmittal5  MITS:12318  06/13/08
                //p_objXmlOut = objCodeList.QuickLookup(sIndex, sLookupString, sLookupType, sLOB, sTypeLimits, sSessionDsnId, bDescriptionSearch, sFilter, sSessionLOB, lDeptEId, sFormName, sTriggerDate, sEventDate);

                //p_objXmlOut = objCodeList.QuickLookup(sIndex, sLookupString, sLookupType, sLOB, sTypeLimits, sSessionDsnId, bDescriptionSearch, sFilter, sSessionLOB, lDeptEId, sFormName, sTriggerDate, sEventDate, sTitle);
                //zalam 08/27/2008 Mits:-11708
                //pmittal5  MITS:12369  08/28/08  -Passing Org Level in case of Org Hierarchy type code table to filter the respective node level.
                p_objXmlOut = objCodeList.QuickLookup(sIndex, sLookupString, sLookupType, sLOB, sTypeLimits, sSessionDsnId, bDescriptionSearch, sFilter, sSessionLOB, lDeptEId, sFormName, sTriggerDate, sEventDate, sTitle, sSessionClaimId, sOrgLevel);
                //p_objXmlOut = objCodeList.QuickLookup(sIndex, sLookupString, sLookupType, sLOB, sTypeLimits, sSessionDsnId, bDescriptionSearch, sFilter, sSessionLOB, lDeptEId, sFormName, sTriggerDate, sEventDate, sTitle, sSessionClaimId, sOrgLevel);
                
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("CodesListAdaptor.QuickLookup.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objCodeList = null;
				objElement=null;
			}
		}
        public bool QuickLookupData(QuickLookupRequest p_objRequest, ref CodeListType p_objCodeList, ref BusinessAdaptorErrors p_objErrOut)
        {
            CodesListManager objCodeList = null;		//Application layer component
            string sIndex;							//Department Id
            string sLookupString;					//String to search
            string sLookupType;						//Type/Code table to search
            string sLOB;							//Line Of Business
            string sTypeLimits;						//Limits for Claim_Types or Event_Types
            string sSessionDsnId;					//DSN Id stored in the session
            bool bDescriptionSearch;                //whether we want search in short code + desciption
            XmlElement objElement = null;				//used for parsing the input xml
            string sFilter = "";                    //Additional Filter for Custom Lookup for MCIC
            //PJS MITS 11571,03-11-2008 
            string sSessionLOB = "";
            long lDeptEId = 0;
            string sTableName = "";
            string sFormName = "";						//FormName on which the code is required
            string sTriggerDate = "";					//Date for filtering
            string sEventDate = "";                 //needed for trigger date when form is a claim
            //PJS MITS 11571
            string sTitle = String.Empty; 
            string sSessionClaimId = String.Empty;
            string sOrgLevel = String.Empty;
            string sInsuredEid = String.Empty; //PJS MITS 15220
            string sEventId = string.Empty;
			string sParentCodeID = string.Empty;    //Code id of the parent--stara 05/18/09
            Int32 iCurrentPageNumber = 1;
            string sPolicyId = string.Empty;
            string sCovTypeCodeId = string.Empty;
            string sLossTypeCodeId = string.Empty;       //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
            string sTransId = string.Empty;
            string sClamantEid = string.Empty;
            //rupal:r8 first & final pamt enh
            string sIsFirstFinal = string.Empty;
            string sPayCol = string.Empty;//AA
            string sCovgSeqNum = string.Empty;//rupal:policy system interface
            string sPolUnitRowId = string.Empty;
            string sPolicyLOB = string.Empty;
            string sTransSeqNum = string.Empty;
            string sCoverageKey = string.Empty;     //Ankit Start : Worked on MITS - 34297
			string sFieldName = string.Empty; //igupta3 field added
			string sJurisdiction = string.Empty;
            try
            {
                //check existence of Index which is required
                if (p_objRequest.Index == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }

                sIndex = p_objRequest.Index;
                //PJS : MITS #15268 : Lookup string blank should display all codes
                if (p_objRequest.LookupString == null )
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sLookupString = p_objRequest.LookupString;

                //check existence of Lookup Type which is required
                if (p_objRequest.LookupType == null || p_objRequest.LookupType == "")
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sLookupType = p_objRequest.LookupType;

                //check existence of LOB which is required
                if (p_objRequest.LOB == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sLOB = p_objRequest.LOB;


                sSessionDsnId = userLogin.objRiskmasterDatabase.DataSourceId.ToString();


                //check existence of Type Limits which is required
                if (p_objRequest.TypeLimits == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sTypeLimits = p_objRequest.TypeLimits;
                //Umesh : MITS 11246
                if (p_objRequest.DescriptionSearch == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                bDescriptionSearch = Conversion.ConvertStrToBool(p_objRequest.DescriptionSearch);
                //check for additional filter. MCIC Changes 01/24/08
                if (p_objRequest.Filter != null && p_objRequest.Filter != "")
                {
                    //srajindersin MITS 34769 1/8/2014
                    sFilter = CommonFunctions.HTMLCustomDecode(p_objRequest.Filter);
                }


                //PJS MITS 11571-03-11-2008 : Additional Parameters passed for filtering in Quick lookup

                //check existence of Type Limits which is required
                if (p_objRequest.SessionLOB == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sSessionLOB = p_objRequest.SessionLOB;

                //check existence of Department Id which is required

                if (p_objRequest.DeptEId == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                lDeptEId = Conversion.ConvertStrToLong(p_objRequest.DeptEId);


                //check existence of Form Name which is required
                if (p_objRequest.FormName == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sFormName = p_objRequest.FormName;

                //check existence of Trigger Date which is required
                if (p_objRequest.TriggerDate == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sTriggerDate = p_objRequest.TriggerDate;


                if (p_objRequest.TriggerDate != null && p_objRequest.TriggerDate != "")
                {
                    sEventDate = p_objRequest.TriggerDate;
                }

                //PJS 11571,03-11-2008 -till here
                //pmittal5  MITS:12318  06/13/08  -Passing Title to separate "Search" from other screens in case of Code lookup
                if (p_objRequest.Title == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sTitle = p_objRequest.Title;

                //zalam 08/27/2008 Mits:-11708 Start
                if (p_objRequest.SessionClaimId == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sSessionClaimId = p_objRequest.SessionClaimId;
                //zalam 08/27/2008 Mits:-11708 End

                //pmittal5  MITS:12369  08/28/08
                if (p_objRequest.orgLevel == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sOrgLevel = p_objRequest.orgLevel;
                //End-pmittal5
                sParentCodeID = p_objRequest.ParentCodeID;//--stara Mits 16667 05/18/09
                
               

                // PJS MITS 15220 -check existence of Insured Eid which is required 

                if (p_objRequest.InsuredEid == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sInsuredEid = p_objRequest.InsuredEid;
                if (p_objRequest.EventId == null)
                {
                    p_objErrOut.Add("CodesListAdaptorParameterMissing", Globalization.GetString("CodesListAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sEventId = p_objRequest.EventId;
                iCurrentPageNumber = p_objRequest.CurrentPageNumber;

                if (p_objRequest.PolicyId != null)
                {
                    sPolicyId = p_objRequest.PolicyId;
                }

                if (p_objRequest.CovTypeCodeId != null)
                {
                    sCovTypeCodeId = p_objRequest.CovTypeCodeId;
                }
                //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
                if (p_objRequest.CovTypeCodeId != null)
                    sLossTypeCodeId = p_objRequest.LossTypeCodeId;    
                //Ankit End
                if (p_objRequest.TransId != null)
                {
                    sTransId = p_objRequest.TransId;
                }

                if (p_objRequest.ClaimantEid != null)
                {
                    sClamantEid = p_objRequest.ClaimantEid;
                }

                objCodeList = new CodesListManager(base.m_connectionString,base.ClientId);

                //rupal:start for first & final payment
                if (p_objRequest.IsFFPmt != null)
                {
                    sIsFirstFinal = p_objRequest.IsFFPmt;
                }
                //rupal:end for first & final payment
                //AA
                if (p_objRequest.sPayCol != null)
                {
                    sPayCol = p_objRequest.sPayCol;
                }
                //rupal:policy system interface
                if (p_objRequest.CovgSeqNum != null)
                {
                    sCovgSeqNum = p_objRequest.CovgSeqNum;
                }
                //rupal:end
                if(p_objRequest.PolUnitRowId != null)
                {
                    sPolUnitRowId = p_objRequest.PolUnitRowId;
                }
                if (p_objRequest.TransSeqNum != null)
                {
                    sTransSeqNum = p_objRequest.TransSeqNum;
                }
                //Ankit Start : Worked on MITS - 34297
                if (p_objRequest.CoverageKey != null)
                {
                    sCoverageKey = p_objRequest.CoverageKey;
                }
                //Ankit End
				//added by swati MITS # 36929
                if (p_objRequest.Jurisdiction != null)
                {
                    sJurisdiction = p_objRequest.Jurisdiction;
                }
                //change end here by swati
				//igupta3 : Added new field for supp recog
                if (p_objRequest.FieldName != null)
                {
                    sFieldName = p_objRequest.FieldName;
                }

                if (p_objRequest.PolicyLOB != null)
                    sPolicyLOB = p_objRequest.PolicyLOB;
                //pmittal5 Confidential Record
                objCodeList.userid = base.userID;

                objCodeList.OrgSec = base.userLogin.objRiskmasterDatabase.OrgSecFlag;
                //Aman ML Change
                objCodeList.DBID = base.DSNID.ToString();
                //End- pmittal5
                //get quick look up list
                //PJS 11571,03-11-2008- Passed more parameter to the function
                //pmittal5  MITS:12318  06/13/08
                //p_objXmlOut = objCodeList.QuickLookup(sIndex, sLookupString, sLookupType, sLOB, sTypeLimits, sSessionDsnId, bDescriptionSearch, sFilter, sSessionLOB, lDeptEId, sFormName, sTriggerDate, sEventDate);

                //p_objXmlOut = objCodeList.QuickLookup(sIndex, sLookupString, sLookupType, sLOB, sTypeLimits, sSessionDsnId, bDescriptionSearch, sFilter, sSessionLOB, lDeptEId, sFormName, sTriggerDate, sEventDate, sTitle);
                //zalam 08/27/2008 Mits:-11708
                //pmittal5  MITS:12369  08/28/08  -Passing Org Level in case of Org Hierarchy type code table to filter the respective node level.
                //p_objXmlOut = objCodeList.QuickLookup(sIndex, sLookupString, sLookupType, sLOB, sTypeLimits, sSessionDsnId, bDescriptionSearch, sFilter, sSessionLOB, lDeptEId, sFormName, sTriggerDate, sEventDate, sTitle, sSessionClaimId);
                //Ankit Start : Work for JIRA - 908 (Adding LossType for Where condition)
                //sJurisdiction added by swati for AIC Gap 7 MITS # 36929
                //p_objCodeList = objCodeList.QuickLookupData(sIndex, sLookupString, sLookupType, sLOB, sTypeLimits, sSessionDsnId, bDescriptionSearch, sFilter,
                p_objCodeList = objCodeList.QuickLookupData(sIndex, sLookupString, sLookupType, sLOB, sJurisdiction, sTypeLimits, sSessionDsnId, bDescriptionSearch, sFilter,
                        sSessionLOB, lDeptEId, sFormName, sTriggerDate, sEventDate, sTitle, sSessionClaimId, sOrgLevel, sInsuredEid, sEventId, sParentCodeID,
                        iCurrentPageNumber, sPolicyId, sCovTypeCodeId, sTransId, sClamantEid, sIsFirstFinal, sPolUnitRowId, sPolicyLOB, 
                        sCovgSeqNum, p_objRequest.RsvStatusParent, sTransSeqNum, userLogin.objUser.NlsCode, sFieldName, sCoverageKey, sLossTypeCodeId,sPayCol); //rupal: added sIsFFPmt for firt & final payment
                return true;
            }
            catch (InvalidValueException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CodesListAdaptor.QuickLookup.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objCodeList = null;
                objElement = null;
            }
        }
        #endregion
	}
}
