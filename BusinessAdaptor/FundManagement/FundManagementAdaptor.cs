﻿
/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 12/16/2013 | 34082  | pgupta93   | Changes req for MultiCurrency
 **********************************************************************************************/
using System;
using System.IO;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.FundManagement;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using Riskmaster.Settings;


namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: FundManagementAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 21-Feb-2005
	///* $Author	: Raman Bhatia
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	///* Amendment  -->
	///  1.Date		: 7 Feb 06 
	///    Desc		: Replacing RMO_ACCESS use, and changing params of permission
	///				  violation exception so it can be correctly called. 
	///    Author	: Sumit
	/// <summary>	

	/// <summary>
	/// This class is used to call the application layer component for Fund Management that 
	///	is used for Funds operations.	
	/// </summary>
	public class FundManagementAdaptor : BusinessAdaptorBase 
	{
		#region security codes
		
//		private const int RMO_ACCESS = 0;
//		private const int RMO_VIEW = 1;
//		private const int RMO_UPDATE = 2;
//		private const int RMO_CREATE = 3;
//		private const int RMO_DELETE = 4;
		private const int SUPP_OFFSET = 50;						

		private const int RMB_FUNDS = 9500;
		private const int RMB_FUNDS_APPPAYCOV = 10500;
		
		private const int RMB_FUNDS_CLEARCHK = 10200;

		private const int RMB_FUNDS_TRANSACT = 9650;
		private const int RMO_PRINT_CHK = 30;
		private const int RMO_EDIT_PAYEE = 31;
        //Geeta 04/17/07 Modified to fix Mits 9102 
        private const int RMO_CHANGE_CHK_STATUS = 32;        
		private const int RMO_ALLOW_MANUAL_CHECKS = 34;
		private const int RMO_ALLOW_EDIT_CTL_NUMBER = 35;
		private const int RMO_ALLOW_EDIT_CHK_NUMBER = 36;
		private const int RMO_ALLOW_PAYMENT_CLOSED_CLM =37;
		private const int RMO_ALLOW_EDIT_PRECHECK = 38;
		private const int RMO_ALLOW_EDIT_CLAIM_NUMBER = 39;
		private const int RMO_ALLOW_ENTRY_CLAIM_NUMBER = 41;
		private const int RMO_ALLOW_VOID_CLOSED_CLAIM = 42;
		private const int RMO_FUNDS_TRANSACT_SUPP = 50;

		private const int RMB_FUNDS_TRANSACT_SUPP = 9700;
        //Added by Shivendu for MITS 7361
        private const int RMB_FUNDS_TRANSACT_SUPP_VIEW = 9701;
		
		private const int RMB_FUNDS_BNKACCT = 9800;
		
		private const int RMB_FUNDS_BNKACCT_SUPP = 9850;
			
		private const int RMB_FUNDS_BNKACCT_CHKSTOCK = 9950;
	
		private const int RMB_FUNDS_BNKACCT_SUBACT = 10550;
				
		private const int RMB_FUNDS_PRINTCHK = 10100;
		private const int RMO_ALLOW_CHANGE_CHECK_NUMBER = 31;

		private const int RMB_FUNDS_VOIDCHK = 10250;
		private const int RMO_FUNDS_VOID_PRINTED_CHECK = 30;

		private const int RMB_FUNDS_AUTOCHK = 10400;
		private const int RMO_AUTOCHK_ALLOW_EDIT_CTL_NUMBER = 32;
		private const int RMO_ALLOW_FREEZE_AUTOCHK_BATCH = 33;

		private const int RMB_FUNDS_DEPOSIT = 10650;
		private const int RMB_LOOKUP = 7000000;
		private const int RMO_BANK_ACCT = 30;
        //added by Ashutosh
        private const int RMB_FUNDS_UnClearChecks = 10251;   
  //Animesh Inserted Bulk Check Release
        private const int RMB_FUNDS_BLKCHKREL = 108100;
        //Animesh Insertion ends//skhare7 RMSC merge
		#endregion
		

		#region Constructor 
		/// Name		: FundManagementAdaptor
		/// Author		: Raman Bhatia
		/// Date Created: 21-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		///	<summary>
		/// Constructor for the Class
		/// </summary>
		public FundManagementAdaptor()
		{

		}

		#endregion

		#region Public Methods
        //JIRA-18229 pkumari3 start
        /// <summary>
        /// Get Login name and DateEventDesc
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetLoginInfo(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlNode objNode = null;
            bool bIsValid = true;
            SysSettings objSettings = null;
            objSettings = new SysSettings(this.connectionString, base.ClientId);
            try
            {
                if (p_objXmlIn != null)
                {
                    objNode = p_objXmlIn.SelectSingleNode("//LoginInfo/LoginName");
                    if (objNode != null)
                    {
                        objNode.InnerText = userLogin.LoginName;
                    }
                    objNode = p_objXmlIn.SelectSingleNode("//LoginInfo/prefixdatetimestamp");
                    if (objNode != null)
                    {
                        objNode.InnerText = objSettings.DateEventDesc.ToString();
                    }
                }

            }
            catch (RMAppException p_objEx)
            {
                bIsValid = false;
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                bIsValid = false;
                throw new RMAppException(Globalization.GetString("FundManagementAdaptor.GetLoginInfo.Error", base.ClientId), p_objEx);
            }
            finally
            {
                p_objXmlOut = p_objXmlIn;
            }
            return bIsValid;
        }
        //JIRA-18229 pkumari3 end
        /// <summary>
        /// MITS:34082 MultiCurrency: Added function for get ExchangeRate and it's Effective date
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool bGetExchRateAndEffDate(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FundManager objFundManager = null;
            try
            {
                objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);

                p_objXmlOut = objFundManager.GetExchRateAndEffDate(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            finally
            {
                if (objFundManager != null)
                {
                    objFundManager.Dispose();
                    objFundManager = null;
                }
            }
        }
		#region Apply Payments to coverages 
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 24-Aug-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets All the details of Claims 
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document.
		///		Output XML document
		/// </param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			AddPaymentsToCoverages objPayments=null;	
			XmlDocument objDOC=null;
			string sTemp="";
			try
			{
				if (!userLogin.IsAllowedEx(RMB_FUNDS_APPPAYCOV,RMPermissions.RMO_ACCESS))
					throw new PermissionViolationException(RMPermissions.RMO_ACCESS, RMB_FUNDS_APPPAYCOV);
				objPayments=new AddPaymentsToCoverages(base.userLogin.objRiskmasterDatabase.DataSourceName , base.userLogin.LoginName, base.userLogin.Password, base.ClientId);
				objDOC=new XmlDocument();
                //Changed by Gagan for MITS 22238 : Start
				p_objXmlOut=objPayments.GetPolicies(p_objXmlIn,ref sTemp);
                //Changed by Gagan for MITS 22238 : End
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("FundManagementAdaptor.Get.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				if (objPayments != null)
				{
					objPayments.Dispose();
					objPayments = null;
				}
				objDOC=null;
			}
		}
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 24-Aug-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Apply Payments to coverages 
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document.
		///		Output XML document
		/// </param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool ApplyPayments(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			AddPaymentsToCoverages objPayments=null;	
			XmlDocument objDOC=null;
			try
			{
				objPayments=new AddPaymentsToCoverages(base.userLogin.objRiskmasterDatabase.DataSourceName , base.userLogin.LoginName, base.userLogin.Password, base.ClientId);
				objDOC=new XmlDocument();
				p_objXmlOut=objPayments.ApplyPayments(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("FundManagementAdaptor.ApplyPayments.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objDOC=null;
				if (objPayments != null)
				{
					objPayments.Dispose();
					objPayments = null;
				}
			}
		}
	

/// Name		: CheckUtility
        /// Author		: Shilpi Tara
        /// Date Created: 15-May-2009		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Check the useResFilter Setting in Utility
        /// </summary>
        /// <param name="p_objXmlIn">XML contains the input parameters needed.
        /// </param>
        /// <param name="p_objXmlOut">XML contains the output document.
        ///		Output XML document
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool CheckUtility(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)//added by stara Mits 16667
        {
            FundManager objFundManager=null;
            try
            {

                objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);
                p_objXmlOut=objFundManager.CheckUtility(p_objXmlIn);
                return true;
            }

            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.CheckUtility.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {

                if (objFundManager != null)
                {
                    objFundManager.Dispose();
                    objFundManager = null;
                }
            }

        }

        /// Name		: GetPolicyList
        /// Author		: Alok S Bisht
        /// Date Created: 4-Feb-1983		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Returns list of policies attached to 
        /// </summary>
        /// <param name="p_objXmlIn">XML contains the input parameters needed.
        /// </param>
        /// <param name="p_objXmlOut">XML contains the output document.
        ///		Output XML document
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool GetPolicyList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FundManager objFundManager = null;
            try
            {

                objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);
                p_objXmlOut = objFundManager.GetPolicyList(p_objXmlIn);
                return true;
            }

            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.CheckUtility.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {

                if (objFundManager != null)
                {
                    objFundManager.Dispose();
                    objFundManager = null;
                }
            }

        }

		#region Experience 
		/// Name		: GetExperience
		/// Author		: Raman Bhatia
		/// Date Created: 21-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to FundManager.GetExperience method.
		///		This method will call the methods to get the experience data. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<FundManager>
		///				<IsCollection>Collection Flag</IsCollection>
		///				<EID>ID</EID>
		///			</FundManager>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document.
		///		The structure of output XML document would be:
		///		<Document>
		///			<Experience TransType>
		///				<NumberOfTrans></NumberOfTrans>
		///				<Amount></Amount>
		///				<AvgAmount></AvgAmount>
		///			</Experience>
		///		</Document>
		/// </param>
		/// 
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool GetExperience(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FundManager objFundManager = null;					//object of FundManager Class
			XmlElement objTargetElement = null;					//used for creating the Output Xml
			bool bIsCollection = false;							//Collection Flag passed in input Xml document
			int iEID = 0;										//ID passed in input Xml document

			try
			{
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "IsCollection"); 
				bIsCollection = Conversion.ConvertStrToBool(objTargetElement.InnerText);
				
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "EID"); 
				iEID = Conversion.ConvertStrToInteger(objTargetElement.InnerText);

                objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);
				
				//Calling the GetExperience method to get the output xml document
				
				p_objXmlOut = objFundManager.GetExperience(bIsCollection, iEID);
				return true;            
			}
			
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.Error);
				return false;
			}
			
			catch (Exception p_objException)
			{
				p_objErrOut.Add(p_objException ,Globalization.GetString("FundManagementAdaptor.GetExperience.error", base.ClientId) , BusinessAdaptorErrorType.Error); 
				return false;				
			}
			finally
			{
				if(objFundManager != null)
				{
					objFundManager.Dispose();
					objFundManager=null;
				}
				objTargetElement=null;
			}


		}

		#endregion
		#region Payment Collection Methods :  GetPaymentCollection , DeletePaymentCollection
		/// Name		: GetPaymentCollection
		/// Author		: Raman Bhatia
		/// Date Created: 21-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to FundManager.GetPaymentCollection method.
		///		This method will call the methods that return the read-only status and Ctl number Only.
		///     Actual data will be either in Session XML or it will be a blank Payment Collection.  		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<FundManager>
		///				<TransID>TransId</TransID>
		///				<ClaimId>ClaimId</ClaimId>
		///				<ClaimantEid>ClaimantId</ClaimantEid>
		///				<UnitId>UnitId</UnitId>
		///				<SplitId>SplitId</SplitId>
		///				<AllowPaymentsClosedClaims>AllowPaymentsClosedClaims Flag</AllowPaymentsClosedClaims>
		///			</FundManager>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document.
		///		The structure of output XML document would be:
		///		<Document>
		///			<PaymentCollection>
		///				<IsReadOnly></IsReadOnly>
		///				<CtlNumber></CtlNumber>
		///			</PaymentCollection>
		///		</Document>
		/// </param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool GetPaymentCollection(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FundManager objFundManager = null;					//object of FundManager Class
			XmlElement objTargetElement = null;					//used for creating the Output Xml
			int iTransID = 0;									//TransID passed in input Xml document
			int iClaimId = 0;									//ClaimId passed in input Xml document
			int iClaimantEid = 0;								//ClaimantEid passed in input Xml document
			int iUnitId = 0;									//UnitId passed in input Xml document
			int iSplitId = 0;									//SplitId passed in input Xml document
			bool bAllowPaymentsClosedClaims = false;			//AllowPaymentsClosedClaims flag passed in input Xml document
				
			try 
			{
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "TransID"); 
				iTransID = Conversion.ConvertStrToInteger(objTargetElement.InnerText);				
				
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "ClaimId"); 
				iClaimId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);				
				
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "ClaimantEid"); 
				iClaimantEid = Conversion.ConvertStrToInteger(objTargetElement.InnerText);				
								
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "UnitId"); 
				iUnitId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);				
				
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "SplitId"); 
				iSplitId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);				
				
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "AllowPaymentsClosedClaims"); 
				bAllowPaymentsClosedClaims = Conversion.ConvertStrToBool(objTargetElement.InnerText);

                objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);
                
				//Calling the GetPaymentCollection method to get the output xml document
				p_objXmlOut = objFundManager.GetPaymentCollection(iTransID,iClaimId,iClaimantEid,iUnitId,iSplitId,bAllowPaymentsClosedClaims);

				return true;
	
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.Error);
				return false;
			}
			
			catch (Exception p_objException)
			{
				p_objErrOut.Add(p_objException ,Globalization.GetString("FundManagementAdaptor.GetPaymentCollection.error", base.ClientId) , BusinessAdaptorErrorType.Error); 
				return false;				
			}
			finally
			{
				if(objFundManager != null)
				{
					objFundManager.Dispose();
					objFundManager=null;
				}
				objTargetElement=null;
			}

		}

		/// Name		: DeletePaymentCollection
		/// Author		: Raman Bhatia
		/// Date Created: 21-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to FundManager.DeletePaymentCollection method.
		///		This method will call the methods that delete the entry from m_objFunds object
		///		 and then calls GetXMLForTransaction function to return the display XML. 
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<FundManager>
		///				<SplitXML>XML contains Split information for each payment collection</SplitXML>
		///				<TransactionDataXML>Transaction Data XML</TransactionDataXML>
		///			</FundManager>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document.
		///		The structure of output XML document would be:
		///		<Document>
		///			<Transaction>
		///				<Funds>
		///					<ClaimId></ClaimId>
		///					<ClaimNumber></ClaimNumber>
		///					<ReserveTracking></ReserveTracking>
		///					<TransDate></TransDate>
		///					<CtlNumber></CtlNumber>
		///					<VoidFlag></VoidFlag>
		///					<ClearedFlag></ClearedFlag>
		///					<PayeeTypeCode ShortCode Desc>
		///					</PayeeTypeCode>
		///					<LastName></LastName>
		///					<FirstName></FirstName>
		///					<PayeeEid EntityGlossaryType></PayeeEid>
		///					<PayeeEntityTaxId></PayeeEntityTaxId>
		///					<Addr1></Addr1>
		///					<Addr2></Addr2>
		///					<City></City>
		///					<DateOfCheck></DateOfCheck>
		///					<TransNumber></TransNumber>
		///					<CheckMemo></CheckMemo>
		///					<ClaimantEid></ClaimantEid>
		///					<EnclosureFlag></EnclosureFlag>
		///					<AutoCheckFlag></AutoCheckFlag>
		///					<SettlementFlag></SettlementFlag>
		///					<StateId ShortCode Desc></StateId>
		///					<StatusCode ShortCode Desc ReleasedShortCode ReleasedDesc></StatusCode>
		///					<UnitId></UnitId>
		///					<Vehicle></Vehicle>
		///					<ClaimantLastFirstName></ClaimantLastFirstName>
		///					<IsTandE></IsTandE>
		///					<Notes></Notes>
		///					<BatchNumber></BatchNumber>
		///				</Funds>
		///				<Accounts Count>
		///					<Account AccountName AccountId SubRowId />  
		///				</Accounts>
		///				<Units Count>
		///					<Unit Id VehicleMake VehicleModel />
		///				</Units>
		///				<Claimants Count>
		///					<Claimant LastName FirstName />
		///				</Claimants>
		///				<PayeeTypes Count ShortCode>
		///				<PayeeType CodeId ShortCode CodeDesc />
		///				</PayeeTypes>
		///				<IsReadOnly></IsReadOnly>
		///				<IsClaimFrozen></IsClaimFrozen>
		///				<IsClaimOpen></IsClaimOpen>
		///				<ReserveTracking></ReserveTracking>
		///				<InsufAmntXml></InsufAmntXml>
		///				<UseSubAccounts></UseSubAccounts>
		///				<DataChanged></DataChanged>
		///			</Transaction>
		///		</Document>
		/// </param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool DeletePaymentCollection(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FundManager objFundManager = null;					//object of FundManager Class
			XmlElement objTargetElement = null;					//used for creating the Output Xml
			string sSplitXML = "";								//SplitXML passed in input Xml document
			string sTransactionDataXML = "";					//TransactionDataXML passed in input Xml document

			try 
			{
				
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "SplitXML"); 
				sSplitXML = objTargetElement.InnerXml;
				
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "TransactionDataXML"); 
				sTransactionDataXML = objTargetElement.InnerXml;

                objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);
				
				//Calling the DeletePaymentCollection method to get the output xml document
				p_objXmlOut = objFundManager.DeletePaymentCollection(sSplitXML , sTransactionDataXML);
				return true;
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.Error);
				return false;
			}
			
			catch (Exception p_objException)
			{
				p_objErrOut.Add(p_objException ,Globalization.GetString("FundManagementAdaptor.DeletePaymentCollection.error", base.ClientId) , BusinessAdaptorErrorType.Error); 
				return false;				
			}

			finally
			{
				if(objFundManager != null)
				{
					objFundManager.Dispose();
					objFundManager=null;
				}
				objTargetElement=null;
			}

		}

		#endregion
		#region Navigate Transaction Methods : Prev , Next , First , Last , MoveTo , New  
		/// Name		: GetPreviousTransaction
		/// Author		: Raman Bhatia
		/// Date Created: 21-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to FundManager.GetPreviousTransaction method.
		///		This method will call the methods which gets the previous transaction.
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<FundManager>
		///				<TransID>TransId</TransID>
		///				<AllowPaymentsClosedClaims>AllowPaymentsClosedClaims Flag</AllowPaymentsClosedClaims>
		///			</FundManager>
		///		</Document> 
		/// 
		/// <param name="p_objXmlOut">XML contains the output document.
		///		The structure of output XML document would be:
		///		<Document>
		///			<Transaction>
		///				<TransSplitList>
		///					<TransSplit>
		///						<Crc></Crc>
		///						<TransId></TransId>
		///						<SplitRowId></SplitRowId>
		///						<TransTypeCode ShortCode Desc></TransTypeCode>
		///						<Amount></Amount>
		///						<GlAccountCode ShortCode Desc></GlAccountCode>
		///						<FromDate></FromDate>
		///						<ToDate></ToDate>
		///						<InvoicedBy></InvoicedBy>
		///						<InvoiceNumber></InvoiceNumber>
		///						<InvoiceAmount></InvoiceAmount>
		///						<InvoiceDate></InvoiceDate>
		///						<PoNumber></PoNumber>
		///						<DataChanged></DataChanged>
		///						<ReserveTypeCode ShortCode Desc></ReserveTypeCode>
		///						<NewRecord></NewRecord>
		///					</TransSplit>
		///				</TransSplitList>
		///				<Funds>
		///					<ClaimId></ClaimId>
		///					<ClaimNumber></ClaimNumber>
		///					<ReserveTracking></ReserveTracking>
		///					<TransDate></TransDate>
		///					<CtlNumber></CtlNumber>
		///					<VoidFlag></VoidFlag>
		///					<ClearedFlag></ClearedFlag>
		///					<PayeeTypeCode ShortCode Desc></PayeeTypeCode>
		///					<LastName></LastName>
		///					<FirstName></FirstName>
		///					<PayeeEid EntityGlossaryType></PayeeEid>
		///					<PayeeEntityTaxId></PayeeEntityTaxId>
		///					<Addr1></Addr1>
		///					<Addr2></Addr2>
		///					<City></City>
		///					<DateOfCheck></DateOfCheck>
		///					<TransNumber></TransNumber>
		///					<CheckMemo></CheckMemo>
		///					<ClaimantEid></ClaimantEid>
		///					<EnclosureFlag></EnclosureFlag>
		///					<AutoCheckFlag></AutoCheckFlag>
		///					<SettlementFlag></SettlementFlag>
		///					<StateId ShortCode Desc></StateId>
		///					<StatusCode ShortCode Desc ReleasedShortCode ReleasedDesc></StatusCode>
		///					<UnitId></UnitId>
		///					<Vehicle></Vehicle>
		///					<ClaimantLastFirstName></ClaimantLastFirstName>
		///					<IsTandE></IsTandE>
		///					<Notes></Notes>
		///					<BatchNumber></BatchNumber>
		///				</Funds>
		///				<Accounts Count>
		///					<Account AccountName AccountId SubRowId />
		///				</Accounts>
		///				<Claimants Count>
		///					<Claimant EId LastName FirstName Selected />
		///				</Claimants>
		///				<PayeeTypes Count ShortCode>
		///					<PayeeType CodeId ShortCode CodeDesc />
		///				</PayeeTypes>
		///				<IsReadOnly></IsReadOnly>
		///				<IsClaimFrozen></IsClaimFrozen>
		///				<IsClaimOpen></IsClaimOpen>
		///				<ReserveTracking></ReserveTracking>
		///				<InsufAmntXml></InsufAmntXml>
		///				<UseSubAccounts></UseSubAccounts>
		///				<DataChanged></DataChanged>
		///			</Transaction>
		///		</Document>
		/// </param>
		
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
	
		public bool GetPreviousTransaction(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FundManager objFundManager = null;					//object of FundManager Class
			XmlElement objTargetElement = null;					//used for creating the Output Xml
			int iTransID = 0;									//TransID passed in input Xml document
			bool bAllowPaymentsClosedClaims = false;			//AllowPaymentsClosedClaims flag passed in input Xml document
			
			try 
			{
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "TransID"); 
				iTransID = Conversion.ConvertStrToInteger(objTargetElement.InnerText);				
				
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "AllowPaymentsClosedClaims"); 
				bAllowPaymentsClosedClaims = Conversion.ConvertStrToBool(objTargetElement.InnerText);

                objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);
				
				//Calling the GetPreviousTransaction method to get the output xml document
				p_objXmlOut = objFundManager.GetPreviousTransaction(iTransID,bAllowPaymentsClosedClaims);
				SetSecurityFlags(ref p_objXmlOut);
				return true;
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.SystemError);
				return false;
			}
			
			catch (Exception p_objException)
			{
				p_objErrOut.Add(p_objException ,Globalization.GetString("FundManagementAdaptor.GetPreviousTransaction.error", base.ClientId) , BusinessAdaptorErrorType.Error); 
				return false;				
			}
			finally
			{
				if(objFundManager != null)
				{
					objFundManager.Dispose();
					objFundManager=null;
				}
				objTargetElement=null;
			}

		}

		/// Name		: GetNextTransaction
		/// Author		: Raman Bhatia
		/// Date Created: 21-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to FundManager.GetNextTransaction method.
		///		This method will call the methods, which get the next transaction.
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<FundManager>
		///				<TransID>TransId</TransID>
		///				<AllowPaymentsClosedClaims>AllowPaymentsClosedClaims Flag</AllowPaymentsClosedClaims>
		///			</FundManager>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document.
		///		The structure of output XML document would be:
		///		<Document>
		///			<Transaction>
		///				<TransSplitList>
		///					<TransSplit>
		///						<Crc></Crc>
		///						<TransId></TransId>
		///						<SplitRowId></SplitRowId>
		///						<TransTypeCode ShortCode Desc></TransTypeCode>
		///						<Amount></Amount>
		///						<GlAccountCode ShortCode Desc></GlAccountCode>
		///						<FromDate></FromDate>
		///						<ToDate></ToDate>
		///						<InvoicedBy></InvoicedBy>
		///						<InvoiceNumber></InvoiceNumber>
		///						<InvoiceAmount></InvoiceAmount>
		///						<InvoiceDate></InvoiceDate>
		///						<PoNumber></PoNumber>
		///						<DataChanged></DataChanged>
		///						<ReserveTypeCode ShortCode Desc></ReserveTypeCode>
		///						<NewRecord></NewRecord>
		///					</TransSplit>
		///				</TransSplitList>
		///				<Funds>
		///					<ClaimId></ClaimId>
		///					<ClaimNumber></ClaimNumber>
		///					<ReserveTracking></ReserveTracking>
		///					<TransDate></TransDate>
		///					<CtlNumber></CtlNumber>
		///					<VoidFlag></VoidFlag>
		///					<ClearedFlag></ClearedFlag>
		///					<PayeeTypeCode ShortCode Desc></PayeeTypeCode>
		///					<LastName></LastName>
		///					<FirstName></FirstName>
		///					<PayeeEid EntityGlossaryType></PayeeEid>
		///					<PayeeEntityTaxId></PayeeEntityTaxId>
		///					<Addr1></Addr1>
		///					<Addr2></Addr2>
		///					<City></City>
		///					<DateOfCheck></DateOfCheck>
		///					<TransNumber></TransNumber>
		///					<CheckMemo></CheckMemo>
		///					<ClaimantEid></ClaimantEid>
		///					<EnclosureFlag></EnclosureFlag>
		///					<AutoCheckFlag></AutoCheckFlag>
		///					<SettlementFlag></SettlementFlag>
		///					<StateId ShortCode Desc></StateId>
		///					<StatusCode ShortCode Desc ReleasedShortCode ReleasedDesc></StatusCode>
		///					<UnitId></UnitId>
		///					<Vehicle></Vehicle>
		///					<ClaimantLastFirstName></ClaimantLastFirstName>
		///					<IsTandE></IsTandE>
		///					<Notes></Notes>
		///					<BatchNumber></BatchNumber>
		///				</Funds>
		///				<Accounts Count>
		///					<Account AccountName AccountId SubRowId />
		///				</Accounts>
		///				<Claimants Count>
		///					<Claimant EId LastName FirstName Selected />
		///				</Claimants>
		///				<PayeeTypes Count ShortCode>
		///					<PayeeType CodeId ShortCode CodeDesc />
		///				</PayeeTypes>
		///				<IsReadOnly></IsReadOnly>
		///				<IsClaimFrozen></IsClaimFrozen>
		///				<IsClaimOpen></IsClaimOpen>
		///				<ReserveTracking></ReserveTracking>
		///				<InsufAmntXml></InsufAmntXml>
		///				<UseSubAccounts></UseSubAccounts>
		///				<DataChanged></DataChanged>
		///			</Transaction>
		///		</Document>
		/// </param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
	
		public bool GetNextTransaction(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FundManager objFundManager = null;					//object of FundManager Class
			XmlElement objTargetElement = null;					//used for creating the Output Xml
			int iTransID = 0;									//TransID passed in input Xml document
			bool bAllowPaymentsClosedClaims = false;			//AllowPaymentsClosedClaims flag passed in input Xml document
			
			try 
			{
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "TransID"); 
				iTransID = Conversion.ConvertStrToInteger(objTargetElement.InnerText);				
				
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "AllowPaymentsClosedClaims"); 
				bAllowPaymentsClosedClaims = Conversion.ConvertStrToBool(objTargetElement.InnerText);

                objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);
				
				//Calling the GetNextTransaction method to get the output xml document
				p_objXmlOut = objFundManager.GetNextTransaction(iTransID,bAllowPaymentsClosedClaims);
				SetSecurityFlags(ref p_objXmlOut);
				return true;
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.SystemError);
				return false;
			}
			
			catch (Exception p_objException)
			{
				p_objErrOut.Add(p_objException ,Globalization.GetString("FundManagementAdaptor.GetNextTransaction.error", base.ClientId) , BusinessAdaptorErrorType.Error); 
				return false;				
			}
			finally
			{
				if(objFundManager != null)
				{
					objFundManager.Dispose();
					objFundManager=null;
				}
				objTargetElement=null;
			}

		}

		
		/// Name		: GetFirstTransaction
		/// Author		: Raman Bhatia
		/// Date Created: 21-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to FundManager.GetFirstTransaction method.
		///		This method will call the methods, which get the first transaction.
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<FundManager>
		///				<AllowPaymentsClosedClaims>AllowPaymentsClosedClaims Flag</AllowPaymentsClosedClaims>
		///			</FundManager>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document.
		///		The structure of output XML document would be:
		///		<Document>
		///			<Transaction>
		///				<TransSplitList>
		///					<TransSplit>
		///						<Crc></Crc>
		///						<TransId></TransId>
		///						<SplitRowId></SplitRowId>
		///						<TransTypeCode ShortCode Desc></TransTypeCode>
		///						<Amount></Amount>
		///						<GlAccountCode ShortCode Desc></GlAccountCode>
		///						<FromDate></FromDate>
		///						<ToDate></ToDate>
		///						<InvoicedBy></InvoicedBy>
		///						<InvoiceNumber></InvoiceNumber>
		///						<InvoiceAmount></InvoiceAmount>
		///						<InvoiceDate></InvoiceDate>
		///						<PoNumber></PoNumber>
		///						<DataChanged></DataChanged>
		///						<ReserveTypeCode ShortCode Desc></ReserveTypeCode>
		///						<NewRecord></NewRecord>
		///					</TransSplit>
		///				</TransSplitList>
		///				<Funds>
		///					<ClaimId></ClaimId>
		///					<ClaimNumber></ClaimNumber>
		///					<ReserveTracking></ReserveTracking>
		///					<TransDate></TransDate>
		///					<CtlNumber></CtlNumber>
		///					<VoidFlag></VoidFlag>
		///					<ClearedFlag></ClearedFlag>
		///					<PayeeTypeCode ShortCode Desc></PayeeTypeCode>
		///					<LastName></LastName>
		///					<FirstName></FirstName>
		///					<PayeeEid EntityGlossaryType></PayeeEid>
		///					<PayeeEntityTaxId></PayeeEntityTaxId>
		///					<Addr1></Addr1>
		///					<Addr2></Addr2>
		///					<City></City>
		///					<DateOfCheck></DateOfCheck>
		///					<TransNumber></TransNumber>
		///					<CheckMemo></CheckMemo>
		///					<ClaimantEid></ClaimantEid>
		///					<EnclosureFlag></EnclosureFlag>
		///					<AutoCheckFlag></AutoCheckFlag>
		///					<SettlementFlag></SettlementFlag>
		///					<StateId ShortCode Desc></StateId>
		///					<StatusCode ShortCode Desc ReleasedShortCode ReleasedDesc></StatusCode>
		///					<UnitId></UnitId>
		///					<Vehicle></Vehicle>
		///					<ClaimantLastFirstName></ClaimantLastFirstName>
		///					<IsTandE></IsTandE>
		///					<Notes></Notes>
		///					<BatchNumber></BatchNumber>
		///				</Funds>
		///				<Accounts Count>
		///					<Account AccountName AccountId SubRowId />
		///				</Accounts>
		///				<Claimants Count>
		///					<Claimant EId LastName FirstName Selected />
		///				</Claimants>
		///				<PayeeTypes Count ShortCode>
		///					<PayeeType CodeId ShortCode CodeDesc />
		///				</PayeeTypes>
		///				<IsReadOnly></IsReadOnly>
		///				<IsClaimFrozen></IsClaimFrozen>
		///				<IsClaimOpen></IsClaimOpen>
		///				<ReserveTracking></ReserveTracking>
		///				<InsufAmntXml></InsufAmntXml>
		///				<UseSubAccounts></UseSubAccounts>
		///				<DataChanged></DataChanged>
		///			</Transaction>
		///		</Document>
		/// </param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
	
		public bool GetFirstTransaction(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FundManager objFundManager = null;					//object of FundManager Class
			XmlElement objTargetElement = null;					//used for creating the Output Xml
			bool bAllowPaymentsClosedClaims = false;			//AllowPaymentsClosedClaims flag passed in input Xml document
			
			try 
			{
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "AllowPaymentsClosedClaims"); 
				bAllowPaymentsClosedClaims = Conversion.ConvertStrToBool(objTargetElement.InnerText);

                objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);
				
				//Calling the GetFirstTransaction method to get the output xml document
				p_objXmlOut = objFundManager.GetFirstTransaction(bAllowPaymentsClosedClaims);
				SetSecurityFlags(ref p_objXmlOut);
				return true;
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.SystemError);
				return false;
			}
			
			catch (Exception p_objException)
			{
				p_objErrOut.Add(p_objException ,Globalization.GetString("FundManagementAdaptor.GetFirstTransaction.error", base.ClientId) , BusinessAdaptorErrorType.Error); 
				return false;				
			}
			finally
			{
				if(objFundManager != null)
				{
					objFundManager.Dispose();
					objFundManager=null;
				}
				objTargetElement=null;
			}

		}

		/// Name		: GetLastTransaction
		/// Author		: Raman Bhatia
		/// Date Created: 21-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to FundManager.GetLastTransaction method.
		///		This method will call the methods, which get the last transaction.
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<FundManager>
		///				<AllowPaymentsClosedClaims>AllowPaymentsClosedClaims Flag</AllowPaymentsClosedClaims>
		///			</FundManager>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document.
		///		The structure of output XML document would be:
		///		<Document>
		///			<Transaction>
		///				<TransSplitList>
		///					<TransSplit>
		///						<Crc></Crc>
		///						<TransId></TransId>
		///						<SplitRowId></SplitRowId>
		///						<TransTypeCode ShortCode Desc></TransTypeCode>
		///						<Amount></Amount>
		///						<GlAccountCode ShortCode Desc></GlAccountCode>
		///						<FromDate></FromDate>
		///						<ToDate></ToDate>
		///						<InvoicedBy></InvoicedBy>
		///						<InvoiceNumber></InvoiceNumber>
		///						<InvoiceAmount></InvoiceAmount>
		///						<InvoiceDate></InvoiceDate>
		///						<PoNumber></PoNumber>
		///						<DataChanged></DataChanged>
		///						<ReserveTypeCode ShortCode Desc></ReserveTypeCode>
		///						<NewRecord></NewRecord>
		///					</TransSplit>
		///				</TransSplitList>
		///				<Funds>
		///					<ClaimId></ClaimId>
		///					<ClaimNumber></ClaimNumber>
		///					<ReserveTracking></ReserveTracking>
		///					<TransDate></TransDate>
		///					<CtlNumber></CtlNumber>
		///					<VoidFlag></VoidFlag>
		///					<ClearedFlag></ClearedFlag>
		///					<PayeeTypeCode ShortCode Desc></PayeeTypeCode>
		///					<LastName></LastName>
		///					<FirstName></FirstName>
		///					<PayeeEid EntityGlossaryType></PayeeEid>
		///					<PayeeEntityTaxId></PayeeEntityTaxId>
		///					<Addr1></Addr1>
		///					<Addr2></Addr2>
		///					<City></City>
		///					<DateOfCheck></DateOfCheck>
		///					<TransNumber></TransNumber>
		///					<CheckMemo></CheckMemo>
		///					<ClaimantEid></ClaimantEid>
		///					<EnclosureFlag></EnclosureFlag>
		///					<AutoCheckFlag></AutoCheckFlag>
		///					<SettlementFlag></SettlementFlag>
		///					<StateId ShortCode Desc></StateId>
		///					<StatusCode ShortCode Desc ReleasedShortCode ReleasedDesc></StatusCode>
		///					<UnitId></UnitId>
		///					<Vehicle></Vehicle>
		///					<ClaimantLastFirstName></ClaimantLastFirstName>
		///					<IsTandE></IsTandE>
		///					<Notes></Notes>
		///					<BatchNumber></BatchNumber>
		///				</Funds>
		///				<Accounts Count>
		///					<Account AccountName AccountId SubRowId />
		///				</Accounts>
		///				<Claimants Count>
		///					<Claimant EId LastName FirstName Selected />
		///				</Claimants>
		///				<PayeeTypes Count ShortCode>
		///					<PayeeType CodeId ShortCode CodeDesc />
		///				</PayeeTypes>
		///				<IsReadOnly></IsReadOnly>
		///				<IsClaimFrozen></IsClaimFrozen>
		///				<IsClaimOpen></IsClaimOpen>
		///				<ReserveTracking></ReserveTracking>
		///				<InsufAmntXml></InsufAmntXml>
		///				<UseSubAccounts></UseSubAccounts>
		///				<DataChanged></DataChanged>
		///			</Transaction>
		///		</Document>
		/// </param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
	
		public bool GetLastTransaction(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FundManager objFundManager = null;					//object of FundManager Class
			XmlElement objTargetElement = null;					//used for creating the Output Xml
			bool bAllowPaymentsClosedClaims = false;			//AllowPaymentsClosedClaims flag passed in input Xml document
			
			try 
			{
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "AllowPaymentsClosedClaims"); 
				bAllowPaymentsClosedClaims = Conversion.ConvertStrToBool(objTargetElement.InnerText);

                objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);
				
				//Calling the GetLastTransaction method to get the output xml document
				p_objXmlOut = objFundManager.GetLastTransaction(bAllowPaymentsClosedClaims);
				SetSecurityFlags(ref p_objXmlOut);
				return true;
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.SystemError);
				return false;
			}
			
			catch (Exception p_objException)
			{
				p_objErrOut.Add(p_objException ,Globalization.GetString("FundManagementAdaptor.GetLastTransaction.error", base.ClientId) , BusinessAdaptorErrorType.Error); 
				return false;				
			}
			finally
			{
				if(objFundManager != null)
				{
					objFundManager.Dispose();
					objFundManager=null;
				}
				objTargetElement=null;
			}
		}
		

		/// Name		: GetTransaction
		/// Author		: Raman Bhatia
		/// Date Created: 22-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to FundManager.GetTransaction method.
		///		This method will call the methods, which get a particular transaction.
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<FundManager>
		///				<TransID>TransId</TransID>
		///				<AllowPaymentsClosedClaims>AllowPaymentsClosedClaims Flag</AllowPaymentsClosedClaims>
		///			</FundManager>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document.
		///		The structure of output XML document would be:
		///		<Document>
		///			<Transaction>
		///				<TransSplitList>
		///					<TransSplit>
		///						<Crc></Crc>
		///						<TransId></TransId>
		///						<SplitRowId></SplitRowId>
		///						<TransTypeCode ShortCode Desc></TransTypeCode>
		///						<Amount></Amount>
		///						<GlAccountCode ShortCode Desc></GlAccountCode>
		///						<FromDate></FromDate>
		///						<ToDate></ToDate>
		///						<InvoicedBy></InvoicedBy>
		///						<InvoiceNumber></InvoiceNumber>
		///						<InvoiceAmount></InvoiceAmount>
		///						<InvoiceDate></InvoiceDate>
		///						<PoNumber></PoNumber>
		///						<DataChanged></DataChanged>
		///						<ReserveTypeCode ShortCode Desc></ReserveTypeCode>
		///						<NewRecord></NewRecord>
		///					</TransSplit>
		///				</TransSplitList>
		///				<Funds>
		///					<ClaimId></ClaimId>
		///					<ClaimNumber></ClaimNumber>
		///					<ReserveTracking></ReserveTracking>
		///					<TransDate></TransDate>
		///					<CtlNumber></CtlNumber>
		///					<VoidFlag></VoidFlag>
		///					<ClearedFlag></ClearedFlag>
		///					<PayeeTypeCode ShortCode Desc></PayeeTypeCode>
		///					<LastName></LastName>
		///					<FirstName></FirstName>
		///					<PayeeEid EntityGlossaryType></PayeeEid>
		///					<PayeeEntityTaxId></PayeeEntityTaxId>
		///					<Addr1></Addr1>
		///					<Addr2></Addr2>
		///					<City></City>
		///					<DateOfCheck></DateOfCheck>
		///					<TransNumber></TransNumber>
		///					<CheckMemo></CheckMemo>
		///					<ClaimantEid></ClaimantEid>
		///					<EnclosureFlag></EnclosureFlag>
		///					<AutoCheckFlag></AutoCheckFlag>
		///					<SettlementFlag></SettlementFlag>
		///					<StateId ShortCode Desc></StateId>
		///					<StatusCode ShortCode Desc ReleasedShortCode ReleasedDesc></StatusCode>
		///					<UnitId></UnitId>
		///					<Vehicle></Vehicle>
		///					<ClaimantLastFirstName></ClaimantLastFirstName>
		///					<IsTandE></IsTandE>
		///					<Notes></Notes>
		///					<BatchNumber></BatchNumber>
		///				</Funds>
		///				<Accounts Count>
		///					<Account AccountName AccountId SubRowId />
		///				</Accounts>
		///				<Claimants Count>
		///					<Claimant EId LastName FirstName Selected />
		///				</Claimants>
		///				<PayeeTypes Count ShortCode>
		///					<PayeeType CodeId ShortCode CodeDesc />
		///				</PayeeTypes>
		///				<IsReadOnly></IsReadOnly>
		///				<IsClaimFrozen></IsClaimFrozen>
		///				<IsClaimOpen></IsClaimOpen>
		///				<ReserveTracking></ReserveTracking>
		///				<InsufAmntXml></InsufAmntXml>
		///				<UseSubAccounts></UseSubAccounts>
		///				<DataChanged></DataChanged>
		///			</Transaction>
		///		</Document>
		/// </param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
	
		public bool GetTransaction(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FundManager objFundManager = null;					//object of FundManager Class
			XmlElement objTargetElement = null;					//used for creating the Output Xml
			int iTransID = 0;									//TransID passed in input Xml document
			bool bAllowPaymentsClosedClaims = false;			//AllowPaymentsClosedClaims flag passed in input Xml document
			
			try 
			{
				//Added by Mohit Yadav for Bug No. 001849
				if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT,RMPermissions.RMO_VIEW))
					throw new PermissionViolationException(RMPermissions.RMO_VIEW,RMB_FUNDS_TRANSACT);
				
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "TransID"); 
				iTransID = Conversion.ConvertStrToInteger(objTargetElement.InnerText);				
				
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "AllowPaymentsClosedClaims"); 
				bAllowPaymentsClosedClaims = Conversion.ConvertStrToBool(objTargetElement.InnerText);

                objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);
				
				//Calling the GetTransaction method to get the output xml document
				p_objXmlOut = objFundManager.GetTransaction(iTransID,bAllowPaymentsClosedClaims);
				SetSecurityFlags(ref p_objXmlOut);

				return true;
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.SystemError);
				return false;
			}
			
			catch (Exception p_objException)
			{
				p_objErrOut.Add(p_objException ,Globalization.GetString("FundManagementAdaptor.GetTransaction.error", base.ClientId) , BusinessAdaptorErrorType.Error); 
				return false;				
			}
			finally
			{
				if(objFundManager != null)
				{
					objFundManager.Dispose();
					objFundManager=null;
				}
				objTargetElement=null;
			}

		}
		
		/// Name		: GetNewTransaction
		/// Author		: Raman Bhatia
		/// Date Created: 23-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to both of the overloaded FundManager.GetNewTransaction methods.
		///		This method will call the methods which gets a new transaction for a claim or otherwise.
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document for the first overloaded method 
		///		which gets the new transaction for a claim would be:
		///		<Document>
		///			<FundManager>
		///				<ClaimId>ClaimId</ClaimId>
		///				<ClaimantEid>ClaimantId</ClaimantEid>
		///				<UnitId>UnitId</UnitId>
		///				<IsCollection>Collection Flag</IsCollection>
		///			</FundManager>
		///		</Document> 
		/// 
		///		The structure of input XML document for the second overloaded method 
		///		which gets the new transaction would be:
		///		<Document>
		///			<FundManager>
		///				<IsCollection>Collection Flag</IsCollection>
		///			</FundManager>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		///		<Document>
		///			<Transaction>
		///				<Funds>
		///					<ClaimId></ClaimId>
		///					<ClaimNumber></ClaimNumber>
		///					<ReserveTracking></ReserveTracking>
		///					<TransDate></TransDate>
		///					<CtlNumber></CtlNumber>
		///					<VoidFlag></VoidFlag>
		///					<ClearedFlag></ClearedFlag>
		///					<PayeeTypeCode ShortCode Desc></PayeeTypeCode>
		///					<LastName></LastName>
		///					<FirstName></FirstName>
		///					<PayeeEid EntityGlossaryType></PayeeEid>
		///					<PayeeEntityTaxId></PayeeEntityTaxId>
		///					<Addr1></Addr1>
		///					<Addr2></Addr2>
		///					<City></City>
		///					<DateOfCheck></DateOfCheck>
		///					<TransNumber></TransNumber>
		///					<CheckMemo></CheckMemo>
		///					<ClaimantEid></ClaimantEid>
		///					<EnclosureFlag></EnclosureFlag>
		///					<AutoCheckFlag></AutoCheckFlag>
		///					<SettlementFlag></SettlementFlag>
		///					<StateId ShortCode Desc></StateId>
		///					<StatusCode ShortCode Desc ReleasedShortCode ReleasedDesc></StatusCode>
		///					<UnitId></UnitId>
		///					<Vehicle></Vehicle>
		///					<ClaimantLastFirstName></ClaimantLastFirstName>
		///					<IsTandE></IsTandE>
		///					<Notes></Notes>
		///					<BatchNumber></BatchNumber>
		///				</Funds>
		///				<Accounts Count>
		///					<Account AccountName AccountId SubRowId />
		///				</Accounts>
		///				<Units Count>
		///					<Unit Id VehicleMake VehicleModel Selected />
		///				</Units>
		///				<Claimants Count>
		///					<Claimant EId LastName FirstName Selected />
		///				</Claimants>
		///				<PayeeTypes Count ShortCode>
		///					<PayeeType CodeId ShortCode CodeDesc />
		///				</PayeeTypes>
		///				<IsReadOnly></IsReadOnly>
		///				<IsClaimFrozen></IsClaimFrozen>
		///				<IsClaimOpen></IsClaimOpen>
		///				<ReserveTracking></ReserveTracking>
		///				<InsufAmntXml></InsufAmntXml>
		///				<UseSubAccounts></UseSubAccounts>
		///				<DataChanged></DataChanged>
		///			</Transaction>
		///		</Document>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
	
		public bool GetNewTransaction(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FundManager objFundManager = null;					//object of FundManager Class
			XmlElement objTargetElement = null;					//used for creating the Output Xml
			int iClaimId = 0;                                   //ClaimId passed in input Xml document
			int iClaimantEid = 0;								//ClaimantEid passed in input Xml document
			int iUnitId = 0;									//UnitId passed in input Xml document
			bool bIsCollection = false;							//Collection Flag passed in input Xml document
            //Changed by Gagan for MITS 8515 : Start
            string sClaimNumber = String.Empty;                 //Claim Number passed in input Xml document
            string sSql = String.Empty;
            bool bTabOut = false;
            //Added by Shivendu for MITS 7361
            bool bSuppViewPerm = false;
            //Changed by Gagan for MITS 8515 : End
			try
			{
				if (!userLogin.IsAllowedEx(RMB_FUNDS,RMPermissions.RMO_ACCESS))
					throw new PermissionViolationException(RMPermissions.RMO_ACCESS ,RMB_FUNDS);//,RMO_FUNDS_TRANSACT);
				else if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT,RMPermissions.RMO_ACCESS))
					throw new PermissionViolationException(RMPermissions.RMO_ACCESS,RMB_FUNDS_TRANSACT);
				else if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT,RMPermissions.RMO_CREATE))
					throw new PermissionViolationException(RMPermissions.RMO_CREATE,RMB_FUNDS_TRANSACT);

				//Commented by Mohit
				//else if ( !userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT,RMO_VIEW))
				//	throw new PermissionViolationException(RMB_FUNDS,RMO_VIEW);
				
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "IsCollection"); 
				bIsCollection = Conversion.ConvertStrToBool(objTargetElement.InnerText);

				//selecting which overloaded method to call depending on input XML document
				if ((p_objXmlIn.SelectSingleNode("//" + "ClaimId")==null)&&(p_objXmlIn.SelectSingleNode("//" + "ClaimantEid")==null)&&(p_objXmlIn.SelectSingleNode("//" + "UnitId")==null))
				{
                    objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);
					
					//Calling the GetNewTransaction method to get the output xml document
					p_objXmlOut = objFundManager.GetNewTransaction(bIsCollection);
					//Not sure if there is aneed to migrate full fledged security.
					// This is just to mimick the rmnet behaviour. 1/27 Sumit
					SetEditablePayeeInfoSecurity(ref p_objXmlOut);

					objTargetElement = p_objXmlOut.CreateElement("IsEditableCtrlNumber");
					if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT,RMO_ALLOW_EDIT_CTL_NUMBER))
						objTargetElement.InnerText = "false";
					else
						objTargetElement.InnerText = "true";
					p_objXmlOut.DocumentElement.AppendChild(objTargetElement);

					objTargetElement = p_objXmlOut.CreateElement("IsEditableClaimNumber");
					if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_ALLOW_EDIT_CLAIM_NUMBER))
						objTargetElement.InnerText = "false";
					else
						objTargetElement.InnerText = "true";
					p_objXmlOut.DocumentElement.AppendChild(objTargetElement);

					objTargetElement = p_objXmlOut.CreateElement("IsEntryClaimNumber");
					if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_ALLOW_ENTRY_CLAIM_NUMBER ))
						objTargetElement.InnerText = "false";
					else
						objTargetElement.InnerText = "true";
					p_objXmlOut.DocumentElement.AppendChild(objTargetElement);

                    //Geeta 04/17/07 Modified to fix Mits 9102 
                    objTargetElement = p_objXmlOut.CreateElement("IsChangeCheckStatusAllowed");
                    if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_CHANGE_CHK_STATUS))
                        objTargetElement.InnerText = "false";
                    else
                        objTargetElement.InnerText = "true";
                    p_objXmlOut.DocumentElement.AppendChild(objTargetElement); 
				}

				else
				{
					//Commented Nitesh jan31,2006 Resolved TR:-1856,1854 Starts
					//					if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_ALLOW_PAYMENT_CLOSED_CLM))
					//						throw new PermissionViolationException(RMB_FUNDS_TRANSACT, RMO_ALLOW_PAYMENT_CLOSED_CLM);
					//Commented Nitesh jan31,2006 Resolved TR:-1856,1854 Ends
                    //Changed by Gagan for MITS 8515 : Start
                    objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "ClaimNumber");
                    sClaimNumber = objTargetElement.InnerText;
                    //Changed by Gagan for MITS 8515 : End

					objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "ClaimId"); 
					iClaimId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);				
				
					objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "ClaimantEid"); 
					iClaimantEid = Conversion.ConvertStrToInteger(objTargetElement.InnerText);				
								
					objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "UnitId"); 
					iUnitId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);

                    //Changed by Gagan for MITS 8515 : Start

                    //When user tabs out of Claim number text box
                    if (sClaimNumber != "" && iClaimId == 0)
                    {
                        bTabOut = true;
                        sSql =  "Select Claim_ID from claim where Claim_number = '" + sClaimNumber + "'";
                        using (DbReader objReader = DbFactory.GetDbReader(base.connectionString, sSql))
                        {
                            if (objReader.Read())
                            {
                                iClaimId = objReader.GetInt32("Claim_ID");
                            }
                        }
                    }

                    //Changed by Gagan for MITS 8515 : End

                    objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);
					//Nitesh jan31,2006 Resolved TR:-1856,1854 Starts
					if (!objFundManager.IsClaimOpen(iClaimId,0) )
					{
						if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_ALLOW_PAYMENT_CLOSED_CLM))
							throw new PermissionViolationException(RMPermissions.RMO_ALLOW_PAYMENT_CLOSED_CLM,RMB_FUNDS_TRANSACT);
					}
					//Nitesh jan31,2006 Resolved TR:-1856,1854 Ends
					//Calling the GetNewTransaction method to get the output xml document				
					p_objXmlOut = objFundManager.GetNewTransaction(iClaimId,iClaimantEid,iUnitId,bIsCollection);
                    //Changed by Gagan for MITS 8515 : Start
                    if (bTabOut == true)
                    {
                        objTargetElement = (XmlElement)p_objXmlOut.SelectSingleNode("//" + "ClaimNumber");
                        objTargetElement.InnerText = sClaimNumber;
                    }
                    //Changed by Gagan for MITS 8515 : End

					SetSecurityFlags(ref p_objXmlOut);
				}
				XmlElement tempElement = p_objXmlOut.CreateElement("IsSaveAllowed");
				if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMPermissions.RMO_UPDATE))
					tempElement.InnerText = "false";
				else
					tempElement.InnerText = "true";
				p_objXmlOut.DocumentElement.AppendChild(tempElement);

				tempElement = p_objXmlOut.CreateElement("IsCreateAllowed");
				if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMPermissions.RMO_CREATE))
					tempElement.InnerText = "false";
				else
					tempElement.InnerText = "true";
				p_objXmlOut.DocumentElement.AppendChild(tempElement);
				//Nitesh o2Feb,2006 Resolved TR:-1875 Starts
				tempElement = p_objXmlOut.CreateElement("IsVisibleSupplementalTab");
                //|| condition added by Shivendu for MITS 7361
                bSuppViewPerm = userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT_SUPP_VIEW, RMPermissions.RMO_ACCESS);
                if ((!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT_SUPP, RMPermissions.RMO_ACCESS)) || (!bSuppViewPerm))
					tempElement.InnerText = "false";
				else
					tempElement.InnerText = "true";

				p_objXmlOut.DocumentElement.AppendChild(tempElement);
				//Nitesh o2Feb,2006 Resolved TR:-1875 Ends				

				return true;
				//	if( userLogin.IsAllowed(lGetSecurityBase(frmNewFunds), RMO_ALLOW_ENTRY_CLAIM_NUMBER))
				//		{
				//todo
				//		}
				//		else
				//		{
				//todo
				//		}
				//				if( userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT + SUPP_OFFSET, RMO_ACCESS))
				//				       tabMain.TabVisible(4) = True    SUPPLEMENTAL TAB
				//       
				//				else
				//						tabMain.TabVisible(4) = False
			}
				
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.SystemError);
				return false;
			}
			
			catch (Exception p_objException)
			{
				p_objErrOut.Add(p_objException ,Globalization.GetString("FundManagementAdaptor.GetNewTransaction.error", base.ClientId) , BusinessAdaptorErrorType.Error); 
				return false;				
			}
			finally
			{
				if(objFundManager != null)
				{
					objFundManager.Dispose();
					objFundManager=null;
				}
				objTargetElement=null;
			}

		}


        
		
		#endregion
		/*
        #region Save Transaction
        

		/// Name		: SaveTransaction
		/// Author		: Raman Bhatia
		/// Date Created: 23-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to FundManager.SaveTransaction method.
		///		This method will call the methods that saves the transaction.
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<FundManager>
		///				<SplitXML>XML contains Split information for each payment collection</SplitXML>
		///				<TransactionDataXML>Transaction Data XML</TransactionDataXML>
		///			</FundManager>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		///		<Document>
		///			<Transaction>
		///				<Funds>
		///					<ClaimId></ClaimId>
		///					<ClaimNumber></ClaimNumber>
		///					<ReserveTracking></ReserveTracking>
		///					<TransDate></TransDate>
		///					<CtlNumber></CtlNumber>
		///					<VoidFlag></VoidFlag>
		///					<ClearedFlag></ClearedFlag>
		///					<PayeeTypeCode ShortCode Desc></PayeeTypeCode>
		///					<LastName></LastName>
		///					<FirstName></FirstName>
		///					<PayeeEid EntityGlossaryType></PayeeEid>
		///					<PayeeEntityTaxId></PayeeEntityTaxId>
		///					<Addr1></Addr1>
		///					<Addr2></Addr2>
		///					<City></City>
		///					<DateOfCheck></DateOfCheck>
		///					<TransNumber></TransNumber>
		///					<CheckMemo></CheckMemo>
		///					<ClaimantEid></ClaimantEid>
		///					<EnclosureFlag></EnclosureFlag>
		///					<AutoCheckFlag></AutoCheckFlag>
		///					<SettlementFlag></SettlementFlag>
		///					<StateId ShortCode Desc></StateId>
		///					<StatusCode ShortCode Desc ReleasedShortCode ReleasedDesc></StatusCode>
		///					<UnitId></UnitId>
		///					<Vehicle></Vehicle>
		///					<ClaimantLastFirstName></ClaimantLastFirstName>
		///					<IsTandE></IsTandE>
		///					<Notes></Notes>
		///					<BatchNumber></BatchNumber>
		///				</Funds>
		///				<Accounts Count>
		///					<Account AccountName AccountId SubRowId />
		///				</Accounts>
		///				<Units Count>
		///					<Unit Id VehicleMake VehicleModel Selected />
		///				</Units>
		///				<Claimants Count>
		///					<Claimant EId LastName FirstName Selected />
		///				</Claimants>
		///				<PayeeTypes Count ShortCode>
		///					<PayeeType CodeId ShortCode CodeDesc />
		///				</PayeeTypes>
		///				<IsReadOnly></IsReadOnly>
		///				<IsClaimFrozen></IsClaimFrozen>
		///				<IsClaimOpen></IsClaimOpen>
		///				<ReserveTracking></ReserveTracking>
		///				<InsufAmntXml></InsufAmntXml>
		///				<UseSubAccounts></UseSubAccounts>
		///				<DataChanged></DataChanged>
		///			</Transaction>
		///		</Document>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool SaveTransaction(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FundManager objFundManager = null;					//object of FundManager Class
			XmlElement objTargetElement = null;					//used for creating the Output Xml
			string sSplitXML = "";								//SplitXML passed in input Xml document
			string sTransactionDataXML = "";					//TransactionDataXML passed in input Xml document
			XmlDocument objTransactionDoc = null ;				//Added by Mohit
			int iTransId = 0;									
			int iClaimId = 0;									//Defect #2410 - 03/06/2006 - Anurag

			try 
			{
				objTransactionDoc = new XmlDocument();//Added by Mohit

				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "SplitXML"); 
				sSplitXML = objTargetElement.InnerXml;
				
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "TransactionDataXML"); 
				sTransactionDataXML = objTargetElement.InnerXml;
				
				objTransactionDoc.LoadXml(sTransactionDataXML);	//Added by Mohit
				
				objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName , base.userLogin.LoginName, base.userLogin.Password,base.userLogin.UserId,base.userLogin.GroupId,base.securityConnectionString,base.DSNID,base.userLogin.objUser.ManagerId);

				//Added by Mohit for Bug No. 001851
				objTargetElement = (XmlElement)objTransactionDoc.SelectSingleNode("//TransId");
				iTransId = Conversion.ConvertStrToInteger(objTargetElement.InnerXml);
				
				if (iTransId!=0)
				{
					if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT,RMPermissions.RMO_UPDATE))
						throw new PermissionViolationException(RMPermissions.RMO_UPDATE,RMB_FUNDS_TRANSACT);
				}
				else
				{
					if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT,RMPermissions.RMO_CREATE))
						throw new PermissionViolationException(RMPermissions.RMO_CREATE,RMB_FUNDS_TRANSACT);
				}
				
				//Defect #2410 - 03/06/2006 - Anurag - Check for permissions to save without claim attached,
				objTargetElement = (XmlElement)objTransactionDoc.SelectSingleNode("//ClaimId");
				iClaimId = Conversion.ConvertStrToInteger(objTargetElement.InnerXml);

				if( iClaimId == 0 )
				{
					if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_ALLOW_MANUAL_CHECKS))
						throw new PermissionViolationException(RMPermissions.RMO_ALLOW_MANUAL_CHECKS, RMB_FUNDS_TRANSACT);
				}

				//Calling the SaveTransaction method to get the output xml document
				p_objXmlOut = objFundManager.SaveTransaction(sSplitXML , sTransactionDataXML, this);
				return true;
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.Error);
				return false;
			}
			
			catch (Exception p_objException)
			{
				p_objErrOut.Add(p_objException ,Globalization.GetString("FundManagementAdaptor.SaveTransaction.error") , BusinessAdaptorErrorType.Error); 
				return false;				
			}

			finally
			{
				if(objFundManager != null)
				{
					objFundManager.Dispose();
					objFundManager=null;
				}
				objTargetElement=null;
			}

		}

		#endregion
         */
		#region Add/Edit Split

		/// Name		: AddEditSplitTransaction
		/// Author		: Raman Bhatia
		/// Date Created: 23-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to FundManager.AddEditSplitTransaction method.
		///		This method will call the methods that fetch the XML for Funds Object after Add/Edit Split.
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<FundManager>
		///				<SplitXML>XML contains Split information for each payment collection</SplitXML>
		///				<TransactionDataXML>Transaction Data XML</TransactionDataXML>
		///			</FundManager>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		///		<Document>
		///			<Transaction>
		///				<Funds>
		///					<ClaimId></ClaimId>
		///					<ClaimNumber></ClaimNumber>
		///					<ReserveTracking></ReserveTracking>
		///					<TransDate></TransDate>
		///					<CtlNumber></CtlNumber>
		///					<VoidFlag></VoidFlag>
		///					<ClearedFlag></ClearedFlag>
		///					<PayeeTypeCode ShortCode Desc></PayeeTypeCode>
		///					<LastName></LastName>
		///					<FirstName></FirstName>
		///					<PayeeEid EntityGlossaryType></PayeeEid>
		///					<PayeeEntityTaxId></PayeeEntityTaxId>
		///					<Addr1></Addr1>
		///					<Addr2></Addr2>
		///					<City></City>
		///					<DateOfCheck></DateOfCheck>
		///					<TransNumber></TransNumber>
		///					<CheckMemo></CheckMemo>
		///					<ClaimantEid></ClaimantEid>
		///					<EnclosureFlag></EnclosureFlag>
		///					<AutoCheckFlag></AutoCheckFlag>
		///					<SettlementFlag></SettlementFlag>
		///					<StateId ShortCode Desc></StateId>
		///					<StatusCode ShortCode Desc ReleasedShortCode ReleasedDesc></StatusCode>
		///					<UnitId></UnitId>
		///					<Vehicle></Vehicle>
		///					<ClaimantLastFirstName></ClaimantLastFirstName>
		///					<IsTandE></IsTandE>
		///					<Notes></Notes>
		///					<BatchNumber></BatchNumber>
		///				</Funds>
		///				<Accounts Count>
		///					<Account AccountName AccountId SubRowId />
		///				</Accounts>
		///				<Units Count>
		///					<Unit Id VehicleMake VehicleModel Selected />
		///				</Units>
		///				<Claimants Count>
		///					<Claimant EId LastName FirstName Selected />
		///				</Claimants>
		///				<PayeeTypes Count ShortCode>
		///					<PayeeType CodeId ShortCode CodeDesc />
		///				</PayeeTypes>
		///				<IsReadOnly></IsReadOnly>
		///				<IsClaimFrozen></IsClaimFrozen>
		///				<IsClaimOpen></IsClaimOpen>
		///				<ReserveTracking></ReserveTracking>
		///				<InsufAmntXml></InsufAmntXml>
		///				<UseSubAccounts></UseSubAccounts>
		///				<DataChanged></DataChanged>
		///			</Transaction>
		///		</Document>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool AddEditSplitTransaction(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FundManager objFundManager = null;					//object of FundManager Class
			XmlElement objTargetElement = null;					//used for creating the Output Xml
			string sSplitXML = "";								//SplitXML passed in input Xml document
			string sTransactionDataXML = "";					//TransactionDataXML passed in input Xml document

			try 
			{
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "SplitXML"); 
				sSplitXML = objTargetElement.InnerXml;
				
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "TransactionDataXML"); 
				sTransactionDataXML = objTargetElement.InnerXml;

                objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);
				
				//Calling the AddEditSplitTransaction method to get the output xml document
				p_objXmlOut = objFundManager.AddEditSplitTransaction(sSplitXML , sTransactionDataXML);
				return true;
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.Error);
				return false;
			}
			
			catch (Exception p_objException)
			{
				p_objErrOut.Add(p_objException ,Globalization.GetString("FundManagementAdaptor.AddEditSplitTransaction.error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;				
			}

			finally
			{
				if(objFundManager != null)
				{
					objFundManager.Dispose();
					objFundManager=null;
				}
				objTargetElement=null;
			}

		}
		
		#endregion
		#region Email Documents
		/// Name			: EmailDocuments
		/// Author			: Raman Bhatia
		/// Date Created	: 29-June-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Wrapper to FundManager.EmailDocuments of application
		/// layer that send emails to specified users. 
		/// </summary>
		/// <param name="p_objXmlDocIn"> 
		///	Input paramters in xml format.
		///	Structure:
		///		<EmailDocuments>
		///			<EmailIds>
		///				Comma separated list of EmailIds.
		///			</EmailIds>
		///			<Subject>
		///				Subject of the mail
		///			</Subject>
		///			<Message>
		///				Body of the mail
		///			</Message>	
		///				Comma separated list of UserIds.
		///				
		///		</EmailDocuments>
		/// </param>
		/// <param name="p_objXmlDocOut"> 
		///		Empty
		///	</param>
		/// <param name="p_objErrOut">
		///	Collection of errors/messages.	
		///	</param>
		/// <returns>
		///		true - success
		///		false - failure
		///	</returns>
		public bool EmailDocuments(XmlDocument p_objXmlDocIn,ref XmlDocument p_objXmlDocOut,
			ref BusinessAdaptorErrors p_objErrOut)
		{
			bool bReturnValue = false;
			FundManager objFundManager = null;

			XmlNode objXmlNode = null;
			
			string sEmailIds = string.Empty; 
			string sMessage = string.Empty; 
			string sSubject = string.Empty;

			try
			{
				objXmlNode = p_objXmlDocIn.SelectSingleNode("//EmailDocuments/EmailIds");
				
				if(objXmlNode==null)
				{ 
					bReturnValue = false;
					p_objErrOut.Add("FundManagementAdaptor.EmailDocuments.MissingInputParameter",
						Globalization.GetString("FundManagementAdaptor.EmailDocuments.MissingEmailIdsNode", base.ClientId),
						BusinessAdaptorErrorType.Error);  
					return bReturnValue;
				}

				sEmailIds = objXmlNode.InnerText; 

				
				objXmlNode = null;
				objXmlNode = p_objXmlDocIn.SelectSingleNode("//EmailDocuments/Message");
				
				if(objXmlNode==null)
				{ 
					bReturnValue = false;
					p_objErrOut.Add("FundManagementAdaptor.EmailDocuments.MissingInputParameter",
						Globalization.GetString("FundManagementAdaptor.EmailDocuments.MissingMessageNode", base.ClientId),
						BusinessAdaptorErrorType.Error);  
					return bReturnValue;
				}

				sMessage = objXmlNode.InnerText; 

				objXmlNode = null;
				objXmlNode = p_objXmlDocIn.SelectSingleNode("//EmailDocuments/Subject");
				
				if(objXmlNode==null)
				{ 
					bReturnValue = false;
					p_objErrOut.Add("FundManagementAdaptor.EmailDocuments.MissingInputParameter",
						Globalization.GetString("FundManagementAdaptor.EmailDocuments.MissingMessageNode", base.ClientId),
						BusinessAdaptorErrorType.Error);  
					return bReturnValue;
				}

				sSubject = objXmlNode.InnerText;

                objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);
				
				objFundManager.EmailDocuments(base.userLogin.LoginName, this.securityConnectionString,
					sEmailIds, sSubject, sMessage);
 					
				bReturnValue = true;								
			}
			catch(InitializationException p_objInitializationException)
			{
				bReturnValue = false;
				p_objErrOut.Add(p_objInitializationException,BusinessAdaptorErrorType.Error);   
				return bReturnValue;
			}
			catch(InvalidValueException p_objInvalidValueException)
			{
				bReturnValue = false;
				p_objErrOut.Add(p_objInvalidValueException,BusinessAdaptorErrorType.Error);   
				return bReturnValue;
			}
			catch(RMAppException p_objRMAppException)
			{
				bReturnValue = false;
				p_objErrOut.Add(p_objRMAppException,BusinessAdaptorErrorType.Error);   
				return bReturnValue;
			}
			catch(Exception p_objException)
			{
				bReturnValue = false;
				p_objErrOut.Add(p_objException,Globalization.GetString("FundManagementAdaptor.EmailDocuments.Error", base.ClientId),BusinessAdaptorErrorType.Error);   
				return bReturnValue;
			}
			finally
			{
				objXmlNode = null;
				if(objFundManager != null)
				{
					objFundManager.Dispose();
					objFundManager=null;
				}
			}

			return bReturnValue;
		}

		#endregion
		#region Payment History
		/// Name			: GetPaymentHistory
		/// Author			: Vaibhav Kaushik
		/// Date Created	: 28-July-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Wrapper to FundManager.GetPaymentHistory of application, Returns the Payment History. 
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input Xml has following format :
		///		<GetPaymentHistory>
		///			<ClaimId>17</ClaimId> 
		///			<ClaimantEid>0</ClaimantEid> 
		///			<UnitId>0</UnitId> 
		///			<SortCol/> 
		///			<Ascending/> 
		///		</GetPaymentHistory>
		///	</param>
		/// <param name="p_objXmlOut">
		///		Out Xml has following format :
		///		
		///		<PaymentHistory>
		///			<row>
		///				<VoidFlag>No</VoidFlag> 
		///				<ClearedFlag>No</ClearedFlag> 
		///				<PaymentFlag>Payment</PaymentFlag> 
		///				<CtlNumber>0000012</CtlNumber> 
		///				<TransNumber>0</TransNumber> 
		///				<TransDate>4/5/2002</TransDate> 
		///				<Amount>$1,200.00</Amount> 
		///				<Date>-</Date> 
		///				<InvoiceNumber /> 
		///				<SplitAmount>$1,200.00</SplitAmount> 
		///				<CheckMemo /> 
		///				<Name>Muphey General Repair</Name> 
		///				<User>dtg</User> 
		///				<CheckStatus>R - Released</CheckStatus> 
		///			</row>
		///			<row>
		///				<VoidFlag>No</VoidFlag> 
		///				<ClearedFlag>No</ClearedFlag> 
		///				<PaymentFlag>Payment</PaymentFlag> 
		///				<CtlNumber>0000013</CtlNumber> 
		///				<TransNumber>0</TransNumber> 
		///				<TransDate>4/5/2002</TransDate> 
		///				<Amount>$8,700.00</Amount> 
		///				<Date>-</Date> 
		///				<InvoiceNumber /> 
		///				<SplitAmount>$8,700.00</SplitAmount> 
		///				<CheckMemo /> 
		///				<Name>Anaelle Anaelle</Name> 
		///				<User>dtg</User> 
		///				<CheckStatus>R - Released</CheckStatus> 
		///			</row>
		///			<TotalAll>$9,900.00</TotalAll> 
		///			<TotalCollect>$0.00</TotalCollect> 
		///			<TotalPay>$9,900.00</TotalPay> 
		///			<TotalVoid>$9,900.00</TotalVoid> 
		///			<SortCol>FUNDS.TRANS_DATE</SortCol> 
		///			<Ascending>False</Ascending> 
		///		</PaymentHistory>
		/// </param>
		/// <param name="p_objErrOut">Collection of errors/messages.</param>
		/// <returns>
		///		true - success
		///		false - failure
		///	</returns>
		public bool GetPaymentHistory(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FundManager objFundManager = null;					//object of FundManager Class
			XmlElement objTargetElement = null;					//used for creating the Output Xml
			
			int iClaimId = 0 ;
			int iClaimantEid = 0 ;
			int iUnitId = 0 ;
			string sSortCol = string.Empty ;
			bool bAscending = false ;
            //Ankit-MITS 29909 Start Changes
            int iNextPageNum = 0;
            string sFilterValue = string.Empty;
            //Ankit-MITS-29909 End Changes
            //skhare7 R8 enhancement
            
            int iEntityId = 0;
            string sFormName = string.Empty;
            bool bFlag = false;
            //End
            XmlElement objCaller = null;
            int iCurrCode = 0;//RMA-6086
			try 
			{
				
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "ClaimId"); 
				iClaimId = Conversion.ConvertStrToInteger( objTargetElement.InnerXml );
				
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "ClaimantEid"); 
				iClaimantEid = Conversion.ConvertStrToInteger( objTargetElement.InnerXml );

				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "UnitId"); 
				iUnitId = Conversion.ConvertStrToInteger( objTargetElement.InnerXml );

				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "SortCol"); 
				sSortCol = objTargetElement.InnerXml;

				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "Ascending"); 
				bAscending  = Conversion.ConvertStrToBool( objTargetElement.InnerXml );
                //skhare7 R8 enhancement

                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "EntityId");
                iEntityId = Conversion.CastToType<int>((objTargetElement.InnerXml),out bFlag);
                //End
				
                //Ankit-MITS 29909 Start Changes
                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "NextPageNumber");
                iNextPageNum = Conversion.CastToType<int>((objTargetElement.InnerXml), out bFlag);

                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "FilterValue");
                sFilterValue = (objTargetElement.InnerXml);                                                
                //Ankit-MITS 29909 End Changes
				//RMA-6086
                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//CurrencyType");
                iCurrCode = Conversion.CastToType<int>((objTargetElement.InnerXml), out bFlag);

                if (iCurrCode == 0)
                {
                    objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//SessionCurrencyType");
                    iCurrCode = Conversion.CastToType<int>((objTargetElement.InnerXml), out bFlag);
                }
                //RMA-6086
                    objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);
                objCaller = (XmlElement)p_objXmlIn.SelectSingleNode("//caller");
				//Calling the GetPaymentHistory method to get the output xml document
				//Nikhil Garg		Dated: 27-Jan-2006
				//Passing the User Login object for checking for View Payment History Permission
                if (objCaller == null)
                {
// akaushik5 Commented for Performance Improvement Starts
                    //objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "ClaimId");
                    //iClaimId = Conversion.ConvertStrToInteger(objTargetElement.InnerXml);

                    //objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "ClaimantEid");
                    //iClaimantEid = Conversion.ConvertStrToInteger(objTargetElement.InnerXml);
// akaushik5 Commented for Performance Improvement Ends

                    //Ankit MITS 29909 - Start changes- Change in method signature
                //p_objXmlOut = objFundManager.GetPaymentHistory(iClaimId, iClaimantEid, iUnitId, sSortCol, bAscending, base.userLogin, iEntityId);
                    p_objXmlOut = objFundManager.GetPaymentHistory(iClaimId, iClaimantEid, iUnitId, sSortCol, bAscending, base.userLogin, iEntityId, iNextPageNum, sFilterValue, iCurrCode);//RMA-6086
                //Ankit-MITS 29909 End changes
                }
                else if (objCaller != null && (objCaller.InnerText == "MobileAdjuster" || objCaller.InnerText == "MobilityAdjuster"))
                {
                    iClaimId = objFundManager.getClaimId(p_objXmlIn.SelectSingleNode("//ClaimNumber").InnerText.ToString());
                    p_objXmlOut = objFundManager.GetPaymentHistoryForMobileAdjuster(iClaimId, iClaimantEid, iUnitId, sSortCol, bAscending, base.userLogin, iEntityId, objCaller.InnerText);//Aman Mobile Adjuster
                }
				return true;
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.Error);
				return false;
			}
			
			catch (Exception p_objException)
			{
				p_objErrOut.Add(p_objException ,Globalization.GetString("FundManagementAdaptor.GetPaymentHistory.Error", base.ClientId) , BusinessAdaptorErrorType.Error); 
				return false;				
			}

			finally
			{
				if(objFundManager != null)
				{
					objFundManager.Dispose();
					objFundManager=null;
				}
				objTargetElement=null;
			}

		}

		/// <summary>
		/// Wrapper to FundManager.GetAutoCheckList of application, Returns the automatic checks. 
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input Xml has following format :
		///		<GetPaymentHistory>
		///			<ClaimId>17</ClaimId> 
		///			<ClaimantEid>0</ClaimantEid> 
		///			<UnitId>0</UnitId> 
		///			<SortCol/> 
		///			<Ascending/> 
		///		</GetPaymentHistory>
		///	</param>
		/// <param name="p_objXmlOut">
		/// </param>
		/// <param name="p_objErrOut">Collection of errors/messages.</param>
		/// <returns>
		///		true - success
		///		false - failure
		///	</returns>
		public bool GetAutoCheckList(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FundManager objFundManager = null;					//object of FundManager Class
			XmlElement objTargetElement = null;					//used for creating the Output Xml
			
			int iClaimId = 0 ;
			int iClaimantEid = 0 ;
			int iUnitId = 0 ;
            //skhare7 R8 combined Payment Setup
            int iEntityId = 0;
            int iCurrCode = 0;//RMA-6086
            //end
            		//abisht MITS 10458
            		string sSortCol = string.Empty;
            		bool bAscending = false;
            		//abisht MITS 10458
                    try
                    {
                        objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "ClaimId");
                        iClaimId = Conversion.ConvertStrToInteger(objTargetElement.InnerXml);

                        objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "ClaimantEid");
                        iClaimantEid = Conversion.ConvertStrToInteger(objTargetElement.InnerXml);

                        objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "UnitId");
                        iUnitId = Conversion.ConvertStrToInteger(objTargetElement.InnerXml);
                        //skhare7 r8 Combined Payment
                        objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "EntityId");
                        iEntityId = Conversion.ConvertStrToInteger(objTargetElement.InnerXml);

                        //abisht MITS 10458
                        objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "SortCol");
                        sSortCol = objTargetElement.InnerXml;

                        objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "Ascending");
                        bAscending = Conversion.ConvertStrToBool(objTargetElement.InnerXml);
                        //abisht MITS 10458
						//RMA-6086
                        //objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//CurrencyType");
                        //iCurrCode = Conversion.ConvertStrToInteger(objTargetElement.InnerXml);
                        objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//TransactionHistoryCurrencyType");
                         iCurrCode = Conversion.ConvertStrToInteger(objTargetElement.InnerXml);
                        
						//RMA-6086
                        objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);

                        //Calling the GetAutoCheckList method to get the output xml document
                        //abisht MITS 10458 passed two additional parameters with the Function to support sorting                
                        p_objXmlOut = objFundManager.GetAutoCheckList(iClaimId, iClaimantEid, iUnitId, sSortCol, bAscending, iEntityId,iCurrCode);//RMA-6086

                        return true;
                    }
                    catch (RMAppException p_objException)
                    {
                        p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                        return false;
                    }

                    catch (Exception p_objException)
                    {
                        p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.GetAutoCheckList.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                        return false;
                    }

			finally
			{
				if(objFundManager != null)
				{
					objFundManager.Dispose();
					objFundManager=null;
				}
				objTargetElement=null;
			}

		}

        /// <summary>
        /// Wrapper to FundManager.GetInsuffReserve of application, Returns the automatic checks. 
        /// </summary>
        /// <param name="p_objXmlIn">
        /// Input parameters as xml
        ///	</param>
        /// <param name="p_objXmlOut">
        /// </param>
        /// <param name="p_objErrOut">Collection of errors/messages.</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>
        public bool GetInsuffReserve(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FundManager objFundManager = null;					//object of FundManager Class
            XmlElement objTargetElement = null;					//used for creating the Output Xml


            try
            {

                objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);

                p_objXmlOut = objFundManager.GetInsuffReserve(base.GetSessionObject());
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.GetInsuffReserve.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }

            finally
            {
                if (objFundManager != null)
                {
                    objFundManager.Dispose();
                    objFundManager = null;
                }
                objTargetElement = null;
            }

        }              

		/// <summary>
		/// Create report for all transactions
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// Output structure is as follows-:
		/// <PaymentHistoryReport><File Filename=""></File></PaymentHistoryReport>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool CreatePaymentHistoryReport(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			PaymentHistory objPayHis = null;
			MemoryStream objMemory = null;
			
			XmlElement objElement = null;
			XmlElement objTempElement = null;
			
			int iClaimId = 0 ;
			int iClaimantEid = 0 ;
			int iUnitId = 0 ;
			string sSortCol = string.Empty ;
            //string sSubTitle = string.Empty;
            string sFormTitle = string.Empty;         //pmittal5  MITS 9906  04/24/2008
			bool bAscending = false ;
int iEntityId = 0;
            bool bFlag = false;
            string s_FilterExp = string.Empty;    // MITS Manish added for filter expression
            int iCurrCode = 0;//RMA-6086
			try
			{
                //nadim for 14476..passing Dsn name user login and password also
                //objPayHis = new PaymentHistory(userLogin.objRiskmasterDatabase.ConnectionString);
                //objPayHis = new PaymentHistory(userLogin.objRiskmasterDatabase.ConnectionString,userLogin.objRiskmasterDatabase.DataSourceName,userLogin.LoginName,userLogin.Password);
                objPayHis = new PaymentHistory(userLogin.objRiskmasterDatabase.ConnectionString, userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);
                //nadim for 14476

				objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "ClaimId"); 
				iClaimId = Conversion.ConvertStrToInteger(objElement.InnerXml);

				objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "ClaimantEid"); 
				iClaimantEid = Conversion.ConvertStrToInteger(objElement.InnerXml);

				objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "UnitId"); 
				iUnitId = Conversion.ConvertStrToInteger(objElement.InnerXml);

				objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "SortCol"); 
                // akaushik5 Changed for MITS 37344 Starts
                //sSortCol = objElement.InnerXml;
                sSortCol = FundManagementAdaptor.GetSortColumn(objElement.InnerXml);
                // akaushik5 Changed for MITS 37344 Ends

				objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "Ascending"); 
				bAscending  = Conversion.ConvertStrToBool(objElement.InnerXml);
                //pmittal5  MITS 9906  04/24/2008  -Start
                //objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "SubTitle");
                //sSubTitle  = objElement.InnerXml;
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "FormTitle");
                sFormTitle = objElement.InnerText;
			
                           objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "EntityId");
                iEntityId = Conversion.CastToType<int>((objElement.InnerXml), out bFlag);
                //MITS Manish added filter expression
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "FilterExp");
                s_FilterExp = objElement.InnerText;
				//RMA-6086
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//CurrencyType");
                iCurrCode = Conversion.CastToType<int>((objElement.InnerXml), out bFlag);
                //Added:Yukti, RMA- 360, DT:07/02/2014
                //MITS Manish added filter expression
				//objPayHis.PaymentHistoryReport(iClaimId, iClaimantEid, iUnitId, sSortCol, bAscending, sSubTitle, out objMemory);
                //objPayHis.PaymentHistoryReport(iClaimId, iClaimantEid, iUnitId, sSortCol, bAscending, sFormTitle, s_FilterExp, out objMemory, iEntityId);
                objPayHis.PaymentHistoryReport(iClaimId, iClaimantEid, iUnitId, sSortCol, bAscending, sFormTitle, s_FilterExp, out objMemory, iEntityId, base.userLogin,iCurrCode);//RMA-6086
                //pmittal5  MITS 9906  04/24/2008  -End
                //Ended:Yukti, RMA- 360, DT:07/02/2014

				objTempElement = p_objXmlOut.CreateElement("PaymentHistoryReport");
				p_objXmlOut.AppendChild(objTempElement);
				objTempElement = p_objXmlOut.CreateElement("File");
				objTempElement.SetAttribute("Filename",Path.GetFileName(Path.GetTempFileName()).Replace(".tmp",".pdf"));
				objTempElement.InnerText = Convert.ToBase64String(objMemory.ToArray());
				p_objXmlOut.FirstChild.AppendChild(objTempElement);
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("FundManagementAdaptor.CreatePaymentHistoryReport.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objPayHis=null;
				objMemory=null;
				objElement=null;
				objTempElement = null;
			}
			return true;
		}

		#endregion 
		#region FundsCheck
		/// Name			: LoadChecks4Void
		/// Author			: Raman Bhatia
		/// Date Created	: 04-August-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Wrapper to FundsCheck.LoadChecks4Void of application
		/// layer that loads check information for all the checks which have to be voided. 
		/// </summary>
		/// <param name="p_objXmlDocIn"> 
		///	Input paramters in xml format.
		///	Structure:
		///		<FundsCheck>
		///        <MarkCleared>0</MarkCleared>
		///			<FromDate>10/10/2004</FromDate>
		///			<ToDate>10/10/2005</ToDate>
		///			<UseCollections>false</UseCollections>
		///			<NonPrintedOnly>false</NonPrintedOnly>
		///			<CompanyEID>0</CompanyEID>
		///			<NotAttached>0</NotAttached>
		///		</FundsCheck>
		/// </param>
		/// <param name="p_objXmlDocOut"> 
		///		<Checks>
		///			<Check TransNumber CheckDate CtlNumber LastName FirstName ClaimNumber Amount PaymentFlag AccountName ClaimId>TransId</Check>
		///			<Check TransNumber CheckDate CtlNumber LastName FirstName ClaimNumber Amount PaymentFlag AccountName ClaimId>TransId</Check>
		///		</Checks>
		///	</param>
		/// <param name="p_objErrOut">
		///	Collection of errors/messages.	
		///	</param>
		/// <returns>
		///		true - success
		///		false - failure
		///	</returns>
		public bool LoadChecks4Void  (XmlDocument p_objXmlDocIn,ref XmlDocument p_objXmlDocOut,
			ref BusinessAdaptorErrors p_objErrOut)
		{
			bool bReturnValue = false;
			string sFromDate = "";
			string sToDate = "";
			bool bIsNew = false;
			bool bUseCollections = false;
			bool bNonPrintedOnly = false;
            bool bExcludePrintedPayments = false;
			int iCompanyEID = 0;
			int iNotAttached = 0;
            //rsushilaggar added iVoidReasonFlag Void payment Reason 05/21/2010 MITS 19970
            int iVoidReasonFlag; 
			XmlNode objXmlNode = null;
			FundsCheck objFundsCheck = null;
			XmlDocument objXmlDoc = null;
			XmlElement objXmlElement = null;
            int iAccountID = -1;

			try
			{                
				if (!userLogin.IsAllowedEx(RMB_FUNDS,RMPermissions.RMO_ACCESS))
					throw new PermissionViolationException(RMPermissions.RMO_ACCESS,RMB_FUNDS);
				else if (!userLogin.IsAllowedEx(RMB_FUNDS_VOIDCHK,RMPermissions.RMO_ACCESS))
					throw new PermissionViolationException(RMPermissions.RMO_ACCESS,RMB_FUNDS_VOIDCHK);

                if (!userLogin.IsAllowedEx(RMB_FUNDS_VOIDCHK, RMO_FUNDS_VOID_PRINTED_CHECK))
                {
                    bExcludePrintedPayments = true;
                }

				objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/FromDate");
				sFromDate = objXmlNode.InnerText;

				objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/ToDate");
				sToDate = objXmlNode.InnerText; 

				if(sFromDate=="oneweekbefore" && sToDate=="today")
				{
					bIsNew = true;
					sToDate = Conversion.ConvertStrToLong(Conversion.GetDate(DateTime.Now.Date.ToString())).ToString();
					sFromDate = Conversion.ConvertStrToLong(Conversion.GetDate(DateTime.Now.AddDays(-6).Date.ToString())).ToString(); 
				}
				
				objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/UseCollections");
				bUseCollections  = Conversion.ConvertStrToBool(objXmlNode.InnerText);

				objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/NonPrintedOnly");
				bNonPrintedOnly  = Conversion.ConvertStrToBool(objXmlNode.InnerText);

				objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/CompanyEID");
				iCompanyEID = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

				objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/NotAttached");
				iNotAttached = Conversion.ConvertStrToInteger(objXmlNode.InnerText);
                //smishra25:start :Bank account filter
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/AccountID");
                iAccountID = Conversion.ConvertStrToInteger(objXmlNode.InnerText);
                //smishra25:end :Bank account filter                
				objFundsCheck = new FundsCheck(base.userLogin.objRiskmasterDatabase.DataSourceName , base.userLogin.LoginName, base.userLogin.Password,base.userLogin.UserId,base.userLogin.GroupId, base.ClientId);//rkaur27

                // Mits 10138 : rahul 
                objFundsCheck.m_userLoginIsAllowedEx = userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_ALLOW_VOID_CLOSED_CLAIM);
                				
				objXmlDoc = new XmlDocument();
                objXmlDoc = objFundsCheck.LoadChecks4Void(sFromDate, sToDate, bUseCollections, bNonPrintedOnly, iCompanyEID, iNotAttached, bExcludePrintedPayments, iAccountID);
				
				objXmlElement = p_objXmlDocOut.CreateElement("FundsCheck");
				p_objXmlDocOut.AppendChild(objXmlElement);
				objXmlElement.InnerXml = objXmlDoc.OuterXml;
				
				if(bIsNew)
				{
					objXmlElement.SetAttribute("FromDate" , Conversion.GetDBDateFormat(sFromDate,"d")); 
					objXmlElement.SetAttribute("ToDate" , Conversion.GetDBDateFormat(sToDate,"d")); 
				}
				else
				{
					objXmlElement.SetAttribute("FromDate" , sFromDate); 
					objXmlElement.SetAttribute("ToDate" , sToDate); 
				}

                //rsushilaggar added iVoidReasonFlag Void payment Reason 05/21/2010 MITS 19970
                iVoidReasonFlag = objFundsCheck.GetVoidReasonFlag();
                

                XmlNode m_objNodeEnableVoidReason=p_objXmlDocOut.CreateElement("EnableVoidReason");
                m_objNodeEnableVoidReason.InnerText = iVoidReasonFlag.ToString();
                p_objXmlDocOut.SelectSingleNode("//FundsCheck").AppendChild(m_objNodeEnableVoidReason);
                //rsushilaggar -End

				//loading company data and merging with the checks data
				
				objXmlDoc = objFundsCheck.GetCompanyNames();
				objXmlElement = p_objXmlDocOut.CreateElement("CompanyInformation");
				p_objXmlDocOut.SelectSingleNode("//FundsCheck").AppendChild(objXmlElement);
				objXmlElement.InnerXml = objXmlDoc.SelectSingleNode("//Companies").InnerXml;

                //Ankit Start : Financial Enhancements - Void Code Reason change
                objXmlDoc = objFundsCheck.GetVoidReasonCode();
                objXmlElement = p_objXmlDocOut.CreateElement("VoidReasonCodeInformation");
                p_objXmlDocOut.SelectSingleNode("//FundsCheck").AppendChild(objXmlElement);
                objXmlElement.InnerXml = objXmlDoc.SelectSingleNode("//VoidReasonCodesVal").InnerXml;
                //Ankit End

                //smishra25:Get Bank account names
                GetBankOrSubBankAccounts(ref objXmlDoc, ref objXmlElement, ref p_objXmlDocOut, ref objFundsCheck);
				bReturnValue = true;								
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.Error);
				return false;
			}
			
			catch (Exception p_objException)
			{
				p_objErrOut.Add(p_objException ,Globalization.GetString("FundManagementAdaptor.LoadChecks4Void.error", base.ClientId) , BusinessAdaptorErrorType.Error); 
				return false;				
			}
			finally
			{
				objXmlNode = null;
				if (objFundsCheck != null)
				{
					objFundsCheck.Dispose();
					objFundsCheck = null;	
				}
				
			}

			return bReturnValue;
		}

		/// Name			: LoadChecks4MarkCleared
		/// Author			: Raman Bhatia
		/// Date Created	: 04-August-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Wrapper to FundsCheck.LoadChecks4MarkCleared of application
		/// layer that loads check information for all the checks which have to be marked cleared. 
		/// </summary>
		/// <param name="p_objXmlDocIn"> 
		///	Input paramters in xml format.
		///	Structure:
		///		<FundsCheck>
		///        <MarkCleared>0</MarkCleared>
		///			<FromDate>10/10/2004</FromDate>
		///			<ToDate>10/10/2005</ToDate>
		///			<UseCollections>false</UseCollections>
		///			<NonPrintedOnly>false</NonPrintedOnly>
		///			<CompanyEID>0</CompanyEID>
		///			<NotAttached>0</NotAttached>
		///		</FundsCheck>
		/// </param>
		/// <param name="p_objXmlDocOut"> 
		///		<Checks>
		///			<Check TransNumber CheckDate CtlNumber LastName FirstName ClaimNumber Amount PaymentFlag AccountName ClaimId>TransId</Check>
		///			<Check TransNumber CheckDate CtlNumber LastName FirstName ClaimNumber Amount PaymentFlag AccountName ClaimId>TransId</Check>
		///		</Checks>
		///	</param>
		/// <param name="p_objErrOut">
		///	Collection of errors/messages.	
		///	</param>
		/// <returns>
		///		true - success
		///		false - failure
		///	</returns>
		public bool LoadChecks4MarkCleared  (XmlDocument p_objXmlDocIn,ref XmlDocument p_objXmlDocOut,
			ref BusinessAdaptorErrors p_objErrOut)
		{
			bool bReturnValue = false;
			string sFromDate = "";
			string sToDate = "";
			bool bIsNew = false;
			bool bUseCollections = false;
			bool bNonPrintedOnly = false;
			int iCompanyEID = 0;
			int iNotAttached = 0;
			XmlNode objXmlNode = null;
			FundsCheck objFundsCheck = null;
			XmlDocument objXmlDoc = null;
			XmlElement objXmlElement = null;
            int iAccountID = -1;


			try
			{
				if (!userLogin.IsAllowedEx(RMB_FUNDS_CLEARCHK,RMPermissions.RMO_ACCESS))
					throw new PermissionViolationException(RMPermissions.RMO_ACCESS,RMB_FUNDS_CLEARCHK);
			
				objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/FromDate");
				sFromDate = objXmlNode.InnerText;

				objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/ToDate");
				sToDate = objXmlNode.InnerText; 

				if(sFromDate=="oneweekbefore" && sToDate=="today")
				{
					bIsNew = true;
					sToDate = Conversion.ConvertStrToLong(Conversion.GetDate(DateTime.Now.Date.ToString())).ToString();
					sFromDate = Conversion.ConvertStrToLong(Conversion.GetDate(DateTime.Now.AddDays(-6).Date.ToString())).ToString(); 
				}
				objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/UseCollections");
				bUseCollections  = Conversion.ConvertStrToBool(objXmlNode.InnerText);

				objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/NonPrintedOnly");
				bNonPrintedOnly  = Conversion.ConvertStrToBool(objXmlNode.InnerText);

				objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/CompanyEID");
				iCompanyEID = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

				objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/NotAttached");
				iNotAttached = Conversion.ConvertStrToInteger(objXmlNode.InnerText);
                //smishra25:start :Bank account filter
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/AccountID");
                iAccountID = Conversion.ConvertStrToInteger(objXmlNode.InnerText);
                //smishra25:end :Bank account filter
				objFundsCheck = new FundsCheck(base.userLogin.objRiskmasterDatabase.DataSourceName , base.userLogin.LoginName, base.userLogin.Password,base.userLogin.UserId,base.userLogin.GroupId, base.ClientId);//rkaur27
				
				objXmlDoc = new XmlDocument();
                objXmlDoc = objFundsCheck.LoadChecks4MarkCleared(sFromDate, sToDate, bUseCollections, bNonPrintedOnly, iCompanyEID, iNotAttached, false, iAccountID);
				 					
				objXmlElement = p_objXmlDocOut.CreateElement("FundsCheck");
				p_objXmlDocOut.AppendChild(objXmlElement);
				objXmlElement.InnerXml = objXmlDoc.OuterXml;

				if(bIsNew)
				{
					objXmlElement.SetAttribute("FromDate" , Conversion.GetDBDateFormat(sFromDate,"d")); 
					objXmlElement.SetAttribute("ToDate" , Conversion.GetDBDateFormat(sToDate,"d")); 
				}
				else
				{
					objXmlElement.SetAttribute("FromDate" , sFromDate); 
					objXmlElement.SetAttribute("ToDate" , sToDate); 
				}

				//loading company data and merging with the checks data
				
				objXmlDoc = objFundsCheck.GetCompanyNames();
				objXmlElement = p_objXmlDocOut.CreateElement("CompanyInformation");
				p_objXmlDocOut.SelectSingleNode("//FundsCheck").AppendChild(objXmlElement);
				objXmlElement.InnerXml = objXmlDoc.SelectSingleNode("//Companies").InnerXml;

                //Ankit Start : Financial Enhancements - Void Code Reason change
                objXmlDoc = objFundsCheck.GetVoidReasonCode();
                objXmlElement = p_objXmlDocOut.CreateElement("VoidReasonCodeInformation");
                p_objXmlDocOut.SelectSingleNode("//FundsCheck").AppendChild(objXmlElement);
                objXmlElement.InnerXml = objXmlDoc.SelectSingleNode("//VoidReasonCodesVal").InnerXml;
                //Ankit End

               //smishra25:Get Bank account names
                GetBankOrSubBankAccounts(ref objXmlDoc, ref objXmlElement, ref p_objXmlDocOut, ref objFundsCheck);
				
				bReturnValue = true;								
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.Error);
				return false;
			}
			
			catch (Exception p_objException)
			{
				p_objErrOut.Add(p_objException ,Globalization.GetString("FundManagementAdaptor.LoadChecks4MarkCleared.error", base.ClientId) , BusinessAdaptorErrorType.Error); 
				return false;				
			}
			finally
			{
				objXmlNode = null;
				if (objFundsCheck != null)
				{
					objFundsCheck.Dispose();
					objFundsCheck = null;
				}
				
			}

			return bReturnValue;
		}
        /// Name			: LoadChecks4MarkUnCleared
        /// Author			: Ashutosh
        /// Date Created	: 25-Sep-2011
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Wrapper to FundsCheck.LoadChecks4MarkUnCleared of application
        /// layer that loads check information for all the checks which have to be marked un cleared. 
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        ///	Input paramters in xml format.
        ///	Structure:
        ///		<FundsCheck>
        ///        <MarkCleared>0</MarkCleared>
        ///			<FromDate>10/10/2004</FromDate>
        ///			<ToDate>10/10/2005</ToDate>
        ///			<UseCollections>false</UseCollections>
        ///			<NonPrintedOnly>false</NonPrintedOnly>
        ///			<CompanyEID>0</CompanyEID>
        ///			<NotAttached>0</NotAttached>
        ///		</FundsCheck>
        /// </param>
        /// <param name="p_objXmlDocOut"> 
        ///		<Checks>
        ///			<Check TransNumber CheckDate CtlNumber LastName FirstName ClaimNumber Amount PaymentFlag AccountName ClaimId>TransId</Check>
        ///			<Check TransNumber CheckDate CtlNumber LastName FirstName ClaimNumber Amount PaymentFlag AccountName ClaimId>TransId</Check>
        ///		</Checks>
        ///	</param>
        /// <param name="p_objErrOut">
        ///	Collection of errors/messages.	
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>
        ///	
        public bool LoadChecks4MarkUnCleared(XmlDocument p_objXmlDocIn, ref XmlDocument p_objXmlDocOut,
            ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bReturnValue = false;
            string sFromDate = "";
            string sToDate = "";
            bool bIsNew = false;
            bool bUseCollections = false;
            bool bNonPrintedOnly = false;
            int iCompanyEID = 0;
            int iNotAttached = 0;
            XmlNode objXmlNode = null;
            FundsCheck objFundsCheck = null;
            XmlDocument objXmlDoc = null;
            XmlElement objXmlElement = null;
            int iAccountID = -1;


            try
            {
                if (!userLogin.IsAllowedEx(RMB_FUNDS, RMPermissions.RMO_ACCESS))
                    throw new PermissionViolationException(RMPermissions.RMO_ACCESS, RMB_FUNDS);
                else if (!userLogin.IsAllowedEx(RMB_FUNDS_UnClearChecks, RMPermissions.RMO_ACCESS))
                    throw new PermissionViolationException(RMPermissions.RMO_ACCESS, RMB_FUNDS_UnClearChecks);


                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/FromDate");
                sFromDate = objXmlNode.InnerText;

                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/ToDate");
                sToDate = objXmlNode.InnerText;

                if (sFromDate == "oneweekbefore" && sToDate == "today")
                {
                    bIsNew = true;
                    sToDate = Conversion.ConvertStrToLong(Conversion.GetDate(DateTime.Now.Date.ToString())).ToString();
                    sFromDate = Conversion.ConvertStrToLong(Conversion.GetDate(DateTime.Now.AddDays(-6).Date.ToString())).ToString();
                }
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/UseCollections");
                bUseCollections = Conversion.ConvertStrToBool(objXmlNode.InnerText);

                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/NonPrintedOnly");
                bNonPrintedOnly = Conversion.ConvertStrToBool(objXmlNode.InnerText);

                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/CompanyEID");
                iCompanyEID = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/NotAttached");
                iNotAttached = Conversion.ConvertStrToInteger(objXmlNode.InnerText);
                //smishra25:start :Bank account filter
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/AccountID");
                iAccountID = Conversion.ConvertStrToInteger(objXmlNode.InnerText);
                //smishra25:end :Bank account filter
                objFundsCheck = new FundsCheck(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);//rkaur27

                objXmlDoc = new XmlDocument();
                objXmlDoc = objFundsCheck.LoadChecks4MarkUnCleared(sFromDate, sToDate, bUseCollections, bNonPrintedOnly, iCompanyEID, iNotAttached, false, iAccountID);
              

                objXmlElement = p_objXmlDocOut.CreateElement("FundsCheck");
                p_objXmlDocOut.AppendChild(objXmlElement);
              
                objXmlElement.InnerXml = objXmlDoc.OuterXml;

                if (bIsNew)
                {
                    objXmlElement.SetAttribute("FromDate", Conversion.GetDBDateFormat(sFromDate, "d"));
                    objXmlElement.SetAttribute("ToDate", Conversion.GetDBDateFormat(sToDate, "d"));
                }
                else
                {
                    objXmlElement.SetAttribute("FromDate", sFromDate);
                    objXmlElement.SetAttribute("ToDate", sToDate);
                }

                //loading company data and merging with the checks data

                objXmlDoc = objFundsCheck.GetCompanyNames();
                objXmlElement = p_objXmlDocOut.CreateElement("CompanyInformation");
                p_objXmlDocOut.SelectSingleNode("//FundsCheck").AppendChild(objXmlElement);
                objXmlElement.InnerXml = objXmlDoc.SelectSingleNode("//Companies").InnerXml;

                //Ankit Start : Financial Enhancements - Void Code Reason change
                objXmlDoc = objFundsCheck.GetVoidReasonCode();
                objXmlElement = p_objXmlDocOut.CreateElement("VoidReasonCodeInformation");
                p_objXmlDocOut.SelectSingleNode("//FundsCheck").AppendChild(objXmlElement);
                objXmlElement.InnerXml = objXmlDoc.SelectSingleNode("//VoidReasonCodesVal").InnerXml;
                //Ankit End

                //smishra25:Get Bank account names
                GetBankOrSubBankAccounts(ref objXmlDoc, ref objXmlElement, ref p_objXmlDocOut, ref objFundsCheck);

                bReturnValue = true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.LoadChecks4MarkUnCleared.error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objXmlNode = null;
                if (objFundsCheck != null)
                {
                    objFundsCheck.Dispose();
                    objFundsCheck = null;
                }

            }

            return bReturnValue;
        }

        /// Name			: MarkChecksUnCleared
        /// Author			: Ashutosh Pandey
        /// Date Created	: 25-Sep-2011
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Wrapper to FundsCheck.MarkChecksCleared of application
        /// layer that mark the checks as Un cleared. 
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        ///	Input paramters in xml format.
        ///	Structure:
        ///		<FundsCheck>
        ///			<TransIds>3,4,5..</TransIds> 
        ///		</FundsCheck>
        /// </param>
        /// <param name="p_objXmlDocOut"> 
        ///		
        ///	</param>
        /// <param name="p_objErrOut">
        ///	Collection of errors/messages.	
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>

        public bool MarkChecksUnCleared(XmlDocument p_objXmlDocIn, ref XmlDocument p_objXmlDocOut,
              ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bReturnValue = false;
            string sTransIds = "";
            XmlNode objXmlNode = null;
            FundsCheck objFundsCheck = null;


            try
            {
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/TransIds");
                sTransIds = objXmlNode.InnerText;

                objFundsCheck = new FundsCheck(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);//rkaur27

                if (sTransIds != "")
                    bReturnValue = objFundsCheck.MarkChecksUnCleared(sTransIds);

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.MarkChecksUnCleared.error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objXmlNode = null;
                if (objFundsCheck != null)
                {
                    objFundsCheck.Dispose();
                    objFundsCheck = null;
                }

            }

            return bReturnValue;
        }
        public void GetBankOrSubBankAccounts(ref XmlDocument objXmlDoc, ref XmlElement objXmlElement, ref XmlDocument p_objXmlDocOut, ref FundsCheck objFundsCheck)
        {
            try
            {
                 //smishra25:Start for Bank account filter
                //Retrieve the security credential information to pass to the FundManager object
                string sDSNName = this.userLogin.objRiskmasterDatabase.DataSourceName;
                string sUserName =this.userLogin.LoginName;
                string sPassword = this.userLogin.Password;
                string sAccountCollection = string.Empty;
                string sAccount = string.Empty;
                string sAccountEid = string.Empty;
                int iUserId = this.userLogin.UserId;
                int iGroupId = this.userLogin.GroupId;
                ArrayList arrAcctList = new ArrayList();
                SysSettings objSettings = null;
               
            

                //Create an instance of the FundManager
                objSettings = new SysSettings(this.connectionString, base.ClientId); //Ash - cloud
                using (FundManager objFundMgr = new FundManager(sDSNName, sUserName, sPassword, iUserId, iGroupId, base.ClientId))
                {
                    arrAcctList = objFundMgr.GetAccounts(false);
                }
                           
               
                objXmlDoc = objFundsCheck.GetAccountNames(arrAcctList, false);
                //if (objSettings.UseFundsSubAcc)
                //{
                //    sAccountCollection = "SubBankAccounts";
                //    sAccount = "SubBankAccount";
                //    sAccountEid = "SubBankAccountId";
                //}
                //else
                //{
                    sAccountCollection = "BankAccounts";
                    sAccount = "BankAccount";
                    sAccountEid = "BankAccountId";
                //}
                objXmlElement = p_objXmlDocOut.CreateElement(sAccountCollection);
                p_objXmlDocOut.SelectSingleNode("//FundsCheck").AppendChild(objXmlElement);
                objXmlElement.InnerXml = objXmlDoc.SelectSingleNode("//" + sAccountCollection).InnerXml;
                //smishra25:End for Bank account filter
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
                
            }

            catch (Exception p_objException)
            {
                throw p_objException;
              
            }
        }
		
		/// Name			: GetCompanyNames
		/// Author			: Raman Bhatia
		/// Date Created	: 04-August-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Wrapper to FundsCheck.GetCompanyNames of application
		/// layer that gets the companies information
		/// </summary>
		/// <param name="p_objXmlDocIn"> 
		///	Input paramters in xml format.
		///	Structure:
		///		<FundsCheck>
		///     </FundsCheck>
		/// </param>
		/// <param name="p_objXmlDocOut"> 
		///		<Companies>
		///			<Company CompanyEid>LastName</Check>
		///			<Company CompanyEid>LastName</Check>
		///		</Companies>
		///	</param>
		/// <param name="p_objErrOut">
		///	Collection of errors/messages.	
		///	</param>
		/// <returns>
		///		true - success
		///		false - failure
		///	</returns>
		public bool GetCompanyNames  (XmlDocument p_objXmlDocIn,ref XmlDocument p_objXmlDocOut,
			ref BusinessAdaptorErrors p_objErrOut)
		{
			bool bReturnValue = false;
			FundsCheck objFundsCheck = null;

			try
			{
                objFundsCheck = new FundsCheck(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);//rkaur27
				
				p_objXmlDocOut = objFundsCheck.GetCompanyNames();
				 					
				bReturnValue = true;								
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.Error);
				return false;
			}
			
			catch (Exception p_objException)
			{
				p_objErrOut.Add(p_objException ,Globalization.GetString("FundManagementAdaptor.GetExperience.error", base.ClientId) , BusinessAdaptorErrorType.Error); 
				return false;				
			}
			finally
			{
				if (objFundsCheck != null)
				{
					objFundsCheck.Dispose();
					objFundsCheck = null;	
				}
				
			}

			return bReturnValue;
		}

		/// Name			: MarkChecksCleared
		/// Author			: Raman Bhatia
		/// Date Created	: 05-August-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Wrapper to FundsCheck.MarkChecksCleared of application
		/// layer that mark the checks as cleared. 
		/// </summary>
		/// <param name="p_objXmlDocIn"> 
		///	Input paramters in xml format.
		///	Structure:
		///		<FundsCheck>
		///			<TransIds>3,4,5..</TransIds> 
		///		</FundsCheck>
		/// </param>
		/// <param name="p_objXmlDocOut"> 
		///		
		///	</param>
		/// <param name="p_objErrOut">
		///	Collection of errors/messages.	
		///	</param>
		/// <returns>
		///		true - success
		///		false - failure
		///	</returns>
		public bool MarkChecksCleared  (XmlDocument p_objXmlDocIn,ref XmlDocument p_objXmlDocOut,
			ref BusinessAdaptorErrors p_objErrOut)
		{
			bool bReturnValue = false;
			string sTransIds = "";
			XmlNode objXmlNode = null;
			FundsCheck objFundsCheck = null;


			try
			{
				objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/TransIds");
				sTransIds = objXmlNode.InnerText;

                objFundsCheck = new FundsCheck(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);//rkaur27
				
				if (sTransIds!="")
					bReturnValue = objFundsCheck.MarkChecksCleared(sTransIds);
				 					
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.Error);
				return false;
			}
			
			catch (Exception p_objException)
			{
				p_objErrOut.Add(p_objException ,Globalization.GetString("FundManagementAdaptor.MarkChecksCleared.error", base.ClientId) , BusinessAdaptorErrorType.Error); 
				return false;				
			}
			finally
			{
				objXmlNode = null;
				if (objFundsCheck != null)
				{
					objFundsCheck.Dispose();
					objFundsCheck = null;
				}
				
			}

			return bReturnValue;
		}

        /// Name			: LoadChecks4Release
        /// Author			: Ashutosh K Pandey
        /// Date Created	: 25-Sep-2011
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Wrapper to FundsCheck.LoadChecks4Release of application
        /// layer that loads check information for all the checks which have to be Reset back to Print Status. 
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        ///	Input paramters in xml format.
        ///	Structure:
        ///		<FundsCheck>
        ///        <MarkCleared>0</MarkCleared>
        ///			<FromDate>10/10/2004</FromDate>
        ///			<ToDate>10/10/2005</ToDate>
        ///			<UseCollections>false</UseCollections>
        ///			<NonPrintedOnly>false</NonPrintedOnly>
        ///			<CompanyEID>0</CompanyEID>
        ///			<NotAttached>0</NotAttached>
        ///		</FundsCheck>
        /// </param>
        /// <param name="p_objXmlDocOut"> 
        ///		<Checks>
        ///			<Check TransNumber CheckDate CtlNumber LastName FirstName ClaimNumber Amount PaymentFlag AccountName ClaimId>TransId</Check>
        ///			<Check TransNumber CheckDate CtlNumber LastName FirstName ClaimNumber Amount PaymentFlag AccountName ClaimId>TransId</Check>
        ///		</Checks>
        ///	</param>
        /// <param name="p_objErrOut">
        ///	Collection of errors/messages.	
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>


        public bool LoadChecks4Release(XmlDocument p_objXmlDocIn, ref XmlDocument p_objXmlDocOut,
      ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bReturnValue = false;
            string sFromDate = "";
            string sToDate = "";
            bool bIsNew = false;
            bool bUseCollections = false;
            bool bNonPrintedOnly = false;
            bool bExcludePrintedPayments = false;
            int iCompanyEID = 0;
            int iNotAttached = 0;
            //rsushilaggar added iVoidReasonFlag Void payment Reason 05/21/2010 MITS 19970
            int iVoidReasonFlag;
            XmlNode objXmlNode = null;
            FundsCheck objFundsCheck = null;
            XmlDocument objXmlDoc = null;
            XmlElement objXmlElement = null;
            int iAccountID = -1;
            int iBatchNumber = 0;

            try
            {
                if (!userLogin.IsAllowedEx(RMB_FUNDS, RMPermissions.RMO_ACCESS))
                    throw new PermissionViolationException(RMPermissions.RMO_ACCESS, RMB_FUNDS);
                // rrachev JIRA RMA-13119 Begin
                //else if (!userLogin.IsAllowedEx(RMB_FUNDS_VOIDCHK, RMPermissions.RMO_ACCESS))
                //    throw new PermissionViolationException(RMPermissions.RMO_ACCESS, RMB_FUNDS_VOIDCHK);
                // rrachev JIRA RMA-13119 End

                if (!userLogin.IsAllowedEx(RMB_FUNDS_VOIDCHK, RMO_FUNDS_VOID_PRINTED_CHECK))
                {
                    bExcludePrintedPayments = true;
                }

                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/FromDate");
                sFromDate = objXmlNode.InnerText;

                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/ToDate");
                sToDate = objXmlNode.InnerText;

                if (sFromDate == "oneweekbefore" && sToDate == "today")
                {
                    bIsNew = true;
                    sToDate = Conversion.ConvertStrToLong(Conversion.GetDate(DateTime.Now.Date.ToString())).ToString();
                    sFromDate = Conversion.ConvertStrToLong(Conversion.GetDate(DateTime.Now.AddDays(-6).Date.ToString())).ToString();
                }

                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/UseCollections");
                bUseCollections = Conversion.ConvertStrToBool(objXmlNode.InnerText);

                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/NonPrintedOnly");
                bNonPrintedOnly = Conversion.ConvertStrToBool(objXmlNode.InnerText);

                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/CompanyEID");
                iCompanyEID = Conversion.ConvertStrToInteger(objXmlNode.InnerText);

                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/NotAttached");
                iNotAttached = Conversion.ConvertStrToInteger(objXmlNode.InnerText);
                //smishra25:start :Bank account filter
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/AccountID");
                iAccountID = Conversion.ConvertStrToInteger(objXmlNode.InnerText);
                //smishra25:end :Bank account filter                
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/BatchNumber");
                iBatchNumber = Conversion.ConvertStrToInteger(objXmlNode.InnerText);
                objFundsCheck = new FundsCheck(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);//rkaur27

                // Mits 10138 : rahul 
                objFundsCheck.m_userLoginIsAllowedEx = userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_ALLOW_VOID_CLOSED_CLAIM);

                objXmlDoc = new XmlDocument();
                objXmlDoc = objFundsCheck.LoadChecks4Release(sFromDate, sToDate, bUseCollections, bNonPrintedOnly, iCompanyEID, iNotAttached, bExcludePrintedPayments, iAccountID, iBatchNumber);

                objXmlElement = p_objXmlDocOut.CreateElement("FundsCheck");
                p_objXmlDocOut.AppendChild(objXmlElement);
                objXmlElement.InnerXml = objXmlDoc.OuterXml;

                if (bIsNew)
                {
                    objXmlElement.SetAttribute("FromDate", Conversion.GetDBDateFormat(sFromDate, "d"));
                    objXmlElement.SetAttribute("ToDate", Conversion.GetDBDateFormat(sToDate, "d"));
                }
                else
                {
                    objXmlElement.SetAttribute("FromDate", sFromDate);
                    objXmlElement.SetAttribute("ToDate", sToDate);
                }

                //rsushilaggar added iVoidReasonFlag Void payment Reason 05/21/2010 MITS 19970
                iVoidReasonFlag = objFundsCheck.GetVoidReasonFlag();


                XmlNode m_objNodeEnableVoidReason = p_objXmlDocOut.CreateElement("EnableVoidReason");
                m_objNodeEnableVoidReason.InnerText = iVoidReasonFlag.ToString();
                p_objXmlDocOut.SelectSingleNode("//FundsCheck").AppendChild(m_objNodeEnableVoidReason);
                //rsushilaggar -End

                //loading company data and merging with the checks data

                objXmlDoc = objFundsCheck.GetCompanyNames();
                objXmlElement = p_objXmlDocOut.CreateElement("CompanyInformation");
                p_objXmlDocOut.SelectSingleNode("//FundsCheck").AppendChild(objXmlElement);
                objXmlElement.InnerXml = objXmlDoc.SelectSingleNode("//Companies").InnerXml;



                GetBankOrSubBankAccounts_forReSetCheck(ref objXmlDoc, ref objXmlElement, ref p_objXmlDocOut, ref objFundsCheck);
                bReturnValue = true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.LoadChecks4Release.error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objXmlNode = null;
                if (objFundsCheck != null)
                {
                    objFundsCheck.Dispose();
                    objFundsCheck = null;
                }

            }

            return bReturnValue;
        }

        private void GetBankOrSubBankAccounts_forReSetCheck(ref XmlDocument objXmlDoc, ref XmlElement objXmlElement, ref XmlDocument p_objXmlDocOut, ref FundsCheck objFundsCheck)
        {
            try
            {
                //smishra25:Start for Bank account filter
                //Retrieve the security credential information to pass to the FundManager object
                string sDSNName = this.userLogin.objRiskmasterDatabase.DataSourceName;
                string sUserName = this.userLogin.LoginName;
                string sPassword = this.userLogin.Password;
                string sAccountCollection = string.Empty;
                string sAccount = string.Empty;
                string sAccountEid = string.Empty;
                int iUserId = this.userLogin.UserId;
                int iGroupId = this.userLogin.GroupId;
                ArrayList arrAcctList = new ArrayList();
                SysSettings objSettings = null;



                //Create an instance of the FundManager
                objSettings = new SysSettings(this.connectionString, base.ClientId); //Ash - cloud
                using (FundManager objFundMgr = new FundManager(sDSNName, sUserName, sPassword, iUserId, iGroupId, base.ClientId))
                {
                    arrAcctList = objFundMgr.GetAccounts_forReSetCheck(false);
                }


                objXmlDoc = objFundsCheck.GetAccountNames(arrAcctList, false);

                sAccountCollection = "BankAccounts";
                sAccount = "BankAccount";
                sAccountEid = "BankAccountId";

                objXmlElement = p_objXmlDocOut.CreateElement(sAccountCollection);
                p_objXmlDocOut.SelectSingleNode("//FundsCheck").AppendChild(objXmlElement);
                objXmlElement.InnerXml = objXmlDoc.SelectSingleNode("//" + sAccountCollection).InnerXml;

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;

            }

            catch (Exception p_objException)
            {
                throw p_objException;

            }
        }

		/// Name			: VoidChecks
		/// Author			: Raman Bhatia
		/// Date Created	: 05-August-2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// Wrapper to FundsCheck.VoidChecks of application
		/// layer that voids the checks. 
		/// </summary>
		/// <param name="p_objXmlDocIn"> 
		///	Input paramters in xml format.
		///	Structure:
		///		<FundsCheck>
		///			<TransIds>3,4,5..</TransIds> 
		///		</FundsCheck>
		/// </param>
		/// <param name="p_objXmlDocOut"> 
		///		
		///	</param>
		/// <param name="p_objErrOut">
		///	Collection of errors/messages.	
		///	</param>
		/// <returns>
		///		true - success
		///		false - failure
		///	</returns>
		public bool VoidChecks (XmlDocument p_objXmlDocIn,ref XmlDocument p_objXmlDocOut,
			ref BusinessAdaptorErrors p_objErrOut)
		{
			bool bReturnValue = false;
			string sTransIds = "";
			XmlNode objXmlNode = null;
			FundsCheck objFundsCheck = null;
            //Start rsushilaggar Void payment Reason 05/21/2010 MITS 19970
            string sVoidReason =string.Empty ;
            string sVoidReason_HTMLcomments = string.Empty;
            //end rsushilaggar
			try
			{
				objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/TransIds");
				sTransIds = objXmlNode.InnerText;
                //start rsushilaggar for Void Reason-05/21/2010 MITS 19970
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/VoidReason");
                if (objXmlNode != null)
                    sVoidReason = objXmlNode.InnerText;
                else
                    sVoidReason = "";

                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/VoidReason_HTMLComments");
                if (objXmlNode != null)
                    sVoidReason_HTMLcomments = objXmlNode.InnerText;
                else
                    sVoidReason_HTMLcomments = "";

                objFundsCheck = new FundsCheck(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);//rkaur27
                objFundsCheck.sVoidReason = sVoidReason;
                objFundsCheck.sVoidReason_HTMLComments = sVoidReason_HTMLcomments;
				if (sTransIds!="")
					bReturnValue = objFundsCheck.VoidChecks(sTransIds,base.m_userLogin);
                //End Rahul 					
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.Error);
				return false;
			}
			
			catch (Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.VoidChecks.error", base.ClientId), BusinessAdaptorErrorType.Error); 
				return false;				
			}
			finally
			{
				objXmlNode = null;
				if (objFundsCheck != null)
				{
					objFundsCheck.Dispose();
					objFundsCheck = null;
				}
				
			}

			return bReturnValue;
		}
        /// Name			: ReleaseCheck
        /// Author			: Ashutosh K pandey
        /// Date Created	: 25-Sep-2011
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Wrapper to FundsCheck.ReleaseChecks of application
        /// layer that voids the checks. 
        /// </summary>
        /// <param name="p_objXmlDocIn"> 
        ///	Input paramters in xml format.
        ///	Structure:
        ///		<FundsCheck>
        ///			<TransIds>3,4,5..</TransIds> 
        ///		</FundsCheck>
        /// </param>
        /// <param name="p_objXmlDocOut"> 
        ///		
        ///	</param>
        /// <param name="p_objErrOut">
        ///	Collection of errors/messages.	
        ///	</param>
        /// <returns>
        ///		true - success
        ///		false - failure
        ///	</returns>

        public bool ReleaseChecks(XmlDocument p_objXmlDocIn, ref XmlDocument p_objXmlDocOut,
       ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bReturnValue = false;
            string sTransIds = "";
            XmlNode objXmlNode = null;
            FundsCheck objFundsCheck = null;
            //Start rsushilaggar Void payment Reason 05/21/2010 MITS 19970
            string sVoidReason = string.Empty;
            string sVoidReason_HTMLcomments = string.Empty;
            //end rsushilaggar
            try
            {
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/TransIds");
                sTransIds = objXmlNode.InnerText;
                //start rsushilaggar for Void Reason-05/21/2010 MITS 19970
                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/VoidReason");
                if (objXmlNode != null)
                    sVoidReason = objXmlNode.InnerText;
                else
                    sVoidReason = "";

                objXmlNode = p_objXmlDocIn.SelectSingleNode("//FundsCheck/VoidReason_HTMLComments");
                if (objXmlNode != null)
                    sVoidReason_HTMLcomments = objXmlNode.InnerText;
                else
                    sVoidReason_HTMLcomments = "";

                objFundsCheck = new FundsCheck(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);//rkaur27
                objFundsCheck.sVoidReason = sVoidReason;
                objFundsCheck.sVoidReason_HTMLComments = sVoidReason_HTMLcomments;
                if (sTransIds != "")
                    bReturnValue = objFundsCheck.ReleaseChecks(sTransIds, base.m_userLogin);
                //End Rahul 					
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.ReleaseChecks.error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objXmlNode = null;
                if (objFundsCheck != null)
                {
                    objFundsCheck.Dispose();
                    objFundsCheck = null;
                }

            }

            return bReturnValue;
        }

		#endregion
		#region Payment Notification
		
		/// <summary>
		/// Wrapper to Application layer function GetCustomPaymentNotification()
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetCustomPayNotification(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			PaymentNotification objPayNot = null;
			try
			{
				//objPayNot = new PaymentNotification(userLogin.objRiskmasterDatabase.ConnectionString);
                objPayNot = new PaymentNotification(userLogin.objRiskmasterDatabase.ConnectionString, base.ClientId);
				p_objXmlOut = objPayNot.GetCutomPaymentNotification(p_objXmlIn);
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.CreatePaymentHistoryReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objPayNot = null;
			}
			return true;
		}

		/// <summary>
		/// Wrapper to Application layer function NotifyUserOfFuturePayments()
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool NotifyUserOfFuturePayments(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			PaymentNotification objPayNot = null;
			try
			{
				//objPayNot = new PaymentNotification(userLogin.objRiskmasterDatabase.ConnectionString);
                objPayNot = new PaymentNotification(userLogin.objRiskmasterDatabase.ConnectionString, base.ClientId);
				p_objXmlOut = objPayNot.NotifyUserOfFuturePayments(userID);
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.NotifyUserOfFuturePayments.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objPayNot = null;
			}
			return true;
		}
		/// <summary>
		/// Create report for all transactions
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// Output structure is as follows-:
		/// <PaymentNotificationReport><File Filename=""></File></PaymentNotificationReport>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool CreatePaymentNotificationReport(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			PaymentNotification objPayNot = null;
			MemoryStream objMemory = null;

			XmlElement objTempElement = null;

			try
			{
				//objPayNot = new PaymentNotification(userLogin.objRiskmasterDatabase.ConnectionString);
                objPayNot = new PaymentNotification(userLogin.objRiskmasterDatabase.ConnectionString, base.ClientId);
				p_objXmlIn = objPayNot.GetCutomPaymentNotification(p_objXmlIn);
				objPayNot.CreateNotificationReport(p_objXmlIn, out objMemory);

				objTempElement = p_objXmlOut.CreateElement("PaymentNotificationReport");
				p_objXmlOut.AppendChild(objTempElement);
				objTempElement = p_objXmlOut.CreateElement("File");
				objTempElement.SetAttribute("Filename",Path.GetFileName(Path.GetTempFileName()).Replace(".tmp",".pdf"));
				objTempElement.InnerText = Convert.ToBase64String(objMemory.ToArray());
				p_objXmlOut.FirstChild.AppendChild(objTempElement);
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.CreatePaymentHistoryReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objPayNot=null;
				objMemory=null;
				objTempElement = null;
			}
			return true;
		}

		//kchoudhary2, 12th Dec, MITS 8431 starts
		public bool CreateTransReport(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FundManager objFundManager = null;
			MemoryStream objMemory = null;
			
			XmlElement objElement = null;
			
			int itransID = 0 ;
			
			try
			{
                objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);

				objElement = (XmlElement)p_objXmlIn.SelectSingleNode(@"//TransId"); 
				itransID  = Conversion.ConvertObjToInt(objElement.InnerXml, base.ClientId);

				objMemory = objFundManager.CreateTransReport(itransID);

				//create the output xml document containing the data in Base64 format
				p_objXmlOut=new XmlDocument();
				objElement=p_objXmlOut.CreateElement("TransactionReport");
				p_objXmlOut.AppendChild(objElement);
				objElement=p_objXmlOut.CreateElement("File");				
				if(objMemory != null)
					objElement.InnerText=Convert.ToBase64String(objMemory.ToArray());
				p_objXmlOut.FirstChild.AppendChild(objElement);

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.CreateTransReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objFundManager != null)
                    objFundManager.Dispose();
                if (objMemory != null)
                    objMemory.Dispose();
				objElement=null;
			}
			return true;
		}

		//kchoudhary2, 12th Dec, MITS 8431 ends
		
		/// <summary>
		/// Create Startup report for all transactions
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// Output structure is as follows-:
		/// <PaymentNotificationReport><File Filename=""></File></PaymentNotificationReport>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool CreateStartupPaymentNotificationReport(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			PaymentNotification objPayNot = null;
			MemoryStream objMemory = null;

			XmlElement objTempElement = null;

			try
			{
				//objPayNot = new PaymentNotification(userLogin.objRiskmasterDatabase.ConnectionString);
                objPayNot = new PaymentNotification(userLogin.objRiskmasterDatabase.ConnectionString, base.ClientId);
				p_objXmlIn = objPayNot.NotifyUserOfFuturePayments(userID);
				objPayNot.CreateNotificationReport(p_objXmlIn, out objMemory);

				p_objXmlOut = new XmlDocument();
				objTempElement = p_objXmlOut.CreateElement("PaymentNotificationReport");
				p_objXmlOut.AppendChild(objTempElement);
				objTempElement = p_objXmlOut.CreateElement("File");
				objTempElement.SetAttribute("Filename",Path.GetFileName(Path.GetTempFileName()).Replace(".tmp",".pdf"));
				objTempElement.InnerText = Convert.ToBase64String(objMemory.ToArray());
				p_objXmlOut.FirstChild.AppendChild(objTempElement);
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.CreatePaymentHistoryReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objPayNot=null;
				objMemory=null;
				objTempElement = null;
			}
			return true;
		}

		#endregion
		#region GetReservesInfo
		/// Name		: GetReservesInfo
		/// Author		: Raman Bhatia
		/// Date Created: 06-Dec-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to FundManager.GetReservesInfo method.
		///		This method will call the methods that Get reserves information for a particular transaction type	
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<FundManager>
		///				<ClaimId>ClaimId</ClaimId>
		///				<ClaimantEid>ClaimantId</ClaimantEid>
		///				<UnitId>UnitId</UnitId>
		///				<TransTypeCode>TransTypeCode</TransTypeCode>
		///			</FundManager>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document.
		///		The structure of output XML document would be:
		///		<Document>
		///			<ReservesInformation>
		///				<ReserveType></ReserveType>
		///				<ReserveBalance></ReserveBalance>
		///			</ReservesInformation>
		///		</Document>
		/// </param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool GetReservesInfo(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FundManager objFundManager = null;					//object of FundManager Class
			XmlElement objTargetElement = null;					//used for creating the Output Xml
			int iTransTypeCode = 0;							    //Trans Type Code passed in input Xml document
			int iClaimId = 0;									//ClaimId passed in input Xml document
			int iClaimantEid = 0;								//ClaimantEid passed in input Xml document
			int iUnitId = 0;									//UnitId passed in input Xml document
			int iTransID = 0;
            int iReserveTypeCode = 0; //added by rkaur7 - MITS 17241
            int iPolicyId = 0;
            int iCovTypeCode = 0;
            int iPolicyUnitRowId = 0;//rupal:r8 unit implementation
            int iPmtCurrCode = 0;//Deb Multi Currency
            string sCovgSeqNum = string.Empty;//rupal:policy system interface
            int iLossCode = 0;
            string sTransSeqNum = string.Empty;
            string sCoverageKey = string.Empty;     //Ankit Start : Worked on MITS - 34297
            string sManualDed = string.Empty;
			try 
			{
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "TransTypeCode"); 
				iTransTypeCode = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
                //added by rkaur7 - MITS 17241
                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "ReserveTypeCode");
                iReserveTypeCode = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
                //end by rkaur7
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "ClaimId"); 
				iClaimId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);				
				
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "ClaimantEid"); 
				iClaimantEid = Conversion.ConvertStrToInteger(objTargetElement.InnerText);				
								
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "UnitId"); 
				iUnitId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);	
			
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "TransID"); 
				iTransID = Conversion.ConvertStrToInteger(objTargetElement.InnerText);

                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PolicyId");
                if(objTargetElement != null)
                 iPolicyId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);

                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//CovTypeCode");
                if(objTargetElement != null)
                 iCovTypeCode = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
                //rupal:start, r8 unit implementation
                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "PolicyUnitRowId");
                iPolicyUnitRowId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
                //rupal:end
				//Deb Multi Currency
                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PmtCurrCode");
                iPmtCurrCode = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
                //Deb Multi Currency
                //rupal:start, policy system interface
                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "CovgSeqNum");
                sCovgSeqNum = objTargetElement.InnerText.Trim();
                //rupal:end

                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//LossCode");
                iLossCode = Conversion.ConvertStrToInteger(objTargetElement.InnerText);

                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "TransSeqNum");
                sTransSeqNum = objTargetElement.InnerText.Trim();
                //Ankit Start : Worked on MITS - 34297
                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "CoverageKey");
                sCoverageKey = objTargetElement.InnerText.Trim();
                //Ankit End

                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ManualDed");
                sManualDed = objTargetElement.InnerText.Trim();
                objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);
                
				//Calling the GetPaymentCollection method to get the output xml document
                //updated by rkaur7 - MITS 17241
				//commented by rupal, r8 unit implementation
                //p_objXmlOut = objFundManager.GetReservesInfo(iTransTypeCode ,iReserveTypeCode, iClaimId , iClaimantEid , iUnitId , iTransID , iPolicyId , iCovTypeCode);
                //rupal:start,policy system interface
                //p_objXmlOut = objFundManager.GetReservesInfo(iTransTypeCode, iReserveTypeCode, iClaimId, iClaimantEid, iUnitId, iTransID, iPolicyId, iCovTypeCode, iPolicyUnitRowId, iPmtCurrCode);
                objFundManager.LanguageCode = base.userLogin.objUser.NlsCode; //Aman MITS 31744
                if (sManualDed == "1")
                {
                    p_objXmlOut = objFundManager.GetManualDeductibleReservesInfo(iTransTypeCode, iReserveTypeCode, iClaimId, iClaimantEid, iUnitId, iTransID, iPolicyId, iCovTypeCode, iPolicyUnitRowId, iPmtCurrCode, sCovgSeqNum, iLossCode, sTransSeqNum, sCoverageKey);
                }
                else
                {
                    p_objXmlOut = objFundManager.GetReservesInfo(iTransTypeCode, iReserveTypeCode, iClaimId, iClaimantEid, iUnitId, iTransID, iPolicyId, iCovTypeCode, iPolicyUnitRowId, iPmtCurrCode, sCovgSeqNum, iLossCode, sTransSeqNum, sCoverageKey);     //Ankit Start : Worked on MITS - 34297
                }

				return true;
	
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.Error);
				return false;
			}
			
			catch (Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.GetPaymentCollection.error", base.ClientId), BusinessAdaptorErrorType.Error); 
				return false;				
			}
			finally
			{
				if(objFundManager != null)
				{
					objFundManager.Dispose();
					objFundManager=null;
				}
				objTargetElement=null;
			}
		}
		#endregion

        #region GetReservesCodeInfo
        /// Name		: GetReservesCodeInfo
        /// Author		: Alok Singh Bisht
        /// Date Created: 13-Nov-2011	
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        ///		This method is a wrapper to FundManager.GetReservesCodeInfo method.
        ///		This method will call the methods that Get reserve code information for a particular transaction type	
        /// </summary>
        /// <param name="p_objXmlIn">XML contains the input parameters needed.
        ///		
        ///		The structure of input XML document would be:
        ///		<Document>
        ///			<FundManager>
        ///				<ClaimId>ClaimId</ClaimId>
        ///				<TransTypeCode>TransTypeCode</TransTypeCode>
        ///			</FundManager>
        ///		</Document> 
        /// 
        /// </param>
        /// <param name="p_objXmlOut">XML contains the output document.
        ///		The structure of output XML document would be:
        ///		<Document>
        ///			<ReservesInformation>
        ///				<ReserveType></ReserveType>
        ///			</ReservesInformation>
        ///		</Document>
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>	
        public bool GetReservesCodeInfo(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            FundManager objFundManager = null;					//object of FundManager Class
            XmlElement objTargetElement = null;					//used for creating the Output Xml
            int iTransTypeCode = 0;							    //Trans Type Code passed in input Xml document
            int iClaimId = 0;									//ClaimId passed in input Xml document
            bool bresult = false;
            try
            {
                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "TransTypeCode");
                iTransTypeCode = Conversion.CastToType<Int32>(objTargetElement.InnerText, out bresult);
                //added by rkaur7 - MITS 17241
                
                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "ClaimId");
                iClaimId = Conversion.CastToType<Int32>(objTargetElement.InnerText, out bresult);

                objFundManager = new FundManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.userLogin.UserId, base.userLogin.GroupId, base.ClientId);

                //Calling the GetPaymentCollection method to get the output xml document
                //updated by rkaur7 - MITS 17241
                p_objXmlOut = objFundManager.GetReservesCodeInfo(iTransTypeCode, iClaimId);

                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }

            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.GetPaymentCollection.error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objFundManager != null)
                {
                    objFundManager.Dispose();
                    objFundManager = null;
                }
                objTargetElement = null;
            }
        }
        #endregion

		#region Auto Checks Batch Selection 
		public bool GetAllAutoChecksBatch(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			AutoChecks objAutoChecks = null ;
            //MITS 12091 Raman Bhatia 
            //If claimid is present then filter batchlist by claimid 
            XmlElement objElem = null;
            int iClaimID = 0;

			try 
			{
				objAutoChecks = new AutoChecks( base.userLogin.objRiskmasterDatabase.DataSourceName , base.userLogin.LoginName, base.userLogin.Password ,base.ClientId);//dvatsa-cloud
                objElem = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimID");
                if (objElem != null)
                {
                    iClaimID = Conversion.ConvertStrToInteger(objElem.InnerText);
                }

                p_objXmlOut = objAutoChecks.GetAllAutoChecksBatch(iClaimID);
				return true;	
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.Error);
				return false;
			}
			
			catch (Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.GetAllAutoChecksBatch.Error", base.ClientId), BusinessAdaptorErrorType.Error); 
				return false;				
			}
			finally
			{
				if(objAutoChecks != null)
				{
					objAutoChecks.Dispose();
					objAutoChecks=null;
				}				
			}
		}
		
		#endregion 
		

		/// <summary>
		/// Individual setting of flags so it can be called from outside as well.
		/// Author	Date  
		/// Sumit	1/27/06
		/// </summary>
		/// <param name="p_objXmlOut">Where a particular node needs to be appended.</param>
		private void SetEditablePayeeInfoSecurity(ref XmlDocument p_objXmlOut)
		{
			XmlElement tempElement = null;
			tempElement = p_objXmlOut.CreateElement("IsEditablePayeeInfo");
			if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT,RMO_EDIT_PAYEE))
				tempElement.InnerText = "false";
			else
				tempElement.InnerText = "true";
			p_objXmlOut.DocumentElement.AppendChild(tempElement);

			//Added by Mohit for Bug No. 001849
			tempElement = p_objXmlOut.CreateElement("IsTransactionViewAllowed");
			if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMPermissions.RMO_VIEW))
				tempElement.InnerText = "false";
			else
				tempElement.InnerText = "true";
			p_objXmlOut.DocumentElement.AppendChild(tempElement);
		}

		private void SetSecurityFlags(ref XmlDocument p_objXmlOut)
		{
			XmlElement tempElement = null;
			XmlElement IsClaimOpenElement = null;
			XmlElement objTargetElement = null;
            //Start by Shivendu for MITS 7361
            bool bSuppViewPerm = false;
            //End by Shivendu for MITS 7361
			tempElement = p_objXmlOut.CreateElement("IsEditableCtrlNumber");
			if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT,RMO_ALLOW_EDIT_CTL_NUMBER))
				tempElement.InnerText = "false";
			else
				tempElement.InnerText = "true";

			p_objXmlOut.DocumentElement.AppendChild(tempElement);
			tempElement = p_objXmlOut.CreateElement("IsEditablePreChk");
			if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT,RMO_ALLOW_EDIT_PRECHECK))
			{
				objTargetElement = (XmlElement)p_objXmlOut.SelectSingleNode("//" + "PrecheckFlag"); 
				if (Conversion.ConvertStrToBool(objTargetElement.InnerText))
					tempElement.InnerText = "false";
				else
					tempElement.InnerText = "true";
				objTargetElement = null;
			}
			else
				tempElement.InnerText = "true";
			p_objXmlOut.DocumentElement.AppendChild(tempElement);
			SetEditablePayeeInfoSecurity(ref p_objXmlOut); //Extracted the logic in another function
			
			//Check permissions for void checks
			IsClaimOpenElement = (XmlElement)p_objXmlOut.SelectSingleNode("//Transaction/IsClaimOpen");
			tempElement = p_objXmlOut.CreateElement("IsEnabledVoidChk");
			if (!userLogin.IsAllowedEx(RMB_FUNDS_VOIDCHK, RMPermissions.RMO_ACCESS))
				tempElement.InnerText = "false";
			else if (!userLogin.IsAllowedEx(RMB_FUNDS_VOIDCHK, RMO_FUNDS_VOID_PRINTED_CHECK))
				tempElement.InnerText = "false";
			else if( (IsClaimOpenElement.InnerText.ToLower() == "false") && (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_ALLOW_VOID_CLOSED_CLAIM)))
				tempElement.InnerText = "false";
			else
				tempElement.InnerText = "true";

			p_objXmlOut.DocumentElement.AppendChild(tempElement);

			tempElement = p_objXmlOut.CreateElement("IsEnabledClearChk");
			if (!userLogin.IsAllowedEx(RMB_FUNDS_CLEARCHK, RMPermissions.RMO_ACCESS))
				tempElement.InnerText = "false";
			else
				tempElement.InnerText = "true";
			p_objXmlOut.DocumentElement.AppendChild(tempElement);

			tempElement = p_objXmlOut.CreateElement("IsEditableClaimNumber");
			if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_ALLOW_EDIT_CLAIM_NUMBER))
				tempElement.InnerText = "false";
			else
				tempElement.InnerText = "true";
			p_objXmlOut.DocumentElement.AppendChild(tempElement);

			tempElement = p_objXmlOut.CreateElement("IsEntryClaimNumber");
			if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_ALLOW_ENTRY_CLAIM_NUMBER ))
				tempElement.InnerText = "false";
			else
				tempElement.InnerText = "true";
			p_objXmlOut.DocumentElement.AppendChild(tempElement);

			tempElement = p_objXmlOut.CreateElement("IsEditableCheckNumber");
			if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_ALLOW_EDIT_CHK_NUMBER))
				tempElement.InnerText = "false";
			else
				tempElement.InnerText = "true";
			p_objXmlOut.DocumentElement.AppendChild(tempElement);

			tempElement = p_objXmlOut.CreateElement("IsVisibleSupplementalTab");
            //|| condition added by Shivendu for MITS 7361
            bSuppViewPerm = userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT_SUPP_VIEW, RMPermissions.RMO_ACCESS);
            if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT + SUPP_OFFSET, RMPermissions.RMO_ACCESS) || (!bSuppViewPerm))
				tempElement.InnerText = "false";
			else
				tempElement.InnerText = "true";
			p_objXmlOut.DocumentElement.AppendChild(tempElement);

			tempElement = p_objXmlOut.CreateElement("IsSaveAllowed");
			if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMPermissions.RMO_UPDATE))
				tempElement.InnerText = "false";
			else
				tempElement.InnerText = "true";
			p_objXmlOut.DocumentElement.AppendChild(tempElement);

			tempElement = p_objXmlOut.CreateElement("IsCreateAllowed");
			if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMPermissions.RMO_CREATE))
				tempElement.InnerText = "false";
			else
				tempElement.InnerText = "true";
			p_objXmlOut.DocumentElement.AppendChild(tempElement);
		
			tempElement = p_objXmlOut.CreateElement("IsLookUpAccessAllowed");
			if (userLogin.IsAllowedEx(RMB_LOOKUP))
			{
				if ( userLogin.IsAllowedEx(RMB_LOOKUP, RMO_BANK_ACCT))
					tempElement.InnerText = "true";
			}
			else
				tempElement.InnerText = "false";
			p_objXmlOut.DocumentElement.AppendChild(tempElement);

			tempElement = p_objXmlOut.CreateElement("IsPrintCheckAllowed");
			if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT,RMO_PRINT_CHK))
				tempElement.InnerText = "false";
			else
				tempElement.InnerText = "true";
			p_objXmlOut.DocumentElement.AppendChild(tempElement);

			//Added by Mohit for Bug No. 001849
			tempElement = p_objXmlOut.CreateElement("IsTransactionViewAllowed");
			if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMPermissions.RMO_VIEW))
				tempElement.InnerText = "false";
			else
				tempElement.InnerText = "true";
			p_objXmlOut.DocumentElement.AppendChild(tempElement);

            //Geeta 04/17/07 Modified to fix Mits 9102 
            tempElement = p_objXmlOut.CreateElement("IsChangeCheckStatusAllowed");
            if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_CHANGE_CHK_STATUS))
                tempElement.InnerText = "false";
            else
                tempElement.InnerText = "true";
            p_objXmlOut.DocumentElement.AppendChild(tempElement); 

		}        
		#endregion
        #region Bulk Check Release
        /// Name		: GetBulkCheckData
        /// Author		: Animesh Sahai
        /// Date Created: 22-Jan-2010		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Gets All payment which have the check status as queued payment based 
        /// on the search criteria specified by the user.
        /// </summary>
        /// <param name="p_objXmlIn">XML contains the input parameters needed.
        /// </param>
        /// <param name="p_objXmlOut">XML contains the output document.
        ///		Output XML document
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool GetBulkCheckData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            BulkCheckRelease objBulkCheck = null;
           // XmlDocument objDOC = null;
           // string sTemp = ""; RMSC review defects Removed
            try
            {
                if (!userLogin.IsAllowedEx(RMB_FUNDS_APPPAYCOV, RMPermissions.RMO_ACCESS))
                    throw new PermissionViolationException(RMPermissions.RMO_ACCESS, RMB_FUNDS_APPPAYCOV);
                objBulkCheck = new BulkCheckRelease(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId);
               // objDOC = new XmlDocument();
                p_objXmlOut = objBulkCheck.GetPayments(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.GetBulkCheckData.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objBulkCheck != null)
                {
                    objBulkCheck.Dispose();
                    objBulkCheck = null;
                }
              //  objDOC = null;
            }
        }

        /// Name		: GetInitialScreenData
        /// Author		: Animesh Sahai
        /// Date Created: 5-Feb-2010		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Fetches the initial screen data from the application layer.
        /// </summary>
        /// <param name="p_objXmlIn">XML contains the input parameters needed.
        /// </param>
        /// <param name="p_objXmlOut">XML contains the output document.
        ///		Output XML document
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool GetInitialScreenData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            BulkCheckRelease objBulkCheck = null;
            // XmlDocument objDOC = null;RMSC review defects Removed
            //string sTemp = "";
            try
            {
                objBulkCheck = new BulkCheckRelease(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId);
              //  objDOC = new XmlDocument();
                p_objXmlOut = objBulkCheck.GetInitialScreenData(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.GetInitialScreenData.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objBulkCheck != null)
                {
                    objBulkCheck.Dispose();
                    objBulkCheck = null;
                }
               // objDOC = null;
            }
        }

        /// Name		: ProcessBulkPayments
        /// Author		: Animesh Sahai
        /// Date Created: 10-Feb-2010		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Process the payments selected using the Bulk check utility and returns the payment processing result
        /// </summary>
        /// <param name="p_objXmlIn">XML contains the input parameters needed.
        /// </param>
        /// <param name="p_objXmlOut">XML contains the output document.
        ///		Output XML document
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool ProcessBulkPayments(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            BulkCheckRelease objBulkCheck = null;
           // XmlDocument objDOC = null;
          //  string sTemp = "";
            try
            {
                objBulkCheck = new BulkCheckRelease(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId);
             //   objDOC = new XmlDocument();
                p_objXmlOut = objBulkCheck.ProcessBulkPayments(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("FundManagementAdaptor.ProcessBulkPayments.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objBulkCheck != null)
                {
                    objBulkCheck.Dispose();
                    objBulkCheck = null;
                }
              //  objDOC = null;
            }
        }

        #endregion

        #endregion
        // akaushik5 Added for MITS 37344 Starts
        /// <summary>
        /// Gets the sort column.
        /// </summary>
        /// <param name="sortingColumn">The sorting column.</param>
        /// <returns>Database sorting Column.</returns>
        private static string GetSortColumn(string sortingColumn)
        {
            switch (sortingColumn)
            {
                case "Name_SQL":
                    sortingColumn = "ISNULL(FUNDS.FIRST_NAME + FUNDS.LAST_NAME,FUNDS.LAST_NAME)";
                    break;
                case "Name_Oracle":
                    sortingColumn = "nvl(CONCAT(FIRST_NAME,LAST_NAME),LAST_NAME)";
                    break;
                case "CodeDesc_SQL":
                    sortingColumn = "ISNULL(CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC)";
                    break;
                case "CodeDesc_Oracle":
                    sortingColumn = "nvl(CONCAT(CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC),CODES_TEXT.CODE_DESC)";
                    break;
                //rsriharsha RMA-16952 starts
                case "PayToTheOrderOf_Oracle":
                    sortingColumn = "DBMS_LOB.substr(FUNDS.PAY_TO_THE_ORDER_OF, 4000)";
                    break;
                //rsriharsha RMA-16952 ends
                default: break;
            }
            return sortingColumn;
        }
        // akaushik5 Added for MITS 37344 Ends
    }//skhare7 RMSC merge
}
