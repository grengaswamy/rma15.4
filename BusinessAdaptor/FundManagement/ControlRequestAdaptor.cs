﻿using System;
using System.Collections.Generic;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.Xml;
using Riskmaster.Application.FundManagement;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
    class ControlRequestAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        /// <summary>
        /// Class Constructor.
        /// </summary>
        public ControlRequestAdaptor()
        {

        }

        #endregion

        #region Private Member variables
        #endregion

        #region Private Functions

        #region InitializeControlRequest

        /// <summary>
        /// This function will instantiate the application layer's Search class.
        /// </summary>
        /// <param name="p_objXmlDoc">Output xml document.</param>
        /// <param name="p_objErr">Error collection.</param>
        /// <param name="p_bDmfFlag">Flag to see if Datamodel object needs to be created or not.</param>
        /// <returns>Instance of application layer's ReserveFunds class.</returns>
        private ControlRequest InitializeControlRequest(ref XmlDocument p_objXmlDoc, ref BusinessAdaptorErrors p_objErr, bool p_bDmfFlag)
        {
            ControlRequest objControlRequest = null;
            DataModelFactory objDmf = null;
            try
            {
                if (p_bDmfFlag)
                {
                    objDmf = new DataModelFactory(base.userLogin, base.ClientId);
                    objControlRequest = new ControlRequest(objDmf,base.userLogin,base.ClientId);//sonali
                }
                else
                    objControlRequest = new ControlRequest(base.connectionString,base.ClientId);//sonali

                p_objXmlDoc = new XmlDocument();
                p_objXmlDoc.AppendChild(p_objXmlDoc.CreateElement("ControlRequest"));
                p_objErr = new BusinessAdaptorErrors(base.ClientId);
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }


            return objControlRequest;
        }

        #endregion

        #endregion

        #region Public functions

        #region GetSearchResultsForSelectedFund

        /// <summary>
        /// Retrieves the record for passed in Entity-Id
        /// </summary>
        /// <param name="p_objXmlIn">XML document containing input parameters for Search page creation</param>
        /// <param name="p_objXmlOut">XML document containing Search screen data.</param>
        /// <param name="p_objErrOut">Error object</param>
        /// <returns>true if operation is successful else false</returns>
        public bool GetSearchResultsForSelectedFund(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            int iEntityId = 0;
            bool bResult = false;
            XmlElement objXmlUser = null;
            ControlRequest objControlRequest = null;
            bool isRequestProcessed = false; //mkaran2 - Control Request Functionlity - MITS 34569
            try
            {
                objXmlUser = (XmlElement)p_objXmlIn.SelectSingleNode("//GetEntityData");
                //iEntityId = Conversion.ConvertStrToInteger(objXmlUser.SelectSingleNode("//EntityTableId").InnerText);
                iEntityId = Conversion.CastToType<Int32>(objXmlUser.SelectSingleNode("//EntityTableId").InnerText, out bResult);

                //mkaran2 - Control Request Functionlity - Start  - MITS 34569
                if (objXmlUser.SelectSingleNode("//RequestProcessed") != null && objXmlUser.SelectSingleNode("//RequestProcessed").InnerText != "") 
                {
                    isRequestProcessed = true;
                }
                //mkaran2 - Control Request Functionlity - End  - MITS 34569
                
                using (objControlRequest = InitializeControlRequest(ref p_objXmlOut, ref p_objErrOut, true))
                {
                    //p_objXmlOut = objControlRequest.GetSearchResultsForSelectedFund(iEntityId);
                    p_objXmlOut = objControlRequest.GetSearchResultsForSelectedFund(iEntityId, isRequestProcessed); //mkaran2 - Control Request Functionlity  - MITS 34569
                }
            }
            catch (XmlOperationException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SearchAdaptor.CreateSearchView.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sonali
                return false;
            }
            finally
            {
                //objSearchXML = null;
                objXmlUser = null;
            }
            return (true);
        }

        #endregion

        #region

        public bool ProcessControlRequest(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            int iTransId = 0;
            int iSplitRowId = 0;
            int iDestReserveTypeCodeId = 0;
            int iDestTransTypeCodeId = 0;
            int iDestRCRowId = 0;
            int iReasonTypeCode = 0;

            bool bResult = false;

            XmlElement objXmlControlRequest = null;
            ControlRequest objControlRequest = null;

            try
            {
                objXmlControlRequest = (XmlElement)p_objXmlIn.SelectSingleNode("//ProcessControl");
                iTransId = Conversion.CastToType<Int32>(objXmlControlRequest.SelectSingleNode("//transid").InnerText, out bResult);
                iSplitRowId = Conversion.CastToType<Int32>(objXmlControlRequest.SelectSingleNode("//splitid").InnerText, out bResult);
                iDestTransTypeCodeId = Conversion.CastToType<Int32>(objXmlControlRequest.SelectSingleNode("//transtypecode").InnerText, out bResult);
                iDestReserveTypeCodeId = Conversion.CastToType<Int32>(objXmlControlRequest.SelectSingleNode("//reservetypecode").InnerText, out bResult);
                iReasonTypeCode = Conversion.CastToType<Int32>(objXmlControlRequest.SelectSingleNode("//reasontypecode").InnerText, out bResult);
                iDestRCRowId = Conversion.CastToType<Int32>(objXmlControlRequest.SelectSingleNode("//rcrowid").InnerText, out bResult);
                using (objControlRequest = InitializeControlRequest(ref p_objXmlOut, ref p_objErrOut, true))
                {
                    p_objXmlOut = objControlRequest.ProcessControlRequest(iTransId, iSplitRowId, iDestReserveTypeCodeId, iDestTransTypeCodeId, iDestRCRowId, iReasonTypeCode);
                }

            }
            catch (XmlOperationException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SearchAdaptor.CreateSearchView.Error",base.ClientId), BusinessAdaptorErrorType.Error);//sonali
                return false;
            }
            finally
            {

            }
            return true;
        }

        #endregion

        #endregion
    }
}
