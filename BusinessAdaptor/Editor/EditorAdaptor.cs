using System;
using System.Xml;

using Riskmaster.Settings;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.Editor;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor
{

	/// <summary>	
	///	A class representing the Editor adaptor. It exposes various methods for Spell Checking
	/// and saving the fields into database. 
	/// </summary>
	
	public class EditorAdaptor: BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default class constructor
		/// </summary>
		public EditorAdaptor()
		{	
		}

		#endregion

		#region Public functions
		
		#region GetExistingComments
		/// <summary>
		/// This function is a wrapper over Riskmaster.Application.Editor.GetExistingComments() function. 
		/// This method is basically used for retreiving the Existing comments for specific RecordId
		/// </summary>
		/// <param name="p_objXmlIn">Input xml document.
		/// Following is the structure for input xml-:
		// <Document>
		//	<Editor>
		//		<FormName/>
		//		<RecordId/>
		//	</Editor>
		// </Document>
		/// </param>
		/// <param name="p_objXmlOut">Xml document containing the output xml.
		//	Output structure is as follows-:
		///</param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool GetExistingComments(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			Editor objEditor=null;
            string sFormName = string.Empty, sUserLoginName = string.Empty;
            int iCommentId = 0;
			int iRecordId = 0;
			int iParentSecurityId = 0;
            string sComments = string.Empty;
            string sHTMLComments = string.Empty;

			//XmlElement objXmlElem = null;
			try
			{
				if(p_objXmlIn.SelectSingleNode("//FormName") != null)
					sFormName = p_objXmlIn.SelectSingleNode("//FormName").InnerText;

                if (p_objXmlIn.SelectSingleNode("//CommentId") != null)
                    iCommentId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//CommentId").InnerText);

				if(p_objXmlIn.SelectSingleNode("//RecordId") != null)
					iRecordId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//RecordId").InnerText);

				if(p_objXmlIn.SelectSingleNode("//ParentSecurityId") != null)
					iParentSecurityId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ParentSecurityId").InnerText);

				sUserLoginName = base.loginName;

                //Commented by MGABA2 :for MITS 12510-Need connection string in Editor.cs
				//Initialize the application layer Editor class
				//objEditor=new Editor(base.userLogin); 
                objEditor = new Editor(base.userLogin, base.connectionString, base.ClientId);
                // Check session to see if we have any "work in progress" unsaved comments
                if (this.cachedObjectExists(String.Format("{0}|{1}|Comments", sFormName.ToLower(), iRecordId)))
                {
                    sComments = (string)this.getCachedObject(String.Format("{0}|{1}|Comments", sFormName.ToLower(), iRecordId));
                    sHTMLComments = (string)this.getCachedObject(String.Format("{0}|{1}|HTMLComments", sFormName.ToLower(), iRecordId));
                }

                SysSettings oSettings = new SysSettings(m_connectionString, base.ClientId); //Ash - cloud
                //Start-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims
                //if ((sFormName == "event" || sFormName == "claimgc" || sFormName == "claimwc" || sFormName == "claimdi" || sFormName == "claimva" )
                //    && !oSettings.UseLegacyComments)
                //nsachdeva2 - MITS: 27077 - 6/5/2012
                //if ((sFormName == "event" || sFormName == "claimgc" || sFormName == "claimwc" || sFormName == "claimdi" || sFormName == "claimva" || sFormName == "claimpc" )
                  //  && !oSettings.UseLegacyComments)

                //tanwar2 - mits # 26512 -> start
                //if ((sFormName == "event" || sFormName == "claimgc" || sFormName == "claimwc" || sFormName == "claimdi" || sFormName == "claimva" || sFormName == "claimpc" || sFormName == "litigation")
                //    && !oSettings.UseLegacyComments)
                    //End MITS: 27077
                //End-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims

                if ((sFormName == "event" || sFormName == "claimgc" || sFormName == "claimwc" || sFormName == "claimdi" || sFormName == "claimva" || sFormName == "claimpc" || sFormName == "litigation"))
                {
                    p_objXmlOut = objEditor.GetExistingComments(sFormName, iCommentId, iRecordId, iParentSecurityId, sUserLoginName);

                    if (oSettings.UseLegacyComments && string.IsNullOrEmpty(p_objXmlOut.SelectSingleNode("//HTMLComments").InnerText))
                    {
                        p_objXmlOut = objEditor.GetExistingComments(sFormName, iRecordId, iParentSecurityId, sUserLoginName, sComments, sHTMLComments);
                    }
                }
                //tanwar2 - mits 26512 -> end
                //MITS 26187 hlv 1/15/2013 begin
                else
                {
                    p_objXmlOut = objEditor.GetExistingComments(sFormName, iRecordId, iParentSecurityId, sUserLoginName, sComments, sHTMLComments);
                }
                //MITS 26187 hlv 1/15/2013 end

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("EditorAdaptor.GetExistingComments.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objEditor=null;
			}
			
		}
		#endregion

		#region GetMemoAttributes
		/// <summary>
		/// This function is a wrapper over Riskmaster.Application.Editor.GetMemoAttributes() function. 
		/// This method is basically used for retreiving the Global Variables for Memo fields
		/// </summary>
		/// <param name="p_objXmlIn">Input xml document.
		/// Following is the structure for input xml-:
		// <Document>
		//	<Editor>
		//		<Loaded/>
		//		<FieldName/>
		//	</Editor>
		// </Document>
		/// </param>
		/// <param name="p_objXmlOut">Xml document containing the output xml.
		//	Output structure is as follows-:
		///</param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool GetMemoAttributes(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			Editor objEditor=null;
//			string sLoaded="";
//			string sFieldName = "";
			string sUserLoginName = "";
            string sSupplementalField = string.Empty;
			try
			{
//				if(p_objXmlIn.SelectSingleNode("//Loaded") != null)
//					sLoaded = p_objXmlIn.SelectSingleNode("//Loaded").InnerText;
//
//				if(p_objXmlIn.SelectSingleNode("//FieldName") != null)
//					sFieldName = p_objXmlIn.SelectSingleNode("//RecordId").InnerText;

                if (p_objXmlIn.SelectSingleNode("//IsSuppField") != null)
                    sSupplementalField = p_objXmlIn.SelectSingleNode("//IsSuppField").InnerText;

				sUserLoginName = base.loginName;

				//Initialize the application layer Editor class
				objEditor=new Editor(base.userLogin, base.ClientId);

                p_objXmlOut = objEditor.GetMemoAttributes(sUserLoginName, sSupplementalField);

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("EditorAdaptor.GetMemoAttributes.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objEditor=null;
			}
		}
		#endregion

		#region AddWordToDictionary
		
		public bool AddWordToDictionary(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			bool retVal = false;
			Editor objEditor;
			try
			{
				if(p_objDocIn.SelectSingleNode("//PassedWord").InnerText!="")
				{
					objEditor = new Editor(base.userLogin, base.ClientId);
					retVal = objEditor.AddToDictionary(p_objDocIn.SelectSingleNode("//PassedWord").InnerText);
				}
				return retVal;
			}
			catch(RMAppException p_objException)
			{
				if(p_objErrOut==null)
					p_objErrOut=new BusinessAdaptorErrors(base.ClientId);
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return retVal;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("EditorAdaptor.AddWordToDictionary.GenericError", base.ClientId),BusinessAdaptorErrorType.Error);
				return retVal;
			}
			finally
			{
				p_objDocIn.SelectSingleNode("//form//RetVal").InnerText = retVal.ToString();				
				if(retVal)
					p_objDocIn.SelectSingleNode("//form//SuccessMsg").InnerText = Globalization.GetString("EditorAdaptor.AddWordToDictionary.Success", base.ClientId) ;
				else
					p_objDocIn.SelectSingleNode("//form//SuccessMsg").InnerText = Globalization.GetString("EditorAdaptor.AddWordToDictionary.Failure", base.ClientId) ;
				p_objXmlOut = p_objDocIn ;
				
			}
		}

		#endregion

		#region GetSpellCheckDone
		/// <summary>
		/// This function is a wrapper over Riskmaster.Application.Editor.Editor.GetSpellCheckDone() function. 
		/// This method is basically used for checking the spelling of the passed in string.
		/// </summary>
		/// <param name="p_objXmlIn">Input xml document.
		/// Following is the structure for input xml-:
		// <Document>
		//	<SpellCheck>
		//	    <InputText>
		//	</SpellCheck>
		// </Document>
		/// </param>
		/// <param name="p_objXmlOut">Xml document containing the output xml.
		//	Output structure is as follows-:
		//  <SpellCheck>
		//		<OriginalText/>
		//		<BadWord>
		//			<Suggestion/>
		//			<Suggestion/>
		//		</BadWord>
		//		<BadWord>
		//			<Suggestion/>
		//			<Suggestion/>
		//		</BadWord>
		//	</SpellCheck>
		///</param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool GetSpellCheckDone(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			Editor objEditor=null;
			string sInputText="";
			string sHTMLText="";
			string sOutputText = "";

			XmlElement objXmlElem = null;
			XmlCDataSection objCData = null;
			try
			{
				sInputText=p_objXmlIn.SelectSingleNode("//InputText").InnerText;
				sHTMLText = p_objXmlIn.SelectSingleNode("//HTMLText").InnerText;
				//sInputText = "This is a Cheks Wordl Cheks";
				//Initialize the application layer Editor class
				objEditor=new Editor(base.userLogin, base.ClientId);

				sOutputText = objEditor.GetSpellCheckDone(sInputText, sHTMLText);
				
				objXmlElem = p_objXmlOut.CreateElement("XmlString");
				objCData = p_objXmlOut.CreateCDataSection(sOutputText);
				objXmlElem.AppendChild(objCData);

				p_objXmlOut.AppendChild(objXmlElem);


				//p_objXmlOut = objEditor.GetSpellCheckDone(sInputText);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("EditorAdaptor.GetSpellCheckDone.GenericError", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objEditor=null;
			}
			
		}
		#endregion

		#region SaveComments
		/// <summary>
		/// This function is a wrapper over Riskmaster.Application.Editor.SaveComments() function. 
		/// This method is basically used for retreiving the Existing comments for specific RecordId
		/// </summary>
		/// <param name="p_objXmlIn">Input xml document.
		/// Following is the structure for input xml-:
		// <Document>
		//	<Editor>
		//		<FormName/>
		//		<RecordId/>
		//	</Editor>
		// </Document>
		/// </param>
		/// <param name="p_objXmlOut">Xml document containing the output xml.
		//	Output structure is as follows-:
		///</param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool SaveComments(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			Editor objEditor=null;
			XmlNode objXmlNode = null;
            string sFormName = string.Empty;
            bool bReturn = false;

			try
			{
				if(p_objXmlIn.SelectSingleNode("//Editor") != null)
					objXmlNode = p_objXmlIn.SelectSingleNode("Editor");

                if (p_objXmlIn.SelectSingleNode("//FormName") != null)
                    sFormName = p_objXmlIn.SelectSingleNode("//FormName").InnerText;

				//Initialize the application layer Editor class
				objEditor=new Editor(base.userLogin, m_connectionString, base.ClientId);
                SysSettings oSettings = new SysSettings(m_connectionString, base.ClientId); //Ash - cloud
                //Start-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims
                //if ((sFormName == "event" || sFormName == "claimgc" || sFormName == "claimwc" || sFormName == "claimdi" || sFormName == "claimva")
                //    && !oSettings.UseLegacyComments)
                //nsachdeva2 - MITS: 27077 - 6/5/2012
                //if ((sFormName == "event" || sFormName == "claimgc" || sFormName == "claimwc" 
                  //  || sFormName == "claimdi" || sFormName == "claimva" || sFormName == "claimpc")
                    //&& !oSettings.UseLegacyComments)
                if ((sFormName == "event" || sFormName == "claimgc" || sFormName == "claimwc" 
                    || sFormName == "claimdi" || sFormName == "claimva" || sFormName == "claimpc" || sFormName == "litigation")
                    && !oSettings.UseLegacyComments)
                //End MITS: 27077
                //End-Mridul Bansal. 01/07/10. MITS#18230. Provided support for Property Claims
                {
                    //nsachdeva2 - 02/22/2012 - MITS: 27610
                    //bReturn = objEditor.Save(objXmlNode);
                    bReturn = objEditor.Save(objXmlNode, sFormName);
                    //End MITS: 27610
                }
                else
                {
                    bReturn = objEditor.SaveComments(objXmlNode);
                    this.setCachedObject(String.Format("{0}|{1}|Comments", sFormName.ToLower(), Conversion.ConvertStrToInteger(objXmlNode.SelectSingleNode("//RecordId").InnerText)), objEditor.Comments);
                    this.setCachedObject(String.Format("{0}|{1}|HTMLComments", sFormName.ToLower(), Conversion.ConvertStrToInteger(objXmlNode.SelectSingleNode("//RecordId").InnerText)), objEditor.HTMLComments);
                    //return bReturn;
                }

                //rsolanki2: recent claim updates  MITS 18828
                if (bReturn)
                {
                    //updating the entry in recent claims only if the "Save comment" operation was successfull
                    //if (sFormName.StartsWith("claim", StringComparison.OrdinalIgnoreCase))
                    if (sFormName.Equals("claimgc", StringComparison.OrdinalIgnoreCase)
                        || sFormName.Equals("claimva", StringComparison.OrdinalIgnoreCase)
                        || sFormName.Equals("claimdi", StringComparison.OrdinalIgnoreCase)
                        || sFormName.Equals("claimpc", StringComparison.OrdinalIgnoreCase)
                        || sFormName.Equals("claimwc", StringComparison.OrdinalIgnoreCase)
                        )
                    {
                        CommonFunctions.UpdateRecentRecords(this.connectionString
                            , this.userLogin.UserId.ToString()
                            , "claim"
                            , objXmlNode.SelectSingleNode("//RecordId").InnerText, base.ClientId);
                    }
                    else if (sFormName == "event")
                    {
                        CommonFunctions.UpdateRecentRecords(this.connectionString
                            , this.userLogin.UserId.ToString()
                            , "event"
                            , objXmlNode.SelectSingleNode("//RecordId").InnerText, base.ClientId);
                    }
                }
                return bReturn;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("EditorAdaptor.SaveComments.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objEditor=null;
			}
			
		}
		#endregion

        #region GetComments
        /// <summary>
        /// Author : Sumit Kumar
        /// Date: 10/12/2009
        /// Function - This is used to fetch the comments from the POLICY_X_TRANS_ENH based on the transaction ID passed.
        /// MITS - 18251
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetComments(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Editor objEditor = null;
            try
            {
                objEditor = new Editor(base.userLogin,base.connectionString, base.ClientId);
                p_objXmlOut = objEditor.GetComments(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("EditorAdaptor.GetComments.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objEditor = null;
            }
        }
        #endregion

	#endregion
	}
}
