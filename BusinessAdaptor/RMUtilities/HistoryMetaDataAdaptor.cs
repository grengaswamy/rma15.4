﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.Xml;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    class HistoryMetaDataAdaptor : BusinessAdaptorBase
    {

        public bool GetColumnsInfo(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            HistoryMetaData objHM = null;
            bool bReturnValue = false;
            try
            {
                objHM = new HistoryMetaData(connectionString, base.ClientId);
                bReturnValue = objHM.GetColumnsInfo(p_objXmlIn, ref p_objXmlOut);
                return bReturnValue;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("HistoryMetaDataAdaptor.GetColumnsInfo.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objHM = null;
            }
        }
        public bool GetTables(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            HistoryMetaData objHM = null;
            try
            {
                objHM = new HistoryMetaData(connectionString, base.ClientId);
                p_objXmlOut = objHM.GetTables(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("HistoryMetaDataAdaptor.GetTables.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objHM = null;
            }
        }

        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            HistoryMetaData objHM = null;
            bool bReturnValue = false;
            try
            {
                objHM = new HistoryMetaData(connectionString, base.ClientId);
                bReturnValue = objHM.Save(p_objXmlIn, ref p_objXmlOut);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("HistoryMetaDataAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objHM = null;
            }
        }
    }
}
