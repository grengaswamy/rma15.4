﻿using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
	/// Summary description for VSSInterfaceLogAdaptor.
	/// </summary>
   public class VSSInterfaceLogAdaptor:BusinessAdaptorBase
    {
        public VSSInterfaceLogAdaptor(){}
		public bool Search(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			VSSInterfaceLog objVSSInterfaceLog = null;
			try
			{
                objVSSInterfaceLog = new VSSInterfaceLog(connectionString);
                objVSSInterfaceLog.LanguageCode = base.userLogin.objUser.NlsCode; //tmalhotra3 ML Change
				p_objXmlOut = objVSSInterfaceLog.Search(p_objXmlIn);
				return true;

			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("VSSInterfaceLog.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objVSSInterfaceLog = null;
			}
		}
    }
}
