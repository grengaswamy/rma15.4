﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    public class ClaimLetterAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ClaimLetterAdaptor() { }

        #endregion

        #region Public Methods
        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.ClaimLetter.Get() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.ClaimLetter.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        /// 
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ClaimLetter objSys = null; //Application layer component			
            try
            {
                objSys = new ClaimLetter(connectionString, base.ClientId);
                p_objXmlOut = objSys.Get(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ClaimLetterAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objSys = null;
            }
        }

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.ClaimLetter.Save() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.ClaimLetter.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ClaimLetter objSys = null; //Application layer component		
            try
            {

                objSys = new ClaimLetter(connectionString, userLogin.LoginName, base.ClientId);
                p_objXmlOut = objSys.Save(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ClaimLetterAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objSys = null;
            }
        }

        #endregion
    }

}
