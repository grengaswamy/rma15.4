using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  
namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	/// <summary>
	/// Summary description for JurisPreparerInfoAdaptor.
	/// </summary>
	public class JurisPreparerInfoAdaptor:BusinessAdaptorBase
	{
		public JurisPreparerInfoAdaptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			JurisPreparerInfo objJurisPreparerInfo=null; //Application layer component			
			try
			{
                objJurisPreparerInfo = new JurisPreparerInfo(userLogin.UserId.ToString(), connectionString, base.ClientId);
				p_objXmlOut=objJurisPreparerInfo.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("JurisPreparerInfoAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objJurisPreparerInfo=null;
			}
		}

		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			JurisPreparerInfo objJurisPreparerInfo = null;			
			try
			{
                objJurisPreparerInfo = new JurisPreparerInfo(userLogin.UserId.ToString(), connectionString, base.ClientId);
				objJurisPreparerInfo.Save(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("JurisPreparerInfoAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objJurisPreparerInfo=null;
			}
		}

	}
}
