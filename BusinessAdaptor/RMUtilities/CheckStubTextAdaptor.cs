using System;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    ///************************************************************** 
    ///* $File		: CheckStubTextAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 04/07/2010
    ///* $Author	: Michael Capps
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************

    /// <summary>	
    ///	This class is used to call the application layer component for CheckStubText class  
    /// </summary>
    public class CheckStubTextAdaptor : BusinessAdaptorBase
    {
        public CheckStubTextAdaptor()
        {
        }
        /// <summary>
        /// This function xml containing data to be shown on screen
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        ///	Input structure is as follows-:
        ///	<CheckStubText><RowId></RowId></CheckStubText>
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CheckStubText objOption = null;
            try
            {
                objOption = new CheckStubText(connectionString, base.ClientId);//Add by kuladeep for Jira-63
                p_objXmlOut = objOption.Get(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CheckStubTextAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);//Add by kuladeep for Jira-63
                return false;
            }
            finally
            {
                objOption = null;
            }
        }
        /// <summary>
        /// Deletes the CheckStubText information
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        ///	Output structure is as follows-:
        ///	See Xml file named CheckStubText.xml in Xml folder
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CheckStubText objOption = null;
            try
            {
                objOption = new CheckStubText(connectionString, base.ClientId);//Add by kuladeep for Jira-63
                //rsushilaggar: Get the latest check stub text list and set it in the xml out after deletion
                p_objXmlOut = objOption.Delete(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CheckStubTextAdaptor.Delete.Error", base.ClientId), BusinessAdaptorErrorType.Error);//Add by kuladeep for Jira-63
                return false;
            }
            finally
            {
                objOption = null;
            }
        }

        /// <summary>
        /// Saves the CheckStubText
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        ///	Input structure is as follows-:
        ///	See Xml file named CheckStubText.xml in Xml folder
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CheckStubText objOption = null;
            try
            {
                objOption = new CheckStubText(connectionString, base.ClientId);//Add by kuladeep for Jira-63
                //rsushilaggar: Get the latest check stub text list and set it in the xml out after Adding New/Updating
                p_objXmlOut = objOption.Save(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CheckStubTextAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);//Add by kuladeep for Jira-63
                return false;
            }
            finally
            {
                objOption = null;
            }
        }
    }
}
