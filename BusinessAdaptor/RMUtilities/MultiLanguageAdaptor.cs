﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    /// <summary>
    /// Deb Jena
    /// ML Implementation
    /// </summary>
    public class MultiLanguageAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public MultiLanguageAdaptor() { }
        #endregion
        /// <summary>
        /// Get Languages
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetLanguages(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            MultiLanguage objML = null;
            XmlElement objElement = null;
            string sPageID = string.Empty;
            string sLangId = string.Empty;

            try
            {
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PageId");
                if (objElement != null)
                    sPageID = objElement.InnerText;

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//LangId");
                if (objElement != null)
                    sLangId = objElement.InnerText;

                objML = new MultiLanguage(userLogin.LoginName, connectionString, base.ClientId);
                p_objXmlOut = objML.GetLanguages(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                // p_objErrOut.Add(p_objException, Globalization.GetString("MultiLanguageAdaptor.GetLanguage.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                // throw new Exception(CommonFunctions.FilterBusinessMessage(Globalization.GetString("MultiLanguage.GetLanguage.Error", base.ClientId), sLangId), p_objException);
                throw new Exception(Globalization.GetString("MultiLanguage.GetLanguage.Error", base.ClientId, sLangId), p_objException); //Add by ttumula2 on jan 13 2015 for 6033
                return false;

            }
            finally
            {
                objML = null;
            }
        }
        /// <summary>
        /// Get Seleted Language
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetSelectedMultiLanguageInfo(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            MultiLanguage objML = null; //Application layer component		
            XmlElement objElement = null;
            string sPageID = string.Empty;
            string sLangId = string.Empty;
            try
            {

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PageId");
                if (objElement != null)
                    sPageID = objElement.InnerText;

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//LangId");
                if (objElement != null)
                    sLangId = objElement.InnerText;

                objML = new MultiLanguage(loginName, connectionString, base.ClientId);
                p_objXmlOut = objML.GetSelectedMultiLanguageInfo(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                //p_objErrOut.Add(p_objException, Globalization.GetString("MultiLanguageAdaptor.GetSelectedMultiLanguageInfo.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                //throw new Exception(CommonFunctions.FilterBusinessMessage(Globalization.GetString("MultiLanguage.GetSelectedMultiLanguageInfo.Error", base.ClientId), sLangId), p_objException);
                throw new Exception(Globalization.GetString("MultiLanguage.GetSelectedMultiLanguageInfo.Error", base.ClientId, sLangId), p_objException); //Add by ttumula2 on jan 13 2015 for 6033
                return false;
            }
            finally
            {
                objML = null;
            }
        }
        /// <summary>
        /// Update Language
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            MultiLanguage objML = null;
            XmlElement objElement = null;
            string sPageID = string.Empty;
            string sLangId = string.Empty;

            try
            {
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PageId");
                if (objElement != null)
                    sPageID = objElement.InnerText;

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//LangId");
                if (objElement != null)
                    sLangId = objElement.InnerText;

                objML = new MultiLanguage(loginName, connectionString, base.ClientId);
                p_objXmlOut = objML.Save(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                // p_objErrOut.Add(p_objException, Globalization.GetString("MultiLanguageAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                //throw new Exception(CommonFunctions.FilterBusinessMessage(Globalization.GetString("MultiLanguage.Save.Error", base.ClientId), sLangId), p_objException);
                throw new Exception(Globalization.GetString("MultiLanguage.Save.Error", base.ClientId, sLangId), p_objException); //Add by ttumula2 on jan 13 2015 for 6033
                return false;
            }
            finally
            {
                objML = null;
            }
        }
        /// <summary>
        /// GetFormatLists
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetFormatLists(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            MultiLanguage objML = null; //Application layer component			
            try
            {
                objML = new MultiLanguage(loginName, connectionString, base.ClientId);
                p_objXmlOut = objML.GetFormatLists(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("MultiLanguageAdaptor.GetSelectedMultiLanguageInfo.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objML = null;
            }
        }
    }
}
