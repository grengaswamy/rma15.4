using System;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: CheckOptionsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-May-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for CheckOptions class  
	/// </summary>
	public class CheckOptionsAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public CheckOptionsAdaptor(){}
		#endregion

		#region Public Function
        public bool GetDummyData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CheckOptions objOptions = null;
            XmlDocument objDOC = null;
            try
            {
                objDOC = new XmlDocument();
                objDOC = p_objXmlIn;
                objOptions = new CheckOptions(userLogin.objRiskmasterDatabase.DataSourceId.ToString(), connectionString, base.ClientId);//rkaur27
                p_objXmlOut = objOptions.GetDummyData(objDOC);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CheckOptionsAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                return false;
            }
            finally
            {
                objOptions = null;
                objDOC = null;
            }
        }

 		///MITS 32087  
		/// <summary>
        /// This function xml containing data to be shown on screen
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        ///	Output structure is as follows-:
        ///	See Xml file named CheckOptions.xml in Xml folder
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool GetDuplicateCriteria(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CheckOptions objOptions = null;
            XmlDocument objDOC = null;
            try
            {
                objDOC = new XmlDocument();
                objDOC = p_objXmlIn;
                objOptions = new CheckOptions(userLogin.objRiskmasterDatabase.DataSourceId.ToString(), connectionString, base.ClientId);
                p_objXmlOut = objOptions.GetDuplicateCriteria(objDOC);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CheckOptionsAdaptor.GetDuplicateCriteria.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objOptions = null;
                objDOC = null;
            }
        }


		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	See Xml file named CheckOptions.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			CheckOptions objOptions=null;	
			XmlDocument objDOC=null;
			try
			{
				objDOC=new XmlDocument();
				objDOC=p_objXmlIn;
				objOptions=new CheckOptions(userLogin.objRiskmasterDatabase.DataSourceId.ToString(),connectionString, base.ClientId);//rkaur27
				p_objXmlOut=objOptions.Get(objDOC);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("CheckOptionsAdaptor.Get.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objOptions=null;
				objDOC=null;
			}
		}
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	See Xml file named CheckOptions.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetAddNotifyValues(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			CheckOptions objOptions=null;	
			XmlDocument objDOC=null;
			RMConfigurator objConfig=null;
			try
			{
				objDOC=new XmlDocument();
				objDOC=p_objXmlIn;
				objOptions=new CheckOptions(userLogin.objRiskmasterDatabase.DataSourceId.ToString(),connectionString, base.ClientId);//rkaur27
				p_objXmlOut=objOptions.GetAddNotifyValues(objDOC);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("CheckOptionsAdaptor.Get.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objOptions=null;
				objDOC=null;
			}
		}
		/// <summary>
		/// Deletes the Payment Notification information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	See Xml file named CheckOptions.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool DeletePaymentNotification(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			CheckOptions objOptions=null;	
			try
			{
				objOptions=new CheckOptions(userLogin.objRiskmasterDatabase.DataSourceId.ToString(),connectionString, base.ClientId);
				p_objXmlOut=objOptions.DeletePaymentNotification(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("CheckOptionsAdaptor.DeletePaymentNotification.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objOptions=null;
			}
		}
		/// <summary>
		/// Adds the Payment Notification information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	See Xml file named CheckOptions.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool AddPaymentNotification(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			CheckOptions objOptions=null;	
			XmlDocument objDOC=null;
			XmlNode objNode=null;
			try
			{
				objOptions=new CheckOptions(userLogin.objRiskmasterDatabase.DataSourceId.ToString(),connectionString, base.ClientId);
				p_objXmlOut=objOptions.AddPaymentNotification(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("CheckOptionsAdaptor.AddPaymentNotification.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objOptions=null;
			}
		}
		/// <summary>
		/// Sets the Duplicate Criteria information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	See Xml file named CheckOptions.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool SetDuplicateCriteria(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			CheckOptions objOptions=null;	
			try
			{
				objOptions=new CheckOptions(userLogin.objRiskmasterDatabase.DataSourceId.ToString(),connectionString, base.ClientId);
				p_objXmlOut=objOptions.SetDuplicateCriteria(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("CheckOptionsAdaptor.SetDuplicateCriteria.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objOptions=null;
			}
		}
		/// <summary>
		/// Saves the check options information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	See Xml file named CheckOptions.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			CheckOptions objOptions=null;	
			try
			{
				objOptions=new CheckOptions(userLogin.objRiskmasterDatabase.DataSourceId.ToString(),connectionString, base.ClientId);//rkaur27
				objOptions.Save(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("CheckOptionsAdaptor.Save.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objOptions=null;
			}
		}
        
        /// <summary>
        ///  created by Ashutosh
        /// Saves the Schedule DAte and Description
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        ///	Output structure is as follows-:
        ///	See Xml file named CheckOptions.xml in Xml folder
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        /// 
        public bool SaveSchedule(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CheckOptions objOptions = null;
            try
            {
                objOptions = new CheckOptions(userLogin.objRiskmasterDatabase.DataSourceId.ToString(), connectionString, base.ClientId);//rkaur27
                objOptions.SaveSchedule(p_objXmlIn, userLogin.LoginName);
                return true;
            }
            catch (RMAppException p_objException)

            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CheckOptionsAdaptor.SaveSchedule.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objOptions = null;
            }
        }
    
        //Start Added by Ashutosh R8 Enhancement 
        //For Deletion of schedule date
        /// <summary>
        /// This function xml containing data to be shown on screen
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        ///	Output structure is as follows-:
        ///	See Xml file named CheckOptions.xml in Xml folder
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool DeleteSchedule(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CheckOptions objOptions = null;
            try
            {
                objOptions = new CheckOptions(userLogin.objRiskmasterDatabase.DataSourceId.ToString(), connectionString, base.ClientId);
                objOptions.DeleteSchedule(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CheckOptionsAdaptor.DeleteSchedule.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objOptions = null;
            }
        }

        //Start Added by Ashutosh R8 Enhancement 
        //For geting/Editing  the Schedule Date
        /// <summary>
        /// This function xml containing data to be shown on screen
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml
        ///	Output structure is as follows-:
        ///	See Xml file named CheckOptions.xml in Xml folder
        /// </param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool GetScheduleDates(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CheckOptions objOptions = null;
            XmlDocument objDOC = null;
           
            try
            {
                objDOC = new XmlDocument();
                objDOC = p_objXmlIn;
                objOptions = new CheckOptions(userLogin.objRiskmasterDatabase.DataSourceId.ToString(), connectionString, base.ClientId);//rkaur27
                p_objXmlOut = objOptions.GetScheduleDates(objDOC);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("CheckOptionsAdaptor.GetScheduleDates.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objOptions = null;
                objDOC = null;
            }
        }
		#endregion
	}
}
