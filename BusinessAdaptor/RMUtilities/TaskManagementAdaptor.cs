﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 05/25/2015 | RMA-4606         | nshah28   | Import third party Currency Exchange Rates from flat file
 **********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{

    ///************************************************************** 
    ///* $File		: TaskManagementAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 31-Oct-2007
    ///* $Author	: Shruti Choudhary
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///	This class is used to call the application layer component for TaskManagement.
    /// </summary>
    public class TaskManagementAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        public TaskManagementAdaptor()
		{
        }
        #endregion

        #region Public Functions

        #region Save Functions

        /// <summary>
        /// This method saves the user entered settings in case of OneTime Schedule Type.
        /// </summary>
        /// <param name="p_objXmlIn">
        /// The structure of input XML document would be:
        ///     <Details> 
        ///         <TaskType /> 
        ///         <TaskTypeText /> 
        ///         <ScheduleType /> 
        ///         <ScheduleTypeText></ScheduleTypeText>
        ///         <Date></Date> 
        ///         <Time></Time>
        ///     </Details>
        /// </param>
        /// <param name="p_objXmlOut">
        /// The structure of output XML document would be same as input
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool SaveOneTimeSettings(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;
            XmlElement objTmpElement = null;
            try
            {
               // if (p_objXmlIn.SelectSingleNode("//IsDataIntegratorTask").InnerText == "-1" && p_objXmlIn.SelectSingleNode("//SystemModuleName").InnerText != "PrintCheckBatch" && p_objXmlIn.SelectSingleNode("//SystemModuleName").InnerText != "PolicySystemUpdate" && p_objXmlIn.SelectSingleNode("//SystemModuleName").InnerText != "ClaimBalancing")
                //RMA-4606 nshah28(Comment above condition and including below condition for CurrencyExchangeInterface)
                if (p_objXmlIn.SelectSingleNode("//IsDataIntegratorTask").InnerText == "-1" && p_objXmlIn.SelectSingleNode("//SystemModuleName").InnerText != "PrintCheckBatch" && p_objXmlIn.SelectSingleNode("//SystemModuleName").InnerText != "PolicySystemUpdate" && p_objXmlIn.SelectSingleNode("//SystemModuleName").InnerText != "ClaimBalancing" && p_objXmlIn.SelectSingleNode("//SystemModuleName").InnerText != "CurrencyExchangeInterface")
                {
                    p_objXmlOut = p_objXmlIn;
                    return true;
                }


                objTaskManagement = new TaskManager(userLogin, base.ClientId);//Add by kuladeep for Cloud.
                objTaskManagement.SaveOneTimeSettings(p_objXmlIn);//Add by kuladeep for Cloud.
                p_objXmlOut = p_objXmlIn;
                objTmpElement = (XmlElement)p_objXmlOut.SelectSingleNode("//saved");
                if (objTmpElement != null)
                {
                    objTmpElement.InnerText = "true";
                }
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.SaveOneTimeSettings.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        /// <summary>
        /// This method saves the user entered settings in case of Monthly Schedule Type.
        /// </summary>
        /// <param name="p_objXmlIn">
        /// The structure of input XML document would be:
        ///     <Details> 
        ///         <IntervalType></IntervalType>
        ///         <TaskType /> 
        ///         <TaskTypeText /> 
        ///         <ScheduleType /> 
        ///         <ScheduleTypeText></ScheduleTypeText>
        ///         <Date></Date> 
        ///         <Time></Time>
        ///         <IntervalTypeId></IntervalTypeId>
        ///         <Interval></Interval>
        ///     </Details>
        /// </param>
        /// <param name="p_objXmlOut">
        /// The structure of output XML document would be same as input
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool SavePeriodicalSettings(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objTmpElement = null;
            TaskManager objTaskManagement = null;
            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);//Add by kuladeep for Cloud.
                objTaskManagement.SavePeriodicalSettings(p_objXmlIn);
                p_objXmlOut = p_objXmlIn;
                objTmpElement = (XmlElement)p_objXmlOut.SelectSingleNode("//saved");
                if (objTmpElement != null)
                {
                    objTmpElement.InnerText = "true";
                }

            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.SavePeriodicalSettings.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        /// <summary>
        /// This method saves the user entered settings in case of Weekly Schedule Type.
        /// </summary>
        /// <param name="p_objXmlIn">
        /// The structure of input XML document would be:
        ///     <Details> 
        ///         <TaskType /> 
        ///         <TaskTypeText /> 
        ///         <ScheduleType /> 
        ///         <ScheduleTypeText></ScheduleTypeText>
        ///         <Date></Date> 
        ///         <Time></Time>
        ///         <Mon_Run></Mon_Run>
        ///         <Tue_Run></Tue_Run>
        ///         <Wed_Run></Wed_Run>
        ///         <Thu_Run></Thu_Run>
        ///         <Fri_Run></Fri_Run>
        ///         <Sat_Run></Sat_Run>
        ///         <Sun_Run></Sun_Run>
        ///     </Details>
        /// </param>
        /// <param name="p_objXmlOut">
        /// The structure of output XML document would be same as input
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool SaveWeeklySettings(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objTmpElement = null;
            TaskManager objTaskManagement = null;
            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);//Add by kuladeep for Cloud.
                objTaskManagement.SaveWeeklySettings(p_objXmlIn);
                p_objXmlOut = p_objXmlIn;
                objTmpElement = (XmlElement)p_objXmlOut.SelectSingleNode("//saved");
                if (objTmpElement != null)
                {
                    objTmpElement.InnerText = "true";
                }
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.SaveWeeklySettings.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        /// <summary>
        /// This method saves the user entered settings in case of Yearly Schedule Type.
        /// </summary>
        /// <param name="p_objXmlIn">
        /// The structure of input XML document would be:
        ///     <Details> 
        ///         <TaskType /> 
        ///         <TaskTypeText /> 
        ///         <ScheduleType /> 
        ///         <ScheduleTypeText></ScheduleTypeText>
        ///         <Date></Date> 
        ///         <Time></Time>
        ///         <Jan_Run></Jan_Run>
        ///         <Feb_Run></Feb_Run>
        ///         <Mar_Run></Mar_Run>
        ///         <Apr_Run></Apr_Run>
        ///         <May_Run></May_Run>
        ///         <Jun_Run></Jun_Run>
        ///         <Jul_Run></Jul_Run>
        ///         <Aug_Run></Aug_Run>
        ///         <Sep_Run></Sep_Run>
        ///         <Oct_Run></Oct_Run>
        ///         <Nov_Run></Nov_Run>
        ///         <Dec_Run></Dec_Run>
        ///     </Details>
        /// </param>
        /// <param name="p_objXmlOut">
        /// The structure of output XML document would be same as input
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool SaveYearlySettings(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objTmpElement = null;
            TaskManager objTaskManagement = null;
            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);//Add by kuladeep for Cloud.
                objTaskManagement.SaveYearlySettings(p_objXmlIn);
                p_objXmlOut = p_objXmlIn;
                objTmpElement = (XmlElement)p_objXmlOut.SelectSingleNode("//saved");
                if (objTmpElement != null)
                {
                    objTmpElement.InnerText = "true";
                }
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.SaveYearlySettings.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        /// <summary>
        /// This method saves the user entered settings in case of Monthly Schedule Type.
        /// </summary>
        /// <param name="p_objXmlIn">
        /// The structure of input XML document would be:
        ///     <Details> 
        ///         <TaskType /> 
        ///         <TaskTypeText /> 
        ///         <ScheduleType /> 
        ///         <ScheduleTypeText></ScheduleTypeText>
        ///         <DayOfMonth></DayOfMonth> 
        ///         <Time></Time>
        ///         <Month></Month>
        ///     </Details>
        /// </param>
        /// <param name="p_objXmlOut">
        /// The structure of output XML document would be same as input
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool SaveMonthlySettings(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objTmpElement = null;
            TaskManager objTaskManagement = null;
            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);//Add by kuladeep for Cloud.
                objTaskManagement.SaveMonthlySettings(p_objXmlIn);
                p_objXmlOut = p_objXmlIn;
                objTmpElement = (XmlElement)p_objXmlOut.SelectSingleNode("//saved");
                if (objTmpElement != null)
                {
                    objTmpElement.InnerText = "true";
                }
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.SaveMonthlySettings.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        #endregion

        # region Get Functions

        /// <summary>
        /// This method gets the available task types and schedule types from db.
        /// </summary>
        /// <param name="p_objXmlIn">Not useful here</param>
        /// <param name="p_objXmlOut">
        /// The structure of output XML document would be:
        /// <Data>
        ///     <TaskType>
        ///         <option id="" /> 
        ///         <option id="0">Process WPA Diaries</option> 
        ///     </TaskType>
        ///     <ScheduleType>
        ///         <option id="" /> 
        ///         <option id="1">OneTime</option> 
        ///         <option id="2">Periodically</option> 
        ///         <option id="3">Weekly</option> 
        ///         <option id="4">Monthly</option> 
        ///         <option id="5">Yearly</option> 
        ///         <option id="6">Directory Triggered</option> 
        ///     </ScheduleType>
        /// </Data>
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool GetDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;
            XmlDocument objTmpDocument = null;
            string PageId = string.Empty;
            XmlNode objNode = null;
            
            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);//Add by kuladeep for Cloud.
                objNode = (XmlElement)p_objXmlIn.SelectSingleNode("//PageId");
                PageId = objNode.InnerXml;
                p_objXmlOut = objTaskManagement.GetDetails(PageId);
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.GetDetails.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }


        //public bool GetConfigXml(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        //{
        //    TaskManager objTaskManagement = null;
        //    XmlDocument objTmpDocument = null;

        //    try
        //    {
        //        objTaskManagement = new TaskManager(userLogin);
        //        p_objXmlOut = objTaskManagement.GetConfigXml(p_objXmlIn);
        //    }
        //    catch (RMAppException objException)
        //    {
        //        p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
        //        return false;
        //    }
        //    catch (Exception objException)
        //    {
        //        p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.GetDetails.Error"), BusinessAdaptorErrorType.Error);
        //        return false;
        //    }
        //    finally
        //    {
        //        if (objTaskManagement != null)
        //            objTaskManagement = null;
        //    }

        //    return true;
        //}

        
        /// <summary>
        /// This method gets the interval types
        /// in case of schedule type Periodical.
        /// </summary>
        /// <param name="p_objXmlIn">
        /// The structure of input XML document would be:
        ///     <Details> 
        ///         <IntervalType /> 
        ///         <IntervalTypeId /> 
        ///         <Interval /> 
        ///         <TaskTypeText></TaskTypeText>
        ///         <ScheduleTypeText></ScheduleTypeText> 
        ///         <TaskType></TaskType>
        ///         <ScheduleType>2</ScheduleType>
        ///         <Time /> 
        ///         <Date />
        ///     </Details>
        /// </param>
        /// <param name="p_objXmlOut">
        /// The structure of output XML document would be:
        ///     <Details> 
        ///         <IntervalType> 
        ///             <option id="" /> 
        ///             <option id="1">Minutes</option> 
        ///             <option id="2">Days</option>
        ///             <option id="3">Weeks</option>
        ///             <option id="4">Months</option>
        ///         </IntervalType>
        ///         <IntervalTypeId /> 
        ///         <Interval /> 
        ///         <TaskTypeText></TaskTypeText>
        ///         <ScheduleTypeText></ScheduleTypeText> 
        ///         <TaskType></TaskType>
        ///         <ScheduleType>2</ScheduleType>
        ///         <Time /> 
        ///         <Date />
        ///     </Details>
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool GetIntervalType(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;
            XmlNode objNode = null;
            XmlNode objTmpNode = null;

            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);//Add by kuladeep for Cloud.
                objNode = p_objXmlIn.CreateElement("IntervalType");
                objNode.InnerXml = objTaskManagement.GetIntervalType().InnerXml;
                objTmpNode = p_objXmlIn.SelectSingleNode("//IntervalType");
                p_objXmlIn.DocumentElement.ReplaceChild(objNode, objTmpNode);
                p_objXmlOut = p_objXmlIn;

            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.GetIntervalType.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        //JIRA RMA-4606 nshah28 start
        /// <summary>
        /// This method gets the Document path of specific dsn
        /// </summary>
        /// <param name="p_objXmlIn">
        /// <Details><dsnid>ID</dsnid> 
        /// <docpath>Path</docpath></Details>
        /// </param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetDocPath(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManager = null;
            XmlNode objXmlNode = null;
            XmlNode objTmpNode = null;
            objTmpNode = p_objXmlIn.SelectSingleNode("//docpath");

            try
            {
               
                objXmlNode = p_objXmlIn.CreateElement("docpath");
                if (Convert.ToString(userLogin.objRiskmasterDatabase.DocPathType) == "0")
                {
                    objXmlNode.InnerText = Convert.ToString(userLogin.objRiskmasterDatabase.GlobalDocPath);
                }

                p_objXmlIn.DocumentElement.ReplaceChild(objXmlNode, objTmpNode);
                p_objXmlOut = p_objXmlIn;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.GetDocPath.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManager != null)
                    objTaskManager = null;
            }
            return true;
        }
        //JIRA RMA-4606 nshah28 end
        /// <summary>
        /// MGaba2:R7:History Tracking:This function reads the maximum limit of interval 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetMaxInterval(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;           

            try
            {
                objTaskManagement = new TaskManager(userLogin,base.ClientId); //Add by kuladeep for Cloud.
                p_objXmlOut = objTaskManagement.GetMaxIntervalLimit(p_objXmlIn);               
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.GetMaxInterval.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        /// <summary>
        /// This method gets all the tasks that are running or completed.
        /// </summary>
        /// <param name="p_objXmlIn">
        /// The structure of input XML document is insignificant here.
        /// </param>
        /// <param name="p_objXmlOut">
        /// The structure of output XML document would be:
        ///     <Document> 
        ///         <TaskInfoList /> 
        ///             <listhead /> 
        ///                 <jobid>Job Id</jobid>
        ///                 <taskname>Task Name</taskname>
        ///                 <description>Description</description>
        ///                 <taskstate>Task State</taskstate>
        ///                 <startdttm>Start Date/Time</startdttm>
        ///                 <enddttm>End Date/Time</enddttm>
        ///             </listhead>
        ///             <option>
        ///                 <jobid>2</jobid>
        ///                 <taskname>Process WPA Diaries</taskname>
        ///                 <description>Processes WPA Diaries</description>
        ///                 <taskstate>Running</taskstate>
        ///                 <startdttm>11/12/2007 5:45 PM</startdttm>
        ///                 <enddttm></enddttm>
        ///             </option>
        ///     </Document>
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool GetJobs(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;
            XmlNode objNode = null;
            int iPageNumber = 0;
            //ksahu5 - ML Changes - MITS 33942 -start
            string sLangCode = string.Empty;
            string sPageId = string.Empty;
            objNode = (XmlElement)p_objXmlIn.SelectSingleNode("//LangCodeID");
            sLangCode = objNode.InnerXml;

            objNode = (XmlElement)p_objXmlIn.SelectSingleNode("//PageID");
            sPageId = objNode.InnerXml;

            //ksahu5 - ML Changes - MITS 33942 -end
            try
            {
                objNode = p_objXmlIn.SelectSingleNode("//PageNumber");

                if (objNode != null)
                {
                    iPageNumber = Conversion.ConvertObjToInt(objNode.InnerText, base.ClientId);    
                }

                objTaskManagement = new TaskManager(userLogin, base.ClientId);//Add by kuladeep for Cloud.
                p_objXmlOut = objTaskManagement.GetJobs(iPageNumber,sPageId, sLangCode);  //ksahu5 - ML Changes - MITS 33942
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.GetJobs.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        /// <summary>
        /// This method gets all the tasks that are scheduled.
        /// </summary>
        /// <param name="p_objXmlIn">
        /// The structure of input XML document is insignificant here.
        /// </param>
        /// <param name="p_objXmlOut">
        /// The structure of output XML document would be:
        ///     <Document> 
        ///         <TaskInfoList /> 
        ///             <listhead /> 
        ///                 <scheduleid>Schedule Id</scheduleid>
        ///                 <taskname>Task Name</taskname>
        ///                 <scheduletype>Schedule Type</scheduletype>
        ///                 <scheduletypeid>Schedule Type Id</scheduletypeid>
        ///                 <nextrundttm>Next Run Date/Time</nextrundttm>
        ///             </listhead>
        ///             <option>
        ///                 <scheduleid>2</scheduleid>
        ///                 <taskname>Process WPA Diaries</taskname>
        ///                 <scheduletype>OneTime</scheduletype>
        ///                 <scheduletypeid>1</scheduletypeid>
        ///                 <nextrundttm>11/12/2007 5:45 PM</nextrundttm>
        ///             </option>
        ///     </Document>
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool GetScheduledJobs(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;


            //ksahu5 - ML Changes - MITS 33897 -start
            XmlNode objNode = null;
            string sLangCode = string.Empty;
            string sPageId = string.Empty;
            objNode = (XmlElement)p_objXmlIn.SelectSingleNode("//LangCodeID");
            sLangCode = objNode.InnerXml;

            objNode = (XmlElement)p_objXmlIn.SelectSingleNode("//PageID");
            sPageId = objNode.InnerXml;

            //ksahu5 - ML Changes - MITS 33897 -end
            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);//Add by kuladeep for Cloud.
                p_objXmlOut = objTaskManagement.GetScheduledJobs(sPageId, sLangCode);  //ksahu5 - ML Changes - MITS 33897
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.GetScheduledJobs.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }


        # endregion

        # region Edit
        /// <summary>
        /// This method gets the xml for editing a schedule.
        /// </summary>
        /// <param name="p_objXmlIn">
        /// The structure of input XML document would be:
        ///     <Document> 
        ///         <ScheduleId>34</ScheduleId>
        ///     </Document>
        /// </param>
        /// <param name="p_objXmlOut">
        /// The structure of output XML document would be:
        ///     <Data> 
        ///         <ScheduleId>34</ScheduleId>
        ///         <TaskType>0</TaskType>
        ///         <ScheduleTypeId>1</ScheduleTypeId>
        ///         <Date>11/20/2007</Date>
        ///         <Time>4:00 AM</Time>
        ///         <TaskTypeText>Process WPA Diaries</TaskTypeText>
        ///         <ScheduleTypeText>OneTime</ScheduleTypeText>
        ///         <ScheduleType>
        ///             <option id="1">OneTime</option>
        ///             <option id="2">Periodically</option>
        ///             <option id="3">Weekly</option>
        ///             <option id="4">Monthly</option>
        ///             <option id="5">Yearly</option>
        ///             <option id="6">Directory Triggered</option>
        ///         </ScheduleType>
        ///     </Data>
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool EditOneTime(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;
            XmlElement objTmpElement = null;

            try
            {
                objTaskManagement = new TaskManager(userLogin,base.ClientId);// Code change by kuladeep for Jira-108
                if (p_objXmlIn.SelectSingleNode("//saved") != null && p_objXmlIn.SelectSingleNode("//saved").InnerText == "true")
                {
                    p_objXmlOut = objTaskManagement.EditOneTime(p_objXmlIn);
                    objTmpElement = p_objXmlOut.CreateElement("saved");
                    objTmpElement.InnerText = "true";
                    p_objXmlOut.SelectSingleNode("//Data").AppendChild(objTmpElement);
                }
                else
                {
                    p_objXmlOut = objTaskManagement.EditOneTime(p_objXmlIn);
                }
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.Edit.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        /// <summary>
        /// This method gets the xml for editing a Weekly schedule.
        /// </summary>
        /// <param name="p_objXmlIn">
        /// The structure of input XML document would be:
        ///     <Document> 
        ///         <ScheduleId>34</ScheduleId>
        ///     </Document>
        /// </param>
        /// <param name="p_objXmlOut">
        /// The structure of output XML document would be:
        ///     <Data> 
        ///         <ScheduleId>34</ScheduleId>
        ///         <TaskType>0</TaskType>
        ///         <ScheduleTypeId>3</ScheduleTypeId>
        ///         <Date>11/20/2007</Date>
        ///         <Time>4:00 AM</Time>
        ///         <Mon_Run>False</Mon_Run>
        ///         <Tue_Run>False</Tue_Run>
        ///         <Wed_Run>False</Wed_Run>
        ///         <Thu_Run>False</Thu_Run>
        ///         <Fri_Run>False</Fri_Run>
        ///         <Sat_Run>True</Sat_Run>
        ///         <Sun_Run>False</Sun_Run>
        ///         <TaskTypeText>Process WPA Diaries</TaskTypeText>
        ///         <ScheduleTypeText>Weekly</ScheduleTypeText>
        ///         <ScheduleType>
        ///             <option id="1">OneTime</option>
        ///             <option id="2">Periodically</option>
        ///             <option id="3">Weekly</option>
        ///             <option id="4">Monthly</option>
        ///             <option id="5">Yearly</option>
        ///             <option id="6">Directory Triggered</option>
        ///         </ScheduleType>
        ///     </Data>
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool EditWeekly(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;
            XmlElement objTmpElement = null;

            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);// Code change by kuladeep for Jira-108
                if (p_objXmlIn.SelectSingleNode("//saved") != null && p_objXmlIn.SelectSingleNode("//saved").InnerText == "true")
                {
                    p_objXmlOut = objTaskManagement.EditWeekly(p_objXmlIn);
                    objTmpElement = p_objXmlOut.CreateElement("saved");
                    objTmpElement.InnerText = "true";
                    p_objXmlOut.SelectSingleNode("//Data").AppendChild(objTmpElement);
                }
                else
                {
                    p_objXmlOut = objTaskManagement.EditWeekly(p_objXmlIn);
                }
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.Edit.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        /// <summary>
        /// This method gets the xml for editing an Yearly schedule.
        /// </summary>
        /// <param name="p_objXmlIn">
        /// The structure of input XML document would be:
        ///     <Document> 
        ///         <ScheduleId>34</ScheduleId>
        ///     </Document>
        /// </param>
        /// <param name="p_objXmlOut">
        /// The structure of output XML document would be:
        ///     <Data> 
        ///         <ScheduleId>34</ScheduleId>
        ///         <TaskType>0</TaskType>
        ///         <ScheduleTypeId>5</ScheduleTypeId>
        ///         <Date>11/20/2007</Date>
        ///         <Time>4:00 AM</Time>
        ///         <Jan_Run>False</Jan_Run>
        ///         <Feb_Run>False</Feb_Run>
        ///         <Mar_Run>False</Mar_Run>
        ///         <Apr_Run>False</Apr_Run>
        ///         <May_Run>False</May_Run>
        ///         <Jun_Run>False</Jun_Run>
        ///         <Jul_Run>True</Jul_Run>
        ///         <Aug_Run>False</Aug_Run>
        ///         <Sep_Run>False</Sep_Run>
        ///         <Oct_Run>False</Oct_Run>
        ///         <Nov_Run>False</Nov_Run>
        ///         <Dec_Run>False</Dec_Run>
        ///         <TaskTypeText>Process WPA Diaries</TaskTypeText>
        ///         <ScheduleTypeText>Yearly</ScheduleTypeText>
        ///         <ScheduleType>
        ///             <option id="1">OneTime</option>
        ///             <option id="2">Periodically</option>
        ///             <option id="3">Weekly</option>
        ///             <option id="4">Monthly</option>
        ///             <option id="5">Yearly</option>
        ///             <option id="6">Directory Triggered</option>
        ///         </ScheduleType>
        ///     </Data>
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool EditYearly(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;
            XmlElement objTmpElement = null;

            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);// Code change by kuladeep for Jira-108
                if (p_objXmlIn.SelectSingleNode("//saved") != null && p_objXmlIn.SelectSingleNode("//saved").InnerText == "true")
                {
                    p_objXmlOut = objTaskManagement.EditYearly(p_objXmlIn);
                    objTmpElement = p_objXmlOut.CreateElement("saved");
                    objTmpElement.InnerText = "true";
                    p_objXmlOut.SelectSingleNode("//Data").AppendChild(objTmpElement);
                }
                else
                {
                    p_objXmlOut = objTaskManagement.EditYearly(p_objXmlIn);
                }
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.Edit.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        /// <summary>
        /// This method gets the xml for editing a Monthly schedule.
        /// </summary>
        /// <param name="p_objXmlIn">
        /// The structure of input XML document would be:
        ///     <Document> 
        ///         <ScheduleId>34</ScheduleId>
        ///     </Document>
        /// </param>
        /// <param name="p_objXmlOut">
        /// The structure of output XML document would be:
        ///     <Data> 
        ///         <ScheduleId>34</ScheduleId>
        ///         <TaskType>0</TaskType>
        ///         <ScheduleTypeId>5</ScheduleTypeId>
        ///         <Month>7</Month>
        ///         <DayOfMonth>24</DayOfMonth>
        ///         <Time>4:00 AM</Time>
        ///         <TaskTypeText>Process WPA Diaries</TaskTypeText>
        ///         <ScheduleTypeText>Yearly</ScheduleTypeText>
        ///         <ScheduleType>
        ///             <option id="1">OneTime</option>
        ///             <option id="2">Periodically</option>
        ///             <option id="3">Weekly</option>
        ///             <option id="4">Monthly</option>
        ///             <option id="5">Yearly</option>
        ///             <option id="6">Directory Triggered</option>
        ///         </ScheduleType>
        ///     </Data>
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool EditMonthly(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;
            XmlElement objTmpElement = null;

            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);// Code change by kuladeep for Jira-108
                if (p_objXmlIn.SelectSingleNode("//saved") != null && p_objXmlIn.SelectSingleNode("//saved").InnerText == "true")
                {
                    p_objXmlOut = objTaskManagement.EditMonthly(p_objXmlIn);
                    objTmpElement = p_objXmlOut.CreateElement("saved");
                    objTmpElement.InnerText = "true";
                    p_objXmlOut.SelectSingleNode("//Data").AppendChild(objTmpElement);
                }
                else
                {
                    p_objXmlOut = objTaskManagement.EditMonthly(p_objXmlIn);
                }
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.Edit.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        /// <summary>
        /// This method gets the xml for editing a Periodical schedule.
        /// </summary>
        /// <param name="p_objXmlIn">
        /// The structure of input XML document would be:
        ///     <Document> 
        ///         <ScheduleId>34</ScheduleId>
        ///     </Document>
        /// </param>
        /// <param name="p_objXmlOut">
        /// The structure of output XML document would be:
        ///     <Data> 
        ///         <ScheduleId>34</ScheduleId>
        ///         <TaskType>0</TaskType>
        ///         <ScheduleTypeId>5</ScheduleTypeId>
        ///         <Date>7/24/2008</Date>
        ///         <Time>4:00 AM</Time>
        ///         <IntervalTypeId>1</IntervalTypeId>
        ///         <Interval>3</Interval>
        ///         <TaskTypeText>Process WPA Diaries</TaskTypeText>
        ///         <ScheduleTypeText>Yearly</ScheduleTypeText>
        ///         <ScheduleType>
        ///             <option id="1">OneTime</option>
        ///             <option id="2">Periodically</option>
        ///             <option id="3">Weekly</option>
        ///             <option id="4">Monthly</option>
        ///             <option id="5">Yearly</option>
        ///             <option id="6">Directory Triggered</option>
        ///         </ScheduleType>
        ///         <IntervalType>
        ///             <option id="1">Minutes</option>
        ///             <option id="2">Days</option>
        ///             <option id="3">Weeks</option>
        ///             <option id="4">Months</option>
        ///         </IntervalType>
        ///     </Data>
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool EditPeriodical(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;
            XmlElement objTmpElement = null;

            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);// Code change by kuladeep for Jira-108
                if (p_objXmlIn.SelectSingleNode("//saved") != null && p_objXmlIn.SelectSingleNode("//saved").InnerText == "true")
                {
                    p_objXmlOut = objTaskManagement.EditPeriodical(p_objXmlIn);
                    objTmpElement = p_objXmlOut.CreateElement("saved");
                    objTmpElement.InnerText = "true";
                    p_objXmlOut.SelectSingleNode("//Data").AppendChild(objTmpElement);
                }
                else
                {
                    p_objXmlOut = objTaskManagement.EditPeriodical(p_objXmlIn);
                }
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.Edit.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        # endregion

        # region Schedule to Kill

        /// <summary>
        /// This method changes the status of the task to schedule to run.
        /// </summary>
        /// <param name="p_objXmlIn">
        /// The input XML document.
        /// </param>
        /// <param name="p_objXmlOut">
        /// The output XML document.
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool KillTask(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;
            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);// Code change by kuladeep for Jira-108
                objTaskManagement.KillTask(p_objXmlIn);
                p_objXmlOut = p_objXmlIn;
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.KillTask.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        /// <summary>
        /// This method gets the Status Details for a specific job id.
        /// </summary>
        /// <param name="p_objXmlIn">Contains the job id for which the status is to be retrieved.</param>
        /// <param name="p_objXmlOut">Contains the Status Details for the job id</param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool GetStatusDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;
            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);//Add by kuladeep for cloud.
                p_objXmlOut = objTaskManagement.GetStatusDetails(p_objXmlIn);
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetStatusDetails.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        # endregion

        # region Delete Schedule

        /// <summary>
        /// Deletes the selected schedule.
        /// </summary>
        /// <param name="p_objXmlIn">Input xml</param>
        /// <param name="p_objXmlOut">output xml</param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool DeleteSchedule(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;
            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);// Code change by kuladeep for Jira-108
                objTaskManagement.DeleteSchedule(p_objXmlIn);
                p_objXmlOut = p_objXmlIn;
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.BusinessAdaptor.RMUtilities.TaskManagementAdaptor.DeleteSchedule.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        # endregion

        #endregion



        /// <summary>
        /// This method gets all the tasks from TM_SCHEDULE having TM_USER and TM_DSN column NULL.
        /// </summary>
        /// <param name="p_objXmlIn">
        /// The structure of input XML document is insignificant here.
        /// </param>
        /// <param name="p_objXmlOut">
        /// The structure of output XML document would be:
        /// <Document> 
        /// <ScheduledTasks>
        ///  <Tasks>
        ///    <option value="65">Process WPA Diaries</option>
        ///    <option value="66">Billing Scheduler</option>
        ///    <option value="67">BES Scheduler</option>
        ///  </Tasks>
        ///  <control name="DataSources" type="combobox">
        ///    <option value="609">ARP_1659</option>
        ///    <option value="554">BES_MainDB_PJS</option>
        ///    <option value="612">Boeing</option>    
        ///  </control>
        ///  <control name="Users" type="combobox">
        ///    <option value="612">csc (c sc)</option>
        ///  </control>
        ///</ScheduledTasks>

        ///     </Document>
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool GetTasksForTaskMgrDiary(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;
            bool blnIsTaskExist ;

            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);//Add by kuladeep for Cloud.
                p_objXmlOut = objTaskManagement.GetTasksForTaskMgrDiary(p_objXmlIn, out blnIsTaskExist);
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetTasksForTaskMgrDiary.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        public bool GetPolicySystems(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;

            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);//Add by kuladeep for Cloud.
                p_objXmlOut = objTaskManagement.GetPolicySystems();
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("TaskmanagementAdaptor.GetPolicySystems.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }

            return true;
        }

        /// <summary>
        /// This method updates the TM_USER and TM_DSN columns of the TM_SCHEDULE table.
        /// </summary>
        /// <param name="p_objXmlIn">
        /// The structure of input XML document would be:
        ///     <ScheduledTasks>
        /// <Tasks>65</Tasks>
        ///<DataSourceID></DataSourceID>
        /// <DataSourceName>ARP_1659</DataSourceName>
        /// <User>csc</User>
        /// </ScheduledTasks>
        /// </param>
        /// <param name="p_objXmlOut">        
        /// </param>
        /// <param name="p_objErrOut">Collection of Error Messages</param>
        /// <returns>True/False for success or failure of the function</returns>    
        public bool UpdateTasksForTaskMgrDiary(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;
            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);//Add by kuladeep for Cloud.
                objTaskManagement.UpdateTasksForTaskMgrDiary(p_objXmlIn);               
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.UpdateTasksForTaskMgrDiary.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }
            return true;
        }

        public bool GetBankAccList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            TaskManager objTaskManagement = null;
            try
            {
                objTaskManagement = new TaskManager(userLogin, base.ClientId);//Add by kuladeep for Cloud.
                p_objXmlOut = objTaskManagement.GetAccList(p_objXmlIn);
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception objException)
            {
                p_objErrOut.Add(objException, Globalization.GetString("Riskmaster.Application.RMUtilities.TaskManager.GetBankAccList.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objTaskManagement != null)
                    objTaskManagement = null;
            }
            return true;
        }

    }
}
