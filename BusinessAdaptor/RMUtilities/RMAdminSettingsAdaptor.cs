using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: RMAdminSettingsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 27/05/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of Admin Customization Setting Form.
	/// </summary>
	public class RMAdminSettingsAdaptor:BusinessAdaptorBase
	{
        private string m_strUtilityImagesPath = string.Empty;

		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RMAdminSettingsAdaptor()
        {
            m_strUtilityImagesPath = RMConfigurationManager.GetNameValueSectionSettings("RMUtilities", this.connectionString, base.ClientId)["RMUtilImagesPath"];
        }

		#endregion

		#region Public Methods

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.RMAdminSettings.GetAdminConfig() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.RMAdminSettings.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetAdminConfig(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			RMAdminSettings objAdm=null; //Application layer component		
			XmlElement objElm=null;
			XmlNodeList objNodeList=null;
			try
			{
				objElm=(XmlElement)p_objXmlIn.SelectSingleNode("//FileName");
				if(objElm==null)
					throw new RMAppException(Globalization.GetString("RMAdminSettingsAdaptor.MissingFileName", base.ClientId));//rkaur27



                objAdm = new RMAdminSettings(RMConfigurationSettings.GetSessionDSN(base.ClientId), this.connectionString, base.ClientId);//rkaur27
				p_objXmlOut=objAdm.GetAdminConfig(objElm.InnerText);

				objNodeList=p_objXmlIn.SelectNodes("//Images");
				if(objNodeList.Count>0)
				{
					objElm=(XmlElement)p_objXmlOut.SelectSingleNode("//ImageTop");
                    objElm.SetAttribute("imagepath", m_strUtilityImagesPath);
					objElm.SetAttribute("imageurl",m_strUtilityImagesPath);

					objElm=(XmlElement)p_objXmlOut.SelectSingleNode("//ImageTop/Top1/current");
                    if (objElm.Attributes["name"].Value != string.Empty)
                    {
                        //srajindersin - Pentesting - Cross site Scripting - MITS 27656
                        objElm.InnerText = GetImage(CommonFunctions.HTMLCustomEncode(objElm.Attributes["name"].Value), ref p_objErrOut);
                        //objElm.InnerText = GetImage(objElm.Attributes["name"].Value, ref p_objErrOut);
                        //END srajindersin - Pentesting - Cross site Scripting - MITS 27656
                    }
                    objElm = (XmlElement)p_objXmlOut.SelectSingleNode("//ImageTop/Top1/default");
                    if (objElm.Attributes["name"].Value != string.Empty)
                    {
                        //srajindersin - Pentesting - Cross site Scripting - MITS 27656
                        objElm.InnerText = GetImage(CommonFunctions.HTMLCustomEncode(objElm.Attributes["name"].Value), ref p_objErrOut);
                        //objElm.InnerText = GetImage(objElm.Attributes["name"].Value, ref p_objErrOut);
                        //END srajindersin - Pentesting - Cross site Scripting - MITS 27656
                    }
                    objElm = (XmlElement)p_objXmlOut.SelectSingleNode("//ImageTop/Top2/current");
                    if (objElm.Attributes["name"].Value != string.Empty)
                    {
                        //srajindersin - Pentesting - Cross site Scripting - MITS 27656
                        objElm.InnerText = GetImage(CommonFunctions.HTMLCustomEncode(objElm.Attributes["name"].Value), ref p_objErrOut);
                        //objElm.InnerText = GetImage(objElm.Attributes["name"].Value, ref p_objErrOut);
                        //END srajindersin - Pentesting - Cross site Scripting - MITS 27656
                    }
                    objElm = (XmlElement)p_objXmlOut.SelectSingleNode("//ImageTop/Top2/default");
                    if (objElm.Attributes["name"].Value != string.Empty)
                    {
                        //srajindersin - Pentesting - Cross site Scripting - MITS 27656
                        objElm.InnerText = GetImage(CommonFunctions.HTMLCustomEncode(objElm.Attributes["name"].Value), ref p_objErrOut);
                        //objElm.InnerText = GetImage(objElm.Attributes["name"].Value, ref p_objErrOut);
                        //END srajindersin - Pentesting - Cross site Scripting - MITS 27656
                    }
                    objElm = (XmlElement)p_objXmlOut.SelectSingleNode("//ImageTop/Top3/current");
                    if (objElm.Attributes["name"].Value != string.Empty)
                    {
                        //srajindersin - Pentesting - Cross site Scripting - MITS 27656
                        objElm.InnerText = GetImage(CommonFunctions.HTMLCustomEncode(objElm.Attributes["name"].Value), ref p_objErrOut);
                        //objElm.InnerText = GetImage(objElm.Attributes["name"].Value, ref p_objErrOut);
                        //END srajindersin - Pentesting - Cross site Scripting - MITS 27656
                    }
                    objElm = (XmlElement)p_objXmlOut.SelectSingleNode("//ImageTop/Top3/default");
                    if (objElm.Attributes["name"].Value != string.Empty)
                    {
                        //srajindersin - Pentesting - Cross site Scripting - MITS 27656
                        objElm.InnerText = GetImage(CommonFunctions.HTMLCustomEncode(objElm.Attributes["name"].Value), ref p_objErrOut);
                        //objElm.InnerText = GetImage(objElm.Attributes["name"].Value, ref p_objErrOut);
                        //END srajindersin - Pentesting - Cross site Scripting - MITS 27656
                    }

					objElm=(XmlElement)p_objXmlOut.SelectSingleNode("//ImageTopAdmin");
					objElm.SetAttribute("imagepath",m_strUtilityImagesPath);
					objElm.SetAttribute("imageurl",m_strUtilityImagesPath);

                    objElm = (XmlElement)p_objXmlOut.SelectSingleNode("//ImageTopAdmin/Top1/current");
                    if (objElm.Attributes["name"].Value != string.Empty)
                    {
                        //srajindersin - Pentesting - Cross site Scripting - MITS 27656
                        objElm.InnerText = GetImage(CommonFunctions.HTMLCustomEncode(objElm.Attributes["name"].Value), ref p_objErrOut);
                        //objElm.InnerText = GetImage(objElm.Attributes["name"].Value, ref p_objErrOut);
                        //END srajindersin - Pentesting - Cross site Scripting - MITS 27656
                    }
                    objElm = (XmlElement)p_objXmlOut.SelectSingleNode("//ImageTopAdmin/Top1/default");
                    if (objElm.Attributes["name"].Value != string.Empty)
                    {
                        //srajindersin - Pentesting - Cross site Scripting - MITS 27656
                        objElm.InnerText = GetImage(CommonFunctions.HTMLCustomEncode(objElm.Attributes["name"].Value), ref p_objErrOut);
                        //objElm.InnerText = GetImage(objElm.Attributes["name"].Value, ref p_objErrOut);
                        //END srajindersin - Pentesting - Cross site Scripting - MITS 27656
                    }
                    objElm = (XmlElement)p_objXmlOut.SelectSingleNode("//ImageTopAdmin/Top2/current");
                    if (objElm.Attributes["name"].Value != string.Empty)
                    {
                        //srajindersin - Pentesting - Cross site Scripting - MITS 27656
                        objElm.InnerText = GetImage(CommonFunctions.HTMLCustomEncode(objElm.Attributes["name"].Value), ref p_objErrOut);
                        //objElm.InnerText = GetImage(objElm.Attributes["name"].Value, ref p_objErrOut);
                        //END srajindersin - Pentesting - Cross site Scripting - MITS 27656
                    }
                    objElm = (XmlElement)p_objXmlOut.SelectSingleNode("//ImageTopAdmin/Top2/default");
                    if (objElm.Attributes["name"].Value != string.Empty)
                    {
                        //srajindersin - Pentesting - Cross site Scripting - MITS 27656
                        objElm.InnerText = GetImage(CommonFunctions.HTMLCustomEncode(objElm.Attributes["name"].Value), ref p_objErrOut);
                        //objElm.InnerText = GetImage(objElm.Attributes["name"].Value, ref p_objErrOut);
                        //END srajindersin - Pentesting - Cross site Scripting - MITS 27656
                    }
                    objElm = (XmlElement)p_objXmlOut.SelectSingleNode("//ImageTopAdmin/Top3/current");
                    if (objElm.Attributes["name"].Value != string.Empty)
                    {
                        //srajindersin - Pentesting - Cross site Scripting - MITS 27656
                        objElm.InnerText = GetImage(CommonFunctions.HTMLCustomEncode(objElm.Attributes["name"].Value), ref p_objErrOut);
                        //objElm.InnerText = GetImage(objElm.Attributes["name"].Value, ref p_objErrOut);
                        //END srajindersin - Pentesting - Cross site Scripting - MITS 27656
                    }
                    objElm = (XmlElement)p_objXmlOut.SelectSingleNode("//ImageTopAdmin/Top3/default");
                    if (objElm.Attributes["name"].Value != string.Empty)
                    {
                        //srajindersin - Pentesting - Cross site Scripting - MITS 27656
                        objElm.InnerText = GetImage(CommonFunctions.HTMLCustomEncode(objElm.Attributes["name"].Value), ref p_objErrOut);
                        //objElm.InnerText = GetImage(objElm.Attributes["name"].Value, ref p_objErrOut);
                        //END srajindersin - Pentesting - Cross site Scripting - MITS 27656
                    }
				}

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("RMAdminSettingsAdaptor.GetAdminConfig.Error", base.ClientId),//rkaur27
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objAdm=null;
			}
		}

		private string GetImage(string p_sFileName,  ref BusinessAdaptorErrors p_objErrOut)
		{
			RMAdminSettings objAdm=null; //Application layer component	
			try
			{
                objAdm = new RMAdminSettings(RMConfigurationSettings.GetSessionDSN(base.ClientId), this.connectionString, base.ClientId);//rkaur27
				return objAdm.GetImage(p_sFileName);  
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return string.Empty;
			}
			catch(Exception p_objException)
			{
                //dvatsa
				p_objErrOut.Add(p_objException, Globalization.GetString("RMAdminSettingsAdaptor.GetImage.Error", base.ClientId),//rkaur27
					BusinessAdaptorErrorType.Error);
				return string.Empty;
			}
			finally
			{
				objAdm=null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.RMAdminSettings.SaveAdminConfig() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.RMAdminSettings.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool SaveAdminConfig(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			RMAdminSettings objAdm=null; //Application layer component		
			try
			{
				
                //Changed by Gagan for MITS 10376 : Start
                //dvatsa
                objAdm = new RMAdminSettings(RMConfigurationSettings.GetSessionDSN(base.ClientId), this.connectionString, base.ClientId);
                //Changed by Gagan for MITS 10376 : End
				objAdm.SaveAdminConfig(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                //dvatsa
				p_objErrOut.Add(p_objException, Globalization.GetString("RMAdminSettingsAdaptor.SaveAdminConfig.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objAdm=null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.RMAdminSettings.GetImage() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.RMAdminSettings.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetImage(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			RMAdminSettings objAdm=null; //Application layer component	
			XmlElement objElm=null;
			try
			{
				objElm=(XmlElement)p_objXmlIn.SelectSingleNode("//FileName");
				if(objElm==null)
					throw new RMAppException(Globalization.GetString("RMAdminSettingsAdaptor.MissingFileName", base.ClientId));//rkaur27
                objAdm = new RMAdminSettings(RMConfigurationSettings.GetSessionDSN(base.ClientId), this.connectionString, base.ClientId);
				objAdm.GetImage(objElm.InnerText);  
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("RMAdminSettingsAdaptor.GetImage.Error", base.ClientId),//rkaur27
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objAdm=null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.RMAdminSettings.SetImage() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.RMAdminSettings.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool SetImage(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			RMAdminSettings objAdm=null; //Application layer component			
			XmlElement objElm=null;
			try
			{
				objElm=(XmlElement)p_objXmlIn.SelectSingleNode("//file");
				if(objElm==null)
					throw new RMAppException(Globalization.GetString("RMAdminSettingsAdaptor.MissingFileName", base.ClientId));//rkaur27
                //dvatsa
                objAdm = new RMAdminSettings(RMConfigurationSettings.GetSessionDSN(base.ClientId), this.connectionString, base.ClientId);
				objAdm.SetImage(objElm.Attributes["filename"].Value,objElm.InnerText); 
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                //dvatsa
				p_objErrOut.Add(p_objException, Globalization.GetString("RMAdminSettingsAdaptor.SetImage.Error", base.ClientId),//rkaur27
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objAdm=null;
			}
		}


		public bool GetCustomFiles(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			RMAdminSettings objAdm=null; //Application layer component		
			try
			{
                objAdm = new RMAdminSettings(RMConfigurationSettings.GetSessionDSN(base.ClientId), this.connectionString, base.ClientId);
				p_objXmlOut=objAdm.GetCustomFiles();

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                //dvatsa
				p_objErrOut.Add(p_objException, Globalization.GetString("RMAdminSettingsAdaptor.GetCustomFiles.Error", base.ClientId),//rkaur27
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objAdm=null;
			}
		}


		public bool SetCustomFile(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			RMAdminSettings objAdm=null; //Application layer component			
			XmlElement objElm=null;

			try
			{
				objElm=(XmlElement)p_objXmlIn.SelectSingleNode("//file");
				if(objElm==null)
					throw new RMAppException(Globalization.GetString("RMAdminSettingsAdaptor.MissingFileName", base.ClientId));//rkaur27
                //dvatsa
                objAdm = new RMAdminSettings(RMConfigurationSettings.GetSessionDSN(base.ClientId), this.connectionString, base.ClientId);
				objAdm.SetCustomFile(objElm.Attributes["filename"].Value,objElm.InnerText); 
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                //dvatsa
				p_objErrOut.Add(p_objException, Globalization.GetString("RMAdminSettingsAdaptor.SetCustomFile.Error", base.ClientId),//rkaur27
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objAdm=null;
			}
		}

		#endregion
	}
}
