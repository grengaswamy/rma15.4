using System;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: WorkCompAgentServiceAreaAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-May-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for WorkCompAgentServiceArea class  
	/// </summary>
	public class WorkCompAgentServiceAreaAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public WorkCompAgentServiceAreaAdaptor(){}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		///	<WorkCompAgentServiceArea><Jurisdictions></Jurisdictions><OfficeFunction></OfficeFunction><Agents></Agents></WorkCompAgentServiceArea>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			WorkCompAgentServiceArea objServiceArea=null;	
			try
			{
                objServiceArea = new WorkCompAgentServiceArea(userLogin.LoginName, connectionString, base.ClientId);
                objServiceArea.LanguageCode = base.userLogin.objUser.NlsCode; //Aman ML Change
				p_objXmlOut=objServiceArea.Get(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("WorkCompAgentServiceAreaAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objServiceArea=null;
			}
		}
		/// <summary>
		/// Retrieves the Zip information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		///	See Xml file named WorkCompAgentServiceArea.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool FindZip(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			WorkCompAgentServiceArea objServiceArea=null;	
			try
			{
                objServiceArea = new WorkCompAgentServiceArea(userLogin.LoginName, connectionString, base.ClientId);
                objServiceArea.LanguageCode = base.userLogin.objUser.NlsCode; //Aman ML Change
				p_objXmlOut=objServiceArea.FindZip(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("WorkCompAgentServiceAreaAdaptor.FindZip.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objServiceArea=null;
			}
		}
		/// <summary>
		/// Deletes the Agency information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		///	See Xml file named WorkCompAgentServiceArea.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool DeleteAgency(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			WorkCompAgentServiceArea objServiceArea=null;	
			try
			{
                objServiceArea = new WorkCompAgentServiceArea(userLogin.LoginName, connectionString, base.ClientId);
				objServiceArea.DeleteAgency(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("WorkCompAgentServiceAreaAdaptor.DeleteAgency.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objServiceArea=null;
			}
		}
		/// <summary>
		/// Adds Postal Codes information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		///	See Xml file named WorkCompAgentServiceArea.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool AddJurisPostalCodes(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			WorkCompAgentServiceArea objServiceArea=null;	
			try
			{
                objServiceArea = new WorkCompAgentServiceArea(userLogin.LoginName, connectionString, base.ClientId);
				objServiceArea.AddJurisPostalCodes(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("WorkCompAgentServiceAreaAdaptor.AddJurisPostalCodes.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objServiceArea=null;
			}
		}
		/// <summary>
		/// Deletes the Postal Codes information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		///	See Xml file named WorkCompAgentServiceArea.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool DeleteJurisPostalCodes(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			WorkCompAgentServiceArea objServiceArea=null;	
			try
			{
                objServiceArea = new WorkCompAgentServiceArea(userLogin.LoginName, connectionString, base.ClientId);
				objServiceArea.DeleteJurisPostalCodes(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("WorkCompAgentServiceAreaAdaptor.DeleteJurisPostalCodes.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objServiceArea=null;
			}
		}
		/// <summary>
		/// Saves the Agency/Agent information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		///	See Xml file named WorkCompAgentServiceArea.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			WorkCompAgentServiceArea objServiceArea=null;
			try
			{
                objServiceArea = new WorkCompAgentServiceArea(userLogin.LoginName, connectionString, base.ClientId);
				objServiceArea.Save(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("WorkCompAgentServiceAreaAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objServiceArea=null;
			}
		}
		/// <summary>
		/// Saves the Zip information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		///	See Xml file named WorkCompAgentServiceArea.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool SaveZips(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			WorkCompAgentServiceArea objServiceArea=null;
			try
			{
                objServiceArea = new WorkCompAgentServiceArea(userLogin.LoginName, connectionString, base.ClientId);
				objServiceArea.SaveZips(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("WorkCompAgentServiceAreaAdaptor.SaveZips.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objServiceArea=null;
			}
		}
		#endregion
	}
}
