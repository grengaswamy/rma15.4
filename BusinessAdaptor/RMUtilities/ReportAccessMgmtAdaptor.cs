using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: ReportAccessMgmtAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-May-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for ReportAccessMgmt class
	/// </summary>
	public class ReportAccessMgmtAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public ReportAccessMgmtAdaptor(){}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		/*
			<ReportsAccess>
				<Reports>
					<option value="2_314" ReportName="Executive Summary Report" ReportDesc="Not Specified" User="p (parag sarin)">Executive Summary Report - Not Specified</option>
				</Reports>
				<Filters>
					<option value="0">All Users</option>
					<option value="2">p (parag sarin)</option>
					<option value="3">t (test test)</option>
					<option value="4">m (manager manager)</option>
				</Filters>
			</ReportsAccess>
		*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ReportAccessMgmt objAccess=null;
            string sReportServerConnectString = string.Empty;
			try
			{

                sReportServerConnectString = RMConfigurationManager.GetConnectionString("ReportServer_ConnectionString", base.ClientId); //mbahl3 JIRA [RMACLOUD-131] 
                objAccess = new ReportAccessMgmt(sReportServerConnectString, securityConnectionString, base.ClientId);			
				p_objXmlOut=objAccess.Get(p_objXmlIn);
				
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("ReportAccessMgmtAdaptor.Get.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objAccess=null;
			}
		}
		/// <summary>
		/// This function Saves Report Access data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		/*
			<ReportsAccess>
				<Reports>
					<option value="2_314" ReportName="Executive Summary Report" ReportDesc="Not Specified" User="p (parag sarin)">Executive Summary Report - Not Specified</option>
				</Reports>
				<Filters>
					<option value="0">All Users</option>
					<option value="2">p (parag sarin)</option>
					<option value="3">t (test test)</option>
					<option value="4">m (manager manager)</option>
				</Filters>
			</ReportsAccess>
		*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ReportAccessMgmt objAccess=null;
            string sReportServerConnectString = string.Empty;
			try
			{
                sReportServerConnectString = RMConfigurationManager.GetConnectionString("ReportServer_ConnectionString", base.ClientId); //mbahl3 JIRA [RMACLOUD-131] 
                objAccess = new ReportAccessMgmt(sReportServerConnectString, securityConnectionString, base.ClientId);			
				objAccess.Save(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("ReportAccessMgmtAdaptor.Save.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objAccess=null;
			}
		}
		#endregion
	}
}
