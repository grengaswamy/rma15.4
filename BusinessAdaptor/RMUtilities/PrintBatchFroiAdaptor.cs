﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;
using System.IO;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    ///************************************************************** 
    ///* $File		: PrintBatchFroiAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 02/22/2010
    ///* $Author	: Nadim Zafar
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///	This class is used to call the application layer component for RM Utilities
    ///	which implements the functionality of printing Batch Froi.
    /// </summary>
    public class PrintBatchFroiAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public PrintBatchFroiAdaptor() { }

        #endregion

        #region Public Methods

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.PrintBatchFroiAdaptor.Get() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.PrintBatchFroiAdaptor.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PrintBatchFroi objFroi = null; //Application layer component			
            try
            {
                objFroi = new PrintBatchFroi(connectionString, base.ClientId);
                p_objXmlOut = objFroi.Get(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintBatchFroiAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objFroi = null;
            }
        }

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.PrintBatchFroiAdaptor.GetPath() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.PrintBatchFroiAdaptor.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool GetPath(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PrintBatchFroi objFroi = null; //Application layer component			
            try
            {
                objFroi = new PrintBatchFroi(connectionString, base.ClientId);
                p_objXmlOut = objFroi.GetPath(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintBatchFroiAdaptor.GetPath.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objFroi = null;
            }
        }

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.PrintBatchFroiAdaptor.GetAcord() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.PrintBatchFroiAdaptor.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool GetAcord(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PrintBatchFroi objFroi = null; //Application layer component			
            try
            {
                objFroi = new PrintBatchFroi(connectionString, base.ClientId);
                p_objXmlOut = objFroi.GetAcord(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintBatchFroiAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objFroi = null;
            }
        }

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.PrintBatchFroiAdaptor.Save() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.PrintBatchFroiAdaptor.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PrintBatchFroi objFroi = null; //Application layer component			
            try
            {

                objFroi = new PrintBatchFroi(connectionString, base.ClientId);
                //objFroi = new PrintBatchFroi(connectionString, userLogin.LoginName);
                bool bStatus = false;
                p_objXmlOut = objFroi.Save(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintBatchFroiAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objFroi = null;
            }
        }
        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.PrintBatchFroiAdaptor.SaveAcord() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.PrintBatchFroiAdaptor.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool SaveAcord(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PrintBatchFroi objFroi = null; //Application layer component			
            try
            {

                objFroi = new PrintBatchFroi(connectionString, base.ClientId);
                //objFroi = new PrintBatchFroi(connectionString, userLogin.LoginName);
                bool bStatus = false;
                p_objXmlOut = objFroi.SaveAcord(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintBatchFroiAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objFroi = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetFroiAcordPDF(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objXmlFileName = null;
            XmlElement objXmlElement = null;
            string sPdfFileName = String.Empty;
            string sPDFFilePath = String.Empty;
            MemoryStream objMemoryStream = null;

            try
            {
                objXmlFileName = (XmlElement)p_objXmlIn.SelectSingleNode("//FileName");
                sPdfFileName = objXmlFileName.InnerText;
                sPDFFilePath = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, @"froi");
                sPDFFilePath += "\\" + sPdfFileName;

                objMemoryStream = CreateStream(sPDFFilePath);

                objXmlElement = p_objXmlOut.CreateElement("Froi_Acord");
                p_objXmlOut.AppendChild(objXmlElement);

                objXmlElement = p_objXmlOut.CreateElement("File");
                objXmlElement.InnerText = Convert.ToBase64String(objMemoryStream.ToArray());
                p_objXmlOut.FirstChild.AppendChild(objXmlElement);

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintBatchFroiAdaptor.GetFroiAcordPDF.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objXmlFileName = null;
                objXmlElement = null;
                if (objMemoryStream != null)
                {
                    objMemoryStream.Dispose();
                }
            }
        }

        public bool RemoveFileFromDisk(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objXmlFileName = null;
            XmlElement objXmlElement = null;
            string sPdfFileName = String.Empty;
            string sPDFFilePath = String.Empty;

            try
            {
                objXmlFileName = (XmlElement)p_objXmlIn.SelectSingleNode("//FileName");
                sPdfFileName = objXmlFileName.InnerText;
                sPDFFilePath = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, @"froi");
                sPDFFilePath += "\\" + sPdfFileName;

                File.Delete(sPDFFilePath);

                

                objXmlElement = p_objXmlOut.CreateElement("FroiAcord");
                p_objXmlOut.AppendChild(objXmlElement);

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintBatchFroiAdaptor.RemoveFileFromDisk.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objXmlFileName = null;
                objXmlElement = null;
            }
        }

        /// <summary>
        /// Creates a memory stream of given file
        /// </summary>
        /// <param name="p_sFilePath">Complete File Path</param>
        /// <returns>Memory stream of the given file</returns>
        public  MemoryStream CreateStream(string p_sFilePath)
        {
            MemoryStream objMemoryStream = null;
            FileStream objFileStream = null;
            BinaryReader objBinaryReader = null;
            Byte[] arrByte = null;

            try
            {
                if (!File.Exists(p_sFilePath))
                    throw new FileNotFoundException(Globalization.GetString("Function.FileNotFound.Error", base.ClientId));
                objFileStream = new FileStream(p_sFilePath, FileMode.Open, FileAccess.Read);
                objMemoryStream = new MemoryStream((int)objFileStream.Length);
                objBinaryReader = new BinaryReader(objFileStream);
                arrByte = objBinaryReader.ReadBytes((int)objFileStream.Length);
                objMemoryStream.Write(arrByte, 0, (int)objFileStream.Length);
                return (objMemoryStream);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Function.CreateStream.Error", base.ClientId), p_objEx);
            }
            finally
            {
                if (objFileStream != null)
                {
                    objFileStream.Close();
                    objFileStream.Dispose();
                }
                if (objBinaryReader != null)
                {
                    objBinaryReader.Close();
                    objBinaryReader = null;
                }
                arrByte = null;
                if (objMemoryStream != null)
                {
                    objMemoryStream.Close();
                    objMemoryStream.Dispose();
                }
            }
        }
        #endregion
    }
}