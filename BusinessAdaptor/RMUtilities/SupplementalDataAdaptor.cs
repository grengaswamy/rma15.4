using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: SupplementalDataAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 30/05/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of Supplemental data.
	/// </summary>
	public class SupplementalDataAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public SupplementalDataAdaptor(){}

		#endregion

		#region Public Methods

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.SupplementalData.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.SupplementalData.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			SupplementalData objSupp=null; //Application layer component			
			try
			{
                objSupp = new SupplementalData(connectionString, base.ClientId);
				p_objXmlOut=objSupp.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("SupplementalDataAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objSupp = null;
			}
		}
		#endregion
	}
}