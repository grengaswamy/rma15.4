using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: BRSModifierValueAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 26/05/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of BRS Modifier Value.
	/// </summary>
	public class BRSModifierValueAdaptor :  BusinessAdaptorBase
	{
		/// <summary>
		/// Default Constructor
		/// </summary>
		public BRSModifierValueAdaptor(){}

		#region Public Methods

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.BRSModifierValue.New() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.BRSModifierValue.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool New(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			BRSModifierValue objBRSMod=null; //Application layer component			
			try
			{
				objBRSMod = new BRSModifierValue(connectionString,base.ClientId);
				p_objXmlOut=objBRSMod.New(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("BRSModifierValueAdaptor.New.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objBRSMod = null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.BRSModifierValue.Save() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.BRSModifierValue.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			BRSModifierValue objBRSMod=null; //Application layer component			
			try
			{
				objBRSMod = new BRSModifierValue(connectionString,base.ClientId);
				p_objXmlOut=objBRSMod.Save(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("BRSModifierValueAdaptor.Save.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objBRSMod = null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.BRSModifierValue.Delete() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.BRSModifierValue.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			BRSModifierValue objBRSMod=null; //Application layer component			
			try
			{
				objBRSMod = new BRSModifierValue(connectionString,base.ClientId);
				p_objXmlOut=objBRSMod.Delete(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("BRSModifierValueAdaptor.Delete.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objBRSMod = null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.BRSModifierValue.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.BRSModifierValue.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			BRSModifierValue  objMod=null; //Application layer component			
			try
			{
				objMod = new BRSModifierValue(connectionString,base.ClientId);
				p_objXmlOut=objMod.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("BRSModifierValueAdaptor.Get.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objMod = null;
			}
		}

		#endregion
	}
}