using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: ClaimTypeVsAcordFormSetupAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-May-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for ClaimTypeVsAcordFormSetup class
	/// </summary>
	public class ClaimTypeVsAcordFormSetupAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public ClaimTypeVsAcordFormSetupAdaptor(){}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		/*	<ClaimTypeVsAcordFormSetup>
				<control name="ACORDForm" type="combobox" title="ACORD Form">
				<option value=""></option>
				<option value="9">Automobile Loss Notice</option>
				<option value="10">General Liability Notice of Occurrence/Claim</option>
				<option value="8">Property Loss Notice</option>
				</control>
				<control name="ClaimType" type="combobox" title="Claim Type">
				<option value="">
					</option>
					<option value="55">BI - Bodily Injury</option>
				</control>
				<control name="AcordClaimTypeList" type="radiolist" radio="0">
				<listhead>
				<rowhead colname="AcordFormCol">ACORD Form</rowhead>
				<rowhead colname="ClaimTypeCol">Claim Type</rowhead>
				</listhead>
				<listrow>
				<rowtext type="label" name="AcordValue" title="Property Loss Notice" />
				<rowtext type="label" name="ClaimValue" title="BI - Bodily Injury" />
				</listrow>
				</control>
			</ClaimTypeVsAcordFormSetup>
		*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ClaimTypeVsAcordFormSetup objFormSetup=null;			
			try
			{
                objFormSetup = new ClaimTypeVsAcordFormSetup(connectionString, userLogin.LoginName, base.ClientId);
                objFormSetup.LanguageCode = base.userLogin.objUser.NlsCode; //Aman ML Change
				p_objXmlOut=objFormSetup.Get();
				
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("ClaimTypeVsAcordFormSetupAdaptor.Get.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objFormSetup=null;
			}
		}
		/// <summary>
		/// This function Saves acord and claim type setup data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		/*	<ClaimTypeVsAcordFormSetup>
				<control name="ACORDForm" type="combobox" title="ACORD Form">
				<option value=""></option>
				<option value="9">Automobile Loss Notice</option>
				<option value="10">General Liability Notice of Occurrence/Claim</option>
				<option value="8">Property Loss Notice</option>
				</control>
				<control name="ClaimType" type="combobox" title="Claim Type">
				<option value="">
					</option>
					<option value="55">BI - Bodily Injury</option>
				</control>
				<control name="AcordClaimTypeList" type="radiolist" radio="0">
				<listhead>
				<rowhead colname="AcordFormCol">ACORD Form</rowhead>
				<rowhead colname="ClaimTypeCol">Claim Type</rowhead>
				</listhead>
				<listrow>
				<rowtext type="label" name="AcordValue" title="Property Loss Notice" />
				<rowtext type="label" name="ClaimValue" title="BI - Bodily Injury" />
				</listrow>
				</control>
			</ClaimTypeVsAcordFormSetup>
		*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool DeleteMapping(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ClaimTypeVsAcordFormSetup objFormSetup=null;				
			try
			{

                objFormSetup = new ClaimTypeVsAcordFormSetup(connectionString, userLogin.LoginName, base.ClientId);			
				objFormSetup.DeleteMapping(p_objXmlIn);
				
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ClaimTypeVsAcordFormSetupAdaptor.DeleteMapping.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objFormSetup=null;
			}
		}
		/// <summary>
		/// This function deletes acord and claim type mapping data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		/*	<ClaimTypeVsAcordFormSetup>
				<control name="ACORDForm" type="combobox" title="ACORD Form">
				<option value=""></option>
				<option value="9">Automobile Loss Notice</option>
				<option value="10">General Liability Notice of Occurrence/Claim</option>
				<option value="8">Property Loss Notice</option>
				</control>
				<control name="ClaimType" type="combobox" title="Claim Type">
				<option value="">
					</option>
					<option value="55">BI - Bodily Injury</option>
				</control>
				<control name="AcordClaimTypeList" type="radiolist" radio="0">
				<listhead>
				<rowhead colname="AcordFormCol">ACORD Form</rowhead>
				<rowhead colname="ClaimTypeCol">Claim Type</rowhead>
				</listhead>
				<listrow>
				<rowtext type="label" name="AcordValue" title="Property Loss Notice" />
				<rowtext type="label" name="ClaimValue" title="BI - Bodily Injury" />
				</listrow>
				</control>
			</ClaimTypeVsAcordFormSetup>
		*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ClaimTypeVsAcordFormSetup objFormSetup=null;				
			try
			{

                objFormSetup = new ClaimTypeVsAcordFormSetup(connectionString, userLogin.LoginName, base.ClientId);			
				objFormSetup.Save(p_objXmlIn);
				
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ClaimTypeVsAcordFormSetupAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objFormSetup=null;
			}
		}
		#endregion
	}
}
