﻿


using System;
using System.Data;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;
using Riskmaster.Settings;
using Riskmaster.Security;
using System.Collections;
using Riskmaster.Application.SecurityManagement;


namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    ///************************************************************** 
    ///* $File		: RMXPortalCustomizationAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 08/15/2006
    ///* $Author	: Raman
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///	This class is used to call the application layer component for RM Utilities
    ///	which implements the Acrosoft Pre-Fill Folder Strucure
    /// </summary>
    public class RMXPortalCustomizationAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public RMXPortalCustomizationAdaptor() { }

        #endregion

        #region Public Function
        
        /// <summary>
        ///		rsolanki2: This function gets the user list & group list
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml</param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool GetUserList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {            
            DataSet objGroupsAndUsers;

            //logDebug("RMXPortalCustomizationAdaptor.GetUserList", "starting getUserList");
            RMXPortalCustomization objRMXPortalCustomization = null;            
            try
            {
                // rsolanki2 (19-march 08): transforming the xml using xslCompiledTransform to bring it into 
                // our preferred zapatec tree friendly format. 
                
                // The same could have been done via a processor at Orbeon layer.
                // However, as per benchmarks XslCompiledTransform (.net 2.0) about 25 times faster than 
                // the saxon xslt engine used by orbeon
                // The xslt is precomiled into a assembly using the xsltc.exe utility which comes along VS 2008

                
                //logDebug("RMXPortalCustomizationAdaptor.GetUserList", "creating CustomizeUserList object: dsn=" + userLogin.objRiskmasterDatabase.DataSourceId.ToString() + "  :securityConnectionString=" + securityConnectionString);
                objRMXPortalCustomization = new RMXPortalCustomization(connectionString, userLogin.objRiskmasterDatabase.DataSourceId.ToString(), securityConnectionString, userLogin.UserId,base.ClientId);

                //logDebug("RMXPortalCustomizationAdaptor.GetUserList", "calling CustomizeUserList.getUserList");
                objGroupsAndUsers = objRMXPortalCustomization.GetUserList();
                //logDebug("RMXPortalCustomizationAdaptor.GetUserList", "UserList retrived..calling xsl transform");

                MemoryStream objXmlStreamForGroupsAndUsers = new MemoryStream();
                //XmlWriter objXmlWriterForGroupsAndUsers = new XmlTextWriter(objXmlStreamForGroupsAndUsers, System.Text.Encoding.UTF8);
                objGroupsAndUsers.WriteXml(objXmlStreamForGroupsAndUsers);
                objXmlStreamForGroupsAndUsers.Seek(0, SeekOrigin.Begin);
                p_objXmlOut.Load(objXmlStreamForGroupsAndUsers);

                XslCompiledTransform xslt = new XslCompiledTransform();
                string sPath = RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, @"frameset\RmxPortalGroupAndUsersTransform.xsl");
                xslt.Load(sPath);
                //xslt.Load(typeof(RmxPortalTransform));                
                XmlDocument docToReturn = new XmlDocument();
                MemoryStream objXmlStream = new MemoryStream();
                XmlWriter objXmlWriter = new XmlTextWriter(objXmlStream, System.Text.Encoding.UTF8);
                xslt.Transform(p_objXmlOut, null, objXmlStream);
                objXmlStream.Seek(0, SeekOrigin.Begin);
                docToReturn.Load(objXmlStream);

                p_objXmlOut = docToReturn;

                objGroupsAndUsers.Dispose();
                objXmlStreamForGroupsAndUsers.Dispose();
                objXmlStream.Dispose(); 
                //logDebug("RMXPortalCustomizationAdaptor.GetUserList", "xsl transform completed..exiting funtion");
                
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("RMXPortalCustomizationAdaptor.GetUserList.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }            
            return true;
            
        }
      
        private void logDebug(string sAdaptorMethod, string p_sXmlSring)
        {
            XmlDocument xmlRequest = new XmlDocument();
            xmlRequest.Load(new StringReader("<debug></debug>"));
            XmlElement elem = xmlRequest.CreateElement("text");
            elem.InnerText = p_sXmlSring;
            xmlRequest.DocumentElement.AppendChild(elem);
         
            string sCall = sAdaptorMethod;            
            sCall += " (Debug logging)";

            // Log the overall call failure and the input envelope (only if call failed)
        
            LogItem oLogItem = new LogItem();
            oLogItem.EventId = 0;   // Don't have a meaningful value for this yet
            oLogItem.Category = "CommonWebServiceLog";
            oLogItem.RMParamList.Add("Adaptor Call/Result", sCall);
            oLogItem.Message = sAdaptorMethod + " call debugging.";
            if (xmlRequest != null)
                try
                {
                    oLogItem.RMParamList.Add("XML Input Envelope", xmlRequest.OuterXml);
                }
                catch (Exception)
                {

                    oLogItem.RMParamList.Add("XML Input Envelope", "xmlRequest contents could not be logged - likely too large for available memory.");
                }
            Log.Write(oLogItem, base.ClientId);
        }

        #endregion


    }
}
