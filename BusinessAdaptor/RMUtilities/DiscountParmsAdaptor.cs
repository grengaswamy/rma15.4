using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: DiscountParmsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 31-May-2005
	///* $Author	: Anurag Agarwal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for DiscountParms class  
	/// </summary>
	public class DiscountParmsAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public DiscountParmsAdaptor()
		{
		}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///			<Document>
		///				<discount>
		///					<discountname></discountname>
		///					<lob></lob>
		///					<state></state>
		///					<discounttype></discounttype>
		///					<amount></amount>
		///					<expirationdate></expirationdate>
		///					<effectivedate></effectivedate>
		///					<discountrowid>4</discountrowid>
		///				</discount>					
		///			</Document>
		///	</param>
		/// <param name="p_objXmlOut">
		///		Result as Output xml Output structure is as follows-: 
		///			<Document>
		///				<discount>
		///					<discountname></discountname>
		///					<lob></lob>
		///					<state></state>
		///					<discounttype></discounttype>
		///					<amount></amount>
		///					<expirationdate></expirationdate>
		///					<effectivedate></effectivedate>
		///					<discountrowid>4</discountrowid>
		///					<discountrangelist>
		///						<discountrange>
		///							<addedbyuser></addedbyuser>
		///							<dttmrcdadded></dttmrcdadded>
		///							<updatedbyuser></updatedbyuser>
		///							<dttmrcdlastupd></dttmrcdlastupd>
		///							<amount></amount>
		///							<beginningrange></beginningrange>
		///							<discountrangerowid></discountrangerowid>
		///							<endrange></endrange>
		///						</discountrange>
		///					</discountrangelist>
		///				</discount>
		///			</Document>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			XmlElement objElement=null;					//used for parsing the input xml
			DiscountParms objDiscountParms=null;	

			try
			{
				//check existence of input xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//discount");
				if (objElement==null)
				{
                    p_objErrOut.Add("DiscountParmsAdaptor.Get.Error", Globalization.GetString("DiscountParmsAdaptor.InputXMLMissing", base.ClientId), BusinessAdaptorErrorType.Warning);
					return false;
				}

                objDiscountParms = new DiscountParms(userLogin, loginName, connectionString, base.ClientId);//vkumar258 ML changes		
				p_objXmlOut=objDiscountParms.LoadData(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DiscountParmsAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objDiscountParms=null;
			}
		}

		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///			<Document>
		///				<discount>
		///					<discountname></discountname>
		///					<lob></lob>
		///					<state></state>
		///					<discounttype></discounttype>
		///					<amount></amount>
		///					<expirationdate></expirationdate>
		///					<effectivedate></effectivedate>
		///					<discountrowid>4</discountrowid>
		///				</discount>
		///			</Document>
		///	</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			XmlElement objElement=null;					//used for parsing the input xml
			DiscountParms objDiscountParms=null;	

			try
			{
				//check existence of input xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//discount");
				if (objElement==null)
				{
                    p_objErrOut.Add("DiscountParmsAdaptor.Get.Error", Globalization.GetString("DiscountParmsAdaptor.InputXMLMissing", base.ClientId), BusinessAdaptorErrorType.Warning);
					return false;
				}

                objDiscountParms = new DiscountParms(loginName, connectionString, base.ClientId);			
				objDiscountParms.Save(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DiscountParmsAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objDiscountParms=null;
			}
		}

		/// <summary>
		/// This function Deletes Discount Range List
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///			<Document>
		///				<discountrangerowid></discountrangerowid>
		///			</Document>
		///	</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Delete(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			XmlElement objElement=null;					//used for parsing the input xml
			DiscountParms objDiscountParms=null;	
			int RowID = 0;

			try
			{
				//check existence of input xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//discountrangerowid");
				if (objElement==null)
				{
                    p_objErrOut.Add("DiscountParmsAdaptor.Get.Error", Globalization.GetString("DiscountParmsAdaptor.InputXMLMissing", base.ClientId), BusinessAdaptorErrorType.Warning);
					return false;
				}
				RowID = Conversion.ConvertStrToInteger(objElement.InnerText);
                objDiscountParms = new DiscountParms(loginName, connectionString, base.ClientId);
				
				return objDiscountParms.Delete(RowID);
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DiscountParmsAdaptor.Delete.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objDiscountParms=null;
			}
		}

        // Pankaj Volume Discount Implementation
        public bool UseVoulmeDiscount(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            DiscountParms objDiscountParms = null;
            try
            {
                objDiscountParms = new DiscountParms(loginName, connectionString, base.ClientId);	
                p_objXmlOut = objDiscountParms.VolumeDiscount(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DiscountParmsAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objDiscountParms = null;
            }
        }

		#endregion
	}
}
