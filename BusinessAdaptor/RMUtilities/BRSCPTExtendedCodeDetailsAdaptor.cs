using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: BRSCPTExtendedCodeDetailsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24/05/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of BRS CPT Extended Code Details.
	/// </summary>
	public class BRSCPTExtendedCodeDetailsAdaptor :  BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public BRSCPTExtendedCodeDetailsAdaptor(){}

		#endregion

		#region Public Methods

		/// <summary>
		/// This method is a wrapper to Riskmaster.Application.RMUtilities.BRSCPTExtendedCodeDetailsAdaptor.Get() method.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document</param>
		/// <param name="p_objXmlOut">XML containing the results.The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.BRSCPTExtendedCodeDetails.xml</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			BRSCPTExtendedCodeDetails objBRSCPT=null; //Application layer component			
			try
			{
				objBRSCPT = new BRSCPTExtendedCodeDetails(connectionString,base.ClientId);
				//get the data
				p_objXmlOut=objBRSCPT.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("BRSCPTExtendedCodeDetailsAdaptor.Get.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objBRSCPT = null;
			}
		}

		/// <summary>
		/// This method is a wrapper to Riskmaster.Application.RMUtilities.BRSCPTExtendedCodeDetailsAdaptor.Save() method.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document</param>
		/// <param name="p_objXmlOut">XML containing the results.The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.BRSCPTExtendedCodeDetails.xml</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for success or failure of the function</returns>
		public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			BRSCPTExtendedCodeDetails objBRSCPT=null; //Application layer component			
			try
			{
				objBRSCPT = new BRSCPTExtendedCodeDetails(connectionString,base.ClientId);
				//get the data
				p_objXmlOut=objBRSCPT.Save(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("BRSCPTExtendedCodeDetailsAdaptor.Save.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objBRSCPT = null;
			}
		}

		/// <summary>
		/// This method is a wrapper to Riskmaster.Application.RMUtilities.BRSCPTExtendedCodeDetailsAdaptor.New() method.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document</param>
		/// <param name="p_objXmlOut">XML containing the results.The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.BRSCPTExtendedCodeDetails.xml</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for success or failure of the function</returns>
		public bool New(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			BRSCPTExtendedCodeDetails objBRSCPT=null; //Application layer component			
			try
			{
				objBRSCPT = new BRSCPTExtendedCodeDetails(connectionString,base.ClientId);
				//get the data
				p_objXmlOut=objBRSCPT.New(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException, Globalization.GetString("BRSCPTExtendedCodeDetailsAdaptor.New.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objBRSCPT = null;
			}
		}
        
		#endregion
	}
}