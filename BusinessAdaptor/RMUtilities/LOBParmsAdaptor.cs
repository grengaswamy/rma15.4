using System;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: LOBParmsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-May-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for LOCParms class  
	/// </summary>
	public class LOBParmsAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public LOBParmsAdaptor(){}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	See Xml file named LOCParms.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			LOCParms objParms=null;	
			try
			{
				objParms=new LOCParms(connectionString, base.ClientId);//rkaur27
                objParms.LanguageCode = base.userLogin.objUser.NlsCode; //Aman MITS 31626
				p_objXmlOut=objParms.Get(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("LOBParmsAdaptor.Get.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objParms=null;
			}
		}
		/// <summary>
		/// Saves the LOB information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	See Xml file named LOCParms.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			LOCParms objParms=null;	
			try
			{
                //gagnihotri MITS 11995 Changes made for Audit table
				//objParms=new LOCParms(connectionString);
                objParms = new LOCParms(connectionString, userLogin.LoginName, base.ClientId);//rkaur27
                //start:Added by Nitin Goel,MITS 30910 changes done for National Interstate
                bool bValidate = false;

                //objParms.Save(p_objXmlIn);
                bValidate = Validate(p_objXmlIn, ref p_objErrOut);
                
                if (bValidate)                
                {
                    objParms.Save(p_objXmlIn);
                }              
				//return true;
                return bValidate;
                //end:added by nitin goel
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("LOBParmsAdaptor.Save.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objParms=null;
			}
		}
        /// <summary>
        /// Added by Nitin goel, MITS 30910,01/28/2013
        /// </summary>
        /// <param name="p_objXmlIn"></param>        
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        private bool Validate(XmlDocument p_objXmlIn, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sLobCode = string.Empty;
            XmlNode objNode = null;
            bool bValidateFail = false;
            try
            {
                //start:Added by Nitin Goel,MITS 30910 changes done for National Interstate
               
                objNode = p_objXmlIn.SelectSingleNode(".//AllowToApplyDedPayments");
                if (objNode != null)
                {
                    //Start -  Changed by Nikhil on 09/24/2014.Code review changes
                    // if (objNode.InnerText.ToLower().Equals("true"))
                    if (string.Compare(objNode.InnerText.ToString(), "true", true) == 0)
                    //End -  Changed by Nikhil on 09/24/2014.Code review changes
                    {
                        objNode = p_objXmlIn.SelectSingleNode("//systemcontrol[@name='DedRecReserveType']");
                        if (objNode != null && objNode.Attributes["codeid"] != null)
                        {
                            //Start -  Changed by Nikhil on 09/24/2014.Code review changes
                            // if (objNode.Attributes["codeid"].Value == "0" || string.IsNullOrEmpty(objNode.Attributes["codeid"].Value))
                            if (string.Compare(objNode.Attributes["codeid"].Value.ToString(), "0") == 0 || string.IsNullOrEmpty(objNode.Attributes["codeid"].Value))
                            //END -  Changed by Nikhil on 09/24/2014.Code review changes
                            {
                                bValidateFail = true;
                            }
                        }
                        objNode = p_objXmlIn.SelectSingleNode("//systemcontrol[@name='DedRecTransactionType']");
                        if (objNode != null && objNode.Attributes["codeid"] != null)
                        {
                            //Start -  Changed by Nikhil on 09/24/2014.Code review changes
                            //if (objNode.Attributes["codeid"].Value == "0" || string.IsNullOrEmpty(objNode.Attributes["codeid"].Value))
                            if (string.Compare(objNode.Attributes["codeid"].Value.ToString(), "0") == 0 || string.IsNullOrEmpty(objNode.Attributes["codeid"].Value))
                            //END -  Changed by Nikhil on 09/24/2014.Code review changes
                            {
                                bValidateFail = true;
                            }
                        }

                        //Start: Added by Sumit Agarwal for NI: 10/13/2014
                        objNode = p_objXmlIn.SelectSingleNode("//DedRecoveryIdentifierChar");
                        if (objNode != null)
                        {
                            if (objNode.InnerText.Trim().Length == 0)
                            {
                                bValidateFail = true;
                            }
                        }
                        //End: Added by Sumit Agarwal for NI: 10/13/2014

                        objNode = p_objXmlIn.SelectSingleNode("//ManualDedRecoveryIdentifierChar");
                        if (objNode != null)
                        {
                            if (objNode.InnerText.Trim().Length == 0)
                            {
                                bValidateFail = true;
                            }
                        }
                        //tanwar2 - mits 30910 - start

                        objNode = p_objXmlIn.SelectSingleNode("//LineOfBusiness");
                        if (objNode != null)
                        {
                            sLobCode = objNode.InnerText;
                        }
                    }
                    if (bValidateFail)
                    {
                        switch (sLobCode)
                        {
                            case "241":
                            case "243":
                                p_objErrOut.Add(new RMAppException(Globalization.GetString("LOBParmsAdaptor.DeductibleSettings.Error", base.ClientId)), BusinessAdaptorErrorType.Error);
                                break;
                        }
                        return false;
                    }
                }
                //end:added by nitin goel             
                return true;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            //Start -  Changed by Nikhil on 09/24/2014.Code review changes
            finally
            {
                objNode = null;
            }
            //END -  Changed by Nikhil on 09/24/2014.Code review changes

        }
		/// <summary>
		/// Saves the Claim Type Reserve information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	See Xml file named LOCParms.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool UpdateSysClaimTypeReserve(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			LOCParms objParms=null;	
			try
			{
                //gagnihotri MITS 11995 Changes made for Audit table
				//objParms=new LOCParms(connectionString);
                objParms = new LOCParms(connectionString,userLogin.LoginName, base.ClientId);//rkaur27
				objParms.UpdateSysClaimTypeReserve(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("LOBParmsAdaptor.UpdateSysClaimTypeReserve.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objParms=null;
			}
		}
		/// <summary>
		/// Saves the LOB information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	See Xml file named LOCParms.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool SaveDupNoOfDays(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			LOCParms objParms=null;	
			try
			{
                //gagnihotri MITS 11995 Changes made for Audit table
				//objParms=new LOCParms(connectionString);
                objParms = new LOCParms(connectionString, userLogin.LoginName, base.ClientId);//rkaur27
				p_objXmlOut=objParms.SaveDupNoOfDays(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("LOBParmsAdaptor.SaveDupNoOfDays.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objParms=null;
			}
		}
        //RMA-12047
        /// <summary>
        /// SaveDupPolNoOfDays
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool SaveDupPolNoOfDays(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            LOCParms objParms = null;
            try
            {
                objParms = new LOCParms(connectionString, userLogin.LoginName, base.ClientId);
                bool bReturnStatus = objParms.SaveDupPolNoOfDays(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("LOBParmsAdaptor.SaveDupNoOfDays.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objParms = null;
            }
        }
        //RMA-12047
		/// <summary>
		/// Retrieves the LOB information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	See Xml file named LOCParms.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetReserveType(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			LOCParms objParms=null;	
			try
			{
				objParms=new LOCParms(connectionString, base.ClientId);//rkaur27
				p_objXmlOut=objParms.GetReserveType(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("LOBParmsAdaptor.GetReserveType.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objParms=null;
			}
		}
		#endregion
	}
}
