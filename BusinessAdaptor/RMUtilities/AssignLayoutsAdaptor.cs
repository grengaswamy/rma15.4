using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: AssignLayoutsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-May-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for AssignLayouts class
	/// </summary>
	public class AssignLayoutsAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public AssignLayoutsAdaptor(){}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///<LayoutUsers>
		///		<TopDownUsers>
		///			<option value="u2">sarin parag (p)</option>
		///		</TopDownUsers>
		///		<TabLayoutUsers>
		///			<option value="u3">test test (test)</option>
		///		</TabLayoutUsers>
		///</LayoutUsers>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			AssignLayouts objLayouts=null;			
			try
			{
				objLayouts=new AssignLayouts(connectionString,userLogin.objRiskmasterDatabase.DataSourceId.ToString(),
                    securityConnectionString, base.ClientId);			
				
				p_objXmlOut=objLayouts.Get();
				
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("AssignLayoutsAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objLayouts=null;
			}
		}
		/// <summary>
		/// This function Saves Layout data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///<LayoutUsers>
		///		<TopDownUsers>
		///			<option value="u2">sarin parag (p)</option>
		///		</TopDownUsers>
		///		<TabLayoutUsers>
		///			<option value="u3">test test (test)</option>
		///		</TabLayoutUsers>
		///</LayoutUsers>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			AssignLayouts objLayouts=null;			
			try
			{
				objLayouts=new AssignLayouts(connectionString,userLogin.objRiskmasterDatabase.DataSourceId.ToString(),
                    securityConnectionString, base.ClientId);			
				
				objLayouts.Save(p_objXmlIn);
				
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("AssignLayoutsAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objLayouts=null;
			}
		}
		#endregion
	}
}
