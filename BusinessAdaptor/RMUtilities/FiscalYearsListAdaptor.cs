using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: FiscalYearsListAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-May-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for FiscalYearsList class
	/// </summary>
	public class FiscalYearsListAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public FiscalYearsListAdaptor(){}
		#endregion

		#region Public methods

		/// <summary>
		/// This function returns xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		///<FiscalYearsList><LineOfBusiness></LineOfBusiness><Organization></Organization></FiscalYearsList>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FiscalYearsList objFiscalList=null;		
			try
			{
                //objFiscalList = new FiscalYearsList(connectionString);
                objFiscalList=new FiscalYearsList(connectionString, base.ClientId);
                objFiscalList.LanguageCode = base.userLogin.objUser.NlsCode;
				p_objXmlOut=objFiscalList.Get(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("FiscalYearsListAdaptor.Get.Error",base.ClientId),BusinessAdaptorErrorType.Error);//sonali-cloud
				return false;
			}
			finally
			{
				objFiscalList=null;
			}
		}
		/// <summary>
		/// This function returns report
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<FiscalYearList>
		///		<Report>
		///		</Report>
		///	</FiscalYearList>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool PrintFiscalYearList(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FiscalYearsList objFiscalList=null;	
			XmlNode objNode=null;
			XmlElement objTempElement=null;
			MemoryStream objMemory=null;
			try
			{
                //objFiscalList = new FiscalYearsList(connectionString);
                objFiscalList = new FiscalYearsList(connectionString, base.ClientId);
                objFiscalList.LanguageCode = base.userLogin.objUser.NlsCode;
				objMemory=objFiscalList.PrintFiscalYearList(p_objXmlIn);
				objNode=p_objXmlOut.CreateElement("FiscalYearList");
				p_objXmlOut.AppendChild(objNode);
				objTempElement=p_objXmlOut.CreateElement("Report");
				objTempElement.InnerText=Convert.ToBase64String(objMemory.ToArray());
				p_objXmlOut.FirstChild.AppendChild(objTempElement);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("FiscalYearsListAdaptor.PrintFiscalYearList.Error",base.ClientId),BusinessAdaptorErrorType.Error);//sonali-cloud
				return false;
			}
			finally
			{
				objFiscalList=null;
				objMemory=null;
			}
		}
		/// <summary>
		/// This function returns xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		/*
			<form name=" FiscalYearsList" title="Fiscal Years List" topbuttons="1" supp="">
			<toolbar>
				<button type="new" title="New" />
				<button type="save" title="Save" />
				<button type="delete" title="Delete Record" />
				<button type="recordsummary" title="Record Summary" />
			</toolbar>
			<group name="FiscalYears" title="Fiscal Years List">
				<displaycolumn>
				<control name="RowId" type="id">
				</control>
				<control name="LineOfBusiness" type="code" firstfield="1" codetable="LINE_OF_BUSINESS" title="Line Of Business" maxlength="50">
				</control>
				<control name="AllLOB" type="button" title="All LOB">
				</control>
				</displaycolumn>
				<displaycolumn>
				<control name="Organization" type="code" firstfield="1" codetable="LINE_OF_BUSINESS" title="Organization" maxlength="50">
				</control>
				<control name="AllOrg" type="button" title="All Org">
				</control>
				</displaycolumn>
				<displaycolumn>
				<control name="TableName" type="radiolist">
					<listhead>
					<rowhead colname="FYear">
					</rowhead>
					<rowhead colname="FiscalYear">Fiscal Year</rowhead>
					<rowhead colname="StartDate">Start Date</rowhead>
					<rowhead colname="EndDate">End Date</rowhead>
					<rowhead colname="Periods">Periods</rowhead>
					<rowhead colname="LOB">Line of Business</rowhead>
					<rowhead colname="Organization">Organization</rowhead>
					</listhead>
					<listrow>
					<rowtext type="radio" name="radiolist0" value="2009" title="" />
					<rowtext type="label" name="WCBenefitType" title="2009" />
					<rowtext type="label" name="StartDate" title="4/1/2009" />
					<rowtext type="label" name="EndDate" title="3/31/2010" />
					<rowtext type="label" name="Periods" title="12" />
					<rowtext type="label" name="LOB" title="All Lines" />
					<rowtext type="label" name="Organization" title="Entire Organization" />
					</listrow>
				</control>
				</displaycolumn>
			</group>
			</form>
		*/
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetLobOrg(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FiscalYearsList objFiscalList=null;	
			try
			{
                //objFiscalList = new FiscalYearsList(connectionString);
                objFiscalList = new FiscalYearsList(connectionString, base.ClientId);
                objFiscalList.LanguageCode = base.userLogin.objUser.NlsCode;
				p_objXmlOut=objFiscalList.GetLobOrg(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("FiscalYearsListAdaptor.GetLobOrg.Error",base.ClientId),BusinessAdaptorErrorType.Error);//sonali-cloud
				return false;
			}
			finally
			{
				objFiscalList=null;
			}
		}

		/// <summary>
		/// This function deletes a fiscal year
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		///<FiscalYearsList><RowId></RowId><LineOfBusiness></LineOfBusiness><Organization></Organization></FiscalYearsList>	
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool DeleteFiscalYear(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FiscalYearsList objFiscalList=null;		
			try
			{
                //objFiscalList = new FiscalYearsList(connectionString);
                objFiscalList = new FiscalYearsList(connectionString, base.ClientId);
                objFiscalList.LanguageCode = base.userLogin.objUser.NlsCode;
				objFiscalList.DeleteFiscalYear(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("FiscalYearsListAdaptor.DeleteFiscalYear.Error",base.ClientId),BusinessAdaptorErrorType.Error);//sonali-cloud
				return false;
			}
			finally
			{
				objFiscalList=null;
			}
		}
		#endregion

	}
}
