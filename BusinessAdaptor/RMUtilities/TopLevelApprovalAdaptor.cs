using System;
using System.Xml;
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: TopLevelApprovalAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 02-June-2005
	///* $Author	: Anurag Agarwal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for TopLevelApproval class  
	/// </summary>
	public class TopLevelApprovalAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public TopLevelApprovalAdaptor()
		{
		}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///		<document>
		///			<RowId></RowId>
		///		</document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///		Output structure is as follows-:
		///		See Xml file named TopLevelApproval.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TopLevelApproval objTopLevelApproval=null;	

			try
			{

				objTopLevelApproval=new  TopLevelApproval(userLogin.UserId,userLogin.objRiskmasterDatabase.DataSourceId,connectionString, base.ClientId);
				p_objXmlOut=objTopLevelApproval.Get(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("TopLevelApprovalAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objTopLevelApproval=null;
//				objDOC=null;
			}
		}

		/// <summary>
		///		This function Saves Layout data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TopLevelApproval objTopLevelApproval=null;	
			try
			{
                //gagnihotri MITS 11995 Changes made for Audit table
				//objTopLevelApproval=new TopLevelApproval(userLogin.UserId,userLogin.objRiskmasterDatabase.DataSourceId,connectionString);
                objTopLevelApproval = new TopLevelApproval(userLogin.UserId, userLogin.objRiskmasterDatabase.DataSourceId, connectionString, userLogin.LoginName, base.ClientId);
				objTopLevelApproval.Save(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("TopLevelApprovalAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objTopLevelApproval=null;
			}
		}
		#endregion
	}
}


