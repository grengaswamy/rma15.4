﻿using System;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    public class PolicyBatchFileAdaptor : BusinessAdaptorBase
    {

        public PolicyBatchFileAdaptor() { }

        public bool GetPolicySystems(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
            PolicyBatchFile objPolicyBatchFile = null;	
			try
			{
                objPolicyBatchFile = new PolicyBatchFile(connectionString);
                p_objXmlOut = objPolicyBatchFile.GetPolicySystems(p_objXmlIn);
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PolicyBatchFileAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                objPolicyBatchFile = null;
			}
		}

        public bool CreateFile(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicyBatchFile objPolicyBatchFile = null;
            try
            {
                objPolicyBatchFile = new PolicyBatchFile(connectionString);
                p_objXmlOut = objPolicyBatchFile.CreateFile(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PolicyBatchFileAdaptor.WriteFile.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objPolicyBatchFile = null;
            }
        }

        public bool GetTransactions(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicyBatchFile objPolicyBatchFile = null;
            try
            {
                objPolicyBatchFile = new PolicyBatchFile(connectionString);
                p_objXmlOut = objPolicyBatchFile.GetTransactions(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PolicyBatchFileAdaptor.GetTransaction.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objPolicyBatchFile = null;
            }
        }
    }
}
