using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: FiscalCopyAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-May-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for FiscalCopy class
	/// </summary>
	public class FiscalCopyAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public FiscalCopyAdaptor(){}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		/*
		 <FiscalCopy>
			<FromLineOfBusiness Text="General Claims" Value="241">General Claims</FromLineOfBusiness>
			<FromOrganization Text="" Value="2484">department2</FromOrganization>
			<ToLineOfBusiness Text="" Value="241">GC General Claims</ToLineOfBusiness>
			<ToOrganization Text="Entire Organization" Value="0" />
			<FiscalYear>2009</FiscalYear>
			<OptAllYear>Only 2009</OptAllYear>
		</FiscalCopy>
		*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FiscalCopy objFiscal=null;			
			try
			{
                objFiscal = new FiscalCopy(connectionString, base.ClientId); //sonali-cloud
                objFiscal.LanguageCode = base.userLogin.objUser.NlsCode;
				p_objXmlOut=objFiscal.Get(p_objXmlIn);
				
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("FiscalCopyAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error); //sonali-cloud
				return false;
			}
			finally
			{
				objFiscal=null;
			}
		}
		/// <summary>
		/// This function Saves Layout data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		/*
		 <FiscalCopy>
			<FromLineOfBusiness Text="General Claims" Value="241">General Claims</FromLineOfBusiness>
			<FromOrganization Text="" Value="2484">department2</FromOrganization>
			<ToLineOfBusiness Text="" Value="241">GC General Claims</ToLineOfBusiness>
			<ToOrganization Text="Entire Organization" Value="0" />
			<FiscalYear>2009</FiscalYear>
			<OptAllYear>Only 2009</OptAllYear>
		</FiscalCopy>
		*/
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			FiscalCopy objFiscal=null;	
			string sError="";
			BusinessAdaptorError objSoftErr=null;
			try
			{
				objFiscal=new FiscalCopy(connectionString, base.ClientId); //sonali-cloud
                objFiscal.LanguageCode = base.userLogin.objUser.NlsCode;
				objFiscal.Save(p_objXmlIn,ref sError);
				if (!sError.Trim().Equals(""))
				{
					objSoftErr=new BusinessAdaptorError("FiscalCopySaveError",sError,BusinessAdaptorErrorType.Message);
					p_objErrOut.Add(objSoftErr);		
				}
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("FiscalCopyAdaptor.Save.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objFiscal=null;
				objSoftErr=null;
			}
		}
		#endregion
	}
}
