using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    ///************************************************************** 
    ///* $File		: BRSHospitalPerDiemValuesAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 10/4/2007
    ///* $Author	: Umesh P Kusvaha
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    /// <summary>	
    ///	This class is used to call the application layer component for RM Utilities
    ///	which implements the functionality to set the Per Diem's values .
    /// </summary>
    public class BRSHospitalPerDiemValuesAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public BRSHospitalPerDiemValuesAdaptor() { }

        #endregion

        #region Public Methods

        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.BRSHospitalPerDiemValue.Get() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.BRSModifierValues.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>
        public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            BRSHospitalPerDiemValue objBRSHos = null; //Application layer component			
            try
            {
                objBRSHos = new BRSHospitalPerDiemValue(connectionString,base.ClientId);
                p_objXmlOut = objBRSHos.Get(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("BRSHospitalPerDiemValuesAdaptor.Get.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objBRSHos = null;
            }
        }


        /// <summary>
        ///		This method is a wrapper to Riskmaster.Application.RMUtilities.BRSHospitalPerDiemValue.Save() method.		
        /// </summary>
        /// <param name="p_objXmlIn">Input XML document
        /// </param>
        /// <param name="p_objXmlOut">XML containing the results
        ///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.BRSModifierValue.xml
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>		
        /// <returns>True/False for success or failure of the function</returns>

        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            BRSHospitalPerDiemValue objBRSHos = null; 
            try
            {
                objBRSHos = new BRSHospitalPerDiemValue(connectionString,base.ClientId);
                p_objXmlOut = objBRSHos.Save(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("BRSHospitalPerDiemValue.Save.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objBRSHos = null;
            }
        }
        #endregion
    }
}