using System;
using System.Xml;
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: PVCreateAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 27-May-2005
	///* $Author	: Anurag Agarwal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for PVCreate class  
	/// </summary>
	public class PVCreateAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public PVCreateAdaptor()
		{
		}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///		<Document>
		///			<RowId></RowId>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///		Output structure is as follows-:
		///		See Xml file named PVCreate.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			PVCreate objPVCreate=null;	

			try
			{
                objPVCreate = new PVCreate(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, connectionString, userLogin.objRiskmasterDatabase.DataSourceId.ToString(), base.ClientId); //mbahl3 JIRA [RMACLOUD-123]
				p_objXmlOut=objPVCreate.Get(p_objXmlIn);

				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("PVCreateAdaptor.Get.Error",base.ClientId),BusinessAdaptorErrorType.Error); //mbahl3 JIRA [RMACLOUD-123]
				return false;
			}
			finally
			{
				objPVCreate=null;
			}
		}

		/// <summary>
		///		This function Saves Layout data
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			PVCreate objPVCreate=null;	
			try
			{
                objPVCreate = new PVCreate(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, connectionString, userLogin.objRiskmasterDatabase.DataSourceId.ToString(), base.ClientId); //mbahl3 JIRA [RMACLOUD-123]				
				objPVCreate.Save(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PVCreateAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error); //mbahl3 JIRA [RMACLOUD-123]
				return false;
			}
			finally
			{
				objPVCreate=null;
			}
		}

		/// <summary>
		/// This function deletes the Power View selected
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///		<Document>
		///			<RowId></RowId>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///		Output structure is as follows-:
		///		See Xml file named PVCreate.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Delete(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			PVCreate objPVCreate=null;	
			bool bSuccess = false;

			try
			{
                objPVCreate = new PVCreate(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, connectionString, userLogin.objRiskmasterDatabase.DataSourceId.ToString(), base.ClientId); //mbahl3 JIRA [RMACLOUD-123]
				bSuccess = objPVCreate.Delete(p_objXmlIn);

				return bSuccess;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PVCreateAdaptor.Delete.Error", base.ClientId), BusinessAdaptorErrorType.Error); //mbahl3 JIRA [RMACLOUD-123]
				return false;
			}
			finally
			{
				objPVCreate=null;
			}
		}
		

		/// <summary>
		/// This function clones the Power View selected
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///		<Document>
		///			<RowId></RowId>
		///			<ViewName></ViewName>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///		Output structure is as follows-:
		///		See Xml file named PVCreate.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Clone(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			PVCreate objPVCreate=null;	
			bool bSuccess = false;

			try
			{
                objPVCreate = new PVCreate(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, connectionString, userLogin.objRiskmasterDatabase.DataSourceId.ToString(), base.ClientId); //mbahl3 JIRA [RMACLOUD-123]
				bSuccess = objPVCreate.Clone(p_objXmlIn);

				return bSuccess;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PVCreateAdaptor.Clone.Error", base.ClientId), BusinessAdaptorErrorType.Error); //mbahl3 JIRA [RMACLOUD-123]
				return false;
			}
			finally
			{
				objPVCreate=null;
			}
		}
		
		#endregion
	}
}
