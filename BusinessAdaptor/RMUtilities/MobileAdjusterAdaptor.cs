﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;


namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    public class MobileAdjusterAdaptor : BusinessAdaptorBase
    {
        //akaur9 created this service and wrapper for Mobile Adjuster
        public MobileAdjusterAdaptor() { }



        public bool GetAdjusterRowId(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlNode objXmlNode = null;
            int iClaimId = 0;
            bool bIsPrimaryAdjuster = false;
            DbReader objReader = null;
            DbReader objReaderEntity = null;
            XmlElement objXmlElement = null;
            XmlElement objAdjuster = null;
            XmlElement objRowChild = null;
            string sClaimNumber = string.Empty;
            string sLastName = string.Empty;
            string sFirstName = string.Empty;
            int iRowId = 0;
            int iAdjusterEid = 0;
            if (p_objXmlIn.SelectSingleNode("//caller") != null && ((p_objXmlIn.SelectSingleNode("//caller").InnerText.Equals("MobileAdjuster") || p_objXmlIn.SelectSingleNode("//caller").InnerText.Equals("MobilityAdjuster"))))
            {
                objXmlNode = p_objXmlIn.SelectSingleNode("//ClaimNumber");
                if (objXmlNode != null)
                {
                    sClaimNumber = Conversion.ConvertObjToStr(objXmlNode.InnerText);
                    iClaimId = GetClaimIdForMobileAdj(sClaimNumber);

                }
                if (iClaimId == 0 || iClaimId == -1)
                {
                    throw new Riskmaster.ExceptionTypes.CollectionException(String.Format(Globalization.GetString("MobileAdjusterAdaptor.GetClaimId.Error", base.ClientId), BusinessAdaptorErrorType.Error));
                }
                objXmlNode = p_objXmlIn.SelectSingleNode("//AdjusterEID");
                if (objXmlNode != null)
                {
                    iAdjusterEid = Conversion.ConvertObjToInt(objXmlNode.InnerText, base.ClientId);
                }
                objXmlNode = p_objXmlIn.SelectSingleNode("//CurrentAdjFlag");
                if (objXmlNode != null)
                {
                    bIsPrimaryAdjuster = Conversion.ConvertObjToBool(objXmlNode.InnerText, base.ClientId);
                }
                if (iClaimId != 0 && bIsPrimaryAdjuster == true)
                {
                    try
                    {
                        string sSQL = "SELECT * FROM CLAIM_ADJUSTER WHERE CLAIM_ID = " + iClaimId + " AND CURRENT_ADJ_FLAG = -1";

                        objReader = DbFactory.ExecuteReader(this.connectionString, sSQL);
                        objXmlElement = p_objXmlOut.CreateElement("Adjusters");
                        if (objReader.Read())
                        {
                            //adjuster row id
                            iRowId = Conversion.ConvertObjToInt(objReader.GetValue("ADJ_ROW_ID"), base.ClientId);
                            objAdjuster = p_objXmlOut.CreateElement("Adjuster");

                            objRowChild = p_objXmlOut.CreateElement("AdjusterRowId");
                            objRowChild.InnerText = iRowId + "";
                            objAdjuster.AppendChild(objRowChild);

                            //Entity Id
                            iRowId = Conversion.ConvertObjToInt(objReader.GetValue("ADJUSTER_EID"), base.ClientId);
                            objRowChild = p_objXmlOut.CreateElement("EntityId");
                            objRowChild.InnerText = iRowId + "";
                            objAdjuster.AppendChild(objRowChild);

                            //Name of the Adjuster
                            string sSQLEntity = "SELECT LAST_NAME,FIRST_NAME FROM ENTITY WHERE ENTITY_ID = " + iRowId;

                            objReaderEntity = DbFactory.ExecuteReader(this.connectionString, sSQLEntity);
                            if (objReaderEntity.Read())
                            {
                                sLastName = Conversion.ConvertObjToStr(objReaderEntity.GetValue("LAST_NAME"));
                                sFirstName = Conversion.ConvertObjToStr(objReaderEntity.GetValue("FIRST_NAME"));
                                objRowChild = p_objXmlOut.CreateElement("PrimaryAdjusterName");
                                objRowChild.InnerText = sLastName + " " + sFirstName;
                                objAdjuster.AppendChild(objRowChild);

                            }

                            if (iAdjusterEid != 0)
                            {
                                sSQLEntity = "SELECT DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD,UPDATED_BY_USER, ADDED_BY_USER FROM ENTITY WHERE ENTITY_ID = " + iAdjusterEid;
                                objReaderEntity = DbFactory.ExecuteReader(this.connectionString, sSQLEntity);
                                if (objReaderEntity.Read())
                                {
                                    objRowChild = p_objXmlOut.CreateElement("DttmRcdAdded");
                                    objRowChild.InnerText = Conversion.ConvertObjToStr(objReaderEntity.GetValue("DTTM_RCD_ADDED"));
                                    objAdjuster.AppendChild(objRowChild);

                                    //Date and Time Record Last Updated
                                    objRowChild = p_objXmlOut.CreateElement("DttmRcdLastUpd");
                                    objRowChild.InnerText = Conversion.ConvertObjToStr(objReaderEntity.GetValue("DTTM_RCD_LAST_UPD"));
                                    objAdjuster.AppendChild(objRowChild);

                                    //Updated by User
                                    objRowChild = p_objXmlOut.CreateElement("UpdatedByUser");
                                    objRowChild.InnerText = Conversion.ConvertObjToStr(objReaderEntity.GetValue("UPDATED_BY_USER"));
                                    objAdjuster.AppendChild(objRowChild);

                                    //Added by user
                                    objRowChild = p_objXmlOut.CreateElement("AddedByUser");
                                    objRowChild.InnerText = Conversion.ConvertObjToStr(objReaderEntity.GetValue("ADDED_BY_USER"));
                                    objAdjuster.AppendChild(objRowChild);
                                }
                                objXmlElement.AppendChild(objAdjuster);
                            }
                        }
                        p_objXmlOut.AppendChild(objXmlElement);
                        objReader.Close();
                    }
                    catch (RMAppException p_objException)
                    {
                        p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                        return false;
                    }
                    catch (Exception p_objException)
                    {
                        p_objErrOut.Add(p_objException, Globalization.GetString("MobileAdjusterAdaptor.GetAdjusterRowId.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                        return false;
                    }
                    finally
                    {
                        objReader = null;
                        objReaderEntity = null;
                        objXmlElement = null;
                        objAdjuster = null;
                        objRowChild = null;

                    }
                }
            }
            return true;
        }

        public int GetClaimIdForMobileAdj(string p_sClaimNumber)
        {
            DbReader objReader = null;
            int iClaimId = 0;
            if (!p_sClaimNumber.Equals(""))
            {
                try
                {
                    string sSQL = "SELECT CLAIM_ID from CLAIM where CLAIM_NUMBER =  '" + p_sClaimNumber.Trim() + "'";

                    objReader = DbFactory.ExecuteReader(this.connectionString, sSQL);
                    if (objReader.Read())
                    {
                        iClaimId = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), base.ClientId);
                    }

                    objReader.Close();
                    return iClaimId;
                }
                catch (Exception p_objException)
                {
                    //p_objErrOut.Add(p_objException, Globalization.GetString("MobileAdjusterAdaptor.GetClaimId.Error"), BusinessAdaptorErrorType.Error);
                    return iClaimId;
                }

                finally
                {
                    objReader = null;
                }

            }
            return iClaimId;

        }

    }
}
