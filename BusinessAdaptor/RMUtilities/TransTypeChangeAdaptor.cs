using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities;  

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: TransTypeChangeAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 31/05/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of Trans Type Change form.
	/// </summary>
	public class TransTypeChangeAdaptor :  BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public TransTypeChangeAdaptor(){}

		#endregion

		#region Public Methods

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.TransTypeChange.Get() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.TransTypeChange.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Get(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			TransTypeChange objTrans=null; //Application layer component			
			try
			{
                objTrans = new TransTypeChange(userLogin, connectionString, base.ClientId);//vkumar258 RMA-6037
                objTrans.LanguageCode = base.userLogin.objUser.NlsCode; //Aman ML Change
				p_objXmlOut=objTrans.Get(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("TransTypeChangeAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 109
				return false;
			}
			finally
			{
				objTrans=null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.RMUtilities.TransTypeChange.Save() method.		
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.TransTypeChange.xml
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			TransTypeChange objTrans=null; //Application layer component			
			try
			{
                objTrans = new TransTypeChange(userLogin, connectionString, base.ClientId);//vkumar258 RMA-6037
				p_objXmlOut=objTrans.Save(p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("TransTypeChangeAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);//psharma206 jira 109
				return false;
			}
			finally
			{
				objTrans = null;
			}
		}

		#endregion
	}
}
