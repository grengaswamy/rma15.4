using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;


namespace Riskmaster.BusinessAdaptor.RMUtilities
{
    ///************************************************************** 
    ///* $File		: SupplementalGridParameterSetupAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 27-Aug-2007
    ///* $Author	: Shivendu Shekhar
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************

    /// <summary>	
    ///	This class is used to call the application layer component for SupplementalGridParameterSetup class  
    /// </summary>
    public class SupplementalGridParameterSetupAdaptor:BusinessAdaptorBase
    {
        #region Constructor
		public SupplementalGridParameterSetupAdaptor()
		{
		}
		#endregion
        #region Public Function
		/// <summary>
		/// This function gets the Grid Parameters
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
            SupplementalGridParameterSetup objSuppGrid = null;

            try
            {
                objSuppGrid = new SupplementalGridParameterSetup(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, connectionString, base.ClientId);
                p_objXmlOut = objSuppGrid.Get(p_objXmlIn);

                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SupplementalGridParameterSetupAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objSuppGrid = null;
            }
		}
        /// <summary>
        /// This function saves the Grid Parameters
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml</param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            SupplementalGridParameterSetup objSuppGrid = null;
            

            try
            {
                objSuppGrid = new SupplementalGridParameterSetup(userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, connectionString, base.ClientId);
                objSuppGrid.Save(p_objXmlIn);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SupplementalGridParameterSetupAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objSuppGrid = null;
            }
        }
		#endregion
	
    }
}
