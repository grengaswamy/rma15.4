using System;
using System.Text;
using System.Xml;
using System.IO;
using System.Security;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common; 

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: EarlyPayDiscountsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-May-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for EarlyPayDiscounts class  
	/// </summary>
	public class EarlyPayDiscountsAdaptor:BusinessAdaptorBase
	{
		public EarlyPayDiscountsAdaptor(){}
		
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<EarlyPayDiscounts><RowId>1</RowId></EarlyPayDiscounts>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			EarlyPayDiscounts objDiscounts=null;	
			try
			{
                //vsharma205  RMA-11795 starts --  
                //parameter  m_userLogin added and paased to write Overloaded constructor for RMUtilities\EarlyPayDiscounts.cs for GetUIDate Function 
                //  objDiscounts = new EarlyPayDiscounts(userLogin.LoginName, connectionString, base.ClientId);   --- Obsolete code
                objDiscounts = new EarlyPayDiscounts(m_userLogin, userLogin.LoginName, connectionString, base.ClientId);
                //vsharma205  RMA-11795 ends 
				p_objXmlOut=objDiscounts.Get(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("EarlyPayDiscountsAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objDiscounts=null;
			}
		}
		/// <summary>
		/// Deletes the Claim type options information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	See Xml file named EarlyPayDiscounts.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Delete(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			EarlyPayDiscounts objDiscounts=null;
			try
			{
                objDiscounts = new EarlyPayDiscounts( userLogin.LoginName, connectionString, base.ClientId);
				objDiscounts.Delete(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("EarlyPayDiscountsAdaptor.Delete.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objDiscounts=null;
			}
		}
		/// <summary>
		/// Saves the claim type information
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Input structure is as follows-:
		///	See Xml file named EarlyPayDiscounts.xml in Xml folder
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			EarlyPayDiscounts objDiscounts=null;
            XmlNode objXmlNode = null;
			try
			{
                objDiscounts = new EarlyPayDiscounts(userLogin.LoginName, connectionString, base.ClientId);
				objDiscounts.Save(p_objXmlIn);
				p_objXmlIn.SelectSingleNode("//control[@name='RowId']").InnerText=objDiscounts.KeyFieldValue;
                //Shruti for MITS 9268 starts
                objXmlNode = p_objXmlIn.SelectSingleNode("//SaveMessage");
                if (objXmlNode != null)
                    objXmlNode.InnerText = "Saved";
                //Shruti for MITS 9268 ends
				p_objXmlOut=p_objXmlIn;
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("EarlyPayDiscountsAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objDiscounts=null;
			}
		}
	}
}
