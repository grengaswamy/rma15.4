using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.RMUtilities; 
using System.IO; 

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: BRSImportScheduleAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 25/05/2005
	///* $Author	: Pankaj
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for RM Utilities
	///	which implements the functionality of BRS Import Schedule.
	/// </summary>
	public class BRSImportScheduleAdaptor :  BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public BRSImportScheduleAdaptor(){}

		#endregion

		#region Public Methods

		/// <summary>
		/// This method is a wrapper to Riskmaster.Application.RMUtilities.BRSImportSchedule.ImportSchedule() method.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML document</param>
		/// <param name="p_objXmlOut">XML containing the results.The structure of the output XML document would be as in  in Riskmaster.Application.RMUtilities.BRSImportSchedule.xml</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for success or failure of the function</returns>
		public bool ImportSchedule(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			BRSImportSchedule objBRSImp=null; //Application layer component
			try
			{
				objBRSImp = new BRSImportSchedule (connectionString,p_objXmlIn,base.ClientId);
				//get the data
				p_objXmlOut=objBRSImp.ImportSchedule();
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("BRSImportScheduleAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				if(objBRSImp != null)
				{
					objBRSImp.Dispose();
					objBRSImp=null;
				}
			}
		}
		public bool FileTransfer(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			BRSImportSchedule objBRSImp=null; //Application layer component
			XmlElement objElement=null;	
			string sFileContent = string.Empty;
			string sFileName = string.Empty;
			string strExt = string.Empty; 
			try
			{
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FileContent");
				sFileContent=objElement.InnerText;
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FileName");
				sFileName = objElement.InnerText;

				string [] arrsFileNameParts = null;
				if (sFileName != "")
				{
					arrsFileNameParts = sFileName.Split("\\".ToCharArray());
					foreach(string sFName in arrsFileNameParts)
					{
						if (sFName.IndexOf('.') > 0)
						{
							sFileName = sFName;
						}
					}					
				}

                objBRSImp = new BRSImportSchedule(connectionString, p_objXmlIn, base.ClientId);
				//Tranfer contents
				p_objXmlOut=objBRSImp.FileTranfer(sFileContent,sFileName);
				return true; 
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("BRSImportScheduleAdaptor.FileTransfer.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				if(objBRSImp != null)
				{
					objBRSImp.Dispose();
					objBRSImp=null;
				}
			}
		}

        /// <summary>
        /// DeleteFiles
        /// abisht MITS 10623 11/15/2007
        /// </summary>
        public bool DeleteFiles(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            BRSImportSchedule objBRSImp = null; //Application layer component
            try
            {
                objBRSImp = new BRSImportSchedule(connectionString, p_objXmlIn, base.ClientId);
                objBRSImp.DeleteFiles();
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("BRSImportScheduleAdaptor.DeleteFiles.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
			{
				if(objBRSImp != null)
				{
					objBRSImp.Dispose();
					objBRSImp=null;
				}
			}
        }
		#endregion
	}
}