using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: ExpRateParmsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 30-May-2005
	///* $Author	: Anurag Agarwal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for ExpRateParms class  
	/// </summary>
	public class ExpRateParmsAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public ExpRateParmsAdaptor()
		{
		}
		#endregion

		#region Public Function
		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///			<Document>
		///				<ExpRates>
		///					<amount></amount>
		///					<exposureid></exposureid>
		///					<effectivedate></effectivedate>
		///					<expirationdate></expirationdate>
		///					<expratetype></expratetype>
		///					<state></state>
		///					<fixedorprorate></fixedorprorate>
		///					<baserate></baserate>
		///					<expraterowid>4</expraterowid>
		///				</ExpRates>
		///			</Document>
		///	</param>
		/// <param name="p_objXmlOut">
		///		Result as Output xml Output structure is as follows-: 
		///			<Document>
		///				<ExpRates>
		///					<amount>23</amount>
		///					<exposureid>5266</exposureid>
		///					<effectivedate>20050428</effectivedate>
		///					<expirationdate>20050429</expirationdate>
		///					<expratetype>5238</expratetype>
		///					<state>5</state>
		///					<fixedorprorate>5236</fixedorprorate>
		///					<baserate>12</baserate>
		///					<expraterowid>4</expraterowid>
		///				</ExpRates>
		///			</Document>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			XmlElement objElement=null;					//used for parsing the input xml
			ExpRateParms objExpRateParms=null;	

			try
			{
				//check existence of input xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ExpRates");
				if (objElement==null)
				{
                    p_objErrOut.Add("ExpRateParmsAdaptor.Get.Error", Globalization.GetString("ExpRateParmsAdaptor.InputXMLMissing", base.ClientId), BusinessAdaptorErrorType.Warning);
					return false;
				}

                objExpRateParms = new ExpRateParms(userLogin, loginName, connectionString, base.ClientId);//vkumar258 ML changes	
				p_objXmlOut=objExpRateParms.LoadData(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ExpRateParmsAdaptor.Get.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objExpRateParms=null;
			}
		}

		/// <summary>
		/// This function xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///			<Document>
		///				<ExpRates>
		///					<amount>23</amount>
		///					<exposureid>5266</exposureid>
		///					<effectivedate>20050428</effectivedate>
		///					<expirationdate>20050429</expirationdate>
		///					<expratetype>5238</expratetype>
		///					<state>5</state>
		///					<fixedorprorate>5236</fixedorprorate>
		///					<baserate>12</baserate>
		///					<expraterowid>4</expraterowid> OR <expraterowid></expraterowid>(for new record)
		///				</ExpRates>
		///			</Document>
		///	</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Save(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			XmlElement objElement=null;					//used for parsing the input xml
			ExpRateParms objExpRateParms=null;	

			try
			{
				//check existence of input xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ExpRates");
				if (objElement==null)
				{
                    p_objErrOut.Add("ExpRateParmsAdaptor.Get.Error", Globalization.GetString("ExpRateParmsAdaptor.InputXMLMissing", base.ClientId), BusinessAdaptorErrorType.Warning);
					return false;
				}

                objExpRateParms = new ExpRateParms(loginName, connectionString, base.ClientId);			
				objExpRateParms.Save(p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
                // Start Naresh MITS 9560 Proper Error Messages were not coming while adding already added exposure
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.PopupMessage);
                // End Naresh MITS 9560 Proper Error Messages were not coming while adding already added exposure

				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ExpRateParmsAdaptor.Save.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objExpRateParms=null;
			}
		}


        /// <summary>
        /// This function Deletes Rate Range List
        /// </summary>
        /// <param name="p_objXmlIn">
        ///		Input parameters as xml
        ///			<Document>
        ///				<raterangerowid></raterangerowid>
        ///			</Document>
        ///	</param>
        /// <param name="p_objXmlOut">Result as Output xml</param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        /// Added bY Tushar MITS 18231
        public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            //used for parsing the input xml
            XmlElement objElement = null;					
            ExpRateParms objEXPRateParms = null;
            int RowID = 0;

            try
            {
                //check existence of input xml which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//raterangerowid");
                if (objElement == null)
                {
                    p_objErrOut.Add("ExpRateParmsAdaptor.Delete.Error", Globalization.GetString("ExpRateParmsAdaptor.InputXMLMissing", base.ClientId), BusinessAdaptorErrorType.Warning);
                    return false;
                }
                RowID = Conversion.ConvertStrToInteger(objElement.InnerText);
                objEXPRateParms = new ExpRateParms(loginName, connectionString, base.ClientId);

                return objEXPRateParms.DeleteRange (RowID);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ExpRateParmsAdaptor.Delete.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objEXPRateParms  = null;
            }
        }

		#endregion

	}
}
