﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
   public class LimitTrackingSetupAdaptor : BusinessAdaptorBase 
    {
       public bool LoadData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            LimitTrackingSetup objLimitTrackingSetup = null;
            try
            {
                objLimitTrackingSetup = new LimitTrackingSetup(connectionString, base.ClientId);
                p_objXmlOut = objLimitTrackingSetup.LoadData(p_objXmlIn);  
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("LimitTrackingSetupAdaptor.LoadData", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objLimitTrackingSetup = null;
            }
        }
       public bool SaveData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
       {
           LimitTrackingSetup objLimitTrackingSetup = null;
           try
           {
               objLimitTrackingSetup = new LimitTrackingSetup(connectionString, base.ClientId);
                objLimitTrackingSetup.SaveData(p_objXmlIn);  
               return true;

           }
           catch (RMAppException p_objException)
           {
               p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
               return false;
           }
           catch (Exception p_objException)
           {
               p_objErrOut.Add(p_objException, Globalization.GetString("LimitTrackingSetupAdaptor.SaveData", base.ClientId), BusinessAdaptorErrorType.Error);
               return false;
           }
           finally
           {
               objLimitTrackingSetup = null;
           }
       }
    }
}
