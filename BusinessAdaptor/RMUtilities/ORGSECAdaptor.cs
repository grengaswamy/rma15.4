using System;
using System.IO;
using System.Xml;
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;

namespace Riskmaster.BusinessAdaptor.RMUtilities
{
	///************************************************************** 
	///* $File		: ORGSECAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 27-May-2005
	///* $Author	: Anurag Agarwal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for ORGSEC class  
	/// </summary>
	public class ORGSECAdaptor:BusinessAdaptorBase
	{
		#region Constructor
		public ORGSECAdaptor()
		{
		}
		#endregion

		#region Public Function
		/// <summary>
		/// This function is used to execute a setup for BES
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool LoadOrgSet(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			XmlDocument objXmlDocument = null; 
			ORGSEC objORGSEC = null;	
			try
			{
				objXmlDocument = new XmlDocument();

                //using (objORGSEC = new ORGSEC(userLogin))
                using (objORGSEC = new ORGSEC(userLogin, base.ClientId))
                {
                    objORGSEC.AdminConnString = objORGSEC.GetNewConnString(p_objXmlIn).Trim();
                    if (objORGSEC.LoadOrgSet(p_objXmlIn))
                        p_objXmlOut = objORGSEC.LoadGroupList(p_objXmlIn);
                    else
                    {
                        if (!objORGSEC.IsAdmin && objORGSEC.GetShowOnlyRole)
                        {
                            objXmlDocument.LoadXml("<Document><IsAdmin>False</IsAdmin><ShowOnlyRole>True</ShowOnlyRole><AdminUserID>" + userLogin.objRiskmasterDatabase.RMUserId + "</AdminUserID><AdminPwd>" + userLogin.objRiskmasterDatabase.RMPassword + "</AdminPwd></Document>");
                            p_objXmlOut = objXmlDocument;
                        }
                        else if (!objORGSEC.IsAdmin)
                        {
                            objXmlDocument.LoadXml("<Document><IsAdmin>False</IsAdmin></Document>");
                            p_objXmlOut = objXmlDocument;
                        }
                    }
                }
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.SystemError);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ORGSECAdaptor.LoadOrgSet.Error", base.ClientId), BusinessAdaptorErrorType.SystemError);
				return false;
			}
			finally
			{
				objORGSEC=null;
			}
		}

		/// <summary>
		/// This function is used to execute a setup for BES
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool LoadOrgSetAdmin(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			XmlDocument objXmlDocument = null;
			ORGSEC objORGSEC = null;	
			try
			{
				objXmlDocument = new XmlDocument();

                //using (objORGSEC = new ORGSEC(userLogin))
                using (objORGSEC = new ORGSEC(userLogin, base.ClientId))
                {
                    objORGSEC.AdminConnString = objORGSEC.GetNewConnString(p_objXmlIn);
                    objORGSEC.GetBESOraRole = (p_objXmlIn.SelectSingleNode("//OracleRole").InnerText.Trim());
                    if (objORGSEC.LoadOrgSet(p_objXmlIn))
                        p_objXmlOut = objORGSEC.LoadGroupList(p_objXmlIn);
                    else if (!objORGSEC.IsAdmin)
                    {
                        objXmlDocument.LoadXml("<Document><IsAdmin>False</IsAdmin></Document>");
                        p_objXmlOut = objXmlDocument;
                    }
                }
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return true;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ORGSECAdaptor.LoadOrgSet.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return true;
			}
			finally
			{
				objORGSEC=null;
			}
		}

		/// <summary>
		/// This function is used to Load Group def
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///		<document>
		///			<RowId></RowId>
		///		</document>
		///	</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool LoadGroupDef(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ORGSEC objORGSEC=null;	

			try
			{
                //using (objORGSEC = new ORGSEC(userLogin))
                using (objORGSEC = new ORGSEC(userLogin, base.ClientId))
                {
                    objORGSEC.LanguageCode = base.userLogin.objUser.NlsCode; //tmalhotra3 ML Changes
                    p_objXmlOut = objORGSEC.LoadGroupDef(p_objXmlIn);                    
                }
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ORGSECAdaptor.LoadGroupDef.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objORGSEC=null;
			}
		}

		/// <summary>
		/// This function is used to save a new group
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool SaveGroupDef(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ORGSEC objORGSEC=null;	
            string sChkDefer = "";
			try
			{
                sChkDefer = p_objXmlIn.SelectSingleNode("//Defer").InnerText;
                //using (objORGSEC = new ORGSEC(userLogin, sChkDefer))
                using (objORGSEC = new ORGSEC(userLogin, sChkDefer, base.ClientId))
                {
                    objORGSEC.GetBESOraRole = (p_objXmlIn.SelectSingleNode("//OracleRole").InnerText.Trim());
                    objORGSEC.AdminConnString = objORGSEC.GetNewConnString(p_objXmlIn);
                    objORGSEC.SaveGroupDef(p_objXmlIn);
                }
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ORGSECAdaptor.SaveGroupDef.Error", base.ClientId), BusinessAdaptorErrorType.SystemError);
				return false;
			}
			finally
			{
				objORGSEC=null;
			}
		}

		/// <summary>
		/// This function is used to Update a Group
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool UpdateGroupDef(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ORGSEC objORGSEC=null;	
            string sChkDefer = "";
			try
			{
                sChkDefer = p_objXmlIn.SelectSingleNode("//Defer").InnerText;
                //using (objORGSEC = new ORGSEC(userLogin, sChkDefer))
                using (objORGSEC = new ORGSEC(userLogin, sChkDefer,base.ClientId))
                {
                    objORGSEC.GetBESOraRole = (p_objXmlIn.SelectSingleNode("//OracleRole").InnerText.Trim());
                    objORGSEC.AdminConnString = objORGSEC.GetNewConnString(p_objXmlIn);
                    objORGSEC.UpdateGroupDef(p_objXmlIn);
                }
				return true;
			}
			catch( RMAppException p_objException )
			{
                //nadim for 14334
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ORGSECAdaptor.UpdateGroupDef.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objORGSEC=null;
			}
		}
		
		/// <summary>
		/// This function is used to Delete a Group
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///		<document>
		///			<RowId></RowId>
		///		</document>
		///	</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool DeleteGroupDef(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ORGSEC objORGSEC=null;	
			int iGroupId = 0;
            string sChkDefer = "";

			try
			{
                //using (objORGSEC = new ORGSEC(userLogin, sChkDefer))
                using (objORGSEC = new ORGSEC(userLogin, sChkDefer, base.ClientId))
                {
                    objORGSEC.GetBESOraRole = (p_objXmlIn.SelectSingleNode("//OracleRole").InnerText.Trim());
                    iGroupId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//document/RowId").InnerText);
                    objORGSEC.AdminConnString = objORGSEC.GetNewConnString(p_objXmlIn);
                    objORGSEC.DeleteGroupDef(iGroupId);
                }

				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.SystemError);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ORGSECAdaptor.DeleteGroupDef.Error", base.ClientId), BusinessAdaptorErrorType.SystemError);
				return false;
			}
			finally
			{
				objORGSEC=null;
			}
		}
		
		/// <summary>
		/// This function is used to Load Group def
		/// </summary>
		/// <param name="p_objXmlIn">
		///		Input parameters as xml
		///		<document>
		///			<RowId></RowId>
		///		</document>
		///	</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool DisableOrgSec(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ORGSEC objORGSEC=null;	

			try
			{
                //using (objORGSEC = new ORGSEC(userLogin))
                using (objORGSEC = new ORGSEC(userLogin, base.ClientId))
                {
                    objORGSEC.AdminConnString = objORGSEC.GetNewConnString(p_objXmlIn);
                    objORGSEC.DisableOrgSec(p_objXmlIn);
                }
				return true;


			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.SystemError);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ORGSECAdaptor.DisableOrgSec.Error", base.ClientId), BusinessAdaptorErrorType.SystemError);
				return false;
			}
			finally
			{
				objORGSEC=null;
			}
		}

		/// <summary>
		/// This function is used to execute a setup for BES
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool IsOrgSetFirstTime(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ORGSEC objORGSEC = null;	
			XmlDocument objXmlDocument = null;
            string sDoc = string.Empty;  //Confidential Record
			try
			{
				objXmlDocument = new XmlDocument();
                //using (objORGSEC = new ORGSEC(userLogin))
                using (objORGSEC = new ORGSEC(userLogin, base.ClientId))
                {
                    //pmittal5 03/08/10 - Confidential Record
                    sDoc = "<Document>";
                    if (objORGSEC.IsOrgSetFirstTime())
                        //objXmlDocument.LoadXml("<Document><Flag>True</Flag></Document>");
                        sDoc = sDoc + "<Flag>True</Flag>";
                    else
                        //objXmlDocument.LoadXml("<Document><Flag>False</Flag></Document>");
                        sDoc = sDoc + "<Flag>False</Flag>";
                                        
                    if (objORGSEC.IsConfRecActivate())
                        sDoc = sDoc + "<ConfRec>-1</ConfRec>";
                    else
                        sDoc = sDoc + "<ConfRec>0</ConfRec>";
                    sDoc = sDoc + "</Document>";

                    objXmlDocument.LoadXml(sDoc); 
                    //End - pmittal5

                    p_objXmlOut = objXmlDocument;
                }
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ORGSECAdaptor.IsOrgSetFirstTime.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objORGSEC=null;
			}
		}
		/// <summary>
		/// This function is used to execute a setup for BES
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool PrintOrgSec(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ORGSEC objORGSEC = null;
			MemoryStream objMemory=null;
			XmlElement objTempElement=null;

			try
			{
                //using (objORGSEC = new ORGSEC(userLogin))
                using (objORGSEC = new ORGSEC(userLogin, base.ClientId))
                {
                    using (objMemory = objORGSEC.PrintOrgSec())
                    {
                        objTempElement = p_objXmlOut.CreateElement("OrgSecReport");
                        objTempElement.InnerText = Convert.ToBase64String(objMemory.ToArray());
                        p_objXmlOut.AppendChild(objTempElement);
                    }
                }
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ORGSECAdaptor.PrintOrgSec.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objORGSEC=null;
				objMemory=null;
				objTempElement=null;
			}
		}
		#endregion
	}
}

