using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.SupervisoryApproval;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Settings;
//rsushilaggar MITS 20606 05/05/2010
using System.Net.Mail;  //caggarwal4 merged for MITS 36562

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: SupervisorApprovalAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 18-Feb-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	///**************************************************************
	///* Amendment  -->
	///  1.Date		: 7 Feb 06 
	///    Desc		: Replacing RMO_ACCESS use, and changing params of permission
	///				  violation exception so it can be correctly called. 
	///    Author	: Sumit
	///**************************************************************
	/// <summary>	
	///	A class representing the Supervisory Approval Adaptor.
	/// </summary>
	public class SupervisorApprovalAdaptor:BusinessAdaptorBase
	{
		private const int RMB_FUNDS = 9500;
        private const int RMO_FUNDS_SUP_APPROVE_PAYMENTS = 1100;
        //pkandhari JIRA 8889 starts
        private const int ApprovePermission = 10601;
        private const int VoidPermission = 10602;
        private const int DenyPermission = 10603;
        //pkandhari JIRA 8889 ends
		

		#region Class Constructor

        /// <summary>
        /// Default class constructor
        /// </summary>
		public SupervisorApprovalAdaptor()
        {
        }
		#endregion

		#region Public Functions
		/// <summary>
		/// Nullify/Void checks
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		///		The structure of input XML document would be:
		///		<Document>
		///		 <SupervisorApproval>
		///			<TransIds>
		///				<TransId>TransId</TransId>
		///				<TransId>TransId</TransId>
		///				<TransId>TransId</TransId>
		///			</TransIds>
		///		 </SupervisorApproval>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool VoidSuperChecks(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			string sErrorXml="";
			SupervisorApproval objSupApprove=null;
			XmlDocument objInfoDoc=null;
			XmlNodeList objInfoList=null;
			XmlNodeList objTransIds=null;
			StringBuilder sbTransIds=null;
			
			BusinessAdaptorError objSoftErr=null;
			try
			{
                //Start by Shivendu for MITS 18518
                if (!userLogin.IsAllowedEx(RMB_FUNDS, RMPermissions.RMO_FUNDS_VOIDCHK))
                {
                    throw new PermissionViolationException(RMPermissions.RMO_FUNDS_VOIDCHK, RMB_FUNDS);
                }//if
                //End by Shivendu for MITS 18518
  

				objSupApprove=new SupervisorApproval(connectionString, userLogin.objRiskmasterDatabase.DataSourceName , userLogin.LoginName ,userLogin.Password, base.ClientId);
				//objTransIds=p_objXmlIn.SelectNodes("/Document/SupervisorApproval/TransIds/TransId");
				objTransIds=p_objXmlIn.GetElementsByTagName("TransId");

				sbTransIds=new StringBuilder();
				sbTransIds=sbTransIds.Append(objTransIds[0].InnerText);

				/*for (int i=0;i<objTransIds.Count;i++)
				{
					sbTransIds=sbTransIds.Append(objTransIds[i].InnerText+",");
				}
				if (sbTransIds.ToString().Length > 0)
				{
					sbTransIds.Remove(sbTransIds.Length-1,1);
				}*/
                //smahajan6: Safeway: Payment Supervisory Approval : Start
                objSupApprove.DSNID = base.DSNID;
                objSupApprove.UserId = userLogin.UserId;
                objSupApprove.ManagerId = userLogin.objUser.ManagerId;
                objSupApprove.SecureDSN = base.m_securityConnectionString;
                //start: rsushilaggar MITS 19970 05/21/2010
                if (p_objXmlIn.GetElementsByTagName("Reason") != null && p_objXmlIn.GetElementsByTagName("VoidReason").Count > 0)
                {
                    objSupApprove.Reason = p_objXmlIn.GetElementsByTagName("Reason")[0].InnerText;
                }
                
                if (p_objXmlIn.GetElementsByTagName("VoidReason") != null && p_objXmlIn.GetElementsByTagName("VoidReason").Count > 0)
                {
                    objSupApprove.VoidReason = p_objXmlIn.GetElementsByTagName("VoidReason")[0].InnerText;
                }
                if (p_objXmlIn.GetElementsByTagName("VoidReason_HTMLComments") != null && p_objXmlIn.GetElementsByTagName("VoidReason_HTMLComments").Count > 0)
                {
                    objSupApprove.VoidReason_HTMLComments = p_objXmlIn.GetElementsByTagName("VoidReason_HTMLComments")[0].InnerText;
                }
                //end: rsushilaggar MITS 19970 05/21/2010
                //smahajan6: Safeway: Payment Supervisory Approval : End
				objSupApprove.VoidSuperChecks(sbTransIds.ToString(),out sErrorXml);
				if (sErrorXml.Trim().Length > 0)
				{
					objInfoDoc=new XmlDocument();
					objInfoDoc.LoadXml(sErrorXml);
					objInfoList=objInfoDoc.SelectNodes("//Errors/Error");
					for (int i=0;i<objInfoList.Count;i++)
					{
						objSoftErr=new BusinessAdaptorError("SupervisoryApprovalVoidChecksMessage",objInfoList[i].InnerText,BusinessAdaptorErrorType.Message);
						p_objErrOut.Add(objSoftErr);		
					}
				}
			}
			catch( RMAppException p_objException )
			{
				//p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
                //nehagoel:02062014 : RMA4689 - mits 35272 : script were not getting called when checks are approved from here. 
                if (!string.IsNullOrWhiteSpace(p_objException.Message) && p_objException.Message.Contains("^"))
                {
                    string[] arrError = p_objException.Message.Split('^');
                    foreach (string s in arrError)
                    {
                        if (!string.IsNullOrWhiteSpace(s))
                            p_objErrOut.Add("ValidationScriptError", s, Common.BusinessAdaptorErrorType.Error);
                    }
                }
                else
                {
                    p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                }
                //End
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("SupervisorApprovalAdaptor.VoidSuperChecks.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
                if (objSupApprove != null)// null check added by shivendu for MITS 18518
                {
                    objSupApprove.Dispose();
                }// important to kill DataModelFactory
				objSupApprove=null;
				objInfoDoc=null;
				objInfoList=null;
				sbTransIds=null;
				objTransIds=null;
				objSoftErr=null;
			}
			return true;
		}
		/// <summary>
		/// Deny checks
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		///		The structure of input XML document would be:
		///		<Document>
		///		 <SupervisorApproval>
		///			<TransIds>
		///				<TransId>TransId</TransId>
		///				<TransId>TransId</TransId>
		///				<TransId>TransId</TransId>
		///			</TransIds>
		///		 </SupervisorApproval>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool DenySuperChecks(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			SupervisorApproval objSupApprove=null;
			XmlNodeList objTransIds=null;
			StringBuilder sbTransIds=null;
			
			try
			{
				objSupApprove=new SupervisorApproval(connectionString, userLogin.objRiskmasterDatabase.DataSourceName , userLogin.LoginName ,userLogin.Password, base.ClientId);
				//objTransIds=p_objXmlIn.SelectNodes("/Document/SupervisorApproval/TransIds/TransId");
				objTransIds=p_objXmlIn.GetElementsByTagName("TransId");

				sbTransIds=new StringBuilder();
				sbTransIds=sbTransIds.Append(objTransIds[0].InnerText);

				/*for (int i=0;i<objTransIds.Count;i++)
				{
					sbTransIds=sbTransIds.Append(objTransIds[i].InnerText+",");
				}
				if (sbTransIds.ToString().Length > 0)
				{
					sbTransIds.Remove(sbTransIds.Length-1,1);
				}*/
                //smahajan6: Safeway: Payment Supervisory Approval : Start
                objSupApprove.DSNID = base.DSNID;
                objSupApprove.UserId = userLogin.UserId;
                objSupApprove.ManagerId = userLogin.objUser.ManagerId;
                objSupApprove.SecureDSN = base.m_securityConnectionString;
                 //rsushilaggar MITS 19970 05/21/2010
                if (p_objXmlIn.GetElementsByTagName("Reason") != null && p_objXmlIn.GetElementsByTagName("Reason").Count > 0)
                {
                    objSupApprove.Reason = p_objXmlIn.GetElementsByTagName("Reason")[0].InnerText;
                }
                //smahajan6: Safeway: Payment Supervisory Approval : End
				objSupApprove.DenySuperChecks(sbTransIds.ToString());
                //rsharma220 MITS 36562
                if (!string.IsNullOrEmpty(objSupApprove.ErrorMsg))
                {
                    p_objErrOut.Add(new Exception(), objSupApprove.ErrorMsg, BusinessAdaptorErrorType.Error);
                    return false;
                } 
			}
			catch( RMAppException p_objException )
			{
				//p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
                //nehagoel:02062014 : RMA4689 - mits 35272 : script were not getting called when checks are approved from here. 
                if (!string.IsNullOrWhiteSpace(p_objException.Message) && p_objException.Message.Contains("^"))
                {
                    string[] arrError = p_objException.Message.Split('^');
                    foreach (string s in arrError)
                    {
                        if (!string.IsNullOrWhiteSpace(s))
                            p_objErrOut.Add("ValidationScriptError", s, Common.BusinessAdaptorErrorType.Error);
                    }
                }
                else
                {
                    p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                }
                //End
				return false;
			}
            //rsharma220 MITS 36562
            catch (SmtpException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("SupervisorApprovalAdaptor.DenySuperChecks.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objSupApprove.Dispose(); // important to kill DataModelFactory
				objSupApprove=null;
				sbTransIds=null;
				objTransIds=null;
			}
			return true;
		}
		/// <summary>
		/// Approve checks
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		///		The structure of input XML document would be:
		///		<Document>
		///		 <SupervisorApproval>
		///			<TransIds>
		///				<TransId>TransId</TransId>
		///				<TransId>TransId</TransId>
		///				<TransId>TransId</TransId>
		///			</TransIds>
		///		 </SupervisorApproval>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool ApproveSuperChecks(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			
			SupervisorApproval objSupApprove=null;
			XmlNodeList objTransIds=null;
			StringBuilder sbTransIds=null;
			
			try
			{
				objSupApprove=new SupervisorApproval(connectionString, userLogin.objRiskmasterDatabase.DataSourceName , userLogin.LoginName ,userLogin.Password, base.ClientId);
				//objTransIds=p_objXmlIn.SelectNodes("/Document/SupervisorApproval/TransIds/TransId");
				objTransIds=p_objXmlIn.GetElementsByTagName("TransId");

				sbTransIds=new StringBuilder();
				sbTransIds=sbTransIds.Append(objTransIds[0].InnerText);

				/*for (int i=0;i<objTransIds.Count;i++)
				{
					sbTransIds=sbTransIds.Append(objTransIds[i].InnerText+",");
				}
				if (sbTransIds.ToString().Length > 0)
				{
					sbTransIds.Remove(sbTransIds.Length-1,1);
				}*/
                //smahajan6: Safeway: Payment Supervisory Approval : Start
                objSupApprove.DSNID = base.DSNID;
                objSupApprove.UserId = userLogin.UserId;
                objSupApprove.ManagerId = userLogin.objUser.ManagerId;
                objSupApprove.SecureDSN = base.m_securityConnectionString;
                //rsushilaggar MITS 19970 05/21/2010
                if (p_objXmlIn.GetElementsByTagName("Reason") != null && p_objXmlIn.GetElementsByTagName("Reason").Count > 0)
                {
                    objSupApprove.Reason = p_objXmlIn.GetElementsByTagName("Reason")[0].InnerText;
                }
                //smahajan6: Safeway: Payment Supervisory Approval : End
                //Added by Amitosh For R8 enhancement of OverRide Authority check
                if (p_objXmlIn.GetElementsByTagName("OverRideAmount") != null && p_objXmlIn.GetElementsByTagName("OverRideAmount").Count > 0)
                {
                    objSupApprove.OverRideAmount =Conversion.ConvertStrToInteger(p_objXmlIn.GetElementsByTagName("OverRideAmount")[0].InnerText);
                }

                if (p_objXmlIn.GetElementsByTagName("ApplyOverRide") != null && p_objXmlIn.GetElementsByTagName("ApplyOverRide").Count > 0)
                {
                    objSupApprove.ApplyOverRide =Conversion.ConvertStrToBool(p_objXmlIn.GetElementsByTagName("ApplyOverRide")[0].InnerText);
                }
                //End Amitosh
                if (p_objXmlIn.GetElementsByTagName("ApplyOffSet") != null && p_objXmlIn.GetElementsByTagName("ApplyOffSet").Count > 0)  //mcapps2 MITS 30236
                {  //mcapps2 MITS 30236
                    objSupApprove.ApplyOffSet = Conversion.ConvertStrToBool(p_objXmlIn.GetElementsByTagName("ApplyOffSet")[0].InnerText);  //mcapps2 MITS 30236
                }  //mcapps2 MITS 30236
				objSupApprove.ApproveSuperChecks(sbTransIds.ToString());
                //rsharma220 MITS 36562
                if (!string.IsNullOrEmpty(objSupApprove.ErrorMsg))
                {
                    p_objErrOut.Add(new Exception(), objSupApprove.ErrorMsg, BusinessAdaptorErrorType.Error);
                    return false;
                } 
			}
			catch( RMAppException p_objException )
			{
				//p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
                //nehagoel:02062014 : RMA4689 - mits 35272 : script were not getting called when checks are approved from here. 
                if (!string.IsNullOrWhiteSpace(p_objException.Message) && p_objException.Message.Contains("^"))
                {
                    string[] arrError = p_objException.Message.Split('^');
                    foreach (string s in arrError)
                    {
                        if (!string.IsNullOrWhiteSpace(s))
                            p_objErrOut.Add("ValidationScriptError", s, Common.BusinessAdaptorErrorType.Error);
                    }
                }
                else
                { 
                    p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error); 
                }
                //End
				return false;
			}
            //rsharma220 MITS 36562
            catch (SmtpException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("SupervisorApprovalAdaptor.ApproveSuperChecks.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objSupApprove.Dispose(); // important to kill DataModelFactory
				objSupApprove=null;
				sbTransIds=null;
				objTransIds=null;
			}
			return true;
		}
		/// <summary>
		/// Create report for all transactions
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// Output structure is as follows-:
		/// <SupervisorApproval><File Filename=""></File></SupervisorApproval>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool CreateReportAllTransactions(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			
			SupervisorApproval objSupApprove=null;
			XmlDocument objAllTrans=null;
			XmlDocument objDenyTrans=null;
			XmlElement objTempElement=null;
			MemoryStream objMemory=null;
			string sError=null;
            XmlDocument objXMLDoc = null;
            bool bIsPayment = false;
			try
			{
				objSupApprove=new SupervisorApproval(connectionString, userLogin.objRiskmasterDatabase.DataSourceName , userLogin.LoginName ,userLogin.Password, base.ClientId);
				objSupApprove.SecureDSN=m_securityConnectionString;
				objSupApprove.ManagerId=userLogin.objUser.ManagerId;
				objSupApprove.UserId=userLogin.objUser.UserId;
				objSupApprove.UserName=userLogin.LoginName;
                //Start rsushilaggar 06/04/2010 MITS 20938
                XmlNode node = p_objXmlIn.SelectSingleNode("//Type");
                if (node != null && node.InnerText == "P")
                {
                    objSupApprove.sFilterSql = " AND FUNDS.PAYMENT_FLAG <> 0";
                    bIsPayment = true;

                }
                if (node != null && node.InnerText == "C")
                {
                    objSupApprove.sFilterSql = "AND FUNDS.COLLECTION_FLAG <> 0";
                }
                //end rsushilaggar
                objSupApprove.ListFundsTransactions(p_objXmlIn, out objAllTrans, out objDenyTrans, out sError);
                if (objAllTrans != null)
				{
                    //RMA-14719 achouhan3   Checked when No transaction is there
                    if (objAllTrans.SelectNodes("/FundsTransactions/Transaction") != null && objAllTrans.SelectNodes("/FundsTransactions/Transaction").Count > 0)
                    {
                        //Start rsushilaggar 06/04/2010 MITS 20938
                        objSupApprove.CreateReport(objAllTrans, out objMemory, bIsPayment);
                        objTempElement = p_objXmlOut.CreateElement("SupervisorApproval");
                        p_objXmlOut.AppendChild(objTempElement);
                        objTempElement = p_objXmlOut.CreateElement("File");
                        objTempElement.SetAttribute("Filename", Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".pdf"));
                        objTempElement.InnerText = Convert.ToBase64String(objMemory.ToArray());
                        p_objXmlOut.FirstChild.AppendChild(objTempElement);
                    }
                    else
                    {
                        p_objErrOut.Add("1", Globalization.GetString("SupervisorApprovalAdaptor.CreateReportAllTransactions.NoSupervisoryApproval", base.ClientId), BusinessAdaptorErrorType.Message);//rkaur27
                        objTempElement = p_objXmlOut.CreateElement("SupervisorApproval");
                        p_objXmlOut.AppendChild(objTempElement);
                    }
				}
				else
				{
					p_objErrOut.Add("1",Globalization.GetString("SupervisorApprovalAdaptor.CreateReportAllTransactions.NoSupervisoryApproval", base.ClientId),BusinessAdaptorErrorType.Message);//rkaur27
					objTempElement=p_objXmlOut.CreateElement("SupervisorApproval");
					p_objXmlOut.AppendChild(objTempElement);
				}
				
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("SupervisorApprovalAdaptor.CreateReportAllTransactions.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objSupApprove.Dispose(); // important to kill DataModelFactory
				objSupApprove=null;
				objMemory=null;
				objAllTrans=null;
				objDenyTrans=null;
				objTempElement = null;
			}
			return true;
		}
		/// <summary>
		/// Create report for deny only transactions
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// Output structure is as follows-:
		/// <SupervisorApproval><File Filename=""></File></SupervisorApproval>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool CreateReportDenyTransactions(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			
			SupervisorApproval objSupApprove=null;
			XmlDocument objAllTrans=null;
			XmlDocument objDenyTrans=null;
			XmlElement objTempElement=null;
			MemoryStream objMemory=null;
			string sError=null;
			
			try
			{
				objSupApprove=new SupervisorApproval(connectionString, userLogin.objRiskmasterDatabase.DataSourceName , userLogin.LoginName ,userLogin.Password, base.ClientId);
				objSupApprove.SecureDSN=m_securityConnectionString;
				objSupApprove.ManagerId=userLogin.objUser.ManagerId;
				objSupApprove.UserId=userLogin.objUser.UserId;
				objSupApprove.UserName=userLogin.LoginName;
                objSupApprove.ListFundsTransactions(p_objXmlIn, out objAllTrans, out objDenyTrans, out sError);
                //RMA-14719 achouhan3   Checked when No transaction is there
				if (objDenyTrans !=null)
				{
                     //RMA-14719 achouhan3   Checked when No transaction is there
                    if (objDenyTrans.SelectNodes("/FundsTransactions/Transaction") != null && objDenyTrans.SelectNodes("/FundsTransactions/Transaction").Count > 0)
                    {
                        //Start rsushilaggar 06/04/2010 MITS 20938
                        objSupApprove.CreateReport(objDenyTrans, out objMemory, true);
                        objTempElement = p_objXmlOut.CreateElement("SupervisorApproval");
                        p_objXmlOut.AppendChild(objTempElement);
                        objTempElement = p_objXmlOut.CreateElement("File");
                        objTempElement.SetAttribute("Filename", Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".pdf"));
                        objTempElement.InnerText = Convert.ToBase64String(objMemory.ToArray());
                        p_objXmlOut.FirstChild.AppendChild(objTempElement);
                    }
                    else
                    {
                        p_objErrOut.Add("1", Globalization.GetString("SupervisorApprovalAdaptor.CreateReportDenyTransactions.NoSupervisoryApproval", base.ClientId), BusinessAdaptorErrorType.Message);//rkaur27
                        objTempElement = p_objXmlOut.CreateElement("SupervisorApproval");
                        p_objXmlOut.AppendChild(objTempElement);
                    }
				}
				else
				{
					p_objErrOut.Add("1",Globalization.GetString("SupervisorApprovalAdaptor.CreateReportDenyTransactions.NoSupervisoryApproval", base.ClientId),BusinessAdaptorErrorType.Message);//rkaur27
					objTempElement=p_objXmlOut.CreateElement("SupervisorApproval");
					p_objXmlOut.AppendChild(objTempElement);
				}
				
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("SupervisorApprovalAdaptor.CreateReportDenyTransactions.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objSupApprove.Dispose(); // important to kill DataModelFactory
				objSupApprove=null;
				objMemory=null;
				objAllTrans=null;
				objDenyTrans=null;
				objTempElement = null;
			}
			return true;
		}
		/// <summary>
		/// Lists funds transactions
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// Output structure is as follows-:
		/// <SupervisorApproval>
		///		<AllTransactions>
		///			<Transaction>
		///				<CtlNumber></CtlNumber>
		///				<TransDate></TransDate>
		///				<PayeeName></PayeeName>
		///				<ClaimNumber></ClaimNumber>
		///				<PaymentAmount></PaymentAmount>
		///				<User></User>
		///				<TransactionType></TransactionType>
		///				<SplitAmount></SplitAmount>
		///				<FromToDate>  </FromToDate>
		///				<TransId></TransId>
		///				</Transaction>
		///		</AllTransactions>
		///	</SupervisorApproval>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool ListFundsTransactions(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			SupervisorApproval objSupApprove=null;
			XmlDocument objAllTrans=null;
			XmlDocument objDenyTrans=null;
			XmlDocument objInfoDoc=null;
			StringBuilder sbXmlBuilder=null;
			XmlNodeList objInfoList=null;
			string sError=null;
            //start: rsushilaggar 05/07/2010 MITS 20606
            string sShowLSSLink = "false";
            SysSettings objSettings = null;
            string sUseEntityApproval = "false";
            //start: rsushilaggar 05/07/2010 MITS 20606
            string sApproveTransSearch = "false";
            string sEnableVoidReason = "false";
            string sEnableOffset = "false";  //mcapps2 MITS 30236
            //end: rsushilaggar
            string s_Search = "";
            string sLangCode = string.Empty;
            //RMA-6404  achouhan3   Added to Support Angular Grid
            string sResponseType = String.Empty;
            string sGridId = String.Empty;
            string sPageName= String.Empty;
            //RMA-6404  achouhan3   Added to Support Angular Grid
			try
			{
				if (!userLogin.IsAllowedEx(RMB_FUNDS,RMO_FUNDS_SUP_APPROVE_PAYMENTS))
					throw new PermissionViolationException(RMPermissions.RMO_FUNDS_SUP_APPROVE_PAYMENTS,RMB_FUNDS);
                XmlNode LangCode = p_objXmlIn.SelectSingleNode("//LangCode");
                if (LangCode != null)
                    sLangCode = LangCode.InnerText;
                //RMA-6404      achouhan3    IAdded to Support Angular Grid
                XmlNode Content = p_objXmlIn.SelectSingleNode("//ContentType");
                if (Content != null)
                    sResponseType = Content.InnerText;
                XmlNode xGridID = p_objXmlIn.SelectSingleNode("//GridId");
                if (xGridID!=null)
                    sGridId = (p_objXmlIn.SelectSingleNode("//GridId").InnerText);
                XmlNode xPageName = p_objXmlIn.SelectSingleNode("//PageName");
                if(xPageName!=null)
                    sPageName = (p_objXmlIn.SelectSingleNode("//PageName").InnerText);
                //RMA-6404  achouhan3   Added to Support Angular Grid
				objSupApprove=new SupervisorApproval(connectionString, userLogin.objRiskmasterDatabase.DataSourceName , userLogin.LoginName ,userLogin.Password, base.ClientId);
				objSupApprove.SecureDSN=m_securityConnectionString;
				objSupApprove.ManagerId=userLogin.objUser.ManagerId;
				objSupApprove.UserId=userLogin.objUser.UserId;
                // akaushik5 Added for RMA-17649 Starts
                objSupApprove.GroupId = userLogin.GroupId;
                // akaushik5 Added for RMA-17649 Ends
				objSupApprove.UserName=userLogin.LoginName;
                //smahajan6: Safeway: Payment Supervisory Approval : Start
                objSupApprove.DSNID = base.DSNID;
                string sWarning = string.Empty;
                //Call New function to get Warning Message if Payment placed on Hold
                //RMA-6404  achouhan3   Modified to Support Angular Grid Starts
                if (String.Compare(sResponseType,"JSON",true)!=0)
                    objSupApprove.ListFundsTransactions(p_objXmlIn, out objAllTrans, out objDenyTrans, out sError, out sWarning, sLangCode);
                else
                    //RMA-13778 achouhan3   Added for diaplaying Error Message
                    objSupApprove.GetFundsTransactions(p_objXmlIn, out objAllTrans, out objDenyTrans, out sWarning, out sError, base.userLogin, sPageName, sGridId, sLangCode);
                //RMA-6404  achouhan3   Modified to Support Angular Grid Ends
                XmlNode Node = p_objXmlIn.SelectSingleNode("//SearchEnabled");
                if (Node != null)
                    s_Search = Node.InnerText;

                //Start rsushilaggar 05/07/2010 MITS 20606
                objSettings = new SysSettings(connectionString, base.ClientId); //Ash - cloud
                sUseEntityApproval = objSettings.UseEntityApproval.ToString().ToLower();
                sShowLSSLink = objSettings.ShowLSSInvoice.ToString().ToLower();
                sApproveTransSearch = objSupApprove.UseSupApprovalSearch.ToString().ToLower();
                sEnableVoidReason = objSupApprove.EnableVoidReason.ToString().ToLower();
                sEnableOffset = objSupApprove.EnableOffset.ToString();   //mcapps2 MITS 30236
                sbXmlBuilder = new StringBuilder();
                //RMA-6404  achouhan3   Added to Support Angular Grid
                StringBuilder strTransaction = new StringBuilder();
                sbXmlBuilder.Append("<SupervisorApproval>");
                if (sApproveTransSearch == "false" || s_Search=="true")
                {
                    //End rsushilaggar
                    //RMA-6404  achouhan3   Modified to Support Angular Grid Starts
                    objSupApprove.BuildSearchFilter(p_objXmlIn);
                    if (String.Compare(sResponseType, "JSON", true) != 0)
                        objSupApprove.ListFundsTransactions(p_objXmlIn, out objAllTrans, out objDenyTrans, out sError, sLangCode);
                    else
                    {
                       
                        objSupApprove.GetFundsTransactions(p_objXmlIn, out objAllTrans, out objDenyTrans, out sError, base.userLogin, sPageName, sGridId, sLangCode);
                    }
                    //RMA-6404  achouhan3   Modified to Support Angular Grid Ends
				}
                //RMA-6404  achouhan3   Modified to Support Angular Grid Starts
				if (objAllTrans!=null)
				{
					// Rajeev -- 05/10/05 -- Check for node
                    if (objAllTrans.SelectSingleNode("//FundsTransactions/Transaction") != null)
                    {
                        if (objAllTrans.SelectSingleNode("//FundsTransactions/Transaction").InnerXml.Length > 0)
                        {   
                            sbXmlBuilder.Append("<AllTransactions>");
                            sbXmlBuilder.Append(objAllTrans.SelectSingleNode("//FundsTransactions").InnerXml);
                            sbXmlBuilder.Append("</AllTransactions>");
                        }

                    }
                    else
                    {
                        if (objAllTrans.SelectSingleNode("//FundsTransactions/PaymentsTransaction") != null)
                        {
                            if (objAllTrans.SelectSingleNode("//FundsTransactions/PaymentsTransaction").InnerXml.Length > 0)
                            {
                                strTransaction.Append("<PaymentTransaction>");
                                strTransaction.Append(objAllTrans.SelectSingleNode("//FundsTransactions/PaymentsTransaction").InnerXml);
                                strTransaction.Append("</PaymentTransaction>");
                            }
                        }
                        if (objAllTrans.SelectSingleNode("//FundsTransactions/CollectionTransaction") != null)
                        {
                            if (objAllTrans.SelectSingleNode("//FundsTransactions/CollectionTransaction").InnerXml.Length > 0)
                            {
                                strTransaction.Append("<CollectionTransaction>");
                                strTransaction.Append(objAllTrans.SelectSingleNode("//FundsTransactions/CollectionTransaction").InnerXml);
                                strTransaction.Append("</CollectionTransaction>");
                            }
                        }
                        sbXmlBuilder.Append("<AllTransactions>");
                        sbXmlBuilder.Append(strTransaction);
                        sbXmlBuilder.Append("</AllTransactions>");
                    }
                    
				}
				if (objDenyTrans!=null)
				{
					// Rajeev -- 05/10/05 -- Check for node
					if (objDenyTrans.SelectSingleNode("//FundsTransactions/Transaction") != null)
					{
						if (objDenyTrans.SelectSingleNode("//FundsTransactions/Transaction").InnerXml.Length > 0)
						{
							sbXmlBuilder.Append("<DenyTransactions>");
							sbXmlBuilder.Append(objDenyTrans.SelectSingleNode("//FundsTransactions").InnerXml);
							sbXmlBuilder.Append("</DenyTransactions>");
						}
					}
                    else if (objDenyTrans.SelectSingleNode("//FundsTransactions/DenialTransaction") != null)
                    {
                        if (objDenyTrans.SelectSingleNode("//FundsTransactions/DenialTransaction").InnerXml.Length > 0)
                        {
                            sbXmlBuilder.Append("<DenyTransactions>");
                            sbXmlBuilder.Append(objDenyTrans.SelectSingleNode("//FundsTransactions/DenialTransaction").InnerXml);
                            sbXmlBuilder.Append("</DenyTransactions>");
                        }
                    }
				}
                //RMA-6404  achouhan3   Modified to Support Angular Grid Ends
				if (sError.Trim().Length > 0)
				{
					objInfoDoc=new XmlDocument();
					objInfoDoc.LoadXml(sError);
					objInfoList=objInfoDoc.SelectNodes("//Errors/Error");
					for (int i=0;i<objInfoList.Count;i++)
					{
						//objSoftErr=new BusinessAdaptorError("SupervisoryApprovalListFundsTransactionsMessage",objInfoList[i].InnerText,BusinessAdaptorErrorType.Message);
						//p_objErrOut.Add(objSoftErr);
						sbXmlBuilder.Append("<ErrorTransactions>");
						sbXmlBuilder.Append(objInfoList[i].InnerText);
						sbXmlBuilder.Append("</ErrorTransactions>");
					}
                }
                //smahajan6: Safeway: Payment Supervisory Approval : Start - For adding warning Message
                if (sWarning.Trim().Length > 0)
                {
                    objInfoDoc = new XmlDocument();
                    objInfoDoc.LoadXml(sWarning);
                    objInfoList = objInfoDoc.SelectNodes("//Warnings/Warning");
                    for (int i = 0; i < objInfoList.Count; i++)
                    {
                        sbXmlBuilder.Append("<WarningTransactions>");
                        sbXmlBuilder.Append(objInfoList[i].InnerText);
                        sbXmlBuilder.Append("</WarningTransactions>");
                    }
				}
                //smahajan6: Safeway: Payment Supervisory Approval : end
                sbXmlBuilder.Append("<ScreenUISettings>");
                sbXmlBuilder.Append("<ShowLSSLink>");
                sbXmlBuilder.Append(sShowLSSLink);
                sbXmlBuilder.Append("</ShowLSSLink>");

                sbXmlBuilder.Append("<UseEntityPaymentApproval>");
                sbXmlBuilder.Append(sUseEntityApproval);
                sbXmlBuilder.Append("</UseEntityPaymentApproval>");
                //Start: rsushilaggar Vendor's Approval 07-May-2010 MITS 20606,19970,20938
                sbXmlBuilder.Append("<ApproveTransactionSearch>");
                sbXmlBuilder.Append(sApproveTransSearch);
                sbXmlBuilder.Append("</ApproveTransactionSearch>");
                sbXmlBuilder.Append("<EnableVoidReason>");
                sbXmlBuilder.Append(sEnableVoidReason);
                sbXmlBuilder.Append("</EnableVoidReason>");
                sbXmlBuilder.Append("<EnableOffset>");  //mcapps2 MITS 30236
                sbXmlBuilder.Append(sEnableOffset);  //mcapps2 MITS 30236
                sbXmlBuilder.Append("</EnableOffset>");  //mcapps2 MITS 30236
                sbXmlBuilder.Append("<PutLSSCollectionOnHold>");
                sbXmlBuilder.Append(objSupApprove.PutLSScollectionOnHold.ToString().ToLower());
                sbXmlBuilder.Append("</PutLSSCollectionOnHold>");
                //End: rsushilaggar Vendor's Approval 07-May-2010

                //pkandhari JIRA 8889 starts
                //for apporve
                sbXmlBuilder.Append("<ApprovePermission>");
                sbXmlBuilder.Append(userLogin.IsAllowedEx(ApprovePermission));
                sbXmlBuilder.Append("</ApprovePermission>");
                
                //for Void
                sbXmlBuilder.Append("<VoidPermission>");
                sbXmlBuilder.Append(userLogin.IsAllowedEx(VoidPermission));
                sbXmlBuilder.Append("</VoidPermission>");

                //for deny
                sbXmlBuilder.Append("<DenyPermission>");
                sbXmlBuilder.Append(userLogin.IsAllowedEx(DenyPermission));
                sbXmlBuilder.Append("</DenyPermission>");
                //pkandhari JIRA 8889 ends

                sbXmlBuilder.Append("</ScreenUISettings>");
				sbXmlBuilder.Append("</SupervisorApproval>");
				p_objXmlOut.LoadXml(sbXmlBuilder.ToString());
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,CommonFunctions.FilterBusinessMessage(Globalization.GetString("SupervisorApprovalAdaptor.ListFundsTransactions.Error", base.ClientId),sLangCode),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objSupApprove.Dispose(); // important to kill DataModelFactory
				objSupApprove=null;
				objAllTrans=null;
				objDenyTrans=null;
				sbXmlBuilder=null;
				objInfoDoc=null;
				objInfoList=null;
                objSettings = null;
			}
			return true;
		}

        /// <summary>
        /// GetPayeeCheckData: Get the XML of the list Payee/Checks/Autochecks on either freeze or hold mode
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>


        public bool GetPayeeCheckData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            SupervisorApproval objSupApprove = null;
            XmlDocument objOut = new XmlDocument();
            int i_pagenumber = -1;
            int i_tabindex = -1;

            try
            {
                objSupApprove = new SupervisorApproval(connectionString, userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);

                if (p_objXmlIn.SelectSingleNode("//PageNumber") != null && p_objXmlIn.SelectSingleNode("//PageNumber").InnerText != "")
                    i_pagenumber = Int32.Parse(p_objXmlIn.SelectSingleNode("//PageNumber").InnerText.ToString());

                if (p_objXmlIn.SelectSingleNode("//TabIndex") != null && p_objXmlIn.SelectSingleNode("//TabIndex").InnerText != "")
                    i_tabindex = Int32.Parse(p_objXmlIn.SelectSingleNode("//TabIndex").InnerText.ToString());


                objOut = objSupApprove.GetPayeeCheckData(i_tabindex, i_pagenumber);

                p_objXmlOut = objOut;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SupervisorApprovalAdaptor.GetPayeeCheckData.Error", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                return false;
            }
            finally
            {
                objSupApprove.Dispose(); // important to kill DataModelFactory
                objSupApprove = null;
            }

            return true;
        }

        /// <summary>
        /// SaveAndReleasePayeeCheckData: Change the status of Payee/Check/Schedule Check from Freeze/Hold to UnFreeze/Release
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>

        public bool SaveAndReleasePayeeCheckData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            SupervisorApproval objSupApprove = null;
            XmlDocument objOut = new XmlDocument();
            int i_pagenumber = -1;
            int i_tabindex = -1;
            string templist = string.Empty;

            try
            {
                objSupApprove = new SupervisorApproval(connectionString, userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);

                if (p_objXmlIn.SelectSingleNode("//TabIndex") != null && p_objXmlIn.SelectSingleNode("//TabIndex").InnerText != "")
                    i_tabindex = Int32.Parse(p_objXmlIn.SelectSingleNode("//TabIndex").InnerText.ToString());


                switch (i_tabindex)
                {
                    case 1:
                        templist = p_objXmlIn.SelectSingleNode("//PayeeReleaseList").InnerText;
                        objOut = objSupApprove.SaveAndReleasePayeeCheckData(1, i_pagenumber, templist, null, null);
                        break;
                    case 2:
                        templist = p_objXmlIn.SelectSingleNode("//CheckReleaseList").InnerText;
                        objOut = objSupApprove.SaveAndReleasePayeeCheckData(2, i_pagenumber, null, templist, null);
                        break;
                    case 3:
                        templist = p_objXmlIn.SelectSingleNode("//BatchReleaseList").InnerText;
                        objOut = objSupApprove.SaveAndReleasePayeeCheckData(3, i_pagenumber, null, null, templist);
                        break;
                    default:
                        break;
                }
                
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SupervisorApprovalAdaptor.SaveAndReleasePayeeCheckData.Error", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                return false;
            }
            finally
            {
                objSupApprove.Dispose(); // important to kill DataModelFactory
                objSupApprove = null;
            }

            return true;
        }
		#endregion

        //RMA-6404  achouhan3   Added to restore Grid Preferences Starts
        /// <summary>
        /// This functio is used to get default user pref for the grid
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErr"></param>
        /// <returns></returns>
        public bool RestoreDefaults(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            string sPageName = "";
            SupervisorApproval objSupervisory = null;
            string sGridId = "";
            string sUserPrefString = "";
            XmlElement oUserPrefElement;
            try
            {
                objSupervisory = new SupervisorApproval(base.connectionString, this.m_userLogin.objRiskmasterDatabase.DataSourceName, this.m_userLogin.LoginName, this.m_userLogin.Password, base.ClientId);
                if (p_objXmlIn.SelectSingleNode("//PageName") != null)
                    sPageName = p_objXmlIn.SelectSingleNode("//PageName").InnerText.Trim();
                if (p_objXmlIn.SelectSingleNode("//GridId") != null)
                    sGridId = p_objXmlIn.SelectSingleNode("//GridId").InnerText.Trim();
                sUserPrefString = objSupervisory.RestoreDefaultUserPref(m_userLogin.UserId, sPageName, m_userLogin.DatabaseId, sGridId);
                oUserPrefElement = p_objXmlOut.CreateElement("UserPref");
                oUserPrefElement.InnerText = sUserPrefString;
                p_objXmlOut.AppendChild(oUserPrefElement);
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("GridPreference.RestoreDefaults.Err", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }
        //RMA-6404  achouhan3   Added to restore Grid Preferences Starts
	}
}
