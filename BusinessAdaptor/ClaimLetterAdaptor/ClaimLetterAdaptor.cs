using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Settings;
using Riskmaster.Application.AcrosoftWebserviceWrapper;
using Riskmaster.Application.MailMerge;
using Riskmaster.Application.FileStorage;
using Riskmaster.Application.ClaimLetter;
using Riskmaster.Application.DocumentManagement;


namespace Riskmaster.BusinessAdaptor
{

	///************************************************************** 
	///* $File		: ClaimLetterAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 25-May-2009
	///* $Author	: Mridul Bansal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for Mail Merge which 
	///	contains methods to generate merged documents.	
	/// </summary>
	public class ClaimLetterAdaptor:BusinessAdaptorBase
	{

		private int m_iDocPathType=0;
        private string m_sDocPath = string.Empty;

		#region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
		public ClaimLetterAdaptor()
		{
            //Initiate all Acrosoft settings
            //TODO: Determine if this instantiation needs to be present in multiple classes
            //TODO: or if it can be instantiated and managed in a central location
            RMConfigurationManager.GetAcrosoftSettings();
        }
		#endregion

		#region Public methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetClaimLetter(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            #region Variables
            string sFormName = "";
            string sLtrType = "";
            int lRecordId = 0;
            XmlNode objNode = null;
            ClaimLetter objClmLtr = null;
            string LtrTmplContent = "";
            string[] sData;
            int iLvl = 1;
            #endregion Variables

            try
            {
                sFormName = p_objXmlIn.SelectSingleNode(@"/ClaimLetter/FormName").InnerText;
                lRecordId = Convert.ToInt32(p_objXmlIn.SelectSingleNode(@"/ClaimLetter/RecordId").InnerText);
                sLtrType = p_objXmlIn.SelectSingleNode(@"/ClaimLetter/LetterType").InnerText;

                objClmLtr = new ClaimLetter(userLogin, base.ClientId);
                objClmLtr.DSN = connectionString;
                objClmLtr.GenerateClaimLetter(out LtrTmplContent, sLtrType, lRecordId, out sData);

                //Output XML
                objNode = p_objXmlOut.CreateElement("Template");
                p_objXmlOut.AppendChild(objNode);
                XmlElement e = p_objXmlOut.CreateElement("File");
                if (sLtrType == "ACK")
                    e.SetAttribute("Filename", Generic.GetServerUniqueFileName("ACK", "doc",base.ClientId));
                else
                    e.SetAttribute("Filename", Generic.GetServerUniqueFileName("CL", "doc", base.ClientId));
                e.InnerText = LtrTmplContent;
                objNode.AppendChild(e);

                foreach (string sTmp in sData)
                {
                    e = p_objXmlOut.CreateElement("Level" + iLvl.ToString());
                    e.InnerText = sData[iLvl-1];
                    objNode.AppendChild(e);
                    iLvl += 1;
                }
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ClaimLetterAdaptor.GetClaimLetter.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objNode = null;
                objClmLtr = null;
            }
        }


        public bool GetClmLetter(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            #region Variables
            string sFormName = "";
            string sLtrType = "";
            int lRecordId = 0;
            XmlNode objNode = null;
            ClaimLetter objClmLtr = null;
            string LtrTmplContent = "";
            string[] sData;
            int iLvl = 1;
            MergeManager objMergeManager = null;            
            string sFile = "";
            StringBuilder sbRowid = null;
            string sAttachDocument = "";
            string sDirectDisplay = "";
            int iTemplateID = 0;
            string sDataSource = "";
            XmlDocument p_objXmlTemp = null;
            XmlElement objElement = null;
            string sPassword = "";
            
            DataModelFactory objDataModelFactory = null;//mudabbir added 36022

            #endregion Variables

            try
            {
                sFormName = p_objXmlIn.SelectSingleNode(@"/ClaimLetter/FormName").InnerText;
                lRecordId = Convert.ToInt32(p_objXmlIn.SelectSingleNode(@"/ClaimLetter/RecordId").InnerText);
                sLtrType = p_objXmlIn.SelectSingleNode(@"/ClaimLetter/LetterType").InnerText;                

                objClmLtr = new ClaimLetter(userLogin, base.ClientId);
                objClmLtr.DSN = connectionString;

                sPassword = objClmLtr.GetPassword(lRecordId);
                //If no Password Found at any Org Level, Ack Claim Letter process if not triggered
                if (sPassword == "")
                    return false;
                
                iTemplateID = objClmLtr.GetTemplateID(sLtrType);                                
                p_objXmlTemp = new XmlDocument();
                
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                
                objMergeManager = new MergeManager(userLogin, securityConnectionString, userID.ToString(), base.ClientId);//rkaur27


                objMergeManager.DSN = connectionString;
                objMergeManager.UID = userID.ToString();

                if (m_sDocPath != "")
                {
                    objMergeManager.TemplatePath = m_sDocPath;
                }                
                objMergeManager.DocStorageDSN = m_sDocPath;
                objMergeManager.DocPathType = m_iDocPathType;
                objMergeManager.SecurityDSN = securityConnectionString;
                

                objMergeManager.MoveTo(Conversion.ConvertStrToLong(iTemplateID.ToString()));
                
                objNode = p_objXmlIn.SelectSingleNode(@"/ClaimLetter/RecordId");
                if (objNode.InnerText != null)
                {
                    objMergeManager.CurMerge.RecordId = Conversion.ConvertStrToLong(objNode.InnerText);
                }

                
                objMergeManager.CurMerge.FetchData();

                //added by Navdeep                
                int icount = objMergeManager.CurMerge.GetRecordCount();
                if (icount > 1)
                {
                    objNode = p_objXmlIn.SelectSingleNode(@"/ClaimLetter/RowIds");
                    for (int i = 1; i <= icount; i++)
                    {
                        objElement = p_objXmlIn.CreateElement("RowId");
                        objElement.InnerText = i.ToString();
                        objNode.AppendChild(objElement);
                    }
                }
                //End Navdeep
                                
                sbRowid = new StringBuilder();
                foreach (XmlNode objTemp in objNode.SelectNodes("//RowIds/RowId"))
                {
                    sbRowid.Append(objTemp.InnerText);
                    sbRowid.Append(",");
                }
                if (sbRowid.ToString().Length > 0)
                {
                    sbRowid.Remove(sbRowid.Length - 1, 1);
                }               
                if (sbRowid.Length == 0)
                    sbRowid.Append("1");
                

                //objNode = p_objXmlIn.SelectSingleNode(@"/Template/IsPreFab");
                //if (objNode != null && objNode.InnerText != "")
                //{
                //    objMergeManager.CurMerge.PreFab = true;
                //    objMergeManager.CurMerge.Template.Prefab = objMergeManager.CurMerge.Template.FormFileName;
                //}

                //get the template
                objNode = p_objXmlOut.CreateElement("Template");
                p_objXmlOut.AppendChild(objNode);
                XmlElement e = p_objXmlOut.CreateElement("File");
                e.SetAttribute("Filename", Generic.GetServerUniqueFileName("mmt", "doc",base.ClientId));
                e.InnerText = objMergeManager.CurMerge.Template.FormFileContent;
                objNode.AppendChild(e);

                //get the datasource if user is doing a merge (PreFab means user is creating a merge template)
                //when user is creating template the merge fields are passed by wizard in a hidden field
                if (!objMergeManager.CurMerge.PreFab)
                {
                    e = p_objXmlOut.CreateElement("DataSource");
                    e.InnerText = objMergeManager.CurMerge.GetMergeDataSource(sbRowid.ToString());
                    objNode.AppendChild(e); 
                    //divide the DataSource and split it with the first combination with    
                    //each level of Org Hierarchy. 
                    objClmLtr.PrepareSplitXML(e.InnerText, iTemplateID, (sbRowid.ToString()).Split(',').Length , lRecordId, objNode,  ref p_objXmlOut);                                       
                }

                //get the datasource if user is doing a merge (PreFab means user is creating a merge template)
                //when user is creating template the merge fields are passed by wizard in a hidden field
                if (!objMergeManager.CurMerge.PreFab)
                {
                    e = p_objXmlOut.CreateElement("HeaderContent");
                    e.InnerText = objMergeManager.CurMerge.Template.HeaderFileContent;
                    objNode.AppendChild(e);
                }

                //get number of records
                if (!objMergeManager.CurMerge.PreFab)
                {
                    //string[] arrRowid = (sbRowid.ToString()).Split(',');
                    //int iNumRecords = arrRowid.Length;
                    e = p_objXmlOut.CreateElement("NumRecords");
                    //e.InnerText = iNumRecords.ToString();
                    e.InnerText = p_objXmlOut.SelectSingleNode("//SplitDataNum").InnerText;                    
                    objNode.AppendChild(e);
                }

                //Add password to the XML
                e = p_objXmlOut.CreateElement("Password");
                e.InnerText = sPassword;
                objNode.AppendChild(e);

                //Add Secured File Name 
                e = p_objXmlOut.CreateElement("SecFileName");
                if (sLtrType == "ACK")
                    e.InnerText = sFormName + "_AckLetter";
                if (sLtrType == "CL")
                    e.InnerText = sFormName + "_ClosedLetter";
                objNode.AppendChild(e);

                //Mudabbir added 36022
                objDataModelFactory = new DataModelFactory(m_userLogin, ClientId);
                e = p_objXmlOut.CreateElement("UseSilverlight");
                e.InnerText = objDataModelFactory.Context.InternalSettings.SysSettings.UseSilverlight.ToString().ToLower();
                p_objXmlOut.FirstChild.AppendChild(e);


                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ClaimLetterAdaptor.GetClaimLetter.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objNode = null;
                objClmLtr = null;
                //mudabbir disposed : 36022
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool AttachAndMailLetter(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            #region Variables
            ClaimLetter objClmLtr = null;
            XmlNode objNode = null;
            MemoryStream objMemory = null;
            StringBuilder sFileName = null;
            int iCtr = 1;
            string sLetterType = "";
            string sSubject = "";
            string sNotes = "";
            string sFileNameSuffix = "";
            string sKeywords = "";
            string sClass = "";
            string sCategory = "";
            string sType = "";
            int iRecordId = 0;
            string sClaimNo = "";
            string sTemp = "";
            string sFormName = "";
            string[] sTitle = {"", "", "", "", "", "", "", ""};
            int iPsid = 0;
            bool bMark = false;            
            #endregion Variables

            try
            {
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }

                //Retrieve psid from request - For FUTURE
                objNode = p_objXmlIn.SelectSingleNode(@"/Template/psid");
                if (objNode.InnerText != null)
                {
                    iPsid = Conversion.ConvertStrToInteger(objNode.InnerText);
                }

                sLetterType = p_objXmlIn.SelectSingleNode(@"/Template/LetterType").InnerText;
                if (sLetterType == "ACK")
                {
                    sSubject = "Acknowledgment Claim Letter";
                    sFileNameSuffix = "AckLetter";
                }
                else
                {
                    sSubject = "Closed Claim Letter";
                    sFileNameSuffix = "ClosingLetter";
                }
                sNotes = p_objXmlIn.SelectSingleNode(@"/Template/Notes").InnerText;
                sClass = p_objXmlIn.SelectSingleNode(@"/Template/Class").InnerText;
                sCategory = p_objXmlIn.SelectSingleNode(@"/Template/Category").InnerText;
                sType = p_objXmlIn.SelectSingleNode(@"/Template/Type").InnerText;
                iRecordId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode(@"/Template/RecordId").InnerText);
                sClaimNo = p_objXmlIn.SelectSingleNode(@"/Template/ClaimNumber").InnerText;
                sFormName = p_objXmlIn.SelectSingleNode(@"/Template/FormName").InnerText;

                //Titles - Here Org Hierarchy Level Names
                sTitle[0] = p_objXmlIn.SelectSingleNode(@"/Template/Title1").InnerText;
                sTitle[1] = p_objXmlIn.SelectSingleNode(@"/Template/Title2").InnerText;
                sTitle[2] = p_objXmlIn.SelectSingleNode(@"/Template/Title3").InnerText;
                sTitle[3] = p_objXmlIn.SelectSingleNode(@"/Template/Title4").InnerText;
                sTitle[4] = p_objXmlIn.SelectSingleNode(@"/Template/Title5").InnerText;
                sTitle[5] = p_objXmlIn.SelectSingleNode(@"/Template/Title6").InnerText;
                sTitle[6] = p_objXmlIn.SelectSingleNode(@"/Template/Title7").InnerText;
                sTitle[7] = p_objXmlIn.SelectSingleNode(@"/Template/Title8").InnerText;

                foreach (string sTmp in sTitle)
                {
                    sTemp = sTitle[iCtr - 1];
                    if (sTemp != "")//Attach if Title is non-empty - Empty title signifies that letter has not been generated.
                    {
                        //sFileName = new StringBuilder();
                        //sFileName.Append(sClaimNo + sFileNameSuffix + iCtr.ToString() + ".doc");                        
                        //objMemory = new MemoryStream(Convert.FromBase64String(p_objXmlIn.SelectSingleNode(@"/Template/L" + iCtr + "File").InnerText));
                        //AttachDocument(sFileName.ToString(), objMemory, sSubject, sTemp, sNotes, sKeywords, sClass, sCategory, sType, iRecordId, iPsid, sFormName);
                        bMark = true;
                    }
                    iCtr++;
                }

                //Email Secured Letters
                objClmLtr = new ClaimLetter(userLogin, base.ClientId);
                objClmLtr.DSN = connectionString;
                objClmLtr.SecureConnString = securityConnectionString;
                objClmLtr.RecordId = iRecordId;
                objClmLtr.Password = p_objXmlIn.SelectSingleNode(@"/Template/Password").InnerText;
                objClmLtr.FileSuffix = sFileNameSuffix;

                if (bMark)
                {
                    //Mark Claim
                    if (sLetterType == "ACK")
                        objClmLtr.MarkClaim(iRecordId, -1);
                    else if (sLetterType == "CL")
                        objClmLtr.MarkClaim(iRecordId, 1);

                    iCtr = 1;
                    foreach (string sTmp in sTitle)
                    {
                        sTemp = sTitle[iCtr - 1];
                        if (sTemp != "")//Attach if Title is non-empty - Empty title signifies that letter wasn't generated.
                        {
                            //Send Email
                            objClmLtr.MailClaimLetters(sTemp, p_objXmlIn.SelectSingleNode(@"/Template/L" + iCtr + "SecFile").InnerText, iCtr);
                        }
                        iCtr++;
                    }
                    
                    iCtr = 1;
                    foreach (string sTmp in sTitle)
                    {
                        sTemp = sTitle[iCtr - 1];
                        if (sTemp != "")//Attach if Title is non-empty - Empty title signifies that letter has not been generated.
                        {
                            sFileName = new StringBuilder();
                            sFileName.Append(sClaimNo + sFileNameSuffix + iCtr.ToString() + ".doc");
                            objMemory = new MemoryStream(Convert.FromBase64String(p_objXmlIn.SelectSingleNode(@"/Template/L" + iCtr + "File").InnerText));
                            AttachDocument(sFileName.ToString(), objMemory, sSubject, sTemp, sNotes, sKeywords, sClass, sCategory, sType, iRecordId, iPsid, sFormName);
                            bMark = true;
                        }
                        iCtr++;
                    }
                }
                return true;
            }
            catch (RMAppException p_objException)
            {                         
                p_objErrOut.Add(p_objException,p_objException.Message.ToString(), BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ClaimLetterAdaptor.AttachAndMailLetter.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objNode = null;
                objClmLtr = null;
                objMemory = null;
            }
        }

        /// <summary>
        /// Author : Mridul Bansal.
        /// Date   : 06/11/09
        /// </summary>
        /// <param name="p_sFileName"></param>
        /// <param name="objMemory"></param>
        /// <param name="p_sSubject"></param>
        /// <param name="p_sName"></param>
        /// <param name="p_sNotes"></param>
        /// <param name="p_sKeywords"></param>
        /// <param name="p_sClass"></param>
        /// <param name="p_sCategory"></param>
        /// <param name="p_sType"></param>
        /// <param name="p_iRecordId"></param>
        /// <param name="iPsid"></param>
        /// <param name="p_sFormName"></param>
        private void AttachDocument(string p_sFileName, MemoryStream objMemory, string p_sSubject,
                                    string p_sName, string p_sNotes, string p_sKeywords, string p_sClass, string p_sCategory,
                                    string p_sType, int p_iRecordId, int iPsid, string p_sFormName)
        {
            XmlDocument objDOM = null;
            DocumentManager objDocumentManager = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            long lDocumentId = 0;
            RMConfigurator objConfig = null;
            SysSettings objSettings = null;
            Acrosoft objAcrosoft = null;
            BinaryReader binaryRdr = null;
            Byte[] fileBuffer = null;

            int iRetCd = 0;
            string sAppExcpXml = "";
            objConfig = new RMConfigurator();
            objAcrosoft = new Acrosoft(base.ClientId);//dvatsa-cloud
            Claim objClaim = null;
            Event objEvent = null;
            DataModelFactory objDataModelFactory = null;
            


            try
            {
                objDocumentManager = new DocumentManager(m_userLogin,base.ClientId);
                objDocumentManager.ConnectionString = connectionString;
                objDocumentManager.UserLoginName = userLogin.LoginName;

                objDataModelFactory = new DataModelFactory(m_userLogin,base.ClientId);
                objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_iRecordId);

                if (this.userLogin.DocumentPath.Length > 0)
                {
                    objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                }
                else
                {
                    if (userLogin.objRiskmasterDatabase.DocPathType == 0)
                    {
                        objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
                    }
                    else
                    {
                        objDocumentManager.DocumentStorageType = StorageType.DatabaseStorage;
                    }
                }

                if (this.userLogin.DocumentPath.Length > 0)
                {
                    objDocumentManager.DestinationStoragePath = userLogin.DocumentPath;
                }
                else
                {
                    objDocumentManager.DestinationStoragePath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }

                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("data");
                objDOM.AppendChild(objElemParent);

                objElemChild = objDOM.CreateElement("Document");

                objElemTemp = objDOM.CreateElement("DocumentId");
                objElemTemp.InnerText = "0";
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("FolderId");
                objElemTemp.InnerText = "0";
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Notes");
                objElemTemp.InnerText = p_sNotes;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Class");
                objElemTemp.InnerText = p_sClass;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Category");
                objElemTemp.InnerText = p_sCategory;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Type");
                objElemTemp.InnerText = p_sType;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("CreateDate");
                objElemTemp.InnerText = Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now);
                objElemChild.AppendChild(objElemTemp);
                
                objElemTemp = objDOM.CreateElement("Name");
                objElemTemp.InnerText = p_sName;
                objElemChild.AppendChild(objElemTemp);


                objElemTemp = objDOM.CreateElement("Subject");
                objElemTemp.InnerText = p_sSubject;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("UserLoginName");
                objElemTemp.InnerText = userLogin.LoginName;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("FileName");
                objElemTemp.InnerText = p_sFileName;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("FilePath");
                objElemTemp.InnerText = "";
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Keywords");
                objElemTemp.InnerText = p_sKeywords;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("AttachTable");
                ////Shruti for 7918
                //if (Conversion.ConvertStrToInteger(p_sCatid) == 20)
                //{
                //    objElemTemp.InnerText = p_sFormName.ToUpper();
                //}
                //else
                //{
                //    objElemTemp.InnerText = CatIdToRMTableName(p_sCatid);
                //}
                objElemTemp.InnerText = "CLAIM";
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("Subject");
                objElemTemp.InnerText = p_sSubject;
                objElemChild.AppendChild(objElemTemp);

                objElemTemp = objDOM.CreateElement("AttachRecordId");
                objElemTemp.InnerText = p_iRecordId.ToString();
                objElemChild.AppendChild(objElemTemp);

                //Nikhil Garg		Defect No: 2397		Dated: 03/10/2006
                objElemTemp = objDOM.CreateElement("FormName");
                objElemTemp.InnerText = p_sFormName;
                objElemChild.AppendChild(objElemTemp);

                objDOM.FirstChild.AppendChild(objElemChild);

                if (objClaim.LineOfBusCode == 241)
                    iPsid = 150;
                if (objClaim.LineOfBusCode == 242)
                    iPsid = 6000;
                if (objClaim.LineOfBusCode == 243)
                    iPsid = 3000;
                if (objClaim.LineOfBusCode == 844)
                    iPsid = 60000;

                //08/10/2006.. Raman Bhatia : If Acrosoft is enabled then legacy document management is not used
                //Instead store attachment using Acrosoft

                objSettings = new SysSettings(connectionString, base.ClientId); //Ash - cloud
                if (objSettings.UseAcrosoftInterface == false)
                {
                    objDocumentManager.AddDocument(objDOM.InnerXml, objMemory, iPsid, out lDocumentId);
                } // if
                else
                {
                    binaryRdr = new BinaryReader(objMemory);
                    fileBuffer = binaryRdr.ReadBytes(Convert.ToInt32(objMemory.Length));
                    binaryRdr.Close();

                    //string sTableName = CatIdToRMTableName(p_sCatid);
                    //string sPath = objConfig.GetValue("RMSettings/StandardPaths/UserDataPath");
                    string sPath = RMConfigurator.UserDataPath;
                    string sFullFileName = sPath + @"\MailMerge\" + p_sFileName;
                    
                    //For CLAIM ONLY
                    //objDataModelFactory = new DataModelFactory(m_userLogin);
                    //objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                    //objClaim.MoveTo(p_iRecordId);
                    string sClaimNumber = objClaim.ClaimNumber;
                    string sEventNumber = objClaim.EventNumber;
                    //rsolanki2 :  start updates for MCM mits 19200 
                    Boolean bSuccess = false;
                    string sTempAcrosoftUserId = base.userLogin.LoginName;
                    string sTempAcrosoftPassword = base.userLogin.Password;

                    if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                    {
                        if (AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName] != null
                            && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].RmxUser))
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].AcrosoftUserId;
                            sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].AcrosoftPassword;
                        }
                        else
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                            sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                        }
                    }
                    //creating folder

                    objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                        AcrosoftSection.AcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber,
                        AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                    //storing document

                    iRetCd = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
                        p_sFileName, 
                        fileBuffer, p_sFileName, p_sType, sEventNumber, sClaimNumber,
                        AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", sTempAcrosoftUserId, out sAppExcpXml);
                    
                    //rsolanki2 :  end updates for MCM mits 19200 

                    //objAcrosoft.CreateAttachmentFolder(base.userLogin.LoginName, base.userLogin.Password, AcrosoftSection.AcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber,
                    //    AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                    ////storing document
                    ////01/07/2007 Raman Bhatia: Document needs to be picked from memorystream
                    ////iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, sFullFileName, p_sSubject, p_sType, sEventNumber, sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", m_userLogin.LoginName, out sAppExcpXml);
                    ////iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, p_sFileName, fileBuffer, p_sSubject, p_sType, sEventNumber, sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", m_userLogin.LoginName, out sAppExcpXml);
                    //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, p_sFileName, fileBuffer, p_sFileName, p_sType, sEventNumber, sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", m_userLogin.LoginName, out sAppExcpXml);
                    objClaim = null;
                    objAcrosoft = null;
                    //objDataModelFactory.UnInitialize();
                } // else
            }
            finally
            {
                if (objDataModelFactory != null)
                {
                    objDataModelFactory.Dispose();
                }
                objDOM = null;
                objDocumentManager = null;
                objElemParent = null;
                objElemChild = null;
                objElemTemp = null;
                objConfig = null;
                objSettings = null;
                if (objClaim != null)
                {
                    objClaim.Dispose();
                }
                if (objEvent != null)
                {
                    objEvent.Dispose();
                }
                if (objConfig != null)
                    objConfig = null;
            }
        }
		#endregion
	}
}
