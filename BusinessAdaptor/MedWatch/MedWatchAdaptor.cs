using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.MedWatch;
using Riskmaster.Application.FileStorage;
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: MedWatchManagerAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 18-Feb-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	A class representing the MedWatch Adaptor.
	/// </summary>
	public class MedWatchManagerAdaptor:BusinessAdaptorBase
	{
		#region MedWatchManagerAdaptor constructor
		public MedWatchManagerAdaptor(){}
		#endregion

		#region Public Functions
		/// <summary>
		/// Get attached report based on document id
		/// </summary>
		/// <param name="p_objXmlIn">
		/// The structure of input XML document would be:
		/// <Document>
		///		<MedWatch>
		///			<EventId>Eventid</EventId>
		///			<Documents>
		///				<GetDocument>Document id</GetDocument>
		///				<GetDocument>Document id</GetDocument>
		///			</Documents>
		///		</MedWatch>
		///	</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<MedWatch><File Filename=""></File></MedWatch>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetAttachedReport(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			Manager objManager=null;
			XmlNodeList objInfoList=null;
			MemoryStream objMemory=null;
			XmlElement objTempElement=null;
			
			try
			{
				objManager=new Manager(userLogin,connectionString, base.ClientId);//rkaur27
				objTempElement=p_objXmlOut.CreateElement("MedWatch");
				p_objXmlOut.AppendChild(objTempElement);
				objInfoList=p_objXmlIn.SelectNodes("/Document/MedWatch/Documents/GetDocument");
				for (int i=0;i<objInfoList.Count;i++)
				{
					if (userLogin.objRiskmasterDatabase.DocPathType==0)
					{
						objManager.GetAttachedFile(Conversion.ConvertStrToInteger(objInfoList[i].InnerText),StorageType.FileSystemStorage,userLogin.objRiskmasterDatabase.GlobalDocPath,out objMemory);
					}
					else
					{
						objManager.GetAttachedFile(Conversion.ConvertStrToInteger(objInfoList[i].InnerText),StorageType.DatabaseStorage,userLogin.objRiskmasterDatabase.GlobalDocPath,out objMemory);
					}
					objTempElement=p_objXmlOut.CreateElement("File");
					objTempElement.SetAttribute("Filename",GetFileName()+".fdf");
					objTempElement.InnerText=Convert.ToBase64String(objMemory.ToArray());
					p_objXmlOut.FirstChild.AppendChild(objTempElement);
				}
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MedWatchAdaptor.GetAttachedReport.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				//Modified by SHivendu
                if (objManager != null)
                {
                    objManager.Dispose();
                    objManager = null;
                }
				objInfoList=null;
				objTempElement=null;
				objMemory=null;
			}
		}
		/// <summary>
		/// Attach FDA3500A report
		/// </summary>
		/// <param name="p_objXmlIn">
		/// The structure of input XML document would be:
		/// <Document>
		///		<MedWatch>
		///			<EventId>Eventid</EventId>
		///		</MedWatch>
		///	</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// Output structure is as follows-:
		/// <Reports>
		///		<Report>Document id</Report>
		///</Reports>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool AttachedFDA3500AReport(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			Manager objManager=null;
			XmlNode objNode=null;
			string sFilenames="";
			string sUid="";
			
			try
			{
				objManager=new Manager(userLogin,connectionString, base.ClientId);//rkaur27
				objNode=p_objXmlIn.SelectSingleNode(@"/Document/MedWatch/EventId");
				objManager.GenerateReport(Manager.FdaReportType.FDA3500A,Conversion.ConvertStrToInteger(objNode.InnerText));
				sUid=GetFileName();
				objManager.PrintReportsCollection(sUid,out sFilenames);
				if (userLogin.objRiskmasterDatabase.DocPathType==0)
				{
					objManager.AttachReportsCollection(sUid,Conversion.ConvertStrToInteger(objNode.InnerText),"",StorageType.FileSystemStorage,userLogin.objRiskmasterDatabase.GlobalDocPath,out sFilenames);
				}
				else
				{
					objManager.AttachReportsCollection(sUid,Conversion.ConvertStrToInteger(objNode.InnerText),"",StorageType.DatabaseStorage,userLogin.objRiskmasterDatabase.GlobalDocPath,out sFilenames);
				}
				p_objXmlOut.LoadXml(sFilenames);
				objManager.DestroyReportsCollection(sUid);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MedWatchAdaptor.AttachedFDA3500AReport.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
                //Modified by SHivendu
                if (objManager != null)
                {
                    objManager.Dispose();
                    objManager = null;
                }
				objNode=null;
			}
		}
		/// <summary>
		/// Attach FDA3500 report
		/// </summary>
		/// <param name="p_objXmlIn">
		/// The structure of input XML document would be:
		/// <Document>
		///		<MedWatch>
		///			<EventId>Eventid</EventId>
		///		</MedWatch>
		///	</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// Output structure is as follows-:
		/// <Reports>
		///		<Report>Document id</Report>
		///</Reports>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool AttachedFDA3500Report(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			Manager objManager=null;
			XmlNode objNode=null;
			string sFilenames="";
			string sUid="";
			
			try
			{
				objManager=new Manager(userLogin,connectionString, base.ClientId);//rkaur27
				objNode=p_objXmlIn.SelectSingleNode(@"/Document/MedWatch/EventId");
				objManager.GenerateReport(Manager.FdaReportType.FDA3500,Conversion.ConvertStrToInteger(objNode.InnerText));
				sUid=GetFileName();
				objManager.PrintReportsCollection(sUid,out sFilenames);
				if (userLogin.objRiskmasterDatabase.DocPathType==0)
				{
					objManager.AttachReportsCollection(sUid,Conversion.ConvertStrToInteger(objNode.InnerText),"",StorageType.FileSystemStorage,userLogin.objRiskmasterDatabase.GlobalDocPath,out sFilenames);
				}
				else
				{
					objManager.AttachReportsCollection(sUid,Conversion.ConvertStrToInteger(objNode.InnerText),"",StorageType.DatabaseStorage,userLogin.objRiskmasterDatabase.GlobalDocPath,out sFilenames);
				}
				p_objXmlOut.LoadXml(sFilenames);
				objManager.DestroyReportsCollection(sUid);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MedWatchAdaptor.AttachedFDA3500Report.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
                //Modified by SHivendu
                if (objManager != null)
                {
                    objManager.Dispose();
                    objManager = null;
                }
				objNode=null;
			}
		}
		/// <summary>
		/// Get FDA3500A report
		/// </summary>
		/// <param name="p_objXmlIn">
		/// The structure of input XML document would be:
		/// <Document>
		///		<MedWatch>
		///			<EventId>Eventid</EventId>
		///		</MedWatch>
		///	</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// Output structure is as follows-:
		/// <Reports>
		///		<Report Filename=""></Report>
		///	 </Reports> 
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetFDA3500AReport(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			Manager objManager=null;
			XmlNode objNode=null;
			string sFilenames="";
			string sUid="";
			
			try
			{
				objManager=new Manager(userLogin,connectionString, base.ClientId);//rkaur27
				objNode=p_objXmlIn.SelectSingleNode(@"//MedWatch/EventId");
				objManager.GenerateReport(Manager.FdaReportType.FDA3500A,Conversion.ConvertStrToInteger(objNode.InnerText));
				sUid=GetFileName();
				objManager.PrintReportsCollection(sUid,out sFilenames);
				p_objXmlOut.LoadXml(sFilenames);
				objManager.DestroyReportsCollection(sUid);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MedWatchAdaptor.GetFDA3500AReport.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
                //Modified by SHivendu
                if (objManager != null)
                {
                    objManager.Dispose();
                    objManager = null;
                }
				objNode=null;
			}
		}
		/// <summary>
		/// Get FDA3500 report
		/// </summary>
		/// <param name="p_objXmlIn">
		/// The structure of input XML document would be:
		/// <Document>
		///		<MedWatch>
		///			<EventId>Eventid</EventId>
		///		</MedWatch>
		///	</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// Output structure is as follows-:
		/// <Reports>
		///		<Report Filename=""></Report>
		///	 </Reports> 
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetFDA3500Report(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			Manager objManager=null;
			XmlNode objNode=null;
			string sFilenames="";
			string sUid="";
			
			try
			{
				objManager=new Manager(userLogin,connectionString, base.ClientId);//rkaur27
				objNode=p_objXmlIn.SelectSingleNode(@"//MedWatch/EventId");
				objManager.GenerateReport(Manager.FdaReportType.FDA3500,Conversion.ConvertStrToInteger(objNode.InnerText));
				sUid=GetFileName();
				objManager.PrintReportsCollection(sUid,out sFilenames);
				p_objXmlOut.LoadXml(sFilenames);
				objManager.DestroyReportsCollection(sUid);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MedWatchAdaptor.GetFDA3500Report.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
                //Modified by SHivendu
                if (objManager != null)
                {
                    objManager.Dispose();
                    objManager = null;
                }
				objNode=null;
			}
		}
		#endregion

		#region Private Functions
		/// <summary>
		/// Generates a unique file Name 
		/// </summary>
		/// <param name="p_sFileName">Intiials of the file name </param>
		/// <param name="p_sFileExt">file extension</param>
		/// <returns>Complete file name</returns>	
		private string GetFileName()
		{
			string sFileName=string.Empty;	//for holding file name
			sFileName=DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + 
				DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() +
				DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
			return sFileName;
		}
		#endregion
	}
}
