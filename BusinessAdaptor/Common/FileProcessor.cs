using System;
using System.Collections;
using System.Xml;
using System.Xml.Schema;
using System.Xml.XPath;
using System.Text;
using System.IO;
using System.Web;
using System.Web.Services.Protocols;
using System.Web.Services.Description;
using System.Web.Services.Configuration;
using System.Xml.Serialization;
using System.Resources;

namespace Riskmaster.WebService.Common
{
	public class FileProcessor :  SoapExtension
	{
		// JP If we need HttpContext  -->>>    HttpContext httpctx = HttpContext.Current;

		protected ArrayList m_ExtractedFiles = null;

		// this GetInitializer is called when the SoapExtension is registered
		// in the config file (web/machine.config), it's called the first time
		// the class is invoked
		public override object GetInitializer(System.Type serviceType)
		{
			return null;
		}

		// this GetInitializer is called when the SoapExtension is configured
		// via a custom SoapExtensionAttribute, it's called the first time the
		// WebMethod is invoked
		public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute)
		{
			return null;
		}

		// called each a given [WebMethod] is used (will recieve a ValidationContext object)
		public override void Initialize(object initializer)
		{
		}

		protected Stream stmIncoming = null;
		protected Stream stmOutgoing = null;
		bool bPostDeserialize = false;

		public override Stream ChainStream(Stream stream)
		{
			if (!bPostDeserialize)
			{
				stmIncoming = stream;
				stmOutgoing = new MemoryStream();

				bPostDeserialize = true;

				return stmOutgoing;
			}
			else
				return stream;

		}

		// this will be called four times (before/after serialize/deserialize) for each [WebMethod] 
		public override void ProcessMessage(System.Web.Services.Protocols.SoapMessage message)
		{
			if (message.Stage == System.Web.Services.Protocols.SoapMessageStage.BeforeDeserialize)
			{
				try{
					MemoryStream stmOutput =null;
					try{stmOutput = this.Base64StreamManualScan(stmIncoming);}
					catch (Exception exp) 
					{
						SoapException error = new SoapException(
								"SoapExtension failure: " + exp.Message,
								SoapException.ClientFaultCode, 
								HttpContext.Current.Request.Url.AbsoluteUri,
								exp);
						throw error;
					}
					stmOutput.Flush();

					// Copy either modified (extracted) stream in or original stream if nothing happened (i.e. no image embedded)
					if (m_ExtractedFiles.Count > 0)
					{  // file was extracted
						stmOutput.WriteTo(stmOutgoing);
						stmOutput.Position = 0L;
						FileStream fs = new FileStream(System.IO.Path.GetTempPath() + "\\debug.xml",FileMode.Create);
						Riskmaster.Common.Utilities.CopyStream(stmOutput,fs);
						fs.Close();
					}
					else
					{   // no file extracted - play back original stream
						stmIncoming.Position = 0L;
						Riskmaster.Common.Utilities.CopyStream(stmIncoming, stmOutgoing);
					}

					// Commented out examples of throwing soap exception if we need it
//					XmlDocument errorDoc = new XmlDocument();
//					XmlElement detailElement = errorDoc.CreateElement(SoapException.DetailElementName.Name, SoapException.DetailElementName.Namespace);
//					errorDoc.AppendChild(detailElement);
//					XmlElement failedRulesElement = errorDoc.CreateElement("dm", "failedAssertions", Namespaces.DM);
//					detailElement.AppendChild(failedRulesElement);
//
//					// throw a SoapException with the detail element node we just built
//					SoapException error = new SoapException(
//						"Business rules failed validation",
//						SoapException.ClientFaultCode, 
//						HttpContext.Current.Request.Url.AbsoluteUri,
//						detailElement);
//					throw error;
				}
				finally
				{
					stmOutgoing.Position=0L;
				}
			}
		}

		public MemoryStream Base64StreamManualScan(Stream stmInput)
		{
			m_ExtractedFiles = new ArrayList();
			string sDiskUri ="";
			
			//Hook Up Input\Output Streams...
			StreamReader stmReader = new StreamReader(stmInput);
			MemoryStream stmResult = new MemoryStream();
			StreamWriter stmXmlWriter = new System.IO.StreamWriter(stmResult);
			FileStream stmTempFile = null;

			//Search for "xs:anyURI" in the stream.
			char[] buffSearchWindow;
			char[] buffBase64;
			char curChar;
			bool bIsInBase64=false;
			bool bBeginningBase64 = false;
			long iCurPos = 0L;
			long iPosBase64Start = 0L;
			long iPosBase64End=0L;
			
			while(stmReader.Peek()>=0)
			{
				iCurPos++;
				curChar=(char)stmReader.Read();
				
				if(curChar=='x' && !bIsInBase64)//Possible Start of "xs:anyURI" search string.
				{
					//Check for xs:anyURI
					buffSearchWindow = new char[9];
					buffSearchWindow[0]='x';

					if(8==stmReader.ReadBlock(buffSearchWindow,1,8))
						if(new String(buffSearchWindow)=="xs:anyURI")
							bBeginningBase64=true;

					stmReader.BaseStream.Position=iCurPos;
					stmReader.DiscardBufferedData();
				}
				else if(bBeginningBase64 && curChar=='>')
				{
					sDiskUri = System.IO.Path.GetTempFileName();
					stmXmlWriter.Write(curChar);
					stmXmlWriter.Write(sDiskUri);

					iPosBase64Start = iCurPos;
					bIsInBase64=true;
					bBeginningBase64=false;
				}
				else if(bIsInBase64 && curChar=='<')
				{
					iPosBase64End = iCurPos-1;
					stmReader.BaseStream.Position=iPosBase64Start;
					stmReader.DiscardBufferedData();

					buffBase64 = new char[iPosBase64End-iPosBase64Start];
					stmReader.Read(buffBase64,0,buffBase64.Length);
					stmReader.BaseStream.Position = iCurPos;
					stmReader.DiscardBufferedData();

					byte[] decodeData=Convert.FromBase64CharArray(buffBase64,0,buffBase64.Length);
					
					stmTempFile = new System.IO.FileStream(sDiskUri, System.IO.FileMode.Create);
					stmTempFile.Write(decodeData,0,decodeData.Length);
					stmTempFile.Close();
					m_ExtractedFiles.Add(sDiskUri);
					sDiskUri="";

					bIsInBase64=false;
				}
				
				if(!bIsInBase64)
					stmXmlWriter.Write(curChar);
			}//End While Scanning

			stmXmlWriter.Flush();
			return stmResult;
		}
	}

}
