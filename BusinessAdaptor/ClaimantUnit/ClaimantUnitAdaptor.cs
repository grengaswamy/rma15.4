using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.ClaimantUnit;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: ClaimantUnit.cs 
	///* $Revision	: 
	///* $Date		: 05/25/2014
	///* $Author	: Yukti Goyal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for getting Unit Type
	/// </summary>
  public	class ClaimantUnitAdaptor :  BusinessAdaptorBase
	{
        public bool GetUnitList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrorOut)
        {
            ClaimantUnit objClmntUnit = null;
            string sUnitType = string.Empty;
            try
            {
                objClmntUnit = new ClaimantUnit(connectionString,base.ClientId);
                p_objXmlOut = objClmntUnit.GetUnitInfo(p_objXmlIn);
                
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrorOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrorOut.Add(p_objException, Globalization.GetString("ClaimantUnitAdaptor.GetUnitList.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objClmntUnit = null;
            }

        }
	}
}