using System;
using System.Xml;

using Riskmaster.Application.TimeExpanse;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings ;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: TimeExpenseAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 01-Mar-2005
	///* $Author	: Mohit Yadav
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for Time And Expense
	///	which implements the functionality of Time And Expense Web Forms. 
	/// </summary>
	public class TimeExpenseAdaptor : BusinessAdaptorBase
	{
		
		#region Constructor
		/// <summary>
		/// Constructor for the class.
		/// </summary>
		public TimeExpenseAdaptor()
		{
		}
		#endregion

		#region Public Methods

		#region "GetXmlforInvoiceDetail(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)"
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.TimeExpanse.InvoiceDriver.GetXmlforInvoiceDetail() method.
		///		Gets the InvoiceDetail in XML format
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of XML document would be:
		///		<Document>
		///			<TimeExpense>
		///				<InputXml>
		///				The structure of Input XML would be like:
		///					<form name="frmData" title="Time and Expense Invoice" sid="150" topbuttons="1" actionname="invoice.asp" bcolor="white" headingtype="main">
		///						<body1 req_func="yes" func="setInvoicePage()"/>
		///						<group name="Invoice" title="Invoice">
		///							<error value="0" desc=""/>
		///							<control name="clmNumber" type="text" title="Claim number" value="GCTE512002" lock="true" visible="1"/>
		///							<control name="optallocate" type="checkbox" title="Allocated" value="1" visible="1" onclick="allocationSettings();" Disabled="1"/>
		///							<control name="vendor" type="textandlist" title="Vendor" value="Chen,Mary" isrequ="yes" buttonlink="vendor.asp" visible="1" lock="true" id="231" mid="0"/>
		///							<control name="invNumber" type="text" title="Invoice Number" visible="1" value="TE019"/>
		///							<control name="invAmt" type="text" title="Invoice Amount" visible="1" isrequ="yes" lock="true" value="100.00"/>
		///							<control title="Invoice Date" type="date" name="invDate" visible="1" isrequ="yes">05/01/2002</control>
		///							<control type="id" name="effsdate" visible="1" value="01/01/1991"/>
		///							<control type="id" name="effedate" visible="1" value="12/31/2002"/>
		///							<control type="id" name="action" value="" visible="1"/>
		///							<control type="id" name="h_id" value="" visible="1"/>
		///							<control type="id" name="h_trackid" value="" visible="1"/>
		///							<control type="id" name="h_maxtrackid" value="" visible="1"/>
		///							<control type="id" name="h_trackmode" value="" visible="1"/>
		///							<control type="id" name="mode" value="edit" visible="1"/>
		///							<control type="id" name="claimid" value="135" visible="1"/>
		///							<control type="id" name="h_available" value="" visible="1"/>
		///							<control type="id" name="h_billamt" value="" visible="1"/>
		///							<control type="id" name="h_billable" value="" visible="1"/>
		///							<control type="id" name="h_reservetype" value="" visible="1"/>
		///							<control type="id" name="h_reservetypecode" value="" visible="1"/>
		///							<control type="id" name="h_transtype" value="" visible="1"/>
		///							<control type="id" name="h_transtypecode" value="" visible="1"/>
		///							<control type="id" name="h_itemAmt" value="" visible="1"/>
		///							<control type="id" name="h_invnum" value="" visible="1"/>
		///							<control type="id" name="h_fromdate" value="" visible="1"/>
		///							<control type="id" name="h_todate" value="" visible="1"/>
		///							<control type="id" name="user" value="" visible="1"/>
		///							<control type="id" name="h_unit" value="" visible="1"/>
		///							<control type="id" name="h_rate" value="" visible="1"/>
		///							<control type="id" name="h_override" value="" visible="1"/>
		///							<control type="id" name="h_nonbillreason" value="" visible="1"/>
		///							<control type="id" name="h_nonbillreasoncode" value="" visible="1"/>
		///							<control type="id" name="h_comment" value="" visible="1"/>
		///							<control type="id" name="h_precomment" value="" visible="1"/>
		///							<control type="id" name="h_qty" value="" visible="1"/>
		///							<control type="id" name="h_pid" value="21" visible="1"/>
		///							<control type="id" name="h_reason" value="" visible="1"/>
		///							<control type="id" name="h_postable" value="" visible="1"/>
		///							<control type="id" name="h_allocated" value="1" visible="1"/>
		///							<control type="id" name="req_fields" value="vendor" visible="1"/>
		///							<control type="id" name="h_save" value="" visible="1"/>
		///							<control name="totinvoice" title="Invoiced" type="textlable" visible="1">Invoiced : 100.00</control>
		///							<control name="totbill" title="Billed" type="textlable" visible="1">Billed : 70.00</control>
		///							<control name="totavailable" title="Available : " type="text" visible="1" lock="true"/>
		///						</group>
		///						<group name="InvDetail" title="">
		///							<control title="Invoice Details" maxtrackid="2" isrequ="yes" name="invDetail" islink="yes" linkpage="invdetail.asp" type="invDetail" col1="Transaction Type" col2="invoice Amount" col3="billable" col4="Bill Amount" visible="1"><option bgcolor="data2" claimnum="GCTE512002" trackid="1" id="44" transtype="Film" transtypecode="3798" invamt="$100.00" billamt="$70.00" billable="True" reason="" reservetype="E Expense" reservetypecode="368" invtot="70.00" invnum="" fromdate="05/01/2002" todate="05/01/2002" unit="" rate="2.00" override="False" nonbillreason="" nonbillreasoncode="0" comment="" precomment="" qty="35" pid="21" postable="0" status="U"/></control>
		///							<control name="h_Invoiced" type="id" visible="1" value="100.00"/>
		///							<control name="h_Billed" type="id" visible="1" value="70.00"/>
		///							<control name="h_ratecode" type="id" visible="1" value="2"/>
		///						</group>
		///						<button linkto="Invoice.asp" param="" title="Save and Close" type="save" name="btnsave" disable="" visible="1" enablefornew="invoice"/>
		///						<button linkto="Invoice.asp" title="Cancel" name="btncancel" type="normal" disable="" visible="1" enablefornew="invoice"/>
		///					</form>
		///				</InputXml>
		///				<InvoiceDetailId>Invoice Detail Id</InvoiceDetailId>
		///				<RateCode>Rate Code</RateCode>
		///				<IsAllocated>Value for Allocated</IsAllocated>
		///			</TimeExpense>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<invdet>
		///			<maxtrackid value=""/>
		///			<ratecode value=""/>
		///			<isallocated value=""/>
		///			<trackid value=""/>
		///			<reason value=""/>
		///			<id value=""/>
		///			<transtype value=""/>
		///			<transtypecode value=""/>
		///			<invamt value=""/>
		///			<billamt value=""/>
		///			<billable value=""/>
		///			<reservetype value=""/>
		///			<reservetypecode value=""/>
		///			<invtot value=""/>
		///			<invnum value=""/>
		///			<fromdate value=""/>
		///			<todate value=""/>
		///			<unit value=""/>
		///			<rate value=""/>
		///			<override value=""/>
		///			<nonbillreason value=""/>
		///			<nonbillreasoncode value=""/>
		///			<comment value=""/>
		///			<precomment value=""/>
		///			<qty value=""/>
		///			<pid value=""/>
		///			<postable value=""/>
		///			<status value=""/>
		///		</invdet>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool GetXmlforInvoiceDetail(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.TimeExpanse.InvoiceDriver  objInvDriver=null; //Application layer component
			string sInputXml = string.Empty;			//Input XML passed in the input XML document
			long lInvoiceDetailId = 0;					//Invoice Detail Id passed in the input XML document
			long lRateCode = 0;							//Rate Code passed in the input XML document
			int iIsAllocated = 0;						//Value for Allocated passed in the input XML document

			XmlElement objElement=null;					//used for parsing the input xml

			try
			{
				//check existence of input xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//InputXml");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.GetXmlForInvoiceDetail.Error",Globalization.GetString("TimeExpenseAdaptor.InputXMLMissing", base.ClientId),BusinessAdaptorErrorType.Warning);
					return false;
				}
				sInputXml=objElement.InnerXml;

				//check existence of Invoice Detail Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//InvoiceDetailId");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.GetXmlForInvoiceDetail.Error",Globalization.GetString("TimeExpenseAdaptor.GetXmlForInvoiceDetail.InvDetIDMissing", base.ClientId),BusinessAdaptorErrorType.PopupMessage);
					return false;
				}
				lInvoiceDetailId=Conversion.ConvertStrToLong(objElement.InnerText);

				//check existence of Rate Code which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//RateCode");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.GetXmlForInvoiceDetail.Error",Globalization.GetString("TimeExpenseAdaptor.GetXmlForInvoiceDetail.RateCodeMissing", base.ClientId),BusinessAdaptorErrorType.PopupMessage);
					return false;
				}
				lRateCode=Conversion.ConvertStrToLong(objElement.InnerText);

				//check existence of Value for Allocated which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//IsAllocated");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.GetXmlForInvoiceDetail.Error",Globalization.GetString("TimeExpenseAdaptor.GetXmlForInvoiceDetail.AllocatedInfoMissing", base.ClientId),BusinessAdaptorErrorType.PopupMessage);
					return false;
				}
				iIsAllocated=Conversion.ConvertStrToInteger(objElement.InnerText);

				objInvDriver = new Riskmaster.Application.TimeExpanse.InvoiceDriver(userLogin.objRiskmasterDatabase.DataSourceName,userLogin.LoginName,userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);				

				p_objXmlOut=objInvDriver.GetXmlforInvoiceDetail(sInputXml, lInvoiceDetailId, lRateCode, iIsAllocated);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("TimeExpenseAdaptor.GetXmlForInvoiceDetail.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objInvDriver != null)
                {
                    objInvDriver.Dispose();                    
                }
				objElement=null;
			}
		}

		#endregion

		#region "GetXmlforRatetabledetail(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)"
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.TimeExpanse.InvoiceDriver.GetXmlforRatetabledetail() method.
		///		Gets the RatetableDetail as XML
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of XML document would be:
		///		<Document>
		///			<TimeExpense>
		///				<InputXml> The structure of Input XML document would be:
		///					<form name="frmData" title="Rate Table" sid="150" topbuttons="1" actionname="ratetable.asp" bcolor="white" headingtype="main">
		///						<body1 req_func="yes" func="setRatetable()"/>
		///						<group name="ratetable" title="Rate Table">
		///							<error value="0" desc=""/>
		///							<control name="rtname" type="text" title="Rate Table Name" isrequ="yes" visible="1" value="Admin" lock="true"/>
		///							<control name="customer" type="textandlist" title="Customer" isrequ="yes" visible="1" value="City of Oak Hills COMPANY" id="45" mid="2"/>
		///							<control type="id" name="tableid" value="2" visible="1"/>
		///							<control type="id" name="h_trackid" value="" visible="1"/>
		///							<control type="id" name="h_trackmode" value="" visible="1"/>
		///							<control type="id" name="h_maxtrackid" value="" visible="1"/>
		///							<control type="id" name="h_transtype" value="" visible="1"/>
		///							<control type="id" name="h_transtypecode" value="" visible="1"/>
		///							<control type="id" name="h_save" value="" visible="1"/>
		///							<control type="id" name="h_unit" value="" visible="1"/>
		///							<control type="id" name="h_unitcode" value="" visible="1"/>
		///							<control type="id" name="h_rate" value="" visible="1"/>
		///							<control type="id" name="h_id" value="" visible="1"/>
		///							<control type="id" name="mode" value="edit" visible="1"/>
		///							<control type="id" name="h_transchange" visible="1"/>
		///							<control type="id" name="req_fields" value="customer|rtname|exdate|efdate" visible="1"/>
		///							<control title="Effective Date" type="date" name="efdate" isrequ="yes" visible="1">01/01/1991</control>
		///							<control title="Expiration Date" type="date" name="exdate" isrequ="yes" visible="1">12/31/2002</control>
		///						</group>
		///						<group>
		///							<control title="Rate Details" maxtrackid="29" name="ratetable" islink="yes" isrequ="yes" linkpage="ratetabledetail.asp" type="ratetable" col1="Transaction Type" col2="Unit" col3="Rate" visible="1">
		///								<option bgcolor="data2" trackid="1" transtype="RCS Record Copying Services" transtypecode="2356" unit="Hour" rate="$65.00" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data1" trackid="2" transtype="AFRT Air Freight" transtypecode="3787" unit="Hour" rate="$85.00" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data2" trackid="3" transtype="AMS Account Manage Services" transtypecode="3788" unit="Hour" rate="$25.00" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data1" trackid="4" transtype="ART Artwork &amp; Displays" transtypecode="3790" unit="Hour" rate="$52.55" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data2" trackid="5" transtype="CAR Car Rental" transtypecode="3791" unit="Mile" rate="$0.75" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data1" trackid="6" transtype="CASS Cassettes" transtypecode="3792" unit="Each" rate="$5.25" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data2" trackid="7" transtype="CS Computer Service" transtypecode="3795" unit="Hour" rate="$55.00" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data1" trackid="8" transtype="EXP Express Mail" transtypecode="3796" unit="Each" rate="$4.28" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data2" trackid="9" transtype="FAX Facsimile Charges" transtypecode="3797" unit="Each" rate="$1.50" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data1" trackid="10" transtype="FILM Film" transtypecode="3798" unit="Each" rate="$2.00" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data2" trackid="11" transtype="FSF File Set-Up Fee" transtypecode="3800" unit="Hour" rate="$34.00" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data1" trackid="12" transtype="HOT Hotel" transtypecode="3801" unit="Each" rate="$75.00" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data2" trackid="13" transtype="MILE Mileage" transtypecode="3803" unit="Mile" rate="$0.85" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data1" trackid="14" transtype="O Other" transtypecode="3805" unit="Each" rate="$1.00" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data2" trackid="15" transtype="OS Other Services" transtypecode="3808" unit="Each" rate="$1.00" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data1" trackid="16" transtype="PARK Parking" transtypecode="3809" unit="Hour" rate="$1.50" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data2" trackid="17" transtype="PC Photocopies" transtypecode="3810" unit="Each" rate="$1.00" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data1" trackid="18" transtype="PHO Photos" transtypecode="3811" unit="Each" rate="$2.20" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data2" trackid="19" transtype="PME Photo Mounting Expense" transtypecode="3812" unit="Each" rate="$0.50" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data1" trackid="20" transtype="POST Postage" transtypecode="3813" unit="Each" rate="$0.77" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data2" trackid="21" transtype="PRN Printing" transtypecode="3815" unit="Each" rate="$25.00" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data1" trackid="22" transtype="SALV Salvage" transtypecode="3816" unit="Each" rate="$50.00" user="dtg" datetime="20020425140306" status="U"/>
		///								<option bgcolor="data2" trackid="23" transtype="STOR Storage Fees" transtypecode="3817" unit="Hour" rate="$2.00" user="dtg" datetime="20020425140307" status="U"/>
		///								<option bgcolor="data1" trackid="24" transtype="TOLL Tolls" transtypecode="3818" unit="Mile" rate="$0.25" user="dtg" datetime="20020425140307" status="U"/>
		///								<option bgcolor="data2" trackid="25" transtype="TELE Telephone" transtypecode="3819" unit="Each" rate="$0.50" user="dtg" datetime="20020425140307" status="U"/>
		///								<option bgcolor="data1" trackid="26" transtype="TEANS Transcription" transtypecode="3820" unit="Hour" rate="$0.58" user="dtg" datetime="20020425140307" status="U"/>
		///								<option bgcolor="data2" trackid="27" transtype="VID Video Charges" transtypecode="3821" unit="Each" rate="$9.00" user="dtg" datetime="20020425140307" status="U"/>
		///								<option bgcolor="data1" trackid="28" transtype="WR Weather Report" transtypecode="3823" unit="Each" rate="$0.85" user="dtg" datetime="20020425140307" status="U"/>
		///							</control>
		///						</group>
		///						<button linkto="ratetable.asp" param="" title="Save" type="save" name="btnsave" disable="" visible="1" enablefornew="ratetable"/>
		///						<button title="Back" name="btncancel" type="back" disable="" visible="1"/>
		///					</form>
		///				</InputXml>
		///				<RateId>Rate Id</RateId>
		///			</TimeExpense>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<ratetable>
		///			<maxtrackid value=""/> 
		///			<trackid value=""/>
		///			<transtype value=""/>
		///			<transtypecode value=""/>
		///			<unit value=""/>
		///			<rate value=""/>
		///			<user value=""/>
		///			<datetime value=""/>
		///			<status value=""/>
		///		</ratetable>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool GetXmlforRatetabledetail(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.TimeExpanse.InvoiceDriver  objInvDriver=null; //Application layer component
			string sInputXml = string.Empty;			//Input XML passed in the input XML document
			long lRateId = 0;							//Rate Id passed in the input XML document

			XmlElement objElement=null;					//used for parsing the input xml

			try
			{
				//check existence of input xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//InputXml");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.GetXmlForRateTableDetail.Error",Globalization.GetString("TimeExpenseAdaptor.InputXMLMissing", base.ClientId),BusinessAdaptorErrorType.Warning);
					return false;
				}
				sInputXml=objElement.InnerXml;

				//check existence of Rate Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//RateId");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.GetXmlForRateTableDetail.Error",Globalization.GetString("TimeExpenseAdaptor.GetXmlForRateTableDetail.RateIDMissing", base.ClientId),BusinessAdaptorErrorType.PopupMessage);
					return false;
				}
				lRateId=Conversion.ConvertStrToLong(objElement.InnerText);

				objInvDriver = new Riskmaster.Application.TimeExpanse.InvoiceDriver(userLogin.objRiskmasterDatabase.DataSourceName,userLogin.LoginName,userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);

				p_objXmlOut=objInvDriver.GetXmlforRatetabledetail(sInputXml, lRateId);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("TimeExpenseAdaptor.GetXmlForRateTableDetail.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objInvDriver != null)
                {
                    objInvDriver.Dispose();                    
                }
				objElement=null;
			}
		}
		#endregion

		#region "SaveInvoiceXml(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)"		
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.TimeExpanse.InvoiceDriver.SaveInvoiceXml() method.
		///		Saves the Invoice.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of XML document would be:
		///		<Document>
		///			<TimeExpense>
		///				<InputXml> The structure of Input XML document would be:
		///					<form name="frmData" title="Time and Expense Invoice" sid="150" topbuttons="1" actionname="invoice.asp" bcolor="white" headingtype="main">
		///						<body1 req_func="yes" func="setInvoicePage()"/>
		///						<group name="Invoice" title="Invoice">
		///							<error value="0" desc=""/>
		///							<control name="clmNumber" type="text" title="Claim number" value="GCTE512002" lock="true" visible="1"/>
		///							<control name="optallocate" type="checkbox" title="Allocated" value="1" visible="1" onclick="allocationSettings();" Disabled="1"/>
		///							<control name="vendor" type="textandlist" title="Vendor" value="Chen,Mary" isrequ="yes" buttonlink="vendor.asp" visible="1" lock="true" id="231" mid="0"/>
		///							<control name="invNumber" type="text" title="Invoice Number" visible="1" value="TE019"/>
		///							<control name="invAmt" type="text" title="Invoice Amount" visible="1" isrequ="yes" lock="true" value="100.00"/>
		///							<control title="Invoice Date" type="date" name="invDate" visible="1" isrequ="yes">05/01/2002</control>
		///							<control type="id" name="effsdate" visible="1" value="01/01/1991"/>
		///							<control type="id" name="effedate" visible="1" value="12/31/2002"/>
		///							<control type="id" name="action" value="" visible="1"/>
		///							<control type="id" name="h_id" value="" visible="1"/>
		///							<control type="id" name="h_trackid" value="undefined" visible="1"/>
		///							<control type="id" name="h_maxtrackid" value="2" visible="1"/>
		///							<control type="id" name="h_trackmode" value="" visible="1"/>
		///							<control type="id" name="mode" value="edit" visible="1"/>
		///							<control type="id" name="claimid" value="135" visible="1"/>
		///							<control type="id" name="h_available" value="" visible="1"/>
		///							<control type="id" name="h_billamt" value="" visible="1"/>
		///							<control type="id" name="h_billable" value="" visible="1"/>
		///							<control type="id" name="h_reservetype" value="" visible="1"/>
		///							<control type="id" name="h_reservetypecode" value="" visible="1"/>
		///							<control type="id" name="h_transtype" value="" visible="1"/>
		///							<control type="id" name="h_transtypecode" value="" visible="1"/>
		///							<control type="id" name="h_itemAmt" value="" visible="1"/>
		///							<control type="id" name="h_invnum" value="" visible="1"/>
		///							<control type="id" name="h_fromdate" value="" visible="1"/>
		///							<control type="id" name="h_todate" value="" visible="1"/>
		///							<control type="id" name="user" value="a" visible="1"/>
		///							<control type="id" name="h_unit" value="" visible="1"/>
		///							<control type="id" name="h_rate" value="" visible="1"/>
		///							<control type="id" name="h_override" value="" visible="1"/>
		///							<control type="id" name="h_nonbillreason" value="" visible="1"/>
		///							<control type="id" name="h_nonbillreasoncode" value="" visible="1"/>
		///							<control type="id" name="h_comment" value="" visible="1"/>
		///							<control type="id" name="h_precomment" value="" visible="1"/>
		///							<control type="id" name="h_qty" value="" visible="1"/>
		///							<control type="id" name="h_pid" value="21" visible="1"/>
		///							<control type="id" name="h_reason" value="" visible="1"/>
		///							<control type="id" name="h_postable" value="" visible="1"/>
		///							<control type="id" name="h_allocated" value="1" visible="1"/>
		///							<control type="id" name="req_fields" value="vendor" visible="1"/>
		///							<control type="id" name="h_save" value="" visible="1"/>
		///							<control name="totinvoice" title="Invoiced" type="textlable" visible="1">Invoiced : 0.00</control>
		///							<control name="totbill" title="Billed" type="textlable" visible="1">Billed : 0.00</control>
		///							<control name="totavailable" title="Available : " type="text" visible="1" lock="true"/>
		///						</group>
		///						<group name="InvDetail" title="">
		///							<control title="Invoice Details" maxtrackid="2" isrequ="yes" name="invDetail" islink="yes" linkpage="invdetail.asp" type="invDetail" col1="Transaction Type" col2="invoice Amount" col3="billable" col4="Bill Amount" visible="1"><option bgcolor="data2" claimnum="GCTE512002" trackid="1" id="44" transtype="Film" transtypecode="3798" invamt="$100.00" billamt="$70.00" billable="True" reason="" reservetype="E Expense" reservetypecode="368" invtot="70.00" invnum="" fromdate="05/01/2002" todate="05/01/2002" unit="" rate="2.00" override="False" nonbillreason="" nonbillreasoncode="0" comment="" precomment="" qty="35" pid="21" postable="0" status="U"/></control>
		///							<control name="h_Invoiced" type="id" visible="1" value="0.00"/>
		///							<control name="h_Billed" type="id" visible="1" value="0.00"/>
		///							<control name="h_ratecode" type="id" visible="1" value="2"/>
		///						</group>
		///						<button linkto="Invoice.asp" param="" title="Save and Close" type="save" name="btnsave" disable="" visible="1" enablefornew="invoice"/>
		///						<button linkto="Invoice.asp" title="Cancel" name="btncancel" type="normal" disable="" visible="1" enablefornew="invoice"/>
		///					</form>
		///				</InputXml>
		///				<AllowClosedClaim>Flag for AllowingClosedClaims</AllowClosedClaim>
		///			</TimeExpense>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<form name="frmData" title="Time and Expense Invoice" sid="150" topbuttons="1" actionname="invoice.asp" bcolor="white" headingtype="main">
		///			<body1 req_func="yes" func="setInvoicePage()" />
		///			<group name="Invoice" title="Invoice">
		///				<error value="0" desc="" />
		///				<control name="clmNumber" type="text" title="Claim number" value="GCTE512002" lock="true" visible="1" />
		///				<control name="optallocate" type="checkbox" title="Allocated" value="1" visible="1" onclick="allocationSettings();" Disabled="1" />
		///				<control name="vendor" type="textandlist" title="Vendor" value="Chen,Mary" isrequ="yes" buttonlink="vendor.asp" visible="1" lock="true" id="231" mid="0" />
		///				<control name="invNumber" type="text" title="Invoice Number" visible="1" value="TE019" />
		///				<control name="invAmt" type="text" title="Invoice Amount" visible="1" isrequ="yes" lock="true" value="100.00" />
		///				<control title="Invoice Date" type="date" name="invDate" visible="1" isrequ="yes">05/01/2002</control>
		///				<control type="id" name="effsdate" visible="1" value="01/01/1991" />
		///				<control type="id" name="effedate" visible="1" value="12/31/2002" />
		///				<control type="id" name="action" value="" visible="1" />
		///				<control type="id" name="h_id" value="" visible="1" />
		///				<control type="id" name="h_trackid" value="undefined" visible="1" />
		///				<control type="id" name="h_maxtrackid" value="2" visible="1" />
		///				<control type="id" name="h_trackmode" value="" visible="1" />
		///				<control type="id" name="mode" value="edit" visible="1" />
		///				<control type="id" name="claimid" value="200" visible="1" />
		///				<control type="id" name="h_available" value="" visible="1" />
		///				<control type="id" name="h_billamt" value="" visible="1" />
		///				<control type="id" name="h_billable" value="" visible="1" />
		///				<control type="id" name="h_reservetype" value="" visible="1" />
		///				<control type="id" name="h_reservetypecode" value="" visible="1" />
		///				<control type="id" name="h_transtype" value="" visible="1" />
		///				<control type="id" name="h_transtypecode" value="" visible="1" />
		///				<control type="id" name="h_itemAmt" value="" visible="1" />
		///				<control type="id" name="h_invnum" value="" visible="1" />
		///				<control type="id" name="h_fromdate" value="" visible="1" />
		///				<control type="id" name="h_todate" value="" visible="1" />
		///				<control type="id" name="user" value="a" visible="1" />
		///				<control type="id" name="h_unit" value="" visible="1" />
		///				<control type="id" name="h_rate" value="" visible="1" />
		///				<control type="id" name="h_override" value="" visible="1" />
		///				<control type="id" name="h_nonbillreason" value="" visible="1" />
		///				<control type="id" name="h_nonbillreasoncode" value="" visible="1" />
		///				<control type="id" name="h_comment" value="" visible="1" />
		///				<control type="id" name="h_precomment" value="" visible="1" />
		///				<control type="id" name="h_qty" value="" visible="1" />
		///				<control type="id" name="h_pid" value="21" visible="1" />
		///				<control type="id" name="h_reason" value="" visible="1" />
		///				<control type="id" name="h_postable" value="" visible="1" />
		///				<control type="id" name="h_allocated" value="1" visible="1" />
		///				<control type="id" name="req_fields" value="vendor" visible="1" />
		///				<control type="id" name="h_save" value="" visible="1" />
		///				<control name="totinvoice" title="Invoiced" type="textlable" visible="1">Invoiced : 0.00</control>
		///				<control name="totbill" title="Billed" type="textlable" visible="1">Billed : 0.00</control>
		///				<control name="totavailable" title="Available : " type="text" visible="1" lock="true" />
		///			</group>
		///			<group name="InvDetail" title="">
		///				<control title="Invoice Details" maxtrackid="2" isrequ="yes" name="invDetail" islink="yes" linkpage="invdetail.asp" type="invDetail" col1="Transaction Type" col2="invoice Amount" col3="billable" col4="Bill Amount" visible="1">
		///					<option bgcolor="data2" claimnum="GCTE512002" trackid="1" id="44" transtype="Film" transtypecode="3798" invamt="$100.00" billamt="$70.00" billable="True" reason="" reservetype="E Expense" reservetypecode="368" invtot="70.00" invnum="" fromdate="05/01/2002" todate="05/01/2002" unit="" rate="2.00" override="False" nonbillreason="" nonbillreasoncode="0" comment="" precomment="" qty="35" pid="21" postable="0" status="U" />
		///				</control>
		///				<control name="h_Invoiced" type="id" visible="1" value="0.00" />
		///				<control name="h_Billed" type="id" visible="1" value="0.00" />
		///				<control name="h_ratecode" type="id" visible="1" value="2" />
		///			</group>
		///			<button linkto="Invoice.asp" param="" title="Save and Close" type="save" name="btnsave" disable="" visible="1" enablefornew="invoice" />
		///			<button linkto="Invoice.asp" title="Cancel" name="btncancel" type="normal" disable="" visible="1" enablefornew="invoice" />
		///		</form>
		///	</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool SaveInvoiceXml(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.TimeExpanse.InvoiceDriver  objInvDriver=null; //Application layer component
			string sInputXml = string.Empty;			//Input XML passed in the input XML document
			bool bAllowClosedClaim =false;				//Flag for AllowingClosedClaims passed in the input XML document

			XmlElement objElement=null;					//used for parsing the input xml

			try
			{
				//check existence of input xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//InputXml");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.SaveInvoiceXml.Error",Globalization.GetString("TimeExpenseAdaptor.InputXMLMissing", base.ClientId),BusinessAdaptorErrorType.Warning);
					return false;
				}
				sInputXml=objElement.InnerXml;

				//check existence of AllowingClosedClaims which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//AllowClosedClaim");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.SaveInvoiceXml.Error",Globalization.GetString("TimeExpenseAdaptor.SaveInvoiceXml.ClaimStatusMissing", base.ClientId),BusinessAdaptorErrorType.PopupMessage);
					return false;
				}
				
				bAllowClosedClaim=Convert.ToBoolean(objElement.InnerText);

				objInvDriver = new Riskmaster.Application.TimeExpanse.InvoiceDriver(userLogin.objRiskmasterDatabase.DataSourceName,userLogin.LoginName,userLogin.Password, userLogin.UserId, userLogin.GroupId,base.ClientId);

				p_objXmlOut=objInvDriver.SaveInvoiceXml(sInputXml, bAllowClosedClaim);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("TimeExpenseAdaptor.SaveInvoiceXml.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objInvDriver != null)
                {
                    objInvDriver.Dispose();                    
                }
				objElement=null;
			}
		}
		#endregion

		#region "SaveRateTableXml(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)"
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.TimeExpanse.InvoiceDriver.SaveRateTableXml() method.
		///		Saves a RateTable into database.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of XML document would be:
		///		<Document>
		///			<TimeExpense>
		///				<InputXml> Input XML containing the Ratetable details. The structure of Input XML document would be:
		///					<form name="frmData" title="Rate Table" sid="150" topbuttons="1" actionname="ratetable.asp" bcolor="white" headingtype="main">
		///						<body1 req_func="yes" func="setRatetable()"/>
		///						<group name="ratetable" title="Rate Table">
		///							<error value="0" desc=""/>
		///							<control name="rtname" type="text" title="Rate Table Name" isrequ="yes" visible="1" value="west234last333"/>
		///							<control name="customer" type="textandlist" title="Customer" isrequ="yes" visible="1" value="OH-City of Oak Hills COMPANY" id="45" mid="2"/>
		///							<control type="id" name="tableid" value="0" visible="1"/>
		///							<control type="id" name="h_trackid" value="1" visible="1"/>
		///							<control type="id" name="h_trackmode" value="" visible="1"/>
		///							<control type="id" name="h_maxtrackid" value="2" visible="1"/>
		///							<control type="id" name="h_transtype" value="" visible="1"/>
		///							<control type="id" name="h_transtypecode" value="" visible="1"/>
		///							<control type="id" name="h_save" value="" visible="1"/>
		///							<control type="id" name="h_unit" value="" visible="1"/>
		///							<control type="id" name="h_unitcode" value="" visible="1"/>
		///							<control type="id" name="h_rate" value="" visible="1"/>
		///							<control type="id" name="h_id" value="" visible="1"/>
		///							<control type="id" name="mode" value="new" visible="1"/>
		///							<control type="id" name="h_transchange" visible="1"/>
		///							<control type="id" name="req_fields" value="customer|rtname|exdate|efdate" visible="1"/>
		///							<control title="Effective Date" type="date" name="efdate" isrequ="yes" visible="1">12/10/2004</control>
		///							<control title="Expiration Date" type="date" name="exdate" isrequ="yes" visible="1">12/21/2004</control>
		///						</group>
		///						<group>
		///							<control title="Rate Details" maxtrackid="1" name="ratetable" islink="yes" isrequ="yes" linkpage="ratetabledetail.asp" type="ratetable" col1="Transaction Type" col2="Unit" col3="Rate" visible="1">
		///								<option trackid="1" maxtrackid="2" transtype="asdasd" transtypecode="" unit="HOUR Hour" rate="21312" status="N"/>
		///							</control>
		///						</group>
		///						<button linkto="ratetable.asp" param="" title="Save" type="save" name="btnsave" disable="" visible="1" enablefornew="ratetable"/>
		///						<button title="Back" name="btncancel" type="back" disable="" visible="1"/>
		///					</form>
		///				</InputXml>
		///			</TimeExpense>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would possibly be
		///		<form name="frmData" title="Rate Table" sid="150" topbuttons="1" actionname="ratetable.asp" bcolor="white" headingtype="main"><body1 req_func="yes" func="setRatetable()" />
		///			<group name="ratetable" title="Rate Table">
		///				<error value="0" desc="" />
		///				<control name="rtname" type="text" title="Rate Table Name" isrequ="yes" visible="1" value="west234lastpopo" />
		///				<control name="customer" type="textandlist" title="Customer" isrequ="yes" visible="1" value="OH-City of Oak Hills COMPANY" id="45" mid="2" />
		///				<control type="id" name="tableid" value="0" visible="1" />
		///				<control type="id" name="h_trackid" value="1" visible="1" />
		///				<control type="id" name="h_trackmode" value="" visible="1" />
		///				<control type="id" name="h_maxtrackid" value="2" visible="1" />
		///				<control type="id" name="h_transtype" value="" visible="1" />
		///				<control type="id" name="h_transtypecode" value="" visible="1" />
		///				<control type="id" name="h_save" value="" visible="1" />
		///				<control type="id" name="h_unit" value="" visible="1" />
		///				<control type="id" name="h_unitcode" value="" visible="1" />
		///				<control type="id" name="h_rate" value="" visible="1" />
		///				<control type="id" name="h_id" value="" visible="1" />
		///				<control type="id" name="mode" value="new" visible="1" />
		///				<control type="id" name="h_transchange" visible="1" />
		///				<control type="id" name="req_fields" value="customer|rtname|exdate|efdate" visible="1" />
		///				<control title="Effective Date" type="date" name="efdate" isrequ="yes" visible="1">12/10/2004</control>
		///				<control title="Expiration Date" type="date" name="exdate" isrequ="yes" visible="1">12/21/2004</control>
		///			</group>
		///			<group>
		///				<control title="Rate Details" maxtrackid="1" name="ratetable" islink="yes" isrequ="yes" linkpage="ratetabledetail.asp" type="ratetable" col1="Transaction Type" col2="Unit" col3="Rate" visible="1">
		///					<option trackid="1" maxtrackid="2" transtype="asdasd" transtypecode="" unit="HOUR Hour" rate="21312" status="N" />
		///				</control>
		///			</group>
		///			<button linkto="ratetable.asp" param="" title="Save" type="save" name="btnsave" disable="" visible="1" enablefornew="ratetable" />
		///			<button title="Back" name="btncancel" type="back" disable="" visible="1" />
		///		</form>
		///	</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool SaveRateTableXml(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.TimeExpanse.InvoiceDriver  objInvDriver=null; //Application layer component
			string sInputXml = string.Empty;			//Input XML passed in the input XML document
			
			XmlElement objElement=null;					//used for parsing the input xml

			try
			{
				//check existence of input xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//InputXml");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.SaveRateTableXml.Error",Globalization.GetString("TimeExpenseAdaptor.InputXMLMissing", base.ClientId),BusinessAdaptorErrorType.Warning);
					return false;
				}
				sInputXml=objElement.InnerXml;

				objInvDriver = new Riskmaster.Application.TimeExpanse.InvoiceDriver(userLogin.objRiskmasterDatabase.DataSourceName,userLogin.LoginName,userLogin.Password, userLogin.UserId, userLogin.GroupId,base.ClientId);

				p_objXmlOut=objInvDriver.SaveRateTableXml(sInputXml);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("TimeExpenseAdaptor.SaveRateTableXml.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objInvDriver != null)
                {
                    objInvDriver.Dispose();                    
                }
				objElement=null;
			}
		}
		#endregion

		#region "PrintHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)"
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.TimeExpanse.InvoiceDriver.PrintHistory() method.
		///		Fetches the data in XML format for printing History for the claim Id passed
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<TimeExpense>
		///				<ClaimId>Claim Id</ClaimId>
		///			</TimeExpense>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">
		/// The structure of the output Xml would be:
		///		<print name="" tdate="" city1="" phone1="" fax1="" title="" billtotal="" invtotal="">
		///			<option vendor="" invdate="" invnumber="" invamnt="" billedamnt="" checkstatus="" checknumber="" checkdate="" />
		///		</print>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		 
		public bool PrintHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.TimeExpanse.InvoiceDriver  objInvDriver=null; //Application layer component
			int iClaimId = 0;			//Claim Id passed in the input XML document
			
			XmlElement objElement=null;					//used for parsing the input xml

			try
			{
				//check existence of input xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ClaimId");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.PrintHistory.Error",Globalization.GetString("TimeExpenseAdaptor.PrintHistory.ClaimIDMissing", base.ClientId),BusinessAdaptorErrorType.Warning);
					return false;
				}
				iClaimId=Conversion.ConvertStrToInteger(objElement.InnerXml);

                objInvDriver = new Riskmaster.Application.TimeExpanse.InvoiceDriver(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);

				p_objXmlOut=objInvDriver.PrintHistory(iClaimId);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("TimeExpenseAdaptor.PrintHistory.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objInvDriver != null)
                {
                    objInvDriver.Dispose();                    
                }
				objElement=null;
			}
		}

		#endregion

		#region "PrintReport(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)"
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.TimeExpanse.InvoiceDriver.PrintReport() method.
		///		Fetches the data in XML format for printing Report for the invoice Id passed
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<TimeExpense>
		///				<InvoiceId>Invoice Id</InvoiceId>
		///			</TimeExpense>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">
		/// The structure of the output XML would be:
		///		<print name="" tdate="" city1="" phone1="" fax1="" title="" DateBilled="" ClaimNumber="" Claimant="">
		///			<option invdate="" typecode="" comment="" qty="" unit="" rate="" total="" />
		///		</print> 
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		 
		public bool PrintReport(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.TimeExpanse.InvoiceDriver  objInvDriver=null; //Application layer component
			int iInvoiceId = 0;			//Invoice Id passed in the input XML document
			
			XmlElement objElement=null;					//used for parsing the input xml

			try
			{
				//check existence of input xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//InvoiceId");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.PrintReport.Error",Globalization.GetString("TimeExpenseAdaptor.PrintReport.InvoiceIDMissing", base.ClientId),BusinessAdaptorErrorType.Warning);
					return false;
				}
				iInvoiceId=Conversion.ConvertStrToInteger(objElement.InnerXml);

                objInvDriver = new Riskmaster.Application.TimeExpanse.InvoiceDriver(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);

				p_objXmlOut=objInvDriver.PrintReport(iInvoiceId);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("TimeExpenseAdaptor.PrintReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objInvDriver != null)
                {
                    objInvDriver.Dispose();                    
                }
				objElement=null;
			}
		}

		#endregion

		#region "VoidIt(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)"
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.TimeExpanse.InvoiceDriver.VoidIt() method.
		///		Voids the Invoices.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<TimeExpense>
		///				<InvoiceIds>InvoiceIds Comma seperated</InvoiceIds>
		///				<IsContinue>In case of any error continue or not</IsContinue>
		///				<Status>Status</Status>
		///			</TimeExpense>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Null</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		 
		public bool VoidIt(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.TimeExpanse.InvoiceDriver  objInvDriver=null; //Application layer component
			string sInvoiceIds = string.Empty;			//InvoiceIds Comma seperated passed in the input XML document
			bool bIsContinue = false;					//In case of any error continue or not passed in the input XML document
			string sStatus = string.Empty;				//Status passed in the input XML document
			
			XmlElement objElement=null;					//used for parsing the input xml

			try
			{
				//check existence of InvoiceIds Comma seperated which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//InvoiceIds");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.VoidIt.Error",Globalization.GetString("TimeExpenseAdaptor.VoidIt.InvoiceIDsMissing", base.ClientId),BusinessAdaptorErrorType.Warning);
					return false;
				}
				sInvoiceIds=objElement.InnerXml;

				//check in case of any error continue or not which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//IsContinue");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.VoidIt.Error",Globalization.GetString("TimeExpenseAdaptor.VoidIt.ContinueInfoMissing", base.ClientId),BusinessAdaptorErrorType.PopupMessage);
					return false;
				}
				bIsContinue=Convert.ToBoolean(objElement.InnerXml);

				//check existence of Status which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Status");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.VoidIt.Error",Globalization.GetString("TimeExpenseAdaptor.VoidIt.StatusMissing", base.ClientId),BusinessAdaptorErrorType.PopupMessage);
					return false;
				}
				sStatus=objElement.InnerXml;

				objInvDriver = new Riskmaster.Application.TimeExpanse.InvoiceDriver(userLogin.objRiskmasterDatabase.DataSourceName,userLogin.LoginName,userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);

				p_objXmlOut=objInvDriver.VoidIt(sInvoiceIds, bIsContinue, sStatus);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("TimeExpenseAdaptor.VoidIt.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objInvDriver != null)
                {
                    objInvDriver.Dispose();                    
                }
				objElement=null;
			}
		}

		#endregion

		#region "BillIt(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)" 
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.TimeExpanse.InvoiceDriver.BillIt() method.
		///		Bill the invoices.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<TimeExpense>
		///				<InvoiceIds>InvoiceIds Comma seperated</InvoiceIds>
		///				<IsContinue>In case of any error continue or not</IsContinue>
		///				<AllowClosedClaim>Flag for allowing closed claims</AllowClosedClaim>
		///			</TimeExpense>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Null</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool BillIt(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.TimeExpanse.InvoiceDriver  objInvDriver=null; //Application layer component
			string sInvoiceIds = string.Empty;			//InvoiceIds Comma seperated passed in the input XML document
			bool bIsContinue = false;					//In case of any error continue or not passed in the input XML document
			bool bAllowClosedClaim = false;				//Flag for allowing closed claims passed in the input XML document
			
			XmlElement objElement=null;					//used for parsing the input xml

			try
			{
				//check existence of InvoiceIds Comma seperated which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//InvoiceIds");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.BillIt.Error",Globalization.GetString("TimeExpenseAdaptor.BillIt.InvoiceIDsMissing", base.ClientId),BusinessAdaptorErrorType.Warning);
					return false;
				}
				sInvoiceIds=objElement.InnerXml;

				//check In case of any error continue or not which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//IsContinue");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.BillIt.Error",Globalization.GetString("TimeExpenseAdaptor.BillIt.ContinueInfoMissing", base.ClientId),BusinessAdaptorErrorType.PopupMessage);
					return false;
				}
				bIsContinue=Convert.ToBoolean(objElement.InnerXml);

				//check existence of Flag for allowing closed claims which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//AllowClosedClaim");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.BillIt.Error",Globalization.GetString("TimeExpenseAdaptor.BillIt.ClaimsStatusMissing", base.ClientId),BusinessAdaptorErrorType.PopupMessage);
					return false;
				}
				bAllowClosedClaim=Convert.ToBoolean(objElement.InnerXml);

				objInvDriver = new Riskmaster.Application.TimeExpanse.InvoiceDriver(userLogin.objRiskmasterDatabase.DataSourceName,userLogin.LoginName,userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);

				p_objXmlOut=objInvDriver.BillIt(sInvoiceIds, bIsContinue, bAllowClosedClaim);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("TimeExpenseAdaptor.BillIt.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objInvDriver != null)
                {                    
                    objInvDriver.Dispose();
                }
				objElement=null;
			}
		}

		#endregion

		#region "GetXmlData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)"
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.TimeExpanse.InvoiceDriver.GetXmlData() method.
		///		Returns the data XML for the Formname passed.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<TimeExpense>
		///				<FrmName>Name of the form</FrmName>
		///				<InputXml>Input empty XML
		///				The structure of the input Xml would be:
		///				<TandE/>
		///				<form name="frmData" title="Time and Expense Invoice" sid="150" topbuttons="1" actionname="invoice.asp" bcolor="white" headingtype="main">
		///					<body1 req_func="yes" func="setInvoicePage()"/>
		///						<group name="Invoice" title="Invoice">
		///							<error value="0" desc=""/>
		///							<control name="clmNumber" type="text" title="Claim number" value="" lock="true" visible="1"/>
		///							<control name="optallocate" type="checkbox" title="Allocated" value="1" visible="1" onclick="allocationSettings();"/>
		///							<control name="vendor" type="textandlist" title="Vendor" value="" isrequ="yes" buttonlink="vendor.asp" visible="1"/>
		///							<control name="invNumber" type="text" title="Invoice Number" visible="1" value=""/>
		///							<control name="invAmt" type="text" title="Invoice Amount" visible="1" isrequ="yes"/>
		///							<control title="Invoice Date" type="date" name="invDate" visible="1" isrequ="yes"/>
		///							<control type="id" name="effsdate" visible="1"/>
		///							<control type="id" name="effedate" visible="1"/>
		///							<control type="id" name="action" value="" visible="1"/>
		///							<control type="id" name="h_id" value="" visible="1"/>
		///							<control type="id" name="h_trackid" value="" visible="1"/>
		///							<control type="id" name="h_maxtrackid" value="" visible="1"/>
		///							<control type="id" name="h_trackmode" value="" visible="1"/>
		///							<control type="id" name="mode" value="" visible="1"/>
		///							<control type="id" name="claimid" value="" visible="1"/>
		///							<control type="id" name="h_available" value="" visible="1"/>
		///							<control type="id" name="h_billamt" value="" visible="1"/>
		///							<control type="id" name="h_billable" value="" visible="1"/>
		///							<control type="id" name="h_reservetype" value="" visible="1"/>
		///							<control type="id" name="h_reservetypecode" value="" visible="1"/>
		///							<control type="id" name="h_transtype" value="" visible="1"/>
		///							<control type="id" name="h_transtypecode" value="" visible="1"/>
		///							<control type="id" name="h_itemAmt" value="" visible="1"/>
		///							<control type="id" name="h_invnum" value="" visible="1"/>
		///							<control type="id" name="h_fromdate" value="" visible="1"/>
		///							<control type="id" name="h_todate" value="" visible="1"/>
		///							<control type="id" name="user" value="" visible="1"/>
		///							<control type="id" name="h_unit" value="" visible="1"/>
		///							<control type="id" name="h_rate" value="" visible="1"/>
		///							<control type="id" name="h_override" value="" visible="1"/>
		///							<control type="id" name="h_nonbillreason" value="" visible="1"/>
		///							<control type="id" name="h_nonbillreasoncode" value="" visible="1"/>
		///							<control type="id" name="h_comment" value="" visible="1"/>
		///							<control type="id" name="h_precomment" value="" visible="1"/>
		///							<control type="id" name="h_qty" value="" visible="1"/>
		///							<control type="id" name="h_pid" value="" visible="1"/>
		///							<control type="id" name="h_reason" value="" visible="1"/>
		///							<control type="id" name="h_postable" value="" visible="1"/>
		///							<control type="id" name="h_allocated" value="" visible="1"/>
		///							<control type="id" name="req_fields" value="vendor" visible="1"/>
		///							<control type="id" name="h_save" value="" visible="1"/>
		///							<control name="totinvoice" title="Invoiced" type="textlable" visible="1"/>
		///							<control name="totbill" title="Billed" type="textlable" visible="1"/>
		///							<control name="totavailable" title="Available : " type="text" visible="1" lock="true"/>
		///							<rateandunit></rateandunit>
		///						</group>
		///						<group name="InvDetail" title="">
		///							<control title="Invoice Details" maxtrackid="" isrequ="yes" name="invDetail" islink="yes" linkpage="invdetail.asp" type="invDetail" col1="Transaction Type" col2="invoice Amount" col3="billable" col4="Bill Amount" visible="1"/>
		///							<control name="h_Invoiced" type="id" visible="1" value="0"/>
		///							<control name="h_Billed" type="id" visible="1"/>
		///							<control name="h_ratecode" type="id" visible="1"/>
		///						</group>
		///						<button linkto="Invoice.asp" param="" title="Save and Close" type="save" name="btnsave" disable="" visible="1" enablefornew="invoice"/>
		///						<button linkto="Invoice.asp" title="Cancel" name="btncancel" type="normal" disable="" visible="1" enablefornew="invoice"/>
		///					</form>
		///				</InputXml>
		///				<TableId1>ID variant, its value depend on the basis of formname passed</TableId1>
		///				<TableId2>ID variant, its value depend on the basis of formname passed</TableId2>
		///				<TableId3>ID variant, its value depend on the basis of formname passed</TableId3>
		///			</TimeExpense>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">
		/// The structure for the output Xml document would be:
		///		<form name="frmData" title="Time and Expense Invoice" sid="150" topbuttons="1" actionname="invoice.asp" bcolor="white" headingtype="main">
		///			<body1 req_func="yes" func="setInvoicePage()" />
		///				<group name="Invoice" title="Invoice">
		///					<error value="0" desc="" />
		///					<control name="clmNumber" type="text" title="Claim number" value="GCTE512002" lock="true" visible="1" />
		///					<control name="optallocate" type="checkbox" title="Allocated" value="0" visible="1" onclick="allocationSettings();" Disabled="1" />
		///					<control name="vendor" type="textandlist" title="Vendor" value="Physician,Polly" isrequ="yes" buttonlink="vendor.asp" visible="1" lock="true" id="175" mid="0" />
		///					<control name="invNumber" type="text" title="Invoice Number" visible="0" value="TE020" />
		///					<control name="invAmt" type="text" title="Invoice Amount" visible="0" isrequ="yes" lock="true" value="$100.00" />
		///					<control title="Date" type="date" name="invDate" visible="1" isrequ="yes">20020501</control>
		///					<control type="id" name="effsdate" visible="1" value="19910101" />
		///					<control type="id" name="effedate" visible="1" value="20021231" />
		///					<control type="id" name="action" value="" visible="1" />
		///					<control type="id" name="h_id" value="" visible="1" />
		///					<control type="id" name="h_trackid" value="" visible="1" />
		///					<control type="id" name="h_maxtrackid" value="" visible="1" />
		///					<control type="id" name="h_trackmode" value="" visible="1" />
		///					<control type="id" name="mode" value="edit" visible="1" />
		///					<control type="id" name="claimid" value="135" visible="1" />
		///					<control type="id" name="h_available" value="" visible="1" />
		///					<control type="id" name="h_billamt" value="" visible="1" />
		///					<control type="id" name="h_billable" value="" visible="1" />
		///					<control type="id" name="h_reservetype" value="" visible="1" />
		///					<control type="id" name="h_reservetypecode" value="" visible="1" />
		///					<control type="id" name="h_transtype" value="" visible="1" />
		///					<control type="id" name="h_transtypecode" value="" visible="1" />
		///					<control type="id" name="h_itemAmt" value="" visible="1" />
		///					<control type="id" name="h_invnum" value="" visible="1" />
		///					<control type="id" name="h_fromdate" value="" visible="1" />
		///					<control type="id" name="h_todate" value="" visible="1" />
		///					<control type="id" name="user" value="" visible="1" />
		///					<control type="id" name="h_unit" value="" visible="1" />
		///					<control type="id" name="h_rate" value="" visible="1" />
		///					<control type="id" name="h_override" value="" visible="1" />
		///					<control type="id" name="h_nonbillreason" value="" visible="1" />
		///					<control type="id" name="h_nonbillreasoncode" value="" visible="1" />
		///					<control type="id" name="h_comment" value="" visible="1" />
		///					<control type="id" name="h_precomment" value="" visible="1" />
		///					<control type="id" name="h_qty" value="" visible="1" />
		///					<control type="id" name="h_pid" value="22" visible="1" />
		///					<control type="id" name="h_reason" value="" visible="1" />
		///					<control type="id" name="h_postable" value="" visible="1" />
		///					<control type="id" name="h_allocated" value="0" visible="1" />
		///					<control type="id" name="req_fields" value="vendor" visible="1" />
		///					<control type="id" name="h_save" value="" visible="1" />
		///					<control name="totinvoice" title="Invoiced" type="textlable" visible="0">Invoiced : $100.00</control>
		///					<control name="totbill" title="Billed" type="textlable" visible="1">Billed : $75.00</control>
		///					<control name="totavailable" title="Available : " type="text" visible="0" lock="true" />
		///				</group>
		///				<group name="InvDetail" title="">
		///					<control title="Invoice Details" maxtrackid="2" isrequ="yes" name="invDetail" islink="yes" linkpage="invdetail.asp" type="invDetail" col1="Transaction Type" col2="invoice Amount" col3="billable" col4="Bill Amount" visible="1">
		///						<option bgcolor="data2" claimnum="GCTE512002" trackid="1" id="57" transtype="Hotel" transtypecode="3801" invamt="$100.00" billamt="$0.00" billable="False" reason="" reservetype="E Expense" reservetypecode="368" invtot="0" invnum="" fromdate="20020501" todate="" unit="" rate="0" override="False" nonbillreason="&quot; &quot;" nonbillreasoncode="0" comment="" precomment="" qty="1" pid="22" postable="0" status="U" />
		///					</control>
		///					<control name="h_Invoiced" type="id" visible="1" value="$100.00" />
		///					<control name="h_Billed" type="id" visible="1" value="$75.00" />
		///					<control name="h_ratecode" type="id" visible="1" value="2" />
		///				</group>
		///				<button linkto="Invoice.asp" param="" title="Save and Close" type="save" name="btnsave" disable="" visible="1" enablefornew="invoice" />
		///				<button linkto="Invoice.asp" title="Cancel" name="btncancel" type="normal" disable="" visible="1" enablefornew="invoice" />
		///			</form>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		 
		public bool GetXmlData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.TimeExpanse.InvoiceDriver  objInvDriver=null; //Application layer component
			string sFrmName = string.Empty;				//Name of the form passed in the input XML document
			string sInputXml = string.Empty;			//Input Xml passed in the input XML document
			int iTableId1 = 0;							//ID variant passed in the input XML document
			int iTableId2 = 0;							//ID variant passed in the input XML document
			int iTableId3 = 0;							//ID variant passed in the input XML document
            //Arnab: MITS-10682 - Added sLookUpText to get the Lookup Text as search criteria
            string sLookUpText = string.Empty;          //Lookup text for code id as search criteria
			int iCustomerId = 0;

			XmlElement objElement=null;					//used for parsing the input xml
            //rupal:multicurrency
            int iPmtCurrCode = 0;
            string claimNumber = string.Empty;
			try
			{
				//check existence of Name of the form which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FrmName");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.GetXmlData.Error",Globalization.GetString("TimeExpenseAdaptor.GetXmlData.FormNameMissing", base.ClientId),BusinessAdaptorErrorType.Warning);
					return false;
				}
				sFrmName=objElement.InnerXml;

				//check existence of input xml which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//InputXml");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.GetXmlData.Error",Globalization.GetString("TimeExpenseAdaptor.GetXmlData.InputXMLMissing", base.ClientId),BusinessAdaptorErrorType.Warning);
					return false;
				}
				if(sFrmName == "ratetable")
				{
					this.GetCustomerFieldData(p_objXmlIn,ref p_objXmlIn,ref p_objErrOut) ;
				}
				sInputXml=objElement.InnerXml;

				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//CustomerId");
				if (objElement==null)
				{
					iCustomerId=0;
				}
				else
				{
					iCustomerId=Conversion.ConvertStrToInteger(objElement.InnerText);
				}

				//check existence of ID variant
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TableId1");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.GetXmlData.Error",Globalization.GetString("TimeExpenseAdaptor.GetXmlData.TableIDMissing", base.ClientId),BusinessAdaptorErrorType.PopupMessage);
					return false;
				}
				iTableId1=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of ID variant
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TableId2");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.GetXmlData.Error",Globalization.GetString("TimeExpenseAdaptor.GetXmlData.TableIDMissing", base.ClientId),BusinessAdaptorErrorType.PopupMessage);
					return false;
				}
				iTableId2=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of ID variant
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TableId3");
				if (objElement==null)
				{
					p_objErrOut.Add("TimeExpenseAdaptor.GetXmlData.Error",Globalization.GetString("TimeExpenseAdaptor.GetXmlData.TableIDMissing", base.ClientId),BusinessAdaptorErrorType.PopupMessage);
					return false;
				}
				
				
				iTableId3=Conversion.ConvertStrToInteger(objElement.InnerText);

                //Arnab: MITS-10682 - Search for the existance of Lookup Text
                //check existence of Lookup Text                
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//LookUpText");
                if (objElement != null)                    
                    sLookUpText = objElement.InnerXml.ToString();

                //akaushik5 Added for RMA-19193 Starts
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimNumber");
                if (objElement != null)
                    claimNumber = objElement.InnerXml.ToString();
                //akaushik5 Added for RMA-19193 Ends
                objInvDriver = new Riskmaster.Application.TimeExpanse.InvoiceDriver(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);

                //Arnab: MITS-10682 - Modified mehtod calling as method signature is modified in InvoiceDriver.cs
                //p_objXmlOut=objInvDriver.GetXmlData(sFrmName, sInputXml, iTableId1, iTableId2, iTableId3,iCustomerId);
                /*
                //rupal:multicurrency
                //check existence of PmtCurrencyCodeId
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PmtCurrCodeId");
                if (objElement != null)
                {
                    iPmtCurrCode = Conversion.ConvertStrToInteger(objElement.InnerText);
                }                
                */
                //akaushik5 Changed for RMA-19193 Starts
                //p_objXmlOut = objInvDriver.GetXmlData(sFrmName, sInputXml, iTableId1, iTableId2, iTableId3, sLookUpText, iCustomerId);
                p_objXmlOut = objInvDriver.GetXmlData(sFrmName, sInputXml, iTableId1, iTableId2, iTableId3, sLookUpText, iCustomerId,claimNumber);
                //akaushik5 Changed for RMA-19193 Ends
                //rupal:end

                //MITS-10682 - End
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("TimeExpenseAdaptor.GetXmlData.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objInvDriver != null)
                {
                    objInvDriver.Dispose();                    
                }
				objElement=null;
			}
		}

        #endregion

        //Function added by Nadim for r4ps2 build stability
        public bool GetXmlDataAndDate(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            InvoiceDriver objInvDriver = null; //Application layer component

            string sInputXml = string.Empty;
            int iRateTableId = 0;
            string sRatetableid = string.Empty;
            bool bFirstTime = false;      //pmittal5 Mits 14066 01/06/09
            XmlElement objElement = null; //used for parsing the input xml
            int iClaimId = 0; //rupal
            bool bSuccess = false;

            try
            {
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//RateTableid");

                if (objElement != null)
                {
                    iRateTableId = Conversion.ConvertStrToInteger(objElement.InnerText);
                }

                //pmittal5 Mits 14066 01/06/09
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//FirstTime");

                if (objElement != null)
                {
                    bFirstTime = Conversion.ConvertObjToBool(objElement.InnerText, base.ClientId);
                }

                //rupal:start, r8 multicurrency
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimId");

                if (objElement != null)
                {
                    iClaimId = Conversion.CastToType<int>(objElement.InnerText, out bSuccess);
                }
                //rupal:end

                objInvDriver = new InvoiceDriver(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);
                //p_objXmlOut = objInvDriver.GetXmlDataAndDate(iRateTableId);
                //rupal:start, r8 multicurrency
                //p_objXmlOut = objInvDriver.GetXmlDataAndDate(iRateTableId, bFirstTime);
                p_objXmlOut = objInvDriver.GetXmlDataAndDate(iRateTableId, bFirstTime, iClaimId);
                //rupal:end
                //End - pmittal5
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("TimeExpenseAdaptor.GetXmlData.Error",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objInvDriver != null)
                {
                    objInvDriver.Dispose();
                }
                objElement = null;
            }
        }
        //Function added by Nadim for r4ps2 build stability-end

        // Anjaneya MITS 12347 
        /// <summary>
        /// Updates the claim table for Rate Table Id so that it comes auto selected when user moves to T&E Summary screen
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool UpdateRateTableForClaim(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Riskmaster.Application.TimeExpanse.InvoiceDriver objInvDriver = null; //Application layer component
            int iTableId = 0;			//Table ID passed in the input XML document
            string sClaimId = string.Empty;
            XmlElement objElement = null;					//used for parsing the input xml

            try
            {
                //check existence of system table name which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//RateTableId");
                if (objElement == null)
                {
                    p_objErrOut.Add("TimeExpenseAdaptor.GetXmlData.Error", Globalization.GetString("TimeExpenseAdaptor.DelRateTable.TableIDMissing", base.ClientId), BusinessAdaptorErrorType.Warning);
                    return false;
                }
                iTableId = Convert.ToInt32(objElement.InnerText);
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimId");
                if (objElement == null)
                {
                    p_objErrOut.Add("TimeExpenseAdaptor.GetXmlData.Error", Globalization.GetString("TimeExpenseAdaptor.DelRateTable.Error", base.ClientId), BusinessAdaptorErrorType.Warning);
                    return false;
                }
                sClaimId = objElement.InnerText;
                objInvDriver = new Riskmaster.Application.TimeExpanse.InvoiceDriver(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);

                //delete table
                objInvDriver.UpdateRateTableForClaim(iTableId, sClaimId);

                p_objXmlOut = new XmlDocument();

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("InvoiceDriver.SaveRateTableXml.ErrorSave", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objInvDriver != null)
                {
                    objInvDriver.Dispose();
                }
                objElement = null;
            }
        }

		#region "DelRateTable(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)"
				/// <summary>
				///		This method is a wrapper to Riskmaster.Application.TimeExpanse.InvoiceDriver.DelRateTable() method.
				///		Deletes the entries of Rate Table
				/// </summary>
				/// <param name="p_objXmlIn">XML containing the input parameters needed
				///		The structure of input XML document would be:
				///		<Document>
				///			<TimeExpense>
				///				<TableId>Table ID</TableId>
				///			</TimeExpense>
				///		</Document>
				/// </param>
				/// <param name="p_objXmlOut">Null</param>
				/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
				/// <returns>True/False for success or failure of the function</returns>
				
				public bool DelRateTable(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
				{
					Riskmaster.Application.TimeExpanse.InvoiceDriver  objInvDriver=null; //Application layer component
					int iTableId = 0;			//Table ID passed in the input XML document
					
					XmlElement objElement=null;					//used for parsing the input xml
		
					try
					{
						//check existence of system table name which is required
						objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TableId");
						if (objElement==null)
						{
							p_objErrOut.Add("TimeExpenseAdaptor.DelRateTable.Error",Globalization.GetString("TimeExpenseAdaptor.DelRateTable.TableIDMissing", base.ClientId),BusinessAdaptorErrorType.Warning);
							return false;
						}
						iTableId=Convert.ToInt32(objElement.InnerText);
		
						objInvDriver = new Riskmaster.Application.TimeExpanse.InvoiceDriver(userLogin.objRiskmasterDatabase.DataSourceName,userLogin.LoginName,userLogin.Password, userLogin.UserId, userLogin.GroupId,base.ClientId);
		
						//delete table
						objInvDriver.DelRateTable(iTableId);

						p_objXmlOut = new XmlDocument();
		
						return true;
					}
					catch(RMAppException p_objException)
					{
						p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
						return false;
					}
					catch(Exception p_objException)
					{
						p_objErrOut.Add(p_objException,Globalization.GetString("TimeExpenseAdaptor.DelRateTable.Error", base.ClientId), BusinessAdaptorErrorType.Error);
						return false;
					}
					finally
					{
                        if (objInvDriver != null)
                        {
                            objInvDriver.Dispose();                            
                        }
						objElement=null;
					}
				}

		#endregion
		public bool GetCustomerFieldData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			SysSettings objSettings = null; 
			int iLob = 0; 
			try
			{
                objSettings = new SysSettings(m_connectionString, base.ClientId); //Ash - cloud
				switch (objSettings.RateLevel)
				{
					case 1005 :
						iLob = 1;
						break;
					case 1006 :
						iLob = 2;
						break;
					case 1007 :
						iLob = 3;
						break;
					case 1008 :
						iLob = 4;
						break;
					case 1009 :
						iLob = 5;
						break;
					case 1010 :
						iLob = 6;
						break;
					case 1011 :
						iLob = 7;
						break;
					case 1012 :
						iLob = 8;
						break;
					default:
                        iLob=9;
                      break;
                         
				}
				p_objXmlIn.SelectSingleNode("//control[@name='CustomerFieldData']").InnerText = iLob.ToString() ;
				p_objXmlOut = p_objXmlIn;
				return true ;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("TimeExpenseAdaptor.GetCustomerFieldData.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				
			}
        }

        #endregion
    }
}
