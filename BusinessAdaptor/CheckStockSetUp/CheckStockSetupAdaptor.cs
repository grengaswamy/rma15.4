using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Application.CheckStockSetup;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using System.IO;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: CheckStockSetupService.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 02-Feb-2005
	///* $Author	: Nikhil Kr. Garg
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	///* Amendment  -->
	///  1.Date		: 7 Feb 06 
	///    Desc		: Replacing RMO_ACCESS use, and changing params of permission
	///				  violation exception so it can be correctly called. 
	///    Author	: Sumit
	///**************************************************************
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for Check Stock Setup which 
	///	contains methods to configure the way check look and the information on them.	
	/// </summary>
	public class CheckStockSetupAdaptor: BusinessAdaptorBase
	{
		private const int RMB_FUNDS_BNKACCT = 9800;
		private const int RMO_FUNDS_BNKACCT_SUPP = 50;
		private const int RMO_FUNDS_BNKACCT_CHKSTOCK = 150;
		private const int RMO_FUNDS_BNKACCT_BALANCE = 500;
		private const int RMO_FUNDS_BNKACCT_SUBACT = 750;
		private const int RMB_FUNDS_BNKACCT_CHKSTOCK = 9950;
		//private const int RMO_ACCESS = 0;
		#region Constructor
		/// <summary>
		/// Constructor for the class.
		/// </summary>
		public CheckStockSetupAdaptor()
		{
		}
		#endregion

		#region Public Methods

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.CheckStockSetup.CheckStockSetup.GetCheckStockDetails() method.
		///		This method will return the XML containing the setup related details for a selected check stock. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<CheckStockSetup>
		///				<StockId>StockId</StockId>
		///			</CheckStockSetup>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<CheckStock>
		///			<StockId /><AccountId /><StockName /><FontName /><FontSize /><MICRFlag /><MICRSequence /> 
		///			<MICRType /><MICRFont /><MICRFontSize /><RXLaserFlag /><SoftForm /><RXLaserForm /> 
		///			<RXVoidForm /><RXSpecialForm /><SpecialFlag /><SpecialAmount /><DateX /><DateY /> 
		///			<CheckNumberX /><CheckNumberY /><CheckAmountX /><CheckAmountY /><CheckAmountTextX /> 
		///			<CheckAmountTextY /><ClaimNumberOffsetX /><ClaimNumberOffsetY /><MemoX /><MemoY /> 
		///			<CheckDtlX /><CheckDtlY /><CheckPayerX /><CheckPayerY /><CheckPayeeX /><CheckPayeeY /> 
		///			<CheckOffsetX /><CheckOffsetY /><CheckMICRX /><CheckMICRY /><SecondOffsetX /><SecondOffsetY /> 
		///			<Unit /><DollarSign /><PrintAddress /><PrintTaxIDOnStub /><PrintAdjNamePhoneOnStub /> 
		///			<PreNumStock /><PrintMemo /><PrntEventOnStub /><PrintInsurer /><PrintClaimNumberOnCheck /> 
		///			<PrintCtlNumberOnStub /><PayerLevel /><MemoFontName /><MemoFontSize /><MemoRows /> 
		///			<MemoRowLength /><AddressFlag /><CheckLayout />
		///			<CheckStockForms>
		///				<CheckStockForm>
		///					<StockId /><ImageIdx /><FormType />
		///					<FormSource>contains data in base 64 encoded format</FormSource>
		///				</CheckStockForm>
		///			</CheckStockForms>
		///		</CheckStock> 
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool GetCheckStockDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.CheckStockSetup.CheckStockSetup objCheckStockSetup=null; //Application layer component
			int iStockId=0;							//StockId passed in the input XML document
			XmlElement objElement=null;				//used for parsing the input xml

			try
			{
				//check existence of stockid which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//StockId");
				if (objElement==null)
				{
					p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing",Globalization.GetString("CheckStockSetupAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
					return false;
				}
				iStockId=Conversion.ConvertStrToInteger(objElement.InnerText);

                using (objCheckStockSetup = new Riskmaster.Application.CheckStockSetup.CheckStockSetup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId))//rkaur27
                {

                    //get stock details
                    p_objXmlOut = objCheckStockSetup.GetCheckStockDetails(iStockId);
                }
				
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("CheckStockSetupAdaptor.GetCheckStockDetails.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objCheckStockSetup = null;
				objElement=null;
			}
		}


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.CheckStockSetup.CheckStockSetup.NewCheckStockDetails() method.
		///		This method will add a new check stock. 		
		///		The details for the new check stock will be fetched from the XML string passed to the method.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<CheckStockSetup>
		///				<CheckStockDetail>Check Stock Details</CheckStockDetail>
		///				Note:
		///				<FormSource>
		///					contains the file path given by orbeon after uploading the file to the server 
		///					e.g. file:/C:\Program Files\Apache Software Foundation\Tomcat 5.5\temp/csccheck.emf
		///				</FormSource>
		///			</CheckStockSetup>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<CheckStock>
		///			<StockId /><AccountId /><StockName /><FontName /><FontSize /><MICRFlag /><MICRSequence /> 
		///			<MICRType /><MICRFont /><MICRFontSize /><RXLaserFlag /><SoftForm /><RXLaserForm /> 
		///			<RXVoidForm /><RXSpecialForm /><SpecialFlag /><SpecialAmount /><DateX /><DateY /> 
		///			<CheckNumberX /><CheckNumberY /><CheckAmountX /><CheckAmountY /><CheckAmountTextX /> 
		///			<CheckAmountTextY /><ClaimNumberOffsetX /><ClaimNumberOffsetY /><MemoX /><MemoY /> 
		///			<CheckDtlX /><CheckDtlY /><CheckPayerX /><CheckPayerY /><CheckPayeeX /><CheckPayeeY /> 
		///			<CheckOffsetX /><CheckOffsetY /><CheckMICRX /><CheckMICRY /><SecondOffsetX /><SecondOffsetY /> 
		///			<Unit /><DollarSign /><PrintAddress /><PrintTaxIDOnStub /><PrintAdjNamePhoneOnStub /> 
		///			<PreNumStock /><PrintMemo /><PrntEventOnStub /><PrintInsurer /><PrintClaimNumberOnCheck /> 
		///			<PrintCtlNumberOnStub /><PayerLevel /><MemoFontName /><MemoFontSize /><MemoRows /> 
		///			<MemoRowLength /><AddressFlag /><CheckLayout />
		///			<CheckStockForms>
		///				<CheckStockForm>
		///					<StockId /><ImageIdx /><FormType />
		///					<FormSource>contains data in base 64 encoded format</FormSource>
		///				</CheckStockForm>
		///			</CheckStockForms>
		///		</CheckStock> 
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool NewCheckStockDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.CheckStockSetup.CheckStockSetup objCheckStockSetup=null; //Application layer component
			string sCheckStockDetail=string.Empty;	//Stock details passed in the input XML document
			XmlElement objElement=null;				//used for parsing the input xml

			try
			{
				//check existence of stock details which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//CheckStockDetail");
				if (objElement==null)
				{
					p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing",Globalization.GetString("CheckStockSetupAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
					return false;
				}
				sCheckStockDetail=objElement.InnerXml;

                using (objCheckStockSetup = new Riskmaster.Application.CheckStockSetup.CheckStockSetup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId))//rkaur27
                {
                    // add the check stock details 
                    p_objXmlOut = objCheckStockSetup.NewCheckStockDetails(sCheckStockDetail);
                }
				
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("CheckStockSetupAdaptor.NewCheckStockDetails.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objCheckStockSetup = null;
				objElement=null;
			}
		}


		//Changes made for MITS 7358 by Gagan : Start
		
		/// <summary>
		///	This method gets invoked when user clicks on New button in toolbar
		///	It checks for permissions to create a new check stock
		/// </summary>
		/// <param name="p_objXmlIn"></param>
		/// <param name="p_objXmlOut"></param>
		/// <param name="p_objErrOut">Collection of Errors Messages</param>		
		/// <returns></returns>
		public bool NewCheckStock(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
            //Changed by Saurabh P Arora for MITS 16780: Start 
            XmlElement elemCheckStock = null;
            XmlElement elemUnit = null;
            string sUnitValue = string.Empty;
            //Changed by Saurabh P Arora for MITS 16780: End

			try
			{
				if (!userLogin.IsAllowedEx( RMB_FUNDS_BNKACCT_CHKSTOCK, RMPermissions.RMO_CREATE))					
					throw new PermissionViolationException(RMPermissions.RMO_CREATE,RMB_FUNDS_BNKACCT_CHKSTOCK);
                
                //Changed by Saurabh P Arora for MITS 16780: Start 
                elemCheckStock = p_objXmlOut.CreateElement("CheckStock");
                p_objXmlOut.AppendChild(elemCheckStock);
                //dvatsa - added connection string and client iD
                sUnitValue = RMConfigurationManager.GetDictionarySectionSettings("CheckStock", connectionString, base.ClientId)["CheckStockUnits"].ToString();
                if (sUnitValue == null)
                    sUnitValue = "";

                elemUnit = p_objXmlOut.CreateElement("Unit");
                if (!(sUnitValue.ToLower() == "false"))
                    elemUnit.InnerText = "Inches";
                else
                    elemUnit.InnerText = "Centimeters";

                elemCheckStock.AppendChild(elemUnit);
                //Changed by Saurabh P Arora for MITS 16780: End
				
                return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("CheckStockSetupAdaptor.MoveAndGetStockDetails.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}			
		}


		//Changes made for MITS 7358 by Gagan : End



		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.CheckStockSetup.CheckStockSetup.UpdateCheckStockDetails() method.
		///		This method will update setup related details for an existing check stock. 		
		///		The details will be fetched from the XML string passed to the method.
		/// </summary>
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<CheckStockSetup>
		///				<StockId>StockId</StockId>
		///				<CheckStockDetail>Check Stock Details</CheckStockDetail>
		///				Note:
		///				<FormSource>
		///					contains the file path given by orbeon after uploading the file to the server 
		///					e.g. file:/C:\Program Files\Apache Software Foundation\Tomcat 5.5\temp/csccheck.emf
		///				</FormSource>
		///			</CheckStockSetup>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<CheckStock>
		///			<StockId /><AccountId /><StockName /><FontName /><FontSize /><MICRFlag /><MICRSequence /> 
		///			<MICRType /><MICRFont /><MICRFontSize /><RXLaserFlag /><SoftForm /><RXLaserForm /> 
		///			<RXVoidForm /><RXSpecialForm /><SpecialFlag /><SpecialAmount /><DateX /><DateY /> 
		///			<CheckNumberX /><CheckNumberY /><CheckAmountX /><CheckAmountY /><CheckAmountTextX /> 
		///			<CheckAmountTextY /><ClaimNumberOffsetX /><ClaimNumberOffsetY /><MemoX /><MemoY /> 
		///			<CheckDtlX /><CheckDtlY /><CheckPayerX /><CheckPayerY /><CheckPayeeX /><CheckPayeeY /> 
		///			<CheckOffsetX /><CheckOffsetY /><CheckMICRX /><CheckMICRY /><SecondOffsetX /><SecondOffsetY /> 
		///			<Unit /><DollarSign /><PrintAddress /><PrintTaxIDOnStub /><PrintAdjNamePhoneOnStub /> 
		///			<PreNumStock /><PrintMemo /><PrntEventOnStub /><PrintInsurer /><PrintClaimNumberOnCheck /> 
		///			<PrintCtlNumberOnStub /><PayerLevel /><MemoFontName /><MemoFontSize /><MemoRows /> 
		///			<MemoRowLength /><AddressFlag /><CheckLayout />
		///			<CheckStockForms>
		///				<CheckStockForm>
		///					<StockId /><ImageIdx /><FormType />
		///					<FormSource>contains data in base 64 encoded format</FormSource>
		///				</CheckStockForm>
		///			</CheckStockForms>
		///		</CheckStock> 
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool UpdateCheckStockDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.CheckStockSetup.CheckStockSetup objCheckStockSetup=null; //Application layer component
			int iStockId=0;							//Stock Id passed in the input XML document
			string sCheckStockDetail=string.Empty;	//Stock details passed in the input XML document
			XmlElement objElement=null;				//used for parsing the input xml

			try
			{            
				//Changes made for MITS 7358 by Gagan : Start
				if (!userLogin.IsAllowedEx( RMB_FUNDS_BNKACCT_CHKSTOCK , RMPermissions.RMO_UPDATE))
					throw new PermissionViolationException(RMPermissions.RMO_UPDATE,RMB_FUNDS_BNKACCT_CHKSTOCK);
				//Changes made for MITS 7358 by Gagan : End

				//check existence of stock details which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//CheckStockDetail");
				if (objElement==null)
				{
					p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing",Globalization.GetString("CheckStockSetupAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
					return false;
				}
				sCheckStockDetail=objElement.InnerXml;

				//check existence of stockid which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//StockId");
				if (objElement==null)
				{
					p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing",Globalization.GetString("CheckStockSetupAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
					return false;
				}
				iStockId=Conversion.ConvertStrToInteger(objElement.InnerText);

                using (objCheckStockSetup = new Riskmaster.Application.CheckStockSetup.CheckStockSetup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId))//rkaur27
                {

                    // update the stock details
                    p_objXmlOut = objCheckStockSetup.UpdateCheckStockDetails(iStockId, sCheckStockDetail);
                }

				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("CheckStockSetupAdaptor.UpdateCheckStockDetails.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objCheckStockSetup = null;
				objElement=null;
			}
		}


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.CheckStockSetup.CheckStockSetup.PrintCheckStockSample() method.
		///		This method will generate the sample for the selected check stock.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<CheckStockSetup>
		///				<StockId>StockId</StockId>
		///			</CheckStockSetup>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of output XML document would be:
		///		<checkstocksetup>
		///			<file name="name of file">
		///				data for the file in Base64 encoding
		///			</file>
		///		</checkstocksetup>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool PrintCheckStockSample(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
            Riskmaster.Application.CheckStockSetup.CheckStockSetup objCheckStockSetup = null; //Application layer component
			int iStockId=0;						//Stock Id passed in the input XML document
            //ashish Ahuja
            string sSampleText = string.Empty;
			XmlElement objElement=null;			//used for parsing the input xml
			XmlElement objOutElement=null;		//used for creating the Output Xml
			MemoryStream objReturnValue=null;	//Return value from the Application layer 

			try
			{
				//check existence of stockid which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//StockId");
				if (objElement==null)
				{
					p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing",Globalization.GetString("CheckStockSetupAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
					return false;
				}
				iStockId=Conversion.ConvertStrToInteger(objElement.InnerText);

                //Ashish Ahuja - Pay To The Order Config starts
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//SampleText");
                if (objElement == null)
                {
                    p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing", Globalization.GetString("CheckStockSetupAdaptorParameterMissing", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sSampleText = objElement.InnerText;

                //Ashish Ahuja - Pay To The Order Config ends
                using (objCheckStockSetup = new Riskmaster.Application.CheckStockSetup.CheckStockSetup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId))
                {
                    //get the check stock file
                    objReturnValue = objCheckStockSetup.PrintCheckStockSample(iStockId,sSampleText);
                }

				//create the output xml document containing the data in Base64 format
				p_objXmlOut = new XmlDocument(); 
				objOutElement=p_objXmlOut.CreateElement("checkstocksetup");
				p_objXmlOut.AppendChild(objOutElement);
				objOutElement=p_objXmlOut.CreateElement("file");
				objOutElement.SetAttribute("name",GetFileName("CheckStock","pdf"));
				objOutElement.InnerText=Convert.ToBase64String(objReturnValue.ToArray());
				p_objXmlOut.FirstChild.AppendChild(objOutElement);

				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("CheckStockSetupAdaptor.PrintCheckStockSample.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objCheckStockSetup = null;
				objElement=null;
			}
		}


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.CheckStockSetup.CheckStockSetup.CloneCheckStockDetails() method.
		///		This method will create a copy of the current check stock to use for 
		///		any existing bank account i.e. cloning.	
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<CheckStockSetup>
		///				<StockId>StockId</StockId>
		///				<AccountId>AccountId</AccountId>
		///				<CurBankAccount>Current Bank Account Flag</CurBankAccount>
		///			</CheckStockSetup>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<CheckStock>
		///			<StockId /><AccountId /><StockName /><FontName /><FontSize /><MICRFlag /><MICRSequence /> 
		///			<MICRType /><MICRFont /><MICRFontSize /><RXLaserFlag /><SoftForm /><RXLaserForm /> 
		///			<RXVoidForm /><RXSpecialForm /><SpecialFlag /><SpecialAmount /><DateX /><DateY /> 
		///			<CheckNumberX /><CheckNumberY /><CheckAmountX /><CheckAmountY /><CheckAmountTextX /> 
		///			<CheckAmountTextY /><ClaimNumberOffsetX /><ClaimNumberOffsetY /><MemoX /><MemoY /> 
		///			<CheckDtlX /><CheckDtlY /><CheckPayerX /><CheckPayerY /><CheckPayeeX /><CheckPayeeY /> 
		///			<CheckOffsetX /><CheckOffsetY /><CheckMICRX /><CheckMICRY /><SecondOffsetX /><SecondOffsetY /> 
		///			<Unit /><DollarSign /><PrintAddress /><PrintTaxIDOnStub /><PrintAdjNamePhoneOnStub /> 
		///			<PreNumStock /><PrintMemo /><PrntEventOnStub /><PrintInsurer /><PrintClaimNumberOnCheck /> 
		///			<PrintCtlNumberOnStub /><PayerLevel /><MemoFontName /><MemoFontSize /><MemoRows /> 
		///			<MemoRowLength /><AddressFlag /><CheckLayout />
		///			<CheckStockForms>
		///				<CheckStockForm>
		///					<StockId /><ImageIdx /><FormType />
		///					<FormSource>contains data in base 64 encoded format</FormSource>
		///				</CheckStockForm>
		///			</CheckStockForms>
		///		</CheckStock> 
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool CloneCheckStockDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.CheckStockSetup.CheckStockSetup objCheckStockSetup=null; //Application layer component
			int iStockId=0;						//Stock Id passed in the input XML document
			int iAccountId=0;					//Account Id passed in the input XML document
			bool bCurBankAccount=false;			//Current Bank Account flag passed in the input XML document
			XmlElement objElement=null;			//used for parsing the input xml

			try
			{
				//check existence of stock Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//StockId");
				if (objElement==null)
				{
					p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing",Globalization.GetString("CheckStockSetupAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
					return false;
				}
				iStockId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of Account Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//AccountId");
				if (objElement==null)
				{
					p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing",Globalization.GetString("CheckStockSetupAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
					return false;
				}
				iAccountId=Conversion.ConvertStrToInteger(objElement.InnerText);
				
				//check existence of Current Bank Account flag which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//CurBankAccount");
				if (objElement==null)
				{
					p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing",Globalization.GetString("CheckStockSetupAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
					return false;
				}
				bCurBankAccount=Conversion.ConvertStrToBool(objElement.InnerText);

                using (objCheckStockSetup = new Riskmaster.Application.CheckStockSetup.CheckStockSetup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId))
                {
                    //get the cloned stock details
                    p_objXmlOut = objCheckStockSetup.CloneCheckStockDetails(iStockId, bCurBankAccount, iAccountId);
                }

				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("CheckStockSetupAdaptor.CloneCheckStockDetails.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objCheckStockSetup = null;
				objElement=null;
			}
		}


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.CheckStockSetup.CheckStockSetup.DeleteStockDetails() method.
		///		This method will delete details related to a specific check stock.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<CheckStockSetup>
		///				<StockId>StockId</StockId>
		///			</CheckStockSetup>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Null</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool DeleteStockDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.CheckStockSetup.CheckStockSetup objCheckStockSetup=null; //Application layer component
			int iStockId=0;					//Stock Id passed in the input XML document
			XmlElement objElement=null;		//used for parsing the input xml

			try
			{
				//Changes made for MITS 7358 by Gagan : Start
				if (!userLogin.IsAllowedEx( RMB_FUNDS_BNKACCT_CHKSTOCK , RMPermissions.RMO_DELETE))
					throw new PermissionViolationException(RMPermissions.RMO_DELETE ,RMB_FUNDS_BNKACCT_CHKSTOCK);
				//Changes made for MITS 7358 by Gagan : End

				//check existence of stock Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//StockId");
				if (objElement==null)
				{
					p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing",Globalization.GetString("CheckStockSetupAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
					return false;
				}
				iStockId=Conversion.ConvertStrToInteger(objElement.InnerText);

                using (objCheckStockSetup = new Riskmaster.Application.CheckStockSetup.CheckStockSetup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId))
                {
                    //delete the stock details
                    objCheckStockSetup.DeleteStockDetails(iStockId);
                }
				
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("CheckStockSetupAdaptor.DeleteStockDetails.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objCheckStockSetup = null;
				objElement=null;
			}
		}
		public bool GetAccountNameAndId(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.CheckStockSetup.CheckStockSetup objCheckStockSetup=null;
			
			try
			{
                using (objCheckStockSetup = new Riskmaster.Application.CheckStockSetup.CheckStockSetup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId))
                {
                    p_objXmlOut = objCheckStockSetup.GetAccountNameAndId();
                }

				return true;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("CheckStockSetupAdaptor.GetAccountNameAndId.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objCheckStockSetup = null;
			}
		}

		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.CheckStockSetup.CheckStockSetup.MoveAndGetStockDetails() method.
		///		This method will move to a specified check stock record and return back the details
		///		related to that check stock.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<CheckStockSetup>
		///				<StockId>StockId</StockId>
		///				<AccountId>AccountId</AccountId>
		///				<Navigation>Record to navigate to</Navigation>
		///			</CheckStockSetup>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		///		<CheckStock>
		///			<StockId /><AccountId /><StockName /><FontName /><FontSize /><MICRFlag /><MICRSequence /> 
		///			<MICRType /><MICRFont /><MICRFontSize /><RXLaserFlag /><SoftForm /><RXLaserForm /> 
		///			<RXVoidForm /><RXSpecialForm /><SpecialFlag /><SpecialAmount /><DateX /><DateY /> 
		///			<CheckNumberX /><CheckNumberY /><CheckAmountX /><CheckAmountY /><CheckAmountTextX /> 
		///			<CheckAmountTextY /><ClaimNumberOffsetX /><ClaimNumberOffsetY /><MemoX /><MemoY /> 
		///			<CheckDtlX /><CheckDtlY /><CheckPayerX /><CheckPayerY /><CheckPayeeX /><CheckPayeeY /> 
		///			<CheckOffsetX /><CheckOffsetY /><CheckMICRX /><CheckMICRY /><SecondOffsetX /><SecondOffsetY /> 
		///			<Unit /><DollarSign /><PrintAddress /><PrintTaxIDOnStub /><PrintAdjNamePhoneOnStub /> 
		///			<PreNumStock /><PrintMemo /><PrntEventOnStub /><PrintInsurer /><PrintClaimNumberOnCheck /> 
		///			<PrintCtlNumberOnStub /><PayerLevel /><MemoFontName /><MemoFontSize /><MemoRows /> 
		///			<MemoRowLength /><AddressFlag /><CheckLayout />
		///			<CheckStockForms>
		///				<CheckStockForm>
		///					<StockId /><ImageIdx /><FormType />
		///					<FormSource>contains data in base 64 encoded format</FormSource>
		///				</CheckStockForm>
		///			</CheckStockForms>
		///		</CheckStock> 
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool MoveAndGetStockDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.CheckStockSetup.CheckStockSetup objCheckStockSetup=null; //Application layer component
			int iStockId=0;						//Stock Id passed in the input XML document
			int iAccountId=0;					//Account Id passed in the input XML document
			int iNavigation=0;					//Navigation information passed in the input XML document
			XmlElement objElement=null;			//used for parsing the input xml

			try
			{
				if (!userLogin.IsAllowedEx( RMB_FUNDS_BNKACCT_CHKSTOCK, RMPermissions.RMO_ACCESS))
					throw new PermissionViolationException(RMPermissions.RMO_ACCESS,RMB_FUNDS_BNKACCT_CHKSTOCK);

				//Changes made for MITS 7358 by Gagan : Start
				if (!userLogin.IsAllowedEx( RMB_FUNDS_BNKACCT_CHKSTOCK, RMPermissions.RMO_VIEW))					
					throw new PermissionViolationException(RMPermissions.RMO_VIEW,RMB_FUNDS_BNKACCT_CHKSTOCK);
				//Changes made for MITS 7358 by Gagan : End

				//check existence of stock Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//StockId");
				if (objElement==null)
				{
					p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing",Globalization.GetString("CheckStockSetupAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
					return false;
				}
				iStockId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of account Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//AccountId");
				if (objElement==null)
				{
					p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing",Globalization.GetString("CheckStockSetupAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
					return false;
				}
				iAccountId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of Navigation which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Navigation");
				if (objElement==null)
				{
					p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing",Globalization.GetString("CheckStockSetupAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
					return false;
				}
				iNavigation=Conversion.ConvertStrToInteger(objElement.InnerText);

                using (objCheckStockSetup = new Riskmaster.Application.CheckStockSetup.CheckStockSetup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId))
                {
                    //move to the required record and get the details
                    p_objXmlOut = objCheckStockSetup.MoveAndGetStockDetails(iAccountId, iStockId, iNavigation);
                }

				//Changes made for MITS 7358 by Gagan : Start
				if( p_objXmlOut != null )
				{
					objElement=(XmlElement)p_objXmlOut.SelectSingleNode("//StockId");
					if (objElement!=null)
					{
						iStockId=Conversion.ConvertStrToInteger(objElement.InnerText);		
						
						if(iStockId==0)
						{
							if (!userLogin.IsAllowedEx( RMB_FUNDS_BNKACCT_CHKSTOCK, RMPermissions.RMO_CREATE))					
								throw new PermissionViolationException(RMPermissions.RMO_CREATE,RMB_FUNDS_BNKACCT_CHKSTOCK);
						}				

					}
				}
				//Changes made for MITS 7358 by Gagan : End
				return true;				
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("CheckStockSetupAdaptor.MoveAndGetStockDetails.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objCheckStockSetup = null;
				objElement=null;
			}
		}


		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.CheckStockSetup.CheckStockSetup.ViewImage() method.
		///		This method will return binary data for the requested image corresponding
		///		to a specified check stock.
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<CheckStockSetup>
		///				<StockId>StockId</StockId>
		///				<ImageId>ImageId</ImageId>
		///			</CheckStockSetup>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of output XML document would be:
		///		<checkstocksetup>
		///			<file name="name of file">
		///				data for the file in Base64 encoding
		///			</file>
		///		</checkstocksetup>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool ViewImage(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.CheckStockSetup.CheckStockSetup objCheckStockSetup=null; //Application layer component
			int iStockId=0;						//Stock Id passed in the input XML document
			int iImageId=0;						//Image Id passed in the input XML document
			string sTempFileName = string.Empty ;
			XmlElement objElement=null;			//used for parsing the input xml
			XmlElement objOutElement=null;		//used for creating the Output Xml
			MemoryStream objReturnValue=null;	//Return value from the Application layer 

			try
			{
				//check existence of StockId which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//StockId");
				if (objElement==null)
				{
					p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing",Globalization.GetString("CheckStockSetupAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
					return false;
				}
				iStockId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of ImageId which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ImageId");
				if (objElement==null)
				{
					p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing",Globalization.GetString("CheckStockSetupAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
					return false;
				}
				iImageId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of ImageId which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TempFileName");
				if (objElement==null)
				{
					p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing",Globalization.GetString("CheckStockSetupAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
					return false;
				}
				sTempFileName = objElement.InnerText ;

                using (objCheckStockSetup = new Riskmaster.Application.CheckStockSetup.CheckStockSetup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId))
                {

                    //create the output xml document containing the data in Base64 format
                    objOutElement = p_objXmlOut.CreateElement("checkstocksetup");
                    p_objXmlOut.AppendChild(objOutElement);
                    objOutElement = p_objXmlOut.CreateElement("file");
                    objOutElement.SetAttribute("name", GetFileName("CheckStockSetupImage", "emf"));
                    objOutElement.InnerText = objCheckStockSetup.ViewImage(iStockId, iImageId, sTempFileName);
                    p_objXmlOut.FirstChild.AppendChild(objOutElement);
                }

				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("CheckStockSetupAdaptor.ViewImage.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objCheckStockSetup = null;
				objElement=null;
			}
		}


		public bool SaveToLocalServer(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.CheckStockSetup.CheckStockSetup objCheckStockSetup=null; //Application layer component
			int iStockId=0;						//Stock Id passed in the input XML document
			int iImageId=0;						//Image Id passed in the input XML document
			XmlElement objElement=null;			//used for parsing the input xml
			XmlElement objOutElement=null;		//used for creating the Output Xml
			string sFileContent = string.Empty ;
			string sTempFileName = string.Empty ;
			try
			{
				//check existence of StockId which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FileContent");
				if (objElement==null)
				{
					p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing",Globalization.GetString("CheckStockSetupAdaptorParameterMissing", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
					return false;
				}
				sFileContent=objElement.InnerText;

                using (objCheckStockSetup = new Riskmaster.Application.CheckStockSetup.CheckStockSetup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId))
                {
                    sTempFileName = objCheckStockSetup.SaveToLocalServer(sFileContent);
                }
				
				//create the output xml document containing the data in Base64 format
				objOutElement=p_objXmlOut.CreateElement("checkstocksetup");
				p_objXmlOut.AppendChild(objOutElement);
				objOutElement=p_objXmlOut.CreateElement("TempFileName");				
				objOutElement.InnerText= sTempFileName ;
				p_objXmlOut.FirstChild.AppendChild(objOutElement);

				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("CheckStockSetupAdaptor.SaveToLocalServer.Error", base.ClientId),BusinessAdaptorErrorType.Error);//rkaur27
				return false;
			}
			finally
			{
				objCheckStockSetup = null;
				objElement=null;
			}
		}




		#endregion

		#region Private Functions
		/// <summary>
		/// Generates a unique file Name 
		/// </summary>
		/// <param name="p_sFileName">Intiials of the file name </param>
		/// <param name="p_sFileExt">file extension</param>
		/// <returns>Complete file name</returns>	
		private string GetFileName(string p_sFileName, string p_sFileExt)
		{
			string sFileName=string.Empty;	//for holding file name

			sFileName=p_sFileName + "_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + 
				DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() +
				DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + "." + p_sFileExt;

			return sFileName;
		}
		#endregion
	}
}
