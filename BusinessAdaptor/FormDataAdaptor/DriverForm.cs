﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Text;
using System.Collections.Generic;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// Summary description for Driver Screen.
    /// //Created by mbahl3 
    /// </summary>
    public class DriverForm : DataEntryFormBase
    {
        const string CLASS_NAME = "Driver";
        private Driver Driver { get { return objData as Driver; } }
        private LocalCache objCache { get { return objData.Context.LocalCache; } }
        const string FILTER_KEY_NAME = "EventId";
        const string INSTANCE_SYSEXDATA_PATH = "/Instance/UI/FormVariables/SysExData"; 
         private int m_iEventId = 0;
        private string sConnectionString = null;


        public override void InitNew()
        {
            base.InitNew();
            this.m_iEventId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME, false);
            int iDriverEId = base.GetSysExDataNodeInt("/SysExData/PiEid", false);
            //this.Driver.EventId = this.m_iEventId;

            if (this.Driver.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                (objData as INavigation).Filter = objData.PropertyNameToField("DriverEId") + "=" + base.GetSysExDataNodeInt("/SysExData/entity_id");
            }
            else {
                string sFilter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_iEventId.ToString();
                if (iDriverEId > 0)
                {
                    sFilter += " AND DRIVER_EID=" + iDriverEId;
                    this.Driver.DriverEId = iDriverEId;
                    (objData as INavigation).Filter = sFilter;
                }
            }
        }

        public DriverForm(FormDataAdaptor fda): base(fda)
        {
            base.m_ClassName = CLASS_NAME;
            sConnectionString = fda.connectionString;
        }
        public override void AfterSave()
        {
            base.ResetSysExData("DupSSN", "");  
        }
        public override void OnUpdateForm()
        {         
            base.OnUpdateForm();
            PopulateEntityTypes();
            base.CreateSysExData("DriverType", "");
            if (Driver.DriverEId != 0)
            {
               // this.AddKillNode("Drivertyped");
                this.AddKillNode("entitytableid");
                this.AddDisplayNode("drivertypeid");
                //this.AddDisplayNode("Drivertextname");                
                this.AddDisplayNode("entitytableidtextname");                
            }
            else
            {
                this.AddKillNode("drivertypeid");
                //this.AddKillNode("Drivertextname");
                this.AddKillNode("entitytableidtextname");
                //this.AddDisplayNode("Drivertyped");
                this.AddDisplayNode("entitytableid");
            }
            //avipinsrivas start : Worked for JIRA - 7767
            if (Driver.Context.InternalSettings.SysSettings.UseEntityRole && Driver.DriverEId <= 0)
                base.ResetSysExData("existingrecord", "false");
            else
                base.ResetSysExData("existingrecord", "true");
            if (Driver.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (base.SysEx.SelectSingleNode("//entity_id") != null && !string.IsNullOrEmpty(base.SysEx.SelectSingleNode("//entity_id").InnerText))
                {
                    bool bSuccess;
                    Driver.DriverEId = Conversion.CastToType<int>(base.SysEx.SelectSingleNode("//entity_id").InnerText, out bSuccess);
                }
                else if (Driver.DriverEId > 0)
                    base.ResetSysExData("entity_id", Driver.DriverEId.ToString());
                XmlNode xNode = base.FormVariables.SelectSingleNode("//SysFormId");
                if (xNode != null)
                    xNode.InnerText = Driver.DriverRowId.ToString();
            }
            //avipinsrivas end
            //Check for People Maintenance permission
            PeoplePermissionChecks4Other("driver", m_SecurityId + RMO_UPDATE);
            //added to hide/unhide SSN field
            if (!Driver.Context.RMUser.IsAllowedEx(FormBase.RMO_DRIVER, FormBase.RMO_MAINT_VIEW_SSN))
            {
                base.AddKillNode("taxid");
            }
            if (!Driver.Context.RMUser.IsAllowedEx(FormBase.RMO_DRIVERMAINT_WITHHOLDING))
            {
                base.AddKillNode("Withholding");
            }
            if (!Driver.Context.RMUser.IsAllowedEx(FormBase.RMO_DRIVERMAINT_EFT))
            {
                base.AddKillNode("BankingInfo");
            }
            //added by neha goel MITS# 36916: PMC gap 7 CLUE fields- RMA - 5499
            if (!Driver.Context.InternalSettings.SysSettings.UseCLUEReportingFields)
            {
                base.AddKillNode("VehicleOperRel");
            }
            else
            {
                base.AddDisplayNode("VehicleOperRel");
            }
            //end by neha goel MITS#36916 PMC CLUE gap 6- RMA - 5499
            ApplyFormTitle();
            ERSetting();        //avipinsrivas start : Worked for JIRA - 7767
        }
        //avipinsrivas start : Worked for JIRA - 7767
        private void ApplyFormTitle()
        {
            //string sCaption= Entity.Context.LocalCache.GetUserTableName(Entity.EntityTableId) + 

            string sSubTitleText = string.Empty;
            ArrayList singleRow = null;

            //avipinsrivas start : Worked for JIRA - 7767
            if (Driver.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                int iEntityID = 0;

                if (base.SysEx.SelectSingleNode("//entity_id") != null && !string.IsNullOrEmpty(base.SysEx.SelectSingleNode("//entity_id").InnerText))
                {
                    bool bSuccess;
                    iEntityID = Conversion.CastToType<int>(base.SysEx.SelectSingleNode("//entity_id").InnerText, out bSuccess);
                    if (iEntityID > 0)
                        sSubTitleText += string.Concat(" [ ", Driver.Context.LocalCache.GetEntityLastFirstName(iEntityID), " ]");
                }
            }

            if (string.IsNullOrEmpty(sSubTitleText) && Driver != null)
                sSubTitleText += " [ " + Driver.Default + " ]";

            //avipinsrivas end
            base.ResetSysExData("SubTitle", sSubTitleText);

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", sSubTitleText);
            base.m_ModifiedControls.Add(singleRow);
        }
        private void ERSetting()
        {
            if (Driver.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                //Toolbars
                base.AddKillNode("lookup");
                base.AddKillNode("attach");
                base.AddKillNode("diary");
                //base.AddKillNode("mailmerge");
                //base.AddKillNode("recordsummary");        //avipinsrivas Start : Worked for 13947 (Issue of 13196 - Epic 7767)
                base.AddKillNode("withholding");
                base.AddKillNode("BankingInfo");
                //End Toolbars

                //Controls
                base.AddKillNode("lastname");
                base.AddKillNode("taxid");
                base.AddKillNode("firstname");
                base.AddKillNode("birthdate");
                base.AddKillNode("middlename");
                base.AddKillNode("phone1");
                base.AddKillNode("abbreviation");
                base.AddKillNode("phone2");
                base.AddKillNode("alsoknownas");
                base.AddKillNode("faxnumber");
                base.AddKillNode("addr1");
                base.AddKillNode("title");
                base.AddKillNode("addr2");
                base.AddKillNode("sexcode");
                base.AddKillNode("addr3");
                base.AddKillNode("addr4");
                base.AddKillNode("zipcode");
                base.AddKillNode("city");
                base.AddKillNode("countrycode");
                base.AddKillNode("stateid");
                base.AddKillNode("county");
                //End Controls
            }
            else
                base.AddKillNode("BackToParent");
        }
        //avipinsrivas End
        
        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;
        }
        public override void OnUpdateObject()
        {
            ////avipinsrivas Start : Worked for Jira-340
            //int intTableID = Driver.Context.LocalCache.GetTableId(Globalization.EntityGlossaryTableNames.DRIVERS.ToString());
            //Driver.DriverEntity.UpdateEntityRoles((Driver.DriverEntity as IDataModel), intTableID);
            ////avipinsriavs End
            
            base.OnUpdateObject();
            if(Driver.DriverEId != 0)
                return;
            
            XmlNode objOrig = base.SysEx.SelectSingleNode("/*/DriverType");
            if (Driver.DriverEId == 0 && objOrig != null)
                
                Driver.DriverTypeCode = Conversion.ConvertStrToInteger(objOrig.InnerText);
        }
        private void PopulateEntityTypes()
        {
            
            bool bBlankValue = true;

            
            string strTableName = base.GetSysExDataNodeText("/SysExData/PageTypeName", false);
           
            strTableName = "DRIVER_TYPE";
            
            string sSQL = @"SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT  WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID " +
      " AND DELETED_FLAG = 0 AND CODES.TABLE_ID =(SELECT G.TABLE_ID FROM GLOSSARY_TEXT GT, GLOSSARY G " +
      " WHERE GT.TABLE_ID=G.TABLE_ID AND UPPER(SYSTEM_TABLE_NAME) = '" + strTableName + "') ORDER BY CODES.SHORT_CODE";
           
            XmlElement xmlRoot = base.SysEx.CreateElement("EntityTypeList");
           

            using (DbReader rdr = Driver.Context.DbConnLookup.ExecuteReader(sSQL))
            {

               
                while (rdr.Read())
                {
                    
                        if (bBlankValue)
                        {
                           
                            XmlElement xmlOptionBlank = base.SysEx.CreateElement("option");
                            xmlOptionBlank.InnerText = "";
                            XmlAttribute xmlOptionBlankAttrib = base.SysEx.CreateAttribute("value");
                            xmlOptionBlankAttrib.Value = "";
                            xmlOptionBlank.Attributes.Append(xmlOptionBlankAttrib);
                            xmlRoot.AppendChild(xmlOptionBlank);
                            bBlankValue = false;
                            XmlElement xmlOption = base.SysEx.CreateElement("option");
                            xmlOption.InnerText = rdr.GetString("CODE_DESC");
                            XmlAttribute xmlOptionAttrib = base.SysEx.CreateAttribute("value");
                            xmlOptionAttrib.Value = rdr.GetInt("CODE_ID").ToString();
                            xmlOption.Attributes.Append(xmlOptionAttrib);
                            xmlRoot.AppendChild(xmlOption);
                       
                        }
                        else
                        {
                            XmlElement xmlOption = base.SysEx.CreateElement("option");
                            xmlOption.InnerText = rdr.GetString("CODE_DESC");
                            XmlAttribute xmlOptionAttrib = base.SysEx.CreateAttribute("value");
                            xmlOptionAttrib.Value = rdr.GetInt("CODE_ID").ToString();
                            xmlOption.Attributes.Append(xmlOptionAttrib);
                            xmlRoot.AppendChild(xmlOption);
                        }
                   
                }
                rdr.Close();
            }

            

            XmlNode objOrig = base.SysEx.SelectSingleNode("/*/EntityTypeList");
            if (objOrig != null)
                base.SysEx.DocumentElement.RemoveChild(objOrig);
            base.SysEx.DocumentElement.AppendChild(xmlRoot as XmlNode);
            base.ResetSysExData("DriverType", Driver.DriverTypeCode.ToString());
            base.ResetSysExData("DriverTypeCode", Driver.Context.LocalCache.GetCodeDesc(Driver.DriverTypeCode));

            //base.ResetSysExData("DriverTypeName", Driver.DriverEntity.Context.LocalCache.GetUserTableName(Driver.DriverEntity.EntityTableId));
            //base.ResetSysExData("DriverTypeCode", Driver.Context.LocalCache.GetCodeDesc(Driver.DriverTypeCode));
           
        }
       
    }
}
