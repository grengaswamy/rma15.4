﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Text;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PiWitness Screen.
	/// </summary>
	public class PiWitnessForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PiWitness";
		private PiWitness PiWitness{get{return objData as PiWitness;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "EventId";
		private int m_iEventId = 0;
		private string sConnectionString = null; // Ishan Mobile Apps
        private int m_iParentRowId = 0;
        private string m_sParentTableName = string.Empty;
        const string FILTER_KEY_NAME2 = "ParentRowId";
        const string FILTER_KEY_NAME3 = "ParentTableName";
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();

		public override void InitNew()
		{
			base.InitNew(); 

            //this.m_iEventId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
            //string sFilter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_iEventId.ToString();
            //rsushilaggar JITA 11736  
            //this.m_iEventId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);   
            string sFilter = string.Empty;
            this.m_iParentRowId = base.GetSysExDataNodeInt("/SysExData/ParentRowId", false);
            this.m_sParentTableName = base.GetFormVarNodeText("SysFormPForm");
            this.PiWitness.ParentRowId = this.m_iParentRowId;

            //-----sgupta320:Jira 15676
            
            switch ((this.m_sParentTableName).ToUpper())
            {
                case "CLAIMGC":
                    m_SecurityId = RMO_GC_PERSONINVOLVED;
                    this.PiWitness.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiWitness.ParentTableName + "' ";
                    break;
                case "CLAIMDI":
                    m_SecurityId = RMO_DI_PERSONINVOLVED;
                    this.PiWitness.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiWitness.ParentTableName + "' ";
                    break;
                case "CLAIMPC":
                    m_SecurityId = RMO_PC_PERSONINVOLVED;
                    this.PiWitness.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiWitness.ParentTableName + "' ";
                    break;
                case "CLAIMWC":
                    m_SecurityId = RMO_WC_PERSONINVOLVED;
                    this.PiWitness.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiWitness.ParentTableName + "' ";
                    break;
                case "CLAIMVA":
                    m_SecurityId = RMO_VA_PERSONINVOLVED;
                    this.PiWitness.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiWitness.ParentTableName + "' ";
                    break;
                case "CLAIMXPOLICY":
                    m_SecurityId = RMO_POLICY_PERSONINVOLVED;
                    this.PiWitness.ParentTableName = "POLICY";
                    m_iEventId = CommonFunctions.GetEventIdByPolicyId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiWitness.ParentTableName + "' ";
                    break;
                default:
                    // Defaults to Event's person involved
                    m_iEventId = m_iParentRowId;
                    this.PiWitness.ParentTableName = "EVENT";
                    sFilter = "(" + objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_iEventId.ToString() + " OR (" + objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiWitness.ParentTableName + "'))";
                    break;
            }
            //End rsushilaggar Jira 11736
			//Nikhil Garg	03/02/2006 
			//Moved from problematic Code below (Not to save before user presses Save button)
			int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid",false);
			sFilter += " AND PI_EID=" + iPiEid;

			(objData as INavigation).Filter = sFilter;
			
			this.PiWitness.EventId = this.m_iEventId;
			this.PiWitness.PiEid = iPiEid;

            if (this.PiWitness.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (this.PiWitness.PiEntity.EntityId <= 0 && this.PiWitness.PiEntity.NameType <= 0)
                    this.PiWitness.PiEntity.NameType = objCache.GetCodeId("IND", "ENTITY_NAME_TYPE");
            }

        }

//		public override void AfterAddNew()
//		{
//			base.AfterAddNew ();
//
//			int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid",false);
// 
//			if(iPiEid>0)
//			{
//				string sSQL = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE EVENT_ID=" + this.m_iEventId + " AND PI_EID=" + iPiEid;
//				int iPiRowId = 0;
//				iPiRowId = this.PiWitness.Context.DbConnLookup.ExecuteInt(sSQL);
//				if(iPiRowId>0)
//				{
//					// PI Record already exists
//					this.PiWitness.MoveTo(iPiRowId); 
//				}
//				else
//				{
//					this.PiWitness.PiEid = iPiEid;
//					this.PiWitness.Save();  
//				}
//			}
//		}

		public PiWitnessForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
			sConnectionString = fda.connectionString;  // Ishan Mobile Apps
		}

        //Charanpreet for 12409
        public override void BeforeSave(ref bool Cancel)
        {
            int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid", false);
            int iRoleTableId = this.PiWitness.Context.LocalCache.GetTableId(Globalization.IndividualGlossaryTableNames.WITNESS.ToString());
            this.m_iParentRowId = base.GetSysExDataNodeInt("/SysExData/ParentRowId", false);
         
            if (iPiEid > 0 && (this.m_objData as PiWitness).IsNew)
            {
                //string sSQL = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE EVENT_ID=" + this.m_iEventId + " AND PI_EID=" + iPiEid;
                string sSQL = string.Empty;
                if (this.PiWitness.Context.InternalSettings.SysSettings.UseEntityRole)
                    sSQL = string.Format("SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PARENT_ROW_ID= {0} AND PI_EID= {1} AND ROLE_TABLE_ID= {2} AND  PARENT_TABLE_NAME = '{3}' ", this.m_iParentRowId, iPiEid, iRoleTableId, this.PiWitness.ParentTableName);
                else
                    sSQL = string.Format("SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PARENT_ROW_ID= {0} AND PI_EID= {1} AND  PARENT_TABLE_NAME = '{2}' ", this.m_iParentRowId, iPiEid, this.PiWitness.ParentTableName);

                int iPiRowId = 0;
                iPiRowId = this.PiWitness.Context.DbConnLookup.ExecuteInt(sSQL);
                if (iPiRowId > 0)
                {
                    // PI Record already exists	
                    Cancel = true;
                    // pgupta215: RMA-18997
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), string.Format(Globalization.GetString("Validation.WitnessAlreadyExistsAsPI", base.ClientId, sLangCode), this.PiWitness.ParentTableName.ToLower()), BusinessAdaptorErrorType.Error);
                    return;
                }
            }

        }
        //Charanpreet for 12409 ends
        		
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
            ArrayList singleRow = null;
			base.OnUpdateForm ();

			XmlDocument objSysExDataXmlDoc = base.SysEx;

			string sSubTitleText = string.Empty; 
			
			// Add EventNumber node, if already not present, to avoid Orbeon 'ref' error 			
			XmlNode objEventNumberNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/EventNumber");

			if(objEventNumberNode==null)
			{
                //Deb MITS 27711 
                if (this.PiWitness.Context.InternalSettings.SysSettings.UseAcrosoftInterface)
                {
                    string sEventnumber = this.PiWitness.Context.DbConnLookup.ExecuteString("SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + this.PiWitness.EventId);
                    base.CreateSysExData("EventNumber", sEventnumber);
                }
                else
                {
                    base.CreateSysExData("EventNumber");
                }
                //Deb MITS 27711 
			}
			else
			{
				if(objEventNumberNode.InnerText.Length>0)
					sSubTitleText = " [" + objEventNumberNode.InnerText + "]"; 
				objEventNumberNode = null;
			}
			

			// Add ClaimNumber node, if already not present, to avoid Orbeon 'ref' error
			XmlNode objClaimNumberNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/ClaimNumber");

			if(objClaimNumberNode==null)
			{
				base.CreateSysExData("ClaimNumber"); 
			}			
			else
			{
				if(objClaimNumberNode.InnerText.Length>0)
					sSubTitleText = " [" + objClaimNumberNode.InnerText + "]"; 
				objClaimNumberNode = null;
			}

			// TODO - To find a better way to ascertain which is our starting page (Event or Claim)
			// As of now, first event number and then claim number is checked for subtitle text.
			base.ResetSysExData("SubTitle",sSubTitleText);

            //MGaba2:MITS 22114:Comments screen showing title as "Piemployee Comments undefined" :Start
            //Shifting the code below as event number is required
            //mona
           //singleRow = new ArrayList();
           // base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
           // base.AddElementToList(ref singleRow, "id", "formsubtitle");
           // base.AddElementToList(ref singleRow, "Text", sSubTitleText);
           // base.m_ModifiedControls.Add(singleRow);




			base.ResetSysExData("LastNameFirstName", PiWitness.PiEntity.GetLastFirstName());

            //objSysExDataXmlDoc = null;
            //Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.  
            Event objEvent = (Event)this.PiWitness.Context.Factory.GetDataModelObject("Event", false);
            string sEventDateFromDB = string.Empty;
            if (objEvent != null)
            {
                int iEventId = PiWitness.EventId;
                if (iEventId != 0)
                {
                    objEvent.MoveTo(iEventId);
                    sEventDateFromDB = objEvent.DateOfEvent;
                    if (!String.IsNullOrEmpty(sEventDateFromDB))
                    {
                        if (base.SysEx.SelectSingleNode("DateOfEvent") == null)
                        {
                            string sUIEventDate = string.Empty;
                            sUIEventDate = Conversion.ToDate(sEventDateFromDB).ToString("MM/dd/yyyy");
                            base.CreateSysExData("DateOfEvent", sUIEventDate);
                        }
                    }

                    switch (PiWitness.ParentTableName.ToUpper())  //ijain4 : JIRA 17480 : 20/11/2015
                    {
                        //case "CLAIMGC":
                        //case "CLAIMDI":
                        //case "CLAIMPC":
                        //case "CLAIMWC":
                        case "CLAIM":
                            Claim objClaim = (Claim)this.PiWitness.Context.Factory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(PiWitness.ParentRowId);
                            sSubTitleText = string.Format(" [{0} * {1} {2}]", objClaim.ClaimNumber, PiWitness.PiEntity.FirstName, PiWitness.PiEntity.LastName);
                            break;
                        case "POLICY":
                            Policy objPolicy = (Policy)this.PiWitness.Context.Factory.GetDataModelObject("Policy", false);
                            objPolicy.MoveTo(PiWitness.ParentRowId);
                            sSubTitleText = string.Format(" [{0} * {1} {2}]", objPolicy.PolicyNumber, PiWitness.PiEntity.FirstName, PiWitness.PiEntity.LastName);
                            break;
                        default:
                            sSubTitleText = string.Format(" [{0} * {1} {2}]", objEvent.EventNumber, PiWitness.PiEntity.FirstName, PiWitness.PiEntity.LastName);
                            break;
                    }
                    //ijain4 : JIRA 17480 : 20/11/2015 End
                    //MGaba2:MITS 22114:Comments screen showing title as "Piemployee Comments undefined" :Start
                    //     sSubTitleText = string.Format(" [{0} * {1} {2}]", objEvent.EventNumber, PiWitness.PiEntity.FirstName, PiWitness.PiEntity.LastName); //commented by ijain4 : JIRA 17480 : 20/11/2015 
                    singleRow = new ArrayList();
                    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                    base.AddElementToList(ref singleRow, "id", "formsubtitle");
                    base.AddElementToList(ref singleRow, "Text", sSubTitleText);
                    base.m_ModifiedControls.Add(singleRow);
                    //MGaba2:MITS 22114:End
                }
            }
            //Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.
			if (PiWitness.PiEntity.BirthDate!=string.Empty)
                base.ResetSysExData("EntityAge", Utilities.CalculateAgeInYears(PiWitness.PiEntity.BirthDate, sEventDateFromDB)); //Added sEventDateFromDB for MITS 19315
			else
				base.ResetSysExData("EntityAge","");

			if (this.PiWitness.PiRowId > 0 || this.PiWitness.PiEid <= 0)
				base.AddKillNode("javascript_SetDirtyFlag");

            //smishra25: MITS 24395, MITS 13748 implementation was missing
            //nadim for 13748,added to hide/unhide SSN field
            if (!PiWitness.Context.RMUser.IsAllowedEx(m_SecurityId, FormBase.RMO_PI_WITNESS_VIEW_SSN))
            {
                base.AddKillNode("taxid");

            }
            //nadim for 13748

            // Add DiaryMessage node, if already not present, to avoid Orbeon 'ref' error
            XmlNode objDiaryMessageNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/DiaryMessage");

            if (objDiaryMessageNode == null)
            {
                base.CreateSysExData("DiaryMessage");
            }
			//Shruti Choudhary updated code for mits 10384
            //if (this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[2]/SysFormName") != null)
            //{
            //    ((XmlElement)base.SysView.SelectSingleNode("//internal[@name='SysFormPForm']")).SetAttribute("value",
            //    this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[2]/SysFormName").InnerText);
            //}

            objSysExDataXmlDoc = null;
			//Check for People Maintenance permission
			PeoplePermissionChecks4PI("piwitness", m_SecurityId+RMO_UPDATE);
            if (PiWitness.PolicyUnitRowId != 0)
            {
                DisplayUnitNo();

            }
            else
            {

                base.AddKillNode("Unitno");
            
            }
            //added by neha goel MITS# 36916: PMC gap 7 CLUE fields- RMA - 5499
            if (!PiWitness.Context.InternalSettings.SysSettings.UseCLUEReportingFields)
            {
                base.AddKillNode("ClueSubjIdentifier");
                base.AddKillNode("ClueSubjRemovalIdentifier");
            }
            else
            {
                base.AddDisplayNode("ClueSubjIdentifier");
                base.AddDisplayNode("ClueSubjRemovalIdentifier");
            }
            //end by neha goel MITS#36916 PMC CLUE gap 7- RMA - 5499
            //rsushilaggar JIRA 7767
            if (this.PiWitness.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (this.PiWitness != null && this.PiWitness.PiEntity != null && this.PiWitness.PiEntity.NameType > 0)
                    this.ResetSysExData("EntityType", objCache.GetCodeDesc(this.PiWitness.PiEntity.NameType));
                else
                    this.ResetSysExData("EntityType", objCache.GetCodeDesc(objCache.GetCodeId("IND", "ENTITY_NAME_TYPE")));
            }
            else
            {
                this.AddKillNode("entitytype");
            }
            //end rushilaggar
            
            //----sgupta320: Jira-17668
            if (base.FormVariables.SelectSingleNode("//SysSid") != null)
                base.FormVariables.SelectSingleNode("//SysSid").InnerText = Convert.ToString(m_SecurityId);
		}

		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;

			// Perform data validation
			string sToday = Conversion.ToDbDate(System.DateTime.Now);
			string sTime = Conversion.GetTime(System.DateTime.Now.ToShortTimeString());

			if(PiWitness.PiEntity.BirthDate!="")
			{
				if(PiWitness.PiEntity.BirthDate.CompareTo(sToday)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId), System.DateTime.Now.ToShortDateString()),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}
			// Return true if there were validation errors
			Cancel = bError;
		}
        //Added Rakhi for R7:Add Emp Data Elements
        public override void OnUpdateObject()
        {
            base.OnUpdateObject();
			XmlElement objCaller = null;


			objCaller = (XmlElement)base.FormVariables.SelectSingleNode("//caller");
			if (objCaller != null)
			{
                if (objCaller.InnerText == "MobileAdjuster" || objCaller.InnerText == "MobilityAdjuster")
                {
					string sSQL = " SELECT EVENT_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + base.GetSysExDataNodeText("//ClaimNumber") + "'";
					using (DbReader objReader = DbFactory.ExecuteReader(sConnectionString, sSQL))
					{
						if (objReader.Read())
						{
							PiWitness.EventId = Conversion.ConvertObjToInt(objReader.GetValue("EVENT_ID"), base.ClientId);
							this.m_iEventId = PiWitness.EventId;
							//base.FormVariables.SelectSingleNode("//SysExData//EventId").InnerText = objReader.GetValue("EVENT_ID").ToString();
							if (base.FormVariables.SelectSingleNode("//SysExData//EventId") != null)
							{
								base.FormVariables.SelectSingleNode("//SysExData//EventId").InnerText = objReader.GetValue("EVENT_ID").ToString();

							}

						}
					}
				}
			}
            if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
            {
                PiWitness.PiEntity.FormName = "PiWitnessForm"; //Mits 22497

                #region "Updating Address Info Object"

                string sAddr1 = PiWitness.PiEntity.Addr1.Trim();
                string sAddr2 = PiWitness.PiEntity.Addr2.Trim();
                string sAddr3 = PiWitness.PiEntity.Addr3.Trim();// JIRA 6420 pkandhari
                string sAddr4 = PiWitness.PiEntity.Addr4.Trim();// JIRA 6420 pkandhari
                string sCity = PiWitness.PiEntity.City.Trim();
                int iCountryCode = PiWitness.PiEntity.CountryCode;
                int iStateId = PiWitness.PiEntity.StateId;
                string sEmailAddress = PiWitness.PiEntity.EmailAddress.Trim();
                string sFaxNumber = PiWitness.PiEntity.FaxNumber.Trim();
                string sCounty = PiWitness.PiEntity.County.Trim();
                string sZipCode = PiWitness.PiEntity.ZipCode.Trim();
                //RMA-8753 nshah28(Added by ashish)
                string sSearchString = string.Empty;
                //RMA-8753 nshah28(Added by ashish) END

                if (
                        sAddr1 != string.Empty || sAddr2 != string.Empty || sAddr3 != string.Empty || sAddr4 != string.Empty ||
                        sCity != string.Empty || iCountryCode !=0 || iStateId !=0 || sEmailAddress != string.Empty ||
                        sFaxNumber != string.Empty || sCounty != string.Empty || sZipCode !=string.Empty
                    )
                {

                    if (PiWitness.PiEntity.EntityXAddressesList.Count == 0)
                    {


                        EntityXAddresses objEntityXAddressesInfo = PiWitness.PiEntity.EntityXAddressesList.AddNew();
						//RMA-8753 nshah28(Added by ashish) START
                        //objEntityXAddressesInfo.Addr1 = sAddr1;
                        //objEntityXAddressesInfo.Addr2 = sAddr2;
                        //objEntityXAddressesInfo.Addr3 = sAddr3;// JIRA 6420 pkandhari
                        //objEntityXAddressesInfo.Addr4 = sAddr4;// JIRA 6420 pkandhari
                        //objEntityXAddressesInfo.City =sCity;
                        //objEntityXAddressesInfo.Country =iCountryCode;
                        //objEntityXAddressesInfo.State = iStateId;
                        objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                        objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                        objEntityXAddressesInfo.Address.Addr3 = sAddr3;
                        objEntityXAddressesInfo.Address.Addr4 = sAddr4;
                        objEntityXAddressesInfo.Address.City = sCity;
                        objEntityXAddressesInfo.Address.Country = iCountryCode;
                        objEntityXAddressesInfo.Address.State = iStateId;
                        objEntityXAddressesInfo.Email =sEmailAddress;
                        objEntityXAddressesInfo.Fax =sFaxNumber;
                        //objEntityXAddressesInfo.County =sCounty;
                        //objEntityXAddressesInfo.ZipCode = sZipCode;
                        objEntityXAddressesInfo.Address.County = sCounty;
                        objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                        objEntityXAddressesInfo.EntityId = PiWitness.PiEntity.EntityId;
                        objEntityXAddressesInfo.PrimaryAddress = -1;
                        //objEntityXAddressesInfo.AddressId = -1;

                        AddressForm objAddressForm = new AddressForm(m_fda);
                        sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                        objEntityXAddressesInfo.Address.SearchString = sSearchString;
                        objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, PiWitness.Context.RMDatabase.ConnectionString, base.ClientId);
                        objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
                        //RMA-8753 nshah28(Added by ashish) END
                    }
                    else
                    {
                        foreach (EntityXAddresses objEntityXAddressesInfo in PiWitness.PiEntity.EntityXAddressesList)
                        {
                            if (objEntityXAddressesInfo.PrimaryAddress == -1)
                            {
								//RMA-8753 nshah28(Added by ashish) START
                                //objEntityXAddressesInfo.Addr1 = sAddr1;
                                //objEntityXAddressesInfo.Addr2 = sAddr2;
                                //objEntityXAddressesInfo.Addr3 = sAddr3;// JIRA 6420 pkandhari
                                //objEntityXAddressesInfo.Addr4 = sAddr4;// JIRA 6420 pkandhari
                                //objEntityXAddressesInfo.City = sCity;
                                //objEntityXAddressesInfo.Country = iCountryCode;
                                //objEntityXAddressesInfo.State = iStateId;
                                objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                                objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                                objEntityXAddressesInfo.Address.Addr3 = sAddr3;
                                objEntityXAddressesInfo.Address.Addr4 = sAddr4;
                                objEntityXAddressesInfo.Address.City = sCity;
                                objEntityXAddressesInfo.Address.Country = iCountryCode;
                                objEntityXAddressesInfo.Address.State = iStateId;
                                objEntityXAddressesInfo.Email = sEmailAddress;
                                objEntityXAddressesInfo.Fax = sFaxNumber;
                                //objEntityXAddressesInfo.County = sCounty;
                                //objEntityXAddressesInfo.ZipCode = sZipCode;
                                objEntityXAddressesInfo.Address.County = sCounty;
                                objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                                AddressForm objAddressForm = new AddressForm(m_fda);
                                sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                                objEntityXAddressesInfo.Address.SearchString = sSearchString;
                                objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, PiWitness.Context.RMDatabase.ConnectionString, base.ClientId);
                                objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
								//RMA-8753 nshah28(Added by ashish) END
                                break;
                            }
                        }
                    }
                }
                else
                {
                    if (PiWitness.PiEntity.EntityXAddressesList.Count > 0)
                    {
                        foreach (EntityXAddresses objEntityXAddressesInfo in PiWitness.PiEntity.EntityXAddressesList)
                        {
                            if (objEntityXAddressesInfo.PrimaryAddress == -1)
                            {
                                //PiWitness.PiEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                                PiWitness.PiEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId); //RMA-8753 nshah28(Added by ashish)
                                break;
                            }

                        }
                    }

                }
            #endregion

                #region Added for updating Phone Numbers

                string sOfficePhone = PiWitness.PiEntity.Phone1.Trim();
                string sHomePhone = PiWitness.PiEntity.Phone2.Trim();
                int iHomeCode = objCache.GetCodeId("h", "PHONES_CODES");
                int iOfficeCode = objCache.GetCodeId("o", "PHONES_CODES");
                bool bOfficePhoneEntered = false;
                AddressXPhoneInfo objAddressesInfo = null;
                bool bOfficeCodeExists=false;
                bool bHomePhoneEntered=false;
                bool bHomeCodeExists = false;

                if(sOfficePhone != string.Empty)
                    bOfficePhoneEntered=true;
                if(sHomePhone !=string.Empty)
                    bHomePhoneEntered=true;

                if (PiWitness.PiEntity.AddressXPhoneInfoList.Count == 0)
                {
                    if (bOfficePhoneEntered)
                    {
                        objAddressesInfo = PiWitness.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iOfficeCode;
                        objAddressesInfo.PhoneNo = sOfficePhone;
                    }
                    if (bHomePhoneEntered)
                    {
                        objAddressesInfo = PiWitness.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iHomeCode;
                        objAddressesInfo.PhoneNo = sHomePhone;
                    }
                }
                else
                {
                    foreach (AddressXPhoneInfo objXAddressesInfo in PiWitness.PiEntity.AddressXPhoneInfoList)
                    {
                        if (objXAddressesInfo.PhoneCode == iOfficeCode)
                        {
                            if (bOfficePhoneEntered)
                            {
                                objXAddressesInfo.PhoneNo = sOfficePhone;
                                bOfficeCodeExists = true;
                            }
                            else
                            {
                                PiWitness.PiEntity.AddressXPhoneInfoList.Remove(objXAddressesInfo.PhoneId);
                            }
                            continue;
                        }
                        else if (objXAddressesInfo.PhoneCode == iHomeCode)
                        {
                            if (bHomePhoneEntered)
                            {
                                objXAddressesInfo.PhoneNo = sHomePhone;
                                bHomeCodeExists = true;
                            }
                            else
                            {
                                PiWitness.PiEntity.AddressXPhoneInfoList.Remove(objXAddressesInfo.PhoneId);
                            }
                        }
                    }
                    if (bOfficePhoneEntered && !bOfficeCodeExists)
                    {
                        objAddressesInfo = PiWitness.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iOfficeCode;
                        objAddressesInfo.PhoneNo = sOfficePhone;
                    }
                    if (bHomePhoneEntered && !bHomeCodeExists)
                    {
                        objAddressesInfo = PiWitness.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iHomeCode;
                        objAddressesInfo.PhoneNo = sHomePhone;
                    }
                }
               
#endregion
            }
            
        }
        //Added Rakhi for R7:Add Emp Data Elements
        private void DisplayUnitNo()
        {

            string sStaUnitNo = string.Empty;
            string sRiskLoc = string.Empty;
            string sSubLoc = string.Empty;

            StringBuilder sbSql = new StringBuilder();
            // RMA-10039: Ash, fixed error but in general this is incorrect handling for POINT, Integral, Staging
            sbSql.Append("SELECT POINT_UNIT_DATA.STAT_UNIT_NUMBER, POINT_UNIT_DATA.UNIT_RISK_LOC, POINT_UNIT_DATA.UNIT_RISK_SUB_LOC from POLICY_X_UNIT,POINT_UNIT_DATA,PERSON_INVOLVED ");
            sbSql.Append(" WHERE POLICY_X_UNIT.POLICY_UNIT_ROW_ID=PERSON_INVOLVED.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.UNIT_ID=POINT_UNIT_DATA.UNIT_ID ");
            sbSql.Append(" AND POINT_UNIT_DATA.UNIT_TYPE=POLICY_X_UNIT.UNIT_TYPE AND PERSON_INVOLVED.PI_ROW_ID=" + PiWitness.PiRowId);

            DbReader objReader = null;
            objReader = DbFactory.GetDbReader(base.m_fda.connectionString, sbSql.ToString());
            if (objReader != null)
            {
                if (objReader.Read())
                {
                    sStaUnitNo = objReader.GetValue("STAT_UNIT_NUMBER").ToString();
                    sRiskLoc = objReader.GetValue("UNIT_RISK_LOC").ToString();
                    if (sRiskLoc == string.Empty)
                        sRiskLoc = "-";

                    sSubLoc = objReader.GetValue("UNIT_RISK_SUB_LOC").ToString();
                    if (sSubLoc == string.Empty)
                        sSubLoc =  "-";

                    base.ResetSysExData("UnitNo", sStaUnitNo + "/" + sRiskLoc + "/" + sSubLoc);


                }

            }


        }
	}
}
