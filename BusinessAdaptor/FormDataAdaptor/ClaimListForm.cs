using System;
using Riskmaster.DataModel;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PersonInvolvedList.
	/// </summary>
	public class ClaimListForm: ListFormBase
	{
		const string CLASS_NAME = "ClaimList";

		public override void Init()
		{
			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlDocument objXML = base.SysEx;
			XmlNode objEventId=null;
			try{objEventId = objXML.SelectSingleNode("/SysExData/EventId");}
			catch{};
			
			//Filter by this EventId if Present.
			if(objEventId !=null)
			{
				objDataList.BulkLoadFlag = true;
				objDataList.SQLFilter = "EVENT_ID=" + objEventId.InnerText;
			}
			else
				throw new Riskmaster.ExceptionTypes.CollectionException(String.Format(Globalization.GetString("ListForm.MissingExpectedFilterKey",base.ClientId),"EventId"));
			OnUpdateForm();
		}


		public ClaimListForm(FormDataAdaptor fda):base(fda)
		{
			this.objDataList = fda.Factory.GetDataModelObject(CLASS_NAME,false) as DataCollection;
		}
		private LocalCache objCache{get{return objDataList.Context.LocalCache;}}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm() //Only called from base classes on Refresh()caused by PostBack.
		{
			int iEventID = Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/EventId").InnerXml);

			base.OnUpdateForm ();

			// JP 01.27.2007   Create a PiRowId variable in SysEx to hold a passed out PiRowId to the ClaimWC/DI forms. This is set through javascript via a popup on the form.
			base.CreateSysExData("PiRowId","0");

			// Stash count of PI employees so we can throw up popup to pick employee when creating claim
			int iCount=objDataList.Context.DbConnLookup.ExecuteInt("SELECT COUNT(*) FROM PERSON_INVOLVED WHERE EVENT_ID = " + iEventID.ToString() + " AND PI_TYPE_CODE = 237");
			base.ResetSysExData("PiEmpCount", iCount.ToString());

			XmlDocument objXML = base.SysEx;
			XmlNode objNew;
			XmlNode objOld ;
			
			
			objNew = objXML.CreateElement("Screen");
			objNew.Attributes.Append(objXML.CreateAttribute("type"));
			objNew.Attributes["type"].Value="";

			objOld = objXML.SelectSingleNode("//Screen");
			if(objOld !=null)
				;
				//Let it Ride from the Client... 
				//Will be acted on in the Form XSL
				//No Need to clear since next page request should hit a new
				//form altogether anyhow.
				//objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);
			string sDateOfEvent= objDataList.Context.DbConnLookup.ExecuteString("SELECT DATE_OF_EVENT FROM EVENT WHERE EVENT_ID="+objXML.SelectSingleNode("/SysExData/EventId").InnerXml);
			objNew = objXML.CreateElement("DateofEvent");
			objNew.InnerXml=Conversion.GetDBDateFormat(sDateOfEvent,"d");
			objOld = objXML.SelectSingleNode("//DateofEvent");
			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);
//			//Clear any existing tablename nodes.
//			foreach(XmlNode nd in objXML.SelectNodes("//EntityTableName"))
//				nd.ParentNode.RemoveChild(nd);
//
//			//*********************
//			// Parallel Array to the PIList to be serialized...
//			//*********************
//			foreach(PersonInvolved objPI in objDataList)
//			{
//				//Entity Table Name - Used as fall-back for Blank Entity Name.
//				string sTableName = objCache.GetUserTableName(objPI.PiEntity.EntityTableId);
//				objNew = objXML.CreateElement("EntityTableName");
//				objNew.AppendChild(objXML.CreateCDataSection(sTableName));
//				objXML.DocumentElement.AppendChild(objNew);
//			}
		}
	}
}
