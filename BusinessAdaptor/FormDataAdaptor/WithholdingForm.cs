﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
    public class WithholdingForm : DataEntryFormBase
    {
        const string CLASS_NAME = "EntityXWithholding";
        const string FILTER_KEY_NAME = "EntityId";
        private EntityXWithholding objEntityXWithholding { get { return objData as EntityXWithholding; } }
        private LocalCache objCache { get { return objData.Context.LocalCache; } }
        private string m_Caption = string.Empty;

        public WithholdingForm(FormDataAdaptor fda): base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

        public override string GetCaption()
        {
            return base.GetCaption();
        }

        public override void InitNew()
        {
            base.InitNew();
            objEntityXWithholding.EntityId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME, false);
            if (objEntityXWithholding.EntityId > 0)
                (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + objEntityXWithholding.EntityId;
        }

        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;
            if (objEntityXWithholding.WithholdingFlag)
            {
                if (double.Equals(objEntityXWithholding.PercentageNum, 0.0))
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        Globalization.GetString("Validation.PercentageMandatory", base.ClientId), BusinessAdaptorErrorType.Error);
                    bError = true;
                }

                if (int.Equals(objEntityXWithholding.RecipientEid, 0))
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        Globalization.GetString("Validation.RecipientMandatory", base.ClientId), BusinessAdaptorErrorType.Error);
                    bError = true;
                }
               
            }
            Cancel = bError;
        }

        public override void OnUpdateObject()
        {
            base.OnUpdateObject();
            this.objEntityXWithholding.WithholdingTypeCode = objEntityXWithholding.Context.LocalCache.GetCodeId("BU", "WITHHOLDING_TYPE");
        }

        public override void OnUpdateForm()
        {
            base.OnUpdateForm();
            XmlElement objSysFormPForm = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysFormPForm");
            if (objSysFormPForm != null)
            {
                //mdhamija:R8 Withholding Enhancement, MITS 26019 
                switch(objSysFormPForm.InnerXml)
                {
                    case "orghierarchy":
                        SecurityId = RMO_ORGHIERARCHYMAINT_BACKUPWITHHOLDING;
                        break;
                        //nsachdeva2 - MITS: 27844 - 03/15/2012
                    //case"entity":
                    case "entitymaint":
                        //End MITS: 27844
                        SecurityId = RMO_ENTITYMAINT_BACKUPWITHHOLDING;
                        break;
                    case "employee":
                        SecurityId = RMO_EMPMAINT_BACKUPWITHHOLDING;
                        break;
                    case "physician":
                        SecurityId = RMO_PHYSICIANMAINT_BACKUPWITHHOLDING;
                        break;
                    case "people":
                        SecurityId = RMO_PEOPLEMAINT_BACKUPWITHHOLDING;
                        break;
                    case "staff":
                        SecurityId = RMO_STAFFMAINT_BACKUPWITHHOLDING;
                        break;
                    case "patient" :
                        SecurityId = RMO_PATIENTMAINT_BACKUPWITHHOLDING;
                        break;
                    //Achla MITS 31283--Start
                    case "driver":
                        SecurityId = RMO_DRIVERMAINT_BACKUPWITHHOLDING;
                        break;
                    //Achla MITS 31283--End
                }
            }
            //mdhamija: end
            XmlElement objSysFormId = (XmlElement)base.Adaptor.SafeXPath(@"//ParamList/Param[@name='SysFormId']");
            if (objSysFormId != null)
            {
                if (!base.objData.IsNew)
                    objSysFormId.InnerText = this.objEntityXWithholding.WithholdingRowId.ToString();
            }

            XmlElement objSysSid = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSid");
            objSysSid.InnerText = SecurityId.ToString();

            if (!objEntityXWithholding.Context.RMUser.IsAllowedEx(SecurityId, RMO_VIEW))
            {
                m_CurrentAction = enumFormActionType.None;
                LogSecurityError(RMO_VIEW);
                return;
            }

        }

    }
}
