﻿
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Xml;


namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for AdjusterDatedText Screen.
	/// </summary>
	public class AdjusterDatedTextForm : DataEntryFormBase
	{
		const string CLASS_NAME = "AdjustDatedText";
		const string FILTER_KEY_NAME = "AdjRowId";

		private AdjustDatedText objAdjustDatedText{get{return objData as AdjustDatedText;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		
		public AdjusterDatedTextForm(FormDataAdaptor fda):base(fda)
		{
			this.m_ClassName = CLASS_NAME;
		}

        private string m_Caption = string.Empty;
            //nnorouzi; MITS 19478
        public override string GetCaption()
        {
            return base.GetCaption();
        }

		public override void InitNew()
		{
			base.InitNew();

			int iAdjusterRowId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			int iOpenedInPopup=base.GetSysExDataNodeInt("/SysExData/OpenedInPopup",false);
            //Mjain8(8/8/06) Added for MITS 7619:It is get adjusterrowid when called from comments  
            if (iAdjusterRowId <= 0)
            {
                XmlNode objSysExternalParam = base.SysEx.SelectSingleNode( "//SysExternalParam" );
                if( objSysExternalParam != null )
                {
                    iAdjusterRowId = base.GetSysExDataNodeInt("//SysExternalParam/AdjRowId");
                    base.ResetSysExData("OpenedInPopup", "1");
                }
                base.CreateSysExData("AdjRowId",iAdjusterRowId.ToString());
            }
            if (iAdjusterRowId > 0)
            {
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + iAdjusterRowId;
				objAdjustDatedText.AdjRowId=iAdjusterRowId;
			}
			//Mjain8 MITS 7619 end
		}


		public override void OnUpdateForm()
		{
            //MGaba2: MITS 15467:start
            //Module Security Permission was not working as security id was different each time adjuster 
            //is opened from different claim.Also taking claimid from object as data from sysex is not available.
            
            int iAdjusterRowId = objAdjustDatedText.AdjRowId;
            ClaimAdjuster oAdjuster = (ClaimAdjuster)objAdjustDatedText.Context.Factory.GetDataModelObject("ClaimAdjuster", false);
            oAdjuster.MoveTo(iAdjusterRowId);
            Claim objClaim1 = (Claim)objAdjustDatedText.Context.Factory.GetDataModelObject("Claim", false);
            objClaim1.MoveTo(oAdjuster.ClaimId);

            switch (objClaim1.LineOfBusCode) 
            {
                case 241:
                    m_SecurityId = RMO_GC_ADJ_DATED_TEXT;
                break;
                case 242:
                    m_SecurityId = RMO_VA_ADJ_DATED_TEXT;
                break;
                case 243:
                    m_SecurityId = RMO_WC_ADJ_DATED_TEXT;
                break;
                case 844:
                    m_SecurityId = RMO_DI_ADJ_DATED_TEXT;
                break;
                //Start-Mridul Bansal. 01/04/10. MITS#19314. Support for Property Claims.
                case 845:
                    m_SecurityId = RMO_PC_ADJ_DATED_TEXT;
                    break;
                //End-Mridul Bansal. 01/04/10. MITS#19314. Support for Property Claims.
            }

            //Setting Security Id for further permissions like attachment
            XmlElement objSysSid = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSid");
            objSysSid.InnerText = m_SecurityId.ToString();
            //MGaba2:MITS 16498:Start
            //Create new Permission wasnt working
            if (objAdjustDatedText.AdjDttextRowId == 0)
            {
                if (!objAdjustDatedText.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            else
            {
                //MGaba2:MITS 16498:End
                //View Permission wasnt working    
                if (!objClaim1.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    m_CurrentAction = enumFormActionType.None;
                    //XmlElement objSysSkipBindToControl = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSkipBindToControl");
                    //objSysSkipBindToControl.InnerText = "true";
                    LogSecurityError(RMO_VIEW);
                    return;
                }
            }          

            //MGaba2: MITS 15467:end

            string sDiaryMessage = string.Empty;
			base.OnUpdateForm();            

            ArrayList singleRow = null;


            //nnorouzi; MITS 19478
            m_Caption = this.GetCaption();

			//Pass this subtitle value to view (ref'ed from @valuepath).
			base.ResetSysExData("SubTitle", m_Caption);

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", m_Caption);
            base.m_ModifiedControls.Add(singleRow);



			#region "ToDo: check do we need this"
			//			XmlElement objNotes=null;
			//If g_Params.GetItem("notes") = "true" Then
			//    'this is being called as a popup from the comments button.  remove some buttons
			//    Set objXMLList = m_xmlDom.getElementsByTagName("form")
			//    If objXMLList.length > 0 Then
			//       Set xmlElem = objXMLList.Item(0)
			//    Else
			//        Set xmlElem = m_xmlDom.selectSingleNode("form")
			//    End If
			//    'remove "Back to Adjuster" and lookup button
			//    Set xmlElem2 = m_xmlDom.selectSingleNode("//toolbar/button[@type='lookup']")
			//    xmlElem2.parentNode.removeChild xmlElem2
			//    Set xmlElem2 = m_xmlDom.selectSingleNode("//button[@name='btnBack']")
			//    xmlElem2.parentNode.removeChild xmlElem2
			//    'add new close button
			//    Set xmlElem2 = m_xmlDom.createElement("buttonscript")
			//    xmlElem2.setAttribute "name", "btnClose"
			//    xmlElem2.setAttribute "title", "Close"
			//    xmlElem2.setAttribute "includefilename", "adjdatedtext.js"
			//    xmlElem2.setAttribute "functionname", "CloseAdjDatedText();"
			//    xmlElem.appendChild xmlElem2
			//    'add the "notes=true" parameter so this if block is entered after save
			//    Set xmlElem2 = m_xmlDom.createElement("internal")
			//    xmlElem2.setAttribute "name", "sys_param"
			//    xmlElem2.setAttribute "type", "hidden"
			//    xmlElem2.setAttribute "value", "notes"
			//    xmlElem.appendChild xmlElem2
			//    Set xmlElem2 = m_xmlDom.createElement("internal")
			//    xmlElem2.setAttribute "name", "notes"
			//    xmlElem2.setAttribute "type", "hidden"
			//    xmlElem2.setAttribute "value", "true"
			//    xmlElem.appendChild xmlElem2
			//End If
			#endregion
			
			if (objAdjustDatedText.AdjDttextRowId==0)
			{
				//set login name in enterd by user
				objAdjustDatedText.EnteredByUser=objAdjustDatedText.Context.RMUser.LoginName;
				//set today date in date entered
				objAdjustDatedText.DateEntered=Conversion.ToDbDate(System.DateTime.Now);;
				//set currebt time in time entered
				objAdjustDatedText.TimeEntered=System.DateTime.Now.ToShortTimeString();
			}

			//get adjuster flag from settings
			bool bDateAdjusterFlag=Conversion.ConvertStrToBool(objAdjustDatedText.Context.InternalSettings.SysSettings.DateAdjusterFlag.ToString());

			//Gather all controls who need to be read-only based on business rules.
			if (bDateAdjusterFlag)
			{
				base.AddReadOnlyNode("dateentered");
				base.AddReadOnlyNode("timeentered");
			}
            //MGaba2:MITS 15467
			//int iAdjusterRowId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			int iOpenedInPopup=base.GetSysExDataNodeInt("/SysExData/OpenedInPopup",false);

			//Handling the case where adjuster dated text is called from comments toolbar
			//Mjain8(8/8/06) Changed conditions MITS 7619
			if (iOpenedInPopup==1)
            {
            //    int iClaimId=0;
	
            //    iClaimId=base.GetSysExDataNodeInt("/SysExData/ClaimId",false);

            //    if (iClaimId == 0)
            //        iClaimId = objClaim1.ClaimId; //MGaba2:MITS 15467
            //        //iClaimId = Conversion.ConvertObjToInt(base.Adaptor.SafeParamText("SysFormId"), base.ClientId);

            //    string sClaimNumber = "";

            //    Claim objClaim = null;
            //    using (DataModelFactory objDataModelFactory = new DataModelFactory(base.Adaptor.userLogin))
            //    {
            //        objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
            //        objClaim.MoveTo(iClaimId);

            //        sClaimNumber = objClaim.ClaimNumber;
                    objAdjustDatedText.AdjRowId = objClaim1.CurrentAdjuster.AdjRowId;
            //    }
            //    //objDataModelFactory.Dispose();
            //    objClaim.Dispose();
				
            //    base.ResetSysExData("ClaimNumber",sClaimNumber);
            //    base.ResetSysExData("ClaimId",iClaimId.ToString());
                  base.AddKillNode("btnBack");
                  base.ResetSysExData("OpenedInPopup", "1");

            //    if (sClaimNumber.Trim()!=string.Empty)
            //        sCaption= " (" + sClaimNumber + ") " ;
            //    base.ResetSysExData("SubTitle",sCaption);
			}
			else
				base.AddKillNode("btnClose");
            //Claim objClaim1 = (Claim)objAdjustDatedText.Context.Factory.GetDataModelObject("Claim", false);
            //objClaim1.MoveTo(base.GetSysExDataNodeInt("/SysExData/ClaimId", false));
            if (objClaim1.EventId != 0 && objClaim1.PrimaryClaimant != null)
                sDiaryMessage = objClaim1.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
            else
                sDiaryMessage = "";
            base.ResetSysExData("DiaryMessage", sDiaryMessage);
            objClaim1.Dispose();

		}

		public override void BeforeSave(ref bool Cancel)
		{
			base.BeforeSave (ref Cancel);

			//get adjuster flag from settings
			bool bDateAdjusterFlag=Conversion.ConvertStrToBool(objAdjustDatedText.Context.InternalSettings.SysSettings.DateAdjusterFlag.ToString());
			if (bDateAdjusterFlag)
			{
				//set today date in date entered
				objAdjustDatedText.DateEntered=Conversion.ToDbDate(System.DateTime.Now);;
				//set currebt time in time entered
				objAdjustDatedText.TimeEntered=System.DateTime.Now.ToShortTimeString();
			}
		}

        public override void OnValidate(ref bool Cancel)
        {
            
            string sSql= string.Empty;
            bool bIsAdminGroup = false;
            bool bError = false;
                if (objAdjustDatedText.Context.RMDatabase.OrgSecFlag)
                {
                    sSql = "SELECT GROUP_NAME FROM ORG_SECURITY WHERE GROUP_ENTITIES LIKE '<ALL>' AND GROUP_ID = (SELECT DISTINCT GROUP_ID FROM ORG_SECURITY_USER WHERE USER_ID =  " + objAdjustDatedText.Context.RMUser.UserId + ")";
                    using (DbReader objReader = DbFactory.GetDbReader(objAdjustDatedText.Context.DbConn.ConnectionString, sSql))
                    {
                        if (objReader != null)
                        {
                            if (objReader.Read())
                            {
                                bIsAdminGroup = true;
                            }
                        }
                    }
                    if (!bIsAdminGroup && (objAdjustDatedText.TextTypeCode == 0))
                    {
                        Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                    Globalization.GetString("Validation.AddDatedTextType",base.ClientId), BusinessAdaptorErrorType.Error);
                        bError = true;
                    }
                }
                Cancel = bError;
        }
	}
}
