﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Unit Screen.
	/// </summary>
	public class UnitForm : DataEntryFormBase
	{
		const string CLASS_NAME = "UnitXClaim";
		const string FILTER_KEY_NAME = "ClaimId";
        //smahajan6 - MITS #18644 - 11/18/2009 : Start
        private int m_LOB;
		string sConnectionString = null;
        private int m_iClientId=0;//dvatsa-cloud
        //smahajan6 - MITS #18644 - 11/18/2009 : End
		private UnitXClaim UnitXClaim{get{return objData as UnitXClaim;}}
        private string m_Caption = string.Empty;
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
        public override string GetCaption()
        {
            return base.GetCaption();
        }
		/// <summary>
		/// Class constructor which initializes Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public UnitForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
            //smahajan6 - MITS #18644 - 11/18/2009 : Start
            m_LOB = fda.Factory.Context.LocalCache.GetCodeId("VA", "LINE_OF_BUSINESS");
            //smahajan6 - MITS #18644 - 11/18/2009 : End
			sConnectionString = fda.connectionString;
            m_iClientId = base.ClientId;//dvatsa-cloud
		}
		
		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();

			//set filter for ClaimId
			UnitXClaim.ClaimId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			if (UnitXClaim.ClaimId>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + UnitXClaim.ClaimId;

            this.m_ParentId=UnitXClaim.ClaimId;   
			
		}//end method InitNew()

		/// <summary>
		/// Overrides the base class OnUpdateForm method to handle ClaimNumber in the 
		/// instance data for ref binding.  
		/// </summary>
		/// <remarks>This instance data is specifically held in the SysExData section
		/// of the Instance document.
		/// </remarks>
		/// <example> 
		///		<control name="claimnumber" tabindex="2" type="id" ref="Instance/UI/FormVariables/SysExData/ClaimNumber"/>
		/// </example>
		public override void OnUpdateForm()
        {
            ArrayList singleRow = null;
            base.OnUpdateForm();

            //Update the Instance Document with the necessary SysExData
            base.CreateSysExData("ClaimNumber");
            //Added by Amitosh because m_ParentId is coming as  0 
            
                if(this.m_ParentId == 0)
                this.m_ParentId = UnitXClaim.ClaimId;
            
            // Mihika 12/12/2005. Defect no. 718
            // Killing the 'Financials' node in case 'Claim Level' Reserve Tracking is on
            Claim objClaim = (Claim)UnitXClaim.Context.Factory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(this.m_ParentId);
            if (objClaim.LineOfBusCode == UnitXClaim.Context.LocalCache.GetCodeId("VA", "LINE_OF_BUSINESS"))
            {
                //LOB settings
                Riskmaster.Settings.ColLobSettings objLOB = null;
                Riskmaster.Settings.LobSettings objLOBSettings = null;

                objLOB = new Riskmaster.Settings.ColLobSettings(UnitXClaim.Context.DbConn.ConnectionString, base.ClientId);
                objLOBSettings = objLOB[objClaim.LineOfBusCode];
                int iReserveTracking = objLOBSettings.ReserveTracking;

                if (iReserveTracking == 0)
                    base.AddKillNode("btnFinancials");

                objLOB = null;
                objLOBSettings = null;
            }
            m_Caption = this.GetCaption();
            //skhare7 perfromance chnages 
            //Pass this subtitle value to view (ref'ed from @valuepath).
            base.ResetSysExData("SubTitle", m_Caption);
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", m_Caption);
            base.m_ModifiedControls.Add(singleRow);
            //Added by amitosh for r8 enhancement of vehicleloss
            if (objClaim.LineOfBusCode == 241)
            {
                m_SecurityId = RMO_GC_VEHILCELOSS;

            }
            //Create new Permission wasnt working
            if (UnitXClaim.UnitRowId == 0)
            {
                if (!UnitXClaim.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            else
            {//View Permission wasnt working
                if (!UnitXClaim.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_VIEW);
                    return;
                }
            }

            //smahajan6 - MITS #18644 - 11/18/2009 : Start
            base.CreateSysExData("UseEnhPolFlag");
            base.CreateSysExData("PolicyIdEnh");
            base.CreateSysExData("CheckMaxUnits");

            if (UnitXClaim.Context.InternalSettings.ColLobSettings[m_LOB].UseEnhPolFlag == -1)
            {
                objClaim.MoveTo(UnitXClaim.ClaimId);

                base.ResetSysExData("UseEnhPolFlag", "-1");
                base.ResetSysExData("PolicyIdEnh", objClaim.PrimaryPolicyIdEnh.ToString());

                //Check for Primary Policy Attached to a claim.
                if (objClaim.PrimaryPolicyIdEnh <= 0 && UnitXClaim.Context.InternalSettings.ColLobSettings[m_LOB].IsPolicyFilter)
                {
                    Errors.Add(Globalization.GetString("ValidationError",base.ClientId), Globalization.GetString("Validation.CheckPolicyAttached",base.ClientId),
                                BusinessAdaptorErrorType.Message);//dvatsa
                }

                //Check to Prevent user from adding more than one vehicle unit
                if (CheckMaxUnits())
                {
                    base.ResetSysExData("CheckMaxUnits", "-1");
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.CheckMaxUnits", base.ClientId),
                                BusinessAdaptorErrorType.Message);//dvatsa
                }
            }
            else
            {
                base.ResetSysExData("UseEnhPolFlag", "0");
                base.ResetSysExData("PolicyIdEnh", "0");
                base.ResetSysExData("CheckMaxUnits", "0");
            }

            base.CreateSysExData("IsPolicyFilter");
            if (UnitXClaim.Context.InternalSettings.ColLobSettings[m_LOB].IsPolicyFilter)
            {
                base.ResetSysExData("IsPolicyFilter", "-1");
            }
            else
            {
                base.ResetSysExData("IsPolicyFilter", "0");
            }

            //smahajan6 - MITS #18644 - 11/18/2009 : End
			//asatishchand: Mits 32313 starts
            int iUseClaimProgressNotes = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ClmProgressNotesFlag;
            if (iUseClaimProgressNotes == 0)
            {
                base.AddKillNode("enhancednotes");
            }

            //asatishchand: Mits 32313 Ends.
            //added by neha goel MITS# 36916: PMC gap 7 CLUE fields- RMA - 5499
            if (!objClaim.Context.InternalSettings.SysSettings.UseCLUEReportingFields)
            {
                base.AddKillNode("ClueVehDisposition");
                base.AddKillNode("ClueVehRemovalInd");
            }
            else
            {
                base.AddDisplayNode("ClueVehDisposition");
                base.AddDisplayNode("ClueVehRemovalInd");                
            }
            //end by neha goel MITS#36916 PMC CLUE gap 7- RMA - 5499

            objClaim.Dispose();

            CheckAutoGenerateVin();

            // End - Defect no. 718

            //			if(UnitXClaim.ClaimId==0)
            //			{
            //				AfterAddNew();
            //			}

        }//end method OnUpdateForm()

		//Added by Amitosh for Mobile Adjuster
		public override void OnUpdateObject()
		{
			base.OnUpdateObject();
            //mkaran2 - MITS #23755 - 03/07/2013 : Start
            // When New Vehicle Unit is being created form Vehcile Accident Claim
            // Mapping Unit_X_Claim table fields to Vehicle Table ; not work for Vehicle Edition mode
            if (UnitXClaim.Vehicle.UnitId == 0)
            {
                UnitXClaim.Vehicle.LicenseNumber = UnitXClaim.LicenseNumber;
                UnitXClaim.Vehicle.StateRowId = UnitXClaim.StateRowId;
                UnitXClaim.Vehicle.UnitTypeCode = UnitXClaim.UnitTypeCode;
                UnitXClaim.Vehicle.HomeDeptEid = UnitXClaim.HomeDeptEid;
            }
            //mkaran2 - MITS #23755 - 03/07/2013 : Ends

            XmlElement objCaller = null;			

			objCaller = (XmlElement)base.FormVariables.SelectSingleNode("//caller");
			if (objCaller != null)
			{
                if (objCaller.InnerText == "MobileAdjuster" || objCaller.InnerText == "MobilityAdjuster")
                {
					string sSQL = " SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + base.GetSysExDataNodeText("//ClaimNumber") + "'";
					using (DbReader objReader = DbFactory.ExecuteReader(sConnectionString, sSQL))
					{
						if (objReader.Read())
						{
							UnitXClaim.ClaimId = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), base.ClientId);
							if (base.FormVariables.SelectSingleNode("//SysExData//ClaimId") != null)
							{
								base.FormVariables.SelectSingleNode("//SysExData//ClaimId").InnerText = objReader.GetValue("CLAIM_ID").ToString();

							}

						}
					}
				}
			}

            //smishra54: MITS 33706
            if (string.IsNullOrEmpty(UnitXClaim.Vehicle.VehDesc))
            {
                UnitXClaim.Vehicle.VehDesc = string.Concat(string.IsNullOrEmpty(UnitXClaim.Vehicle.Vin) ? string.Empty : string.Concat(UnitXClaim.Vehicle.Vin, " "),
                    string.IsNullOrEmpty(UnitXClaim.Vehicle.VehicleMake) ? string.Empty : string.Concat(UnitXClaim.Vehicle.VehicleMake, " "), 
                    string.IsNullOrEmpty(UnitXClaim.Vehicle.VehicleModel) ? string.Empty : string.Concat(UnitXClaim.Vehicle.VehicleModel, " "),
                    UnitXClaim.Vehicle.VehicleYear).Trim();
            }
            //smishra54: end
		}
		internal override bool ValidateRequiredViewFields(XmlDocument propertyStore)
		{
			CheckAutoGenerateVin();

			return base.ValidateRequiredViewFields (propertyStore);
		}

		private void CheckAutoGenerateVin()
		{
			//Bug No: 636,649,672. For Autogenerating Vin
			//Nikhil Garg		Dated: 20/Dec/2005
			XmlElement objNotReqNew = null;
			string sNotReqNew=string.Empty;

            //smahajan6 - MITS #18644 - 01/15/2010 : Start
            //To make Vehicle ID as required field , when Policy Management is activated for VA Claim  
            if (objData.Context.InternalSettings.SysSettings.AutoNumVin && 
                UnitXClaim.Context.InternalSettings.ColLobSettings[m_LOB].UseEnhPolFlag != -1)
			//if (objData.Context.InternalSettings.SysSettings.AutoNumVin)
            //smahajan6 - MITS #18644 - 01/15/2010 : End
            {
                //objNotReqNew=(XmlElement)base.SysView.SelectSingleNode("//internal[@name='SysNotReqNew']");
                //sNotReqNew=objNotReqNew.Attributes["value"].Value;
                objNotReqNew = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysNotReqNew");
                sNotReqNew = objNotReqNew.InnerText;

                if (sNotReqNew.IndexOf("vehvin") == -1)
                    sNotReqNew = sNotReqNew + "vehvin|";
                //objNotReqNew.Attributes["value"].Value=sNotReqNew;
                objNotReqNew.InnerText = sNotReqNew;

            }
		}

//		public override void BeforeSave(ref bool Cancel)
//		{
//			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
//			//originally from ScreenFlow\SysExData.
//			XmlDocument objXML = base.SysEx;
//			XmlNode objClaimId=null;
//			try{objClaimId = objXML.GetElementsByTagName("ClaimId")[0];}
//			catch{};
//			//Filter by this EventId if Present.
//			if(objClaimId !=null)
//				if(this.UnitXClaim.ClaimId ==0)
//					this.UnitXClaim.ClaimId = Conversion.ConvertStrToInteger(objClaimId.InnerText);
//
//			  
//			base.BeforeSave (ref Cancel);
//
//		}
//		public override void AfterAddNew()
//		{
//			base.AfterAddNew ();
//
//			XmlDocument objXML = base.SysEx;
//			XmlNode objClaimId=null;
//			try{objClaimId = objXML.GetElementsByTagName(FILTER_KEY_NAME)[0];}
//			catch{};
//			
//			if(objClaimId !=null)
//			{
//				UnitXClaim.ClaimId=Conversion.ConvertStrToInteger(objClaimId.InnerText);
//			}
//		}

		public override void BeforeAction(enumFormActionType eActionType, ref bool Cancel)
		{
			base.BeforeAction (eActionType, ref Cancel);
			switch(eActionType)
			{
				case enumFormActionType.MoveFirst:
				case enumFormActionType.MoveLast:
				case enumFormActionType.MoveTo:
					UnitXClaim.ClaimId=0;
					break;
			}
		}	
		
		public override void AfterAction(enumFormActionType eActionType)
		{
			base.AfterAction(eActionType);
			switch(eActionType)
			{
				case enumFormActionType.MoveFirst:
				case enumFormActionType.MoveLast:
				case enumFormActionType.MoveTo:
					if(UnitXClaim.ClaimId==0) //No record found - treat as new.
						UnitXClaim.ClaimId= this.m_ParentId;
					break;
			}
		}
        /// Name		    :   CheckMaxUnits
        /// Author		    :   smahajan6
        /// Date Created	:   11/18/2009
        /// MITS            :   18644
        /// <summary>
        /// Prevent user from adding more than one Vehicle Unit
        /// </summary>
        /// <returns>Boolean</returns>
        private bool CheckMaxUnits()
        {
            int MAX_UNITS = 1;
            int iCount = 0;
            bool bError = false;
            bool blnSuccess = false;
            string sSQL = string.Empty;
            
            //Validation Check Before Adding New Vehicle Unit
            if (UnitXClaim.UnitId <= 0)
            {
                sSQL = "SELECT COUNT(*) AS COUNT FROM UNIT_X_CLAIM WHERE CLAIM_ID = " + UnitXClaim.ClaimId;

                iCount = Conversion.CastToType<int>(UnitXClaim.Context.DbConnLookup.ExecuteScalar(sSQL).ToString(), out blnSuccess);
                if (iCount >= MAX_UNITS)
                {
                    bError = true;
                }
            }
            return bError;
        }
		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;
            //smahajan6 - MITS #18644 - 11/18/2009 : Start
            //Check to Prevent user from adding more than one vehicle unit
            if (UnitXClaim.Context.InternalSettings.ColLobSettings[m_LOB].UseEnhPolFlag == -1)
            {
                if(CheckMaxUnits())
                {
                    Errors.Add(Globalization.GetString("ValidationError",base.ClientId),Globalization.GetString("Validation.CheckMaxUnits",base.ClientId),
                        BusinessAdaptorErrorType.Error);//dvatsa

                    bError = true;
                }
            }
            //smahajan6 - MITS #18644 - 11/18/2009 : End

			// Perform data validation
			string sToday = Conversion.ToDbDate(System.DateTime.Now);
			//Nitesh:MITS 7709 vehicle year is not a required field.
			if(UnitXClaim.Vehicle.VehicleYear!=0)
			{
				if (UnitXClaim.Vehicle.VehicleYear>(System.DateTime.Now.Year+10) || UnitXClaim.Vehicle.VehicleYear<1900)
				{
					Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThanAndLessThanVehicleYear", base.ClientId), Globalization.GetString("Field.VehicleYear", base.ClientId), (System.DateTime.Now.Year + 10), 1900),
						BusinessAdaptorErrorType.Error);//dvatsa

					bError = true;
				}
			}
            // MGaba2 for MITS 15283: Start
            Claim objClaim = (Claim)UnitXClaim.Context.Factory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(this.m_ParentId);


            //There is a case when a record is edited and we are changing vehicle id to an already attached unit
            //in that scenario,UnitXClaim.UnitId to the unit id with which it was saved before and  UnitXClaim.Vehicle.UnitId
            //is the currently selected id.So we have two cases :new and edit 

            int iCurrentUnitId = UnitXClaim.Vehicle.UnitId;
            int iUnitId = UnitXClaim.UnitId;
            if ((iUnitId == 0 && iCurrentUnitId != 0) || (iUnitId != 0 && iCurrentUnitId != iUnitId))
            {
                foreach (UnitXClaim objUnit in objClaim.UnitList)
                {
                    if (objUnit.Vehicle.UnitId == iCurrentUnitId && objUnit.ClaimId == UnitXClaim.ClaimId)
                    {
                        // Unit Record already exists	
                        Cancel = true;
                        Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                            "Selected Unit already exists for this Claim.",
                            BusinessAdaptorErrorType.Error);
                        return;
                    }
                }
            }
            // MGaba2 for MITS 15283: End
			// Return true if there were validation errors
			Cancel = bError;
		}

        /// <summary>
        /// pmittal5 MITS:12239 09/12/08
        /// Overrides the base class AfterApplyLookupData method to populate fields of Vehicle when it is selected.
        /// </summary>
        public override void AfterApplyLookupData(XmlDocument objPropertyStore)
        {
            base.AfterApplyLookupData(objPropertyStore);

            XmlNode objUnitNode = (XmlNode)objPropertyStore.SelectSingleNode("//UnitXClaim");
            if (objUnitNode != null)
            {
                XmlNode objVehicleNode = (XmlNode)objPropertyStore.SelectSingleNode("//UnitXClaim/Vehicle");

                objUnitNode.SelectSingleNode("LicenseNumber").InnerText = objVehicleNode.SelectSingleNode("LicenseNumber").InnerText;
                objUnitNode.SelectSingleNode("HomeDeptEid ").Attributes["codeid"].Value = objVehicleNode.SelectSingleNode("HomeDeptEid ").Attributes["codeid"].Value;
                objUnitNode.SelectSingleNode("HomeDeptEid ").InnerText = objVehicleNode.SelectSingleNode("HomeDeptEid ").InnerText;
                objUnitNode.SelectSingleNode("StateRowId ").Attributes["codeid"].Value = objVehicleNode.SelectSingleNode("StateRowId ").Attributes["codeid"].Value;
                objUnitNode.SelectSingleNode("StateRowId ").InnerText = objVehicleNode.SelectSingleNode("StateRowId ").InnerText;
                objUnitNode.SelectSingleNode("UnitTypeCode ").Attributes["codeid"].Value = objVehicleNode.SelectSingleNode("UnitTypeCode ").Attributes["codeid"].Value;
                objUnitNode.SelectSingleNode("UnitTypeCode ").InnerText = objVehicleNode.SelectSingleNode("UnitTypeCode ").InnerText;

                objVehicleNode = null;
            }
            objUnitNode = null;
        }//End AfterApplyLookupData()
	}
}
