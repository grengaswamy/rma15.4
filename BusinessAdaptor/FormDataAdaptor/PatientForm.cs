/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 02/10/2014 | 34276  | abhadouria   | changes req for Entity Address Type
 * 02/11/2014 | 34276  | anavinkumars   | Enhancement to Entity related functionality in RMA 
 * 02/27/2015 | RMA-6865        | nshah28      | Swiss Re - Ability to set Effective Date_or_Expiration Date on Address
 **********************************************************************************************/
using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.DataModel;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.Application.SupportScreens;
using Riskmaster.BusinessAdaptor.Common;
using System.Collections.Generic;
using Riskmaster.Db; // MITS 34276 Added for using Database context
namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for Patient Screen.
	/// </summary>
	public class PatientForm : DataEntryFormBase
	{
		const string CLASS_NAME = "Patient";
		private Patient Patient{get{return objData as Patient;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "PatientId";
        //Added Rakhi for R7:Add Emp Data Elements
        const string INSTANCE_SYSEXDATA_PATH = "/Instance/UI/FormVariables/SysExData"; 
        string sOfficePhone = string.Empty;
        string sHomePhone = string.Empty;
        int iHomeType = 0;
        int iOfficeType = 0;
        //Added Rakhi for R7:Add Emp Data Elements
		//To track patientid
		private bool m_IsNew=false;
		/// <summary>
		/// Class constructor which initializes Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public PatientForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;

			/*if (objData == null)
				InitNew();*/
		}

		public override void InitNew()
		{
			base.InitNew();

            if (this.Patient.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                //this.Patient.PatientEid = base.GetSysExDataNodeInt("/SysExData/patentityid");
                (objData as INavigation).Filter = objData.PropertyNameToField("PatientEid") + "=" + base.GetSysExDataNodeInt("/SysExData/patentityid");
            }
		}
		public override void BeforeSave(ref bool bCancel)
		{
			m_IsNew=Patient.IsNew;
            //START rsushilaggar Entity Payment Approval(make the people entity approval status as pending approval) MITS 20606 05/05/2010
            if (Patient.Context.InternalSettings.SysSettings.UseEntityApproval)
            {
                if (Patient.PatientEntity.EntityApprovalStatusCode == 0)
                {
                    Patient.PatientEntity.EntityApprovalStatusCode = Patient.Context.InternalSettings.CacheFunctions.GetCodeIDWithShort("P", "ENTITY_APPRV_REJ");
                }
            }
            //End rsushilaggar
		}
		public override void AfterSave()
		{
            HipaaLog obj=new HipaaLog(Patient.Context.DbConn.ConnectionString, base.ClientId);
			string sFormName="";
			string sFromExists="";
			sFromExists=base.GetSysExDataNodeText("//SysExternalParamfrom");
			if (sFromExists==null || sFromExists=="")
			{
				sFormName="Patient Tracking";
			}
			else
			{
				sFormName="Search/Patient Tracking";
			}
			if (m_IsNew==false)
			{
				obj.LogHippaInfo(Patient.PatientEid,"0","0",sFormName,"UD",Patient.Context.RMUser.LoginName);
			}
		}
		public override void BeforeDelete(ref bool Cancel)
		{
			HipaaLog obj=new HipaaLog(Patient.Context.DbConn.ConnectionString, base.ClientId);
			string sFormName="";
			string sFromExists="";
			sFromExists=base.GetSysExDataNodeText("//SysExternalParamfrom");
			if (sFromExists==null || sFromExists=="")
			{
				sFormName="Patient Tracking";
			}
			else
			{
				sFormName="Search/Patient Tracking";
			}
			obj.LogHippaInfo(Patient.PatientEid,"0","0",sFormName,"DL",Patient.Context.RMUser.LoginName);
		}
		public override void BeforeApplyLookupData(ref bool Cancel, ArrayList arrToSaveFields)
		{
			arrToSaveFields.Add("EntityTableId");
            //Added Rakhi for Mits  18137:To Retain Initials after submit
            arrToSaveFields.Add("Abbreviation");
            //Added Rakhi for Mits  18137:To Retain Initials after submit
		}

        // Anjaneya : MITS 9678
        private void ApplyFormTitle()
        {
            //string sCaption= Entity.Context.LocalCache.GetUserTableName(Entity.EntityTableId) + 
            string sSubTitleText = string.Empty;
            ArrayList singleRow = null;
            //avipinsrivas start : Worked for JIRA - 7767
            if (Patient.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                int iEntityID = 0;

                if (base.SysEx.SelectSingleNode("//patentityid") != null && !string.IsNullOrEmpty(base.SysEx.SelectSingleNode("//patentityid").InnerText))
                {
                    bool bSuccess;
                    iEntityID = Conversion.CastToType<int>(base.SysEx.SelectSingleNode("//patentityid").InnerText, out bSuccess);
                    if (iEntityID > 0)
                        sSubTitleText += string.Concat(" [ ", Patient.Context.LocalCache.GetEntityLastFirstName(iEntityID), " ]");
                }
            }

            if (string.IsNullOrEmpty(sSubTitleText) && Patient != null)
                sSubTitleText = " [ " + Patient.Default + " ]";


            //avipinsrivas end
            //Pass this title value to view (ref'ed from @valuepath).
            base.ResetSysExData("SubTitle", sSubTitleText);

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", sSubTitleText);
            base.m_ModifiedControls.Add(singleRow);
        }
        // Anjaneya : MITS 9678 ends
		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		/*public override void InitNew()
		{
			base.InitNew();

			Patient.PatientId = this.m_ParentId;

		}//end method InitNew()*/
			
		/// <summary>
		/// Overrides the base class OnUpdateForm method to handle PiRowId in the 
		/// instance data for ref binding.  
		/// </summary>
		/// <remarks>This instance data is specifically held in the SysExData section
		/// of the Instance document.
		/// </remarks>
		/// <example> 
		///		<control name="pirowid" tabindex="2" type="id" ref="Instance/UI/FormVariables/SysExData/ClaimNumber"/>
		/// </example>
		public override void OnUpdateForm()
		{
            XmlDocument objXmlDoc = null;
            base.OnUpdateForm();
            //avipinsrivas start : Worked for JIRA - 7767
            if (Patient.Context.InternalSettings.SysSettings.UseEntityRole && Patient.PatientEid <= 0)
                base.ResetSysExData("existingrecord", "false");
            else
                base.ResetSysExData("existingrecord", "true");
            if (Patient.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (base.SysEx.SelectSingleNode("//patentityid") != null && !string.IsNullOrEmpty(base.SysEx.SelectSingleNode("//patentityid").InnerText))
                {
                    bool bSuccess;
                    Patient.PatientEid = Conversion.CastToType<int>(base.SysEx.SelectSingleNode("//patentityid").InnerText, out bSuccess);
                }
                else if (Patient.PatientEid > 0)
                    base.ResetSysExData("patentityid", Patient.PatientEid.ToString());

                //rkatyal4 : JIRA 15437 start
                XmlNode xNode = base.FormVariables.SelectSingleNode("//SysFormId");
                if (xNode != null)
                    xNode.InnerText = Patient.PatientEid.ToString();
                //rkatyal4 : JIRA 15437 end
            }
            //avipinsrivas end
            //Added Rakhi for R7:Add Emp Data Elements
            XmlNode objPhoneTypeList = base.SysEx.SelectSingleNode("//PhoneTypeList");
            XmlNode objPhoneNumbers = base.SysEx.SelectSingleNode("//PhoneNumbers");
            objXmlDoc = new XmlDocument();
            objXmlDoc = CommonForm.PopulatePhoneTypes(Patient.Context.DbConn.ConnectionString, ref iOfficeType, ref iHomeType,base.Adaptor.userLogin.objUser.NlsCode); //Aman ML Change
            if (objPhoneTypeList != null)
                base.SysEx.DocumentElement.RemoveChild(objPhoneTypeList);
            base.SysEx.DocumentElement.AppendChild(base.SysEx.ImportNode(objXmlDoc.SelectSingleNode("PhoneTypeList"), true));

            if (objPhoneNumbers == null || base.m_fda.SafeFormVariableParamText("SysCmd") == "8")
            {
                string sPhoneNumbers = PopulatePhoneNumbers();
                base.ResetSysExData("PhoneNumbers", sPhoneNumbers);
            }
            else
            {
                objPhoneTypeList = base.SysEx.SelectSingleNode("//PhoneTypeList");
                if (objPhoneTypeList != null)
                    CommonForm.SetPhoneNumbers(objPhoneNumbers, objPhoneTypeList);

            }
            if (base.SysEx.SelectSingleNode("//PhoneType1") == null)
            {
                base.ResetSysExData("PhoneType1", iOfficeType.ToString());
            }
            if (base.SysEx.SelectSingleNode("//PhoneNumber1") == null || base.m_fda.SafeFormVariableParamText("SysCmd") == "8")
            {
                base.ResetSysExData("PhoneNumber1", sOfficePhone);
            }
            if (base.SysEx.SelectSingleNode("//PhoneType2") == null)
            {
                base.ResetSysExData("PhoneType2", iHomeType.ToString());
            }
            if (base.SysEx.SelectSingleNode("//PhoneNumber2") == null || base.m_fda.SafeFormVariableParamText("SysCmd") == "8")
            {
                base.ResetSysExData("PhoneNumber2", sHomePhone);
            }
            //Added Rakhi for R7:Add Emp Data Elements
            ApplyFormTitle();
			if (Patient.PatientEid!=0)
			{
                string sFromExists=base.GetSysExDataNodeText("//SysExternalParam/from");
				HipaaLog objHipaa=new HipaaLog(Patient.Context.DbConn.ConnectionString, base.ClientId);
				string sFormName="";
				if (sFromExists!=null && sFromExists!="")
				{
					base.CreateSysExData("SysExternalParamfrom","Search");
				}
				sFromExists=base.GetSysExDataNodeText("//SysExternalParamfrom");
				if (sFromExists==null || sFromExists=="")
				{
					sFormName="Patient Tracking";
				}
				else
				{
					sFormName="Search/Patient Tracking";
				}
				if (m_IsNew)
				{
					objHipaa.LogHippaInfo(Patient.PatientEid,"0","0",sFormName,"NW",Patient.Context.RMUser.LoginName);
				}
				else if (base.m_fda.SafeFormVariableParamText("SysCmd")!="6" && base.m_fda.SafeFormVariableParamText("SysCmd")!="5" && base.m_fda.SafeFormVariableParamText("SysCmd")!="7" && base.m_fda.SafeFormVariableParamText("SysCmd")!="8" && base.m_fda.SafeFormVariableParamText("SysCmd")!="")
				{
					objHipaa.LogHippaInfo(Patient.PatientEid,"0","0",sFormName,"VW",Patient.Context.RMUser.LoginName);
				}
				objHipaa=null;
			}
			//Check for People Maintenance permission
			PeoplePermissionChecks4Other("patient", m_SecurityId+RMO_UPDATE);

            //Added Rakhi for R7:Add Employee Data Elements
            bool bMulAddresses = this.objData.Context.InternalSettings.SysSettings.UseMultipleAddresses && !this.objData.Context.InternalSettings.SysSettings.UseEntityRole;        //avipinsrivas start : Worked for JIRA - 7767
            if (bMulAddresses)
            {
                base.AddDisplayNode("addressesinfo");
                SetPrimaryAddressReadOnly();
                bool bIsFirstRecord = false;
                XmlDocument objEntityXAddressInfoXmlDoc = GetEntityXAddressInfo();
                XmlNode objOldEntityXAddressInfoNode = SysEx.DocumentElement.SelectSingleNode(objEntityXAddressInfoXmlDoc.DocumentElement.LocalName);
                if (objOldEntityXAddressInfoNode != null)
                {
                    SysEx.DocumentElement.RemoveChild(objOldEntityXAddressInfoNode);
                }
                XmlNode objEntityXAddressInfoNode =
                    SysEx.ImportNode(objEntityXAddressInfoXmlDoc.DocumentElement, true);
                SysEx.DocumentElement.AppendChild(objEntityXAddressInfoNode);

                if (SysEx.DocumentElement.SelectSingleNode("IsPostBack") == null)
                    CreateSysExData("IsPostBack");
                if (SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag") != null)
                {
                    if (SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag").InnerText == "true")
                    {
                        if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7")
                        {
                            base.ResetSysExData("IsPostBack", "1");
                        }
                    }
                }
                if (this.Patient.PatientEntity.EntityXAddressesList.Count == 0)
                    bIsFirstRecord = true;

                if (base.SysEx.SelectSingleNode("//PrimaryFlag") == null)
                    base.ResetSysExData("PrimaryFlag", "false");

                if (base.SysEx.SelectSingleNode("//PrimaryRow") == null)
                    base.ResetSysExData("PrimaryRow", "");

                if (base.SysEx.SelectSingleNode("//PrimaryAddressChanged") == null)
                    base.ResetSysExData("PrimaryAddressChanged", "false");

                base.ResetSysExData("EntityXAddressesInfoSelectedId", "");
                base.ResetSysExData("EntityXAddressesInfoGrid_RowDeletedFlag", "false");
                base.ResetSysExData("EntityXAddressesInfoGrid_RowAddedFlag", "false");
                base.ResetSysExData("UseMultipleAddresses", bMulAddresses.ToString());
                base.ResetSysExData("IsFirstRecord", bIsFirstRecord.ToString());
            }
            else
            {
                base.AddKillNode("addressesinfo");
                base.AddKillNode("EntityXAddressesInfoGrid");//Mits 22246:GridTitle and Buttons getting visible in topdown Layout
            }
            //if (base.SysEx.SelectSingleNode("//PopupGridRowsDeleted") == null || base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
            //    base.ResetSysExData("PopupGridRowsDeleted", "");

            XmlNode objRecordSummary = base.SysEx.SelectSingleNode("//IsRecordSummary");
            if (objRecordSummary != null && objRecordSummary.InnerText == "true")
            {
                CommonForm.DisplayPhoneNumbersRecordSummary(base.SysEx, base.SysView);
            }
            //Added Rakhi for R7:Add Employee Data Elements

            //MITS:34276 Starts-- Entity ID Type tab display
            XmlDocument objEntityIDTypeAsXmlDoc = GetEntityIdTypeInfoData();

            XmlNode objOldEntityIdTypeAsNode = SysEx.DocumentElement.SelectSingleNode(objEntityIDTypeAsXmlDoc.DocumentElement.LocalName);
            if (objOldEntityIdTypeAsNode != null)
            {
                SysEx.DocumentElement.RemoveChild(objOldEntityIdTypeAsNode);
            }
            XmlNode objEntityIdTypeAsNode = SysEx.ImportNode(objEntityIDTypeAsXmlDoc.DocumentElement, true);
            SysEx.DocumentElement.AppendChild(objEntityIdTypeAsNode);

            if (SysEx.DocumentElement.SelectSingleNode("IsPostBack") == null)
                CreateSysExData("IsPostBack");

            if (SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag") != null && SysEx.DocumentElement.SelectSingleNode("EntityXContactInfoGrid_RowDeletedFlag") != null && SysEx.DocumentElement.SelectSingleNode("EntityXEntityIDTypeGrid_RowDeletedFlag") != null)
            {
                if ((SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag").InnerText == "true") || (SysEx.DocumentElement.SelectSingleNode("EntityXContactInfoGrid_RowDeletedFlag").InnerText == "true") || (SysEx.DocumentElement.SelectSingleNode("EntityXEntityIDTypeGrid_RowDeletedFlag").InnerText == "true"))
                { //Parijat: Mits 9610
                    if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7")
                    {
                        base.ResetSysExData("IsPostBack", "1");
                    }
                }
            }

            base.ResetSysExData("EntityXEntityIDTypeSelectedId", "");
            base.ResetSysExData("EntityXEntityIDTypeGrid_RowDeletedFlag", "false");
            base.ResetSysExData("EntityXEntityIDTypeGrid_RowAddedFlag", "false");
            //MITS:34276 Ends

            //Start rsushilaggar Entity Payment Approval (only execute this block if the Use Entity approval check box is "off") MITS 20606 05/05/2010
            if (!Patient.Context.InternalSettings.SysSettings.UseEntityApproval)
            {
                base.AddKillNode("entityapprovalstatuscode");
                base.AddKillNode("rejectreasontext");
            }
            //End rsushilaggar
            //smishra25: MITS 24395, MITS 13748 implementation was missing
            //nadim for 13748,added to hide/unhide SSN field
            if (!Patient.Context.RMUser.IsAllowedEx(FormBase.RMO_PATIENT, FormBase.RMO_MAINT_VIEW_SSN))
            {
                base.AddKillNode("pattaxid");

            }
            //nadim for 13748

			//Raman Bhatia - Acrosoft Phase 1 changes.. Document Management Not available for Employee if Acrosoft is enabled
			bool bUseAcrosoftInterface = Patient.Context.InternalSettings.SysSettings.UseAcrosoftInterface;
            bool bUsePaperVision = Patient.Context.InternalSettings.SysSettings.UsePaperVisionInterface;//Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            //rbhatia4:R8: Use Media View Setting : July 11 2011
            //asingh263 mits 30117 starts
            RMConfigurationManager.GetAcrosoftSettings();
            bool bUseRMADocumentManagement = !object.ReferenceEquals(AcrosoftSection.UseRMADocumentManagementForEntity, null) && AcrosoftSection.UseRMADocumentManagementForEntity.ToLower().Equals("true"); 
            if ((bUseAcrosoftInterface && !bUseRMADocumentManagement) || bUsePaperVision || Patient.Context.InternalSettings.SysSettings.UseMediaViewInterface)
                //asingh263 mits 30117 ends
            {
                base.AddKillNode("attachdocuments");//Mona:PaperVisionMerge:Animesh Inserted MITS 16697
                base.AddKillNode("attach");
            }

            //MITS:34276 Starts -- Entity ID Type hide when no view permission
            if (!Patient.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_VIEW))
            {
                base.AddKillNode("TABSentityidtypeinfo");
                base.AddKillNode("TBSPentityidtypeinfo");
            }
            //MITS:34276 Ends -- Entity ID Type hide when no view permission

            //Added by Manika for R8 enhancement of EFT and Withholding
            if (!Patient.Context.RMUser.IsAllowedEx(FormBase.RMO_PATIENTMAINT_EFT))
            {
                base.AddKillNode("BankingInfo");
            }
            if (!Patient.Context.RMUser.IsAllowedEx(FormBase.RMO_PATIENTMAINT_WITHHOLDING))
            {
                base.AddKillNode("Withholding");
            }
            //End Manika

            ERSetting();            //avipinsrivas start : Worked for JIRA - 7767
		}
        //avipinsrivas start : Worked for JIRA - 7767
        private void ERSetting()
        {
            if (Patient.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                //Toolbars
                base.AddKillNode("lookup");
                base.AddKillNode("attach");
                base.AddKillNode("diary");
                //base.AddKillNode("mailmerge");
                //base.AddKillNode("recordsummary");        //avipinsrivas Start : Worked for 13947 (Issue of 13196 - Epic 7767)
                base.AddKillNode("withholding");
                base.AddKillNode("BankingInfo");
                //End Toolbars

                //Controls
                //First Tab Start
                base.AddKillNode("patlastname");
                base.AddKillNode("pattaxid");
                base.AddKillNode("patfirstname");
                base.AddKillNode("pattitle");
                base.AddKillNode("patmiddlename");
                base.AddKillNode("patbirthdate");
                base.AddKillNode("patalsoknownas");
                base.AddKillNode("patfaxnumber");
                base.AddKillNode("pataddr1");
                base.AddKillNode("patabbreviation");
                base.AddKillNode("pataddr2");
                base.AddKillNode("patsexcode");
                base.AddKillNode("pataddr3");
                base.AddKillNode("pataddr4");
                base.AddKillNode("patzipcode");
                base.AddKillNode("patcity");
                base.AddKillNode("patcountrycode");
                base.AddKillNode("patstateid");
                base.AddKillNode("patcounty");
                base.AddKillNode("entityapprovalstatuscode");
                base.AddKillNode("rejectreasontext");
                //avipinsrivas Start : Worked for 13953 (Issue of 13196 - Epic 7767)
                base.AddKillNode("patphone1");
                base.AddKillNode("patphone2");
                //avipinsrivas end
                //First Tab End
                //End Controls

                //Tabs
                base.AddKillNode("addressesinfo");
                base.AddKillNode("entityidtypeinfo");
                //End Tabs
            }
            else
                base.AddKillNode("BackToParent");
        }
        //avipinsrivas End
        public override void OnUpdateObject()
        {
            base.OnUpdateObject();
            //Added Rakhi for R7:Add Emp Data Elements
            #region "Updating Address Info Object"
            bool bMulAddresses = this.objData.Context.InternalSettings.SysSettings.UseMultipleAddresses;
            //MITS:34276 Starts-- changes for Entity ID Type
            XmlAttribute objTypeAttributeNode = null;
            bool bIsNew = false;
            //MITS:34276 Ends
            //avipinsrivas start : Worked for JIRA - 7767
            if (!this.objData.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (bMulAddresses)
            {
                SortedList objEntityXAddressesInfoList = new SortedList();
                SortedList objAddressesInfoList = new SortedList();
                //XmlDocument objAddressXPhoneInfoDoc = null;
                bool bIsSuccess = false;
                //MITS:34276 starts-- commented and shifted up 
                //XmlAttribute objTypeAttributeNode = null;
                //bool bIsNew = false;
                //MITS:34276 ends
                XmlDocument objEntityXAddressXmlDoc = null;
                XmlElement objEntityXAddressRootElement = null;
                int iAddressIdKey = -1;
                int iRowIdKey = -1; //AA//RMA-8753 nshah28(Added by ashish)

                int iEntityXAddressesInfoSelectedId =
                    base.GetSysExDataNodeInt("EntityXAddressesInfoSelectedId", true);

                string sEntityXAddressesInfoRowAddedFlag =
                    base.GetSysExDataNodeText("EntityXAddressesInfoGrid_RowAddedFlag", true);
                bool bEntityXAddressesInfoRowAddedFlag =
                    sEntityXAddressesInfoRowAddedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

                string sEntityXAddressesInfoRowDeletedFlag =
                    base.GetSysExDataNodeText("EntityXAddressesInfoGrid_RowDeletedFlag", true);
                bool bEntityXAddressesInfoRowDeletedFlag =
                    sEntityXAddressesInfoRowDeletedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

                XmlNode objEntityXAddressesInfoNode = base.SysEx.DocumentElement.SelectSingleNode("EntityXAddressesInfo");

                if (objEntityXAddressesInfoNode != null)
                {
                    // Loop through data for all rows of the grid
                    foreach (XmlNode objOptionNode in objEntityXAddressesInfoNode.SelectNodes("option"))
                    {
                        objTypeAttributeNode = objOptionNode.Attributes["type"];
                        bIsNew = false;
                        if (objTypeAttributeNode != null && objTypeAttributeNode.InnerText.Equals("new") == true)
                        {
                            // This is the 'extra' hidden <option> for capturing a new grid row data
                            bIsNew = true;
                        }
                        if ((bIsNew == false) || (bIsNew == true && bEntityXAddressesInfoRowAddedFlag == true))
                        {
                            objEntityXAddressXmlDoc = new XmlDocument();
                            objEntityXAddressRootElement = objEntityXAddressXmlDoc.CreateElement("EntityXAddresses");
                            objEntityXAddressXmlDoc.AppendChild(objEntityXAddressRootElement);

                            #region AddressXPhone Info Grid
                            //XmlNode objAddressNode = objOptionNode.SelectSingleNode("AddressXPhoneInfo");
                            //if (objOptionNode != null && !(bIsNew == false && bEntityXAddressesInfoRowDeletedFlag == true && iAddressIdKey == iEntityXAddressesInfoSelectedId))
                            //{

                            //    XmlElement objAddressXPhoneRootElement = null;
                            //    int iPhoneId = 0;
                            //    bool bResult = false;
                            //    string sKey = string.Empty;
                            //    foreach (XmlNode objPhoneNode in objAddressNode.SelectNodes("option"))
                            //    {
                            //        objAddressXPhoneInfoDoc = new XmlDocument();
                            //        objAddressXPhoneRootElement = objAddressXPhoneInfoDoc.CreateElement("AddressXPhoneInfo");
                            //        objAddressXPhoneInfoDoc.AppendChild(objAddressXPhoneRootElement);
                            //        objPhoneNode.SelectSingleNode("ContactId").InnerText = objOptionNode.SelectSingleNode("ContactId").InnerText;
                            //        objAddressXPhoneRootElement.InnerXml = objPhoneNode.InnerXml;
                            //        bResult = System.Int32.TryParse(objAddressXPhoneRootElement.SelectSingleNode("PhoneId").InnerText, out iPhoneId);
                            //        if (iPhoneId != 0)
                            //        {
                            //            sKey = objPhoneNode.SelectSingleNode("ContactId").InnerText + "_" + objAddressXPhoneRootElement.SelectSingleNode("PhoneId").InnerText;
                            //            objAddressesInfoList.Add(sKey, objAddressXPhoneInfoDoc);
                            //        }
                            //    }
                            //}
                            //objOptionNode.RemoveChild(objAddressNode);
                            #endregion //AddressXPhone Info Grid

                            objEntityXAddressRootElement.InnerXml = objOptionNode.InnerXml;

                            //RMA-8753 nshah28 start(Added by ashish)
                            string strSearchString = string.Empty;
                            string sStateCode = string.Empty;
                            string sStateDesc = string.Empty;
                            string sCountry = string.Empty;
                            AddressForm objAddressForm = new AddressForm(m_fda);
                            strSearchString = objAddressForm.CreateSearchString(objEntityXAddressRootElement, objData.Context.LocalCache);

                            XmlElement objRootElement = objEntityXAddressXmlDoc.CreateElement("SearchString");
                            objRootElement.InnerText = strSearchString; //change by nshah28
                            objEntityXAddressRootElement.AppendChild(objRootElement);

                            iRowIdKey =
                                Conversion.CastToType<int>(objEntityXAddressRootElement.SelectSingleNode("RowId").InnerText, out bIsSuccess);
                            //RMA-8753 nshah28 End(Added by ashish)

                            //iAddressIdKey =
                            //    Conversion.CastToType<int>(objEntityXAddressRootElement.SelectSingleNode("AddressId").InnerText, out bIsSuccess);

                            if ((bIsNew == true && bEntityXAddressesInfoRowAddedFlag == true) ||
                                (iAddressIdKey < 0 && objEntityXAddressRootElement.SelectSingleNode("EntityId").InnerText == ""))
                                objEntityXAddressRootElement.SelectSingleNode("EntityId").InnerText = "0";
                            else
                                objEntityXAddressRootElement.SelectSingleNode("EntityId").InnerText = Patient.PatientEid.ToString();  //RMA-8753 nshah28(Added by ashish)
                            //Marking Changed Address as Primary Address
                            XmlNode objPrimaryAddressChanged = base.SysEx.SelectSingleNode("//PrimaryAddressChanged");
                            XmlNode objNewPrimaryAddress = objEntityXAddressRootElement.SelectSingleNode("NewPrimaryRecord"); //For identification of New Primary Address
                            if (objPrimaryAddressChanged != null && objPrimaryAddressChanged.InnerText.ToLower() == "true")
                            {
                                if (objNewPrimaryAddress != null)
                                {
                                    XmlNode objPrimaryAddress = objEntityXAddressRootElement.SelectSingleNode("PrimaryAddress");
                                    if (objPrimaryAddress != null)
                                    {
                                        if (objNewPrimaryAddress.InnerText.ToLower() == "true")
                                        {
                                            objPrimaryAddress.InnerText = "True";
                                        }

                                        else
                                        {
                                            objPrimaryAddress.InnerText = "False";
                                        }
                                    }
                                }
                            }
                            if (objNewPrimaryAddress != null)
                            {
                                objEntityXAddressRootElement.RemoveChild(objNewPrimaryAddress); //Since it is not a Datamodel Property,Removing it before the processing.
                            }
                            //Marking Changed Address as Primary Address
                            //RMA-8753 nshah28 Start(Added by ashish)
                            if (bIsNew == false && bEntityXAddressesInfoRowDeletedFlag == true && iRowIdKey == iEntityXAddressesInfoSelectedId)
                            {
                                // Aditya - This particular Tpp has been deleted. 
                                continue;
                            }
                            objEntityXAddressesInfoList.Add(iRowIdKey, objEntityXAddressXmlDoc);
                            //RMA-8753 nshah28 End(Added by ashish)
                        }
                    }
                }

               /* foreach (EntityXAddresses objEntityXAddressesInfo in Patient.PatientEntity.EntityXAddressesList)
                {
                    if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7" &&
                        (objEntityXAddressesInfo.AddressId < 0 || (bEntityXAddressesInfoRowDeletedFlag == true && objEntityXAddressesInfo.AddressId == iEntityXAddressesInfoSelectedId)))
                    {
                        Patient.PatientEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                    }
                    else if (objEntityXAddressesInfo.AddressId < 0)
                        Patient.PatientEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                } */

                //RMA-8753 nshah28 Start(Added by ashish)
                foreach (EntityXAddresses objEntityXAddressesInfo in Patient.PatientEntity.EntityXAddressesList)
                {
                    if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7" &&
                        (objEntityXAddressesInfo.RowId < 0 || (bEntityXAddressesInfoRowDeletedFlag == true && objEntityXAddressesInfo.RowId == iEntityXAddressesInfoSelectedId)))
                    {
                        Patient.PatientEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId);
                    }
                    else if (objEntityXAddressesInfo.RowId < 0)
                        Patient.PatientEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId);
                }  //RMA-8753 nshah28 End(Added by ashish)
                EntityXAddresses objTmpEntityXAddressesInfo = null;
                XmlDocument objTmpEntityXAddressXMLDoc = null;
                //AddressXPhoneInfo objTmpAddressXPhoneInfo = null;
                //XmlDocument objTmpAddressXPhoneXMLDoc = null;
                //string sOrgAddressId = string.Empty;
                //Parijat:Mits 9610
                ArrayList arrAddressIds = new ArrayList();
                ArrayList arrRowIds = new ArrayList();  //RMA-8753 nshah28(Added by ashish)
                //
                for (int iListIndex = 0; iListIndex < objEntityXAddressesInfoList.Count; iListIndex++)
                {
                    objTmpEntityXAddressXMLDoc = (XmlDocument)objEntityXAddressesInfoList.GetByIndex(iListIndex);
                    //sOrgAddressId = objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("AddressId").InnerText.Trim();
                    if (objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText.Trim() == "" || Conversion.CastToType<int>(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText, out bIsSuccess) < 0)
                    {
                        objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText = ((-1 * iListIndex) - 1).ToString();
                        objTmpEntityXAddressesInfo = Patient.PatientEntity.EntityXAddressesList.AddNew();
                    }
                    else
                        objTmpEntityXAddressesInfo = Patient.PatientEntity.EntityXAddressesList[Conversion.CastToType<int>(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText, out bIsSuccess)];
                    //Parijat:Mits 9610
                    arrAddressIds.Add(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("AddressId").InnerText);
                    arrRowIds.Add(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText);//RMA-8753 nshah28(Added by ashish)
                  /*  if(objTmpEntityXAddressesInfo!=null)
                        objTmpEntityXAddressesInfo.PopulateObject(objTmpEntityXAddressXMLDoc); */
                    //RMA 8753 Ashish Ahuja For Address Master Enhancement Start
                    if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//RowId").InnerText != String.Empty)
                        objTmpEntityXAddressesInfo.RowId = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//RowId").InnerText);
                    if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//AddressType").Attributes["codeid"].Value != String.Empty)
                        objTmpEntityXAddressesInfo.AddressType = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//AddressType").Attributes["codeid"].Value);
                    if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//PrimaryAddress").InnerText != String.Empty)
                        objTmpEntityXAddressesInfo.PrimaryAddress = Convert.ToBoolean(objTmpEntityXAddressXMLDoc.SelectSingleNode("//PrimaryAddress").InnerText) ? -1 : 0;
                    //objTmpEntityXAddressesInfo.PrimaryAddress = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//PrimaryAddress").InnerText);
                    objTmpEntityXAddressesInfo.Address.Addr1 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr1").InnerText;
                    objTmpEntityXAddressesInfo.Address.Addr2 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr2").InnerText;
                    objTmpEntityXAddressesInfo.Address.Addr3 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr3").InnerText;
                    objTmpEntityXAddressesInfo.Address.Addr4 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr4").InnerText;
                    objTmpEntityXAddressesInfo.Address.City = objTmpEntityXAddressXMLDoc.SelectSingleNode("//City").InnerText;
                    if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//Country").Attributes["codeid"].Value != String.Empty)
                        objTmpEntityXAddressesInfo.Address.Country = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//Country").Attributes["codeid"].Value);
                    if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//State").Attributes["codeid"].Value != String.Empty)
                        objTmpEntityXAddressesInfo.Address.State = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//State").Attributes["codeid"].Value);
                    objTmpEntityXAddressesInfo.Email = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Email").InnerText;
                    objTmpEntityXAddressesInfo.Fax = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Fax").InnerText; ;
                    objTmpEntityXAddressesInfo.Address.County = objTmpEntityXAddressXMLDoc.SelectSingleNode("//County").InnerText;
                    objTmpEntityXAddressesInfo.Address.ZipCode = objTmpEntityXAddressXMLDoc.SelectSingleNode("//ZipCode").InnerText;
                    objTmpEntityXAddressesInfo.EffectiveDate = objTmpEntityXAddressXMLDoc.SelectSingleNode("//EffectiveDate").InnerText;
                    objTmpEntityXAddressesInfo.ExpirationDate = objTmpEntityXAddressXMLDoc.SelectSingleNode("//ExpirationDate").InnerText;

                    objTmpEntityXAddressesInfo.Address.SearchString = objTmpEntityXAddressXMLDoc.SelectSingleNode("//SearchString").InnerText;
                    int iAddressId = CommonFunctions.CheckAddressDuplication(objTmpEntityXAddressXMLDoc.SelectSingleNode("//SearchString").InnerText, Patient.Context.RMDatabase.ConnectionString, base.ClientId);
                    objTmpEntityXAddressesInfo.AddressId = iAddressId;
                    objTmpEntityXAddressesInfo.Address.AddressId = iAddressId;
                    //RMA 8753 Ashish Ahuja For Address Master Enhancement End

                    #region Phone Info Details
                    //int iPhoneIndex = 0;
                    //for (int iCount = 0; iCount < objAddressesInfoList.Count; iCount++)
                    //{
                    //    objTmpAddressXPhoneXMLDoc = (XmlDocument)objAddressesInfoList.GetByIndex(iCount);
                    //    if (sOrgContactId == objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("ContactId").InnerText.Trim())
                    //    {
                    //        if (objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText.Trim() == "" || Conversion.ConvertStrToInteger(objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText) < 0)
                    //        {
                    //            objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText = ((-1 * iPhoneIndex) - 1).ToString();
                    //            objTmpAddressXPhoneInfo = objTmpEntityXAddressesInfo.AddressXPhoneInfoList.AddNew();
                    //            iPhoneIndex++;
                    //        }
                    //        else
                    //            objTmpAddressXPhoneInfo = objTmpEntityXAddressesInfo.AddressXPhoneInfoList[Conversion.ConvertStrToInteger(objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText)];
                    //        objTmpAddressXPhoneInfo.PopulateObject(objTmpAddressXPhoneXMLDoc);
                    //        objTmpAddressXPhoneInfo.ContactId = objTmpEntityXAddressesInfo.ContactId;
                    //    }
                    //}
                    #endregion //Phone Info Details

                    #region Added code to remove the saved phone numbers
                    //if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
                    //{
                    //    XmlNode objPhoneNumbersDeleted = base.SysEx.SelectSingleNode("//PopupGridRowsDeleted");
                    //    if (objPhoneNumbersDeleted != null && !String.IsNullOrEmpty(objPhoneNumbersDeleted.InnerText))
                    //    {
                    //        string[] strSplit = objPhoneNumbersDeleted.InnerText.Split(new Char[] { '|' });
                    //        foreach (AddressXPhoneInfo objAddressXPhoneInfo in objTmpEntityXAddressesInfo.AddressXPhoneInfoList)
                    //        {
                    //            foreach (string sPhoneNumber in strSplit)
                    //            {
                    //                int iPhoneId = Conversion.ConvertStrToInteger(sPhoneNumber);
                    //                if (iPhoneId == objAddressXPhoneInfo.PhoneId)
                    //                {
                    //                    objTmpEntityXAddressesInfo.AddressXPhoneInfoList.Remove(iPhoneId);
                    //                    break;
                    //                }

                    //            }
                    //        }
                    //    }
                    //}
                    #endregion //Added code to remove the saved phone numbers
                }
                //Parijat:Mits 9610
                if (base.m_fda.SafeFormVariableParamText("SysCmd") != "8")
                {
                    foreach (EntityXAddresses objEntityXAddressesInfo in Patient.PatientEntity.EntityXAddressesList)
                    {
                       /* if (!arrAddressIds.Contains(objEntityXAddressesInfo.AddressId.ToString()))
                        {
                            Patient.PatientEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                        } */
                        //RMA-8753 nshah28 Start(Added by ashish)
                        if (!arrRowIds.Contains(objEntityXAddressesInfo.RowId.ToString()))
                        {
                            Patient.PatientEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId);
                        }
                        //RMA-8753 nshah28 End(Added by ashish)
                        
                    }
                }
            }
            else
            {
                if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
                {
                    string sAddr1 = Patient.PatientEntity.Addr1;
                    string sAddr2 = Patient.PatientEntity.Addr2;
                    string sAddr3 = Patient.PatientEntity.Addr3;// JIRA 6420 pkandhari
                    string sAddr4 = Patient.PatientEntity.Addr4;// JIRA 6420 pkandhari
                    string sCity = Patient.PatientEntity.City;
                    int iCountryCode = Patient.PatientEntity.CountryCode;
                    int iStateId = Patient.PatientEntity.StateId;
                    string sEmailAddress = Patient.PatientEntity.EmailAddress;
                    string sFaxNumber = Patient.PatientEntity.FaxNumber;
                    string sCounty = Patient.PatientEntity.County;
                    string sZipCode = Patient.PatientEntity.ZipCode;
                    //RMA-8753 nshah28 start(Added by ashish)
                    string sSearchString = string.Empty;
                    //RMA-8753 nshah28 end(Added by ashish)
                    if (Patient.PatientEntity.EntityXAddressesList.Count > 0)
                    {
                        foreach (EntityXAddresses objEntityXAddressesInfo in Patient.PatientEntity.EntityXAddressesList)
                        {
                            if (objEntityXAddressesInfo.PrimaryAddress == -1)
                            {
								//RMA-8753 nshah28(Added by ashish) START
                                objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                                objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                                objEntityXAddressesInfo.Address.Addr3 = sAddr3;// JIRA 6420 pkandhari
                                objEntityXAddressesInfo.Address.Addr4 = sAddr4;// JIRA 6420 pkandhari
                                objEntityXAddressesInfo.Address.City = sCity;
                                objEntityXAddressesInfo.Address.Country = iCountryCode;
                                objEntityXAddressesInfo.Address.State = iStateId;
                                objEntityXAddressesInfo.Email = sEmailAddress;
                                objEntityXAddressesInfo.Fax = sFaxNumber;
                                objEntityXAddressesInfo.Address.County = sCounty;
                                objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                                AddressForm objAddressForm = new AddressForm(m_fda);
                                sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                                objEntityXAddressesInfo.Address.SearchString = sSearchString;
                                objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, Patient.Context.RMDatabase.ConnectionString, base.ClientId);
                                objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
								//RMA-8753 nshah28(Added by ashish) END
                                break;
                            }

                        }
                    }
                    else
                    {
                        if (
                                sAddr1 != string.Empty || sAddr2 != string.Empty || sAddr3 != string.Empty || sAddr4 != string.Empty ||
                                sCity != string.Empty || iCountryCode != 0 || iStateId != 0 || sEmailAddress != string.Empty ||
                                sFaxNumber != string.Empty || sCounty != string.Empty || sZipCode != string.Empty
                            )
                        {
                            EntityXAddresses objEntityXAddressesInfo = Patient.PatientEntity.EntityXAddressesList.AddNew();
							//RMA-8753 nshah28(Added by ashish) START
                            objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                            objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                            objEntityXAddressesInfo.Address.Addr3 = sAddr3;// JIRA 6420 pkandhari
                            objEntityXAddressesInfo.Address.Addr4 = sAddr4;// JIRA 6420 pkandhari
                            objEntityXAddressesInfo.Address.City = sCity;
                            objEntityXAddressesInfo.Address.Country = iCountryCode;
                            objEntityXAddressesInfo.Address.State = iStateId;
                            objEntityXAddressesInfo.Email = sEmailAddress;
                            objEntityXAddressesInfo.Fax = sFaxNumber;
                            objEntityXAddressesInfo.Address.County = sCounty;
                            objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                            AddressForm objAddressForm = new AddressForm(m_fda);
                            sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                            objEntityXAddressesInfo.Address.SearchString = sSearchString;
                            objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, Patient.Context.RMDatabase.ConnectionString, base.ClientId);
                            objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
							//RMA-8753 nshah28(Added by ashish) END
                            objEntityXAddressesInfo.EntityId = Patient.PatientEntity.EntityId;
                            objEntityXAddressesInfo.PrimaryAddress = -1;
                            //objEntityXAddressesInfo.AddressId = -1;
                        }
                    }
                }
            }
            }
            //avipinsrivas end
            #endregion

            #region Update Phone Numbers
            if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
            {
                Patient.PatientEntity.FormName = "PatientForm"; //Mits 22497

                XmlNode objPhoneNumber = base.SysEx.SelectSingleNode("//PhoneNumbers");
                if (objPhoneNumber != null)
                {
                    string sPhoneData = objPhoneNumber.InnerText;
                    string[] sPhoneNumbers = sPhoneData.Split(new string[] { CommonForm.OUTER_PHONE_DELIMITER }, StringSplitOptions.None);
                    foreach (string sPhoneNumber in sPhoneNumbers)
                    {
                        string[] sNumber = sPhoneNumber.Split(new string[] { CommonForm.INNER_PHONE_DELIMITER }, StringSplitOptions.None);
                        if (sNumber.Length > 2)
                        {
                            string sPhoneId = sNumber[0];
                            string sPhoneType = sNumber[1];
                            string sPhoneNo = sNumber[2];
                            bool bPhoneCodeExists = false;
                            string sPhoneDesc = string.Empty;
                            string sPhoneShortCode = string.Empty;
                            if (Patient.PatientEntity.AddressXPhoneInfoList.Count > 0)
                            {
                                foreach (AddressXPhoneInfo objAddressXPhoneInfo in Patient.PatientEntity.AddressXPhoneInfoList)
                                {
                                    if (objAddressXPhoneInfo.PhoneCode == Convert.ToInt32(sPhoneType))
                                    {
                                        if (sPhoneNo.Trim() != string.Empty)
                                            objAddressXPhoneInfo.PhoneNo = sPhoneNo;
                                        else
                                            Patient.PatientEntity.AddressXPhoneInfoList.Remove(objAddressXPhoneInfo.PhoneId);

                                        bPhoneCodeExists = true;
                                        break;
                                    }

                                }
                                if (!bPhoneCodeExists && sPhoneNo.Trim() != string.Empty)
                                {
                                    AddressXPhoneInfo objAddressXPhoneInfo = Patient.PatientEntity.AddressXPhoneInfoList.AddNew();
                                    objAddressXPhoneInfo.PhoneId = Convert.ToInt32(sPhoneId);
                                    objAddressXPhoneInfo.PhoneCode = Convert.ToInt32(sPhoneType);
                                    objAddressXPhoneInfo.PhoneNo = sPhoneNo;
                                }
                            }
                            else
                            {
                                if (sPhoneNo.Trim() != string.Empty)
                                {
                                    AddressXPhoneInfo objAddressXPhoneInfo = Patient.PatientEntity.AddressXPhoneInfoList.AddNew();
                                    objAddressXPhoneInfo.PhoneId = Convert.ToInt32(sPhoneId);
                                    objAddressXPhoneInfo.PhoneCode = Convert.ToInt32(sPhoneType);
                                    objAddressXPhoneInfo.PhoneNo = sPhoneNo;
                                }
                            }
                            //Added to save Office and Alt phone numbers in Entity Table as well
                            //sPhoneDesc = this.objCache.GetCodeDesc(Convert.ToInt32(sPhoneType)).Trim().ToLower();
                            //{
                            //    if (sPhoneDesc == "office")
                            //        Patient.PatientEntity.Phone1 = sPhoneNo;
                            //    else if (sPhoneDesc == "home")
                            //        Patient.PatientEntity.Phone2 = sPhoneNo;
                            //}
                            //Added to save Office and Alt phone numbers in Entity Table as well
                            //Aman ML Change
                            sPhoneShortCode = this.objCache.GetShortCode(Convert.ToInt32(sPhoneType)).Trim().ToLower();
                            {
                                if (sPhoneShortCode == "o")
                                    Patient.PatientEntity.Phone1 = sPhoneNo;
                                else if (sPhoneShortCode == "h")
                                    Patient.PatientEntity.Phone2 = sPhoneNo;
                            }
                            //Aman ML Change
                        }
                    }



                }
            }

            #endregion

            //Added Rakhi for R7:Add Emp Data Elements
            //MITS:34276 : Starts -- Entity ID Type Update
            #region "Updating Entity Id Type"
            SortedList objEntityIdTypeAsList = new SortedList();

            XmlNode objEntityIdTypeAsNode = base.SysEx.DocumentElement.SelectSingleNode("EntityXEntityIDType");
            objTypeAttributeNode = null;

            bIsNew = false;
            int iEntityIdTypeKey = -1;

            XmlDocument objEntityXIDTypeXmlDoc = null;
            XmlElement objEntityXIDTypeRootElement = null;

            int iEntityIdTypeSelectedId = base.GetSysExDataNodeInt("EntityXEntityIDTypeSelectedId", true);

            string sEntityIdTypeRowAddedFlag = base.GetSysExDataNodeText("EntityXEntityIDTypeGrid_RowAddedFlag", true);
            bool bEntityIdTypeRowAddedFlag = sEntityIdTypeRowAddedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

            string sEntityIdTypeRowDeletedFlag = base.GetSysExDataNodeText("EntityXEntityIDTypeGrid_RowDeletedFlag", true);
            bool bEntityIdTypeRowDeletedFlag = sEntityIdTypeRowDeletedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

            if (objEntityIdTypeAsNode != null)
            {
                // Loop through data for all rows of the grid
                foreach (XmlNode objOptionNode in objEntityIdTypeAsNode.SelectNodes("option"))
                {
                    objTypeAttributeNode = objOptionNode.Attributes["type"];
                    bIsNew = false;
                    if (objTypeAttributeNode != null && objTypeAttributeNode.InnerText.Equals("new") == true)
                    {
                        // This is the 'extra' hidden <option> for capturing a new grid row data
                        bIsNew = true;
                    }
                    if ((bIsNew == false) || (bIsNew == true && bEntityIdTypeRowAddedFlag == true))
                    {
                        objEntityXIDTypeXmlDoc = new XmlDocument();
                        objEntityXIDTypeRootElement = objEntityXIDTypeXmlDoc.CreateElement("EntityXEntityIDType");
                        objEntityXIDTypeXmlDoc.AppendChild(objEntityXIDTypeRootElement);
                        objEntityXIDTypeRootElement.InnerXml = objOptionNode.InnerXml;

                        iEntityIdTypeKey = Conversion.ConvertStrToInteger(objEntityXIDTypeRootElement.SelectSingleNode("IdNumRowId").InnerText);

                        if ((bIsNew == true && bEntityIdTypeRowAddedFlag == true) || (iEntityIdTypeKey < 0 && objEntityXIDTypeRootElement.SelectSingleNode("EntityId").InnerText == ""))
                            objEntityXIDTypeRootElement.SelectSingleNode("EntityId").InnerText = "0";

                        if (bIsNew == false && bEntityIdTypeRowDeletedFlag == true && iEntityIdTypeKey == iEntityIdTypeSelectedId)
                        {
                            continue;
                        }

                        objEntityIdTypeAsList.Add(iEntityIdTypeKey, objEntityXIDTypeXmlDoc);
                    }
                }
            }

            foreach (EntityXEntityIDType objEntityXEntityIDTypeAs in Patient.PatientEntity.EntityXEntityIDTypeList)
            {
                if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7" && (objEntityXEntityIDTypeAs.IdNumRowId < 0 || (bEntityIdTypeRowDeletedFlag == true && objEntityXEntityIDTypeAs.IdNumRowId == iEntityIdTypeSelectedId)))
                {
                    Patient.PatientEntity.EntityXEntityIDTypeList.Remove(objEntityXEntityIDTypeAs.IdNumRowId);
                }
                else if (objEntityXEntityIDTypeAs.IdNumRowId < 0)
                    Patient.PatientEntity.EntityXEntityIDTypeList.Remove(objEntityXEntityIDTypeAs.IdNumRowId);
            }


            EntityXEntityIDType objTmpEntityXEntityIDTypeAs = null;
            XmlDocument objTmpEntityXEntityIdTypeDoc = null;

            ArrayList arrEntityIdTypes = new ArrayList();

            for (int iListIndex = 0; iListIndex < objEntityIdTypeAsList.Count; iListIndex++)
            {
                objTmpEntityXEntityIdTypeDoc = (XmlDocument)objEntityIdTypeAsList.GetByIndex(iListIndex);

                if (objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText.Trim() == "" || Conversion.ConvertStrToInteger(objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText) < 0)
                {
                    objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText = ((-1 * iListIndex) - 1).ToString();
                    objTmpEntityXEntityIDTypeAs = Patient.PatientEntity.EntityXEntityIDTypeList.AddNew();
                    objTmpEntityXEntityIDTypeAs.PopulateObject(objTmpEntityXEntityIdTypeDoc);
                }
                else
                {
                    objTmpEntityXEntityIDTypeAs = Patient.PatientEntity.EntityXEntityIDTypeList[Conversion.ConvertStrToInteger(objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText)];

                    objTmpEntityXEntityIDTypeAs.PopulateObject(objTmpEntityXEntityIdTypeDoc);
                }

                arrEntityIdTypes.Add(objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText);
            }


            foreach (EntityXEntityIDType objEntityXEntityIDType in Patient.PatientEntity.EntityXEntityIDTypeList)
            {
                if (!arrEntityIdTypes.Contains(objEntityXEntityIDType.IdNumRowId.ToString()))
                {
                    Patient.PatientEntity.EntityXEntityIDTypeList.Remove(objEntityXEntityIDType.IdNumRowId);
                }
            }

            #endregion
            //MITS:34276-- Entity ID Type tab display end
            
                //if(Patient.PatientEntity.IsEntityRoleExists(Patient.PatientEntity.EntityXRoleList,Patient.Context.LocalCache.GetTableId(Globalization.PersonInvolvedLookupsGlossaryTableNames.PATIENTS.ToString())) < 0)
                //{
                    //Patient.PatientEntity.UpdateEntityRoles((Patient.PatientEntity as IDataModel),Patient.Context.LocalCache.GetTableId(Globalization.PersonInvolvedLookupsGlossaryTableNames.PATIENTS.ToString()));
                //    EntityXRole objEntityRole = (EntityXRole)Patient.Context.Factory.GetDataModelObject("EntityXRole", false);
                //    objEntityRole.EntityTableId = Patient.Context.LocalCache.GetTableId(Globalization.PersonInvolvedLookupsGlossaryTableNames.PATIENTS.ToString());
                //    Patient.PatientEntity.EntityXRoleList.Add(objEntityRole);
                //    objEntityRole.Dispose();
                //    objEntityRole = null;
                //}
        }
        private XmlDocument GetEntityXAddressInfo()
        {
            
            XmlElement objOptionXmlElement = null;
            XmlElement objXmlElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlAttribute objXmlAttribute = null;
            int iEntityXAddressInfoCount = 0;
            string sStateCode = string.Empty;
            string sStateDesc = string.Empty;
            string sCountryCode = string.Empty;
            string sCountryDesc = string.Empty;
            string sPrimaryAddressRef = string.Empty;
            string p_ShortCode = String.Empty; //MITS:34276 Entity Address Type
            string p_sDesc = String.Empty;  //MITS:34276 Entity Address Type

            XmlDocument objEntityXAddressInfoXmlDoc = new XmlDocument();
            XmlElement objRootElement = objEntityXAddressInfoXmlDoc.CreateElement("EntityXAddressesInfo");
            objEntityXAddressInfoXmlDoc.AppendChild(objRootElement);

            objListHeadXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("listhead");

            //MITS:34276 Entity Address Type START
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressType");
            objXmlElement.InnerText = "Address Type";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //MITS:34276 Entity Address Type END

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr1");
            objXmlElement.InnerText = "Address1";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr2");
            objXmlElement.InnerText = "Address2";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
            objXmlElement.InnerText = "Address3";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
            objXmlElement.InnerText = "Address4";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("City");
            objXmlElement.InnerText = "City";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("State");
            objXmlElement.InnerText = "State";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Country");
            objXmlElement.InnerText = "Country";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("County");
            objXmlElement.InnerText = "County";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ZipCode");
            objXmlElement.InnerText = "Zip Code";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Email");
            objXmlElement.InnerText = "E-Mail";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Fax");
            objXmlElement.InnerText = "Fax No.";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PrimaryAddress");
            objXmlElement.InnerText = "Primary Address";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            //JIRA:6865 START:
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EffectiveDate");
            objXmlElement.InnerText = "Effective Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ExpirationDate");
            objXmlElement.InnerText = "Expiration Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //JIRA:6865 END:

            objRootElement.AppendChild(objListHeadXmlElement);
            objListHeadXmlElement = null;


            foreach (EntityXAddresses objEntityXAddressInfo in this.Patient.PatientEntity.EntityXAddressesList)
            {

                iEntityXAddressInfoCount++;

                objOptionXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("option");

                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("ref");
                objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                    + "/" + objEntityXAddressInfoXmlDoc.DocumentElement.LocalName
                    + "/option[" + iEntityXAddressInfoCount.ToString() + "]";
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                sPrimaryAddressRef = objXmlAttribute.InnerText;
                objXmlAttribute = null;

                //MITS:34276 Entity Address Type START
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressType");
                this.objCache.GetCodeInfo(objEntityXAddressInfo.AddressType, ref p_ShortCode, ref p_sDesc);
                objXmlElement.InnerText = p_ShortCode + " " + p_sDesc;
                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXAddressInfo.AddressType.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;
                //MITS:34276 Entity Address Type END
				//RMA-8753 nshah28(Added by ashish) START

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr1");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr1;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr2");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr2;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr3;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr4;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("City");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.City;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("State");
                this.objCache.GetStateInfo(objEntityXAddressInfo.Address.State, ref sStateCode, ref sStateDesc);
                objXmlElement.InnerText = sStateCode + " " + sStateDesc;
                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXAddressInfo.Address.State.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Country");
                this.objCache.GetCodeInfo(objEntityXAddressInfo.Address.Country, ref sCountryCode, ref sCountryDesc, base.Adaptor.userLogin.objUser.NlsCode);  //Aman ML Change
                objXmlElement.InnerText = sCountryCode + " " + sCountryDesc;
                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXAddressInfo.Address.Country.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("County");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.County;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;


                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ZipCode");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.ZipCode;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Email");
                objXmlElement.InnerText = objEntityXAddressInfo.Email;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Fax");
                objXmlElement.InnerText = objEntityXAddressInfo.Fax;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PrimaryAddress");
                int iPrimaryAddress = Convert.ToInt32(objEntityXAddressInfo.PrimaryAddress);
                if (iPrimaryAddress == -1)
                    objXmlElement.InnerText = "True";
                else
                    objXmlElement.InnerText = "False";
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                //JIRA:6865 START:
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EffectiveDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXAddressInfo.EffectiveDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ExpirationDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXAddressInfo.ExpirationDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                //JIRA:6865 END:

                /************** Hidden Columns on the Grid ******************/
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("NewPrimaryRecord");//for identication of changed Primary Address
                objXmlElement.InnerText = "False";
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressId");
                objXmlElement.InnerText = objEntityXAddressInfo.AddressId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                //RMA-8753 nshah28(Added by ashish) start
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("RowId");
                objXmlElement.InnerText = objEntityXAddressInfo.RowId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                //RMA-8753 nshah28(Added by ashish) end

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EntityId");
                objXmlElement.InnerText = objEntityXAddressInfo.EntityId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;


                //XmlDocument objAddressXPhoneInfoXmlDoc = GetAddressXPhoneInfo(objEntityXAddressInfo);
                //objOptionXmlElement.AppendChild(objEntityXAddressInfoXmlDoc.ImportNode(objAddressXPhoneInfoXmlDoc.SelectSingleNode("AddressXPhoneInfo"), true));

                objRootElement.AppendChild(objOptionXmlElement);
                objOptionXmlElement = null;


            }

            iEntityXAddressInfoCount++;

            objOptionXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("option");

            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("ref");
            objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                + "/" + objEntityXAddressInfoXmlDoc.DocumentElement.LocalName
                + "/option[" + iEntityXAddressInfoCount.ToString() + "]";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("type");
            objXmlAttribute.InnerText = "new";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            //MITS:34276 Entity Address Type START
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressType");
            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;
            //MITS:34276 Entity Address Type END

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr1");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr2");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("City");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("State");
            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Country");
            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("County");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ZipCode");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Email");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Fax");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PrimaryAddress");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            //JIRA:6865 START:
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EffectiveDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ExpirationDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //JIRA:6865 END:

            /************** Hidden Columns on the Grid ******************/
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("NewPrimaryRecord"); //for identication of changed Primary Address
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            //RMA-8753 nshah28 start
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("RowId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //RMA-8753 nshah28 end
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EntityId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            #region PhoneInfo

            //            XmlElement objInnerXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressXPhoneInfo");
//            objOptionXmlElement.AppendChild(objInnerXmlElement);

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("listhead");
//            string sNode = @"<PhoneCode>Phone Code</PhoneCode>
//                             <PhoneNo>Phone Number</PhoneNo>";
//            objXmlElement.InnerXml = sNode;
//            objInnerXmlElement.AppendChild(objXmlElement);

//            XmlElement objInnerOptionXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("option");

//            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("ref");
//            objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
//                + "/AddressXPhoneInfo" + "/option[" + iEntityXAddressInfoCount.ToString() + "]";
//            objInnerOptionXmlElement.Attributes.Append(objXmlAttribute);
//            objXmlAttribute = null;

//            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("type");
//            objXmlAttribute.InnerText = "new";
//            objInnerOptionXmlElement.Attributes.Append(objXmlAttribute);
//            objXmlAttribute = null;

//            objInnerXmlElement.AppendChild(objInnerOptionXmlElement);

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PhoneCode");
//            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
//            objXmlAttribute.InnerText = "-1";
//            objXmlElement.Attributes.Append(objXmlAttribute);
//            objInnerOptionXmlElement.AppendChild(objXmlElement);
//            objXmlAttribute = null;
//            objXmlElement = null;

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PhoneNo");
//            objInnerOptionXmlElement.AppendChild(objXmlElement);
//            objXmlElement = null;

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ContactId");
//            objInnerOptionXmlElement.AppendChild(objXmlElement);
//            objXmlElement = null;

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PhoneId");
//            objInnerOptionXmlElement.AppendChild(objXmlElement);
            //            objXmlElement = null;

            #endregion //PhoneInfo

            objRootElement.AppendChild(objOptionXmlElement);
            objOptionXmlElement = null;

            objRootElement = null;
            return objEntityXAddressInfoXmlDoc;
        }

        #region GetAddressXPhoneInfo
        //private XmlDocument GetAddressXPhoneInfo(EntityXAddresses objEntityXAddresses)
        //{
        //    XmlDocument objAddressXPhoneInfoXmlDoc = new XmlDocument();

        //    XmlElement objRootElement = objAddressXPhoneInfoXmlDoc.CreateElement("AddressXPhoneInfo");
        //    objAddressXPhoneInfoXmlDoc.AppendChild(objRootElement);

        //    XmlElement objOptionXmlElement = null;
        //    XmlElement objXmlElement = null;
        //    XmlElement objListHeadXmlElement = null;
        //    XmlAttribute objXmlAttribute = null;
        //    int iAddressXPhoneInfoCount = 0;
        //    string sPhoneCode = "";
        //    string sPhoneDesc = "";



        //    objListHeadXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("listhead");


        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneCode");
        //    objXmlElement.InnerText = "Phone Code";
        //    objListHeadXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneNo");
        //    objXmlElement.InnerText = "Phone Number";
        //    objListHeadXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objRootElement.AppendChild(objListHeadXmlElement);
        //    objListHeadXmlElement = null;


        //    foreach (AddressXPhoneInfo objAddressXPhoneInfo in objEntityXAddresses.AddressXPhoneInfoList)
        //    {

        //        iAddressXPhoneInfoCount++;

        //        objOptionXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("option");

        //        objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("ref");
        //        objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
        //            + "/" + objAddressXPhoneInfoXmlDoc.DocumentElement.LocalName
        //            + "/option[" + iAddressXPhoneInfoCount.ToString() + "]";
        //        objOptionXmlElement.Attributes.Append(objXmlAttribute);
        //        objXmlAttribute = null;



        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneCode");
        //        this.objCache.GetCodeInfo(objAddressXPhoneInfo.PhoneCode, ref sPhoneCode, ref sPhoneDesc);
        //        objXmlElement.InnerText = sPhoneCode + " " + sPhoneDesc;
        //        objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("codeid");
        //        objXmlAttribute.InnerText = objAddressXPhoneInfo.PhoneCode.ToString();
        //        objXmlElement.Attributes.Append(objXmlAttribute);
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlAttribute = null;
        //        objXmlElement = null;


        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneNo");
        //        objXmlElement.InnerText = objAddressXPhoneInfo.PhoneNo;
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlElement = null;


        //        /************** Hidden Columns on the Grid ******************/
        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("ContactId");
        //        objXmlElement.InnerText = objAddressXPhoneInfo.ContactId.ToString();
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlElement = null;

        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneId");
        //        objXmlElement.InnerText = objAddressXPhoneInfo.PhoneId.ToString();
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlElement = null;

        //        objRootElement.AppendChild(objOptionXmlElement);
        //        objOptionXmlElement = null;

        //    }

        //    iAddressXPhoneInfoCount++;

        //    objOptionXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("option");

        //    objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("ref");
        //    objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
        //        + "/" + objAddressXPhoneInfoXmlDoc.DocumentElement.LocalName
        //        + "/option[" + iAddressXPhoneInfoCount.ToString() + "]";
        //    objOptionXmlElement.Attributes.Append(objXmlAttribute);
        //    objXmlAttribute = null;

        //    objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("type");
        //    objXmlAttribute.InnerText = "new";
        //    objOptionXmlElement.Attributes.Append(objXmlAttribute);
        //    objXmlAttribute = null;


        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneCode");
        //    objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("codeid");
        //    objXmlAttribute.InnerText = "-1";
        //    objXmlElement.Attributes.Append(objXmlAttribute);
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlAttribute = null;
        //    objXmlElement = null;


        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneNo");
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    /************** Hidden Columns on the Grid ******************/
        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("ContactId");
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneId");
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objRootElement.AppendChild(objOptionXmlElement);
        //    objOptionXmlElement = null;

        //    objRootElement = null;
        //    return objAddressXPhoneInfoXmlDoc;
        //}
        #endregion //GetAddressXPhoneInfo
        public override void OnValidate(ref bool Cancel)
        {
             bool bMulAddresses = this.objData.Context.InternalSettings.SysSettings.UseMultipleAddresses && !this.objData.Context.InternalSettings.SysSettings.UseEntityRole;
             if (bMulAddresses)
             {
                 XmlNode PrimaryAddress = base.SysEx.DocumentElement.SelectSingleNode("PrimaryFlag");
                 if (PrimaryAddress != null && Patient.PatientEntity.EntityXAddressesList.Count != 0 && PrimaryAddress.InnerText.ToLower() == "false")
                 {
                     Errors.Add("PrimaryAddressError", "No Address Marked as Primary for this Entity", BusinessAdaptorErrorType.Error);
                     Cancel = true;
                 }
            }
             //MITS 34276 Starts:Check for Unique Entity ID Number and Entity ID Type Combination
             List<EntityXEntityIDType> lstEntityXIDType = new List<EntityXEntityIDType>();
             bool bDupEntityIDType = false;
             foreach (EntityXEntityIDType objEntityIDType in Patient.PatientEntity.EntityXEntityIDTypeList)
             {
                 if (!bDupEntityIDType && Riskmaster.Common.CommonFunctions.IsEntityIDNumberExists(Patient.PatientEntity.Context.RMDatabase.ConnectionString, objEntityIDType.IdNumRowId, objEntityIDType.Table, objEntityIDType.IdNum, objEntityIDType.IdType))
                 {
                     Errors.Add("EntityIDNumberError", "Entity ID Number with Entity ID Type already exists",
                         BusinessAdaptorErrorType.Error);
                     Cancel = true;
                 }
                 if (!bDupEntityIDType && lstEntityXIDType.Exists(x => x.IdType == objEntityIDType.IdType && x.IdNum == objEntityIDType.IdNum))
                 {
                     Errors.Add("EntityIDNumberError", "Entity ID Number with Entity ID Type already exists",
                          BusinessAdaptorErrorType.Error);
                     Cancel = true;
                     bDupEntityIDType = true;
                 }
                 else
                     lstEntityXIDType.Add(objEntityIDType);
             }
            //MITS 34276 Ends:Check for Unique Entity ID Number and Entity ID Type Combination
        }
        private void SetPrimaryAddressReadOnly()
        {

            base.AddReadOnlyNode("pataddr1");
            base.AddReadOnlyNode("pataddr2");
            base.AddReadOnlyNode("pataddr3");// JIRA 6420 pkandhari
            base.AddReadOnlyNode("pataddr4");// JIRA 6420 pkandhari
            base.AddReadOnlyNode("patcity");
            base.AddReadOnlyNode("patcountrycode");
            base.AddReadOnlyNode("patcounty");
            base.AddReadOnlyNode("patstateid");
            base.AddReadOnlyNode("patfaxnumber");
            base.AddReadOnlyNode("patzipcode");

        }
        private string PopulatePhoneNumbers()
        {

            XmlDocument SysExDoc = new XmlDocument();
            string sNumber = string.Empty;
            foreach (AddressXPhoneInfo objPhoneInfo in Patient.PatientEntity.AddressXPhoneInfoList)
            {
                //Added to set Default types to office and home
                if (objPhoneInfo.PhoneCode == iOfficeType)
                {
                    sOfficePhone = objPhoneInfo.PhoneNo;

                }
                else if (objPhoneInfo.PhoneCode == iHomeType)
                {
                    sHomePhone = objPhoneInfo.PhoneNo;
                }
                //Added to set Default types to office and home
                if (String.IsNullOrEmpty(sNumber))
                {
                    sNumber = objPhoneInfo.PhoneId + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneCode + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneNo;
                }
                else
                {
                    sNumber += CommonForm.OUTER_PHONE_DELIMITER + objPhoneInfo.PhoneId + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneCode + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneNo;
                }
                XmlNode objNode = base.SysEx.SelectSingleNode("//PhoneTypeList");
                if (objNode != null)
                {
                    foreach (XmlNode node in objNode.ChildNodes)
                    {
                        if (objPhoneInfo.PhoneCode.ToString() == node.Attributes["value"].Value)
                        {
                            node.InnerText += " " + CommonForm.MARK_PHONECODE;
                            break;
                        }
                    }
                }

            }

            return sNumber;
        }
        public override void AfterApplyLookupData(XmlDocument objPropertyStore)
        {
            XmlDocument objTargetDoc = new XmlDocument();
            XmlDocument objConfigDom = new XmlDocument();
            XmlNode objSource = null;
            XmlNode objTarget = null;


            string sTempXml = objPropertyStore.SelectSingleNode("Instance").InnerXml;

            objPropertyStore.LoadXml(sTempXml);

            base.AfterApplyLookupData(objPropertyStore);

            if (base.SysLookup.SelectSingleNode("/SysLookup/SysLookupAttachNodePath").InnerText.IndexOf("PatientEntity") > 0)
            {
                this.Patient.Refresh();  //Emptying the Patient object
                objConfigDom.LoadXml("<Patient><PatientEntity/><Supplementals/></Patient>");
                objTargetDoc.LoadXml(this.Patient.SerializeObject(objConfigDom));

                //Patient Entity data copied from PropertyStore to the dummy document
                objSource = (XmlNode)objTargetDoc.SelectSingleNode("//PatientEntity");
                objTarget = objTargetDoc.CreateElement(objSource.Name);
                objTarget.InnerXml = objPropertyStore.SelectSingleNode("//PatientEntity").InnerXml;
                objSource.ParentNode.ReplaceChild(objTarget, objSource);
                objSource = null;
                objTarget = null;

                //Replacing the PropertyStore with dummy document
                objSource = (XmlNode)objPropertyStore.SelectSingleNode("//Patient");
                objTarget = objPropertyStore.CreateElement(objSource.Name);
                objTarget.InnerXml = objTargetDoc.SelectSingleNode("//Patient").InnerXml;
                objSource.ParentNode.ReplaceChild(objTarget, objSource);

                objSource = null;
                objTarget = null;
            }
        }//End AfterApplyLookupData()

        //MITS:34276 Starts-- Entity ID Type tab display
        private XmlDocument GetEntityIdTypeInfoData()
        {
            XmlDocument objEntityIDTypeAsXmlDoc = new XmlDocument();

            XmlElement objRootElement = objEntityIDTypeAsXmlDoc.CreateElement("EntityXEntityIDType");
            //05-may-2013     MITS #34276 Entity ID Type Permission starts
            XmlAttribute objXmlPermissionAttribute = null;
            if (!Patient.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_VIEW))
            {

                objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsViewable");
                objXmlPermissionAttribute.InnerText = "false";
                objRootElement.Attributes.Append(objXmlPermissionAttribute);
                objXmlPermissionAttribute = null;
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);
            }
            else
            {
                if (!Patient.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_NEW))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsNew");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);

                if (!Patient.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_EDIT))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsUpdate");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);

                if (!Patient.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_DELETE))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsDelete");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);
                if (!Patient.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_NEW))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorNew");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);
            }
            //05-may-2013    MITS #34276  Entity ID Type Permission Ends 

            XmlElement objOptionXmlElement = null;
            XmlElement objXmlElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlAttribute objXmlAttribute = null;
            int iEntityIDTypeAsCount = 0;
            string sIDCode = string.Empty;
            string sIDDesc = string.Empty;

            objListHeadXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("listhead");

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdType");
            objXmlElement.InnerText = "Entity ID Type";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNum");
            objXmlElement.InnerText = "Entity ID Number";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffStartDate");
            objXmlElement.InnerText = "Effective Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffEndDate");
            objXmlElement.InnerText = "Expiration Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objRootElement.AppendChild(objListHeadXmlElement);
            objListHeadXmlElement = null;

            foreach (EntityXEntityIDType objEntityXEntityIDType in this.Patient.PatientEntity.EntityXEntityIDTypeList)
            {
                iEntityIDTypeAsCount++;

                objOptionXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("option");

                objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("ref");
                objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH + "/" + objEntityIDTypeAsXmlDoc.DocumentElement.LocalName + "/option[" + iEntityIDTypeAsCount.ToString() + "]";
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                objXmlAttribute = null;

                //05-may-2013    MITS #34276  ID Type Vendor Permission Starts 
                this.objCache.GetCodeInfo(objEntityXEntityIDType.IdType, ref sIDCode, ref sIDDesc);               
                if (sIDCode.Trim().ToUpper() == "VENDOR")
                {
                    if (!Patient.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_VIEW))
                    {
                        objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorView");
                        objXmlAttribute.InnerText = "false";
                        objOptionXmlElement.Attributes.Append(objXmlAttribute);
                        objXmlAttribute = null;
                    }                   
                    if (!Patient.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_UPDATE))
                    {
                        objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorUpdate");
                        objXmlAttribute.InnerText = "false";
                        objOptionXmlElement.Attributes.Append(objXmlAttribute);
                        objXmlAttribute = null;
                    }
                    if (!Patient.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_DELETE))
                    {
                        objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorDelete");
                        objXmlAttribute.InnerText = "false";
                        objOptionXmlElement.Attributes.Append(objXmlAttribute);
                        objXmlAttribute = null;
                    }
                }
                //05-may-2013    MITS #34276  ID Type Vendor Permission ends

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdType");
                this.objCache.GetCodeInfo(objEntityXEntityIDType.IdType, ref sIDCode, ref sIDDesc);
                objXmlElement.InnerText = sIDCode + " " + sIDDesc;
                objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXEntityIDType.IdType.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNum");
                objXmlElement.InnerText = objEntityXEntityIDType.IdNum.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffStartDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXEntityIDType.EffStartDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffEndDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXEntityIDType.EffEndDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                /************** Hidden Columns on the Grid ******************/
                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNumRowId");
                objXmlElement.InnerText = objEntityXEntityIDType.IdNumRowId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EntityId");
                objXmlElement.InnerText = objEntityXEntityIDType.EntityId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;


                objRootElement.AppendChild(objOptionXmlElement);
                objOptionXmlElement = null;

            }

            iEntityIDTypeAsCount++;

            objOptionXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("option");

            objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("ref");
            objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                + "/" + objEntityIDTypeAsXmlDoc.DocumentElement.LocalName
                + "/option[" + iEntityIDTypeAsCount.ToString() + "]";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("type");
            objXmlAttribute.InnerText = "new";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdType");
            objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNum");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffStartDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffEndDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;


            /************** Hidden Columns on the Grid ******************/
            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNumRowId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EntityId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objRootElement.AppendChild(objOptionXmlElement);
            objOptionXmlElement = null;

            objRootElement = null;
            return objEntityIDTypeAsXmlDoc;

        }
        //MITS:34276 Ends
	}
}