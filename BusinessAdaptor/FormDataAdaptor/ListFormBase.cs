using System;
using Riskmaster.DataModel;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Class implements common list screen operations. Any extended operation  
	/// can be done in specific derived screen classes.                       
	/// Author: Brian Battah, 05/19/2005             
	/// </summary>
	public class ListFormBase: FormBase
	{
		public ListFormBase(FormDataAdaptor fda):base(fda)
		{
			//Stash "Parent" Reference.
			m_fda = fda;
		}
		
		private object m_objDataList ;	// Data object which is a datacollection object that 
		// implements INavigation or IPersistence.


		public virtual void BeforeAddNew(ref bool Cancel){;}
		public virtual void AfterAddNew(){;}

		public virtual void BeforeRemove(ref bool Cancel,XmlDocument propertyStore){;}
		public virtual void AfterRemove(XmlDocument propertyStore){;}

		public virtual void BeforeMoveFirst(ref bool Cancel){;}
		public virtual void AfterMoveFirst(){;}
		public virtual void OnParentFormChanged(){;}

		public override void UpdateObject()
		{
			base.UpdateObject ();
		}
		override public void UpdateForm()
		{
			try
			{
//				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_VIEW))
//					return;
				OnUpdateForm();
				m_DataChanged = false;
			}
			catch(Exception e)
			{
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.UpdateForm.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				return;
			}
		}
		public DataCollection objDataList
		{
			set{
				m_objDataList = value;
				//Will cause object to reset 
				//ok as implemented - 
				//may confuse maintainers with apparent data loss if property set is invoked again... 
				(m_objDataList as DataCollection).BulkLoadFlag=true;
			}
			get{return m_objDataList as DataCollection;}
		}
		public INavigation pINav
		{
			get{return m_objDataList as INavigation;}
		}
		public IPersistence pIPer
		{
			get{return m_objDataList as IPersistence;}
		}
		override public bool Refresh(){return Refresh(false);} 
		override public bool Refresh(bool viewOnly) 
		{
			bool bCancel = false;
			try
			{
				m_CurrentAction = enumFormActionType.Refresh;
				
				int RMO_PERM;
				if(pIPer !=null && pIPer.IsNew)
					RMO_PERM = RMO_CREATE;
				else
					RMO_PERM= RMO_VIEW;

				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_PERM))
				{
					m_CurrentAction = enumFormActionType.None;
					return false;
				}
				
				BeforeRefresh(ref bCancel);
				if(!bCancel)
					BeforeAction(m_CurrentAction,ref bCancel);
				
				if(bCancel)
				{
					this.Cancelled=true;
					m_CurrentAction = enumFormActionType.None;
					return false;
				}

				if(!viewOnly && pIPer !=null)
					pIPer.Refresh();
				
				UpdateForm();
				m_DataChanged = false;

				AfterRefresh();
				AfterAction(m_CurrentAction);
			}				
			catch(Exception e)
			{
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.Refresh.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				m_CurrentAction = enumFormActionType.None;
				return false;
			}
			m_CurrentAction = enumFormActionType.None;
			return true;

		}
		public virtual bool AddNew()
		{
			bool bCancel=false;
			try
			{
				m_CurrentAction = enumFormActionType.AddNew;
				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, FormBase.RMO_CREATE))
				{
					m_CurrentAction = enumFormActionType.None;
					return false;
				}
				
				BeforeAddNew(ref bCancel);
				if(!bCancel)
					BeforeAction(m_CurrentAction,ref bCancel);
				
				if(bCancel)
				{
					this.Cancelled=true;
					m_CurrentAction = enumFormActionType.None;
					return false;
				}

				
				objDataList.Save();// = this.objDataList.Context.Factory.GetDataModelObject(objDataList.GetType().Name,false) as DataCollection;
				
				UpdateForm();
				AfterAddNew();
				AfterAction(m_CurrentAction);
			}
			catch(Exception e)
			{
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.AddNew.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				m_CurrentAction = enumFormActionType.None;
				return false;
			}

			m_CurrentAction = enumFormActionType.None;
			return true;
		}
		
		#region Children Operations NOT PORTED
		// BSB NOT PORTED - Current Interface is not MDI so this doesn't really apply.

		//Private Sub IFormOperation_OnDoNotSave()
		//RaiseEvent OnDoNotSave
		//If Not m_ParentForm Is Nothing And Not m_objData Is Nothing Then
		//Dim pIPer As IPersistence
		//On Error Resume Next
		//Set pIPer = m_objData
		//If Err.Number = 0 Then
		//' dbas 03/09/2000
		//On Error Resume Next ' Ignore errors at this point
		//pIPer.Refresh
		//Dim ErrNum As Long, ErrDesc As String, ErrSrc As String
		//ErrNum = Err.Number: ErrDesc = Err.Description: ErrSrc = Err.Source
		//' We get data not found on one-to-one relations between objects, and
		//' in some special cases. In any case we don't care about that error
		//' since at this point there is nothing we can do about it.
		//If ErrNum <> ErrDataNotFound And ErrNum <> 0 Then
		//On Error GoTo 0
		//Err.Raise ErrNum, "IFormOperation.OnDontSave." & ErrSrc, ErrDesc
		//End If
		//Set pIPer = Nothing
		//End If
		//End If
		//End Sub
		//Private Property Set IFormOperation_ParentForm(ByVal RHS As Form)
		//
		//If Not m_ParentForm Is RHS Then
		//Set m_ParentForm = RHS
		//RaiseEvent OnParentFormChanged
		//End If
		//
		//End Property
		//
		//Private Property Get IFormOperation_ParentForm() As Form
		//Set IFormOperation_ParentForm = m_ParentForm
		//End Property
		//
		// public void CancelChildren(){}
		//On Error GoTo hError
		//Dim pIFormOp As IFormOperation
		//Dim frmChild As Form
		//Dim boolCancel As Boolean
		//
		//m_CurrentAction = eatCancelChildren
		//
		//RaiseEvent BeforeCancelChildren(boolCancel)
		//If Not boolCancel Then RaiseEvent BeforeAction(m_CurrentAction, boolCancel)
		//If boolCancel Then GoTo hExit
		//
		//Do While colChildren.Count > 0
		//Set frmChild = colChildren(1)
		//Set pIFormOp = frmChild.objFormOperation
		//pIFormOp.CancelChildren
		//pIFormOp.DataChanged = False
		//Set pIFormOp = Nothing
		//' This will trigger ChildIsUnloading Method on same interface
		//' and this item will be removed from collection there
		//Unload frmChild
		//Loop
		//
		//RaiseEvent AfterCancelChildren
		//RaiseEvent AfterAction(m_CurrentAction)
		//
		//hExit:
		//m_CurrentAction = eatNone
		//Exit Sub
		//
		//hError:
		//If iGeneralErrorExt(Err.Number, Err.Source, Err.Description, "CFormOperation.CancelChildren") = vbRetry Then
		//Resume
		//End If
		//
		//Resume hExit
		//
		//End Sub
			
		
		//Private Sub IFormOperation_ChildIsUnloading(frmChild As Form)
		//Dim c As Long, f As Long
		//RaiseEvent OnChildIsUnloading(frmChild)
		//
		//On Error Resume Next
		//colChildren.Remove LCase$(frmChild.Name)
		//If Err.Number > 0 Then
		//' May be it does not have a key try to search for it
		//c = colChildren.Count
		//For f = 1 To c
		//If colChildren(f) Is frmChild Then
		//colChildren.Remove f
		//Exit For
		//End If
		//Next
		//End If
		//On Error GoTo 0
		//
		//End Sub
		//
		#endregion
		bool DataChanged
		{
			set{m_DataChanged = value;}
			get
			{
				bool b = false;
				try
				{
					if(m_DataChanged) 
						return true;
					//No Child Forms Eliminates some complexity here.

					//Give derived code a chance to intervene.
					GetDataChanged(ref b);
				}
				catch(Exception e)
				{
                    Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.DataChanged.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				}
				return b;
			}
		}

		//This is a sham since PopulateObject on a DataCollection will Delete all the way from the Database.
		public bool Remove(XmlDocument propertyStore) 
		{
			bool bCancel = false;
			try
			{
				m_CurrentAction = enumFormActionType.Delete;
				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, FormBase.RMO_DELETE))
				{
					m_CurrentAction = enumFormActionType.None;
					LogSecurityError(RMO_DELETE);
					return false;
				}
				
				BeforeRemove(ref bCancel, propertyStore);
				if(!bCancel)
					BeforeAction(m_CurrentAction,ref bCancel);
				
				if(bCancel)
				{
					this.Cancelled=true;
					m_CurrentAction = enumFormActionType.None;
					return false;
				}

				// BSB 01.30.2005 Hacked this routine to allow full application of delete permissions.
				// Update the in memory instance 
				// Note that deleted items arrive tagged and are
				// removed automatically in this process.
				objDataList.PopulateObject(propertyStore); 
					
				//pIPer.Save(); //In memory list had it's node(s) removed during PopulateObject.
							  // Now just make this official by calling Save and updating the database.
				
				UpdateForm();
				AfterRemove(propertyStore);
				AfterAction(m_CurrentAction);
			}
            //pmahli  MITS 11143 1/7/2008 - Start
            catch (RMAppException p_objException)
            {
                Errors.Add(p_objException, Common.BusinessAdaptorErrorType.Error);
                m_CurrentAction = enumFormActionType.None;
                return false;
            }
            //pmahli MITS 11143 1/7/2008 - End
			catch(Exception e)
			{
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.Delete.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				m_CurrentAction = enumFormActionType.None;
				return false;
			}
			m_CurrentAction = enumFormActionType.None;
			return true;
		}

		public bool MoveFirst() 
		{
			bool bCancel = false;
			try
			{
				m_CurrentAction = enumFormActionType.MoveFirst;
				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, FormBase.RMO_VIEW))
				{
					m_CurrentAction = enumFormActionType.None;
					return false;
				}
				
				BeforeMoveFirst(ref bCancel);
				if(!bCancel)
					BeforeAction(m_CurrentAction,ref bCancel);
				
				if(bCancel)
				{
					this.Cancelled=true;
					m_CurrentAction = enumFormActionType.None;
					return false;
				}
				pINav.MoveFirst();
				
				UpdateForm();
				m_DataChanged = false;

				AfterMoveFirst();
				AfterAction(m_CurrentAction);
			}				
			catch(Exception e)
			{
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.MoveFirst.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				m_CurrentAction = enumFormActionType.None;
				return false;
			}
			m_CurrentAction = enumFormActionType.None;
			return true;
		}
		


//		public bool Save()
//		{
//			bool bCancel = false;
//			try
//			{
//				m_CurrentAction = enumFormActionType.Save;
//				
//				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, FormBase.RMO_UPDATE))
//					if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, FormBase.RMO_CREATE) && pIPer.IsNew)
//					{
//						//we dont' have any children to pass the save attempt to.
//						m_CurrentAction = enumFormActionType.None;
//						return false;
//					}
//				
//				if(this.m_DataChanged)
//				{
//					BeforeSave(ref bCancel);
//					if(!bCancel)
//						BeforeAction(m_CurrentAction,ref bCancel);
//				
//					if(bCancel)
//					{
//						m_CurrentAction = enumFormActionType.None;
//						return false;
//					}
//				}
//
//				if(this.Validate())
//				{
//					if(m_DataChanged && pIPer !=null)
//					{
//						pIPer.Save();
//						m_DataChanged = false;
//					}
//					//No Children to "Loop Through"
//
//					UpdateForm();
//					m_DataChanged = false;
//
//					AfterSave();
//					AfterAction(m_CurrentAction);
//				}
//			}				
//			catch(Exception e)
//			{
//				Errors.Add(e,Globalization.GetString("FormDataAdaptor.ScreenBase.Save.Exception"),Common.BusinessAdaptorErrorType.Error);
//				m_CurrentAction = enumFormActionType.None;
//				return false;
//			}
//			m_CurrentAction = enumFormActionType.None;
//			return true;
//
//		}

		#region legacy Save Operation
		//Private Function IFormOperation_Save() As Boolean
		//On Error GoTo hError
		//Dim boolCancel As Boolean
		//Dim pIPer As IPersistence
		//Dim pIFormOp As IFormOperation
		//Dim frmChild As Form
		//Dim boolEvent As Boolean
		//
		//IFormOperation_Save = True
		//
		//m_CurrentAction = eatSave
		//
		//Set pIPer = m_objData
		//
		//If Not IsActionAllowed(m_SecurityId, RMO_UPDATE) Then
		//	If Not (IsActionAllowed(m_SecurityId, RMO_CREATE) And pIPer.IsNew) Then
		//For Each frmChild In colChildren
		//Set pIFormOp = frmChild.objFormOperation
		//' If Child Save fails cancel loop
		//If Not pIFormOp.Save() Then
		//IFormOperation_Save = False ' Inidicate fail saving for whole group
		//Exit For
		//End If
		//Next
		//'Beep
		//'IFormOperation_Save = False
		//GoTo hExit
		//End If
		//End If
		//
		//' Visit children and see if any are Memo forms. If so, have them update parent with their data   JP 1/4/2000
		//For Each frmChild In colChildren
		//If TypeOf frmChild Is MemoMDI Then
		//Set pIFormOp = frmChild.objFormOperation
		//pIFormOp.UpdateObject
		//End If
		//Next
		//
		//' Fire BeforeSave event
		//If m_DataChanged Then
		//' Raise Event only if we will attepmt to save
		//boolEvent = True
		//RaiseEvent BeforeSave(boolCancel)
		//If Not boolCancel Then RaiseEvent BeforeAction(m_CurrentAction, boolCancel)
		//If boolCancel Then IFormOperation_Save = False: GoTo hExit
		//End If
		//
		//'GRD 4/15/2002  Internationalization
		//subSetSystemStatus g_Localize.getMessage("CM_CFormOperation", "IFormOperation_Save.Saving", m_StatusFormName)
		//subSetStatusMeter 10
		//' Save Form Only if validation goes through
		//If IFormOperation_Validate() Then
		//subSetStatusMeter 20
		//' Save This object
		//If m_DataChanged And Not pIPer Is Nothing Then
		//subSetStatusMeter 30
		//'RaiseEvent UpdateObject       THIS IS MOVED TO VALIDATION
		//subSetStatusMeter 40
		//pIPer.Save
		//subSetStatusMeter 50
		//m_DataChanged = False
		//End If
		//Set pIPer = Nothing
		//'IFormOperation_Save = True
		//
		//' Loop through children if any and save them too
		//subSetStatusMeter 70
		//For Each frmChild In colChildren
		//Set pIFormOp = frmChild.objFormOperation
		//' If Child Save fails cancel loop
		//If Not pIFormOp.Save() Then
		//IFormOperation_Save = False ' Inidicate fail saving for whole group
		//Exit For
		//End If
		//Next
		//subSetStatusMeter 80
		//RaiseEvent UpdateForm
		//'02/11/02 VD this is just temporary, will be removed as soon as we 'll add object for T&E
		//'Dim frmA As Form
		//'For Each frmA In Forms
		//'validation is OK, then update ACCT_REC by setting posted flag to -1
		//'If TypeName(frmA) = "frmTandEClaimLevel" Then
		//'frmA.PostIt
		//'End If
		//'Next
		//If IFormOperation_Save Then m_DataChanged = False
		//
		//' JP 4/24/2003  *BEGIN*   Moved call to AfterSave and AfterAction events here because they shouldn't be called if save failed because of validation error.
		//If boolEvent Then
		//' Raise event only if we attempted save
		//RaiseEvent AfterSave
		//RaiseEvent AfterAction(m_CurrentAction)
		//End If
		//' JP 4/24/2003  *END*
		//
		//subSetStatusMeter 100
		//Else
		//IFormOperation_Save = False
		//End If
		//
		//' JP 4/24/2003   Moved up. Shouldn't get called if validation fails.       If boolEvent Then
		//' JP 4/24/2003   Moved up. Shouldn't get called if validation fails.          ' Raise event only if we attempted save
		//' JP 4/24/2003   Moved up. Shouldn't get called if validation fails.          RaiseEvent AfterSave
		//' JP 4/24/2003   Moved up. Shouldn't get called if validation fails.          RaiseEvent AfterAction(m_CurrentAction)
		//' JP 4/24/2003   Moved up. Shouldn't get called if validation fails.       End If
		//
		//hExit:
		//m_CurrentAction = eatNone
		//subSetSystemStatus ""
		//subSetStatusMeter 0
		//Exit Function
		//
		//hError:
		//If iGeneralErrorExt(Err.Number, Err.Source, Err.Description, "CFormOperation.Save") = vbRetry Then
		//Resume
		//End If
		//IFormOperation_Save = False
		//Resume hExit
		//
		//End Function

		#endregion
		#region Juris and Supp Screen - TODO
		//Private Sub IFormOperation_ShowJurisdictional()
		//'Dim boolCancel As Boolean
		//'On Error GoTo hError
		//'If Not m_SupportJurisdictional Then GoTo hExit
		//'
		//'RaiseEvent BeforeShowJurisdictional(boolCancel)
		//'If boolCancel Then GoTo hExit
		//'
		//'Rem JP 1/24/2000 Implemented
		//'' We need updated object
		//'RaiseEvent UpdateObject
		//'
		//'' Show jurisdictionals form
		//'Dim frmA As Form
		//'
		//'For Each frmA In colChildren
		//'   If TypeOf frmA Is frmJurisdictionals Then
		//'      frmA.ZOrder
		//'      Exit Sub
		//'   End If
		//'Next
		//'
		//'Dim frmJur As New frmJurisdictionals
		//'Load frmJur
		//'Dim pIFormOp As IFormOperation
		//'Set pIFormOp = frmJur.objFormOperation
		//'Set pIFormOp.ParentForm = m_Form
		//'pIFormOp.SecurityId = m_SecurityId + JUR_OFFSET
		//'Set frmJur.Jurisdictionals = m_objData.Jurisdictionals
		//'frmJur.Caption = "Jurisdictional data for: " & m_Form.Caption
		//'frmJur.Show
		//'colChildren.Add frmJur, LCase$(frmJur.Name)
		//'
		//'RaiseEvent AfterShowJurisdictional
		//'
		//'hExit:
		//'   Exit Sub
		//'
		//'hError:
		//'If iGeneralErrorExt(Err.Number, Err.Source, Err.Description, "CFormOperation.ShowJurisdictional") = vbRetry Then
		//'   Resume
		//'End If
		//'
		//'Resume hExit
		//'
		//End Sub
		//
		//Private Sub IFormOperation_ShowSupplementals()
		//'Dim boolCancel As Boolean
		//'On Error GoTo hError
		//'If Not m_SupportSupplementals Then GoTo hExit
		//'
		//'RaiseEvent BeforeShowSupplementals(boolCancel)
		//'If boolCancel Then GoTo hExit
		//'
		//'' We need updated object becouse of group assoc.
		//'RaiseEvent UpdateObject
		//'
		//'' Show supplementals form
		//'Dim frmA As Form
		//'
		//'For Each frmA In colChildren
		//'   If TypeOf frmA Is frmSupplementals Then
		//'      frmA.ZOrder
		//'      Exit Sub
		//'   End If
		//'Next
		//'
		//'Dim frmSupp As New frmSupplementals
		//'Load frmSupp
		//'Dim pIFormOp As IFormOperation
		//'Set pIFormOp = frmSupp.objFormOperation
		//'Set pIFormOp.ParentForm = m_Form
		//'pIFormOp.SecurityId = m_SecurityId + SUPP_OFFSET
		//'Set frmSupp.Supplementals = m_objData.Supplementals
		//'frmSupp.Caption = "Supplemental data for: " & m_Form.Caption
		//'frmSupp.Show
		//'colChildren.Add frmSupp, LCase$(frmSupp.Name)
		//'
		//'RaiseEvent AfterShowSupplementals(frmSupp)
		//'
		//'hExit:
		//'   Exit Sub
		//'
		//'hError:
		//'If iGeneralErrorExt(Err.Number, Err.Source, Err.Description, "CFormOperation.ShowSupplementals") = vbRetry Then
		//'   Resume
		//'End If
		//'
		//'Resume hExit
		//'
		//End Sub
		//
		//
		//Private Property Let IFormOperation_SupportJurisdictional(ByVal RHS As Boolean)
		//m_SupportJurisdictional = RHS
		//End Property
		//
		//Private Property Get IFormOperation_SupportJurisdictional() As Boolean
		//IFormOperation_SupportJurisdictional = m_SupportJurisdictional
		//End Property
		//
		//
		//Private Property Let IFormOperation_SupportQuickDiary(ByVal RHS As Boolean)
		//m_SupportQuickDiary = RHS
		//End Property
		//
		//Private Property Get IFormOperation_SupportQuickDiary() As Boolean
		//IFormOperation_SupportQuickDiary = m_SupportQuickDiary
		//End Property
		//
		//Private Property Let IFormOperation_SupportSupplementals(ByVal RHS As Boolean)
		//m_SupportSupplementals = RHS
		//End Property
		//
		//Private Property Get IFormOperation_SupportSupplementals() As Boolean
		//IFormOperation_SupportSupplementals = m_SupportSupplementals
		//End Property
		//
		//
		#endregion
		

//		public bool Validate()
//		{
//			bool bCancel = false;
//			bool bRet = true;
//			try
//			{
//				UpdateObject();
//				if(m_DataChanged)
//				{
////					//Required Fields Check
////					if(m_objData is Supplementals)
////						bRet = ValidateSupplementals();
////
////					if(m_objData is ADMTable)
////						bRet = ValidateAdminTracking();
////					else
////						bRet = UnivValidate();
//					
//					//Do Any Custom Validation Stuff
//					OnValidate(ref bCancel);
//					bRet = bRet && !bCancel;
//					
//					// Doing this validation after Custom allows 
//					// custom logic to set Supp Values prior to attempting supp validation.
////					if(this.m_SupportSupplementals && !(m_objData is Supplementals))
////						bRet &= ValidateSupplementals();
//
//					//Run Validation from DataModel
//					if(pIPer != null)
//					{
//						bRet &= pIPer.Validate();
//						foreach(object o in this.objData.Context.ScriptValidationErrors)
//							Errors.Add("ValidationScriptError",o.ToString(),Common.BusinessAdaptorErrorType.Error);
//					}
//					//No Children to validate so we're done.	
//				}
//
//			}
//			catch(Exception e)
//			{
//				Errors.Add(e,Globalization.GetString("FormDataAdaptor.ScreenBase.Validate.Exception"),Common.BusinessAdaptorErrorType.Error);
//				return false;
//			}
//			return bRet;
//		}

		//BSB TODO - This function enforced fields set as required in REQ_FIELDS database table.
		// In World this is meaningfull. In RMNet these settings are simply ignored.  The question is:
		// What to do about it in Wizard.  Obviously a direct port is impossible since the linkage from
		// record to screen control uses Visual Basic control names which are not available in the GUI
		// any more...
		// Stubbed for now.
		private bool UnivValidate()
		{


			return true;
		}

		//Private Function IFormOperation_Validate() As Boolean
		//Dim b As Boolean
		//Dim boolCancel As Boolean
		//Dim lErrNum As Long
		//
		//On Error GoTo hError
		//
		//Unload frmValidateErrors
		//
		//' Update data object
		//RaiseEvent UpdateObject
		//
		//' Assume validation will go through
		//IFormOperation_Validate = True
		//
		//If m_DataChanged Then
		//' Check required fields
		//
		//If TypeOf m_Form Is frmAdminTracking Then
		//IFormOperation_Validate = IFormOperation_Validate And ValidateAdminTracking()
		//Else
		//IFormOperation_Validate = IFormOperation_Validate And UnivValidateForm(m_Form)
		//End If
		//
		//' Do any custom form validation stuff
		//RaiseEvent OnValidate(boolCancel)
		//
		//IFormOperation_Validate = IFormOperation_Validate And (Not boolCancel)
		//
		//' Do Supplemental fields required field validation
		//If m_SupportSupplementals Then
		//IFormOperation_Validate = IFormOperation_Validate And ValidateSupplementals()
		//End If
		//
		//' Run any validation contained in object
		//Dim pIPer As IPersistence
		//Dim lstItem As ListItem
		//Set pIPer = m_objData
		//
		//If Not pIPer Is Nothing Then
		//On Error Resume Next
		//b = pIPer.Validate()
		//lErrNum = Err.Number
		//On Error GoTo hError
		//If lErrNum > 0 Or Not b Then
		//If lErrNum = ErrValidation Or lErrNum = 0 And Not b Then
		//Dim objErr As CValidationError
		//For Each objErr In g_objRMApp.ValidationErrors
		//'GRD 4/15/2002  Internationalization
		//Set lstItem = frmValidateErrors.lstList.ListItems.Add(, , g_Localize.getMessage("CM_CFormOperation", "IFormOperation_Validate.ScriptValidation"))
		//lstItem.SubItems(1) = objErr.Description
		//Next
		//Else
		//Err.Raise lErrNum
		//End If
		//End If
		//
		//IFormOperation_Validate = IFormOperation_Validate And b
		//End If
		//End If
		//
		//' If validation didn't went through plug in form so Error report screen can link properly
		//If Not IFormOperation_Validate Then
		//If frmValidateErrors.lstList.ListItems.Count > 0 Then
		//frmValidateErrors.lblParentWindow = m_Form.hwnd
		//Beep
		//Else
		//Unload frmValidateErrors
		//End If
		//Else
		//' Validation OK for now validate children if any
		//' Validate all children first to make sure validation is complete
		//Dim pIFormOp As IFormOperation
		//Dim frmChild As Form
		//For Each frmChild In colChildren
		//Set pIFormOp = frmChild.objFormOperation
		//If pIFormOp.DataChanged Then
		//' If Child Save fails cancel loop
		//If Not pIFormOp.Validate() Then
		//IFormOperation_Validate = False ' Inidicate fail saving for whole group
		//Exit For
		//End If
		//End If
		//Next
		//End If
		//
		//hExit:
		//Exit Function
		//
		//hError:
		//IFormOperation_Validate = False
		//If iGeneralErrorExt(Err.Number, Err.Source, Err.Description, "CFormOperation.Validate") = vbRetry Then
		//Resume
		//End If
		//Resume hExit
	}
}
