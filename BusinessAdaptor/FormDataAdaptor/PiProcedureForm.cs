﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Application.SupportScreens;
//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PatientProcedure Screen.
	/// </summary>
	public class PiProcedureForm : DataEntryFormBase
	{
        //pmittal5  MITS:12030  07/15/08
		//const string CLASS_NAME = "PatientXProcedure";
        const string CLASS_NAME = "PiXProcedure";

        //To track patientid for MITS 11839
        private bool m_IsNew = false;

        //pmittal5  MITS:12030  07/15/08
		//private PatientXProcedure PatientXProcedure{get{return objData as PatientXProcedure;}}
        private PiXProcedure PiXProcedure { get { return objData as PiXProcedure; } }
		
		private LocalCache objCache{get{return objData.Context.LocalCache;}}

        //pmittal5  MITS:12030  07/15/08
		//const string FILTER_KEY_NAME = "PatientId";
        const string FILTER_KEY_NAME = "PiRowId";

        private string m_LINE_OF_BUS_SHORT_CODE = string.Empty; //-----sgupta320 Jira=17835
        
		/// <summary>
		/// Class constructor which initializes Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public PiProcedureForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = 	CLASS_NAME;		
		}

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();
			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
            //XmlNode objPatientId=null;
            //try
            //{
            //    objPatientId = base.SysEx.GetElementsByTagName(FILTER_KEY_NAME)[0];
            //}
            //catch{}; 
			
            //if(objPatientId !=null)
            //    this.m_ParentId = Conversion.ConvertObjToInt(objPatientId.InnerText, base.ClientId);


            ////Assign the Patient ID of the DataModel class 
            ////to the ParentId of the current class		
            //PatientXProcedure.PatientId = this.m_ParentId;

            ////Filter by this Patient ID if present
            //if(this.m_ParentId != 0)
            //{
            //    (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId;
            //}

            //pmittal5  MITS:12030  07/15/08

            //-----sgupta320 Jira=17835
            XmlDocument objXML = base.SysEx;
            XmlNode objPIRowId = null;
            try { objPIRowId = objXML.SelectSingleNode("/SysExData/PiRowId"); }
            catch { };
            this.m_LINE_OF_BUS_SHORT_CODE =
                CommonFunctions.GetLOBCode(objPIRowId.InnerText.ToString(), this.objCache.DbConnectionString);

            switch ((this.m_LINE_OF_BUS_SHORT_CODE).ToUpper())
            {
                case "GC":
                    m_SecurityId = RMO_GC_PERSONINVOLVED_Procedure;
                    break;
                case "DI":
                    m_SecurityId = RMO_DI_PERSONINVOLVED_Procedure;
                    break;
                case "PC":
                    m_SecurityId = RMO_PC_PERSONINVOLVED_Procedure;
                    break;
                case "WC":
                    m_SecurityId = RMO_WC_PERSONINVOLVED_Procedure;
                    break;
                case "VA":
                    m_SecurityId = RMO_VA_PERSONINVOLVED_Procedure;
                    break;
                case "POLICY":
                    m_SecurityId = RMO_POLICY_PERSONINVOLVED_Procedure;
                    break;
            }
            //----end sgupta320 Jira=17835

            this.m_ParentId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME, false);
            (objData as INavigation).Filter =
                objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId.ToString();
            this.PiXProcedure.PiRowId = this.m_ParentId;
	        //End - pmittal5

		}//end method InitNew()

        //Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
        public override void OnUpdateForm()
        {
            base.OnUpdateForm();

            //pmittal5  MITS:12030  07/15/08  -Changing the Class name of the form

            //Start by Shivendu for MITS 11839
            //if (this.PatientXProcedure.ProcRowId != 0)
            //{
            //    HipaaLog obj = new HipaaLog(this.PatientXProcedure.Context.DbConn.ConnectionString);
            //    string sFormName = "Patient Procedure";
            //    Patient objTmp = (Patient)this.PatientXProcedure.Context.Factory.GetDataModelObject("Patient", false);
            //    objTmp.MoveTo(this.PatientXProcedure.PatientId);
            //    int iPatientEid = objTmp.PatientEid;
            //    objTmp.Dispose();
            //    if (m_IsNew)
            //    {
            //        obj.LogHippaInfo(iPatientEid, "0", "0", sFormName, "NW", this.PatientXProcedure.Context.RMUser.LoginName);
            //    }
            //    else if (base.m_fda.SafeFormVariableParamText("SysCmd") != "6" && base.m_fda.SafeFormVariableParamText("SysCmd") != "5" && base.m_fda.SafeFormVariableParamText("SysCmd") != "7" && base.m_fda.SafeFormVariableParamText("SysCmd") != "8" && base.m_fda.SafeFormVariableParamText("SysCmd") != "")
            //    {
            //        obj.LogHippaInfo(iPatientEid, "0", "0", sFormName, "VW", this.PatientXProcedure.Context.RMUser.LoginName);
            //    }
            //}
            //End by Shivendu for MITS 11839

            if (this.PiXProcedure.PiProcRowId != 0)
            {
                HipaaLog obj = new HipaaLog(this.PiXProcedure.Context.DbConn.ConnectionString, base.ClientId);
                string sFormName = "Patient Procedure";
                PiPatient objTmp = (PiPatient)this.PiXProcedure.Context.Factory.GetDataModelObject("PiPatient", false);
                objTmp.MoveTo(this.PiXProcedure.PiRowId);
                int iPatientEid = objTmp.PiEid;
                objTmp.Dispose();
                if (m_IsNew)
                {
                    obj.LogHippaInfo(iPatientEid, "0", "0", sFormName, "NW", this.PiXProcedure.Context.RMUser.LoginName);
                }
                else if (base.m_fda.SafeFormVariableParamText("SysCmd") != "6" && base.m_fda.SafeFormVariableParamText("SysCmd") != "5" && base.m_fda.SafeFormVariableParamText("SysCmd") != "7" && base.m_fda.SafeFormVariableParamText("SysCmd") != "8" && base.m_fda.SafeFormVariableParamText("SysCmd") != "")
                {
                    obj.LogHippaInfo(iPatientEid, "0", "0", sFormName, "VW", this.PiXProcedure.Context.RMUser.LoginName);
                }
            }
            //End - pmittal5

            //----sgupta320: Jira-17835
            if (base.FormVariables.SelectSingleNode("//SysSid") != null)
                base.FormVariables.SelectSingleNode("//SysSid").InnerText = Convert.ToString(m_SecurityId);

        }
        public override void BeforeSave(ref bool bCancel)
        {
            //pmittal5  MITS:12030  07/15/08
            //m_IsNew = PatientXProcedure.IsNew;
            m_IsNew = PiXProcedure.IsNew;
        }
        public override void AfterSave()
        {
            //pmittal5  MITS:12030  07/15/08 -Changing the reference of the Class

            //Start by Shivendu for MITS 11839
            //HipaaLog obj = new HipaaLog(this.PatientXProcedure.Context.DbConn.ConnectionString);
            //string sFormName = "Patient Procedure";
            //Patient objTmp = (Patient)this.PatientXProcedure.Context.Factory.GetDataModelObject("Patient", false);
            //objTmp.MoveTo(this.PatientXProcedure.PatientId);
            //int iPatientEid = objTmp.PatientEid;
            //objTmp.Dispose();
            //if (m_IsNew == false)
            //{
            //    obj.LogHippaInfo(iPatientEid, "0", "0", sFormName, "UD", this.PatientXProcedure.Context.RMUser.LoginName);
            //}
            //End by Shivendu for MITS 11839

            HipaaLog obj = new HipaaLog(this.PiXProcedure.Context.DbConn.ConnectionString, base.ClientId);
            string sFormName = "Patient Procedure";
            PiPatient objTmp = (PiPatient)this.PiXProcedure.Context.Factory.GetDataModelObject("PiPatient", false);
            objTmp.MoveTo(this.PiXProcedure.PiRowId);
            int iPatientEid = objTmp.PiEid;
            objTmp.Dispose();
            if (m_IsNew == false)
            {
                obj.LogHippaInfo(iPatientEid, "0", "0", sFormName, "UD", this.PiXProcedure.Context.RMUser.LoginName);
            }

            //End - pmittal5
        }
        public override void BeforeDelete(ref bool Cancel)
        {
            //pmittal5  MITS:12030  07/15/08 -Changing the reference of the Class

            //Start by Shivendu for MITS 11839
            //HipaaLog obj = new HipaaLog(this.PatientXProcedure.Context.DbConn.ConnectionString);
            //string sFormName = "Patient Procedure";
            //Patient objTmp = (Patient)this.PatientXProcedure.Context.Factory.GetDataModelObject("Patient", false);
            //objTmp.MoveTo(this.PatientXProcedure.PatientId);
            //int iPatientEid = objTmp.PatientEid;
            //objTmp.Dispose();
            //obj.LogHippaInfo(iPatientEid, "0", "0", sFormName, "DL", this.PatientXProcedure.Context.RMUser.LoginName);
            //End by Shivendu for MITS 11839

            HipaaLog obj = new HipaaLog(this.PiXProcedure.Context.DbConn.ConnectionString, base.ClientId);
            string sFormName = "Patient Procedure";
            PiPatient objTmp = (PiPatient)this.PiXProcedure.Context.Factory.GetDataModelObject("PiPatient", false);
            objTmp.MoveTo(this.PiXProcedure.PiRowId);
            int iPatientEid = objTmp.PiEid;
            objTmp.Dispose();
            obj.LogHippaInfo(iPatientEid, "0", "0", sFormName, "DL", this.PiXProcedure.Context.RMUser.LoginName);

            //End - pmittal5
        }
	}
}
