﻿
using System;
using Riskmaster.DataModel;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Collections;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.Security.RMApp;
using Riskmaster.Settings;
using System.Text;
using System.Collections.Generic;

namespace Riskmaster.BusinessAdaptor
{
		/// <summary>
	/// Class implements common screen operations. Any extended operation  
	/// can be done in specific derived screen classes.                       
	/// Author: Brian Battah, 04/28/2005             
	/// </summary>
	public class DataEntryFormBase : FormBase
	{
	    internal const string FLDTYPE_ID = "id";
		internal const string FLDTYPE_TEXT = "text";
		internal const string FLDTYPE_CODE = "code";
		internal const string FLDTYPE_CODEDETAIL = "codewithdetail";
		internal const string FLDTYPE_ORGH = "orgh";
		internal const string FLDTYPE_DATE = "date";
		internal const string FLDTYPE_DATEBSCRIPT = "datebuttonscript";
		internal const string FLDTYPE_TIME = "time";
		internal const string FLDTYPE_MEMO = "memo";
		internal const string FLDTYPE_TEXTML = "textml";
		internal const string FLDTYPE_TEXT_LABEL = "textlabel";
		internal const string FLDTYPE_BOOL = "checkbox";
		internal const string FLDTYPE_RADIO = "radio";
		internal const string FLDTYPE_PHONE = "phone";
		internal const string FLDTYPE_SSN = "ssn";
		internal const string FLDTYPE_TAXID = "taxid";
		internal const string FLDTYPE_ZIP = "zip";
		internal const string FLDTYPE_POLICYNUMBER = "policynumberlookup";
		internal const string FLDTYPE_PLANNUMBER = "plannumberlookup";
		internal const string FLDTYPE_NUMERIC = "numeric";
		internal const string FLDTYPE_CURRENCY = "currency";
		internal const string FLDTYPE_CODELIST = "codelist";
		internal const string FLDTYPE_ENTITYLIST = "entitylist";
		internal const string FLDTYPE_EIDLOOKUP = "eidlookup";
		internal const string FLDTYPE_POLICYLOOKUP = "policylookup";
		internal const string FLDTYPE_PLANLOOKUP = "planlookup";
		internal const string FLDTYPE_POLICYSEARCH = "policysearch";
		internal const string FLDTYPE_LABEL = "label";
		internal const string FLDTYPE_COMBOBOX = "combobox";
		internal const string FLDTYPE_ACCOUNTLIST = "accountlist";
		internal const string FLDTYPE_READONLY = "readonly";
		internal const string FLDTYPE_EIN = "ein";
		internal const string FLDTYPE_HIDDEN = "hidden";
		internal const string ATT_REQUIRED = "required";
		internal const string ATT_READONLY = "readonly";
		internal const string ATT_TYPE = "type";
		internal const string ATT_CODEID = "codeid";
		internal const string ATT_TITLE = "title";
		internal const string ATT_NAME = "name";
		internal const string ATT_REF = "ref";
		internal const string ATT_IDREF = "idref";
        private bool b_IsSuppUpdatePermission = true;//Added for Mits 22873

		public DataEntryFormBase(FormDataAdaptor fda):base(fda)
		{
			//Stash "Parent" Reference.
			m_fda = fda;
		}
        //Deb ML Changes
        public string LanguageCode
        {
            get
            {
               return Convert.ToString(m_fda.userLogin.objUser.NlsCode);
            }
        }
        
        //Deb ML Changes
		internal object m_objData ;	// Data object which is a data object that 
		public string m_ClassName="";//smishra25:Changing the access modifier to public
        public bool b_UpdateFormCalledOnValidationFailure = false;
        public bool b_UpdateFormCalledOnValidationFailure_Funds = false;//rupal:mits 27178

		protected int m_ParentId;
		protected PreInitHandler m_PreInitHandler = null;
		override public void Init(){InitNew();}
		
		public virtual void InitNew()
		{
			string sTypeName;

			if(objData != null && m_ClassName=="")
				sTypeName = objData.GetType().Name;
			else
				sTypeName = m_ClassName;
			 //Allows DataModel to throw the exception if ClassName is invalid.
			if(m_PreInitHandler!=null)
				objData = this.m_fda.Factory.GetDataModelObject(sTypeName,false,m_PreInitHandler) as  DataObject;
			else
				objData = this.m_fda.Factory.GetDataModelObject(sTypeName,false) as  DataObject;
            
            //Enable this object to call custom script
            objData.FiringScriptFlag = 2;
		}
        //Added for Mits 22873
        public bool IsSuppUpdatePermission
        {
            get 
            { 
                return b_IsSuppUpdatePermission; 
            }
            set
            {
                b_IsSuppUpdatePermission = value;
            }

        }
        //Added for Mits 22873
        public virtual string GetCaption()
        {
            string sCaption = "";
            int captionLevel = 0;
            string sTmp = "";
            LobSettings objLobSettings = null;
            Event objEvent = null;
            Claim objClaim = null; //(Claim)objData.Context.Factory.GetDataModelObject("Claim", false);
            Claimant objClaimant = null;
            ClaimAdjuster objAdjuster = null;
            ClaimXLitigation objLitigation = null;
            FundsAutoBatch objFundsAuto = null;
            //rharma220 MITS 34388
            SysSettings objSettings = new SysSettings(this.Adaptor.connectionString, base.ClientId); //Ash - cloud
            objData.LoadParent();
            if (objData.Parent is Claim)
            {
                objClaim = (Claim)objData.Parent;
            }
            else if (objData.Parent is ClaimAdjuster)
            {
                objAdjuster = (ClaimAdjuster)objData.Parent;
                objAdjuster.LoadParent();
                objClaim = (Claim)objAdjuster.Parent;
            }
            else if (objData.Parent is ClaimXLitigation)
            {
                objLitigation = (ClaimXLitigation)objData.Parent;
                objLitigation.LoadParent();
                objClaim = (Claim)objLitigation.Parent;
            }
            /*else if (objData is FundsAutoBatch)
            {
                objFundsAuto = (FundsAutoBatch)objData;
                objClaim = (Claim)objData.Context.Factory.GetDataModelObject("Claim", false);
                objClaimant.MoveTo(objFundsAuto.c);
            }*/


            //Handle User Specific Caption Level
            if (objClaim != null && (!objClaim.IsNew || objClaim.PrimaryClaimant.ClaimantRowId != 0))
            {
                objLobSettings = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode];
                objEvent = (Event)objClaim.Parent;
                if (objLobSettings.CaptionLevel != 0)
                    captionLevel = objLobSettings.CaptionLevel;
                else
                    captionLevel = 1006;
                // Changed by Amitosh for Mits 24084 (02/28/2011)
                //if (captionLevel < 1006 || captionLevel > 1012)
                    if (captionLevel < 1005 || captionLevel > 1012)
                    captionLevel = 0;

                if (captionLevel != 1012)
                    sCaption = objClaim.Context.InternalSettings.CacheFunctions.GetOrgParent(objEvent.DeptEid, captionLevel, 0, ref captionLevel);//captionLevel discarded
                else
                {
                    objClaim.Context.InternalSettings.CacheFunctions.GetOrgInfo(objEvent.DeptEid, ref sCaption, ref sTmp);
                    sCaption += sTmp;
                }
                sCaption = sCaption.Substring(0, Math.Min(sCaption.Length, 25));
                               
                if (objClaim.EventId != 0)
                {
                    if (objData is Claimant)
                    {
                        objClaimant = (Claimant)objData;
                    }
                    else if (objClaim.PrimaryClaimant != null)
                    {
                        objClaimant = objClaim.PrimaryClaimant;
                    }
                    else if (objClaim.ClaimantList != null && objClaim.ClaimantList.Count > 0 && objClaim.ClaimantList[objClaim.ClaimantList.Count - 1] != null)
                    {
                        objClaimant = objClaim.ClaimantList[0]; 
                    }

                    if (objClaimant != null && objClaimant.ClaimantEntity != null)
                    {
                        //rharma220 MITS 34388 Start
                        if (objSettings.MultiCovgPerClm == -1)
                        {
                            int OldCid = objClaimant.ClaimantEid;
                            if(!string.IsNullOrEmpty(base.GetSysExDataNodeText("ClaimantEIdPassedIn")))
                            { 
                                objClaimant.ClaimantEid = Convert.ToInt32(base.GetSysExDataNodeText("ClaimantEIdPassedIn"));
                            }
                            sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * " +
                            objClaimant.ClaimantEntity.GetLastFirstName() + "]";
                            objClaimant.ClaimantEid = OldCid;
                        }
                        else
                        {
                            sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * " +
                                objClaimant.ClaimantEntity.GetLastFirstName() + "]";
                        }
                        //rharma220 MITS 34388 End
                    }
                    else
                    {
                        sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * - ]";
                    }
                }
            }
            return sCaption;
        }

		public virtual void BeforeAddNew(ref bool Cancel){;}
		public virtual void AfterAddNew(){;} 

		public virtual void BeforeDelete(ref bool Cancel){;}
		public virtual void AfterDelete(){;}

		public virtual void BeforeMoveFirst(ref bool Cancel ){;}
		public virtual void AfterMoveFirst(){;}

		public virtual void BeforeMoveLast(ref bool Cancel){;}
		public virtual void AfterMoveLast(){;}

		public virtual void BeforeMoveNext(ref bool Cancel){;}
		public virtual void AfterMoveNext(){;}

		public virtual void BeforeMovePrevious(ref bool Cancel){;}
		public virtual void AfterMovePrevious(){;}

		public virtual void BeforeMoveTo(ref bool Cancel, int[] keyValue){;}
		public virtual void AfterMoveTo(){;}


		public virtual void BeforeSave(ref bool Cancel){;}
		public virtual void AfterSave(){;}

		public virtual void BeforeShowJurisdictional(ref bool Cancel){;}
		public virtual void AfterShowJurisdictional(){;}

		public virtual void BeforeApplyLookupData(ref bool Cancel, ArrayList arrToSaveFields){;}
		public virtual void AfterApplyLookupData(XmlDocument objPropertyStore){;}

		public virtual void OnParentFormChanged(){;}

        //rupal:start
        /*
        1.This method can be used to perform any activity after validate() function and just before saving the record.
        2.This method is different from BeforeSave() method as AfterValidate() can be called after all the validations 
        have successfully validated and then perform some before save activities as well as validations for these before save activities in AfterValidate()
         * */
        public virtual bool AfterValidate() { return true; }
        //rupal:end


		//BSB Break out INav and IPer in addition to objData for the 
		// ADMTable case where it's not a bona-fide DataObject but 
		// supports the proper interfaces anyway.
		public DataObject objData
		{
			set{m_objData = value ;}
			get{return m_objData as  DataObject;}
		}
		public INavigation pINav
		{
			get{return m_objData as  INavigation;}
		}
		public IPersistence pIPer
		{
			get{return m_objData as  IPersistence;}
		}

        /// <summary>
        /// Retrieves the UserLoginLimits object 
        /// from the Session database
        /// </summary>
        /// <returns>instance of the UserLoginLimits object</returns>
        public UserLoginLimits GetUserLoginLimits()
        {
            const string SESSION_OBJ_USERLIMITS = "OBJ_RMUSERLIMITS";

            return base.m_fda.getCachedObject(SESSION_OBJ_USERLIMITS) as UserLoginLimits;
        }//method: GetUserLoginLimits()

		protected bool ValidateSupplementals()
		{
			// Returns True if validation is OK
			bool ret = true;
			try
			{
				//Dim lstItem =  ListItem
				SupplementalFieldTypes EN;
				Supplementals objSupps = null;
				bool b;

				if(m_objData.GetType().GetProperty("Supplementals")!=null)  //has prop check
					objSupps = (objData.GetProperty("Supplementals") as  Supplementals); //Fetch prop value.
				
				//Bail if the Current Object Doesn't Even Support Supplementals...
				if(objSupps ==null)
					return true;
                //deb
                //if (IsSuppUpdatePermission) //Added If for Mits 22873
                //{
                    if (objSupps.IsAnyRequired)
                    {

                        //' Form was not loaded do the validation on the object itself
                        foreach (SupplementalField objSuppField in objSupps)
                        {
                            EN = objSuppField.FieldType;
                            if (objSuppField.Required &&
                                EN != SupplementalFieldTypes.SuppTypePrimaryKey &&
                                EN != SupplementalFieldTypes.SuppTypeSecondaryKey && objSuppField.Visible)
                            {
                                b = false;
                                if (EN == SupplementalFieldTypes.SuppTypeClaimLookup ||
                                    EN == SupplementalFieldTypes.SuppTypeCode ||
                                    EN == SupplementalFieldTypes.SuppTypeEntity ||
                                    EN == SupplementalFieldTypes.SuppTypeEventLookup ||
                                    EN == SupplementalFieldTypes.SuppTypeVehicleLookup ||
                                    EN == SupplementalFieldTypes.SuppTypeCurrency)// JP 12/1/2003   Didn't handle entity supps
                                    b = ("" + objSuppField.Value == "" || "" + objSuppField.Value == "0");
                                else if (objSuppField.IsMultiValue || objSuppField.IsUserLookup)
                                    b = (objSuppField.Values.Count == 0);
                                //							TODO BSB Implement Supp Grid - Ugh (Needs support from DataModel.)
                                //							else if(EN == SupplementalFieldTypes.suppTypeGrid) //'VD 03/04/2005 Crawford Grid Control Supp field
                                //								b = (objSuppField.ArrayValue.ColCount > 0 && objSuppField.ArrayValue.RowCount == 0);
                                else if ("" + objSuppField.Value == "")
                                    b = true;
                                if (b)  //Set Validation Error
                                {
                                    this.Adaptor.m_err.Add("Validation Error", String.Format(Globalization.GetString("FormDataAdaptor.ScreenBase.ValidateSupplementals.RequiredSupplementalField", base.ClientId), objSuppField.Caption), Common.BusinessAdaptorErrorType.Error);
                                    ret = false;
                                }
                            }//if required and visible non-key field.
                        }//for each
                    }
                //}
                //else //Added else for Mits 22873
                //{
                //    this.Adaptor.m_err.Add("Validation Error", Globalization.GetString("SuppPermission.NoUpdate"), Common.BusinessAdaptorErrorType.Error);
                //    ret = false;
                //}
			}
			catch(Exception e)
			{
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.ValidateSupplementals.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
			}
			return ret;
		}
		//		TODO BSB Implement Supp Grid - Ugh (Needs support from DataModel.)
		//'VD 03/04/2005 Crawford Grid Control Supp field
		//For Each objSuppField In m_objData.Supplementals
		//EN = objSuppField.FieldType
		//If EN = suppTypeGrid And objSuppField.Required Then   'VD validate Grid only if required field
		//If objSuppField.ArrayValue.ColCount > 0 And objSuppField.Visible Then
		//If (objSuppField.ArrayValue.RowCount) < (objSuppField.ArrayValue.MinRows) Then
		//Set lstItem = frmValidateErrors.lstList.ListItems.Add(, , objSuppField.Caption)
		//lstItem.SubItems(1) = "There Should be minimum " & objSuppField.ArrayValue.MinRows & " row(s) in this supplemental grid."
		//ValidateSupplementals = False
		//End If
		//End If
		//End If
		//Next
		//}catch(Exception e)
		//{hError:
		//ValidateSupplementals = False
		//If iGeneralErrorExt(Err.Number, Err.Source, Err.Description, "CFormOperation.ValidateSupplementals") = vbRetry Then
		//End If
		//
		//	return ret;
        public virtual Hashtable GetTrimHashTable()
        {
            return new Hashtable();
        }
		public virtual bool AddNew()
		{
			bool bCancel=false;
			try
			{
				m_CurrentAction = enumFormActionType.AddNew;
                //deb:MITS 20003
				//if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_CREATE))
				//{
					//m_CurrentAction = enumFormActionType.None;
					//LogSecurityError(RMO_CREATE);
					//return false;
				//}
				
				BeforeAddNew(ref bCancel);
				if(!bCancel)
					BeforeAction(m_CurrentAction,ref bCancel);
				
				if(bCancel)
				{
					this.Cancelled=true;
					m_CurrentAction = enumFormActionType.None;
					return false;
				}

				//Addnew has been removed - just create a whole new object?
                //MITS 12028 duplicate with objFormBase.Init(); in FormDataAdaptor.Navigator
				//InitNew();

                //MITS 12028 Call custom script Init function
                ((DataRoot)m_objData).InitializeScriptData();

				AfterAddNew();
				AfterAction(m_CurrentAction);

				UpdateForm();
			}
			catch(Exception e)
			{
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.AddNew.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				m_CurrentAction = enumFormActionType.None;
				return false;
			}

			m_CurrentAction = enumFormActionType.None;
			return true;
		}
		
		public bool Delete() 
		{
			bool bCancel = false;
			try
			{
				m_CurrentAction = enumFormActionType.Delete;
				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_DELETE))
				{
					m_CurrentAction = enumFormActionType.None;
					LogSecurityError(RMO_DELETE);
					return false;
				}
				
				BeforeDelete(ref bCancel);
				if(!bCancel)
					BeforeAction(m_CurrentAction,ref bCancel);
				
				if(bCancel)
				{
					this.Cancelled=true;
					m_CurrentAction = enumFormActionType.None;
					return false;
				}
				ClearViewNodeChanges();
                //nsachdeva2 - MITS: 27470 - 02/16/2012
                //this.objData.FiringScriptFlag = 6;
                if (this.objData != null)
                {
                    this.objData.FiringScriptFlag = 6;
                }
                //End MITS: 27470
				pIPer.Delete();
				InitNew();
				AfterDelete();
				AfterAction(m_CurrentAction);

				UpdateForm();
			}				
			catch(Exception e)
			{
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.Delete.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				m_CurrentAction = enumFormActionType.None;
                foreach (System.Collections.DictionaryEntry objError in this.objData.Context.ScriptValidationErrors)
                    Errors.Add("ValidationScriptError", objError.Value.ToString(), Common.BusinessAdaptorErrorType.Error);
				return false;
			}
			m_CurrentAction = enumFormActionType.None;
			return true;
		}

		public bool MoveFirst() 
		{
			bool bCancel = false;
			try
			{
				m_CurrentAction = enumFormActionType.MoveFirst;
				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_VIEW))
				{
					m_CurrentAction = enumFormActionType.None;
					LogSecurityError(RMO_VIEW);
					return false;
				}
				
				BeforeMoveFirst(ref bCancel);
				if(!bCancel)
					BeforeAction(m_CurrentAction,ref bCancel);
				
				if(bCancel)
				{
					this.Cancelled=true;
					m_CurrentAction = enumFormActionType.None;
					return false;
				}
				ClearViewNodeChanges();
				pINav.MoveFirst();

				//If No Record Exists, then we have a security problem as this will turn into a
				// valid "AddNew" screen for this record - even if user has no "CREATE" permission.
				if((m_objData as ADMTable !=null &&   (m_objData as ADMTable).KeyValue==0)||
				   (m_objData as DataObject !=null && (m_objData as DataObject).KeyFieldValue==0))
					if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_CREATE))
					{
						m_CurrentAction = enumFormActionType.None;
						LogSecurityError(RMO_VIEW_BLANK_NO_CREATE);
						return false;
					}
				
				AfterMoveFirst();
				AfterAction(m_CurrentAction);

				UpdateForm();
				m_DataChanged = false;

			}				
			catch(Exception e)
			{
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.MoveFirst.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				m_CurrentAction = enumFormActionType.None;
				return false;
			}
			m_CurrentAction = enumFormActionType.None;
			return true;
		}
		public bool MoveLast() 
		{
			bool bCancel = false;
			try
			{
				m_CurrentAction = enumFormActionType.MoveLast;
				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_VIEW))
				{
					m_CurrentAction = enumFormActionType.None;
					LogSecurityError(RMO_VIEW);
					return false;
				}
				
				BeforeMoveLast(ref bCancel);
				if(!bCancel)
					BeforeAction(m_CurrentAction,ref bCancel);
				
				if(bCancel)
				{
					this.Cancelled=true;
					m_CurrentAction = enumFormActionType.None;
					return false;
				}
				ClearViewNodeChanges();

				pINav.MoveLast();
				
				AfterMoveLast();
				AfterAction(m_CurrentAction);
				UpdateForm();
				m_DataChanged = false;

			}				
			catch(Exception e)
			{
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.MoveLast.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				m_CurrentAction = enumFormActionType.None;
				return false;
			}
			m_CurrentAction = enumFormActionType.None;
			return true;
		}
		public bool MoveNext() 
		{
			bool bCancel = false;
			try
			{
				m_CurrentAction = enumFormActionType.MoveNext;
				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_VIEW))
				{
					LogSecurityError(RMO_VIEW);
					m_CurrentAction = enumFormActionType.None;
					return false;
				}
				
				BeforeMoveNext(ref bCancel);
				if(!bCancel)
					BeforeAction(m_CurrentAction,ref bCancel);
				
				if(bCancel)
				{
					this.Cancelled=true;
					m_CurrentAction = enumFormActionType.None;
					return false;
				}
				ClearViewNodeChanges();
				pINav.MoveNext();
				
				AfterMoveNext();
				AfterAction(m_CurrentAction);
				UpdateForm();
				m_DataChanged = false;

			}				
			catch(Exception e)
			{
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.MoveNext.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				m_CurrentAction = enumFormActionType.None;
				return false;
			}
			m_CurrentAction = enumFormActionType.None;
			return true;
		}
		public bool MovePrevious() 
		{
			bool bCancel = false;
			try
			{
				m_CurrentAction = enumFormActionType.MovePrevious;
				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_VIEW))
				{
					m_CurrentAction = enumFormActionType.None;
					LogSecurityError(RMO_VIEW);
					return false;
				}
				
				BeforeMovePrevious(ref bCancel);
				if(!bCancel)
					BeforeAction(m_CurrentAction,ref bCancel);
				
				if(bCancel)
				{
					this.Cancelled=true;
					m_CurrentAction = enumFormActionType.None;
					return false;
				}
				ClearViewNodeChanges();
				pINav.MovePrevious();
				
				AfterMovePrevious();
				AfterAction(m_CurrentAction);
				UpdateForm();
				m_DataChanged = false;

			}				
			catch(Exception e)
			{
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.MovePrevious.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				m_CurrentAction = enumFormActionType.None;
				return false;
			}
			m_CurrentAction = enumFormActionType.None;
			return true;
		}
	
		override public bool MoveTo(params int[] paramValue) //MITS 12151
		{
			// BSB "Claim Record Filtering Fix"- don't move if target recordid is 0.
			// This was clearing the LOB on claim which was set =  part of a record filter.
			if(paramValue[0]==0)
				return false;

			bool bCancel = false;
			try
			{
				m_CurrentAction = enumFormActionType.MoveTo;
                //MITS 31335: aanandpraka2 : Uncommenting the following lines of code. This was causing 
                //the NO VIEW func check to be skipped for all maintenance windows.                                        
                if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_VIEW);
                    return false;
                }                
                //MITS 31335: End changes

				BeforeMoveTo(ref bCancel,paramValue);
				if(!bCancel)
					BeforeAction(m_CurrentAction,ref bCancel);
				
				if(bCancel)
				{
					this.Cancelled=true;
					m_CurrentAction = enumFormActionType.None;
					return false;
				}

				if(paramValue.Length !=1)
                    Errors.Add("InvalidArgs", Globalization.GetString("FormDataAdaptor.ScreenBase.MoveTo.InvalidArgs", base.ClientId), Common.BusinessAdaptorErrorType.Error);

				ClearViewNodeChanges();
				pINav.MoveTo(paramValue[0]);
				
				AfterMoveTo();
				AfterAction(m_CurrentAction);

				UpdateForm();
				m_DataChanged = false;

			}				
			catch(Exception e)
			{
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.MoveTo.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				m_CurrentAction = enumFormActionType.None;
				return false;
			}
			m_CurrentAction = enumFormActionType.None;
			return true;

		}
//		public bool Refresh(){return Refresh(false);} 
//		public bool Refresh(bool viewOnly) 
//		{
//			bool bCancel = false;
//			try
//			{
//				m_CurrentAction = enumFormActionType.Refresh;
//				
//				int RMO_PERM;
//				if(pIPer !=null && pIPer.IsNew)
//					RMO_PERM = RMO_CREATE;
//				else
//					RMO_PERM= RMO_VIEW;
//
//				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_PERM))
//				{
//					m_CurrentAction = enumFormActionType.None;
//					return false;
//				}
//				
//				BeforeRefresh(ref bCancel);
//				if(!bCancel)
//					BeforeAction(m_CurrentAction,ref bCancel);
//				
//				if(bCancel)
//				{
//					m_CurrentAction = enumFormActionType.None;
//					return false;
//				}
//
//				if(!viewOnly && pIPer !=null)
//					pIPer.Refresh();
//				
//				UpdateForm();
//				m_DataChanged = false;
//
//				AfterRefresh();
//				AfterAction(m_CurrentAction);
//			}				
//			catch(Exception e)
//			{
//				Errors.Add(e,Globalization.GetString("FormDataAdaptor.ScreenBase.Refresh.Exception"),Common.BusinessAdaptorErrorType.Error);
//				m_CurrentAction = enumFormActionType.None;
//				return false;
//			}
//			m_CurrentAction = enumFormActionType.None;
//			return true;
//
//		}

		public bool Save()
		{
			bool bCancel = false;
			try
			{
				m_CurrentAction = enumFormActionType.Save;
				//deb : MITS 20003
                //if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_UPDATE))
                //{
                //    if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_CREATE) && pIPer.IsNew)
                //    {
                //        LogSecurityError(RMO_CREATE);
                //        m_CurrentAction = enumFormActionType.None;
                //        return false;
                //    }
					
                //    if(!pIPer.IsNew)
                //    {
                //        LogSecurityError(RMO_UPDATE);
                //        m_CurrentAction = enumFormActionType.None;
                //        return false;
                //    }
                //}
				
				// Pull in any comments that were saved into session previously
				if(objData != null )
				{
					if (objData.KeyFieldValue > 0)
					{
						string sFormName = this.GetType().Name;
						sFormName = sFormName.Substring(0, sFormName.Length - 4).ToLower(); // take off Form on end
						string sKey = sFormName.ToLower() + "|" + objData.KeyFieldValue.ToString();
						string sComments=null,sHTMLComments=null;

						if (Adaptor.cachedObjectExists(sKey + "|Comments"))
						{
							sComments = (string) Adaptor.getCachedObject(sKey + "|Comments");
							Adaptor.removeCachedObject(sKey + "|Comments");
							this.m_DataChanged = true;  // force this since we just changed comments
						}
						if (Adaptor.cachedObjectExists(sKey + "|HTMLComments"))
						{
							sHTMLComments = (string) Adaptor.getCachedObject(sKey + "|HTMLComments");
							Adaptor.removeCachedObject(sKey + "|HTMLComments");
							this.m_DataChanged = true;  // force this since we just changed comments
						}
						this.SetComments(sComments, sHTMLComments);   // set Comments and HTMLComments properties (overridden for EmployeeForm)
					}
				}

				if(this.m_DataChanged)
				{
					BeforeSave(ref bCancel);
					if(!bCancel)
						BeforeAction(m_CurrentAction,ref bCancel);
				
					if(bCancel)
					{
						this.Cancelled=true;
						m_CurrentAction = enumFormActionType.None;
						return false;
					}
				}
                //rupal: added the condition AfterValidate() to create reserves on the fly for first & final payment
                if (this.Validate() && this.AfterValidate())
                {
                    if (m_DataChanged && pIPer != null)
                    {
                        pIPer.Save();
                        m_DataChanged = false;
                    }
                    //No Children to "Loop Through"
                    ClearViewNodeChanges();
                    AfterSave();
                    AfterAction(m_CurrentAction);
                    UpdateForm();
                    m_DataChanged = false;

                    #region recent claims updates
                    
                    //Added by Nitin in order to implement recent claims and events functionality in R5
                    string recordType = string.Empty;
                    string recordId = string.Empty;
                    string recordNumber = string.Empty;

                    if (m_ClassName == "Event")
                    {
                        recordId = ((Riskmaster.DataModel.Event)(objData)).EventId.ToString();
                        recordNumber = ((Riskmaster.DataModel.Event)(objData)).EventNumber;
                        recordType = "event";

                        CommonFunctions.UpdateRecentRecords(this.Adaptor.connectionString
                            , this.Adaptor.userLogin.UserId.ToString()
                            , recordType
                            , recordId
                            , recordNumber,base.ClientId);
                    }
                    else if (m_ClassName == "Claim")
                    {
                        recordId = ((Riskmaster.DataModel.Claim)(objData)).ClaimId.ToString();
                        recordNumber = ((Riskmaster.DataModel.Claim)(objData)).ClaimNumber;
                        recordType = "claim";

                        CommonFunctions.UpdateRecentRecords(this.Adaptor.connectionString
                            , this.Adaptor.userLogin.UserId.ToString()
                            , recordType
                            , recordId
                            , recordNumber, base.ClientId);
                    }
                    else if (m_ClassName == "EventXDatedText")
                    {
                        //rsolanki2: mits 22598 recent claim updated for event dated text
                        Riskmaster.DataModel.EventXDatedText objEventXDatedText = objData as Riskmaster.DataModel.EventXDatedText;

                        if (objEventXDatedText != null)
                        {
                            recordId = objEventXDatedText.EventId.ToString();
                            recordType = "event";
                            CommonFunctions.UpdateRecentRecords(this.Adaptor.connectionString
                                , this.Adaptor.userLogin.UserId.ToString()
                                , recordType
                                , recordId, base.ClientId);
                        }
                    }
                    else if (m_ClassName == "EventXOsha")
                    {
                        //gdass2: mits 22681 recent claim updated for event Osha 
                        Riskmaster.DataModel.EventXOsha objEventXOsha = objData as Riskmaster.DataModel.EventXOsha;

                        if (objEventXOsha != null)
                        {
                            recordId = objEventXOsha.EventId.ToString();
                            recordType = "event";
                            CommonFunctions.UpdateRecentRecords(this.Adaptor.connectionString
                                , this.Adaptor.userLogin.UserId.ToString()
                                , recordType
                                , recordId, base.ClientId);
                        }
                    }
                    else if (m_ClassName == "AdjustDatedText")
                    {
                        Riskmaster.DataModel.AdjustDatedText objAdjustDatedText = objData as Riskmaster.DataModel.AdjustDatedText;

                        if (objAdjustDatedText != null)
                        {
                            recordId = string.Empty;//objAdjustDatedText.Properties.m_ParentId.ToString(); 

                            //rsolanki2: mits 22598 recent claim updated for adjuster dated text
                            //recordId 
                            //    = ((Riskmaster.DataModel.Claim)((((Riskmaster.DataModel.DataRoot)(objAdjustDatedText)).Parent).Parent)).ClaimId.ToString();
                            // the following is just the expansion of the above with null checks added.

                            Riskmaster.DataModel.DataRoot obj = objAdjustDatedText as Riskmaster.DataModel.DataRoot;
                            if (obj != null)
                            {
                                Riskmaster.DataModel.ClaimAdjuster objtemp = obj.Parent as Riskmaster.DataModel.ClaimAdjuster;
                                if (objtemp != null && objtemp.Parent != null)
                                {
                                    Riskmaster.DataModel.Claim objClaim = objtemp.Parent as Riskmaster.DataModel.Claim;
                                    if (objClaim!=null)
                                    {
                                        recordId = objClaim.ClaimId.ToString();
                                        recordNumber = objClaim.ClaimNumber.ToString();
                                        if (!string.IsNullOrEmpty(recordId))
                                        {
                                            recordType = "claim";
                                            CommonFunctions.UpdateRecentRecords(this.Adaptor.connectionString
                                                , this.Adaptor.userLogin.UserId.ToString()
                                                , recordType
                                                , recordId
                                                , recordNumber, base.ClientId);
                                        }
                                    }
                                    objClaim = null;
                                }
                                objtemp = null;
                            }
                            obj = null;
                        }

                    }
                    else
                    {
                        //rsolanki2: recent claim updates start: MITS 18828
                        if (objData != null)
                        {
                            Object objParent = (Riskmaster.DataModel.DataRoot)(objData).Parent;
                            if (objParent != null)
                            {
                                Type objectType = objParent.GetType();
                                if (objectType.FullName == "Riskmaster.DataModel.Claim")
                                {
                                    recordId = ((Riskmaster.DataModel.Claim)(objParent)).ClaimId.ToString();
                                    recordNumber = ((Riskmaster.DataModel.Claim)(objParent)).ClaimNumber;
                                    recordType = "claim";
                                    CommonFunctions.UpdateRecentRecords(this.Adaptor.connectionString
                                        , this.Adaptor.userLogin.UserId.ToString()
                                        , recordType
                                        , recordId
                                        , recordNumber, base.ClientId);
                                }
                                else if (objectType.FullName == "Riskmaster.DataModel.Event")
                                {
                                    recordId = ((Riskmaster.DataModel.Event)(objParent)).EventId.ToString();
                                    recordNumber = ((Riskmaster.DataModel.Event)(objParent)).EventNumber;
                                    recordType = "event";

                                    CommonFunctions.UpdateRecentRecords(this.Adaptor.connectionString
                                        , this.Adaptor.userLogin.UserId.ToString()
                                        , recordType
                                        , recordId
                                        , recordNumber, base.ClientId);
                                }

                            }
                            objParent = null;
                        }
                        //rsolanki2: recent claim updates end: MITS 18828                   
                    }
                    //Ended by Nitin in order to implement recent claims and events functionality in R5
                    #endregion
                }
                //else if (string.Compare(m_ClassName, "Funds", true)==0)
                //{
                //    //The following code will add the splits xml back to the response
                //    UpdateForm();
                //    m_DataChanged = false;
                //}//smishra25:MITS 19150 Commented else if, UpdateForm() will be called from FormDataAdaptor
                else
                {
                    //smishra25:Adding the switch statement.
                    switch (m_ClassName.ToUpper().Trim())
                    {
                        case "FUNDSAUTOBATCH":
                        case "FUNDS":
                        case "POLICYENH":
                            //rupal:start, mits 27178
                            //the similar flag already exists but it is set after updateform is called, I needed this flag set before updateform is called.
                            b_UpdateFormCalledOnValidationFailure_Funds = true;
                            //rupal:end
                             UpdateForm();//Calling update form for MITS 18937.
                             m_DataChanged = false;
                             b_UpdateFormCalledOnValidationFailure = true;
                            break;
                        default:
                            break;

                    }
                }
			}				
			catch(Exception e)
			{
                if((((Riskmaster.DataModel.DataRoot)(this.m_objData))).Context.ScriptValidationErrors.Count==0) //Mits id : 27990 - Govind
                    Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.Save.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				//BSB 04.26.2007 May have blown out on a nested validate invoked during save...
                //if statement added by Shivendu for MITS 19002
                if (string.Compare(m_ClassName, "ADMTable", true) == 0)
                {
                    foreach (System.Collections.DictionaryEntry objError in (((Riskmaster.DataModel.DataRoot)(this.m_objData))).Context.ScriptValidationErrors)
                        Errors.Add("ValidationScriptError", objError.Value.ToString(), Common.BusinessAdaptorErrorType.Error);
                }
                else
                {
                    foreach (System.Collections.DictionaryEntry objError in this.objData.Context.ScriptValidationErrors)
                        Errors.Add("ValidationScriptError", objError.Value.ToString(), Common.BusinessAdaptorErrorType.Error);
                }
				m_CurrentAction = enumFormActionType.None;
				return false;
			}
			m_CurrentAction = enumFormActionType.None;
			return true;

		}

		#region legacy Save Operation
		//Private Function IFormOperation_Save() =  Boolean
		//On Error GoTo hError
		//Dim boolCancel =  Boolean
		//Dim pIPer =  IPersistence
		//Dim pIFormOp =  IFormOperation
		//Dim frmChild =  Form
		//Dim boolEvent =  Boolean
		//
		//IFormOperation_Save = True
		//
		//m_CurrentAction = eatSave
		//
		//Set pIPer = m_objData
		//
		//If Not IsActionAllowed(m_SecurityId, RMO_UPDATE) Then
		//	If Not (IsActionAllowed(m_SecurityId, RMO_CREATE) And pIPer.IsNew) Then
		//For Each frmChild In colChildren
		//Set pIFormOp = frmChild.objFormOperation
		//' If Child Save fails cancel loop
		//If Not pIFormOp.Save() Then
		//IFormOperation_Save = False ' Inidicate fail saving for whole group
		//Exit For
		//End If
		//Next
		//'Beep
		//'IFormOperation_Save = False
		//GoTo hExit
		//End If
		//End If
		//
		//' Visit children and see if any are Memo forms. If so, have them update parent with their data   JP 1/4/2000
		//For Each frmChild In colChildren
		//If TypeOf frmChild Is MemoMDI Then
		//Set pIFormOp = frmChild.objFormOperation
		//pIFormOp.UpdateObject
		//End If
		//Next
		//
		//' Fire BeforeSave event
		//If m_DataChanged Then
		//' Raise Event only if we will attepmt to save
		//boolEvent = True
		//RaiseEvent BeforeSave(boolCancel)
		//If Not boolCancel Then RaiseEvent BeforeAction(m_CurrentAction, boolCancel)
		//If boolCancel Then IFormOperation_Save = False: GoTo hExit
		//End If
		//
		//'GRD 4/15/2002  Internationalization
		//subSetSystemStatus g_Localize.getMessage("CM_CFormOperation", "IFormOperation_Save.Saving", m_StatusFormName)
		//subSetStatusMeter 10
		//' Save Form Only if validation goes through
		//If IFormOperation_Validate() Then
		//subSetStatusMeter 20
		//' Save This object
		//If m_DataChanged And Not pIPer Is Nothing Then
		//subSetStatusMeter 30
		//'RaiseEvent UpdateObject       THIS IS MOVED TO VALIDATION
		//subSetStatusMeter 40
		//pIPer.Save
		//subSetStatusMeter 50
		//m_DataChanged = False
		//End If
		//Set pIPer = Nothing
		//'IFormOperation_Save = True
		//
		//' Loop through children if any and save them too
		//subSetStatusMeter 70
		//For Each frmChild In colChildren
		//Set pIFormOp = frmChild.objFormOperation
		//' If Child Save fails cancel loop
		//If Not pIFormOp.Save() Then
		//IFormOperation_Save = False ' Inidicate fail saving for whole group
		//Exit For
		//End If
		//Next
		//subSetStatusMeter 80
		//RaiseEvent UpdateForm
		//'02/11/02 VD this is just temporary, will be removed =  soon =  we 'll add object for T&E
		//'Dim frmA =  Form
		//'For Each frmA In Forms
		//'validation is OK, then update ACCT_REC by setting posted flag to -1
		//'If TypeName(frmA) = "frmTandEClaimLevel" Then
		//'frmA.PostIt
		//'End If
		//'Next
		//If IFormOperation_Save Then m_DataChanged = False
		//
		//' JP 4/24/2003  *BEGIN*   Moved call to AfterSave and AfterAction events here because they shouldn't be called if save failed because of validation error.
		//If boolEvent Then
		//' Raise event only if we attempted save
		//RaiseEvent AfterSave
		//RaiseEvent AfterAction(m_CurrentAction)
		//End If
		//' JP 4/24/2003  *END*
		//
		//subSetStatusMeter 100
		//Else
		//IFormOperation_Save = False
		//End If
		//
		//' JP 4/24/2003   Moved up. Shouldn't get called if validation fails.       If boolEvent Then
		//' JP 4/24/2003   Moved up. Shouldn't get called if validation fails.          ' Raise event only if we attempted save
		//' JP 4/24/2003   Moved up. Shouldn't get called if validation fails.          RaiseEvent AfterSave
		//' JP 4/24/2003   Moved up. Shouldn't get called if validation fails.          RaiseEvent AfterAction(m_CurrentAction)
		//' JP 4/24/2003   Moved up. Shouldn't get called if validation fails.       End If
		//
		//hExit:
		//m_CurrentAction = eatNone
		//subSetSystemStatus ""
		//subSetStatusMeter 0
		//Exit Function
		//
		//hError:
		//If iGeneralErrorExt(Err.Number, Err.Source, Err.Description, "CFormOperation.Save") = vbRetry Then
		//Resume
		//End If
		//IFormOperation_Save = False
		//Resume hExit
		//
		//End Function

		#endregion
		#region Juris and Supp Screen - TODO
		//Private Sub IFormOperation_ShowJurisdictional()
		//'Dim boolCancel =  Boolean
		//'On Error GoTo hError
		//'If Not m_SupportJurisdictional Then GoTo hExit
		//'
		//'RaiseEvent BeforeShowJurisdictional(boolCancel)
		//'If boolCancel Then GoTo hExit
		//'
		//'Rem JP 1/24/2000 Implemented
		//'' We need updated object
		//'RaiseEvent UpdateObject
		//'
		//'' Show jurisdictionals form
		//'Dim frmA =  Form
		//'
		//'For Each frmA In colChildren
		//'   If TypeOf frmA Is frmJurisdictionals Then
		//'      frmA.ZOrder
		//'      Exit Sub
		//'   End If
		//'Next
		//'
		//'Dim frmJur =  New frmJurisdictionals
		//'Load frmJur
		//'Dim pIFormOp =  IFormOperation
		//'Set pIFormOp = frmJur.objFormOperation
		//'Set pIFormOp.ParentForm = m_Form
		//'pIFormOp.SecurityId = m_SecurityId + JUR_OFFSET
		//'Set frmJur.Jurisdictionals = m_objData.Jurisdictionals
		//'frmJur.Caption = "Jurisdictional data for: " & m_Form.Caption
		//'frmJur.Show
		//'colChildren.Add frmJur, LCase$(frmJur.Name)
		//'
		//'RaiseEvent AfterShowJurisdictional
		//'
		//'hExit:
		//'   Exit Sub
		//'
		//'hError:
		//'If iGeneralErrorExt(Err.Number, Err.Source, Err.Description, "CFormOperation.ShowJurisdictional") = vbRetry Then
		//'   Resume
		//'End If
		//'
		//'Resume hExit
		//'
		//End Sub
		//
		//Private Sub IFormOperation_ShowSupplementals()
		//'Dim boolCancel =  Boolean
		//'On Error GoTo hError
		//'If Not m_SupportSupplementals Then GoTo hExit
		//'
		//'RaiseEvent BeforeShowSupplementals(boolCancel)
		//'If boolCancel Then GoTo hExit
		//'
		//'' We need updated object becouse of group = soc.
		//'RaiseEvent UpdateObject
		//'
		//'' Show supplementals form
		//'Dim frmA =  Form
		//'
		//'For Each frmA In colChildren
		//'   If TypeOf frmA Is frmSupplementals Then
		//'      frmA.ZOrder
		//'      Exit Sub
		//'   End If
		//'Next
		//'
		//'Dim frmSupp =  New frmSupplementals
		//'Load frmSupp
		//'Dim pIFormOp =  IFormOperation
		//'Set pIFormOp = frmSupp.objFormOperation
		//'Set pIFormOp.ParentForm = m_Form
		//'pIFormOp.SecurityId = m_SecurityId + SUPP_OFFSET
		//'Set frmSupp.Supplementals = m_objData.Supplementals
		//'frmSupp.Caption = "Supplemental data for: " & m_Form.Caption
		//'frmSupp.Show
		//'colChildren.Add frmSupp, LCase$(frmSupp.Name)
		//'
		//'RaiseEvent AfterShowSupplementals(frmSupp)
		//'
		//'hExit:
		//'   Exit Sub
		//'
		//'hError:
		//'If iGeneralErrorExt(Err.Number, Err.Source, Err.Description, "CFormOperation.ShowSupplementals") = vbRetry Then
		//'   Resume
		//'End If
		//'
		//'Resume hExit
		//'
		//End Sub
		//
		//
		//Private Property Let IFormOperation_SupportJurisdictional(ByVal RHS =  Boolean)
		//m_SupportJurisdictional = RHS
		//End Property
		//
		//Private Property Get IFormOperation_SupportJurisdictional() =  Boolean
		//IFormOperation_SupportJurisdictional = m_SupportJurisdictional
		//End Property
		//
		//
		//Private Property Let IFormOperation_SupportQuickDiary(ByVal RHS =  Boolean)
		//m_SupportQuickDiary = RHS
		//End Property
		//
		//Private Property Get IFormOperation_SupportQuickDiary() =  Boolean
		//IFormOperation_SupportQuickDiary = m_SupportQuickDiary
		//End Property
		//
		//Private Property Let IFormOperation_SupportSupplementals(ByVal RHS =  Boolean)
		//m_SupportSupplementals = RHS
		//End Property
		//
		//Private Property Get IFormOperation_SupportSupplementals() =  Boolean
		//IFormOperation_SupportSupplementals = m_SupportSupplementals
		//End Property
		//
		//
		#endregion
//		public void UpdateObject()
//		{
//			try
//			{
//				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_UPDATE))
//					return;
//				//By Default - Just apply any XML from the Adaptor to the Current Object
//				//				XmlDocument propertyStore = new XmlDocument();
//				//				propertyStore.LoadXml(sPropertyStore);
//				//
//				//				XmlDocument propertyStore = new XmlDocument();
//				//				propertyStore.LoadXml(sPropertyStore);
//
//				OnUpdateObject();
//				m_DataChanged = m_DataChanged || objData.AnyDataChanged;
//			}
//			catch(Exception e)
//			{
//				Errors.Add(e,Globalization.GetString("FormDataAdaptor.ScreenBase.UpdateObject.Exception"),Common.BusinessAdaptorErrorType.Error);
//				return;
//			}
//		}		
		override public void UpdateObject()
		{
			try
			{
//				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_UPDATE))
//				{
//					LogSecurityError(RMO_UPDATE);
//					return;
//				}
				OnUpdateObject();
                // m_DataChanged |= m_DataChanged || objData.AnyDataChanged; MITS 32157
                m_DataChanged = m_DataChanged || objData.AnyDataChanged;
                // RMA-9521 : Unable to save the last and first name in Event->Reported Info And  Maintenance->Policy tracking->Broker Information
                objData.DataChanged = m_DataChanged;
			}
			catch(Exception e)
			{
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.UpdateObject.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				return;
			}
		}		
		private void UpdateFormImpRecordSummary()
		{
			try
			{

//				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_VIEW))
//				{
//					LogSecurityError(RMO_VIEW);
//					return;
//				}

				bool bIsPowerView = (Conversion.ConvertStrToInteger(((XmlElement)SysView.SelectSingleNode("//form")).GetAttribute("viewid"))!=0);
				//bool bIsSuppViewAllowed = Adaptor.userLogin.IsAllowedEx(m_SecurityId + RMO_SUPP, RMO_VIEW);
                //Updated by Geeta sharma for mit 10729
                bool bIsSuppViewAllowed = Adaptor.userLogin.IsAllowedSuppEx(m_SecurityId + RMO_SUPP, RMO_ACCESS); 
				bool bIsJurisViewAllowed = Adaptor.userLogin.IsAllowedEx(m_SecurityId + JUR_OFFSET, RMO_VIEW);

				//If objData has supplementals and not PowerView then send back a view section suitable to 
				// present them.
				if(objData.GetType().GetProperty("Supplementals") !=null)
				{
					Supplementals objSupp = (objData.GetProperty("Supplementals",new object[]{}) as  Supplementals);

					if(bIsPowerView)
					{
                        //Start by Shivendu for Supplemental Grid
                        XmlElement gridEle = null;
                        string gridName = null;
                        XmlNodeList gridNodes = SysView.SelectNodes(@"//control[@type='ZapatecGrid']");
                        for (int i = 0; i < gridNodes.Count; i++)
                        {
                            gridEle = gridNodes[i] as XmlElement;
                            gridName = gridEle.GetAttribute("name");
                            gridName = gridName.ToUpper().Trim();
                            gridName = gridName.Substring(5);
                            foreach (SupplementalField objFld in objSupp)
                            {
                                if (objFld.FieldName == gridName)
                                {
                                    gridEle.SetAttribute("gridXML", objFld.gridXML.InnerXml);
                                    gridEle.SetAttribute("minRows", objFld.minRows);
                                    gridEle.SetAttribute("maxRows", objFld.maxRows);
                                }

                            }
                        }
                        //End by Shivendu for Supplemental Grid

                        //If it's power view then supps are in-lined.
						// Just apply Grp Assoc manually on selected fields.
						// Normally, it will be handled for the tab as part of ViewXml property
						if(objSupp.UsesAssociation)
							foreach(SupplementalField objFld in objSupp)
								if(!objFld.Visible)
									AddKillNode(objFld.FieldName.ToLower());
					}
					else if(bIsSuppViewAllowed)//Not PowerView and Has Supp View Permissions...
					{
						XmlElement objElem = SysViewSection.SelectSingleNode(@"//section[@name='suppdata']") as  XmlElement;
						if(objElem == null)
						{
							objElem = SysViewSection.CreateElement("section");
							objElem.SetAttribute("name","suppdata");
							SysViewSection.DocumentElement.AppendChild(objElem);
						}
						if(objSupp != null)
							objElem.InnerXml = objSupp.ViewXml.OuterXml;
					}
				}

				//If objData has jurisdictionals then send back a view section suitable to 
				// present them.
				if(objData.GetType().GetProperty("Jurisdictionals") !=null && bIsJurisViewAllowed)
				{
					XmlElement objElem = SysViewSection.SelectSingleNode(@"//section[@name='jurisdata']") as  XmlElement;
					if(objElem == null)
					{
						objElem = SysViewSection.CreateElement("section");
						objElem.SetAttribute("name","jurisdata");
						SysViewSection.DocumentElement.AppendChild(objElem);
					}
					Jurisdictionals objJur = (objData.GetProperty("Jurisdictionals",new object[]{}) as  Jurisdictionals);
					if(objJur !=null)
						objElem.InnerXml =objJur.ViewXml.OuterXml;
				}

				//If objData has acord and not a PowerView then send back a view section suitable to 
				// present them.
				if(objData.GetType().GetProperty("Acord") !=null && !bIsPowerView)
				{
					XmlElement objElem = SysViewSection.SelectSingleNode(@"//section[@name='acorddata']") as  XmlElement;
					if(objElem == null)
					{
						objElem = SysViewSection.CreateElement("section");
						objElem.SetAttribute("name","acorddata");
						SysViewSection.DocumentElement.AppendChild(objElem);
					}
					Acord objAcord = (objData.GetProperty("Acord",new object[]{}) as  Acord);
					if(objAcord !=null)
						objElem.InnerXml =objAcord.ViewXml.OuterXml;
				}

				OnUpdateForm();
				// non fatal warning errors for MCIC
				foreach( System.Collections.DictionaryEntry objError in this.objData.Context.ScriptWarningErrors)
					Errors.Add("WarningScriptError", objError.Value.ToString() ,Common.BusinessAdaptorErrorType.Warning);

				m_DataChanged = false;
			}
			catch(Exception e)
			{
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.UpdateForm.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				return;
			}
		}

		override public void UpdateForm()
		{
			try
			{

//				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_VIEW))
//				{
//					LogSecurityError(RMO_VIEW);
//					return;
//				}
                if (SysView.SelectSingleNode("//form") != null)
                {
                    UpdateFormImpRecordSummary();
                }
                else
                {
                    bool bIsPowerView = false; // (Conversion.ConvertStrToInteger(((XmlElement)SysView.SelectSingleNode("//form")).GetAttribute("viewid")) != 0);
                    //bool bIsSuppViewAllowed = Adaptor.userLogin.IsAllowedEx(m_SecurityId + RMO_SUPP, RMO_VIEW);
                    //Updated by Geeta sharma for mit 10729
                    //MGaba2:MITS 23888: SMS for "Litigation - Supplementals" screen is not working properly.
                    //bool bIsSuppViewAllowed = Adaptor.userLogin.IsAllowedSuppEx(m_SecurityId + RMO_SUPP, RMO_ACCESS);
                    bool bIsJurisViewAllowed = Adaptor.userLogin.IsAllowedEx(m_SecurityId + JUR_OFFSET, RMO_VIEW);

                    //If objData has supplementals and not PowerView then send back a view section suitable to 
                    // present them.
                    /*
                    if(objData.GetType().GetProperty("Supplementals") !=null)
                    {
                        Supplementals objSupp = (objData.GetProperty("Supplementals",new object[]{}) as  Supplementals);

                        if(bIsPowerView)
                        {
                            //Start by Shivendu for Supplemental Grid
                            XmlElement gridEle = null;
                            string gridName = null;
                            XmlNodeList gridNodes = SysView.SelectNodes(@"//control[@type='ZapatecGrid']");
                            for (int i = 0; i < gridNodes.Count; i++)
                            {
                                gridEle = gridNodes[i] as XmlElement;
                                gridName = gridEle.GetAttribute("name");
                                gridName = gridName.ToUpper().Trim();
                                gridName = gridName.Substring(5);
                                foreach (SupplementalField objFld in objSupp)
                                {
                                    if (objFld.FieldName == gridName)
                                    {
                                        gridEle.SetAttribute("gridXML", objFld.gridXML.InnerXml);
                                        gridEle.SetAttribute("minRows", objFld.minRows);
                                        gridEle.SetAttribute("maxRows", objFld.maxRows);
                                    }

                                }
                            }
                            //End by Shivendu for Supplemental Grid

                            //Start by Shivendu for MITS 10766
                            string suppFieldName = null;
                            bool suppFieldNotDeleted = false;
                            XmlElement suppFieldInView = null;
                            XmlNodeList suppFieldsList = SysView.SelectNodes(@"//group[@name='suppgroup']//control");
                            if (suppFieldsList != null)
                            {
                            
                                for (int i = 0; i < suppFieldsList.Count; i++)
                                {
                                    suppFieldInView = suppFieldsList[i] as XmlElement;
                                    suppFieldName = suppFieldInView.GetAttribute("name");
                                    suppFieldName = suppFieldName.ToUpper().Trim();
                                    suppFieldName = suppFieldName.Substring(5);
                                    suppFieldNotDeleted = false;
                                    foreach (SupplementalField objFld in objSupp)
                                    {
                                        if (objFld.FieldName == suppFieldName)
                                        {
                                            suppFieldNotDeleted = true;
                                        }

                                    }
                                    if (!suppFieldNotDeleted)
                                    {
                                        suppFieldName = "supp_" + suppFieldName; 
                                        AddKillNode(suppFieldName.ToLower());
                                    }
                                
                                }
                            }
                            //End by Shivendu for MITS 10766


                            //If it's power view then supps are in-lined.
                            // Just apply Grp Assoc manually on selected fields.
                            // Normally, it will be handled for the tab as part of ViewXml property
                            if(objSupp.UsesAssociation)
                                foreach(SupplementalField objFld in objSupp)
                                    if(!objFld.Visible)
                                        AddKillNode(objFld.FieldName.ToLower());
                        }
                        else if(!bIsSuppViewAllowed)//Not PowerView and Has Supp View Permissions...
                        {
                            XmlElement objElem = SysViewSection.SelectSingleNode(@"//section[@name='suppdata']") as  XmlElement;
                            if(objElem == null)
                            {
                                objElem = SysViewSection.CreateElement("section");
                                objElem.SetAttribute("name","suppdata");
                                SysViewSection.DocumentElement.AppendChild(objElem);
                            }
                            if(objSupp != null)
                                objElem.InnerXml = objSupp.ViewXml.OuterXml;
                        }
                    }
                    */
                    //MGaba2:MITS 23888: SMS for "Litigation - Supplementals" screen is not working properly.
                    //if (!bIsSuppViewAllowed)
                    //    KillNodes.Add("suppgroupk", "suppgroup");

                    //If objData has jurisdictionals then send back a view section suitable to 
                    // present them.
                    //Just to kill the node if it's allowed.
                    if (objData.GetType().GetProperty("Jurisdictionals") != null && bIsJurisViewAllowed)//Parijat : Jurisdictionals
                    {

                        //Parijat:For jurisdictional tab -filling the data in sysviewSection
                        XmlElement objElem = SysViewSection.SelectSingleNode(@"//section[@name='jurisdata']") as XmlElement;
                        if (objElem == null)
                        {
                            objElem = SysViewSection.CreateElement("section");
                            objElem.SetAttribute("name", "jurisdata");
                            SysViewSection.DocumentElement.AppendChild(objElem);
                        }
                        Jurisdictionals objJur = (objData.GetProperty("Jurisdictionals", new object[] { }) as Jurisdictionals);
                        if (objJur != null)
                            objElem.InnerXml = objJur.ViewXml.OuterXml;

                    }
                    else//Parijat: Jurisdictionals
                    {
                        if (!KillNodes.ContainsKey("pvjurisgroupk"))
                            KillNodes.Add("pvjurisgroupk", "pvjurisgroup");
                    }

                    //If objData has acord and not a PowerView then send back a view section suitable to 
                    // present them.
                    /* This should be generated by powerview update tool
                    if(objData.GetType().GetProperty("Acord") !=null && !bIsPowerView)
                    {
                        XmlElement objElem = SysViewSection.SelectSingleNode(@"//section[@name='acorddata']") as  XmlElement;
                        if(objElem == null)
                        {
                            objElem = SysViewSection.CreateElement("section");
                            objElem.SetAttribute("name","acorddata");
                            SysViewSection.DocumentElement.AppendChild(objElem);
                        }
                        Acord objAcord = (objData.GetProperty("Acord",new object[]{}) as  Acord);
                        if(objAcord !=null)
                            objElem.InnerXml =objAcord.ViewXml.OuterXml;
                    }
                     */
                }
				OnUpdateForm();
                //MGaba2:MITS 23888: SMS for "Litigation - Supplementals" screen is not working properly.
                //Reason Being Security Id was not available before OnUpdateForm
                bool bIsSuppViewAllowed = Adaptor.userLogin.IsAllowedSuppEx(m_SecurityId + RMO_SUPP, RMO_ACCESS);

                //srajindersin - to fix an error that arises if Key is already present in hashtable - 03/20/2012
                //if (!bIsSuppViewAllowed)
                //  KillNodes.Add("suppgroupk", "suppgroup");
                if ((!bIsSuppViewAllowed) && (!KillNodes.Contains("suppgroupk")))
                    KillNodes.Add("suppgroupk", "suppgroup");
                //END srajindersin - to fix an error that arises if Key is already present in hashtable - 03/20/2012

				// non fatal warning errors for MCIC
				foreach( System.Collections.DictionaryEntry objError in this.objData.Context.ScriptWarningErrors)
					Errors.Add("WarningScriptError", objError.Value.ToString() ,Common.BusinessAdaptorErrorType.Warning);

				m_DataChanged = false;
			}
			catch(Exception e)
			{
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.UpdateForm.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				return;
			}
		}
		override public bool Refresh(){return Refresh(false);} 
		override public bool Refresh(bool viewOnly) 
		{
		bool bCancel = false;
		try
		{
		m_CurrentAction = enumFormActionType.Refresh;
				
		int RMO_PERM;
		if(pIPer !=null && pIPer.IsNew)
		RMO_PERM = RMO_CREATE;
		else
		RMO_PERM= RMO_VIEW;

		if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_PERM))
		{
			m_CurrentAction = enumFormActionType.None;
			LogSecurityError(RMO_PERM);
			return false;
		}
				
		BeforeRefresh(ref bCancel);
		if(!bCancel)
		BeforeAction(m_CurrentAction,ref bCancel);
					
		if(bCancel)
		{
			this.Cancelled=true;
			m_CurrentAction = enumFormActionType.None;
			return false;
		}

		if(!viewOnly && pIPer !=null)
			pIPer.Refresh();

		ClearViewNodeChanges();
			
		AfterRefresh();
		AfterAction(m_CurrentAction);

		UpdateForm();
		m_DataChanged = false;

		}				
		catch(Exception e)
		{
            Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.Refresh.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
		m_CurrentAction = enumFormActionType.None;
		return false;
		}
		m_CurrentAction = enumFormActionType.None;
		return true;

}
		override public bool Validate()
		{
			bool bCancel = false;
			bool bRet = true;
			try
			{
	//			UpdateObject();
				if(m_DataChanged)
				{
					
					//Do Any Custom Validation Stuff
					OnValidate(ref bCancel);
					bRet = bRet && !bCancel;
					
					// Doing this validation after Custom allows 
					// custom logic to set Supp Values prior to attempting supp validation.
					if(objData.GetType().GetProperty("Supplementals") !=null)
						bRet &= ValidateSupplementals();

					//Run Validation from DataModel
					if(pIPer != null)
					{
						bRet &= pIPer.Validate();
						foreach( System.Collections.DictionaryEntry objError in this.objData.Context.ScriptValidationErrors)
							Errors.Add("ValidationScriptError", objError.Value.ToString() ,Common.BusinessAdaptorErrorType.Error);
					}
					//No Children to validate so we're done.	
				}

			}
			catch(Exception e)
			{
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.Validate.Exception", base.ClientId), Common.BusinessAdaptorErrorType.Error);
				return false;
			}
			return bRet;
		}       

		public string ApplyLookupData()
		{
			string sClassName="";
			string sAttachNodePath="";
			string sResultConfig="";
			int recordId=0;
			bool bAttachToSysEx = false;
			XmlDocument objAttachToDom = new XmlDocument();

			string sDataXml="";
			XmlDocument objResultDom = new XmlDocument();
			XmlDocument objPropertyStore = new XmlDocument();
			XmlDocument objConfigDom = new XmlDocument();
			string sClassSaveField="";
			ArrayList arrClassSaveFields=new ArrayList();  // Field to save entry on if Add New or Edit
			Hashtable objSaveFields = new Hashtable();
			string sSaveEntry="";
            XmlNode objNode = null;
			bool bCancel=false;

			m_CurrentAction = enumFormActionType.ApplyLookupData;
			
			BeforeApplyLookupData(ref bCancel,arrClassSaveFields);
			if(!bCancel)
				BeforeAction(m_CurrentAction,ref bCancel);

			//Fetch "Lookup Parameter Data"
			try{sClassName=SysLookup.SelectSingleNode("/SysLookup/SysLookupClass").InnerText;}
			catch{}
			try{sAttachNodePath=SysLookup.SelectSingleNode("/SysLookup/SysLookupAttachNodePath").InnerText;}
			catch{}
			try{recordId=Conversion.ConvertStrToInteger(SysLookup.SelectSingleNode("/SysLookup/SysLookupRecordId").InnerText);}
			catch{}
			try{sResultConfig=SysLookup.SelectSingleNode("/SysLookup/SysLookupResultConfig").InnerXml;}
			catch{}
			try
			{
				// Split out XPath name of node to save if we're doing add new or edit
				int iPos = sClassName.IndexOf('|');
				if (iPos > -1)
				{
					sClassSaveField = sClassName.Substring(iPos + 1);
					sClassName = sClassName.Substring(0, iPos);
					arrClassSaveFields.Add(sClassSaveField);
				}

				//Pick up the existing XML Branch that we are replacing and use it =  input 
				// for the SerializationConfig info in the obj.SerializeObject(existingXMLDom) call.
				string sPropertyStore = "";
				string sContainerSerialConfig = "";
				bAttachToSysEx = (sAttachNodePath.IndexOf("SysExData") !=-1);

				if(m_fda.HasParam("SysPropertyStore"))
					sPropertyStore = m_fda.SafeParam("SysPropertyStore").InnerXml; 
				
				if(m_fda.HasFormVariableParam("SysSerializationConfig"))
					sContainerSerialConfig = m_fda.SafeFormVariableParam("SysSerializationConfig").InnerXml; 
				

				if(sPropertyStore=="")
					sPropertyStore="<Instance />";
				objPropertyStore.LoadXml(sPropertyStore);

				//Example of attach point on SysEx rather than PropertyStore.
				//Instance/UI/FormVariables/SysExData/Parent/Instance/Event/EventId 
				if(bAttachToSysEx)
				{
					sAttachNodePath = sAttachNodePath.Substring(sAttachNodePath.IndexOf("SysExData"));
					objConfigDom.LoadXml(String.Format("<{0} />",sClassName));					
					objAttachToDom = this.SysEx;
				}
				else
				{
					objResultDom.LoadXml("<Instance>" + sContainerSerialConfig + "</Instance>");
					objConfigDom = Utilities.XPointerDoc(sAttachNodePath,objResultDom);
					objAttachToDom = objPropertyStore;
				}

				//Get the Data
				DataObject obj = this.m_fda.Factory.GetDataModelObject(sClassName,false) as  DataObject;
				if(obj==null)
                    Errors.Add(Globalization.GetString("LookupError", base.ClientId), String.Format(Globalization.GetString("LookupData.Exception", base.ClientId), sClassName, recordId), BusinessAdaptorErrorType.Error);
				
				if(recordId>0)
					obj.MoveTo(recordId);
			
				//Get the xml (Apply Specific SerializationConfig if one was supplied.)
				if(sResultConfig.Trim() !="")
                {
                    // Start Naresh Checks whether the Config Dom is null, if yes then Allocates Memory
                    if(objConfigDom == null)
                        objConfigDom = new XmlDocument();
                    // End Naresh Checks whether the Config Dom is null, if yes then Allocates Memory
                    //objConfigDom.LoadXml("<" + sResultConfig + "/>");
                    sResultConfig = sResultConfig.Replace("&lt;", "<").Replace("&gt;", ">");
                    objConfigDom.LoadXml(sResultConfig);
                }

				objResultDom.LoadXml(obj.SerializeObject(objConfigDom));

				//Save original value if necessary.
				if (recordId<=0)   // only copy saved fields if this is an Add New case. Never do this for a lookup entity.
					foreach(string sField in arrClassSaveFields)
					{
                        objNode = objAttachToDom.SelectSingleNode(sAttachNodePath + "/" + sField); 
                        if (objNode != null) //Added Null Check for Mits 22246
                        {
                            sSaveEntry = objNode.InnerText;
                            objSaveFields.Add(sField, sSaveEntry);
                        }
					}

				//Apply the new xml.
				XmlNode objTarget = objAttachToDom.SelectSingleNode(sAttachNodePath);
				XmlNode objSource = objAttachToDom.CreateElement(objTarget.Name);
				objSource.InnerXml = objResultDom.DocumentElement.InnerXml;
				objTarget.ParentNode.ReplaceChild(objSource,objTarget);
				
				// Replace original value if necessary (Add New or Edit from lookup screen)
				// if (sSaveEntry!="")
				foreach(string sField in objSaveFields.Keys)
				{
					XmlCDataSection objCData=null;
					objTarget = objAttachToDom.SelectSingleNode(sAttachNodePath + "/" + sField);
					objCData = objAttachToDom.CreateCDataSection(objSaveFields[sField].ToString());

					// Remove any existing CData and then add in the new one
					foreach(XmlNode oNode in objTarget.ChildNodes)
						if (oNode.NodeType == XmlNodeType.CDATA)
							objTarget.RemoveChild(oNode);

					objTarget.AppendChild(objCData);
				}

				// BSB Default implementation returns the sysproperty as a string and must continue to do so.
				// However, it is important to know that in the case where the attachnode was in SysEx,
				// this updated SysEx will be sent to the client by the Navigate routine as it packages up the XML from 
				// the this.SysEx DOM object.
				AfterApplyLookupData(objAttachToDom);
				AfterAction(m_CurrentAction);


				//Save the changes to the web service parameter for our Response
				sDataXml = objPropertyStore.OuterXml;
				
				//BSB 02.03.2006 Added full "action" logic to update Sreen View after Lookup.
				XmlDocument propertyStore = new XmlDocument();
				propertyStore.LoadXml(sDataXml);
				objData.PopulateObject(propertyStore); //Update the in memory instance.
				UpdateObject(); // BSB\Vaibhav - Allow update Freshly Populated object with SysEx data etc if necessary.
				
				UpdateForm(); //Render Screen based on new memory instance.

			}
            catch (Exception e) { throw new ExceptionTypes.RMAppException(String.Format(Globalization.GetString("LookupData.Exception", base.ClientId), sClassName, recordId, sAttachNodePath), e); }
			finally{base.m_CurrentAction= enumFormActionType.None;}
			return sDataXml;
		}	
		
		/// <summary>
		/// Updates comments properties on object. Can be overridden for cases like EmployeeForm which don't have comments fields on top level object.
		/// </summary>
		/// <param name="sComments"></param>
		/// <param name="sHTMLComments"></param>
		virtual protected void SetComments(string sComments, string sHTMLComments)
		{
			if (sComments != null)
				objData.SetProperty("Comments", sComments);

			if (sHTMLComments != null)
				objData.SetProperty("HTMLComments", sHTMLComments);
		}

		//BSB TODO - This function enforced fields set =  required in REQ_FIELDS database table.
		// In World this is meaningfull. In RMNet these settings are simply ignored.  The question is:
		// What to do about it in Wizard.  Obviously a direct port is impossible since the linkage from
		// record to screen control uses Visual Basic control names which are not available in the GUI
		// any more...
		// Stubbed for now.
		private bool UnivValidate()
		{


			return true;
		}

		//BSB 2005.11.15 The view sections (Supp, Juris,Acord etc) that were most recently sent to the GUI 
		// are available in the SysViewSections and not directly in the SysView.
		// For that reson we tack them in here as appropriate.
		internal virtual bool ValidateRequiredViewFields(XmlDocument propertyStore)
		{
			//Temporarily Insert the Dynamic Sections of the View.
			// Example: 		<section name="acorddata"/>
			XmlElement objInsertUnder = null;
			XmlElement objEltToCopy = null;
			XmlNodeList objNodes = this.SysViewSection.SelectNodes("//section");
			string sectionName = "";
			XmlDocument objDom = new XmlDocument();
			objDom.LoadXml(this.SysPostedView.OuterXml);
			foreach(XmlNode objNode in objNodes)
			{
				objEltToCopy = objNode as XmlElement; 
				sectionName = objEltToCopy.GetAttribute(ATT_NAME);
				objInsertUnder = objDom.SelectSingleNode(String.Format("//section[@name='{0}']",sectionName)) as XmlElement;
				if(objInsertUnder != null)
					objInsertUnder.InnerXml = objEltToCopy.OuterXml;
			}
			return ValidateRequiredViewFields(propertyStore,objDom);

		}

		//Purpose: To ensure that any field marked =  "required" at the GUI still\actually 
		// has a value that arrives here at the business layer.
		//Param: propertyStore is the xml "instance" document that contains "values".
		//Param: domTarget is the view document or dynamic section that has required controls to validate.
		//Return "true" if validation is successfull.
		internal bool ValidateRequiredViewFields(XmlDocument propertyStore, XmlDocument domTarget)
		{
			if(propertyStore==null)
				return false;
			string sTmp = "";
			string sFieldTitle = "";
			string sInvFields = "";
			string [] arrInvFields = {""};
			bool bVisible = true;
			bool bIsValid = true;
            //shruti FMLA starts
            bool bIsLeaveOnly = false;
            //shruti FMLA ends
			XmlNodeList objNodes = domTarget.SelectNodes("//control");
			XmlElement objField =null;
			XmlElement objValue =null;

			XmlElement objNotReqNew = (XmlElement) this.SysView.SelectSingleNode("//internal[@name='SysNotReqNew']");

			//-- ABhateja, 08.14.2006, MITS 7627 -START-
			//-- Get all the invisible supp. fields from SysInvisible (Group Assoc. Case)
			XmlElement objInvisible = propertyStore.SelectSingleNode("//SysInvisible") as XmlElement;
			if(objInvisible != null)
			{
				sInvFields = objInvisible.InnerXml;

				if(sInvFields.Trim() != "")
				{
					arrInvFields = new string[1];
					if(sInvFields.IndexOf(",") > 0)
						arrInvFields = sInvFields.Split(',');
					else
						arrInvFields[0] = sInvFields;
				}
			}
			//-- ABhateja, 08.14.2006, MITS 7627 -END-
			
			if(objNotReqNew ==null)
				objNotReqNew = SysView.CreateElement("dummy");

            //shruti FMLA starts
            XmlNode objleave = propertyStore.SelectSingleNode("//LeaveOnlyFlag");
            if (objleave != null)
            {
                if (objleave.InnerText.ToLower() == "true")
                {
                    bIsLeaveOnly = true;
                }
            }
            //shruti FMLA ends
			foreach(XmlNode objNode in objNodes)
			{
				objField = objNode as  XmlElement;

				//-- ABhateja, 08.09.2006, MITS 7627 -START-
				//-- Supplemental fields handling
				//-- Skip "invisible" fields (case when using Group Assoc.)
				sTmp = objField.GetAttribute(ATT_NAME);
				if(sTmp.Length>=5 && sTmp.Substring(0,5) == "supp_")
				{
					for(int i=0;i<arrInvFields.Length;i++)
					{
						if(objField.GetAttribute(ATT_NAME).ToLower().Trim() == arrInvFields[i].ToLower().Trim())
						{
							bVisible = false;		
							break;
						}
					}

					if(!bVisible)
						continue;
				}
				//-- ABhateja, 08.09.2006, MITS 7627 -END-

				//  Check required fields
				if(objField.GetAttribute(ATT_REQUIRED) == "yes")
				{	//Exclude fields marked =  not required for a new Record
                    //Shruti FMLA Enhancement starts
                    if(bIsLeaveOnly)
                    {
                        if (objField.GetAttribute(ATT_NAME) == "jurisdiction" || objField.GetAttribute(ATT_NAME) == "planname" || objField.GetAttribute(ATT_NAME) == "disfromdate")
                            continue;
                    }
                    //Shruti FMLA Enhancement ends
					if(this.objData.IsNew && ((string) objNotReqNew.GetAttribute("value")).IndexOf(objField.GetAttribute("name"))>=0)
						continue;

					// Get Field type needed to check required thing
					sTmp = objField.GetAttribute(ATT_TYPE);
					if(!(sTmp == FLDTYPE_CODELIST | sTmp == FLDTYPE_COMBOBOX | sTmp == FLDTYPE_ENTITYLIST)) 
					{
						//BSB Add "pre-emptive case" for @idref as the "data" location specified on a control.
						if(objField.HasAttribute(ATT_IDREF))
							objValue = (XmlElement) propertyStore.SelectSingleNode(objField.GetAttribute(ATT_IDREF).Replace("UI/FormVariables/",""));
						else
							objValue = propertyStore.SelectSingleNode(objField.GetAttribute(ATT_REF).Replace("UI/FormVariables/","")) as XmlElement;

						if(objValue==null)
						{
							bIsValid = false;
							sFieldTitle  = objField.GetAttribute(ATT_TITLE);
							break;
						}
						if(objValue.HasAttribute(ATT_CODEID) && (0 == Conversion.ConvertObjToInt(objValue.GetAttribute(ATT_CODEID), base.ClientId)))
						{
							bIsValid = false;
							sFieldTitle  = objField.GetAttribute(ATT_TITLE);
							break;
						}
						if("" == objValue.InnerText.Trim())
						{
							bIsValid = false;
							sFieldTitle  = objField.GetAttribute(ATT_TITLE);
							break;
						}
					}
				}
			}
			if(!bIsValid) // This field is required and it is empty, write error log and cancel operation
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId), String.Format(Globalization.GetString("DataEntryFormBase.RequiredFieldError", base.ClientId), sFieldTitle),
					BusinessAdaptorErrorType.Error);

			return bIsValid;
		}

		protected void PeoplePermissionChecks4PI(string sFormName, int iOwnEditSecurityID)
		{
			XmlNode objFormNode = null;
			XmlNodeList objCreatableNodeList = null;
			XmlNodeList objEntityLockNodeList = null;
			bool bAllowCreateNewEntity = false;
			bool bIsNewEntity = false;
			bool bAllowEdit = false;
			
			if( !base.ModuleSecurityEnabled )
				return;

			if(Adaptor.userLogin.IsAllowedEx(RMO_PEOPLE+RMO_CREATE))
				bAllowCreateNewEntity = true;

			if(Adaptor.userLogin.IsAllowedEx(iOwnEditSecurityID) && Adaptor.userLogin.IsAllowedEx(RMO_PEOPLE+RMO_UPDATE))
				bAllowEdit = true;

			//Check if the user try to add an existing entity as a new person involved.
			//(m_objData as PersonInvolved).PiEid
			bIsNewEntity = (0 >= base.GetSysExDataNodeInt("/SysExData/PiEid",false));

			if( pIPer.IsNew ) //For new record
			{
				if( bAllowCreateNewEntity )
					return;
				else if( bIsNewEntity  )
				{
					LogSecurityError(RMO_CREATE);
					return;
				}
			}
			else
			{
				if( bAllowEdit && bAllowCreateNewEntity )
					return;
			}
	
			objFormNode = base.SysView.SelectSingleNode("//form[@name='" + sFormName + "']");
			if( objFormNode == null )
				return;

			//Force to choose an existing one
			objCreatableNodeList = objFormNode.SelectNodes("//control[@creatable='1']");
			if( objCreatableNodeList != null )
			{
				foreach(XmlNode objCreatableNode in objCreatableNodeList)
				{
					objCreatableNode.Attributes.RemoveNamedItem("creatable");
				}
			}

			//Make all control with attribute entitylock ='true' to readonly
			objEntityLockNodeList = objFormNode.SelectNodes("//control[(@entitylock='true') and (@type !='id') and (@type !='hidden')]");
			if( objEntityLockNodeList != null )
			{
				foreach(XmlNode objEntityLockNode in objEntityLockNodeList)
				{
					XmlElement objElt = objEntityLockNode as XmlElement;

					if(objElt.GetAttribute("type")=="entitylookup" || objElt.GetAttribute("type")=="pientitylookup")
						objElt.SetAttribute("locked", "true");  // locked is text box disabled but selection button still there
					else
						base.AddReadOnlyNode( objEntityLockNode.Attributes.GetNamedItem("name").InnerText );
				}
			}

		}

		protected void PeoplePermissionChecks4Other(string sFormName, int iOwnEditSecurityID)
		{
			PeopleEntityPermissionChecks(sFormName, iOwnEditSecurityID, RMO_PEOPLE);
		}

		protected void EntityPermissionChecks(string sFormName, int iOwnEditSecurityID)
		{
			PeopleEntityPermissionChecks(sFormName, iOwnEditSecurityID, RMO_ENTITYMAINTENANCE);
		}

		protected void PeopleEntityPermissionChecks(string sFormName, int iOwnEditSecurityID, int iPeopleEntitySecID)
		{
			XmlNode objFormNode = null;
			XmlNodeList objCreatableNodeList = null;
			XmlNodeList objPrefixedNodeList = null;
			XmlAttribute objAttr = null;
			string sFieldMark = "";
			string sSysCmd = "";
			bool bIsNewRecord = false;
			bool bAllowCreate = false;
			bool bAllowEdit = false;
			
			if( !base.ModuleSecurityEnabled )
				return;

			if(Adaptor.userLogin.IsAllowedEx(iPeopleEntitySecID+RMO_CREATE))
				bAllowCreate = true;

			if(Adaptor.userLogin.IsAllowedEx(iOwnEditSecurityID) && Adaptor.userLogin.IsAllowedEx(iPeopleEntitySecID+RMO_UPDATE))
				bAllowEdit = true;

			if( pIPer.IsNew ) //For new record
			{
				if( bAllowCreate )
					return;

				bIsNewRecord = true;
			}
			else
			{
				if( bAllowEdit && bAllowCreate )
					return;
			}
	
			objFormNode = base.SysView.SelectSingleNode("//form[@name='" + sFormName + "']");
			if( objFormNode == null )
				return;

			objCreatableNodeList = objFormNode.SelectNodes("//control[@creatable='1']");
			foreach(XmlNode objCreatableNode in objCreatableNodeList)
			{
				objCreatableNode.Attributes.RemoveNamedItem("creatable");
				//if( bIsNewRecord || bAllowEdit)
				//	continue;
				
				objAttr = (XmlAttribute)objCreatableNode.Attributes.GetNamedItem("fieldmark");
				if( objAttr != null )
				{
					sFieldMark = objAttr.InnerText;
					objPrefixedNodeList = objFormNode.SelectNodes("//control[starts-with(@name,'" + sFieldMark + "') and @type !='id' and (@type !='hidden')]");
					if( objPrefixedNodeList == null )
						continue;

					foreach(XmlNode objPrefixedNode in objPrefixedNodeList)
					{
						XmlElement objElt = objPrefixedNode as XmlElement;

						if(objElt.GetAttribute("type")=="entitylookup" || objElt.GetAttribute("type")=="pientitylookup")
							objElt.SetAttribute("locked", "true");
						else
							base.AddReadOnlyNode( objPrefixedNode.Attributes.GetNamedItem("name").InnerText );
					}
				}
			}

			//Make all control with attribute entitylock ='true' to readonly
			objPrefixedNodeList = objFormNode.SelectNodes("//control[(@entitylock='true') and (@type !='id') and (@type !='hidden')]");
			if( objPrefixedNodeList != null )
			{
				foreach(XmlNode objEntityLockNode in objPrefixedNodeList)
				{
					XmlElement objElt = objEntityLockNode as XmlElement;

					if(objElt.GetAttribute("type")=="entitylookup" || objElt.GetAttribute("type")=="pientitylookup")
						objElt.SetAttribute("locked", "true");  // locked is text box disabled but selection button still there
					else
						base.AddReadOnlyNode( objEntityLockNode.Attributes.GetNamedItem("name").InnerText );
				}
			}


		}
        
        //MITS 11579 Abhishek Start
        /// <summary>
        /// Function checks the security permission to Entry/Edit of entity fields.
        /// If no permssion than entity fields are diabled for editing
        /// </summary>
        /// <param name="sFormName">form name</param>
        /// <param name="iOwnEditSecurityID">security Id</param>
        protected void AdjusterPermissionChecksEntityEdit(string sFormName, int iOwnEditSecurityID)
        {
            //XmlNode objFormNode = null;
            //XmlNodeList objCreatableNodeList = null;
            //XmlNodeList objPrefixedNodeList = null;
            //XmlAttribute objAttr = null;
            //string sFieldMark = "";

            if (!base.ModuleSecurityEnabled)
                return;

            if (Adaptor.userLogin.IsAllowedEx(iOwnEditSecurityID))
                return;

            //objFormNode = base.SysView.SelectSingleNode("//form[@name='" + sFormName + "']");
            //if (objFormNode == null)
            //    return;

            //objCreatableNodeList = objFormNode.SelectNodes("//control[@creatable='1']");
            //foreach (XmlNode objCreatableNode in objCreatableNodeList)
            //{
            //    objCreatableNode.Attributes.RemoveNamedItem("creatable");

            //    objAttr = (XmlAttribute)objCreatableNode.Attributes.GetNamedItem("fieldmark");
            //    if (objAttr != null)
            //    {
            //        sFieldMark = objAttr.InnerText;
            //        objPrefixedNodeList = objFormNode.SelectNodes("//control[starts-with(@name,'" + sFieldMark + "') and @type !='id' and (@type !='hidden')]");
            //        if (objPrefixedNodeList == null)
            //            continue;

            //        foreach (XmlNode objPrefixedNode in objPrefixedNodeList)
            //        {
            //            XmlElement objElt = objPrefixedNode as XmlElement;

            //            if (objElt.GetAttribute("type") == "entitylookup" || objElt.GetAttribute("type") == "pientitylookup")
            //                objElt.SetAttribute("locked", "true");
            //            else
            //                base.AddReadOnlyNode(objPrefixedNode.Attributes.GetNamedItem("name").InnerText);
            //        }
            //    }
            //}

            ////Make all control with attribute entitylock ='true' to readonly
            //objPrefixedNodeList = objFormNode.SelectNodes("//control[(@entitylock='true') and (@type !='id') and (@type !='hidden')]");
            //if (objPrefixedNodeList != null)
            //{
            //    foreach (XmlNode objEntityLockNode in objPrefixedNodeList)
            //    {
            //        XmlElement objElt = objEntityLockNode as XmlElement;

            //        if (objElt.GetAttribute("type") == "entitylookup" || objElt.GetAttribute("type") == "pientitylookup")
            //            objElt.SetAttribute("locked", "true");  // locked is text box disabled but selection button still there
            //        else
            //            base.AddReadOnlyNode(objEntityLockNode.Attributes.GetNamedItem("name").InnerText);
            //    }
            //}


            // Added by Akashyap3 on 04-May-09 : MITS no. : 15797
            //base.AddReadOnlyNode("adjlastname");
            base.AddReadOnlyNode("adjfirstname");
            base.AddReadOnlyNode("adjtitle");
            base.AddReadOnlyNode("adjmiddlename");
            base.AddReadOnlyNode("adjaddr1");
            base.AddReadOnlyNode("adjphone1");
            base.AddReadOnlyNode("adjaddr2");
            base.AddReadOnlyNode("adjaddr3");
            base.AddReadOnlyNode("adjaddr4");
            base.AddReadOnlyNode("adjphone2");
            base.AddReadOnlyNode("adjcity");
            base.AddReadOnlyNode("adjfaxnumber");
            base.AddReadOnlyNode("adjstateid");
            base.AddReadOnlyNode("adjzipcode");
            base.AddReadOnlyNode("adjcountrycode");

            //---- Added by Ashutosh Kashyap on 15-May-09 for MITS : 15797
            ArrayList singleRow = null;
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.TextBox.ToString());
            base.AddElementToList(ref singleRow, "id", "adjlastname");
            base.AddElementToList(ref singleRow, "ReadOnly", "True");
            base.AddElementToList(ref singleRow, "CssClass", "disabled");                        
            base.m_ModifiedControls.Add(singleRow);
            //-----------------------------------------------------------

        }
        //MITS 11579 End

        /// <summary>
        /// tkr MITS 8764.  Fill Event Detail location information from
        /// the Department IF all 4 criteria are met:
        ///1.  non-save postback (SysCmd of 7, see form.js, FillEventLocationDetails()) AND 
        ///2.  a dept is selected AND
        ///3.  On Premises checkbox selected AND
        ///4.  ALL event location fields are empty
        /// </summary>
        /// <param name="objEvent">Event object</param>
        protected void PopulateEventDetailLocation(Event objEvent)
        {
            //MGaba2:MITS 15642:changed the if condition so that it should load data if user is asking it to do so
            //if (m_fda.SafeFormVariableParamText("SysCmd") == "7"    
            //        && objEvent.OnPremiseFlag == true 
            //        && objEvent.DeptEid != 0
            //        && string.IsNullOrEmpty(objEvent.Addr1)
            //        && string.IsNullOrEmpty(objEvent.Addr2)
            //        && string.IsNullOrEmpty(objEvent.City)
            //        && objEvent.StateId == 0
            //        && string.IsNullOrEmpty(objEvent.ZipCode)
            //        && string.IsNullOrEmpty(objEvent.CountyOfInjury)
            //        && objEvent.CountryCode == 0)
                 if (m_fda.SafeFormVariableParamText("SysCmd") == "7"    
                    && objEvent.OnPremiseFlag == true 
                    && objEvent.DeptEid != 0 )
            {
                string SQL = String.Format(@"SELECT * FROM ENTITY WHERE ENTITY_ID = {0}", objEvent.DeptEid);
                using (DbReader objReader = objEvent.Context.DbConnLookup.ExecuteReader(SQL))
                {
                    while (objReader.Read())
                    {
                        objEvent.Addr1 = Conversion.ConvertObjToStr(objReader.GetValue("ADDR1"));
                        objEvent.Addr2 = Conversion.ConvertObjToStr(objReader.GetValue("ADDR2"));
                        objEvent.Addr3 = Conversion.ConvertObjToStr(objReader.GetValue("ADDR3"));
                        objEvent.Addr4 = Conversion.ConvertObjToStr(objReader.GetValue("ADDR4"));
                        objEvent.City = Conversion.ConvertObjToStr(objReader.GetValue("CITY"));
                        objEvent.StateId = Conversion.ConvertObjToInt(objReader.GetValue("STATE_ID"), base.ClientId);
                        objEvent.ZipCode = Conversion.ConvertObjToStr(objReader.GetValue("ZIP_CODE"));
                        objEvent.CountyOfInjury = Conversion.ConvertObjToStr(objReader.GetValue("COUNTY"));
                        objEvent.CountryCode = Conversion.ConvertObjToInt(objReader.GetValue("COUNTRY_CODE"), base.ClientId);
                    }//while
                    objReader.Close();
                }
            }
        }

        //avipinsrivas Start : Worked for JIRA - 340 (Entity Role)
        /// <summary>
        /// This Function will be called from person involved to check if Entity id for the same Event is Exist or not
        /// </summary>
        /// <param name="iEventID">Event ID of Person Involved</param>
        /// <param name="iEntityID">Entity ID of Entity DataModel</param>
        /// <param name="iErRowID">Entity Role ID(PI_ER_ROW_ID) of Person Involved</param>
        /// <param name="sFrom">Calling Person Involved Page Name From Gloalization.cs</param>
        /// <param name="sConnString">Used Connection string for Query</param>
        /// <param name="bUseEntityRole">True if Use Entity Role Setting is on else False</param>
        /// <returns>will return true if Person_Involved --> PI_ROW_ID column > 0</returns>
        public bool CheckPIEntity(int iEventID, int iEntityID, int iErRowID, string sFrom, string sConnString, bool bUseEntityRole, string sLangCode = "1033")
        {
            int iQryResult = 0;
            bool bOutput = false;
            StringBuilder sbQuery = null;
            Dictionary<string, int> objDictParams = null;
            object objCount = null;
            bool bSuccess;
            try
            {
                sbQuery = new StringBuilder();
                objDictParams = new Dictionary<string, int>();
                
                sbQuery.Append(" SELECT MAX (PI_ROW_ID) ");
                sbQuery.Append(" FROM PERSON_INVOLVED ");
                sbQuery.Append(" WHERE EVENT_ID = {0} ");
                sbQuery.Append(" AND PI_EID = {1} ");
                if (bUseEntityRole)
                    sbQuery.Append(" AND PI_ER_ROW_ID = {2} ");
                sbQuery.Replace("{0}", "~EVENTID~");
                sbQuery.Replace("{1}", "~ENTITYID~");
                if(bUseEntityRole)
                    sbQuery.Replace("{2}", "~ERROWID~");
                objDictParams.Add("EVENTID", iEventID);
                objDictParams.Add("ENTITYID", iEntityID);
                if(bUseEntityRole)
                    objDictParams.Add("ERROWID", iErRowID);

                objCount = DbFactory.ExecuteScalar(sConnString, sbQuery.ToString(), objDictParams);
                if (objCount != null && objCount != System.DBNull.Value && Conversion.CastToType<Int32>(objCount.ToString(), out bSuccess) > 0)
                    iQryResult = Conversion.CastToType<Int32>(objCount.ToString(), out bSuccess);
                if (iQryResult > 0)
                {
                    bOutput = true;
                    if (string.Equals(sFrom, Globalization.PersonInvolvedGlossaryTableNames.MEDSTAFF.ToString(), StringComparison.OrdinalIgnoreCase))
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.MedStaffEventAlreadyExists", base.ClientId, sLangCode), Riskmaster.BusinessAdaptor.Common.BusinessAdaptorErrorType.Error);
                    else if (string.Equals(sFrom, Globalization.PersonInvolvedGlossaryTableNames.EMPLOYEE.ToString(), StringComparison.OrdinalIgnoreCase))
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.EmployeeEventAlreadyExists", base.ClientId, sLangCode), BusinessAdaptorErrorType.Error);
                    else if (string.Equals(sFrom, Globalization.PersonInvolvedGlossaryTableNames.DRIVER.ToString(), StringComparison.OrdinalIgnoreCase))
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.DriverEventAlreadyExists", base.ClientId, sLangCode), BusinessAdaptorErrorType.Error);
                    else if (string.Equals(sFrom, Globalization.PersonInvolvedGlossaryTableNames.OTHERPERSON.ToString(), StringComparison.OrdinalIgnoreCase))
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.OtherPersonEventAlreadyExists", base.ClientId, sLangCode), BusinessAdaptorErrorType.Error);
                    else if (string.Equals(sFrom, Globalization.PersonInvolvedGlossaryTableNames.PATIENT.ToString(), StringComparison.OrdinalIgnoreCase))
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.PatientEventAlreadyExists", base.ClientId, sLangCode), BusinessAdaptorErrorType.Error);
                    else if (string.Equals(sFrom, Globalization.PersonInvolvedGlossaryTableNames.PHYSICIAN.ToString(), StringComparison.OrdinalIgnoreCase))
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.PhysicianEventAlreadyExists", base.ClientId, sLangCode), BusinessAdaptorErrorType.Error);
                    else if (string.Equals(sFrom, Globalization.PersonInvolvedGlossaryTableNames.WITNESS.ToString(), StringComparison.OrdinalIgnoreCase))
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.WitnessEventAlreadyExists", base.ClientId, sLangCode), BusinessAdaptorErrorType.Error);
                }
            }
            finally
            {
                sbQuery = null;
                objDictParams = null;
                objCount = null;
            }

            return bOutput;
        }
        //avipinsrivas End

        //rsolanki2 : mits 18828: moving this function to riskmaster.common.CommonFunction class

        /// <summary>
        /// created by Nitin in order to implement recent claims and events functionality in R5
        /// </summary>
        /// <param name="recordType"></param>
        /// <param name="recordId"></param>
        /// <param name="recordNumber"></param>

        //private void UpdateRecentRecords(string recordType, string recordId, string recordNumber)
        //{
        //    string sSql = string.Empty;
        //    string sXml = string.Empty;
        //    DbReader objReader = null;
        //    XmlDocument objPrefXml = null;
        //    XmlNode recentRecordNode = null;
        //    XmlNode tempNode = null;
        //    XmlElement newRecentNode = null;
        //    XmlNodeList recentRecordList = null;
        //    XmlAttribute atbRecordType = null;
        //    XmlAttribute atbRecordId = null;
        //    XmlAttribute atbRecordNumber = null;
        //    DbConnection objConn = null;
        //    DbCommand objCommand = null;
        //    DbParameter objParam = null;
        //    int recentRecordCount = 0;

        //    try
        //    {
        //        objConn = DbFactory.GetDbConnection(Adaptor.connectionString);
        //        objConn.Open();
        //        objCommand = objConn.CreateCommand();

        //        sSql = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID=" + Adaptor.userID;
        //        objReader = DbFactory.GetDbReader(Adaptor.connectionString, sSql);
        //        if (objReader.Read())
        //        {
        //            sXml = objReader.GetValue("PREF_XML").ToString();
        //        }
        //        objReader.Close();

        //        if (sXml != string.Empty)
        //        {
        //            try
        //            {
        //                objPrefXml = new XmlDocument();
        //                objPrefXml.LoadXml(sXml);
        //            }
        //            catch (Exception)
        //            {
        //                //delete bad formatted xml string from database
        //                sSql = "DELETE FROM USER_PREF_XML WHERE USER_ID=" + Adaptor.userID;
        //                objCommand.Parameters.Clear();
        //                objCommand.CommandText = sSql;
        //                objCommand.ExecuteNonQuery();
        //                sXml = string.Empty;
        //            }
        //        }

        //        if (sXml != string.Empty)
        //        {
        //            recentRecordNode = objPrefXml.SelectSingleNode("setting/RecentRecords");

        //            if (recentRecordNode != null)
        //            {
        //                //if same node already exists then don't do any thing and return 
        //                if (recentRecordNode.SelectSingleNode("record[(@recordid='" + recordId + "') and (@recordtype='" + recordType + "')]") != null)
        //                {
        //                    return;
        //                }

        //                recentRecordList = recentRecordNode.SelectNodes("record[@recordtype='" + recordType + "']");
        //                recentRecordCount = recentRecordList.Count;

        //                if (recentRecordCount == 10)
        //                {
        //                    //remove the very first record and inser this new record at last postition
        //                    tempNode = recentRecordList.Item(0);
        //                    recentRecordNode.RemoveChild(tempNode);
        //                }
        //            }
        //            else
        //            {
        //                recentRecordNode = objPrefXml.CreateNode(XmlNodeType.Element, "RecentRecords", null);
        //                objPrefXml.SelectSingleNode("setting").AppendChild(recentRecordNode);
        //            }

        //            newRecentNode = objPrefXml.CreateElement("record");

        //            atbRecordType = objPrefXml.CreateAttribute("recordtype");
        //            atbRecordType.Value = recordType;
        //            newRecentNode.Attributes.Append(atbRecordType);

        //            atbRecordId = objPrefXml.CreateAttribute("recordid");
        //            atbRecordId.Value = recordId;
        //            newRecentNode.Attributes.Append(atbRecordId);

        //            atbRecordNumber = objPrefXml.CreateAttribute("recordnumber");
        //            atbRecordNumber.Value = recordNumber;
        //            newRecentNode.Attributes.Append(atbRecordNumber);

        //            recentRecordNode.AppendChild(newRecentNode);

        //            sXml = objPrefXml.InnerXml;

        //            //Update USER_PREF_XML table with the updated XML
        //            sSql = "UPDATE USER_PREF_XML SET PREF_XML=~PXML~ WHERE USER_ID=" + Adaptor.userID;
        //            objCommand.Parameters.Clear();
        //            objParam = objCommand.CreateParameter();
        //            objParam.Direction = System.Data.ParameterDirection.Input;
        //            objParam.Value = sXml;
        //            objParam.ParameterName = "PXML";
        //            objParam.SourceColumn = "PREF_XML";
        //            objCommand.Parameters.Add(objParam);
        //            objCommand.CommandText = sSql;
        //            objCommand.ExecuteNonQuery();
        //        }
        //        else
        //        {
        //            //if user xml is not present in the database then insert a record for the same
        //            sXml = "<setting><RecentRecords><record recordtype='" + recordType + "' recordid='" + recordId + "' recordnumber='" + recordNumber + "'/></RecentRecords></setting>";
        //            sSql = "INSERT INTO USER_PREF_XML(USER_ID,PREF_XML) VALUES(" + Adaptor.userID + ", ~PXML~)";
        //            objCommand.Parameters.Clear();
        //            objParam = objCommand.CreateParameter();
        //            objParam.Direction = System.Data.ParameterDirection.Input;
        //            objParam.Value = sXml;
        //            objParam.ParameterName = "PXML";
        //            objParam.SourceColumn = "PREF_XML";
        //            objCommand.Parameters.Add(objParam);
        //            objCommand.CommandText = sSql;
        //            objCommand.ExecuteNonQuery();
        //        }
        //    }
        //    //Riskmaster.Db is throwing following exception so need to catch the same
        //    catch (RMAppException p_objExp)
        //    {
        //        throw p_objExp;
        //    }
        //    catch (Exception p_objExp)
        //    {
        //        throw new RMAppException(Globalization.GetString("OrgHierarchy.GetOrgPreferenceXml.GeneralError"),
        //            p_objExp);
        //    }
        //    finally
        //    {
        //        if (objReader != null)
        //        {
        //            if (!objReader.IsClosed)
        //            {
        //                objReader.Close();
        //            }
        //            objReader.Dispose();
        //        }

        //        if (objConn != null)
        //        {
        //            if (objConn.State == System.Data.ConnectionState.Open)
        //            {
        //                objConn.Close();
        //            }
        //            objConn.Dispose();
        //        }
        //    }
        //}

        //Private Function IFormOperation_Validate() =  Boolean
		//Dim b =  Boolean
		//Dim boolCancel =  Boolean
		//Dim lErrNum =  Long
		//
		//On Error GoTo hError
		//
		//Unload frmValidateErrors
		//
		//' Update data object
		//RaiseEvent UpdateObject
		//
		//' = sume validation will go through
		//IFormOperation_Validate = True
		//
		//If m_DataChanged Then
		//' Check required fields
		//
		//If TypeOf m_Form Is frmAdminTracking Then
		//IFormOperation_Validate = IFormOperation_Validate And ValidateAdminTracking()
		//Else
		//IFormOperation_Validate = IFormOperation_Validate And UnivValidateForm(m_Form)
		//End If
		//
		//' Do any custom form validation stuff
		//RaiseEvent OnValidate(boolCancel)
		//
		//IFormOperation_Validate = IFormOperation_Validate And (Not boolCancel)
		//
		//' Do Supplemental fields required field validation
		//If m_SupportSupplementals Then
		//IFormOperation_Validate = IFormOperation_Validate And ValidateSupplementals()
		//End If
		//
		//' Run any validation contained in object
		//Dim pIPer =  IPersistence
		//Dim lstItem =  ListItem
		//Set pIPer = m_objData
		//
		//If Not pIPer Is Nothing Then
		//On Error Resume Next
		//b = pIPer.Validate()
		//lErrNum = Err.Number
		//On Error GoTo hError
		//If lErrNum > 0 Or Not b Then
		//If lErrNum = ErrValidation Or lErrNum = 0 And Not b Then
		//Dim objErr =  CValidationError
		//For Each objErr In g_objRMApp.ValidationErrors
		//'GRD 4/15/2002  Internationalization
		//Set lstItem = frmValidateErrors.lstList.ListItems.Add(, , g_Localize.getMessage("CM_CFormOperation", "IFormOperation_Validate.ScriptValidation"))
		//lstItem.SubItems(1) = objErr.Description
		//Next
		//Else
		//Err.Raise lErrNum
		//End If
		//End If
		//
		//IFormOperation_Validate = IFormOperation_Validate And b
		//End If
		//End If
		//
		//' If validation didn't went through plug in form so Error report screen can link properly
		//If Not IFormOperation_Validate Then
		//If frmValidateErrors.lstList.ListItems.Count > 0 Then
		//frmValidateErrors.lblParentWindow = m_Form.hwnd
		//Beep
		//Else
		//Unload frmValidateErrors
		//End If
		//Else
		//' Validation OK for now validate children if any
		//' Validate all children first to make sure validation is complete
		//Dim pIFormOp =  IFormOperation
		//Dim frmChild =  Form
		//For Each frmChild In colChildren
		//Set pIFormOp = frmChild.objFormOperation
		//If pIFormOp.DataChanged Then
		//' If Child Save fails cancel loop
		//If Not pIFormOp.Validate() Then
		//IFormOperation_Validate = False ' Inidicate fail saving for whole group
		//Exit For
		//End If
		//End If
		//Next
		//End If
		//
		//hExit:
		//Exit Function
		//
		//hError:
		//IFormOperation_Validate = False
		//If iGeneralErrorExt(Err.Number, Err.Source, Err.Description, "CFormOperation.Validate") = vbRetry Then
		//Resume
		//End If
		//Resume hExit
	}
}
