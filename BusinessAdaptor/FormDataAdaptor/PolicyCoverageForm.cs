﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 05/22/2014 |  33573  | dagrawal   | SRe Gap34B - Claims Made Handling
 * 06/02/2014 |  34278   | abhadouria  | SRe Gap 20 - Add  Reinsurance indicator
 **********************************************************************************************/
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Text;
using System.Collections.Generic;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PolicyCoverage Screen.
	/// </summary>
	public class PolicyCoverageForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PolicyXCvgType";
		private PolicyXCvgType PolicyXCvgType{get{return objData as PolicyXCvgType;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "PolicyId";

		/// <summary>
		/// Class constructor to initialize Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public PolicyCoverageForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();

			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
            //XmlNode objPolicyId=null;
            
            //try
            //{
            //    objPolicyId = base.SysEx.GetElementsByTagName(FILTER_KEY_NAME)[0];
            //}//end try
            //catch
            //{
            //}//end catch
            //if(objPolicyId !=null)
            //    this.m_ParentId = Conversion.ConvertObjToInt(objPolicyId.InnerText, base.ClientId);

            //PolicyXCvgType.PolicyId = this.m_ParentId;

            ////Filter by this PhysEid if Present.
            //if(this.m_ParentId != 0)
            //{
            //    (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId;
            //}
            XmlNode objId = null;
            if (objData.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
            {
                objId = base.SysEx.GetElementsByTagName("PolicyUnitRowId")[0];
                if (objId != null)
                {
                    try
                    {
                        this.m_ParentId = Conversion.ConvertObjToInt(objId.InnerText.Split("_".ToCharArray()[0])[2], base.ClientId);
                    }
                    catch (Exception e)
                    {
                        this.m_ParentId = Conversion.ConvertObjToInt(objId.InnerText, base.ClientId);
                    }
                }

            }
            else
            {
                objId = base.SysEx.GetElementsByTagName(FILTER_KEY_NAME)[0];
                if (objId != null)
                    this.m_ParentId = Conversion.ConvertObjToInt(objId.InnerText, base.ClientId);
            }
            if (objData.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
            {
                PolicyXCvgType.PolicyUnitRowId = this.m_ParentId;
            }
            else
            {
                PolicyXCvgType.PolicyId = this.m_ParentId;
            }
            if (this.m_ParentId != 0)
            {
                if (objData.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
                {
                    (objData as INavigation).Filter = objData.PropertyNameToField("PolicyUnitRowId") + "=" + this.m_ParentId;
                }
                else
                {
                    (objData as INavigation).Filter = objData.PropertyNameToField("PolicyId") + "=" + this.m_ParentId;
                }
                
            }
			
		}//end method InitNew()

	
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel pro  perties.
		public override void OnUpdateForm()
        {
            //ArrayList singleRow = null;
            //base.OnUpdateForm ();
            //string sPolicyLOB = base.SysEx.SelectSingleNode("/SysExData/hdnPolicyLOB").InnerText.Trim();
            ArrayList singleRow = null;
            base.OnUpdateForm();
            DbReader objReader = null;
            int iPolicyLOB = 0;
            string sSQL = string.Empty;
            string sBasePolicyLOB = string.Empty;
            int iPolicySystemId = 0;
            
            //PJS MITS # 16563 - Adding Lookupdata attributes for policynumberlookup
            if (PolicyXCvgType.Context.InternalSettings.SysSettings.UseEnhPolFlag != -1)
            {
                singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.PolicyNumberLookup.ToString());
                base.AddElementToList(ref singleRow, "id", "nextpolicyid");
                base.AddElementToList(ref singleRow, "idref", "/Instance/PolicyXCvgType/NextPolicyId");
                base.AddElementToList(ref singleRow, "ref", "/Instance/PolicyXCvgType/NextPolicyId/@defaultdetail");
                base.AddElementToList(ref singleRow, "type", "policynumberlookup");
                string sParameters = "return lookupData('NextPolicyId','policy',-1,'NextPolicyId',9)";
                base.AddElementToList(ref singleRow, "onclientclickparameters", sParameters);
            }
            base.m_ModifiedControls.Add(singleRow);

            if (objData.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
            {
                XmlNode xnode = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysFormPIdName");
                if (xnode != null)
                {
                    xnode.InnerText = "PolicyUnitRowId";
                }
                //need to remove this and currect the lookup functionality for coverage lookup
                //in case the units are being used in policies.//skhare7 changed for policy interface
               // base.AddKillNode("lookup");

                sSQL = "SELECT POLICY_LOB_CODE,POLICY.POLICY_SYSTEM_ID FROM POLICY, POLICY_X_UNIT WHERE POLICY_UNIT_ROW_ID=" + PolicyXCvgType.PolicyUnitRowId + " AND POLICY.POLICY_ID = POLICY_X_UNIT.POLICY_ID";
                using (objReader = PolicyXCvgType.Context.DbConnLookup.ExecuteReader(sSQL))
                {

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            iPolicyLOB = Conversion.ConvertObjToInt(objReader.GetValue("POLICY_LOB_CODE").ToString(), base.ClientId);
                            iPolicySystemId = Conversion.ConvertObjToInt(objReader.GetValue("POLICY_SYSTEM_ID").ToString(), base.ClientId);
                        }
                    }
                }
                //string[] sCollection = sPolicyLOB.Split(' ');
                sBasePolicyLOB = objCache.GetRelatedShortCode(iPolicyLOB);
                if (sBasePolicyLOB != "PL")
                {
                    base.AddKillNode("limitcovA");
                    base.AddKillNode("limitcovB");
                    base.AddKillNode("limitcovC");
                    base.AddKillNode("limitcovD");
                    base.AddKillNode("limitcovE");
                    base.AddKillNode("limitcovF");
                }
                if (sBasePolicyLOB != "WL")
                {
                    base.AddKillNode("WcDedAmt");
                    base.AddKillNode("WcDedAggr");
                }
                //MITS 34278 START
                base.AddDisplayNode("Reinsurance");
                //MITS 34278 END
                //Ashish Ahuja: Claims Made Jira 1342
                base.AddDisplayNode("ClaimsMadeIndicator");

                if (iPolicySystemId > 0)
                {
                    base.AddKillNode("policylimit");
                    base.AddKillNode("occurrencelimit");
                    base.AddKillNode("claimlimit");
                    base.AddKillNode("totalpayments");
                    base.AddKillNode("perpersonlimit");
                    base.AddKillNode("limit");

                    RenderLimitInfoGrid(base.SysEx);
                }
                else
                {
                    base.AddKillNode("limitinfo");   
                }
            }
            else
            {
                XmlNode xnode = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysFormPIdName");
                if (xnode != null)
                {
                    xnode.InnerText = "policyid";
                }
                //MITS 34278 START
                base.AddKillNode("Reinsurance");
                base.AddKillNode("limitinfo");   
                //MITS 34278 END 
                //Ashish Ahuja: Claims Made Jira 1342
                base.AddDisplayNode("ClaimsMadeIndicator");
            }
           // base.ResetSysExData("PolicyUnitRowId", this.m_ParentId.ToString());//MGaba2:MITS 15642

            //akaur9 MITS 25163- Policy Interface Implementation --start
           
            int iSystemPolicyId = base.GetSysExDataNodeInt("//hdnPolicySystemId");
            if (iSystemPolicyId > 0)
            {
                base.AddReadOnlyNode("readOnlyPage");
                base.AddKillNode("new");
                base.AddKillNode("save");
                base.AddKillNode("delete");
                //MITS:33573 starts
                base.AddDisplayNode("RetroDate");
                base.AddDisplayNode("TailDate");
                //MITS:33573 ends
            
                //Start:commented by Nitin goel, MITS 37653,06/14/2014, Class code is needed for GC policies also.
                //// add coverage class code field only for external WL policy                
                //if (iPolicyLOB > 0 && sBasePolicyLOB.Trim() != "WL")
                //{
                //    base.AddKillNode("CvgClassCode");
                //}
                //end: added by Nitin goel
            }
            else
            {
                base.AddKillNode("btnAdditionalDetailsCoverage");   //Policy Inetrface Implementation aaggarwal29: Additional detail btn should be visible for external policies only 
                base.AddKillNode("perpersonlimit");
                base.AddKillNode("policysystemdata");
                //MITS:33573 starts
                base.AddKillNode("RetroDate");
                base.AddKillNode("TailDate");
                //MITS:33573 ends
				
                //tanwar2 - mits 30910 - start
                base.AddReadOnlyNode("deductibletype");
                //tanwar2 - mits 30910 - end
            }
            //akaur9 MITS 25163- Policy Interface Implementation --End


		}
        internal void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText)
        {
            try
            {
                XmlElement objChildNode = null;
                this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", base.ClientId), p_objEx);
            }
        }

        internal void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode)
        {
            try
            {
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
                p_objChildNode.InnerText = p_sText;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", base.ClientId), p_objEx);
            }
        }

        internal void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", base.ClientId), p_objEx);
            }

        }
        private string GetCoverageIds(bool bIsAggregate)
        {
            string sCovIds = string.Empty;
            string sSQL = sSQL = "SELECT POLICY_X_CVG_TYPE.POLCVG_ROW_ID FROM POLICY,POLICY_X_UNIT, POLICY_X_CVG_TYPE WHERE POLICY.EXTERNAL_POLICY_KEY='" + PolicyXCvgType.GetPolicyKey() + "' AND  POLICY.POLICY_ID=POLICY_X_UNIT.POLICY_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID= POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID AND POLICY_X_CVG_TYPE.COVERAGE_KEY='" + PolicyXCvgType.CoverageKey + "'";
            if (!bIsAggregate)
            {
                sSQL = sSQL + " AND POLICY.POLICY_ID=" + GetPolicyID();
            }

            using (DbReader objRdr = DbFactory.GetDbReader(PolicyXCvgType.Context.DbConn.ConnectionString, sSQL))
            {
                while (objRdr.Read())
                {
                    if (string.IsNullOrEmpty(sCovIds))
                    {
                        sCovIds = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                    }
                    else
                    {
                        sCovIds = sCovIds + "," + Conversion.ConvertObjToStr(objRdr.GetValue(0));
                    }
                }
            }

            return sCovIds;

        }
        private double GetTolalReserve(string sCovIds, string sRsvTypeCode)
        {
            string sSQL = string.Empty;
            double dblAmount = 0;

            if (!string.IsNullOrEmpty(sRsvTypeCode))
            {
                sSQL = "SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT,COVERAGE_X_LOSS  WHERE RESERVE_TYPE_CODE IN (" + sRsvTypeCode + ") AND COVERAGE_X_LOSS.POLCVG_ROW_ID IN (" + sCovIds + " ) AND COVERAGE_X_LOSS.CVG_LOSS_ROW_ID= RESERVE_CURRENT.POLCVG_LOSS_ROW_ID AND RESERVE_TYPE_CODE NOT IN (" + PolicyXCvgType.Context.LocalCache.GetChildCodeIds(PolicyXCvgType.Context.LocalCache.GetTableId("RESERVE_TYPE"), PolicyXCvgType.Context.LocalCache.GetCodeId("R", "MASTER_RESERVE")) + ")";



                return Conversion.ConvertObjToDouble(DbFactory.ExecuteScalar(PolicyXCvgType.Context.DbConn.ConnectionString, sSQL), base.ClientId);
            }
            return 0;
        }

        private double GetPaidAmountonCoverage(string sCovIds, string sRsvTypeCode)
        {
            string sSQL = string.Empty;
            double dblFundsAmount = 0;

            if (!string.IsNullOrEmpty(sRsvTypeCode))
            {
                sSQL = "SELECT SUM(PAID_TOTAL) FROM RESERVE_CURRENT,COVERAGE_X_LOSS  WHERE RESERVE_TYPE_CODE IN (" + sRsvTypeCode + ") AND COVERAGE_X_LOSS.POLCVG_ROW_ID IN (" + sCovIds + " ) AND COVERAGE_X_LOSS.CVG_LOSS_ROW_ID= RESERVE_CURRENT.POLCVG_LOSS_ROW_ID";



                return Conversion.ConvertObjToDouble(DbFactory.ExecuteScalar(PolicyXCvgType.Context.DbConn.ConnectionString, sSQL), base.ClientId);
            }
            return 0;

        }
        private double GetCollectionAmountonCoverage(string sCovIds, string sRsvTypeCode)
        {
            string sSQL = string.Empty;
            double dblFundsAmount = 0;

            if (!string.IsNullOrEmpty(sRsvTypeCode))
            {
                sSQL = "SELECT SUM(COLLECTION_TOTAL) FROM RESERVE_CURRENT,COVERAGE_X_LOSS  WHERE RESERVE_TYPE_CODE IN (" + sRsvTypeCode + ") AND COVERAGE_X_LOSS.POLCVG_ROW_ID IN (" + sCovIds + " ) AND COVERAGE_X_LOSS.CVG_LOSS_ROW_ID= RESERVE_CURRENT.POLCVG_LOSS_ROW_ID";



                return Conversion.ConvertObjToDouble(DbFactory.ExecuteScalar(PolicyXCvgType.Context.DbConn.ConnectionString, sSQL), base.ClientId);
            }
            return 0;

        }
        private double GetCollectiononDeductibleReserve(string sCovIds)
        {
            string sSQL = string.Empty;
            string sDedResvTypeCode = string.Empty;
            int iPolicyLOB = 0;

            sSQL = "SELECT POLICY_LOB_CODE FROM POLICY, POLICY_X_UNIT WHERE POLICY_UNIT_ROW_ID=" + PolicyXCvgType.PolicyUnitRowId + " AND POLICY.POLICY_ID = POLICY_X_UNIT.POLICY_ID";
            using ( DbReader  objReader = PolicyXCvgType.Context.DbConnLookup.ExecuteReader(sSQL))
            {

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iPolicyLOB = Conversion.ConvertObjToInt(objReader.GetValue("POLICY_LOB_CODE").ToString(), base.ClientId);
                    }
                }
            }
            sSQL = "SELECT LINE_OF_BUS_CODE FROM CODES WHERE CODE_ID=" + iPolicyLOB;

            int iLOB = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(PolicyXCvgType.Context.DbConn.ConnectionString, sSQL), base.ClientId);
            if (PolicyXCvgType.Context.InternalSettings.ColLobSettings[iLOB].ApplyDedToPaymentsFlag==-1)
            {

               

                sSQL = "SELECT DISTINCT DED_REC_RESERVE_TYPE FROM SYS_PARMS_LOB  WHERE LINE_OF_BUS_CODE=" + iLOB;
                using (DbReader objRdr = DbFactory.GetDbReader(PolicyXCvgType.Context.DbConn.ConnectionString, sSQL))
                {
                    while (objRdr.Read())
                    {
                        if (string.IsNullOrEmpty(sDedResvTypeCode))
                        {
                            sDedResvTypeCode = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                        }
                        else
                        {
                            sDedResvTypeCode = sDedResvTypeCode + "," + Conversion.ConvertObjToStr(objRdr.GetValue(0));
                        }

                    }
                }

                sSQL = "SELECT SUM(COLLECTION_TOTAL) FROM RESERVE_CURRENT,COVERAGE_X_LOSS  WHERE COVERAGE_X_LOSS.POLCVG_ROW_ID IN (" + sCovIds + " ) AND COVERAGE_X_LOSS.CVG_LOSS_ROW_ID= RESERVE_CURRENT.POLCVG_LOSS_ROW_ID AND RESERVE_TYPE_CODE IN (" + sDedResvTypeCode + ")";



                return Conversion.ConvertObjToDouble(DbFactory.ExecuteScalar(PolicyXCvgType.Context.DbConn.ConnectionString, sSQL), base.ClientId);
            }
            else
            {
                return 0;
            }

        }
        private int GetPolicyID()
        {
           string sSQL= "SELECT POLICY_X_UNIT.POLICY_ID FROM POLICY_X_UNIT WHERE POLICY_UNIT_ROW_ID=" + PolicyXCvgType.PolicyUnitRowId;
           return Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(PolicyXCvgType.Context.DbConn.ConnectionString, sSQL), base.ClientId);

        }
        public void RenderLimitInfoGrid(XmlDocument objSysEx)
        {
            XmlElement objLimitInfoElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlElement objOptionXmlElement = null;
            SortedList CovLimit = null;
            string sCovIds = string.Empty;
            try
            {
                objLimitInfoElement = (XmlElement)objSysEx.SelectSingleNode("/SysExData/LimitInfoList");
                if (objLimitInfoElement != null)
                    objLimitInfoElement.ParentNode.RemoveChild(objLimitInfoElement);

                CreateElement(objSysEx.DocumentElement, "LimitInfoList", ref objLimitInfoElement);


                CreateElement(objLimitInfoElement, "listhead", ref objListHeadXmlElement);

                CreateAndSetElement(objListHeadXmlElement, "LimitType", "Limit Type");
                CreateAndSetElement(objListHeadXmlElement, "LimitAmount", "Limit Amount");

                CreateAndSetElement(objListHeadXmlElement, "Reserving", "Total Reserve");
                CreateAndSetElement(objListHeadXmlElement, "PaidAmount", "Paid Amount");
                CreateAndSetElement(objListHeadXmlElement, "Collection", "Collection/Recovery");
                CreateAndSetElement(objListHeadXmlElement, "LimitRemaining", "Policy Limit Remaining");
                CreateAndSetElement(objListHeadXmlElement, "LimitRemainingWithColl", "Limit Remaining (Inc. Recovery/collection)");
              
                CovLimit = CoverageLimit();
                if (CovLimit.Count > 0)
                {
                   int iPolicyId = GetPolicyID();
                   int iPolicyLob = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(PolicyXCvgType.Context.DbConn.ConnectionString, "SELECT POLICY_LOB_CODE FROM POLICY WHERE POLICY_ID=" + iPolicyId), base.ClientId);
                   int iLOB = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(PolicyXCvgType.Context.DbConn.ConnectionString, "SELECT LINE_OF_BUS_CODE FROM CODES WHERE CODE_ID=" + iPolicyLob), base.ClientId);


                    for (int iIndex = 0; iIndex < CovLimit.Count; iIndex++)
                    {

                        PolCovLimit objPolCovLimit = (PolCovLimit)CovLimit.GetByIndex(iIndex);

                        string sRsvTypeCode = string.Empty;
                        using (DbReader objrdr1 = DbFactory.GetDbReader(PolicyXCvgType.Context.DbConn.ConnectionString, "SELECT CODE1 FROM CODE_X_CODE WHERE DELETED_FLAG=0 AND CODE2=" + objPolCovLimit.LimitTypeCode + " AND REL_TYPE_CODE=" + PolicyXCvgType.Context.LocalCache.GetCodeId("RTTOLT", "CODE_REL_TYPE")))
                        {
                            while (objrdr1.Read())
                            {
                                if (string.IsNullOrEmpty(sRsvTypeCode))
                                {
                                    sRsvTypeCode = Conversion.ConvertObjToStr(objrdr1.GetValue(0));
                                }
                                else
                                {
                                    sRsvTypeCode = sRsvTypeCode + "," + Conversion.ConvertObjToStr(objrdr1.GetValue(0));
                                }
                            }
                        }
                        double dblPaidTotal = 0;
                        double dblDedCollection = 0;
                        double dblCollectionTotal = 0;
                        double dblTotalReserve = 0;
                        if (PolicyXCvgType.Context.LocalCache.GetRelatedShortCode(objPolCovLimit.LimitTypeCode) == "AGG")
                        {
                            sCovIds = GetCoverageIds(true);
                            if (PolicyXCvgType.Context.InternalSettings.SysSettings.PolicyLimitExceeded && PolicyXCvgType.Context.InternalSettings.ColLobSettings[iLOB].UseLimitTracking)
                            {
                                dblPaidTotal = GetPaidAmountonCoverage(sCovIds, sRsvTypeCode);

                                if (!string.IsNullOrEmpty(sRsvTypeCode))
                                {
                                    dblDedCollection = GetCollectiononDeductibleReserve(sCovIds);
                                }

                                dblCollectionTotal = GetCollectionAmountonCoverage(sCovIds, sRsvTypeCode);
                                dblTotalReserve = GetTolalReserve(sCovIds, sRsvTypeCode);
                            }
                            CreateElement(objLimitInfoElement, "option", ref objOptionXmlElement);
                            objOptionXmlElement.SetAttribute("ref", "/Instance/UI/FormVariables/SysExData/" + objOptionXmlElement.ParentNode.LocalName + "/option[" + iIndex.ToString() + "]");


                            CreateAndSetElement(objOptionXmlElement, "LimitType", PolicyXCvgType.Context.LocalCache.GetCodeDesc(objPolCovLimit.LimitTypeCode) + " (" + PolicyXCvgType.Context.LocalCache.GetCodeDesc(PolicyXCvgType.Context.LocalCache.GetRelatedCodeId(objPolCovLimit.LimitTypeCode)) + ")");
                            CreateAndSetElement(objOptionXmlElement, "LimitAmount", CommonFunctions.ConvertByCurrencyCode(PolicyXCvgType.Context.InternalSettings.SysSettings.BaseCurrencyType, objPolCovLimit.LimitAmount, PolicyXCvgType.Context.DbConn.ConnectionString, base.ClientId));
                            CreateAndSetElement(objOptionXmlElement, "Reserving", CommonFunctions.ConvertByCurrencyCode(PolicyXCvgType.Context.InternalSettings.SysSettings.BaseCurrencyType, dblTotalReserve, PolicyXCvgType.Context.DbConn.ConnectionString, base.ClientId));

                            CreateAndSetElement(objOptionXmlElement, "PaidAmount", CommonFunctions.ConvertByCurrencyCode(PolicyXCvgType.Context.InternalSettings.SysSettings.BaseCurrencyType, dblPaidTotal - dblDedCollection, PolicyXCvgType.Context.DbConn.ConnectionString, base.ClientId));
                            CreateAndSetElement(objOptionXmlElement, "Collection", CommonFunctions.ConvertByCurrencyCode(PolicyXCvgType.Context.InternalSettings.SysSettings.BaseCurrencyType, dblCollectionTotal, PolicyXCvgType.Context.DbConn.ConnectionString, base.ClientId));


                            CreateAndSetElement(objOptionXmlElement, "LimitRemaining", CommonFunctions.ConvertByCurrencyCode(PolicyXCvgType.Context.InternalSettings.SysSettings.BaseCurrencyType, objPolCovLimit.LimitAmount - (dblPaidTotal - dblDedCollection), PolicyXCvgType.Context.DbConn.ConnectionString, base.ClientId));

                            CreateAndSetElement(objOptionXmlElement, "LimitRemainingWithColl", CommonFunctions.ConvertByCurrencyCode(PolicyXCvgType.Context.InternalSettings.SysSettings.BaseCurrencyType, objPolCovLimit.LimitAmount - (dblPaidTotal - dblCollectionTotal - dblDedCollection), PolicyXCvgType.Context.DbConn.ConnectionString, base.ClientId));



                            CreateAndSetElement(objOptionXmlElement, "LimitRowId", objPolCovLimit.LimitRowId.ToString());

                           // RenderErosionInfoGrid(objSysEx, base.GetSysExDataNodeInt("LimitSelectedId"));

                        }

                        else
                        {
                            sCovIds = GetCoverageIds(false);
                            if (PolicyXCvgType.Context.InternalSettings.SysSettings.PolicyLimitExceeded && PolicyXCvgType.Context.InternalSettings.ColLobSettings[iLOB].UseLimitTracking)
                            {
                                 dblPaidTotal = GetPaidAmountonCoverage(sCovIds, sRsvTypeCode);
                               
                                if (!string.IsNullOrEmpty(sRsvTypeCode))
                                {
                                    dblDedCollection = GetCollectiononDeductibleReserve(sCovIds);
                                }
                                 dblCollectionTotal = GetCollectionAmountonCoverage(sCovIds, sRsvTypeCode);
                                 dblTotalReserve = GetTolalReserve(sCovIds, sRsvTypeCode);
                            }
                            CreateElement(objLimitInfoElement, "option", ref objOptionXmlElement);
                            objOptionXmlElement.SetAttribute("ref", "/Instance/UI/FormVariables/SysExData/" + objOptionXmlElement.ParentNode.LocalName + "/option[" + iIndex.ToString() + "]");


                            CreateAndSetElement(objOptionXmlElement, "LimitType", PolicyXCvgType.Context.LocalCache.GetCodeDesc(objPolCovLimit.LimitTypeCode) + " (" + PolicyXCvgType.Context.LocalCache.GetCodeDesc(PolicyXCvgType.Context.LocalCache.GetRelatedCodeId(objPolCovLimit.LimitTypeCode)) + ")");
                            CreateAndSetElement(objOptionXmlElement, "LimitAmount", CommonFunctions.ConvertByCurrencyCode(PolicyXCvgType.Context.InternalSettings.SysSettings.BaseCurrencyType, objPolCovLimit.LimitAmount, PolicyXCvgType.Context.DbConn.ConnectionString, base.ClientId));
                            CreateAndSetElement(objOptionXmlElement, "Reserving", CommonFunctions.ConvertByCurrencyCode(PolicyXCvgType.Context.InternalSettings.SysSettings.BaseCurrencyType, dblTotalReserve, PolicyXCvgType.Context.DbConn.ConnectionString, base.ClientId));

                            CreateAndSetElement(objOptionXmlElement, "PaidAmount", CommonFunctions.ConvertByCurrencyCode(PolicyXCvgType.Context.InternalSettings.SysSettings.BaseCurrencyType, dblPaidTotal - dblDedCollection, PolicyXCvgType.Context.DbConn.ConnectionString, base.ClientId));
                            CreateAndSetElement(objOptionXmlElement, "Collection", CommonFunctions.ConvertByCurrencyCode(PolicyXCvgType.Context.InternalSettings.SysSettings.BaseCurrencyType, dblCollectionTotal, PolicyXCvgType.Context.DbConn.ConnectionString, base.ClientId));


                            CreateAndSetElement(objOptionXmlElement, "LimitRemaining", CommonFunctions.ConvertByCurrencyCode(PolicyXCvgType.Context.InternalSettings.SysSettings.BaseCurrencyType, objPolCovLimit.LimitAmount - (dblPaidTotal - dblDedCollection), PolicyXCvgType.Context.DbConn.ConnectionString, base.ClientId));

                            CreateAndSetElement(objOptionXmlElement, "LimitRemainingWithColl", CommonFunctions.ConvertByCurrencyCode(PolicyXCvgType.Context.InternalSettings.SysSettings.BaseCurrencyType, objPolCovLimit.LimitAmount - (dblPaidTotal - dblCollectionTotal - dblDedCollection), PolicyXCvgType.Context.DbConn.ConnectionString, base.ClientId));



                            CreateAndSetElement(objOptionXmlElement, "LimitRowId", objPolCovLimit.LimitRowId.ToString());

                           
                        }
                    }
                    RenderErosionInfoGrid(objSysEx, base.GetSysExDataNodeInt("LimitSelectedId"), PolicyXCvgType.Context.InternalSettings.SysSettings.PolicyLimitExceeded && PolicyXCvgType.Context.InternalSettings.ColLobSettings[iLOB].UseLimitTracking);
                }
                    else
                {
             
                    XmlElement objTempElement = null;
                    int iIndex = 0;

                    CreateElement(objLimitInfoElement, "option", ref objOptionXmlElement);
                    objOptionXmlElement.SetAttribute("ref", "/Instance/UI/FormVariables/SysExData/" + objOptionXmlElement.ParentNode.LocalName + "/option[" + iIndex.ToString() + "]");


                    CreateAndSetElement(objOptionXmlElement, "LimitType", "");
                    CreateAndSetElement(objOptionXmlElement, "LimitAmount", "");
                    CreateAndSetElement(objOptionXmlElement, "Reserving", "");

                    CreateAndSetElement(objOptionXmlElement, "PaidAmount", "");
                    CreateAndSetElement(objOptionXmlElement, "Collection", "");


                    CreateAndSetElement(objOptionXmlElement, "LimitRemaining","");

                    CreateAndSetElement(objOptionXmlElement, "LimitRemainingWithColl", "");



                    CreateAndSetElement(objOptionXmlElement, "LimitRowId", "0");
                    RenderErosionInfoGrid(objSysEx,0,false);
                }
                
            }
            finally
            {
                objLimitInfoElement = null;
                objListHeadXmlElement = null;
                objOptionXmlElement = null;
            }
        }

        
        private string GetClaimAdjuster(int iClaimId)
        {
            string sAdjuster = string.Empty;


            using (DbReader objRdr = DbFactory.GetDbReader(PolicyXCvgType.Context.DbConn.ConnectionString, "SELECT LAST_NAME,FIRST_NAME,MIDDLE_NAME,CURRENT_ADJ_FLAG,ADJ_ROW_ID FROM ENTITY,CLAIM_ADJUSTER WHERE CLAIM_ID=" + iClaimId + " AND ADJUSTER_EID= ENTITY_ID ORDER BY ADJ_ROW_ID DESC"))
            {
                while (objRdr.Read())
                {


                    if (Conversion.ConvertObjToBool(objRdr.GetValue("CURRENT_ADJ_FLAG"), base.ClientId))
                    {
                        sAdjuster = Conversion.ConvertObjToStr(objRdr.GetValue("FIRST_NAME")) + " " + Conversion.ConvertObjToStr(objRdr.GetValue("MIDDLE_NAME")) + " " + Conversion.ConvertObjToStr(objRdr.GetValue("LAST_NAME"));
                        break;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sAdjuster))
                        {
                            sAdjuster = Conversion.ConvertObjToStr(objRdr.GetValue("FIRST_NAME")) + " " + Conversion.ConvertObjToStr(objRdr.GetValue("MIDDLE_NAME")) + " " + Conversion.ConvertObjToStr(objRdr.GetValue("LAST_NAME"));
                        }
                    }

                }
            }
            return sAdjuster;
        }
        private string GetAdjusterName(int iAdjEid, int iClaimId)
        {
            string sAdjuster = string.Empty;

            if (iAdjEid > 0)
            {
                using (DbReader objRdr = DbFactory.GetDbReader(PolicyXCvgType.Context.DbConn.ConnectionString, "SELECT LAST_NAME,FIRST_NAME,MIDDLE_NAME FROM ENTITY WHERE  ENTITY_ID=" + iAdjEid))
                {
                    if (objRdr.Read())
                    {
                        sAdjuster = Conversion.ConvertObjToStr(objRdr.GetValue("FIRST_NAME")) + " " + Conversion.ConvertObjToStr(objRdr.GetValue("MIDDLE_NAME")) + " " + Conversion.ConvertObjToStr(objRdr.GetValue("LAST_NAME"));
                    }
                }
            }
            if (string.IsNullOrEmpty(sAdjuster))
            {
                sAdjuster = GetClaimAdjuster(iClaimId);
            }

            return sAdjuster;
        }
        private bool CheckPerRsvSettings(int p_iPerRsvFlag, int p_iReserveTypeCode, int p_iLob)
        {
            List<string> lstResTypeCode = null;
            string sSQL = string.Empty;
            //string sConnectionString = m_sConnectionString;
            try
            {
                lstResTypeCode = new List<string>();
                sSQL = string.Format("SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL WHERE LINE_OF_BUS_CODE = {0}" +
                                   "AND COLL_IN_RSV_BAL = {1}", p_iLob, p_iPerRsvFlag);
                using (DbReader objReader = DbFactory.GetDbReader(PolicyXCvgType.Context.DbConn.ConnectionString, sSQL))
                {
                    while (objReader.Read())
                    {
                        lstResTypeCode.Add(Convert.ToString(objReader.GetValue("RES_TYPE_CODE")));
                    }
                }

                if (!lstResTypeCode.Contains(Convert.ToString(p_iReserveTypeCode)))
                {
                    return false;
                }
            }
            catch (Exception exe)
            {
                throw exe;
            }
            finally
            {
                lstResTypeCode = null;
            }

            return true;
        }
        private double CalculateIncurred(double p_dBalance, double p_dPaid, double p_dCollect,
           bool p_bCollInRsvBal, bool p_bCollInIncurredBal, int p_iReserveTypeCode, bool p_bCollInPerRsvBal, bool p_bCollInPerIncurredBal, int p_iLOB)
        {
            double dTmp2 = 0;
            double dTmp = 0;

            bool bIsRecoveryReserve = false;
            LocalCache objCache = null;
            objCache = new LocalCache(PolicyXCvgType.Context.DbConn.ConnectionString, base.ClientId);
            if (p_iReserveTypeCode != 0 && (objCache.GetRelatedShortCode(p_iReserveTypeCode) == "R"))
            {
                bIsRecoveryReserve = true;
            }
            objCache.Dispose();
            if (bIsRecoveryReserve)
            {
                if (p_dBalance < 0)
                {
                    dTmp = p_dCollect;  //MITS 35837
                }
                else
                {
                    dTmp = p_dBalance + p_dCollect;  //MITS 35837
                }
            }
            else
            {

                if (p_bCollInPerRsvBal)
                    p_bCollInPerRsvBal = this.CheckPerRsvSettings(-1, p_iReserveTypeCode, p_iLOB);
                if (p_bCollInPerIncurredBal)
                    p_bCollInPerIncurredBal = this.CheckPerRsvSettings(0, p_iReserveTypeCode, p_iLOB);
                if (p_bCollInRsvBal || p_bCollInPerRsvBal)
                {
                    dTmp2 = p_dPaid - p_dCollect;

                    if (p_dBalance < 0)
                    {
                        dTmp = dTmp2;
                    }
                    else
                    {
                        dTmp = p_dBalance + dTmp2;
                    }
                }
                else
                {
                    if (p_dBalance < 0)
                    {
                        dTmp = p_dPaid;
                    }
                    else
                    {
                        dTmp = p_dBalance + p_dPaid;
                    }
                }
            }
            if (((p_bCollInIncurredBal) && (!bIsRecoveryReserve)) || p_bCollInPerIncurredBal)
            {
                dTmp = dTmp - p_dCollect;
            }

            return dTmp;
        }
        public void RenderErosionInfoGrid(XmlDocument objSysEx, int iLimitRowID,bool bApplyLimit)
        {
            StringBuilder sSQL = new StringBuilder();
            XmlElement objLimitInfoElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlElement objOptionXmlElement = null;
            XmlElement objTempElement = null;
            int iIndex = 0;
            bool bIsAggregateLimit = false;
            int iLimitTypeCode = 0;
            string sRsvTypeCode = string.Empty;
            double dRBalance = 0;
            double dRIncurred = 0;
            double dBalTotal = 0;
            double dPaidTotal = 0;
            double dCollTotal = 0;
            double dIncTotal = 0;
            bool bCollInRsvBal = false, bPerRsvColInRsvBal = false;
            bool bCollInIncurredBal = false, bPerRsvColInIncrBal = false;
            bool bResByClaimType = false;
            int iPolicyLob = 0;
            int iLOB = 0;
            StringBuilder sbSQL = null;
            int iPolicyId = 0;
            try
            {
                iPolicyId = GetPolicyID();
                iPolicyLob = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(PolicyXCvgType.Context.DbConn.ConnectionString, "SELECT POLICY_LOB_CODE FROM POLICY WHERE POLICY_ID=" + iPolicyId), base.ClientId);
                iLOB = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(PolicyXCvgType.Context.DbConn.ConnectionString, "SELECT LINE_OF_BUS_CODE FROM CODES WHERE CODE_ID=" + iPolicyLob), base.ClientId);

                    sbSQL = new StringBuilder();
                    sbSQL.Append("SELECT RESERVE_TRACKING, COLL_IN_RSV_BAL,PER_RSV_COLL_IN_RSV_BAL,PER_RSV_COLL_IN_INCR_BAL,BAL_TO_ZERO , NEG_BAL_TO_ZERO ,SET_TO_ZERO, COLL_IN_INCUR_BAL, RES_BY_CLM_TYPE "); //Parijat: MITS 7977
                    sbSQL.Append("FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + iLOB);

                    using (DbReader objReader = DbFactory.GetDbReader(PolicyXCvgType.Context.DbConn.ConnectionString, sbSQL.ToString()))
                    {
                        if (objReader != null)
                        {
                            if (objReader.Read())
                            {

                                bCollInRsvBal = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                                    objReader.GetValue("COLL_IN_RSV_BAL").ToString()), base.ClientId);
                                bCollInIncurredBal = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                                    objReader.GetValue("COLL_IN_INCUR_BAL").ToString()), base.ClientId);
                                bResByClaimType = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                                    objReader.GetValue("RES_BY_CLM_TYPE").ToString()), base.ClientId);
                                bPerRsvColInRsvBal = Convert.ToBoolean(objReader.GetInt16("PER_RSV_COLL_IN_RSV_BAL"));//asharma326 JIRA 870
                                bPerRsvColInIncrBal = Convert.ToBoolean(objReader.GetInt16("PER_RSV_COLL_IN_INCR_BAL"));//asharma326 JIRA 870

                            }
                            objReader.Close();
                        }
                    }


                    objLimitInfoElement = (XmlElement)objSysEx.SelectSingleNode("/SysExData/ErosionInfoList");
                    if (objLimitInfoElement != null)
                        objLimitInfoElement.ParentNode.RemoveChild(objLimitInfoElement);

                    CreateElement(objSysEx.DocumentElement, "ErosionInfoList", ref objLimitInfoElement);


                    CreateElement(objLimitInfoElement, "listhead", ref objListHeadXmlElement);

                    CreateAndSetElement(objListHeadXmlElement, "ClaimNumber", "Claim Number", ref objTempElement);
                    CreateAndSetElement(objListHeadXmlElement, "Adjuster", "Adjuster", ref objTempElement);

                    CreateAndSetElement(objListHeadXmlElement, "RsvBalance", "Reserve Balance", ref objTempElement);
                    CreateAndSetElement(objListHeadXmlElement, "PaidAmount", "Paid Amount", ref objTempElement);
                    CreateAndSetElement(objListHeadXmlElement, "Collection", "Collection/Recovery", ref objTempElement);
                CreateAndSetElement(objListHeadXmlElement, "Incurred", "Incurred Amount", ref objTempElement);
                if (bApplyLimit && (iLimitRowID > 0))
                {
                        iLimitTypeCode = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(PolicyXCvgType.Context.DbConn.ConnectionString, "SELECT LIMIT_TYPE_CODE FROM POL_COV_LIMIT WHERE LIMIT_ROW_ID=" + iLimitRowID), base.ClientId);
                        using (DbReader objrdr = DbFactory.GetDbReader(PolicyXCvgType.Context.DbConn.ConnectionString, "SELECT CODE1 FROM CODE_X_CODE WHERE DELETED_FLAG=0 AND CODE2=" + iLimitTypeCode + " AND REL_TYPE_CODE=" + PolicyXCvgType.Context.LocalCache.GetCodeId("RTTOLT", "CODE_REL_TYPE")))
                        {
                            while (objrdr.Read())
                            {
                                if (string.IsNullOrEmpty(sRsvTypeCode))
                                {
                                    sRsvTypeCode = Conversion.ConvertObjToStr(objrdr.GetValue(0));
                                }
                                else
                                {
                                    sRsvTypeCode = sRsvTypeCode + "," + Conversion.ConvertObjToStr(objrdr.GetValue(0));
                                }
                            }
                        }

                        if (PolicyXCvgType.Context.LocalCache.GetRelatedShortCode(iLimitTypeCode) == "AGG")
                        {
                            bIsAggregateLimit = true;
                        }
                        else
                            bIsAggregateLimit = false;


                        base.AddDisplayNode("ErosionGrid");

                        sSQL = sSQL.Append("SELECT CL.CLAIM_NUMBER, RC.ASSIGNADJ_EID,RC.BALANCE_AMOUNT,RC.PAID_TOTAL,RC.COLLECTION_TOTAL,RC.INCURRED_AMOUNT,RC.RC_ROW_ID,RC.CLAIM_ID,RC.RESERVE_TYPE_CODE ");
                        sSQL = sSQL.Append(" FROM  POLICY_X_CVG_TYPE PCT, COVERAGE_X_LOSS CXL, RESERVE_CURRENT RC,CLAIM CL");


                        sSQL = sSQL.Append(" WHERE ");
                        //sSQL = sSQL.Append(" POL.POLICY_ID= " + Policy.PolicyId);

                        //sSQL = sSQL.Append(" AND POL.POLICY_ID = POLU.POLICY_ID");

                        sSQL = sSQL.Append(" PCT.POLCVG_ROW_ID IN (" + GetCoverageIds(bIsAggregateLimit) + ")");
                        sSQL = sSQL.Append(" AND PCT.POLCVG_ROW_ID= CXL.POLCVG_ROW_ID AND CXL.CVG_LOSS_ROW_ID = RC.POLCVG_LOSS_ROW_ID ");
                        sSQL = sSQL.Append("   AND  RC.CLAIM_ID=CL.CLAIM_ID ");

                        if (!string.IsNullOrEmpty(sRsvTypeCode))
                        {
                            sSQL = sSQL.Append("   AND  RC.RESERVE_TYPE_CODE IN (" + sRsvTypeCode + ") ");




                            using (DbReader objRdr = DbFactory.GetDbReader(PolicyXCvgType.Context.DbConn.ConnectionString, sSQL.ToString()))
                            {
                                while (objRdr.Read())
                                {
                                    CreateElement(objLimitInfoElement, "option", ref objOptionXmlElement);
                                    objOptionXmlElement.SetAttribute("ref", "/Instance/UI/FormVariables/SysExData/" + objOptionXmlElement.ParentNode.LocalName + "/option[" + iIndex.ToString() + "]");


                                    CreateAndSetElement(objOptionXmlElement, "ClaimNumber", Conversion.ConvertObjToStr(objRdr.GetValue("CLAIM_NUMBER")), ref objTempElement);
                                    CreateAndSetElement(objOptionXmlElement, "Adjuster", GetAdjusterName(Conversion.ConvertObjToInt(objRdr.GetValue("ASSIGNADJ_EID"), base.ClientId), Conversion.ConvertObjToInt(objRdr.GetValue("CLAIM_ID"), base.ClientId)), ref objTempElement);
                                    CreateAndSetElement(objOptionXmlElement, "RsvBalance", Conversion.ConvertObjToStr(objRdr.GetValue("BALANCE_AMOUNT")), ref objTempElement);

                                    CreateAndSetElement(objOptionXmlElement, "PaidAmount", Conversion.ConvertObjToStr(objRdr.GetValue("PAID_TOTAL")), ref objTempElement);
                                    CreateAndSetElement(objOptionXmlElement, "Collection", Conversion.ConvertObjToStr(objRdr.GetValue("COLLECTION_TOTAL")), ref objTempElement);
                                    CreateAndSetElement(objOptionXmlElement, "Incurred", CalculateIncurred(Conversion.ConvertObjToDouble(objRdr.GetValue("BALANCE_AMOUNT"), base.ClientId), Conversion.ConvertObjToDouble(objRdr.GetValue("PAID_TOTAL"), base.ClientId), Conversion.ConvertObjToDouble(objRdr.GetValue("COLLECTION_TOTAL"), base.ClientId), bCollInRsvBal, bCollInIncurredBal, Conversion.ConvertObjToInt(objRdr.GetValue("RESERVE_TYPE_CODE"), base.ClientId), bPerRsvColInRsvBal, bPerRsvColInIncrBal, iLOB).ToString(), ref objTempElement);

                                    CreateAndSetElement(objOptionXmlElement, "ErosionRowId", Conversion.ConvertObjToStr(objRdr.GetValue("RC_ROW_ID")), ref objTempElement);
                                    iIndex++;
                                }
                            }
                        }
                  //  }
                }
                else
                {
                    base.AddKillNode("ErosionGrid");
                    CreateElement(objLimitInfoElement, "option", ref objOptionXmlElement);
                    objOptionXmlElement.SetAttribute("ref", "/Instance/UI/FormVariables/SysExData/" + objOptionXmlElement.ParentNode.LocalName + "/option[" + iIndex.ToString() + "]");


                    CreateAndSetElement(objOptionXmlElement, "ClaimNumber", "", ref objTempElement);
                    CreateAndSetElement(objOptionXmlElement, "Adjuster", "", ref objTempElement);
                    CreateAndSetElement(objOptionXmlElement, "RsvBalance", "", ref objTempElement);

                    CreateAndSetElement(objOptionXmlElement, "PaidAmount", "", ref objTempElement);
                    CreateAndSetElement(objOptionXmlElement, "Collection", "", ref objTempElement);
                    CreateAndSetElement(objOptionXmlElement, "Incurred", "", ref objTempElement);

                    CreateAndSetElement(objOptionXmlElement, "ErosionRowId", "0", ref objTempElement);
                }

            }
            finally
            {
                objLimitInfoElement = null;
                objListHeadXmlElement = null;
                objOptionXmlElement = null;
            }
        }

        public SortedList CoverageLimit( )
        {
            SortedList objCovLimitSorted = null;
           
             
                int iKey = 0;
                objCovLimitSorted = new SortedList();
                foreach (PolCovLimit objPolCovLimit in PolicyXCvgType.PolCovLimitList)
                {
                    if (objPolCovLimit.LimitRowId > 0)
                    {
                        iKey = objPolCovLimit.LimitRowId;
                        objCovLimitSorted.Add(iKey, objPolCovLimit);
                    }
                }
            
            return (objCovLimitSorted);
        }

	
	
	}
}
