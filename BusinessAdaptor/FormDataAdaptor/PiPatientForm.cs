﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Application.SupportScreens;
using System.Text;
//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PiPatient Screen.
	/// </summary>
	public class PiPatientForm : DataEntryFormBase
	{
        private string m_sConnectionString = "";	//pmittal5 MITS 11623 4/2/2008
		const string CLASS_NAME = "PiPatient";
		private PiPatient PiPatient{get{return objData as PiPatient;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "EventId";
		private int m_iEventId = 0;
        string sSql = string.Empty;
		//To track patientid
		private bool m_IsNew=false;
        private int m_iParentRowId = 0;
        private string m_sParentTableName = string.Empty;
        const string FILTER_KEY_NAME2 = "ParentRowId";
        const string FILTER_KEY_NAME3 = "ParentTableName";
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();

		public override void InitNew()
		{
			base.InitNew(); 

            //this.m_iEventId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
            //string sFilter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_iEventId.ToString();
            //rsushilaggar JITA 11736  
            //this.m_iEventId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);   
            string sFilter = string.Empty;
            this.m_iParentRowId = base.GetSysExDataNodeInt("/SysExData/ParentRowId", false);
            this.m_sParentTableName = base.GetFormVarNodeText("SysFormPForm");
            this.PiPatient.ParentRowId = this.m_iParentRowId;
            //-----sgupta320:Jira 15676
            switch ((this.m_sParentTableName).ToUpper())
            {
                case "CLAIMGC":
                    m_SecurityId = RMO_GC_PERSONINVOLVED;
                    this.PiPatient.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.m_sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiPatient.ParentTableName + "' ";
                    break;
                case "CLAIMDI":
                    m_SecurityId = RMO_DI_PERSONINVOLVED;
                    this.PiPatient.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.m_sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiPatient.ParentTableName + "' ";
                    break;
                case "CLAIMPC":
                    m_SecurityId = RMO_PC_PERSONINVOLVED;
                    this.PiPatient.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.m_sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiPatient.ParentTableName + "' ";
                    break;
                case "CLAIMWC":
                    m_SecurityId = RMO_WC_PERSONINVOLVED;
                    this.PiPatient.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.m_sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiPatient.ParentTableName + "' ";
                    break;
                case "CLAIMVA":
                    m_SecurityId = RMO_VA_PERSONINVOLVED;
                    this.PiPatient.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.m_sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiPatient.ParentTableName + "' ";
                    break;
                case "CLAIMXPOLICY":
                    m_SecurityId = RMO_POLICY_PERSONINVOLVED;
                    this.PiPatient.ParentTableName = "POLICY";
                    m_iEventId = CommonFunctions.GetEventIdByPolicyId(this.m_iParentRowId, this.m_sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiPatient.ParentTableName + "' ";
                    break;
                default:
                    // Defaults to Event's person involved
                    m_iEventId = m_iParentRowId;
                    this.PiPatient.ParentTableName = "EVENT";
                    sFilter = "(" + objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_iEventId.ToString() + " OR (" + objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiPatient.ParentTableName + "'))";
                    break;
            }
            //End rsushilaggar Jira 11736
			
			//BSB Moved from problematic Code below (Saved before patient_id was populated)
			int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid",false);

            // Naresh/Alok Get the PatientId from the SysEx
            int iPatientId = base.GetSysExDataNodeInt("/SysExData/PatientId", false);
            sFilter += " AND PI_EID=" + iPiEid;

			(objData as INavigation).Filter = sFilter;
			
			this.PiPatient.EventId = this.m_iEventId;
            //pmittal5 MITS 11623 4/2/2008  -Start
            //this.PiPatient.PiEid = iPiEid;
            //Start: rsushilaggar : MITS 19892 12/03/2010

            // Naresh/Alok If the PatientId is zero, then the Page is loading for the first time
            // In This case Patient Id comes in PiEid
            // At the time of Save, PatientId is present in SysEx, and Patient Entity Id Comes in PiEid
            // So We assign the value of PatientId in iPiEid
            if (iPatientId != 0)
            {
                iPiEid = iPatientId;
            }
            //this.PiPatient.PatientId = iPiEid;// reverted the changes for MITS 30805 rsushilaggar
            //End: rsushilaggar 
           //dbisht6 Entity Role
        //    string sUseFullEntitySearch = CommonFunctions.GetUserPrefNodeVal(m_sConnectionString, this.PiPatient.Context.RMUser.UserId, "setting//UseFullEntitySearch");
            
            //rsushilaggar JIRA 11730
            if (this.PiPatient.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                this.PiPatient.PiEid = iPiEid;
            }
            else
            {
                string sSql = "SELECT PATIENT_EID FROM PATIENT WHERE PATIENT_ID = " + iPiEid;
                DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                if (objReader.Read())
                {
                    this.PiPatient.PiEid =Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("PATIENT_EID"), base.ClientId);
                }
                objReader.Close();
            }
            //pmittal5 MITS 11623 4/2/2008  -End
            //rsushilaggar JIRA 7767
            if (PiPatient.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (PiPatient.PiEntity.EntityId <= 0 && PiPatient.PiEntity.NameType <= 0)
                    PiPatient.PiEntity.NameType = objCache.GetCodeId("IND", "ENTITY_NAME_TYPE");
            }
		}

//		public override void AfterAddNew()
//		{
//			base.AfterAddNew ();
//
//			int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid",false);
// 
//			if(iPiEid>0)
//			{
//				string sSQL = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE EVENT_ID=" + this.m_iEventId + " AND PI_EID=" + iPiEid;
//				int iPiRowId = 0;
//				iPiRowId = this.PiPatient.Context.DbConnLookup.ExecuteInt(sSQL);
//				if(iPiRowId>0)
//				{
//					// PI Record already exists
//					this.PiPatient.MoveTo(iPiRowId); 
//				}
//				else
//				{
//					// TODO - Confirm whether this would do the trick. 
//					// PiPatient, PiPhysician and PiMedStaff are different from rest of the bunch.
//					this.PiPatient.PiEid = iPiEid;
//					this.PiPatient.Patient.PatientEid = iPiEid;   // JP 1.21.2006   Need to point Patient record at entity sent in.
//					this.PiPatient.Save();  
//				}
//			}
//		}
		public string GetSearchStringFromFormName(string p_sFormName,
			string p_sFormId,ref string p_sClaimId,ref string p_sEventId)
		{
			string sSearchString="";
			switch(p_sFormName)
			{
				case "event":
					sSearchString="Event Occurrence/Person Involved"; 
					p_sEventId=p_sFormId;
					break;
				case "claimgc":
					sSearchString="General Claims/Person Involved"; 
					p_sClaimId=p_sFormId;
					break;
				case "claimwc":
					sSearchString="Workers' Compensation /Person Involved"; 
					p_sClaimId=p_sFormId;
					break;
				case "claimdi":
					sSearchString="Non-Occupational Claims/Person Involved"; 
					p_sClaimId=p_sFormId;
					break;
				case "claimva":
					sSearchString="Vehicle Accident Claims/Person Involved"; 
					p_sClaimId=p_sFormId;
					break;
				default:
					sSearchString="Person Involved List"; 
					break;
					
			}
			return sSearchString;
		}
		public override void BeforeSave(ref bool bCancel)
		{
			m_IsNew=PiPatient.IsNew;
            int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid", false);
            int iRoleTableId = this.PiPatient.Context.LocalCache.GetTableId(Globalization.IndividualGlossaryTableNames.PATIENTS.ToString());
            this.m_iParentRowId = base.GetSysExDataNodeInt("/SysExData/ParentRowId", false);

            if (iPiEid > 0 && (this.m_objData as PiPatient).IsNew)
            {
                //string sSQL = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE EVENT_ID=" + this.m_iEventId + " AND PI_EID=" + iPiEid;
                string sSQL = string.Empty;
                if (this.PiPatient.Context.InternalSettings.SysSettings.UseEntityRole)
                    sSQL = string.Format("SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PARENT_ROW_ID= {0} AND PI_EID= {1} AND ROLE_TABLE_ID= {2} AND  PARENT_TABLE_NAME = '{3}' ", this.m_iParentRowId, iPiEid, iRoleTableId, this.PiPatient.ParentTableName);
                else
                    sSQL = string.Format("SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PARENT_ROW_ID= {0} AND PI_EID= {1} AND  PARENT_TABLE_NAME = '{2}' ", this.m_iParentRowId, iPiEid, this.PiPatient.ParentTableName);

                int iPiRowId = 0;
                iPiRowId = this.PiPatient.Context.DbConnLookup.ExecuteInt(sSQL);
                if (iPiRowId > 0)
                {
                    // PI Record already exists	
                    bCancel = true;
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), string.Format(Globalization.GetString("Validation.PatientAlreadyExistsAsPI", base.ClientId, sLangCode), this.PiPatient.ParentTableName.ToLower()), BusinessAdaptorErrorType.Error);
                    return;
                }
            }
		}
		public override void AfterSave()
		{
			
			HipaaLog obj=new HipaaLog(this.PiPatient.Context.DbConn.ConnectionString, base.ClientId);
            //XmlDocument objSysFormStack=  base.SysFormStack;
            //XmlNode objNode=objSysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[position()=last()]");
            //string sFormName="";
            //string sFormId="0";
			string sEventId="0";
			string sClaimId="0";
            //if (objNode!=null)
            //{
            //    sFormName=objNode.SelectSingleNode("SysFormName").InnerText;
            //    sFormId=objNode.SelectSingleNode("SysFormId").InnerText;
            //    sFormName=GetSearchStringFromFormName(sFormName,sFormId,ref sClaimId,ref sEventId);
            //}
			if (sEventId=="0")
			{
				sEventId=this.PiPatient.EventId.ToString();
			}
			if (m_IsNew==false)
			{
				//obj.LogHippaInfo(this.PiPatient.PiEid,sEventId,sClaimId,sFormName,"UD",this.PiPatient.Context.RMUser.LoginName);
                obj.LogHippaInfo(this.PiPatient.PiEid, sEventId, sClaimId,"Event Occurrence/Person Involved", "UD", this.PiPatient.Context.RMUser.LoginName);
			}
		}
		public override void BeforeDelete(ref bool Cancel)
		{
			HipaaLog obj=new HipaaLog(this.PiPatient.Context.DbConn.ConnectionString, base.ClientId);
            //XmlDocument objSysFormStack=  base.SysFormStack;
            //XmlNode objNode=objSysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[position()=last()]");
            //string sFormName="";
            //string sFormId="0";
			string sEventId="0";
			string sClaimId="0";
            //if (objNode!=null)
            //{
            //    sFormName=objNode.SelectSingleNode("SysFormName").InnerText;
            //    sFormId=objNode.SelectSingleNode("SysFormId").InnerText;
            //    sFormName=GetSearchStringFromFormName(sFormName,sFormId,ref sClaimId,ref sEventId);
            //}
			if (sEventId=="0")
			{
				sEventId=this.PiPatient.EventId.ToString();
			}
			//obj.LogHippaInfo(this.PiPatient.PiEid,sEventId,sClaimId,sFormName,"DL",this.PiPatient.Context.RMUser.LoginName);
            obj.LogHippaInfo(this.PiPatient.PiEid, sEventId, sClaimId, "Event Occurrence/Person Involved", "DL", this.PiPatient.Context.RMUser.LoginName);
		}
	
        public PiPatientForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
            //pmittal5 MITS 11623 4/2/2008
            m_sConnectionString = fda.connectionString;
		}
		
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
            //Start by Shivendu for MITS 11831
            int iCount = 0;
            if (this.PiPatient.PatientId != 0)
            {
                using (DbReader objRead = PiPatient.Context.DbConnLookup.ExecuteReader(
                        String.Format(@"SELECT COUNT(*) FROM PATIENT 
					WHERE PATIENT_ID = '{0}'", this.PiPatient.PatientId)))
                {
                    if (objRead.Read())
                    {
                        iCount = Conversion.ConvertObjToInt(objRead.GetValue(0), base.ClientId);
                    }
                }

                if (SysEx.DocumentElement.SelectSingleNode("CheckIfPatientExists") == null)
                {
                    CreateSysExData("CheckIfPatientExists");

                }
                if (iCount <= 0)
                {
                    base.ResetSysExData("CheckIfPatientExists", "0");
                }
                else
                {
                    base.ResetSysExData("CheckIfPatientExists", "1");
                }
            }
            //End by SHivendu for MITS 11831
			if (this.PiPatient.PatientId!=0)
			{
				HipaaLog objHipaa=new HipaaLog(this.PiPatient.Context.DbConn.ConnectionString, base.ClientId);
                //XmlDocument objSysFormStack=  base.SysFormStack;
                //XmlNode objNode=objSysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[position()=last()]");
                //string sFormName="";
                //string sFormId="0";
				string sEventId="0";
				string sClaimId="0";
                //if (objNode!=null)
                //{
                //    sFormName=objNode.SelectSingleNode("SysFormName").InnerText;
                //    sFormId=objNode.SelectSingleNode("SysFormId").InnerText;
                //    sFormName=GetSearchStringFromFormName(sFormName,sFormId,ref sClaimId,ref sEventId);
                //}
				if (sEventId=="0")
				{
					sEventId=this.PiPatient.EventId.ToString();
				}
				if (m_IsNew)
				{
					//objHipaa.LogHippaInfo(this.PiPatient.PiEid,sEventId,sClaimId,sFormName,"NW",this.PiPatient.Context.RMUser.LoginName);
                    objHipaa.LogHippaInfo(this.PiPatient.PiEid, sEventId, sClaimId, "Event Occurrence/Person Involved", "NW", this.PiPatient.Context.RMUser.LoginName);
				}
				else if (base.m_fda.SafeFormVariableParamText("SysCmd")!="6" && base.m_fda.SafeFormVariableParamText("SysCmd")!="5" && base.m_fda.SafeFormVariableParamText("SysCmd")!="7" && base.m_fda.SafeFormVariableParamText("SysCmd")!="8" && base.m_fda.SafeFormVariableParamText("SysCmd")!="")
				{
					//objHipaa.LogHippaInfo(this.PiPatient.PiEid,sEventId,sClaimId,sFormName,"VW",this.PiPatient.Context.RMUser.LoginName);
                    objHipaa.LogHippaInfo(this.PiPatient.PiEid, sEventId, sClaimId, "Event Occurrence/Person Involved", "VW", this.PiPatient.Context.RMUser.LoginName);
				}
			}
			
			XmlDocument objSysExDataXmlDoc = base.SysEx;

			string sSubTitleText = string.Empty; 
			
			// Add EventNumber node, if already not present, to avoid Orbeon 'ref' error 			
			XmlNode objEventNumberNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/EventNumber");

			if(objEventNumberNode==null)
			{
                //Deb MITS 27711 
                if (this.PiPatient.Context.InternalSettings.SysSettings.UseAcrosoftInterface)
                {
                    string sEventnumber = this.PiPatient.Context.DbConnLookup.ExecuteString("SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + this.PiPatient.EventId);
                    base.CreateSysExData("EventNumber", sEventnumber);
                }
                else
                {
                    base.CreateSysExData("EventNumber");
                }
                //Deb MITS 27711 
			}
			else
			{
				if(objEventNumberNode.InnerText.Length>0)
					sSubTitleText = " [" + objEventNumberNode.InnerText + "]"; 
				objEventNumberNode = null;
			}
            //smishra25: MITS 24395, MITS 13748 implementation was missing
            //nadim for 13748,added to hide/unhide SSN field
            if (!PiPatient.Context.RMUser.IsAllowedEx(m_SecurityId, FormBase.RMO_PI_PATIENT_VIEW_SSN))
            {
                base.AddKillNode("taxid");

            }
            //nadim for 13748

			// Add ClaimNumber node, if already not present, to avoid Orbeon 'ref' error
			XmlNode objClaimNumberNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/ClaimNumber");

			if(objClaimNumberNode==null)
			{
				base.CreateSysExData("ClaimNumber"); 
			}			
			else
			{
				if(objClaimNumberNode.InnerText.Length>0)
					sSubTitleText = " [" + objClaimNumberNode.InnerText + "]"; 
				objClaimNumberNode = null;
			}
			
			// TODO - To find a better way to ascertain which is our starting page (Event or Claim)
			// As of now, first event number and then claim number is checked for subtitle text.
			base.ResetSysExData("SubTitle",sSubTitleText);

            //mona
            ArrayList singleRow = null;
         

			//objSysExDataXmlDoc = null;

            //Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.  
            Event objEvent = (Event)this.PiPatient.Context.Factory.GetDataModelObject("Event", false);
            string sEventDateFromDB = string.Empty;
            if (objEvent != null)
            {
                int iEventId = PiPatient.EventId;
                if (iEventId != 0)
                {
                    objEvent.MoveTo(iEventId);
                    sEventDateFromDB = objEvent.DateOfEvent;
                    if (!String.IsNullOrEmpty(sEventDateFromDB))
                    {
                        if (base.SysEx.SelectSingleNode("DateOfEvent") == null)
                        {
                            string sUIEventDate = string.Empty;
                            sUIEventDate = Conversion.ToDate(sEventDateFromDB).ToString("MM/dd/yyyy");
                            base.CreateSysExData("DateOfEvent", sUIEventDate);
                        }
                    }
                    switch (PiPatient.ParentTableName.ToUpper())  //ijain4 : JIRA 17480 : 20/11/2015
                    {
                        //case "CLAIMGC":
                        //case "CLAIMDI":
                        //case "CLAIMPC":
                        //case "CLAIMWC":
                        case "CLAIM":
                            Claim objClaim = (Claim)this.PiPatient.Context.Factory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(PiPatient.ParentRowId);
                            sSubTitleText = string.Format(" [{0} * {1} {2}]", objClaim.ClaimNumber, PiPatient.PiEntity.FirstName, PiPatient.PiEntity.LastName);
                            break;
                        case "POLICY":
                            Policy objPolicy = (Policy)this.PiPatient.Context.Factory.GetDataModelObject("Policy", false);
                            objPolicy.MoveTo(PiPatient.ParentRowId);
                            sSubTitleText = string.Format(" [{0} * {1} {2}]", objPolicy.PolicyNumber, PiPatient.PiEntity.FirstName, PiPatient.PiEntity.LastName);
                            break;
                        default:
                            sSubTitleText = string.Format(" [{0} * {1} {2}]", objEvent.EventNumber, PiPatient.PiEntity.FirstName, PiPatient.PiEntity.LastName);
                            break;
                    }
                    //ijain4 : JIRA 17480 : 20/11/2015 End
                    //skhare7 perf changes for rmA14.1 Start
                    singleRow = new ArrayList();
                  //  sSubTitleText = string.Format(" [{0} * {1} {2}]", objEvent.EventNumber, PiPatient.PiEntity.FirstName, PiPatient.PiEntity.LastName);
                    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                    base.AddElementToList(ref singleRow, "id", "formsubtitle");
                    base.AddElementToList(ref singleRow, "Text", sSubTitleText);
                    base.m_ModifiedControls.Add(singleRow);
                    //skhare7 perf changes for rmA14.1 End

                }
            }
            //Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.
			if (PiPatient.Patient.PatientEntity.BirthDate!=string.Empty)
                base.ResetSysExData("EntityAge", Utilities.CalculateAgeInYears(PiPatient.Patient.PatientEntity.BirthDate, sEventDateFromDB)); //Added sEventDateFromDB for MITS 19315
			else
				base.ResetSysExData("EntityAge","");

			if (this.PiPatient.PiRowId > 0 || this.PiPatient.PiEid <= 0)
				base.AddKillNode("javascript_SetDirtyFlag");

            // Add DiaryMessage node, if already not present, to avoid Orbeon 'ref' error
            XmlNode objDiaryMessageNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/DiaryMessage");

            if (objDiaryMessageNode == null)
            {
                base.CreateSysExData("DiaryMessage");
            }
			//Shruti Choudhary updated code for mits 10384
            //if (this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[2]/SysFormName") != null)
            //{
            //    ((XmlElement)base.SysView.SelectSingleNode("//internal[@name='SysFormPForm']")).SetAttribute("value",
            //    this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[2]/SysFormName").InnerText);
            //}

            objSysExDataXmlDoc = null;

			//Check for People Maintenance permission
			PeoplePermissionChecks4PI("pipatient", m_SecurityId+RMO_UPDATE);
            if (PiPatient.PolicyUnitRowId != 0)
            {
                DisplayUnitNo();

             
            }
            else
            {
                base.AddKillNode("Unitno");
            }
            //rsushilaggar JIRA 7767
            if (this.PiPatient.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (this.PiPatient != null && this.PiPatient.PiEntity != null && this.PiPatient.PiEntity.NameType > 0)
                    this.ResetSysExData("EntityType", objCache.GetCodeDesc(this.PiPatient.PiEntity.NameType));
                else
                    this.ResetSysExData("EntityType", objCache.GetCodeDesc(objCache.GetCodeId("IND", "ENTITY_NAME_TYPE")));
            }
            else
            {
                this.AddKillNode("entitytype");
            }
            //end rushilaggar
           
            //----sgupta320: Jira-17668
            if (base.FormVariables.SelectSingleNode("//SysSid") != null)
                base.FormVariables.SelectSingleNode("//SysSid").InnerText = Convert.ToString(m_SecurityId);
		}

		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;
			int iCount=0;
			// Perform data validation
			string sToday = Conversion.ToDbDate(System.DateTime.Now);
			string sTime = Conversion.GetTime(System.DateTime.Now.ToShortTimeString());

			//Date of Birth must <= Today
			if(PiPatient.Patient.PatientEntity.BirthDate!="")
			{
				if(PiPatient.Patient.PatientEntity.BirthDate.CompareTo(sToday)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId), System.DateTime.Now.ToShortDateString()),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}

			//Date of death must be equal or greater than date of birth
			if(PiPatient.Patient.DateOfDeath!="")
			{
				if(PiPatient.Patient.DateOfDeath.CompareTo(sToday)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.DateOfDeath", base.ClientId), System.DateTime.Now.ToShortDateString()),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				if(PiPatient.Patient.PatientEntity.BirthDate!="")
				{
					if(PiPatient.Patient.DateOfDeath.CompareTo(PiPatient.Patient.PatientEntity.BirthDate)<0)
					{
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.DateOfDeath", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId)),
							BusinessAdaptorErrorType.Error);

						bError = true;
					}
				}
			}

			//Date of Death must be greater or equal to date of admission
			if(PiPatient.Patient.DateOfDeath!="" && PiPatient.Patient.DateOfAdmission!="")
			{
				if(PiPatient.Patient.DateOfDeath.CompareTo(PiPatient.Patient.DateOfAdmission)<0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.DateOfDeath", base.ClientId), Globalization.GetString("Field.DateOfAdmission", base.ClientId)),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}

			//Date of Admission must be Greater or equal to date of birth
			if(PiPatient.Patient.PatientEntity.BirthDate!="" && PiPatient.Patient.DateOfAdmission!="")
			{
				if(PiPatient.Patient.DateOfAdmission.CompareTo(PiPatient.Patient.PatientEntity.BirthDate)<0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.DateOfAdmission", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId)),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}


			//Date of Discharge must be >= than Date of Admission
			if(PiPatient.Patient.DateOfDischarge!="" && PiPatient.Patient.DateOfAdmission!="")
			{
				if(PiPatient.Patient.DateOfDischarge.CompareTo(PiPatient.Patient.DateOfAdmission)<0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThanDateOfAdmission", base.ClientId), Globalization.GetString("Field.DateOfDischarge", base.ClientId)),
						BusinessAdaptorErrorType.Error); //tmalhotra3:MITS 31125

					bError = true;
				}
			}


			//Date of NICU Admission must be Greater or equal to date of birth
			if(PiPatient.Patient.PatientEntity.BirthDate!="" && PiPatient.Patient.NbNicuAdmDate!="")
			{
				if(PiPatient.Patient.NbNicuAdmDate.CompareTo(PiPatient.Patient.PatientEntity.BirthDate)<0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.NbNicuAdmDate", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId)),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}

			//Date of NICU Discharge must be >= than Date of NICU Admission
			if(PiPatient.Patient.NbNicuDischDate!="" && PiPatient.Patient.NbNicuAdmDate!="")
			{
				if(PiPatient.Patient.NbNicuDischDate.CompareTo(PiPatient.Patient.NbNicuAdmDate)<0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.NbNicuDischDate", base.ClientId), Globalization.GetString("Field.NbNicuAdmDate", base.ClientId)),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}
			
			// Return true if there were validation errors
			Cancel = bError;
		}
        //Added Rakhi for R7:Add Emp Data Elements
        public override void OnUpdateObject()
        {
            base.OnUpdateObject();
			//ijha:Mobile Adjuster
			XmlElement objCaller = null;


			objCaller = (XmlElement)base.FormVariables.SelectSingleNode("//caller");
			if (objCaller != null)
			{
                if (objCaller.InnerText == "MobileAdjuster" || objCaller.InnerText == "MobilityAdjuster")
                {
					string sSQL = " SELECT EVENT_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + base.GetSysExDataNodeText("//ClaimNumber") + "'";
					using (DbReader objReader1 = DbFactory.ExecuteReader(m_sConnectionString, sSQL))
					{
						if (objReader1.Read())
						{
							PiPatient.EventId = Conversion.ConvertObjToInt(objReader1.GetValue("EVENT_ID"), base.ClientId);
							this.m_iEventId = PiPatient.EventId;
							//base.FormVariables.SelectSingleNode("//SysExData//EventId").InnerText = objReader.GetValue("EVENT_ID").ToString();
							if (base.FormVariables.SelectSingleNode("//SysExData//EventId") != null)
							{
								base.FormVariables.SelectSingleNode("//SysExData//EventId").InnerText = objReader1.GetValue("EVENT_ID").ToString();

							}

						}
					}
				}
			}
			// ijha end
            if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
            {
                PiPatient.Patient.PatientEntity.FormName = "PiPatientForm"; //Mits 22497

                #region "Updating Address Info Object"

                string sAddr1 = PiPatient.Patient.PatientEntity.Addr1;
                string sAddr2 = PiPatient.Patient.PatientEntity.Addr2;
                string sAddr3 = PiPatient.Patient.PatientEntity.Addr3;// JIRA 6420 pkandhari
                string sAddr4 = PiPatient.Patient.PatientEntity.Addr4;// JIRA 6420 pkandhari
                string sCity = PiPatient.Patient.PatientEntity.City;
                int iCountryCode = PiPatient.Patient.PatientEntity.CountryCode;
                int iStateId = PiPatient.Patient.PatientEntity.StateId;
                string sEmailAddress = PiPatient.Patient.PatientEntity.EmailAddress;
                string sFaxNumber = PiPatient.Patient.PatientEntity.FaxNumber;
                string sCounty = PiPatient.Patient.PatientEntity.County;
                string sZipCode = PiPatient.Patient.PatientEntity.ZipCode;
                //RMA-8753 nshah28(Added by ashish)
                string sSearchString = string.Empty;
                //RMA-8753 nshah28(Added by ashish) END

                if (
                        sAddr1 != string.Empty || sAddr2 != string.Empty || sAddr3 != string.Empty || sAddr4 != string.Empty ||
                        sCity != string.Empty || iCountryCode != 0 || iStateId != 0 || sEmailAddress != string.Empty ||
                        sFaxNumber != string.Empty || sCounty != string.Empty || sZipCode != string.Empty
                    )
                {
                    if (PiPatient.Patient.PatientEntity.EntityXAddressesList.Count == 0)
                    {


                        EntityXAddresses objEntityXAddressesInfo = PiPatient.Patient.PatientEntity.EntityXAddressesList.AddNew();
						//RMA-8753 nshah28(Added by ashish) START
                        //objEntityXAddressesInfo.Addr1 = sAddr1;
                        //objEntityXAddressesInfo.Addr2 = sAddr2;
                        //objEntityXAddressesInfo.Addr3 = sAddr3;// JIRA 6420 pkandhari
                        //objEntityXAddressesInfo.Addr4 = sAddr4;// JIRA 6420 pkandhari
                        //objEntityXAddressesInfo.City = sCity;
                        //objEntityXAddressesInfo.Country = iCountryCode;
                        //objEntityXAddressesInfo.State = iStateId;
                        objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                        objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                        objEntityXAddressesInfo.Address.Addr3 = sAddr3;
                        objEntityXAddressesInfo.Address.Addr4 = sAddr4;
                        objEntityXAddressesInfo.Address.City = sCity;
                        objEntityXAddressesInfo.Address.Country = iCountryCode;
                        objEntityXAddressesInfo.Address.State = iStateId;
                        objEntityXAddressesInfo.Email = sEmailAddress;
                        objEntityXAddressesInfo.Fax = sFaxNumber;
                        //objEntityXAddressesInfo.County = sCounty;
                        //objEntityXAddressesInfo.ZipCode = sZipCode;
						objEntityXAddressesInfo.Address.County = sCounty;
                        objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                        objEntityXAddressesInfo.EntityId = PiPatient.Patient.PatientEntity.EntityId;
                        objEntityXAddressesInfo.PrimaryAddress = -1;
                        //objEntityXAddressesInfo.AddressId = -1;

                        AddressForm objAddressForm = new AddressForm(m_fda);
                        sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                        objEntityXAddressesInfo.Address.SearchString = sSearchString;
                        objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, PiPatient.Context.RMDatabase.ConnectionString, base.ClientId);
                        objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
                        //RMA-8753 nshah28(Added by ashish) END

                    }
                    else
                    {
                        foreach (EntityXAddresses objEntityXAddressesInfo in PiPatient.Patient.PatientEntity.EntityXAddressesList)
                        {
                            if (objEntityXAddressesInfo.PrimaryAddress == -1)
                            {
								//RMA-8753 nshah28(Added by ashish) START
                                //objEntityXAddressesInfo.Addr1 = sAddr1;
                                //objEntityXAddressesInfo.Addr2 = sAddr2;
                                //objEntityXAddressesInfo.Addr3 = sAddr3;// JIRA 6420 pkandhari
                                //objEntityXAddressesInfo.Addr4 = sAddr4;// JIRA 6420 pkandhari
                                //objEntityXAddressesInfo.City = sCity;
                                //objEntityXAddressesInfo.Country = iCountryCode;
                                //objEntityXAddressesInfo.State = iStateId;
                                objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                                objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                                objEntityXAddressesInfo.Address.Addr3 = sAddr3;
                                objEntityXAddressesInfo.Address.Addr4 = sAddr4;
                                objEntityXAddressesInfo.Address.City = sCity;
                                objEntityXAddressesInfo.Address.Country = iCountryCode;
                                objEntityXAddressesInfo.Address.State = iStateId;
                                objEntityXAddressesInfo.Email = sEmailAddress;
                                objEntityXAddressesInfo.Fax = sFaxNumber;
                                //objEntityXAddressesInfo.County = sCounty;
                                //objEntityXAddressesInfo.ZipCode = sZipCode;
                                objEntityXAddressesInfo.Address.County = sCounty;
                                objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                                AddressForm objAddressForm = new AddressForm(m_fda);
                                sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                                objEntityXAddressesInfo.Address.SearchString = sSearchString;
                                objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, PiPatient.Context.RMDatabase.ConnectionString, base.ClientId);
                                objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
                                //RMA-8753 nshah28(Added by ashish) END
                                break;
                            }

                        }
                    }
                }
                else
                {
                    if (PiPatient.Patient.PatientEntity.EntityXAddressesList.Count > 0)
                    {
                        foreach (EntityXAddresses objEntityXAddressesInfo in PiPatient.Patient.PatientEntity.EntityXAddressesList)
                        {
                            if (objEntityXAddressesInfo.PrimaryAddress == -1)
                            {
                                //PiPatient.Patient.PatientEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                                PiPatient.Patient.PatientEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId); //RMA-8753 nshah28(Added by ashish)
                                break;
                            }

                        }
                    }

                }
            #endregion

                #region Added for updating Phone Numbers

                string sOfficePhone = PiPatient.Patient.PatientEntity.Phone1.Trim();
                string sHomePhone = PiPatient.Patient.PatientEntity.Phone2.Trim();
                int iHomeCode = objCache.GetCodeId("h", "PHONES_CODES");
                int iOfficeCode = objCache.GetCodeId("o", "PHONES_CODES");
                bool bOfficePhoneEntered = false;
                AddressXPhoneInfo objAddressesInfo = null;
                bool bOfficeCodeExists = false;
                bool bHomePhoneEntered = false;
                bool bHomeCodeExists = false;

                if (sOfficePhone != string.Empty)
                    bOfficePhoneEntered = true;
                if (sHomePhone != string.Empty)
                    bHomePhoneEntered = true;

                if (PiPatient.Patient.PatientEntity.AddressXPhoneInfoList.Count == 0)
                {
                    if (bOfficePhoneEntered)
                    {
                        objAddressesInfo = PiPatient.Patient.PatientEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iOfficeCode;
                        objAddressesInfo.PhoneNo = sOfficePhone;
                    }
                    if (bHomePhoneEntered)
                    {
                        objAddressesInfo = PiPatient.Patient.PatientEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iHomeCode;
                        objAddressesInfo.PhoneNo = sHomePhone;
                    }
                }
                else
                {
                    foreach (AddressXPhoneInfo objXAddressesInfo in PiPatient.Patient.PatientEntity.AddressXPhoneInfoList)
                    {
                        if (objXAddressesInfo.PhoneCode == iOfficeCode)
                        {
                            if (bOfficePhoneEntered)
                            {
                                objXAddressesInfo.PhoneNo = sOfficePhone;
                                bOfficeCodeExists = true;
                            }
                            else
                            {
                                PiPatient.Patient.PatientEntity.AddressXPhoneInfoList.Remove(objXAddressesInfo.PhoneId);
                            }
                            continue;
                        }
                        else if (objXAddressesInfo.PhoneCode == iHomeCode)
                        {
                            if (bHomePhoneEntered)
                            {
                                objXAddressesInfo.PhoneNo = sHomePhone;
                                bHomeCodeExists = true;
                            }
                            else
                            {
                                PiPatient.Patient.PatientEntity.AddressXPhoneInfoList.Remove(objXAddressesInfo.PhoneId);
                            }
                        }
                    }
                    if (bOfficePhoneEntered && !bOfficeCodeExists)
                    {
                        objAddressesInfo = PiPatient.Patient.PatientEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iOfficeCode;
                        objAddressesInfo.PhoneNo = sOfficePhone;
                    }
                    if (bHomePhoneEntered && !bHomeCodeExists)
                    {
                        objAddressesInfo = PiPatient.Patient.PatientEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iHomeCode;
                        objAddressesInfo.PhoneNo = sHomePhone;
                    }
                }

                #endregion
            }
            
        }
        //Added Rakhi for R7:Add Emp Data Elements
        private void DisplayUnitNo()
        {

            string sStaUnitNo = string.Empty;
            string sRiskLoc = string.Empty;
            string sSubLoc = string.Empty;

            StringBuilder sbSql = new StringBuilder();
            // RMA-10039: Ash, fixed error but in general this is incorrect handling for POINT, Integral, Staging
            sbSql.Append("SELECT POINT_UNIT_DATA.STAT_UNIT_NUMBER, POINT_UNIT_DATA.UNIT_RISK_LOC, POINT_UNIT_DATA.UNIT_RISK_SUB_LOC from POLICY_X_UNIT,POINT_UNIT_DATA,PERSON_INVOLVED ");
            sbSql.Append(" WHERE POLICY_X_UNIT.POLICY_UNIT_ROW_ID=PERSON_INVOLVED.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.UNIT_ID=POINT_UNIT_DATA.UNIT_ID ");
            sbSql.Append(" AND POINT_UNIT_DATA.UNIT_TYPE=POLICY_X_UNIT.UNIT_TYPE AND PERSON_INVOLVED.PI_ROW_ID=" +PiPatient.PiRowId);

            DbReader objReader = null;
            objReader = DbFactory.GetDbReader(base.m_fda.connectionString, sbSql.ToString());
            if (objReader != null)
            {
                if (objReader.Read())
                {
                    sStaUnitNo = objReader.GetValue("STAT_UNIT_NUMBER").ToString();
                    sRiskLoc = objReader.GetValue("UNIT_RISK_LOC").ToString();
                    if (sRiskLoc == string.Empty)
                        sRiskLoc =  "-";

                    sSubLoc = objReader.GetValue("UNIT_RISK_SUB_LOC").ToString();
                    if (sSubLoc == string.Empty)
                        sSubLoc =  "-";

                    base.ResetSysExData("UnitNo", sStaUnitNo + "/" + sRiskLoc + "/" + sSubLoc);


                }

            }


        }
	}
}
