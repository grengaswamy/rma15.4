﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Application.SupportScreens;
//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PatientProcedure Screen.
	/// </summary>
	public class PatientProcedureForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PatientXProcedure";
		//To track patientid
		private bool m_IsNew=false;
		private PatientXProcedure PatientXProcedure{get{return objData as PatientXProcedure;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "PatientId";

		/// <summary>
		/// Class constructor which initializes Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public PatientProcedureForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = 	CLASS_NAME;		
		}

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();

			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlNode objPatientId=null;

			try
			{
				objPatientId = base.SysEx.GetElementsByTagName(FILTER_KEY_NAME)[0];
			}//end try
			catch
			{
			}; //end catch
			
			if(objPatientId !=null)
				this.m_ParentId = Conversion.ConvertObjToInt(objPatientId.InnerText, base.ClientId);


			//Assign the Patient ID of the DataModel class 
			//to the ParentId of the current class		
			PatientXProcedure.PatientId = this.m_ParentId;

			//Filter by this Patient ID if present
			if(this.m_ParentId != 0)
			{
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId;
			}
			
		}//end method InitNew()
		
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm();
			if (this.PatientXProcedure.ProcRowId!=0)
			{
				HipaaLog obj=new HipaaLog(this.PatientXProcedure.Context.DbConn.ConnectionString, base.ClientId);
				string sFormName="Patient Procedure";
				Patient objTmp=(Patient)this.PatientXProcedure.Context.Factory.GetDataModelObject("Patient",false);
				objTmp.MoveTo(this.PatientXProcedure.PatientId);
				int iPatientEid=objTmp.PatientEid;
				objTmp.Dispose();
				if (m_IsNew)
				{
					obj.LogHippaInfo(iPatientEid,"0","0",sFormName,"NW",this.PatientXProcedure.Context.RMUser.LoginName);
				}
				else if (base.m_fda.SafeFormVariableParamText("SysCmd")!="6" && base.m_fda.SafeFormVariableParamText("SysCmd")!="5" && base.m_fda.SafeFormVariableParamText("SysCmd")!="7" && base.m_fda.SafeFormVariableParamText("SysCmd")!="8" && base.m_fda.SafeFormVariableParamText("SysCmd")!="")
				{
					obj.LogHippaInfo(iPatientEid,"0","0",sFormName,"VW",this.PatientXProcedure.Context.RMUser.LoginName);
				}
			}
			
		}
		public override void BeforeSave(ref bool bCancel)
		{
			m_IsNew=PatientXProcedure.IsNew;
		}
		public override void AfterSave()
		{
			HipaaLog obj=new HipaaLog(this.PatientXProcedure.Context.DbConn.ConnectionString, base.ClientId);
			string sFormName="Patient Procedure";
			Patient objTmp=(Patient)this.PatientXProcedure.Context.Factory.GetDataModelObject("Patient",false);
			objTmp.MoveTo(this.PatientXProcedure.PatientId);
			int iPatientEid=objTmp.PatientEid;
			objTmp.Dispose();
			if (m_IsNew==false)
			{
				obj.LogHippaInfo(iPatientEid,"0","0",sFormName,"UD",this.PatientXProcedure.Context.RMUser.LoginName);
			}
		}
		public override void BeforeDelete(ref bool Cancel)
		{
			HipaaLog obj=new HipaaLog(this.PatientXProcedure.Context.DbConn.ConnectionString, base.ClientId);
			string sFormName="Patient Procedure";
			Patient objTmp=(Patient)this.PatientXProcedure.Context.Factory.GetDataModelObject("Patient",false);
			objTmp.MoveTo(this.PatientXProcedure.PatientId);
			int iPatientEid=objTmp.PatientEid;
			objTmp.Dispose();
			obj.LogHippaInfo(iPatientEid,"0","0",sFormName,"DL",this.PatientXProcedure.Context.RMUser.LoginName);
		}
	}
}
