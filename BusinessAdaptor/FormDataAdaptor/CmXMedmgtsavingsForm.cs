using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for CmXMedmgtsavingsForm.
	/// </summary>
	public class CmXMedmgtsavingsForm : DataEntryFormBase
	{
		const string CLASS_NAME = "CmXMedmgtsavings";
		private CmXMedmgtsavings CmXMedmgtsavings{get{return objData as CmXMedmgtsavings;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "CmmsRowId";

		public override void InitNew()
		{
			base.InitNew(); 
			this.CmXMedmgtsavings.CasemgtRowId = base.GetSysExDataNodeInt("/SysExData/CasemgtRowId");
			//this.m_iCmmsRowId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,true);
			//(objData as INavigation).Filter = 
			//	objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_iCmmsRowId.ToString();		
			//this.CmXMedmgtsavings.CmmsRowId = this.m_iCmmsRowId;	
		}

		public CmXMedmgtsavingsForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();	
	
			//for Displaying Back button on Entity Maintenance Screen
			base.ResetSysExData("DisplayBackButton","true");
		}

        // akaushik5 Added for MITS 37554 Starts
        /// <summary>
        /// Called when [security identifier changed].
        /// </summary>
        public override void OnSecurityIdChanged()
        {
            base.OnSecurityIdChanged();
            int claimId = default(int);
            int casemgtRowId = default(int);
            bool success = default(bool);
            XmlDocument objXML = base.SysEx;

            XmlNode objCasemgtRowIdNode = objXML.SelectSingleNode("/SysExData/CasemgtRowId");
            if (!object.ReferenceEquals(objCasemgtRowIdNode, null))
            {
                casemgtRowId = Conversion.CastToType<int>(objCasemgtRowIdNode.InnerText, out success);
            }

            if (!casemgtRowId.Equals(default(int)))
            {
                using (CaseManagement objCaseManagement = this.objData.Context.Factory.GetDataModelObject("CaseManagement", false) as CaseManagement)
                {
                    objCaseManagement.MoveTo(casemgtRowId);
                    claimId = objCaseManagement.ClaimId;

                    if (!claimId.Equals(default(int)))
                    {
                        using (Claim objClaim = this.objData.Context.Factory.GetDataModelObject("Claim", false) as Claim)
                        {
                            objClaim.MoveTo(claimId);
                            switch (objClaim.LineOfBusCode)
                            {
                                // WC Claim
                                case 243:
                                    base.m_SecurityId = FormBase.RMO_WC_MEDMGTSAVINGS;
                                    break;
                                // DI Claim
                                case 844:
                                    base.m_SecurityId = FormBase.RMO_DI_MEDMGTSAVINGS;
                                    break;
                            }
                        }
                    }
                }
            }
        }
        // akaushik5 Added for MITS 37554 Ends
    }
}
