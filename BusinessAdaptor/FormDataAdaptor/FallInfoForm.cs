using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;


namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for EventScreen.
	/// </summary>
	public class FallInfoForm : DataEntryFormBase
	{
		const string CLASS_NAME = "FallIndicator";

		private FallIndicator FallIndicator{get{return objData as FallIndicator;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		
		public override void InitNew()
		{
			base.InitNew();
            //parijat : Mits 9937
            if (base.m_fda.SafeFormVariableParamText("SysCmd") != "0" || base.m_fda.SafeParamText("SysFormId") == "0")
            {
                this.m_ParentId = base.GetSysExDataNodeInt("/SysExData/EventId", true);
                (objData as INavigation).Filter = "EVENT_ID=" + this.m_ParentId.ToString();
            }
			// Ideally we would like to do this, but since EventId is a KeyField
			// for FALL_INDICATOR table, this does not allow loading of the data object
			//this.FallIndicator.EventId = iEventId; 
		}

		public FallInfoForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}
		
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();

			XmlDocument objSysExDataXmlDoc = base.SysEx;

			string sSubTitleText = string.Empty; 
			
			// Add EventNumber node, if already not present, to avoid Orbeon 'ref' error 			
			XmlNode objEventNumberNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/EventNumber");

			if(objEventNumberNode==null)
			{
				base.CreateSysExData("EventNumber"); 
			}
			else
			{
				if(objEventNumberNode.InnerText.Length>0)
					sSubTitleText = " [" + objEventNumberNode.InnerText + "]"; 
				objEventNumberNode = null;
			}

			// TODO - Confirm if we would like to use SubTitle set by the Event Screen
			base.ResetSysExData("SubTitle",sSubTitleText); 
			objSysExDataXmlDoc = null;

		}
		// BSB Hack due to unique 1:1 addition situation where this child is being created on the fly.
		// Aditya - Hack still required even for the InitNew() approach. See comment in InitNew().
		public override void BeforeSave(ref bool Cancel)
		{
			if(this.FallIndicator.EventId ==0)
				this.FallIndicator.EventId = this.m_ParentId;

			base.BeforeSave (ref Cancel);

		}

		// Mihika - Defect No.178 - 10/21/2005
		// throwing the proper parent key back on in case the MoveFirst\Last request
		// didn't find any records.
		public override void AfterAction(enumFormActionType eActionType)
		{
			base.AfterAction(eActionType);
			switch(eActionType)
			{
                //enumFormActionType.AddNew: is added by Nitin for Mits 14986
                // on 12-May-2009
                case enumFormActionType.AddNew:
                case enumFormActionType.MoveFirst:
				case enumFormActionType.MoveLast:
				case enumFormActionType.MoveTo:
					if(FallIndicator.EventId==0) //No record found - treat as new.
						FallIndicator.EventId = this.m_ParentId;
					break;
			}
		}
	}
}
