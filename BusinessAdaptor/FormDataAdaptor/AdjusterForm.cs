﻿
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Xml;
using System.Text;
using Riskmaster.BusinessAdaptor.AutoFroiAcord;
using Riskmaster.Application.VSSInterface;
//using Riskmaster.Application.VSSInterface;
namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Adjuster Screen.
	/// </summary>
	public class AdjusterForm : DataEntryFormBase
	{
		const string CLASS_NAME = "ClaimAdjuster";
		const string FILTER_KEY_NAME = "ClaimId";
        private string sConnectionString = null;

		private ClaimAdjuster objClaimAdjuster{get{return objData as ClaimAdjuster;}}
		
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
        //Mridul 05/28/09 MITS 16745-Chubb ACK Letter Enhancement
        bool bTriggerLetter = false;
		
        private string m_Caption = string.Empty;
            //nnorouzi; MITS 19478
        public override string GetCaption()
        {
            return base.GetCaption();
        }

		public AdjusterForm(FormDataAdaptor fda):base(fda)
		{
			this.m_ClassName = CLASS_NAME;
            sConnectionString = fda.connectionString;
		}

		public override void InitNew()
		{
			base.InitNew();

			//set filter for ClaimId
			objClaimAdjuster.ClaimId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			if (objClaimAdjuster.ClaimId>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + objClaimAdjuster.ClaimId;
          
		}

		/// <summary>
		/// Overrides the base class OnUpdateForm method to handle ClaimNumber in the 
		/// instance data for ref binding.  
		/// </summary>
		/// <remarks>This instance data is specifically held in the SysExData section
		/// of the Instance document.
		/// </remarks>
		/// <example> 
		///		<control name="claimnumber" tabindex="2" type="id" ref="Instance/UI/FormVariables/SysExData/ClaimNumber"/>
		/// </example>
		public override void OnUpdateForm()
		{
			int iUseClaimProgressNotes = 0;
			base.OnUpdateForm ();
            ArrayList singleRow = null;
            string sDiaryMessage = "";
            XmlDocument objXML = base.SysView;            

            //Module Security Permission was not working as security id was different each time adjuster 
            //is opened from different claim.Also taking claimid from object as data from sysex is not available.

            Claim objClaim = (Claim)objClaimAdjuster.Context.Factory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(objClaimAdjuster.ClaimId);

            switch (objClaim.LineOfBusCode) 
            {
                case 241:
                    m_SecurityId = RMO_GC_ADJUSTER;
                break;
                case 242:
                    m_SecurityId = RMO_VA_ADJUSTER;
                break;
                case 243:
                    m_SecurityId = RMO_WC_ADJUSTER;
                break;
                case 844:
                    m_SecurityId = RMO_DI_ADJUSTER;
                break;
                //Start-Mridul Bansal. 01/04/10. MITS#18230. Support for Property Claims.
                case 845:
                    m_SecurityId = RMO_PC_ADJUSTER;
                    break;
                //End-Mridul Bansal. 01/04/10. MITS#18230. Support for Property Claims.
            }

            //Setting Security Id for further permissions like attachment
            XmlElement objSysSid = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSid");
            objSysSid.InnerText = m_SecurityId.ToString();

            //Create new Permission wasnt working
            if (objClaimAdjuster.AdjRowId == 0)
            {
                if (!objClaimAdjuster.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            else
            {//View Permission wasnt working
                if (!objClaimAdjuster.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    m_CurrentAction = enumFormActionType.None;
                    //XmlElement objSysSkipBindToControl = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSkipBindToControl");
                    //objSysSkipBindToControl.InnerText = "true";
                    LogSecurityError(RMO_VIEW);
                    return;
                }
            }
            //Mridul 05/28/09 MITS 16745 - Pass value of Template for Claim Letter 
            base.CreateSysExData("ClaimLetterTmplId", "0");
            
            //mgaba2:mits 29116
            iUseClaimProgressNotes = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ClmProgressNotesFlag;
            if (iUseClaimProgressNotes == 0)
            {
                base.AddKillNode("enhancednotes");
            }

            //nnorouzi; MITS 19478
            m_Caption = this.GetCaption();

			//Pass this subtitle value to view (ref'ed from @valuepath).
			base.ResetSysExData("SubTitle", m_Caption);

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", m_Caption);
            base.m_ModifiedControls.Add(singleRow);


            
            //Claim objClaim = (Claim)objClaimAdjuster.Context.Factory.GetDataModelObject("Claim", false);
            //objClaim.MoveTo(objClaimAdjuster.ClaimId);
            if (objClaim.EventId != 0 && objClaim.PrimaryClaimant != null)
                sDiaryMessage = objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
            else
                sDiaryMessage = "";
            base.ResetSysExData("DiaryMessage", sDiaryMessage);
           

            //Shruti for 10384
            //if (this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[1]/SysFormName") != null)
            //{
            //    ((XmlElement)base.SysView.SelectSingleNode("//internal[@name='SysFormPForm']")).SetAttribute("value",
            //        this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[1]/SysFormName").InnerText);
            //}
  
            		
			//Check for People Maintenance permission
			PeoplePermissionChecks4Other("adjuster", m_SecurityId+RMO_UPDATE);
            // 03/01/2007 REM  to customize the page flow : Umesh
            //Abhishek MITS 11579 Check For Entity Entry/Edit start
            //objTempNode = (XmlElement)this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[SysFormName='claimgc']");
            //if (objTempNode != null)
            //{
            //    AdjusterPermissionChecksEntityEdit("adjuster", RMO_GC_ENTITY_EDIT);
            //}
            //else
            //{
            //    objTempNode = (XmlElement)this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[SysFormName='claimwc']");
            //    if (objTempNode != null)
            //    {
            //        AdjusterPermissionChecksEntityEdit("adjuster", RMO_WC_ENTITY_EDIT);
            //    }
            //    else
            //    {
            //        objTempNode = (XmlElement)this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[SysFormName='claimva']");
            //        if (objTempNode != null)
            //        {
            //            AdjusterPermissionChecksEntityEdit("adjuster", RMO_VC_ENTITY_EDIT);
            //        }
            //        else
            //        {
            //            objTempNode = (XmlElement)this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[SysFormName='claimdi']");
            //            if (objTempNode != null)
            //            {
            //                AdjusterPermissionChecksEntityEdit("adjuster", RMO_NON_OC_ENTITY_EDIT);
            //            }
            //        }
            //    }
            //}
            //Mona:Removing screenflow stack as it is no more in use
            if (objClaimAdjuster.ClaimId != 0)
            {
                switch (objClaim.LineOfBusCode)
                {
                    case 241:
                        AdjusterPermissionChecksEntityEdit("adjuster", RMO_GC_ENTITY_EDIT);
                    break;
                    //rkulavil: RMA-17099 starts
                    //case 242:
                    //    AdjusterPermissionChecksEntityEdit("adjuster", RMO_WC_ENTITY_EDIT);
                    //break;
                    //case 243:
                    //    AdjusterPermissionChecksEntityEdit("adjuster", RMO_VC_ENTITY_EDIT);
                    //break;
                    case 242:
                        AdjusterPermissionChecksEntityEdit("adjuster", RMO_VC_ENTITY_EDIT);
                        break;
                    case 243:
                        AdjusterPermissionChecksEntityEdit("adjuster", RMO_WC_ENTITY_EDIT);
                        break;
                    //rkulavil: RMA-17099 ends
                    case 844:
                        AdjusterPermissionChecksEntityEdit("adjuster", RMO_NON_OC_ENTITY_EDIT);
                    break;

                }
            }

             objClaim.Dispose();

            
		}

		public override void BeforeApplyLookupData(ref bool Cancel, ArrayList arrToSaveFields)
		{
			arrToSaveFields.Add("EntityTableId");
		}

        public override void AfterSave()
        {
            base.AfterSave();
            //Added by Navdeep for Auto FROI ACORD - Chubb
            
            
            AutoFroiAcordAdaptor objAutoFroiAcord = null;
            objAutoFroiAcord = new AutoFroiAcordAdaptor(base.ClientId);
            string sReason = string.Empty;            
            int iRowID = 0;
            //End Navdeep for Auto FROI ACORD - Chubb
            //added by Mridul - MITS 16745 - ACK LETTER
            int iLtrGenerated = 0;
            int iClaimType = 0;
            int iEmailAck = 0;
            //string sVssFlag = ""; //averma62 - VSS export adjuster
            Event objEvent = null;
            StringBuilder sbSql = null;
            StringBuilder sOrgEid = null;
            DbReader objReader = null;
            //End Mridul - MITS 16745 - ACK LETTER
            //navdeep - Auto FROI ACORD For Chubb - Triggering Criteria
            #region Chubb AUTO FROI ACORD
            Claim objClaim = (Claim)objClaimAdjuster.Context.Factory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(objClaimAdjuster.ClaimId);
            if (objData.Context.InternalSettings.SysSettings.AutoFROIACORDFlag)
            {
                if (objClaimAdjuster.CurrentAdjFlag == true && objClaim.MailSent == "")
                {                    
                    //Commented By Navdeep - Table now changed to ADJUSTER_FROI_ACORD
                    //iRowID = Riskmaster.Common.Conversion.ConvertObjToInt(objCon.ExecuteScalar("SELECT ROW_ID FROM EMAIL_FROI_ACORD WHERE ADJUSTERS LIKE '%," + objClaim.CurrentAdjuster.AdjusterEid + ",%'"), base.ClientId);
                    iRowID = Conversion.ConvertObjToInt(objClaimAdjuster.Context.DbConn.ExecuteScalar("SELECT ROW_ID FROM ADJUSTER_FROI_ACORD WHERE ADJUSTER_EID = " + objClaimAdjuster.AdjusterEid), base.ClientId);                    
                    if (iRowID > 0)
                    {
                        // Geeta 19341  : Split architecture issue  ACORD and FROI
                        XmlNode objHostNode = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/RequestHost");
                        if (objHostNode != null)
                        {
                            objAutoFroiAcord.RequestHost = objHostNode.InnerText;
                        }
                       
                        sReason = objAutoFroiAcord.generateFROIACORD(objClaim);
                        objAutoFroiAcord.FROIACORDERRLOG(objClaim, sReason);
                    }
                    else
                    {
                        objAutoFroiAcord.FROIACORDERRLOG(objClaim, "Current Adjuster was not found in Assigned Adjuster List");
                    }

                }
                else if (objClaimAdjuster.CurrentAdjFlag == false && objClaim.MailSent == "")
                {
                    objAutoFroiAcord.FROIACORDERRLOG(objClaim, "Triggering criteria not met.");
                }
            }
            #endregion Chubb AUTO FROI ACORD
            //Mridul. MITS 16745-Chubb Enhancement
            #region ClaimLetter
            int iTempId = 0;
            
            base.ResetSysExData("ClaimLetterTmplId", "");
            if (objData.Context.InternalSettings.SysSettings.ClaimLetterFlag)
            {
                iTempId = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar("SELECT ACKLTR_TMPL_ID FROM SYS_PARMS_CLM_LTR"), base.ClientId);
                if (bTriggerLetter && iTempId != 0)
                {
                    //If generated before
                    iLtrGenerated = Conversion.ConvertObjToInt(objClaimAdjuster.Context.DbConn.ExecuteScalar("SELECT LTR_FLAG FROM CLM_LTR_LOG WHERE LTR_FLAG = -1 AND CLAIM_ID = " + objClaimAdjuster.ClaimId.ToString()), base.ClientId);
                    if (iLtrGenerated == 0)
                    {
                        iClaimType = Conversion.ConvertObjToInt(objClaimAdjuster.Context.DbConn.ExecuteScalar("SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + objClaimAdjuster.ClaimId), base.ClientId);
                        //Commented BY Navdeep - Table changed to SYS_UTIL_CLM_LTR instead of SYS_PARMS_CLM_LTR for Claim Type IDS
                        //string sTypeNotInList = Conversion.ConvertObjToStr(objClaimAdjuster.Context.DbConn.ExecuteScalar("SELECT DTTM_RCD_LAST_UPD FROM SYS_PARMS_CLM_LTR WHERE X_CLAIM_TYPE_CODE NOT LIKE '%," + iClaimType.ToString() + ",%'"));                        
                        string sTypeNotInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT COLUMN_DESC FROM SYS_UTIL_CLM_LTR WHERE ROW_ID = 2 AND CODE_EID IN (" + iClaimType + ")"));
                        //Check if Claim Type is in Included List
                        //Commented by Code - Change due to above modified Query
                        //if (sTypeNotInList != "")
                        if (string.IsNullOrEmpty(sTypeNotInList))
                        {
                            //string sAdjNotInList = Conversion.ConvertObjToStr(objClaimAdjuster.Context.DbConn.ExecuteScalar("SELECT ADJUSTERS FROM SYS_PARMS_CLM_LTR WHERE ADJUSTERS NOT LIKE '%," + objClaimAdjuster.AdjusterEid + ",%'"));
                            //Commented BY Navdeep - Table changed to SYS_UTIL_CLM_LTR instead of SYS_PARMS_CLM_LTR for Adjuster EIDs
                            //string sAdjNotInList = Conversion.ConvertObjToStr(objClaimAdjuster.Context.DbConn.ExecuteScalar("SELECT ADJUSTERS FROM SYS_PARMS_CLM_LTR WHERE ADJUSTERS LIKE '%," + objClaimAdjuster.AdjusterEid + ",%'"));

                            string sAdjNotInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT COLUMN_DESC FROM SYS_UTIL_CLM_LTR WHERE ROW_ID = 1 AND CODE_EID IN (" + objClaimAdjuster.AdjusterEid + ")"));
                            //Check if Current Adjuster is in Included List
                            //if (sAdjNotInList != "")
                            if (!string.IsNullOrEmpty(sAdjNotInList))
                            {
                                //Check if ACK should be generated
                                objEvent = (Event)m_fda.Factory.GetDataModelObject("Event", false);
                                objEvent.MoveTo(objClaim.EventId);
                                sbSql = new StringBuilder();
                                sOrgEid = new StringBuilder();
                                sbSql.Append("SELECT * FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid);
                                using (objReader = objClaim.Context.DbConn.ExecuteReader(sbSql.ToString()))
                                {
                                    if (objReader.Read())
                                        sOrgEid.Append(objReader.GetInt("DEPARTMENT_EID") + "," + objReader.GetInt("FACILITY_EID") + "," + objReader.GetInt("LOCATION_EID") + "," + objReader.GetInt("DIVISION_EID") + "," + objReader.GetInt("REGION_EID") + "," + objReader.GetInt("OPERATION_EID") + "," + objReader.GetInt("COMPANY_EID") + "," + objReader.GetInt("CLIENT_EID"));
                                }

                                //Geeta : Mits 18718 for Acord Comma Seperated List Issue
                                iEmailAck = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar("SELECT COUNT(EMAIL_ACK) FROM ENT_X_CONTACTINFO EXC,CONTACT_LOB WHERE EXC.EMAIL_ACK = -1 AND EXC.ENTITY_ID IN (" + sOrgEid.ToString() + ") AND EXC.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode ), base.ClientId);

                                //Send CLOSED Letter Signal
                                if (iEmailAck > 0)
                                    base.ResetSysExData("ClaimLetterTmplId", "ACK");
                            }
                        }
                    }
                }
            }
            bTriggerLetter = false;
            #endregion ClaimLetter

            #region VSS Adjuster Export "Asharma326 For VSS-RMA Enhancemnts"
            //No need to check claim vss flag
            //sVssFlag = Conversion.ConvertObjToStr(objClaimAdjuster.Context.DbConn.ExecuteScalar("SELECT VSS_CLAIM_IND FROM CLAIM WHERE CLAIM_ID = " + objClaimAdjuster.ClaimId));
            if (objClaimAdjuster.Context.InternalSettings.SysSettings.EnableVSS.Equals(-1)
                &&(objClaimAdjuster.CurrentAdjFlag == true)
                &&(objClaim.ReserveCurrentList.Count>0))//MITS 33302
            {
                    //  Riskmaster.Application.PatriotProtectorWrapper.PatriotProtectorWrapper obj = new Riskmaster.Application.PatriotProtectorWrapper.PatriotProtectorWrapper(FundsObject.PayeeEntity.Context.RMDatabase.DataSourceName, FundsObject.PayeeEntity.Context.RMUser.LoginName, FundsObject.PayeeEntity.Context.RMUser.Password);
                    // obj.AsynchPatriotProtectorCall(FundsObject.PayeeEntity.FirstName, FundsObject.PayeeEntity.LastName, FundsObject.Table, FundsObject.TransId, FundsObject.PayeeEntity, FundsObject.TransId, -1, -1, true, FundsObject.CtlNumber);
                    //VssExportAsynCall objVss = new VssExportAsynCall(objClaimAdjuster.Context.DbConn.Database, base.Adaptor.userLogin.LoginName, base.Adaptor.userLogin.Password);
                VssExportAsynCall objVss = new VssExportAsynCall(base.Adaptor.userLogin.objRiskmasterDatabase.DataSourceName, base.Adaptor.userLogin.LoginName, base.Adaptor.userLogin.Password,base.ClientId);
                    objVss.AsynchVssReserveExport(objClaimAdjuster.ClaimId, 0, 0, 0, objClaimAdjuster.AdjusterEid, "", "", "", "", "Adjuster");
            }
            #endregion
            #region Add adjuster as Person involved if utility setting is turn on
            //spahariya MITS 28867 - start

            if (objClaim.Context.InternalSettings.SysSettings.AddAdjAsPI)
            {
                bool bIsNotPresent = true;
                string sSql = string.Empty;

                sSql = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE UPPER(PARENT_TABLE_NAME) LIKE 'EVENT' AND PI_EID = " + objClaimAdjuster.AdjusterEid + " AND  PARENT_ROW_ID =" + objClaim.EventId.ToString(); // jira 18932
                using (DbReader objReaderPI = objClaimAdjuster.Context.DbConn.ExecuteReader(sSql))
                {
                    if (objReaderPI.Read())
                        bIsNotPresent = false;
                }
                if (bIsNotPresent) //Adjuster is added/updated  and is not already prsent in person involved.
                {
                    PersonInvolved objPersonInvolved = (PersonInvolved)objClaimAdjuster.Context.Factory.GetDataModelObject("PersonInvolved", false);
                    objPersonInvolved.PiEid = objClaimAdjuster.AdjusterEid;
                    objPersonInvolved.EventId = objClaim.EventId;
                    objPersonInvolved.PiTypeCode = objCache.GetCodeId("O", "PERSON_INV_TYPE");
                    // jira 18932	 start
                    objPersonInvolved.ParentRowId = objClaim.EventId;
                    objPersonInvolved.ParentTableName = "EVENT";
                    objPersonInvolved.RoleTableId = objCache.GetTableId(Globalization.IndividualGlossaryTableNames.ADJUSTERS.ToString());
                    // jira 18932 end
                    objPersonInvolved.Save();

                }
            }
            #endregion
        }
        public override void BeforeSave(ref bool Cancel)
        {
            string sPropertyStore = "";
            if (base.m_fda.HasParam("SysPropertyStore"))
                sPropertyStore = base.m_fda.SafeParam("SysPropertyStore").InnerXml;

            XmlDocument propertyStore = new XmlDocument();
            propertyStore.LoadXml(sPropertyStore);

            XmlNode objNodeTest = propertyStore.SelectSingleNode("//RMUserId/@codeid");
            if (objNodeTest != null)
            {
                this.objClaimAdjuster.AdjusterEntity.RMUserId = Conversion.ConvertStrToInteger(objNodeTest.InnerText);
            }           
            
            base.BeforeSave(ref Cancel);

            // MITS 10004 : Anjaneya : check for validation that multiple adjuster cannot be selected with single RM login 
            bool isSave = true;
            int rmUserId = -1;
            bool isDuplicateAdjuster = default(bool); //mkaran2 ; MITS 36188
            string sSQLString = " SELECT CA.ADJUSTER_EID, E.RM_USER_ID FROM CLAIM_ADJUSTER CA, ENTITY E WHERE CA.CLAIM_ID = " + objClaimAdjuster.ClaimId.ToString() +
                                " AND CA.ADJUSTER_EID = E.ENTITY_ID ";

            if (objNodeTest != null)
            {
                rmUserId = Conversion.ConvertStrToInteger(objNodeTest.InnerText);
            }

            //if (rmUserId != 0)
            //{
            //    using (DbReader claimAdjuster = objClaimAdjuster.Context.DbConn.ExecuteReader(sSQLString))
            //    {
            //        while (claimAdjuster.Read())
            //        {
            //            if (objClaimAdjuster.AdjusterEid != claimAdjuster.GetInt("ADJUSTER_EID") && rmUserId == claimAdjuster.GetInt("RM_USER_ID"))
            //            {
            //                isSave = false;
            //            }
            //        }
            //    }
            //}

            //if (!isSave)
            //{
            //    Cancel = true;
            //    Errors.Add(Globalization.GetString("SaveError"),
            //                "The selected RISKMASTER Login user is already in use. Please select a new one.",
            //                BusinessAdaptorErrorType.Error);
            //}

            //mkaran2 ; MITS 36188;Start
            if (rmUserId != 0 || this.objClaimAdjuster.IsNew)
            {
                using (DbReader claimAdjuster = objClaimAdjuster.Context.DbConn.ExecuteReader(sSQLString))
                {
                    while (claimAdjuster.Read())
                    {
                        if (rmUserId != 0 && objClaimAdjuster.AdjusterEid != claimAdjuster.GetInt("ADJUSTER_EID") && rmUserId == claimAdjuster.GetInt("RM_USER_ID"))
                        {
                            isSave = false;
                        }

                        if (this.objClaimAdjuster.IsNew && objClaimAdjuster.AdjusterEntity.EntityId.Equals(claimAdjuster.GetInt("ADJUSTER_EID")))
                        {
                            isSave = false;
                            isDuplicateAdjuster = true;
                        }
                    }
                }
            }

            if (!isSave)
            {
                Cancel = true;
                if (isDuplicateAdjuster)  
                {
                    Errors.Add(Globalization.GetString("SaveError", base.ClientId),
                            Globalization.GetString("AdjusterForm.ExistingClaimAdjuster.Error", base.ClientId),
                            BusinessAdaptorErrorType.Error);
                }
                else  
                {
                    Errors.Add(Globalization.GetString("SaveError", base.ClientId),
                                Globalization.GetString("AdjusterForm.ExistingLoginUser.Error", base.ClientId),
                                BusinessAdaptorErrorType.Error);
                }
            }
            //mkaran2 ; MITS 36188;End           

            //Mridul 05/28/09 MITS 16745-Chubb ACK Letter Enhancement
            if (isSave && objClaimAdjuster.AdjRowId == 0)
                bTriggerLetter = true;
        }
        public override void OnUpdateObject()
        {
            base.OnUpdateObject();

            //ijha:Mobile Adjuster
            XmlElement objCaller = null;
            DbReader objReader = null;


            objCaller = (XmlElement)base.FormVariables.SelectSingleNode("//caller");
            if (objCaller != null)
            {
                if (objCaller.InnerText == "MobileAdjuster" || objCaller.InnerText == "MobilityAdjuster")
                {
                    string sSQL = "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER='" + base.GetSysExDataNodeText("//ClaimNumber") + "'";

                    objReader = DbFactory.ExecuteReader(this.sConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        objClaimAdjuster.ClaimId = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), base.ClientId);
                        if (base.FormVariables.SelectSingleNode("//SysExData//ClaimId") != null)
                        {
                            base.FormVariables.SelectSingleNode("//SysExData//ClaimId").InnerText = objReader.GetValue("CLAIM_ID").ToString();

                        }
                    }
                }

                // ijha end
            }
            
        }

        //srajindersin 5/22/2014 MITS 20123
        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;

            if ((!Adaptor.userLogin.IsAllowedEx(m_SecurityId + RMO_UPDATE)) && objClaimAdjuster.AdjusterEid > 0)
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    Globalization.GetString("Permission.NoUpdate", base.ClientId), BusinessAdaptorErrorType.Error);

                bError = true;
            }
            Cancel = bError;
        }
	}
}
