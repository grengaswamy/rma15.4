﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Application.FundManagement;
using System.Text;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Plan Screen.
	/// </summary>
	public class PlanForm : DataEntryFormBase
	{
		const string CLASS_NAME = "DisabilityPlan";

		private DisabilityPlan objDisabilityPlan{get{return objData as DisabilityPlan;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}

		private bool killbackbutton=true;

		public PlanForm(FormDataAdaptor fda):base(fda)
		{
			this.m_ClassName = CLASS_NAME;
		}
		
		public override void InitNew()
		{
			base.InitNew();

			//Nikhil Garg		Dated: 20-Jan-2006
			//check whether we need to provide the back button
			//back button means that the user has come here by clikcing the "Open" button on some page.
//			if (base.m_fda.SafeFormVariableParamText("SysCmd")=="0" && base.m_fda.SafeParamText("SysFormId")!="0")
//			{
				string displayBackButton=base.GetSysExDataNodeText("/SysExData/DisplayBackButton");

				if (displayBackButton=="true")
					killbackbutton=false;
//			}
//			else
//				killbackbutton=true;
		}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
            string sName = string.Empty; 
			base.OnUpdateForm ();

            if (this.objDisabilityPlan.PlanName != null)
                sName = " [ " + this.objDisabilityPlan.PlanName.ToString() + " ]";

            if (sName != "")
            {
                ArrayList singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                base.AddElementToList(ref singleRow, "id", "formsubtitle");
                base.AddElementToList(ref singleRow, "Text", sName);
                base.m_ModifiedControls.Add(singleRow);
            }

			if (killbackbutton)
				base.AddKillNode("btnBack");

			//Handle Locked down Toolbar buttons.
			if(!objDisabilityPlan.Context.RMUser.IsAllowedEx(FormBase.RMO_LOOKUP,FormBase.RMO_DISABILITY_PLAN_SEARCH))
				base.AddKillNode("lookup");

			AppendBankAccountList();

            AppendPrefPaymentDay();

			AppendPrefPaySchCodeList();

			//11/07/2006 Raman Bhatia: LTD Module Implementation
			//New Combo Box for Preferred Monthly Day

			AppendPrefMonthlyDayList();

            AppendTypeofMonthlyPayList();
            AppendDaysOfMonth();
			//Raman Bhatia - Acrosoft Phase 1 changes.. Document Management Not available for Employee if Acrosoft is enabled
			bool bUseAcrosoftInterface = objDisabilityPlan.Context.InternalSettings.SysSettings.UseAcrosoftInterface;
            bool bUsePaperVision = objDisabilityPlan.Context.InternalSettings.SysSettings.UsePaperVisionInterface;//Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            //rbhatia4:R8: Use Media View Setting : July 11 2011
            if (bUseAcrosoftInterface || bUsePaperVision) // || objDisabilityPlan.Context.InternalSettings.SysSettings.UseMediaViewInterface)
            {
                base.AddKillNode("attachdocuments");//Mona:PaperVisionMerge:Animesh Inserted MITS 16697
                base.AddKillNode("attach");
            }

            
		}

		public override void OnUpdateObject()
		{
			base.OnUpdateObject ();

            //pmittal5 Mits 16223 05/05/09 - Handling Sub Bank Account case
            int iSubAccountID = 0;
            string sSQL = "";
            DbReader objReader = null;

            if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
            {
                iSubAccountID = objDisabilityPlan.SubAccRowId;
                if (iSubAccountID == 0)
                {
                    objDisabilityPlan.BankAccId = 0;
                }
                sSQL = "SELECT ACCOUNT_ID FROM BANK_ACC_SUB WHERE SUB_ROW_ID=" + iSubAccountID.ToString();
                using (objReader = objDisabilityPlan.Context.DbConnLookup.ExecuteReader(sSQL))
                {

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            objDisabilityPlan.BankAccId = Conversion.ConvertObjToInt(objReader.GetValue("ACCOUNT_ID"), base.ClientId);
                        }

                    }
                }

            }
            //End - pmittal5

			if((objDisabilityPlan.Context.InternalSettings.CacheFunctions.GetShortCode(objDisabilityPlan.PrefPaySchCode)).ToUpper()=="PM")
			{
				if(objDisabilityPlan.ClassList.Count > 0)
				{
					//kdb 04/05/2005 Change the work period option if the preferred schedule is modified
					foreach (DisabilityClass objDisabilityClass in objDisabilityPlan.ClassList)
					{
						if ((objDisabilityClass.ActualWorkWeek != false) || (objDisabilityClass.Day5WorkWeek != false) || (objDisabilityClass.Day7WorkWeek != false)) 
						{
							objDisabilityClass.ActualMonth = true;
							objDisabilityClass.ActualWorkWeek = false;
							objDisabilityClass.Day5WorkWeek = false;
							objDisabilityClass.Day7WorkWeek = false;
						}
					} // foreach
				} // if

			} // if
			else if(((objDisabilityPlan.Context.InternalSettings.CacheFunctions.GetShortCode(objDisabilityPlan.PrefPaySchCode)).ToUpper()=="PB")
				|| ((objDisabilityPlan.Context.InternalSettings.CacheFunctions.GetShortCode(objDisabilityPlan.PrefPaySchCode)).ToUpper()=="PW"))
			{
				if(objDisabilityPlan.ClassList.Count > 0)
				{
					foreach (DisabilityClass objDisabilityClass in objDisabilityPlan.ClassList)
					{
						//kdb 04/05/2005 Change the work period option if the preferred schedule is modified
						if ((objDisabilityClass.ActualMonth != false) || (objDisabilityClass.Day30WorkMonth != false)) 
						{
							objDisabilityClass.ActualMonth = false;
							objDisabilityClass.ActualWorkWeek = true;
							objDisabilityClass.Day30WorkMonth = false;
						}
					} // foreach
				} // if
			} // else


		}

		public override void BeforeSave(ref bool Cancel)
		{
			base.BeforeSave (ref Cancel);
			
            //if (objDisabilityPlan.Context.InternalSettings.SysSettings.UseFundsSubAcc)
            //{
            //    objDisabilityPlan.SubAccRowId=objDisabilityPlan.BankAccId;
            //    objDisabilityPlan.BankAccId=0;
            //}
            //else
            //    objDisabilityPlan.SubAccRowId=0;
            //Start by Shivendu for MITS 11683
            if (objDisabilityPlan.EffectiveDate.CompareTo(objDisabilityPlan.ExpirationDate) > 0)
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), "Expiration Date ", "Effective Date "),
                    BusinessAdaptorErrorType.Error);

                Cancel = true;
                return;
            }
            //End by Shivendu for MITS 11683
		}

		private void AppendBankAccountList()
		{
			//Variable declarations
			ArrayList arrAcctList = new ArrayList();
			XmlDocument objXML = base.SysEx;
			XmlNode objOld=null;
			XmlElement objNew=null;
			XmlCDataSection objCData=null;

			//Retrieve the security credential information to pass to the FundManager object
			string sDSNName = base.m_fda.userLogin.objRiskmasterDatabase.DataSourceName;
			string sUserName = base.m_fda.userLogin.LoginName;
			string sPassword = base.m_fda.userLogin.Password;
			int iUserId = base.m_fda.userLogin.UserId;
			int iGroupId = base.m_fda.userLogin.GroupId;

			//Create an instance of the FundManager
            using (FundManager objFundMgr = new FundManager(sDSNName, sUserName, sPassword, iUserId, iGroupId, base.ClientId))
            {
                arrAcctList = objFundMgr.GetAccounts(objDisabilityPlan.Context.InternalSettings.SysSettings.UseFundsSubAcc);
            }

			//Create the necessary SysExData to be used in ref binding
			objOld = objXML.SelectSingleNode("//AccountList");
			objNew = objXML.CreateElement("AccountList");

            // Start Naresh MITS 10009 Blank Value Added in Combo
            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;

            // Blank value for the combo box
            xmlOption = objXML.CreateElement("option");
            xmlOptionAttrib = objXML.CreateAttribute("value");
            xmlOption.Attributes.Append(xmlOptionAttrib);
            objNew.AppendChild(xmlOption);
            xmlOptionAttrib = null;
            xmlOption = null;
            // End Naresh Blank Value Added in Combo

			//Loop through and create all the option values for the combobox control
			foreach (FundManager.AccountDetail item in arrAcctList)
			{
				xmlOption = objXML.CreateElement("option");
				objCData = objXML.CreateCDataSection(item.AccountName);
				xmlOption.AppendChild(objCData);
				xmlOptionAttrib = objXML.CreateAttribute("value");
              //pmittal5 Mits 16223 05/05/09 - Handling Sub Bank Account case
                if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                    xmlOptionAttrib.Value = item.SubRowId.ToString();
                else
                    xmlOptionAttrib.Value = item.AccountId.ToString();
              //End- pmittal5
				xmlOption.Attributes.Append(xmlOptionAttrib);
				objNew.AppendChild(xmlOption);
			}//end foreach

			//Add the XML to the SysExData 
			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);

            //pmittal5 Mits 16223 05/05/09 - Changing "ref" of Bank Account if Use Sub Account is checked
            objOld = objXML.SelectSingleNode("//ControlAppendAttributeList");
            objNew = objXML.CreateElement("ControlAppendAttributeList");
            XmlElement objElem = null;
            XmlElement objChild = null;

            objElem = objXML.CreateElement("bankaccount");
            objChild = objXML.CreateElement("ref");
            if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                objChild.SetAttribute("value", "/Instance/DisabilityPlan/SubAccRowId");
            else
                objChild.SetAttribute("value", "/Instance/DisabilityPlan/BankAccId");

            objElem.AppendChild(objChild); ;
            objNew.AppendChild(objElem);

            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);
            //End - pmittal5

			//Clean up
			arrAcctList = null;
			objOld = null;
			objNew = null;
			objCData = null;
		}

		private void AppendPrefPaySchCodeList()
		{
			string sSQL=string.Empty;
			XmlDocument objXML = base.SysEx;
			XmlNode objOld=null;
			XmlElement objNew=null;
			XmlCDataSection objCData=null;
			DbReader objReader=null;
			XmlElement xmlOption=null;
			XmlAttribute xmlOptionAttrib=null;

			//Create the necessary SysExData to be used in ref binding
			objOld = objXML.SelectSingleNode("//PrefPaySchCode");
			objNew = objXML.CreateElement("PrefPaySchCode");			
			if(objDisabilityPlan.Context.DbConn.DatabaseType==eDatabaseType.DBMS_IS_ORACLE)
			{
				sSQL =	"SELECT CODES.CODE_ID, CODES.SHORT_CODE || ' - ' || CODES_TEXT.CODE_DESC" ;
			}
			else
			{
				sSQL =	"SELECT CODES.CODE_ID, CODES.SHORT_CODE + ' - ' + CODES_TEXT.CODE_DESC" ;
			}
				sSQL = sSQL +
					" FROM CODES, CODES_TEXT, GLOSSARY" +
					" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID" +
					" AND CODES.TABLE_ID = GLOSSARY.TABLE_ID" +
					" AND GLOSSARY.SYSTEM_TABLE_NAME = 'PERIOD_TYPES'" +
					" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)" +
			//11/07/2006 Raman Bhatia: LTD Module Implementation
			//Add "Monthly" Option
			//		" AND CODES.SHORT_CODE IN ('PW','PB')";
					" AND CODES.SHORT_CODE IN ('PW','PB','PM')";

                using (objReader = objDisabilityPlan.Context.DbConnLookup.ExecuteReader(sSQL))
                {

                    //Raman Bhatia: rmworld does not let this field be empty

                    /*
                    //create one empty element
                    xmlOption = objXML.CreateElement("option");
                    xmlOption.InnerText=string.Empty;
                    xmlOptionAttrib = objXML.CreateAttribute("value");
                    xmlOptionAttrib.Value = "0";
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    objNew.AppendChild(xmlOption);
                    */

                    if (objReader != null)
                    {
                        //Loop through and create all the option values for the combobox control
                        while (objReader.Read())
                        {
                            xmlOption = objXML.CreateElement("option");
                            objCData = objXML.CreateCDataSection(Conversion.ConvertObjToStr(objReader[1]));
                            xmlOption.AppendChild(objCData);
                            xmlOptionAttrib = objXML.CreateAttribute("value");
                            xmlOptionAttrib.Value = Conversion.ConvertObjToInt(objReader[0], base.ClientId).ToString();
                            xmlOption.Attributes.Append(xmlOptionAttrib);
                            objNew.AppendChild(xmlOption);
                        }//end foreach

                        //objReader.Close();
                        //objReader = null;
                    }
                }
			//Add the XML to the SysExData 
			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);
			
		}
        //MITS 13371 : Umesh
        private void AppendPrefPaymentDay()
        {
            string sSQL = string.Empty;
            XmlDocument objXML = base.SysEx;
            XmlNode objOld = null;
            XmlElement objNew = null;
            XmlCDataSection objCData = null;
            DbReader objReader = null;
            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;

            //Create the necessary SysExData to be used in ref binding
            objOld = objXML.SelectSingleNode("//PrefDayCode");
            objNew = objXML.CreateElement("PrefDayCode");
            if (objDisabilityPlan.Context.DbConn.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
            {
                sSQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE || ' - ' || CODES_TEXT.CODE_DESC";
            }
            else
            {
                sSQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE + ' - ' + CODES_TEXT.CODE_DESC";
            }
            sSQL = sSQL +
                " FROM CODES, CODES_TEXT, GLOSSARY" +
                " WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID" +
                " AND CODES.TABLE_ID = GLOSSARY.TABLE_ID" +
                " AND GLOSSARY.SYSTEM_TABLE_NAME = 'PREF_PAYMENT_DAY'" +
                " AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)";
               

            using (objReader = objDisabilityPlan.Context.DbConnLookup.ExecuteReader(sSQL))
            {

                if (objReader != null)
                {
                    //Loop through and create all the option values for the combobox control
                    while (objReader.Read())
                    {
                        xmlOption = objXML.CreateElement("option");
                        objCData = objXML.CreateCDataSection(Conversion.ConvertObjToStr(objReader[1]));
                        xmlOption.AppendChild(objCData);
                        xmlOptionAttrib = objXML.CreateAttribute("value");
                        xmlOptionAttrib.Value = Conversion.ConvertObjToInt(objReader[0], base.ClientId).ToString();
                        xmlOption.Attributes.Append(xmlOptionAttrib);
                        objNew.AppendChild(xmlOption);
                    }//end foreach

                    //objReader.Close();
                    //objReader = null;
                }
            }
            //Add the XML to the SysExData 
            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);

        }
        //MITS 13371 : End

		private void AppendPrefMonthlyDayList()
		{
			string sSQL=string.Empty;
			XmlDocument objXML = base.SysEx;
			XmlNode objOld=null;
			XmlElement objNew=null;
			XmlCDataSection objCData=null;
			DbReader objReader=null;
			XmlElement xmlOption=null;
			XmlAttribute xmlOptionAttrib=null;

			//Create the necessary SysExData to be used in ref binding
			objOld = objXML.SelectSingleNode("//PrefDayOfMCode");
			objNew = objXML.CreateElement("PrefDayOfMCode");			
			if(objDisabilityPlan.Context.DbConn.DatabaseType==eDatabaseType.DBMS_IS_ORACLE)
			{
				sSQL =	"SELECT CODES.CODE_ID, CODES.SHORT_CODE || ' - ' || CODES_TEXT.CODE_DESC" ;
			}
			else
			{
				sSQL =	"SELECT CODES.CODE_ID, CODES.SHORT_CODE + ' - ' + CODES_TEXT.CODE_DESC" ;
			}
			sSQL = sSQL +
				" FROM CODES, CODES_TEXT, GLOSSARY" +
				" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID" +
				" AND CODES.TABLE_ID = GLOSSARY.TABLE_ID" +
				" AND GLOSSARY.SYSTEM_TABLE_NAME = 'DAY_OF_MONTH'" +
				" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)" +
				" AND CODES.SHORT_CODE IN ('1','2','3','4')";

            using (objReader = objDisabilityPlan.Context.DbConnLookup.ExecuteReader(sSQL))
            {


                //create one empty element
                //MITS 13371 : Umesh
                //xmlOption = objXML.CreateElement("option");
                //xmlOption.InnerText = string.Empty;
                //xmlOptionAttrib = objXML.CreateAttribute("value");
                //xmlOptionAttrib.Value = "0";
                //xmlOption.Attributes.Append(xmlOptionAttrib);
                //objNew.AppendChild(xmlOption);
                //MITS 13371 : End

                if (objReader != null)
                {
                    //Loop through and create all the option values for the combobox control
                    while (objReader.Read())
                    {
                        xmlOption = objXML.CreateElement("option");
                        objCData = objXML.CreateCDataSection(Conversion.ConvertObjToStr(objReader[1]));
                        xmlOption.AppendChild(objCData);
                        xmlOptionAttrib = objXML.CreateAttribute("value");
                        xmlOptionAttrib.Value = Conversion.ConvertObjToInt(objReader[0], base.ClientId).ToString();
                        xmlOption.Attributes.Append(xmlOptionAttrib);
                        objNew.AppendChild(xmlOption);
                    }//end foreach

                    //objReader.Close();
                    //objReader = null;
                }
            }
			//Add the XML to the SysExData 
			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);
			
		}
        //MITS 14889 : Umesh 
        private void AppendTypeofMonthlyPayList()
        {
            string sSQL = string.Empty;
            XmlDocument objXML = base.SysEx;
            XmlNode objOld = null;
            XmlElement objNew = null;
            XmlCDataSection objCData = null;
            DbReader objReader = null;
            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;

            //Create the necessary SysExData to be used in ref binding
            objOld = objXML.SelectSingleNode("//TypeofMonthlyPay");
            objNew = objXML.CreateElement("TypeofMonthlyPay");
            if (objDisabilityPlan.Context.DbConn.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
            {
                sSQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE || ' - ' || CODES_TEXT.CODE_DESC";
            }
            else
            {
                sSQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE + ' - ' + CODES_TEXT.CODE_DESC";
            }
            sSQL = sSQL +
                " FROM CODES, CODES_TEXT, GLOSSARY" +
                " WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID" +
                " AND CODES.TABLE_ID = GLOSSARY.TABLE_ID" +
                " AND CODES_TEXT.LANGUAGE_CODE = 1033" +
                " AND GLOSSARY.SYSTEM_TABLE_NAME = 'MONTHLY_PAY_TYPE'" +
                " AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)";
            //Aman ML Change
            StringBuilder sbSQL = new StringBuilder();
            sbSQL = CommonFunctions.PrepareMultilingualQuery(sSQL, "MONTHLY_PAY_TYPE", base.Adaptor.userLogin.objUser.NlsCode);

            using (objReader = objDisabilityPlan.Context.DbConnLookup.ExecuteReader(sbSQL.ToString()))   //Aman ML Change
            {



                xmlOption = objXML.CreateElement("option");
                xmlOption.InnerText = string.Empty;
                xmlOptionAttrib = objXML.CreateAttribute("value");
                xmlOptionAttrib.Value = "0";
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
               

                if (objReader != null)
                {
                    //Loop through and create all the option values for the combobox control
                    while (objReader.Read())
                    {
                        xmlOption = objXML.CreateElement("option");
                        objCData = objXML.CreateCDataSection(Conversion.ConvertObjToStr(objReader[1]));
                        xmlOption.AppendChild(objCData);
                        xmlOptionAttrib = objXML.CreateAttribute("value");
                        xmlOptionAttrib.Value = Conversion.ConvertObjToInt(objReader[0], base.ClientId).ToString();
                        xmlOption.Attributes.Append(xmlOptionAttrib);
                        objNew.AppendChild(xmlOption);
                    }//end foreach

                    //objReader.Close();
                    //objReader = null;
                }
            }
            //Add the XML to the SysExData 
            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);

        }        
        private void AppendDaysOfMonth()
        {
            //string sSQL = string.Empty;
            XmlDocument objXML = base.SysEx;
            XmlNode objOld = null;
            XmlElement objNew = null;
            XmlCDataSection objCData = null;
            DbReader objReader = null;
            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;
            const int MAX_DAYS_IN_MONTH = 31;

            //Create the necessary SysExData to be used in ref binding
            objOld = objXML.SelectSingleNode("//PrefDate");
            objNew = objXML.CreateElement("PrefDate");





            //Loop through and create all the option values for the combobox control
            for (int icount = 1; icount <= MAX_DAYS_IN_MONTH; icount++)
            {
                xmlOption = objXML.CreateElement("option");
                objCData = objXML.CreateCDataSection(icount.ToString());
                xmlOption.AppendChild(objCData);
                xmlOptionAttrib = objXML.CreateAttribute("value");
                xmlOptionAttrib.Value = icount.ToString();
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
            }//end foreach



            //Add the XML to the SysExData 
            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);
        }
	}
}
