﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Application.PolicySystemInterface;

namespace Riskmaster.BusinessAdaptor
{
   public class CommonForm : DataEntryFormBase
    {
        //skhare7 Point Policy Interface
       enum PolicyFieldsData
       { symbol, policynumber, mastercompany, locationcompany, module, insuredname, issuecode }
       //skhare7 Point Policy Interface End
        public const string INNER_PHONE_DELIMITER = "@~";
        public const string OUTER_PHONE_DELIMITER = "@@";
        public const string MARK_PHONECODE = "<<";


        public CommonForm(FormDataAdaptor fda): base(fda)
		{
			
		}
        public override void InitNew()
        {
            base.InitNew();

        }
        public static void DisplayPhoneNumbersRecordSummary(XmlDocument objSysExData,XmlDocument objViewData)
        {

            XmlNode objPhoneNode = objSysExData.SelectSingleNode("//PhoneTypeList");
            XmlNode objPhoneNumber = objSysExData.SelectSingleNode("//PhoneNumbers");
            if (objPhoneNode != null && objPhoneNumber != null)
            {
                string sPhoneData = objPhoneNumber.InnerText;
                string[] sPhoneNumbers = sPhoneData.Split(new string[] { OUTER_PHONE_DELIMITER }, StringSplitOptions.None);
                StringBuilder sXml = new StringBuilder("<displaycolumn name='phonenumbers'>");
                foreach (string sPhoneNumber in sPhoneNumbers)
                {
                    string[] sNumber = sPhoneNumber.Split(new string[] { INNER_PHONE_DELIMITER }, StringSplitOptions.None);
                    if (sNumber.Length > 2)
                    {
                        string sPhoneId = sNumber[0];
                        string sPhoneType = sNumber[1];
                        string sPhoneNo = sNumber[2];
                        foreach (XmlNode objChildnode in objPhoneNode.ChildNodes)
                        {
                            if (sPhoneType == objChildnode.Attributes["value"].Value)
                            {
                                String sPhoneValue = objChildnode.InnerText;
                                if (sPhoneValue.IndexOf(MARK_PHONECODE) != -1)
                                {
                                    sPhoneValue = sPhoneValue.Replace(MARK_PHONECODE, "");
                                }
                                sXml = sXml.AppendFormat("<control name='{0}' type='phone' title='{0} Phone' ref='/Instance/UI/FormVariables/SysExData/{0}' />", sPhoneValue.Trim());
                                XmlNode phonenode = objSysExData.CreateElement(sPhoneValue.Trim());
                                phonenode.InnerText = sPhoneNo;
                                if (objSysExData.SelectSingleNode(sPhoneValue.Trim()) == null)
                                    objSysExData.DocumentElement.AppendChild(phonenode);
                                break;
                            }
                        }

                    }
                }
                sXml = sXml.Append("</displaycolumn>");
                System.Xml.Linq.XElement oElement = System.Xml.Linq.XElement.Parse(sXml.ToString());
                XmlNode objView = objViewData.SelectSingleNode("//form/group[@selected='1']");
                if (objView != null)
                {
                    if (objView.SelectSingleNode("displaycolumn[@name='phonenumbers']") == null)
                        objView.InnerXml += oElement;

                    //Removing Phone Nodes and Types coming within the Xml
                    XmlNode objPhoneNumber1 = objSysExData.SelectSingleNode("//PhoneNumber1");
                    if (objPhoneNumber1 != null)
                    {
                        objSysExData.DocumentElement.RemoveChild(objPhoneNumber1);
                        XmlNode objControlNode = objView.SelectSingleNode("//displaycolumn/control[@name='phonenumber1']");
                        if (objControlNode != null)
                        {
                            objControlNode.ParentNode.RemoveChild(objControlNode);
                        }
                    }
                    XmlNode objPhoneNumber2 = objSysExData.SelectSingleNode("//PhoneNumber2");
                    if (objPhoneNumber2 != null)
                    {
                        objSysExData.DocumentElement.RemoveChild(objPhoneNumber2);
                        XmlNode objControlNode = objView.SelectSingleNode("//displaycolumn/control[@name='phonenumber2']");
                        if (objControlNode != null)
                        {
                            objControlNode.ParentNode.RemoveChild(objControlNode);
                        }
                    }
                    XmlNode objPhoneType1 = objSysExData.SelectSingleNode("//PhoneType1");
                    if (objPhoneType1 != null)
                    {
                        objSysExData.DocumentElement.RemoveChild(objPhoneType1);
                        XmlNode objControlNode = objView.SelectSingleNode("//displaycolumn/control[@name='phonetype1']");
                        if (objControlNode != null)
                        {
                            objControlNode.ParentNode.RemoveChild(objControlNode);
                        }
                    }
                    XmlNode objPhoneType2 = objSysExData.SelectSingleNode("//PhoneType2");
                    if (objPhoneType2 != null)
                    {
                        objSysExData.DocumentElement.RemoveChild(objPhoneType2);
                        XmlNode objControlNode = objView.SelectSingleNode("//displaycolumn/control[@name='phonetype2']");
                        if (objControlNode != null)
                        {
                            objControlNode.ParentNode.RemoveChild(objControlNode);
                        }
                    }
                    //rjhamb Mits 21591:When generating the record summary there are two fields "Phone 1" and "Phone 2" which always shows " Unknown Control Type "
                    XmlNode objPhone1 = objView.SelectSingleNode("//displaycolumn/control[@name='phone1']");
                    if(objPhone1==null)
                        objPhone1 = objView.SelectSingleNode("//displaycolumn/control[@name='patphone1']"); //On Patient Screen
                    if (objPhone1 != null)
                    {
                        objPhone1.ParentNode.RemoveChild(objPhone1);
                    }
                    XmlNode objPhone2 = objView.SelectSingleNode("//displaycolumn/control[@name='phone2']");
                    if(objPhone2==null)
                        objPhone2 = objView.SelectSingleNode("//displaycolumn/control[@name='patphone2']"); //On Patient Screen
                    if (objPhone2 != null)
                    {
                        objPhone2.ParentNode.RemoveChild(objPhone2);
                    }
                    //rjhamb Mits 21591:When generating the record summary there are two fields "Phone 1" and "Phone 2" which always shows " Unknown Control Type "
                }
                
                //Removing Phone Nodes and Types coming within the Xml
            }
        }
        public static void SetPhoneNumbers(XmlNode objPhoneNumber, XmlNode objPhoneTypeList)
        {

            string sPhoneData = objPhoneNumber.InnerText;
            string[] sPhoneNumbers = sPhoneData.Split(new string[] { OUTER_PHONE_DELIMITER }, StringSplitOptions.None);
            foreach (string sPhoneNumber in sPhoneNumbers)
            {
                string[] sNumber = sPhoneNumber.Split(new string[] { INNER_PHONE_DELIMITER }, StringSplitOptions.None);
                if (sNumber.Length > 2)
                {
                    string sPhoneId = sNumber[0];
                    string sPhoneType = sNumber[1];
                    string sPhoneNo = sNumber[2];

                    //MarkPhoneType(sPhoneType, sPhoneNo);
                    foreach (XmlNode node in objPhoneTypeList.ChildNodes)
                    {
                        if (sPhoneType == node.Attributes["value"].Value && sPhoneNo.Trim() != string.Empty)
                        {
                            node.InnerText += " " + MARK_PHONECODE;
                            break;
                        }
                    }

                }
            }

        }
        public static XmlDocument PopulatePhoneTypes(string sConnectionString, ref int iOfficeType, ref int iHomeType, int iLangCode)
        {

            XmlDocument SysExDoc = new XmlDocument();
            XmlElement xmlRootElement = null;
            //Aman ML Change
//            string sSQL = @"SELECT C.CODE_ID,C.SHORT_CODE,CT.CODE_DESC FROM CODES C,CODES_TEXT CT
//                            WHERE C.CODE_ID=CT.CODE_ID
//                            AND C.TABLE_ID=(SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='PHONES_CODES')
//                            ORDER BY C.CODE_ID";

            string sSQL = @"SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT
                            WHERE CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = 1033
                            AND CODES.TABLE_ID=(SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='PHONES_CODES')
                            ORDER BY CODES.CODE_ID";
            StringBuilder sbSQL = new StringBuilder();
            sbSQL = CommonFunctions.PrepareMultilingualQuery(sSQL, "PHONES_CODES", iLangCode);  
            xmlRootElement = SysExDoc.CreateElement("PhoneTypeList");
            SysExDoc.AppendChild(xmlRootElement);
            using (DbReader rdr = DbFactory.GetDbReader(sConnectionString, sbSQL.ToString()))  //Aman ML Change
            {
                //Loop through and create all the option values for the combobox control
                while (rdr.Read())
                {
                        XmlElement xmlOption = SysExDoc.CreateElement("option");
                        string sCodeDesc = rdr.GetString("CODE_DESC");
                        //int iCodeId = rdr.GetInt("CODE_ID");
                        int iCodeId = rdr.GetInt(0); //Aman ML Change as it does exist in the order by query
                        if (String.Compare(sCodeDesc.Trim(),"office",true)==0)
                        {
                            iOfficeType = iCodeId;

                        }
                        else if (String.Compare(sCodeDesc.Trim(),"home",true)==0)
                        {
                            iHomeType = iCodeId;
                        }
                        xmlOption.InnerText = sCodeDesc;
                        XmlAttribute xmlOptionAttrib = SysExDoc.CreateAttribute("value");
                        xmlOptionAttrib.Value = iCodeId.ToString();
                        xmlOption.Attributes.Append(xmlOptionAttrib);
                        xmlRootElement.AppendChild(xmlOption);
                }
            }

            return SysExDoc;
        }
        public static string PointPolicyDataToDisplay(string sConnectionString,int p_iPolicyId,int p_iClientId)
    {
            string sXML=string.Empty;
            string sNode=string.Empty;
            string  sDataTodisplay=string.Empty;
            bool bItemFlag=false;
            LocalCache objCache = null;
            PolicySystemInterface objPolicyInterface = new PolicySystemInterface(sConnectionString, p_iClientId);
            string sData = string.Empty;
            sXML = objPolicyInterface.GetDownLoadXMLData("POLICY", p_iPolicyId, p_iPolicyId);
            objCache = new LocalCache(sConnectionString, p_iClientId);
            string sPolicyFields = objCache.GetPolicyInterfaceConfigSettings("PolicyInterface", "PolicyDownloadDataToDisplay");
   
        string[] values = Enum.GetNames(typeof(PolicyFieldsData));
      
        string[] arrValues = sPolicyFields.Split(',');
           
            for (int i = 0; i <= arrValues.Length-1; i++)
            {
                 
                    if (arrValues[i] == values[0])//symbol
                    {
                        sNode = "CompanyProductCd";
                        bItemFlag = false; ;
                        sDataTodisplay = objPolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", p_iPolicyId, p_iPolicyId, sNode, bItemFlag, sXML);
                    }
                    else  if (arrValues[i] == values[1])//policynumber
                    {
                        sNode = "PolicyNumber";
                        bItemFlag = false;
                        sData = objPolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", p_iPolicyId, p_iPolicyId, sNode, bItemFlag, sXML);
                       
                            if(sData!=string.Empty)
                            {
                                if (sDataTodisplay != string.Empty)
                                    sDataTodisplay = sDataTodisplay + "|" + sData;
                                else
                                    sDataTodisplay = sData;
                           
                            }
                       

                    }
                    else if (arrValues[i] == values[2])//mastercompany
                    {
                        sNode = "com.csc_MasterCompany";
                        bItemFlag = true;
                        sData = objPolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", p_iPolicyId, p_iPolicyId, sNode, bItemFlag, sXML);

                        if (sData != string.Empty)
                        {
                            if (sDataTodisplay != string.Empty)
                                sDataTodisplay = sDataTodisplay + "|" + sData;
                            else
                                sDataTodisplay = sData;

                        }
                       
                    }
                    else if (arrValues[i] == values[3])//locationcompany
                    {
                        sNode = "com.csc_LocCompany";
                        bItemFlag = true;
                        sData = objPolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", p_iPolicyId, p_iPolicyId, sNode, bItemFlag, sXML);

                        if (sData != string.Empty)
                        {
                            if (sDataTodisplay != string.Empty)
                                sDataTodisplay = sDataTodisplay + "|" + sData;
                            else
                                sDataTodisplay = sData;

                        }
                       
                    }
                    else if (arrValues[i] == values[4])//module
                    {
                        sNode = "com.csc_Module";
                        bItemFlag = true;
                        sData = objPolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", p_iPolicyId, p_iPolicyId, sNode, bItemFlag, sXML);

                        if (sData != string.Empty)
                        {
                            if (sDataTodisplay != string.Empty)
                                sDataTodisplay = sDataTodisplay + "|" + sData;
                            else
                                sDataTodisplay = sData;

                        }
                       
                    }
                    else if (arrValues[i] == values[5])//insuredname
                    {
                        sNode = "CommercialName";
                        bItemFlag = false;
                        sData = objPolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", p_iPolicyId, p_iPolicyId, sNode, bItemFlag, sXML);

                        if (sData != string.Empty)
                        {
                            if (sDataTodisplay != string.Empty)
                                sDataTodisplay = sDataTodisplay + "|" + sData;
                            else
                                sDataTodisplay = sData;

                        }
                       
                       
                    }
                    else if (arrValues[i] == values[6])//issuecode
                    {
                        sNode = " com.csc_IssueCodeDesc";
                        bItemFlag = false;
                        sData = objPolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", p_iPolicyId, p_iPolicyId, sNode, bItemFlag, sXML);

                        if (sData != string.Empty)
                        {
                            if (sDataTodisplay != string.Empty)
                                sDataTodisplay = sDataTodisplay + "|" + sData;
                            else
                                sDataTodisplay = sData;

                        }
                       
                       
                      
                    }
                
        }
            objCache = null;
            return sDataTodisplay;
    }


        
    }
}
