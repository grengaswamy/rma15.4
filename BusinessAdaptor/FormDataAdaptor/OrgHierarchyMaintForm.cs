/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 02/11/2014 | 34276  | anavinkumars   | Add Contact Type field in Contacts info tab 
 * 02/10/2014 | 34276  | abhadouria   | changes req for Entity Address Type
 * 02/11/2014 | 34276  | anavinkumars   | Enhancement to Entity related functionality in RMA 
 * 02/23/2015 | RMA-6865  | nshah28   | Swiss Re - Ability to set Effective Date_or_Expiration Date on Address
 **********************************************************************************************/
using System;
using System.Data;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using System.Collections.Generic;
using System.Text; 

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for OrgHierarchyMaint Screen.
	/// </summary>
	public class OrgHierarchyMaintForm : DataEntryFormBase
	{
		const string CLASS_NAME = "OrgHierarchy";
		private OrgHierarchy Entity {get{return objData as OrgHierarchy;}}
        //Added Rakhi for R7:Add Emp Data Elements
        string sOfficePhone = string.Empty;
        string sHomePhone = string.Empty;
        int iHomeType = 0;
        int iOfficeType = 0;
        //Added Rakhi for R7:Add Emp Data Elements
        private ArrayList arDeptList = null;//Deb
		private LocalCache objCache{get{return objData.Context.LocalCache;}}

		// TODO - Remove this hardcoding, if possible
		const string INSTANCE_SYSEXDATA_PATH = "/Instance/UI/FormVariables/SysExData";

		private bool killbackbutton=true;

		public OrgHierarchyMaintForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			XmlDocument objXmlDoc=null;
			base.OnUpdateForm ();
            //MGaba2:MITS 29068
            if (Entity.Context.InternalSettings.SysSettings.UseMultiCurrency == 0)
            {
                base.AddKillNode("currencytypetext");
            }
            //Added Rakhi for R7:Add Emp Data Elements
            XmlNode objPhoneTypeList = base.SysEx.SelectSingleNode("//PhoneTypeList");
            XmlNode objPhoneNumbers = base.SysEx.SelectSingleNode("//PhoneNumbers");

            objXmlDoc = new XmlDocument();
            objXmlDoc = CommonForm.PopulatePhoneTypes(Entity.Context.DbConn.ConnectionString, ref iOfficeType, ref iHomeType,base.Adaptor.userLogin.objUser.NlsCode); //Aman ML Change
            if (objPhoneTypeList != null)
                base.SysEx.DocumentElement.RemoveChild(objPhoneTypeList);
            base.SysEx.DocumentElement.AppendChild(base.SysEx.ImportNode(objXmlDoc.SelectSingleNode("PhoneTypeList"), true));

            if (objPhoneNumbers == null)
            {
                string sPhoneNumbers = PopulatePhoneNumbers();
                base.ResetSysExData("PhoneNumbers", sPhoneNumbers);
            }
            else
            {
                objPhoneTypeList = base.SysEx.SelectSingleNode("//PhoneTypeList");
                if (objPhoneTypeList != null)
                    CommonForm.SetPhoneNumbers(objPhoneNumbers, objPhoneTypeList);

            }
            if (base.SysEx.SelectSingleNode("//PhoneType1") == null)
            {
                base.ResetSysExData("PhoneType1", iOfficeType.ToString());
            }
            if (base.SysEx.SelectSingleNode("//PhoneNumber1") == null)
            {
                base.ResetSysExData("PhoneNumber1", sOfficePhone);
            }
            if (base.SysEx.SelectSingleNode("//PhoneType2") == null)
            {
                base.ResetSysExData("PhoneType2", iHomeType.ToString());
            }
            if (base.SysEx.SelectSingleNode("//PhoneNumber2") == null)
            {
                base.ResetSysExData("PhoneNumber2", sHomePhone);
            }
            //Added Rakhi for R7:Add Emp Data Elements
            //Anurag - Added for missing tabs contact info & Operating as on entity maintainenance screen
			// Operating As Grid Data
			XmlDocument objEntityXOperatingAsXmlDoc = GetEntityXOperatingAsData();
			
			XmlNode objOldEntityXOperatingAsNode = SysEx.DocumentElement.SelectSingleNode(objEntityXOperatingAsXmlDoc.DocumentElement.LocalName);
			if(objOldEntityXOperatingAsNode!=null)
			{
				SysEx.DocumentElement.RemoveChild(objOldEntityXOperatingAsNode);  
			}
			XmlNode objEntityXOperatingAsNode = 
				SysEx.ImportNode(objEntityXOperatingAsXmlDoc.DocumentElement ,true);
			SysEx.DocumentElement.AppendChild(objEntityXOperatingAsNode);  	

			if(SysEx.DocumentElement.SelectSingleNode("IsPostBack") == null)
				CreateSysExData("IsPostBack"); 
            //sgoel6 Medicare 05/14/2009
            if (SysEx.DocumentElement.SelectSingleNode("MMSEAEditFlag") == null)
                CreateSysExData("MMSEAEditFlag");
            // Animesh Inserted RMSC BulkCheck Release  //skhare7 RMSC Merge 28397
            if (SysEx.DocumentElement.SelectSingleNode("BillReviewFlag") == null)
                CreateSysExData("BillReviewFlag");
            if (SysEx.DocumentElement.SelectSingleNode("BillReviewOrgLevel") == null)
                CreateSysExData("BillReviewOrgLevel");
            bool bUseBillReview = Entity.Context.InternalSettings.SysSettings.UseBillReview;
            if (bUseBillReview)
                base.ResetSysExData("BillReviewFlag", "true");
            else
                base.ResetSysExData("BillReviewFlag", "false");
            int iBillReviewOrgLevel = Entity.Context.InternalSettings.SysSettings.OrgBillLevel;
            base.ResetSysExData("BillReviewOrgLevel", iBillReviewOrgLevel.ToString());
            //Animesh Insertion Ends //skhare7 RMSC merge 
            //skhare7 MITS 28760
            if (SysEx.DocumentElement.SelectSingleNode("BillReviewOrgLevelEid") == null)
                CreateSysExData("BillReviewOrgLevelEid");
            int iBillReviewOrgEId = Entity.Context.InternalSettings.SysSettings.OrgBillLevelEntityId;
            base.ResetSysExData("BillReviewOrgLevelEid", iBillReviewOrgEId.ToString());
            //skhare7 MITS 28760 End
           //Parijat:Mits 9938
            if (SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag") != null && SysEx.DocumentElement.SelectSingleNode("EntityXContactInfoGrid_RowDeletedFlag") != null)
            {
                if ((SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag").InnerText == "true") || (SysEx.DocumentElement.SelectSingleNode("EntityXContactInfoGrid_RowDeletedFlag").InnerText == "true"))
                { //Parijat: Mits 9610
                    if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7")
                    {
                        base.ResetSysExData("IsPostBack", "1");
                    }
                }
            }

			base.ResetSysExData("EntityXOperatingAsSelectedId",""); 
			base.ResetSysExData("EntityXOperatingAsGrid_RowDeletedFlag","false"); 
			base.ResetSysExData("EntityXOperatingAsGrid_RowAddedFlag", "false");

			// Contact Info
			XmlDocument objEntityXContactInfoXmlDoc = GetEntityXContactInfoData();
			
			XmlNode objOldEntityXContactInfoNode = SysEx.DocumentElement.SelectSingleNode(objEntityXContactInfoXmlDoc.DocumentElement.LocalName);
			if(objOldEntityXContactInfoNode!=null)
			{
				SysEx.DocumentElement.RemoveChild(objOldEntityXContactInfoNode);  
			}
			XmlNode objEntityXContactInfoNode = 
				SysEx.ImportNode(objEntityXContactInfoXmlDoc.DocumentElement ,true);
			SysEx.DocumentElement.AppendChild(objEntityXContactInfoNode);  	

			base.ResetSysExData("EntityXContactInfoSelectedId",""); 
			base.ResetSysExData("EntityXContactInfoGrid_RowDeletedFlag","false"); 
			base.ResetSysExData("EntityXContactInfoGrid_RowAddedFlag", "false");

            //Client Limits Org Hierarchy
            
            XmlDocument objClientLimitsInfoXmlDoc = GetClientLimitsInfoData();

            XmlNode objOldClientLimitsInfoNode = SysEx.DocumentElement.SelectSingleNode(objClientLimitsInfoXmlDoc.DocumentElement.LocalName);
            if (objOldClientLimitsInfoNode != null)
            {
                SysEx.DocumentElement.RemoveChild(objOldClientLimitsInfoNode);
            }
            XmlNode objClientLimitsInfoNode =
                SysEx.ImportNode(objClientLimitsInfoXmlDoc.DocumentElement, true);
            SysEx.DocumentElement.AppendChild(objClientLimitsInfoNode);

            base.ResetSysExData("ClientLimitsSelectedId", "");
            base.ResetSysExData("ClientLimitsGrid_RowDeletedFlag", "false");
            base.ResetSysExData("ClientLimitsGrid_RowAddedFlag", "false");

            //Client Limits Org Hierarchy
            //Added Rakhi for R7:Add Employee Data Elements
            bool bMulAddresses = this.objData.Context.InternalSettings.SysSettings.UseMultipleAddresses;
            if (bMulAddresses)
            {
                base.AddDisplayNode("addressesinfo");
                SetPrimaryAddressReadOnly();
                bool bIsFirstRecord = false;
                XmlDocument objEntityXAddressInfoXmlDoc = GetEntityXAddressInfo();
                XmlNode objOldEntityXAddressInfoNode = SysEx.DocumentElement.SelectSingleNode(objEntityXAddressInfoXmlDoc.DocumentElement.LocalName);
                if (objOldEntityXAddressInfoNode != null)
                {
                    SysEx.DocumentElement.RemoveChild(objOldEntityXAddressInfoNode);
                }
                XmlNode objEntityXAddressInfoNode =
                    SysEx.ImportNode(objEntityXAddressInfoXmlDoc.DocumentElement, true);
                SysEx.DocumentElement.AppendChild(objEntityXAddressInfoNode);

                if (SysEx.DocumentElement.SelectSingleNode("IsPostBack") == null)
                    CreateSysExData("IsPostBack");
                if (SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag") != null)
                {
                    if (SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag").InnerText == "true")
                    {
                        if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7")
                        {
                            base.ResetSysExData("IsPostBack", "1");
                        }
                    }
                }
                if (this.Entity.EntityXAddressesList.Count == 0)
                    bIsFirstRecord = true;

                if (base.SysEx.SelectSingleNode("//PrimaryFlag") == null)
                    base.ResetSysExData("PrimaryFlag", "false");

                if (base.SysEx.SelectSingleNode("//PrimaryRow") == null)
                    base.ResetSysExData("PrimaryRow", "");

                if (base.SysEx.SelectSingleNode("//PrimaryAddressChanged") == null)
                    base.ResetSysExData("PrimaryAddressChanged", "false");

                base.ResetSysExData("EntityXAddressesInfoSelectedId", "");
                base.ResetSysExData("EntityXAddressesInfoGrid_RowDeletedFlag", "false");
                base.ResetSysExData("EntityXAddressesInfoGrid_RowAddedFlag", "false");
                base.ResetSysExData("UseMultipleAddresses", bMulAddresses.ToString());
                base.ResetSysExData("IsFirstRecord", bIsFirstRecord.ToString());
            }
            else
            {
                base.AddKillNode("addressesinfo");
                base.AddKillNode("EntityXAddressesInfoGrid");//Mits 22246:GridTitle and Buttons getting visible in topdown Layout
            }
            //if (base.SysEx.SelectSingleNode("//PopupGridRowsDeleted") == null || base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
            //    base.ResetSysExData("PopupGridRowsDeleted", "");
            //Added Rakhi for R7:Add Employee Data Elements


			base.ResetSysExData("CheckSSN","1");

			//Raman Bhatia - Acrosoft Phase 1 changes.. Document Management Not available for Employee if Acrosoft is enabled
			bool bUseAcrosoftInterface = Entity.Context.InternalSettings.SysSettings.UseAcrosoftInterface;
            bool bUsePaperVision = Entity.Context.InternalSettings.SysSettings.UsePaperVisionInterface;//Mona:PaperVisionMerge:Animesh Inserted MITS 16697
            //rbhatia4:R8: Use Media View Setting : July 11 2011
            // akaushik5 Changed for MITS 30117 Starts
            //if (bUseAcrosoftInterface || bUsePaperVision || Entity.Context.InternalSettings.SysSettings.UseMediaViewInterface)
            RMConfigurationManager.GetAcrosoftSettings();
            bool bUseRMADocumentManagement = !object.ReferenceEquals(AcrosoftSection.UseRMADocumentManagementForEntity, null) && AcrosoftSection.UseRMADocumentManagementForEntity.ToLower().Equals("true");
            if ((bUseAcrosoftInterface && !bUseRMADocumentManagement) || bUsePaperVision || Entity.Context.InternalSettings.SysSettings.UseMediaViewInterface)
            // akaushik5 Changed for MITS 30117 Ends
            {
                base.AddKillNode("attachdocuments");//Mona:PaperVisionMerge:Animesh Inserted MITS 16697
                base.AddKillNode("attach");
            }
            
            //JAP - LSS changes - lock down form as read-only if LSS interface created the record
            //if (Entity.AddedByUser == "LSSINF")
            //{
            //    XmlElement xmlFormElement = (XmlElement)base.SysView.SelectSingleNode("//form");
            //    if (xmlFormElement != null) xmlFormElement.SetAttribute("readonly", "1");
            //}

            //Yatharth: R7 Time Zone Enhancement :START
            if ((Entity.EntityTableId != Entity.Context.InternalSettings.SysSettings.OrgTimeZoneLevel) && (base.CurrentAction.ToString() != "AddNew"))
            {

                base.AddKillNode("timezonetracking");
                base.AddKillNode("timezonecode");
                base.AddKillNode("daylightsavings");
            }

            if(base.CurrentAction.ToString() == "AddNew")
            {
                base.ResetSysExData("UtilTimeZoneLevel", Entity.Context.InternalSettings.SysSettings.OrgTimeZoneLevel.ToString());
            }
            //Yatharth: R7 Time Zone Enhancement :END

            //Start rsushilaggar Entity Payment Approval (only execute this block if the Use Entity approval check box is "off") MITS 20606 05/05/2010
            if (!Entity.Context.InternalSettings.SysSettings.UseEntityApproval)
            {
                base.AddKillNode("entityapprovalstatuscode");
                base.AddKillNode("rejectreasontext");
            }
            //End rsushilaggar Entity Payment Approval MITS 20606 04/02/2010

            //MITS:34276 : Starts -- Entity ID Type View Permission start
            if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_VIEW))
            {
                base.AddKillNode("TABSentityidtypeinfo");
                base.AddKillNode("TBSPentityidtypeinfo");
            }
            //MITS:34276 : Ends -- Entity ID Type View Permission end

            //Added by Amitosh for R8 enhancement of EFT
            if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ORGHIERARCHYMAINT_EFT))
            {
                base.AddKillNode("BankingInfo");
            }
            //End Amitosh
            //Added by Manika for R8 enhancement Withholding
            if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ORGHIERARCHYMAINT_WITHHOLDING))
            {
                base.AddKillNode("Withholding");
            }
            //End Manika

            //MITS:34276 : Starts-- Entity ID Type tab display start
            XmlDocument objEntityIDTypeAsXmlDoc = GetEntityIdTypeInfoData();

            XmlNode objOldEntityIdTypeAsNode = SysEx.DocumentElement.SelectSingleNode(objEntityIDTypeAsXmlDoc.DocumentElement.LocalName);
            if (objOldEntityIdTypeAsNode != null)
            {
                SysEx.DocumentElement.RemoveChild(objOldEntityIdTypeAsNode);
            }
            XmlNode objEntityIdTypeAsNode = SysEx.ImportNode(objEntityIDTypeAsXmlDoc.DocumentElement, true);
            SysEx.DocumentElement.AppendChild(objEntityIdTypeAsNode);

            if (SysEx.DocumentElement.SelectSingleNode("IsPostBack") == null)
                CreateSysExData("IsPostBack");

            if (SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag") != null && SysEx.DocumentElement.SelectSingleNode("EntityXContactInfoGrid_RowDeletedFlag") != null && SysEx.DocumentElement.SelectSingleNode("EntityXEntityIDTypeGrid_RowDeletedFlag") != null)
            {
                if ((SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag").InnerText == "true") || (SysEx.DocumentElement.SelectSingleNode("EntityXContactInfoGrid_RowDeletedFlag").InnerText == "true") || (SysEx.DocumentElement.SelectSingleNode("EntityXEntityIDTypeGrid_RowDeletedFlag").InnerText == "true"))
                { //Parijat: Mits 9610
                    if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7")
                    {
                        base.ResetSysExData("IsPostBack", "1");
                    }
                }
            }

            base.ResetSysExData("EntityXEntityIDTypeSelectedId", "");
            base.ResetSysExData("EntityXEntityIDTypeGrid_RowDeletedFlag", "false");
            base.ResetSysExData("EntityXEntityIDTypeGrid_RowAddedFlag", "false");


            //MITS:34276 : Ends-- Entity ID Type tab display start
            //added by swati agarwal For PMC Gap 6 DCI fields MITS#36997 - RMA - 5497
            if (!Entity.Context.InternalSettings.SysSettings.UseDCIReportingFields)
            {
                base.AddKillNode("nccicarriercodetext");
            }
            else
            {
                base.AddDisplayNode("nccicarriercodetext");
            }
		}

		private void PopulateAttributeList(bool p_bIsPEId)
		{
			XmlNode objOld=null;
			XmlElement objNew=null;
			XmlElement objElem=null;
			XmlDocument objXML=base.SysEx;
			XmlElement objChild=null;
			XmlElement objElem1099=null;
			XmlElement objChild1099=null;
			objOld = objXML.SelectSingleNode("//ControlAppendAttributeList");
			objNew = objXML.CreateElement("ControlAppendAttributeList");
			
			if(p_bIsPEId == true)
				objElem=objXML.CreateElement("parenteid");
			objElem1099=objXML.CreateElement("parent1099eid");

			if(p_bIsPEId == true)
			{
				objChild=objXML.CreateElement("tableid");
				objChild.SetAttribute("value","0");
			}

			objChild1099=objXML.CreateElement("tableid");
			objChild1099.SetAttribute("value","0");

			//Entity.EntityTableId != Entity.Context.LocalCache.GetTableId("ATTORNEYS") || Entity.EntityTableId != Entity.Context.LocalCache.GetTableId("REHAB_PROVIDER")
			if(Entity.EntityTableId == Entity.Context.LocalCache.GetTableId("ST_AGENCY_LOCATION"))
			{
				if(p_bIsPEId == true)
					objChild.SetAttribute("value","STATE_AGENCY");
				objChild1099.SetAttribute("value","STATE_AGENCY");
			}
		
			if(p_bIsPEId == true)
				objElem.AppendChild(objChild);
			objElem1099.AppendChild(objChild1099);
			if(p_bIsPEId == true)
				objNew.AppendChild(objElem);
			objNew.AppendChild(objElem1099);

			if (Entity.EntityId!=0)
			{
				objElem=objXML.CreateElement("entitytableidtextname");

				objChild=objXML.CreateElement("required");
				objChild.SetAttribute("value","yes");
				objElem.AppendChild(objChild);
				objNew.AppendChild(objElem);
			}

			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);
			

		}
		
		
		
        //This method will be invoked both on Post-Back and on Save.
		public override void OnUpdateObject()
		{
			base.OnUpdateObject ();

            base.ResetSysExData("ParentEid", "");

            #region "Updating Contact Info Object"
            SortedList objEntityXContactInfoList = new SortedList();

            XmlNode objEntityXContactInfoNode = base.SysEx.DocumentElement.SelectSingleNode("EntityXContactInfo");  
            XmlAttribute objTypeAttributeNode = null;

            bool bIsNew = false;
            int iContactIdKey = -1;

            XmlDocument objEntityXContactXmlDoc = null;
            XmlElement objEntityXContactRootElement = null;
			
            int iEntityXContactInfoSelectedId = 
                base.GetSysExDataNodeInt("EntityXContactInfoSelectedId",true); 

            string sEntityXContactInfoRowAddedFlag =  
                base.GetSysExDataNodeText("EntityXContactInfoGrid_RowAddedFlag",true); 
            bool bEntityXContactInfoRowAddedFlag = 
                sEntityXContactInfoRowAddedFlag.ToUpper().CompareTo("TRUE")==0?true:false;  

            string sEntityXContactInfoRowDeletedFlag =  
                base.GetSysExDataNodeText("EntityXContactInfoGrid_RowDeletedFlag",true); 
            bool bEntityXContactInfoRowDeletedFlag = 
                sEntityXContactInfoRowDeletedFlag.ToUpper().CompareTo("TRUE")==0?true:false;  


            if(objEntityXContactInfoNode!=null)
            {
                // Loop through data for all rows of the grid
                foreach(XmlNode objOptionNode in objEntityXContactInfoNode.SelectNodes("option"))
                {
                    objTypeAttributeNode = objOptionNode.Attributes["type"];
                    bIsNew = false;
                    if(objTypeAttributeNode!=null && objTypeAttributeNode.InnerText.Equals("new")==true)
                    {
                        // This is the 'extra' hidden <option> for capturing a new grid row data
                        bIsNew = true;
                    }					
                    if((bIsNew==false)||(bIsNew==true&&bEntityXContactInfoRowAddedFlag==true))
                    {
                        objEntityXContactXmlDoc = new XmlDocument();
                        objEntityXContactRootElement = objEntityXContactXmlDoc.CreateElement("EntityXContactInfo");
                        objEntityXContactXmlDoc.AppendChild(objEntityXContactRootElement);
                        objEntityXContactRootElement.InnerXml = objOptionNode.InnerXml;

                        iContactIdKey = 
                            Conversion.ConvertStrToInteger(objEntityXContactRootElement.SelectSingleNode("ContactId").InnerText); 

                        if((bIsNew==true&&bEntityXContactInfoRowAddedFlag==true)||
                            (iContactIdKey < 0 && objEntityXContactRootElement.SelectSingleNode("EntityId").InnerText == ""))
                            objEntityXContactRootElement.SelectSingleNode("EntityId").InnerText = "0";

                        if(bIsNew==false&&bEntityXContactInfoRowDeletedFlag==true&&iContactIdKey==iEntityXContactInfoSelectedId)
                        {
                            // Aditya - This particular Tpp has been deleted. 
                            continue;
                        }
							
                        objEntityXContactInfoList.Add(iContactIdKey,objEntityXContactXmlDoc);   
                    }
                }
            }

            foreach(EntityXContactInfo objEntityXContactInfo in Entity.EntityXContactInfoList)
            {
                if(base.m_fda.SafeFormVariableParamText("SysCmd")=="7" && 
                    (objEntityXContactInfo.ContactId<0 || (bEntityXContactInfoRowDeletedFlag==true&&objEntityXContactInfo.ContactId==iEntityXContactInfoSelectedId)))
                    Entity.EntityXContactInfoList.Remove(objEntityXContactInfo.ContactId);
                else if (objEntityXContactInfo.ContactId<0)
                    Entity.EntityXContactInfoList.Remove(objEntityXContactInfo.ContactId);
            }
            EntityXContactInfo objTmpEntityXContactInfo = null;
            XmlDocument objTmpEntityXContactXMLDoc = null;
            //Parijat: Similar to Mits 9610
            ArrayList arrContactIds = new ArrayList();
            //
            for(int iListIndex=0; iListIndex<objEntityXContactInfoList.Count; iListIndex++)
            {
                objTmpEntityXContactXMLDoc = (XmlDocument) objEntityXContactInfoList.GetByIndex(iListIndex);

                if(objTmpEntityXContactXMLDoc.DocumentElement.SelectSingleNode("ContactId").InnerText.Trim() == "" || Conversion.ConvertStrToInteger(objTmpEntityXContactXMLDoc.DocumentElement.SelectSingleNode("ContactId").InnerText)< 0 )
                {
                    objTmpEntityXContactXMLDoc.DocumentElement.SelectSingleNode("ContactId").InnerText = ((-1 * iListIndex)-1).ToString(); 
                    objTmpEntityXContactInfo = Entity.EntityXContactInfoList.AddNew();
                    objTmpEntityXContactInfo.PopulateObject(objTmpEntityXContactXMLDoc); 
                }
                else
                {
                    objTmpEntityXContactInfo=Entity.EntityXContactInfoList[Conversion.ConvertStrToInteger(objTmpEntityXContactXMLDoc.DocumentElement.SelectSingleNode("ContactId").InnerText)];
                    objTmpEntityXContactInfo.PopulateObject(objTmpEntityXContactXMLDoc); 
                }
                //Parijat:Similar to Mits 9610
                arrContactIds.Add(objTmpEntityXContactXMLDoc.DocumentElement.SelectSingleNode("ContactId").InnerText);
            }
            //Parijat:Similar to Mits 9610
            foreach (EntityXContactInfo objEntityXContactInfo in Entity.EntityXContactInfoList)
            {
                if (!arrContactIds.Contains(objEntityXContactInfo.ContactId.ToString()))
                {
                    Entity.EntityXContactInfoList.Remove(objEntityXContactInfo.ContactId);
                }
            }
            #endregion

            #region "Updating Operating As Object"
            SortedList objEntityXOperatingAsList = new SortedList();

            XmlNode objEntityXOperatingAsNode = base.SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAs");
            objTypeAttributeNode = null;

            bIsNew = false;
            int iOperatingIdKey = -1;

            XmlDocument objEntityXOAXmlDoc = null;
            XmlElement objEntityXOARootElement = null;

            int iEntityXOperatingAsSelectedId =
                base.GetSysExDataNodeInt("EntityXOperatingAsSelectedId", true);

            string sEntityXOperatingAsRowAddedFlag =
                base.GetSysExDataNodeText("EntityXOperatingAsGrid_RowAddedFlag", true);
            bool bEntityXOperatingAsRowAddedFlag =
                sEntityXOperatingAsRowAddedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

            string sEntityXOperatingAsRowDeletedFlag =
                base.GetSysExDataNodeText("EntityXOperatingAsGrid_RowDeletedFlag", true);
            bool bEntityXOperatingAsRowDeletedFlag =
                sEntityXOperatingAsRowDeletedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;


            if (objEntityXOperatingAsNode != null)
            {
                // Loop through data for all rows of the grid
                foreach (XmlNode objOptionNode in objEntityXOperatingAsNode.SelectNodes("option"))
                {
                    objTypeAttributeNode = objOptionNode.Attributes["type"];
                    bIsNew = false;
                    if (objTypeAttributeNode != null && objTypeAttributeNode.InnerText.Equals("new") == true)
                    {
                        // This is the 'extra' hidden <option> for capturing a new grid row data
                        bIsNew = true;
                    }
                    if ((bIsNew == false) || (bIsNew == true && bEntityXOperatingAsRowAddedFlag == true))
                    {
                        objEntityXOAXmlDoc = new XmlDocument();
                        objEntityXOARootElement = objEntityXOAXmlDoc.CreateElement("EntityXOperatingAs");
                        objEntityXOAXmlDoc.AppendChild(objEntityXOARootElement);
                        objEntityXOARootElement.InnerXml = objOptionNode.InnerXml;

                        iOperatingIdKey =
                            Conversion.ConvertStrToInteger(objEntityXOARootElement.SelectSingleNode("OperatingId").InnerText);

                        if ((bIsNew == true && bEntityXOperatingAsRowAddedFlag == true) ||
                        (iOperatingIdKey < 0 && objEntityXOARootElement.SelectSingleNode("EntityId").InnerText == ""))
                            objEntityXOARootElement.SelectSingleNode("EntityId").InnerText = "0";

                        if (bIsNew == false && bEntityXOperatingAsRowDeletedFlag == true && iOperatingIdKey == iEntityXOperatingAsSelectedId)
                        {
                            // Aditya - This particular Tpp has been deleted. 
                            continue;
                        }

                        objEntityXOperatingAsList.Add(iOperatingIdKey, objEntityXOAXmlDoc);
                    }
                }
            }

            foreach (EntityXOperatingAs objEntityXOperatingAs in Entity.EntityXOperatingAsList)
            {
                if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7" &&
                    (objEntityXOperatingAs.OperatingId < 0 || (bEntityXOperatingAsRowDeletedFlag == true && objEntityXOperatingAs.OperatingId == iEntityXOperatingAsSelectedId)))
                {
                    Entity.EntityXOperatingAsList.Remove(objEntityXOperatingAs.OperatingId);

                }
                else if (objEntityXOperatingAs.OperatingId < 0)
                    Entity.EntityXOperatingAsList.Remove(objEntityXOperatingAs.OperatingId);
            }
            EntityXOperatingAs objTmpEntityXOperatingAs = null;
            XmlDocument objTmpEntityXOAXMLDoc = null;

            ArrayList arrOperatingIds = new ArrayList();

            for (int iListIndex = 0; iListIndex < objEntityXOperatingAsList.Count; iListIndex++)
            {
                objTmpEntityXOAXMLDoc = (XmlDocument)objEntityXOperatingAsList.GetByIndex(iListIndex);

                if (objTmpEntityXOAXMLDoc.DocumentElement.SelectSingleNode("OperatingId").InnerText.Trim() == "" || Conversion.ConvertStrToInteger(objTmpEntityXOAXMLDoc.DocumentElement.SelectSingleNode("OperatingId").InnerText) < 0)
                {
                    objTmpEntityXOAXMLDoc.DocumentElement.SelectSingleNode("OperatingId").InnerText = ((-1 * iListIndex) - 1).ToString();
                    objTmpEntityXOperatingAs = Entity.EntityXOperatingAsList.AddNew();
                }
                else
                    objTmpEntityXOperatingAs = Entity.EntityXOperatingAsList[Conversion.ConvertStrToInteger(objTmpEntityXOAXMLDoc.DocumentElement.SelectSingleNode("OperatingId").InnerText)];

                arrOperatingIds.Add(objTmpEntityXOAXMLDoc.DocumentElement.SelectSingleNode("OperatingId").InnerText);
                objTmpEntityXOperatingAs.PopulateObject(objTmpEntityXOAXMLDoc);
            }

            foreach (EntityXOperatingAs objEntityXOperatingAs in Entity.EntityXOperatingAsList)
            {
                if (!arrOperatingIds.Contains(objEntityXOperatingAs.OperatingId.ToString()))
                {
                    Entity.EntityXOperatingAsList.Remove(objEntityXOperatingAs.OperatingId);
                }
            }
            #endregion

            #region "Updating Client Limits Object"
            SortedList objClientLimitsInfoList = new SortedList();

            XmlNode objClientLimitsInfoNode = base.SysEx.DocumentElement.SelectSingleNode("ClientLimits");
            objTypeAttributeNode = null;

            bIsNew = false;
            int iClientLimitsRowIdKey = -1;

            XmlDocument objClientLimitsXmlDoc = null;
            XmlElement objClientLimitsRootElement = null;

            int iClientLimitsSelectedId =
                base.GetSysExDataNodeInt("ClientLimitsSelectedId", true);

            string sClientLimitsRowAddedFlag =
                base.GetSysExDataNodeText("ClientLimitsGrid_RowAddedFlag", true);
            bool bClientLimitsRowAddedFlag =
                sClientLimitsRowAddedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

            string sClientLimitsRowDeletedFlag =
                base.GetSysExDataNodeText("ClientLimitsGrid_RowDeletedFlag", true);
            bool bClientLimitsRowDeletedFlag =
                sClientLimitsRowDeletedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;


            if (objClientLimitsInfoNode != null)
            {
                // Loop through data for all rows of the grid
                foreach (XmlNode objOptionNode in objClientLimitsInfoNode.SelectNodes("option"))
                {
                    objTypeAttributeNode = objOptionNode.Attributes["type"];
                    bIsNew = false;
                    if (objTypeAttributeNode != null && objTypeAttributeNode.InnerText.Equals("new") == true)
                    {
                        // This is the 'extra' hidden <option> for capturing a new grid row data
                        bIsNew = true;
                    }
                    if ((bIsNew == false) || (bIsNew == true && bClientLimitsRowAddedFlag == true))
                    {
                        objClientLimitsXmlDoc = new XmlDocument();
                        objClientLimitsRootElement = objClientLimitsXmlDoc.CreateElement("ClientLimits");
                        objClientLimitsXmlDoc.AppendChild(objClientLimitsRootElement);
                        objClientLimitsRootElement.InnerXml = objOptionNode.InnerXml;

                        iClientLimitsRowIdKey =
                            Conversion.ConvertStrToInteger(objClientLimitsRootElement.SelectSingleNode("ClientLimitsRowId").InnerText);

                  
                        if ((bIsNew == true && bClientLimitsRowAddedFlag == true) ||
                        (iClientLimitsRowIdKey < 0 && objClientLimitsRootElement.SelectSingleNode("ClientEid").InnerText == ""))
                            objClientLimitsRootElement.SelectSingleNode("ClientEid").InnerText = "0";


                        if (bIsNew == false && bClientLimitsRowDeletedFlag == true && iClientLimitsRowIdKey == iClientLimitsSelectedId)
                        {
                            // Aditya - This particular Tpp has been deleted. 
                            continue;
                        }

                        objClientLimitsInfoList.Add(iClientLimitsRowIdKey, objClientLimitsXmlDoc);
                    }
                }
            }

            foreach (ClientLimits objClientLimits in Entity.ClientLimitsList)
            {
                if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7" &&
                    (objClientLimits.ClientLimitsRowId < 0 || (bClientLimitsRowDeletedFlag == true && objClientLimits.ClientLimitsRowId == iClientLimitsSelectedId)))
                {
                    Entity.ClientLimitsList.Remove(objClientLimits.ClientLimitsRowId);

                }
                else if (objClientLimits.ClientLimitsRowId < 0)
                    Entity.ClientLimitsList.Remove(objClientLimits.ClientLimitsRowId);
            }
            ClientLimits objTmpClientLimits = null;
            XmlDocument objTmpClientLimitsXMLDoc = null;

            ArrayList arrClientLimitsRowIds = new ArrayList();

            for (int iListIndex = 0; iListIndex < objClientLimitsInfoList.Count; iListIndex++)
            {
                objTmpClientLimitsXMLDoc = (XmlDocument)objClientLimitsInfoList.GetByIndex(iListIndex);

                
                
                if (objTmpClientLimitsXMLDoc.DocumentElement.SelectSingleNode("ClientLimitsRowId").InnerText.Trim() == "" || Conversion.ConvertStrToInteger(objTmpClientLimitsXMLDoc.DocumentElement.SelectSingleNode("ClientLimitsRowId").InnerText) < 0)
                {
                    objTmpClientLimitsXMLDoc.DocumentElement.SelectSingleNode("ClientLimitsRowId").InnerText = ((-1 * iListIndex) - 1).ToString();
                    objTmpClientLimits = Entity.ClientLimitsList.AddNew();
                }
                else
                    objTmpClientLimits = Entity.ClientLimitsList[Conversion.ConvertStrToInteger(objTmpClientLimitsXMLDoc.DocumentElement.SelectSingleNode("ClientLimitsRowId").InnerText)];
                    
                    

                arrClientLimitsRowIds.Add(objTmpClientLimitsXMLDoc.DocumentElement.SelectSingleNode("ClientLimitsRowId").InnerText);
                
                objTmpClientLimits.PopulateObject(objTmpClientLimitsXMLDoc);
            }

            foreach (ClientLimits objClientLimits in Entity.ClientLimitsList)
            {
                if (!arrClientLimitsRowIds.Contains(objClientLimits.ClientLimitsRowId.ToString()))
                {
                    Entity.ClientLimitsList.Remove(objClientLimits.ClientLimitsRowId);
                }
            }
            #endregion
            //Added Rakhi for R7:Add Emp Data Elements
            #region "Updating Address Info Object"
            bool bMulAddresses = this.objData.Context.InternalSettings.SysSettings.UseMultipleAddresses;
            if (bMulAddresses)
            {
                SortedList objEntityXAddressesInfoList = new SortedList();
                SortedList objAddressesInfoList = new SortedList();//Added For R7:Add Emp Data Elements
                //XmlDocument objAddressXPhoneInfoDoc = null; //Added For R7:Add Emp Data Elements
                bool bIsSuccess = false;
                int iAddressIdKey = -1;
                int iRowIdKey = -1;//AA//RMA-8753 nshah28(Added by ashish)

                XmlDocument objEntityXAddressXmlDoc = null;
                XmlElement objEntityXAddressRootElement = null;

                objTypeAttributeNode = null;
                bIsNew = false;
                
                int iEntityXAddressesInfoSelectedId =
                    base.GetSysExDataNodeInt("EntityXAddressesInfoSelectedId", true);

                string sEntityXAddressesInfoRowAddedFlag =
                    base.GetSysExDataNodeText("EntityXAddressesInfoGrid_RowAddedFlag", true);
                bool bEntityXAddressesInfoRowAddedFlag =
                    sEntityXAddressesInfoRowAddedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

                string sEntityXAddressesInfoRowDeletedFlag =
                    base.GetSysExDataNodeText("EntityXAddressesInfoGrid_RowDeletedFlag", true);
                bool bEntityXAddressesInfoRowDeletedFlag =
                    sEntityXAddressesInfoRowDeletedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;


                XmlNode objEntityXAddressesInfoNode = base.SysEx.DocumentElement.SelectSingleNode("EntityXAddressesInfo");
                if (objEntityXAddressesInfoNode != null)
                {
                    // Loop through data for all rows of the grid
                    foreach (XmlNode objOptionNode in objEntityXAddressesInfoNode.SelectNodes("option"))
                    {
                        objTypeAttributeNode = objOptionNode.Attributes["type"];
                        bIsNew = false;
                        if (objTypeAttributeNode != null && objTypeAttributeNode.InnerText.Equals("new") == true)
                        {
                            // This is the 'extra' hidden <option> for capturing a new grid row data
                            bIsNew = true;
                        }
                        if ((bIsNew == false) || (bIsNew == true && bEntityXAddressesInfoRowAddedFlag == true))
                        {
                            objEntityXAddressXmlDoc = new XmlDocument();
                            objEntityXAddressRootElement = objEntityXAddressXmlDoc.CreateElement("EntityXAddresses");
                            objEntityXAddressXmlDoc.AppendChild(objEntityXAddressRootElement);

                            #region AddressXPhone Info Grid
                            //XmlNode objAddressNode = objOptionNode.SelectSingleNode("AddressXPhoneInfo");
                            //if (objOptionNode != null && !(bIsNew == false && bEntityXAddressesInfoRowDeletedFlag == true && iAddressIdKey == iEntityXAddressesInfoSelectedId))
                            //{

                            //    XmlElement objAddressXPhoneRootElement = null;
                            //    int iPhoneId = 0;
                            //    bool bResult = false;
                            //    string sKey = string.Empty;
                            //    foreach (XmlNode objPhoneNode in objAddressNode.SelectNodes("option"))
                            //    {
                            //        objAddressXPhoneInfoDoc = new XmlDocument();
                            //        objAddressXPhoneRootElement = objAddressXPhoneInfoDoc.CreateElement("AddressXPhoneInfo");
                            //        objAddressXPhoneInfoDoc.AppendChild(objAddressXPhoneRootElement);
                            //        objPhoneNode.SelectSingleNode("ContactId").InnerText = objOptionNode.SelectSingleNode("ContactId").InnerText;
                            //        objAddressXPhoneRootElement.InnerXml = objPhoneNode.InnerXml;
                            //        bResult = Int32.TryParse(objAddressXPhoneRootElement.SelectSingleNode("PhoneId").InnerText, out iPhoneId);
                            //        if (iPhoneId != 0)
                            //        {
                            //            sKey = objPhoneNode.SelectSingleNode("ContactId").InnerText + "_" + objAddressXPhoneRootElement.SelectSingleNode("PhoneId").InnerText;
                            //            objAddressesInfoList.Add(sKey, objAddressXPhoneInfoDoc);
                            //        }
                            //    }
                            //}
                            //objOptionNode.RemoveChild(objAddressNode);
                            #endregion //AddressXPhone Info Grid

                            objEntityXAddressRootElement.InnerXml = objOptionNode.InnerXml;

                            //AA //RMA-8753 nshah28 start
                            string strSearchString = string.Empty;
                            string sStateCode = string.Empty;
                            string sStateDesc = string.Empty;
                            string sCountry = string.Empty;

                            AddressForm objAddressForm = new AddressForm(m_fda);

                            strSearchString = objAddressForm.CreateSearchString(objEntityXAddressRootElement, objData.Context.LocalCache);

                            //RMA-8753 nshah28(Added by ashish)

                            XmlElement objRootElement = objEntityXAddressXmlDoc.CreateElement("SearchString");
                            objRootElement.InnerText = strSearchString; //change by nshah28
                            objEntityXAddressRootElement.AppendChild(objRootElement);
                            //RMA-8753 nshah28(Added by ashish)

                            
                            //iAddressIdKey =
                            //   Conversion.CastToType<int>(objEntityXAddressRootElement.SelectSingleNode("AddressId").InnerText, out bIsSuccess);
                            iRowIdKey =
                             Conversion.CastToType<int>(objEntityXAddressRootElement.SelectSingleNode("RowId").InnerText, out bIsSuccess);


                            if ((bIsNew == true && bEntityXAddressesInfoRowAddedFlag == true) ||
                                (iAddressIdKey < 0 && objEntityXAddressRootElement.SelectSingleNode("EntityId").InnerText == ""))
                                objEntityXAddressRootElement.SelectSingleNode("EntityId").InnerText = "0";
                            else
                                objEntityXAddressRootElement.SelectSingleNode("EntityId").InnerText = Entity.EntityId.ToString();
                            //RMA-8753 nshah28 end
                            //Marking Changed Address as Primary Address
                            XmlNode objPrimaryAddressChanged = base.SysEx.SelectSingleNode("//PrimaryAddressChanged");
                            XmlNode objNewPrimaryAddress = objEntityXAddressRootElement.SelectSingleNode("NewPrimaryRecord"); //For identification of New Primary Address
                            if (objPrimaryAddressChanged != null && objPrimaryAddressChanged.InnerText.ToLower() == "true")
                            {
                                if (objNewPrimaryAddress != null)
                                {
                                    XmlNode objPrimaryAddress = objEntityXAddressRootElement.SelectSingleNode("PrimaryAddress");
                                    if (objPrimaryAddress != null)
                                    {
                                        if (objNewPrimaryAddress.InnerText.ToLower() == "true")
                                        {
                                            objPrimaryAddress.InnerText = "True";
                                        }

                                        else
                                        {
                                            objPrimaryAddress.InnerText = "False";
                                        }
                                    }
                                }
                            }
                            if (objNewPrimaryAddress != null)
                            {
                                objEntityXAddressRootElement.RemoveChild(objNewPrimaryAddress); //Since it is not a Datamodel Property,Removing it before the processing.
                            }
                            //Marking Changed Address as Primary Address

                            if (bIsNew == false && bEntityXAddressesInfoRowDeletedFlag == true && iRowIdKey == iEntityXAddressesInfoSelectedId) //RMA-8753 nshah28
                            {
                                // Aditya - This particular Tpp has been deleted. 
                                continue;
                            }
                            objEntityXAddressesInfoList.Add(iRowIdKey, objEntityXAddressXmlDoc);//AA//RMA-8753 nshah28(Added by ashish)
                        }
                    }
                }

                //AA//RMA-8753 nshah28 
                //foreach (EntityXAddresses objEntityXAddressesInfo in Entity.EntityXAddressesList)
                //{
                //    if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7" &&
                //        (objEntityXAddressesInfo.AddressId < 0 || (bEntityXAddressesInfoRowDeletedFlag == true && objEntityXAddressesInfo.AddressId == iEntityXAddressesInfoSelectedId)))
                //    {
                //        Entity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                //    }
                //    else if (objEntityXAddressesInfo.AddressId < 0)
                //        Entity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                //}
                foreach (EntityXAddresses objEntityXAddressesInfo in Entity.EntityXAddressesList)
                {
                    if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7" &&
                        (objEntityXAddressesInfo.RowId < 0 || (bEntityXAddressesInfoRowDeletedFlag == true && objEntityXAddressesInfo.RowId == iEntityXAddressesInfoSelectedId)))
                    {
                        Entity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId);
                    }
                    else if (objEntityXAddressesInfo.RowId < 0)
                        Entity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId);
                }
                //AA//RMA-8753 nshah28 END
                EntityXAddresses objTmpEntityXAddressesInfo = null;
                XmlDocument objTmpEntityXAddressXMLDoc = null;
                //AddressXPhoneInfo objTmpAddressXPhoneInfo = null;
                //XmlDocument objTmpAddressXPhoneXMLDoc = null;
                //string sOrgAddressId = string.Empty;
                //Parijat:Mits 9610
                ArrayList arrAddressIds = new ArrayList();
                ArrayList arrRowIds = new ArrayList(); //RMA-8753 nshah28 
                //
                for (int iListIndex = 0; iListIndex < objEntityXAddressesInfoList.Count; iListIndex++)
                {
                    objTmpEntityXAddressXMLDoc = (XmlDocument)objEntityXAddressesInfoList.GetByIndex(iListIndex);
                    //sOrgAddressId = objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("AddressId").InnerText.Trim();
                    //AA//RMA-8753 nshah28 STARt
                    if (objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText.Trim() == "" || Conversion.CastToType<int>(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText, out bIsSuccess) < 0)
                    {
                        objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText = ((-1 * iListIndex) - 1).ToString();
                        objTmpEntityXAddressesInfo = Entity.EntityXAddressesList.AddNew();
                    }
                    else
                        objTmpEntityXAddressesInfo = Entity.EntityXAddressesList[Conversion.CastToType<int>(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText, out bIsSuccess)];
                    //Parijat:Mits 9610
                    arrAddressIds.Add(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("AddressId").InnerText);
                    arrRowIds.Add(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText);//RMA-8753 nshah28 
                    //objTmpEntityXAddressesInfo.PopulateObject(objTmpEntityXAddressXMLDoc);
                    //AA//RMA-8753 nshah28 END
                    //RMA 8753 Ashish Ahuja For Address Master Enhancement Start
                    if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//RowId").InnerText != String.Empty)
                        objTmpEntityXAddressesInfo.RowId = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//RowId").InnerText);
                    if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//AddressType").Attributes["codeid"].Value != String.Empty)
                        objTmpEntityXAddressesInfo.AddressType = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//AddressType").Attributes["codeid"].Value);
                    if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//PrimaryAddress").InnerText != String.Empty)
                        objTmpEntityXAddressesInfo.PrimaryAddress = Convert.ToBoolean(objTmpEntityXAddressXMLDoc.SelectSingleNode("//PrimaryAddress").InnerText) ? -1 : 0;
                    //objTmpEntityXAddressesInfo.PrimaryAddress = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//PrimaryAddress").InnerText);
                    objTmpEntityXAddressesInfo.Address.Addr1 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr1").InnerText;
                    objTmpEntityXAddressesInfo.Address.Addr2 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr2").InnerText;
                    objTmpEntityXAddressesInfo.Address.Addr3 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr3").InnerText;
                    objTmpEntityXAddressesInfo.Address.Addr4 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr4").InnerText;
                    objTmpEntityXAddressesInfo.Address.City = objTmpEntityXAddressXMLDoc.SelectSingleNode("//City").InnerText;
                    if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//Country").Attributes["codeid"].Value != String.Empty)
                        objTmpEntityXAddressesInfo.Address.Country = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//Country").Attributes["codeid"].Value);
                    if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//State").Attributes["codeid"].Value != String.Empty)
                        objTmpEntityXAddressesInfo.Address.State = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//State").Attributes["codeid"].Value);
                    objTmpEntityXAddressesInfo.Email = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Email").InnerText;
                    objTmpEntityXAddressesInfo.Fax = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Fax").InnerText; ;
                    objTmpEntityXAddressesInfo.Address.County = objTmpEntityXAddressXMLDoc.SelectSingleNode("//County").InnerText;
                    objTmpEntityXAddressesInfo.Address.ZipCode = objTmpEntityXAddressXMLDoc.SelectSingleNode("//ZipCode").InnerText;
                    objTmpEntityXAddressesInfo.EffectiveDate = objTmpEntityXAddressXMLDoc.SelectSingleNode("//EffectiveDate").InnerText;
                    objTmpEntityXAddressesInfo.ExpirationDate = objTmpEntityXAddressXMLDoc.SelectSingleNode("//ExpirationDate").InnerText;

                    objTmpEntityXAddressesInfo.Address.SearchString = objTmpEntityXAddressXMLDoc.SelectSingleNode("//SearchString").InnerText;
                    int iAddressId = CommonFunctions.CheckAddressDuplication(objTmpEntityXAddressXMLDoc.SelectSingleNode("//SearchString").InnerText, Entity.Context.RMDatabase.ConnectionString, base.ClientId);
                    objTmpEntityXAddressesInfo.AddressId = iAddressId;
                    objTmpEntityXAddressesInfo.Address.AddressId = iAddressId;
                    //RMA 8753 Ashish Ahuja For Address Master Enhancement End

                    #region Phone Info Details
                    //int iPhoneIndex = 0;
                    //for (int iCount = 0; iCount < objAddressesInfoList.Count; iCount++)
                    //{
                    //    objTmpAddressXPhoneXMLDoc = (XmlDocument)objAddressesInfoList.GetByIndex(iCount);
                    //    if (sOrgContactId == objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("ContactId").InnerText.Trim())
                    //    {
                    //        if (objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText.Trim() == "" || Conversion.ConvertStrToInteger(objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText) < 0)
                    //        {
                    //            objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText = ((-1 * iPhoneIndex) - 1).ToString();
                    //            objTmpAddressXPhoneInfo = objTmpEntityXAddressesInfo.AddressXPhoneInfoList.AddNew();
                    //            iPhoneIndex++;
                    //        }
                    //        else
                    //            objTmpAddressXPhoneInfo = objTmpEntityXAddressesInfo.AddressXPhoneInfoList[Conversion.ConvertStrToInteger(objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText)];
                    //        objTmpAddressXPhoneInfo.PopulateObject(objTmpAddressXPhoneXMLDoc);
                    //        objTmpAddressXPhoneInfo.ContactId = objTmpEntityXAddressesInfo.ContactId;
                    //    }
                    //}
                    #endregion //Phone Info Details

                    #region Added code to remove the saved phone numbers
                    //if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
                    //{
                    //    XmlNode objPhoneNumbersDeleted = base.SysEx.SelectSingleNode("//PopupGridRowsDeleted");
                    //    if (objPhoneNumbersDeleted != null && !String.IsNullOrEmpty(objPhoneNumbersDeleted.InnerText))
                    //    {
                    //        string[] strSplit = objPhoneNumbersDeleted.InnerText.Split(new Char[] { '|' });
                    //        foreach (AddressXPhoneInfo objAddressXPhoneInfo in objTmpEntityXAddressesInfo.AddressXPhoneInfoList)
                    //        {
                    //            foreach (string sPhoneNumber in strSplit)
                    //            {
                    //                int iPhoneId = Conversion.ConvertStrToInteger(sPhoneNumber);
                    //                if (iPhoneId == objAddressXPhoneInfo.PhoneId)
                    //                {
                    //                    objTmpEntityXAddressesInfo.AddressXPhoneInfoList.Remove(iPhoneId);
                    //                    break;
                    //                }

                    //            }
                    //        }
                    //    }
                    //}
                    #endregion //Added code to remove the saved phone numbers
                }
                //Parijat:Mits 9610
                //RMA-8753 nshah28 //AA START
                foreach (EntityXAddresses objEntityXAddressesInfo in Entity.EntityXAddressesList)
                {
                    if (!arrRowIds.Contains(objEntityXAddressesInfo.RowId.ToString()))
                    {
                        Entity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId);
                    }
                }
                //RMA-8753 nshah28 //AA END
            }
            else
            {
                if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
                {
                    string sAddr1 = Entity.Addr1;
                    string sAddr2 = Entity.Addr2;
                    string sAddr3 = Entity.Addr3;// JIRA 6420 pkandhari
                    string sAddr4 = Entity.Addr4;// JIRA 6420 pkandhari
                    string sCity = Entity.City;
                    int iCountryCode = Entity.CountryCode;
                    int iStateId = Entity.StateId;
                    string sEmailAddress = Entity.EmailAddress;
                    string sFaxNumber = Entity.FaxNumber;
                    string sCounty = Entity.County;
                    string sZipCode = Entity.ZipCode;
                    //RMA-8753 nshah28(Added by ashish)
                    string sSearchString = string.Empty;
                    //RMA-8753 nshah28(Added by ashish) END
                    if (Entity.EntityXAddressesList.Count > 0)
                    {
                        foreach (EntityXAddresses objEntityXAddressesInfo in Entity.EntityXAddressesList)
                        {
                            if (objEntityXAddressesInfo.PrimaryAddress == -1)
                            {
                                //RMA-8753 nshah28(Added by ashish) START
                                //objEntityXAddressesInfo.Addr1 = sAddr1;
                                //objEntityXAddressesInfo.Addr2 = sAddr2;
                                //objEntityXAddressesInfo.Addr3 = sAddr3;// JIRA 6420 pkandhari
                                //objEntityXAddressesInfo.Addr4 = sAddr4;// JIRA 6420 pkandhari
                                //objEntityXAddressesInfo.City = sCity;
                                //objEntityXAddressesInfo.Country = iCountryCode;
                                //objEntityXAddressesInfo.State = iStateId;
                                objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                                objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                                objEntityXAddressesInfo.Address.Addr3 = sAddr3;
                                objEntityXAddressesInfo.Address.Addr4 = sAddr4;
                                objEntityXAddressesInfo.Address.City = sCity;
                                objEntityXAddressesInfo.Address.Country = iCountryCode;
                                objEntityXAddressesInfo.Address.State = iStateId;
                                objEntityXAddressesInfo.Email = sEmailAddress;
                                objEntityXAddressesInfo.Fax = sFaxNumber;
                                //objEntityXAddressesInfo.County = sCounty;
                                //objEntityXAddressesInfo.ZipCode = sZipCode;
                                objEntityXAddressesInfo.Address.County = sCounty;
                                objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                                AddressForm objAddressForm = new AddressForm(m_fda);
                                sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                                objEntityXAddressesInfo.Address.SearchString = sSearchString;
                                objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, Entity.Context.RMDatabase.ConnectionString, base.ClientId);
                                objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
                                //RMA-8753 nshah28(Added by ashish) END
                                break;
                            }

                        }
                    }
                    else
                    {
                        //RMA-8753 nshah28(Added by ashish) START
                        if (
                                sAddr1 != string.Empty || sAddr2 != string.Empty || sAddr3 != string.Empty || sAddr4 != string.Empty || sCity != string.Empty ||
                                iCountryCode != 0 || iStateId != 0 || sEmailAddress != string.Empty ||
                                sFaxNumber != string.Empty || sCounty != string.Empty || sZipCode != string.Empty
                            )
                        {
                            EntityXAddresses objEntityXAddressesInfo = Entity.EntityXAddressesList.AddNew();
                            //objEntityXAddressesInfo.Addr1 = sAddr1;
                            //objEntityXAddressesInfo.Addr2 = sAddr2;
                            //objEntityXAddressesInfo.Addr3 = sAddr3;// JIRA 6420 pkandhari
                            //objEntityXAddressesInfo.Addr4 = sAddr4;// JIRA 6420 pkandhari
                            //objEntityXAddressesInfo.City = sCity;
                            //objEntityXAddressesInfo.Country = iCountryCode;
                            //objEntityXAddressesInfo.State = iStateId;
                            objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                            objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                            objEntityXAddressesInfo.Address.Addr3 = sAddr3;
                            objEntityXAddressesInfo.Address.Addr4 = sAddr4;
                            objEntityXAddressesInfo.Address.City = sCity;
                            objEntityXAddressesInfo.Address.Country = iCountryCode;
                            objEntityXAddressesInfo.Address.State = iStateId;
                            AddressForm objAddressForm = new AddressForm(m_fda);
                            sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                            objEntityXAddressesInfo.Address.SearchString = sSearchString;
                            objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, Entity.Context.RMDatabase.ConnectionString, base.ClientId);
                            objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
                            objEntityXAddressesInfo.Email = sEmailAddress;
                            objEntityXAddressesInfo.Fax = sFaxNumber;
                            //objEntityXAddressesInfo.County = sCounty;
                            //objEntityXAddressesInfo.ZipCode = sZipCode;
                            objEntityXAddressesInfo.Address.County = sCounty;
                            objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                            objEntityXAddressesInfo.EntityId = Entity.EntityId;
                            objEntityXAddressesInfo.PrimaryAddress = -1;
                            //objEntityXAddressesInfo.AddressId = -1;
                            //RMA-8753 nshah28(Added by ashish) END
                        }
                    }
                }
            }
            #endregion

            #region Update Phone Numbers
            if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
            {
                Entity.FormName = "OrgHierarchyForm"; //Mits 22497

                XmlNode objPhoneNumber = base.SysEx.SelectSingleNode("//PhoneNumbers");
                if (objPhoneNumber != null)
                {
                    string sPhoneData = objPhoneNumber.InnerText;
                    string[] sPhoneNumbers = sPhoneData.Split(new string[] { CommonForm.OUTER_PHONE_DELIMITER }, StringSplitOptions.None);
                    foreach (string sPhoneNumber in sPhoneNumbers)
                    {
                        string[] sNumber = sPhoneNumber.Split(new string[] { CommonForm.INNER_PHONE_DELIMITER }, StringSplitOptions.None);
                        if (sNumber.Length > 2)
                        {
                            string sPhoneId = sNumber[0];
                            string sPhoneType = sNumber[1];
                            string sPhoneNo = sNumber[2];
                            bool bPhoneCodeExists = false;
                            string sPhoneDesc = string.Empty;
                            string sPhoneShortCode = string.Empty;
                            if (Entity.AddressXPhoneInfoList.Count > 0)
                            {
                                foreach (AddressXPhoneInfo objAddressXPhoneInfo in Entity.AddressXPhoneInfoList)
                                {
                                    if (objAddressXPhoneInfo.PhoneCode == Convert.ToInt32(sPhoneType))
                                    {
                                        if (sPhoneNo.Trim() != string.Empty)
                                            objAddressXPhoneInfo.PhoneNo = sPhoneNo;
                                        else
                                            Entity.AddressXPhoneInfoList.Remove(objAddressXPhoneInfo.PhoneId);

                                        bPhoneCodeExists = true;
                                        break;
                                    }

                                }
                                if (!bPhoneCodeExists && sPhoneNo.Trim() != string.Empty)
                                {
                                    AddressXPhoneInfo objAddressXPhoneInfo = Entity.AddressXPhoneInfoList.AddNew();
                                    objAddressXPhoneInfo.PhoneId = Convert.ToInt32(sPhoneId);
                                    objAddressXPhoneInfo.PhoneCode = Convert.ToInt32(sPhoneType);
                                    objAddressXPhoneInfo.PhoneNo = sPhoneNo;
                                }
                            }
                            else
                            {
                                if (sPhoneNo.Trim() != string.Empty)
                                {
                                    AddressXPhoneInfo objAddressXPhoneInfo = Entity.AddressXPhoneInfoList.AddNew();
                                    objAddressXPhoneInfo.PhoneId = Convert.ToInt32(sPhoneId);
                                    objAddressXPhoneInfo.PhoneCode = Convert.ToInt32(sPhoneType);
                                    objAddressXPhoneInfo.PhoneNo = sPhoneNo;
                                }
                            }
                            //Added to save Office and Alt phone numbers in Entity Table as well
                            //sPhoneDesc = this.objCache.GetCodeDesc(Convert.ToInt32(sPhoneType)).Trim().ToLower();
                            //{
                            //    if (sPhoneDesc == "office")
                            //        Entity.Phone1 = sPhoneNo;
                            //    else if (sPhoneDesc == "home")
                            //        Entity.Phone2 = sPhoneNo;
                            //}
                            //Added to save Office and Alt phone numbers in Entity Table as well
                            //Aman ML Change
                            sPhoneShortCode = this.objCache.GetShortCode(Convert.ToInt32(sPhoneType)).Trim().ToLower();
                            {
                                if (sPhoneShortCode == "o")
                                    Entity.Phone1 = sPhoneNo;
                                else if (sPhoneShortCode == "h")
                                    Entity.Phone2 = sPhoneNo;
                            }
                            //Aman ML Change
                        }
                    }



                }
            }

            #endregion
            //Added Rakhi for R7:Add Emp Data Elements

            //sgoel6 Medicare 05/14/2009
            string sEntityMMSEATINEditFlag = 
                base.GetSysExDataNodeText("MMSEAEditFlag",true);
            if (sEntityMMSEATINEditFlag.ToLower() == "true")
                Entity.MMSEATINEditFlag = true;

            //if(Entity.EntityId!=0)
            //    return;
            //MITS:34276 : Starts -- Entity ID Update
            #region "Updating Entity Id Type"
            SortedList objEntityIdTypeAsList = new SortedList();

            XmlNode objEntityIdTypeAsNode = base.SysEx.DocumentElement.SelectSingleNode("EntityXEntityIDType");
            objTypeAttributeNode = null;

            bIsNew = false;
            int iEntityIdTypeKey = -1;

            XmlDocument objEntityXIDTypeXmlDoc = null;
            XmlElement objEntityXIDTypeRootElement = null;

            int iEntityIdTypeSelectedId = base.GetSysExDataNodeInt("EntityXEntityIDTypeSelectedId", true);

            string sEntityIdTypeRowAddedFlag = base.GetSysExDataNodeText("EntityXEntityIDTypeGrid_RowAddedFlag", true);
            bool bEntityIdTypeRowAddedFlag = sEntityIdTypeRowAddedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

            string sEntityIdTypeRowDeletedFlag = base.GetSysExDataNodeText("EntityXEntityIDTypeGrid_RowDeletedFlag", true);
            bool bEntityIdTypeRowDeletedFlag = sEntityIdTypeRowDeletedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

            if (objEntityIdTypeAsNode != null)
            {
                // Loop through data for all rows of the grid
                foreach (XmlNode objOptionNode in objEntityIdTypeAsNode.SelectNodes("option"))
                {
                    objTypeAttributeNode = objOptionNode.Attributes["type"];
                    bIsNew = false;
                    if (objTypeAttributeNode != null && objTypeAttributeNode.InnerText.Equals("new") == true)
                    {
                        // This is the 'extra' hidden <option> for capturing a new grid row data
                        bIsNew = true;
                    }
                    if ((bIsNew == false) || (bIsNew == true && bEntityIdTypeRowAddedFlag == true))
                    {
                        objEntityXIDTypeXmlDoc = new XmlDocument();
                        objEntityXIDTypeRootElement = objEntityXIDTypeXmlDoc.CreateElement("EntityXEntityIDType");
                        objEntityXIDTypeXmlDoc.AppendChild(objEntityXIDTypeRootElement);
                        objEntityXIDTypeRootElement.InnerXml = objOptionNode.InnerXml;

                        iEntityIdTypeKey = Conversion.ConvertStrToInteger(objEntityXIDTypeRootElement.SelectSingleNode("IdNumRowId").InnerText);

                        if ((bIsNew == true && bEntityIdTypeRowAddedFlag == true) || (iEntityIdTypeKey < 0 && objEntityXIDTypeRootElement.SelectSingleNode("EntityId").InnerText == ""))
                            objEntityXIDTypeRootElement.SelectSingleNode("EntityId").InnerText = "0";

                        if (bIsNew == false && bEntityIdTypeRowDeletedFlag == true && iEntityIdTypeKey == iEntityIdTypeSelectedId)
                        {
                            continue;
                        }

                        objEntityIdTypeAsList.Add(iEntityIdTypeKey, objEntityXIDTypeXmlDoc);
                    }
                }
            }

            foreach (EntityXEntityIDType objEntityXEntityIDTypeAs in Entity.EntityXEntityIDTypeList)
            {
                if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7" && (objEntityXEntityIDTypeAs.IdNumRowId < 0 || (bEntityIdTypeRowDeletedFlag == true && objEntityXEntityIDTypeAs.IdNumRowId == iEntityIdTypeSelectedId)))
                {
                    Entity.EntityXEntityIDTypeList.Remove(objEntityXEntityIDTypeAs.IdNumRowId);
                }
                else if (objEntityXEntityIDTypeAs.IdNumRowId < 0)
                    Entity.EntityXEntityIDTypeList.Remove(objEntityXEntityIDTypeAs.IdNumRowId);
            }


            EntityXEntityIDType objTmpEntityXEntityIDTypeAs = null;
            XmlDocument objTmpEntityXEntityIdTypeDoc = null;

            ArrayList arrEntityIdTypes = new ArrayList();

            for (int iListIndex = 0; iListIndex < objEntityIdTypeAsList.Count; iListIndex++)
            {
                objTmpEntityXEntityIdTypeDoc = (XmlDocument)objEntityIdTypeAsList.GetByIndex(iListIndex);

                if (objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText.Trim() == "" || Conversion.ConvertStrToInteger(objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText) < 0)
                {
                    objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText = ((-1 * iListIndex) - 1).ToString();
                    objTmpEntityXEntityIDTypeAs = Entity.EntityXEntityIDTypeList.AddNew();
                    objTmpEntityXEntityIDTypeAs.PopulateObject(objTmpEntityXEntityIdTypeDoc);
                }
                else
                {
                    objTmpEntityXEntityIDTypeAs = Entity.EntityXEntityIDTypeList[Conversion.ConvertStrToInteger(objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText)];

                    objTmpEntityXEntityIDTypeAs.PopulateObject(objTmpEntityXEntityIdTypeDoc);
                }

                arrEntityIdTypes.Add(objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText);
            }


            foreach (EntityXEntityIDType objEntityXEntityIDType in Entity.EntityXEntityIDTypeList)
            {
                if (!arrEntityIdTypes.Contains(objEntityXEntityIDType.IdNumRowId.ToString()))
                {
                    Entity.EntityXEntityIDTypeList.Remove(objEntityXEntityIDType.IdNumRowId);
                }
            }

            #endregion
            //MITS:34276 : Ends -- Entity ID Type Update
			
		}

		private XmlDocument GetEntityXOperatingAsData()
		{
			XmlDocument objEntityXOperatingAsXmlDoc = new XmlDocument();
 
			XmlElement objRootElement = objEntityXOperatingAsXmlDoc.CreateElement("EntityXOperatingAs"); 
			objEntityXOperatingAsXmlDoc.AppendChild(objRootElement);
			
			XmlElement objOptionXmlElement = null;
			XmlElement objXmlElement = null;
			XmlElement objListHeadXmlElement = null;
			XmlAttribute objXmlAttribute = null;
			int iEntityXOperatingAsCount = 0;

			objListHeadXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("listhead"); 
					
			objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("Initials");
            //objXmlElement.InnerText = "Abbreviation"; Commented Rakhi
            objXmlElement.InnerText = "Initial";
			objListHeadXmlElement.AppendChild(objXmlElement); 					
			objXmlElement = null;

			objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("OperatingAs");
            //objXmlElement.InnerText = "Operating Name"; Commented Rakhi
            objXmlElement.InnerText = "Name";
			objListHeadXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objRootElement.AppendChild(objListHeadXmlElement);
			objListHeadXmlElement = null;

			foreach(EntityXOperatingAs objEntityXOperatingAs in this.Entity.EntityXOperatingAsList)
			{
				
				iEntityXOperatingAsCount++;

				objOptionXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("option"); 
					
				objXmlAttribute = objEntityXOperatingAsXmlDoc.CreateAttribute("ref");  
				objXmlAttribute.InnerText =		INSTANCE_SYSEXDATA_PATH 
					+ "/" + objEntityXOperatingAsXmlDoc.DocumentElement.LocalName  
					+ "/option[" + iEntityXOperatingAsCount.ToString() + "]" ;
				objOptionXmlElement.Attributes.Append(objXmlAttribute);
				objXmlAttribute = null;

				objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("Initials");
				objXmlElement.InnerText = objEntityXOperatingAs.Initials;
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("OperatingAs");
				objXmlElement.InnerText = objEntityXOperatingAs.OperatingAs.ToString();
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				/************** Hidden Columns on the Grid ******************/
				//objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("Initial");
				//TODO MITS 9079
				//objXmlElement.InnerText = objEntityXOperatingAs.Initials;
				//objOptionXmlElement.AppendChild(objXmlElement); 
				//objXmlElement = null;

				objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("OperatingId");
				objXmlElement.InnerText = objEntityXOperatingAs.OperatingId.ToString();
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("EntityId");
				objXmlElement.InnerText = objEntityXOperatingAs.EntityId.ToString();	
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objRootElement.AppendChild(objOptionXmlElement);
				objOptionXmlElement = null;

			}
			
			iEntityXOperatingAsCount++;

			objOptionXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("option"); 
					
			objXmlAttribute = objEntityXOperatingAsXmlDoc.CreateAttribute("ref");  
			objXmlAttribute.InnerText =		INSTANCE_SYSEXDATA_PATH 
				+ "/" + objEntityXOperatingAsXmlDoc.DocumentElement.LocalName  
				+ "/option[" + iEntityXOperatingAsCount.ToString() + "]" ;
			objOptionXmlElement.Attributes.Append(objXmlAttribute);
			objXmlAttribute = null;

			objXmlAttribute = objEntityXOperatingAsXmlDoc.CreateAttribute("type"); 
			objXmlAttribute.InnerText = "new";
			objOptionXmlElement.Attributes.Append(objXmlAttribute);
			objXmlAttribute = null;

			objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("Initials");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("OperatingAs");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			/************** Hidden Columns on the Grid ******************/
			//objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("Initial");
			//objOptionXmlElement.AppendChild(objXmlElement); 
			//objXmlElement = null;

			objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("OperatingId");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("EntityId");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objRootElement.AppendChild(objOptionXmlElement);
			objOptionXmlElement = null;

			objRootElement = null;			
			return objEntityXOperatingAsXmlDoc;	
		}

		private XmlDocument GetEntityXContactInfoData()
		{
			XmlDocument objEntityXContactInfoXmlDoc = new XmlDocument();
 
			XmlElement objRootElement = objEntityXContactInfoXmlDoc.CreateElement("EntityXContactInfo"); 
			objEntityXContactInfoXmlDoc.AppendChild(objRootElement);
			
			XmlElement objOptionXmlElement = null;
			XmlElement objXmlElement = null;
			XmlElement objListHeadXmlElement = null;
			XmlAttribute objXmlAttribute = null;
            XmlAttribute objXmlContactIDAttr = null;
			int iEntityXContactInfoCount = 0;
			string sStateCode = "";
			string sStateDesc = "";
            string sLOBCode = "";
			string sLOBDesc = "";            
            string sLOB = "";
            //MITS:34276 Contact Type START
            string sContactCode = String.Empty;
            string sContactDesc = String.Empty;
            //MITS:34276 Contact Type END
           
			objListHeadXmlElement = objEntityXContactInfoXmlDoc.CreateElement("listhead");

            //MITS:34276 Contact Type START
            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ContactType");
            objXmlElement.InnerText = "Contact type";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //MITS:34276 Contact Type END

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ContactName");
			objXmlElement.InnerText = "Name";
			objListHeadXmlElement.AppendChild(objXmlElement); 					
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Title");
			objXmlElement.InnerText = "Title";
			objListHeadXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Initials");
			objXmlElement.InnerText = "Initials";
			objListHeadXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr1");
			objXmlElement.InnerText = "Address1";
			objListHeadXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr2");
			objXmlElement.InnerText = "Address2";
			objListHeadXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
            objXmlElement.InnerText = "Address3";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
            objXmlElement.InnerText = "Address4";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("City");
			objXmlElement.InnerText = "City";
			objListHeadXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("State");
			objXmlElement.InnerText = "State";
			objListHeadXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ZipCode");
			objXmlElement.InnerText = "Zip Code";
			objListHeadXmlElement.AppendChild(objXmlElement); 

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Phone");
			objXmlElement.InnerText = "Phone";
			objListHeadXmlElement.AppendChild(objXmlElement);

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Fax");
			objXmlElement.InnerText = "Fax No.";
			objListHeadXmlElement.AppendChild(objXmlElement);

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Email");
			objXmlElement.InnerText = "E-Mail";
			objListHeadXmlElement.AppendChild(objXmlElement);

            //addded by Navdeep - New Fields in Contact Info Screen - Same need to be mapped to Grid : Chubb req
            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("LineofBusList");
            objXmlElement.InnerText = "Line of Business";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("EmailAck");
            objXmlElement.InnerText = "E-Mail Ack";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("EmailFroi");
            objXmlElement.InnerText = "E-Mail FROI";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("EmailAcord");
            objXmlElement.InnerText = "E-Mail ACORD";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //end by Navdeep - Chubb
			objXmlElement = null;
			objRootElement.AppendChild(objListHeadXmlElement);
			objListHeadXmlElement = null;

			foreach(EntityXContactInfo objEntityXContactInfo in this.Entity.EntityXContactInfoList)
			{
				
				iEntityXContactInfoCount++;

				objOptionXmlElement = objEntityXContactInfoXmlDoc.CreateElement("option"); 
					
				objXmlAttribute = objEntityXContactInfoXmlDoc.CreateAttribute("ref");  
				objXmlAttribute.InnerText =		INSTANCE_SYSEXDATA_PATH 
					+ "/" + objEntityXContactInfoXmlDoc.DocumentElement.LocalName  
					+ "/option[" + iEntityXContactInfoCount.ToString() + "]" ;
				objOptionXmlElement.Attributes.Append(objXmlAttribute);
				objXmlAttribute = null;

                //MITS:34276 Contact Type START
                objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ContactType");
                this.objCache.GetCodeInfo(objEntityXContactInfo.ContactType, ref sContactCode, ref sContactDesc);
                objXmlElement.InnerText = sContactCode + " " + sContactDesc;
                objXmlAttribute = objEntityXContactInfoXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXContactInfo.ContactType.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;
                //MITS:34276 Contact Type END

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ContactName");
				objXmlElement.InnerText = objEntityXContactInfo.ContactName;
				objOptionXmlElement.AppendChild(objXmlElement); 					
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Title");
				objXmlElement.InnerText = objEntityXContactInfo.Title;
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Initials");
				objXmlElement.InnerText = objEntityXContactInfo.Initials;
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr1");
				objXmlElement.InnerText = objEntityXContactInfo.Addr1;
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr2");
				objXmlElement.InnerText = objEntityXContactInfo.Addr2;
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

                objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
                objXmlElement.InnerText = objEntityXContactInfo.Addr3;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
                objXmlElement.InnerText = objEntityXContactInfo.Addr4;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("City");
				objXmlElement.InnerText = objEntityXContactInfo.City;
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;
				
				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("State");
				this.objCache.GetStateInfo(objEntityXContactInfo.State,ref sStateCode, ref sStateDesc);
				objXmlElement.InnerText = sStateCode + " " + sStateDesc;
				objXmlAttribute = objEntityXContactInfoXmlDoc.CreateAttribute("codeid");  
				objXmlAttribute.InnerText =	objEntityXContactInfo.State.ToString();	
				objXmlElement.Attributes.Append(objXmlAttribute);					
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlAttribute = null;
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ZipCode");
				objXmlElement.InnerText = objEntityXContactInfo.ZipCode;
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Phone");
				objXmlElement.InnerText = objEntityXContactInfo.Phone;
				objOptionXmlElement.AppendChild(objXmlElement);
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Fax");
				objXmlElement.InnerText = objEntityXContactInfo.Fax;
				objOptionXmlElement.AppendChild(objXmlElement);
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Email");
				objXmlElement.InnerText = objEntityXContactInfo.Email;
				objOptionXmlElement.AppendChild(objXmlElement);
				objXmlElement = null;
                //addded by Navdeep - New Fields in Contact Info Screen - Same need to be mapped to Grid: Chubb requirement
                objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("LineofBusList");
                objXmlAttribute = objEntityXContactInfoXmlDoc.CreateAttribute("codeid");
                objXmlContactIDAttr = objEntityXContactInfoXmlDoc.CreateAttribute("contactid");

                //In RMWORLD Line of Business was stored comma seperated, so need to handle in those    
                //Geeta : Mits 18718 for Acord Comma Seperated List Issue
                foreach (System.Collections.DictionaryEntry objLOB in objEntityXContactInfo.LineofBusList)
                {
                    sLOB = Conversion.ConvertObjToStr(objLOB.Value);
                    this.objCache.GetCodeInfo(Riskmaster.Common.Conversion.ConvertStrToInteger(sLOB), ref sLOBCode, ref sLOBDesc);
                    objXmlElement.InnerText = sLOBCode + " " + objXmlElement.InnerText;
                    objXmlAttribute.InnerText = sLOB + " " + objXmlAttribute.InnerText;
                }                    
                objXmlContactIDAttr.InnerText =Convert.ToString(objEntityXContactInfo.ContactId);
                objXmlElement.Attributes.Append(objXmlAttribute);
                objXmlElement.Attributes.Append(objXmlContactIDAttr);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;

                objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("EmailAck");                
                objXmlElement.InnerText = objEntityXContactInfo.EmailAck.ToString();                         
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("EmailFroi");                
                objXmlElement.InnerText = objEntityXContactInfo.EmailFroi.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("EmailAcord");                
                objXmlElement.InnerText = objEntityXContactInfo.EmailAcord.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                // End by Navdeep

				/************** Hidden Columns on the Grid ******************/
				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ContactId");
				objXmlElement.InnerText = objEntityXContactInfo.ContactId.ToString();
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("EntityId");
				objXmlElement.InnerText = objEntityXContactInfo.EntityId.ToString();	
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objRootElement.AppendChild(objOptionXmlElement);
				objOptionXmlElement = null;

			}
			
			iEntityXContactInfoCount++;

			objOptionXmlElement = objEntityXContactInfoXmlDoc.CreateElement("option"); 
					
			objXmlAttribute = objEntityXContactInfoXmlDoc.CreateAttribute("ref");  
			objXmlAttribute.InnerText =		INSTANCE_SYSEXDATA_PATH 
				+ "/" + objEntityXContactInfoXmlDoc.DocumentElement.LocalName  
				+ "/option[" + iEntityXContactInfoCount.ToString() + "]" ;
			objOptionXmlElement.Attributes.Append(objXmlAttribute);
			objXmlAttribute = null;

			objXmlAttribute = objEntityXContactInfoXmlDoc.CreateAttribute("type"); 
			objXmlAttribute.InnerText = "new";
			objOptionXmlElement.Attributes.Append(objXmlAttribute);
			objXmlAttribute = null;

            //MITS:34276 Contact Type START
            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ContactType");
            objXmlAttribute = objEntityXContactInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;
            //MITS:34276 Contact Type END

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ContactName");
			objOptionXmlElement.AppendChild(objXmlElement); 					
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Title");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Initials");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr1");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr2");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("City");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("State");
			objXmlAttribute = objEntityXContactInfoXmlDoc.CreateAttribute("codeid");  
			objXmlAttribute.InnerText =	"-1";
			objXmlElement.Attributes.Append(objXmlAttribute);					
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlAttribute = null;
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ZipCode");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Phone");
			objOptionXmlElement.AppendChild(objXmlElement);
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Fax");
			objOptionXmlElement.AppendChild(objXmlElement);
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Email");
			objOptionXmlElement.AppendChild(objXmlElement);
			objXmlElement = null;

            //addded by Navdeep - New Fields in Contact Info Screen :chubb requirement
            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("LineofBusList");
            objXmlAttribute = objEntityXContactInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;            

            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("EmailAck");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("EmailFroi");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("EmailAcord");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //addded by Navdeep
			/************** Hidden Columns on the Grid ******************/
			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ContactId");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("EntityId");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objRootElement.AppendChild(objOptionXmlElement);
			objOptionXmlElement = null;

			objRootElement = null;			
			return objEntityXContactInfoXmlDoc;	
		}
        private XmlDocument GetClientLimitsInfoData()
        {
            XmlDocument objClientLimitsXmlDoc = new XmlDocument();

            XmlElement objRootElement = objClientLimitsXmlDoc.CreateElement("ClientLimits");
            objClientLimitsXmlDoc.AppendChild(objRootElement);

            XmlElement objOptionXmlElement = null;
            XmlElement objXmlElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlAttribute objXmlAttribute = null;
            int iClientLimitsCount = 0;
            string sCodeType = "";
            string sCodeDesc = "";
            string sTypeOfLimit = "";

            objListHeadXmlElement = objClientLimitsXmlDoc.CreateElement("listhead");

            objXmlElement = objClientLimitsXmlDoc.CreateElement("LobCode");
            objXmlElement.InnerText = "LOB";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objClientLimitsXmlDoc.CreateElement("ReserveTypeCode");
            objXmlElement.InnerText = "Reserve Type";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objClientLimitsXmlDoc.CreateElement("MaxAmount");
            objXmlElement.InnerText = "Max Amount";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objClientLimitsXmlDoc.CreateElement("PaymentFlag_Text");
            objXmlElement.InnerText = "Type Of Limit";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objRootElement.AppendChild(objListHeadXmlElement);
            objListHeadXmlElement = null;

            foreach (ClientLimits objClientLimits in this.Entity.ClientLimitsList)
            {

                iClientLimitsCount++;

                objOptionXmlElement = objClientLimitsXmlDoc.CreateElement("option");

                objXmlAttribute = objClientLimitsXmlDoc.CreateAttribute("ref");
                objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                    + "/" + objClientLimitsXmlDoc.DocumentElement.LocalName
                    + "/option[" + iClientLimitsCount.ToString() + "]";
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                objXmlAttribute = null;

                objXmlElement = objClientLimitsXmlDoc.CreateElement("LobCode");
                this.objCache.GetCodeInfo(objClientLimits.LobCode, ref sCodeType, ref sCodeDesc);
                objXmlElement.InnerText = sCodeType + " " + sCodeDesc;
                objXmlAttribute = objClientLimitsXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objClientLimits.LobCode.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;
                

                objXmlElement = objClientLimitsXmlDoc.CreateElement("ReserveTypeCode");              
                this.objCache.GetCodeInfo(objClientLimits.ReserveTypeCode, ref sCodeType, ref sCodeDesc,base.Adaptor.userLogin.objUser.NlsCode);  //Aman ML Change
                objXmlElement.InnerText = sCodeType + " " + sCodeDesc;
                objXmlAttribute = objClientLimitsXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objClientLimits.ReserveTypeCode.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;
                


                objXmlElement = objClientLimitsXmlDoc.CreateElement("MaxAmount");
                objXmlElement.InnerText = objClientLimits.MaxAmount.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objClientLimitsXmlDoc.CreateElement("PaymentFlag_Text");
                if (objClientLimits.PaymentFlag == 0)
                {
                    sTypeOfLimit = "Reserve";
                }
                else
                {
                    sTypeOfLimit = "Payment";
                }
                objXmlElement.InnerText = sTypeOfLimit;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                sTypeOfLimit = "";

                objXmlElement = objClientLimitsXmlDoc.CreateElement("ClientLimitsRowId");
                objXmlElement.InnerText = objClientLimits.ClientLimitsRowId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objClientLimitsXmlDoc.CreateElement("ClientEid");
                objXmlElement.InnerText = objClientLimits.ClientEid.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                objXmlElement = objClientLimitsXmlDoc.CreateElement("PaymentFlag");
                objXmlElement.InnerText = objClientLimits.PaymentFlag.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objRootElement.AppendChild(objOptionXmlElement);
                objOptionXmlElement = null;

            }

            iClientLimitsCount++;

            objOptionXmlElement = objClientLimitsXmlDoc.CreateElement("option");

            objXmlAttribute = objClientLimitsXmlDoc.CreateAttribute("ref");
            objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                + "/" + objClientLimitsXmlDoc.DocumentElement.LocalName
                + "/option[" + iClientLimitsCount.ToString() + "]";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            objXmlAttribute = objClientLimitsXmlDoc.CreateAttribute("type");
            objXmlAttribute.InnerText = "new";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            objXmlElement = objClientLimitsXmlDoc.CreateElement("LobCode");
            objXmlAttribute = objClientLimitsXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;
           

            objXmlElement = objClientLimitsXmlDoc.CreateElement("ReserveTypeCode");
            objXmlAttribute = objClientLimitsXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;
           

            objXmlElement = objClientLimitsXmlDoc.CreateElement("MaxAmount");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objClientLimitsXmlDoc.CreateElement("PaymentFlag_Text");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;


            objXmlElement = objClientLimitsXmlDoc.CreateElement("ClientLimitsRowId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objClientLimitsXmlDoc.CreateElement("ClientEid");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objClientLimitsXmlDoc.CreateElement("PaymentFlag");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objRootElement.AppendChild(objOptionXmlElement);
            objOptionXmlElement = null;

            objRootElement = null;
            return objClientLimitsXmlDoc;
        }

        //rsushilaggar: MITS 19832 Date: 02-Mar-2010
        /// <summary>
        /// override the function to check the entity abbreviation exists or not 
        /// </summary>
        /// <param name="Cancel"></param>
        public override void OnValidate(ref bool Cancel)
        {
            if (IsEntityAbbreExists(Entity.EntityId, Entity.EntityTableId, Entity.Abbreviation,Entity.ParentEid))
            {
                Errors.Add(Globalization.GetString("EntityAbbreExists", base.ClientId),
                    string.Format(Globalization.GetString("Entity.EntityAbbreExists", base.ClientId), "(No Clone Permission)"),
                    BusinessAdaptorErrorType.Error);
                Cancel = true;
            }
            bool bMulAddresses = this.objData.Context.InternalSettings.SysSettings.UseMultipleAddresses;
            if (bMulAddresses)
            {

                XmlNode PrimaryAddress = base.SysEx.DocumentElement.SelectSingleNode("PrimaryFlag");
                if (PrimaryAddress != null && Entity.EntityXAddressesList.Count != 0 && PrimaryAddress.InnerText.ToLower() == "false")
                {
                    Errors.Add("PrimaryAddressError", "No Address Marked as Primary for this Entity", BusinessAdaptorErrorType.Error);
                    Cancel = true;
                }
            }

            //MITS 34276 Starts:Check for Unique Entity ID Number and Entity ID Type Combination
            List<EntityXEntityIDType> lstEntityXIDType = new List<EntityXEntityIDType>();
            bool bDupEntityIDType = false;
            foreach (EntityXEntityIDType objEntityIDType in Entity.EntityXEntityIDTypeList)
            {
                if (!bDupEntityIDType && Riskmaster.Common.CommonFunctions.IsEntityIDNumberExists(Entity.Context.RMDatabase.ConnectionString, objEntityIDType.IdNumRowId, objEntityIDType.Table, objEntityIDType.IdNum, objEntityIDType.IdType))
                {
                    Errors.Add("EntityIDNumberError", "Entity ID Number with Entity ID Type already exists",
                        BusinessAdaptorErrorType.Error);
                    Cancel = true;
                }
                if (!bDupEntityIDType && lstEntityXIDType.Exists(x => x.IdType == objEntityIDType.IdType && x.IdNum == objEntityIDType.IdNum))
                {
                    Errors.Add("EntityIDNumberError", "Entity ID Number with Entity ID Type already exists",
                         BusinessAdaptorErrorType.Error);
                    Cancel = true;
                    bDupEntityIDType = true;
                }
                else
                    lstEntityXIDType.Add(objEntityIDType);
            }
            //MITS 34276 Ends:Check for Unique Entity ID Number and Entity ID Type Combination
        }
        /// <summary>
        /// This function is to check whether the org hierarchy abbreviation exists or not.
        /// </summary>
        /// <param name="p_iEntityId"></param>
        /// <param name="p_iTableId"></param>
        /// <param name="p_sAbbrevation"></param>
        /// <param name="p_iParentEid"></param>
        /// <returns></returns>
        public bool IsEntityAbbreExists(int p_iEntityId, int p_iTableId, string p_sAbbrevation,int p_iParentEid)
        {
            bool IsAbbreExists = false;
            //string sql = string.Format("SELECT ENTITY_ID FROM ENTITY WHERE ABBREVIATION = '{0}' AND ENTITY_TABLE_ID = {1} AND PARENT_EID= {2} AND DELETED_FLAG = 0", p_sAbbrevation, p_iTableId, p_iParentEid);
            //if (p_iEntityId != 0)
            //    sql += string.Format(" AND ENTITY_ID <> {0}",p_iEntityId);

            //MITS 20731 Raman Bhatia: 05/11/2010
            //If single quotes are there is abbreviation then sql fails
            
            DbParameter objParam = null;
            DbCommand objCommand = null;
            objCommand = Entity.Context.DbConn.CreateCommand();
            string sql = string.Format("SELECT ENTITY_ID FROM ENTITY WHERE ABBREVIATION = ~PABR~ AND ENTITY_TABLE_ID = {0} AND PARENT_EID= {1} AND DELETED_FLAG = 0", p_iTableId, p_iParentEid);
            if (p_iEntityId != 0)
                sql += string.Format(" AND ENTITY_ID <> {0}",p_iEntityId);
            objCommand.Parameters.Clear();
            objParam = objCommand.CreateParameter();
            objParam.Direction = System.Data.ParameterDirection.Input;
            objParam.Value = p_sAbbrevation;
            objParam.ParameterName = "PABR";
            objParam.SourceColumn = "ABBREVIATION";
            objCommand.Parameters.Add(objParam);
            objCommand.CommandText = sql;
            using (DbReader objReader = objCommand.ExecuteReader())
            {
                while (objReader.Read())
                    {
                        IsAbbreExists = true;
                    }
                }
        
    
            
            //using (DbReader objReader = Entity.Context.DbConn.ExecuteReader(sql))
            //{
            //    while (objReader.Read())
            //    {
            //        IsAbbreExists = true;
            //    }
            //}
            return IsAbbreExists;
        }
        //END: rsushilaggar
        private XmlDocument GetEntityXAddressInfo()
        {
            XmlElement objOptionXmlElement = null;
            XmlElement objXmlElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlAttribute objXmlAttribute = null;
            int iEntityXAddressInfoCount = 0;
            string sStateCode = string.Empty;
            string sStateDesc = string.Empty;
            string sCountryCode = string.Empty;
            string sCountryDesc = string.Empty;
            string sPrimaryAddressRef = string.Empty;
            string p_ShortCode = String.Empty; //MITS:34276 Entity Address Type
            string p_sDesc = String.Empty;  //MITS:34276 Entity Address Type

            XmlDocument objEntityXAddressInfoXmlDoc = new XmlDocument();
            XmlElement objRootElement = objEntityXAddressInfoXmlDoc.CreateElement("EntityXAddressesInfo");
            objEntityXAddressInfoXmlDoc.AppendChild(objRootElement);

            objListHeadXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("listhead");

            //MITS:34276 Entity Address Type START
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressType");
            objXmlElement.InnerText = "Address Type";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //MITS:34276 Entity Address Type END

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr1");
            objXmlElement.InnerText = "Address1";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr2");
            objXmlElement.InnerText = "Address2";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
            objXmlElement.InnerText = "Address3";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
            objXmlElement.InnerText = "Address4";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("City");
            objXmlElement.InnerText = "City";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("State");
            objXmlElement.InnerText = "State";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Country");
            objXmlElement.InnerText = "Country";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("County");
            objXmlElement.InnerText = "County";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ZipCode");
            objXmlElement.InnerText = "Zip Code";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Email");
            objXmlElement.InnerText = "E-Mail";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Fax");
            objXmlElement.InnerText = "Fax No.";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PrimaryAddress");
            objXmlElement.InnerText = "Primary Address";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            //JIRA:6865 START:
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EffectiveDate");
            objXmlElement.InnerText = "Effective Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ExpirationDate");
            objXmlElement.InnerText = "Expiration Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //JIRA:6865 END:

            objRootElement.AppendChild(objListHeadXmlElement);
            objListHeadXmlElement = null;


            foreach (EntityXAddresses objEntityXAddressInfo in this.Entity.EntityXAddressesList)
            {

                iEntityXAddressInfoCount++;

                objOptionXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("option");

                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("ref");
                objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                    + "/" + objEntityXAddressInfoXmlDoc.DocumentElement.LocalName
                    + "/option[" + iEntityXAddressInfoCount.ToString() + "]";
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                sPrimaryAddressRef = objXmlAttribute.InnerText;
                objXmlAttribute = null;

                //MITS:34276 Entity Address Type START
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressType");
                this.objCache.GetCodeInfo(objEntityXAddressInfo.AddressType, ref p_ShortCode, ref p_sDesc);
                objXmlElement.InnerText = p_ShortCode + " " + p_sDesc;
                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXAddressInfo.AddressType.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;
                //MITS:34276 Entity Address Type END

                //RMA-8753 nshah28(Added by ashish) START
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr1");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr1;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr2");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr2;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr3;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr4;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("City");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.City;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("State");
                this.objCache.GetStateInfo(objEntityXAddressInfo.Address.State, ref sStateCode, ref sStateDesc);
                objXmlElement.InnerText = sStateCode + " " + sStateDesc;
                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXAddressInfo.Address.State.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Country");
                this.objCache.GetCodeInfo(objEntityXAddressInfo.Address.Country, ref sCountryCode, ref sCountryDesc, base.Adaptor.userLogin.objUser.NlsCode);
                objXmlElement.InnerText = sCountryCode + " " + sCountryDesc;
                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXAddressInfo.Address.Country.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("County");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.County;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;


                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ZipCode");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.ZipCode;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                //RMA-8753 nshah28(Added by ashish) END

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Email");
                objXmlElement.InnerText = objEntityXAddressInfo.Email;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Fax");
                objXmlElement.InnerText = objEntityXAddressInfo.Fax;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PrimaryAddress");
                //int iPrimaryAddress = objEntityXAddressInfo.PrimaryAddress;
                int iPrimaryAddress = Convert.ToInt32(objEntityXAddressInfo.PrimaryAddress); //RMA-8753 nshah28
                if (iPrimaryAddress == -1)
                    objXmlElement.InnerText = "True";
                else
                    objXmlElement.InnerText = "False";
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                //JIRA:6865 START:
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EffectiveDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXAddressInfo.EffectiveDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ExpirationDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXAddressInfo.ExpirationDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                //JIRA:6865 END:

                /************** Hidden Columns on the Grid ******************/
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("NewPrimaryRecord");//for identication of changed Primary Address
                objXmlElement.InnerText = "False";
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressId");
                objXmlElement.InnerText = objEntityXAddressInfo.AddressId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                //AA//AA //RMA-8753 nshah28
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("RowId");
                objXmlElement.InnerText = objEntityXAddressInfo.RowId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                //AA //RMA-8753 nshah28 END
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EntityId");
                objXmlElement.InnerText = objEntityXAddressInfo.EntityId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;


                //XmlDocument objAddressXPhoneInfoXmlDoc = GetAddressXPhoneInfo(objEntityXAddressInfo);
                //objOptionXmlElement.AppendChild(objEntityXAddressInfoXmlDoc.ImportNode(objAddressXPhoneInfoXmlDoc.SelectSingleNode("AddressXPhoneInfo"), true));

                objRootElement.AppendChild(objOptionXmlElement);
                objOptionXmlElement = null;


            }

            iEntityXAddressInfoCount++;

            objOptionXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("option");

            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("ref");
            objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                + "/" + objEntityXAddressInfoXmlDoc.DocumentElement.LocalName
                + "/option[" + iEntityXAddressInfoCount.ToString() + "]";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("type");
            objXmlAttribute.InnerText = "new";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            //MITS:34276 Entity Address Type START
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressType");
            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;
            //MITS:34276 Entity Address Type END

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr1");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr2");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("City");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("State");
            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Country");
            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("County");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ZipCode");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Email");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Fax");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PrimaryAddress");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            //JIRA:6865 START:
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EffectiveDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ExpirationDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //JIRA:6865 END:

            /************** Hidden Columns on the Grid ******************/
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("NewPrimaryRecord"); //for identication of changed Primary Address
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            //AA //RMA-8753 nshah28
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("RowId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //AA //RMA-8753 nshah28 END
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EntityId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            #region PhoneInfo


            //            XmlElement objInnerXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressXPhoneInfo");
            //            objOptionXmlElement.AppendChild(objInnerXmlElement);

            //            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("listhead");
            //            string sNode = @"<PhoneCode>Phone Code</PhoneCode>
            //                             <PhoneNo>Phone Number</PhoneNo>";
            //            objXmlElement.InnerXml = sNode;
            //            objInnerXmlElement.AppendChild(objXmlElement);

            //            XmlElement objInnerOptionXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("option");

            //            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("ref");
            //            objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
            //                + "/AddressXPhoneInfo" + "/option[" + iEntityXAddressInfoCount.ToString() + "]";
            //            objInnerOptionXmlElement.Attributes.Append(objXmlAttribute);
            //            objXmlAttribute = null;

            //            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("type");
            //            objXmlAttribute.InnerText = "new";
            //            objInnerOptionXmlElement.Attributes.Append(objXmlAttribute);
            //            objXmlAttribute = null;

            //            objInnerXmlElement.AppendChild(objInnerOptionXmlElement);

            //            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PhoneCode");
            //            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
            //            objXmlAttribute.InnerText = "-1";
            //            objXmlElement.Attributes.Append(objXmlAttribute);
            //            objInnerOptionXmlElement.AppendChild(objXmlElement);
            //            objXmlAttribute = null;
            //            objXmlElement = null;

            //            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PhoneNo");
            //            objInnerOptionXmlElement.AppendChild(objXmlElement);
            //            objXmlElement = null;

            //            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ContactId");
            //            objInnerOptionXmlElement.AppendChild(objXmlElement);
            //            objXmlElement = null;

            //            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PhoneId");
            //            objInnerOptionXmlElement.AppendChild(objXmlElement);
            //            objXmlElement = null;

            #endregion //PhoneInfo

            objRootElement.AppendChild(objOptionXmlElement);
            objOptionXmlElement = null;

            objRootElement = null;
            return objEntityXAddressInfoXmlDoc;
        }

        #region GetAddressXPhoneInfo
        //private XmlDocument GetAddressXPhoneInfo(EntityXAddresses objEntityXAddresses)
        //{
        //    XmlDocument objAddressXPhoneInfoXmlDoc = new XmlDocument();

        //    XmlElement objRootElement = objAddressXPhoneInfoXmlDoc.CreateElement("AddressXPhoneInfo");
        //    objAddressXPhoneInfoXmlDoc.AppendChild(objRootElement);

        //    XmlElement objOptionXmlElement = null;
        //    XmlElement objXmlElement = null;
        //    XmlElement objListHeadXmlElement = null;
        //    XmlAttribute objXmlAttribute = null;
        //    int iAddressXPhoneInfoCount = 0;
        //    string sPhoneCode = "";
        //    string sPhoneDesc = "";



        //    objListHeadXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("listhead");


        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneCode");
        //    objXmlElement.InnerText = "Phone Code";
        //    objListHeadXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneNo");
        //    objXmlElement.InnerText = "Phone Number";
        //    objListHeadXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objRootElement.AppendChild(objListHeadXmlElement);
        //    objListHeadXmlElement = null;


        //    foreach (AddressXPhoneInfo objAddressXPhoneInfo in objEntityXAddresses.AddressXPhoneInfoList)
        //    {

        //        iAddressXPhoneInfoCount++;

        //        objOptionXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("option");

        //        objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("ref");
        //        objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
        //            + "/" + objAddressXPhoneInfoXmlDoc.DocumentElement.LocalName
        //            + "/option[" + iAddressXPhoneInfoCount.ToString() + "]";
        //        objOptionXmlElement.Attributes.Append(objXmlAttribute);
        //        objXmlAttribute = null;



        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneCode");
        //        this.objCache.GetCodeInfo(objAddressXPhoneInfo.PhoneCode, ref sPhoneCode, ref sPhoneDesc);
        //        objXmlElement.InnerText = sPhoneCode + " " + sPhoneDesc;
        //        objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("codeid");
        //        objXmlAttribute.InnerText = objAddressXPhoneInfo.PhoneCode.ToString();
        //        objXmlElement.Attributes.Append(objXmlAttribute);
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlAttribute = null;
        //        objXmlElement = null;


        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneNo");
        //        objXmlElement.InnerText = objAddressXPhoneInfo.PhoneNo;
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlElement = null;


        //        /************** Hidden Columns on the Grid ******************/
        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("ContactId");
        //        objXmlElement.InnerText = objAddressXPhoneInfo.ContactId.ToString();
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlElement = null;

        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneId");
        //        objXmlElement.InnerText = objAddressXPhoneInfo.PhoneId.ToString();
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlElement = null;

        //        objRootElement.AppendChild(objOptionXmlElement);
        //        objOptionXmlElement = null;

        //    }

        //    iAddressXPhoneInfoCount++;

        //    objOptionXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("option");

        //    objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("ref");
        //    objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
        //        + "/" + objAddressXPhoneInfoXmlDoc.DocumentElement.LocalName
        //        + "/option[" + iAddressXPhoneInfoCount.ToString() + "]";
        //    objOptionXmlElement.Attributes.Append(objXmlAttribute);
        //    objXmlAttribute = null;

        //    objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("type");
        //    objXmlAttribute.InnerText = "new";
        //    objOptionXmlElement.Attributes.Append(objXmlAttribute);
        //    objXmlAttribute = null;


        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneCode");
        //    objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("codeid");
        //    objXmlAttribute.InnerText = "-1";
        //    objXmlElement.Attributes.Append(objXmlAttribute);
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlAttribute = null;
        //    objXmlElement = null;


        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneNo");
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    /************** Hidden Columns on the Grid ******************/
        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("ContactId");
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneId");
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objRootElement.AppendChild(objOptionXmlElement);
        //    objOptionXmlElement = null;

        //    objRootElement = null;
        //    return objAddressXPhoneInfoXmlDoc;
        //}
        #endregion //GetAddressXPhoneInfo

        private void SetPrimaryAddressReadOnly()
        {

            base.AddReadOnlyNode("addr1");
            base.AddReadOnlyNode("addr2");
            base.AddReadOnlyNode("addr3");// JIRA 6420 pkandhari
            base.AddReadOnlyNode("addr4");// JIRA 6420 pkandhari
            base.AddReadOnlyNode("city");
            base.AddReadOnlyNode("countrycode");
            base.AddReadOnlyNode("county");
            base.AddReadOnlyNode("stateid");
            base.AddReadOnlyNode("emailaddress");
            base.AddReadOnlyNode("faxnumber");
            base.AddReadOnlyNode("zipcode");

        }
        private string PopulatePhoneNumbers()
        {

            XmlDocument SysExDoc = new XmlDocument();
            string sNumber = string.Empty;
            foreach (AddressXPhoneInfo objPhoneInfo in Entity.AddressXPhoneInfoList)
            {
                //Added to set Default types to office and home
                if (objPhoneInfo.PhoneCode == iOfficeType)
                {
                    sOfficePhone = objPhoneInfo.PhoneNo;

                }
                else if (objPhoneInfo.PhoneCode == iHomeType)
                {
                    sHomePhone = objPhoneInfo.PhoneNo;
                }
                //Added to set Default types to office and home
                if (String.IsNullOrEmpty(sNumber))
                {
                    sNumber = objPhoneInfo.PhoneId + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneCode + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneNo;
                }
                else
                {
                    sNumber += CommonForm.OUTER_PHONE_DELIMITER + objPhoneInfo.PhoneId + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneCode + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneNo;
                }
                XmlNode objNode = base.SysEx.SelectSingleNode("//PhoneTypeList");
                if (objNode != null)
                {
                    foreach (XmlNode node in objNode.ChildNodes)
                    {
                        if (objPhoneInfo.PhoneCode.ToString() == node.Attributes["value"].Value)
                        {
                            node.InnerText += " " + CommonForm.MARK_PHONECODE;
                            break;
                        }
                    }
                }

            }

            return sNumber;
        }

        //MITS:34276-- Entity ID Type tab display start
        private XmlDocument GetEntityIdTypeInfoData()
        {
            XmlDocument objEntityIDTypeAsXmlDoc = new XmlDocument();

            XmlElement objRootElement = objEntityIDTypeAsXmlDoc.CreateElement("EntityXEntityIDType");
            //achouhan3 05-may-2013     MITS #34276 Entity View Permission starts
            XmlAttribute objXmlPermissionAttribute = null;
            if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_VIEW))
            {

                objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsViewable");
                objXmlPermissionAttribute.InnerText = "false";
                objRootElement.Attributes.Append(objXmlPermissionAttribute);
                objXmlPermissionAttribute = null;
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);
            }
            else
            {
                if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_NEW))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsNew");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);

                if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_EDIT))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsUpdate");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);

                if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_DELETE))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsDelete");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);
                if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_NEW))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorNew");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);
            }
            //achouhan3 05-may-2013    MITS #34276  Entity View Permission Ends 

            XmlElement objOptionXmlElement = null;
            XmlElement objXmlElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlAttribute objXmlAttribute = null;
            int iEntityIDTypeAsCount = 0;
            string sIDCode = string.Empty;
            string sIDDesc = string.Empty;

            objListHeadXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("listhead");

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdType");
            objXmlElement.InnerText = "Entity ID Type";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNum");
            objXmlElement.InnerText = "Entity ID Number";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffStartDate");
            objXmlElement.InnerText = "Effective Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffEndDate");
            objXmlElement.InnerText = "Expiration Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objRootElement.AppendChild(objListHeadXmlElement);
            objListHeadXmlElement = null;

            foreach (EntityXEntityIDType objEntityXEntityIDType in this.Entity.EntityXEntityIDTypeList)
            {
                iEntityIDTypeAsCount++;

                objOptionXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("option");

                objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("ref");
                objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH + "/" + objEntityIDTypeAsXmlDoc.DocumentElement.LocalName + "/option[" + iEntityIDTypeAsCount.ToString() + "]";
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                objXmlAttribute = null;
                //achouhan3 05-may-2013    MITS #34276  ID Type Vendor Permission Starts 
                this.objCache.GetCodeInfo(objEntityXEntityIDType.IdType, ref sIDCode, ref sIDDesc);              
                if (sIDCode.Trim().ToUpper() == "VENDOR")
                {
                    if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_VIEW))
                    {
                        objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorView");
                        objXmlAttribute.InnerText = "false";
                        objOptionXmlElement.Attributes.Append(objXmlAttribute);
                        objXmlAttribute = null;
                    }                    
                    if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_UPDATE))
                    {
                        objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorUpdate");
                        objXmlAttribute.InnerText = "false";
                        objOptionXmlElement.Attributes.Append(objXmlAttribute);
                        objXmlAttribute = null;
                    }
                    if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_DELETE))
                    {
                        objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorDelete");
                        objXmlAttribute.InnerText = "false";
                        objOptionXmlElement.Attributes.Append(objXmlAttribute);
                        objXmlAttribute = null;
                    }
                }
                //achouhan3 05-may-2013    MITS #34276  ID Type Vendor Permission Ends 
                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdType");
                this.objCache.GetCodeInfo(objEntityXEntityIDType.IdType, ref sIDCode, ref sIDDesc);
                objXmlElement.InnerText = sIDCode + " " + sIDDesc;
                objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXEntityIDType.IdType.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNum");
                objXmlElement.InnerText = objEntityXEntityIDType.IdNum.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffStartDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXEntityIDType.EffStartDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffEndDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXEntityIDType.EffEndDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                /************** Hidden Columns on the Grid ******************/
                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNumRowId");
                objXmlElement.InnerText = objEntityXEntityIDType.IdNumRowId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EntityId");
                objXmlElement.InnerText = objEntityXEntityIDType.EntityId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objRootElement.AppendChild(objOptionXmlElement);
                objOptionXmlElement = null;

            }

            iEntityIDTypeAsCount++;

            objOptionXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("option");

            objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("ref");
            objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                + "/" + objEntityIDTypeAsXmlDoc.DocumentElement.LocalName
                + "/option[" + iEntityIDTypeAsCount.ToString() + "]";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("type");
            objXmlAttribute.InnerText = "new";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdType");
            objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNum");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffStartDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffEndDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;


            /************** Hidden Columns on the Grid ******************/
            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNumRowId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EntityId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objRootElement.AppendChild(objOptionXmlElement);
            objOptionXmlElement = null;

            objRootElement = null;
            return objEntityIDTypeAsXmlDoc;

        }
        //MITS:34276-- Entity ID Type tab display end
        /// <summary>
        /// override the BeforeSave function to set entity approval status "Pending" if it is 0 or "".
        /// </summary>
        /// <param name="Cancel"></param>
        public override void BeforeSave(ref bool Cancel)
        {
            //Start rsushilaggar Entity Payment Approval(make the people entity approval status as pending approval) MITS 20606 05/05/2010
            if (Entity.Context.InternalSettings.SysSettings.UseEntityApproval)
            {
                if (Entity.EntityApprovalStatusCode == 0)
                {
                    Entity.EntityApprovalStatusCode = Entity.Context.InternalSettings.CacheFunctions.GetCodeIDWithShort("P", "ENTITY_APPRV_REJ");
                }
            }
            //End rsushilaggar
        }
                
	}
}