﻿

using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary> neha added on 08/24/2011
    /// Summary description for Case Management Screen. 
    /// </summary>
    public class CaseManagementForm : DataEntryFormBase
    {
        const string CLASS_NAME = "CaseManagement";
		private CaseManagement CaseManagement{get{return objData as CaseManagement;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "PIROWID";
		private int m_PiRowId = 0;
        //Neha Start
        int iUseCaseMgmt = 0;
        int iUseUnitStat = 0;
        int iCaseMgmtHisId = 0;
        string[] sDaysOfWeek = new string[7];
        bool bIsEmployee=false;
        public struct Holidays
        {
            internal string sDate;
            internal string sDesc;
            internal long[] lOrgID;
        }
        public Holidays[] SYS_HOLIDAYS;
        //Mridul 05/28/09 MITS 16745-Chubb ACK Letter Enhancement
        //neha End
		
        public override void Init()
        {
            base.Init();
            iUseCaseMgmt = Conversion.ConvertStrToInteger(CaseManagement.Context.DbConn.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'CASE_MGT_FLAG'"));

            //BOB Reserve Enhancement
            iUseUnitStat = Conversion.ConvertStrToInteger(CaseManagement.Context.DbConn.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'UNIT_STAT'"));

        }
		public override void InitNew()
		{
			base.InitNew(); 
           
           this.m_PiRowId = base.GetSysExDataNodeInt("/SysExData/PiRowId", false);
           string sFilter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_PiRowId;
			
			

			(objData as INavigation).Filter = sFilter;

            this.CaseManagement.PiRowId = this.m_PiRowId;
			

			

		}



		public CaseManagementForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

        
        
        public override void AfterSave()
        {
            
            base.ResetSysExData("DupSSN", "");            
        }

        
       
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
            XmlDocument objXML;
          
			XmlElement objWeeksPerMonth = null;       



			if (this.SecurityId!=RMB_WC_PI && this.SecurityId!=RMB_DI_PI)
			{
				if(!CaseManagement.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_PI_EMPLOYEE))
				{
					LogSecurityError(RMO_ACCESS);
					return;
				}
			}
            
			XmlDocument objSysExDataXmlDoc = base.SysEx;

			string sSubTitleText = string.Empty;
            base.ResetSysExData("SubTitle",sSubTitleText);
       		//Raman Bhatia - LTD Phase 3 changes : Weeks Per Month should be passed via a node in SYSEX
            objXML = base.SysEx;
			string sSQL =	"SELECT NO_WEEKS_PER_MONTH FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = 844";

			DbReader objReader = null;
            using (objReader = objData.Context.DbConnLookup.ExecuteReader(sSQL))
            {

               
                objWeeksPerMonth = objXML.CreateElement("weekspermonth");
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        objWeeksPerMonth.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }

                }
            }
			objXML.DocumentElement.AppendChild(objWeeksPerMonth);
            string sWorkFlags = string.Empty;
            PiEmployee parentPi = (CaseManagement.Parent as PiEmployee);
            parentPi.PopulateObject(Utilities.XPointerDoc("//Parent/Instance", base.SysEx), true);
            parentPi.PiRowId = CaseManagement.PiRowId;
            if (parentPi != null)
                sWorkFlags = parentPi.WorkSunFlag + ","
                    + parentPi.WorkMonFlag + ","
                    + parentPi.WorkTueFlag + ","
                    + parentPi.WorkWedFlag + ","
                    + parentPi.WorkThuFlag + ","
                    + parentPi.WorkFriFlag + ","
                    + parentPi.WorkSatFlag;

            base.ResetSysExData("WorkFlags", sWorkFlags);

            sDaysOfWeek = sWorkFlags.Split(",".ToCharArray());
            //neha start
            LoadPIChilds(ref objXML);
            //nehaend
            XmlNode objOld = null;
            XmlElement objNew = null;

            objOld = objXML.SelectSingleNode("//SysExData/Parent");
            objNew = objXML.CreateElement("Parent");
            objNew.InnerXml = (CaseManagement.Parent as DataObject).SerializeObject();

            if (objOld != null)
            {
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            }
            else
                objXML.DocumentElement.AppendChild(objNew);
            XmlNode objDiaryMessageNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/DiaryMessage");
            
            if (objDiaryMessageNode == null)
            {
                base.CreateSysExData("DiaryMessage");
            }
            XmlNode objPirowid = objSysExDataXmlDoc.SelectSingleNode("/SysExData/PiRowId");

            if (objPirowid == null)
            {
                base.CreateSysExData("PiRowId",CaseManagement.PiRowId.ToString());
            }
            objSysExDataXmlDoc = null;
		}

		internal override bool ValidateRequiredViewFields(XmlDocument propertyStore)
		{
			return base.ValidateRequiredViewFields (propertyStore);
		}

		
		public override void OnValidate(ref bool Cancel)
		{
			

          
		}

       
        //neha start
        private bool DiagHistIsModified(PiXDiagHist objOrig, PiXDiagHist objNew)
        {
            if (objNew == null)
                return false;
            if (objOrig == null)
            {
                if (objNew.DiagnosisCode != 0)
                    return true;
                else
                    return false;
            }
            if (objOrig.DiagnosisCode != objNew.DiagnosisCode) return true;
            if (objOrig.DateChanged != objNew.DateChanged) return true;
            if (objOrig.ChgdByUser != objNew.ChgdByUser) return true;
                   if (objOrig.DisMdaTopic != objNew.DisMdaTopic) return true;
            if (objOrig.DisFactor != objNew.DisFactor) return true;
            if (objOrig.ApprovedByUser != objNew.ApprovedByUser) return true;
            if (objOrig.Reason != objNew.Reason) return true;

            return false;
        }
        //neha end
    
        public override void OnUpdateObject()
        {
            m_DataChanged = true;
            XmlDocument objXML = base.SysEx;
            //Storing Current SysEx in an xmldocument
            PiEmployee parentPi = (CaseManagement.Parent as PiEmployee);
            XmlElement oCurSysExele = (XmlElement)base.SysEx.SelectSingleNode("//Parent/Instance");
            if (oCurSysExele != null)
            {
                string sCurrentSysEx = oCurSysExele.InnerXml;
                XmlDocument oCurrentSysEx = new XmlDocument();
                oCurrentSysEx.InnerXml = sCurrentSysEx;

                //Preparing SysEx from Personinvovlved

                base.SysEx.SelectSingleNode("//Parent").InnerXml = parentPi.SerializeObject();
                base.CopyNodes(ref objXML, oCurrentSysEx, "PiEmployee");
            }

            base.OnUpdateObject();
            if (CaseManagement.PiRowId != parentPi.PiRowId)
			{
				CaseManagement.Parent=null;
                parentPi = (CaseManagement.Parent as PiEmployee);
			}
            parentPi.PopulateObject(Utilities.XPointerDoc("//Parent/Instance", base.SysEx), true);
            parentPi.PiRowId = CaseManagement.PiRowId;
            CaseManagement.DataChanged = true;

        //    XmlDocument objDoc = new XmlDocument();
            if (iUseCaseMgmt == -1)
            {

                //XmlElement objDiagHistElem1 = (XmlElement)objXML.SelectSingleNode("/SysExData/PiXDiagHistList");
                //if (objDiagHistElem1 != null)
                //{
                //    XmlDocument objDoc = new XmlDocument();
                //    objDoc.LoadXml(objXML.SelectSingleNode("/SysExData/PiXDiagHistList").InnerXml);
                //    PiXDiagHist objPiXDiagHist = (PiXDiagHist)CaseManagement.Context.Factory.GetDataModelObject("PiXDiagHist", false);
                //    objPiXDiagHist.PopulateObject(objDoc);
                //    PiXDiagHist objMostRecent = parentPi.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHist();

                //    if (DiagHistIsModified(objMostRecent, objPiXDiagHist))
                //    {
                //        objPiXDiagHist.PiXDiagrowId = -1;
                //        objPiXDiagHist.ChgdByUser = CaseManagement.Context.RMUser.LoginName;
                //        // npadhy Start MITS 18544 Do not default the Date to today's date. Rather We should save the Date which is passed from UI
                //        //objPiXDiagHist.DateChanged = System.DateTime.Today.ToShortDateString();
                //        // npadhy End MITS 18544 
                //        parentPi.PiXDiagHistList.Add(objPiXDiagHist);
                //    }
                //}

                try
                {
                    bool addICD10 = false;
                    bool addICD9 = false;
                    string ICDFlag = string.Empty;
                    if (objXML.SelectSingleNode("//SysExData//ICDFlag") != null)
                        ICDFlag = objXML.SelectSingleNode("//SysExData//ICDFlag").InnerXml.ToString();
                    else
                        ICDFlag = "0";
                    PiXDiagHist objPiXDiagHist = null;
                    PiXDiagHist objPiXDiagHist10 = null;

                    XmlElement objDiagHistElem = (XmlElement)objXML.SelectSingleNode("/SysExData/PiXDiagHistList");
                    if (objDiagHistElem != null)
                    {
                        XmlDocument objDoc = new XmlDocument();
                        objDoc.LoadXml(objXML.SelectSingleNode("/SysExData/PiXDiagHistList").InnerXml);
                        objPiXDiagHist = (PiXDiagHist)CaseManagement.Context.Factory.GetDataModelObject("PiXDiagHist", false);
                        objPiXDiagHist.PopulateObject(objDoc);
                        //srajindersin MITS 36903 12/08/2014 - Claim Save performance
                        PiXDiagHist objMostRecent = parentPi.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHistbyPiRowId(Convert.ToString(CaseManagement.PiRowId));

                    
                        if (DiagHistIsModified(objMostRecent, objPiXDiagHist))
                        {
                            objPiXDiagHist.IsICD10 = 0;
                            objPiXDiagHist.ChgdByUser = CaseManagement.Context.RMUser.LoginName;
                           
                            addICD9 = true;
                        }

                        objDoc = null;
                        objMostRecent = null;
                    }
                    XmlElement objDiagHistICD10Elem = (XmlElement)objXML.SelectSingleNode("/SysExData/PiXDiagHistICD10List");
                    //RMA-1209 Starts - ICD 10 Code not getting deleted after adding once
                    if (objDiagHistICD10Elem != null)
                    {
                    
                        XmlDocument objDoc = new XmlDocument();
                        objDoc.LoadXml(objXML.SelectSingleNode("/SysExData/PiXDiagHistICD10List").InnerXml);
                        objPiXDiagHist10 = (PiXDiagHist)CaseManagement.Context.Factory.GetDataModelObject("PiXDiagHist", false);
                        objPiXDiagHist10.PopulateObject(objDoc);

                        PiXDiagHist objICD10MostRecent = parentPi.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHistICD10byPiRowId(Convert.ToString(CaseManagement.PiRowId));
                        if (DiagHistIsModified(objICD10MostRecent, objPiXDiagHist10))
                        {
                            objPiXDiagHist10.ChgdByUser = CaseManagement.Context.RMUser.LoginName;
                            objPiXDiagHist10.IsICD10 = -1;
                            addICD10 = true;
                        }
                        objICD10MostRecent = null;
                      
                        objDoc = null;
                       
                    }


                    switch (ICDFlag)
                    {
                        case "0":
                            if (objPiXDiagHist.PiXDiagrowId > objPiXDiagHist10.PiXDiagrowId && addICD9)
                            {
                                objPiXDiagHist.PiXDiagrowId = -1;
                                parentPi.PiXDiagHistList.Add(objPiXDiagHist);
                            }
                            else if (addICD10)
                            {
                                objPiXDiagHist10.PiXDiagrowId = -1;
                                parentPi.PiXDiagHistList.Add(objPiXDiagHist10);
                            }
                            break;
                        case "1":
                            if (addICD9)
                            {
                                objPiXDiagHist.PiXDiagrowId = -1;
                                parentPi.PiXDiagHistList.Add(objPiXDiagHist);
                            }
                            break;
                        case "2":
                            if (addICD10)
                            {
                                objPiXDiagHist10.PiXDiagrowId = -1;
                                parentPi.PiXDiagHistList.Add(objPiXDiagHist10);
                            }
                            break;
                        case "3":
                            if (addICD10)
                            {
                                objPiXDiagHist10.PiXDiagrowId = -1;
                                parentPi.PiXDiagHistList.Add(objPiXDiagHist10);
                            }

                            if (addICD9)
                            {
                                objPiXDiagHist.PiXDiagrowId = -1;
                                parentPi.PiXDiagHistList.Add(objPiXDiagHist);
                            }
                            break;
                        case "4":
                            if (addICD9)
                            {
                                objPiXDiagHist.PiXDiagrowId = -1;
                                parentPi.PiXDiagHistList.Add(objPiXDiagHist);
                            }

                            if (addICD10)
                            {
                                objPiXDiagHist10.PiXDiagrowId = -1;
                                parentPi.PiXDiagHistList.Add(objPiXDiagHist10);
                            }
                            break;
                    }

                    objDiagHistElem = null;
                    objDiagHistICD10Elem = null;
                }

                catch (Exception ex)
                {
                    throw ex;
                }
            }
          

            
        }
       //Added Rakhi for R7:Add Emp Data Elements
        private void LoadPIChilds(ref XmlDocument objXML)
        {
            PiEmployee objEmployee = (PiEmployee)this.CaseManagement.Context.Factory.GetDataModelObject("PiEmployee", false);
            objEmployee.MoveTo(CaseManagement.PiRowId);   			
            //Creates Node PiXDiagHist for Diagnosis History for Medical Info and Case Management.
            //Added By Rahul.
            XmlNode objOldNode = objXML.SelectSingleNode("//SysExData/PiXDiagHistList");
            XmlDocument objTempDoc = new XmlDocument();
            XmlElement objElem = null;

            objElem = null;
            #region Load MMI History
            //Create XML for MMI History . Added by Rahul Sharma
            objOldNode = null;
            objOldNode = objXML.SelectSingleNode("//SysExData/PiXMMIHistList");
            objElem = null;
            int iMaxPiXMMIHistid = 0;
            PiXMMIHist objXMMIHist = null;
            if (objEmployee.PiXMMIHistList.Count != 0)
            {
                //Finding the max. row id to display MMI History.
                foreach (PiXMMIHist objPiXMMIHist in objEmployee.PiXMMIHistList)
                {
                    if (objPiXMMIHist.PiXMmirowId > iMaxPiXMMIHistid)
                    {
                        //sgoel6 04/28/2009 MITS 14887
                        if (objPiXMMIHist.DeletedFlag != -1)
                            iMaxPiXMMIHistid = objPiXMMIHist.PiXMmirowId;
                    }
                }
                objElem = objXML.CreateElement("PiXMMIHistList");
                if (iMaxPiXMMIHistid == 0)
                    objXMMIHist = (PiXMMIHist)CaseManagement.Context.Factory.GetDataModelObject("PiXMMIHist", false);
                else
                    objXMMIHist = objEmployee.PiXMMIHistList[iMaxPiXMMIHistid];

                //Creating XML for MMI History
                objElem.InnerXml = objXMMIHist.SerializeObject();
            }
            else
            {
                objElem = objXML.CreateElement("PiXMMIHistList");
                objXMMIHist = (PiXMMIHist)CaseManagement.Context.Factory.GetDataModelObject("PiXMMIHist", false);
                objElem.InnerXml = objXMMIHist.SerializeObject();
            }
            if (objOldNode != null)
                objXML.DocumentElement.ReplaceChild(objElem, objOldNode);
            else
                objXML.DocumentElement.AppendChild(objElem);
            //End of MMI History.
            #endregion
            if (iUseCaseMgmt == -1)
            {
                    #region Load the Highest Diagnosis History
                    PiXDiagHist objDiaHist = null;
                    PiXDiagHist objDiaHist10 = null;
                    int iMaxDays = 0;
                    int iMinDays = 0;
                    int iOptDays = 0;
                    objOldNode = objXML.SelectSingleNode("/PiXDiagHistList");
                    string pirowid = CaseManagement.PiRowId.ToString();    //MITS 32423 :Praveen ICD-10 Changes
                    objDiaHist = objEmployee.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHistbyPiRowId(pirowid);    //MITS 32423 :Praveen ICD-10 Changes
                    //objDiaHist10 = objClaim.PrimaryPiEmployee.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHistICD10byPiRowId(pirowid);

                    if (objDiaHist != null)
                    {
                        iMaxDays = objDiaHist.DisMaxDuration;
                        iMinDays = objDiaHist.DisMinDuration;
                        iOptDays = objDiaHist.DisOptDuration;
                    }
                    else
                        objDiaHist = CaseManagement.Context.Factory.GetDataModelObject("PiXDiagHist", false) as PiXDiagHist;

                    objElem = objXML.CreateElement("PiXDiagHistList");
                    objElem.InnerXml = objDiaHist.SerializeObject();

                    if (objOldNode != null)
                        objXML.DocumentElement.ReplaceChild(objElem, objOldNode);
                    else
                        objXML.DocumentElement.AppendChild(objElem);

                    //create dummy sysex nodes for Binding Min/Max/Opt values


                    //MITS 32423 : Praveen Load ICD10 Diagonosis start
                    objElem = null;


                    //tkatsarski: 11/12/14 Start changes for RMA-3912
                    objOldNode = objXML.SelectSingleNode("/PiXDiagHistICD10List");
                    objDiaHist10 = objEmployee.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHistICD10byPiRowId(pirowid);
                    if (objDiaHist10 != null)
                    {
                        //Can modify data here
                    }
                    else
                        objDiaHist10 = CaseManagement.Context.Factory.GetDataModelObject("PiXDiagHist", false) as PiXDiagHist;

                    objElem = objXML.CreateElement("PiXDiagHistICD10List");
                    objElem.InnerXml = objDiaHist10.SerializeObject();

                    if (objOldNode != null)
                        objXML.DocumentElement.ReplaceChild(objElem, objOldNode);
                    else
                        objXML.DocumentElement.AppendChild(objElem);
                    //tkatsarski: 11/12/14 End changes for RMA-3912

                    base.ResetSysExData("ICDFlag", "0");
                    base.ResetSysExData("hdnicdSelected", "0");

                    if (objDiaHist.PiXDiagrowId > objDiaHist10.PiXDiagrowId)
                    {
                        base.ResetSysExData("PiXDiagHist_DisMaxDuration", objDiaHist.DisMaxDuration.ToString());
                        base.ResetSysExData("PiXDiagHist_DisMinDuration", objDiaHist.DisMinDuration.ToString());
                        base.ResetSysExData("PiXDiagHist_DisOptDuration", objDiaHist.DisOptDuration.ToString());
                        base.ResetSysExData("DisMdaTopic", objDiaHist.DisMdaTopic.ToString());
                        base.ResetSysExData("DisFactor", objDiaHist.DisFactor.ToString());
                    }
                    else
                    {
                        base.ResetSysExData("PiXDiagHist_DisMaxDuration", objDiaHist10.DisMaxDuration.ToString());
                        base.ResetSysExData("PiXDiagHist_DisMinDuration", objDiaHist10.DisMinDuration.ToString());
                        base.ResetSysExData("PiXDiagHist_DisOptDuration", objDiaHist10.DisOptDuration.ToString());
                        base.ResetSysExData("DisMdaTopic", objDiaHist10.DisMdaTopic.ToString());
                        base.ResetSysExData("DisFactor", objDiaHist10.DisFactor.ToString());
                    }

                    objDiaHist = null;
                    objDiaHist10 = null;
                    //MITS 32423 : Praveen Load ICD10 Diagonosis End
                

                #endregion

                #region Load Primary Case Manager
                //Creating XML for case Management History -- Added By Rahul Sharma.
                CmXCmgrHist objCmMgrHist = null;
                objOldNode = null;
                objOldNode = objXML.SelectSingleNode("//CmXCmgrHistList");
                objElem = null;
                // Code moved from Init() and added here to get Primary Case Manager . 
                //Its manadatory for displaying data of Primary Case Manager in Case Management Tab.
                //Start.
                foreach (CmXCmgrHist objCmXCmgrHist in CaseManagement.CaseMgrHistList)
                {
                    if (objCmXCmgrHist.PrimaryCmgrFlag)
                    {
                        iCaseMgmtHisId = objCmXCmgrHist.CmcmhRowId;
                    }
                }
                //End.
                if (CaseManagement.CaseMgrHistList.Count != 0)
                {
                    if (iCaseMgmtHisId != 0)
                    {
                        objCmMgrHist = CaseManagement.CaseMgrHistList[iCaseMgmtHisId];

                        int iCaseMgrDays = 0;
                        TimeSpan objDateDiff;
                        objElem = objXML.CreateElement("CmXCmgrHistList");
                        objElem.InnerXml = objCmMgrHist.SerializeObject();
                        if (objCmMgrHist.DateClosed == "" || objCmMgrHist.DateClosed == null)
                        {
                            if (objCmMgrHist.RefDate == "" || objCmMgrHist.RefDate == null)
                                iCaseMgrDays = 0;
                            else
                            {
                                objDateDiff = System.DateTime.Today.Subtract(Conversion.ToDate(objCmMgrHist.RefDate));
                                iCaseMgrDays = objDateDiff.Days;
                            }
                        }
                        else
                        {
                            if (objCmMgrHist.RefDate == "" || objCmMgrHist.RefDate == null)
                                iCaseMgrDays = 0;
                            else
                            {
                                objDateDiff = Conversion.ToDate(objCmMgrHist.DateClosed).Subtract(Conversion.ToDate(objCmMgrHist.RefDate));
                                iCaseMgrDays = objDateDiff.Days;
                            }
                        }
                        if (iCaseMgrDays < 0)
                            base.ResetSysExData("CaseMgrDays", "");
                        else
                            base.ResetSysExData("CaseMgrDays", iCaseMgrDays.ToString());
                    }
                    else
                    {
                        objElem = objXML.CreateElement("CmXCmgrHistList");
                        objCmMgrHist = (CmXCmgrHist)CaseManagement.Context.Factory.GetDataModelObject("CmXCmgrHist", false);
                        objElem.InnerXml = objCmMgrHist.SerializeObject();
                        base.ResetSysExData("CaseMgrDays", "");
                    }
                }
                else
                {
                    objElem = objXML.CreateElement("CmXCmgrHistList");
                    objCmMgrHist = (CmXCmgrHist)CaseManagement.Context.Factory.GetDataModelObject("CmXCmgrHist", false);
                    objElem.InnerXml = objCmMgrHist.SerializeObject();
                    base.ResetSysExData("CaseMgrDays", "");
                }
                if (objOldNode != null)
                    objXML.DocumentElement.ReplaceChild(objElem, objOldNode);
                else
                    objXML.DocumentElement.AppendChild(objElem);
                #endregion

                #region Load Work Loss
                objOldNode = null;
                objOldNode = objXML.SelectSingleNode("//SysExData/PiXWorkLossList");
                objElem = null;
                int iPiXWorkLoss = 0;
                PiXWorkLoss objPiX_WorkLoss = null;
                DateTime objDate1 = System.DateTime.Today;
                DateTime objDate2 = System.DateTime.Today;
                if (objEmployee.PiXWorkLossList.Count != 0)
                {

                    foreach (PiXWorkLoss objPiXWorkLoss in objEmployee.PiXWorkLossList)
                    {
                        iPiXWorkLoss = objPiXWorkLoss.PiWlRowId;
                        break;
                    }
                    objPiX_WorkLoss = objEmployee.PiXWorkLossList[iPiXWorkLoss];
                    if (CaseManagement.EstRetWorkDate != "")
                    objDate2 = Conversion.ToDate(CaseManagement.EstRetWorkDate);
                    objDate1 = Conversion.ToDate(objPiX_WorkLoss.DateLastWorked);
                    if (objEmployee.JobClassCode != 0)
                    {
                        DateTime objMaxDate = System.DateTime.Today;
                        DateTime objMinDate = System.DateTime.Today;
                        DateTime objOptDate = System.DateTime.Today;
                        if (objPiX_WorkLoss.DateLastWorked != "")
                        {
                            objMaxDate = Conversion.ToDate(objPiX_WorkLoss.DateLastWorked).AddDays(Conversion.ConvertObjToDouble(iMaxDays, base.ClientId) + 1);
                            base.ResetSysExData("WL_MaxDate", objMaxDate.ToShortDateString());
                            objMinDate = Conversion.ToDate(objPiX_WorkLoss.DateLastWorked).AddDays(Conversion.ConvertObjToDouble(iMinDays, base.ClientId) + 1);
                            base.ResetSysExData("WL_MinDate", objMinDate.ToShortDateString());
                            objOptDate = Conversion.ToDate(objPiX_WorkLoss.DateLastWorked).AddDays(Conversion.ConvertObjToDouble(iOptDays, base.ClientId) + 1);
                            base.ResetSysExData("WL_OptDate", objOptDate.ToShortDateString());
                        }
                        else
                        {
                            base.ResetSysExData("WL_MaxDate", "");
                            base.ResetSysExData("WL_MinDate", "");
                            base.ResetSysExData("WL_OptDate", "");
                        }
                    }
                    else
                    {
                        base.ResetSysExData("WL_MaxDate", "");		// Nikhil Garg 01.16.2006   Needs to get created if not already
                        base.ResetSysExData("WL_MinDate", "");		// Nikhil Garg 01.16.2006   Needs to get created if not already
                        base.ResetSysExData("WL_OptDate", "");		// Nikhil Garg 01.16.2006   Needs to get created if not already
                    }
                    int iTotalLostDays = 0;
                    int iEstDisability = 0;
                    if (objPiX_WorkLoss.DateReturned == "")
                    {
                        base.ResetSysExData("ChkWorkLoss", "True");

                        iEstDisability = GetWorkLossDayCount(Conversion.ToDate(objPiX_WorkLoss.DateLastWorked), objDate2, sDaysOfWeek);
                        base.ResetSysExData("EstDisability", iEstDisability.ToString() + " Days");
                        int i = 0;
                        foreach (PiXWorkLoss objPiXWorkLoss in objEmployee.PiXWorkLossList)
                        {
                            if (i > 0)
                            {
                                iTotalLostDays = iTotalLostDays + objPiXWorkLoss.Duration;
                            }
                            ++i;
                        }
                        iTotalLostDays = iTotalLostDays + GetWorkLossDayCount(Conversion.ToDate(objPiX_WorkLoss.DateLastWorked), System.DateTime.Today, sDaysOfWeek);
                    }
                    else
                    {
                        base.ResetSysExData("ChkWorkLoss", "False");
                        foreach (PiXWorkLoss objPiXWorkLoss in objEmployee.PiXWorkLossList)
                            iTotalLostDays = iTotalLostDays + objPiXWorkLoss.Duration;

                        //						base.ResetSysExData("EstDisability","");	// JP 12.20.2005   Needs to get created if not already
                        	//				base.ResetSysExData("WL_MaxDate","");		// JP 12.20.2005   Needs to get created if not already
                        //						base.ResetSysExData("WL_MinDate","");		// JP 12.20.2005   Needs to get created if not already
                        //						base.ResetSysExData("WL_OptDate","");		// JP 12.20.2005   Needs to get created if not already
                    }
                    base.ResetSysExData("TotalLostDays", iTotalLostDays.ToString() + " Days");
                    objElem = objXML.CreateElement("PiXWorkLossList");
                    objElem.InnerXml = objPiX_WorkLoss.SerializeObject();
                }
                else
                {
                    objElem = objXML.CreateElement("PiXWorkLossList");
                    objPiX_WorkLoss = (PiXWorkLoss)CaseManagement.Context.Factory.GetDataModelObject("PiXWorkLoss", false);
                    objElem.InnerXml = objPiX_WorkLoss.SerializeObject();
                    base.ResetSysExData("TotalLostDays", "");
                    base.ResetSysExData("ChkWorkLoss", "False");
                    //base.ResetSysExData("WL_MaxDate", "");
                    //base.ResetSysExData("WL_MinDate", "");
                   // base.ResetSysExData("WL_OptDate", "");
                    base.ResetSysExData("EstDisability", "");
                }
                if (objOldNode != null) 
                    objXML.DocumentElement.ReplaceChild(objElem, objOldNode);
                else 
                    objXML.DocumentElement.AppendChild(objElem);
                #endregion

                    #region Load Restriction
                    // Creating XML for PI Restriction--- Added By Rahul Sharma.
                    objOldNode = null;
                    objOldNode = objXML.SelectSingleNode("//SysExData/PiXRestrictList");
                objElem = null;
                int iPiXRestrict = 0;
                PiXRestrict objPiX_Restrict = null;
                if (objEmployee.PiXRestrictList.Count != 0)
                {
                    foreach (PiXRestrict objPiXRestrict in objEmployee.PiXRestrictList)
                    {
                        iPiXRestrict = objPiXRestrict.PiRestrictRowId;
                        break;
                    }
                    string sDateLastRes = "False";
                    int iEstLenDis = 0;
                    int iTotalRestDays = 0;
                    objPiX_Restrict = objEmployee.PiXRestrictList[iPiXRestrict];
                    objElem = objXML.CreateElement("PiXRestrictList");
                    objElem.InnerXml = objPiX_Restrict.SerializeObject();

                    DateTime sDate2 = System.DateTime.Today;

                    //if (CaseManagement.CaseManagement.EstRelRstDate != "")
                    //    sDate2 = Conversion.ToDate(CaseManagement.CaseManagement.EstRelRstDate);
                    DateTime sDate1 = System.DateTime.Today;
                    sDate1 = Conversion.ToDate(objPiX_Restrict.DateFirstRestrct);

                    if (objPiX_Restrict.DateLastRestrct == "")
                    {
                        sDateLastRes = "True";
                        int i = 0;
                        foreach (PiXRestrict objPiXRestrict in objEmployee.PiXRestrictList)
                        {
                            if (i > 0)
                            {
                                iTotalRestDays = iTotalRestDays + objPiXRestrict.Duration;
                            }
                            ++i;
                        }
                        iEstLenDis = GetWorkDay(Conversion.ToDate(objPiX_Restrict.DateFirstRestrct), sDate2, sDaysOfWeek);//has to change "1" neha
                        base.ResetSysExData("EstLenDis", iEstLenDis.ToString() + " Days");
                        iTotalRestDays = iTotalRestDays + GetWorkDay(Conversion.ToDate(objPiX_Restrict.DateFirstRestrct), System.DateTime.Today, sDaysOfWeek);
                        base.ResetSysExData("TotalResDays", iTotalRestDays.ToString() + " Days");
                    }
                    else
                    {
                        sDateLastRes = "False";
                        foreach (PiXRestrict objPiXRestrict in objEmployee.PiXRestrictList)
                            iTotalRestDays = iTotalRestDays + objPiXRestrict.Duration;
                        base.ResetSysExData("EstLenDis", "");
                        base.ResetSysExData("TotalResDays", iTotalRestDays.ToString() + " Days");
                    }
                    base.ResetSysExData("ChkCurrResDays", sDateLastRes);
                    if (objEmployee.JobClassCode != 0)
                    {
                        DateTime objMaxDate = System.DateTime.Today;
                        DateTime objMinDate = System.DateTime.Today;
                        DateTime objOptDate = System.DateTime.Today;
                        if (objPiX_Restrict.DateFirstRestrct != "")
                        {
                            objMaxDate = Conversion.ToDate(objPiX_Restrict.DateFirstRestrct).AddDays(Conversion.ConvertObjToDouble(iMaxDays, base.ClientId));
                            base.ResetSysExData("RW_MaxDate", objMaxDate.ToShortDateString());
                            objMinDate = Conversion.ToDate(objPiX_Restrict.DateFirstRestrct).AddDays(Conversion.ConvertObjToDouble(iMinDays, base.ClientId));
                            base.ResetSysExData("RW_MinDate", objMinDate.ToShortDateString());
                            objOptDate = Conversion.ToDate(objPiX_Restrict.DateFirstRestrct).AddDays(Conversion.ConvertObjToDouble(iOptDays, base.ClientId));
                            base.ResetSysExData("RW_OptDate", objOptDate.ToShortDateString());
                        }
                        else
                        {
                            base.ResetSysExData("RW_MaxDate", "");
                            base.ResetSysExData("RW_MinDate", "");
                            base.ResetSysExData("RW_OptDate", "");
                        }
                    }
                    else
                    {
                        base.ResetSysExData("RW_MaxDate", "");
                        base.ResetSysExData("RW_MinDate", "");
                        base.ResetSysExData("RW_OptDate", "");
                    }
                }
                else
                {
                    objElem = objXML.CreateElement("PiXRestrictList");
                    objPiX_Restrict = (PiXRestrict)CaseManagement.Context.Factory.GetDataModelObject("PiXRestrict", false);
                    objElem.InnerXml = objPiX_Restrict.SerializeObject();
                    base.ResetSysExData("ChkCurrResDays", "False");
                    base.ResetSysExData("RW_MaxDate", "");
                    base.ResetSysExData("RW_MinDate", "");
                    base.ResetSysExData("RW_OptDate", "");
                    base.ResetSysExData("EstLenDis", "");
                    base.ResetSysExData("TotalResDays", "");
                }
                if (objOldNode != null)
                    objXML.DocumentElement.ReplaceChild(objElem, objOldNode);
                else
                    objXML.DocumentElement.AppendChild(objElem);
                #endregion

               
                base.ResetSysExData("UseCaseMgmt", "true");
            }
            else
            {
                base.ResetSysExData("UseCaseMgmt", "false");
             }
        }
        private int GetWorkLossDayCount(DateTime sLastWorkDate, DateTime sReturnedToWorkDate, string[] sDaysOfWeek)
        {
            DateTime objLastWorkedDate = System.DateTime.Today;
            DateTime objReturnedToWorkDate = System.DateTime.Today;
            TimeSpan objDays;
            int iDayOfWeekStart = 0;
            int iDayOfWeekEnd = 0;
            int iNewDays = 0;
            int iDays = 0;
            string sParent = string.Empty;

            objLastWorkedDate = sLastWorkDate.AddDays(1);
            objReturnedToWorkDate = sReturnedToWorkDate;
            objDays = sReturnedToWorkDate.Subtract(objLastWorkedDate);
            iDays = objDays.Days;
            iDayOfWeekStart = (int)objLastWorkedDate.DayOfWeek;
            iDayOfWeekEnd = (int)objReturnedToWorkDate.DayOfWeek;
            subInitHolidayInfo();
            for (int i = 1; i <= iDays; i++)
            {
                if (sDaysOfWeek[(int)objLastWorkedDate.AddDays(i - 1).DayOfWeek] == "True")
                    iNewDays = iNewDays + 1;
            }
            return iNewDays;

        }
        private int GetWorkDay(DateTime dDate1, DateTime dDate2, string[] sDaysOfWeek)
        {
            int iDays = 0;
            int iDayOfWeekStart = 0;
            int iDayOfWeekEnd = 0;
            int iNewDays = 0;
            string sParent = string.Empty;
            TimeSpan objDays;

            objDays = dDate2.Subtract(dDate1);
            iDays = objDays.Days + 1;
            iDayOfWeekStart = (int)dDate1.DayOfWeek;
            iDayOfWeekEnd = (int)dDate2.DayOfWeek;
            for (int i = 1; i <= iDays; i++)
            {
                if (sDaysOfWeek[(int)dDate1.AddDays(i - 1).DayOfWeek] == "True")
                    iNewDays = iNewDays + 1;
            }
            return iNewDays;
        }
       
        //Get the Work Loss days count between 2 dates.
        private void subInitHolidayInfo()
        {
            string sDate1 = string.Empty;
            string sDate2 = string.Empty;
            int i = 0;
            int j = 0;
            int count = 1;

            using (DbReader objCount = DbFactory.GetDbReader(CaseManagement.Context.DbConn.ConnectionString, "SELECT COUNT(1) COUNTI FROM SYS_HOLIDAYS"))
            {
                while (objCount.Read())
                {
                    count = Conversion.ConvertObjToInt(objCount.GetValue("COUNTI"), base.ClientId);
                    SYS_HOLIDAYS = new Holidays[count];
                }
            }

            using (DbReader objReader = DbFactory.GetDbReader(CaseManagement.Context.DbConn.ConnectionString, "SELECT HOL_DATE,ORG_EID FROM SYS_HOLIDAYS ORDER BY HOL_DATE,ORG_EID"))
            {
                while (objReader.Read())
                {

                    sDate1 = objReader.GetString("HOL_DATE");
                    if (sDate1 != sDate2)
                    {
                        if (sDate2 != "")
                            i = i + 1;
                        SYS_HOLIDAYS[i].sDate = sDate1;
                        j = 0;
                        SYS_HOLIDAYS[i].lOrgID = new long[count];
                        SYS_HOLIDAYS[i].lOrgID[j] = Conversion.ConvertObjToInt64(objReader.GetValue("ORG_EID"), base.ClientId);
                        sDate2 = sDate1;
                    }
                    else
                    {
                        j = j + 1;
                        SYS_HOLIDAYS[i].lOrgID[j] = Conversion.ConvertObjToInt64(objReader.GetValue("ORG_EID"), base.ClientId);
                    }
                }
            }
        }
    }
}
