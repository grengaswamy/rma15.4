using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// Demand Offer Screen.
    /// </summary>
    public class DemandOfferForm : DataEntryFormBase
    {
        const string CLASS_NAME = "DemandOffer";
        const string FILTER_KEY_NAME = "ParentId";
        const string PARENT_NAME1 = "subrogation";
        const string PARENT_NAME2 = "litigation";
        const string PARENT_NAME3 = "unit";
        const string PARENT_NAME4 = "propertyloss";
        const string PARENT_NAME5 = "piinjury"; //Neha saving it from injury
        const string PARENT_NAME6 = "piemployee";
        //added by swati for MITS # 35363
        const string PARENT_NAME7 = "claimant";
        //change end here by swati

        private DemandOffer objDemandOffer { get { return objData as DemandOffer; } }

        private LocalCache objCache { get { return objData.Context.LocalCache; } }

        public DemandOfferForm(FormDataAdaptor fda)
            : base(fda)
        {
            base.m_ClassName = CLASS_NAME;
        }

        private string m_Caption = string.Empty;
        //nnorouzi; MITS 19478
        public override string GetCaption()
        {
            return base.GetCaption();
        }

        public override void InitNew()
        {
            string sParentSysName = string.Empty;
            base.InitNew();
            //set filter for ClaimId
            objDemandOffer.ParentId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME, false);
            if (objDemandOffer.ParentId > 0)
                (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + objDemandOffer.ParentId;

            sParentSysName = base.GetFormVarNodeText("SysFormPForm");
            if (sParentSysName == PARENT_NAME1)
            {
                objDemandOffer.ParentName = typeof(ClaimXSubrogation).Name;
            }
            else if (sParentSysName == PARENT_NAME2)
            {
                objDemandOffer.ParentName = typeof(ClaimXLitigation).Name;
            }
            //Added by Amitosh for R8 enhancement
            else if (sParentSysName == PARENT_NAME3)
            {
                objDemandOffer.ParentName = typeof(UnitXClaim).Name;
            }
            else if (sParentSysName == PARENT_NAME4)
            {
                objDemandOffer.ParentName = typeof(ClaimXPropertyLoss).Name;
            }
            //End Amitosh
            //Neha
            else if (sParentSysName == PARENT_NAME5 || sParentSysName == PARENT_NAME6)
            {
                objDemandOffer.ParentName = typeof(PiEmployee).Name;
            }
            //added by swati for WWIG MITS # 35363
            else if (sParentSysName == PARENT_NAME7)
            {
                objDemandOffer.ParentName = typeof(Claimant).Name;
            }
            //change end here by swati
        }

        //Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
        public override void OnUpdateForm()
        {
            int claimId = 0;
            DataObject objParent = null;
            Claim objClaim = (Claim)objDemandOffer.Context.Factory.GetDataModelObject("Claim", false);

            base.OnUpdateForm();

            if (objDemandOffer.ParentName == typeof(ClaimXSubrogation).Name)
            {
                objParent = (ClaimXSubrogation)objDemandOffer.Context.Factory.GetDataModelObject("ClaimXSubrogation", false);
                objParent.MoveTo(objDemandOffer.ParentId);
                claimId = ((ClaimXSubrogation)objParent).ClaimId;
            }
            else if (objDemandOffer.ParentName == typeof(ClaimXLitigation).Name)
            {
                objParent = (ClaimXLitigation)objDemandOffer.Context.Factory.GetDataModelObject("ClaimXLitigation", false);
                objParent.MoveTo(objDemandOffer.ParentId);
                claimId = ((ClaimXLitigation)objParent).ClaimId;
            }
            //Added by Amitosh for R8 enhancement of VehicleLoss
            else if (objDemandOffer.ParentName == typeof(UnitXClaim).Name)
            {
                objParent = (UnitXClaim)objDemandOffer.Context.Factory.GetDataModelObject("UnitXClaim", false);
                objParent.MoveTo(objDemandOffer.ParentId);
                claimId = ((UnitXClaim)objParent).ClaimId;
            }
            else if (objDemandOffer.ParentName == typeof(ClaimXPropertyLoss).Name)
            {
                objParent = (ClaimXPropertyLoss)objDemandOffer.Context.Factory.GetDataModelObject("ClaimXPropertyLoss", false);
                objParent.MoveTo(objDemandOffer.ParentId);
                claimId = ((ClaimXPropertyLoss)objParent).ClaimId;
            }
            //end Amitosh
            else if (objDemandOffer.ParentName == typeof(PiEmployee).Name)
            {
                objParent = (PiEmployee)objDemandOffer.Context.Factory.GetDataModelObject("PiEmployee", false);
                objParent.MoveTo(objDemandOffer.ParentId);
                // claimId = ((PiEmployee)objParent).ClaimId;
            }
            //change start here by swati MITS # 35363
            else if (objDemandOffer.ParentName == typeof(Claimant).Name)
            {
                objParent = (Claimant)objDemandOffer.Context.Factory.GetDataModelObject("Claimant", false);
                objParent.MoveTo(objDemandOffer.ParentId);
                claimId = ((Claimant)objParent).ClaimId;
            }
            //change end here by swati
            objClaim.MoveTo(claimId);
            //added by Manika for R8 enhancement of Salvage: MITS 26295
            if (objParent is UnitXClaim)
            {
                switch (objClaim.LineOfBusCode)
                {
                    case 241:
                        m_SecurityId = RMO_GC_VEHICLELOSS_DEMANDOFFER;
                        break;
                }
            }
            else if (objParent is ClaimXPropertyLoss)
            {
                switch (objClaim.LineOfBusCode)
                {
                    case 241:
                        m_SecurityId = RMO_GC_PROPERTYLOSS_DEMANDOFFER;
                        break;
                }
            }
            //End MITS 26295
            //change start here by swati WWIG Gap 23 MITS # 35363
            else if (objParent is Claimant)
            {
                switch (objClaim.LineOfBusCode)
                {
                    case 241:
                        m_SecurityId = RMO_GC_CLAIMANT_DEMAND_OFFER;
                        break;
                    case 242:
                        m_SecurityId = RMO_VA_CLAIMANT_DEMAND_OFFER;
                        break;
                    case 845:
                        m_SecurityId = RMO_PC_CLAIMANT_DEMAND_OFFER;
                        break;
                }
            }
            //change end here by swati
            else if (objParent is ClaimXSubrogation)
            {
                switch (objClaim.LineOfBusCode)
                {
                    case 241:
                        m_SecurityId = RMO_GC_SUBROGATION_DEMAND_OFFER;
                        break;
                    case 242:
                        m_SecurityId = RMO_VA_SUBROGATION_DEMAND_OFFER;
                        break;
                    case 243:
                        m_SecurityId = RMO_WC_SUBROGATION_DEMAND_OFFER;
                        break;
                    case 844:
                        m_SecurityId = RMO_DI_SUBROGATION_DEMAND_OFFER;
                        break;
                    case 845:
                        m_SecurityId = RMO_PC_SUBROGATION_DEMAND_OFFER;
                        break;
                }
            }
            else
            {
                switch (objClaim.LineOfBusCode)
                {
                    case 241:
                        m_SecurityId = RMO_GC_LITIGATION_DEMAND_OFFER;
                        break;
                    case 242:
                        m_SecurityId = RMO_VA_LITIGATION_DEMAND_OFFER;
                        break;
                    case 243:
                        m_SecurityId = RMO_WC_LITIGATION_DEMAND_OFFER;
                        break;
                    case 844:
                        m_SecurityId = RMO_DI_LITIGATION_DEMAND_OFFER;
                        break;
                    case 845:
                        m_SecurityId = RMO_PC_LITIGATION_DEMAND_OFFER;
                        break;
                }
            }

            //Setting Security Id 
            XmlElement objSysSid = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSid");
            objSysSid.InnerText = m_SecurityId.ToString();

            if (objDemandOffer.DemandOfferRowId == 0)
            {
                if (!objDemandOffer.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            else
            {
                if (!objClaim.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_VIEW);
                    return;
                }
            }

            //Geeta 08/12/09 : Mits 14081 End


            //nnorouzi; MITS 19478
            m_Caption = this.GetCaption();
            base.ResetSysExData("SubTitle", m_Caption);
            ArrayList singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", m_Caption);
            base.m_ModifiedControls.Add(singleRow);

            //Check for People Maintenance permission
            PeoplePermissionChecks4Other("demandoffer", m_SecurityId + RMO_UPDATE);
        }
    }
}

