using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Text;
using System.Collections.Generic;
using Riskmaster.Cache;
//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for QMQualMgrReview Screen.
	/// </summary>
	public class QMQualMgrReviewForm : DataEntryFormBase
	{
		const string CLASS_NAME = "EventQM";
		private EventQM EventQM{get{return objData as EventQM;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "EventId";
        const string FORM_NAME = "qmqualmgrreview";
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
		public override void InitNew()
		{
			base.InitNew();
			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlDocument objXML = base.SysEx;
			XmlNode objEventId=null;
			try{objEventId = objXML.GetElementsByTagName(FILTER_KEY_NAME)[0];}
			catch{};
			
			//Filter by this EventId if Present.
            if (objEventId != null && objEventId.InnerText != "") //AMAN MITS 27730
			{
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + objEventId.InnerText;
			}
       
		}

		public QMQualMgrReviewForm(FormDataAdaptor fda):base(fda)
		{
			this.m_ClassName = CLASS_NAME;
            sLangCode = Convert.ToString(fda.userLogin.objUser.NlsCode);
		}
		
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{           
            base.OnUpdateForm();

            XmlDocument objXML = base.SysEx;
            XmlNode objNew = objXML.CreateElement("QmReviewerUid");
            XmlNode objOld = objXML.SelectSingleNode("//QmReviewerUid");

            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);

			base.ResetSysExData("Duration",CalculateDuration());
           
		}
		private string CalculateDuration()
		{
			TimeSpan objDays;
			string sDuration = string.Empty;
            StringBuilder sbSQL = new StringBuilder();
            DbReader objReader = null;
            Dictionary<string, string> strDictResourceValues = new Dictionary<string, string>();
            string sResKeys = "'lblNA','lblDays'";
			Event objEvent = (Event) EventQM.Context.Factory.GetDataModelObject("Event",false);
			objEvent.MoveTo(EventQM.EventId);
            sbSQL = sbSQL.Append(string.Format(@"SELECT LR.RESOURCE_KEY, LR.RESOURCE_VALUE FROM LOCAL_RESOURCE LR
                                                INNER JOIN PAGE_INFO PI ON PI.PAGE_ID=LR.PAGE_ID 
                                            WHERE PI.PAGE_NAME='{0}' AND LR.LANGUAGE_ID={1} AND LR.RESOURCE_KEY IN ({2})", FORM_NAME + ".aspx", Convert.ToInt32(sLangCode), sResKeys));
            objReader = DbFactory.ExecuteReader(ConfigurationInfo.GetViewConnectionString(base.ClientId), sbSQL.ToString());
            while (objReader.Read())
            {
                strDictResourceValues.Add(objReader.GetString(0), objReader.GetString(1));
            }
            objReader.Close();
            sbSQL.Clear();

            if (strDictResourceValues.Count == 0) // fall back
            {
                sbSQL = sbSQL.Append(string.Format(@"SELECT LR.RESOURCE_KEY, LR.RESOURCE_VALUE FROM LOCAL_RESOURCE LR
                                                INNER JOIN PAGE_INFO PI ON PI.PAGE_ID=LR.PAGE_ID 
                                            WHERE PI.PAGE_NAME='{0}' AND LR.LANGUAGE_ID={1} AND LR.RESOURCE_KEY IN ({2})", FORM_NAME + ".aspx", Convert.ToInt32(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0]), sResKeys));
                objReader = DbFactory.ExecuteReader(ConfigurationInfo.GetViewConnectionString(base.ClientId), sbSQL.ToString());
                while (objReader.Read())
                {
                    strDictResourceValues.Add(objReader.GetString(0), objReader.GetString(1));
                }
                objReader.Close();
            }
			if(EventQM.QmReviewDate.Trim()!="")
			{
				objDays = Conversion.ToDate(EventQM.QmReviewDate).Subtract(Conversion.ToDate(objEvent.DateOfEvent));
				if(objDays.Days > 1000 || objDays.Days < 0)
					//sDuration = " NA";
                    sDuration = strDictResourceValues["lblNA"];
				else
					//sDuration = objDays.Days.ToString()+" Days";
                    sDuration = objDays.Days.ToString() + strDictResourceValues["lblDays"];
			}
			else
				//sDuration = " NA";
                sDuration = strDictResourceValues["lblNA"];
			return sDuration;
		}
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;
			Event objEvent = (Event) EventQM.Context.Factory.GetDataModelObject("Event",false);
			objEvent.MoveTo(EventQM.EventId);
			
			if(EventQM.QmReviewDate.Trim()!="")
			{
				if(objEvent.DateOfEvent.CompareTo(EventQM.QmReviewDate)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.ReviewDateMustBeGreaterThanEqualToEventDate", base.ClientId, sLangCode), "Review Date", Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}

			//Defect No: 002512. Nikhil Garg	Dated: 16-Mar-2006
			//check for QM Review Date > CD Review date
			if(EventQM.QmReviewDate.Trim()!="" && EventQM.CdReviewDate.Trim()!="")
			{
				if(EventQM.CdReviewDate.CompareTo(EventQM.QmReviewDate)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.QualityManagerReviewDate", base.ClientId, sLangCode), Globalization.GetString("Field.CommitteeDepartmentReviewDate", base.ClientId, sLangCode)),
						BusinessAdaptorErrorType.Error);

					bError = true;
					Cancel=bError;
					return;
				}
			}
			
			if(EventQM.QmReviewDate.Trim()!="" && EventQM.PaReviewDate.Trim()!="")		//check for QM Review Date > PA Review date
			{
				if(EventQM.PaReviewDate.CompareTo(EventQM.QmReviewDate)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.QualityManagerReviewDate", base.ClientId, sLangCode), Globalization.GetString("Field.PhysicianAdvisorReviewDate", base.ClientId, sLangCode)),
						BusinessAdaptorErrorType.Error);

					bError = true;
					Cancel=bError;
					return;
				}
			}
			
			if (EventQM.QmReviewDate.Trim()!="" && EventQM.IrReviewDate.Trim()!="")	//check for QM Review Date > IR Review date
			{
				if(EventQM.IrReviewDate.CompareTo(EventQM.QmReviewDate)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.QualityManagerReviewDate", base.ClientId, sLangCode), Globalization.GetString("Field.InitialReviewReviewDate", base.ClientId, sLangCode)),
						BusinessAdaptorErrorType.Error);

					bError = true;
					Cancel=bError;
					return;
				}
			}
			
			Cancel=bError;
		}
	}
}