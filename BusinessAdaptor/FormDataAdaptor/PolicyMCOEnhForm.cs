﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PolicyMCOEnh Screen.
	/// </summary>
	public class PolicyMCOEnhForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PolicyXMcoEnh";
		private PolicyXMcoEnh PolicyXMcoEnh{get{return objData as PolicyXMcoEnh;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "PolicyId";

		/// <summary>
		/// Class constructor to initialize Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public PolicyMCOEnhForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

        public override void InitNew()
        {
            base.InitNew();

            XmlNode objPolicyId = null;

            try
            {
                objPolicyId = base.SysEx.GetElementsByTagName(FILTER_KEY_NAME)[0];
            }
            catch
            {
            }; 

            if (objPolicyId != null)
                this.m_ParentId = Conversion.ConvertObjToInt(objPolicyId.InnerText, base.ClientId);

            PolicyXMcoEnh.PolicyId = this.m_ParentId;

            //Filter by this PhysEid if Present.
            if (this.m_ParentId != 0)
            {
                (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId;
            }

        }

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
			
			XmlDocument objXML = base.SysEx;
			XmlElement objOrigValues = objXML.CreateElement( "OrigValues" );
			XmlNode objOrigValuesOld = objXML.SelectSingleNode("//OrigValues");
            objOrigValues.InnerText = PolicyXMcoEnh.McoEid + "|" + PolicyXMcoEnh.McoBeginDate + "|" + PolicyXMcoEnh.McoEndDate ;

			if(objOrigValuesOld !=null)
				objXML.DocumentElement.ReplaceChild(objOrigValues,objOrigValuesOld);
			else
				objXML.DocumentElement.AppendChild( objOrigValues );			

		}
	}
}
