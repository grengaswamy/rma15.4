﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Text;
using System.Collections.Generic;


namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PiOther Screen.
	/// </summary>
	public class PiOtherForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PiOther";
		private PiOther PiOther{get{return objData as PiOther;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "EventId";
		private int m_iEventId = 0;
		private string sConnectionString = null;  // Ishan Mobile Apps
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
        private int m_iParentRowId = 0;
        private string m_sParentTableName = string.Empty;
        const string FILTER_KEY_NAME2 = "ParentRowId";
        const string FILTER_KEY_NAME3 = "ParentTableName";

        public override void InitNew()
		{
			base.InitNew(); 

            //this.m_iEventId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
            //string sFilter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_iEventId.ToString();
            //rsushilaggar JITA 11736  
            //this.m_iEventId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);   
            string sFilter = string.Empty;
            this.m_iParentRowId = base.GetSysExDataNodeInt("/SysExData/ParentRowId", false);
            this.m_sParentTableName = base.GetFormVarNodeText("SysFormPForm");
            this.PiOther.ParentRowId = this.m_iParentRowId;

            //-----sgupta320:Jira 15676 
            switch ((this.m_sParentTableName).ToUpper())
            {
                case "CLAIMGC":
                    m_SecurityId = RMO_GC_PERSONINVOLVED;
                    this.PiOther.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiOther.ParentTableName + "' ";
                    break;
                case "CLAIMDI":
                    m_SecurityId = RMO_DI_PERSONINVOLVED;
                    this.PiOther.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiOther.ParentTableName + "' ";
                    break;
                case "CLAIMPC":
                    m_SecurityId = RMO_PC_PERSONINVOLVED;
                    this.PiOther.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiOther.ParentTableName + "' ";
                    break;
                case "CLAIMWC":
                    m_SecurityId = RMO_WC_PERSONINVOLVED;
                    this.PiOther.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiOther.ParentTableName + "' ";
                    break;
                case "CLAIMVA":
                    m_SecurityId = RMO_VA_PERSONINVOLVED;
                    this.PiOther.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiOther.ParentTableName + "' ";
                    break;
                case "CLAIMXPOLICY":
                    m_SecurityId = RMO_POLICY_PERSONINVOLVED;
                    this.PiOther.ParentTableName = "POLICY";
                    m_iEventId = CommonFunctions.GetEventIdByPolicyId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiOther.ParentTableName + "' ";
                    break;
                default:
                    // Defaults to Event's person involved
                    m_iEventId = m_iParentRowId;
                    this.PiOther.ParentTableName = "EVENT";
                    sFilter = "(" + objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_iEventId.ToString() + " OR (" + objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiOther.ParentTableName + "'))";
                    break;
            }
            //End rsushilaggar Jira 11736
			
			//Nikhil Garg	03/02/2006 
			//Moved from problematic Code below (Not to save before user presses Save button)
            //npradeepshar 07/05/2011 R8 Added Driver code
			int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid",false);
			sFilter += " AND PI_EID=" + iPiEid;
            int iPiRowId = base.GetSysExDataNodeInt("/SysExData/PiRowId",false);
            if (iPiRowId > 0)
            {
                sFilter += " AND PI_ROW_ID=" + iPiRowId;
                this.PiOther.PiRowId = iPiRowId;
            }
            //npradeepshar End
			(objData as INavigation).Filter = sFilter;
			
			this.PiOther.EventId = this.m_iEventId;
			this.PiOther.PiEid = iPiEid;
            
            //rsushilaggar JIRA 7767
            if (PiOther.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (PiOther.PiRowId <= 0 || PiOther.PiEntity.EntityId <= 0)
                    PiOther.PiEntity.NameType = objCache.GetCodeId("IND", "ENTITY_NAME_TYPE");
            }

		}

//		public override void AfterAddNew()
//		{
//			base.AfterAddNew ();
//
//			int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid",false);
//
//			if(iPiEid>0)
//			{
//				string sSQL = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE EVENT_ID=" + this.m_iEventId + " AND PI_EID=" + iPiEid;
//				int iPiRowId = 0;
//				iPiRowId = this.PiOther.Context.DbConnLookup.ExecuteInt(sSQL);
//				if(iPiRowId>0)
//				{
//					// PI Record already exists
//					this.PiOther.MoveTo(iPiRowId);   
//				}
//				else
//				{
//					this.PiOther.PiEid = iPiEid;
//					this.PiOther.Save();  
//				}
//			}
//		}

		public PiOtherForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
			sConnectionString = fda.connectionString;  // Ishan Mobile Apps
            sLangCode = Convert.ToString(fda.userLogin.objUser.NlsCode);
		}
        
        //Charanpreet for 12409
        public override void BeforeSave(ref bool Cancel)
        {
            bool bSucess = false;
            //Added:Yukti, JIRA :17661, DT:11/19/2015
            int iTableId = 0;
            int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid", false);
            this.m_iParentRowId = base.GetSysExDataNodeInt("/SysExData/ParentRowId", false);
                       
            string sSQL = string.Empty;
           // string sTableName = Conversion.CastToType<string>(base.GetSysExDataNodeText("/SysExData/EntityTypeName", true), out bSucess);
            string sTableName = Convert.ToString(base.GetSysExDataNodeText("/SysExData/EntityTypeName", true));

            //sgupta320: 17461 PI-other not saving.
            //if (iPiEid == 0)
            //{
                if (string.IsNullOrEmpty(sTableName))
                {
                    iTableId = Conversion.CastToType<int>(base.GetSysExDataNodeText("/SysExData/EntityXTableID", true), out bSucess);
                }
                else
                {
                    iTableId = this.PiOther.Context.LocalCache.GetTableId(sTableName);
                }
                if (this.PiOther.Context.InternalSettings.SysSettings.UseEntityRole)
                {
                    //Start rsushilaggar Jira 11730
                    this.PiOther.RoleTableId = Conversion.CastToType<Int32>(base.GetSysExDataNodeText("/SysExData/EntityXTableID", true), out bSucess);
                    if (this.PiOther.RoleTableId == 0)
                    {
                        //string sTableName = Conversion.CastToType<string>(base.GetSysExDataNodeText("/SysExData/EntityTypeName", true), out bSucess);
                        this.PiOther.RoleTableId = iTableId;
                    }
                }

                else
                {
                    this.PiOther.PiEntity.EntityTableId = iTableId;
                }

            //}

            if (iPiEid > 0 && (this.m_objData as PiOther).IsNew)
            {

                if (this.PiOther.Context.InternalSettings.SysSettings.UseEntityRole)
                {
                    this.PiOther.RoleTableId = Conversion.CastToType<Int32>(base.GetSysExDataNodeText("/SysExData/EntityXTableID", true), out bSucess);
                    sSQL = string.Format("SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PARENT_ROW_ID= {0} AND PI_EID= {1} AND ROLE_TABLE_ID= {2} AND  PARENT_TABLE_NAME = '{3}' ", this.m_iParentRowId, iPiEid, this.PiOther.RoleTableId, this.PiOther.ParentTableName);
                }
                else
                    sSQL = string.Format("SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PARENT_ROW_ID= {0} AND PI_EID= {1} AND  PARENT_TABLE_NAME = '{2}' ", this.m_iParentRowId, iPiEid, this.PiOther.ParentTableName);

                int iPiRowId = 0;
                iPiRowId = this.PiOther.Context.DbConnLookup.ExecuteInt(sSQL);
                if (iPiRowId > 0)
                {
                    // PI Record already exists	
                    Cancel = true;
                    // pgupta215: RMA-18997
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), string.Format(Globalization.GetString("Validation.OtherPersonAlreadyExistsAsPI", base.ClientId, sLangCode), this.PiOther.ParentTableName.ToLower()), BusinessAdaptorErrorType.Error);
                    return;
                }
            }
            
        }
        //Charanpreet for 12409 ends

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
            ArrayList singleRow = null;//MGaba2:MITS 22114
          

            base.OnUpdateForm ();

			XmlDocument objSysExDataXmlDoc = base.SysEx;

			string sSubTitleText = string.Empty; 
           
			// Add EventNumber node, if already not present, to avoid Orbeon 'ref' error 			
			XmlNode objEventNumberNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/EventNumber");
            
            if(objEventNumberNode==null)
			{
				base.CreateSysExData("EventNumber"); 
			}
			else
			{
				if(objEventNumberNode.InnerText.Length>0)
					sSubTitleText = " [" + objEventNumberNode.InnerText + "]"; 
				objEventNumberNode = null;
			}
			

			// Add ClaimNumber node, if already not present, to avoid Orbeon 'ref' error
			XmlNode objClaimNumberNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/ClaimNumber");

			if(objClaimNumberNode==null)
			{
				base.CreateSysExData("ClaimNumber"); 
			}			
			else
			{
				if(objClaimNumberNode.InnerText.Length>0)
					sSubTitleText = " [" + objClaimNumberNode.InnerText + "]"; 
				objClaimNumberNode = null;
			}
			
			// TODO - To find a better way to ascertain which is our starting page (Event or Claim)
			// As of now, first event number and then claim number is checked for subtitle text.
			base.ResetSysExData("SubTitle",sSubTitleText);

            //MGaba2:MITS 22114:Comments screen showing title as "Piemployee Comments undefined" :Start
            //Shifting the code below as event number is required

            //ArrayList singleRow = new ArrayList();
            //base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            //base.AddElementToList(ref singleRow, "id", "formsubtitle");
            //base.AddElementToList(ref singleRow, "Text", sSubTitleText);
            //base.m_ModifiedControls.Add(singleRow);

			//objSysExDataXmlDoc = null; commented r2r3 merge issue
			//Shruti Choudhary updated code for mits 10384
            //if (this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[2]/SysFormName") != null)
            //{
            //    ((XmlElement)base.SysView.SelectSingleNode("//internal[@name='SysFormPForm']")).SetAttribute("value",
            //        this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[2]/SysFormName").InnerText);
            //}

			PopulateEntityTypes();
            //avipinsrivas Start : Worked for Jira-340
            bool bHideTypeTextBox = false;

            if (this.PiOther.Context.InternalSettings.SysSettings.UseEntityRole)
            {
            	bHideTypeTextBox = true;
                //rsushilaggar JIRA 7767
                if (this.PiOther != null && this.PiOther.PiEntity != null && this.PiOther.PiEntity.NameType > 0)
                    this.ResetSysExData("EntityType", objCache.GetCodeDesc(this.PiOther.PiEntity.NameType));
                else
                    this.ResetSysExData("EntityType", objCache.GetCodeDesc(objCache.GetCodeId("IND", "ENTITY_NAME_TYPE")));
            }
            else
            {
                int iEntityTableId = 0;
                iEntityTableId = PiOther.PiEntity.EntityTableId;
                if ((iEntityTableId != 0 && !PiOther.PiEntity.IsNew) || (string.Compare(objCache.GetTableName(iEntityTableId), "DRIVER_INSURED") == 0) || (string.Compare(objCache.GetTableName(iEntityTableId), "DRIVER_OTHER") == 0))
                    bHideTypeTextBox = false;
                else
                    bHideTypeTextBox = true;
                //rsushilaggar JIRA 7767
                this.AddKillNode("entitytype");
            }

            if (bHideTypeTextBox)
            {
                this.AddKillNode("entitytableidtext");
                this.AddKillNode("entitytableidtextname");
            }
            else
            {
                this.AddKillNode("entitytableid");
                singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.ToggleControls.ToString());
                base.AddElementToList(ref singleRow, "id", "entitytableidtextname");
                base.AddElementToList(ref singleRow, "Text", "true");
                base.m_ModifiedControls.Add(singleRow);

                XmlNode objOld = null;
                XmlElement objNew = null;
                XmlDocument objXML = base.SysEx;
                XmlElement objElem = null;
                XmlElement objChild = null;

                objOld = objXML.SelectSingleNode("//ControlAppendAttributeList");
                objNew = objXML.CreateElement("ControlAppendAttributeList");
                objElem = objXML.CreateElement("entitytableidtextname");

                objChild = objXML.CreateElement("required");
                objChild.SetAttribute("value", "yes");
                objElem.AppendChild(objChild);
                objNew.AppendChild(objElem);
                if (objOld != null)
                    objXML.DocumentElement.ReplaceChild(objNew, objOld);
                else
                    objXML.DocumentElement.AppendChild(objNew);
            }
            if (!this.PiOther.Context.InternalSettings.SysSettings.UseEntityRole)
                bHideTypeTextBox = int.Equals(this.PiOther.PiEntity.EntityId, 0);

            base.ResetSysExData("HideTypeTextBox", bHideTypeTextBox ? "TRUE" : "FALSE");
            //avipinsrivas End
            //Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.  
            Event objEvent = (Event)this.PiOther.Context.Factory.GetDataModelObject("Event", false);
            string sEventDateFromDB = string.Empty;
            if (objEvent != null)
            {
                int iEventId = PiOther.EventId;
                if (iEventId != 0)
                {
                    objEvent.MoveTo(iEventId);
                    sEventDateFromDB = objEvent.DateOfEvent;
                    if (!String.IsNullOrEmpty(sEventDateFromDB))
                    {
                        if (base.SysEx.SelectSingleNode("DateOfEvent") == null)
                        {
                            string sUIEventDate = string.Empty;
                            sUIEventDate = Conversion.ToDate(sEventDateFromDB).ToString("MM/dd/yyyy");
                            base.CreateSysExData("DateOfEvent", sUIEventDate);
                        }
                    }
                    //MGaba2:MITS 22114:Comments screen showing title as "Piemployee Comments undefined" :Start
                  //  sSubTitleText = string.Format(" [{0} * {1} {2}]", objEvent.EventNumber, PiOther.PiEntity.FirstName, PiOther.PiEntity.LastName);

                    switch (PiOther.ParentTableName.ToUpper())  //ijain4 : JIRA 17480 : 20/11/2015
                    {
                        case "CLAIM":
                            Claim objClaim = (Claim)this.PiOther.Context.Factory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(PiOther.ParentRowId);
                            sSubTitleText = string.Format(" [{0} * {1} {2}]", objClaim.ClaimNumber, PiOther.PiEntity.FirstName, PiOther.PiEntity.LastName);
                            break;
                        case "POLICY":
                            Policy objPolicy = (Policy)this.PiOther.Context.Factory.GetDataModelObject("Policy", false);
                            objPolicy.MoveTo(PiOther.ParentRowId);
                            sSubTitleText = string.Format(" [{0} * {1} {2}]", objPolicy.PolicyNumber, PiOther.PiEntity.FirstName, PiOther.PiEntity.LastName);
                            break;
                        default:
                            sSubTitleText = string.Format(" [{0} * {1} {2}]", objEvent.EventNumber, PiOther.PiEntity.FirstName, PiOther.PiEntity.LastName);
                            break;
                    }
                    //ijain4 : JIRA 17480 : 20/11/2015
                    singleRow = new ArrayList();
                    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                    base.AddElementToList(ref singleRow, "id", "formsubtitle");
                    base.AddElementToList(ref singleRow, "Text", sSubTitleText);
                    base.m_ModifiedControls.Add(singleRow);
                    //MGaba2:MITS 22114:End
                }
            }
            //Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event. 

			if (PiOther.PiEntity.BirthDate!=string.Empty)
                base.ResetSysExData("EntityAge", Utilities.CalculateAgeInYears(PiOther.PiEntity.BirthDate, sEventDateFromDB)); //Added sEventDateFromDB for MITS 19315
			else
				base.ResetSysExData("EntityAge","");

			if (this.PiOther.PiRowId > 0 || this.PiOther.PiEid <= 0)
				base.AddKillNode("javascript_SetDirtyFlag");

            //smishra25: MITS 24395, MITS 13748 implementation was missing
            //nadim for 13748,added to hide/unhide SSN field
            if (!PiOther.Context.RMUser.IsAllowedEx(m_SecurityId, FormBase.RMO_PI_OTHERS_VIEW_SSN))
            {
                base.AddKillNode("taxid");

            }
            //nadim for 13748

            // Add DiaryMessage node, if already not present, to avoid Orbeon 'ref' error
            XmlNode objDiaryMessageNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/DiaryMessage");

            if (objDiaryMessageNode == null)
            {
                base.CreateSysExData("DiaryMessage");
            }
            objSysExDataXmlDoc = null;

			//Check for People Maintenance permission
            //skhare7 Point Policy interface
			PeoplePermissionChecks4PI("piother", m_SecurityId+RMO_UPDATE);
            
            if (PiOther.PolicyUnitRowId != 0)
            {
                DisplayUnitNo();


            }
            else
            {
                base.AddKillNode("Unitno");
            }

            //// dbisht6 mits35331


          //  if ((!(String.IsNullOrEmpty(PiOther.PiEntity.ClientSequenceNumber.ToString()))) || PiOther.PiEntity.ClientSequenceNumber<=0)
            //if(IsExternalInsured(PiOther.PiEntity.EntityId))
            //{
            //    base.AddReadOnlyNode("readOnlyPageExceptSupp");
            //    this.AddKillNode("delete");

            //}

            //dbisht6

            //added by neha goel MITS# 36916: PMC gap 7 CLUE fields- RMA - 5499
            if (!PiOther.Context.InternalSettings.SysSettings.UseCLUEReportingFields)
            {
                base.AddKillNode("MortgageeLoanNumber");
                base.AddKillNode("ClueSubjIdentifier");
                base.AddKillNode("ClueSubjRemovalIdentifier");
            }
            else
            {
                base.AddDisplayNode("MortgageeLoanNumber");
                base.AddDisplayNode("ClueSubjIdentifier");
                base.AddDisplayNode("ClueSubjRemovalIdentifier");
            }
            //end by neha goel MITS#36916 PMC CLUE gap 7- RMA - 5499
            //----sgupta320: Jira-17668
            if (base.FormVariables.SelectSingleNode("//SysSid") != null)
                base.FormVariables.SelectSingleNode("//SysSid").InnerText = Convert.ToString(m_SecurityId);

            if (this.PiOther.PiRowId > 0 && !(this.m_objData as PiOther).IsNew && this.PiOther.ParentTableName == "POLICY" && this.PiOther.Context.InternalSettings.SysSettings.UseEntityRole)
                AddReadOnlyNode("entitytableid");
        }
        // this function was no longer needed hence commented by dbisht6
        //public bool IsExternalInsured(int iEntityID)
        //{
        //    bool isExternal = false;
        //   // string connectionString = Entity.Context.DbConn.ConnectionString;
        //    int policySysID = 0;
        //    StringBuilder sb = new StringBuilder();

        //    Dictionary<string, int> dict = new Dictionary<string, int>();
        //    sb.Append(" SELECT P.POLICY_SYSTEM_ID from POLICY_X_INSURED PXE ");
        //    sb.Append(" INNER JOIN POLICY P ON PXE.POLICY_ID=P.POLICY_ID");
        //    sb.Append(" WHERE PXE.INSURED_EID = ~ENTITYID~ ");
        //    dict.Add("ENTITYID", iEntityID);
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(sb.ToString()))
        //        {
        //            policySysID = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(sConnectionString, sb.ToString(), dict), base.ClientId);
        //        }

        //        if (policySysID > 0)
        //            isExternal = true;
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    finally
        //    {
        //        sb = null;
        //        dict = null;
        //    }
        //    return isExternal;
        //}
      
        private void DisplayUnitNo()
        {

            string sStaUnitNo = string.Empty;
            string sRiskLoc = string.Empty;
            string sSubLoc = string.Empty;

            StringBuilder sbSql = new StringBuilder();
            // RMA-10039: Ash, fixed error but in general this is incorrect handling for POINT, Integral, Staging
            sbSql.Append("SELECT POINT_UNIT_DATA.STAT_UNIT_NUMBER, POINT_UNIT_DATA.UNIT_RISK_LOC, POINT_UNIT_DATA.UNIT_RISK_SUB_LOC from POLICY_X_UNIT,POINT_UNIT_DATA,PERSON_INVOLVED ");
            sbSql.Append(" WHERE POLICY_X_UNIT.POLICY_UNIT_ROW_ID=PERSON_INVOLVED.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.UNIT_ID=POINT_UNIT_DATA.UNIT_ID ");
            sbSql.Append(" AND POINT_UNIT_DATA.UNIT_TYPE=POLICY_X_UNIT.UNIT_TYPE AND PERSON_INVOLVED.PI_ROW_ID=" + PiOther.PiRowId);
            
            DbReader objReader = null;
            objReader = DbFactory.GetDbReader(base.m_fda.connectionString, sbSql.ToString());
            if (objReader != null)
            {
                if (objReader.Read())
                {
                    sStaUnitNo = objReader.GetValue("STAT_UNIT_NUMBER").ToString();
                    sRiskLoc = objReader.GetValue("UNIT_RISK_LOC").ToString();
                    if (sRiskLoc == string.Empty)
                        sRiskLoc = "-";

                    sSubLoc = objReader.GetValue("UNIT_RISK_SUB_LOC").ToString();
                    if (sSubLoc == string.Empty)
                        sSubLoc = "-";

                    base.ResetSysExData("UnitNo", sStaUnitNo + "/" + sRiskLoc + "/" + sSubLoc);


                }

            }


		}

		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;

			// Perform data validation
			string sToday = Conversion.ToDbDate(System.DateTime.Now);
			string sTime = Conversion.GetTime(System.DateTime.Now.ToShortTimeString());

			if(PiOther.PiEntity.BirthDate!="")
			{
				if(PiOther.PiEntity.BirthDate.CompareTo(sToday)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId, sLangCode), Globalization.GetString("Field.BirthDate", base.ClientId, sLangCode), System.DateTime.Now.ToShortDateString()),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}
			
			// Return true if there were validation errors
			Cancel = bError;
		}

		private void PopulateEntityTypes()
		{
            //Added by Shivendu to add a new value "" in the drop down
            bool bBlankValue = true;

            //npradeepshar 07/05/2011 R8 Added Driver code
            string strTableName = base.GetSysExDataNodeText("/SysExData/PageTypeName", false);
            if (string.Compare(strTableName,"")!=0)
            {
                strTableName = " AND UPPER(SYSTEM_TABLE_NAME) = '" + strTableName.ToUpper() + "'"; //akaur9 11/16/2011 MITS 26588
            }
            //npradeepshar 07/05/2011 End
            string sSQL = string.Empty;
            //rsuhsilaggar JIRA 7767
            if(this.PiOther.Context.InternalSettings.SysSettings.UseEntityRole)
             sSQL = @"SELECT GT.TABLE_NAME, G.TABLE_ID 
							FROM GLOSSARY_TEXT GT, GLOSSARY G 
							WHERE GT.TABLE_ID=G.TABLE_ID AND G.GLOSSARY_TYPE_CODE IN (4,7) "+strTableName+" ORDER BY GT.TABLE_NAME";
            else
                sSQL = @"SELECT GT.TABLE_NAME, G.TABLE_ID 
							FROM GLOSSARY_TEXT GT, GLOSSARY G 
							WHERE GT.TABLE_ID=G.TABLE_ID AND G.GLOSSARY_TYPE_CODE=7 " + strTableName + " ORDER BY GT.TABLE_NAME";
			//Create the necessary SysExData to be used in ref binding
			XmlElement xmlRoot = base.SysEx.CreateElement("EntityTypeList");

			using(DbReader rdr = PiOther.PiEntity.Context.DbConnLookup.ExecuteReader(sSQL))
			{

				//Loop through and create all the option values for the combobox control
                while (rdr.Read())
                {
                    // Reference to employee and quest tables not added in the combo 'Type of person' - as per RM World
                    //srajindersin 02/06/2012 MITS 25339 reverted
                    //srajindersin 01/27/2012 MITS 25339
                    //if (rdr.GetString("TABLE_NAME") != "Employees" && rdr.GetString("TABLE_NAME") != "Medical Staff" && rdr.GetString("TABLE_NAME") != "Physicians")//gdass2 MITS 25339
                    //srajindersin - MITS 28954 07/31/2012 - new entries created in Glossary for following 5 items.
                    if (rdr.GetString("TABLE_NAME") != "Employees" && rdr.GetString("TABLE_NAME") != "Medical Staff" && rdr.GetString("TABLE_NAME") != "Physicians"
                        && rdr.GetString("TABLE_NAME") != "Patients" && rdr.GetString("TABLE_NAME") != "Witness")
                    //END srajindersin 01/27/2012 MITS 25339
                    //srajindersin 02/06/2012 MITS 25339 reverted
                    {
                        //If condition added by Shivendu to add a new value "" in the drop down
                        if (bBlankValue)
                        {
                            //Start by Shivendu to add a new value "" in the drop down
                            XmlElement xmlOptionBlank = base.SysEx.CreateElement("option");
                            xmlOptionBlank.InnerText = "";
                            XmlAttribute xmlOptionBlankAttrib = base.SysEx.CreateAttribute("value");
                            xmlOptionBlankAttrib.Value = "";
                            xmlOptionBlank.Attributes.Append(xmlOptionBlankAttrib);
                            xmlRoot.AppendChild(xmlOptionBlank);
                            bBlankValue = false;
                            //Start by Shivendu for MITS 11709
                            XmlElement xmlOption = base.SysEx.CreateElement("option");
                            xmlOption.InnerText = rdr.GetString("TABLE_NAME");
                            XmlAttribute xmlOptionAttrib = base.SysEx.CreateAttribute("value");
                            xmlOptionAttrib.Value = rdr.GetInt("TABLE_ID").ToString();
                            xmlOption.Attributes.Append(xmlOptionAttrib);
                            xmlRoot.AppendChild(xmlOption);
                            //End by Shivendu for MITS 11709
                            //End by Shivendu to add a new value "" in the drop down
                        }
                        else
                        {
                            XmlElement xmlOption = base.SysEx.CreateElement("option");
                            xmlOption.InnerText = rdr.GetString("TABLE_NAME");
                            XmlAttribute xmlOptionAttrib = base.SysEx.CreateAttribute("value");
                            xmlOptionAttrib.Value = rdr.GetInt("TABLE_ID").ToString();
                            xmlOption.Attributes.Append(xmlOptionAttrib);
                            xmlRoot.AppendChild(xmlOption);
                        }
                    }
                }
                    rdr.Close();    
   			}

			//Add the XML to the SysExData 
			XmlNode objOrig = base.SysEx.SelectSingleNode("/*/EntityTypeList");
			if(objOrig!=null)
				base.SysEx.DocumentElement.RemoveChild(objOrig);
			base.SysEx.DocumentElement.AppendChild(xmlRoot as XmlNode);

            //Start rsushilaggar JIRA 11730
            if (this.PiOther.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                base.CreateSysExData("EntityXTableID", this.PiOther.RoleTableId.ToString());
                base.CreateSysExData("entitytableidtext", this.PiOther.RoleTableId.ToString());
                base.ResetSysExData("EntityTypeName", PiOther.PiEntity.Context.LocalCache.GetUserTableName(PiOther.RoleTableId));
            }
            else
            {
                base.CreateSysExData("EntityXTableID", this.PiOther.PiEntity.EntityTableId.ToString());
                base.ResetSysExData("EntityTypeName", PiOther.PiEntity.Context.LocalCache.GetUserTableName(PiOther.PiEntity.EntityTableId));
            }
        }
        //Added Rakhi for R7:Add Emp Data Elements
        public override void OnUpdateObject()
        {
            base.OnUpdateObject();
			//ijha:Mobile Adjuster
			XmlElement objCaller = null;
			


			objCaller = (XmlElement)base.FormVariables.SelectSingleNode("//caller");
			if (objCaller != null)
			{
                if (objCaller.InnerText == "MobileAdjuster" || objCaller.InnerText == "MobilityAdjuster")
                {
					string sSQL = " SELECT EVENT_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + base.GetSysExDataNodeText("//ClaimNumber") + "'";
					using (DbReader objReader = DbFactory.ExecuteReader(sConnectionString, sSQL))
					{
						if (objReader.Read())
						{
							PiOther.EventId = Conversion.ConvertObjToInt(objReader.GetValue("EVENT_ID"), base.ClientId);
							this.m_iEventId = PiOther.EventId;
							//base.FormVariables.SelectSingleNode("//SysExData//EventId").InnerText = objReader.GetValue("EVENT_ID").ToString();
							if (base.FormVariables.SelectSingleNode("//SysExData//EventId") != null)
							{
								base.FormVariables.SelectSingleNode("//SysExData//EventId").InnerText = objReader.GetValue("EVENT_ID").ToString();

							}

						}
					}
				}
			}
			// ijha end
           
            if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
            {
                PiOther.PiEntity.FormName = "PiOtherForm"; //Mits 22497

                #region "Updating Address Info Object"

                string sAddr1 = PiOther.PiEntity.Addr1;
                string sAddr2 = PiOther.PiEntity.Addr2;
                string sAddr3 = PiOther.PiEntity.Addr3;// JIRA 6420 pkandhari
                string sAddr4 = PiOther.PiEntity.Addr4;// JIRA 6420 pkandhari
                string sCity = PiOther.PiEntity.City;
                int iCountryCode = PiOther.PiEntity.CountryCode;
                int iStateId = PiOther.PiEntity.StateId;
                string sEmailAddress = PiOther.PiEntity.EmailAddress;
                string sFaxNumber = PiOther.PiEntity.FaxNumber;
                string sCounty = PiOther.PiEntity.County;
                string sZipCode = PiOther.PiEntity.ZipCode;
                //RMA-8753 nshah28(Added by ashish)
                string sSearchString = string.Empty;
                //RMA-8753 nshah28(Added by ashish) END
                if (
                        sAddr1 != string.Empty || sAddr2 != string.Empty || sAddr3 != string.Empty || sAddr4 != string.Empty ||
                        sCity != string.Empty || iCountryCode != 0 || iStateId != 0 || sEmailAddress != string.Empty ||
                        sFaxNumber != string.Empty || sCounty != string.Empty || sZipCode != string.Empty
                    )
                {
                    if (PiOther.PiEntity.EntityXAddressesList.Count == 0)
                    {
                        

                        EntityXAddresses objEntityXAddressesInfo = PiOther.PiEntity.EntityXAddressesList.AddNew();
						//RMA-8753 nshah28(Added by ashish) START
                        objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                        objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                        objEntityXAddressesInfo.Address.Addr3 = sAddr3;// JIRA 6420 pkandhari
                        objEntityXAddressesInfo.Address.Addr4 = sAddr4;// JIRA 6420 pkandhari
                        objEntityXAddressesInfo.Address.City = sCity;
                        objEntityXAddressesInfo.Address.Country = iCountryCode;
                        objEntityXAddressesInfo.Address.State = iStateId;
                        objEntityXAddressesInfo.Email = sEmailAddress;
                        objEntityXAddressesInfo.Fax = sFaxNumber;
                        objEntityXAddressesInfo.Address.County = sCounty;
                        objEntityXAddressesInfo.Address.ZipCode = sZipCode;
						//RMA-8753 nshah28(Added by ashish) END
                        objEntityXAddressesInfo.EntityId = PiOther.PiEntity.EntityId;
                        objEntityXAddressesInfo.PrimaryAddress = -1;
                        //objEntityXAddressesInfo.AddressId = -1;
                        AddressForm objAddressForm = new AddressForm(m_fda);
                        sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                        objEntityXAddressesInfo.Address.SearchString = sSearchString;
                        objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, PiOther.Context.RMDatabase.ConnectionString, base.ClientId);
                        objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
                        
                    }
                    else
                    {
                        foreach (EntityXAddresses objEntityXAddressesInfo in PiOther.PiEntity.EntityXAddressesList)
                        {
                            if (objEntityXAddressesInfo !=null && objEntityXAddressesInfo.PrimaryAddress == -1)
                            {
								//RMA-8753 nshah28(Added by ashish) START
                                objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                                objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                                objEntityXAddressesInfo.Address.Addr3 = sAddr3;// JIRA 6420 pkandhari
                                objEntityXAddressesInfo.Address.Addr4 = sAddr4;// JIRA 6420 pkandhari
                                objEntityXAddressesInfo.Address.City = sCity;
                                objEntityXAddressesInfo.Address.Country = iCountryCode;
                                objEntityXAddressesInfo.Address.State = iStateId;
                                objEntityXAddressesInfo.Email = sEmailAddress;
                                objEntityXAddressesInfo.Fax = sFaxNumber;
                                objEntityXAddressesInfo.Address.County = sCounty;
                                objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                                AddressForm objAddressForm = new AddressForm(m_fda);
                                sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                                objEntityXAddressesInfo.Address.SearchString = sSearchString;
                                objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, PiOther.Context.RMDatabase.ConnectionString, base.ClientId);
                                objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
								//RMA-8753 nshah28(Added by ashish) END
                                break;
                            }

                        }
                    }
                }
                else
                {
                    if (PiOther.PiEntity.EntityXAddressesList.Count > 0)
                    {
                        foreach (EntityXAddresses objEntityXAddressesInfo in PiOther.PiEntity.EntityXAddressesList)
                        {
                            if (objEntityXAddressesInfo !=null && objEntityXAddressesInfo.PrimaryAddress == -1)
                            {
                                //PiOther.PiEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                                PiOther.PiEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId); //RMA-8753 nshah28(Added by ashish)
                                break;
                            }

                        }
                    }

                }
            #endregion

                #region Added for updating Phone Numbers

                string sOfficePhone =  PiOther.PiEntity.Phone1.Trim();
                string sHomePhone =  PiOther.PiEntity.Phone2.Trim();
                int iHomeCode = objCache.GetCodeId("h", "PHONES_CODES");
                int iOfficeCode = objCache.GetCodeId("o", "PHONES_CODES");
                bool bOfficePhoneEntered = false;
                AddressXPhoneInfo objAddressesInfo = null;
                bool bOfficeCodeExists = false;
                bool bHomePhoneEntered = false;
                bool bHomeCodeExists = false;

                if (sOfficePhone != string.Empty)
                    bOfficePhoneEntered = true;
                if (sHomePhone != string.Empty)
                    bHomePhoneEntered = true;

                if ( PiOther.PiEntity.AddressXPhoneInfoList.Count == 0)
                {
                    if (bOfficePhoneEntered)
                    {
                        objAddressesInfo =  PiOther.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iOfficeCode;
                        objAddressesInfo.PhoneNo = sOfficePhone;
                    }
                    if (bHomePhoneEntered)
                    {
                        objAddressesInfo =  PiOther.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iHomeCode;
                        objAddressesInfo.PhoneNo = sHomePhone;
                    }
                }
                else
                {
                    foreach (AddressXPhoneInfo objXAddressesInfo in  PiOther.PiEntity.AddressXPhoneInfoList)
                    {
                        if (objXAddressesInfo.PhoneCode == iOfficeCode)
                        {
                            if (bOfficePhoneEntered)
                            {
                                objXAddressesInfo.PhoneNo = sOfficePhone;
                                bOfficeCodeExists = true;
                            }
                            else
                            {
                                 PiOther.PiEntity.AddressXPhoneInfoList.Remove(objXAddressesInfo.PhoneId);
                            }
                            continue;
                        }
                        else if (objXAddressesInfo.PhoneCode == iHomeCode)
                        {
                            if (bHomePhoneEntered)
                            {
                                objXAddressesInfo.PhoneNo = sHomePhone;
                                bHomeCodeExists = true;
                            }
                            else
                            {
                                 PiOther.PiEntity.AddressXPhoneInfoList.Remove(objXAddressesInfo.PhoneId);
                            }
                        }
                    }
                    if (bOfficePhoneEntered && !bOfficeCodeExists)
                    {
                        objAddressesInfo =  PiOther.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iOfficeCode;
                        objAddressesInfo.PhoneNo = sOfficePhone;
                    }
                    if (bHomePhoneEntered && !bHomeCodeExists)
                    {
                        objAddressesInfo =  PiOther.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iHomeCode;
                        objAddressesInfo.PhoneNo = sHomePhone;
                    }
                }

                #endregion
            }
           
        }
        //Added Rakhi for R7:Add Emp Data Elements
        //avipinsrivas Start : Worked for Jira-340
        //private int GetEntityRoleTableID(int iPiErRowID, EntityXRoleList objEXRList = null)
        //{
        //    int iEntityTableID = 0;

        //    if (objEXRList != null)
        //    {
        //        if (this.PiOther.Context.InternalSettings.SysSettings.UseEntityRole)
        //        {
        //            if (iPiErRowID > 0 && objEXRList.Count > 0)
        //            {
        //                foreach (EntityXRole objEXR in objEXRList)
        //                {
        //                    if (int.Equals(iPiErRowID, objEXR.ERRowId))
        //                        iEntityTableID = objEXR.EntityTableId;
        //                }
        //            }
        //        }
        //    }
        //    else if (iPiErRowID > 0)
        //    {
        //        StringBuilder sbQuery = null;
        //        Dictionary<string, string> objDictParams = null;
        //        bool bSuccess;
        //        try
        //        {
        //            sbQuery = new StringBuilder();
        //            objDictParams = new Dictionary<string, string>();

        //            sbQuery.Append(" SELECT ENTITY_TABLE_ID ");
        //            sbQuery.Append(" FROM  ENTITY_X_ROLES");
        //            sbQuery.Append(" WHERE ER_ROW_ID = {0}");

        //            sbQuery.Replace("{0}", "~ERROWID~");

        //            objDictParams.Add("ERROWID", iPiErRowID.ToString());

        //            iEntityTableID = Conversion.CastToType<Int32>(DbFactory.ExecuteScalar(sConnectionString, sbQuery.ToString(), objDictParams).ToString(), out bSuccess);
        //        }
        //        finally
        //        {
        //            sbQuery = null;
        //            objDictParams = null;
        //        }
        //    }

        //    return iEntityTableID;
        //}
        //avipinsrivas End
	}
}
