using System;
using Riskmaster.DataModel;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using Riskmaster.Settings;
namespace Riskmaster.BusinessAdaptor
{
	public enum enumFormActionType:int
	{
		None = 0,
		MoveFirst = 1,
		MoveLast = 2,
		MoveNext = 3,
		MovePrevious = 4,
		AddNew = 5,
		Save = 6,
		Delete = 7,
		Refresh = 8,
		MoveTo = 9,
		CancelChildren = 10,
		ApplyLookupData = 11};

	// Enum covers Form Navigation Type reflecting tool bar scroll button availability.
	// form behaviour like scrolling from record to record, new, delete, save record
	public enum enumFormNavigationType:int
	{
		FormNavDefault = 0,       // Standard Navigation Scrolling, New, Delete, Lookup, Save enabled
		FormNavOneToOne = 1,      // Form used to show only one record, New, Delete, Scroll, Lookup disabled, Save enabled
		FormNavNone = 2,
		FormNavSaveOnly = 3};
    public enum enummodifiedcontrolType:int
    {
        FormButton = 0,
        PolicyLookup = 1,
        Labels = 2,
        ComboBox = 3,
        PiEntityLookup = 4,
        ToggleControls=5,
        TextBox=6,
        RadioButton=7,
        Hidden=8,
        Div=9,
        Date=10,
        Checkbox=11,
        PolicyNumberLookup
    };

	/// <summary>
	/// Class implements common screen operations. Any extended operation  
	/// can be done in specific derived screen classes.                       
	/// Author: Brian Battah, 04/28/2005             
	/// </summary>
	public class FormBase
	{
		
		//Used here to Identify Riskmaster Security Offsets
		public const int RMO_ACCESS = 0;
		public const int RMO_VIEW = 1;
		public const int RMO_VIEW_BLANK_NO_CREATE=6; //BSB 02.17.2006 Identify Failed View attempt when
										   // NO data exists AND user does not have "Create" permission
										   // which prevents the system from showing a "blank" record as normal.
		public const int RMO_UPDATE = 2;
		public const int RMO_CREATE = 3;
		public const int RMO_DELETE = 4;
		public const int RMO_IMPORT = 5;
		public const int RMO_SUPP = 50;
		public const int RMO_ATTACHACCESS = 6;
		public const int RMO_SEARCH  = 8000000;
		public const int RMO_LOOKUP  = 7000000;
		public const int RMO_CLAIM_SEARCH = 1;
		public const int RMO_EVENT_SEARCH = 2;
		public const int RMO_EMPLOYEE_SEARCH = 3;
		public const int RMO_ENTITY_SEARCH = 4;
		public const int RMO_VEHICLE_SEARCH = 5;
		public const int RMO_POLICY_SEARCH = 6;
		public const int RMO_PAYMENT_SEARCH = 7;
		public const int RMO_PATIENT_SEARCH = 8;
		public const int RMO_PHYSICIAN_SEARCH = 9;
		public const int RMO_MEDICAL_STAFF_SEARCH = 10;
		public const int RMO_DISABILITY_PLAN_SEARCH = 11;
        //Start-Mridul Bansal. 10/19/09 MITS:18230
        public const int RMO_PROPERTY_SEARCH = 12;
        //End-Mridul Bansal. 10/19/09 MITS:18230
		public const int RMO_BANK_ACCOUNT_LOOKUP = 30;
		public const int RMO_BANK_ACCOUNT_DEPOSIT_LOOKUP = 31;
		public const int RMO_PEOPLE = 14000;
		public const int RMO_ENTITYMAINTENANCE=16500;
        //nadim for 13748,added to hide/unhide SSN field
        public const int RMO_PATIENT = 21000;
        public const int RMO_PHYSICIAN = 21200;
        public const int RMO_MED_STAFF = 21300;
        public const int RMO_GC_CLAIMANTS = 1800;
        public const int RMO_VA_CLAIMANTS = 7650;
        //nadim for 13748,added to hide/unhide SSN field
        //Aman Driver Enh
        public const int RMO_DRIVER = 25200;
        //Aman Driver Enh
		// Record specific
		public const int RMO_EVENT = 1100;
		public const int RMO_EVENT_CHANGE_DEPARTMENT = 31;
		public const int RMO_CLAIM_CHANGE_DEPARTMENT = 33;
		public const int RMO_POLICY_MCO_CHANGE = 34;
		public const int RMO_CLAIM_ALLOW_CLOSE = 30;
		public const int RMO_CLAIM_ALLOW_REOPEN = 31;
		public const int RMO_CLAIM_UPDATE_CLOSED = 32;
		public const int RMO_CLAIM_NUMBER_ALLOW_EDIT = 35;
        public const int RMO_ALLOW_EDIT_EVENT_NUM = 36;  // 'user edit the event number (if "no" the event number box is locked; if "yes" and the user edits the event number from a claim this changes the event number field of both the claim record and the event record)
		public const int RMO_CLAIM_NUMBER_ALLOW_ENTRY = 37;
		public const int RMO_ALLOW_ENTRY_EVENT_NUM = 38;  //  'user enter an event number (if "no" a user can choose an event from a new claim and if none is chosen a new event is auto-generated with the new claim)
		public const int RMO_SHOW_QUALITY_MANAGEMENT_TAB = 39;
		public const int RMO_LAUNCH_CLAIM_FROM_EVENT_SCREEN=30;
		//'Public Const RMO_ALLOW_CHOOSE_EVENT_NUM = 9999  'make "..." event lookup button disappear, thus forcing user to create new event with every claim
        //nadim for 13748,added for hide/unhide SSN field
        public const int RMO_VIEW_SSN = 5;
        public const int RMO_MAINT_VIEW_SSN = 11;
        public const int RMO_PI_EMP_VIEW_SSN = 11;
        public const int RMO_PI_MED_VIEW_SSN = 12;
        public const int RMO_PI_OTHERS_VIEW_SSN = 13;
        public const int RMO_PI_PATIENT_VIEW_SSN = 14;
        public const int RMO_PI_PHYSICIAN_VIEW_SSN = 15;
        public const int RMO_PI_WITNESS_VIEW_SSN = 16;
        public const int RMO_PI_DRIVER_VIEW_SSN = 17; //Aman Driver Enh

        //nadim for 13748
        public const int RMO_ENABLE_SALARY=30;
		public const int RMO_VIEW_HOSPITAL_INFO=35;
		public const int RMO_VIEW_PHYSICIAN_INFO=36;
		public const int RMO_VIEW_DIAGNOSIS_INFO=37;
		public const int RMO_VIEW_TREATMENT_INFO=38;
		public const int RMO_WC_EMPLOYEE=4800;
		public const int RMO_DI_EMPLOYEE=61800;
		public const int RMO_PRINT_JURISDICTIONAL_FORMS=30;
		public const int JUR_OFFSET = 100;
		public const int RMO_PI_EMPLOYEE = 31;
		public const int RMB_WC_PI = 3150;
		public const int RMB_DI_PI = 60150;

        //Abhishek MITS 11579 Start
        public const int RMO_GC_ENTITY_EDIT = 1080;
        public const int RMO_WC_ENTITY_EDIT = 3930;
        public const int RMO_VC_ENTITY_EDIT = 6930;
        public const int RMO_NON_OC_ENTITY_EDIT = 60930;
        //Abhishek MITS 11579 End
        //pmahli MITS 11535 2/15/2008 - Defined a new constant for Autocheck Enter/Edit Payee Fields Directly
        public const int RMO_AUTOCHECKS_EDITPAYEE = 31;
        //Changed by gagan for MITS 11589 : Start
        public const int RMO_AUTOCHECKS_EDIT_ENTER_CONTROL_NUMBER = 32;
        //Changed by gagan for MITS 11589 : End
        //Shruti for 11244
        public const int RMO_POLICY_CLONE = 250;
        //Shruti for 11658
        //Added By Nitika for MITS 28869 : Start
        //public const int RMO_COPY_TRANSACTION = 1251;
        //Added By Nitika for MITS 28869 : End
        public const int RMO_EXPO_ROLLUP = 30;
        //MGaba2: MITS 10241 : BackDating of Claim Status History: Start
        public const int RMO_GC_BKDATE_CLAIM_STATUS_HISTORY = 1220399914;
        public const int RMO_WC_BKDATE_CLAIM_STATUS_HISTORY = 1220397065;
        public const int RMO_VA_BKDATE_CLAIM_STATUS_HISTORY = 1220394066;
        public const int RMO_DI_BKDATE_CLAIM_STATUS_HISTORY = 1220340067;
        //MGaba2: MITS 10241 :End
        //Start:Added by Nitin Goel:Correct SMS setting for Property Claim,04/02/2010,MITS#19314
        public const int RMO_PC_BKDATE_CLAIM_STATUS_HISTORY = 1220360068;
        //End:Nitin Goel,04/02/2010:Correct SMS setting for Property Claim,MITS#19314

        //MGaba2: MITS 15467:start
        public const int RMO_GC_ADJUSTER = 1050;
        public const int RMO_VA_ADJUSTER = 6900;
        public const int RMO_WC_ADJUSTER = 3900;
        public const int RMO_DI_ADJUSTER = 60900;

        public const int RMO_GC_ADJ_DATED_TEXT = 1200;
        public const int RMO_VA_ADJ_DATED_TEXT = 7050;
        public const int RMO_WC_ADJ_DATED_TEXT = 4050;
        public const int RMO_DI_ADJ_DATED_TEXT = 61050;
        //Start-Mridul Bansal. 01/04/10. MITS#19314. Support for Property Claims.
        public const int RMO_PC_ADJUSTER = 41050;
        //Start:Added by Nitin Goel:Correct SMS setting for Property Claim,04/02/2010,MITS#19314
        //public const int RMO_PC_ADJ_DATED_TEXT = 41090;
        public const int RMO_PC_ADJ_DATED_TEXT = 41200;
        //End:Nitin Goel,04/02/2010:Correct SMS setting for Property Claim,MITS#19314l
        //End-Mridul Bansal. 01/04/10. MITS#19314. Support for Property Claims.
        //MGaba2: MITS 15467:End

        //MGaba2:MITS 16498:Start

        public const int RMO_GC_CLAIMANT = 1800;
        public const int RMO_VA_CLAIMANT = 7650;

        public const int RMO_GC_DEFENDANT = 1650;
        public const int RMO_VA_DEFENDANT = 7500;
        public const int RMO_WC_DEFENDANT = 4500;
        public const int RMO_DI_DEFENDANT = 61500;

        public const int RMO_GC_LITIGATION = 1350;
        public const int RMO_VA_LITIGATION = 7200;
        public const int RMO_WC_LITIGATION = 4200;
        public const int RMO_DI_LITIGATION = 61200;

        public const int RMO_GC_SUBROGATION = 44000;
        public const int RMO_VA_SUBROGATION = 44100;
        public const int RMO_WC_SUBROGATION = 44200;
        public const int RMO_DI_SUBROGATION = 44300;
        public const int RMO_PC_SUBROGATION = 44400;

        //asingh263 mits 34597 starts
        public const int RMO_GC_ARBITRATION = 47000;
        public const int RMO_VA_ARBITRATION = 47100;
        public const int RMO_WC_ARBITRATION = 47200;
        public const int RMO_DI_ARBITRATION = 47300;
        public const int RMO_PC_ARBITRATION = 47400;
        //asingh263 mits 34597 ends


        //added by Amitosh for R8 enhancement of PropertyLoss
        public const int RMO_GC_PROPERTYLOSS = 46000;  
        //end Amitosh
        //added by Amitosh for R8 enhancement of Salvage
        public const int RMO_GC_PROPERTYLOSS_SALVAGE = 49000;
        //end Amitosh
        //added by Manika for R8 enhancement of Salvage
        public const int RMO_GC_VEHICLELOSS_SALVAGE = 57000;
        //end Manika
        //added by Manika for R8 enhancement of Demand Offer
        public const int RMO_GC_VEHICLELOSS_DEMANDOFFER = 58000;
        public const int RMO_GC_PROPERTYLOSS_DEMANDOFFER = 50600;
        //end Manika

        //added by Amitosh for R8 enhancement of VehicleLoss
        public const int RMO_GC_VEHILCELOSS = 56000;
        //end Amitosh
        //added by Amitosh for R8 enhancement of LiabilityLoss

        public const int RMO_GC_LIABILITYLOSS = 54000;
        public const int RMO_WC_LIABILITYLOSS = 55000;

        //end Amitosh

        //added by rupal for policy system interface
        
        public const int RMO_WC_SITELOSS = 90000;
        public const int RMO_GC_OTHERUNITLOSS = 91000;
        public const int RMO_WC_OTHERUNITLOSS = 92000;

        //end rupal

        public const int RMO_GC_SUBROGATION_DEMAND_OFFER = 44500;
        public const int RMO_VA_SUBROGATION_DEMAND_OFFER = 44600;
        public const int RMO_WC_SUBROGATION_DEMAND_OFFER = 44700;
        public const int RMO_DI_SUBROGATION_DEMAND_OFFER = 44800;
        public const int RMO_PC_SUBROGATION_DEMAND_OFFER = 44900;

        public const int RMO_GC_LITIGATION_DEMAND_OFFER = 45000;
        public const int RMO_VA_LITIGATION_DEMAND_OFFER = 45100;
        public const int RMO_WC_LITIGATION_DEMAND_OFFER = 45200;
        public const int RMO_DI_LITIGATION_DEMAND_OFFER = 45300;
        public const int RMO_PC_LITIGATION_DEMAND_OFFER = 45400;

        //change start here by swati MITS # 35363
        public const int RMO_GC_CLAIMANT_DEMAND_OFFER = 1860;
        public const int RMO_VA_CLAIMANT_DEMAND_OFFER = 7680;
        public const int RMO_PC_CLAIMANT_DEMAND_OFFER = 41860;
        //change end here by swati

        //Geeta 08/12/09 : Mits 14081 start     
        public const int RMO_GC_LITIGATION_EXPERT = 1500;
        public const int RMO_VA_LITIGATION_EXPERT = 7350;
        public const int RMO_WC_LITIGATION_EXPERT = 4350;
        public const int RMO_DI_LITIGATION_EXPERT = 61350;
        //Geeta 08/12/09 : Mits 14081 End

        //rsushilaggar MITS 29691 Date 04/01/2013
        //public const int RMO_WC_ACCOMMODATION = 23400;
        //public const int RMO_DI_ACCOMMODATION = 623400;
        

        //Start-Mridul Bansal. 01/04/10. MITS#19314. Support for Property Claims.
        //Start:Added by Nitin Goel:Correct SMS setting for Property Claim,04/02/2010,MITS#19314
        //public const int RMO_PC_CLAIMANT = 41700;
        public const int RMO_PC_CLAIMANT = 41800;
        //End:Nitin Goel,04/02/2010:Correct SMS setting for Property Claim,MITS#19314
        public const int RMO_PC_DEFENDANT = 41650;
        public const int RMO_PC_LITIGATION = 41350;
        //Start:Added by Nitin Goel:Correct SMS setting for Property Claim,04/02/2010,MITS#19314
        //public const int RMO_PC_LITIGATION_EXPERT = 41400;
        public const int RMO_PC_LITIGATION_EXPERT = 41500;
        //End:Nitin Goel,04/02/2010:Correct SMS setting for Property Claim,MITS#19314l
        //End-Mridul Bansal. 01/04/10. MITS#19314. Support for Property Claims.

        //Yatharth : Mits 18097 Start - Added the function id for the Allow Acess of WC-PDF Forms Permission
        public const int RMO_ALLOW_WC_PDF_FORMS = 4860;
        //Yatharth : Mits 18097 End

        //Start:Sumit (10/21/2010)- MITS# 22730
        public const int RMB_POLMGTPC_ENH = 42000;
        //End:Sumit

        //smishra54: R8 Withholding Enhancement, MITS 26019
        public const int RMO_ENTITYMAINT_BACKUPWITHHOLDING = 16540;
        public const int RMO_ORGHIERARCHYMAINT_BACKUPWITHHOLDING = 17040;
        //smishra:End
        //Added by Manika for Withholding Enhancement, MITS 26019
        public const int RMO_EMPMAINT_WITHHOLDING = 15570;
        public const int RMO_PEOPLEMAINT_WITHHOLDING = 18512;
        public const int RMO_PHYSICIANMAINT_WITHHOLDING = 21370;
        public const int RMO_PATIENTMAINT_WITHHOLDING = 21512;
        public const int RMO_STAFFMAINT_WITHHOLDING = 21380;
        public const int RMO_DRIVERMAINT_WITHHOLDING = 25370; //Aman Driver Enh

        public const int RMO_EMPMAINT_BACKUPWITHHOLDING = 15670;
        public const int RMO_PEOPLEMAINT_BACKUPWITHHOLDING = 18540;
        public const int RMO_PHYSICIANMAINT_BACKUPWITHHOLDING = 21470;
        public const int RMO_PATIENTMAINT_BACKUPWITHHOLDING = 21540;
        public const int RMO_STAFFMAINT_BACKUPWITHHOLDING = 21480;
        public const int RMO_DRIVERMAINT_BACKUPWITHHOLDING = 25380;  //Achla MITS 31283
        //End Manika
        //Added by Manika for EFT Payments, MITS 26019 
        public const int RMO_EMPLOYEEMAINT_EFT = 17513;
        public const int RMO_PEOPLEMAINT_EFT = 18513;
        public const int RMO_PHYSICIANMAINT_EFT = 21580;
        public const int RMO_PATIENTMAINT_EFT = 21513;
        public const int RMO_STAFFMAINT_EFT = 22360;
        public const int RMO_DRIVERMAINT_EFT = 25580; //Aman Driver Enh
        //End Manika
        //Added by Amitosh for EFT Payments
        public const int RMO_ENTITYMAINT_EFT = 16513;
        public const int RMO_ENTITYMAINT_WITHHOLDING = 16512;
        public const int RMO_ORGHIERARCHYMAINT_EFT = 17011;
        public const int RMO_ORGHIERARCHYMAINT_WITHHOLDING = 17010;
        //End Amitosh

        //PSHEKHAWAT : Added for Accommodation and Vocational Rehab, MITS:23341
        public const int RMO_WC_ACCOMMODATION = 23400;
        public const int RMO_WC_VOCREHAB = 23500;
        public const int RMO_DI_ACCOMMODATION = 623400;
        public const int RMO_DI_VOCREHAB = 623500;
        //PSHEKHAWAT : End

        //aaggarwal29 : Added for Treatment Plan ;MITS 26215 start
        public const int RMO_WC_TREATMENT_PLAN = 23200;
        public const int RMO_DI_TREATMENT_PLAN = 623200;
        //aaggarwal29 : MITS 26215 end

        //Start averma62 MITS 32103 
        // akaushik5 Changed for MITS 37554 Starts
        //public const int RMO_WC_MEDMGTSAVINGS = 27000;  //For (WC) Medical management savings
        public const int RMO_WC_MEDMGTSAVINGS = 23300;  //For (WC) Medical management savings
        // akaushik5 Changed for MITS 37554 Ends
        public const int RMO_DI_MEDMGTSAVINGS = 623300; //For (Non Occ) Medical management savings
        //End averma62 MITS 32103

        //Manish MITS 26426
        public const int FINAL_PAYMENT = 10254;

        //aaggarwal29 : Added for Case Management;MITS 28485 start
        public const int RMO_WC_CASE_MANAGEMENT = 23100;
        public const int RMO_DI_CASE_MANAGEMENT = 623100;
        public const int RMO_WC_MANAGER_NOTES = 231000;
        public const int RMO_DI_MANAGER_NOTES = 633100;
        //aaggarwal29 : MITS 28485 end
        //Deb : MITS 32257
        public const int RMO_ALLOW_UPDATE_EXISTING_ENTITY = 73;

        //zmohammad MITs 35169 : Checking for SMS permission.
        public const int RMO_ALLOW_EDIT_MDA_DURATIONS = 40;

        //Amandeep Catastrophe Enhancement MITS 28528
        public const int RMO_CATASTROPHEMAINT = 80000;

        //MITS:34276-- Entity ID Type View Permission start        
        public const int RMO_ENT_IDTYPE = 1220400250;
        public const int RMO_ENT_IDTYPE_VIEW = 1220400251;
        public const int RMO_ENT_IDTYPE_EDIT = 1220400252;
        public const int RMO_ENT_IDTYPE_DELETE = 1220400254;
        public const int RMO_ENT_IDTYPE_NEW = 1220400253;

        public const int RMO_ENT_VENDORTYPE = 1220400255;
        public const int RMO_ENT_VENDORTYPE_VIEW = 1220400256;
        public const int RMO_ENT_VENDORTYPE_NEW = 1220400258;
        public const int RMO_ENT_VENDORTYPE_UPDATE = 1220400257;
        public const int RMO_ENT_VENDORTYPE_DELETE = 1220400259;
        //MITS:34276-- Entity ID Type View Permission end
        //akaushik5 Added for MITS 37383 Starts
        /// <summary>
        /// The Admin Tracling Email Notification. 
        /// </summary>
        public const int RMO_ADM_EMAIL_NOTIFICATION = 33200;
        //akaushik5 Added for MITS 37383 Ends

        //bsharma33 :  RMA-3887 Reserve supplementals started
        public const int RMO_GC_RESERVE = 2250;
        public const int RMO_WC_RESERVE = 5400;
        //bsharma33 :  RMA-3887 Reserve supplementals ends
        // npadhy Deductible Edit rules
        public const int GC_DED = 190;
        public const int GC_DED_VIEW = 1;
        public const int GC_DED_EDIT = 2;
        public const int GC_DED_TYP_EDIT = 30;
        public const int GC_DED_AMT_EDIT = 31;
        public const int GC_DIM_TYP_EDIT = 32;
        public const int GC_DIM_PER_EDIT = 33;
        public const int GC_DED_SUPP = 190;//skhare7 JIRA 14055


        public const int WC_DED = 3040;
        public const int WC_DED_VIEW = 1;
        public const int WC_DED_EDIT = 2;
        public const int WC_DED_TYP_EDIT = 30;
        public const int WC_DED_AMT_EDIT = 31;
        public const int WC_DIM_TYP_EDIT = 32;
        public const int WC_DIM_PER_EDIT = 33;
        public const int WC_DED_SUPP = 3040;//skhare7 JIRA 14055
        //Raman Bhatia: ArrayList to maintain all modified nodes in R5
        //protected ArrayList m_ModifiedControls = null;
        public List<ArrayList> m_ModifiedControls = null;
        private string[] m_stringSeparator = new string[] { "|||" };
        // vsharma203 Added for Restricting Add New Button 
        //on QuickLookup for exinsting Claimant
        public const int RMO_Restrict_Add_New = 12;
        // vsharma203 Added for Restricting Add New Button ends

        //-----sgupta320 Jira-15676
        public const int RMO_GC_PERSONINVOLVED = 27100;
        public const int RMO_DI_PERSONINVOLVED = 31100;
        public const int RMO_PC_PERSONINVOLVED = 29100;
        public const int RMO_WC_PERSONINVOLVED = 28100;
        public const int RMO_VA_PERSONINVOLVED = 30100;
        public const int RMO_POLICY_PERSONINVOLVED = 32100;
        //-----sgupta320 Jira-151676 end


        //-----sgupta320 Jira-17835
        public const int RMO_GC_PERSONINVOLVED_WorkLoss = 27550;
        public const int RMO_GC_PERSONINVOLVED_Restrictions = 27400;
        public const int RMO_GC_PERSONINVOLVED_Dependents = 27250;
        public const int RMO_GC_PERSONINVOLVED_Procedure = 27700;

        public const int RMO_DI_PERSONINVOLVED_WorkLoss = 31550;
        public const int RMO_DI_PERSONINVOLVED_Restrictions = 31400;
        public const int RMO_DI_PERSONINVOLVED_Dependents = 31250;
        public const int RMO_DI_PERSONINVOLVED_Procedure = 31700;

        public const int RMO_PC_PERSONINVOLVED_WorkLoss = 29550;
        public const int RMO_PC_PERSONINVOLVED_Restrictions = 29400;
        public const int RMO_PC_PERSONINVOLVED_Dependents = 29250;
        public const int RMO_PC_PERSONINVOLVED_Procedure = 29700;

        public const int RMO_WC_PERSONINVOLVED_WorkLoss = 28550;
        public const int RMO_WC_PERSONINVOLVED_Restrictions = 28400;
        public const int RMO_WC_PERSONINVOLVED_Dependents = 28250;
        public const int RMO_WC_PERSONINVOLVED_Procedure = 28700;

        public const int RMO_VA_PERSONINVOLVED_WorkLoss = 30550;
        public const int RMO_VA_PERSONINVOLVED_Restrictions = 30400;
        public const int RMO_VA_PERSONINVOLVED_Dependents = 30250;
        public const int RMO_VA_PERSONINVOLVED_Procedure = 30700;

        public const int RMO_POLICY_PERSONINVOLVED_WorkLoss = 32550;
        public const int RMO_POLICY_PERSONINVOLVED_Restrictions = 32400;
        public const int RMO_POLICY_PERSONINVOLVED_Dependents = 32250;
        public const int RMO_POLICY_PERSONINVOLVED_Procedure = 32700;


        //-----sgupta320 Jira-17835 end

        public int ClientId
        {
            get;
            set;
        }

		public FormBase(FormDataAdaptor fda)
		{
			//Stash "Parent" Reference.
			m_fda = fda;
			//Create and stash UI Extender for Scripting
			m_Extender = new FormExtender(this);
            m_ModifiedControls = new List<ArrayList>();
            ClientId = fda.ClientId;
		}
		//BSB	May need to use WeakReference here if Adaptor ends up with a reference to this object...;
		protected FormDataAdaptor m_fda  = null;
		public FormDataAdaptor Adaptor{get{return m_fda;}}
		
		//BSB01.10.2006 Added Extender for UI Scripting
		protected FormExtender m_Extender = null;
		public FormExtender Extender
		{
			get{return m_Extender;}
		}

		protected bool m_DataChanged;

		//	protected int m_objFormStack = new FormStack();
		//	public FormStack FormStack{get{return m_objFormStack;}}

		protected int m_SecurityId;
		public int SecurityId
		{
			get{return m_SecurityId;}
			set
			{
				if(m_SecurityId != value)
				{
					m_SecurityId = value;
					OnSecurityIdChanged();
				}
			}
		}

		protected enumFormNavigationType m_FormNavigationType;
		public enumFormNavigationType FormNavigationType{get{return m_FormNavigationType;}set{m_FormNavigationType = value;}}

		//		private bool m_SupportJurisdictional;
		//		private bool m_SupportSupplementals;
		//		private bool m_SupportQuickDiary;
		//		private string m_StatusFormName;

		protected enumFormActionType m_CurrentAction;
		public enumFormActionType CurrentAction{get{return m_CurrentAction;}set{m_CurrentAction = value;}}


		
		//BSB 02.27.2006 Added to allow form code to signal FDA logic that a requested operation has been 
		// cancelled.  Typical reaction of FDA in that case would be to restore the view information 
		// that was reieved at the start of the request in order to safely add an error message at the top.
		protected bool m_bCancelled;
		public bool Cancelled{get{return m_bCancelled;}set{m_bCancelled = value;}}

		// Events for custom Implementations

		//public virtual void CreateRecordFilter(){;}
		public virtual void Init(){;}

		

		public virtual void GetDataChanged(ref bool boolDataChanged){;}
		public bool ModuleSecurityEnabled{get{return m_fda.userLogin.objRiskmasterDatabase.Status;}}
		
		public virtual void BeforeRefresh(ref bool Cancel){;}
		public virtual void AfterRefresh(){;}
		public virtual void OnValidate(ref bool Cancel){;}
		//BSB Add script hook here for UpdateForm - this is essentially the form rendering
		// logic.
        public virtual void OnUpdateForm()
        {
            using (LocalCache objLocalCache = new LocalCache(m_fda.connectionString,ClientId))
            {
                if (objLocalCache.IsScriptEditorEnabled())
                {
                    string sRenderScriptPrefix = "AfterRender";
                    m_fda.Factory.Context.ScriptEngine.RunScriptMethod(sRenderScriptPrefix + this.GetType().Name, this.Extender);

                }
            }
        }
		public virtual void OnUpdateObject(){;}
		public virtual void OnSecurityIdChanged(){;}

		public virtual void BeforeAction(enumFormActionType eActionType, ref bool Cancel){;}
		public virtual void AfterAction(enumFormActionType eActionType){;}

		protected BusinessAdaptorErrors Errors{get{return Adaptor.m_err;}}

		//Holds dynamic view sections to be substituted for placeholders
		// in the orbeon view document if appropriate.
		private XmlDocument m_SysViewSection = new XmlDocument();
		public XmlDocument SysViewSection{get{ return m_SysViewSection;}}

		//Holds posted dynamic view sections to be substituted for placeholders
		// in the orbeon view document if appropriate. Used to "restore" previous screen view
		// in case of validation or other expected processing errors.
		private XmlDocument m_SysPostedViewSection = new XmlDocument();
		public XmlDocument SysPostedViewSection{get{ return m_SysPostedViewSection;}}

		//BSB Holds complete view (raw as retrieved from db)
		private XmlDocument m_SysView = new XmlDocument();
		public XmlDocument SysView{get{ return m_SysView;}}

        //holds FormVariables (because view is not available)
        private XmlDocument m_FormVariables = new XmlDocument();
        public XmlDocument FormVariables { get { return m_FormVariables; } }

		//BSB Holds complete view (as posted)
		private XmlDocument m_SysPostedView = new XmlDocument();
		public XmlDocument SysPostedView{get{ return m_SysPostedView;}}

		// BSB Holds Extended Parameter information for the current form.
		private XmlDocument m_SysEx = new XmlDocument();
		public XmlDocument SysEx{get{return m_SysEx;}}

		// BSB Holds Extended Parameter information for the current form.
		private XmlDocument m_SysLookup = new XmlDocument();
		public XmlDocument SysLookup{get{return m_SysLookup;}}

		// BSB Holds ScreenStack contains information about the "ancestry" of the 
		// current form.
		private XmlDocument m_SysFormStack = new XmlDocument();
		public XmlDocument SysFormStack{get{ return m_SysFormStack;}}
		
		// BSB/Nikhil Holds individual control attributes and information
		// to be modified from the view.  
		// Implemented so that the populating of this information could be centralized in case
		// the implementation has to change (in a single place) going forward.
		private Hashtable m_ResetAttributes = new Hashtable();
		public Hashtable ResetAttributes{get{return m_ResetAttributes;}}

		// BSB Holds individual control names\types to be removed from the view.  
		// Implemented so that the populating of this information could be centralized in case
		// the implementation has to change (in a single place) going forward.
		private Hashtable m_KillNodeNames = new Hashtable();
		public Hashtable KillNodes{get{return m_KillNodeNames;}}

		// BSB Holds individual control names\types to be removed from the view.  
		// Implemented so that the populating of this information could be centralized in case
		// the implementation has to change (in a single place) going forward.
		private Hashtable m_ReadOnlyNodeNames = new Hashtable();
        private Hashtable m_ReadWriteNodeNames = new Hashtable();
        public Hashtable ReadWriteNodes { get { return m_ReadWriteNodeNames; } }
		public Hashtable ReadOnlyNodes{get{return m_ReadOnlyNodeNames;}}
        private Hashtable m_DisplayNodeNames = new Hashtable();
        public Hashtable DisplayNodes { get { return m_DisplayNodeNames; } }
        //Amitosh mits 23441 01/20/2011
        private Hashtable m_ReadOnlyFieldMark = new Hashtable();
        public Hashtable ReadOnlyFieldMarkNode { get { return m_ReadOnlyFieldMark; } }

        //Added by Amitosh for R8 enhancement of Liabilityloss
        private Hashtable m_KillFieldMark = new Hashtable();
        public Hashtable KillFieldMarkNodes { get { return m_KillFieldMark; } }

        private Hashtable m_SysRequiredFields = new Hashtable();
        public Hashtable SysRequiredFields { get { return m_SysRequiredFields; } }
        public bool AddKillNode(string sNodeName)
		{
			try{
                //srajindersin MITS 32606 dt:05/14/2013
                if (!KillNodes.Contains(sNodeName + "k"))
                    KillNodes.Add(sNodeName+"k",sNodeName);
            }
			catch{return false;}
			return true;}
		public bool AddReadOnlyNode(string sNodeName)
		{
            try {
                //srajindersin MITS 32606 dt:05/14/2013
                if (!ReadOnlyNodes.Contains(sNodeName + "k"))
                    ReadOnlyNodes.Add(sNodeName + "k", sNodeName); 
            }
			catch{return false;}
			return true;}
        public bool AddReadWriteNode(string sNodeName)
        {
            try {
                //srajindersin MITS 32606 dt:05/14/2013
                if (!ReadWriteNodes.Contains(sNodeName + "k"))
                    ReadWriteNodes.Add(sNodeName + "k", sNodeName); 
            }
            catch { return false; }
            return true;
        }

        //JIRA RMA-9252 ajohari2: Start
        public bool RemoveReadWriteNode(string sNodeName)
        {
            try
            {
                if (ReadWriteNodes.Contains(sNodeName + "k"))
                {
                    ReadWriteNodes.Remove(string.Format("{0}k", sNodeName));
                }
            }
            catch { return false; }
            return true;
        }
        //JIRA RMA-9252 ajohari2: End

        public bool AddSysRequiredFields(string sNodeName)
        {
            try { SysRequiredFields.Add(sNodeName + "k", sNodeName); }
            catch { return false; }
            return true;
        }
        //Amitosh mits 23441 01/20/2011
        public bool AddReadOnlyFieldMarkNode(string sNodeName)
        {
            try { ReadOnlyFieldMarkNode.Add(sNodeName + "k", sNodeName); }
            catch { return false; }
            return true;
        }
        //End Amitosh
        //Added by Amitosh for R8 enhancement of LiabilityLoss
        public bool AddKillFieldMarkNode(string sNodeName)
        {
            try { KillFieldMarkNodes.Add(sNodeName + "k", sNodeName); }
            catch { return false; }
            return true;
        }
        //End Amitosh
		
		  public bool AddDisplayNode(string sNodeName)
        {
            try {
                //srajindersin MITS 32606 dt:05/14/2013
                if (!DisplayNodes.Contains(sNodeName + "k"))
                    DisplayNodes.Add(sNodeName + "k", sNodeName); 
            }
            catch { return false; }
            return true;
        }
		
		internal void ClearViewNodeChanges()
		{
			m_KillNodeNames = new Hashtable();
			m_ReadOnlyNodeNames = new Hashtable();
            m_DisplayNodeNames = new Hashtable();
            m_ReadWriteNodeNames = new Hashtable();
            //Amitosh mits 23441 01/20/2011
            m_ReadOnlyFieldMark = new Hashtable();
            //Added by Amitosh for R8 enhancement of LiabilityLoss
            m_KillFieldMark = new Hashtable();
		}

		internal static string SafeGetAttString(XmlNode objNode,string attName)
		{
			string ret = "";
			if(objNode.Attributes.GetNamedItem(attName)!=null)
				ret = objNode.Attributes.GetNamedItem(attName).Value;
			return ret;
		}
		
		// BSB 02.02.2006 RestorePostedViewData
		// Called by FDA to "revert" the screen to the state in which it was posted.
 		// Used for example when validation has failed.
		internal virtual void RestorePostedViewData()
		{
			SysView.DocumentElement.InnerXml = SysPostedView.DocumentElement.FirstChild.InnerXml;
			SysViewSection.DocumentElement.InnerXml = SysPostedViewSection.DocumentElement.InnerXml;
		}

		/// <summary>
		/// BSB 10.27.2005 Update: 
		/// As a performance fix, we now recieve the entire legacy xml view.
		/// As a result, we need to completely apply these changes here. 
		/// Other changes may now be added directly in the C# Form implementation code.
		/// Nothing more is done with "ControlXXXList" nodes on the Orbeon side.
		/// </summary>
		internal void ApplyViewNodeChanges()
		{
			//10.25.2005 Nikhil\BSB Fix for both control and it's parent group being in the 
			// Kill List and the potential that if done in the incorrect order an exception can 
			// be thrown.
			XmlNodeList objSuppNodes = this.SysViewSection.SelectNodes("//button | //control | //buttonscript");
			XmlNodeList objNodes = this.SysView.SelectNodes("//button | //control | //buttonscript");
			foreach(DictionaryEntry e in KillNodes)
			{
				foreach(XmlNode objNode in objNodes)
					if(SafeGetAttString(objNode,"name") == e.Value.ToString())
						objNode.ParentNode.RemoveChild(objNode);
				//pankaj 11/18/05 Bug #418 GA Work.In case of non-Power views Supplementals are inside SysViewSection
				foreach(XmlNode objNode in objSuppNodes)
					if(SafeGetAttString(objNode,"name") == e.Value.ToString())
						objNode.ParentNode.RemoveChild(objNode);
			}

			XmlNodeList objGroupNodes = this.SysView.SelectNodes("//group");
			foreach(DictionaryEntry e in KillNodes)
				foreach(XmlNode objNode in objGroupNodes)
					if(SafeGetAttString(objNode,"name") == e.Value.ToString())
						objNode.ParentNode.RemoveChild(objNode);

			foreach(DictionaryEntry e in ReadOnlyNodes)
            {
				foreach(XmlNode objNode in objNodes)
					if(SafeGetAttString(objNode,"name") == e.Value.ToString())//Make it Read-Only
					{
						XmlElement objElt = objNode as XmlElement;
						XmlAttribute objAtt = null;
						

						if(objElt.GetAttribute("type")=="memo")
						{
							objElt.SetAttribute("type","readonly");
							objAtt = SysView.CreateAttribute("readonlymemo");
							objAtt.Value ="1";
						}
						else
						{
                            // Mihika - add formatas attribute for date, currenc, datetime and time types of controls
                            if (objElt.GetAttribute("type") == "date" || objElt.GetAttribute("type") == "currency"
                                || objElt.GetAttribute("type") == "datetime" || objElt.GetAttribute("type") == "time")
                                if (!objElt.HasAttribute("formatas"))
                                {
                                    objAtt = SysView.CreateAttribute("formatas");
                                    objAtt.Value = objElt.GetAttribute("type");
                                    objElt.Attributes.Append(objAtt);
                                    objAtt = null;
                                }
							if(objElt.HasAttribute("type"))
								objElt.SetAttribute("type","readonly");
							//BSB Not sure stylesheet wise why the following att matters but 
							// that's the way it was...
							objAtt = SysView.CreateAttribute("readonly");
							objAtt.Value ="true";
						}
						objElt.Attributes.Append(objAtt);
                    }
                // Mihika - MCIC - In case of non-Power views Supplementals are inside SysViewSection
                foreach (XmlNode objNode in objSuppNodes)
                    if (SafeGetAttString(objNode, "name") == e.Value.ToString())//Make it Read-Only
                    {
                        XmlElement objElt = objNode as XmlElement;
                        XmlAttribute objAtt = null;


                        if (objElt.GetAttribute("type") == "memo")
                        {
                            objElt.SetAttribute("type", "readonly");
                            objAtt = SysViewSection.CreateAttribute("readonlymemo");
                            objAtt.Value = "1";
                        }
                        else
                        {
                            if (objElt.GetAttribute("type") == "date" || objElt.GetAttribute("type") == "currency"
                                || objElt.GetAttribute("type") == "datetime" || objElt.GetAttribute("type") == "time")
                                if (!objElt.HasAttribute("formatas"))
                                {
                                    objAtt = SysViewSection.CreateAttribute("formatas");
                                    objAtt.Value = objElt.GetAttribute("type");
                                    objElt.Attributes.Append(objAtt);
                                    objAtt = null;
                                }

                            if (objElt.HasAttribute("type"))
                                objElt.SetAttribute("type", "readonly");
                            //BSB Not sure stylesheet wise why the following att matters but 
                            // that's the way it was...
                            objAtt = SysViewSection.CreateAttribute("readonly");
                            objAtt.Value = "true";
                        }
                        objElt.Attributes.Append(objAtt);
                    }
					}

			//Handle the case for AttributeChanges...
            foreach (XmlNode objNode in SysEx.SelectNodes("//ControlAppendAttributeList/*"))
            {
                foreach (XmlNode objTarget in objNodes)
                    if (SafeGetAttString(objTarget, "name") == objNode.LocalName)
                        foreach (XmlNode objAttListNode in objNode.ChildNodes)
                        {
                            objTarget.Attributes.RemoveNamedItem(objAttListNode.LocalName);
                            XmlAttribute objAtt = SysView.CreateAttribute(objAttListNode.LocalName);
                            objAtt.Value = objAttListNode.Attributes["value"].Value;
                            objTarget.Attributes.Append(objAtt);
                        }

                // Naresh The Attributes for the Groups can be changed at Runtime by adding them to ControlAppendAttributeList
                foreach (XmlNode objTarget in objGroupNodes)
                    if (SafeGetAttString(objTarget, "name") == objNode.LocalName)
                        foreach (XmlNode objAttListNode in objNode.ChildNodes)
                        {
                            objTarget.Attributes.RemoveNamedItem(objAttListNode.LocalName);
                            XmlAttribute objAtt = SysView.CreateAttribute(objAttListNode.LocalName);
                            objAtt.Value = objAttListNode.Attributes["value"].Value;
                            objTarget.Attributes.Append(objAtt);
                        }
            }

            //Raman Bhatia: Code commented for R5.. SysView is no longer available here.. All customization would happen at page level
            //UpdateViews will now create appropriate nodes in SysEx and send customized value to UI.. This function does not get call now..
            
            //Changed by Gagan for MITS 10376 : Start
            //UpdateViews(this.SysView);
            //UpdateViews(this.SysViewSection);
            //Changed by Gagan for MITS 10376 : Start

            

		return;
		}




        //Changed by Gagan for MITS 10376 : Start

        /// <summary>
        /// Update all the views as per the dimensions
        /// for freecode,memo,textml and readonly memo
        /// </summary>
        /// <param name="p_objXmlDoc"></param>
        //public void UpdateViews(XmlDocument p_objXmlDoc)
        //{
        //    int iTextMLCols = 0;
        //    int iTextMLRows = 0;
        //    int iFreeCodeCols = 0;
        //    int iFreeCodeRows = 0;
        //    int iReadOnlyMemoCols = 0;
        //    int iReadOnlyMemoRows = 0;
        //    int iMemoCols = 0;
        //    int iMemoRows = 0;
        //    string sSQL = String.Empty;
        //    XmlNodeList objNodeList = null;
        //    XmlAttribute objAttribute = null;
        //    LocalCache objLocalCache = null;


        //    objLocalCache = new LocalCache(m_fda.connectionString);

        //    iTextMLCols = objLocalCache.GetWidth("TextML");
        //    iFreeCodeCols = objLocalCache.GetWidth("FreeCode");
        //    iReadOnlyMemoCols = objLocalCache.GetWidth("ReadOnlyMemo");
        //    iMemoCols = objLocalCache.GetWidth("Memo");

        //    iTextMLRows = objLocalCache.GetHeight("TextML");
        //    iFreeCodeRows = objLocalCache.GetHeight("FreeCode");
        //    iReadOnlyMemoRows = objLocalCache.GetHeight("ReadOnlyMemo");
        //    iMemoRows = objLocalCache.GetHeight("Memo");

        //    objNodeList = p_objXmlDoc.SelectNodes("//control[@type='textml']");
        //    foreach (XmlNode objNode in objNodeList)
        //    {
        //        if (((XmlElement)objNode).Attributes["cols"] == null)
        //        {
        //            objAttribute = p_objXmlDoc.CreateAttribute("cols");
        //            ((XmlElement)objNode).SetAttributeNode(objAttribute);
        //        }
        //        ((XmlElement)objNode).Attributes["cols"].Value = iTextMLCols.ToString();

        //        if (((XmlElement)objNode).Attributes["rows"] == null)
        //        {
        //            objAttribute = p_objXmlDoc.CreateAttribute("rows");
        //            ((XmlElement)objNode).SetAttributeNode(objAttribute);
        //        }
        //        ((XmlElement)objNode).Attributes["rows"].Value = iTextMLRows.ToString();
        //    }

        //    objNodeList = p_objXmlDoc.SelectNodes("//control[@type='freecode']");
        //    foreach (XmlNode objNode in objNodeList)
        //    {
        //        if (((XmlElement)objNode).Attributes["cols"] == null)
        //        {
        //            objAttribute = p_objXmlDoc.CreateAttribute("cols");
        //            ((XmlElement)objNode).SetAttributeNode(objAttribute);
        //        }
        //        ((XmlElement)objNode).Attributes["cols"].Value = iFreeCodeCols.ToString();
        //        if (((XmlElement)objNode).Attributes["rows"] == null)
        //        {
        //            objAttribute = p_objXmlDoc.CreateAttribute("rows");
        //            ((XmlElement)objNode).SetAttributeNode(objAttribute);
        //        }
        //        ((XmlElement)objNode).Attributes["rows"].Value = iFreeCodeRows.ToString();
        //    }

        //    objNodeList = p_objXmlDoc.SelectNodes("//control[@type='readonlymemo']");
        //    foreach (XmlNode objNode in objNodeList)
        //    {
        //        if (((XmlElement)objNode).Attributes["cols"] == null)
        //        {
        //            objAttribute = p_objXmlDoc.CreateAttribute("cols");
        //            ((XmlElement)objNode).SetAttributeNode(objAttribute);
        //        }
        //        ((XmlElement)objNode).Attributes["cols"].Value = iReadOnlyMemoCols.ToString();
        //        if (((XmlElement)objNode).Attributes["rows"] == null)
        //        {
        //            objAttribute = p_objXmlDoc.CreateAttribute("rows");
        //            ((XmlElement)objNode).SetAttributeNode(objAttribute);
        //        }
        //        ((XmlElement)objNode).Attributes["rows"].Value = iReadOnlyMemoRows.ToString();
        //    }

        //    objNodeList = p_objXmlDoc.SelectNodes("//control[@type='memo']");
        //    foreach (XmlNode objNode in objNodeList)
        //    {
        //        if (((XmlElement)objNode).Attributes["cols"] == null)
        //        {
        //            objAttribute = p_objXmlDoc.CreateAttribute("cols");
        //            ((XmlElement)objNode).SetAttributeNode(objAttribute);
        //        }
        //        ((XmlElement)objNode).Attributes["cols"].Value = iMemoCols.ToString();
        //        if (((XmlElement)objNode).Attributes["rows"] == null)
        //        {
        //            objAttribute = p_objXmlDoc.CreateAttribute("rows");
        //            ((XmlElement)objNode).SetAttributeNode(objAttribute);
        //        }

        //        ((XmlElement)objNode).Attributes["rows"].Value = iMemoRows.ToString();
        //    }

        //    objNodeList = null;
        //    objAttribute = null;
        //}

        //Changed by Gagan for MITS 10376 : End

        //Created by Raman for MITS 10376 handling in R5
        /// <summary>
        /// This function would store all rows and cols customized values
        /// for freecode,memo,textml and readonly memo
        /// </summary>
        internal void StoreMemoCustomizations()
        {
            int iTextMLCols = 0;
            int iTextMLRows = 0;
            int iFreeCodeCols = 0;
            int iFreeCodeRows = 0;
            int iReadOnlyMemoCols = 0;
            int iReadOnlyMemoRows = 0;
            int iMemoCols = 0;
            int iMemoRows = 0;
            int iHtmlCols = 0;
            int iHtmlRows = 0;
            string sSQL = String.Empty;
            XmlNodeList objNodeList = null;
            XmlAttribute objAttribute = null;
            XmlElement objEle = null;
            XmlElement objParentEle = null;

            using (LocalCache objLocalCache = new LocalCache(m_fda.connectionString,ClientId))
            {
                iTextMLCols = objLocalCache.GetWidth("TextML");
                iFreeCodeCols = objLocalCache.GetWidth("FreeCode");
                iReadOnlyMemoCols = objLocalCache.GetWidth("ReadOnlyMemo");
                iMemoCols = objLocalCache.GetWidth("Memo");
                iHtmlCols = objLocalCache.GetWidth("HtmlText");//asharma326 jira 6422
                iTextMLRows = objLocalCache.GetHeight("TextML");
                iFreeCodeRows = objLocalCache.GetHeight("FreeCode");
                iReadOnlyMemoRows = objLocalCache.GetHeight("ReadOnlyMemo");
                iMemoRows = objLocalCache.GetHeight("Memo");
                iHtmlRows = objLocalCache.GetHeight("HtmlText");//asharma326 jira 6422
            }

            objParentEle = (XmlElement) this.SysEx.SelectSingleNode("//MemoCustomizations");
            if (objParentEle == null)
            {
                CreateSysExData("MemoCustomizations", "");
                objParentEle = (XmlElement)this.SysEx.SelectSingleNode("//MemoCustomizations");
            }

            objEle = this.SysEx.CreateElement("TextML");
            objEle.SetAttribute("rows", iTextMLRows.ToString());
            objEle.SetAttribute("cols", iTextMLCols.ToString());
            objParentEle.AppendChild(objEle);

            objEle = this.SysEx.CreateElement("FreeCode");
            objEle.SetAttribute("rows", iFreeCodeRows.ToString());
            objEle.SetAttribute("cols", iFreeCodeCols.ToString());
            objParentEle.AppendChild(objEle);

            objEle = this.SysEx.CreateElement("ReadOnlyMemo");
            objEle.SetAttribute("rows", iReadOnlyMemoRows.ToString());
            objEle.SetAttribute("cols", iReadOnlyMemoCols.ToString());
            objParentEle.AppendChild(objEle);

            objEle = this.SysEx.CreateElement("Memo");
            objEle.SetAttribute("rows", iMemoRows.ToString());
            objEle.SetAttribute("cols", iMemoCols.ToString());
            objParentEle.AppendChild(objEle);

            //asharma326 JIRA# 6422 Starts 
            objEle = this.SysEx.CreateElement("HtmlText");
            objEle.SetAttribute("rows", iHtmlRows.ToString());
            objEle.SetAttribute("cols", iHtmlCols.ToString());
            objParentEle.AppendChild(objEle);
            //asharma326 JIRA# 6422 Ends
        }
        /// <summary>
        /// This function adds element to ModifiedControlArrayList
        /// </summary>
        /// <param name="p_arrayList"></param>
        /// <param name="p_sAttribute"></param>
        /// <param name="p_sValue"></param>
        internal void AddElementToList(ref ArrayList p_arrayList , string p_sAttribute, string p_sValue)
        {
            if (p_arrayList != null)
            {
                p_arrayList.Add(string.Concat( p_sAttribute , m_stringSeparator[0] , p_sValue));
            }
            
        }
        /// <summary>
        /// This function would iterate through the modified controls collection for R5 and store all the information in SysEx for handling at page level
        /// </summary>
        internal void StoreModifiedControls()
        {
            XmlElement objEle = null;
            XmlElement objParentEle = null;
            string[] singleRowContentSplit = null;
            
            
            try
            {
                objParentEle = (XmlElement)this.SysEx.SelectSingleNode("//ModifiedControlsList");
                if (objParentEle == null)
                {
                    CreateSysExData("ModifiedControlsList", "");
                    objParentEle = (XmlElement)this.SysEx.SelectSingleNode("//ModifiedControlsList");
                }
                foreach (ArrayList singleRow in m_ModifiedControls)
                {
                    objEle = this.SysEx.CreateElement("Control");
                    foreach (string singleRowContent in singleRow)
                    {
                        singleRowContentSplit = singleRowContent.Split(m_stringSeparator, StringSplitOptions.RemoveEmptyEntries);

                        if (singleRowContentSplit.Length < 2)
                        {
                            objEle.SetAttribute(singleRowContentSplit[0].ToString(), "");
                        }
                        else
                        {
                            objEle.SetAttribute(singleRowContentSplit[0].ToString(), singleRowContentSplit[1].ToString());
                        }
                    }
                    objParentEle.AppendChild(objEle);

                }
            }
            catch (Exception e)
            {
            }
        }
		internal void ClearSysExternalParam()
		{
			XmlElement objElt = this.SysEx.SelectSingleNode("//SysExternalParam") as XmlElement;
			if(objElt!=null)
				objElt.ParentNode.RemoveChild(objElt);
		}

		bool DataChanged
		{
			set{m_DataChanged = value;}
			get
			{
				bool b = false;
				try
				{
					if(m_DataChanged) 
						return true;
					//No Child Forms Eliminates some complexity here.

					//Give derived code a chance to intervene.
					GetDataChanged(ref b);
				}
				catch(Exception e)
				{
                    Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.DataChanged.Exception", ClientId), Common.BusinessAdaptorErrorType.Error);
				}
				return b;
			}
		}
		public virtual bool Refresh(){return Refresh(false);} 
		public virtual bool Refresh(bool viewOnly) 
		{return true;}

        //Geeta 07/25/08 : Mits 12151 
        public virtual bool MoveTo(params int[] paramValue){ return false; } 
		virtual public void UpdateObject(){;}
		
		virtual public void UpdateForm(){;}
		virtual public bool Validate(){return false;}

		#region SysEx Util Functions

		protected int GetSysExDataNodeInt(string sNode){return GetSysExDataNodeInt(sNode,false);}
		protected int GetSysExDataNodeInt(string sNode,bool bRequired)
		{
			return Conversion.ConvertStrToInteger(GetSysExDataNodeText(sNode,bRequired));
		}
		protected string GetSysExDataNodeText(string sNode){return GetSysExDataNodeText(sNode,false);}
		
		//Enhanced for use with an explicit xpath OR a simple node name.
		protected string GetSysExDataNodeText(string sNode,bool bRequired)
		{
			try
			{
                //Deb : Performance Changes
                if (sNode.IndexOf("/") >= 0)
                {
                    if (SysEx.SelectSingleNode(sNode) != null)
                        return SysEx.SelectSingleNode(sNode).InnerText;
					else if (bRequired)
                    return SysEx.GetElementsByTagName(sNode)[0].InnerText;
               		else if (SysEx.GetElementsByTagName(sNode)[0] != null)
                    return SysEx.GetElementsByTagName(sNode)[0].InnerText;
               
                    else
                    {
                        if (bRequired)
                            throw new Riskmaster.ExceptionTypes.SysExDataNotFoundException(String.Format(Globalization.GetString("FormBase.GetSysExDataNodeText", ClientId), sNode));
                        else
                            return "";
                    }
                }
                //Deb : Performance Changes
                else
                    return SysEx.GetElementsByTagName(sNode)[0].InnerText;
			}
			catch
			{
				if(bRequired)
                    throw new Riskmaster.ExceptionTypes.SysExDataNotFoundException(String.Format(Globalization.GetString("FormBase.GetSysExDataNodeText", ClientId), sNode));
				else
					return "";
			}
		}

        protected string GetFormVarNodeText(string sNode)
        {
            if (sNode.IndexOf("/") >= 0)
                return FormVariables.SelectSingleNode(sNode).InnerText;
            else
                return FormVariables.GetElementsByTagName(sNode)[0].InnerText;
        }

		protected void ResetSysExData(string sNode, string sValue)
		{
			try
            {
                //srajindersin MITS 32606 dt:05/14/2013
                if (this.SysEx.SelectSingleNode("/SysExData/" + sNode) != null)
                    SysEx.DocumentElement.RemoveChild(this.SysEx.SelectSingleNode("/SysExData/"+sNode));
            }
			catch{};
			CreateSysExData(sNode,sValue);
		}
		protected void CreateSysExData(string strNode){CreateSysExData(strNode, "");}

		/// <summary>
		/// Creates an empty XML Element for a specific XML node used by the SysExData
		/// in the Instance Document.  Typically used so that the screen will 
		/// still render in Orbeon
		/// </summary>
		/// <param name="strNode">string indicating the Xml Element to create</param>
		protected void CreateSysExData(string sNode, string sValue)
		{
			XmlNode objNew;
			//Determine if the specific Node already exists in the SysExData of the Instance Document
			if (SysEx.SelectSingleNode("/SysExData/" + sNode) == null)
			{	
				//Create a new XML Element for the specified Xml Node
				objNew = SysEx.CreateElement(sNode);

				//Set the element inner text to an empty string
				objNew.AppendChild(SysEx.CreateCDataSection(sValue));
				//objNew.InnerText = FormDataAdaptor.CData();

				//Add the new XML Element to the existing Instance Data
				SysEx.DocumentElement.AppendChild(objNew);
			}
		}
#endregion
		#region SysViewSection Util Functions
		internal void ResetSysSections()
		{
			//Clears out Sections...
			m_SysViewSection = new XmlDocument();
			m_SysViewSection.AppendChild(m_SysViewSection.CreateElement("SysViewSection"));
		}

		internal void RemoveSysSection(string sSectionName)
		{
			try{SysViewSection.DocumentElement.RemoveChild(this.SysViewSection.SelectSingleNode("/SysViewSection/section[@name='"+sSectionName+"']"));}
			catch{};
		}

		internal void CreateSysSection(string sSectionName){CreateSysSection(sSectionName,"");}
		internal void CreateSysSection(string sSectionName,string innerXml)
		{
			XmlNode objNew;
			XmlNode objAtt;

			//Determine if the specific Node already exists in the SysExData of the Instance Document
			if (this.SysViewSection.SelectSingleNode("/SysViewSection/section[@name='"+sSectionName+"']") == null)
			{	
				//Create a new XML Element for the specified Xml Node
				objNew = this.SysViewSection.CreateElement("section");
				objAtt = this.SysViewSection.CreateAttribute("name");
				objAtt.Value = sSectionName;
				objNew.Attributes.Append(objAtt as XmlAttribute);

				//Toss in the section contents.
				objNew.InnerXml = innerXml;

				//Add the new XML Element to the existing Instance Data
				this.SysViewSection.DocumentElement.AppendChild(objNew);
			}
		}

		internal void ResetSysSection(string sSectionName){ResetSysSection(sSectionName,"");}
		internal void ResetSysSection(string sSectionName,string innerXml)
		{
			RemoveSysSection(sSectionName);
			CreateSysSection(sSectionName,innerXml);
		}

#endregion 
		protected void SafeSetAttribute(XmlNode nd, string attName, string attValue)
		{
			XmlAttribute objAtt = nd.OwnerDocument.CreateAttribute(attName);
			objAtt.Value=attValue;
			try{nd.Attributes.RemoveNamedItem(attName);}catch{}
			nd.Attributes.Append(objAtt);
		}

		protected string ToLeadingCaps(string s)
		{
			return s.Substring(0,1).ToUpper() + s.Substring(1,s.Length-1).ToLower();
		}
		internal void LogSecurityError(int errorType)
		{
			string sPermission="";

			switch(errorType)
			{
				case RMO_ACCESS:
                    sPermission = Globalization.GetString("Permission.NoAccess", ClientId);
					break;
				case RMO_VIEW:
                    sPermission = Globalization.GetString("Permission.NoView", ClientId);
					break;
				case RMO_UPDATE:
                    sPermission = Globalization.GetString("Permission.NoUpdate", ClientId);
					break;
				case RMO_CREATE:
                    sPermission = Globalization.GetString("Permission.NoCreate", ClientId);
					break;
				case RMO_DELETE:
                    sPermission = Globalization.GetString("Permission.NoDelete", ClientId);
					break;
				case RMO_VIEW_BLANK_NO_CREATE:
                    sPermission = Globalization.GetString("Permission.ViewBlankNoCreate", ClientId);
					break;
			//		ElseIf lErrorType = RMO_EVENT + RMO_EVENT_CHANGE_DEPARTMENT Then
			//		sErrText = sErrText & "(No Permission to Change Department)."
			//		ElseIf lErrorType = m_SecurityId + RMO_CLAIM_CHANGE_DEPARTMENT Then
			//		sErrText = sErrText & "(No Permission to Change Department)."
			//		ElseIf lErrorType = m_SecurityId + RMO_POLICY_MCO_CHANGE Then
			//		sErrText = sErrText & "(No Permission to Change POLICY MCO)."
			//		ElseIf lErrorType = m_SecurityId + RMO_CLAIM_ALLOW_CLOSE Then
			//		sErrText = sErrText & "(No Permission to Close Claim)."
			//		ElseIf lErrorType = m_SecurityId + RMO_CLAIM_ALLOW_REOPEN Then
			//		sErrText = sErrText & "(No Permission to Re-Open Claim)."
			//		End If
			
			}
            //Changed Rakhi for MITS 12804:To display only the Relevant Error Message
            //Errors.Add(Globalization.GetString("PermissionFailure"),
            //    string.Format(Globalization.GetString("Permission.FullPermissionMsg"),sPermission),
            //    BusinessAdaptorErrorType.Error);
            Errors.Add(Globalization.GetString("PermissionFailure", ClientId), sPermission, BusinessAdaptorErrorType.Error);
        }
            //Changed Rakhi for MITS 12804:To display only the Relevant Error Message
        
        /// <summary>
        /// This function will copy data from all child nodes of a specified parent node from an xml to another
        /// </summary>
        /// <param name="copyto">target xml</param>
        /// <param name="copyfrom">source xml</param>
        /// <param name="parentname">parent node</param>
        internal void CopyNodes(ref XmlDocument copyto, XmlDocument copyfrom, string parentname)
        {
            //Copying over data updated by user..
            XmlNodeList objCurNodLst = copyfrom.SelectSingleNode("//" + parentname).SelectNodes("*");
            string sCurrentNodeValue;
            string sCurrentXPath;
            XmlNode objSysExNode;

            foreach (XmlNode objNode in objCurNodLst)
            {
                sCurrentNodeValue = objNode.InnerText;
                sCurrentXPath = string.Concat( "//" , parentname , "/" , objNode.Name);
                objSysExNode = copyto.SelectSingleNode(sCurrentXPath);
                if (objSysExNode != null)
                {
                    objSysExNode.InnerText = sCurrentNodeValue;

                    //Copy over all attributes too
                    foreach (XmlAttribute oAttr in objNode.Attributes)
                    {
                        if (objSysExNode.Attributes[oAttr.Name] != null)
                        {
                            objSysExNode.Attributes[oAttr.Name].Value = oAttr.Value;
                        }
                    }
                }

               

            }
        }
	}
}
