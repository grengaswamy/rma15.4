﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Subrogation Screen.
	/// </summary>
	public class SubrogationForm : DataEntryFormBase
	{
		const string CLASS_NAME = "ClaimXSubrogation";
		const string FILTER_KEY_NAME = "ClaimId";

		private ClaimXSubrogation objClaimXSubrogation{get{return objData as ClaimXSubrogation;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}

        private string m_Caption = string.Empty;
            //nnorouzi; MITS 19478
        public override string GetCaption()
        {
            return base.GetCaption();
        }
	
		public SubrogationForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		public override void InitNew()
		{
			base.InitNew();

			//set filter for ClaimId
			objClaimXSubrogation.ClaimId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			if (objClaimXSubrogation.ClaimId>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + objClaimXSubrogation.ClaimId;

		}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
            ArrayList singleRow = null;
            int iUseClaimProgressNotes = 0;
			base.OnUpdateForm ();

            //MGaba2:MITS 16498:Start
            //Module Security Permission was not working as security id was different each time defendant 
            //is opened from different claim.

            Claim objClaim = (Claim)objClaimXSubrogation.Context.Factory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(objClaimXSubrogation.ClaimId);

            switch (objClaim.LineOfBusCode)
            {
                case 241:
                    m_SecurityId = RMO_GC_SUBROGATION;
                    break;
                case 242:
                    m_SecurityId = RMO_VA_SUBROGATION;
                    break;
                case 243:
                    m_SecurityId = RMO_WC_SUBROGATION;
                    break;
                case 844:
                    m_SecurityId = RMO_DI_SUBROGATION;
                    break;
                //Start-Mridul Bansal. 01/04/10. MITS#19314. Support for Property Claims.
                case 845:
                    m_SecurityId = RMO_PC_SUBROGATION;
                    break;
                //End-Mridul Bansal. 01/04/10. MITS#19314. Support for Property Claims.
            }

            //Setting Security Id for further permissions like attachment
            XmlElement objSysSid = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSid");
            objSysSid.InnerText = m_SecurityId.ToString();

            //Create new Permission wasnt working
            if (objClaimXSubrogation.SubrogationRowId == 0)
            {
                if (!objClaimXSubrogation.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            else
            {//View Permission wasnt working
                if (!objClaimXSubrogation.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_VIEW);
                    return;
                }
            }

            //MGaba2:MITS 16498:End

            //Start by Shivendu for MITS 9467
            if (SysEx.DocumentElement.SelectSingleNode("LINEOFBUSCODE") == null)
            {
                CreateSysExData("LINEOFBUSCODE");
                base.ResetSysExData("LINEOFBUSCODE", (objClaimXSubrogation.Parent as Claim).LineOfBusCode.ToString());
            }
            //End by Shivendu for MITS 9467

            //mgaba2:mits 29116
            iUseClaimProgressNotes = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ClmProgressNotesFlag;
            if (iUseClaimProgressNotes == 0)
            {
                base.AddKillNode("enhancednotes");
            }

            //nnorouzi; MITS 19478			
            m_Caption = this.GetCaption();

            //Pass this subtitle value to view (ref'ed from @valuepath).
            base.ResetSysExData("SubTitle", m_Caption);
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", m_Caption);
            base.m_ModifiedControls.Add(singleRow);



			//Check for People Maintenance permission
			PeoplePermissionChecks4Other("subrogation", m_SecurityId+RMO_UPDATE);

			//JAP - LSS changes - lock down form as read-only if LSS interface created the record
			if (objClaimXSubrogation.AddedByUser == "LSSINF")
			{
                //gagnihotri MITS 16453 05/12/2009
                //XmlElement xmlFormElement = (XmlElement)base.SysView.SelectSingleNode("//form");
                //if (xmlFormElement != null) xmlFormElement.SetAttribute("readonly", "1");
                base.ResetSysExData("FormReadOnly", "Disable");
            }
            else
                base.ResetSysExData("FormReadOnly", "Enable");


            //MITS 17107
            int iAttFlag = 0;
            iAttFlag = Conversion.ConvertObjToInt(objClaimXSubrogation.Context.DbConn.ExecuteInt(
                "SELECT ATTACHMENTS_FLAG FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'CLAIM_X_SUBROGATION'"), base.ClientId);

            if (SysEx.DocumentElement.SelectSingleNode("AttachDocFlag") == null) CreateSysExData("AttachDocFlag");                
            
            if (iAttFlag == 1) base.ResetSysExData("AttachDocFlag", "true");
            else base.ResetSysExData("AttachDocFlag", "false");
		}

		//validate data according to the Business Rules
		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;
			
			// Perform data validation
			string sToday = Conversion.ToDbDate(System.DateTime.Now);
			Claim objClaim = (Claim) this.objClaimXSubrogation.Context.Factory.GetDataModelObject("Claim",false);    
			objClaim.MoveTo(objClaimXSubrogation.ClaimId);

			//DATA Validation goes here...

			// Return true if there were validation errors
			Cancel = bError;
		}

		public override void BeforeApplyLookupData(ref bool Cancel, ArrayList arrToSaveFields)
		{
			arrToSaveFields.Add("EntityTableId");
		}
	}
}
