using System;
using System.Xml;
using Riskmaster.Application.FundManagement;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Security;
using Riskmaster.Settings;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Deposit Screen.
	/// </summary>
	public class DepositForm : DataEntryFormBase
	{
		const string CLASS_NAME = "FundsDeposit";
		const string FILTER_KEY_NAME = "AccountId"; //TODO: Allow for sub-account filter key.
        public const string CTL_BANK_ACCOUNT = "subbankaccount";

		private FundsDeposit FundsDeposit{get{return objData as FundsDeposit;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		
		/// <summary>
		/// Class constructor which initializes the Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public DepositForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();

			//FundsDeposit.BankAccId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,true);

			//By Nikhil: Deposit can  open from the left menu as well as from Bank Accounts Screen
			//			 Hence Accounts Id might or might not be present.
			//Dated: 28-Oct-2005
			FundsDeposit.BankAccId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME);

			//set filter for BankAccId
			if (FundsDeposit.BankAccId>0)
				(objData as INavigation).Filter = objData.PropertyNameToField("BankAccId") + "=" + FundsDeposit.BankAccId;
			
		}//end method InitNew()

        public override void OnUpdateObject()
        {
            
        }

		/// <summary>
		/// Overrides the base class OnUpdateForm method to handle a Bank Account List in the 
		/// instance data for ref binding.  
		/// </summary>
		/// <remarks>This instance data is specifically held in the SysExData section
		/// of the Instance document.
		/// </remarks>
		public override void OnUpdateForm()
		{
            //Variable declarations
            XmlCDataSection objCData = null;
			XmlDocument objXML = base.SysEx;
			XmlNode objOld=null;
			XmlElement objNew=null;

            XmlNode objOldDepositType = null;
            XmlElement objNewDepositType = null;

			//Call the base implementation of OnUpdateForm from the FormBase class
			base.OnUpdateForm();

            //Create Deposit Type List
            AppendDepositTypeList();

			//Create the BankAccountList necessary to populate the DropDownList on the FDM XML form
			AppendBankAccountList();
			
			// Add the SubBankAccount Flag.
			objOld = objXML.SelectSingleNode("//SubAccounts");
			objNew = objXML.CreateElement("SubAccounts");
			
			objNew.InnerText = FundsDeposit.Context.InternalSettings.SysSettings.UseFundsSubAcc.ToString().ToLower() ;					

			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);

			// Add the Label of drop down.
            //string sTitle = string.Empty ;
            //if (!FundsDeposit.Context.InternalSettings.SysSettings.UseFundsSubAcc)
            //{
            //    //Because bank account/sub account binding to the same xml element
            //    sTitle = "Bank Account";
            //    FundsDeposit oFD = (FundsDeposit)objData;
            //    oFD.SubAccId = oFD.BankAccId;
            //}
            //else
            //    sTitle = "Sub Bank Account";

            ////Mona
            //ArrayList singleRow = new ArrayList();
            //base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.ComboBox.ToString());
            //base.AddElementToList(ref singleRow, "id", "subbankaccount");
            //base.AddElementToList(ref singleRow, "Text", sTitle);
            //base.m_ModifiedControls.Add(singleRow);
            


            //objOld = objXML.SelectSingleNode("//ControlAppendAttributeList");
            //objNew = objXML.CreateElement("ControlAppendAttributeList");
            //XmlElement objElem = null;
            //XmlElement objChild = null;
		
            //objElem = objXML.CreateElement("subbankaccount");
            //objChild = objXML.CreateElement("title");

            //objChild.SetAttribute( "value", sTitle );
			
            //objElem.AppendChild(objChild);;
            //objNew.AppendChild(objElem);

            //if(objOld !=null)
            //    objXML.DocumentElement.ReplaceChild(objNew,objOld);
            //else
            //    objXML.DocumentElement.AppendChild(objNew);
			
			
			//By Nikhil: Kill Back Button when page is opened from Left menu link
			//Dated: 28-Oct-2005
            //Changed for MITS 15155 : start
            //if (base.SysEx.SelectSingleNode("/SysExData/" + FILTER_KEY_NAME) == null)

            if (base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME) == 0)
                base.AddKillNode("btnBack");                
                    

			//if( FundsDeposit.TransDate == "" )
			//	FundsDeposit.TransDate = Conversion.ToDbDate(System.DateTime.Now);	
            //Changed for MITS 15155 : end
		 
			if( FundsDeposit.VoidclearDate == "" )
				FundsDeposit.VoidclearDate = Conversion.ToDbDate(System.DateTime.Now);



            objOld = objXML.SelectSingleNode("//ControlAppendAttributeList");
            objNew = objXML.CreateElement("ControlAppendAttributeList");

            // Change the Ref of Bank Account to Point to Sub Bank Account when UseSubBankAccount Setting is true
            if (FundsDeposit.Context.InternalSettings.SysSettings.UseFundsSubAcc)
            {
                this.AppendRuntimeAttribute(objNew, DepositForm.CTL_BANK_ACCOUNT, "ref", "/Instance/FundsDeposit/SubAccId");
                //this.AppendRuntimeAttribute(objNew, DepositForm.CTL_BANK_ACCOUNT, "title", "Sub Bank Account");
            }
            else
            {
                this.AppendRuntimeAttribute(objNew, DepositForm.CTL_BANK_ACCOUNT, "ref", "/Instance/FundsDeposit/BankAccId");
               // this.AppendRuntimeAttribute(objNew, DepositForm.CTL_BANK_ACCOUNT, "title", "Bank Account");
                FundsDeposit oFD = (FundsDeposit)objData;
                oFD.SubAccId = oFD.BankAccId;
            }

            

            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);




		}//end method OnUpdateForm()



        private void AppendRuntimeAttribute(XmlElement p_objParentNode, string p_sControlName, string p_sAttributeName, string p_sValue)
        {
            XmlElement objControlNode = null;
            XmlElement objAttributeNode = null;

            objControlNode = p_objParentNode.OwnerDocument.CreateElement(p_sControlName);
            objAttributeNode = p_objParentNode.OwnerDocument.CreateElement(p_sAttributeName);
            objAttributeNode.SetAttribute("value", p_sValue);
            objControlNode.AppendChild(objAttributeNode);
            p_objParentNode.AppendChild(objControlNode);
        }





		public override void BeforeSave(ref bool Cancel)
		{
			base.BeforeSave (ref Cancel);
			if (base.SysEx.SelectSingleNode("/SysExData/" + FILTER_KEY_NAME)==null)
			{
				if( FundsDeposit.Context.InternalSettings.SysSettings.UseFundsSubAcc )
				{
					// DropDown represents SubBankAccountId , and Account Id is Zero.
					BankAccSub objBankAccSub = ( BankAccSub ) FundsDeposit.Context.Factory.GetDataModelObject( "BankAccSub" , false );
					objBankAccSub.MoveTo( FundsDeposit.SubAccId );
					FundsDeposit.BankAccId = objBankAccSub.AccountId  ; 					
				}
				else
				{
					// DropDown represents BankAccountId , and Account Id is Zero.
					FundsDeposit.BankAccId = FundsDeposit.SubAccId ;
					FundsDeposit.SubAccId = 0 ;
				}
			}	
			else
			{
				if( !FundsDeposit.Context.InternalSettings.SysSettings.UseFundsSubAcc )
				{
					// DropDown represents BankAccountId , and Account Id has some valid value.
                    //Changed for MITS 15155 : start
                    //if(FundsDeposit.BankAccId == 0)
                    //    FundsDeposit.BankAccId = FundsDeposit.SubAccId;

					//FundsDeposit.SubAccId = 0 ;
                    //Changed for MITS 15155 : end
				}
                else //Added for MITS 15970
                {
                    BankAccSub objBankAccSub = (BankAccSub)FundsDeposit.Context.Factory.GetDataModelObject("BankAccSub", false);
                    objBankAccSub.MoveTo(FundsDeposit.SubAccId);
                    FundsDeposit.BankAccId = objBankAccSub.AccountId;
                } //Added for MITS 15970
			}
		}

		/// <summary>
		/// Adds the necessary Bank Account List information to SysExData
		/// </summary>
		/// <remarks>The Bank Account List information is created in a manner
		/// that can be used to populate DropDownLists.  The XPath to the SysExData
		/// will be used for binding to the itemset ref tag in the FDM XML Document
		/// </remarks>
		private void AppendBankAccountList()
		{
			//Variable declarations
			ArrayList arrAcctList = new ArrayList();
			XmlDocument objXML = base.SysEx;
			XmlNode objOld=null;
			XmlElement objNew=null;
			XmlElement objElem=null;
			XmlCDataSection objCData=null;

			//Retrieve the security credential information to pass to the FundManager object
			string sDSNName = base.m_fda.userLogin.objRiskmasterDatabase.DataSourceName;
			string sUserName = base.m_fda.userLogin.LoginName;
			string sPassword = base.m_fda.userLogin.Password;
			int iUserId = base.m_fda.userLogin.UserId;
			int iGroupId = base.m_fda.userLogin.GroupId;

            using (FundManager objFundMgr = new FundManager(sDSNName, sUserName, sPassword, iUserId, iGroupId, base.ClientId))
            {
                if (base.SysEx.SelectSingleNode("/SysExData/" + FILTER_KEY_NAME) == null)
                    arrAcctList = objFundMgr.GetAccounts(FundsDeposit.Context.InternalSettings.SysSettings.UseFundsSubAcc);
                else
                    //Changed for MITS 15155 : start
                    //arrAcctList = objFundMgr.GetAccounts(FundsDeposit.Context.InternalSettings.SysSettings.UseFundsSubAcc, FundsDeposit.BankAccId);
                    arrAcctList = objFundMgr.GetAccounts(FundsDeposit.Context.InternalSettings.SysSettings.UseFundsSubAcc, base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME) );
                    //Changed for MITS 15155 : end
            }

			//Create the necessary SysExData to be used in ref binding
			objOld = objXML.SelectSingleNode("//AccountList");
			objNew = objXML.CreateElement("AccountList");

            // Start Naresh MITS 10009 Blank Value Added in Combo
            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;

            // Blank value for the combo box
            xmlOption = objXML.CreateElement("option");
            xmlOptionAttrib = objXML.CreateAttribute("value");
            xmlOption.Attributes.Append(xmlOptionAttrib);
            objNew.AppendChild(xmlOption);
            xmlOptionAttrib = null;
            xmlOption = null;
            // End Naresh Blank Value Added in Combo

			//Loop through and create all the option values for the combobox control
			foreach (FundManager.AccountDetail item in arrAcctList)
			{
				xmlOption = objXML.CreateElement("option");
				//Create a CData section to handle any possible strange characters
				//in the Account Name that may cause problems with the output XML
				objCData = objXML.CreateCDataSection(item.AccountName);
				xmlOption.AppendChild(objCData);
				 xmlOptionAttrib = objXML.CreateAttribute("value");
				if( FundsDeposit.Context.InternalSettings.SysSettings.UseFundsSubAcc )
					xmlOptionAttrib.Value = item.SubRowId.ToString(); 
				else
					xmlOptionAttrib.Value = item.AccountId.ToString();
				xmlOption.Attributes.Append(xmlOptionAttrib);
				objNew.AppendChild(xmlOption);
			}//end foreach
			if(arrAcctList.Count==0)
			{
				objElem=objXML.CreateElement("option");
				objElem.SetAttribute("value","0");
				objElem.InnerText=string.Empty;
				objNew.AppendChild(objElem);
			}
			//Add the XML to the SysExData 
			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);

			//Clean up
			arrAcctList = null;
			objOld = null;
			objNew = null;
			objCData = null;

		}//end method AppendBankAccountList()
        private void AppendDepositTypeList()
        {
            //Variable declarations
            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;
            XmlDocument objXML = base.SysEx;
            XmlNode objOld = null;
            XmlElement objNew = null;
            XmlCDataSection objCData = null;

            bool bUseSubAccounts = FundsDeposit.Context.InternalSettings.SysSettings.UseFundsSubAcc;

            //Create the necessary SysExData to be used in ref binding
            objOld = objXML.SelectSingleNode("//deposittype");
            objNew = objXML.CreateElement("deposittype");

            xmlOption = objXML.CreateElement("option");
            objCData = objXML.CreateCDataSection("Deposit");
            xmlOption.AppendChild(objCData);
            xmlOptionAttrib = objXML.CreateAttribute("value");
            xmlOptionAttrib.Value = "0";
            xmlOption.Attributes.Append(xmlOptionAttrib);
            objNew.AppendChild(xmlOption);

            xmlOption = objXML.CreateElement("option");
            objCData = objXML.CreateCDataSection("Adjustment");
            xmlOption.AppendChild(objCData);
            xmlOptionAttrib = objXML.CreateAttribute("value");
            xmlOptionAttrib.Value = "1";
            xmlOption.Attributes.Append(xmlOptionAttrib);
            objNew.AppendChild(xmlOption);

            if (bUseSubAccounts)
            {
                xmlOption = objXML.CreateElement("option");
                objCData = objXML.CreateCDataSection("Money Market Deposit");
                xmlOption.AppendChild(objCData);
                xmlOptionAttrib = objXML.CreateAttribute("value");
                xmlOptionAttrib.Value = "2";
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);

                xmlOption = objXML.CreateElement("option");
                objCData = objXML.CreateCDataSection("Money Market Transfer");
                xmlOption.AppendChild(objCData);
                xmlOptionAttrib = objXML.CreateAttribute("value");
                xmlOptionAttrib.Value = "3";
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);

                xmlOption = objXML.CreateElement("option");
                objCData = objXML.CreateCDataSection("Money Market Adjustment");
                xmlOption.AppendChild(objCData);
                xmlOptionAttrib = objXML.CreateAttribute("value");
                xmlOptionAttrib.Value = "4";
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
            }
            
            //Add the XML to the SysExData 
            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);

            //Clean up 
             objOld = null;
            objNew = null;
            objCData = null;

        }//end method AppendDepositTypeList()
		
	}
}