﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 02/11/2014 | 34276  | anavinkumars   | Add Contact Type field in Contacts info tab
 * 02/10/2014 | 34276  | abhadouria   | changes req for Entity Address Type
 * 02/11/2014 | 34276  | anavinkumars   | Enhancement to Entity related functionality in RMA 
 * 02/27/2015 | RMA-6865        | nshah28      | Swiss Re - Ability to set Effective Date_or_Expiration Date on Address
 **********************************************************************************************/
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Application.SupportScreens;
using Riskmaster.Settings;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// People Screen.
	/// </summary>
	public class PeopleForm : DataEntryFormBase
	{
		const string CLASS_NAME = "Entity";
		
		private Entity Entity{get{return objData as Entity;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		//To track patientid
		private bool m_IsNew=false;
		// TODO - Remove this hardcoding, if possible
		const string INSTANCE_SYSEXDATA_PATH = "/Instance/UI/FormVariables/SysExData";
        //Added Rakhi for R7:Add Emp Data Elements
        string sOfficePhone = string.Empty;
        string sHomePhone = string.Empty;
        int iHomeType = 0;
        int iOfficeType = 0;
        //Added Rakhi for R7:Add Emp Data Elements

		public PeopleForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;

		}//end class constructor
		public override void InitNew()
		{
			base.InitNew();
			int tableId=base.GetSysExDataNodeInt("EntityType");
			if(tableId !=0)
				Entity.EntityTableId = tableId;
            //mbahl3  mits:29316
            int iIDType = base.GetSysExDataNodeInt("IDType");
            if (iIDType != 0)
                Entity.IDType = iIDType;
            //mbahl3 mits:29316

            //ToDo:Need to see if this is needed. coz' from the UI it is not possible to change Entity Type
			//once user has moved to some record
			//Entity.LockEntityTableChange = (tableId!=0);
			Entity.HideDeletedRecords = true;
            if (tableId == 0)
            {
                if (!(Entity.Context.InternalSettings.SysSettings.UseEntityRole)) //RMA-Ash
                    (Entity as INavigation).Filter = " ENTITY_TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE GLOSSARY_TYPE_CODE=7)";
                else
                    (Entity as INavigation).Filter = " NAME_TYPE =  " + "SELECT CODE_ID FROM CODES WHERE SHORT_CODE = 'IND' AND TABLE_ID = (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'ENTITY_NAME_TYPE')";
            }
            else
                (Entity as INavigation).Filter = " ENTITY_TABLE_ID=" + Convert.ToString(Entity.EntityTableId); //RMA-Ash
			//(Entity as INavigation).Filter = " ENTITY_TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE GLOSSARY_TYPE_CODE=7)"; 

		}
		public override void BeforeSave(ref bool Cancel)
		{
			m_IsNew=Entity.IsNew;
            if (IsDuplicateSSN())
            { //MGaba2:MITS 9052      
                Cancel = true;
                XmlElement objSysSkipBindToControl = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSkipBindToControl");
                objSysSkipBindToControl.InnerText = "true";
                return;
            }
            //Debabrata Biswas Entity Payment Approval(make the people entity approval status as pending approval) MITS 20606 04/02/2010
            if (Entity.Context.InternalSettings.SysSettings.UseEntityApproval)
            {
                if (Entity.EntityApprovalStatusCode == 0)
                {
                    Entity.EntityApprovalStatusCode = Entity.Context.InternalSettings.CacheFunctions.GetCodeIDWithShort("P", "ENTITY_APPRV_REJ");
                }
            }
            //End Debabrata Biswas MITS 20606 04/02/2010
		}
        private bool IsDuplicateSSN()
        {
            //MGaba2:MITS 9052
            string sSQL = "";
            
            if (base.SysEx.SelectSingleNode("//DupSSN").InnerText.Trim() == "AfterIgnore")
                return false;
            DbConnection objDbConnection = null;

            if ( Entity.EntityId.ToString() != "0")
            {
                sSQL = "SELECT TAX_ID FROM ENTITY WHERE ENTITY_ID = " + Entity.EntityId.ToString();
                objDbConnection = DbFactory.GetDbConnection(Entity.Context.DbConn.ConnectionString);
                if (objDbConnection != null)
                    objDbConnection.Open();
                string sTaxId = Conversion.ConvertObjToStr(objDbConnection.ExecuteScalar(sSQL));
                if (sTaxId == Entity.TaxId)
                    return false;
                objDbConnection.Close();
            }

            //sSQL = "SELECT TAX_ID FROM ENTITY WHERE TAX_ID = " + Utilities.FormatSqlFieldValue(Entity.TaxId.Trim()) + " AND ENTITY_ID <> " + Entity.EntityId.ToString();
            sSQL = "SELECT TAX_ID FROM ENTITY WHERE TAX_ID = " + Utilities.FormatSqlFieldValue(Entity.TaxId.Trim()) + " AND ENTITY_ID <> " + Entity.EntityId.ToString() + " AND DELETED_FLAG = 0 ";
            objDbConnection = DbFactory.GetDbConnection(Entity.Context.DbConn.ConnectionString);
            if (objDbConnection != null)
                objDbConnection.Open();
            Object objDuplicate = objDbConnection.ExecuteScalar(sSQL);
            objDbConnection.Close();
            if (objDuplicate != null)
            {               
                return true;
            }
            return false;
        }

		public override void AfterSave()
		{
			if (Entity.EntityTableId==1061)
			{
				HipaaLog obj=new HipaaLog(this.Entity.Context.DbConn.ConnectionString, base.ClientId);
				string sFormName="";
				sFormName="People Maintenance";
				if (m_IsNew==false)
				{
					obj.LogHippaInfo(this.Entity.EntityId,"0","0",sFormName,"UD",this.Entity.Context.RMUser.LoginName);
				}
			}
            //MGaba2:MITS 9052
            base.ResetSysExData("DupSSN", "");           
            //OFAC Check for the entries created from people maintainence screen
            //START
            if ((Entity.Context.InternalSettings.SysSettings.DoOfacCheck) && (Entity.OverrideOfacCheck == false) && (Entity.DttmRcdAdded == Entity.DttmRcdLastUpd))
            {
                Riskmaster.Application.PatriotProtectorWrapper.PatriotProtectorWrapper obj = new Riskmaster.Application.PatriotProtectorWrapper.PatriotProtectorWrapper(Entity.Context.RMDatabase.DataSourceName, Entity.Context.RMUser.LoginName, Entity.Context.RMUser.Password, base.ClientId);

                obj.AsynchPatriotProtectorCall(Entity.FirstName, Entity.LastName, Entity.Table, Entity.EntityId, Entity, -1, -1, -1,true,null);
            }
			//END	
		}
		public override void BeforeDelete(ref bool Cancel)
		{
			if (Entity.EntityTableId==1061)
			{
				HipaaLog obj=new HipaaLog(this.Entity.Context.DbConn.ConnectionString, base.ClientId);
				string sFormName="People Maintenance";
				obj.LogHippaInfo(this.Entity.EntityId,"0","0",sFormName,"DL",this.Entity.Context.RMUser.LoginName);
			}

		}
		public override void OnUpdateForm()
		{

            XmlDocument objXmlDoc = null;
            base.OnUpdateForm();
            //mbahl3 mits:29316
            base.CreateSysExData("IDType", "");
            base.CreateSysExData("SSNCodeId", Entity.Context.LocalCache.GetCodeId("S", "TAX_ID_TYPES").ToString());
            //mbahl3 mits:29316
            
            //mbahl3 mits 30224
            string sPresentdate = String.Empty;
            if (Entity.EntityId <= 0)
            {
                if (Entity.EffectiveDate == String.Empty)
                {
                    sPresentdate = DateTime.Now.ToShortDateString();
                    Entity.EffectiveDate = sPresentdate;
                }
            }
            //mbahl3 mits 30224

			if (Entity.EntityTableId ==1061)
			{
				if (Entity.EntityId !=0)
				{
					HipaaLog obj=new HipaaLog(this.Entity.Context.DbConn.ConnectionString, base.ClientId);
					string sFormName="";
					sFormName="People Maintenance";
					if (m_IsNew)
					{
						obj.LogHippaInfo(this.Entity.EntityId,"0","0",sFormName,"NW",this.Entity.Context.RMUser.LoginName);
					}
					else if (base.m_fda.SafeFormVariableParamText("SysCmd")!="6" && base.m_fda.SafeFormVariableParamText("SysCmd")!="5" && base.m_fda.SafeFormVariableParamText("SysCmd")!="7" && base.m_fda.SafeFormVariableParamText("SysCmd")!="8" && base.m_fda.SafeFormVariableParamText("SysCmd")!="")
					{
						obj.LogHippaInfo(this.Entity.EntityId,"0","0",sFormName,"VW",this.Entity.Context.RMUser.LoginName);
					}
				}
			}
			//Handle Locked down Toolbar buttons.
			if(!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_SEARCH,FormBase.RMO_ENTITY_SEARCH))
				base.AddKillNode("search");
			if(!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_LOOKUP,FormBase.RMO_ENTITY_SEARCH))
				base.AddKillNode("lookup");

            // Shruti 16-Mar-2007 Mits 9052
            //MITS 9052:Start          
            if (base.SysEx.SelectSingleNode("//DupSSN") == null) //MGaba2:MITS 9052      
                base.ResetSysExData("DupSSN", "");
            //MITS 9052:End
            // akaushik5 Changed for MITS 37242 Starts
            //if(!SysSettings.IsBRSInstalled(base.ClientId))
            if (!this.Entity.Context.InternalSettings.SysSettings.IsBRSInstalled())
                // akaushik5 Changed for MITS 37242 Ends
				base.AddKillNode("btnProviderContracts");
            
            //Start by Shivendu for MITS 9720
            if (Entity.EntityId == 0)
                base.ResetSysExData("ContractCount", "0");
            else
			    base.ResetSysExData("ContractCount",Entity.Context.DbConnLookup.ExecuteInt("SELECT COUNT(*) FROM PROVIDER_CONTRACTS WHERE PROVIDER_EID=" + Entity.EntityId).ToString());
            //End by Shivendu for MITS 9720

			//avipinsrivas start : Worked for Jira-340
            if (Entity.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                this.AddKillNode("entitytableid");
                this.AddKillNode("entitytableidtext");
                this.AddKillNode("entitytableidtextname");
                this.AddDisplayNode("entityrole");
            }
            else
            {
                this.AddKillNode("entityrole");
                if (Entity.EntityId != 0)
                {
                    this.AddKillNode("entitytableid");
                    this.AddDisplayNode("entitytableidtext");
                    this.AddDisplayNode("entitytableidtextname");
                    //ArrayList singleRow = new ArrayList();
                    //base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.ToggleControls.ToString());
                    //base.AddElementToList(ref singleRow, "id", "entitytableidtextname");
                    //base.AddElementToList(ref singleRow, "Text", "true");
                    //base.m_ModifiedControls.Add(singleRow);
                }
                else
                {
                    this.AddKillNode("entitytableidtext");
                    this.AddKillNode("entitytableidtextname");
                    this.AddDisplayNode("entitytableid");
                }
            }
            //avipinsrivas End

            //Debabrata Biswas (only execute this block if the Use Entity approval check box is "off")MITS# 20606 04/02/2010
            if (!Entity.Context.InternalSettings.SysSettings.UseEntityApproval)
            {
                base.AddKillNode("entityapprovalstatuscode");
                base.AddKillNode("rejectreasontext");
            }
            //Debabrata Biswas MITS 20606 date 04/02/2010
			ApplyFormTitle();
			PopulateEntityTypes();
            PopulateIDTypes(); //mbahl3 mits:29316
			//For Example Only
			RemoveViewNodesByEntityType();
			if(Entity.EntityTableId == Entity.Context.LocalCache.GetTableId("REHAB_JOB_PROVIDER") || Entity.EntityTableId == Entity.Context.LocalCache.GetTableId("ATTORNEYS") || Entity.EntityTableId == Entity.Context.LocalCache.GetTableId("REHAB_PROVIDER"))
			{
				PopulateAttributeList(true);
                base.AddDisplayNode("parenteid");
			}
			else
			{
				base.AddKillNode("parenteid");
				PopulateAttributeList(false);		
			}
            //Added Rakhi for R7:Add Emp Data Elements
            XmlNode objPhoneTypeList = base.SysEx.SelectSingleNode("//PhoneTypeList");
            XmlNode objPhoneNumbers = base.SysEx.SelectSingleNode("//PhoneNumbers");

            objXmlDoc = new XmlDocument();
            objXmlDoc = CommonForm.PopulatePhoneTypes(Entity.Context.DbConn.ConnectionString, ref iOfficeType, ref iHomeType,base.Adaptor.userLogin.objUser.NlsCode); //Aman ML Change
            if (objPhoneTypeList != null)
                base.SysEx.DocumentElement.RemoveChild(objPhoneTypeList);
            base.SysEx.DocumentElement.AppendChild(base.SysEx.ImportNode(objXmlDoc.SelectSingleNode("PhoneTypeList"), true));

            if (objPhoneNumbers == null)
            {
                string sPhoneNumbers = PopulatePhoneNumbers();
                base.ResetSysExData("PhoneNumbers", sPhoneNumbers);
            }
            else
            {
                objPhoneTypeList = base.SysEx.SelectSingleNode("//PhoneTypeList");
                if(objPhoneTypeList!=null)
                    CommonForm.SetPhoneNumbers(objPhoneNumbers, objPhoneTypeList);

            }
            if (base.SysEx.SelectSingleNode("//PhoneType1") == null)
            {
                base.ResetSysExData("PhoneType1", iOfficeType.ToString());
            }
            if (base.SysEx.SelectSingleNode("//PhoneNumber1") == null)
            {
                base.ResetSysExData("PhoneNumber1", sOfficePhone);
            }
            if (base.SysEx.SelectSingleNode("//PhoneType2") == null)
            {
                base.ResetSysExData("PhoneType2", iHomeType.ToString());
            }
            if (base.SysEx.SelectSingleNode("//PhoneNumber2") == null)
            {
                base.ResetSysExData("PhoneNumber2", sHomePhone);
            }
            //Added Rakhi for R7:Add Emp Data Elements
			//Anurag - Added for missing tabs contact info & Operating as on entity maintainenance screen
			// Operating As Grid Data
			XmlDocument objEntityXOperatingAsXmlDoc = GetEntityXOperatingAsData();

			XmlNode objOldEntityXOperatingAsNode = SysEx.DocumentElement.SelectSingleNode(objEntityXOperatingAsXmlDoc.DocumentElement.LocalName);
			if(objOldEntityXOperatingAsNode!=null)
			{
				SysEx.DocumentElement.RemoveChild(objOldEntityXOperatingAsNode);  
			}
			XmlNode objEntityXOperatingAsNode = 
				SysEx.ImportNode(objEntityXOperatingAsXmlDoc.DocumentElement ,true);
			SysEx.DocumentElement.AppendChild(objEntityXOperatingAsNode);  	

			if(SysEx.DocumentElement.SelectSingleNode("IsPostBack") == null)
				CreateSysExData("IsPostBack");
            //Parijat:Similar to Mits 9938 required in 9936
            if (SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag") != null && SysEx.DocumentElement.SelectSingleNode("EntityXContactInfoGrid_RowDeletedFlag") != null)
            {
                if ((SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag").InnerText == "true") || (SysEx.DocumentElement.SelectSingleNode("EntityXContactInfoGrid_RowDeletedFlag").InnerText == "true"))
                { 
                    if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7")
                    {
                        base.ResetSysExData("IsPostBack", "1");
                    }
                }
            }


			base.ResetSysExData("EntityXOperatingAsSelectedId",""); 
			base.ResetSysExData("EntityXOperatingAsGrid_RowDeletedFlag","false"); 
			base.ResetSysExData("EntityXOperatingAsGrid_RowAddedFlag", "false");

			//Contact Info Grid Data
			XmlDocument objEntityXContactInfoXmlDoc = GetEntityXContactInfoData();
			
			XmlNode objOldEntityXContactInfoNode = SysEx.DocumentElement.SelectSingleNode(objEntityXContactInfoXmlDoc.DocumentElement.LocalName);
			if(objOldEntityXContactInfoNode!=null)
			{
				SysEx.DocumentElement.RemoveChild(objOldEntityXContactInfoNode);  
			}
			XmlNode objEntityXContactInfoNode = 
				SysEx.ImportNode(objEntityXContactInfoXmlDoc.DocumentElement ,true);
			SysEx.DocumentElement.AppendChild(objEntityXContactInfoNode);  	

			base.ResetSysExData("EntityXContactInfoSelectedId",""); 
			base.ResetSysExData("EntityXContactInfoGrid_RowDeletedFlag","false"); 
			base.ResetSysExData("EntityXContactInfoGrid_RowAddedFlag", "false");
            //Added Rakhi for R7:Add Employee Data Elements
            bool bMulAddresses = this.objData.Context.InternalSettings.SysSettings.UseMultipleAddresses;
            if (bMulAddresses)
            {
                base.AddDisplayNode("addressesinfo");
                SetPrimaryAddressReadOnly();
                bool bIsFirstRecord = false;
                XmlDocument objEntityXAddressInfoXmlDoc = GetEntityXAddressInfo();
                XmlNode objOldEntityXAddressInfoNode = SysEx.DocumentElement.SelectSingleNode(objEntityXAddressInfoXmlDoc.DocumentElement.LocalName);
                if (objOldEntityXAddressInfoNode != null)
                {
                    SysEx.DocumentElement.RemoveChild(objOldEntityXAddressInfoNode);
                }
                XmlNode objEntityXAddressInfoNode =
                    SysEx.ImportNode(objEntityXAddressInfoXmlDoc.DocumentElement, true);
                SysEx.DocumentElement.AppendChild(objEntityXAddressInfoNode);

                if (SysEx.DocumentElement.SelectSingleNode("IsPostBack") == null)
                    CreateSysExData("IsPostBack");
                if (SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag") != null)
                {
                    if (SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag").InnerText == "true")
                    {
                        if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7")
                        {
                            base.ResetSysExData("IsPostBack", "1");
                        }
                    }
                }
                if (this.Entity.EntityXAddressesList.Count == 0)
                    bIsFirstRecord = true;

                if (base.SysEx.SelectSingleNode("//PrimaryFlag") == null)
                    base.ResetSysExData("PrimaryFlag", "false");

                if (base.SysEx.SelectSingleNode("//PrimaryRow") == null)
                    base.ResetSysExData("PrimaryRow", "");

                if (base.SysEx.SelectSingleNode("//PrimaryAddressChanged") == null)
                    base.ResetSysExData("PrimaryAddressChanged", "false");

                base.ResetSysExData("EntityXAddressesInfoSelectedId", "");
                base.ResetSysExData("EntityXAddressesInfoGrid_RowDeletedFlag", "false");
                base.ResetSysExData("EntityXAddressesInfoGrid_RowAddedFlag", "false");
                base.ResetSysExData("UseMultipleAddresses", bMulAddresses.ToString());
                base.ResetSysExData("IsFirstRecord", bIsFirstRecord.ToString());
            }
            else
            {
                base.AddKillNode("addressesinfo");
                base.AddKillNode("EntityXAddressesInfoGrid");//Mits 22246:GridTitle and Buttons getting visible in topdown Layout
            }

            //MITS:34276 Starts-- Entity ID Type tab display
            XmlDocument objEntityIDTypeAsXmlDoc = GetEntityIdTypeInfoData();

            XmlNode objOldEntityIdTypeAsNode = SysEx.DocumentElement.SelectSingleNode(objEntityIDTypeAsXmlDoc.DocumentElement.LocalName);
            if (objOldEntityIdTypeAsNode != null)
            {
                SysEx.DocumentElement.RemoveChild(objOldEntityIdTypeAsNode);
            }
            XmlNode objEntityIdTypeAsNode = SysEx.ImportNode(objEntityIDTypeAsXmlDoc.DocumentElement, true);
            SysEx.DocumentElement.AppendChild(objEntityIdTypeAsNode);

            if (SysEx.DocumentElement.SelectSingleNode("IsPostBack") == null)
                CreateSysExData("IsPostBack");

            if (SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag") != null && SysEx.DocumentElement.SelectSingleNode("EntityXContactInfoGrid_RowDeletedFlag") != null && SysEx.DocumentElement.SelectSingleNode("EntityXEntityIDTypeGrid_RowDeletedFlag") != null)
            {
                if ((SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAsGrid_RowDeletedFlag").InnerText == "true") || (SysEx.DocumentElement.SelectSingleNode("EntityXContactInfoGrid_RowDeletedFlag").InnerText == "true") || (SysEx.DocumentElement.SelectSingleNode("EntityXEntityIDTypeGrid_RowDeletedFlag").InnerText == "true"))
                { //Parijat: Mits 9610
                    if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7")
                    {
                        base.ResetSysExData("IsPostBack", "1");
                    }
                }
            }

            base.ResetSysExData("EntityXEntityIDTypeSelectedId", "");
            base.ResetSysExData("EntityXEntityIDTypeGrid_RowDeletedFlag", "false");
            base.ResetSysExData("EntityXEntityIDTypeGrid_RowAddedFlag", "false");
            //MITS:34276 Ends

            //if (base.SysEx.SelectSingleNode("//PopupGridRowsDeleted") == null || base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
            //    base.ResetSysExData("PopupGridRowsDeleted", "");
            //Added Rakhi for R7:Add Employee Data Elements

			if (Entity.BirthDate!=string.Empty)
                base.ResetSysExData("EntityAge",Utilities.CalculateAgeInYears(Entity.BirthDate));
			else
				base.ResetSysExData("EntityAge","");
            //smishra25: MITS 24395, MITS 13748 implementation was missing
            //nadim for 13748,added to hide/unhide SSN field
            //pyadav25 - MITS 31338 - 04/24/2013 Add support for View SSN permission
            if (Entity.EntityId>0 && !Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_PEOPLE, FormBase.RMO_MAINT_VIEW_SSN))
            {
                base.AddKillNode("taxid");

            }
            //nadim for 13748

			//Raman Bhatia - Acrosoft Phase 1 changes.. Document Management Not available for Employee if Acrosoft is enabled
			bool bUseAcrosoftInterface = Entity.Context.InternalSettings.SysSettings.UseAcrosoftInterface;
            bool bUsePaperVision = Entity.Context.InternalSettings.SysSettings.UsePaperVisionInterface;//Mona:PaperVisionMerge : Animesh Inserted MITS 16697
            //rbhatia4:R8: Use Media View Setting : July 11 2011
            //if (bUseAcrosoftInterface || bUsePaperVision || Entity.Context.InternalSettings.SysSettings.UseMediaViewInterface)
            //asingh263 mits 30117 starts
            RMConfigurationManager.GetAcrosoftSettings();
            bool bUseRMADocumentManagement = !object.ReferenceEquals(AcrosoftSection.UseRMADocumentManagementForEntity, null) && AcrosoftSection.UseRMADocumentManagementForEntity.ToLower().Equals("true");

            if ((bUseAcrosoftInterface && !bUseRMADocumentManagement) || bUsePaperVision|| Entity.Context.InternalSettings.SysSettings.UseMediaViewInterface)
            //asingh263 mits 30117 ends
            {
                base.AddKillNode("attachdocuments");//Mona:PaperVisionMerge:Animesh Inserted MITS 16697
                base.AddKillNode("attach");
            }

			//JAP - LSS changes - lock down form as read-only if LSS interface created the record
			if (Entity.AddedByUser == "LSSINF")
            {
                //gagnihotri MITS 16453 05/12/2009
                //XmlElement xmlFormElement = (XmlElement)base.SysView.SelectSingleNode("//form");
                //if (xmlFormElement != null) xmlFormElement.SetAttribute("readonly", "1");
                base.ResetSysExData("FormReadOnly", "Disable");
            }
            else
                base.ResetSysExData("FormReadOnly", "Enable");

            //Payee Check: Override OFAC Check Checkbox should not be visible if the utlity setting is OFF
            if (!Entity.Context.InternalSettings.SysSettings.DoOfacCheck)
                base.AddKillNode("OverrideOfacCheck");

            //MITS:34276 Starts -- Entity ID Type View Pemission
            if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_VIEW))
            {
                base.AddKillNode("TABSentityidtypeinfo");
                base.AddKillNode("TBSPentityidtypeinfo");
            }
            //MITS:34276 Ends -- Entity ID Type View Pemission

            //Added by Manika for R8 enhancement of EFT and Withholding
            if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_PEOPLEMAINT_EFT))
            {
                base.AddKillNode("BankingInfo");
            }
            if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_PEOPLEMAINT_WITHHOLDING))
            {
                base.AddKillNode("Withholding");
            }
            //End Manika


            //dbisht6 start on mits 35331

            //if (Entity.ClientSequenceNumber != null && Entity.ClientSequenceNumber != "")
          //  if ((!string.IsNullOrEmpty(Entity.ClientSequenceNumber.ToString()))||Entity.ClientSequenceNumber<=0)
            //if(IsExternalInsured(Entity.EntityId))
            //{
            //    base.AddReadOnlyNode("readOnlyPageExceptSupp");
            //    base.AddKillNode("delete");
            //}

            //dbisht6

            // mkaran2 Start : JIRA RMA 3640
            if (base.SysEx.SelectSingleNode("//hdnTaxId") == null)
                base.ResetSysExData("hdnTaxId", Entity.UnMaskedTaxId);
            //mkaran2 End

		}
        //dbisht6 mits 35331 reopen
        // this function was no longer needed hence commented by dbisht6
        //public bool IsExternalInsured(int iEntityID) 
        //{
        //    bool isExternal = false;
        //    string connectionString = Entity.Context.DbConn.ConnectionString;
        //    int policySysID = 0;
        //    StringBuilder sb = new StringBuilder();
        //    Dictionary<string, int> dict = new Dictionary<string, int>();
        //    sb.Append(" SELECT P.POLICY_SYSTEM_ID from POLICY_X_INSURED PXE ");
        //    sb.Append(" INNER JOIN POLICY P ON PXE.POLICY_ID=P.POLICY_ID");
        //    sb.Append(" WHERE PXE.INSURED_EID = ~ENTITYID~ ");
        //    dict.Add("ENTITYID", iEntityID);
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(sb.ToString()))
        //        {
        //            policySysID = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(connectionString, sb.ToString(), dict), base.ClientId);
        //        }

        //        if (policySysID > 0)
        //            isExternal = true;
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    finally
        //    {
        //        sb = null;
        //        dict = null;
        //    }
        //    return isExternal;
        //}
        ////dbisht6 end

        private void RemoveViewNodesByEntityType()
		{

            if (Entity.EntityTableId != Entity.Context.LocalCache.GetTableId("ADJUSTERS"))
            {
                base.AddKillNode("rmsysuser");
            }
            else
            {
                base.AddDisplayNode("rmsysuser");
            }
//			if(Entity.EntityTableId != Entity.Context.LocalCache.GetTableId("REHAB_JOB_PROVIDER")) //&& Entity.EntityTableId == Entity.Context.LocalCache.GetTableId("ATTORNEYS") && Entity.EntityTableId == Entity.Context.LocalCache.GetTableId("REHAB_PROVIDER"))
//			{
//				base.AddKillNode("parenteid");
//			}
//			if(Entity.EntityTableId != Entity.Context.LocalCache.GetTableId("ATTORNEYS")) //&& Entity.EntityTableId == Entity.Context.LocalCache.GetTableId("ATTORNEYS") && Entity.EntityTableId == Entity.Context.LocalCache.GetTableId("REHAB_PROVIDER"))
//			{
//				base.AddKillNode("parenteid");
//				base.ResetSysExData("ControlAppendAttributeList","");
//			}
//			if(Entity.EntityTableId != Entity.Context.LocalCache.GetTableId("REHAB_PROVIDER")) //&& Entity.EntityTableId == Entity.Context.LocalCache.GetTableId("ATTORNEYS") && Entity.EntityTableId == Entity.Context.LocalCache.GetTableId("REHAB_PROVIDER"))
//			{
//				base.AddKillNode("parenteid");
//			}
		}

		private void PopulateAttributeList(bool p_bIsPEId)
		{
			XmlNode objOld=null;
			XmlElement objNew=null;
			XmlElement objElem=null;
			XmlElement objElem1099=null;
			XmlDocument objXML=base.SysEx;
			XmlElement objChild=null;
			XmlElement objChild1099=null;
			objOld = objXML.SelectSingleNode("//ControlAppendAttributeList");
			objNew = objXML.CreateElement("ControlAppendAttributeList");
			
			if(p_bIsPEId == true)
				objElem=objXML.CreateElement("parenteid");
			objElem1099=objXML.CreateElement("parent1099eid");

			if(p_bIsPEId == true)
			{
				objChild=objXML.CreateElement("tableid");
				objChild.SetAttribute("value","0");
			}

			objChild1099=objXML.CreateElement("tableid");
			objChild1099.SetAttribute("value","0");
			
			//Entity.EntityTableId != Entity.Context.LocalCache.GetTableId("ATTORNEYS") || Entity.EntityTableId != Entity.Context.LocalCache.GetTableId("REHAB_PROVIDER")
			 if(Entity.EntityTableId == Entity.Context.LocalCache.GetTableId("ATTORNEYS"))
				{
					if(p_bIsPEId == true)
						objChild.SetAttribute("value","ATTORNEY_FIRMS");
					objChild1099.SetAttribute("value","ATTORNEY_FIRMS");
				}
			if(Entity.EntityTableId == Entity.Context.LocalCache.GetTableId("REHAB_PROVIDER"))
			{
				if(p_bIsPEId == true)
					objChild.SetAttribute("value","REHAB_PROV_FIRMS");
				objChild1099.SetAttribute("value","REHAB_PROV_FIRMS");
			}
			if(Entity.EntityTableId == Entity.Context.LocalCache.GetTableId("REHAB_JOB_PROVIDER"))
			{
				if(p_bIsPEId == true)
					objChild.SetAttribute("value","REHAB_JOB_FIRMS");
				objChild1099.SetAttribute("value","REHAB_JOB_FIRMS");
			}
		
			if(p_bIsPEId == true)
				objElem.AppendChild(objChild);
			objElem1099.AppendChild(objChild1099);
			if(p_bIsPEId == true)
				objNew.AppendChild(objElem);
			objNew.AppendChild(objElem1099);

			if (Entity.EntityId!=0)
			{
				objElem=objXML.CreateElement("entitytableidtextname");

				objChild=objXML.CreateElement("required");
				objChild.SetAttribute("value","yes");
				objElem.AppendChild(objChild);
				objNew.AppendChild(objElem);
			}

			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);			
		}

		private void PopulateEntityTypes()
		{
			int iTableId = 0;
            //Added by Shivendu to add a new value "" in the drop down
            bool bBlankValue = true;
			string sSQL = @"SELECT GT.TABLE_NAME, G.TABLE_ID 
							FROM GLOSSARY_TEXT GT, GLOSSARY G 
							WHERE GT.TABLE_ID=G.TABLE_ID AND G.GLOSSARY_TYPE_CODE=7 ORDER BY GT.TABLE_NAME";
				
			
			//Create the necessary SysExData to be used in ref binding
			XmlElement xmlRoot = base.SysEx.CreateElement("EntityTypeList");

			using(DbReader rdr = Entity.Context.DbConnLookup.ExecuteReader(sSQL))
			{
				//Loop through and create all the option values for the combobox control
				while(rdr.Read())
				{
					// Reference to employee and quest tables not added in the combo 'Type of person' - as per RM World
                    //srajindersin - MITS 28954 07/31/2012 - new entries created in Glossary for following 5 items.
                    iTableId = rdr.GetInt("TABLE_ID");
                    if (iTableId != Entity.Context.LocalCache.GetTableId("EMPLOYEES") &&
                        iTableId != Entity.Context.LocalCache.GetTableId("PHYSICIANS") &&
                        iTableId != Entity.Context.LocalCache.GetTableId("MEDICAL_STAFF") &&
                        iTableId != Entity.Context.LocalCache.GetTableId("PATIENTS") &&
                        iTableId != Entity.Context.LocalCache.GetTableId("WITNESS"))
					{
                        //If condition added by Shivendu to add a new value "" in the drop down
                        if (bBlankValue)
                        {
                            //Start by Shivendu to add a new value "" in the drop down
                            XmlElement xmlOptionBlank = base.SysEx.CreateElement("option");
                            xmlOptionBlank.InnerText = "";
                            XmlAttribute xmlOptionBlankAttrib = base.SysEx.CreateAttribute("value");
                            xmlOptionBlankAttrib.Value = "";
                            xmlOptionBlank.Attributes.Append(xmlOptionBlankAttrib);
                            xmlRoot.AppendChild(xmlOptionBlank);
                            bBlankValue = false;
                            //Start by Shivendu for MITS 11709
                            XmlElement xmlOption = base.SysEx.CreateElement("option");
                            xmlOption.InnerText = rdr.GetString("TABLE_NAME");
                            XmlAttribute xmlOptionAttrib = base.SysEx.CreateAttribute("value");
                            xmlOptionAttrib.Value = iTableId.ToString();
                            xmlOption.Attributes.Append(xmlOptionAttrib);
                            xmlRoot.AppendChild(xmlOption);
                            //End by Shivendu for MITS 11709
                            //End by Shivendu to add a new value "" in the drop down
                        }
                        else
                        {
                            XmlElement xmlOption = base.SysEx.CreateElement("option");
                            xmlOption.InnerText = rdr.GetString("TABLE_NAME");
                            XmlAttribute xmlOptionAttrib = base.SysEx.CreateAttribute("value");
                            xmlOptionAttrib.Value = iTableId.ToString();
                            xmlOption.Attributes.Append(xmlOptionAttrib);
                            xmlRoot.AppendChild(xmlOption);
                        }
					}
				}
				rdr.Close();
			}

			//Add the XML to the SysExData 
			XmlNode objOrig = base.SysEx.SelectSingleNode("/*/EntityTypeList");
			if(objOrig!=null)
				base.SysEx.DocumentElement.RemoveChild(objOrig);
			base.SysEx.DocumentElement.AppendChild(xmlRoot as XmlNode);
			base.ResetSysExData("EntityType",Entity.EntityTableId.ToString());
			base.ResetSysExData("EntityTypeName",Entity.Context.LocalCache.GetUserTableName(Entity.EntityTableId));

		}
        //mbahl3 mits:29316
        private void PopulateIDTypes()
        {
            string sSQL = @"SELECT CODES_TEXT.CODE_ID,CODES_TEXT.CODE_DESC FROM CODES_TEXT,CODES,GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME = 'TAX_ID_TYPES' AND CODES.CODE_ID=CODES_TEXT.CODE_ID AND GLOSSARY.TABLE_ID = CODES.TABLE_ID ORDER BY CODE_DESC DESC";


            XmlElement xmlRoot = base.SysEx.CreateElement("IDTypeList");

            using (DbReader rdr = Entity.Context.DbConnLookup.ExecuteReader(sSQL))
            {
                while (rdr.Read())
                {
                    XmlElement xmlOption = base.SysEx.CreateElement("option");
                    xmlOption.InnerText = Conversion.ConvertObjToStr(rdr.GetValue("CODE_DESC"));
                    XmlAttribute xmlOptionAttrib = base.SysEx.CreateAttribute("value");
                    xmlOptionAttrib.Value = Conversion.ConvertObjToStr(rdr.GetValue("CODE_ID"));
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    xmlRoot.AppendChild(xmlOption);

                }
                rdr.Close();
            }

            XmlNode objOrig = base.SysEx.SelectSingleNode("/*/IDTypeList");
            if (objOrig != null)
                base.SysEx.DocumentElement.RemoveChild(objOrig);
            base.SysEx.DocumentElement.AppendChild(xmlRoot as XmlNode);
            //mkaran2 - MITS 36802 : : start
            if ((DBNull.Value.Equals(Entity.IDType) || Entity.IDType ==0 )&& !string.IsNullOrEmpty(Entity.TaxId))
            {
                if (Entity.TaxId.Trim().IndexOf('-') != 2)
                Entity.IDType = Entity.Context.LocalCache.GetCodeId("S", "TAX_ID_TYPES");
                else
                Entity.IDType = Entity.Context.LocalCache.GetCodeId("F", "TAX_ID_TYPES");
            }
            //mkaran2 - MITS 36802 : : end
            base.ResetSysExData("IDType", Entity.IDType.ToString());
            //mbahl3 mits:29316
            ArrayList singleRow = new ArrayList();

            if (Entity.IDType > 0)
            {
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                base.AddElementToList(ref singleRow, "id", "lbl_taxid");
                if (Entity.Context.LocalCache.GetCodeId("S", "TAX_ID_TYPES") == Entity.IDType)
                    base.AddElementToList(ref singleRow, "Text", "SSN");
                else
                    base.AddElementToList(ref singleRow, "Text", "FEIN");

                base.m_ModifiedControls.Add(singleRow);
            }
           
        }
        //mbahl3 mits:29316
		private void ApplyFormTitle()
		{
			//string sCaption= Entity.Context.LocalCache.GetUserTableName(Entity.EntityTableId) + 
			string sCaption= "People Maintenance";
            // Anjaneya : MITS 9678
            if (Entity != null)
            {
                sCaption += " [ " + Entity.Default + " ]";
            }
			//Pass this subtitle value to view (ref'ed from @valuepath).
			base.ResetSysExData("SubTitle",sCaption);
            ArrayList singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", sCaption);
            base.m_ModifiedControls.Add(singleRow);
		}


		//This method will be invoked both on Post-Back and on Save.
		public override void OnUpdateObject()
        {
            //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            if (base.SysEx.SelectSingleNode("//claimid") != null && !string.IsNullOrEmpty(base.SysEx.SelectSingleNode("//claimid").InnerText))
                Entity.ClaimId = Convert.ToInt32(base.SysEx.SelectSingleNode("//claimid").InnerText);
            //Ankit End
            // mkaran2 Start : JIRA RMA 3640                    
            if (Entity.Context.InternalSettings.SysSettings.MaskSSN)
            {
                XmlNode objHiddenTaxId = base.SysEx.SelectSingleNode("//hdnTaxId");
                if (objHiddenTaxId != null && !string.IsNullOrEmpty(objHiddenTaxId.InnerText))
                            Entity.TaxId = objHiddenTaxId.InnerText;
            }
            // mkaran2 End
			base.OnUpdateObject ();

            //avipinsrivas start : Worked for Jira-340
            #region "Updating Entity Roles Object"
            //if (base.SysEx.SelectSingleNode("//EntityXRoleList//@codeid") != null)
            //{
            //    int iErRowID;
            //    string sTrimString = "</EntityXRoleList>";
            //    //avipinsrivas Start : Worked for 7488 (Issue of 4634 - Epic 340)
            //    string sPeopleRoleTableIDs = string.Empty;
            //    string sOtherRolesTableIds = string.Empty;
            //    //avipinsrivas End
            //    string sEntityRoleList = base.SysEx.SelectSingleNode("//EntityXRoleList//@codeid").InnerText;
            //    if (sEntityRoleList.Contains(sTrimString))
            //    {
            //        sEntityRoleList = sEntityRoleList.Remove(0, (sEntityRoleList.IndexOf(sTrimString) + sTrimString.Length)).Trim();
            //        if (base.SysEx.SelectSingleNode("//EntityXRoleTableIds") != null && !string.IsNullOrEmpty(base.SysEx.SelectSingleNode("//EntityXRoleTableIds").InnerText))
            //            sEntityRoleList = string.Concat(sEntityRoleList, " ", base.SysEx.SelectSingleNode("//EntityXRoleTableIds").InnerText);
            //    }
            //    //avipinsrivas Start : Worked for 7488 (Issue of 4634 - Epic 340)
            //    if (!string.IsNullOrEmpty(sEntityRoleList))
            //        sPeopleRoleTableIDs = Entity.GetSpaceSepTableId(sEntityRoleList, 7, false);
            //    if (base.SysEx.SelectSingleNode("//EntityXRoleTableIds") != null && !string.IsNullOrEmpty(base.SysEx.SelectSingleNode("//EntityXRoleTableIds").InnerText))
            //        sOtherRolesTableIds = Entity.GetSpaceSepTableId(base.SysEx.SelectSingleNode("//EntityXRoleTableIds").InnerText, 7, true);

            //    sEntityRoleList = string.Concat(sPeopleRoleTableIDs, " ", sOtherRolesTableIds);
            //    //avipinsrivas End
            //    Entity.UpdateEntityRoles((Entity as IDataModel), sEntityRoleList.Trim(), false, out iErRowID);
            //}
            #endregion
            //avipinsrivas End

			#region "Updating Contact Info Object"
			SortedList objEntityXContactInfoList = new SortedList(); 

			XmlNode objEntityXContactInfoNode = base.SysEx.DocumentElement.SelectSingleNode("EntityXContactInfo");  
			XmlAttribute objTypeAttributeNode = null;

			bool bIsNew = false;
			int iContactIdKey = -1;

			XmlDocument objEntityXContactXmlDoc = null;
			XmlElement objEntityXContactRootElement = null;
			
			int iEntityXContactInfoSelectedId = 
				base.GetSysExDataNodeInt("EntityXContactInfoSelectedId",true); 

			string sEntityXContactInfoRowAddedFlag =  
				base.GetSysExDataNodeText("EntityXContactInfoGrid_RowAddedFlag",true); 
			bool bEntityXContactInfoRowAddedFlag = 
				sEntityXContactInfoRowAddedFlag.ToUpper().CompareTo("TRUE")==0?true:false;  

			string sEntityXContactInfoRowDeletedFlag =  
				base.GetSysExDataNodeText("EntityXContactInfoGrid_RowDeletedFlag",true); 
			bool bEntityXContactInfoRowDeletedFlag = 
				sEntityXContactInfoRowDeletedFlag.ToUpper().CompareTo("TRUE")==0?true:false;  


			if(objEntityXContactInfoNode!=null)
			{
				// Loop through data for all rows of the grid
				foreach(XmlNode objOptionNode in objEntityXContactInfoNode.SelectNodes("option"))
				{
					objTypeAttributeNode = objOptionNode.Attributes["type"];
					bIsNew = false;
					if(objTypeAttributeNode!=null && objTypeAttributeNode.InnerText.Equals("new")==true)
					{
						// This is the 'extra' hidden <option> for capturing a new grid row data
						bIsNew = true;
					}					
					if((bIsNew==false)||(bIsNew==true&&bEntityXContactInfoRowAddedFlag==true))
					{
						objEntityXContactXmlDoc = new XmlDocument();
						objEntityXContactRootElement = objEntityXContactXmlDoc.CreateElement("EntityXContactInfo");
						objEntityXContactXmlDoc.AppendChild(objEntityXContactRootElement);
						objEntityXContactRootElement.InnerXml = objOptionNode.InnerXml;

						iContactIdKey = 
							Conversion.ConvertStrToInteger(objEntityXContactRootElement.SelectSingleNode("ContactId").InnerText); 
                        if ((bIsNew == true && bEntityXContactInfoRowAddedFlag == true) ||
                            (iContactIdKey < 0 && objEntityXContactRootElement.SelectSingleNode("EntityId").InnerText == ""))
                        {
                         //   if (bIsNew == true && bEntityXContactInfoRowAddedFlag == true)
                                objEntityXContactRootElement.SelectSingleNode("EntityId").InnerText = "0";
                        }

						if(bIsNew==false&&bEntityXContactInfoRowDeletedFlag==true&&iContactIdKey==iEntityXContactInfoSelectedId)
						{
							// Aditya - This particular Tpp has been deleted. 
							continue;
						}
							
						objEntityXContactInfoList.Add(iContactIdKey,objEntityXContactXmlDoc);   
					}
				}
			}

			foreach(EntityXContactInfo objEntityXContactInfo in Entity.EntityXContactInfoList)
			{
				if(base.m_fda.SafeFormVariableParamText("SysCmd")=="7" && 
					(objEntityXContactInfo.ContactId<0 || (bEntityXContactInfoRowDeletedFlag==true&&objEntityXContactInfo.ContactId==iEntityXContactInfoSelectedId)))
					Entity.EntityXContactInfoList.Remove(objEntityXContactInfo.ContactId);
				else if (objEntityXContactInfo.ContactId<0)
					Entity.EntityXContactInfoList.Remove(objEntityXContactInfo.ContactId);
			}		
			EntityXContactInfo objTmpEntityXContactInfo = null;
			XmlDocument objTmpEntityXContactXMLDoc = null;
            //Parijat: Mits 9936
            ArrayList arrContactIds = new ArrayList();
            //

			for(int iListIndex=0; iListIndex<objEntityXContactInfoList.Count; iListIndex++)
			{
				objTmpEntityXContactXMLDoc = (XmlDocument) objEntityXContactInfoList.GetByIndex(iListIndex);

				if(objTmpEntityXContactXMLDoc.DocumentElement.SelectSingleNode("ContactId").InnerText.Trim() == "" || Conversion.ConvertStrToInteger(objTmpEntityXContactXMLDoc.DocumentElement.SelectSingleNode("ContactId").InnerText)< 0 )
				{
					objTmpEntityXContactXMLDoc.DocumentElement.SelectSingleNode("ContactId").InnerText = ((-1 * iListIndex)-1).ToString(); 
					objTmpEntityXContactInfo = Entity.EntityXContactInfoList.AddNew();
					objTmpEntityXContactInfo.PopulateObject(objTmpEntityXContactXMLDoc); 
				}
				else
				{
					objTmpEntityXContactInfo=Entity.EntityXContactInfoList[Conversion.ConvertStrToInteger(objTmpEntityXContactXMLDoc.DocumentElement.SelectSingleNode("ContactId").InnerText)];
					objTmpEntityXContactInfo.PopulateObject(objTmpEntityXContactXMLDoc); 
				}
                //Parijat:Mits 9936 similar to 9610
                arrContactIds.Add(objTmpEntityXContactXMLDoc.DocumentElement.SelectSingleNode("ContactId").InnerText);
            }
            //Parijat:Mits 9936 similar to 9610
            foreach (EntityXContactInfo objEntityXContactInfo in Entity.EntityXContactInfoList)
            {
                if (!arrContactIds.Contains(objEntityXContactInfo.ContactId.ToString()))
                {
                    Entity.EntityXContactInfoList.Remove(objEntityXContactInfo.ContactId);
                }
            }

			#endregion

			#region "Updating Operating As Object"
			SortedList objEntityXOperatingAsList = new SortedList(); 

			XmlNode objEntityXOperatingAsNode = base.SysEx.DocumentElement.SelectSingleNode("EntityXOperatingAs");  
			objTypeAttributeNode = null;

			bIsNew = false;
			int iOperatingIdKey = -1;

			XmlDocument objEntityXOAXmlDoc = null;
			XmlElement objEntityXOARootElement = null;
			
			int iEntityXOperatingAsSelectedId = 
				base.GetSysExDataNodeInt("EntityXOperatingAsSelectedId",true); 

			string sEntityXOperatingAsRowAddedFlag =  
				base.GetSysExDataNodeText("EntityXOperatingAsGrid_RowAddedFlag",true); 
			bool bEntityXOperatingAsRowAddedFlag = 
				sEntityXOperatingAsRowAddedFlag.ToUpper().CompareTo("TRUE")==0?true:false;  

			string sEntityXOperatingAsRowDeletedFlag =  
				base.GetSysExDataNodeText("EntityXOperatingAsGrid_RowDeletedFlag",true); 
			bool bEntityXOperatingAsRowDeletedFlag = 
				sEntityXOperatingAsRowDeletedFlag.ToUpper().CompareTo("TRUE")==0?true:false;  


			if(objEntityXOperatingAsNode!=null)
			{
				// Loop through data for all rows of the grid
				foreach(XmlNode objOptionNode in objEntityXOperatingAsNode.SelectNodes("option"))
				{
					objTypeAttributeNode = objOptionNode.Attributes["type"];
					bIsNew = false;
					if(objTypeAttributeNode!=null && objTypeAttributeNode.InnerText.Equals("new")==true)
					{
						// This is the 'extra' hidden <option> for capturing a new grid row data
						bIsNew = true;
					}					
					if((bIsNew==false)||(bIsNew==true&&bEntityXOperatingAsRowAddedFlag==true))
					{
						objEntityXOAXmlDoc = new XmlDocument();
						objEntityXOARootElement = objEntityXOAXmlDoc.CreateElement("EntityXOperatingAs");
						objEntityXOAXmlDoc.AppendChild(objEntityXOARootElement);
						objEntityXOARootElement.InnerXml = objOptionNode.InnerXml;

						iOperatingIdKey = 
							Conversion.ConvertStrToInteger(objEntityXOARootElement.SelectSingleNode("OperatingId").InnerText); 

                        if ((bIsNew == true && bEntityXOperatingAsRowAddedFlag == true) ||
                            (iOperatingIdKey < 0 && objEntityXOARootElement.SelectSingleNode("EntityId").InnerText == ""))
                        {//if(bIsNew==true&&bEntityXOperatingAsRowAddedFlag==true)
                            objEntityXOARootElement.SelectSingleNode("EntityId").InnerText = "0";
                        }

						if(bIsNew==false&&bEntityXOperatingAsRowDeletedFlag==true&&iOperatingIdKey==iEntityXOperatingAsSelectedId)
						{
							// Aditya - This particular Tpp has been deleted. 
							continue;
						}
							
						objEntityXOperatingAsList.Add(iOperatingIdKey,objEntityXOAXmlDoc);   
					}
				}
			}

			foreach(EntityXOperatingAs objEntityXOperatingAs in Entity.EntityXOperatingAsList)
			{
				if(base.m_fda.SafeFormVariableParamText("SysCmd")=="7" && 
					(objEntityXOperatingAs.OperatingId<0 || (bEntityXOperatingAsRowDeletedFlag==true&&objEntityXOperatingAs.OperatingId==iEntityXOperatingAsSelectedId)))
					Entity.EntityXOperatingAsList.Remove(objEntityXOperatingAs.OperatingId);
				else if (objEntityXOperatingAs.OperatingId<0)
					Entity.EntityXOperatingAsList.Remove(objEntityXOperatingAs.OperatingId);
			}
			EntityXOperatingAs objTmpEntityXOperatingAs = null;
			XmlDocument objTmpEntityXOAXMLDoc = null;
            //Parijat: Mits 9936 similar to 9610
            ArrayList arrOperatingIds = new ArrayList();
            //

			for(int iListIndex=0; iListIndex<objEntityXOperatingAsList.Count; iListIndex++)
			{
				objTmpEntityXOAXMLDoc = (XmlDocument) objEntityXOperatingAsList.GetByIndex(iListIndex);

				if(objTmpEntityXOAXMLDoc.DocumentElement.SelectSingleNode("OperatingId").InnerText.Trim() == "" || Conversion.ConvertStrToInteger(objTmpEntityXOAXMLDoc.DocumentElement.SelectSingleNode("OperatingId").InnerText)< 0 )
				{
					objTmpEntityXOAXMLDoc.DocumentElement.SelectSingleNode("OperatingId").InnerText = ((-1 * iListIndex)-1).ToString(); 
					objTmpEntityXOperatingAs = Entity.EntityXOperatingAsList.AddNew();
				}
				else
					objTmpEntityXOperatingAs=Entity.EntityXOperatingAsList[Conversion.ConvertStrToInteger(objTmpEntityXOAXMLDoc.DocumentElement.SelectSingleNode("OperatingId").InnerText)];
				objTmpEntityXOperatingAs.PopulateObject(objTmpEntityXOAXMLDoc);
                //Parijat:Mits 9936 similar to 9610
                arrOperatingIds.Add(objTmpEntityXOAXMLDoc.DocumentElement.SelectSingleNode("OperatingId").InnerText);

			}
            //Parijat:Mits 9936 similar to 9610
            foreach (EntityXOperatingAs objEntityXOperatingAs in Entity.EntityXOperatingAsList)
            {
                if (!arrOperatingIds.Contains(objEntityXOperatingAs.OperatingId.ToString()))
                {
                    Entity.EntityXOperatingAsList.Remove(objEntityXOperatingAs.OperatingId);
                }
            }


			#endregion
            //Added Rakhi for R7:Add Emp Data Elements
            #region "Updating Address Info Object"
            bool bMulAddresses = this.objData.Context.InternalSettings.SysSettings.UseMultipleAddresses;
            if (bMulAddresses)
            {
                SortedList objEntityXAddressesInfoList = new SortedList();
                SortedList objAddressesInfoList = new SortedList();//Added For R7:Add Emp Data Elements
                //XmlDocument objAddressXPhoneInfoDoc = null; //Added For R7:Add Emp Data Elements
                bool bIsSuccess = false;
                XmlDocument objEntityXAddressXmlDoc = null;
                XmlElement objEntityXAddressRootElement = null;
                int iAddressIdKey = -1;
                int iRowIdKey = -1;//AA//RMA-8753 nshah28(Added by ashish)

                objTypeAttributeNode = null;
                bIsNew = false;

                int iEntityXAddressesInfoSelectedId =
                    base.GetSysExDataNodeInt("EntityXAddressesInfoSelectedId", true);

                string sEntityXAddressesInfoRowAddedFlag =
                    base.GetSysExDataNodeText("EntityXAddressesInfoGrid_RowAddedFlag", true);
                bool bEntityXAddressesInfoRowAddedFlag =
                    sEntityXAddressesInfoRowAddedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

                string sEntityXAddressesInfoRowDeletedFlag =
                    base.GetSysExDataNodeText("EntityXAddressesInfoGrid_RowDeletedFlag", true);
                bool bEntityXAddressesInfoRowDeletedFlag =
                    sEntityXAddressesInfoRowDeletedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;


                XmlNode objEntityXAddressesInfoNode = base.SysEx.DocumentElement.SelectSingleNode("EntityXAddressesInfo");
                if (objEntityXAddressesInfoNode != null)
                {
                    // Loop through data for all rows of the grid
                    foreach (XmlNode objOptionNode in objEntityXAddressesInfoNode.SelectNodes("option"))
                    {
                        objTypeAttributeNode = objOptionNode.Attributes["type"];
                        bIsNew = false;
                        if (objTypeAttributeNode != null && objTypeAttributeNode.InnerText.Equals("new") == true)
                        {
                            // This is the 'extra' hidden <option> for capturing a new grid row data
                            bIsNew = true;
                        }
                        if ((bIsNew == false) || (bIsNew == true && bEntityXAddressesInfoRowAddedFlag == true))
                        {
                            objEntityXAddressXmlDoc = new XmlDocument();
                            objEntityXAddressRootElement = objEntityXAddressXmlDoc.CreateElement("EntityXAddresses");
                            objEntityXAddressXmlDoc.AppendChild(objEntityXAddressRootElement);

                            #region AddressXPhone Info Grid
                            //XmlNode objAddressNode = objOptionNode.SelectSingleNode("AddressXPhoneInfo");
                            //if (objOptionNode != null && !(bIsNew == false && bEntityXAddressesInfoRowDeletedFlag == true && iAddressIdKey == iEntityXAddressesInfoSelectedId))
                            //{

                            //    XmlElement objAddressXPhoneRootElement = null;
                            //    int iPhoneId = 0;
                            //    bool bResult = false;
                            //    string sKey = string.Empty;
                            //    foreach (XmlNode objPhoneNode in objAddressNode.SelectNodes("option"))
                            //    {
                            //        objAddressXPhoneInfoDoc = new XmlDocument();
                            //        objAddressXPhoneRootElement = objAddressXPhoneInfoDoc.CreateElement("AddressXPhoneInfo");
                            //        objAddressXPhoneInfoDoc.AppendChild(objAddressXPhoneRootElement);
                            //        objPhoneNode.SelectSingleNode("ContactId").InnerText = objOptionNode.SelectSingleNode("ContactId").InnerText;
                            //        objAddressXPhoneRootElement.InnerXml = objPhoneNode.InnerXml;
                            //        bResult = Int32.TryParse(objAddressXPhoneRootElement.SelectSingleNode("PhoneId").InnerText, out iPhoneId);
                            //        if (iPhoneId != 0)
                            //        {
                            //            sKey = objPhoneNode.SelectSingleNode("ContactId").InnerText + "_" + objAddressXPhoneRootElement.SelectSingleNode("PhoneId").InnerText;
                            //            objAddressesInfoList.Add(sKey, objAddressXPhoneInfoDoc);
                            //        }
                            //    }
                            //}
                            //objOptionNode.RemoveChild(objAddressNode);
                            #endregion //AddressXPhone Info Grid

                            objEntityXAddressRootElement.InnerXml = objOptionNode.InnerXml;

                            //AA //RMA-8753 nshah28 start
                            string strSearchString = string.Empty;
                            string sStateCode = string.Empty;
                            string sStateDesc = string.Empty;
                            string sCountry = string.Empty;

                            AddressForm objAddressForm = new AddressForm(m_fda);

                            strSearchString = objAddressForm.CreateSearchString(objEntityXAddressRootElement, objData.Context.LocalCache);
                            //RMA-8753 nshah28(Added by ashish)

                            XmlElement objRootElement = objEntityXAddressXmlDoc.CreateElement("SearchString");
                            objRootElement.InnerText = strSearchString; //change by nshah28
                            objEntityXAddressRootElement.AppendChild(objRootElement);
                            //RMA-8753 nshah28(Added by ashish) END

                            //iAddressIdKey =
                            //    Conversion.CastToType<int>(objEntityXAddressRootElement.SelectSingleNode("AddressId").InnerText, out bIsSuccess);

                            iRowIdKey =
                               Conversion.CastToType<int>(objEntityXAddressRootElement.SelectSingleNode("RowId").InnerText, out bIsSuccess);

                            if ((bIsNew == true && bEntityXAddressesInfoRowAddedFlag == true) ||
                                (iAddressIdKey < 0 && objEntityXAddressRootElement.SelectSingleNode("EntityId").InnerText == ""))
                                objEntityXAddressRootElement.SelectSingleNode("EntityId").InnerText = "0";
                            else
                                objEntityXAddressRootElement.SelectSingleNode("EntityId").InnerText = Entity.EntityId.ToString();
                            //RMA-8753 nshah28 END
                            //Marking Changed Address as Primary Address
                            XmlNode objPrimaryAddressChanged = base.SysEx.SelectSingleNode("//PrimaryAddressChanged");
                            XmlNode objNewPrimaryAddress = objEntityXAddressRootElement.SelectSingleNode("NewPrimaryRecord"); //For identification of New Primary Address
                            if (objPrimaryAddressChanged != null && objPrimaryAddressChanged.InnerText.ToLower() == "true")
                            {
                                if (objNewPrimaryAddress != null)
                                {
                                    XmlNode objPrimaryAddress = objEntityXAddressRootElement.SelectSingleNode("PrimaryAddress");
                                    if (objPrimaryAddress != null)
                                    {
                                        if (objNewPrimaryAddress.InnerText.ToLower() == "true")
                                        {
                                            objPrimaryAddress.InnerText = "True";
                                        }

                                        else
                                        {
                                            objPrimaryAddress.InnerText = "False";
                                        }
                                    }
                                }
                            }
                            if (objNewPrimaryAddress != null)
                            {
                                objEntityXAddressRootElement.RemoveChild(objNewPrimaryAddress); //Since it is not a Datamodel Property,Removing it before the processing.
                            }
                            //Marking Changed Address as Primary Address
                            if (bIsNew == false && bEntityXAddressesInfoRowDeletedFlag == true && iRowIdKey == iEntityXAddressesInfoSelectedId) //RMA-8753 nshah28
                            {
                                // Aditya - This particular Tpp has been deleted. 
                                continue;
                            }
                            objEntityXAddressesInfoList.Add(iRowIdKey, objEntityXAddressXmlDoc);//AA//RMA-8753 nshah28(Added by ashish)
                        }
                    }
                }

                //AA//RMA-8753 nshah28
                //foreach (EntityXAddresses objEntityXAddressesInfo in Entity.EntityXAddressesList)
                //{
                //    if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7" &&
                //        (objEntityXAddressesInfo.AddressId < 0 || (bEntityXAddressesInfoRowDeletedFlag == true && objEntityXAddressesInfo.AddressId == iEntityXAddressesInfoSelectedId)))
                //    {
                //        Entity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                //    }
                //    else if (objEntityXAddressesInfo.AddressId < 0)
                //        Entity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                //}
                foreach (EntityXAddresses objEntityXAddressesInfo in Entity.EntityXAddressesList)
                {
                    if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7" &&
                        (objEntityXAddressesInfo.RowId < 0 || (bEntityXAddressesInfoRowDeletedFlag == true && objEntityXAddressesInfo.RowId == iEntityXAddressesInfoSelectedId)))
                    {
                        Entity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId);
                    }
                    else if (objEntityXAddressesInfo.RowId < 0)
                        Entity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId);
                }
                //AA//RMA-8753 nshah28 END
                EntityXAddresses objTmpEntityXAddressesInfo = null;
                XmlDocument objTmpEntityXAddressXMLDoc = null;
                //AddressXPhoneInfo objTmpAddressXPhoneInfo = null;
                //XmlDocument objTmpAddressXPhoneXMLDoc = null;
                //string sOrgAddressId = string.Empty;
                //Parijat:Mits 9610
                ArrayList arrAddressIds = new ArrayList();
                ArrayList arrRowIds = new ArrayList(); //RMA-8753 nshah28 
                //
                for (int iListIndex = 0; iListIndex < objEntityXAddressesInfoList.Count; iListIndex++)
                {
                    objTmpEntityXAddressXMLDoc = (XmlDocument)objEntityXAddressesInfoList.GetByIndex(iListIndex);
                    //sOrgAddressId = objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("AddressId").InnerText.Trim();
                    //AA//RMA-8753 nshah28 STARt
                    if (objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText.Trim() == "" || Conversion.CastToType<int>(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText, out bIsSuccess) < 0)
                    {
                        objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText = ((-1 * iListIndex) - 1).ToString();
                        objTmpEntityXAddressesInfo = Entity.EntityXAddressesList.AddNew();
                    }
                    else
                        objTmpEntityXAddressesInfo = Entity.EntityXAddressesList[Conversion.CastToType<int>(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText, out bIsSuccess)];
                    //Parijat:Mits 9610
                    arrAddressIds.Add(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("AddressId").InnerText);
                    //objTmpEntityXAddressesInfo.PopulateObject(objTmpEntityXAddressXMLDoc);
                    arrRowIds.Add(objTmpEntityXAddressXMLDoc.DocumentElement.SelectSingleNode("RowId").InnerText);//RMA-8753 nshah28 
                    //AA//RMA-8753 nshah28 END
                    //RMA 8753 Ashish Ahuja For Address Master Enhancement Start
                    if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//RowId").InnerText != String.Empty)
                        objTmpEntityXAddressesInfo.RowId = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//RowId").InnerText);
                    if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//AddressType").Attributes["codeid"].Value != String.Empty)
                        objTmpEntityXAddressesInfo.AddressType = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//AddressType").Attributes["codeid"].Value);
                    if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//PrimaryAddress").InnerText != String.Empty)
                        objTmpEntityXAddressesInfo.PrimaryAddress = Convert.ToBoolean(objTmpEntityXAddressXMLDoc.SelectSingleNode("//PrimaryAddress").InnerText) ? -1 : 0;
                    //objTmpEntityXAddressesInfo.PrimaryAddress = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//PrimaryAddress").InnerText);
                    objTmpEntityXAddressesInfo.Address.Addr1 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr1").InnerText;
                    objTmpEntityXAddressesInfo.Address.Addr2 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr2").InnerText;
                    objTmpEntityXAddressesInfo.Address.Addr3 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr3").InnerText;
                    objTmpEntityXAddressesInfo.Address.Addr4 = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Addr4").InnerText;
                    objTmpEntityXAddressesInfo.Address.City = objTmpEntityXAddressXMLDoc.SelectSingleNode("//City").InnerText;
                    if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//Country").Attributes["codeid"].Value != String.Empty)
                        objTmpEntityXAddressesInfo.Address.Country = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//Country").Attributes["codeid"].Value);
                    if (objTmpEntityXAddressXMLDoc.SelectSingleNode("//State").Attributes["codeid"].Value != String.Empty)
                        objTmpEntityXAddressesInfo.Address.State = Convert.ToInt32(objTmpEntityXAddressXMLDoc.SelectSingleNode("//State").Attributes["codeid"].Value);
                    objTmpEntityXAddressesInfo.Email = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Email").InnerText;
                    objTmpEntityXAddressesInfo.Fax = objTmpEntityXAddressXMLDoc.SelectSingleNode("//Fax").InnerText; ;
                    objTmpEntityXAddressesInfo.Address.County = objTmpEntityXAddressXMLDoc.SelectSingleNode("//County").InnerText;
                    objTmpEntityXAddressesInfo.Address.ZipCode = objTmpEntityXAddressXMLDoc.SelectSingleNode("//ZipCode").InnerText;
                    objTmpEntityXAddressesInfo.EffectiveDate = objTmpEntityXAddressXMLDoc.SelectSingleNode("//EffectiveDate").InnerText;
                    objTmpEntityXAddressesInfo.ExpirationDate = objTmpEntityXAddressXMLDoc.SelectSingleNode("//ExpirationDate").InnerText;

                    objTmpEntityXAddressesInfo.Address.SearchString = objTmpEntityXAddressXMLDoc.SelectSingleNode("//SearchString").InnerText;
                    int iAddressId = CommonFunctions.CheckAddressDuplication(objTmpEntityXAddressXMLDoc.SelectSingleNode("//SearchString").InnerText, Entity.Context.RMDatabase.ConnectionString, base.ClientId);
                    objTmpEntityXAddressesInfo.AddressId = iAddressId;
                    objTmpEntityXAddressesInfo.Address.AddressId = iAddressId;
                    //RMA 8753 Ashish Ahuja For Address Master Enhancement End

                    #region Phone Info Details
                    //int iPhoneIndex = 0;
                    //for (int iCount = 0; iCount < objAddressesInfoList.Count; iCount++)
                    //{
                    //    objTmpAddressXPhoneXMLDoc = (XmlDocument)objAddressesInfoList.GetByIndex(iCount);
                    //    if (sOrgContactId == objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("ContactId").InnerText.Trim())
                    //    {
                    //        if (objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText.Trim() == "" || Conversion.ConvertStrToInteger(objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText) < 0)
                    //        {
                    //            objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText = ((-1 * iPhoneIndex) - 1).ToString();
                    //            objTmpAddressXPhoneInfo = objTmpEntityXAddressesInfo.AddressXPhoneInfoList.AddNew();
                    //            iPhoneIndex++;
                    //        }
                    //        else
                    //            objTmpAddressXPhoneInfo = objTmpEntityXAddressesInfo.AddressXPhoneInfoList[Conversion.ConvertStrToInteger(objTmpAddressXPhoneXMLDoc.DocumentElement.SelectSingleNode("PhoneId").InnerText)];
                    //        objTmpAddressXPhoneInfo.PopulateObject(objTmpAddressXPhoneXMLDoc);
                    //        objTmpAddressXPhoneInfo.ContactId = objTmpEntityXAddressesInfo.ContactId;
                    //    }
                    //}
                    #endregion //Phone Info Details

                    #region Added code to remove the saved phone numbers
                    //if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
                    //{
                    //    XmlNode objPhoneNumbersDeleted = base.SysEx.SelectSingleNode("//PopupGridRowsDeleted");
                    //    if (objPhoneNumbersDeleted != null && !String.IsNullOrEmpty(objPhoneNumbersDeleted.InnerText))
                    //    {
                    //        string[] strSplit = objPhoneNumbersDeleted.InnerText.Split(new Char[] { '|' });
                    //        foreach (AddressXPhoneInfo objAddressXPhoneInfo in objTmpEntityXAddressesInfo.AddressXPhoneInfoList)
                    //        {
                    //            foreach (string sPhoneNumber in strSplit)
                    //            {
                    //                int iPhoneId = Conversion.ConvertStrToInteger(sPhoneNumber);
                    //                if (iPhoneId == objAddressXPhoneInfo.PhoneId)
                    //                {
                    //                    objTmpEntityXAddressesInfo.AddressXPhoneInfoList.Remove(iPhoneId);
                    //                    break;
                    //                }

                    //            }
                    //        }
                    //    }
                    //}
                    #endregion //Added code to remove the saved phone numbers
                }
                //Parijat:Mits 9610
                foreach (EntityXAddresses objEntityXAddressesInfo in Entity.EntityXAddressesList)
                {
                    if (!arrRowIds.Contains(objEntityXAddressesInfo.RowId.ToString()))//RMA-8753 nshah28
                    {
                        Entity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId);//RMA-8753 nshah28
                    }
                }
            }
            else
            {
                if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
                {
                    string sAddr1 = Entity.Addr1;
                    string sAddr2 = Entity.Addr2;
                    string sAddr3 = Entity.Addr3;// JIRA 6420 pkandhari
                    string sAddr4 = Entity.Addr4;// JIRA 6420 pkandhari
                    string sCity = Entity.City;
                    int iCountryCode = Entity.CountryCode;
                    int iStateId = Entity.StateId;
                    string sEmailAddress = Entity.EmailAddress;
                    string sFaxNumber = Entity.FaxNumber;
                    string sCounty = Entity.County;
                    string sZipCode = Entity.ZipCode;
                    //RMA-8753 nshah28(Added by ashish)
                    string sSearchString = string.Empty;
                    //RMA-8753 nshah28(Added by ashish) END
                    if (Entity.EntityXAddressesList.Count > 0)
                    {
                        foreach (EntityXAddresses objEntityXAddressesInfo in Entity.EntityXAddressesList)
                        {
                            if (objEntityXAddressesInfo.PrimaryAddress == -1)
                            {
                                //RMA-8753 nshah28(Added by ashish) START
                                //objEntityXAddressesInfo.Addr1 = sAddr1;
                                //objEntityXAddressesInfo.Addr2 = sAddr2;
                                //objEntityXAddressesInfo.Addr3 = sAddr3;// JIRA 6420 pkandhari
                                //objEntityXAddressesInfo.Addr4 = sAddr4;// JIRA 6420 pkandhari
                                //objEntityXAddressesInfo.City = sCity;
                                //objEntityXAddressesInfo.Country = iCountryCode;
                                //objEntityXAddressesInfo.State = iStateId;
                                objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                                objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                                objEntityXAddressesInfo.Address.Addr3 = sAddr3;
                                objEntityXAddressesInfo.Address.Addr4 = sAddr4;
                                objEntityXAddressesInfo.Address.City = sCity;
                                objEntityXAddressesInfo.Address.Country = iCountryCode;
                                objEntityXAddressesInfo.Address.State = iStateId;
                                objEntityXAddressesInfo.Email = sEmailAddress;
                                objEntityXAddressesInfo.Fax = sFaxNumber;
                                //objEntityXAddressesInfo.County = sCounty;
                                //objEntityXAddressesInfo.ZipCode = sZipCode;
                                objEntityXAddressesInfo.Address.County = sCounty;
                                objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                                AddressForm objAddressForm = new AddressForm(m_fda);
                                sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                                objEntityXAddressesInfo.Address.SearchString = sSearchString;
                                objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, Entity.Context.RMDatabase.ConnectionString, base.ClientId);
                                objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
                                //RMA-8753 nshah28(Added by ashish) END
                                break;
                            }

                        }
                    }
                    else
                    {
                        //RMA-8753 nshah28(Added by ashish) START
                        if (
                                sAddr1 != string.Empty || sAddr2 != string.Empty || sAddr3 != string.Empty || sAddr4 != string.Empty ||
                                sCity != string.Empty || iCountryCode != 0 || iStateId != 0 || sEmailAddress != string.Empty ||
                                sFaxNumber != string.Empty || sCounty != string.Empty || sZipCode != string.Empty
                            )
                        {
                            EntityXAddresses objEntityXAddressesInfo = Entity.EntityXAddressesList.AddNew();
                            //objEntityXAddressesInfo.Addr1 = sAddr1;
                            //objEntityXAddressesInfo.Addr2 = sAddr2;
                            //objEntityXAddressesInfo.Addr3 = sAddr3;// JIRA 6420 pkandhari
                            //objEntityXAddressesInfo.Addr4 = sAddr4;// JIRA 6420 pkandhari
                            //objEntityXAddressesInfo.City = sCity;
                            //objEntityXAddressesInfo.Country = iCountryCode;
                            //objEntityXAddressesInfo.State = iStateId;
                            objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                            objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                            objEntityXAddressesInfo.Address.Addr3 = sAddr3;
                            objEntityXAddressesInfo.Address.Addr4 = sAddr4;
                            objEntityXAddressesInfo.Address.City = sCity;
                            objEntityXAddressesInfo.Address.Country = iCountryCode;
                            objEntityXAddressesInfo.Address.State = iStateId;
                            AddressForm objAddressForm = new AddressForm(m_fda);
                            sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                            objEntityXAddressesInfo.Address.SearchString = sSearchString;
                            objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, Entity.Context.RMDatabase.ConnectionString, base.ClientId);
                            objEntityXAddressesInfo.Email = sEmailAddress;
                            objEntityXAddressesInfo.Fax = sFaxNumber;
                            //objEntityXAddressesInfo.County = sCounty;
                            //objEntityXAddressesInfo.ZipCode = sZipCode;
                            objEntityXAddressesInfo.Address.County = sCounty;
                            objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                            objEntityXAddressesInfo.EntityId = Entity.EntityId;
                            objEntityXAddressesInfo.PrimaryAddress = -1;
                            objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
                            //objEntityXAddressesInfo.AddressId = -1;
                            //RMA-8753 nshah28(Added by ashish) END
                        }
                    }
                }
            }
            #endregion

            #region Update Phone Numbers
            if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
            {
                Entity.FormName = "PeopleForm"; //Mits 22497

                XmlNode objPhoneNumber = base.SysEx.SelectSingleNode("//PhoneNumbers");
                if (objPhoneNumber != null)
                {
                    string sPhoneData = objPhoneNumber.InnerText;
                    string[] sPhoneNumbers = sPhoneData.Split(new string[] { CommonForm.OUTER_PHONE_DELIMITER }, StringSplitOptions.None);
                    foreach (string sPhoneNumber in sPhoneNumbers)
                    {
                        string[] sNumber = sPhoneNumber.Split(new string[] { CommonForm.INNER_PHONE_DELIMITER }, StringSplitOptions.None);
                        if (sNumber.Length > 2)
                        {
                            string sPhoneId = sNumber[0];
                            string sPhoneType = sNumber[1];
                            string sPhoneNo = sNumber[2];
                            bool bPhoneCodeExists = false;
                            string sPhoneDesc = string.Empty;
                            string sPhoneShortCode = string.Empty;
                            if (Entity.AddressXPhoneInfoList.Count > 0)
                            {
                                foreach (AddressXPhoneInfo objAddressXPhoneInfo in Entity.AddressXPhoneInfoList)
                                {
                                    if (objAddressXPhoneInfo.PhoneCode == Convert.ToInt32(sPhoneType))
                                    {
                                        if (sPhoneNo.Trim() != string.Empty)
                                            objAddressXPhoneInfo.PhoneNo = sPhoneNo;
                                        else
                                            Entity.AddressXPhoneInfoList.Remove(objAddressXPhoneInfo.PhoneId);

                                        bPhoneCodeExists = true;
                                        break;
                                    }

                                }
                                if (!bPhoneCodeExists && sPhoneNo.Trim() != string.Empty)
                                {
                                    AddressXPhoneInfo objAddressXPhoneInfo = Entity.AddressXPhoneInfoList.AddNew();
                                    objAddressXPhoneInfo.PhoneId = Convert.ToInt32(sPhoneId);
                                    objAddressXPhoneInfo.PhoneCode = Convert.ToInt32(sPhoneType);
                                    objAddressXPhoneInfo.PhoneNo = sPhoneNo;
                                }
                            }
                            else
                            {
                                if (sPhoneNo.Trim() != string.Empty)
                                {
                                    AddressXPhoneInfo objAddressXPhoneInfo = Entity.AddressXPhoneInfoList.AddNew();
                                    objAddressXPhoneInfo.PhoneId = Convert.ToInt32(sPhoneId);
                                    objAddressXPhoneInfo.PhoneCode = Convert.ToInt32(sPhoneType);
                                    objAddressXPhoneInfo.PhoneNo = sPhoneNo;
                                }
                            }
                            //Added to save Office and Alt phone numbers in Entity Table as well
                            //sPhoneDesc = this.objCache.GetCodeDesc(Convert.ToInt32(sPhoneType)).Trim().ToLower();
                            //{
                            //    if (sPhoneDesc == "office")
                            //        Entity.Phone1 = sPhoneNo;
                            //    else if (sPhoneDesc == "home")
                            //        Entity.Phone2 = sPhoneNo;
                            //}
                            //Added to save Office and Alt phone numbers in Entity Table as well
                            //Aman ML Change
                            sPhoneShortCode = this.objCache.GetShortCode(Convert.ToInt32(sPhoneType)).Trim().ToLower();
                            {
                                if (sPhoneShortCode == "o")
                                    Entity.Phone1 = sPhoneNo;
                                else if (sPhoneShortCode == "h")
                                    Entity.Phone2 = sPhoneNo;
                            }
                            //Aman ML Change
                        }
                    }



                }
            }

            #endregion

            //Added Rakhi for R7:Add Emp Data Elements
            //MITS:34276-- Entity ID Type Update
            #region "Updating Entity Id Type"
            SortedList objEntityIdTypeAsList = new SortedList();

            XmlNode objEntityIdTypeAsNode = base.SysEx.DocumentElement.SelectSingleNode("EntityXEntityIDType");
            objTypeAttributeNode = null;

            bIsNew = false;
            int iEntityIdTypeKey = -1;

            XmlDocument objEntityXIDTypeXmlDoc = null;
            XmlElement objEntityXIDTypeRootElement = null;

            int iEntityIdTypeSelectedId = base.GetSysExDataNodeInt("EntityXEntityIDTypeSelectedId", true);

            string sEntityIdTypeRowAddedFlag = base.GetSysExDataNodeText("EntityXEntityIDTypeGrid_RowAddedFlag", true);
            bool bEntityIdTypeRowAddedFlag = sEntityIdTypeRowAddedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

            string sEntityIdTypeRowDeletedFlag = base.GetSysExDataNodeText("EntityXEntityIDTypeGrid_RowDeletedFlag", true);
            bool bEntityIdTypeRowDeletedFlag = sEntityIdTypeRowDeletedFlag.ToUpper().CompareTo("TRUE") == 0 ? true : false;

            if (objEntityIdTypeAsNode != null)
            {
                // Loop through data for all rows of the grid
                foreach (XmlNode objOptionNode in objEntityIdTypeAsNode.SelectNodes("option"))
                {
                    objTypeAttributeNode = objOptionNode.Attributes["type"];
                    bIsNew = false;
                    if (objTypeAttributeNode != null && objTypeAttributeNode.InnerText.Equals("new") == true)
                    {
                        // This is the 'extra' hidden <option> for capturing a new grid row data
                        bIsNew = true;
                    }
                    if ((bIsNew == false) || (bIsNew == true && bEntityIdTypeRowAddedFlag == true))
                    {
                        objEntityXIDTypeXmlDoc = new XmlDocument();
                        objEntityXIDTypeRootElement = objEntityXIDTypeXmlDoc.CreateElement("EntityXEntityIDType");
                        objEntityXIDTypeXmlDoc.AppendChild(objEntityXIDTypeRootElement);
                        objEntityXIDTypeRootElement.InnerXml = objOptionNode.InnerXml;

                        iEntityIdTypeKey = Conversion.ConvertStrToInteger(objEntityXIDTypeRootElement.SelectSingleNode("IdNumRowId").InnerText);

                        if ((bIsNew == true && bEntityIdTypeRowAddedFlag == true) || (iEntityIdTypeKey < 0 && objEntityXIDTypeRootElement.SelectSingleNode("EntityId").InnerText == ""))
                            objEntityXIDTypeRootElement.SelectSingleNode("EntityId").InnerText = "0";

                        if (bIsNew == false && bEntityIdTypeRowDeletedFlag == true && iEntityIdTypeKey == iEntityIdTypeSelectedId)
                        {
                            continue;
                        }

                        objEntityIdTypeAsList.Add(iEntityIdTypeKey, objEntityXIDTypeXmlDoc);
                    }
                }
            }

            foreach (EntityXEntityIDType objEntityXEntityIDTypeAs in Entity.EntityXEntityIDTypeList)
            {
                if (base.m_fda.SafeFormVariableParamText("SysCmd") == "7" && (objEntityXEntityIDTypeAs.IdNumRowId < 0 || (bEntityIdTypeRowDeletedFlag == true && objEntityXEntityIDTypeAs.IdNumRowId == iEntityIdTypeSelectedId)))
                {
                    Entity.EntityXEntityIDTypeList.Remove(objEntityXEntityIDTypeAs.IdNumRowId);
                }
                else if (objEntityXEntityIDTypeAs.IdNumRowId < 0)
                    Entity.EntityXEntityIDTypeList.Remove(objEntityXEntityIDTypeAs.IdNumRowId);
            }


            EntityXEntityIDType objTmpEntityXEntityIDTypeAs = null;
            XmlDocument objTmpEntityXEntityIdTypeDoc = null;

            ArrayList arrEntityIdTypes = new ArrayList();

            for (int iListIndex = 0; iListIndex < objEntityIdTypeAsList.Count; iListIndex++)
            {
                objTmpEntityXEntityIdTypeDoc = (XmlDocument)objEntityIdTypeAsList.GetByIndex(iListIndex);

                if (objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText.Trim() == "" || Conversion.ConvertStrToInteger(objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText) < 0)
                {
                    objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText = ((-1 * iListIndex) - 1).ToString();
                    objTmpEntityXEntityIDTypeAs = Entity.EntityXEntityIDTypeList.AddNew();
                    objTmpEntityXEntityIDTypeAs.PopulateObject(objTmpEntityXEntityIdTypeDoc);
                }
                else
                {
                    objTmpEntityXEntityIDTypeAs = Entity.EntityXEntityIDTypeList[Conversion.ConvertStrToInteger(objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText)];

                    objTmpEntityXEntityIDTypeAs.PopulateObject(objTmpEntityXEntityIdTypeDoc);
                }

                arrEntityIdTypes.Add(objTmpEntityXEntityIdTypeDoc.DocumentElement.SelectSingleNode("IdNumRowId").InnerText);
            }


            foreach (EntityXEntityIDType objEntityXEntityIDType in Entity.EntityXEntityIDTypeList)
            {
                if (!arrEntityIdTypes.Contains(objEntityXEntityIDType.IdNumRowId.ToString()))
                {
                    Entity.EntityXEntityIDTypeList.Remove(objEntityXEntityIDType.IdNumRowId);
                }
            }

            #endregion
            //MITS:34276-- Entity ID Type Update

			if(Entity.EntityId!=0)
				return;

			//Only Apply Entity Type Change (if any requested) on a New Record.
			XmlNode objOrig = base.SysEx.SelectSingleNode("/*/EntityType");
			if(Entity.EntityId ==0 && objOrig !=null)
				Entity.EntityTableId = Conversion.ConvertStrToInteger(objOrig.InnerText);
		}

		private XmlDocument GetEntityXOperatingAsData()
		{
			XmlDocument objEntityXOperatingAsXmlDoc = new XmlDocument();
 
			XmlElement objRootElement = objEntityXOperatingAsXmlDoc.CreateElement("EntityXOperatingAs"); 
			objEntityXOperatingAsXmlDoc.AppendChild(objRootElement);
			
			XmlElement objOptionXmlElement = null;
			XmlElement objXmlElement = null;
			XmlElement objListHeadXmlElement = null;
			XmlAttribute objXmlAttribute = null;
			int iEntityXOperatingAsCount = 0;

			objListHeadXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("listhead"); 
					
			objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("Initials");
			objXmlElement.InnerText = "Abbreviation";
			objListHeadXmlElement.AppendChild(objXmlElement); 					
			objXmlElement = null;

			objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("OperatingAs");
			objXmlElement.InnerText = "Operating Name";
			objListHeadXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objRootElement.AppendChild(objListHeadXmlElement);
			objListHeadXmlElement = null;

			foreach(EntityXOperatingAs objEntityXOperatingAs in this.Entity.EntityXOperatingAsList)
			{
				
				iEntityXOperatingAsCount++;

				objOptionXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("option"); 
					
				objXmlAttribute = objEntityXOperatingAsXmlDoc.CreateAttribute("ref");  
				objXmlAttribute.InnerText =		INSTANCE_SYSEXDATA_PATH 
					+ "/" + objEntityXOperatingAsXmlDoc.DocumentElement.LocalName  
					+ "/option[" + iEntityXOperatingAsCount.ToString() + "]" ;
				objOptionXmlElement.Attributes.Append(objXmlAttribute);
				objXmlAttribute = null;

				objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("Initials");
				objXmlElement.InnerText = objEntityXOperatingAs.Initials;
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("OperatingAs");
				objXmlElement.InnerText = objEntityXOperatingAs.OperatingAs.ToString();
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				/************** Hidden Columns on the Grid ******************/
				//objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("Initial");				
				//objXmlElement.InnerText = objEntityXOperatingAs.Initials; //MITS 9079 not sure 
																			//whats the use have removed it from form as well
				//objOptionXmlElement.AppendChild(objXmlElement); 
				//objXmlElement = null;

				objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("OperatingId");
				objXmlElement.InnerText = objEntityXOperatingAs.OperatingId.ToString();
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("EntityId");
				objXmlElement.InnerText = objEntityXOperatingAs.EntityId.ToString();	
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objRootElement.AppendChild(objOptionXmlElement);
				objOptionXmlElement = null;

			}
			
			iEntityXOperatingAsCount++;

			objOptionXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("option"); 
					
			objXmlAttribute = objEntityXOperatingAsXmlDoc.CreateAttribute("ref");  
			objXmlAttribute.InnerText =		INSTANCE_SYSEXDATA_PATH 
				+ "/" + objEntityXOperatingAsXmlDoc.DocumentElement.LocalName  
				+ "/option[" + iEntityXOperatingAsCount.ToString() + "]" ;
			objOptionXmlElement.Attributes.Append(objXmlAttribute);
			objXmlAttribute = null;

			objXmlAttribute = objEntityXOperatingAsXmlDoc.CreateAttribute("type"); 
			objXmlAttribute.InnerText = "new";
			objOptionXmlElement.Attributes.Append(objXmlAttribute);
			objXmlAttribute = null;

			objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("Initials");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("OperatingAs");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			/************** Hidden Columns on the Grid ******************/
			//objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("Initial"); //9079 not sure why we use it ?
			//objOptionXmlElement.AppendChild(objXmlElement); 
			//objXmlElement = null;

			objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("OperatingId");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXOperatingAsXmlDoc.CreateElement("EntityId");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objRootElement.AppendChild(objOptionXmlElement);
			objOptionXmlElement = null;

			objRootElement = null;			
			return objEntityXOperatingAsXmlDoc;	
		}

		private XmlDocument GetEntityXContactInfoData()
		{
			XmlDocument objEntityXContactInfoXmlDoc = new XmlDocument();
 
			XmlElement objRootElement = objEntityXContactInfoXmlDoc.CreateElement("EntityXContactInfo"); 
			objEntityXContactInfoXmlDoc.AppendChild(objRootElement);
			
			XmlElement objOptionXmlElement = null;
			XmlElement objXmlElement = null;
			XmlElement objListHeadXmlElement = null;
			XmlAttribute objXmlAttribute = null;
			int iEntityXContactInfoCount = 0;
			string sStateCode = "";
			string sStateDesc = "";
            //MITS:34276 Contact Type START
            string sContactCode = String.Empty;
            string sContactDesc = String.Empty;
            //MITS:34276 Contact Type END

			objListHeadXmlElement = objEntityXContactInfoXmlDoc.CreateElement("listhead");

            //MITS:34276 Contact Type START
            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ContactType");
            objXmlElement.InnerText = "Contact type";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //MITS:34276 Contact Type END

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ContactName");
			objXmlElement.InnerText = "Name";
			objListHeadXmlElement.AppendChild(objXmlElement); 					
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Title");
			objXmlElement.InnerText = "Title";
			objListHeadXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Initials");
			objXmlElement.InnerText = "Initials";
			objListHeadXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr1");
			objXmlElement.InnerText = "Address1";
			objListHeadXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr2");
			objXmlElement.InnerText = "Address2";
			objListHeadXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
            objXmlElement.InnerText = "Address3";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
            objXmlElement.InnerText = "Address4";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("City");
			objXmlElement.InnerText = "City";
			objListHeadXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("State");
			objXmlElement.InnerText = "State";
			objListHeadXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ZipCode");
			objXmlElement.InnerText = "Zip Code";
			objListHeadXmlElement.AppendChild(objXmlElement); 

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Phone");
			objXmlElement.InnerText = "Phone";
			objListHeadXmlElement.AppendChild(objXmlElement);

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Fax");
			objXmlElement.InnerText = "Fax No.";
			objListHeadXmlElement.AppendChild(objXmlElement);

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Email");
			objXmlElement.InnerText = "E-Mail";
			objListHeadXmlElement.AppendChild(objXmlElement);

			objXmlElement = null;
			objRootElement.AppendChild(objListHeadXmlElement);
			objListHeadXmlElement = null;

			foreach(EntityXContactInfo objEntityXContactInfo in this.Entity.EntityXContactInfoList)
			{
				
				iEntityXContactInfoCount++;

				objOptionXmlElement = objEntityXContactInfoXmlDoc.CreateElement("option"); 
					
				objXmlAttribute = objEntityXContactInfoXmlDoc.CreateAttribute("ref");  
				objXmlAttribute.InnerText =		INSTANCE_SYSEXDATA_PATH 
					+ "/" + objEntityXContactInfoXmlDoc.DocumentElement.LocalName  
					+ "/option[" + iEntityXContactInfoCount.ToString() + "]" ;
				objOptionXmlElement.Attributes.Append(objXmlAttribute);
				objXmlAttribute = null;

                //MITS:34276 Contact Type START
                objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ContactType");
                this.objCache.GetCodeInfo(objEntityXContactInfo.ContactType, ref sContactCode, ref sContactDesc);
                objXmlElement.InnerText = sContactCode + " " + sContactDesc;
                objXmlAttribute = objEntityXContactInfoXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXContactInfo.ContactType.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;
                //MITS:34276 Contact Type END

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ContactName");
				objXmlElement.InnerText = objEntityXContactInfo.ContactName;
				objOptionXmlElement.AppendChild(objXmlElement); 					
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Title");
				objXmlElement.InnerText = objEntityXContactInfo.Title;
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Initials");
				objXmlElement.InnerText = objEntityXContactInfo.Initials;
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr1");
				objXmlElement.InnerText = objEntityXContactInfo.Addr1;
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr2");
				objXmlElement.InnerText = objEntityXContactInfo.Addr2;
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

                objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
                objXmlElement.InnerText = objEntityXContactInfo.Addr3;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
                objXmlElement.InnerText = objEntityXContactInfo.Addr4;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("City");
				objXmlElement.InnerText = objEntityXContactInfo.City;
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;
				
				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("State");
				this.objCache.GetStateInfo(objEntityXContactInfo.State,ref sStateCode, ref sStateDesc);
				objXmlElement.InnerText = sStateCode + " " + sStateDesc;
				objXmlAttribute = objEntityXContactInfoXmlDoc.CreateAttribute("codeid");  
				objXmlAttribute.InnerText =	objEntityXContactInfo.State.ToString();	
				objXmlElement.Attributes.Append(objXmlAttribute);					
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlAttribute = null;
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ZipCode");
				objXmlElement.InnerText = objEntityXContactInfo.ZipCode;
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Phone");
				objXmlElement.InnerText = objEntityXContactInfo.Phone;
				objOptionXmlElement.AppendChild(objXmlElement);
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Fax");
				objXmlElement.InnerText = objEntityXContactInfo.Fax;
				objOptionXmlElement.AppendChild(objXmlElement);
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Email");
				objXmlElement.InnerText = objEntityXContactInfo.Email;
				objOptionXmlElement.AppendChild(objXmlElement);
				objXmlElement = null;

				/************** Hidden Columns on the Grid ******************/
				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ContactId");
				objXmlElement.InnerText = objEntityXContactInfo.ContactId.ToString();
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("EntityId");
				objXmlElement.InnerText = objEntityXContactInfo.EntityId.ToString();	
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;

				objRootElement.AppendChild(objOptionXmlElement);
				objOptionXmlElement = null;

			}
			
			iEntityXContactInfoCount++;

			objOptionXmlElement = objEntityXContactInfoXmlDoc.CreateElement("option"); 
					
			objXmlAttribute = objEntityXContactInfoXmlDoc.CreateAttribute("ref");  
			objXmlAttribute.InnerText =		INSTANCE_SYSEXDATA_PATH 
				+ "/" + objEntityXContactInfoXmlDoc.DocumentElement.LocalName  
				+ "/option[" + iEntityXContactInfoCount.ToString() + "]" ;
			objOptionXmlElement.Attributes.Append(objXmlAttribute);
			objXmlAttribute = null;

			objXmlAttribute = objEntityXContactInfoXmlDoc.CreateAttribute("type"); 
			objXmlAttribute.InnerText = "new";
			objOptionXmlElement.Attributes.Append(objXmlAttribute);
			objXmlAttribute = null;

            //MITS:34276 Contact Type START
            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ContactType");
            objXmlAttribute = objEntityXContactInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;
            //MITS:34276 Contact Type END

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ContactName");
			objOptionXmlElement.AppendChild(objXmlElement); 					
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Title");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Initials");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr1");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr2");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("City");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("State");
			objXmlAttribute = objEntityXContactInfoXmlDoc.CreateAttribute("codeid");  
			objXmlElement.Attributes.Append(objXmlAttribute);					
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlAttribute = null;
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ZipCode");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Phone");
			objOptionXmlElement.AppendChild(objXmlElement);
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Fax");
			objOptionXmlElement.AppendChild(objXmlElement);
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("Email");
			objOptionXmlElement.AppendChild(objXmlElement);
			objXmlElement = null;

			/************** Hidden Columns on the Grid ******************/
			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("ContactId");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objXmlElement = objEntityXContactInfoXmlDoc.CreateElement("EntityId");
			objOptionXmlElement.AppendChild(objXmlElement); 
			objXmlElement = null;

			objRootElement.AppendChild(objOptionXmlElement);
			objOptionXmlElement = null;

			objRootElement = null;			
			return objEntityXContactInfoXmlDoc;	
		}
        private XmlDocument GetEntityXAddressInfo()
        {
            XmlElement objOptionXmlElement = null;
            XmlElement objXmlElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlAttribute objXmlAttribute = null;
            int iEntityXAddressInfoCount = 0;
            string sStateCode = string.Empty;
            string sStateDesc = string.Empty;
            string sCountryCode = string.Empty;
            string sCountryDesc = string.Empty;
            string sPrimaryAddressRef = string.Empty;
            string p_ShortCode = String.Empty; //MITS:34276 Entity Address Type
            string p_sDesc = String.Empty;  //MITS:34276 Entity Address Type

            XmlDocument objEntityXAddressInfoXmlDoc = new XmlDocument();
            XmlElement objRootElement = objEntityXAddressInfoXmlDoc.CreateElement("EntityXAddressesInfo");
            objEntityXAddressInfoXmlDoc.AppendChild(objRootElement);

            objListHeadXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("listhead");

            //MITS:34276 Entity Address Type START
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressType");
            objXmlElement.InnerText = "Address Type";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //MITS:34276 Entity Address Type END


            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr1");
            objXmlElement.InnerText = "Address1";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr2");
            objXmlElement.InnerText = "Address2";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
            objXmlElement.InnerText = "Address3";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
            objXmlElement.InnerText = "Address4";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("City");
            objXmlElement.InnerText = "City";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("State");
            objXmlElement.InnerText = "State";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Country");
            objXmlElement.InnerText = "Country";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("County");
            objXmlElement.InnerText = "County";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ZipCode");
            objXmlElement.InnerText = "Zip Code";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Email");
            objXmlElement.InnerText = "E-Mail";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Fax");
            objXmlElement.InnerText = "Fax No.";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PrimaryAddress");
            objXmlElement.InnerText = "Primary Address";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            //JIRA:6865 START:
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EffectiveDate");
            objXmlElement.InnerText = "Effective Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ExpirationDate");
            objXmlElement.InnerText = "Expiration Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //JIRA:6865 END:

            objRootElement.AppendChild(objListHeadXmlElement);
            objListHeadXmlElement = null;


            foreach (EntityXAddresses objEntityXAddressInfo in this.Entity.EntityXAddressesList)
            {

                iEntityXAddressInfoCount++;

                objOptionXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("option");

                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("ref");
                objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                    + "/" + objEntityXAddressInfoXmlDoc.DocumentElement.LocalName
                    + "/option[" + iEntityXAddressInfoCount.ToString() + "]";
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                sPrimaryAddressRef = objXmlAttribute.InnerText;
                objXmlAttribute = null;

                //MITS:34276 Entity Address Type START
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressType");
                this.objCache.GetCodeInfo(objEntityXAddressInfo.AddressType, ref p_ShortCode, ref p_sDesc);
                objXmlElement.InnerText = p_ShortCode + " " + p_sDesc;
                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXAddressInfo.AddressType.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;
                ////MITS:34276 Entity Address Type END

				//RMA-8753 nshah28(Added by ashish) START
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr1");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr1;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr2");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr2;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr3;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
                objXmlElement.InnerText = objEntityXAddressInfo.Address.Addr4;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("City");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.City;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("State");
                this.objCache.GetStateInfo(objEntityXAddressInfo.Address.State, ref sStateCode, ref sStateDesc);
                objXmlElement.InnerText = sStateCode + " " + sStateDesc;
                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXAddressInfo.Address.State.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Country");
                this.objCache.GetCodeInfo(objEntityXAddressInfo.Address.Country, ref sCountryCode, ref sCountryDesc, base.Adaptor.userLogin.objUser.NlsCode);
                objXmlElement.InnerText = sCountryCode + " " + sCountryDesc;
                objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXAddressInfo.Address.Country.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("County");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.County;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;


                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ZipCode");
                objXmlElement.InnerText = objEntityXAddressInfo.Address.ZipCode;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
				//RMA-8753 nshah28(Added by ashish) END

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Email");
                objXmlElement.InnerText = objEntityXAddressInfo.Email;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Fax");
                objXmlElement.InnerText = objEntityXAddressInfo.Fax;
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PrimaryAddress");
                //int iPrimaryAddress = objEntityXAddressInfo.PrimaryAddress;
                int iPrimaryAddress = Convert.ToInt32(objEntityXAddressInfo.PrimaryAddress); //RMA-8753 nshah28
                if (iPrimaryAddress == -1)
                    objXmlElement.InnerText = "True";
                else
                    objXmlElement.InnerText = "False";
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                //JIRA:6865 START:
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EffectiveDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXAddressInfo.EffectiveDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ExpirationDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXAddressInfo.ExpirationDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                //JIRA:6865 END:

                /************** Hidden Columns on the Grid ******************/
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("NewPrimaryRecord");//for identication of changed Primary Address
                objXmlElement.InnerText = "False";
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressId");
                objXmlElement.InnerText = objEntityXAddressInfo.AddressId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                //AA//AA //RMA-8753 nshah28
                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("RowId");
                objXmlElement.InnerText = objEntityXAddressInfo.RowId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                //AA //RMA-8753 nshah28 END

                objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EntityId");
                objXmlElement.InnerText = objEntityXAddressInfo.EntityId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;


                //XmlDocument objAddressXPhoneInfoXmlDoc = GetAddressXPhoneInfo(objEntityXAddressInfo);
                //objOptionXmlElement.AppendChild(objEntityXAddressInfoXmlDoc.ImportNode(objAddressXPhoneInfoXmlDoc.SelectSingleNode("AddressXPhoneInfo"), true));

                objRootElement.AppendChild(objOptionXmlElement);
                objOptionXmlElement = null;


            }

            iEntityXAddressInfoCount++;

            objOptionXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("option");

            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("ref");
            objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                + "/" + objEntityXAddressInfoXmlDoc.DocumentElement.LocalName
                + "/option[" + iEntityXAddressInfoCount.ToString() + "]";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("type");
            objXmlAttribute.InnerText = "new";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            //MITS:34276 Entity Address Type START 
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressType");
            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;
            //MITS:34276 Entity Address Type END

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr1");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr2");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr3");// JIRA 6420 pkandhari
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Addr4");// JIRA 6420 pkandhari
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("City");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("State");
            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Country");
            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("County");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ZipCode");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Email");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("Fax");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PrimaryAddress");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            //JIRA:6865 START:
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EffectiveDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ExpirationDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //JIRA:6865 END:

            /************** Hidden Columns on the Grid ******************/
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("NewPrimaryRecord"); //for identication of changed Primary Address
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

             //AA //RMA-8753 nshah28
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("RowId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;
            //AA //RMA-8753 nshah28 END
            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("EntityId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            #region PhoneInfo

            //            XmlElement objInnerXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("AddressXPhoneInfo");
//            objOptionXmlElement.AppendChild(objInnerXmlElement);

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("listhead");
//            string sNode = @"<PhoneCode>Phone Code</PhoneCode>
//                             <PhoneNo>Phone Number</PhoneNo>";
//            objXmlElement.InnerXml = sNode;
//            objInnerXmlElement.AppendChild(objXmlElement);

//            XmlElement objInnerOptionXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("option");

//            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("ref");
//            objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
//                + "/AddressXPhoneInfo" + "/option[" + iEntityXAddressInfoCount.ToString() + "]";
//            objInnerOptionXmlElement.Attributes.Append(objXmlAttribute);
//            objXmlAttribute = null;

//            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("type");
//            objXmlAttribute.InnerText = "new";
//            objInnerOptionXmlElement.Attributes.Append(objXmlAttribute);
//            objXmlAttribute = null;

//            objInnerXmlElement.AppendChild(objInnerOptionXmlElement);

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PhoneCode");
//            objXmlAttribute = objEntityXAddressInfoXmlDoc.CreateAttribute("codeid");
//            objXmlAttribute.InnerText = "-1";
//            objXmlElement.Attributes.Append(objXmlAttribute);
//            objInnerOptionXmlElement.AppendChild(objXmlElement);
//            objXmlAttribute = null;
//            objXmlElement = null;

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PhoneNo");
//            objInnerOptionXmlElement.AppendChild(objXmlElement);
//            objXmlElement = null;

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("ContactId");
//            objInnerOptionXmlElement.AppendChild(objXmlElement);
//            objXmlElement = null;

//            objXmlElement = objEntityXAddressInfoXmlDoc.CreateElement("PhoneId");
//            objInnerOptionXmlElement.AppendChild(objXmlElement);
            //            objXmlElement = null;

            #endregion//PhoneInfo

            objRootElement.AppendChild(objOptionXmlElement);
            objOptionXmlElement = null;

            objRootElement = null;
            return objEntityXAddressInfoXmlDoc;
        }

        #region GetAddressXPhoneInfo
        //private XmlDocument GetAddressXPhoneInfo(EntityXAddresses objEntityXAddresses)
        //{
        //    XmlDocument objAddressXPhoneInfoXmlDoc = new XmlDocument();

        //    XmlElement objRootElement = objAddressXPhoneInfoXmlDoc.CreateElement("AddressXPhoneInfo");
        //    objAddressXPhoneInfoXmlDoc.AppendChild(objRootElement);

        //    XmlElement objOptionXmlElement = null;
        //    XmlElement objXmlElement = null;
        //    XmlElement objListHeadXmlElement = null;
        //    XmlAttribute objXmlAttribute = null;
        //    int iAddressXPhoneInfoCount = 0;
        //    string sPhoneCode = "";
        //    string sPhoneDesc = "";



        //    objListHeadXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("listhead");


        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneCode");
        //    objXmlElement.InnerText = "Phone Code";
        //    objListHeadXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneNo");
        //    objXmlElement.InnerText = "Phone Number";
        //    objListHeadXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objRootElement.AppendChild(objListHeadXmlElement);
        //    objListHeadXmlElement = null;


        //    foreach (AddressXPhoneInfo objAddressXPhoneInfo in objEntityXAddresses.AddressXPhoneInfoList)
        //    {

        //        iAddressXPhoneInfoCount++;

        //        objOptionXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("option");

        //        objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("ref");
        //        objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
        //            + "/" + objAddressXPhoneInfoXmlDoc.DocumentElement.LocalName
        //            + "/option[" + iAddressXPhoneInfoCount.ToString() + "]";
        //        objOptionXmlElement.Attributes.Append(objXmlAttribute);
        //        objXmlAttribute = null;



        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneCode");
        //        this.objCache.GetCodeInfo(objAddressXPhoneInfo.PhoneCode, ref sPhoneCode, ref sPhoneDesc);
        //        objXmlElement.InnerText = sPhoneCode + " " + sPhoneDesc;
        //        objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("codeid");
        //        objXmlAttribute.InnerText = objAddressXPhoneInfo.PhoneCode.ToString();
        //        objXmlElement.Attributes.Append(objXmlAttribute);
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlAttribute = null;
        //        objXmlElement = null;


        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneNo");
        //        objXmlElement.InnerText = objAddressXPhoneInfo.PhoneNo;
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlElement = null;


        //        /************** Hidden Columns on the Grid ******************/
        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("ContactId");
        //        objXmlElement.InnerText = objAddressXPhoneInfo.ContactId.ToString();
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlElement = null;

        //        objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneId");
        //        objXmlElement.InnerText = objAddressXPhoneInfo.PhoneId.ToString();
        //        objOptionXmlElement.AppendChild(objXmlElement);
        //        objXmlElement = null;

        //        objRootElement.AppendChild(objOptionXmlElement);
        //        objOptionXmlElement = null;

        //    }

        //    iAddressXPhoneInfoCount++;

        //    objOptionXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("option");

        //    objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("ref");
        //    objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
        //        + "/" + objAddressXPhoneInfoXmlDoc.DocumentElement.LocalName
        //        + "/option[" + iAddressXPhoneInfoCount.ToString() + "]";
        //    objOptionXmlElement.Attributes.Append(objXmlAttribute);
        //    objXmlAttribute = null;

        //    objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("type");
        //    objXmlAttribute.InnerText = "new";
        //    objOptionXmlElement.Attributes.Append(objXmlAttribute);
        //    objXmlAttribute = null;


        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneCode");
        //    objXmlAttribute = objAddressXPhoneInfoXmlDoc.CreateAttribute("codeid");
        //    objXmlAttribute.InnerText = "-1";
        //    objXmlElement.Attributes.Append(objXmlAttribute);
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlAttribute = null;
        //    objXmlElement = null;


        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneNo");
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    /************** Hidden Columns on the Grid ******************/
        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("ContactId");
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objXmlElement = objAddressXPhoneInfoXmlDoc.CreateElement("PhoneId");
        //    objOptionXmlElement.AppendChild(objXmlElement);
        //    objXmlElement = null;

        //    objRootElement.AppendChild(objOptionXmlElement);
        //    objOptionXmlElement = null;

        //    objRootElement = null;
        //    return objAddressXPhoneInfoXmlDoc;
        //}

        #endregion //GetAddressXPhoneInfo

        //MITS:34276 Starts-- Entity ID Type tab display
        private XmlDocument GetEntityIdTypeInfoData()
        {
            XmlDocument objEntityIDTypeAsXmlDoc = new XmlDocument();

            XmlElement objRootElement = objEntityIDTypeAsXmlDoc.CreateElement("EntityXEntityIDType");
            //MITS #34276 : Entity ID Type Permission starts
            XmlAttribute objXmlPermissionAttribute = null;
            if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_VIEW))
            {

                objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsViewable");
                objXmlPermissionAttribute.InnerText = "false";
                objRootElement.Attributes.Append(objXmlPermissionAttribute);
                objXmlPermissionAttribute = null;
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);
            }
            else
            {
                if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_NEW))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsNew");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);

                if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_EDIT))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsUpdate");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);

                if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_IDTYPE_DELETE))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsDelete");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);
                if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_NEW))
                {
                    objXmlPermissionAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorNew");
                    objXmlPermissionAttribute.InnerText = "false";
                    objRootElement.Attributes.Append(objXmlPermissionAttribute);
                    objXmlPermissionAttribute = null;
                }
                objEntityIDTypeAsXmlDoc.AppendChild(objRootElement);
            }
            //MITS #34276  Entity ID Type Permission Ends 

            XmlElement objOptionXmlElement = null;
            XmlElement objXmlElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlAttribute objXmlAttribute = null;
            int iEntityIDTypeAsCount = 0;
            string sIDCode = string.Empty;
            string sIDDesc = string.Empty;

            objListHeadXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("listhead");

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdType");
            objXmlElement.InnerText = "Entity ID Type";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNum");
            objXmlElement.InnerText = "Entity ID Number";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffStartDate");
            objXmlElement.InnerText = "Effective Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffEndDate");
            objXmlElement.InnerText = "Expiration Date";
            objListHeadXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objRootElement.AppendChild(objListHeadXmlElement);
            objListHeadXmlElement = null;

            foreach (EntityXEntityIDType objEntityXEntityIDType in this.Entity.EntityXEntityIDTypeList)
            {
                iEntityIDTypeAsCount++;

                objOptionXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("option");

                objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("ref");
                objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH + "/" + objEntityIDTypeAsXmlDoc.DocumentElement.LocalName + "/option[" + iEntityIDTypeAsCount.ToString() + "]";
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                objXmlAttribute = null;

                //achouhan3 05-may-2013    MITS #34276  ID Type Vendor Permission Starts 
                this.objCache.GetCodeInfo(objEntityXEntityIDType.IdType, ref sIDCode, ref sIDDesc);
               
                if (sIDCode.Trim().ToUpper() == "VENDOR")
                {
                    if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_VIEW))
                    {
                        objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorView");
                        objXmlAttribute.InnerText = "false";
                        objOptionXmlElement.Attributes.Append(objXmlAttribute);
                        objXmlAttribute = null;
                    }
                    
                    if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_UPDATE))
                    {
                        objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorUpdate");
                        objXmlAttribute.InnerText = "false";
                        objOptionXmlElement.Attributes.Append(objXmlAttribute);
                        objXmlAttribute = null;
                    }
                    if (!Entity.Context.RMUser.IsAllowedEx(FormBase.RMO_ENT_VENDORTYPE_DELETE))
                    {
                        objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("IsVendorDelete");
                        objXmlAttribute.InnerText = "false";
                        objOptionXmlElement.Attributes.Append(objXmlAttribute);
                        objXmlAttribute = null;
                    }
                }
                //achouhan3 05-may-2013    MITS #34276  ID Type Vendor Permission ends

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdType");
                this.objCache.GetCodeInfo(objEntityXEntityIDType.IdType, ref sIDCode, ref sIDDesc);
                objXmlElement.InnerText = sIDCode + " " + sIDDesc;
                objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = objEntityXEntityIDType.IdType.ToString();
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNum");
                objXmlElement.InnerText = objEntityXEntityIDType.IdNum.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffStartDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXEntityIDType.EffStartDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffEndDate");
                objXmlElement.InnerText = Conversion.GetDBDateFormat(objEntityXEntityIDType.EffEndDate, "MM/dd/yyyy");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                /************** Hidden Columns on the Grid ******************/
                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNumRowId");
                objXmlElement.InnerText = objEntityXEntityIDType.IdNumRowId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EntityId");
                objXmlElement.InnerText = objEntityXEntityIDType.EntityId.ToString();
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objRootElement.AppendChild(objOptionXmlElement);
                objOptionXmlElement = null;

            }

            iEntityIDTypeAsCount++;

            objOptionXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("option");

            objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("ref");
            objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                + "/" + objEntityIDTypeAsXmlDoc.DocumentElement.LocalName
                + "/option[" + iEntityIDTypeAsCount.ToString() + "]";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("type");
            objXmlAttribute.InnerText = "new";
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objXmlAttribute = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdType");
            objXmlAttribute = objEntityIDTypeAsXmlDoc.CreateAttribute("codeid");
            objXmlAttribute.InnerText = "-1";
            objXmlElement.Attributes.Append(objXmlAttribute);
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlAttribute = null;
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNum");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffStartDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EffEndDate");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;


            /************** Hidden Columns on the Grid ******************/
            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("IdNumRowId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objXmlElement = objEntityIDTypeAsXmlDoc.CreateElement("EntityId");
            objOptionXmlElement.AppendChild(objXmlElement);
            objXmlElement = null;

            objRootElement.AppendChild(objOptionXmlElement);
            objOptionXmlElement = null;

            objRootElement = null;
            return objEntityIDTypeAsXmlDoc;

        }
        //MITS:34276 Ends

        public override void OnValidate(ref bool Cancel)
        {
            bool bMulAddresses = this.objData.Context.InternalSettings.SysSettings.UseMultipleAddresses;
            bool bError = false;  //mbahl3 mits 30224
            if (bMulAddresses)
            {
                XmlNode PrimaryAddress = base.SysEx.DocumentElement.SelectSingleNode("PrimaryFlag");
                if (PrimaryAddress != null && Entity.EntityXAddressesList.Count != 0 && PrimaryAddress.InnerText.ToLower() == "false")
                {
                    Errors.Add("PrimaryAddressError", "No Address Marked as Primary for this Entity", BusinessAdaptorErrorType.Error);
                    Cancel = true;
                    return; //mbahl3 mits 30224
                }
            }
            //mbahl3 mits 30224


            if (!string.IsNullOrEmpty(Entity.EffectiveDate) && !string.IsNullOrEmpty(Entity.ExpirationDate))
            {

                if (Entity.ExpirationDate.CompareTo(Entity.EffectiveDate) < 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        Globalization.GetString("Validation.ExpirationDateMustBeGreaterThanEffectiveDate", base.ClientId),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                    Cancel = true;
                    return;
                }

            }	 
            //MITS 34276 Starts:Check for Unique Entity ID Number and Entity ID Type Combination
            List<EntityXEntityIDType> lstEntityXIDType = new List<EntityXEntityIDType>();
            bool bDupEntityIDType = false;
            foreach (EntityXEntityIDType objEntityIDType in Entity.EntityXEntityIDTypeList)
            {
                if (!bDupEntityIDType && Riskmaster.Common.CommonFunctions.IsEntityIDNumberExists(Entity.Context.RMDatabase.ConnectionString, objEntityIDType.IdNumRowId, objEntityIDType.Table, objEntityIDType.IdNum, objEntityIDType.IdType))
                {
                    Errors.Add("EntityIDNumberError", "Entity ID Number with Entity ID Type already exists",
                        BusinessAdaptorErrorType.Error);
                    Cancel = true;
                }
                if (!bDupEntityIDType && lstEntityXIDType.Exists(x => x.IdType == objEntityIDType.IdType && x.IdNum == objEntityIDType.IdNum))
                {
                    Errors.Add("EntityIDNumberError", "Entity ID Number with Entity ID Type already exists",
                         BusinessAdaptorErrorType.Error);
                    Cancel = true;
                    bDupEntityIDType = true;
                }
                else
                    lstEntityXIDType.Add(objEntityIDType);
            }
            //MITS 34276 Ends:Check for Unique Entity ID Number and Entity ID Type Combination
        }           
             //mbahl3 mits 30224

        
        private void SetPrimaryAddressReadOnly()
        {

            base.AddReadOnlyNode("addr1");
            base.AddReadOnlyNode("addr2");
            base.AddReadOnlyNode("addr3");// JIRA 6420 pkandhari
            base.AddReadOnlyNode("addr4");// JIRA 6420 pkandhari
            base.AddReadOnlyNode("city");
            base.AddReadOnlyNode("countrycode");
            base.AddReadOnlyNode("county");
            base.AddReadOnlyNode("stateid");
            base.AddReadOnlyNode("emailaddress");
            base.AddReadOnlyNode("faxnumber");
            base.AddReadOnlyNode("zipcode");

        }
        private string PopulatePhoneNumbers()
        {

            XmlDocument SysExDoc = new XmlDocument();
            string sNumber = string.Empty;
            foreach (AddressXPhoneInfo objPhoneInfo in Entity.AddressXPhoneInfoList)
            {
                //Added to set Default types to office and home
                if (objPhoneInfo.PhoneCode == iOfficeType)
                {
                    sOfficePhone = objPhoneInfo.PhoneNo;

                }
                else if (objPhoneInfo.PhoneCode == iHomeType)
                {
                    sHomePhone = objPhoneInfo.PhoneNo;
                }
                //Added to set Default types to office and home
                if (String.IsNullOrEmpty(sNumber))
                {
                    sNumber = objPhoneInfo.PhoneId + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneCode + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneNo;
                }
                else
                {
                    sNumber += CommonForm.OUTER_PHONE_DELIMITER + objPhoneInfo.PhoneId + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneCode + CommonForm.INNER_PHONE_DELIMITER + objPhoneInfo.PhoneNo;
                }
                XmlNode objNode = base.SysEx.SelectSingleNode("//PhoneTypeList");
                if (objNode != null)
                {
                    foreach (XmlNode node in objNode.ChildNodes)
                    {
                        if (objPhoneInfo.PhoneCode.ToString() == node.Attributes["value"].Value)
                        {
                            node.InnerText += " " + CommonForm.MARK_PHONECODE;
                            break;
                        }
                    }
                }

            }

            return sNumber;
        }
	}
}
