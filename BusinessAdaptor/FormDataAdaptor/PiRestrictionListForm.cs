using System;
using Riskmaster.DataModel;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.BusinessAdaptor
{
	public class PiRestrictionListForm: ListFormBase
	{
		const string CLASS_NAME = "PiXRestrictList";
        private string m_LINE_OF_BUS_SHORT_CODE = string.Empty; //-----sgupta320 Jira=17835

		public override void Init()
		{
			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlDocument objXML = base.SysEx;
			XmlNode objPIRowId=null;
			try{objPIRowId = objXML.SelectSingleNode("/SysExData/PiRowId");}
			catch{};

            //-----sgupta320 Jira=17835
            this.m_LINE_OF_BUS_SHORT_CODE =
                CommonFunctions.GetLOBCode(objPIRowId.InnerText.ToString(), this.objCache.DbConnectionString);

            switch ((this.m_LINE_OF_BUS_SHORT_CODE).ToUpper())
            {
                case "GC":
                    m_SecurityId = RMO_GC_PERSONINVOLVED_Restrictions;
                    break;
                case "DI":
                    m_SecurityId = RMO_DI_PERSONINVOLVED_Restrictions;
                    break;
                case "PC":
                    m_SecurityId = RMO_PC_PERSONINVOLVED_Restrictions;
                    break;
                case "WC":
                    m_SecurityId = RMO_WC_PERSONINVOLVED_Restrictions;
                    break;
                case "VA":
                    m_SecurityId = RMO_VA_PERSONINVOLVED_Restrictions;
                    break;
                case "POLICY":
                    m_SecurityId = RMO_POLICY_PERSONINVOLVED_Restrictions;
                    break;
            }
            //----end sgupta320 Jira=17835
			
			//Filter by this CasemgtRowId if Present.
			if(objPIRowId !=null)
			{
				objDataList.BulkLoadFlag = true;
				objDataList.SQLFilter = "PI_ROW_ID=" + objPIRowId.InnerText;
			}
			else
                throw new Riskmaster.ExceptionTypes.CollectionException(String.Format(Globalization.GetString("ListForm.MissingExpectedFilterKey", base.ClientId), "ClaimId"));
			OnUpdateForm();
		}


		public PiRestrictionListForm(FormDataAdaptor fda):base(fda)
		{
			this.objDataList = fda.Factory.GetDataModelObject(CLASS_NAME,false) as DataCollection;
		}
		private LocalCache objCache{get{return objDataList.Context.LocalCache;}}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm() //Only called from base classes on Refresh()caused by PostBack.
		{
			
			base.OnUpdateForm ();
			

			XmlDocument objXML = base.SysEx;
			XmlNode objNew;
			XmlNode objOld ;
            int iEventId = 0;
			
			objNew = objXML.CreateElement("Screen");
			objNew.Attributes.Append(objXML.CreateAttribute("type"));
			objNew.Attributes["type"].Value="";

			objOld = objXML.SelectSingleNode("//Screen");
			if(objOld !=null)
				;
				//Let it Ride from the Client... 
				//Will be acted on in the Form XSL
				//No Need to clear since next page request should hit a new
				//form altogether anyhow.
				//objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);


			//Clear any existing tablename nodes.
			foreach(XmlNode nd in objXML.SelectNodes("//EntityTableName"))
				nd.ParentNode.RemoveChild(nd);
            //ijha:24978: OSHA 300 rules
            if (objXML.SelectSingleNode("/SysExData/EventId") == null)
            {
                XmlNode objPIRowId = null;
                try { objPIRowId = objXML.SelectSingleNode("/SysExData/PiRowId"); }
                catch { };
                objNew = objXML.CreateElement("EventId");
                iEventId = objDataList.Context.DbConn.ExecuteInt("SELECT DISTINCT EVENT_ID FROM PERSON_INVOLVED WHERE PI_ROW_ID=" + objPIRowId.InnerText);
                objNew.InnerText = iEventId.ToString();
                objXML.DocumentElement.AppendChild(objNew);
            }
            else
            {
                iEventId = Convert.ToInt32(objXML.SelectSingleNode("/SysExData/EventId").InnerText);
            }
            //rsushilaggar MITS 37986
            Event objEvent = (Event)objDataList.Context.Factory.GetDataModelObject("Event", false);
            objEvent.MoveTo(iEventId);
            base.ResetSysExData("DateOfEvent",objEvent.DateOfEvent);

            //----sgupta320: Jira-17835
            if (base.FormVariables.SelectSingleNode("//SysSid") != null)
                base.FormVariables.SelectSingleNode("//SysSid").InnerText = Convert.ToString(m_SecurityId);
		}
	}
}
