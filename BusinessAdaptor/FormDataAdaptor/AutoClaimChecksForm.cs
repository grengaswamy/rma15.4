﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using Riskmaster.Settings;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Application.FundManagement;
using System.Data;
using System.Collections.Generic;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Manages data for Auto Checks form.
	/// </summary>
	public class AutoClaimChecksForm : DataEntryFormBase
    {
        //Start by Shivendu for MITS 7293
        private const int RMB_FUNDS_AUTOCHK = 10400;
        private const int RMB_ALLOW_FREEZE_AUTOCHK_BATCH = 33;
        //private const int RMB_FUNDS_TRANSACT = 9650; //asharma326 MITS 31267
        private const int RMO_CHANGE_PAYEE = 38;
		private const int RMO_EDIT_PAYEE = 31;
        private const int RMO_EDIT_MAILTO = 9690;//AA 7199
        //End by Shivendu for MITS 7293
        //skhare7  R8 combined Payment
        private const int RMO_ALLOW_EDIT_PAYTOTHEOREDEROF = 34;
        private const int RMO_LOOKUP_TAX_ID = 36; //MITS 32715 mcapps2 
        private const int RMO_ALLOW_EDIT_GENERATEPAYTOTHEOREDEROF = 37;   //zmohammad MITs 34390 
        //Start -  Added by Nikhil. SMS Settings for deductible procesing
        private const int DED_PROC_SHOW = 9698;
        //End - Added by Nikhil. SMS Settings for deductible procesing

        #region mits 10527
        private class ThirdPartyPaymentComparer : IComparer
        {
            /// <summary>
            /// tkr Mits 10527 use when creating SortedList of Third Party Payments.
            /// Sort by payment number, then by autotransid.  
            /// SortedList key is comma-delimited string "[paymnumber],[autotransid]"
            /// </summary>
            /// <param name="x">[paynumber],[autotransid]</param>
            /// <param name="y">[paynumber],[autotransid]</param>
            /// <returns></returns>
            int IComparer.Compare(Object x, Object y)
            {
                string[] arr = x.ToString().Split(',');
                int xPayNumber = int.Parse(arr[0]);
                int xTransId = Math.Abs(int.Parse(arr[1]));

                arr = y.ToString().Split(',');
                int yPayNumber = int.Parse(arr[0]);
                int yTransId = Math.Abs(int.Parse(arr[1]));

                if (xPayNumber > yPayNumber)
                    return 1;
                else if (xPayNumber < yPayNumber)
                    return -1;
                else //(xPayNumber == yPayNumber)
                {
                    if (xTransId > yTransId)
                        return 1;
                    else if (xTransId < yTransId)
                        return -1;
                    else
                        return 0;
                }
            }
        }

        /// <summary>
        /// tkr tkr MITS 10527 must sort on pay number, then autotransid.
        /// if simply concantenate the two then an id starting with 10 sorts above id starting with 2-9,
        /// this breaks FirstPayment() function.
        /// </summary>
        /// <returns></returns>
        private SortedList GetThirdPartyPaymentsSortedList()
        {
            SortedList sl = new SortedList(new ThirdPartyPaymentComparer());
            return sl;
        }
        #endregion

        #region Constants
        const string CLASS_NAME = "FundsAutoBatch";
		const string INSTANCE_SYSEXDATA_PATH = "/Instance/UI/FormVariables/SysExData";
		#endregion 

		#region Variables 
		private int iNextAutoSplitIdKey = -999;
        private int m_iDefaultDistributionType = 0;
		private FundManager m_objFundManager = null ;
		private string m_sOldSelectedPayeeTypeCode = string.Empty ;
		//Changed by Gagan for MITS 11991 : Start
        /// <summary>
        /// Private variable to store connection string
        /// </summary>
        private string m_sConnectionString = "";
        /// <summary>
        /// Private variable for preventing saving of duplicate payments 
        /// </summary>
        private bool m_DoNotSaveDups = false;

        //Ankit Start : Financial Enhancement - Prefix and Suffix Changes
        private string m_sEntityIDs = string.Empty;
        private bool m_bIsPrefixEnabled = false;
        private bool m_bIsSuffixEnabled = false;
        //Ankit End

        /// <summary>
        /// Private variable to store the Duplicate Payment List
        /// </summary>
        private string m_sConfirmation = "";
		//Changed by Gagan for MITS 11991 : End
        private string sFromRsvListingPage = string.Empty;
        private bool bCarrierClaims=false;
        private static string sBaseCurrCulture = string.Empty;//Deb Multi Currency


        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = string.Empty;
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = string.Empty;
        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = string.Empty;

        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;
		#endregion 

		#region Properties
		private FundsAutoBatch FundsAutoBatch{get{return objData as FundsAutoBatch;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		private DataModelFactory Factory{get{return objData.Context.Factory;}}
        // akaushik5 Changed for MITS 37253 Starts
        //private static string[] array = null;
        private string[] array = null;
        // akaushik5 Changed for MITS 37253 Ends
        public string[] arrayPayeeType = null;
		private FundManager FundManagerObject
		{
			get
			{
				if( m_objFundManager == null )
				{
					//Retrieve the security credential information to pass to the FundManager object
					string sDSNName = base.m_fda.userLogin.objRiskmasterDatabase.DataSourceName;
					string sUserName = base.m_fda.userLogin.LoginName;
					string sPassword = base.m_fda.userLogin.Password;
					int iUserId = base.m_fda.userLogin.UserId;
					int iGroupId = base.m_fda.userLogin.GroupId;

					//Create and return the instance of the FundManager object
					m_objFundManager = new FundManager(sDSNName,sUserName, sPassword, iUserId, iGroupId, base.ClientId);
				}
				return m_objFundManager ;
			}
		}
		#endregion 

		#region Constructor
		public AutoClaimChecksForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		//Changed by Gagan for MITS 11991 : Start
            m_sConnectionString = fda.connectionString;
		//Changed by Gagan for MITS 11991 : End
            m_sDsnName = fda.userLogin.objRiskmasterDatabase.DataSourceName;
            m_sUserName = fda.userLogin.LoginName;
            m_sPassword = fda.userLogin.Password;
            m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, base.ClientId);//rkaur27
		}
		
		#endregion 
		
		#region OnUpdateObject
		/*
		 * Vaibhav 14 Feb 2006
		 * 
		 * This Function Reads the Future Payment Data form SysExData.
		 * 
		 * It iterates through each "option" element, to create the list of Non-Thired-Party-Payments Splits XML.
		 * 
		 * Note that if "Tag" value is equal to zero, It means it is pure NonThiredPartySplit Xml.
		 * 
		 * In case if "Tag" value is not zero and this is the one which is also edited, update the 
		 * thired-party-split data in SysEx.
		 * 
		 */ 
        private int GetCoverageRowId(int iPoliUnitId, int iCvgTypeCode)
        {
            int iPolCvgRowId=0;
            string sSQL = "SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID ="+ iPoliUnitId + " AND COVERAGE_TYPE_CODE ="+ iCvgTypeCode;
            using(DbReader objReader = DbFactory.GetDbReader(FundsAutoBatch.Context.DbConn.ConnectionString,sSQL))
            {
                if(objReader.Read())
                {
                    iPolCvgRowId= Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                }
            }
            
            return iPolCvgRowId;
        }
		private void GetNonThiredPartyPaymentSplitsXmlList( SortedList m_objNonThiredPartyPaymentSplitsXmlList , ref int p_iThiredPartyPaymentDeletedSplitTag )
		{
			XmlNode objFuturePaymentsNode = null ;
			XmlAttribute objTypeAttributeNode = null;
			XmlDocument objFASplitXmlDoc = null;
			XmlElement objFASplitRootElement = null;
			
			bool bIsNew = false;
			int iTag = -1;
			int iAutoSplitIdKey = 0 ; 
			int iFuturePaymentsSelectedId = 0; 
			string sFuturePaymentsRowAddedFlag =  string.Empty ; 
			string sFuturePaymentsRowDeletedFlag = string.Empty; 
			string sFuturePaymentsRowEditFlag = string.Empty; 
			bool bFuturePaymentsRowAddedFlag = false;  
			bool bFuturePaymentsRowDeletedFlag = false;
			bool bFuturePaymentsRowEditFlag = false;

			objFuturePaymentsNode = base.SysEx.DocumentElement.SelectSingleNode("FuturePayments");  

			iFuturePaymentsSelectedId = base.GetSysExDataNodeInt("FuturePaymentSelectedId",true); 
			sFuturePaymentsRowAddedFlag =  base.GetSysExDataNodeText("FuturePaymentsGrid_RowAddedFlag",true); 
			bFuturePaymentsRowAddedFlag = sFuturePaymentsRowAddedFlag.ToUpper().CompareTo("TRUE")==0?true:false;  
			sFuturePaymentsRowDeletedFlag = base.GetSysExDataNodeText("FuturePaymentsGrid_RowDeletedFlag",true); 
			bFuturePaymentsRowDeletedFlag = sFuturePaymentsRowDeletedFlag.ToUpper().CompareTo("TRUE")==0?true:false;
			sFuturePaymentsRowEditFlag = base.GetSysExDataNodeText("FuturePaymentsGrid_RowEditFlag",true); 
			bFuturePaymentsRowEditFlag = sFuturePaymentsRowEditFlag.ToUpper().CompareTo("TRUE")==0?true:false;
			
			if(objFuturePaymentsNode!=null)
			{
				// Loop through data for all rows of the grid
				foreach(XmlNode objOptionNode in objFuturePaymentsNode.SelectNodes("option"))
				{
					objTypeAttributeNode = objOptionNode.Attributes["type"];
					bIsNew = false;
					if(objTypeAttributeNode!=null && objTypeAttributeNode.InnerText.Equals("new")==true)
					{
						// New Row has been added in the Grid.
						bIsNew = true;
					}					
										
					if((bIsNew==false)||(bIsNew==true&&bFuturePaymentsRowAddedFlag==true))
					{
						objFASplitXmlDoc = new XmlDocument();
						objFASplitRootElement = objFASplitXmlDoc.CreateElement("FundsAutoSplit");
						objFASplitXmlDoc.AppendChild(objFASplitRootElement);
						objFASplitRootElement.InnerXml = objOptionNode.InnerXml;

						iAutoSplitIdKey = Conversion.ConvertStrToInteger(objFASplitRootElement.SelectSingleNode("AutoSplitId").InnerText); 							
						iTag = Conversion.ConvertStrToInteger(objOptionNode.SelectSingleNode("Tag").InnerText);   

						// 'Tag' == 0 implies this is a 'pure' Payee Funds Auto Split. 
						// We can also have Payee Funds Auto Splits created when a third party
						// payments are deducted against payee. Such split objects have 'negative'
						// amounts. In those cases the 'Tag' value is not 0.('Deduct Against Payee' Check Box)
						
						if(iTag==0)
						{
							/*
							 * Vaibhav 14 Feb 2006
							 * This split is a Non-Thired Party Split, Add It in collection of Splits,
							 */
							if(bIsNew==false&&bFuturePaymentsRowDeletedFlag==true&&iAutoSplitIdKey==iFuturePaymentsSelectedId)
							{
								// This particular split is deleted. Let's not use it for building the Payee Funds Auto Object
								continue;
							}
							iAutoSplitIdKey = iNextAutoSplitIdKey-- ;
							objFASplitRootElement.SelectSingleNode("AutoSplitId").InnerText = iAutoSplitIdKey.ToString();

							XmlNode objreservebalance= objFASplitRootElement.SelectSingleNode("reservebalance");
							objFASplitRootElement.RemoveChild( objreservebalance );
                            
                            if (objFASplitRootElement.SelectSingleNode("RCRowId") != null)
                            {

                                Claim objClaim = (Claim)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Claim", false);
                                objClaim.MoveTo(Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimId").InnerText));
                                if (objClaim.LineOfBusCode == 243)
                                {
                                    objFASplitRootElement.SelectSingleNode("RCRowId").InnerText = CommonFunctions.GetRCRowId(FundsAutoBatch.Context.DbConn.ConnectionString, Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimId").InnerText), Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimantEid").Attributes[0].Value), Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/UnitId").InnerText), Conversion.ConvertStrToInteger(objFASplitRootElement.SelectSingleNode("ReserveTypeCode").Attributes[0].Value), GetCoverageRowId(Conversion.ConvertStrToInteger(objFASplitRootElement.SelectSingleNode("UnitID").Attributes[0].Value), Conversion.ConvertStrToInteger(objFASplitRootElement.SelectSingleNode("CoverageTypeCode").Attributes[0].Value)), Conversion.ConvertStrToInteger(objFASplitRootElement.SelectSingleNode("DisabilityTypeCode").Attributes[0].Value), Conversion.ConvertStrToBool(FundsAutoBatch.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString()), base.ClientId).ToString();
                                    XmlNode objLossNode = objFASplitRootElement.SelectSingleNode("DisabilityTypeCode");
                                    objFASplitRootElement.RemoveChild(objLossNode);
                                    XmlNode objDisabilityCatNode = objFASplitRootElement.SelectSingleNode("DisabilityCatCode");
                                    objFASplitRootElement.RemoveChild(objDisabilityCatNode);
                                }
                                else
                                {
                                    objFASplitRootElement.SelectSingleNode("RCRowId").InnerText = CommonFunctions.GetRCRowId(FundsAutoBatch.Context.DbConn.ConnectionString, Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimId").InnerText), Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimantEid").Attributes[0].Value), Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/UnitId").InnerText), Conversion.ConvertStrToInteger(objFASplitRootElement.SelectSingleNode("ReserveTypeCode").Attributes[0].Value), GetCoverageRowId(Conversion.ConvertStrToInteger(objFASplitRootElement.SelectSingleNode("UnitID").Attributes[0].Value), Conversion.ConvertStrToInteger(objFASplitRootElement.SelectSingleNode("CoverageTypeCode").Attributes[0].Value)), Conversion.ConvertStrToInteger(objFASplitRootElement.SelectSingleNode("LossTypeCode").Attributes[0].Value), Conversion.ConvertStrToBool(FundsAutoBatch.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString()), base.ClientId).ToString();
                                    XmlNode objLossNode = objFASplitRootElement.SelectSingleNode("LossTypeCode");
                                    objFASplitRootElement.RemoveChild(objLossNode);
                                }
                            }
							m_objNonThiredPartyPaymentSplitsXmlList.Add( iAutoSplitIdKey , objFASplitXmlDoc );   
						}
						else
						{
							/*
							 * Vaibhav 14 Feb 2006
							 * This split is a thired party payment split.
							 */
							if( bIsNew==false&&bFuturePaymentsRowEditFlag==true&&iAutoSplitIdKey==iFuturePaymentsSelectedId)
							{
								/*
								 * Vaibhav 14 Feb 2006
								 * This Thired party payment split has been edited by user.
								 * Update the Thired Party Payment Split with new split data.
								 */
								// Thired-Party-Payment Edit.
								XmlNode objThiredPartyOptionNode = base.SysEx.DocumentElement.SelectSingleNode( "ThirdPartyPayments/option/Tag[ .=" + iTag.ToString() + "]/..");
								foreach( XmlNode objSourceNode in objFASplitRootElement.ChildNodes )
								{
									// Vaibhav : dont update the AutoSplitId
									if( objSourceNode.LocalName == "AutoSplitId" )
										continue ;

									XmlNode objTargetNode = objThiredPartyOptionNode.SelectSingleNode( objSourceNode.LocalName );
									if( objTargetNode != null )
									{
										objThiredPartyOptionNode.RemoveChild( objTargetNode );
										objThiredPartyOptionNode.AppendChild( objThiredPartyOptionNode.OwnerDocument.ImportNode( objSourceNode , true ) );
										objTargetNode = objThiredPartyOptionNode.SelectSingleNode( objSourceNode.LocalName );
										
										if( objSourceNode.LocalName == "Amount" )
											objTargetNode.InnerText = (Conversion.ConvertStrToDouble(objTargetNode.InnerText) * -1 ).ToString();
									}
								}								
							}
							else if( bIsNew==false&&bFuturePaymentsRowDeletedFlag==true&&iAutoSplitIdKey==iFuturePaymentsSelectedId)
							{
								/*
									* Vaibhav 17 Feb 2006
									* This Thired party payment split has been Deleted by user.
									* Delete the Thired Party Payment Split.
									* split would be skiped when we do build the Thired-Party-Payment Xml.
									*/
								// Thired-Party-Payment Edit.
								p_iThiredPartyPaymentDeletedSplitTag = iTag ;
							}
						}
					}
				}
			}
			objFuturePaymentsNode = null;			
		}


		private void GetThiredPartyPaymentsXmlList( SortedList m_objNonThiredPartyPaymentSplitsXmlList , SortedList m_objThiredPartyPaymentsXmlList , int p_iThiredPartyPaymentDeletedSplitTag )
		{			
			XmlNode objThirdPartyPaymentsNode = null ;
			XmlAttribute objTypeAttributeNode = null;
			XmlDocument objTppFAXmlDoc = null;
			XmlNode objSelectedOptionChildNode = null;
			XmlElement objFARootElement = null;
			XmlElement objFASplitListRootElement = null;
			XmlDocument objFASplitXmlDoc = null;
			XmlElement objFASplitRootElement = null;
			
			int iTppSelectedId = 0 ;
			int iAutoTransIdKey = -1;
			int iAutoSplitIdKey = -1;
			int iTag = 0 ;
			string sReserveTypeDescription = string.Empty;
			string sTppRowAddedFlag =  string.Empty ; 
			string sTppRowDeletedFlag = string.Empty ; 
			bool bTppRowAddedFlag = false ;  
			bool bTppRowDeletedFlag = false ;
			bool bIsNew = false;
			
			objThirdPartyPaymentsNode = base.SysEx.DocumentElement.SelectSingleNode("ThirdPartyPayments");  
			
			iTppSelectedId = base.GetSysExDataNodeInt("ThirdPartyPaymentSelectedId",true); 
			sTppRowAddedFlag =  base.GetSysExDataNodeText("ThirdPartyPaymentsGrid_RowAddedFlag",true); 
			bTppRowAddedFlag = sTppRowAddedFlag.ToUpper().CompareTo("TRUE")==0?true:false;  
			sTppRowDeletedFlag = base.GetSysExDataNodeText("ThirdPartyPaymentsGrid_RowDeletedFlag",true); 
			bTppRowDeletedFlag = sTppRowDeletedFlag.ToUpper().CompareTo("TRUE")==0?true:false;  

			
			// Get Serialized Xml for FundsAuto Object. This Xml would be used as a reference, to identify
			// TPP FundsAuto xml nodes in the <option> tag of that grid data. The <option> tag of this particular
			// TPP grid has data for both TPP FundsAuto and its one and only FundsAutoSplit.
			FundsAuto objTempFA = (FundsAuto)this.FundsAutoBatch.Context.Factory.GetDataModelObject("FundsAuto",false);     
			string sFASerializedXml = objTempFA.SerializeObject();  
			XmlDocument objFASerializedXmlDoc = new XmlDocument();			
			objFASerializedXmlDoc.LoadXml(sFASerializedXml); 
			XmlNode objFANode = objFASerializedXmlDoc.SelectSingleNode("//FundsAuto");

			// Get Serialized Xml for FundsAutoSplit Object. This Xml would be used as a reference, to identify
			// TPP FundsAutoSplit xml nodes in the <option> tag of that grid data. The <option> tag of this particular
			// TPP grid has data for both TPP FundsAuto and its one and only FundsAutoSplit.
			FundsAutoSplit objTempFASplit =	(FundsAutoSplit)this.FundsAutoBatch.Context.Factory.GetDataModelObject("FundsAutoSplit",false);     
			string sFASplitSerializedXml = objTempFASplit.SerializeObject();  
			XmlDocument objFASplitSerializedXmlDoc = new XmlDocument();			
			objFASplitSerializedXmlDoc.LoadXml(sFASplitSerializedXml); 
			XmlNode objFASplitNode = objFASplitSerializedXmlDoc.SelectSingleNode("//FundsAutoSplit"); 

			if(objThirdPartyPaymentsNode!=null)
			{
				foreach(XmlNode objOptionNode in objThirdPartyPaymentsNode.SelectNodes("option"))
				{
					objTypeAttributeNode = objOptionNode.Attributes["type"];
					bIsNew = false;
					if(objTypeAttributeNode!=null && objTypeAttributeNode.InnerText.Equals("new")==true)
					{
						// This is the 'extra' hidden <option> for capturing a new grid row data
						bIsNew = true;
					}
					if((bIsNew==false)||(bIsNew==true&&bTppRowAddedFlag==true))
					{						
						objTppFAXmlDoc = new XmlDocument();
						objFARootElement = objTppFAXmlDoc.CreateElement("FundsAuto");
						objTppFAXmlDoc.AppendChild(objFARootElement);
						
						// Refer the Serialized FundsAuto Xml to extract out Xml nodes 
						// from the innerxml of <option> with the same name.
						foreach(XmlNode objFAChildElement in objFANode.ChildNodes)
						{
							objSelectedOptionChildNode = objOptionNode.SelectSingleNode(objFAChildElement.LocalName);   
							if(objSelectedOptionChildNode!=null)
							{
								objFARootElement.AppendChild(objTppFAXmlDoc.ImportNode(objSelectedOptionChildNode,true));								
							}
						}
						
						iAutoTransIdKey = Conversion.ConvertStrToInteger(objFARootElement.SelectSingleNode("AutoTransId").InnerText);   
						iTag = Conversion.ConvertStrToInteger(objOptionNode.SelectSingleNode("Tag").InnerText);   
						if(bIsNew==false&&bTppRowDeletedFlag==true&&iAutoTransIdKey==iTppSelectedId)
						{
							// Aditya - This particular Tpp has been deleted. 
							continue;
						}
						if(bIsNew==false&& p_iThiredPartyPaymentDeletedSplitTag == iTag )
						{
							/*
							* Vaibhav 17 Feb 2006
							* This Thired party payment has been Deleted by user from the Furure Payment TAB.
							* Delete this Thired Party Payment.
							*/
							// Thired-Party-Payment Deletion from Future Payment Grid.
							continue ;
						}
						
						/*
						* Vaibhav 17 Feb 2006
						* Update the Common data from the Future-Payments to Thired-Party-Payments.
						*/
						objFARootElement.AppendChild( objTppFAXmlDoc.ImportNode( base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimId" ) , true ) );
						objFARootElement.AppendChild( objTppFAXmlDoc.ImportNode( base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimNumber" ) , true ) );
						objFARootElement.AppendChild( objTppFAXmlDoc.ImportNode( base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimantEid" ) , true ) );
						objFARootElement.AppendChild( objTppFAXmlDoc.ImportNode( base.SysEx.SelectSingleNode("/SysExData/FundsAuto/UnitId" ) , true ) );
						objFARootElement.AppendChild( objTppFAXmlDoc.ImportNode( base.SysEx.SelectSingleNode("/SysExData/FundsAuto/CtlNumber" ) , true ) );
						// npadhy JIRA 6418 Distribution Type - We do not need to track EFTPayments. We track it using DstributionType Below
                        //Added by Amitosh for EFt Payments
                        //objFARootElement.AppendChild(objTppFAXmlDoc.ImportNode(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/IsEFTPayment"), true));
                        //Deb Multi Currency
                        objFARootElement.AppendChild(objTppFAXmlDoc.ImportNode(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/PmtCurrencyType"), true));
                        //Deb Multi Currency
                        if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                        {
                            objFARootElement.AppendChild(objTppFAXmlDoc.ImportNode(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/SubAccId"), true));
                            objFARootElement.AppendChild(objTppFAXmlDoc.ImportNode(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/AccountId"), true));
                        }
                        else
                            objFARootElement.AppendChild( objTppFAXmlDoc.ImportNode( base.SysEx.SelectSingleNode("/SysExData/FundsAuto/AccountId" ) , true ) );
						
                        // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                        objFARootElement.AppendChild(objTppFAXmlDoc.ImportNode(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/DstrbnType"), true));
                        // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
						// Add AutoSplitList and the FundsAutoSplit to this Tpp FundsAuto Xml.
						objFASplitListRootElement = objTppFAXmlDoc.CreateElement("AutoSplitList");
						objTppFAXmlDoc.DocumentElement.AppendChild(objFASplitListRootElement); 

						objFASplitRootElement = objTppFAXmlDoc.CreateElement("FundsAutoSplit");
						objFASplitListRootElement.AppendChild(objFASplitRootElement); 

						// Refer the Serialized FundsAutoSplit Xml to extract out Xml nodes 
						// from the innerxml of <option> with the same name.																		
						foreach(XmlNode objFASplitChildElement in objFASplitNode.ChildNodes)
						{
							objSelectedOptionChildNode = objOptionNode.SelectSingleNode(objFASplitChildElement.LocalName);   
							if(objSelectedOptionChildNode!=null)
							{
								objFASplitRootElement.AppendChild(objTppFAXmlDoc.ImportNode(objSelectedOptionChildNode,true));								
							}
						}						
 
						objFASplitRootElement.OwnerDocument.DocumentElement.SelectSingleNode("/FundsAuto/AutoSplitList/FundsAutoSplit/AutoSplitId").InnerText = (iNextAutoSplitIdKey--).ToString();
						objFASplitRootElement.OwnerDocument.DocumentElement.SelectSingleNode("/FundsAuto/AutoSplitList/FundsAutoSplit/AutoTransId").InnerText = "-1" ;

                        objSelectedOptionChildNode = objOptionNode.SelectSingleNode("PolicyID");
                        if (objSelectedOptionChildNode != null)
                        {
                            objFASplitRootElement.AppendChild(objTppFAXmlDoc.ImportNode(objSelectedOptionChildNode, true));
                        }

                        //rupal:start, r8 unit implementation
                        objSelectedOptionChildNode = objOptionNode.SelectSingleNode("UnitID");
                        if (objSelectedOptionChildNode != null)
                        {
                            objFASplitRootElement.AppendChild(objTppFAXmlDoc.ImportNode(objSelectedOptionChildNode, true));
                        }
                        //rupal:end

                        objSelectedOptionChildNode = objOptionNode.SelectSingleNode("CoverageTypeCode");
                        if (objSelectedOptionChildNode != null)
                        {
                            objFASplitRootElement.AppendChild(objTppFAXmlDoc.ImportNode(objSelectedOptionChildNode, true));
                        }

                        objSelectedOptionChildNode = objOptionNode.SelectSingleNode("CovgSeqNum");
                        if (objSelectedOptionChildNode != null)
                            objFASplitRootElement.AppendChild(objTppFAXmlDoc.ImportNode(objSelectedOptionChildNode, true));
                        //Ankit : End


                        if (objOptionNode.SelectSingleNode("RCRowId") != null)
                        {

                            Claim objClaim = (Claim)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimId").InnerText));
                            if (objClaim.LineOfBusCode == 243)
                            {

                                objOptionNode.SelectSingleNode("RCRowId").InnerText = CommonFunctions.GetRCRowId(FundsAutoBatch.Context.DbConn.ConnectionString, Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimId").InnerText), Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimantEid").Attributes[0].Value), Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/UnitId").InnerText), Conversion.ConvertStrToInteger(objFASplitRootElement.SelectSingleNode("ReserveTypeCode").Attributes[0].Value), GetCoverageRowId(Conversion.ConvertStrToInteger(objFASplitRootElement.SelectSingleNode("UnitID").Attributes[0].Value), Conversion.ConvertStrToInteger(objFASplitRootElement.SelectSingleNode("CoverageTypeCode").Attributes[0].Value)), Conversion.ConvertStrToInteger(objOptionNode.SelectSingleNode("DisabilityTypeCode").Attributes[0].Value), Conversion.ConvertStrToBool(FundsAutoBatch.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString()), base.ClientId).ToString();
                                XmlNode objLossNode = objOptionNode.SelectSingleNode("DisabilityTypeCode");
                                objOptionNode.RemoveChild(objLossNode);
                            }
                            else
                            {
                                objOptionNode.SelectSingleNode("RCRowId").InnerText = CommonFunctions.GetRCRowId(FundsAutoBatch.Context.DbConn.ConnectionString, Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimId").InnerText), Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimantEid").Attributes[0].Value), Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/UnitId").InnerText), Conversion.ConvertStrToInteger(objFASplitRootElement.SelectSingleNode("ReserveTypeCode").Attributes[0].Value), GetCoverageRowId(Conversion.ConvertStrToInteger(objFASplitRootElement.SelectSingleNode("UnitID").Attributes[0].Value), Conversion.ConvertStrToInteger(objFASplitRootElement.SelectSingleNode("CoverageTypeCode").Attributes[0].Value)), Conversion.ConvertStrToInteger(objOptionNode.SelectSingleNode("LossTypeCode").Attributes[0].Value), Conversion.ConvertStrToBool(FundsAutoBatch.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString()), base.ClientId).ToString();
                                XmlNode objLossNode = objOptionNode.SelectSingleNode("LossTypeCode");
                                objOptionNode.RemoveChild(objLossNode);
                            }


                            objFASplitRootElement.AppendChild(objTppFAXmlDoc.ImportNode(objOptionNode.SelectSingleNode("RCRowId"), true));
                          
                        }
						// If this Tpp is to be deducted against Payee, add a 'negative' split to Payee FundsAuto XML
						if(objOptionNode.SelectSingleNode("DeductAgainstPayeeFlag").InnerText.Equals("True")==true)
						{
							objFASplitXmlDoc = new XmlDocument();
							objFASplitXmlDoc.AppendChild(objFASplitXmlDoc.ImportNode(objFASplitRootElement,true));
							
							XmlNode objAmountXmlNode = objFASplitXmlDoc.DocumentElement.SelectSingleNode("Amount");
							objAmountXmlNode.InnerText  = (Conversion.ConvertStrToDouble(objAmountXmlNode.InnerText) * -1).ToString();
							
							iAutoSplitIdKey = Conversion.ConvertStrToInteger( objFASplitXmlDoc.DocumentElement.SelectSingleNode("AutoSplitId").InnerText );

							m_objNonThiredPartyPaymentSplitsXmlList.Add( iAutoSplitIdKey , objFASplitXmlDoc );
						}
											
						m_objThiredPartyPaymentsXmlList.Add(iAutoTransIdKey,objTppFAXmlDoc);					
					}
				}
			}

			objFASplitXmlDoc = null;
			objFASplitRootElement = null;
			objThirdPartyPaymentsNode = null;
			objTempFA = null;
			objFASerializedXmlDoc = null;
			objFANode = null;
			objTempFASplit = null;
			objFASplitSerializedXmlDoc = null;
			objFASplitNode = null;
			objSelectedOptionChildNode = null;
			objTppFAXmlDoc = null;
			objFARootElement = null;
			objFASplitListRootElement = null;			
		}

		private XmlDocument TrimUnusedFAChildNodes(XmlDocument objFASerialXml)
		{
			// 03.03.2006 
			// BSB Remove AutoSplitList Child node since it will be duplicated in the data sent to PopulateObject
			// otherwise. (No error just bad practice)
			XmlElement eltToRemove = objFASerialXml.SelectSingleNode("/FundsAuto/AutoSplitList") as XmlElement;
			if(eltToRemove!=null)
				eltToRemove.ParentNode.RemoveChild(eltToRemove);
			// 03.03.2006 
			// BSB Remove Entity Child since you can only select an existing entity here - not create one.
			//	(and having an entity on all of the FA children causes a "looping update" of the same
			//	record - which blows up.
			eltToRemove = objFASerialXml.SelectSingleNode("/FundsAuto/PayeeEntity") as XmlElement;
			if(eltToRemove!=null)
				eltToRemove.ParentNode.RemoveChild(eltToRemove);

			return objFASerialXml;
		}
		public override void OnUpdateObject()
		{
			base.OnUpdateObject();
            XmlDocument objXML = base.SysEx;
            //string[] array = null;

			/* Aditya - 16-Nov-2005 - This method gets the FundsAutoBatch object and more importantly its 
			 * child FundsAutoList ready from the UI data. Essentially FundsAutoList is a collection of all 
			 * future(auto) payments to be made to the Payee and Third Party Payee's. 
			 * 
			 * e.g. If 3 monthly payments are scheduled for a Payee and 2 Third Party Payees, 
			 * FundsAutoList would have (1+2)  * 3 = 9 FundsAuto Objects.
			 * 
			 * Each of these FundsAuto objects have a child AutoSplitList, that is a collection of 
			 * FundsAutoSplit objects. As of now, Riskmaster supports Splits for Payee FundsAuto objects
			 * but for Third Party Payee FundsAuto Objects, there is only one split.
			 * 
			 * The key point here is that 'Auto' tables only store information for the future payments.
			 * Once a check is printed for an 'auto' payment, the particular records are deleted from 
			 * 'Auto' tables. Refer--> Riskmaster.Application.PrintChecks.CheckManager.InsertAutoCheckIntoFunds()
			 * method.
			 * 
			 * Essentially this method builds up the FundsAutoList for FundsAutoBatch object, by creating
			 * FundsAuto objects (and their child FundsAutoSplit objects), using PopulateObject()
			 * 
			 * Except for a few fields (e.g. PayNumber), all Payee FundsAuto Objects have the same data.
			 * This common Payee data is captured on the 'Scheduled Checks' tab, and is bound to 'FundsAuto'
			 * node in SysExData. We build up the Payee FundsAuto Objects through this XML along with 
			 * Split Xml's available in <option> tags of FuturePayments Grid data.
			 * 
			 * The Xml's for TPP FundsAuto (and its one and only split) is available in <option> tag of
			 * ThirdPartyPaymentsGrid data.
			 * */

			// Common XML for Payee FundsAuto Objects
			XmlDocument objPayeeFAXmlDoc = new XmlDocument();
			int iThiredPartyPaymentDeletedSplitTag = -1 ;
			
			// Update the AccountId in case SubAccount In Use.
			if( this.FundsAutoBatch.Context.InternalSettings.SysSettings.UseFundsSubAcc )
			{
				// DropDown represents SubBankAccountId 
				BankAccSub objBankAccSub = ( BankAccSub ) this.FundsAutoBatch.Context.Factory.GetDataModelObject( "BankAccSub" , false );
				objBankAccSub.MoveTo( Conversion.ConvertStrToInteger( base.SysEx.SelectSingleNode("/SysExData/FundsAuto/SubAccId" ).InnerText ) );


                XmlElement objNew = null;
                objNew = base.SysEx.CreateElement("AccountId");
                XmlNode objFundsAuto = base.SysEx.SelectSingleNode("/SysExData/FundsAuto");
                objFundsAuto.AppendChild(objNew);
                
				base.SysEx.SelectSingleNode("/SysExData/FundsAuto/AccountId" ).InnerText = objBankAccSub.AccountId.ToString()  ;
			}
			
			// Get <FundsAuto> from SysExData
			objPayeeFAXmlDoc.AppendChild( objPayeeFAXmlDoc.ImportNode( base.SysEx.DocumentElement.SelectSingleNode("FundsAuto"),true)); 
			objPayeeFAXmlDoc = TrimUnusedFAChildNodes(objPayeeFAXmlDoc);
			
			/*  The aim is build a complete Payee FundsAuto XML including the splits
				*  that can be fed into PopulateObject(). Lets first work out the splits. 
				* */

			SortedList objNonThiredPartyPaymentSplitsXmlList = new SortedList(); 
			SortedList objThiredPartyPaymentsXmlList = new SortedList();

			this.GetNonThiredPartyPaymentSplitsXmlList( objNonThiredPartyPaymentSplitsXmlList , ref iThiredPartyPaymentDeletedSplitTag );
			this.GetThiredPartyPaymentsXmlList( objNonThiredPartyPaymentSplitsXmlList , objThiredPartyPaymentsXmlList , iThiredPartyPaymentDeletedSplitTag );
			
			/* At this point we have one Payee FundsAuto Xml, 
				* list of Payee FundsAutoSplit XMLs and 
				* list of Tpp FundsAuto Xml's.
				* Let's manipulate the FundsAutoList child of FundsAutoBatch object.
				* */
			FundsAutoList objFundsAutoList = this.FundsAutoBatch.FundsAutoList;
            foreach (FundsAuto objFundsAuto in objFundsAutoList)
            {
               objFundsAutoList.Remove(objFundsAuto.AutoTransId);
            }
			
			/** Add splits Xml's to Payee FundsAuto Xml**/
			XmlElement objPayeeSplitListElement = objPayeeFAXmlDoc.CreateElement("AutoSplitList");
			XmlDocument objFuturePaymentsFASplitXmlDoc = null;
			XmlNode objPayeeSplit = null;
			objPayeeFAXmlDoc.DocumentElement.AppendChild(objPayeeSplitListElement);
			for(int iListIndex=0; iListIndex<objNonThiredPartyPaymentSplitsXmlList.Count; iListIndex++)
			{
				objFuturePaymentsFASplitXmlDoc = (XmlDocument) objNonThiredPartyPaymentSplitsXmlList.GetByIndex(iListIndex);
                //Deb Multi Currency	
                XmlNode objBaseCurrency = objFuturePaymentsFASplitXmlDoc.SelectSingleNode("//BaseCurrencyType");
                if (objBaseCurrency != null)
                    objFuturePaymentsFASplitXmlDoc.SelectSingleNode("//FundsAutoSplit").RemoveChild(objBaseCurrency);
                XmlNode objUseMultiCurrency = objFuturePaymentsFASplitXmlDoc.SelectSingleNode("//UseMultiCurrency");
                if (objUseMultiCurrency != null)
                    objFuturePaymentsFASplitXmlDoc.SelectSingleNode("//FundsAutoSplit").RemoveChild(objUseMultiCurrency);
                //Deb Multi Currency	
				objPayeeSplit = objPayeeFAXmlDoc.ImportNode(objFuturePaymentsFASplitXmlDoc.DocumentElement,true);  
				objPayeeSplitListElement.AppendChild(objPayeeSplit);  
			}
		
			string sPayeeTypeCode = "";
			sPayeeTypeCode = base.SysEx.DocumentElement.SelectSingleNode( "/SysExData/PayeeTypeCode").InnerText ;
			m_sOldSelectedPayeeTypeCode = sPayeeTypeCode ;
			if( !Riskmaster.Common.Utilities.IsNumeric( sPayeeTypeCode ) )
			{
				if( sPayeeTypeCode.Length > 0 )
					if( sPayeeTypeCode.Substring( 0 , 1 ) == "e" || sPayeeTypeCode.Substring( 0 ,1 ) == "o" ) 
						sPayeeTypeCode = sPayeeTypeCode.Substring( 1 );
			}
			objPayeeFAXmlDoc.DocumentElement.SelectSingleNode("PayeeTypeCode").Attributes["codeid"].Value = sPayeeTypeCode;
	
			double dAmount = 0.0;
			bool bIsAmountCalculated = false;

			int iPayNumber = 0;
			if(this.FundsAutoBatch.CurrentPayment==0)
			{
				this.FundsAutoBatch.CurrentPayment = 1;
			}

			// First 'in-memory' item of collection is assigned a key value of -1			
			int iTempKeyForFundsAutoObject = -1;

			int iPaymentsCount = this.FundsAutoBatch.PaymentsToDate + 1;
			bool bCalculatePrintDateFlag = false; 
			
			DateTime dtPrintDate;
			DateTime dtNextPrintDate = Conversion.ToDate(this.FundsAutoBatch.StartDate);  
            dtNextPrintDate = AdjustPrintDate(this.FundsAutoBatch.AdjustPrintDatesFlag, dtNextPrintDate);
			// Vaibhav 8/16/2006 : Calculate the start date by skipping the interval for the payments already made. 
            //Add PaymentIntervalDays by kuladeep for Day interval for auto checks mits:19360 
            //CalculateDateRange(dtNextPrintDate, this.FundsAutoBatch.CurrentPayment  - 1 ,this.FundsAutoBatch.PaymentInterval,out dtNextPrintDate);
            CalculateDateRange(dtNextPrintDate, this.FundsAutoBatch.CurrentPayment - 1, this.FundsAutoBatch.PaymentInterval, this.FundsAutoBatch.PaymentIntervalDays, out dtNextPrintDate);//Ashish Ahuja RMA-9496

			//Conversion.ToDate(objPayeeFAXmlDoc.DocumentElement.SelectSingleNode("PrintDate").InnerText);

			FundsAuto objPayeeFundsAuto = null;
			XmlDocument objTmpTppFAXmlDoc = null;
			FundsAuto objTppFundsAuto = null;
			
			// Vaibhav 8/16/2006 : Init the payment count with the payments already made. 
			iPayNumber = this.FundsAutoBatch.CurrentPayment - 1 ;
			// Create FundsAuto Objects for remaining payments
            while(iPaymentsCount<= this.FundsAutoBatch.TotalPayments)
			{
				iPayNumber++;
                
				// Add Payee FundsAuto object to FundsAutoList Collection
				objPayeeFAXmlDoc.DocumentElement.SelectSingleNode("AutoTransId").InnerText = iTempKeyForFundsAutoObject.ToString();  
				iTempKeyForFundsAutoObject--;
				objPayeeFundsAuto = ( FundsAuto ) this.FundsAutoBatch.Context.Factory.GetDataModelObject( "FundsAuto", false );
                
                ResetResRowId(ref objPayeeFAXmlDoc, objPayeeFundsAuto);

                objPayeeFundsAuto.PopulateObject(objPayeeFAXmlDoc);
                //pmittal5 Mits 15308 05/06/09 - Adding a new Payee Entity for Non Third Party Payment
                if (objPayeeFundsAuto.PayeeEid == 0 && iPaymentsCount <= 1)//rsushilaggar MITS 27286 Date 02/29/2012
                    UpdateFundsAutoPayeeEntity(objPayeeFundsAuto);
                
				objFundsAutoList.Add( objPayeeFundsAuto );
                UpdateFundsAutoMailToData(objPayeeFundsAuto);//AA 7199

                foreach (FundsAuto fundsAuto in objFundsAutoList)
                {
                    fundsAuto.MailToAddress.Isdupeoverride = this.SysEx.SelectSingleNode("//dupeoverride").InnerText;
                    //fundsAuto.MailToAddress.SearchString = strSearchString;
                    if (this.SysEx.SelectSingleNode("//dupeoverride") != null)
                    {
                        if (this.SysEx.SelectSingleNode("//HdnSearchString") != null && this.SysEx.SelectSingleNode("//HdnSearchString").InnerText.ToLower().Trim() == fundsAuto.MailToAddress.SearchString)
                            fundsAuto.MailToAddress.Isdupeoverride = this.SysEx.SelectSingleNode("//dupeoverride").InnerText.Trim();

                        fundsAuto.MailToAddress.AddressId = 0;
                        fundsAuto.MailToAddressId = 0;

                    }
                }

				objPayeeFundsAuto.PayNumber = iPayNumber;
				objPayeeFundsAuto.EndPayNumber = this.FundsAutoBatch.TotalPayments;
				//BSB 11.22.2006 - Putting this incorrect value into  CheckBatchNum causes 
				// random deletions of unpaid checks when the AutoBatchId 
				// coincidentally happens to correspond to a valid "print batch" number.
				// The intent is for this field only to hold an ID related to print batches. The
				// following line was VERY BAD.
				//objPayeeFundsAuto.CheckBatchNum = this.FundsAutoBatch.AutoBatchId;  
				objPayeeFundsAuto.CheckBatchNum = 0;
                //Deb Multi Currency
                if (objPayeeFundsAuto.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                {
                    objPayeeFundsAuto.ClaimCurrencyType = objPayeeFundsAuto.Context.DbConn.ExecuteInt("SELECT CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID=" + objPayeeFundsAuto.ClaimId);
                    //Deb MITS 27171
                    //if (objPayeeFundsAuto.PmtToClaimCurRate == 0 || objPayeeFundsAuto.AutoSplitList.Count > objPayeeFundsAuto.AutoSplitList.CommittedCount)
                    objPayeeFundsAuto.PmtToClaimCurRate = CommonFunctions.ExchangeRateSrc2Dest(objPayeeFundsAuto.PmtCurrencyType, objPayeeFundsAuto.ClaimCurrencyType, m_sConnectionString, base.ClientId);
                    //if (objPayeeFundsAuto.PmtToBaseCurRate == 0 || objPayeeFundsAuto.AutoSplitList.Count > objPayeeFundsAuto.AutoSplitList.CommittedCount)
                    objPayeeFundsAuto.PmtToBaseCurRate = CommonFunctions.ExchangeRateSrc2Dest(objPayeeFundsAuto.PmtCurrencyType, objPayeeFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, base.ClientId);
                    //Deb MITS 27171
                }
                //Deb Multi Currency
                //MITS 32180 mcapps2 04/01/2013 Start
                string sPayToOrder = objPayeeFundsAuto.PayToTheOrderOf;
                sPayToOrder = sPayToOrder.Replace("\n", " ").Replace("\r", " ");
                objPayeeFundsAuto.PayToTheOrderOf = sPayToOrder;
                //MITS 32180 mcapps2 04/01/2013 End
				if(bIsAmountCalculated==false)
				{
					foreach(FundsAutoSplit objFASplit in objPayeeFundsAuto.AutoSplitList)
					{
                        //Deb Multi Currency
                        if (objFASplit.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                        {
                            objFASplit.PmtCurrencyAmount = objFASplit.Amount;
                            objFASplit.PmtCurrencyInvAmount = objFASplit.InvoiceAmount;
                            objFASplit.Amount = objFASplit.PmtCurrencyAmount * objPayeeFundsAuto.PmtToBaseCurRate;
                            objFASplit.InvoiceAmount = objFASplit.PmtCurrencyInvAmount * objPayeeFundsAuto.PmtToBaseCurRate;
                            //Deb MITS 27171
                            //if (objPayeeFundsAuto.BaseToClaimCurRate == 0 || objPayeeFundsAuto.AutoSplitList.Count > objPayeeFundsAuto.AutoSplitList.CommittedCount)
                            objPayeeFundsAuto.BaseToClaimCurRate = CommonFunctions.ExchangeRateSrc2Dest(objPayeeFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, objPayeeFundsAuto.ClaimCurrencyType, m_sConnectionString, base.ClientId);
                            //Deb MITS 27171
                            objFASplit.ClaimCurrencyAmount = objFASplit.Amount * objPayeeFundsAuto.BaseToClaimCurRate;
                            objFASplit.ClaimCurrencyInvAmount = objFASplit.InvoiceAmount * objPayeeFundsAuto.BaseToClaimCurRate;
                            dAmount += objFASplit.PmtCurrencyAmount;
                        }
                        else
                        {
                            dAmount += objFASplit.Amount;
                        }
                        //Deb Multi Currency
					}
					bIsAmountCalculated = true;
				}
                //Deb Multi Currency
                if (objPayeeFundsAuto.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                {
                    //Deb MITS 27171
                    //if (objPayeeFundsAuto.BaseToClaimCurRate == 0 || objPayeeFundsAuto.AutoSplitList.Count > objPayeeFundsAuto.AutoSplitList.CommittedCount)
                    objPayeeFundsAuto.BaseToClaimCurRate = CommonFunctions.ExchangeRateSrc2Dest(objPayeeFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, objPayeeFundsAuto.ClaimCurrencyType, m_sConnectionString, base.ClientId);
                    //if (objPayeeFundsAuto.BaseToPmtCurRate == 0 || objPayeeFundsAuto.AutoSplitList.Count > objPayeeFundsAuto.AutoSplitList.CommittedCount)
                    objPayeeFundsAuto.BaseToPmtCurRate = CommonFunctions.ExchangeRateSrc2Dest(objPayeeFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, objPayeeFundsAuto.PmtCurrencyType, m_sConnectionString, base.ClientId);
                    //Deb MITS 27171
                    objPayeeFundsAuto.PmtCurrencyAmount = dAmount;
                    objPayeeFundsAuto.Amount = objPayeeFundsAuto.PmtCurrencyAmount * objPayeeFundsAuto.PmtToBaseCurRate;
                    objPayeeFundsAuto.ClaimCurrencyAmount = objPayeeFundsAuto.Amount * objPayeeFundsAuto.BaseToClaimCurRate;
                    if (iPayNumber > 1)
                    {
                        foreach (FundsAutoSplit objFASplit in objPayeeFundsAuto.AutoSplitList)
                        {
                            objFASplit.PmtCurrencyAmount = objFASplit.Amount;
                            objFASplit.PmtCurrencyInvAmount = objFASplit.InvoiceAmount;
                            objFASplit.Amount = objFASplit.PmtCurrencyAmount * objPayeeFundsAuto.PmtToBaseCurRate;
                            objFASplit.InvoiceAmount = objFASplit.PmtCurrencyInvAmount * objPayeeFundsAuto.PmtToBaseCurRate;
                            objFASplit.ClaimCurrencyAmount = objFASplit.Amount * objPayeeFundsAuto.BaseToClaimCurRate;
                            objFASplit.ClaimCurrencyInvAmount = objFASplit.InvoiceAmount * objPayeeFundsAuto.BaseToClaimCurRate;
                        }
                    }
                }
                //Deb Multi Currency
                else
                {
                objPayeeFundsAuto.Amount = Math.Round(dAmount, 2, MidpointRounding.AwayFromZero); 
                    //nsachdeva2 - MITS:27275 - Put the default values in tables in case multi currency is off.
                    objPayeeFundsAuto.ClaimCurrencyType = objPayeeFundsAuto.PmtCurrencyType;
                    objPayeeFundsAuto.PmtToClaimCurRate = 1.0;
                    objPayeeFundsAuto.PmtToBaseCurRate = 1.0;
                    objPayeeFundsAuto.BaseToClaimCurRate = 1.0;
                    objPayeeFundsAuto.BaseToPmtCurRate = 1.0;
                    foreach (FundsAutoSplit objFASplit in objPayeeFundsAuto.AutoSplitList)
                    {
                        objFASplit.PmtCurrencyAmount = objFASplit.Amount;
                        objFASplit.PmtCurrencyInvAmount = objFASplit.InvoiceAmount;
                        objFASplit.ClaimCurrencyAmount = objFASplit.Amount;
                        objFASplit.ClaimCurrencyInvAmount = objFASplit.InvoiceAmount;
                    }    
                    //End MITS:27275
                }
                //Deb Multi Currency
				if(bCalculatePrintDateFlag==true)
				{
					dtPrintDate = dtNextPrintDate;
                    //Add PaymentIntervalDays by kuladeep for Day interval for auto checks mits:19360 
                    //CalculateDateRange(dtPrintDate, 1, this.FundsAutoBatch.PaymentInterval, out dtNextPrintDate);
                    //CalculateDateRange(dtPrintDate, 1, this.FundsAutoBatch.PaymentInterval, this.FundsAutoBatch.PaymentIntervalDays, out dtNextPrintDate);
                    CalculateDateRange(dtPrintDate, 1, this.FundsAutoBatch.PaymentInterval, this.FundsAutoBatch.PaymentIntervalDays, out dtNextPrintDate);
					objPayeeFundsAuto.PrintDate = dtNextPrintDate.ToShortDateString();
                                            
					foreach(FundsAutoSplit objFASplit in objPayeeFundsAuto.AutoSplitList)
					{
						if(objFASplit.FromDate != "")
						{
							DateTime dtNextDate;
							DateTime dtFromDate = Conversion.ToDate(objFASplit.FromDate);

                            //Add PaymentIntervalDays by kuladeep for Day interval for auto checks mits:19360 
                            //CalculateDateRange(dtFromDate, (iPayNumber - this.FundsAutoBatch.CurrentPayment), this.FundsAutoBatch.PaymentInterval, out dtNextDate);
                            CalculateDateRange(dtFromDate, (iPayNumber - this.FundsAutoBatch.CurrentPayment), this.FundsAutoBatch.PaymentInterval, this.FundsAutoBatch.PaymentIntervalDays, out dtNextDate); //Ashish Ahuja RMA-9496   
							objFASplit.FromDate = Conversion.ToDbDate(dtNextDate);  

							dtFromDate = Conversion.ToDate(objFASplit.ToDate);

                            //Add PaymentIntervalDays by kuladeep for Day interval for auto checks mits:19360 
                            //CalculateDateRange(dtFromDate, (iPayNumber - this.FundsAutoBatch.CurrentPayment), this.FundsAutoBatch.PaymentInterval, out dtNextDate);    
                            CalculateDateRange(dtFromDate, (iPayNumber - this.FundsAutoBatch.CurrentPayment), this.FundsAutoBatch.PaymentInterval, this.FundsAutoBatch.PaymentIntervalDays, out dtNextDate);  //Ashish Ahuja RMA-9496  
							objFASplit.ToDate = Conversion.ToDbDate(dtNextDate);  
						}
					}
				}		
				else
				{
					objPayeeFundsAuto.PrintDate = dtNextPrintDate.ToShortDateString();
				}

				for(int iListIndex=0; iListIndex<objThiredPartyPaymentsXmlList.Count; iListIndex++ )
				{
					objTmpTppFAXmlDoc = (XmlDocument) objThiredPartyPaymentsXmlList.GetByIndex(iListIndex);
			
					objTmpTppFAXmlDoc.DocumentElement.SelectSingleNode("AutoTransId").InnerText = iTempKeyForFundsAutoObject.ToString();
					iTempKeyForFundsAutoObject--;
                    
                    objTppFundsAuto = ( FundsAuto ) this.FundsAutoBatch.Context.Factory.GetDataModelObject( "FundsAuto", false );
                    ResetResRowId(ref objTmpTppFAXmlDoc, objTppFundsAuto);
					objTppFundsAuto.PopulateObject(objTmpTppFAXmlDoc);
                    //pmittal5 Mits 15308 05/06/09 - Adding a new Payee Entity for Third Party Payment
                    if (objTppFundsAuto.PayeeEid == 0)
                        UpdateFundsAutoPayeeEntity(objTppFundsAuto);
                    //UpdateFundsAutoMailToData(objTppFundsAuto);//AA 7199
					objFundsAutoList.Add( objTppFundsAuto );

					objTppFundsAuto.ThirdPartyFlag = 1;
					objTppFundsAuto.PayNumber = iPayNumber;
					objTppFundsAuto.CheckBatchNum = this.FundsAutoBatch.AutoBatchId;
					objTppFundsAuto.SettlementFlag = false;

					// Vaibhav 15 Jun 2006, 
					// specify the added postfix of control number for third party payees.
					objTppFundsAuto.CtlNumber = "-T" + ( iListIndex + 1 ) ;

                    objTppFundsAuto.PayeeTypeCode = this.objCache.GetCodeId("O", "PAYEE_TYPE");
                    objTppFundsAuto.EndPayNumber = this.FundsAutoBatch.TotalPayments;

					objTppFundsAuto.PrintDate = dtNextPrintDate.ToString();
                    //Deb Multi Currency- We can remove this in future if not needed
                    if (objTppFundsAuto.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                    {
                        objTppFundsAuto.ClaimCurrencyType = objTppFundsAuto.Context.DbConn.ExecuteInt("SELECT CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID=" + objPayeeFundsAuto.ClaimId);
                        //Deb MITS 27171
                        //if (objTppFundsAuto.BaseToClaimCurRate == 0 || objTppFundsAuto.AutoSplitList.Count > objPayeeFundsAuto.AutoSplitList.CommittedCount)
                        objTppFundsAuto.BaseToClaimCurRate = CommonFunctions.ExchangeRateSrc2Dest(objPayeeFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, objPayeeFundsAuto.ClaimCurrencyType, m_sConnectionString, base.ClientId);
                        //if (objTppFundsAuto.BaseToPmtCurRate == 0 || objTppFundsAuto.AutoSplitList.Count > objPayeeFundsAuto.AutoSplitList.CommittedCount)
                        objTppFundsAuto.BaseToPmtCurRate = CommonFunctions.ExchangeRateSrc2Dest(objPayeeFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, objPayeeFundsAuto.PmtCurrencyType, m_sConnectionString, base.ClientId);
                        //Deb MITS 27171
                        double dExhRatePmtToClaim = CommonFunctions.ExchangeRateSrc2Dest(objPayeeFundsAuto.PmtCurrencyType, objPayeeFundsAuto.ClaimCurrencyType, m_sConnectionString, base.ClientId);
                        double dExhRatePmtToBase = CommonFunctions.ExchangeRateSrc2Dest(objPayeeFundsAuto.PmtCurrencyType, objPayeeFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, base.ClientId);
                        objTppFundsAuto.PmtToClaimCurRate = dExhRatePmtToClaim;
                        objTppFundsAuto.PmtToBaseCurRate = dExhRatePmtToBase;
                        objTppFundsAuto.BaseToClaimCurRate = objTppFundsAuto.BaseToClaimCurRate;
                        objTppFundsAuto.PmtCurrencyAmount = objTppFundsAuto.Amount;
                        objTppFundsAuto.Amount = objTppFundsAuto.PmtCurrencyAmount * objPayeeFundsAuto.PmtToBaseCurRate;
                        objTppFundsAuto.ClaimCurrencyAmount = objTppFundsAuto.Amount * objTppFundsAuto.BaseToClaimCurRate;
                        foreach (FundsAutoSplit objFASplit in objTppFundsAuto.AutoSplitList)
                        {
                            objFASplit.PmtCurrencyAmount = objFASplit.Amount;
                            objFASplit.PmtCurrencyInvAmount = objFASplit.InvoiceAmount;
                            objFASplit.Amount = objFASplit.PmtCurrencyAmount * objPayeeFundsAuto.PmtToBaseCurRate;
                            objFASplit.InvoiceAmount = objFASplit.PmtCurrencyInvAmount * objPayeeFundsAuto.PmtToBaseCurRate;
                        }
                    }
                    //Deb Multi Currency
					if(bCalculatePrintDateFlag==true)
					{
						foreach(FundsAutoSplit objFASplit in objTppFundsAuto.AutoSplitList)
						{
							DateTime dtNextDate;
                            DateTime dtFromDate;

                            if (!string.IsNullOrEmpty(objFASplit.FromDate))
                            {
                                dtFromDate = Conversion.ToDate(objFASplit.FromDate);
                                //Add PaymentIntervalDays by kuladeep for Day interval for auto checks mits:19360 
                                //CalculateDateRange(dtFromDate, (iPayNumber - this.FundsAutoBatch.CurrentPayment), this.FundsAutoBatch.PaymentInterval, out dtNextDate);
                                CalculateDateRange(dtFromDate, (iPayNumber - this.FundsAutoBatch.CurrentPayment), this.FundsAutoBatch.PaymentInterval, this.FundsAutoBatch.PaymentIntervalDays, out dtNextDate);//Ashish Ahuja RMA-9496
                                objFASplit.FromDate = Conversion.ToDbDate(dtNextDate);
                            
                                dtFromDate = Conversion.ToDate(objFASplit.ToDate);
                                //Add PaymentIntervalDays by kuladeep for Day interval for auto checks mits:19360 
                                //CalculateDateRange(dtFromDate, (iPayNumber - this.FundsAutoBatch.CurrentPayment), this.FundsAutoBatch.PaymentInterval, out dtNextDate);
                                CalculateDateRange(dtFromDate, (iPayNumber - this.FundsAutoBatch.CurrentPayment), this.FundsAutoBatch.PaymentInterval, this.FundsAutoBatch.PaymentIntervalDays, out dtNextDate);//Ashish Ahuja RMA-9496
                                objFASplit.ToDate = Conversion.ToDbDate(dtNextDate);
                            }
						}
					}
				}

				if(bCalculatePrintDateFlag==false)
				{
					bCalculatePrintDateFlag = true;
				}

				iPaymentsCount++;

                //Added by amitsoh for R8 enhancement of Combined Payments
                if (objXML.SelectSingleNode("//FundsAutoXPayeeList/@codeid") != null)
                {
                    string sFundsAutoXPayeeList = objXML.SelectSingleNode("//FundsAutoXPayeeList").Attributes["codeid"].Value;
                    if (!string.IsNullOrEmpty(sFundsAutoXPayeeList))//Deb
                    {
                        array = sFundsAutoXPayeeList.Split(' ');
                    }
                }
                if (array != null)
                {
                    //Ankit Start : Financial Enhancements - Payee Phrase Change
                    int intPhraseTypeCode;
                    int intBeforePayee;
                    XmlNode objSavePayeePhrase = base.SysEx.DocumentElement.SelectSingleNode("/SysExData/SavePayeePhrase");
                    //Ankit End

                    for (int count = 0; count <= array.Length - 1; count++)
                    {
                        bool bAdd = true;
                        if (array[count] != string.Empty)
                        {
                            arrayPayeeType = array[count].Split("#".ToCharArray()[0]);
                            //Start MITS 26517 Added by Ashutosh
                            foreach (FundsAutoXPayee OFundsXPayee in objPayeeFundsAuto.FundsAutoXPayeeList)
                            {
                                if (Conversion.ConvertStrToInteger(arrayPayeeType[0]) == OFundsXPayee.PayeeEid)
                                {
                                    bAdd = false;
                                    break;
                                }
                            }
                            //End MITS 26517

                            if (bAdd)
                            {
                                FundsAutoXPayee objFundsAutoXPayee = objPayeeFundsAuto.FundsAutoXPayeeList.AddNew();
                                objFundsAutoXPayee.PayeeEid = Conversion.ConvertStrToInteger(arrayPayeeType[0]);
                                
                                //Ankit Start : Financial Enhancements - Payee Phrase Change
                                if (objSavePayeePhrase != null)
                                {
                                    GetFundsXPayeeColoumnsVal(arrayPayeeType[0], objSavePayeePhrase, out intPhraseTypeCode, out intBeforePayee);
                                    if (!int.Equals(intPhraseTypeCode, -2) && !int.Equals(intBeforePayee, -2))
                                    {
                                        objFundsAutoXPayee.PhraseTypeCode = intPhraseTypeCode;
                                        objFundsAutoXPayee.BeforePayee = intBeforePayee;
                                    }
                                }
                                //Ankit End

                                if (count == 0)
                                {
                                    objFundsAutoXPayee.Payee1099Flag = true;
                                }
                                else
                                {
                                    objFundsAutoXPayee.Payee1099Flag = false;
                                }
                                if (arrayPayeeType.Length > 0)
                                    if (arrayPayeeType[1].Substring(0, 1) == "e" || arrayPayeeType[1].Substring(0, 1) == "o")
                                        arrayPayeeType[1] = arrayPayeeType[1].Substring(1);

                                objFundsAutoXPayee.PayeeTypeCode = Conversion.ConvertStrToInteger(arrayPayeeType[1]);
                            }
                        }
                    }

                    //Ankit Start : Financial Enhancements - Payee Phrase Change
                    if (objSavePayeePhrase != null)
                        base.SysEx.DocumentElement.RemoveChild(objSavePayeePhrase);
                    //Ankit End
                }
                foreach (FundsAutoXPayee OFundsXPayee in objPayeeFundsAuto.FundsAutoXPayeeList)
                {
                    bool bDelete = true;
                    for (int count = 0; count <= array.Length - 1; count++)
                    {
                        arrayPayeeType = array[count].Split("#".ToCharArray()[0]);
                        if (Conversion.ConvertStrToInteger(arrayPayeeType[0]) == OFundsXPayee.PayeeEid)
                        {
                            bDelete = false;
                            break;
                        }
                    }
                    if (bDelete)
                    {
                        objPayeeFundsAuto.FundsAutoXPayeeList.Remove(OFundsXPayee.PayeeRowId);

                    }
                }

                //End Amitosh

			}
            //04/05/2013 - MITS 32221 mcapps2 Start
            //if (objPayeeFundsAuto.FundsAutoXPayeeList.Count > 0)
            //{
            //   objPayeeFundsAuto.SettlementFlag = true;
            //}           
            //04/05/2013 - MITS 32221 mcapps2 End                  

            //mkaran2 - MITS 35403 - Roll up problem in autochecks - Start  
            if (objPayeeFundsAuto != null) // igupta3 Mits#35607
            {
                // akaushik5 Changed for RMA-1713 Starts
                //if (objPayeeFundsAuto.FundsAutoXPayeeList.Count > 1)
                //{
                //    foreach (FundsAuto OFundsAuto in this.FundsAutoBatch.FundsAutoList)
                //    {
                //        OFundsAuto.SettlementFlag = true;
                //    }
                //}
                foreach (FundsAuto OFundsAuto in this.FundsAutoBatch.FundsAutoList)
                {
                    OFundsAuto.SettlementFlag = objPayeeFundsAuto.FundsAutoXPayeeList.Count > 1;
                }
                // akaushik5 Changed for RMA-1713 Ends
            }
            // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            // We still mark the Property as true, as we might be using this flag in any of the reports
            if (objCache.GetCodeId("EFT", "DISTRIBUTION_TYPE") == objPayeeFundsAuto.DstrbnType)
                objPayeeFundsAuto.IsEFTPayment = true;
            // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            //mkaran2 - MITS 35403 - Roll up problem in autochecks - End       
			DateTime dtPaymentEndDate;
			DateTime dtPaymentStartDate = Conversion.ToDate(this.FundsAutoBatch.StartDate); 
            //Add PaymentIntervalDays by kuladeep for Day interval for auto checks mits:19360
            //CalculateDateRange(dtPaymentStartDate,this.FundsAutoBatch.TotalPayments -1 ,this.FundsAutoBatch.PaymentInterval,out dtPaymentEndDate);
            CalculateDateRange(dtPaymentStartDate, this.FundsAutoBatch.TotalPayments - 1, this.FundsAutoBatch.PaymentInterval, this.FundsAutoBatch.PaymentIntervalDays, out dtPaymentEndDate);//Ashish Ahuja RMA-9496       
			this.FundsAutoBatch.EndDate = dtPaymentEndDate.ToString();
            
            		
			objPayeeFAXmlDoc = null;
			objNonThiredPartyPaymentSplitsXmlList = null;
			objThiredPartyPaymentsXmlList = null;
			objFundsAutoList = null;
			objPayeeSplitListElement = null;
			objFuturePaymentsFASplitXmlDoc = null;
			objPayeeSplit = null;
			objPayeeFundsAuto = null;
			objTmpTppFAXmlDoc = null;


           
		}

        //Ankit Start : Financial Enhancements - Payee Phrase Change
        private void GetFundsXPayeeColoumnsVal(string PayeeEid, XmlNode objSavePayeePhrase, out int PhraseTypeCode, out int BeforePayee)
        {
            string strSavePayeePhrase = string.Empty;
            string strCombinedColumnVal = string.Empty;
            string[] arrSavePayeePhrase = null;
            string[] arrSepColumneVal = null;
            PhraseTypeCode = -2;
            BeforePayee = -2;

            strSavePayeePhrase = objSavePayeePhrase.InnerText;
            if (!string.IsNullOrEmpty(strSavePayeePhrase))
            {
                arrSavePayeePhrase = strSavePayeePhrase.Split('@');
                if (arrSavePayeePhrase.Length > 0)
                {
                    strCombinedColumnVal = Array.Find(arrSavePayeePhrase, element => element.StartsWith(PayeeEid + "|", StringComparison.Ordinal));
                    if (!string.IsNullOrEmpty(strCombinedColumnVal.Trim()))
                    {
                        arrSepColumneVal = strCombinedColumnVal.Split('|');
                        if (arrSepColumneVal.Length > 1 && !string.IsNullOrEmpty(arrSepColumneVal[1]))
                        {
                            PhraseTypeCode = Conversion.ConvertStrToInteger(arrSepColumneVal[1]);
                            if (arrSepColumneVal.Length > 2 && !string.IsNullOrEmpty(arrSepColumneVal[2]))
                                BeforePayee = Conversion.ConvertStrToInteger(arrSepColumneVal[2]);
                        }
                    }
                }
            }
        }
        //Ankit End
        //AA 7199
        private void UpdateFundsAutoMailToData(FundsAuto objFundsAuto)
        {
            int iMailToEid = 0;
            string sLastName = string.Empty;
            string sFirstName = string.Empty;
            string sAddr1 = string.Empty;
            string sAddr2 = string.Empty;
            string sAddr3 = string.Empty;
            string sAddr4 = string.Empty;
            string sCity = string.Empty;
            int iStateId = 0;
            string sZipCode = string.Empty;
            string sTaxId = string.Empty;
            int iCountryCode = 0;
            string sCounty = string.Empty;
            string strSearchString = string.Empty;
            string sStateDesc = string.Empty;
            string sStateCode = string.Empty;
            string sCountryDesc = string.Empty;
            string sCountryCode = string.Empty;
            int iAddressId = 0;
            DbReader objReader = null;
            LocalCache objLocalCache = null;
            string sSQL = string.Empty;
            int iIndex = 0;
            DataModelFactory objDataModelFactory = null;
            Address objAddress = null;
            try
            {
                if (m_sConnectionString == "")
                {
                    m_sConnectionString = m_fda.connectionString;
                }

                objLocalCache = new LocalCache(m_sConnectionString, base.ClientId);
                if (objCache.GetShortCode(objFundsAuto.DstrbnType) == "WTR" || objCache.GetShortCode(objFundsAuto.DstrbnType) == "EFT")
                {
                    objFundsAuto.MailToEid = 0;
                    objFundsAuto.MailToEntity.LastName = string.Empty;
                    objFundsAuto.MailToEntity.FirstName = string.Empty;
                    objFundsAuto.MailToAddress.Addr1 = string.Empty;
                    objFundsAuto.MailToAddress.Addr2 = string.Empty;
                    objFundsAuto.MailToAddress.Addr3 = string.Empty;
                    objFundsAuto.MailToAddress.Addr4 = string.Empty;
                    objFundsAuto.MailToAddress.City = string.Empty;
                    objFundsAuto.MailToAddress.State = 0;
                    objFundsAuto.MailToAddress.ZipCode = string.Empty;
                    objFundsAuto.MailToAddress.Country = 0;
                    objFundsAuto.MailToAddress.County = string.Empty;
                }
                iMailToEid = objFundsAuto.MailToEid;
                sLastName = objFundsAuto.MailToEntity.LastName;
                sFirstName = objFundsAuto.MailToEntity.FirstName;
                sAddr1 = objFundsAuto.MailToAddress.Addr1;
                sAddr2 = objFundsAuto.MailToAddress.Addr2;
                sAddr3 = objFundsAuto.MailToAddress.Addr3;
                sAddr4 = objFundsAuto.MailToAddress.Addr4;
                sCity = objFundsAuto.MailToAddress.City;
                iStateId = objFundsAuto.MailToAddress.State;
                sZipCode = objFundsAuto.MailToAddress.ZipCode;
                iCountryCode = objFundsAuto.MailToAddress.Country;
                sCounty = objFundsAuto.MailToAddress.County;

                objCache.GetStateInfo(iStateId, ref sStateCode, ref sStateDesc);
                objCache.GetCodeInfo(iCountryCode, ref sCountryCode, ref sCountryDesc);

                strSearchString = sAddr1 + sAddr2 + sAddr3 + sAddr4 + sCity + sStateDesc + sCountryDesc + sZipCode + "0";
                strSearchString = strSearchString.Replace(" ", "").ToLower();

                if (iMailToEid == 0)
                {

                    sSQL = " SELECT ENTITY_ID FROM ENTITY WHERE LAST_NAME ='" + sLastName.Trim().Replace("'", "''") + "'";
                    sFirstName = sFirstName.Trim().Replace("'", "''");

                    if (sFirstName == "")
                        sSQL += " AND (FIRST_NAME = '' OR FIRST_NAME IS NULL)";
                    else
                        sSQL += " AND FIRST_NAME = '" + sFirstName + "'";

                    sTaxId = objFundsAuto.MailToEntity.TaxId.Trim();

                    iIndex = sTaxId.IndexOf("-");
                    while (iIndex != -1)
                    {
                        sTaxId = sTaxId.Substring(0, iIndex) + sTaxId.Substring(iIndex + 1);
                        iIndex = sTaxId.IndexOf("-");
                    }
                    sTaxId = sTaxId.Replace("#", "");
                    sTaxId = sTaxId.Trim();
                    if (sTaxId != "")
                        sSQL += " AND TAX_ID IN ('" + sTaxId + "','" + Int32.Parse(sTaxId).ToString("00-0000000") + "','" + Int32.Parse(sTaxId).ToString("000-00-0000") + "')";

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            iMailToEid = Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_ID"), base.ClientId);
                            objFundsAuto.MailToEid = iMailToEid;

                        }
                        objReader.Close();
                    }

                }  
               
                {
                    XmlNode nodeDup = null;
                    XmlNode nodeAddress = null;
                    XmlNode nodeSearchString = null;
                    iAddressId = CommonFunctions.CheckAddressDuplication(strSearchString, m_sConnectionString, base.ClientId);
                
                    objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, base.ClientId);
                    
                    objFundsAuto.MailToAddress.Isdupeoverride = this.SysEx.SelectSingleNode("//dupeoverride").InnerText;
                    objFundsAuto.MailToAddress.SearchString = strSearchString;
                   
                 

                   
                }
                if (iMailToEid == 0)
                {
                    objFundsAuto.MailToEntity.LastName = sLastName;
                    objFundsAuto.MailToEntity.FirstName = sFirstName;
                    objFundsAuto.MailToAddress.Addr1 = sAddr1;
                    objFundsAuto.MailToAddress.Addr2 = sAddr2;
                    objFundsAuto.MailToAddress.Addr3 = sAddr3;
                    objFundsAuto.MailToAddress.Addr4 = sAddr4;
                    objFundsAuto.MailToAddress.City = sCity;
                    objFundsAuto.MailToAddress.State = iStateId;
                    objFundsAuto.MailToAddress.ZipCode = sZipCode;
                    objFundsAuto.MailToAddress.Country = iCountryCode;
                    objFundsAuto.MailToAddress.County = sCounty;
                    objFundsAuto.MailToAddressId = iAddressId;
                    if (!objFundsAuto.Context.InternalSettings.SysSettings.UseEntityRole)
                        objFundsAuto.MailToEntity.EntityTableId = objLocalCache.GetTableId("OTHER_PEOPLE");
                    objFundsAuto.MailToEntity.EntityApprovalStatusCode = objLocalCache.GetCodeId("P", "ENTITY_APPRV_REJ");

                }
                else
                {
                    objFundsAuto.MailToEid = iMailToEid;
                    objFundsAuto.MailToAddress.Addr1 = sAddr1;
                    objFundsAuto.MailToAddress.Addr2 = sAddr2;
                    objFundsAuto.MailToAddress.Addr3 = sAddr3;
                    objFundsAuto.MailToAddress.Addr4 = sAddr4;
                    objFundsAuto.MailToAddress.City = sCity;

                    objFundsAuto.MailToAddress.State = iStateId;
                    objFundsAuto.MailToAddress.Country = iCountryCode;
                    objFundsAuto.MailToAddress.ZipCode = sZipCode;
                    objFundsAuto.MailToAddress.County = sCounty;
                    objFundsAuto.MailToAddressId = iAddressId;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            finally
            {
            }
        }
        //pmittal5 Mits 15308 05/06/09 - Adding a new Payee Entity 
        private void UpdateFundsAutoPayeeEntity(FundsAuto objFundsAuto)
        {
            int iPayeeEid = 0;
            string sLastName = string.Empty;
            string sFirstName = string.Empty;
            string sAddr1 = string.Empty;
            string sAddr2 = string.Empty;
            string sAddr3 = string.Empty;// JIRA 6420 pkandhari
            string sAddr4 = string.Empty;// JIRA 6420 pkandhari
            string sCity = string.Empty;
            int iStateId = 0;
            string sZipCode = string.Empty;
            string sTaxId = string.Empty;

            DbReader objReader = null;
            LocalCache objLocalCache = null;
            string sSQL = string.Empty;
            int iIndex = 0;
            
            try
            {
                if (m_sConnectionString == "")
                {
                    m_sConnectionString = m_fda.connectionString;
                }

                objLocalCache = new LocalCache(m_sConnectionString,base.ClientId);

                iPayeeEid = objFundsAuto.PayeeEid;

                sLastName = objFundsAuto.LastName;
                sFirstName = objFundsAuto.FirstName;
                sAddr1 = objFundsAuto.Addr1;
                sAddr2 = objFundsAuto.Addr2;
                sAddr3 = objFundsAuto.Addr3;// JIRA 6420 pkandhari
                sAddr4 = objFundsAuto.Addr4;// JIRA 6420 pkandhari
                sCity = objFundsAuto.City;
                iStateId = objFundsAuto.StateId;
                sZipCode = objFundsAuto.ZipCode;

                //pmital5 Mits 15308 05/07/09 - Save Tax Id if New Entity is added 
                if (objFundsAuto.PayeeEntity.TaxId == "")  
                    objFundsAuto.PayeeEntity.TaxId = base.SysEx.DocumentElement.SelectSingleNode("//PayeeEntity/TaxId").InnerText;
                 
                sTaxId = objFundsAuto.PayeeEntity.TaxId.Trim();
                
                // If a tax id is provided, we will search with it along with the name, else we will
                // Just use last and first name.
                if (iPayeeEid == 0)
                {

                    sSQL = " SELECT ENTITY_ID FROM ENTITY WHERE LAST_NAME ='" + sLastName.Trim().Replace("'", "''") + "'";
                    sFirstName = sFirstName.Trim().Replace("'", "''");

                    if (sFirstName == "")
                        sSQL += " AND (FIRST_NAME = '' OR FIRST_NAME IS NULL)";
                    else
                        sSQL += " AND FIRST_NAME = '" + sFirstName + "'";

                    sTaxId = objFundsAuto.PayeeEntity.TaxId.Trim();

                    iIndex = sTaxId.IndexOf("-");
                    while (iIndex != -1)
                    {
                        sTaxId = sTaxId.Substring(0, iIndex) + sTaxId.Substring(iIndex + 1);
                        iIndex = sTaxId.IndexOf("-");
                    }

                    sTaxId = sTaxId.Trim();
                    if (sTaxId != "")
                        sSQL += " AND TAX_ID IN ('" + sTaxId + "','" + Int32.Parse(sTaxId).ToString("00-0000000") + "','" + Int32.Parse(sTaxId).ToString("000-00-0000") + "')";

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            iPayeeEid = Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_ID"), base.ClientId);
                            objFundsAuto.PayeeEid = iPayeeEid;

                            objFundsAuto.LastName = sLastName;
                            objFundsAuto.FirstName = sFirstName;
                            objFundsAuto.Addr1 = sAddr1;
                            objFundsAuto.Addr2 = sAddr2;
                            objFundsAuto.Addr3 = sAddr3;// JIRA 6420 pkandhari
                            objFundsAuto.Addr4 = sAddr4;// JIRA 6420 pkandhari
                            objFundsAuto.City = sCity;
                            objFundsAuto.StateId = iStateId;
                            objFundsAuto.ZipCode = sZipCode;
                        }
                        objReader.Close();
                    }

                    if (objFundsAuto.PayeeEid == 0)
                    {
                        objFundsAuto.PayeeEntity.LastName = sLastName;
                        objFundsAuto.PayeeEntity.FirstName = sFirstName;
                        objFundsAuto.PayeeEntity.Addr1 = sAddr1;
                        objFundsAuto.PayeeEntity.Addr2 = sAddr2;
                        objFundsAuto.PayeeEntity.Addr3 = sAddr3;// JIRA 6420 pkandhari
                        objFundsAuto.PayeeEntity.Addr4 = sAddr4;// JIRA 6420 pkandhari
                        objFundsAuto.PayeeEntity.City = sCity;
                        objFundsAuto.PayeeEntity.StateId = iStateId;
                        objFundsAuto.PayeeEntity.ZipCode = sZipCode;
                        if (!objFundsAuto.Context.InternalSettings.SysSettings.UseEntityRole)      //avipinsrivas Start : Worked for 7535 (Issue of 4634 - Epic 340)
                            objFundsAuto.PayeeEntity.EntityTableId = objLocalCache.GetTableId("OTHER_PEOPLE");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            finally
            {
            }
         }

        /// <summary>
        /// Reserve Amount Detail Structure
        /// </summary>
        private struct ReserveAmount
        {
            /// <summary>
            /// Private variable for Reserve Type Code.
            /// </summary>
            private int iReserveTypeCode;
            /// <summary>
            /// Private variable for Amount.
            /// </summary>
            private double dblAmount;
            /// <summary>
            /// Read/Write property for ReserveTypeCode.
            /// </summary>
            internal int ReserveTypeCode
            {
                get
                {
                    return (iReserveTypeCode);
                }
                set
                {
                    iReserveTypeCode = value;
                }
            }
            /// <summary>
            /// Read/Write property for Amount.
            /// </summary>
            internal double Amount
            {
                get
                {
                    return (dblAmount);
                }
                set
                {
                    dblAmount = value;
                }
            }
        }

        private bool InsuffReserves(FundsAuto p_FundsAuto, int p_iLOB)
        {
            ReserveAmount structReserveAmount;

            Claim objClaim = null;
            SessionManager objSessionManager = null;
            ArrayList arrlstReserveAmount = new ArrayList();
            
            double dblTempAmount = 0;
            double dblBalance = 0;

            int iIndex = 0;
            int iReserveTypeIndex = 0;

            bool bFound = false;
            try
            {
                objSessionManager = m_fda.GetSessionObject();
                
                if (p_FundsAuto.Context.InternalSettings.ColLobSettings[p_iLOB].InsufFundsAutoFlag)
                {
                    foreach (FundsAutoSplit objAutoSplit in p_FundsAuto.AutoSplitList)
                    {
                        bFound = false;

                        for (iIndex = 0; iIndex < arrlstReserveAmount.Count; iIndex++)
                        {
                            structReserveAmount = (ReserveAmount)arrlstReserveAmount[iIndex];
                            if (structReserveAmount.ReserveTypeCode == objAutoSplit.ReserveTypeCode)
                            {
                                double dTempAmount = structReserveAmount.Amount + objAutoSplit.Amount;

                                arrlstReserveAmount.RemoveAt(iIndex);
                                structReserveAmount = new ReserveAmount();
                                structReserveAmount.ReserveTypeCode = objAutoSplit.ReserveTypeCode;
                                structReserveAmount.Amount = dTempAmount;
                                arrlstReserveAmount.Insert(iIndex, structReserveAmount);
                                bFound = true;
                            }
                        }
                        if (!bFound)
                        {
                            structReserveAmount = new ReserveAmount();
                            structReserveAmount.ReserveTypeCode = objAutoSplit.ReserveTypeCode;
                            structReserveAmount.Amount = objAutoSplit.Amount;
                            arrlstReserveAmount.Add(structReserveAmount);
                        }
                    }
                }

                objClaim = (Claim)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_FundsAuto.ClaimId);

                if (p_FundsAuto.Context.LocalCache.GetShortCode(p_FundsAuto.Context.LocalCache.GetRelatedCodeId(objClaim.ClaimStatusCode)) == "O")
                {
                    if (p_FundsAuto.Context.InternalSettings.ColLobSettings[p_iLOB].InsufFundsAutoFlag)
                    {
                        if (arrlstReserveAmount.Count > 0)
                        {
                            for (iIndex = 0; iIndex < arrlstReserveAmount.Count; iIndex++)
                            {
                                structReserveAmount = (ReserveAmount)arrlstReserveAmount[iIndex];
                                dblBalance = this.FundManagerObject.GetReserveBalance(p_FundsAuto.ClaimId, p_FundsAuto.ClaimantEid, p_FundsAuto.UnitId, structReserveAmount.ReserveTypeCode, p_iLOB);
                                if (structReserveAmount.Amount * (this.FundsAutoBatch.TotalPayments - this.FundsAutoBatch.PaymentsToDate) - dblBalance > 0.009 && p_FundsAuto.Context.InternalSettings.ColLobSettings[p_iLOB].NoNegResAdjFlag == -1)
                                {//InsuffReserve, non neg flag set
                                    dblTempAmount = this.FundManagerObject.GetCurrentReserveAmount(p_FundsAuto.ClaimId, p_FundsAuto.ClaimantEid, p_FundsAuto.UnitId, structReserveAmount.ReserveTypeCode);
                                    XmlDocument objInsAmntXml = AppendInsResrvAmntXml(dblBalance, structReserveAmount.Amount * this.FundsAutoBatch.TotalPayments, dblTempAmount, structReserveAmount.ReserveTypeCode, p_FundsAuto);
                                    
                                    base.ResetSysExData("InsuffReserveNonNeg", "true");
                                    base.ResetSysExData("InsuffReserve", "false");

                                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), "There are insufficient reserves for this payment.", BusinessAdaptorErrorType.Error);

                                    //TODO: This code is not mapped to the user session therefore user context is lost
                                    objSessionManager.SetBinaryItem("InsufAmntXml", Utilities.BinarySerialize(objInsAmntXml.OuterXml), base.ClientId);

                                    return false;
                                }
                                else if (structReserveAmount.Amount * (this.FundsAutoBatch.TotalPayments - this.FundsAutoBatch.PaymentsToDate) - dblBalance > 0.009) // && base.GetSysExDataNodeText("Reason") != "AUTO-ADJUST")/ R5 PS2 Merge: 10/01/2009
                                {
                                    string sSkipReserveTypes = base.GetSysExDataNodeText("Skipreservetypes");
                                    iReserveTypeIndex = sSkipReserveTypes.IndexOf(structReserveAmount.ReserveTypeCode.ToString());
                                    if (iReserveTypeIndex == -1)
                                    {
                                        dblTempAmount = this.FundManagerObject.GetCurrentReserveAmount(p_FundsAuto.ClaimId, p_FundsAuto.ClaimantEid, p_FundsAuto.UnitId, structReserveAmount.ReserveTypeCode);
                                        XmlDocument objInsAmntXml = AppendInsResrvAmntXml(dblBalance, structReserveAmount.Amount * this.FundsAutoBatch.TotalPayments, dblTempAmount, structReserveAmount.ReserveTypeCode, p_FundsAuto);
                                    
                                        base.ResetSysExData("InsuffReserveNonNeg", "false");
                                        base.ResetSysExData("InsuffReserve", "true");

                                        //TODO: This code is not mapped to the user session therefore user context is lost
                                        objSessionManager.SetBinaryItem("InsufAmntXml", Utilities.BinarySerialize(objInsAmntXml.OuterXml), base.ClientId);

                                        return false;
                                    }
                                    else
                                    {
                                        base.ResetSysExData("InsuffReserveNonNeg", "false");
                                        base.ResetSysExData("InsuffReserve", "false");
                                    }
                                }
                                else
                                {
                                    base.ResetSysExData("InsuffReserveNonNeg", "false");
                                    base.ResetSysExData("InsuffReserve", "false");
                                }
                            }
                        }
                    }
                    base.ResetSysExData("Skipreservetypes", "");
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AutoClaimChecksForm.Validate.Error", base.ClientId), p_objEx);
            }
            finally
            {
                if(objSessionManager != null)
                    objSessionManager.Dispose();
                if (objClaim != null)
                    objClaim.Dispose();
            }
            return true;
		}

        /// <summary>
        /// Get Xml For Insufficient Reserve Amount
        /// </summary>
        /// <param name="p_dblBalance">Balance Amount</param>
        /// <param name="p_dblPayment">Payment Amount</param>
        /// <param name="p_dblReserve">Reserve Amount</param>
        /// <param name="p_iReserveTypeCode">Reserve Type Code</param>
        private XmlDocument AppendInsResrvAmntXml(double p_dblBalance, double p_dblPayment, double p_dblReserve, int p_iReserveTypeCode, FundsAuto p_FundsAuto)
        {
            XmlDocument objDoc;

            double dblAddReserve = 0.0;
            double dblSetReserve = 0.0;
            double dReserveLimit = 0.0;
            string sXmlForInsResrvAmnt = "";
            string sDesc = "";
            string sShortCode = "";
            string sIsResLimited = "No";

            LobSettings objLobSettings = null;
            ColLobSettings objColLobSettings = null;
            objColLobSettings = new ColLobSettings(m_sConnectionString, base.ClientId);
            Claim objClaim = (Claim)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(p_FundsAuto.ClaimId);

            int iLob = objClaim.LineOfBusCode;
            objClaim = null;
            objLobSettings = objColLobSettings[iLob];

            if (objLobSettings != null && objLobSettings.ResLmtFlag)
            {
                if (objLobSettings.ResLmtFlag == true)
                {
                    sIsResLimited = "Yes";
                }
            }

            try
            {
                dblAddReserve = p_dblPayment - p_dblBalance;
                if (dblAddReserve < 0.0)
                {
                    dblAddReserve = -dblAddReserve;
                }
                dblSetReserve = p_dblBalance - (p_dblPayment + p_dblReserve);
                if (dblSetReserve < 0.0)
                {
                    dblSetReserve = -dblSetReserve;
                }
                dReserveLimit = GetReserveLimits(p_FundsAuto.ClaimId, p_iReserveTypeCode, m_fda.userID, m_fda.groupID);
                //Deb Multi Currency
                string sClaimCurrency = string.Empty;
                if (p_FundsAuto.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                {
                    //double dExhRateBaseToClm = FundsObject.Context.DbConn.ExecuteDouble(string.Format("SELECT EXCHANGE_RATE FROM CURRENCY_RATE WHERE SOURCE_CURRENCY_CODE={0} AND DESTINATION_CURRENCY_CODE={1}", FundsObject.Context.InternalSettings.SysSettings.BaseCurrencyType, FundsObject.ClaimCurrencyType));
                    if(p_FundsAuto.BaseToClaimCurRate == 0)
                        p_FundsAuto.BaseToClaimCurRate = CommonFunctions.ExchangeRateSrc2Dest(p_FundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, p_FundsAuto.ClaimCurrencyType, m_sConnectionString, base.ClientId);
                    p_dblBalance = p_dblBalance * p_FundsAuto.BaseToClaimCurRate;
                    p_dblReserve = p_dblReserve * p_FundsAuto.BaseToClaimCurRate;
                    p_dblPayment = p_dblPayment * p_FundsAuto.BaseToClaimCurRate;
                    dblSetReserve = dblSetReserve * p_FundsAuto.BaseToClaimCurRate;
                    dblAddReserve = dblAddReserve * p_FundsAuto.BaseToClaimCurRate;
                    dReserveLimit = dReserveLimit * p_FundsAuto.BaseToClaimCurRate;
                    p_FundsAuto.Context.LocalCache.GetCodeInfo(p_FundsAuto.ClaimCurrencyType, ref sShortCode, ref sDesc);
                    sClaimCurrency = sDesc.Split('|')[1];
                    sShortCode = string.Empty;
                    sDesc = string.Empty;
                }
                else
                {
                    p_FundsAuto.Context.LocalCache.GetCodeInfo(p_FundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, ref sShortCode, ref sDesc);
                    sClaimCurrency = sDesc.Split('|')[1];
                    sShortCode = string.Empty;
                    sDesc = string.Empty;
                }
                //Deb Multi Currency
                sXmlForInsResrvAmnt = "~insufres scr=\"1\" userid=\"" + p_FundsAuto.Context.RMUser.UserId.ToString() + "\" groupid=\"" + p_FundsAuto.Context.RMUser.GroupId.ToString() + "\" claimid=\""
                    + p_FundsAuto.ClaimId.ToString() + "\"|"
                    + "~unit|" + p_FundsAuto.UnitId.ToString() + "~/unit|"
                    + "~claimant|" + p_FundsAuto.ClaimantEid.ToString() + "~/claimant|"
                    + "~balance|" + Math.Round(p_dblBalance, 2, MidpointRounding.AwayFromZero) + "~/balance|"
                    + "~reserve|" + Math.Round(p_dblReserve, 2, MidpointRounding.AwayFromZero) + "~/reserve|"
                    + "~payment|" + Math.Round(p_dblPayment, 2, MidpointRounding.AwayFromZero) + "~/payment|"
                    + "~setreserve|" + Math.Round(dblSetReserve, 2, MidpointRounding.AwayFromZero) + "~/setreserve|"
                    + "~addreserve|" + Math.Round(dblAddReserve, 2, MidpointRounding.AwayFromZero) + "~/addreserve|"
                    + "~newamount|0~/newamount|"
                    + "~isreslimit|" + sIsResLimited + "~/isreslimit|"
                    + "~reslimit|" + Math.Round(dReserveLimit, 2, MidpointRounding.AwayFromZero) + "~/reslimit|" 
                    //Deb Multi Currency
                    +"~ClaimCurrency|" + sClaimCurrency + "~/ClaimCurrency|";
                    //Deb Multi Currency


                p_FundsAuto.Context.LocalCache.GetCodeInfo(p_iReserveTypeCode, ref sShortCode, ref sDesc, base.Adaptor.userLogin.objUser.NlsCode); //Aman Ml Change

                sXmlForInsResrvAmnt += "~rcode id=\"" + p_iReserveTypeCode.ToString()
                    + "\"|" + sDesc + "~/rcode|" + "~/insufres|";

                sXmlForInsResrvAmnt = sXmlForInsResrvAmnt.Replace('~', '<');
                sXmlForInsResrvAmnt = sXmlForInsResrvAmnt.Replace('|', '>');

                objDoc = new XmlDocument();
                objDoc.LoadXml(sXmlForInsResrvAmnt);


                return objDoc;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AutoClaimChecksForm.Validate.Error", base.ClientId), p_objEx);
            }
            finally
            {

            }
        }
        private double GetReserveLimits(int p_iClaimId, int p_iReserveTypeCode, int p_iUserId, int p_iGroupId)
        {
            double dResLimit = 0.0;
            string sSQL = null;

            //Database objects
            DbReader objReader = null;
            DbConnection objConn = null;

            m_sConnectionString = m_fda.connectionString;

            try
            {
                sSQL = "SELECT MAX(MAX_AMOUNT)  MAX_AMT FROM RESERVE_LIMITS, CLAIM";
                sSQL = sSQL + " WHERE (USER_ID = " + p_iUserId;
                if (p_iGroupId != 0)
                {
                    sSQL = sSQL + " OR GROUP_ID = " + p_iGroupId;
                }
                sSQL = sSQL + " ) AND CLAIM.CLAIM_ID = " + p_iClaimId;
                sSQL = sSQL + " AND CLAIM.LINE_OF_BUS_CODE = RESERVE_LIMITS.LINE_OF_BUS_CODE";
                sSQL = sSQL + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode;

                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();

                objReader = objConn.ExecuteReader(sSQL);

                if (objReader.Read())
                {
                    dResLimit = Conversion.ConvertStrToDouble(objReader.GetValue("MAX_AMT").ToString());
                }
                objReader.Close();
                objConn.Close();
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
            return dResLimit;
        }

        private bool ValidatePaymentAmount(FundsAuto p_FundsAuto)
        {
            DbReader objReader = null;
            
            string sSql = "";
            string sMaxYearText = "";
            string sTransYear = "";

            int iYear = 0;

            long lPayeeType = p_FundsAuto.PayeeTypeCode;
            long lTransCode = 0;

            double dWeeklyRate = 0;
            double dCompRate = 0;
            double dPaymentAmount = 0;
            double dMaxCompRate = 0;

            bool bFlagPTDPS = false;
            bool bFlagPTD = false;
            bool bFlagTrans = false;
            bool bCompRateFromFL = false;
            try
            {
                if (lPayeeType == p_FundsAuto.Context.LocalCache.GetCodeId("C", "PAYEE_TYPE"))
                {
                    sSql = "SELECT COMP_RATE FROM CLAIM WHERE CLAIM_NUMBER = '" + p_FundsAuto.ClaimNumber + "'";
                    objReader = DbFactory.GetDbReader(p_FundsAuto.Context.RMDatabase.ConnectionString, sSql);
                    if (objReader != null && objReader.Read())
                    {
                        dCompRate = objReader.GetDouble("COMP_RATE");
                    }

                    foreach (FundsAutoSplit objAutoSplit in p_FundsAuto.AutoSplitList)
                    {
                        lTransCode = objAutoSplit.TransTypeCode;
                        if (lTransCode == p_FundsAuto.Context.LocalCache.GetCodeId("TPD", "TRANS_TYPES")
                            || lTransCode == p_FundsAuto.Context.LocalCache.GetCodeId("TTD", "TRANS_TYPES")
                            || lTransCode == p_FundsAuto.Context.LocalCache.GetCodeId("TR", "TRANS_TYPES")
                            || lTransCode == p_FundsAuto.Context.LocalCache.GetCodeId("PPD", "TRANS_TYPES")
                            || lTransCode == p_FundsAuto.Context.LocalCache.GetCodeId("PTD", "TRANS_TYPES")
                            || lTransCode == p_FundsAuto.Context.LocalCache.GetCodeId("SI", "TRANS_TYPES")
                            || lTransCode == p_FundsAuto.Context.LocalCache.GetCodeId("DB", "TRANS_TYPES"))
                        {
                            if (lTransCode == p_FundsAuto.Context.LocalCache.GetCodeId("PTD", "TRANS_TYPES"))
                            {
                                bFlagPTD = true;
                            }
                            dPaymentAmount = dPaymentAmount + objAutoSplit.Amount;
                            bFlagTrans = true;
                        }
                    }

                    if (bFlagPTD)
                    {
                        foreach (FundsAutoSplit objAutoSplit in p_FundsAuto.AutoSplitList)
                        {
                            if (objAutoSplit.TransTypeCode == p_FundsAuto.Context.LocalCache.GetCodeId("PS", "TRANS_TYPES"))
                            {
                                dPaymentAmount = dPaymentAmount + objAutoSplit.Amount;
                                bFlagPTDPS = true;
                            }
                        }
                    }
                    if (bFlagPTDPS) //in case of PTD and PS
                    {
                        sSql = "SELECT WEEKLY_RATE FROM PERSON_INVOLVED WHERE PI_EID = " + p_FundsAuto.ClaimantEid;
                        sSql = sSql + " AND EVENT_ID = (SELECT EVENT_ID FROM CLAIM WHERE CLAIM_ID = " + p_FundsAuto.ClaimId + ")";

                        objReader = DbFactory.GetDbReader(p_FundsAuto.Context.RMDatabase.ConnectionString, sSql);
                        if (objReader != null && objReader.Read())
                        {
                            dWeeklyRate = objReader.GetDouble("WEEKLY_RATE");
                        }
                        objReader.Close();

                        sTransYear = DateTime.Now.Year.ToString();
                        sSql = "SELECT MAX_RATE_AMT, MAX_YEAR_TEXT FROM WC_MAX_RATE_CALC WHERE MAX_YEAR_TEXT  = '" + sTransYear + "'";
                        objReader = DbFactory.GetDbReader(p_FundsAuto.Context.RMDatabase.ConnectionString, sSql);
                        if (objReader != null && objReader.Read())
                        {
                            dMaxCompRate = objReader.GetDouble("MAX_RATE_AMT");
                            iYear = Conversion.ConvertObjToInt(objReader.GetValue("MAX_YEAR_TEXT"), base.ClientId);
                        }
                        objReader.Close();

                        dCompRate = Math.Round(dWeeklyRate * 0.6667, 2, MidpointRounding.AwayFromZero);

                        if (dCompRate > dMaxCompRate)
                        {
                            dCompRate = dMaxCompRate;
                            bCompRateFromFL = true;
                        }

                        if (dCompRate < dPaymentAmount)
                        {
                            if (bCompRateFromFL)
                            {
                                XmlNode tempNode = base.SysEx.SelectSingleNode("//CompRate");
                                if (tempNode != null)
                                {
                                    tempNode.InnerText = "CompRateFromFL_" + iYear.ToString() + "_" + dMaxCompRate.ToString();
                                }
                                return false;
                            }
                            else
                            {
                                XmlNode tempNode = base.SysEx.SelectSingleNode("//CompRate");
                                if (tempNode != null)
                                {
                                    tempNode.InnerText = "CompRate_" + dCompRate.ToString();
                                }
                                return false;
                            }
                        }
                    }
                    else //case with no combination of PTD and PS
                    {
                        if (bFlagTrans)
                        {
                            if (dCompRate < dPaymentAmount)
                            {
                                sSql = "SELECT DATE_OF_EVENT FROM EVENT , CLAIM  WHERE EVENT.EVENT_ID = CLAIM.EVENT_ID AND CLAIM.CLAIM_NUMBER = '" + p_FundsAuto.ClaimNumber + "'";
                                objReader = DbFactory.GetDbReader(p_FundsAuto.Context.RMDatabase.ConnectionString, sSql);
                                if (objReader != null && objReader.Read())
                                {
                                    sMaxYearText = objReader.GetString("DATE_OF_EVENT").Substring(0, 4);
                                }
                                objReader.Close();

                                sSql = "SELECT MAX_RATE_AMT, MAX_YEAR_TEXT FROM WC_MAX_RATE_CALC WHERE MAX_YEAR_TEXT  = " + sMaxYearText;
                                objReader = DbFactory.GetDbReader(p_FundsAuto.Context.RMDatabase.ConnectionString, sSql);
                                if (objReader != null && objReader.Read())
                                {
                                    dMaxCompRate = objReader.GetDouble("MAX_RATE_AMT");
                                    iYear = Conversion.ConvertStrToInteger(objReader.GetString("MAX_YEAR_TEXT"));
                                }
                                objReader.Close();

                                if (dCompRate >= dMaxCompRate)
                                {
                                    //m_arrlstConfirmations.Add("CompRateFromFL_" + iYear.ToString() + "_" + dCompRate.ToString());
                                    XmlNode tempNode = base.SysEx.SelectSingleNode("//CompRate");
                                    if (tempNode != null)
                                    {
                                        tempNode.InnerText = "CompRateFromFL_" + iYear.ToString() + "_" + dMaxCompRate.ToString();
                                    }
                                    return false;
                                }
                                else
                                {
                                    //m_arrlstConfirmations.Add("CompRate_" + dCompRate.ToString());
                                    XmlNode tempNode = base.SysEx.SelectSingleNode("//CompRate");
                                    if (tempNode != null)
                                    {
                                        tempNode.InnerText = "CompRate_" + dCompRate.ToString();
                                    }
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AutoClaimChecksForm.Validate.Error", base.ClientId), p_objEx);
            }
            return true;
        }

        //Add Day interval 'p_iPaymentIntervalDays' for auto checks by kuladeep mits:19360
        private void CalculateDateRange(DateTime p_dtFromDate, int p_iCount, int p_iPaymentIntervalType, int p_iPaymentIntervalDays, out DateTime p_dtToDate)
        {
            p_dtToDate = p_dtFromDate;

            // TODO - Optimize
            if (p_iPaymentIntervalType == this.objCache.GetCodeId("PD", "PERIOD_TYPES"))
            {
                //Add Day interval for auto checks by kuladeep mits:19360
                //p_dtToDate = p_dtFromDate.AddDays(p_iCount);
                p_dtToDate = p_dtFromDate.AddDays(p_iPaymentIntervalDays * p_iCount);
            }
			else if(p_iPaymentIntervalType==this.objCache.GetCodeId("PW", "PERIOD_TYPES"))
			{
				p_dtToDate = p_dtFromDate.AddDays(7 * p_iCount);	
			}
			else if(p_iPaymentIntervalType==this.objCache.GetCodeId("PB", "PERIOD_TYPES"))
			{
				p_dtToDate = p_dtFromDate.AddDays(14 * p_iCount);
			}
			else if(p_iPaymentIntervalType==this.objCache.GetCodeId("PM", "PERIOD_TYPES"))
			{
                p_dtToDate = p_dtFromDate.AddMonths(p_iCount);
			}
			else if(p_iPaymentIntervalType==this.objCache.GetCodeId("PQ", "PERIOD_TYPES"))
			{
                p_dtToDate = p_dtFromDate.AddMonths(3 * p_iCount);
                //p_dtToDate = p_dtStartDate.AddMonths(3 * p_iCountRows);//Ashish Ahuja RMA-9496
			}
			else if(p_iPaymentIntervalType==this.objCache.GetCodeId("PY", "PERIOD_TYPES"))
			{
				p_dtToDate = p_dtFromDate.AddYears(p_iCount);
			}		

            //rupal:start, MITS 26022, after calculating date range, we need to again check for dates based on schedule dates setup
            p_dtToDate = AdjustPrintDate(this.FundsAutoBatch.AdjustPrintDatesFlag, p_dtToDate);
            //rupal:end
		}

		#endregion 

		//Changed by Gagan for MITS 11991 : Start        

        #region Check Duplicate Payments        
 
        /// <summary>
        /// Checks for Duplicate Payments while saving auto checks
        /// </summary>
        /// <returns>Whether duplicate checks paymets found</returns>
        public bool CheckForDuplicatePayments(FundsAuto p_objFundsAuto)
        {
            string sSQL = "";
            string sFundsAutoSQL = "";
            DbReader objReader = null;
            Entity objEntity = null;            
         
            bool bCheckForDupPayments = false;
            bool bCheckDupPay = false;
            int iPayCriteria = 0;
            int iNumDays = 0;
            string sDatePast = "";
            string sDateFuture = "";            
            int iTransTypeCode = 0;            
            bool bFlag = false;                      
            DbReader objReader2 = null;
            int iClaimID = 0;            
            string sTaxId = "";            
            string sPayeeEIDs = "";
            DateTime datDate;
            DateTime datDatePast;
            DateTime datDateFuture;
            ArrayList arrlstTransID = null;
            ArrayList arrlstAutoTransID = null;
            double dblAmount = 0.0;
            string sInvoiceNumbers = "";
            string sComma = "";
            string sTempValue = string.Empty;
            int iLastTransId = 0;
            int iLastAutoTransId = 0;
            int iLastAutoBatchId = 0;
            int iTransId = 0;
            int iAutoTransId = 0;
            int iAutoBatchId = 0;
            bool bExit = false;            
            bool bEntityFound = false;
            string sLastName = "";
            string sFirstName = "";

            StringBuilder sTempSql = null;

            
                arrlstTransID = new ArrayList();
                arrlstAutoTransID = new ArrayList();
                sSQL = "SELECT DUPLICATE_FLAG,DUP_PAY_CRITERIA,DUP_NUM_DAYS FROM CHECK_OPTIONS";

                /*rs = DB_CreateRecordset(dbODBC1, SQL, DB_FORWARD_ONLY, 0)
                *****************/
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        bCheckDupPay = objReader.GetBoolean("DUPLICATE_FLAG");
                        iPayCriteria = Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("DUP_PAY_CRITERIA"), base.ClientId);
                        iNumDays = Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("DUP_NUM_DAYS"), base.ClientId);
                        
                    }

                    objReader.Close();
                }

                if (bCheckDupPay == false)
                    return false;

                sSQL = "SELECT FUNDS.TRANS_ID,FUNDS.TRANS_DATE FROM FUNDS, FUNDS_TRANS_SPLIT "
                         + " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID ";


                sFundsAutoSQL = "SELECT FUNDS_AUTO.AUTO_TRANS_ID,FUNDS_AUTO.PRINT_DATE,FUNDS_AUTO.AUTO_BATCH_ID";
                sFundsAutoSQL += " FROM FUNDS_AUTO, FUNDS_AUTO_SPLIT ";
                sFundsAutoSQL += " WHERE FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID";

                if (iPayCriteria != 7 && iPayCriteria != 10 && iPayCriteria != 11 && iPayCriteria != 13 && iPayCriteria != 14)
                {
                    sSQL += " AND FUNDS.CLAIM_ID = " +  p_objFundsAuto.ClaimId;
                    sFundsAutoSQL += " AND FUNDS_AUTO.CLAIM_ID = " + p_objFundsAuto.ClaimId;
                }

                if (iPayCriteria != 7 && iPayCriteria != 10 && iPayCriteria != 11 && iPayCriteria != 13 && iPayCriteria != 14 && iPayCriteria != 15)
                {
                    sSQL += " AND FUNDS.PAYEE_EID = " + p_objFundsAuto.PayeeEid;
                    sFundsAutoSQL += " AND FUNDS_AUTO.PAYEE_EID = " + p_objFundsAuto.PayeeEid;
                }
                
                #region Switch on PayCriteria

                switch (iPayCriteria)
                {
                    //If Trim$(txtTransDate) = "" Then bCheckForDupPayments = False: GoTo hExit
                    case 1:
                        if (this.FundsAutoBatch.StartDate.Trim() == "")
                        {
                            bCheckForDupPayments = false;
                            return (false);
                        }

                        datDate = Conversion.ToDate(this.FundsAutoBatch.StartDate);  
                        //datDate = Convert.ToDateTime(Conversion.GetDBDateFormat(this.FundsAutoBatch.StartDate, "d"));

                        datDatePast = datDate.AddDays((double)-iNumDays);
                        datDateFuture = datDate.AddDays((double)iNumDays);
                        sDatePast = Conversion.ToDbDate(datDatePast);
                        sDateFuture = Conversion.ToDbDate(datDateFuture);                        

                        sSQL += " AND FUNDS.TRANS_DATE >= '" + sDatePast;
                        sSQL += "' AND FUNDS.TRANS_DATE <= '" + sDateFuture + "'";

                        sFundsAutoSQL += " AND FUNDS_AUTO.PRINT_DATE >= '" + sDatePast + "' AND FUNDS_AUTO.PRINT_DATE <= '" + sDateFuture + "'";                      
                        break;
                    case 2:  //payment total                                                
                        sSQL += " AND (ABS(FUNDS.AMOUNT - " + p_objFundsAuto.Amount.ToString() + ") < .005)";
                        sFundsAutoSQL += " AND (ABS(FUNDS_AUTO.AMOUNT - " + p_objFundsAuto.Amount.ToString() + ") < .005)";
                        break;
                    case 3:                        
                        bFlag = false;
                         
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            //if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //continue;                            
                            sDatePast = objFundsAutoSplit.FromDate;
                            sDateFuture = objFundsAutoSplit.ToDate;

                            if (sDatePast != "" && sDateFuture != "")
                            {
                                if (!bFlag)
                                {
                                    sFundsAutoSQL += " AND (";
                                    bFlag = true;
                                }
                                else
                                {
                                    sFundsAutoSQL += " OR";
                                }

                                sFundsAutoSQL += " ((FROM_DATE <= '" + sDatePast + "'";
                                sFundsAutoSQL += " AND TO_DATE >= '" + sDatePast + "')";
                                sFundsAutoSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                sFundsAutoSQL += " AND TO_DATE >= '" + sDateFuture + "'))";
                            }
                        }
                        if (bFlag)
                            sFundsAutoSQL += ")";

                        bFlag = false;
                         
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            //if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //continue;                            
                            sDatePast = objFundsAutoSplit.FromDate;
                            sDateFuture = objFundsAutoSplit.ToDate;

                            if (sDatePast != "" && sDateFuture != "")
                            {
                                if (!bFlag)
                                {
                                    sSQL += " AND (";
                                    bFlag = true;
                                }
                                else
                                {
                                    sSQL += " OR";
                                }

                                sSQL += " ((FROM_DATE <= '" + sDatePast + "'";
                                sSQL += " AND TO_DATE >= '" + sDatePast + "')";
                                sSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                sSQL += " AND TO_DATE >= '" + sDateFuture + "'))";
                            }
                        }
                        if (bFlag)
                            sSQL += ")";

                        break;

                    case 4:

                        bFlag = false;

                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            //   if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //        continue;
                            //***************************
                            dblAmount = objFundsAutoSplit.Amount;
                            iTransTypeCode = objFundsAutoSplit.TransTypeCode;
                            if (!bFlag)
                            {
                                sSQL += " AND (";
                                sFundsAutoSQL += " AND (";
                                bFlag = true;
                            }
                            else
                            {
                                sSQL += " OR ";
                                sFundsAutoSQL += " OR ";
                            }

                            sSQL += " (TRANS_TYPE_CODE = " + iTransTypeCode.ToString();                            
                            sSQL += " AND (ABS(FUNDS_TRANS_SPLIT.AMOUNT - " + dblAmount.ToString() + ") < .005))";

                            sFundsAutoSQL += " (TRANS_TYPE_CODE = " + iTransTypeCode.ToString() ;                            
                            sFundsAutoSQL += " AND (ABS(ABS(FUNDS_AUTO_SPLIT.AMOUNT) - " + dblAmount.ToString() + ") < .005))";
                        }

                        if (bFlag)
                        {
                            sSQL += ")";
                            sFundsAutoSQL += ")";
                        }

                        break;
                    case 5:
                        //vDate = txtTransDate
                        //p_objFundsAuto.d
                        //datDate = Convert.ToDateTime(Conversion.GetDBDateFormat(p_objFundsAuto.PrintDate, "d"));
                        datDate = Conversion.ToDate(p_objFundsAuto.PrintDate);  

                        datDatePast = datDate.AddDays((double)-iNumDays);
                        datDateFuture = datDate.AddDays((double)iNumDays);
                        sDatePast = Conversion.ToDbDate(datDatePast);
                        sDateFuture = Conversion.ToDbDate(datDateFuture);                       

                        sSQL += " AND FUNDS.TRANS_DATE >= '" + sDatePast
                              + "' AND FUNDS.TRANS_DATE <= '" + sDateFuture + "'";
                        sSQL += " AND (ABS(FUNDS.AMOUNT - " + p_objFundsAuto.Amount.ToString() + ") < .005)";

                        sFundsAutoSQL += " AND FUNDS_AUTO.PRINT_DATE >= '" + sDatePast
                              + "' AND FUNDS_AUTO.PRINT_DATE <= '" + sDateFuture + "'";
                        sFundsAutoSQL += " AND (ABS(FUNDS_AUTO.AMOUNT - " + p_objFundsAuto.Amount.ToString() + ") < .005)";

                        break;
                    case 6:
                        datDate = Conversion.ToDate(p_objFundsAuto.PrintDate);
                        datDatePast = datDate.AddDays((double)-iNumDays);
                        datDateFuture = datDate.AddDays((double)iNumDays);
                        sDatePast = Conversion.ToDbDate(datDatePast);
                        sDateFuture = Conversion.ToDbDate(datDateFuture);

                        sFundsAutoSQL += " AND FUNDS_AUTO.PRINT_DATE >= '" + sDatePast + 
                                         "' AND FUNDS_AUTO.PRINT_DATE <= '" + sDateFuture + "'";                                     
                      
                        bFlag = false;
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            //   if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //        continue;                            
                            dblAmount = objFundsAutoSplit.Amount;
                            iTransTypeCode = objFundsAutoSplit.TransTypeCode;
                            if (!bFlag)
                            {
                                sSQL += " AND (";
                                sFundsAutoSQL += " AND (";
                                bFlag = true;
                            }
                            else
                            {
                                sSQL += " OR";
                                sFundsAutoSQL += " OR";
                            }

                            sSQL += " (TRANS_TYPE_CODE = " + iTransTypeCode.ToString();                            
                            sSQL += " AND (ABS(FUNDS_TRANS_SPLIT.AMOUNT - " + dblAmount.ToString() + ") < .005))";

                            sFundsAutoSQL += " (FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE = " + iTransTypeCode.ToString();                            
                            sFundsAutoSQL += " AND (ABS(ABS(FUNDS_AUTO_SPLIT.AMOUNT) - " + dblAmount.ToString() + ") < .005))";
                        }

                        if (bFlag)
                        {
                            sSQL += ")";
                            sFundsAutoSQL += ")";
                        }                        
                        break;


                    case 7:
                        //check for duplicates accross difference claims with the same payee and trans dates where
                        //claimant has the same tax id

                        iClaimID = p_objFundsAuto.ClaimId;
                        if (iClaimID == 0)
                        {
                            bExit = true;
                            break;
                        }
                        //R5 PS2 merge: 10/01/2009: Start
                        if (p_objFundsAuto.ClaimId > 0)
                        {
                            Claim objClaim = (Claim)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(p_objFundsAuto.ClaimId);
                            Entity objThisEntity = (Entity)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Entity", false);
                            string sClaimantSSNs = string.Empty;
                            string sClaimantEIDs = string.Empty;
                            foreach (Claimant objClaimant in objClaim.ClaimantList)
                            {
                                objThisEntity.MoveTo(objClaimant.ClaimantEid);
                                sTaxId = objThisEntity.TaxId;
                                sTaxId = sTaxId.Replace("-", "");
                                sTaxId = sTaxId.Trim();
                                if (sTaxId != "")
                                {
                                    if (sClaimantSSNs == string.Empty)
                                    {
                                        sClaimantSSNs = sTaxId + "','" + Int32.Parse(sTaxId).ToString("00-0000000") + "','" + Int32.Parse(sTaxId).ToString("000-00-0000") + "'";
                                    }
                                    else
                                    {
                                        sClaimantSSNs = sClaimantSSNs + ",'" + sTaxId + "','" + Int32.Parse(sTaxId).ToString("00-0000000") + "','" + Int32.Parse(sTaxId).ToString("000-00-0000") + "'";
                                    }
                                }
                                else
                                {
                                    if (sClaimantEIDs == string.Empty && objClaimant.ClaimantEid != 0)
                                    {
                                        sClaimantEIDs = objClaimant.ClaimantEid.ToString();
                                    }
                                    else
                                    {
                                        sClaimantEIDs = sClaimantEIDs + "," + objClaimant.ClaimantEid.ToString();
                                    }
                                }
                            }
                            if (sClaimantSSNs == string.Empty && sClaimantEIDs == string.Empty)
                            {
                                sSQL += " AND (FUNDS.CLAIM_ID = " + p_objFundsAuto.ClaimId + ")";

                                sFundsAutoSQL += " AND (FUNDS_AUTO.CLAIM_ID = " + p_objFundsAuto.ClaimId + ")";
                            }
                            else if (sClaimantSSNs != string.Empty && sClaimantEIDs != string.Empty)
                            {
                                sSQL += " AND ((FUNDS.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT,ENTITY WHERE CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('" + sClaimantSSNs + ")))";
                                sSQL += " OR ((FUNDS.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT WHERE CLAIMANT_EID IN (" + sClaimantEIDs + ")))))";

                                sFundsAutoSQL += " AND ((FUNDS_AUTO.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT,ENTITY WHERE CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('" + sClaimantSSNs + ")))";
                                sFundsAutoSQL += " OR ((FUNDS_AUTO.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT WHERE CLAIMANT_EID IN (" + sClaimantEIDs + ")))))";
                            }
                            else if (sClaimantSSNs != string.Empty)
                            {
                                sSQL += " AND (FUNDS.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT,ENTITY WHERE CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('" + sClaimantSSNs + ")))";
                                sFundsAutoSQL += " AND (FUNDS_AUTO.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT,ENTITY WHERE CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('" + sClaimantSSNs + ")))";
                            }
                            else if (sClaimantEIDs != string.Empty)
                            {
                                sSQL += " AND (FUNDS.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT WHERE CLAIMANT_EID IN (" + sClaimantEIDs + ")))";
                                sFundsAutoSQL += " AND (FUNDS_AUTO.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT WHERE CLAIMANT_EID IN (" + sClaimantEIDs + ")))";
                            }                                                        
                        }
                        //R5 PS2 Merge: End

                        bFlag = false;
                         
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            //if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //continue;                            
                            sDatePast = objFundsAutoSplit.FromDate;
                            sDateFuture = objFundsAutoSplit.ToDate;

                            if (sDatePast != "" && sDateFuture != "")
                            {
                                if (!bFlag)
                                {
                                    sFundsAutoSQL += " AND ((";
                                    sSQL += " AND ((";
                                    bFlag = true;
                                }
                                else
                                {
                                    sFundsAutoSQL += " OR (";
                                    sSQL += " OR (";
                                }

                                sFundsAutoSQL += " (FROM_DATE <= '" + sDatePast + "'";
                                sFundsAutoSQL += " AND TO_DATE >= '" + sDatePast + "')";
                                sFundsAutoSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                sFundsAutoSQL += " AND TO_DATE >= '" + sDateFuture + "'))";

                                sSQL += " (FROM_DATE <= '" + sDatePast + "'";
                                sSQL += " AND TO_DATE >= '" + sDatePast + "')";
                                sSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                sSQL += " AND TO_DATE >= '" + sDateFuture + "'))";
                            }
                        }
                        
                        if (bFlag)
                        {
                            sSQL += ")";
                            sFundsAutoSQL += ")";
                        }

                        break;
                    case 8:                        
                        sInvoiceNumbers = "";
                        sComma = "";
                        datDate = Conversion.ToDate(p_objFundsAuto.PrintDate);  

                        datDatePast = datDate.AddDays((double)-iNumDays);
                        datDateFuture = datDate.AddDays((double)iNumDays);
                        sDatePast = Conversion.ToDbDate(datDatePast);
                        sDateFuture = Conversion.ToDbDate(datDateFuture);                       

                        
                        sFundsAutoSQL += " AND FUNDS_AUTO.PRINT_DATE >= '" + sDatePast
                                      + "' AND FUNDS_AUTO.PRINT_DATE <= '" + sDateFuture + "'";
                        sFundsAutoSQL += " AND (ABS(FUNDS_AUTO.AMOUNT - " + p_objFundsAuto.Amount.ToString() + ") < .005)";

                        
                        sSQL += " AND FUNDS.TRANS_DATE >= '" + sDatePast
                              + "' AND FUNDS.TRANS_DATE <= '" + sDateFuture + "'";
                        sSQL += " AND (ABS(FUNDS.AMOUNT - " + p_objFundsAuto.Amount.ToString() + ") < .005)";

                        // Item One                                                 
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            // if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //   continue;
                            if(objFundsAutoSplit.InvoiceNumber != "")
                            {
                                sInvoiceNumbers += sComma + "'" + objFundsAutoSplit.InvoiceNumber + "'";
                                sComma = ", ";
                             
                            }                            
                        }

                        if(sInvoiceNumbers != "")
                        {
                           sFundsAutoSQL += " AND FUNDS_AUTO_SPLIT.INVOICE_NUMBER IN (" + sInvoiceNumbers + ")";
                           sSQL += " AND FUNDS_TRANS_SPLIT.INVOICE_NUMBER IN (" + sInvoiceNumbers + ")";
                        }
                        
                        break;
                    case 9:
                        sInvoiceNumbers = "";
                        sComma = "";
                        
                        // Item One                                                 
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            // if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //   continue;
                            if(objFundsAutoSplit.InvoiceNumber != "")
                            {
                                sInvoiceNumbers += sComma + "'" + objFundsAutoSplit.InvoiceNumber + "'";
                                sComma = ", ";                             
                            }                            
                        }

                        if(sInvoiceNumbers != "")
                        {
                           sFundsAutoSQL += " AND FUNDS_AUTO_SPLIT.INVOICE_NUMBER IN (" + sInvoiceNumbers + ")";
                           sSQL += " AND FUNDS_TRANS_SPLIT.INVOICE_NUMBER IN (" + sInvoiceNumbers + ")";

                           sFundsAutoSQL +=  " AND FUNDS_AUTO.PAYEE_EID = " +  p_objFundsAuto.PayeeEid;
                           sSQL += " AND FUNDS.PAYEE_EID = " +  p_objFundsAuto.PayeeEid;

                        }
                        
                        break;

                    case 10: 
                        //Payment Total
                        sSQL += " AND (ABS(FUNDS.AMOUNT - " +  p_objFundsAuto.Amount.ToString() + ") < .005)";
                        sFundsAutoSQL += " AND (ABS(FUNDS_AUTO.AMOUNT - " + p_objFundsAuto.Amount.ToString() + ") < .005)";

                        //Split amounts
                        bFlag = false;
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            //   if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //        continue;                            
                            dblAmount = objFundsAutoSplit.Amount;

                            if (!bFlag)
                            {
                                sSQL += " AND (";
                                sFundsAutoSQL += " AND (";
                                bFlag = true;
                            }
                            else
                            {
                                sSQL += " OR";
                                sFundsAutoSQL += " OR";
                            }

                            sSQL += " (ABS(FUNDS_TRANS_SPLIT.AMOUNT - " + dblAmount.ToString() + ") < .005)";

                            sFundsAutoSQL += " (ABS(ABS(FUNDS_AUTO_SPLIT.AMOUNT) - " + dblAmount.ToString() + ") < .005)";
                        }

                        if (bFlag)
                        {
                            sSQL += ")";
                            sFundsAutoSQL += ")";
                        }                        
                        
                        //To From Dates
                        bFlag = false;                         
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            //if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //continue;                            
                            sDatePast = objFundsAutoSplit.FromDate;
                            sDateFuture = objFundsAutoSplit.ToDate;

                            if (sDatePast != "" && sDateFuture != "")
                            {
                                if (!bFlag)
                                {
                                    sFundsAutoSQL += " AND ((";
                                    sSQL += " AND ((";
                                    bFlag = true;
                                }
                                else
                                {
                                    sFundsAutoSQL += " OR (";
                                    sSQL += " OR (";
                                }

                                sFundsAutoSQL += " (FROM_DATE <= '" + sDatePast + "'";
                                sFundsAutoSQL += " AND TO_DATE >= '" + sDatePast + "')";
                                sFundsAutoSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                sFundsAutoSQL += " AND TO_DATE >= '" + sDateFuture + "'))";

                                sSQL += " (FROM_DATE <= '" + sDatePast + "'";
                                sSQL += " AND TO_DATE >= '" + sDatePast + "')";
                                sSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                sSQL += " AND TO_DATE >= '" + sDateFuture + "'))";
                            }
                        }
                        
                        if (bFlag)
                        {
                            sSQL += ")";
                            sFundsAutoSQL += ")";
                        }

                        //Claimant SSN
                        if (p_objFundsAuto.ClaimId > 0)
                        {
                            Claim objClaim = (Claim)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(p_objFundsAuto.ClaimId);
                            Entity objThisEntity = (Entity)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Entity", false);
                            string sClaimantSSNs = string.Empty;
                            string sClaimantEIDs = string.Empty;
                            foreach (Claimant objClaimant in objClaim.ClaimantList)
                            {
                                objThisEntity.MoveTo(objClaimant.ClaimantEid);
                                sTaxId = objThisEntity.TaxId;
                                sTaxId = sTaxId.Replace("-", "");
                                sTaxId = sTaxId.Trim();
                                if (sTaxId != "")
                                {
                                    if (sClaimantSSNs == string.Empty)
                                    {
                                        sClaimantSSNs = sTaxId + "','" + Int32.Parse(sTaxId).ToString("00-0000000") + "','" + Int32.Parse(sTaxId).ToString("000-00-0000") + "'";
                                    }
                                    else
                                    {
                                        sClaimantSSNs = sClaimantSSNs + ",'" + sTaxId + "','" + Int32.Parse(sTaxId).ToString("00-0000000") + "','" + Int32.Parse(sTaxId).ToString("000-00-0000") + "'";
                                    }
                                }
                                else
                                {
                                    if (sClaimantEIDs == string.Empty && objClaimant.ClaimantEid != 0)
                                    {
                                        sClaimantEIDs = objClaimant.ClaimantEid.ToString();
                                    }
                                    else
                                    {
                                        sClaimantEIDs = sClaimantEIDs + "," + objClaimant.ClaimantEid.ToString();
                                    }
                                }
                            }
                            if (sClaimantSSNs == string.Empty && sClaimantEIDs == string.Empty)
                            {
                                sSQL += " AND (FUNDS.CLAIM_ID = " + p_objFundsAuto.ClaimId + ")";

                                sFundsAutoSQL += " AND (FUNDS_AUTO.CLAIM_ID = " + p_objFundsAuto.ClaimId + ")";
                            }
                            else if (sClaimantSSNs != string.Empty && sClaimantEIDs != string.Empty)
                            {
                                sSQL += " AND ((FUNDS.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT,ENTITY WHERE CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('" + sClaimantSSNs + ")))";
                                sSQL += " OR ((FUNDS.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT WHERE CLAIMANT_EID IN (" + sClaimantEIDs + ")))))";

                                sFundsAutoSQL += " AND ((FUNDS_AUTO.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT,ENTITY WHERE CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('" + sClaimantSSNs + ")))";
                                sFundsAutoSQL += " OR ((FUNDS_AUTO.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT WHERE CLAIMANT_EID IN (" + sClaimantEIDs + ")))))";                                
                            }
                            else if (sClaimantSSNs != string.Empty)
                            {
                                sSQL += " AND (FUNDS.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT,ENTITY WHERE CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('" + sClaimantSSNs + ")))";
                                sFundsAutoSQL += " AND (FUNDS_AUTO.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT,ENTITY WHERE CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('" + sClaimantSSNs + ")))";
                            }
                            else if (sClaimantEIDs != string.Empty)
                            {
                                sSQL += " AND (FUNDS.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT WHERE CLAIMANT_EID IN (" + sClaimantEIDs + ")))";
                                sFundsAutoSQL += " AND (FUNDS_AUTO.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT WHERE CLAIMANT_EID IN (" + sClaimantEIDs + ")))";
                            }                            
                        }
                        
                        bFlag = false;

                        //Invoice Numbers
                        sInvoiceNumbers = "";
                        sComma = "";
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            // if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //   continue;
                            if(objFundsAutoSplit.InvoiceNumber != "")
                            {
                                sInvoiceNumbers += sComma + "'" + objFundsAutoSplit.InvoiceNumber + "'";
                                sComma = ", ";                             
                            }                            
                        }

                        if(sInvoiceNumbers != "")
                        {
                           sFundsAutoSQL += " AND FUNDS_AUTO_SPLIT.INVOICE_NUMBER IN (" + sInvoiceNumbers + ")";
                           sSQL += " AND FUNDS_TRANS_SPLIT.INVOICE_NUMBER IN (" + sInvoiceNumbers + ")";                   
                        }

                        break;

                    case 11:

                        //We Get the payee tax id                        
                        objEntity = (Entity)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Entity", false);
                        try
                        {
                            objEntity.MoveTo(p_objFundsAuto.PayeeEid);
                            bEntityFound = true;
                        }
                        catch
                        {
                            bEntityFound = false;
                        }

                        if (bEntityFound)
                        {
                            sTaxId = objEntity.TaxId;
                            sTaxId = sTaxId.Replace("-", "");
                            sTaxId = sTaxId.Trim();
                        }
                        else
                        {
                            sTaxId = "";
                        }

                        sLastName = p_objFundsAuto.LastName.Replace("'", "''");
                        sFirstName = p_objFundsAuto.FirstName.Replace("'", "''");
                        sSQL += " AND ((((FIRST_NAME = '" + sFirstName + "') OR (FIRST_NAME IS NULL)) AND ((LAST_NAME = '" + sLastName + "') OR (LAST_NAME IS NULL)))";
                        

                        // npadhy Start MITS 22662 Performance Issue with Subqueries Fixed
                        sTempSql = new StringBuilder();
                        sTempSql.AppendFormat("SELECT PAYEE_EID FROM FUNDS, ENTITY WHERE ((FUNDS.PAYEE_EID = ENTITY.ENTITY_ID AND FUNDS.PAYEE_EID = {0})", p_objFundsAuto.PayeeEid);
                        if (sTaxId != "")
                        {
                            sTempSql.AppendFormat(" OR (FUNDS.PAYEE_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('{0}','{1}','{2}' )))", sTaxId, Int32.Parse(sTaxId).ToString("00-0000000"), Int32.Parse(sTaxId).ToString("000-00-0000")); ;
                        }
                        else
                        {
                            sTempSql.Append(")");
                        }

                        objReader2 = DbFactory.GetDbReader(m_sConnectionString,sTempSql.ToString());
                        sPayeeEIDs = string.Empty;
                        if (objReader2 != null)
                        {
                            string sThisPayeeEid = string.Empty;

                            while (objReader2.Read())
                            {
                                sThisPayeeEid = string.Empty;
                                sThisPayeeEid = (objReader2.GetValue(0)).ToString();
                                if (sPayeeEIDs.IndexOf(sThisPayeeEid) == -1)
                                {
                                    if (sPayeeEIDs != "")
                                    {
                                        sPayeeEIDs += ",";
                                    }
                                    sPayeeEIDs += (objReader2.GetValue(0)).ToString();
                                }
                            }
                            objReader2.Close();
                        }
                        if (!string.IsNullOrEmpty(sPayeeEIDs))
                        {
                            sSQL += " OR (FUNDS.PAYEE_EID IN (";
                            sSQL += sPayeeEIDs + ")))";
                        }
                        else
                        {
                            sSQL += ")";
                        }

                        sFundsAutoSQL += " AND ((((FIRST_NAME = '" + sFirstName + "') OR (FIRST_NAME IS NULL)) AND ((LAST_NAME = '" + sLastName + "') OR (LAST_NAME IS NULL)))";
                        
                        sTempSql.Length = 0;

                        sTempSql.AppendFormat("SELECT PAYEE_EID FROM FUNDS_AUTO, ENTITY WHERE ((FUNDS_AUTO.PAYEE_EID = ENTITY.ENTITY_ID AND FUNDS_AUTO.PAYEE_EID = {0})" , p_objFundsAuto.PayeeEid );

                       if (sTaxId != "")
                        {
                            sTempSql.AppendFormat(" OR (FUNDS_AUTO.PAYEE_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('{0}','{1}','{2}' )))", sTaxId, Int32.Parse(sTaxId).ToString("00-0000000"), Int32.Parse(sTaxId).ToString("000-00-0000")); ;
                        }
                        else
                        {
                            sTempSql.Append(")");
                        }

                        objReader2 = DbFactory.GetDbReader(m_sConnectionString,sTempSql.ToString());
                        sPayeeEIDs = string.Empty;
                        if (objReader2 != null)
                        {
                            string sThisPayeeEid = string.Empty;

                            while (objReader2.Read())
                            {
                                sThisPayeeEid = string.Empty;
                                sThisPayeeEid = (objReader2.GetValue(0)).ToString();
                                if (sPayeeEIDs.IndexOf(sThisPayeeEid) == -1)
                                {
                                    if (sPayeeEIDs != "")
                                    {
                                        sPayeeEIDs += ",";
                                    }
                                    sPayeeEIDs += (objReader2.GetValue(0)).ToString();
                                }
                            }
                            objReader2.Close();
                        }
                        if (!string.IsNullOrEmpty(sPayeeEIDs))
                        {
                            sFundsAutoSQL += " OR (FUNDS_AUTO.PAYEE_EID IN (";
                            sFundsAutoSQL += sPayeeEIDs + ")))";
                        }
                        else
                        {
                            sFundsAutoSQL += ")";
                        }
                       

                        // npadhy End MITS 22662 Performance Issue with Subqueries Fixed

                        //Overlapping date Range
                        bFlag = false;
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            //if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                              //  continue;

                            sDatePast = objFundsAutoSplit.FromDate;
                            sDateFuture = objFundsAutoSplit.ToDate;

                            if (sDatePast != "" && sDateFuture != "")
                            {
                                if (!bFlag)
                                {
                                    sSQL += " AND ((";
                                    bFlag = true;
                                }
                                else
                                {
                                    sSQL += " OR ";
                                }

                                sSQL += " ((FROM_DATE <= '" + sDatePast + "'";
                                sSQL += " AND TO_DATE >= '" + sDatePast + "')";
                                sSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                sSQL += " AND TO_DATE >= '" + sDateFuture + "'))";
                            }
                        }

                        if (bFlag)
                        {
                            // Item one ....
                            foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                            {
                                //if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                //  continue;
                                if (objFundsAutoSplit.InvoiceNumber != "")
                                    sSQL += " OR FUNDS_TRANS_SPLIT.INVOICE_NUMBER = '" + objFundsAutoSplit.InvoiceNumber + "'))";
                                else
                                    sSQL += "))";
                                break;
                            }
                        }
                        else
                        {
                            foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                            {
                               // if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                               //     continue;

                                if (objFundsAutoSplit.InvoiceNumber != "")
                                    sSQL += " AND FUNDS_TRANS_SPLIT.INVOICE_NUMBER = '" + objFundsAutoSplit.InvoiceNumber + "'";
                                break;

                            }
                        }

                        bFlag = false;
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            //if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //  continue;

                            sDatePast = objFundsAutoSplit.FromDate;
                            sDateFuture = objFundsAutoSplit.ToDate;

                            if (sDatePast != "" && sDateFuture != "")
                            {
                                if (!bFlag)
                                {
                                    sFundsAutoSQL += " AND ((";
                                    bFlag = true;
                                }
                                else
                                {
                                    sFundsAutoSQL += " OR ";
                                }

                                sFundsAutoSQL += " ((FROM_DATE <= '" + sDatePast + "'";
                                sFundsAutoSQL += " AND TO_DATE >= '" + sDatePast + "')";
                                sFundsAutoSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                sFundsAutoSQL += " AND TO_DATE >= '" + sDateFuture + "'))";
                            }
                        }

                        if (bFlag)
                        {
                            // Item one ....
                            foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                            {
                                //if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                //  continue;
                                if (objFundsAutoSplit.InvoiceNumber != "")
                                    sFundsAutoSQL += " OR FUNDS_AUTO_SPLIT.INVOICE_NUMBER = '" + objFundsAutoSplit.InvoiceNumber + "'))";
                                else
                                    sFundsAutoSQL += "))";
                                break;
                            }
                        }
                        else
                        {
                            foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                            {
                                //if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                //    continue;

                                if (objFundsAutoSplit.InvoiceNumber != "")
                                    sFundsAutoSQL += " AND FUNDS_AUTO_SPLIT.INVOICE_NUMBER = '" + objFundsAutoSplit.InvoiceNumber + "'";
                                break;

                            }
                        }

                        break;                    

                    case 12:

                        sInvoiceNumbers = "";
                        sComma = "";
                        datDate = Conversion.ToDate(p_objFundsAuto.PrintDate);

                        datDatePast = datDate.AddDays((double)-iNumDays);
                        datDateFuture = datDate.AddDays((double)iNumDays);
                        sDatePast = Conversion.ToDbDate(datDatePast);
                        sDateFuture = Conversion.ToDbDate(datDateFuture);


                        sFundsAutoSQL += " AND FUNDS_AUTO.PRINT_DATE >= '" + sDatePast
                                      + "' AND FUNDS_AUTO.PRINT_DATE <= '" + sDateFuture + "'";
                        sFundsAutoSQL += " AND (ABS(FUNDS_AUTO.AMOUNT - " + p_objFundsAuto.Amount.ToString() + ") < .005)";


                        sSQL += " AND FUNDS.TRANS_DATE >= '" + sDatePast
                              + "' AND FUNDS.TRANS_DATE <= '" + sDateFuture + "'";
                        sSQL += " AND (ABS(FUNDS.AMOUNT - " + p_objFundsAuto.Amount.ToString() + ") < .005)";

                        // Item One                                                 
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            // if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //   continue;
                            if (objFundsAutoSplit.InvoiceNumber != "")
                            {
                                sInvoiceNumbers += sComma + "'" + objFundsAutoSplit.InvoiceNumber + "'";
                                sComma = ", ";

                            }
                        }

                        if (sInvoiceNumbers != "")
                        {
                            sFundsAutoSQL += " AND FUNDS_AUTO_SPLIT.INVOICE_NUMBER IN (" + sInvoiceNumbers + ")";
                            sSQL += " AND FUNDS_TRANS_SPLIT.INVOICE_NUMBER IN (" + sInvoiceNumbers + ")";
                        }



                        //Split amounts
                        bFlag = false;
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            //   if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //        continue;                            
                            dblAmount = objFundsAutoSplit.Amount;
                            iTransTypeCode = objFundsAutoSplit.TransTypeCode;
                            if (!bFlag)
                            {
                                sSQL += " AND (";
                                sFundsAutoSQL += " AND (";
                                bFlag = true;
                            }
                            else
                            {
                                sSQL += " OR";
                                sFundsAutoSQL += " OR";
                            }

                            sSQL += " (TRANS_TYPE_CODE = " + iTransTypeCode.ToString();                            
                            sSQL += " AND (ABS(FUNDS_TRANS_SPLIT.AMOUNT - " + dblAmount.ToString() + ") < .005))";

                            sFundsAutoSQL += " (FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE = " + iTransTypeCode.ToString();                            
                            sFundsAutoSQL += " AND (ABS(ABS(FUNDS_AUTO_SPLIT.AMOUNT) - " + dblAmount.ToString() + ") < .005))";
                        }

                        if (bFlag)
                        {
                            sSQL += ")";
                            sFundsAutoSQL += ")";
                        }                                               

                        break;

                    case 13:
                        //Payment Total
                        sSQL += " AND (ABS(FUNDS.AMOUNT - " + p_objFundsAuto.Amount.ToString() + ") < .005)";
                        sFundsAutoSQL += " AND (ABS(FUNDS_AUTO.AMOUNT - " + p_objFundsAuto.Amount.ToString() + ") < .005)";

                        //Split amounts
                        bFlag = false;
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            //   if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //        continue;                            
                            dblAmount = objFundsAutoSplit.Amount;
                            
                            if (!bFlag)
                            {
                                sSQL += " AND (";
                                sFundsAutoSQL += " AND (";
                                bFlag = true;
                            }
                            else
                            {
                                sSQL += " OR";
                                sFundsAutoSQL += " OR";
                            }

                            sSQL += " (ABS(FUNDS_TRANS_SPLIT.AMOUNT - " + dblAmount.ToString() + ") < .005)";//Removing an extra ) which was causing the crterion 13 to break

                            sFundsAutoSQL += " (ABS(ABS(FUNDS_AUTO_SPLIT.AMOUNT) - " + dblAmount.ToString() + ") < .005)";//Removing an extra ) which was causing the crterion 13 to break
                        }

                        if (bFlag)
                        {
                            sSQL += ")";
                            sFundsAutoSQL += ")";
                        }

                        //To From Dates
                        bFlag = false;
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            //if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //continue;                            
                            sDatePast = objFundsAutoSplit.FromDate;
                            sDateFuture = objFundsAutoSplit.ToDate;

                            if (sDatePast != "" && sDateFuture != "")
                            {
                                if (!bFlag)
                                {
                                    sFundsAutoSQL += " AND ((";
                                    sSQL += " AND ((";
                                    bFlag = true;
                                }
                                else
                                {
                                    sFundsAutoSQL += " OR (";
                                    sSQL += " OR (";
                                }

                                sFundsAutoSQL += " (FROM_DATE <= '" + sDatePast + "'";
                                sFundsAutoSQL += " AND TO_DATE >= '" + sDatePast + "')";
                                sFundsAutoSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                sFundsAutoSQL += " AND TO_DATE >= '" + sDateFuture + "'))";

                                sSQL += " (FROM_DATE <= '" + sDatePast + "'";
                                sSQL += " AND TO_DATE >= '" + sDatePast + "')";
                                sSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                sSQL += " AND TO_DATE >= '" + sDateFuture + "'))";
                            }
                        }

                        if (bFlag)
                        {
                            sSQL += ")";
                            sFundsAutoSQL += ")";
                        }

                        //Payee SSN
                        sPayeeEIDs = "";
                        sTaxId = p_objFundsAuto.PayeeEntity.TaxId;
                        sTaxId = sTaxId.Replace("-", "");
                        sTaxId = sTaxId.Trim();

                        if (sTaxId != "")
                        {
                            objReader2 = DbFactory.GetDbReader(m_sConnectionString,
                                         "SELECT ENTITY_ID FROM ENTITY WHERE TAX_ID IN " +
                                         "('" + sTaxId + "','" + Int32.Parse(sTaxId).ToString("00-0000000") + "','" + Int32.Parse(sTaxId).ToString("000-00-0000") + "') ");
                            if (objReader2 != null)
                            {
                                while (objReader2.Read())
                                {
                                    if (sPayeeEIDs != "")
                                    {
                                        sPayeeEIDs += ",";
                                    }

                                    sPayeeEIDs += (objReader2.GetValue(0)).ToString();
                                }
                                objReader2.Close();
                            }
                            sSQL = sSQL + " AND FUNDS.PAYEE_EID IN (" + sPayeeEIDs + ")";
                            sFundsAutoSQL = sFundsAutoSQL + " AND FUNDS_AUTO.PAYEE_EID IN (" + sPayeeEIDs + ")";
                        }

                        //Invoice Numbers
                        sInvoiceNumbers = "";
                        sComma = "";
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            // if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //   continue;
                            if (objFundsAutoSplit.InvoiceNumber != "")
                            {
                                sInvoiceNumbers += sComma + "'" + objFundsAutoSplit.InvoiceNumber + "'";
                                sComma = ", ";
                            }
                        }

                        if (sInvoiceNumbers != "")
                        {
                            sFundsAutoSQL += " AND FUNDS_AUTO_SPLIT.INVOICE_NUMBER IN (" + sInvoiceNumbers + ")";
                            sSQL += " AND FUNDS_TRANS_SPLIT.INVOICE_NUMBER IN (" + sInvoiceNumbers + ")";
                        }

                        break;
                    case 14:
                        //Payment Total
                        sSQL += " AND (ABS(FUNDS.AMOUNT - " + p_objFundsAuto.Amount.ToString() + ") < .005)";
                        sFundsAutoSQL += " AND (ABS(FUNDS_AUTO.AMOUNT - " + p_objFundsAuto.Amount.ToString() + ") < .005)";

                        //Split amounts
                        bFlag = false;
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            //   if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //        continue;                            
                            dblAmount = objFundsAutoSplit.Amount;

                            if (!bFlag)
                            {
                                sSQL += " AND (";
                                sFundsAutoSQL += " AND (";
                                bFlag = true;
                            }
                            else
                            {
                                sSQL += " OR";
                                sFundsAutoSQL += " OR";
                            }

                            sSQL += " (ABS(FUNDS_TRANS_SPLIT.AMOUNT - " + dblAmount.ToString() + ") < .005)";

                            sFundsAutoSQL += " (ABS(ABS(FUNDS_AUTO_SPLIT.AMOUNT) - " + dblAmount.ToString() + ") < .005)";
                        }

                        if (bFlag)
                        {
                            sSQL += ")";
                            sFundsAutoSQL += ")";
                        }

                        //To From Dates
                        bFlag = false;
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            //if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //continue;                            
                            sDatePast = objFundsAutoSplit.FromDate;
                            sDateFuture = objFundsAutoSplit.ToDate;

                            if (sDatePast != "" && sDateFuture != "")
                            {
                                if (!bFlag)
                                {
                                    sFundsAutoSQL += " AND ((";
                                    sSQL += " AND ((";
                                    bFlag = true;
                                }
                                else
                                {
                                    sFundsAutoSQL += " OR (";
                                    sSQL += " OR (";
                                }

                                sFundsAutoSQL += " (FROM_DATE <= '" + sDatePast + "'";
                                sFundsAutoSQL += " AND TO_DATE >= '" + sDatePast + "')";
                                sFundsAutoSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                sFundsAutoSQL += " AND TO_DATE >= '" + sDateFuture + "'))";

                                sSQL += " (FROM_DATE <= '" + sDatePast + "'";
                                sSQL += " AND TO_DATE >= '" + sDatePast + "')";
                                sSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                sSQL += " AND TO_DATE >= '" + sDateFuture + "'))";
                            }
                        }

                        if (bFlag)
                        {
                            sSQL += ")";
                            sFundsAutoSQL += ")";
                        }

                        //Claimant SSN
                        if (p_objFundsAuto.ClaimId > 0)
                        {
                            Claim objClaim = (Claim)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(p_objFundsAuto.ClaimId);
                            Entity objThisEntity = (Entity)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Entity", false);
                            string sClaimantSSNs = string.Empty;
                            string sClaimantEIDs = string.Empty;
                            foreach (Claimant objClaimant in objClaim.ClaimantList)
                            {
                                objThisEntity.MoveTo(objClaimant.ClaimantEid);
                                //skhare7 Start : Worked on JIRA - 1394
                                //sTaxId = objThisEntity.TaxId;
                                sTaxId = objThisEntity.UnMaskedTaxId;
                                ////skhare7 End
                                sTaxId = sTaxId.Replace("-", "");
                                sTaxId = sTaxId.Trim();
                                if (sTaxId != "")
                                {
                                    if (sClaimantSSNs == string.Empty)
                                    {
                                        sClaimantSSNs = sTaxId + "','" + Int32.Parse(sTaxId).ToString("00-0000000") + "','" + Int32.Parse(sTaxId).ToString("000-00-0000") + "'";
                                    }
                                    else
                                    {
                                        sClaimantSSNs = sClaimantSSNs + ",'" + sTaxId + "','" + Int32.Parse(sTaxId).ToString("00-0000000") + "','" + Int32.Parse(sTaxId).ToString("000-00-0000") + "'";
                                    }
                                }
                                else
                                {
                                    if (sClaimantEIDs == string.Empty && objClaimant.ClaimantEid != 0)
                                    {
                                        sClaimantEIDs = objClaimant.ClaimantEid.ToString();
                                    }
                                    else
                                    {
                                        sClaimantEIDs = sClaimantEIDs + "," + objClaimant.ClaimantEid.ToString();
                                    }
                                }
                            }
                            if (sClaimantSSNs == string.Empty && sClaimantEIDs == string.Empty)
                            {
                                sSQL += " AND (FUNDS.CLAIM_ID = " + p_objFundsAuto.ClaimId + ")";

                                sFundsAutoSQL += " AND (FUNDS_AUTO.CLAIM_ID = " + p_objFundsAuto.ClaimId + ")";
                            }
                            else if (sClaimantSSNs != string.Empty && sClaimantEIDs != string.Empty)
                            {
                                sSQL += " AND ((FUNDS.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT,ENTITY WHERE CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('" + sClaimantSSNs + ")))";
                                sSQL += " OR ((FUNDS.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT WHERE CLAIMANT_EID IN (" + sClaimantEIDs + ")))))";

                                sFundsAutoSQL += " AND ((FUNDS_AUTO.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT,ENTITY WHERE CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('" + sClaimantSSNs + ")))";
                                sFundsAutoSQL += " OR ((FUNDS_AUTO.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT WHERE CLAIMANT_EID IN (" + sClaimantEIDs + ")))))";
                            }
                            else if (sClaimantSSNs != string.Empty)
                            {
                                sSQL += " AND (FUNDS.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT,ENTITY WHERE CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('" + sClaimantSSNs + ")))";
                                sFundsAutoSQL += " AND (FUNDS_AUTO.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT,ENTITY WHERE CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('" + sClaimantSSNs + ")))";
                            }
                            else if (sClaimantEIDs != string.Empty)
                            {
                                sSQL += " AND (FUNDS.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT WHERE CLAIMANT_EID IN (" + sClaimantEIDs + ")))";
                                sFundsAutoSQL += " AND (FUNDS_AUTO.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT WHERE CLAIMANT_EID IN (" + sClaimantEIDs + ")))";
                            }
                        }

                        bFlag = false;

                        //Invoice Numbers
                        sInvoiceNumbers = "";
                        sComma = "";
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            // if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //   continue;
                            if (objFundsAutoSplit.InvoiceNumber != "")
                            {
                                sInvoiceNumbers += sComma + "'" + objFundsAutoSplit.InvoiceNumber + "'";
                                sComma = ", ";
                            }
                        }

                        if (sInvoiceNumbers != "")
                        {
                            sFundsAutoSQL += " AND FUNDS_AUTO_SPLIT.INVOICE_NUMBER IN (" + sInvoiceNumbers + ")";
                            sSQL += " AND FUNDS_TRANS_SPLIT.INVOICE_NUMBER IN (" + sInvoiceNumbers + ")";
                        }

                        break;

                    case 15:

                        //We Get the payee tax id                        
                        objEntity = (Entity)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Entity", false);
                        try
                        {
                            objEntity.MoveTo(p_objFundsAuto.PayeeEid);
                            bEntityFound = true;
                        }
                        catch
                        {
                            bEntityFound = false;
                        }

                        if (bEntityFound)
                        {
                            sTaxId = objEntity.TaxId;
                            sTaxId = sTaxId.Replace("-", "");
                            sTaxId = sTaxId.Trim();
                        }
                        else
                        {
                            sTaxId = "";
                        }

                        sLastName = p_objFundsAuto.LastName.Replace("'", "''");
                        sFirstName = p_objFundsAuto.FirstName.Replace("'", "''");

                        sSQL += " AND ((((FIRST_NAME = '" + sFirstName + "') OR (FIRST_NAME IS NULL)) AND ((LAST_NAME = '" + sLastName + "') OR (LAST_NAME IS NULL)))";
                        // akaushik5 Changed for MITS 38205 Starts
                        //sSQL += " OR (FUNDS.PAYEE_EID IN (SELECT PAYEE_EID FROM FUNDS, ENTITY WHERE ((FUNDS.PAYEE_EID = ENTITY.ENTITY_ID AND FUNDS.PAYEE_EID = " + p_objFundsAuto.PayeeEid + " AND FUNDS.CLAIM_ID = " + p_objFundsAuto.ClaimId + ")";
                        sSQL += string.Format(" OR (FUNDS.PAYEE_EID IN (SELECT DISTINCT PAYEE_EID FROM FUNDS JOIN ENTITY ON FUNDS.PAYEE_EID = ENTITY.ENTITY_ID WHERE FUNDS.CLAIM_ID = {0} AND ( FUNDS.PAYEE_EID = {1} ", p_objFundsAuto.ClaimId, p_objFundsAuto.PayeeEid);
                        // akaushik5 Changed for MITS 38205 Ends

                        if (sTaxId != "")
                        {
                            // akaushik5 Changed for MITS 38205 Starts
                            //sSQL += " OR (FUNDS.CLAIM_ID = " + p_objFundsAuto.ClaimId + " AND FUNDS.PAYEE_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('" + sTaxId + "','" + Int32.Parse(sTaxId).ToString("00-0000000") + "','" + Int32.Parse(sTaxId).ToString("000-00-0000") + "' ))))))";
                            sSQL += string.Format(" OR ENTITY.TAX_ID IN ('{0}','{1}','{2}')", sTaxId, Int32.Parse(sTaxId).ToString("00-0000000"), Int32.Parse(sTaxId).ToString("000-00-0000"));
                            // akaushik5 Changed for MITS 38205 Ends
                        }

                        sSQL += "))))";


                        sFundsAutoSQL += " AND ((((FIRST_NAME = '" + sFirstName + "') OR (FIRST_NAME IS NULL)) AND ((LAST_NAME = '" + sLastName + "') OR (LAST_NAME IS NULL)))";
                        // akaushik5 Changed for MITS 38205 Starts
                        //sFundsAutoSQL += " OR (FUNDS_AUTO.PAYEE_EID IN (SELECT PAYEE_EID FROM FUNDS_AUTO, ENTITY WHERE ((FUNDS_AUTO.PAYEE_EID = ENTITY.ENTITY_ID AND FUNDS_AUTO.PAYEE_EID = " + p_objFundsAuto.PayeeEid + " AND FUNDS_AUTO.CLAIM_ID = " + p_objFundsAuto.ClaimId + ")";
                        sFundsAutoSQL += string.Format(" OR (FUNDS_AUTO.PAYEE_EID IN (SELECT DISTINCT PAYEE_EID FROM FUNDS_AUTO JOIN ENTITY ON FUNDS_AUTO.PAYEE_EID = ENTITY.ENTITY_ID WHERE FUNDS_AUTO.CLAIM_ID = {0} AND ( FUNDS_AUTO.PAYEE_EID = {1} ", p_objFundsAuto.ClaimId, p_objFundsAuto.PayeeEid);
                        // akaushik5 Changed for MITS 38205 Ends

                        if (sTaxId != "")
                        {
                            // akaushik5 Changed for MITS 38205 Starts
                            //sFundsAutoSQL += " OR (FUNDS_AUTO.CLAIM_ID = " + p_objFundsAuto.ClaimId + " AND FUNDS_AUTO.PAYEE_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('" + sTaxId + "','" + Int32.Parse(sTaxId).ToString("00-0000000") + "','" + Int32.Parse(sTaxId).ToString("000-00-0000") + "' ))))))";
                            sFundsAutoSQL += string.Format(" OR ENTITY.TAX_ID IN ('{0}','{1}','{2}')", sTaxId, Int32.Parse(sTaxId).ToString("00-0000000"), Int32.Parse(sTaxId).ToString("000-00-0000"));
                            // akaushik5 Changed for MITS 38205 Ends
                        }

                        sFundsAutoSQL += "))))";


                        //Overlapping date Range
                        bFlag = false;
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            //if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //  continue;

                            sDatePast = objFundsAutoSplit.FromDate;
                            sDateFuture = objFundsAutoSplit.ToDate;

                            if (sDatePast != "" && sDateFuture != "")
                            {
                                if (!bFlag)
                                {
                                    sSQL += " AND ((";
                                    bFlag = true;
                                }
                                else
                                {
                                    sSQL += " OR ";
                                }

                                sSQL += " ((FROM_DATE <= '" + sDatePast + "'";
                                sSQL += " AND TO_DATE >= '" + sDatePast + "')";
                                sSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                sSQL += " AND TO_DATE >= '" + sDateFuture + "'))";
                            }
                        }

                        if (bFlag)
                        {
                            // Item one ....
                            foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                            {
                                //if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                //  continue;
                                if (objFundsAutoSplit.InvoiceNumber != "")
                                    sSQL += " OR FUNDS_TRANS_SPLIT.INVOICE_NUMBER = '" + objFundsAutoSplit.InvoiceNumber + "'))";
                                else
                                    sSQL += "))";
                                break;
                            }
                        }
                        else
                        {
                            foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                            {
                                // if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                //     continue;

                                if (objFundsAutoSplit.InvoiceNumber != "")
                                    sSQL += " AND FUNDS_TRANS_SPLIT.INVOICE_NUMBER = '" + objFundsAutoSplit.InvoiceNumber + "'";
                                break;

                            }
                        }

                        bFlag = false;
                        foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                        {
                            //if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            //  continue;

                            sDatePast = objFundsAutoSplit.FromDate;
                            sDateFuture = objFundsAutoSplit.ToDate;

                            if (sDatePast != "" && sDateFuture != "")
                            {
                                if (!bFlag)
                                {
                                    sFundsAutoSQL += " AND ((";
                                    bFlag = true;
                                }
                                else
                                {
                                    sFundsAutoSQL += " OR ";
                                }

                                sFundsAutoSQL += " ((FROM_DATE <= '" + sDatePast + "'";
                                sFundsAutoSQL += " AND TO_DATE >= '" + sDatePast + "')";
                                sFundsAutoSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                sFundsAutoSQL += " AND TO_DATE >= '" + sDateFuture + "'))";
                            }
                        }

                        if (bFlag)
                        {
                            // Item one ....
                            foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                            {
                                //if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                //  continue;
                                if (objFundsAutoSplit.InvoiceNumber != "")
                                    sFundsAutoSQL += " OR FUNDS_AUTO_SPLIT.INVOICE_NUMBER = '" + objFundsAutoSplit.InvoiceNumber + "'))";
                                else
                                    sFundsAutoSQL += "))";
                                break;
                            }
                        }
                        else
                        {
                            foreach (FundsAutoSplit objFundsAutoSplit in p_objFundsAuto.AutoSplitList)
                            {
                                //if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                //    continue;

                                if (objFundsAutoSplit.InvoiceNumber != "")
                                    sFundsAutoSQL += " AND FUNDS_AUTO_SPLIT.INVOICE_NUMBER = '" + objFundsAutoSplit.InvoiceNumber + "'";
                                break;

                            }
                        }

                        break;                    


                }
                //END SWITCH
                #endregion

                if (!bExit)
                {                    
                    //if (m_objFunds.PaymentFlag)
                    //    sSQL += " AND FUNDS.PAYMENT_FLAG <> 0 ";
                    //else
                    //    sSQL += " AND FUNDS.PAYMENT_FLAG = 0 ";

                    sFundsAutoSQL += " ORDER BY FUNDS_AUTO.AUTO_TRANS_ID, FUNDS_AUTO.AUTO_BATCH_ID, FUNDS_AUTO.PRINT_DATE";
                    sSQL += " ORDER BY FUNDS.TRANS_DATE DESC,FUNDS.TRANS_ID";
                    
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);                   

                    if (objReader != null)
                    {
                     //Get duplicate Trans ids from funds                    
                        while (objReader.Read())
                        {
                            iTransId = Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("TRANS_ID"), base.ClientId);
                            if(iLastTransId !=  iTransId)
                            {
                                arrlstTransID.Add(iTransId);
                                iLastTransId =  iTransId;
                            }
                        }

                        if (objReader != null)
                            objReader.Close();
                    }

                    //Get duplicate Auto Transids from funds auto 
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sFundsAutoSQL);
                    if(objReader != null)
                    {
                        while (objReader.Read())
                        {
                            iAutoBatchId = Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("AUTO_BATCH_ID"), base.ClientId);
                            iAutoTransId = Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("AUTO_TRANS_ID"), base.ClientId);

                            if (iLastAutoTransId != iAutoTransId && iLastAutoBatchId != iAutoBatchId && this.FundsAutoBatch.AutoBatchId != iAutoBatchId)
                            {                                
                                arrlstAutoTransID.Add(iAutoTransId);
                                iLastAutoBatchId = iAutoBatchId;
                                iLastAutoTransId = iAutoTransId;                                
                            }
                        }

                        if (objReader != null)
                            objReader.Close();
                        
                    }
                        
                    if(arrlstTransID.Count > 0 || arrlstAutoTransID.Count > 0)
                    {
                        //Deb Multi Currency Added p_objFundsAuto parameter
                        m_sConfirmation = GenerateTransSplitHTML(arrlstTransID,arrlstAutoTransID,p_objFundsAuto);
                        bCheckForDupPayments = true;
                    }
                }
            if (objReader != null)
             {
                objReader.Close();
                objReader.Dispose();
             }
             if (objReader2 != null)
             {
                objReader2.Close();
                objReader2.Dispose();
             }
             objEntity = null;            

            return (bCheckForDupPayments);

        }

        /// <summary>
		/// Get Trans Split HTML
		/// </summary>
		/// <param name="p_arrlstTransId">TransId List</param>
        /// <param name="p_arrlstAutoTransId">Auto TransId List</param>         
		/// <returns>Trans Split HTML string</returns>
        private string GenerateTransSplitHTML(ArrayList p_arrlstTransId, ArrayList p_arrlstAutoTransId,FundsAuto p_objFundsAuto)//Deb Multi Currency
        {
            DbReader objReader = null;
            LocalCache objLocalCache = null;

            int iIndex = 0;
            int iTransId = 0;
            int iAutoTransId = 0;            
            bool bFirst = true;
            bool bVoid = false;
            string sHTML = "";
            string sTransDate = "";
            string sFirstName = "";
            string sName = "";
            string sFromDate = "";
            string sToDate = "";
            string sTransTypeShortCode = "";
            string sTransTypeDesc = "";
            string sAmount = "";
            string sSQL = "";
            //Deb Multi Currency
            double dExhRateBaseToPmt = 1;
            double dAmount10 = 0;
            double dAmount4 = 0;
            double dAmount9 = 0;
            double dAmount3 = 0;
            //Deb Multi Currency
            
   				objLocalCache = new LocalCache( m_sConnectionString,base.ClientId);
                for (iIndex = 0; iIndex < p_arrlstTransId.Count; iIndex++)
                {
                    iTransId = (int)p_arrlstTransId[iIndex];

                    sSQL = " SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.VOID_FLAG,FUNDS.TRANS_DATE,"
                            + " FUNDS.AMOUNT, FUNDS.FIRST_NAME, FUNDS.LAST_NAME, "
                            + " FUNDS_TRANS_SPLIT.FROM_DATE, FUNDS_TRANS_SPLIT.TO_DATE,"
                            + " FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE, FUNDS_TRANS_SPLIT.AMOUNT,FUNDS.CLAIM_NUMBER,"
                            + " FUNDS_TRANS_SPLIT.INVOICE_NUMBER"
                            + " FROM FUNDS,FUNDS_TRANS_SPLIT"
                            + " WHERE FUNDS.TRANS_ID = " + iTransId.ToString()
                            + " AND FUNDS_TRANS_SPLIT.TRANS_ID = " + iTransId.ToString();

                    bFirst = true ;
					objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				
					if( objReader != null )
					{
                        // Process rows - 1st one has all info, subsequent are only for detail info
						while( objReader.Read() )
						{
							sHTML += "*" ;
							if( bFirst )
							{
								bFirst = false ;
								sTransDate = Conversion.GetDBDateFormat( objReader.GetString( "TRANS_DATE" ) , "d" );
								sHTML += sTransDate + "|" ;
							
								sHTML += objReader.GetString( "CTL_NUMBER" ) + "|" ;

                                //formulate name of payee
								sFirstName = objReader.GetString( "FIRST_NAME" );
								if( sFirstName != "" )
									sFirstName = sFirstName + " " ;
								sName = sFirstName + objReader.GetString( "LAST_NAME" );

								sHTML += sName + "|" ;
							
								bVoid = objReader.GetBoolean( "VOID_FLAG" );
								if( bVoid )
									sHTML += "Yes" + "|" ;
								else
									sHTML += "No" + "|" ;

								sFromDate = Conversion.GetDBDateFormat( objReader.GetString( "FROM_DATE" ) , "d" );
								sToDate = Conversion.GetDBDateFormat( objReader.GetString( "TO_DATE" ) , "d" );
								sHTML += sFromDate + " - " + sToDate + "|" ;

                                objLocalCache.GetCodeInfo(Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), base.ClientId), ref sTransTypeShortCode, ref sTransTypeDesc, base.Adaptor.userLogin.objUser.NlsCode); //Aman Ml Change

								sHTML += sTransTypeDesc + "|" ;
                                //Deb Multi Currency
                                if (p_objFundsAuto.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                                {
                                    //dExhRateBaseToPmt = FundsObject.Context.DbConn.ExecuteDouble(string.Format("SELECT EXCHANGE_RATE FROM CURRENCY_RATE WHERE SOURCE_CURRENCY_CODE={0} AND DESTINATION_CURRENCY_CODE={1}", FundsObject.Context.InternalSettings.SysSettings.BaseCurrencyType, FundsObject.PmtCurrencyType));
                                    if (p_objFundsAuto.BaseToPmtCurRate == 0)
                                        dExhRateBaseToPmt = CommonFunctions.ExchangeRateSrc2Dest(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, p_objFundsAuto.PmtCurrencyType, m_sConnectionString, base.ClientId);
                                    else
                                        dExhRateBaseToPmt = p_objFundsAuto.BaseToPmtCurRate;
                                    dAmount10 = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(10)));
                                    dAmount10 = dAmount10 * dExhRateBaseToPmt;
                                    if (p_objFundsAuto.PmtCurrencyType > 0)
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.PmtCurrencyType, dAmount10, m_sConnectionString,base.ClientId);
                                    }
                                    else if (p_objFundsAuto.ClaimCurrencyType > 0)
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.ClaimCurrencyType, dAmount10, m_sConnectionString, base.ClientId);
                                    }
                                    else
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, dAmount10, m_sConnectionString, base.ClientId);
                                    }
                                }
                                else
                                {
                                    dAmount10 = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(10)));
                                    sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, dAmount10, m_sConnectionString, base.ClientId);
                                }
								//sAmount = string.Format("{0:C}" , Riskmaster.Common.Conversion.ConvertStrToDouble( Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue( 10 ) )) );
								sHTML += sAmount + "|" ;

                                if (p_objFundsAuto.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                                {
                                    dAmount4 = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(4)));
                                    dAmount4 = dAmount4 * dExhRateBaseToPmt;
                                    if (p_objFundsAuto.PmtCurrencyType > 0)
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.PmtCurrencyType, dAmount4, m_sConnectionString, base.ClientId);
                                    }
                                    else if (p_objFundsAuto.ClaimCurrencyType > 0)
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.ClaimCurrencyType, dAmount4, m_sConnectionString, base.ClientId);
                                    }
                                    else
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, dAmount4, m_sConnectionString, base.ClientId);
                                    }
                                }
                                else
                                {
                                    dAmount4 = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(4)));
                                    sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, dAmount4, m_sConnectionString, base.ClientId);
                                }
                                
                                //compute amount of transaction and total payments/collections/voided amounts
                                //sAmount = string.Format("{0:C}", Riskmaster.Common.Conversion.ConvertStrToDouble(Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue(4))));
                                //Deb Multi Currency
								sHTML += sAmount + "|" ;

								sHTML += objReader.GetString( "CLAIM_NUMBER") + "|" ;
                                sHTML += objReader.GetString("INVOICE_NUMBER") + "|";
							}
							else
							{
								sHTML += "\"|" ;
								sHTML += "\"|" ;
								sHTML += "\"|" ;
								sHTML += "\"|" ;

								sFromDate = Conversion.GetDBDateFormat( objReader.GetString( "FROM_DATE" ) , "d" );
								sToDate = Conversion.GetDBDateFormat( objReader.GetString( "TO_DATE" ) , "d" );
								sHTML += sFromDate + " - " + sToDate + "|" ;

                                objLocalCache.GetCodeInfo(Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), base.ClientId), ref sTransTypeShortCode, ref sTransTypeDesc, base.Adaptor.userLogin.objUser.NlsCode); //Aman Ml Change

								sHTML += sTransTypeDesc + "|" ;
                                //Deb Multi Currency
                                if (p_objFundsAuto.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                                {
                                    //dExhRateBaseToPmt = FundsObject.Context.DbConn.ExecuteDouble(string.Format("SELECT EXCHANGE_RATE FROM CURRENCY_RATE WHERE SOURCE_CURRENCY_CODE={0} AND DESTINATION_CURRENCY_CODE={1}", FundsObject.Context.InternalSettings.SysSettings.BaseCurrencyType, FundsObject.PmtCurrencyType));
                                    if (p_objFundsAuto.BaseToPmtCurRate == 0)
                                        dExhRateBaseToPmt = CommonFunctions.ExchangeRateSrc2Dest(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, p_objFundsAuto.PmtCurrencyType, m_sConnectionString, base.ClientId);
                                    else
                                        dExhRateBaseToPmt = p_objFundsAuto.BaseToPmtCurRate;
                                    dAmount10 = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(10)));
                                    dAmount10 = dAmount10 * dExhRateBaseToPmt;
                                    if (p_objFundsAuto.PmtCurrencyType > 0)
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.PmtCurrencyType, dAmount10, m_sConnectionString, base.ClientId);
                                    }
                                    else if (p_objFundsAuto.ClaimCurrencyType > 0)
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.ClaimCurrencyType, dAmount10, m_sConnectionString, base.ClientId);
                                    }
                                    else
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, dAmount10, m_sConnectionString, base.ClientId);
                                    }
                                }
                                else
                                {
                                    dAmount10 = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(10)));
                                    sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, dAmount10, m_sConnectionString, base.ClientId);
                                }
                                //Deb Multi Currency
                                //sAmount = string.Format("{0:C}", Riskmaster.Common.Conversion.ConvertStrToDouble(Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue(10))));
								sHTML += sAmount + "|" ;

								sHTML += "\"|" ;
								sHTML += "\"|" ;
                                sHTML += "\"|";
							} //end if							
						}//end while
                        objReader.Close();
					}//end if				
			    }//end for  

               //Generating HTML for Auto Trans Ids

                for (iIndex = 0; iIndex < p_arrlstAutoTransId.Count; iIndex++)
                {
                    iAutoTransId = (int)p_arrlstAutoTransId[iIndex];

                    //Get all the details for this record (including ALL splits)
                    sSQL = "SELECT FUNDS_AUTO.CTL_NUMBER, FUNDS_AUTO.AUTO_TRANS_ID, FUNDS_AUTO.PRINT_DATE,"
                           + " FUNDS_AUTO.AMOUNT, FUNDS_AUTO.FIRST_NAME, FUNDS_AUTO.LAST_NAME, "
                           + " FUNDS_AUTO_SPLIT.FROM_DATE, FUNDS_AUTO_SPLIT.TO_DATE,"
                           + " FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE, FUNDS_AUTO_SPLIT.AMOUNT,FUNDS_AUTO.CLAIM_NUMBER,"
                           + " FUNDS_AUTO_SPLIT.INVOICE_NUMBER" 
                           + " FROM FUNDS_AUTO,FUNDS_AUTO_SPLIT"
                           + " WHERE FUNDS_AUTO.AUTO_TRANS_ID = " + iAutoTransId + " AND FUNDS_AUTO_SPLIT.AUTO_TRANS_ID = " + iAutoTransId;
                    
                    bFirst = true;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    if (objReader != null)
                    {
                        // Process rows - 1st one has all info, subsequent are only for detail info
                        while (objReader.Read())
                        {
                            sHTML += "*";
                            if (bFirst)
                            {
                                bFirst = false;
                                sTransDate = Conversion.GetDBDateFormat(objReader.GetString("PRINT_DATE"), "d");
                                sHTML += sTransDate + "|";

                                sHTML += objReader.GetString("CTL_NUMBER") + "|";

                                //formulate name of payee
                                sFirstName = objReader.GetString("FIRST_NAME");
                                if (sFirstName != "")
                                    sFirstName = sFirstName + " ";
                                sName = sFirstName + objReader.GetString("LAST_NAME");

                                sHTML += sName + "|";

                                //bVoid = objReader.GetBoolean("VOID_FLAG");
                                //if (bVoid)
                                //    sHTML += "Yes" + "|";
                                //else
                                    sHTML += "No" + "|";

                                sFromDate = Conversion.GetDBDateFormat(objReader.GetString("FROM_DATE"), "d");
                                sToDate = Conversion.GetDBDateFormat(objReader.GetString("TO_DATE"), "d");
                                sHTML += sFromDate + " - " + sToDate + "|";

                                objLocalCache.GetCodeInfo(Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), base.ClientId), ref sTransTypeShortCode, ref sTransTypeDesc, base.Adaptor.userLogin.objUser.NlsCode); //Aman Ml Change

                                sHTML += sTransTypeDesc + "|";
                                //Deb Multi Currency
                                if (p_objFundsAuto.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                                {
                                    //dExhRateBaseToPmt = FundsObject.Context.DbConn.ExecuteDouble(string.Format("SELECT EXCHANGE_RATE FROM CURRENCY_RATE WHERE SOURCE_CURRENCY_CODE={0} AND DESTINATION_CURRENCY_CODE={1}", FundsObject.Context.InternalSettings.SysSettings.BaseCurrencyType, FundsObject.PmtCurrencyType));
                                    if (p_objFundsAuto.BaseToPmtCurRate == 0)
                                        dExhRateBaseToPmt = CommonFunctions.ExchangeRateSrc2Dest(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, p_objFundsAuto.PmtCurrencyType, m_sConnectionString, base.ClientId);
                                    else
                                        dExhRateBaseToPmt = p_objFundsAuto.BaseToPmtCurRate;
                                    dAmount9 = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(9)));
                                    dAmount9 = dAmount9 * dExhRateBaseToPmt;
                                    if (p_objFundsAuto.PmtCurrencyType > 0)
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.PmtCurrencyType, dAmount9, m_sConnectionString, base.ClientId);
                                    }
                                    else if (p_objFundsAuto.ClaimCurrencyType > 0)
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.ClaimCurrencyType, dAmount9, m_sConnectionString, base.ClientId);
                                    }
                                    else
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, dAmount9, m_sConnectionString, base.ClientId);
                                    }
                                }
                                else
                                {
                                    dAmount9 = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(9)));
                                    sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, dAmount9, m_sConnectionString, base.ClientId);
                                }
                                //sAmount = string.Format("{0:C}", Riskmaster.Common.Conversion.ConvertStrToDouble(Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue(9))));
                                sHTML += sAmount + "|";

                                if (p_objFundsAuto.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                                {
                                    dAmount3 = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(3)));
                                    dAmount3 = dAmount3 * dExhRateBaseToPmt;
                                    if (p_objFundsAuto.PmtCurrencyType > 0)
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.PmtCurrencyType, dAmount3, m_sConnectionString, base.ClientId);
                                    }
                                    else if (p_objFundsAuto.ClaimCurrencyType > 0)
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.ClaimCurrencyType, dAmount3, m_sConnectionString, base.ClientId);
                                    }
                                    else
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, dAmount3, m_sConnectionString, base.ClientId);
                                    }
                                }
                                else
                                {
                                    dAmount3 = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(3)));
                                    sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, dAmount3, m_sConnectionString, base.ClientId);
                                }
                                //Deb Multi Currency
                                //compute amount of transaction and total payments/collections/voided amounts
                                //sAmount = string.Format("{0:C}", Riskmaster.Common.Conversion.ConvertStrToDouble(Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue(3))));
                                sHTML += sAmount + "|";

                                sHTML += objReader.GetString("CLAIM_NUMBER") + "|";
                                //sHTML += "\"|";                               
                                sHTML += objReader.GetString("INVOICE_NUMBER") + "|";
                            }
                            else
                            {
                                sHTML += "\"|";
                                sHTML += "\"|";
                                sHTML += "\"|";
                                sHTML += "\"|";

                                sFromDate = Conversion.GetDBDateFormat(objReader.GetString("FROM_DATE"), "d");
                                sToDate = Conversion.GetDBDateFormat(objReader.GetString("TO_DATE"), "d");
                                sHTML += sFromDate + " - " + sToDate + "|";

                                objLocalCache.GetCodeInfo(Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), base.ClientId), ref sTransTypeShortCode, ref sTransTypeDesc, base.Adaptor.userLogin.objUser.NlsCode); //Aman Ml Change

                                sHTML += sTransTypeDesc + "|";
                                //Deb Multi Currency
                                if (p_objFundsAuto.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                                {
                                    //dExhRateBaseToPmt = FundsObject.Context.DbConn.ExecuteDouble(string.Format("SELECT EXCHANGE_RATE FROM CURRENCY_RATE WHERE SOURCE_CURRENCY_CODE={0} AND DESTINATION_CURRENCY_CODE={1}", FundsObject.Context.InternalSettings.SysSettings.BaseCurrencyType, FundsObject.PmtCurrencyType));
                                    if (p_objFundsAuto.BaseToPmtCurRate == 0)
                                        dExhRateBaseToPmt = CommonFunctions.ExchangeRateSrc2Dest(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, p_objFundsAuto.PmtCurrencyType, m_sConnectionString, base.ClientId);
                                    else
                                        dExhRateBaseToPmt = p_objFundsAuto.BaseToPmtCurRate;
                                    dAmount9 = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(9)));
                                    dAmount9 = dAmount9 * dExhRateBaseToPmt;
                                    if (p_objFundsAuto.PmtCurrencyType > 0)
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.PmtCurrencyType, dAmount9, m_sConnectionString, base.ClientId);
                                    }
                                    else if (p_objFundsAuto.ClaimCurrencyType > 0)
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.ClaimCurrencyType, dAmount9, m_sConnectionString, base.ClientId);
                                    }
                                    else
                                    {
                                        sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, dAmount9, m_sConnectionString, base.ClientId);
                                    }
                                }
                                else
                                {
                                    dAmount9 = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(9)));
                                    sAmount = CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, dAmount9, m_sConnectionString, base.ClientId);
                                }
                                //sAmount = string.Format("{0:C}", Riskmaster.Common.Conversion.ConvertStrToDouble(Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue(9))));
                                //Deb Multi Currency
                                sHTML += sAmount + "|";

                                sHTML += "\"|";
                                sHTML += "\"|";
                                sHTML += "\"|";
                            } //end if							
                        }//end while
                    }//end if				
                }//end for                           
            
                if (objLocalCache != null)
                    objLocalCache.Dispose();
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			
			return ( sHTML );
		}        


        #endregion

		//Changed by Gagan for MITS 11991 : End
		#region OnValidate

		public override void OnValidate(ref bool Cancel)
		{
            DbReader objReader = null;
            CCacheFunctions objCCacheFunctions = null;
            SysSettings objSysSettings = null;
            objSysSettings = new SysSettings(m_sConnectionString, ClientId); //Ash - cloud
            bool bAutoCloseTransTypeUsed = false;
			bool bError = false;
            int iReserveTracking = 0;
			SetDoNotSaveDups();
            string sPayeeNames = string.Empty;
            string sError = string.Empty;
            string sSQL = string.Empty;
            // Perform data validation

            SortedList objNonThiredPartyPaymentList = GetThirdPartyPaymentsSortedList();//MITS 33560
            SortedList objThiredPartyPaymentList = GetThirdPartyPaymentsSortedList();
			SetPaymentsList(objNonThiredPartyPaymentList , objThiredPartyPaymentList);

            objCCacheFunctions = new CCacheFunctions(m_sConnectionString, base.ClientId);
            ReserveCurrent objRsvCurrent = null;
            bool bAllowSuppPayment = false;
               bool bContainsOpenReserve = false;
         ArrayList objClosedReservelist = null;
         bool bContainsCloseReserve = false;
            //Added by amitosh for eft
            if (objThiredPartyPaymentList.Count > 0)
            {
                for (int iIndex = 0; iIndex < objThiredPartyPaymentList.Count; iIndex++)
                {
                    FundsAuto objFundsAuto = (FundsAuto)objThiredPartyPaymentList.GetByIndex(iIndex);
                    if (objFundsAuto.PayNumber != this.FirstPayment(objThiredPartyPaymentList).PayNumber)
                        continue;

                    if (objFundsAuto.AccountId >0 && objFundsAuto.PayeeEid > 0 && IsEftBank(objFundsAuto.AccountId) && !CommonFunctions.hasEFTBankInfo(objFundsAuto.PayeeEid, objCache.GetCodeId("A", "EFT_BANKING_STATUS"), m_sConnectionString))
                    {
                        if (string.IsNullOrEmpty(sPayeeNames))
                        {
                            if (string.IsNullOrEmpty(objFundsAuto.FirstName))
                                sPayeeNames = objFundsAuto.LastName;
                            else
                                sPayeeNames = objFundsAuto.FirstName + " " + objFundsAuto.LastName;
                        }
                        else
                        {

                            if (string.IsNullOrEmpty(objFundsAuto.FirstName))
                                sPayeeNames = sPayeeNames + ","+ objFundsAuto.LastName;
                            else
                                sPayeeNames = sPayeeNames + "," + objFundsAuto.FirstName + " " + objFundsAuto.LastName;
                        }
                    }
                }

            }

            if (!string.IsNullOrEmpty(sPayeeNames))
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId), "Third Party Payees " + sPayeeNames + "  have no active banking information", BusinessAdaptorErrorType.Error);
                bError = true;
            }
            //end Amitosh
            if (FirstPayment(objNonThiredPartyPaymentList).ClaimId != 0)
            {
                if (bIsClaimClosed(FirstPayment(objNonThiredPartyPaymentList).ClaimId))
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), "The Claim Is Closed.  Scheduled Checks Can Not Be Created On Closed Claims.", BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }

			if( FirstPayment( objNonThiredPartyPaymentList ).AutoSplitList.Count <= 0 )
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.AutoChecks.NoPaymentFound", base.ClientId), BusinessAdaptorErrorType.Error);
				bError = true;
			}

			if( FirstPayment( objNonThiredPartyPaymentList ).Amount <= 0 )
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.AutoChecks.NegativePaymentsError", base.ClientId), BusinessAdaptorErrorType.Error);
				bError = true;
			}

            //MITS 30191 Raman Bhatia: Implementation of recovery reserve
            foreach (FundsAutoSplit objFundsAutoSplit in FirstPayment(objNonThiredPartyPaymentList).AutoSplitList)
            {
                
                if (objFundsAutoSplit.Context.LocalCache.GetRelatedShortCode(objFundsAutoSplit.ReserveTypeCode) == "R")
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("ValidationError.PaymentOnRecoveryReserve", base.ClientId), BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            if(!bError && objThiredPartyPaymentList.Count > 0)
            {
                foreach (FundsAutoSplit objFundsAutoSplit in FirstPayment(objThiredPartyPaymentList).AutoSplitList)
                {

                    if (objFundsAutoSplit.Context.LocalCache.GetRelatedShortCode(objFundsAutoSplit.ReserveTypeCode) == "R")
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("ValidationError.PaymentOnRecoveryReserve", base.ClientId), BusinessAdaptorErrorType.Error);
                        bError = true;
                    }
                }
            }

			// Make sure that if we are detail level that appropriate claimant or unit is selected
			FundsAuto oFundsAuto = FirstPayment( objNonThiredPartyPaymentList );
            //Start: rsushilaggar 30-Apr-2010 Vendors and Funds Approval MITS 20606
            //Start: Sumit-08/19/2010 - MITS# 21762 - Check for Payee Approval status only if setting is on from Utilities
            if (oFundsAuto.Context.InternalSettings.SysSettings.UseEntityApproval)
            {
                if (oFundsAuto.PayeeEntity.EntityApprovalStatusCode != objCache.GetCodeId("A", "ENTITY_APPRV_REJ"))
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.AutoChecks.EntityApproveStatus", base.ClientId), BusinessAdaptorErrorType.Error);
                    bError = true;
                }

                for (int i = 0; i < objThiredPartyPaymentList.Count; i++)
                {
                    FundsAuto oFundsAutoThirdParty = ((FundsAuto)objThiredPartyPaymentList.GetByIndex(i));
                    if (oFundsAutoThirdParty.PayeeEntity.EntityApprovalStatusCode != objCache.GetCodeId("A", "ENTITY_APPRV_REJ"))
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.AutoChecks.ThirdPartyEntityApproveStatus", base.ClientId), BusinessAdaptorErrorType.Error);
                        bError = true;
                        break;
                    }
                    oFundsAutoThirdParty.Dispose();
                }
            }
            //End: Sumit
            //End: rsushilaggar 
			Claim objClaim = (Claim) this.FundsAutoBatch.Context.Factory.GetDataModelObject("Claim",false) ;
			objClaim.MoveTo( oFundsAuto.ClaimId );

			int iLob = objClaim.LineOfBusCode;
            // MITS 18694 mcapps2 - Begin
            if (iLob > 0)
            {
                iReserveTracking = this.FundsAutoBatch.Context.InternalSettings.ColLobSettings[iLob].ReserveTracking;
            }
            // MITS 18694 mcapps2 - End
            if (IsClaimFrozen(oFundsAuto.ClaimId))
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.AutoChecks.PaymentFrozen", base.ClientId), BusinessAdaptorErrorType.Error);
                bError = true;
            }


            if ((iLob == 243 || iLob == 844 || (iLob == 241 && iReserveTracking == 1)) && !bError)   // WC or DI or GC detail level - require claimant entry
			{
				if (oFundsAuto.ClaimantEid == 0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.AutoChecks.ClaimantRequired", base.ClientId), BusinessAdaptorErrorType.Error);
					bError = true;
				}
			}
            else if ((iLob == 242 && iReserveTracking == 1) && !bError)  // VA detail level - either claimant or unit - but not both - must be selected
            {
                if (oFundsAuto.ClaimantEid != 0 && oFundsAuto.UnitId != 0)   // both unit and claimant filled in - don't allow
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.AutoChecks.ClaimantOrUnitOnly", base.ClientId), BusinessAdaptorErrorType.Error);
                    bError = true;
                }
                else if (oFundsAuto.ClaimantEid == 0 && oFundsAuto.UnitId == 0)  // neither claimant or unit filled in - don't allow
                {
                    if ((base.GetSysExDataNodeText("ClaimantEIdPassedIn") != null) && (base.GetSysExDataNodeText("ClaimantEIdPassedIn") != "0") && (base.GetSysExDataNodeText("ClaimantEIdPassedIn") != ""))
                    {
                        oFundsAuto.ClaimantEid = Conversion.ConvertStrToInteger(base.GetSysExDataNodeText("ClaimantEIdPassedIn"));
                    }
                    if ((base.GetSysExDataNodeText("UnitIdPassedIn") != null) && (base.GetSysExDataNodeText("UnitIdPassedIn") != "0") && (base.GetSysExDataNodeText("UnitIdPassedIn") != ""))
                    {
                        oFundsAuto.UnitId = Conversion.ConvertStrToInteger(base.GetSysExDataNodeText("UnitIdPassedIn"));
                    }
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.AutoChecks.ClaimantOrUnitRequired", base.ClientId), BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            if (base.GetSysExDataNodeText("IsDuplicatePayment") == "Ignore")
            {
                bError = true; //We have already tested the insufficient reserves and are validating duplicate payments so we don't need to go validate the reserves again.
            }
            //Shruti for Max rate and Insufficient reserve check
			// MITS 18694 mcapps2 - Begin
            if (iLob > 0)
            {
                if ((oFundsAuto.Context.InternalSettings.ColLobSettings[iLob].InsufFundsAutoFlag && !InsuffReserves(oFundsAuto, iLob)) && !bError)
                {
                    bError = true;
                }
            }
			// MITS 18694 mcapps2 - End
            if ((base.GetSysExDataNodeText("CompRate") != "Ignore" && oFundsAuto.Context.InternalSettings.SysSettings.UseFLMaxRate == -1 && objClaim.FilingStateId == 11 && !ValidatePaymentAmount(oFundsAuto)) && !bError)
            {
                bError = true;
            }
            //rkaur27 - JIRA RMA-18583 :Start
            Event objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
            if (objClaim.EventId > 0)
            {
                objEvent.MoveTo(objClaim.EventId);
            }

            if ((objEvent.DateOfEvent != "") && (this.FundsAutoBatch.StartDate != ""))
            {
                if (Conversion.ToDate(objEvent.DateOfEvent) > Conversion.ToDate(this.FundsAutoBatch.StartDate))
                {
                  string  sAutoCloseMessage = "The Transaction Date can not be before the Date of Event.";
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), sAutoCloseMessage, BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            //rkaur27 - JIRA RMA-18583 :End

            //Shruti for Max rate and Insufficient reserve check ends
            //Claim Type Change Option 01/09/2013  MITS 23295 
            CheckClaimTypeChangeOption();

            //Auto Close Begin  12/25/2012
            //-- Auto-close functionality
            bool bIncludeAutochecks = false;
            string sIncludeAutochecks = string.Empty;
            objSysSettings = new SysSettings(m_sConnectionString, ClientId); //Ash - cloud
            sSQL = "SELECT INC_SCHEDULED_CHECKS FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + iLob;
            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

            if (objReader != null)
            {
                while (objReader.Read())
                {
                    bIncludeAutochecks = Conversion.ConvertObjToBool(objReader.GetValue("INC_SCHEDULED_CHECKS"), base.ClientId);
                }

            }
            objReader.Close();
            if (bIncludeAutochecks)
            {
                string sTransTypeCodes = string.Empty;
                if (objNonThiredPartyPaymentList.Count > 0)
                {
                    foreach (FundsAutoSplit objThisSplit in FirstPayment(objNonThiredPartyPaymentList).AutoSplitList)
                    {
                        if (sTransTypeCodes == "")
                        {
                            sTransTypeCodes = objThisSplit.TransTypeCode.ToString();
                        }
                        else
                        {
                            sTransTypeCodes = sTransTypeCodes + ",";
                            sTransTypeCodes = sTransTypeCodes + objThisSplit.TransTypeCode.ToString();
                        }
                    }

                    bAutoCloseTransTypeUsed = false;
                    string sAutoCloseTransType = string.Empty;
                    if (sTransTypeCodes != string.Empty)
                    {
                        try
                        {
                            sSQL = "SELECT TRANS_TYPE_CODE FROM SYS_TRANS_CLOSE WHERE LINE_OF_BUS_CODE = " + iLob;
                            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                            if (objReader != null)
                            {
                                while (objReader.Read())
                                {
                                    if (sTransTypeCodes.Contains(Conversion.ConvertObjToStr(objReader.GetInt("TRANS_TYPE_CODE"))))
                                    {
                                        bAutoCloseTransTypeUsed = true;
                                        sAutoCloseTransType = Conversion.ConvertObjToStr(objReader.GetInt("TRANS_TYPE_CODE"));
                                        break;
                                    }
                                }
                            }
                            objReader.Close();
                        }
                        finally
                        {
                            if (objReader != null)
                            {
                                objReader.Dispose();
                            }
                        }
                    }
                    if (bAutoCloseTransTypeUsed)
                    {
                        string sAutoCloseMessage = string.Empty;
                        string sAutoCloseShortCode = string.Empty;
                        string sAutoCloseDesc = string.Empty;
                        int iAutoCloseTransType = Conversion.ConvertStrToInteger(sAutoCloseTransType);
                        objCCacheFunctions.GetCodeInfo(iAutoCloseTransType, ref sAutoCloseShortCode, ref sAutoCloseDesc);
                        sAutoCloseMessage = "The Transaction Type " + sAutoCloseDesc + " is set as 'Auto Close' type.  The claim Status for Claim " + oFundsAuto.ClaimNumber + " will be changed to 'Closed' when an Autocheck from this Autocheck Batch is Printed.";
                        //m_arrlstWarnings.Add(sAutoCloseMessage);
                        Errors.Add("WarningScriptError", sAutoCloseMessage, BusinessAdaptorErrorType.Warning);
                        bError = false;
                    }
                }
            }
            //Auto Close End

            if ((base.GetSysExDataNodeText("IsDuplicatePayment") == "Ignore") && (bError == true))
            {
                bError = false; //We set bError = true so we could skip checking insufficient reserves but now we need to set it back so we can continue with duplicate payment validation
            }
            //Changed by Gagan for MITS 11991 : Start

            if ((base.GetSysExDataNodeText("IsDuplicatePayment") != "Ignore") && !bError)
            {
                //MITS 19063 - mcapps2 Begin
                if (oFundsAuto.AutoBatchId == 0) 
                {
                    if (CheckForDuplicatePayments(oFundsAuto))
                    {
                        base.ResetSysExData("IsDuplicatePayment", "True");
                        base.ResetSysExData("HTMLDuplicatePayment", m_sConfirmation);

                        bError = true;
                    }
                    else
                    {
                        base.ResetSysExData("IsDuplicatePayment", "False");
                        base.ResetSysExData("HTMLDuplicatePayment", "");
                    }
                    if (m_DoNotSaveDups)
                    {
                        base.ResetSysExData("IsDoNotSaveDups", "True");
                    }
                    else
                    {
                        base.ResetSysExData("IsDoNotSaveDups", "");
                    }
                }
                else
                {
                    base.ResetSysExData("IsDuplicatePayment", "False");
                    base.ResetSysExData("HTMLDuplicatePayment", "");
                    base.ResetSysExData("IsDoNotSaveDups", "");
                }
                //MITS 19063 - mcapps2 End
            }
            else
            {
                //base.ResetSysExData("IsDuplicatePayment", "");
                //base.ResetSysExData("HTMLDuplicatePayment", "");
            }
            //Changed by Gagan for MITS 11991 : End

            // abhateja 04.06.2009
            // In case of a validation error need to reset the "FuturePayment" and "ThirdParty" grids 
            // because onUpdateForm() is not getting called in these cases(validation errors).
            // This is required for the user grid view control to work.
            if (bError)
            {
                // Append Future Payments Grid Data
                this.AppenedFuturePaymentsGridData(objNonThiredPartyPaymentList);

                // Append Third Party Payments Grid Data
                this.AppenedThirdPartyPaymentsGridData(objThiredPartyPaymentList, objNonThiredPartyPaymentList);	
            }
			else
            {
                //tanwar2 - mits 33318 - start
                //Validation errors can still be added from scripting module.
                //Without these lines of codes - the grids would get removed when validation error occurrs in scripting module.
                this.AppenedFuturePaymentsGridData(objNonThiredPartyPaymentList);
                this.AppenedThirdPartyPaymentsGridData(objThiredPartyPaymentList, objNonThiredPartyPaymentList);
                //tanwar2 - mits 33318 - end
            }
            if ((base.GetSysExDataNodeText("IsDuplicatePayment") == "Ignore") && !bError)
            {
                base.ResetSysExData("IsDuplicatePayment", "");
                base.ResetSysExData("HTMLDuplicatePayment", "");
            }

            if (Conversion.ConvertStrToBool(FundsAutoBatch.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString()))
            {
                FundsAutoList objFundsAutoList = this.FundsAutoBatch.FundsAutoList;
                 objClosedReservelist = new ArrayList();
                foreach (FundsAuto objFundsAuto in objFundsAutoList)
                {
                    foreach (FundsAutoSplit objFundsAutoSplit in objFundsAuto.AutoSplitList)
                    {

                        //if (objFundsAutoSplit.RCRowId > 0)
                        //{
                        //    objRsvCurrent = (ReserveCurrent)objFundsAutoSplit.Context.Factory.GetDataModelObject("ReserveCurrent", false);
                        //    objRsvCurrent.MoveTo(objFundsAutoSplit.RCRowId);
                        //    if (objFundsAutoSplit.Context.LocalCache.GetShortCode(objFundsAutoSplit.Context.LocalCache.GetRelatedCodeId(objRsvCurrent.ResStatusCode)) == "C")
                        //    {
                        //        bisSuppPayment = true;
                        //        break;

                        //    }
                        //}


                        if (objFundsAutoSplit.RCRowId > 0)
                        {
                            objRsvCurrent = (ReserveCurrent)FundsAutoBatch.Context.Factory.GetDataModelObject("ReserveCurrent", false);
                            objRsvCurrent.MoveTo(objFundsAutoSplit.RCRowId);
                            if ((FundsAutoBatch.Context.LocalCache.GetShortCode(FundsAutoBatch.Context.LocalCache.GetRelatedCodeId(objRsvCurrent.ResStatusCode)) == "O"))
                                bContainsOpenReserve = true;
                            if ((FundsAutoBatch.Context.LocalCache.GetShortCode(FundsAutoBatch.Context.LocalCache.GetRelatedCodeId(objRsvCurrent.ResStatusCode)) == "C") || (FundsAutoBatch.Context.LocalCache.GetShortCode(FundsAutoBatch.Context.LocalCache.GetRelatedCodeId(objRsvCurrent.ResStatusCode)) == "R"))
                            {
                                bAllowSuppPayment = true;
                                bContainsCloseReserve = true;
                                if ((objClosedReservelist.Count > 0) && (!objClosedReservelist.Contains(objRsvCurrent.ReserveTypeCode)))
                                {
                                    bAllowSuppPayment = false;
                                    break;
                                }

                                objClosedReservelist.Add(objRsvCurrent.ReserveTypeCode);



                            }
                            if (bContainsCloseReserve && bContainsOpenReserve)
                            {
                                bAllowSuppPayment = false;
                                break;
                            }
                            objRsvCurrent = null;

                        }


                    }

                }

                if (objRsvCurrent != null)
                {
                    objRsvCurrent.Dispose();
                    objRsvCurrent = null;
                }
                if (bContainsCloseReserve)
                {
                    if (!bAllowSuppPayment)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.AutoChecks.SupplementalPaymentError", base.ClientId), BusinessAdaptorErrorType.Error);
                        bError = true;

                    }
                    else
                    {

                      foreach (FundsAuto objFundsAuto in objFundsAutoList)
                        {
                            objFundsAuto.IsSupplementalPay = true;
                        }
                    }
                }
            }




			// Return true if there were validation errors
			Cancel = bError;
		}
		#endregion 

        /// <summary>
        /// This method implements the Claim Type Change Option if any.   MITS 23295 
        /// </summary>
        private void CheckClaimTypeChangeOption()
        {
            int iCurrClaimType = 0;
            int iLOB = 0;
            int iYesCodeId = 0;
            int iFirstNewClaimType = -1;
            int iClaimId = 0;
            DbReader objReader = null;
            DbConnection objConn = null;
            Claim objClaim = null;

            SortedList objNonThiredPartyPaymentList = GetThirdPartyPaymentsSortedList();//MITS 33560
            SortedList objThiredPartyPaymentList = GetThirdPartyPaymentsSortedList();
            SetPaymentsList(objNonThiredPartyPaymentList, objThiredPartyPaymentList);
            iClaimId = FirstPayment(objNonThiredPartyPaymentList).ClaimId;

            try
            {
                objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + iClaimId);
                if (objReader.Read())
                {
                    iCurrClaimType = objReader.GetInt32("CLAIM_TYPE_CODE");
                }
                objReader.Close();
                objClaim = (Claim)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(iClaimId);

                iLOB = objClaim.LineOfBusCode;
                iYesCodeId = this.objCache.GetCodeId("A15", "PSO_YES_NO"); 
                objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT DEST_CLAIM_TYPE FROM CL_CHG_OPTIONS WHERE LOB = " + iLOB + " AND SOURCE_CLAIM_TYPE = " + iCurrClaimType + " AND INC_SCHEDULED_CHECKS = " + iYesCodeId);
                if (!objReader.Read())
                {
                    return;
                }
                objReader.Close();

                foreach (FundsAutoSplit objFundsAutoSplit in FirstPayment(objNonThiredPartyPaymentList).AutoSplitList)
                {
                    if (iFirstNewClaimType == -1)
                    {
                        objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT DEST_CLAIM_TYPE FROM CL_CHG_OPTIONS WHERE  LOB = " + iLOB + " AND SOURCE_CLAIM_TYPE = " + iCurrClaimType + " AND TRIG_TRANS_TYPE = " + objFundsAutoSplit.TransTypeCode + " AND INC_SCHEDULED_CHECKS = " + iYesCodeId);
                        if (objReader.Read())
                        {
                            iFirstNewClaimType = objReader.GetInt32("DEST_CLAIM_TYPE");
                            break;
                        }
                        objReader.Dispose();
                    }
                }
                if (iFirstNewClaimType == -1)
                {
                    return;
                }
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery("UPDATE CLAIM SET CLAIM_TYPE_CODE = " + iFirstNewClaimType + " WHERE CLAIM_ID = " + iClaimId);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("AutoClaimChecksForm.CheckClaimTypeChangeOption.Error", base.ClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }

		#region AfterApplyLookupData
		public override void AfterApplyLookupData(XmlDocument objPropertyStore)
		{
			base.AfterApplyLookupData (objPropertyStore);

			//copy lookup data into fundsauto
			XmlNode objFundsAutoNode = SysEx.DocumentElement.SelectSingleNode("FundsAuto");
			if(objFundsAutoNode != null)
			{
				XmlNode objPayeeEntityNode = SysEx.DocumentElement.SelectSingleNode("FundsAuto/PayeeEntity");

				objFundsAutoNode.SelectSingleNode("LastName").InnerText = objPayeeEntityNode.SelectSingleNode("LastName").InnerText; 
				objFundsAutoNode.SelectSingleNode("FirstName").InnerText = objPayeeEntityNode.SelectSingleNode("FirstName").InnerText; 
				objFundsAutoNode.SelectSingleNode("MiddleName").InnerText = objPayeeEntityNode.SelectSingleNode("MiddleName").InnerText; 
				objFundsAutoNode.SelectSingleNode("Addr1").InnerText = objPayeeEntityNode.SelectSingleNode("Addr1").InnerText; 
				objFundsAutoNode.SelectSingleNode("Addr2").InnerText = objPayeeEntityNode.SelectSingleNode("Addr2").InnerText; 
                objFundsAutoNode.SelectSingleNode("Addr3").InnerText = objPayeeEntityNode.SelectSingleNode("Addr3").InnerText;// JIRA 6420 pkandhari
                objFundsAutoNode.SelectSingleNode("Addr4").InnerText = objPayeeEntityNode.SelectSingleNode("Addr4").InnerText;// JIRA 6420 pkandhari
				objFundsAutoNode.SelectSingleNode("City").InnerText = objPayeeEntityNode.SelectSingleNode("City").InnerText; 
				objFundsAutoNode.SelectSingleNode("StateId").InnerText = objPayeeEntityNode.SelectSingleNode("StateId").InnerText; 
				objFundsAutoNode.SelectSingleNode("StateId").Attributes["codeid"].Value = objPayeeEntityNode.SelectSingleNode("StateId").Attributes["codeid"].Value; 
				objFundsAutoNode.SelectSingleNode("ZipCode").InnerText = objPayeeEntityNode.SelectSingleNode("ZipCode").InnerText; 
				//objFundsAutoNode.SelectSingleNode("PayeeTypeCode").InnerText = objPayeeEntityNode.SelectSingleNode("StateId").InnerText; 
				objFundsAutoNode.SelectSingleNode("PayeeTypeCode").Attributes["codeid"].Value = SysEx.DocumentElement.SelectSingleNode("/SysExData/PayeeTypeCode").InnerText; 
			}
			
		}

		#endregion 
				
		#region OnUpdateForm
		// Vaibhav 14 Feb 2006 :
		// Following function has been reconstructed and based on the SortedLists of payments.
		// Remove dependency on the Database sort.
		public override void OnUpdateForm()
		{
            int iUseClaimProgressNotes = 0;
			base.OnUpdateForm ();
            //pmahli MITS 11535 2/15/2008 - Start
            //If permission is not granted in SMS some fields are made readonly
            if (!FundsAutoBatch.Context.RMUser.IsAllowedEx(m_SecurityId, FormBase.RMO_AUTOCHECKS_EDITPAYEE))
            {
                base.AddReadOnlyNode("pye_firstname");
                base.AddReadOnlyNode("pye_middlename");
                base.AddReadOnlyNode("pye_addr1");
                base.AddReadOnlyNode("pye_addr2");
                base.AddReadOnlyNode("pye_addr3");// JIRA 6420 pkandhari
                base.AddReadOnlyNode("pye_addr4");// JIRA 6420 pkandhari
                base.AddReadOnlyNode("pye_city");
                base.AddReadOnlyNode("pye_stateid");
                base.AddReadOnlyNode("pye_zipcode");
            }
            if (!m_fda.userLogin.IsAllowedEx(RMO_EDIT_MAILTO))
            {
                base.AddReadOnlyNode("mailto_addr1");
                base.AddReadOnlyNode("mailto_addr2");
                base.AddReadOnlyNode("mailto_addr3");
                base.AddReadOnlyNode("mailto_addr4");
                base.AddReadOnlyNode("mailto_city");
                base.AddReadOnlyNode("mailto_countrycode");
                base.AddReadOnlyNode("mailto_stateid");
                base.AddReadOnlyNode("mailto_county");
                base.AddReadOnlyNode("mailto_zipcode");
            }
            if (SysEx.SelectSingleNode("/SysExData/HdnAllowMailToAddress") != null)
            {
                base.ResetSysExData("HdnAllowMailToAddress", m_fda.userLogin.IsAllowedEx(RMO_EDIT_MAILTO).ToString());
            }
            //skhare7 R8 Combine Payment Pay to The order Of setting Permissions
            //asharma326 MITS 31267
            if (!m_fda.userLogin.IsAllowedEx(RMB_FUNDS_AUTOCHK, RMO_EDIT_PAYEE))
            {
                //base.AddKillNode("pye_lastname_creatable");
                if (!m_fda.userLogin.IsAllowedEx(RMB_FUNDS_AUTOCHK, RMO_CHANGE_PAYEE) && this.FundsAutoBatch.AutoBatchId > 0)
                {
                    base.AddReadOnlyNode("pye_lastname");
                }
                else
                {
                    base.AddReadWriteNode("pye_lastname");
                }
            }
            else
            {
				//base.AddDisplayNode("pye_lastname_creatable");
                base.AddReadWriteNode("pye_lastname");
            }

            // akaushik5 Added for RMA-15118 Starts
            if (!m_fda.userLogin.IsAllowedEx(RMB_FUNDS_AUTOCHK, RMO_CHANGE_PAYEE) && this.FundsAutoBatch.AutoBatchId > 0)
            {
                base.AddReadOnlyNode("cbopayeetype");
                base.AddReadOnlyNode("entitylistbtndel");
                base.ResetSysExData("IsChangePayeeAllowed", "false");
            }
            else
            {
                base.AddReadWriteNode("cbopayeetype");
                base.ResetSysExData("IsChangePayeeAllowed", "true");
            }
            // akaushik5 Added for RMA-15118 Ends

            //vkumar258 RMA-15086 Starts:
            if (!m_fda.userLogin.IsAllowedEx(RMO_PEOPLE, RMO_CREATE) || !m_fda.userLogin.IsAllowedEx(RMO_ENTITYMAINTENANCE, RMO_CREATE))
            {
                base.AddKillNode("pye_lastname_creatable");
            }
            else
            {
                base.AddDisplayNode("pye_lastname_creatable");
            }
            //vkumar258 RMA-15086 Ends

            //Ankit Start : Financial Enhancement - Prefix and Suffix Changes
            //Ankit Start : Getting User's Language Code
            UserLogin objUserLogin = new UserLogin(base.ClientId);//rkaur27
            int iUserLangCode = objUserLogin.objUser.NlsCode;
            int iBaseLangCode = Convert.ToInt32(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]); //rkaur27
            //Ankit End
            SetCheckOptions();
            //Ankit End
			  //zmohammad MITs 34390 Start
            if (!m_fda.userLogin.IsAllowedEx(RMB_FUNDS_AUTOCHK, RMO_ALLOW_EDIT_PAYTOTHEOREDEROF))
            {
                base.AddReadOnlyNode("paytotheorderbtnMemo");

            }
            else
            {
                base.AddReadWriteNode("paytotheorderbtnMemo");

            }
            //End skhare7
          
            if (!m_fda.userLogin.IsAllowedEx(RMB_FUNDS_AUTOCHK, RMO_ALLOW_EDIT_GENERATEPAYTOTHEOREDEROF))
            {
                base.AddReadOnlyNode("btngeneratepaytotheorderof");
            }
            else
            {
                base.AddReadWriteNode("btngeneratepaytotheorderof");
            }
              //zmohammad MITs 34390 End
            //MITS 18536 Start
            if (SysEx.SelectSingleNode("/SysExData/AddCombPayAccount") == null)
            {
                base.ResetSysExData("AddCombPayAccount", "");
            }
            //MITS 18536 End

            //base.AddReadOnlyNode("clm_lastname");
            //base.AddReadOnlyNode("unit");

            //pmahli MITS 11535 2/15/2008 - End
            Claim objClaim = null;  //zalam mits:-11188 03/24/2008 

            SortedList objNonThiredPartyPaymentList = GetThirdPartyPaymentsSortedList();//MITS 33560
            SortedList objThiredPartyPaymentList = GetThirdPartyPaymentsSortedList();
			SetPaymentsList( objNonThiredPartyPaymentList , objThiredPartyPaymentList );

			// If New Autocheck Record, Set the default FundsAutoObject for Non-Thired-Party-Payment.
			if( objNonThiredPartyPaymentList.Count <= 0 )
			{
				FundsAuto objFundsAutoFirst = (FundsAuto) this.FundsAutoBatch.Context.Factory.GetDataModelObject("FundsAuto",false);     				
				objFundsAutoFirst.PayNumber = 1 ;
				objNonThiredPartyPaymentList.Add( objFundsAutoFirst.PayNumber , objFundsAutoFirst );
				
				this.FundsAutoBatch.TotalPayments = 1;
				this.FundsAutoBatch.StartDate = DateTime.Now.ToShortDateString(); 
				this.FundsAutoBatch.EndDate = DateTime.Now.ToShortDateString();
				this.FundsAutoBatch.PaymentInterval = 1040;

				// For moving from Financials. ( Non FDM Screen to Fdm Screen ).
				XmlNode objSysExternalParam = base.SysEx.SelectSingleNode( "//SysExternalParam" );
				if( objSysExternalParam != null )
				{
                    int iUnitID = 0;
                    int iUnitRowID = 0;
                    string sSQL = string.Empty;

                    sFromRsvListingPage = base.GetSysExDataNodeText("//SysExternalParam/FromRsvListing");

					objFundsAutoFirst.ClaimNumber = base.GetSysExDataNodeText( "//SysExternalParam/ClaimNumber");
					objFundsAutoFirst.ClaimId = base.GetSysExDataNodeInt( "//SysExternalParam/ClaimId");

                    base.ResetSysExData("ClaimantEIdPassedIn", base.GetSysExDataNodeText("//SysExternalParam/ClaimantEid"));
                    base.ResetSysExData("UnitIdPassedIn", base.GetSysExDataNodeText("//SysExternalParam/UnitID"));

                    iUnitRowID = Conversion.ConvertObjToInt(base.GetSysExDataNodeText("//SysExternalParam/UnitRowID"), base.ClientId);
                    iUnitID = Conversion.ConvertObjToInt(base.GetSysExDataNodeText("//SysExternalParam/UnitID"), base.ClientId);
                    if (iUnitID == 0)
                    {
                        if (iUnitRowID > 0)
                        {
                            sSQL = "SELECT UNIT_ID FROM UNIT_X_CLAIM WHERE UNIT_ROW_ID = " + iUnitRowID;
                            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                            {
                                if (objReader.Read())
                                {
                                    objFundsAutoFirst.UnitId = objReader.GetInt("UNIT_ID");
                                    iUnitID = objFundsAutoFirst.UnitId;

                                }
                            }
                        }
                    }
                   
                    base.ResetSysExData("UnitIdPassedIn", iUnitID.ToString());
                    base.ResetSysExData("UnitrowIdPassedIn", iUnitRowID.ToString());
				}
                if (sFromRsvListingPage == "")
                {
                    sFromRsvListingPage = "0";
                }
                else
                {
                    sFromRsvListingPage = "1";
                } // else
                if (!FundsAutoBatch.Context.RMUser.IsAllowedEx(m_SecurityId, FormBase.RMO_AUTOCHECKS_EDIT_ENTER_CONTROL_NUMBER))
                {//Changed by gagan for MITS 11589 : End 
                    base.AddReadOnlyNode("ctlnumber");
                }
			}
            else
            {
                if (this.FundsAutoBatch.AutoBatchId > 0)
                {
                    base.AddReadOnlyNode("ctlnumber");
                }
            }
			// Set and Appened the Claim Info Data to SysEx.
			this.SetClaimInfo( FirstPayment( objNonThiredPartyPaymentList ) );
            //AA 7199 Start
            FundsAuto objFundsAutoCheck = FirstPayment(objNonThiredPartyPaymentList);
            if (!FundsAutoBatch.Context.RMUser.IsAllowedEx(RMO_EDIT_MAILTO) || objCache.GetShortCode(objFundsAutoCheck.DstrbnType) == "WTR" || objCache.GetShortCode(objFundsAutoCheck.DstrbnType) == "EFT")
            {
                base.AddReadOnlyNode("mailto_addr1");
                base.AddReadOnlyNode("mailto_addr2");
                base.AddReadOnlyNode("mailto_addr3");
                base.AddReadOnlyNode("mailto_addr4");
                base.AddReadOnlyNode("mailto_city");
                base.AddReadOnlyNode("mailto_countrycode");
                base.AddReadOnlyNode("mailto_stateid");
                base.AddReadOnlyNode("mailto_county");
                base.AddReadOnlyNode("mailto_zipcode");
            }
            if (!(objCache.GetShortCode(objFundsAutoCheck.DstrbnType) == "WTR" || objCache.GetShortCode(objFundsAutoCheck.DstrbnType) == "EFT"))
            {
                base.AddReadWriteNode("mailto_lastname");
                base.AddReadWriteNode("mailtopye");
            }
            //AA 7199 End

            if (SysEx.SelectSingleNode("/SysExData/Lookuptaxid") == null)  //MITS 32715 mcapps2 Start
            {
                base.ResetSysExData("Lookuptaxid", "");
            }

            if (!m_fda.userLogin.IsAllowedEx(RMB_FUNDS_AUTOCHK, RMO_LOOKUP_TAX_ID)) // Dont allow look up with tax id if permission not given
            {
                base.AddReadOnlyNode("pye_taxid");
                base.ResetSysExData("Allowtaxidlookup", "");
            }
            else
            {
                base.AddReadWriteNode("pye_taxid");
                base.ResetSysExData("Allowtaxidlookup", "true");
            }

            if (m_fda.userLogin.IsAllowedEx(RMB_FUNDS_AUTOCHK, RMO_LOOKUP_TAX_ID))
            {
                if (base.GetSysExDataNodeText("Gettaxid") == "get")
                {
                    string sLookupEntityEIDs = sGetEntityWithTaxId();
                    //int iLookUpEntityEID = iGetEntityWithTaxId();
                    //if (iLookUpEntityEID != 0)
                    if (sLookupEntityEIDs != string.Empty)
                    {
                        base.ResetSysExData("Foundeid", sLookupEntityEIDs);
                        base.ResetSysExData("Gettaxid", "found");
                    }
                    else
                    {
                        base.ResetSysExData("Foundeid", "");
                        base.ResetSysExData("Gettaxid", "notfound");
                    }
                }
                else
                {
                    base.ResetSysExData("Foundeid", "");
                    base.ResetSysExData("Gettaxid", "");
                }
            }
            else
            {
                base.ResetSysExData("Gettaxid", "");
            }  //MITS 32715 mcapps2 End
            //Set Claim Number Field Read Only
            this.SetClaimNumberReadOnly( FirstPayment( objNonThiredPartyPaymentList ) );
		
			// Appened FundsAuto Node.
			this.AppenedFundsAutoNode( FirstPayment( objNonThiredPartyPaymentList ) );									
			
			// This code should be moved to a common place. Bank Account 
			// List is being fetched at more than one place in the product.
			this.AppendBankAccountList();

			// Appened Payee Type Combobox List data.
			//rupal:r8 enh to include person involved as payee type
            this.AppendPayeeTypeList(FirstPayment(objNonThiredPartyPaymentList).PayeeTypeCode, FirstPayment(objNonThiredPartyPaymentList).PayeeEid); 
			
			// Appened Payee Type combobox selected Code
			this.AppenedSelectedPayeeTypeCode( FirstPayment( objNonThiredPartyPaymentList ).PayeeTypeCode , FirstPayment( objNonThiredPartyPaymentList ).PayeeEid );

			// Amounts for a single pay number			
			this.CalculateAmounts( objNonThiredPartyPaymentList , objThiredPartyPaymentList ); 

			// Appened Payment Information Grid Data
			this.AppenedPaymentInformationGridData( objNonThiredPartyPaymentList ); 
  			
			// Appened Future Payments Grid Data
			this.AppenedFuturePaymentsGridData( objNonThiredPartyPaymentList );

            if (SysEx.SelectSingleNode("/SysExData/ReserveID") == null)
            {
                base.ResetSysExData("ReserveID", "");
                base.ResetSysExData("ReserveIDDesc", "");
            }
            if (SysEx.SelectSingleNode("/SysExData/PolID") == null)
            {
                base.ResetSysExData("PolID", "");
                base.ResetSysExData("PolIDDesc", "");
            }
            //rupal:start, r8 unit implementation
            if (SysEx.SelectSingleNode("/SysExData/PolUnitRowID") == null)
            {
                base.ResetSysExData("PolUnitRowID", "");
                base.ResetSysExData("PolUnitDesc", "");
            }
            //rupal:end
            if (SysEx.SelectSingleNode("/SysExData/CovID") == null)
            {
                base.ResetSysExData("CovID", "");
                base.ResetSysExData("CovIDDesc", "");
            }
            if (SysEx.SelectSingleNode("/SysExData/LossID") == null)
            {
                base.ResetSysExData("LossID", "");
                base.ResetSysExData("LossDesc", "");
            }	
            setReserveID(objNonThiredPartyPaymentList);
								
			// Appened Third Party Payments Grid Data
			/*
			 * Vaibhav Kaushik 11 Feb 2006 
			 * We are passing the NonThiredPartyPaymentList only and only to calculate "Deduct Against Payee Flag".
			 * This Flag does not have the direct Database property and caculated by looking at the NonThiredPartyPaymentList
			 * 
			 * NonThiredPartyPaymentList MUST not be used except caculating "Deduct Against Payee Flag".
			 * 
			 * For all other purposes, objThiredPartyPaymentList MUST be used.
			 */
			this.AppenedThirdPartyPaymentsGridData( objThiredPartyPaymentList , objNonThiredPartyPaymentList );	
			
			// Read Utility setting for Max number of Autochecks. 
			this.GetMaxNumberOfAutoChecks();
            //AA 7199            
            if (base.SysEx.SelectSingleNode("//dupeoverride") == null)
                base.ResetSysExData("dupeoverride", "");
            if (base.SysEx.SelectSingleNode("//HdnIsDupAddr") == null)
                base.ResetSysExData("HdnIsDupAddr", "");
            if (base.SysEx.SelectSingleNode("//HdnDupAddrId") == null)
                base.ResetSysExData("HdnDupAddrId", "");
            if (base.SysEx.SelectSingleNode("//HdnSearchString") == null)
                base.ResetSysExData("HdnSearchString", "");
            if (base.SysEx.SelectSingleNode("//HdnEditAddress") == null)
                base.ResetSysExData("HdnEditAddress", "");
            if (base.SysEx.SelectSingleNode("//HdnAllowMailToAddress") == null)
                base.ResetSysExData("HdnAllowMailToAddress", "");

			base.ResetSysExData( "FinalPaymentDate" , Conversion.GetDBDateFormat( FundsAutoBatch.EndDate , "MM/dd/yyyy" ) );							
            //Shruti
            //base.ResetSysExData("Skipreservetypes", "");
            //base.ResetSysExData("InsuffReserve", "");
            //base.ResetSysExData("IsDuplicatePayment", "");
            //base.ResetSysExData("UnitIDIsUnitRowId", "");
            //base.ResetSysExData("InsuffReserveNonNeg", "");
            //base.ResetSysExData("CompRate", "");
            //base.ResetSysExData("Reason", "");
            //zmohammad MITS 37253 : Merging into CDR Start
            this.setXMLVariables();
            //zmohammad MITS 37253 : Merging into CDR End
            //zalam mits:-11188 03/24/2008
            objClaim = (Claim)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(FirstPayment(objNonThiredPartyPaymentList).ClaimId);
            if (objClaim.ClaimId > 0)
            {
                //mgaba2:mits 29116
                iUseClaimProgressNotes = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ClmProgressNotesFlag;
                if (iUseClaimProgressNotes == 0)
                {
                    base.AddKillNode("enhancednotes");
                }
                else
                {
                    base.AddDisplayNode("enhancednotes");
                }
            }
            else
            {
                base.AddKillNode("enhancednotes");
            }
            int iDeptEid = (objClaim.Parent as Event).DeptEid;            
            base.ResetSysExData("DeptEid", iDeptEid.ToString());
            //zalam mits:-11188 03/24/2008 END
            //Start by Shivendu for MITS 7293
            CheckFreezePaymentPemission();
            //End by Shivendu for MITS 7293

			//pmittal5 Mits 15308 05/08/09 - Enable the Tax Id field if New Payee Entity is getting added
            if (base.SysEx.DocumentElement.SelectSingleNode("//FundsAuto/PayeeEid").Attributes["codeid"].Value == "0" && base.SysEx.DocumentElement.SelectSingleNode("//FundsAuto/LastName").InnerText != "")
            {
                base.AddReadWriteNode("pye_taxid");
                base.ReadOnlyNodes.Remove("pye_taxidk");
            }
            else
            {
                base.ReadWriteNodes.Remove("pye_taxidk");
                base.AddReadOnlyNode("pye_taxid");
            }//End - pmittal5
            //BOB
            //string sMultipleCoverages = string.Empty;
           
            /*rupal:start: changed the code as subsequent server side postback were setting the MultipleCoverages to blank even if it is on
            if (SysEx.SelectSingleNode("/SysExData/MultipleCoverages") == null && sMultipleCoverages == "-1")
            {
                base.ResetSysExData("MultipleCoverages", "True");
            }
            else
            {
                base.ResetSysExData("MultipleCoverages", "");
            }*/

            //sMultipleCoverages = FundsAutoBatch.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString();
            if (FundsAutoBatch.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString() == "-1")
            {
                base.ResetSysExData("MultipleCoverages", "True");
            }
            else
            {
                base.ResetSysExData("MultipleCoverages", "");
            }
            //rupal:end
            //BOB
            //Added by Amitosh for R8 enhancement of Combined Payments
            XmlDocument objDoc = base.SysEx;
            RenderPayeesList(ref objDoc);
            //Added by amitosh for R8 enhancement of EFT

            //start:rupal, MITS 26022, refresh the page on adjustprintdatesflag flag change
            //when a new auto check record, if schedules dates is ON for auto checks, "Auto Adjust Print Dates" check box will bydefault be selected and we need to update the print dates also accordingly 
            //if AutoTransId == 0, ==> a new auto check record, update print dates if required based on schedule dates settings
            //base.AddReadOnlyNode("IsEFTPayment");
            //JIRA:438 START: ajohari2
            int iBankAccount = 0;
            bool bIsEFTPayment = true;

            if (base.SysEx.SelectSingleNode("//FundsAuto/AccountId") != null && base.SysEx.SelectSingleNode("//FundsAuto/AccountId").InnerText != "")
            {
                int.TryParse(base.SysEx.SelectSingleNode("//FundsAuto/AccountId").InnerText, out iBankAccount);
            }

            if (base.SysEx.SelectSingleNode("//FundsAuto/hdnIsEFTPayment") == null)
            {
                base.ResetSysExData("hdnIsEFTPayment", "false");
            }

            if (iBankAccount > 0 && IsEftBank(iBankAccount))
            {
                // npadhy JIRA 6418 Starts - As we have removed the EFT Checkbox from UI, we remove the Enabled control property
                //base.AddDisplayNode("IsEFTPayment");
                // npadhy JIRA 6418 Ends - As we have removed the EFT Checkbox from UI, we remove the Enabled control property
                if (base.SysEx.SelectSingleNode("//FundsAuto/IsEFTPayment") != null && base.SysEx.SelectSingleNode("//FundsAuto/IsEFTPayment").InnerText != "")
                {
                    bool.TryParse(base.SysEx.SelectSingleNode("//FundsAuto/IsEFTPayment").InnerText, out bIsEFTPayment);
                }

                if (bIsEFTPayment)
                {
                    base.ResetSysExData("hdnIsEFTPayment", "true");
                }
                else
                {
                    base.ResetSysExData("hdnIsEFTPayment", "false");
                }

            }
            // npadhy JIRA 6418 Starts - As we have removed the EFT Checkbox from UI, we remove the disabled control property
            //else
            //{
            //    //base.AddReadOnlyNode("IsEFTPayment");
            //}
            // Take the Distribution Type of EFT to UI as we will compare the Distribution Type selected value with this value.
            int iEFTDistributionType = objCache.GetCodeId("EFT", "DISTRIBUTION_TYPE");
            base.ResetSysExData("EFTDistributionType", iEFTDistributionType.ToString());
            string sEFTDistributionTypeDesc = string.Empty;
            sEFTDistributionTypeDesc = objCache.GetCodeDesc(iEFTDistributionType);
            base.ResetSysExData("EFTDistributionTypeDesc", sEFTDistributionTypeDesc);
            // npadhy JIRA 6418 Starts - As we have removed the EFT Checkbox from UI, we remove the disabled control property
            //JIRA:438 End: 
            base.AddReadOnlyNode("IsSupplementalPay");
            if (!(CommonFunctions.IsScheduleDatesOn("SCH_DATE_AUTOCHECK", FundsAutoBatch.Context.DbConn.ConnectionString, base.ClientId)))
            {
                base.AddKillNode("adjustprintdatesflag");
            }            
            //if control comes to else part, adjustprintdatesflag is true,MITS 26022
            else if (base.GetSysExDataNodeText("//FundsAuto/AutoTransId").Equals("0"))
            {
                
                //when a new auto check record, if schedules dates is ON for auto checks, "Auto Adjust Print Dates" check box will bydefault be selected and we need to update the print dates also accordingly 
                //in case of new funds auto record, mark this flag to true explicitely
                FundsAutoBatch.AdjustPrintDatesFlag = true;
                               
            }
            //end:rupal
            //Added by amitosh for QBE enhancement of Payee Phrase List
            if (base.SysEx.SelectSingleNode("/SysExData/FundsAutoXPayeeList").ChildNodes.Count == 0)
                base.AddReadOnlyNode("payeephrase");

            AppendPayeePhraseList();

            //end Amitosh 
            
            //Ankit Start : Financial Enhancements - Memo Phrase change
            base.AddDisplayNode("memophrase");
            AppendMemoPhraseList(iUserLangCode, iBaseLangCode);
            //Ankit End
            
            //Ankit Start : Financial Enhancements - Payee Phrase Change
            base.CreateSysExData("SavePayeePhrase", "");
            //Ankit End

            //Ankit Start : Financial Enhancement - Prefix and Suffix Changes
            base.ResetSysExData("IsPrefixEnable", m_bIsPrefixEnabled.ToString().ToLower());
            base.ResetSysExData("IsSuffixEnable", m_bIsSuffixEnabled.ToString().ToLower());
            base.CreateSysExData("PrefixDesc", "");
            base.CreateSysExData("SuffixCommonDesc", "");
            if (!string.IsNullOrEmpty(m_sEntityIDs))
                ResetPrefixAndSuffixList(iUserLangCode, iBaseLangCode);
            //Ankit End
            //rma 11121 starts
            //for carrier claims, auto claim support from funds screen is disabled with 11121, auto checks can be opened from financial reserve listing screen only. so claiamnt should be read only in case of auto checks
            if (bCarrierClaims)
            {
                base.AddReadOnlyNode("clm_lastname");                
            }
            //rma 11121
		}

        private int iGetEntityWithTaxId()  //MITS 32715 mcapps2 Start
        {
            int iReturn = 0;
            string sSQL = string.Empty;

            string sThisTaxId = base.GetSysExDataNodeText("Lookuptaxid");
            sThisTaxId = sThisTaxId.Replace("-", "");

            sSQL = "SELECT ENTITY_ID FROM ENTITY WHERE TAX_ID IN ('" + sThisTaxId + "','" + Int32.Parse(sThisTaxId).ToString("00-0000000") + "','" + Int32.Parse(sThisTaxId).ToString("000-00-0000") + "' )";

            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
            {

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        iReturn = Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_ID"), base.ClientId);
                        break;
                    }
                    objReader.Close();
                }
            }

            return iReturn;
        }
        private string sGetEntityWithTaxId()
        {
            int iReturn = 0;
            string sSQL = string.Empty;
            string sReturnEIDs = string.Empty;

            string sThisTaxId = base.GetSysExDataNodeText("Lookuptaxid");
            sThisTaxId = sThisTaxId.Replace("-", "");

            sSQL = "SELECT ENTITY_ID FROM ENTITY WHERE TAX_ID IN ('" + sThisTaxId + "','" + Int32.Parse(sThisTaxId).ToString("00-0000000") + "','" + Int32.Parse(sThisTaxId).ToString("000-00-0000") + "' )";

            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
            {

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        iReturn = Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_ID"), base.ClientId);
                        if (sReturnEIDs == "")
                        {
                            sReturnEIDs = iReturn.ToString();
                        }
                        else
                        {
                            sReturnEIDs += ", " + iReturn.ToString();
                        }
                    }
                    objReader.Close();
                }
            }

            return sReturnEIDs;
        }  //MITS 32715 mcapps2 End

        /// <summary>
        /// Sets XML Variables.
        /// </summary>
        private void setXMLVariables()
        {
            if (SysEx.SelectSingleNode("/SysExData/Skipreservetypes") == null)
            {
                base.ResetSysExData("Skipreservetypes", "");
            }

            if (SysEx.SelectSingleNode("/SysExData/InsuffReserve") == null)
            {
                base.ResetSysExData("InsuffReserve", "");
            }

            if (SysEx.SelectSingleNode("/SysExData/IsDuplicatePayment") == null)
            {
                base.ResetSysExData("IsDuplicatePayment", "");
            }

            if (SysEx.SelectSingleNode("/SysExData/UnitIDIsUnitRowId") == null)
            {
                base.ResetSysExData("UnitIDIsUnitRowId", "");
            }

            if (SysEx.SelectSingleNode("/SysExData/InsuffReserveNonNeg") == null)
            {
                base.ResetSysExData("InsuffReserveNonNeg", "");
            }

            if (SysEx.SelectSingleNode("/SysExData/CompRate") == null)
            {
                base.ResetSysExData("CompRate", "");
            }

            if (SysEx.SelectSingleNode("/SysExData/Reason") == null)
            {
                base.ResetSysExData("Reason", "");
            }
        }

        //Ankit Start : Financial Enhancements - Void Code Reason/ Memo Phrase change
        private void ResetPrefixAndSuffixList(int UserLangCode, int BaseLangCode)
        {
            StringBuilder sbSQL = null;
            string strPrefix = string.Empty;
            string strSuffixCommon = string.Empty;
            bool isBaseLangCode = false;
            try
            {
                if (int.Equals(UserLangCode, BaseLangCode) || int.Equals(UserLangCode, 0))
                    isBaseLangCode = true;

                sbSQL = new StringBuilder();

                sbSQL.Append(" SELECT E.ENTITY_ID, CTP.CODE_DESC PREFIX, CTS.CODE_DESC SUFFIX_COMMON, ");
                if (isBaseLangCode)
                    sbSQL.Append(" '' USER_PREFIX, '' USER_SUFFIX_COMMON ");
                else
                    sbSQL.Append(" UCTP.CODE_DESC USER_PREFIX, UCTS.CODE_DESC USER_SUFFIX_COMMON ");
                sbSQL.Append(" FROM ENTITY E ");
                sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT CTP ON CTP.CODE_ID = E.PREFIX AND CTP.LANGUAGE_CODE = " + BaseLangCode + " ");
                sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT CTS ON CTS.CODE_ID = E.SUFFIX_COMMON AND CTS.LANGUAGE_CODE = " + BaseLangCode + " ");
                if (!isBaseLangCode)
                {
                    sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT UCTP ON UCTP.CODE_ID = E.PREFIX AND UCTP.LANGUAGE_CODE = " + UserLangCode + " ");
                    sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT UCTS ON UCTS.CODE_ID = E.SUFFIX_COMMON AND UCTS.LANGUAGE_CODE = " + UserLangCode + " ");
                }
                sbSQL.Append(" WHERE E.ENTITY_ID In ( " + m_sEntityIDs + " ) ");
                sbSQL.Append(" And E.DELETED_FLAG <> -1 ");


                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    int iEntityID = 0;
                    string sPrefix = string.Empty;
                    string sSuffixCommon = string.Empty;

                    while (objReader.Read())
                    {
                        iEntityID = Convert.ToInt32(objReader.GetValue("ENTITY_ID"));
                        sPrefix = Convert.ToString(objReader.GetValue("USER_PREFIX"));
                        if (string.IsNullOrEmpty(sPrefix.Trim()))
                            sPrefix = Convert.ToString(objReader.GetValue("PREFIX"));

                        sSuffixCommon = Convert.ToString(objReader.GetValue("USER_SUFFIX_COMMON"));
                        if (string.IsNullOrEmpty(sSuffixCommon.Trim()))
                            sSuffixCommon = Convert.ToString(objReader.GetValue("SUFFIX_COMMON"));

                        if (string.IsNullOrEmpty(strPrefix))
                            strPrefix = iEntityID.ToString() + "#$" + (string.IsNullOrEmpty(sPrefix) ? string.Empty : sPrefix);
                        else
                            strPrefix = strPrefix + "@^" + iEntityID.ToString() + "#$" + (string.IsNullOrEmpty(sPrefix) ? string.Empty : sPrefix);

                        if (string.IsNullOrEmpty(strSuffixCommon))
                            strSuffixCommon = iEntityID.ToString() + "#$" + (string.IsNullOrEmpty(sSuffixCommon) ? string.Empty : sSuffixCommon);
                        else
                            strSuffixCommon = strSuffixCommon + "@^" + iEntityID.ToString() + "#$" + (string.IsNullOrEmpty(sSuffixCommon) ? string.Empty : sSuffixCommon);
                    }
                }
                ResetSysExData("Prefix", strPrefix);
                ResetSysExData("SuffixCommon", strSuffixCommon);
            }
            finally
            {
                sbSQL = null;
            }
        }

        //Ankit Start : Financial Enhancements - Memo Phrase change
        private void AppendMemoPhraseList(int UserLangCode, int BaseLangCode)
        {
            StringBuilder sbSQL = null;
            XmlDocument objSysExDataXmlDoc = base.SysEx;
            XmlNode objOldMemoPhraseListNode = null;
            XmlElement objNewMemoPhraseListNode = null;

            XmlElement objOptionXmlElement = null;
            XmlAttribute objXmlAttribute = null;
            XmlCDataSection objCData = null;
            bool isBaseLangCode = false;

            try
            {
                if (int.Equals(UserLangCode, BaseLangCode) || int.Equals(UserLangCode, 0))
                    isBaseLangCode = true;

                sbSQL = new StringBuilder();
                objOldMemoPhraseListNode = objSysExDataXmlDoc.SelectSingleNode("//MemoPhraseList");
                objNewMemoPhraseListNode = objSysExDataXmlDoc.CreateElement("MemoPhraseList");


                sbSQL.Append(" SELECT -1 CODE_ID, '' SHORT_CODE, '' CODE_DESC, '' USER_CODE_DESC ");
                if (DbFactory.IsOracleDatabase(m_sConnectionString))
                    sbSQL.Append(" FROM DUAL ");
                sbSQL.Append(" UNION ");
                sbSQL.Append(" SELECT CT.CODE_ID, C.SHORT_CODE, CT.CODE_DESC, ");
                if (isBaseLangCode)
                    sbSQL.Append(" '' USER_CODE_DESC ");
                else
                    sbSQL.Append(" UCT.CODE_DESC USER_CODE_DESC ");
                sbSQL.Append(" FROM CODES C ");
                sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT CT On C.CODE_ID = CT.CODE_ID AND CT.LANGUAGE_CODE = " + BaseLangCode + " ");
                if (!isBaseLangCode)
                    sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT UCT On C.CODE_ID = UCT.CODE_ID AND UCT.LANGUAGE_CODE = " + UserLangCode + " ");
                sbSQL.Append(" INNER JOIN GLOSSARY G ON G.TABLE_ID = C.TABLE_ID ");
                sbSQL.Append(" WHERE UPPER(G.SYSTEM_TABLE_NAME) = 'MEMO_PHRASE' ");
                sbSQL.Append(" And C.DELETED_FLAG <> -1 ");
                sbSQL.Append(" ORDER BY CODE_ID, CODE_DESC ");

                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    while (objReader.Read())
                    {
                        objOptionXmlElement = objSysExDataXmlDoc.CreateElement("option");
                        if (string.IsNullOrEmpty(Convert.ToString(objReader.GetValue("USER_CODE_DESC")).Trim()))
                            objCData = objSysExDataXmlDoc.CreateCDataSection(Convert.ToString(objReader.GetValue("CODE_DESC")));
                        else
                            objCData = objSysExDataXmlDoc.CreateCDataSection(Convert.ToString(objReader.GetValue("USER_CODE_DESC")));
                        objOptionXmlElement.AppendChild(objCData);
                        objXmlAttribute = objSysExDataXmlDoc.CreateAttribute("value");
                        objXmlAttribute.Value = Convert.ToString(objReader.GetValue("SHORT_CODE"));
                        objOptionXmlElement.Attributes.Append(objXmlAttribute);
                        objNewMemoPhraseListNode.AppendChild(objOptionXmlElement);
                        objCData = null;
                        objXmlAttribute = null;
                        objOptionXmlElement = null;
                    }
                }

                if (objOldMemoPhraseListNode != null)
                    objSysExDataXmlDoc.DocumentElement.ReplaceChild(objNewMemoPhraseListNode, objOldMemoPhraseListNode);
                else
                    objSysExDataXmlDoc.DocumentElement.AppendChild(objNewMemoPhraseListNode);

            }
            finally
            {
                sbSQL = null;
                objOldMemoPhraseListNode = null;
                objNewMemoPhraseListNode = null;
                objCData = null;
                objSysExDataXmlDoc = null;

            }
        }
        //Ankit End

		/// <summary>
        /// IsClaimClosed
        /// </summary>
        /// <param name="bIsClaimId">Claim Id</param>
        /// <returns>Claim Closed Flag</returns>
        private bool bIsClaimClosed(int iClaimId)
        {
            Claim objClaim;
            LocalCache objLocalCache = null;

            bool bIsClaimClosed = false;
            int iStatusCode = 0;

            try
            {
                if (iClaimId != 0)
                {
                    objClaim = (Claim)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Claim", false);
                    if (iClaimId > 0)
                    {
                        objClaim.MoveTo(iClaimId);
                    }
                    else
                    {
                        throw new RMAppException(Globalization.GetString("AutoClaimChecksForm.IsClaimOpen.InvalidClaim", base.ClientId));
                    }
                    iStatusCode = objClaim.ClaimStatusCode;
                    objLocalCache = new LocalCache(m_sConnectionString,base.ClientId);

                    if (objLocalCache.GetShortCode(objLocalCache.GetRelatedCodeId(iStatusCode)).ToUpper() == "C")
                    {
                        bIsClaimClosed = true;
                    }
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AutoClaimChecksbIsClaimClosed.Error", base.ClientId), p_objEx);
            }
            finally
            {
                objClaim = null;
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                }
            }
            return (bIsClaimClosed);
        }


        private void SetDoNotSaveDups()
        {
            DbReader objReader = null;
            string sSQL = string.Empty;
            m_DoNotSaveDups = false;

            try
            {
                sSQL = "SELECT DO_NOT_SAVE_DUPS FROM CHECK_OPTIONS";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        m_DoNotSaveDups = objReader.GetBoolean("DO_NOT_SAVE_DUPS");
                    }
                }
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
        }

        //Ankit Start : Financial Enhancement - Prefix and Suffix Changes
        private void SetCheckOptions()
        {
            DbReader objReader = null;
            string sSQL = string.Empty;
            m_DoNotSaveDups = false;

            try
            {
                sSQL = "SELECT INCLUDE_PREFIX, INCLUDE_SUFFIX, DEF_DSTRBN_TYPE_CODE FROM CHECK_OPTIONS";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        m_bIsPrefixEnabled = objReader.GetBoolean("INCLUDE_PREFIX");
                        m_bIsSuffixEnabled = objReader.GetBoolean("INCLUDE_SUFFIX");
                        m_iDefaultDistributionType = objReader.GetInt32("DEF_DSTRBN_TYPE_CODE");
                    }
                }
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
        }
        //Ankit End

        private void setReserveID(SortedList m_objNonThiredPartyPaymentList)
        {
            if (m_objNonThiredPartyPaymentList.Count > 0)
            {
                foreach (FundsAutoSplit objFundsAutoSplit in FirstPayment(m_objNonThiredPartyPaymentList).AutoSplitList)
                {
                    if ((SysEx.SelectSingleNode("/SysExData/ReserveID") == null) || (SysEx.SelectSingleNode("/SysExData/ReserveID").Value == ""))
                    {
                        base.ResetSysExData("ReserveID", objFundsAutoSplit.ReserveTypeCode.ToString());
                        int iReserveID = int.Parse(SysEx.SelectSingleNode("/SysExData/ReserveID").Value);
                        base.ResetSysExData("ReserveIDDesc", GetReserveIDDesc(iReserveID));                       
         
                    }

                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iReserveID"></param>
        /// <returns></returns>
        private string GetReserveIDDesc(int iReserveID)
        {
            LocalCache objLocalCache = null;
            string sReserveIDDesc = "";
            try
            {
                objLocalCache = new LocalCache(m_sConnectionString,base.ClientId);
                sReserveIDDesc = objLocalCache.GetCodeDesc(iReserveID);

            }
            finally
            {
               if(objLocalCache != null)
               {
                   objLocalCache.Dispose();
               }
            }

            return sReserveIDDesc;
        }


		private void GetMaxNumberOfAutoChecks()
		{
			DbReader objReader = null ;
			string sSQL = "" ;
			int iMaxNumberOfAutoChecks = 0 ;
             //skhare7 : combined Payment R8
            int iMaxPayees = 0;
			
			try
			{//skhare7 r8 combined Payment
                sSQL = "SELECT MAX_AUTO_CHECKS,MAX_PAYEES FROM CHECK_OPTIONS";
				objReader = DbFactory.GetDbReader( FundsAutoBatch.Context.RMDatabase.ConnectionString , sSQL );	
		
				if( objReader != null )
				{
					if( objReader.Read() )
					{
						iMaxNumberOfAutoChecks = Conversion.ConvertObjToInt( objReader.GetValue( "MAX_AUTO_CHECKS" ), base.ClientId );
                        //skhare7 R8 combined Payment
                        iMaxPayees = Conversion.ConvertObjToInt(objReader.GetValue("MAX_PAYEES"), base.ClientId);
					}
				}
				objReader.Close();
				base.ResetSysExData( "MaxNumberOfAutoChecks" , iMaxNumberOfAutoChecks.ToString() );
                base.ResetSysExData("MaxPayees",iMaxPayees.ToString());
			}
			finally
			{				
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
		}


		private void AppenedSelectedPayeeTypeCode( int p_iPayeeTypeCode , int p_iPayeeEid )
		{
			string sSQL = string.Empty ;
			string sPayeeTypeCodeDesc = string.Empty ;
			string sSelectedPayeeTypeCode = string.Empty ;
			int iTableType = 0 ;
			int iOtherPayeeTypeCodeId = 0 ;

            if (p_iPayeeTypeCode != -1)
            {
                sPayeeTypeCodeDesc = this.objCache.GetShortCode(p_iPayeeTypeCode);
                //avipinsrivas Start : Worked for Issue 4634 - Epic 340
                iOtherPayeeTypeCodeId = this.objCache.GetCodeId(sPayeeTypeCodeDesc, "PAYEE_TYPE");
                if (sPayeeTypeCodeDesc == "O")      // People / Entity
                {
                    //sSQL = "SELECT GLOSSARY.GLOSSARY_TYPE_CODE FROM ENTITY, GLOSSARY WHERE ENTITY.ENTITY_ID=" 
                    //    + p_iPayeeEid + " AND ENTITY.ENTITY_TABLE_ID=GLOSSARY.TABLE_ID";

                    //iTableType = this.FundsAutoBatch.Context.DbConnLookup.ExecuteInt(sSQL);
                    //iOtherPayeeTypeCodeId = this.objCache.GetCodeId("O", "PAYEE_TYPE"); 

                    //// TODO - Confirm whether these numeric values are constant across all client databases
                    //if(iTableType==7 || iTableType==4) // People
                    //{
                    //    sSelectedPayeeTypeCode = "e" + iOtherPayeeTypeCodeId;
                    //}
                    //else if(iTableType==5) // Org Hierarchy
                    //{
                    //    sSelectedPayeeTypeCode = "o" + iOtherPayeeTypeCodeId;	
                    //}
                    //else // If PAYEE_EID is 0 (often case in conversions)
                    //{
                    //    sSelectedPayeeTypeCode = "o" + iOtherPayeeTypeCodeId;
                    //}
                    if (int.Equals(p_iPayeeEid, 0))         //(often case during conversions)
                        sSelectedPayeeTypeCode = "o" + iOtherPayeeTypeCodeId;
                    else
                        sSelectedPayeeTypeCode = "e" + iOtherPayeeTypeCodeId;
                }
                else if (sPayeeTypeCodeDesc == "H")     // Org Hierarchy
                    sSelectedPayeeTypeCode = "o" + iOtherPayeeTypeCodeId;
                //avipinsrivas End
                else if (sPayeeTypeCodeDesc == "C")
                {
                    sSelectedPayeeTypeCode = this.objCache.GetCodeId("C", "PAYEE_TYPE").ToString();
                }
                //start:rupal, r8 enh to inculde person involved as payee type
                else if (sPayeeTypeCodeDesc == "P")
                {
                    sSelectedPayeeTypeCode = this.objCache.GetCodeId("P", "PAYEE_TYPE").ToString();
                }//end:rupal
				else
				{
					sSelectedPayeeTypeCode = "" ;
				}
			}

			if( p_iPayeeEid != 0 )			
				base.ResetSysExData("PayeeTypeCode",sSelectedPayeeTypeCode); 
			else
				base.ResetSysExData("PayeeTypeCode", m_sOldSelectedPayeeTypeCode ); 				
		}

        private void SetClaimNumberReadOnly(FundsAuto p_objFundsAuto)
        {
            //Ashish Ahuja - Mits 32843 
            //if ((p_objFundsAuto.AutoTransId == 0) && (p_objFundsAuto.ClaimId == 0))
            if ((p_objFundsAuto.AutoTransId == 0 || p_objFundsAuto.AutoTransId == -1) && (p_objFundsAuto.ClaimId == 0))
            {
                base.AddReadWriteNode("claimnumber");
                base.ResetSysExData("IsClaimNumberReadOnly", "");
            }
            else
            {
                base.AddReadOnlyNode("claimnumber");
                base.ResetSysExData("IsClaimNumberReadOnly", "True");
            }
        }

        private void SetClaimInfo(FundsAuto p_objFundsAuto)
		{
			Claim objClaim = null ;

            Entity objEntity = null;
            LocalCache objLocalCache = null;

			string sClaimantLastNameFirstName = string.Empty ;
			string sUnitName = string.Empty ;
			int iLob = 0 ;
			int iReserveTracking=0;
            int iStatusCode = 0;
            //skhare7 
            string sClaimant = string.Empty;
			try
			{
				if(p_objFundsAuto.ClaimId > 0)
                {
                    bCarrierClaims = Conversion.ConvertStrToBool(p_objFundsAuto.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString());
					objClaim = (Claim) this.FundsAutoBatch.Context.Factory.GetDataModelObject("Claim",false) ;
					objClaim.MoveTo( p_objFundsAuto.ClaimId );
                    iStatusCode = objClaim.ClaimStatusCode;
                    objLocalCache = new LocalCache(m_sConnectionString,base.ClientId);

                    if (objLocalCache.GetShortCode(objLocalCache.GetRelatedCodeId(iStatusCode)).ToUpper() == "C")
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId), "The Claim Is Closed.  Scheduled Checks Can Not Be Created On Closed Claims.", BusinessAdaptorErrorType.Error);
                    }

                    base.ResetSysExData("ThisClaimNumber", p_objFundsAuto.ClaimNumber);

					iLob = objClaim.LineOfBusCode;
					iReserveTracking = this.FundsAutoBatch.Context.InternalSettings.ColLobSettings[iLob].ReserveTracking;

                    base.ResetSysExData("Reservetracking", iReserveTracking.ToString());
                    if (p_objFundsAuto.AutoBatchId == 0)
                    {
                        //start:rupal, bob change
                        if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm != 0)
                        {
                            //if carrier claim is on, we need claimant in all kind of LOB
                            if (base.GetSysExDataNodeText("ClaimantEIdPassedIn") != null) 
                            {
                                if ((base.GetSysExDataNodeText("ClaimantEIdPassedIn").ToString() != "") && (base.GetSysExDataNodeText("ClaimantEIdPassedIn").ToString() != "0"))
                                {
                                    p_objFundsAuto.ClaimantEid = Conversion.ConvertStrToInteger(base.GetSysExDataNodeText("ClaimantEIdPassedIn"));
                                }
                                else
                                {
                                    if ((base.GetSysExDataNodeText("FundsAuto/ClaimantEid").ToString() != "") && (base.GetSysExDataNodeText("FundsAuto/ClaimantEid").ToString() != "0"))
                                    {
                                        p_objFundsAuto.ClaimantEid = Conversion.ConvertStrToInteger(base.GetSysExDataNodeText("FundsAuto/ClaimantEid")); ;
                                    }
                                }
                            }
                        }
                        //start:end

                        if (iLob == 243 || iLob == 844)
                        {
                            p_objFundsAuto.ClaimantEid = objClaim.PrimaryClaimant.ClaimantEid;
                            base.AddReadOnlyNode("clm_lastname");
                            base.AddReadOnlyNode("unit");
                        }
                        else if (iReserveTracking != 0)     // GC/VA - detail level tracking
                        {
                            if (iLob == 241)
                            {  // GC detail level
                                if (base.GetSysExDataNodeText("ClaimantEIdPassedIn") != null) //MITS 16726
                                {
                                    if ((base.GetSysExDataNodeText("ClaimantEIdPassedIn").ToString() != "") && (base.GetSysExDataNodeText("ClaimantEIdPassedIn").ToString() != "0"))
                                    {
                                        p_objFundsAuto.ClaimantEid = Conversion.ConvertStrToInteger(base.GetSysExDataNodeText("ClaimantEIdPassedIn"));
                                    }
                                    else
                                    {
                                        if ((base.GetSysExDataNodeText("FundsAuto/ClaimantEid").ToString() != "") && (base.GetSysExDataNodeText("FundsAuto/ClaimantEid").ToString() != "0"))
                                        {
                                            p_objFundsAuto.ClaimantEid = Conversion.ConvertStrToInteger(base.GetSysExDataNodeText("FundsAuto/ClaimantEid")); ;
                                        }
                                    }
                                }
                                if ((p_objFundsAuto.ClaimantEid == 0) && (sFromRsvListingPage == "0"))
                                {
                                    if (objClaim.ClaimantList.Count == 1)
                                   {
                                        foreach (Claimant objClaimanTemp in objClaim.ClaimantList)
                                        {
                                            p_objFundsAuto.ClaimantEid = objClaimanTemp.ClaimantEid;
                                            break;
                                        }
                                    }
                                }
                                base.AddReadOnlyNode("unit");
                                if (sFromRsvListingPage != "0")
                                {
                                    base.AddReadOnlyNode("clm_lastname");
                                }
                            }
                            else if (iLob == 242)
                            {
                                if (base.GetSysExDataNodeText("UnitIdPassedIn") != null)
                                {
                                    if ((base.GetSysExDataNodeText("UnitIdPassedIn") != "") && (base.GetSysExDataNodeText("UnitIdPassedIn") != "0"))
                                    {
                                        if (base.GetSysExDataNodeText("UnitIDIsUnitRowId") == "True")
                                        {
                                            int iThisUnitID = 0;
                                            iThisUnitID = iGetUnitIdFromRowId(Conversion.ConvertStrToInteger(base.GetSysExDataNodeText("UnitIdPassedIn")), p_objFundsAuto.ClaimId);
                                            base.ResetSysExData("UnitIdPassedIn", iThisUnitID.ToString()); 
                                        }
                                    }
                                }
                                //  VA detail level
                                if (base.GetSysExDataNodeText("ClaimantEIdPassedIn") != null) //MITS 16726
                                {
                                    if ((base.GetSysExDataNodeText("ClaimantEIdPassedIn").ToString() != "") && (base.GetSysExDataNodeText("ClaimantEIdPassedIn").ToString() != "0"))
                                    {
                                        p_objFundsAuto.ClaimantEid = Conversion.ConvertStrToInteger(base.GetSysExDataNodeText("ClaimantEIdPassedIn"));
                                    }
                                    else
                                    {
                                        if ((base.GetSysExDataNodeText("FundsAuto/ClaimantEid").ToString() != "") && (base.GetSysExDataNodeText("FundsAuto/ClaimantEid").ToString() != "0"))
                                        {
                                            p_objFundsAuto.ClaimantEid = Conversion.ConvertStrToInteger(base.GetSysExDataNodeText("FundsAuto/ClaimantEid")); ;
                                        }
                                    }
                                }
                                if ((p_objFundsAuto.ClaimantEid == 0) && (sFromRsvListingPage == "0"))
                                {
                                    if ((objClaim.ClaimantList.Count == 1) && (objClaim.UnitList.Count == 0))
                                    {
                                        foreach (Claimant objClaimanTemp in objClaim.ClaimantList)
                                        {
                                            p_objFundsAuto.ClaimantEid = objClaimanTemp.ClaimantEid;
                                            break;
                                        }
                                    }
                                }
                                if (base.GetSysExDataNodeText("UnitIdPassedIn") != null)
                                {
                                    if ((base.GetSysExDataNodeText("UnitIdPassedIn").ToString() != "") && (base.GetSysExDataNodeText("UnitIdPassedIn").ToString() != "0"))
                                    {
                                        p_objFundsAuto.UnitId = Conversion.ConvertStrToInteger(base.GetSysExDataNodeText("UnitIdPassedIn"));
                                    }
                                    else
                                    {
                                        if ((base.GetSysExDataNodeText("FundsAuto/UnitId").ToString() != "") && (base.GetSysExDataNodeText("FundsAuto/UnitId").ToString() != "0"))
                                        {
                                            p_objFundsAuto.UnitId = Conversion.ConvertStrToInteger(base.GetSysExDataNodeText("FundsAuto/UnitId")); ;
                                        }
                                    }
                                }
                                if ((p_objFundsAuto.UnitId == 0) && (sFromRsvListingPage == "0"))
                                {
                                    if ((objClaim.UnitList.Count == 1) && (objClaim.ClaimantList.Count == 0))
                                    {
                                        foreach (UnitXClaim objUnitTemp in objClaim.UnitList)
                                        {
                                            p_objFundsAuto.UnitId = objUnitTemp.UnitId;
                                            break;
                                        }
                                    }
                                }
                                if ((p_objFundsAuto.ClaimantEid == 0) && (p_objFundsAuto.UnitId == 0))
                                {
                                    if ((objClaim.ClaimantList.Count == 0) && (objClaim.UnitList.Count == 0))
                                    {
                                        base.AddReadOnlyNode("unit");
                                        base.AddReadOnlyNode("clm_lastname");
                                    }
                                    else if ((objClaim.ClaimantList.Count != 0) && (objClaim.UnitList.Count == 0))
                                    {
                                        base.AddReadOnlyNode("unit");
                                        if (sFromRsvListingPage != "0")
                                        {
                                            base.AddReadOnlyNode("clm_lastname");
                                        }
                                    }
                                    else if ((objClaim.ClaimantList.Count == 0) && (objClaim.UnitList.Count != 0))
                                    {
                                        base.AddReadOnlyNode("clm_lastname");
                                        if (sFromRsvListingPage != "0")
                                        {
                                            base.AddReadOnlyNode("unit");
                                        }
                                    }
                                    else
                                    {
                                        p_objFundsAuto.ClaimantEid = 0;
                                        p_objFundsAuto.UnitId = 0;
                                    }
                                }
                                else if ((p_objFundsAuto.ClaimantEid != 0) && (p_objFundsAuto.UnitId == 0))
                                {
                                    base.AddReadOnlyNode("unit");
                                    if (sFromRsvListingPage != "0")
                                    {
                                        base.AddReadOnlyNode("clm_lastname");
                                    }
                                }
                                else if ((p_objFundsAuto.ClaimantEid == 0) && (p_objFundsAuto.UnitId != 0))
                                {
                                    base.AddReadOnlyNode("clm_lastname");
                                    if (sFromRsvListingPage != "0")
                                    {
                                        base.AddReadOnlyNode("unit");
                                    } 
                                }
                                else
                                {
                                    p_objFundsAuto.ClaimantEid = 0;
                                    p_objFundsAuto.UnitId = 0;
                                }
                            }

                        }
                        else  // GC/VA - claim level tracking
                        {
                            if (!bCarrierClaims)
                            {
                                base.AddReadOnlyNode("clm_lastname");
                                base.AddReadOnlyNode("unit");
                            }
                        }
                    }
                    else
                    {
                        base.ResetSysExData("ClaimantEIdPassedIn", p_objFundsAuto.ClaimantEid.ToString());
                        base.ResetSysExData("UnitIdPassedIn", p_objFundsAuto.UnitId.ToString()); 
                    }
                    if (base.SysEx.SelectSingleNode("//FundsAuto/UnitId") != null)
                    {
                        if ((p_objFundsAuto.UnitId == 0) && ((base.SysEx.SelectSingleNode("//FundsAuto/UnitId").InnerXml != "") || (base.SysEx.SelectSingleNode("//FundsAuto/UnitId").InnerXml != "0")))
                        {
                            p_objFundsAuto.UnitId = Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("//FundsAuto/UnitId").InnerXml);
                        }
                    }
                    base.AddDisplayNode("backtofinancials");
                    //skhare7 R8 combined Payment
               //     sClaimantLastNameFirstName = objClaim.Context.LocalCache.GetEntityLastFirstName(objClaim.PrimaryClaimant.ClaimantEid);
              
                    
                    if (p_objFundsAuto.UnitId != 0)
                    {
                        Vehicle objVehicle = null;
                        objVehicle = (Vehicle)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Vehicle", false);
                        objVehicle.MoveTo(p_objFundsAuto.UnitId);
                        sUnitName = objVehicle.VehicleMake;
                    }
                    else
                    {
                        sUnitName = "";
                    }
                    base.ResetSysExData("unitid", p_objFundsAuto.UnitId.ToString());
                    base.ResetSysExData("unit", sUnitName);
                    base.ResetSysExData("clm_entityid", p_objFundsAuto.ClaimantEid.ToString());
                 
                    //base.AddReadOnlyNode("clm_lastname");
                    //base.AddReadOnlyNode("unit");

                    // abhateja 02.10.2009, Get the claimant list and pass on the claimant information
                    // to SysEx if there is only one claimant.
                    base.ResetSysExData("ClaimantCount", objClaim.ClaimantList.Count.ToString());
                
                    if (bCarrierClaims)
                    {
                        StringBuilder objClaimantInformation = new StringBuilder();
                        objLocalCache = new LocalCache(m_sConnectionString,base.ClientId);
                        string sStateAbbr = string.Empty;
                        string sStateDesc = string.Empty;

                        foreach (Claimant objClaimant in objClaim.ClaimantList)
                        {
                            if (string.Compare((base.SysEx.SelectSingleNode("//ClaimantEIdPassedIn")).InnerText.ToString(), objClaimant.ClaimantEid.ToString(), true) == 0)
                            {
                                objEntity = (Entity)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Entity", false);
                                //rma 16514 starts
                                objEntity.MoveTo(objClaimant.ClaimantEid);
                                sClaimantLastNameFirstName = objEntity.LastName + "|" + objEntity.FirstName + "|" + objEntity.TaxId + "_" + objClaim.PrimaryClaimant.ClaimantEid;
                                sClaimant = objEntity.LastName + "," + objEntity.FirstName;
                                //rma 16514 ends
                                //skhare7 R8 combined Payment
                                if (sClaimantLastNameFirstName == string.Empty)
                                {
                                    objEntity.MoveTo(objClaim.PrimaryClaimant.ClaimantEid);
                                    sClaimantLastNameFirstName = objEntity.LastName + "|" + objEntity.FirstName + "|" + objEntity.TaxId + "_" + objClaim.PrimaryClaimant.ClaimantEid;
                                    sClaimant = objEntity.LastName + "," + objEntity.FirstName;
                                }
                                CreateClaimantInfo(objEntity, objLocalCache, objClaimantInformation, ref sStateAbbr, ref sStateDesc, objClaimant);
                            }
                            else
                                continue;
                        }
                    }
                    else if (objClaim.ClaimantList.Count == 1)
                    {
                        StringBuilder objClaimantInformation = new StringBuilder();
                        objLocalCache = new LocalCache(m_sConnectionString,base.ClientId);
                        string sStateAbbr = string.Empty;
                        string sStateDesc = string.Empty;
                        foreach (Claimant objClaimant in objClaim.ClaimantList)
                        {
                            objEntity = (Entity)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Entity", false);
                            if (sClaimantLastNameFirstName == string.Empty)
                            {
                                objEntity.MoveTo(objClaim.PrimaryClaimant.ClaimantEid);
                                sClaimantLastNameFirstName = objEntity.LastName + "|" + objEntity.FirstName + "|" + objEntity.TaxId + "_" + objClaim.PrimaryClaimant.ClaimantEid;
                                sClaimant = objEntity.LastName + "," + objEntity.FirstName;
                            }
                            CreateClaimantInfo(objEntity, objLocalCache, objClaimantInformation, ref sStateAbbr, ref sStateDesc, objClaimant);
                        }
                    }
                    //start:rupal, r8 enh to include person involved as Payee Type
                    if (objClaim.ClaimId > 0 && objClaim.Context.InternalSettings.SysSettings.AddPersonInvolvedAsPayeeType || (p_objFundsAuto.PayeeEid != 0 && p_objFundsAuto.PayeeTypeCode == objLocalCache.GetCodeId("P", "PAYEE_TYPE")))
                    {
                        CreatePersonInvolvedInfo(objLocalCache, objClaim.EventId);
                    }
                    //end:rupal
				}
				else
				{
					base.AddReadOnlyNode( "unit" );
					base.AddReadOnlyNode( "clm_lastname" );
                    base.AddKillNode("backtofinancials");						
				}
				
				// On existing batch, don't allow change of claim, claimant or unit
                if (this.FundsAutoBatch.AutoBatchId > 0)
                {
                    base.AddReadOnlyNode("claimnumber");
                    base.AddReadOnlyNode("clm_lastname");
                    base.AddReadOnlyNode("unit");
                }
                //else
                //{
                //    base.AddReadWriteNode("claimnumber");
                //}
                base.ResetSysExData("clm_lastname", sClaimant);
                base.ResetSysExData("ClaimantName", sClaimantLastNameFirstName);
                base.ResetSysExData("ClaimantLastNameFirstName", sClaimant);
                base.ResetSysExData("UnitName", sUnitName);
                base.ResetSysExData("Lob", iLob.ToString());


                if (p_objFundsAuto.DstrbnType == 0)
                {
                    if (m_iDefaultDistributionType != objCache.GetCodeId("MAL", "DISTRIBUTION_TYPE"))
                    {
                        p_objFundsAuto.DstrbnType = m_iDefaultDistributionType;
                    }
                    else if(m_iDefaultDistributionType != 0)
                    {
                        p_objFundsAuto.DstrbnType = objCache.GetCodeId("RML", "DISTRIBUTION_TYPE");
                    }
                }
            }
            finally
            {
                objClaim = null;
            }
        }

        private void CreateClaimantInfo(Entity objEntity, LocalCache objLocalCache, StringBuilder objClaimantInformation, ref string sStateAbbr, ref string sStateDesc, Claimant objClaimant)
        {
            //no country fields in case of autochecks so commented these line. 
            // akaushik5 Added for MITS 38152 Starts        
            //string sCountryAbbr = string.Empty;
            //string sCountryDesc = string.Empty;
            // akaushik5 Added for MITS 38152 End
            objEntity.MoveTo(objClaimant.ClaimantEid);

                //objClaimantInformation.Append(objClaimant.ClaimantEid);
                objClaimantInformation.Append("EID:\"" + objClaimant.ClaimantEid + "\",");
                //objClaimantInformation.Append("^*^*^");
                //objClaimantInformation.Append(objEntity.LastName);
                objClaimantInformation.Append("LAST_NAME:\"" + objEntity.LastName + "\",");
                //objClaimantInformation.Append("^*^*^");
                //objClaimantInformation.Append(objEntity.FirstName);
                objClaimantInformation.Append("FIRST_NAME:\"" + objEntity.FirstName + "\",");
                //objClaimantInformation.Append("^*^*^");
                //objClaimantInformation.Append(objEntity.TaxId);
                objClaimantInformation.Append("TAX_ID:\"" + objEntity.TaxId + "\",");
                //objClaimantInformation.Append("^*^*^");
                //objClaimantInformation.Append(objEntity.Addr1);
                objClaimantInformation.Append("ADDR1:\"" + objEntity.Addr1 + "\",");
                //objClaimantInformation.Append("^*^*^");
                //objClaimantInformation.Append(objEntity.Addr2);
                objClaimantInformation.Append("ADDR2:\"" + objEntity.Addr2 + "\",");
                //objClaimantInformation.Append("^*^*^");
                //objClaimantInformation.Append(objEntity.City);
                objClaimantInformation.Append("CITY:\"" + objEntity.City + "\",");
                //objClaimantInformation.Append("^*^*^");
                //objClaimantInformation.Append(objEntity.StateId);
                objClaimantInformation.Append("STATE_ID:\"" + objEntity.StateId + "\",");

                objLocalCache.GetStateInfo(objEntity.StateId, ref sStateAbbr, ref sStateDesc);

                //objClaimantInformation.Append("^*^*^");
                //objClaimantInformation.Append(sStateAbbr);
                objClaimantInformation.Append("STATE_ABBR:\"" + sStateAbbr + "\",");
                //objClaimantInformation.Append("^*^*^");
                //objClaimantInformation.Append(sStateDesc);
                objClaimantInformation.Append("STATE_DESC:\"" + sStateDesc + "\",");
                //objClaimantInformation.Append("^*^*^");
                //objClaimantInformation.Append(objEntity.ZipCode);
                objClaimantInformation.Append("ZIP_CODE:\"" + objEntity.ZipCode + "\",");
            // akaushik5 Added for MITS 38152 Starts    
            //objClaimantInformation.Append("^*^*^");
            //objClaimantInformation.Append(objEntity.CountryCode);
            //objClaimantInformation.Append("^*^*^");
            //objLocalCache.GetCodeInfo(objEntity.CountryCode , ref sCountryAbbr, ref sCountryDesc, base.Adaptor.userLogin.objUser.NlsCode); //Aman ML Change
            //objClaimantInformation.Append(sCountryAbbr);
            //objClaimantInformation.Append("^*^*^");
            //objClaimantInformation.Append(sCountryDesc);
            // akaushik5 Added for MITS 38152 Ends
            
                //Added by Amitosh For EFT Payment
                //objClaimantInformation.Append("^*^*^");
                if (CommonFunctions.hasEFTBankInfo(objClaimant.ClaimantEid, objLocalCache.GetCodeId("A", "EFT_BANKING_STATUS"), objLocalCache.DbConnectionString))
                {
                    //objClaimantInformation.Append("true");
                    objClaimantInformation.Append("EFT_BANKINFO:\"true\",");

                }
                else
                {
                    //objClaimantInformation.Append("false");
                    objClaimantInformation.Append("EFT_BANKINFO:\"false\",");

                }
                //objClaimantInformation.Append("^*^*^");
                //objClaimantInformation.Append(objEntity.Addr3);// JIRA 6420 pkandhari
                objClaimantInformation.Append("ADDR3:\"" + objEntity.Addr3 + "\",");
                //objClaimantInformation.Append("^*^*^");
                //objClaimantInformation.Append(objEntity.Addr4);// JIRA 6420 pkandhari
                objClaimantInformation.Append("ADDR4:\"" + objEntity.Addr4 + "\"");
                //base.ResetSysExData("ClaimantInformation", objClaimantInformation.ToString());
                base.ResetSysExData("ClaimantInformation", "{"+objClaimantInformation.ToString()+"}");

        }
		
		//start:rupal, r8 enh to add person involved as payee type
        private void CreatePersonInvolvedInfo(LocalCache objLocalCache,int iEventId)
        {
            string sSQLPI = "SELECT PERSON_INVOLVED.PI_EID,ENTITY.LAST_NAME,ENTITY.FIRST_NAME,ENTITY.TAX_ID,ENTITY.ADDR1,ENTITY.ADDR2,ENTITY.ADDR3,ENTITY.ADDR4,ENTITY.CITY,ENTITY.STATE_ID,ENTITY.ZIP_CODE,ENTITY.COUNTRY_CODE FROM PERSON_INVOLVED,ENTITY WHERE PERSON_INVOLVED.PI_EID=ENTITY.ENTITY_ID AND EVENT_ID= " + iEventId;

            using (DataSet objDS = DbFactory.GetDataSet(m_sConnectionString, sSQLPI, base.ClientId))//rkaur27
            {
                if (objDS != null && objDS.Tables[0] != null && objDS.Tables[0].Rows.Count == 1)
                {
                    base.ResetSysExData("PersonInvolvedCount", objDS.Tables[0].Rows.Count.ToString());

                    StringBuilder objPersonInvolvedInformation = new StringBuilder();
                    objLocalCache = new LocalCache(m_sConnectionString,base.ClientId);
                    string sStateAbbr = string.Empty;
                    string sStateDesc = string.Empty;
                    string sCountryAbbr = string.Empty;
                    string sCountryDesc = string.Empty;
                    bool bSuccess = false;

                    DataRow objDR = objDS.Tables[0].Rows[0];
                    int iPiEId = Conversion.CastToType<int>(objDR["PI_EID"].ToString(), out bSuccess);
                    int iStateId = Conversion.CastToType<int>(objDR["STATE_ID"].ToString(), out bSuccess);
                    int iCountryCode = Conversion.CastToType<int>(objDR["COUNTRY_CODE"].ToString(), out bSuccess);
                    
                    //Ankit Start : Worked on MITS - 31358
                    if (iPiEId != 0)
                    {
                        if (string.IsNullOrEmpty(m_sEntityIDs))
                            m_sEntityIDs = iPiEId.ToString();
                        else
                            m_sEntityIDs = m_sEntityIDs + ", " + iPiEId.ToString();
                    }
                    //Ankit End

                    //objPersonInvolvedInformation.Append(iPiEId.ToString());
                    objPersonInvolvedInformation.Append("EID:\"" + iPiEId.ToString() + "\",");
                    //objPersonInvolvedInformation.Append("^*^*^");
                    //objPersonInvolvedInformation.Append(objDR["LAST_NAME"].ToString());
                    objPersonInvolvedInformation.Append("LAST_NAME:\"" + objDR["LAST_NAME"].ToString() + "\",");
                    //objPersonInvolvedInformation.Append("^*^*^");
                    //objPersonInvolvedInformation.Append(objDR["FIRST_NAME"].ToString());
                    objPersonInvolvedInformation.Append("FIRST_NAME:\"" + objDR["FIRST_NAME"].ToString() + "\",");
                    //objPersonInvolvedInformation.Append("^*^*^");
                    //objPersonInvolvedInformation.Append(objDR["TAX_ID"].ToString());
                    objPersonInvolvedInformation.Append("TAX_ID:\"" + objDR["TAX_ID"].ToString() + "\",");
                    //objPersonInvolvedInformation.Append("^*^*^");
                    //objPersonInvolvedInformation.Append(objDR["ADDR1"].ToString());
                    objPersonInvolvedInformation.Append("ADDR1:\"" + objDR["ADDR1"].ToString() + "\",");
                    //objPersonInvolvedInformation.Append("^*^*^");
                    //objPersonInvolvedInformation.Append(objDR["ADDR2"].ToString());
                    objPersonInvolvedInformation.Append("ADDR2:\"" + objDR["ADDR2"].ToString() + "\",");
                    //objPersonInvolvedInformation.Append("^*^*^");
                    //objPersonInvolvedInformation.Append(objDR["CITY"].ToString());
                    objPersonInvolvedInformation.Append("CITY:\"" + objDR["CITY"].ToString() + "\",");
                    //objPersonInvolvedInformation.Append("^*^*^");
                    //objPersonInvolvedInformation.Append(iStateId.ToString());
                    objPersonInvolvedInformation.Append("STATEID:\"" + iStateId.ToString() + "\",");

                    objLocalCache.GetStateInfo(iStateId, ref sStateAbbr, ref sStateDesc);

                    //objPersonInvolvedInformation.Append("^*^*^");
                    //objPersonInvolvedInformation.Append(sStateAbbr);
                    objPersonInvolvedInformation.Append("STATE_ABBR:\"" + sStateAbbr + "\",");
                    //objPersonInvolvedInformation.Append("^*^*^");
                    //objPersonInvolvedInformation.Append(sStateDesc);
                    objPersonInvolvedInformation.Append("STATE_DESC:\"" + sStateDesc + "\",");
                    //objPersonInvolvedInformation.Append("^*^*^");
                    //objPersonInvolvedInformation.Append(objDR["ZIP_CODE"].ToString());
                    objPersonInvolvedInformation.Append("ZIP_CODE:\"" + objDR["ZIP_CODE"].ToString() + "\",");
                    //objPersonInvolvedInformation.Append("^*^*^");
                    //objPersonInvolvedInformation.Append(iCountryCode.ToString());
                    objPersonInvolvedInformation.Append("COUNTRY_CODE:\"" + iCountryCode.ToString() + "\",");

                    //Aman ML Change                   

                    objLocalCache.GetCodeInfo(iCountryCode, ref sCountryAbbr, ref sCountryDesc, base.Adaptor.userLogin.objUser.NlsCode);  //Aman ML Change
                    
                    //objPersonInvolvedInformation.Append("^*^*^");
                    //objPersonInvolvedInformation.Append(sCountryAbbr);
                    objPersonInvolvedInformation.Append("COUNTRY_ABBR:\"" + sCountryAbbr + "\",");
                    //objPersonInvolvedInformation.Append("^*^*^");
                    //objPersonInvolvedInformation.Append(sCountryDesc);
                    objPersonInvolvedInformation.Append("COUNTRY_DESC:\"" + sCountryDesc + "\",");
                    //Added by Amitosh For EFT Payment
                    // akaushik5 Added for MITS 38152 Starts      
                    //objPersonInvolvedInformation.Append("^*^*^");
                    //// akaushik5 Added for MITS 38152 Ends
                    //if (CommonFunctions.hasEFTBankInfo(iPiEId, objLocalCache.GetCodeId("A", "EFT_BANKING_STATUS"), objLocalCache.DbConnectionString))
                    //{
                    //    //objPersonInvolvedInformation.Append("true");
                    //    objPersonInvolvedInformation.Append("EFTBankInfo:true,");

                    //}
                    //else
                    //{
                    //    //objPersonInvolvedInformation.Append("false");
                    //    objPersonInvolvedInformation.Append("EFTBankInfo:false,");

                    //}
                    //Ashish Ahuja RMA-11612
                    if (CommonFunctions.hasEFTBankInfo(iPiEId, objLocalCache.GetCodeId("A", "EFT_BANKING_STATUS"), objLocalCache.DbConnectionString))
                    {
                        objPersonInvolvedInformation.Append("EFT_BANKINFO:\"true\",");
                    }
                    else
                    {
                        objPersonInvolvedInformation.Append("EFT_BANKINFO:\"false\",");
                    }
                    //objPersonInvolvedInformation.Append("^*^*^");
                    //objPersonInvolvedInformation.Append(objDR["ADDR3"].ToString());// JIRA 6420 pkandhari
                    objPersonInvolvedInformation.Append("ADDR3:\"" + objDR["ADDR3"].ToString() + "\",");
                    //objPersonInvolvedInformation.Append("^*^*^");
                    //objPersonInvolvedInformation.Append(objDR["ADDR4"].ToString());// JIRA 6420 pkandhari
                    objPersonInvolvedInformation.Append("ADDR4:\"" + objDR["ADDR4"].ToString() + "\"");
                    //base.ResetSysExData("PersonInvolvedInformation", objPersonInvolvedInformation.ToString());
                    base.ResetSysExData("PersonInvolvedInformation", "{"+objPersonInvolvedInformation.ToString()+"}");
                }
            }
        }
//end:rupal
        private string sGetUnitDesc(int iUnitID, int iClaimID)
        {
            DbReader objReader = null;
            string sSQL = string.Empty;
            string sModelDesc = string.Empty;

            try
            {
                sSQL = "SELECT VEHICLE_MODEL FROM VEHICLE, UNIT_X_CLAIM WHERE VEHICLE.UNIT_ID = UNIT_X_CLAIM.UNIT_ID AND UNIT_X_CLAIM.CLAIM_ID = " + iClaimID + " AND UNIT_X_CLAIM.UNIT_ID = " + iUnitID;

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        sModelDesc = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }
                }
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return sModelDesc;
        }
        private int iGetUnitIdFromRowId(int iUnitRowID, int iClaimID)
        {
            DbReader objReader = null;
            string sSQL = string.Empty;
            int iThisUnitID = 0;

            try
            {
                sSQL = "SELECT UNIT_ID FROM UNIT_X_CLAIM WHERE UNIT_ROW_ID = " + iUnitRowID + " AND CLAIM_ID = " + iClaimID;

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iThisUnitID = Conversion.ConvertObjToInt(objReader.GetValue("UNIT_ID"), base.ClientId);
                    }
                }
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return iThisUnitID;
        }
        /// <summary>
        /// Is Claim Frozen
        /// </summary>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <returns>Claim Frozen Flag</returns>
        private bool IsClaimFrozen(int p_iClaimId)
        {
            Claim objClaim = null;
            bool bIsClaimFrozen = false;

            try
            {
                //if (p_iClaimId == 0)
                //    p_iClaimId = m_objFundManager.ClaimId;

                if (p_iClaimId != 0)
                {
                    objClaim = (Claim)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(p_iClaimId);

                    bIsClaimFrozen = objClaim.PaymntFrozenFlag;
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundsForm.IsClaimFrozen.Error", base.ClientId), p_objEx);
            }
            finally
            {
                objClaim = null;
            }
            return (bIsClaimFrozen);
        }


		private void AppenedFundsAutoNode( FundsAuto p_objFundsAuto )
		{
			XmlDocument objSerializationConfigXmlDoc = null ;
			XmlDocument objFundsAutoXmlDoc = null ;
			XmlNode objFundsAutoNode = null ;

			string sFundsAutoData = string.Empty ;

			objSerializationConfigXmlDoc = new XmlDocument();
			objSerializationConfigXmlDoc.LoadXml("<FundsAuto><PayeeEntity/><MailToEntity /><MailToAddress /></FundsAuto>"); 
            //Deb Multi Currency
            int iBaseCurrCode = p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType;
            if (p_objFundsAuto.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
            {
                if (p_objFundsAuto.IsNew)
                {
                    if (iBaseCurrCode > 0)
                    {
                        int iClaimCurrCode = p_objFundsAuto.Context.DbConn.ExecuteInt("SELECT CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID =" + p_objFundsAuto.ClaimId);
                        if (iClaimCurrCode > 0 && p_objFundsAuto.PmtCurrencyType <= 0)
                        {
                            p_objFundsAuto.PmtCurrencyType = iClaimCurrCode;
                        }
                        else if (p_objFundsAuto.PmtCurrencyType <= 0)
                        {
                            p_objFundsAuto.PmtCurrencyType = iBaseCurrCode;
                        }
                    }
                    else
                    {
                        Errors.Add(Globalization.GetString("BaseCurrency.Error", base.ClientId), "Please set the Base Currency in the Utilities", BusinessAdaptorErrorType.Error);
                    }
                    if (p_objFundsAuto.AutoSplitList.Count > 0)
                    {
                        base.AddReadOnlyNode("currencytypetext");
                    }
                }
                else
                {
                    if (iBaseCurrCode > 0)
                    {
                        int iClaimCurrCode = p_objFundsAuto.Context.DbConn.ExecuteInt("SELECT CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_ID =" + p_objFundsAuto.ClaimId);
                        if (iClaimCurrCode > 0 && p_objFundsAuto.PmtCurrencyType <= 0)
                        {
                            p_objFundsAuto.PmtCurrencyType = iClaimCurrCode;
                        }
                        else if (p_objFundsAuto.PmtCurrencyType <= 0)
                        {
                            p_objFundsAuto.PmtCurrencyType = iBaseCurrCode;
                        }
                    }
                    else
                    {
                        Errors.Add(Globalization.GetString("BaseCurrency.Error", base.ClientId), "Please set the Base Currency in the Utilities", BusinessAdaptorErrorType.Error);
                    }
                    int iTransCount = p_objFundsAuto.Context.DbConn.ExecuteInt("SELECT COUNT(1) FROM FUNDS_AUTO WHERE CLAIM_ID=" + p_objFundsAuto.ClaimId);
                    if (iTransCount > 0)
                    {
                        base.AddReadOnlyNode("currencytypetext");
                    }
                }
            }
            else
            {
                if (iBaseCurrCode > 0)
                {
                    if (p_objFundsAuto.PmtCurrencyType <= 0)
                    {
                        p_objFundsAuto.PmtCurrencyType = iBaseCurrCode;
                    }
                }
                base.AddKillNode("currencytypetext");
            }
            //Deb Multi Currency
			sFundsAutoData = p_objFundsAuto.SerializeObject( objSerializationConfigXmlDoc ); 
			
			objFundsAutoXmlDoc = new XmlDocument();		
			objFundsAutoXmlDoc.LoadXml(sFundsAutoData);

            /*
            //start:rupal, bob change
            XmlNode objClaimantEId = objFundsAutoXmlDoc.SelectSingleNode("//FundsAuto/ClaimantEid");
            if (objClaimantEId != null)
            {
                if (string.IsNullOrEmpty(objClaimantEId.InnerText))
                {
                    string sClaimandEId = base.GetSysExDataNodeText("//SysExternalParam/ClaimantEid");
                    objClaimantEId.InnerText = sClaimandEId;
                    objClaimantEId.Attributes["codeid"].Value = sClaimandEId;
                }
            }
            //end:rupal
            */
			objFundsAutoNode = SysEx.DocumentElement.SelectSingleNode("FundsAuto");
			if( objFundsAutoNode != null )
				SysEx.DocumentElement.RemoveChild( objFundsAutoNode );

          

			objFundsAutoNode = SysEx.ImportNode(objFundsAutoXmlDoc.SelectSingleNode("//FundsAuto"),true);
			SysEx.DocumentElement.AppendChild(objFundsAutoNode);    
		}


		private void SetPaymentsList( SortedList p_objNonThiredPartyPayment , SortedList p_objThiredPartyPayment )
		{
			foreach( FundsAuto objFundsAuto in this.FundsAutoBatch.FundsAutoList )
			{
				if( objFundsAuto.ThirdPartyFlag == 0 )
				{
					/* Vaibhav Kaushik 11 Feb 2006 
					 * This is Non Thired Party Payment.
					 * 
					 * Number of Non Thired Part Payments = Total Number Of Payments
					 * 
					 * List is Sorted on PayNumber.
					 */
                    // averma62 MTIS 31329 p_objNonThiredPartyPayment.Add( objFundsAuto.PayNumber, objFundsAuto );
                    p_objNonThiredPartyPayment.Add(objFundsAuto.PayNumber + "," + objFundsAuto.AutoTransId.ToString(), objFundsAuto);// averma62 MTIS 31329
				}
				else
				{
                    /* Vaibhav Kaushik 11 Feb 2006 
                     * This is Thired Party Payment.
                     * 
                     * Number of Thired Part Payments = Total Number Of Payments * Number of Thired Party Payments
                     * 
                     * List is sorted on PayNumber and TransId.
                     * tkr MITS 10527 add IComparer, was improperly sorting "10..." before "2..."
                     */
                    p_objThiredPartyPayment.Add(objFundsAuto.PayNumber.ToString() + "," + objFundsAuto.AutoTransId.ToString(), objFundsAuto);
				}				
			}
		}

		
		private FundsAuto FirstPayment( SortedList m_objFundsPayment )
		{
			return( (FundsAuto)m_objFundsPayment.GetByIndex(0) );
		}


		#region Caculate Amounts
		private void CalculateAmounts( SortedList m_objNonThiredPartyPaymentList, SortedList m_objThiredPartyPaymentList )
		{
			double dGrossAmount = 0.0;
			double dTppTotalAmount = 0.0;
			double dNetAmount = 0.0;
			
			int iSelectedPayNumber = 0;
			int iNoOfTpps = 0;			

			/*
			 * Vaibhav Kaushik 11 Feb 2006 
			 * Code is rewritten to make independent to the sortorder. 
			 */
			// Calculate amounts for any one pay number
			if( m_objNonThiredPartyPaymentList.Count > 0 )
			{
				iSelectedPayNumber = FirstPayment( m_objNonThiredPartyPaymentList ).PayNumber ;
				dNetAmount += FirstPayment( m_objNonThiredPartyPaymentList ).Amount;
				foreach( FundsAutoSplit objNonThiredPartyPaymentSplit in FirstPayment( m_objNonThiredPartyPaymentList ).AutoSplitList )
				{
					if( objNonThiredPartyPaymentSplit.Tag == 0 ) 
					{
						// This is Non-Thired Party Split Payment.
						dGrossAmount += objNonThiredPartyPaymentSplit.Amount;   
					}		
					else
					{
						// This is Thired Party Negative Payment.
						dTppTotalAmount += objNonThiredPartyPaymentSplit.Amount;						
					}
				}
				for( int iIndex = 0 ; iIndex < m_objThiredPartyPaymentList.Count ; iIndex++ ) 
				{			
					FundsAuto objThiredPartyPayment = ( FundsAuto ) m_objThiredPartyPaymentList.GetByIndex(iIndex) ;
					if( objThiredPartyPayment.PayNumber == FirstPayment( m_objNonThiredPartyPaymentList ).PayNumber )
					{
						// Only single split, for thired party payments.
						iNoOfTpps++;
					}
				}
			}
            if (this.FundsAutoBatch.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
            {
                //double dExhRateBaseToPmt = FundsAutoBatch.Context.DbConn.ExecuteDouble(string.Format("SELECT EXCHANGE_RATE FROM CURRENCY_RATE WHERE SOURCE_CURRENCY_CODE={0} AND DESTINATION_CURRENCY_CODE={1}", FundsAutoBatch.Context.InternalSettings.SysSettings.BaseCurrencyType, FirstPayment(m_objNonThiredPartyPaymentList).PmtCurrencyType));
                double dExhRateBaseToPmt = FirstPayment(m_objNonThiredPartyPaymentList).BaseToPmtCurRate;
                if(FirstPayment(m_objNonThiredPartyPaymentList).BaseToPmtCurRate == 0)
                    dExhRateBaseToPmt = CommonFunctions.ExchangeRateSrc2Dest(FundsAutoBatch.Context.InternalSettings.SysSettings.BaseCurrencyType, FirstPayment(m_objNonThiredPartyPaymentList).PmtCurrencyType, m_sConnectionString, base.ClientId);
                string sPmtCurr = FundsAutoBatch.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + FirstPayment(m_objNonThiredPartyPaymentList).PmtCurrencyType);
                string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                if (!string.IsNullOrEmpty(sPmtCurr))
                {
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sPmtCurr.Split('|')[1]);
                    base.ResetSysExData("NonThiredPartyAmount", (dGrossAmount * dExhRateBaseToPmt).ToString());
                    base.ResetSysExData("GrossAmount", string.Format("{0:C}", dGrossAmount * dExhRateBaseToPmt * this.FundsAutoBatch.TotalPayments));
                    base.ResetSysExData("NetAmount", string.Format("{0:C}", dNetAmount * dExhRateBaseToPmt * this.FundsAutoBatch.TotalPayments));
                    base.ResetSysExData("NoOfThirdParties", iNoOfTpps.ToString());
                    base.ResetSysExData("ThirdPartyTotal", string.Format("{0:C}", dTppTotalAmount * dExhRateBaseToPmt * this.FundsAutoBatch.TotalPayments));
                    // Vaibhav 01-Dec-2006
                    // GrossToDate is not a calculated field. 
                    // Print check update GrossToDate Field each time whenever a check is printed from the Auto Check Batch.
                    //base.ResetSysExData("GrossToDate", string.Format("{0:C}" , (dGrossAmount - dTppTotalAmount ) * (iSelectedPayNumber - 1 ) ) );    
                    base.ResetSysExData("GrossToDate", string.Format("{0:C}", this.FundsAutoBatch.GrossToDate));
                    base.ResetSysExData("TotalNumberOfChecks", ((iNoOfTpps * this.FundsAutoBatch.TotalPayments) + this.FundsAutoBatch.TotalPayments).ToString());
                    base.ResetSysExData("PaymentGrandTotal", string.Format("{0:C}", (dGrossAmount * dExhRateBaseToPmt * this.FundsAutoBatch.TotalPayments)));
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sCulture);
                }
            }
            else
            {
                string sBaseCurr = FundsAutoBatch.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + FundsAutoBatch.Context.InternalSettings.SysSettings.BaseCurrencyType);
                string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                if (!string.IsNullOrEmpty(sBaseCurr))
                {
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sBaseCurr.Split('|')[1]);
                    base.ResetSysExData("NonThiredPartyAmount", dGrossAmount.ToString());
                    base.ResetSysExData("GrossAmount", string.Format("{0:C}", dGrossAmount * this.FundsAutoBatch.TotalPayments));
                    base.ResetSysExData("NetAmount", string.Format("{0:C}", dNetAmount * this.FundsAutoBatch.TotalPayments));
                    base.ResetSysExData("NoOfThirdParties", iNoOfTpps.ToString());
                    base.ResetSysExData("ThirdPartyTotal", string.Format("{0:C}", dTppTotalAmount * this.FundsAutoBatch.TotalPayments));
                    // Vaibhav 01-Dec-2006
                    // GrossToDate is not a calculated field. 
                    // Print check update GrossToDate Field each time whenever a check is printed from the Auto Check Batch.
                    //base.ResetSysExData("GrossToDate", string.Format("{0:C}" , (dGrossAmount - dTppTotalAmount ) * (iSelectedPayNumber - 1 ) ) );    
                    base.ResetSysExData("GrossToDate", string.Format("{0:C}", this.FundsAutoBatch.GrossToDate));
                    base.ResetSysExData("TotalNumberOfChecks", ((iNoOfTpps * this.FundsAutoBatch.TotalPayments) + this.FundsAutoBatch.TotalPayments).ToString());
                    base.ResetSysExData("PaymentGrandTotal", string.Format("{0:C}", (dGrossAmount * this.FundsAutoBatch.TotalPayments)));
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sCulture);
                }
            }

		}
		
		#endregion 
		
		#region Appened Data for Payment Information.
		/* This method creates data for Payment Information Grid.
			 * <PaymentInformation>
			 *		<listhead>
			 *			<PaymentNumber/>
			 *			.
			 *			.		
			 *		</listhead>
			 *		<option>
			 *			<PaymentNumber/>
			 *			.
			 *			. 
			 *		</option>
			 *		<option>
			 *			<PaymentNumber/>
			 *			.
			 *			. 
			 *		</option>
			 * </PaymentInformation>
			 * 
			 * Nodes under <listhead> are the Grid headers. Grid rows are represented by <option> tags.
			 * */
		private void AppenedPaymentInformationGridData( SortedList p_objNonThiredPartyPaymentList )
		{
			XmlElement objPaymentInformationElement = null ;
			XmlElement objListHeadXmlElement = null ;
			XmlElement objOptionXmlElement = null;
			
			int iPaymentCount = 0;
			double dActualSplitTotal = 0.0;
			double dTppSplitTotal = 0.0;
			try
			{
				objPaymentInformationElement = (XmlElement)base.SysEx.SelectSingleNode( "/SysExData/PaymentInformation");
				if( objPaymentInformationElement != null )
					objPaymentInformationElement.ParentNode.RemoveChild( objPaymentInformationElement );
					
				//base.SysEx.DocumentElement.RemoveChild( (XmlNode)objPaymentInformationElement.p );

				this.CreateElement( base.SysEx.DocumentElement , "PaymentInformation" , ref objPaymentInformationElement );			
			
				// Create HEADER nodes for Grid.
				this.CreateElement( objPaymentInformationElement , "listhead" , ref objListHeadXmlElement );
				this.CreateAndSetElement( objListHeadXmlElement , "PaymentNumber" , "Payment Number" );
				this.CreateAndSetElement( objListHeadXmlElement , "PrintDate" , "Print Date" );
				this.CreateAndSetElement( objListHeadXmlElement , "PaymentTotal" , "Payment Total" );
				this.CreateAndSetElement( objListHeadXmlElement , "ThirdPartyTotal" , "Third Party Total" );
				this.CreateAndSetElement( objListHeadXmlElement , "NetPayment" , "Net Payment" );
			
				for( int iIndex = 0 ; iIndex < p_objNonThiredPartyPaymentList.Count ; iIndex++ ) 
				{			
					FundsAuto objFundsAuto = ( FundsAuto ) p_objNonThiredPartyPaymentList.GetByIndex(iIndex) ;

					dActualSplitTotal = 0.0;
					dTppSplitTotal = 0.0;
                    //Deb Multi Currency
                    if (objFundsAuto.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                    {
                        foreach (FundsAutoSplit objFundsAutoSplit in objFundsAuto.AutoSplitList)
                        {
                            if (objFundsAutoSplit.Tag == 0)
                                dActualSplitTotal += objFundsAutoSplit.PmtCurrencyAmount;
                            else
                                dTppSplitTotal += (-1 * objFundsAutoSplit.PmtCurrencyAmount);
                        }
                    }
                    else
                    {
                        foreach (FundsAutoSplit objFundsAutoSplit in objFundsAuto.AutoSplitList)
                        {
                            if (objFundsAutoSplit.Tag == 0)
                                dActualSplitTotal += objFundsAutoSplit.Amount;
                            else
                                dTppSplitTotal += (-1 * objFundsAutoSplit.Amount);
                        }
                    }
                    //Deb Multi Currency
					this.CreateElement( objPaymentInformationElement , "option" , ref objOptionXmlElement );
                    
                    // This ref is inconsistent with the Future Payments grid and
                    // not being recognised.Don't know why it was being used like 
                    // this in R4.
                    //objOptionXmlElement.SetAttribute( "ref" , INSTANCE_SYSEXDATA_PATH + "/" + objPaymentInformationElement.OwnerDocument.DocumentElement.LocalName  + "/option[" + (++iPaymentCount).ToString() + "]" );
                    objOptionXmlElement.SetAttribute("ref", INSTANCE_SYSEXDATA_PATH + "/" + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iPaymentCount).ToString() + "]");

                    
			
					//this.CreateAndSetElement( objOptionXmlElement , "AutoTransId" , objFundsAuto.AutoTransId.ToString() );
					this.CreateAndSetElement( objOptionXmlElement , "PaymentNumber" , objFundsAuto.PayNumber.ToString() );
					this.CreateAndSetElement( objOptionXmlElement , "PrintDate" , Conversion.GetDBDateFormat( objFundsAuto.PrintDate , "d" ) );

                    //Deb Multi Currency
                    //MITS 21405: SMISHRA54 - Negative amount should be display with parenthesis like $-150 as ($150)
                    //this.CreateAndSetElement(objOptionXmlElement, "PaymentTotal", dActualSplitTotal.ToString("c"));
                    //this.CreateAndSetElement(objOptionXmlElement, "ThirdPartyTotal", dTppSplitTotal.ToString("c"));
                    //this.CreateAndSetElement(objOptionXmlElement, "NetPayment", Math.Round((dActualSplitTotal - dTppSplitTotal), 2).ToString("c"));					
                    //MITS 21405: End
                    if (objFundsAuto.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                    {
                        if (objFundsAuto.PmtCurrencyType > 0)
                        {
                            //double dExhRateBaseToPmt = objFundsAuto.Context.DbConn.ExecuteDouble(string.Format("SELECT EXCHANGE_RATE FROM CURRENCY_RATE WHERE SOURCE_CURRENCY_CODE={0} AND DESTINATION_CURRENCY_CODE={1}", objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, objFundsAuto.PmtCurrencyType));
                            //Commenting as not required
                            //double dExhRateBaseToPmt = 1;
                            //if (objFundsAuto.BaseToPmtCurRate == 0)
                            //    dExhRateBaseToPmt =  CommonFunctions.ExchangeRateSrc2Dest(objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, objFundsAuto.PmtCurrencyType, m_sConnectionString);
                            string sPmtCurr = objFundsAuto.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + objFundsAuto.PmtCurrencyType);
                            string sCulture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                            if (!string.IsNullOrEmpty(sPmtCurr))
                            {
                                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sPmtCurr.Split('|')[1]);
                                this.CreateAndSetElement(objOptionXmlElement, "PaymentTotal", (dActualSplitTotal).ToString("c"));
                                this.CreateAndSetElement(objOptionXmlElement, "ThirdPartyTotal", (dTppSplitTotal).ToString("c"));
                                this.CreateAndSetElement(objOptionXmlElement, "NetPayment", (Math.Round((dActualSplitTotal - dTppSplitTotal), 2)).ToString("c"));
                                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sCulture);
                            }
                        }
                        else
                        {
                            this.CreateAndSetElement(objOptionXmlElement, "PaymentTotal", dActualSplitTotal.ToString("c"));
                            this.CreateAndSetElement(objOptionXmlElement, "ThirdPartyTotal", dTppSplitTotal.ToString("c"));
                    this.CreateAndSetElement(objOptionXmlElement, "NetPayment", Math.Round((dActualSplitTotal - dTppSplitTotal), 2, MidpointRounding.AwayFromZero).ToString("c"));					
                        }
                    }
                    else
                    {
                        this.CreateAndSetElement(objOptionXmlElement, "PaymentTotal", dActualSplitTotal.ToString("c"));
                        this.CreateAndSetElement(objOptionXmlElement, "ThirdPartyTotal", dTppSplitTotal.ToString("c"));
                        this.CreateAndSetElement(objOptionXmlElement, "NetPayment", Math.Round((dActualSplitTotal - dTppSplitTotal), 2).ToString("c"));	
                    }
                    //Deb Multi Currency
				}
			}
			finally
			{
				objPaymentInformationElement = null ;
				objListHeadXmlElement = null ;
				objOptionXmlElement = null;
			}			
		}

		#endregion 

		#region Appened Data for Non-Thired Party Payment Split Collection.
		/*
		 * Vaibhav Kaushik 11 Feb 2006
		 * This function is used to serialize the FundsAutoSplit Object for *****Non Thired Party Payments***** Only.
		 * 
		 * It take care ihe case if Funds-Auto-Splt object is a ****NEW**** Thired Party Payment.
		 * 
		 * We use this function to serialize the Xml in place of DataModel serialize function, cause we need the 
		 * data in a specific format to disply the content using the *****Grid control***** of xforms-control.xsl .
		 * 
		 */ 
		private void XmlForFundsAutoSplit( XmlElement p_objRootElement , FundsAutoSplit p_objFundsAutoSplit , int p_iCount,FundsAuto p_objFundsAuto )
		{
			XmlElement objOptionXmlElement = null;
			XmlElement objXmlElement = null;
			double dReserveBalance = 0.0 ;
			string sTransTypeCodeDescription = string.Empty; 
			string sReserveTypeCodeDescription = string.Empty ;
			string sGlAccountCodeDescription = string.Empty ;
            Policy objPolicy = null;
            int iResult = 0;
            int iCoverageId = 0;
            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;
            int iCoveTypeCode = 0;
            XmlNode objCoverageType = null;
            string sCovgSeqNum = string.Empty;//Ankit : Policy System Interface
            //start - Added by Nikhil.
            bool bOverrideDedFlagVisible = false;
            bool bIsDedProcAllowed = false;
            //End - Added by Nikhil.
			try
			{
				this.CreateElement( p_objRootElement , "option" , ref objOptionXmlElement );
				objOptionXmlElement.SetAttribute( "ref" , INSTANCE_SYSEXDATA_PATH + "/" + objOptionXmlElement.ParentNode.LocalName  + "/option[" + p_iCount.ToString() + "]" );
				if( p_objFundsAutoSplit.AutoSplitId == 0 )
					objOptionXmlElement.SetAttribute( "type" , "new" );
			
				if( p_objFundsAutoSplit.AutoSplitId != 0 )
				{
                    //Change by kuladeep for mits:33564 Start
                    //sTransTypeCodeDescription = this.objCache.GetCodeDesc(p_objFundsAutoSplit.TransTypeCode);
                    //sReserveTypeCodeDescription = this.objCache.GetCodeDesc(p_objFundsAutoSplit.ReserveTypeCode);
                    //sGlAccountCodeDescription = this.objCache.GetCodeDesc(p_objFundsAutoSplit.GlAccountCode);
                    sTransTypeCodeDescription = this.objCache.GetCodeDesc(p_objFundsAutoSplit.TransTypeCode, base.Adaptor.userLogin.objUser.NlsCode);
                    sReserveTypeCodeDescription = this.objCache.GetCodeDesc(p_objFundsAutoSplit.ReserveTypeCode, base.Adaptor.userLogin.objUser.NlsCode);
                    sGlAccountCodeDescription = this.objCache.GetCodeDesc(p_objFundsAutoSplit.GlAccountCode, base.Adaptor.userLogin.objUser.NlsCode);
                    //Change by kuladeep for mits:33564 End

					//dReserveBalance = GetFundManagerObject().GetReserveBalance( objFundsAutoFirst.ClaimId , objFundsAutoFirst.ClaimantEid , objFundsAutoFirst.UnitId , p_objFundsAutoSplit.ReserveTypeCode , iLob , p_objFundsAutoSplit.AutoTransId );							
				}
			
				this.CreateAndSetElement( objOptionXmlElement , "TransTypeCode" , sTransTypeCodeDescription , ref objXmlElement );
				objXmlElement.SetAttribute( "codeid" , p_objFundsAutoSplit.TransTypeCode.ToString() );

				this.CreateAndSetElement( objOptionXmlElement , "ReserveTypeCode" , sReserveTypeCodeDescription , ref objXmlElement );
				objXmlElement.SetAttribute( "codeid" , p_objFundsAutoSplit.ReserveTypeCode.ToString() );

                //MITS 21405: SMISHRA54 - Negative amount should be display with parenthesis like $-150 as ($150)
                //Deb Multi Currency
                //this.CreateAndSetElement(objOptionXmlElement, "Amount", p_objFundsAutoSplit.Amount.ToString("c"));
                if (p_objFundsAutoSplit.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                {
                    if (p_objFundsAuto.PmtCurrencyType > 0)
                    {
                        this.CreateAndSetElement(objOptionXmlElement, "Amount", p_objFundsAutoSplit.PmtCurrencyAmount.ToString("c"), CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.PmtCurrencyType, p_objFundsAutoSplit.PmtCurrencyAmount, m_sConnectionString, base.ClientId), "CurrencyValue");
                    }
                    //else if (p_objFundsAuto.ClaimCurrencyType > 0)
                    //{
                    //    this.CreateAndSetElement(objOptionXmlElement, "Amount", p_objFundsAutoSplit.ClaimCurrencyAmount.ToString("c"), CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.ClaimCurrencyType, p_objFundsAutoSplit.ClaimCurrencyAmount, m_sConnectionString), "CurrencyValue");
                    //}
                    else
                    {
                        this.CreateAndSetElement(objOptionXmlElement, "Amount", p_objFundsAutoSplit.Amount.ToString("c"), CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, p_objFundsAutoSplit.Amount, m_sConnectionString, base.ClientId), "CurrencyValue");
                    }
                }
                else
                {
                    this.CreateAndSetElement(objOptionXmlElement, "Amount", p_objFundsAutoSplit.Amount.ToString("c"), CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, p_objFundsAutoSplit.Amount, m_sConnectionString, base.ClientId), "CurrencyValue");
                }
                //Deb Multi Currency
                //MITS 21405: End
				this.CreateAndSetElement( objOptionXmlElement , "FromDate" , Conversion.GetDBDateFormat( p_objFundsAutoSplit.FromDate , "d" ) );
				this.CreateAndSetElement( objOptionXmlElement , "ToDate" , Conversion.GetDBDateFormat( p_objFundsAutoSplit.ToDate, "d" ) );					
					
				/************** Hidden Columns on the Grid ******************/
				this.CreateAndSetElement( objOptionXmlElement , "AutoSplitId" , p_objFundsAutoSplit.AutoSplitId.ToString() );
			
				this.CreateAndSetElement( objOptionXmlElement , "GlAccountCode" , sGlAccountCodeDescription , ref objXmlElement );
				objXmlElement.SetAttribute( "codeid" , p_objFundsAutoSplit.GlAccountCode.ToString() );
					
				this.CreateAndSetElement( objOptionXmlElement , "InvoicedBy" , p_objFundsAutoSplit.InvoicedBy );					
				this.CreateAndSetElement( objOptionXmlElement , "InvoiceNumber" , p_objFundsAutoSplit.InvoiceNumber );
                //Deb Multi Currency	
				//this.CreateAndSetElement( objOptionXmlElement , "InvoiceAmount" , p_objFundsAutoSplit.InvoiceAmount.ToString() );
                if (p_objFundsAutoSplit.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                {
                    if (p_objFundsAuto.PmtCurrencyType > 0)
                    {
                        this.CreateAndSetElement(objOptionXmlElement, "InvoiceAmount", p_objFundsAutoSplit.PmtCurrencyInvAmount.ToString("c"), CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.PmtCurrencyType, p_objFundsAutoSplit.PmtCurrencyInvAmount, m_sConnectionString, base.ClientId), "CurrencyValue");
                    }
                    //else if (p_objFundsAuto.ClaimCurrencyType > 0)
                    //{
                    //    this.CreateAndSetElement(objOptionXmlElement, "InvoiceAmount", p_objFundsAutoSplit.ClaimCurrencyInvAmount.ToString("c"), CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.ClaimCurrencyType, p_objFundsAutoSplit.ClaimCurrencyInvAmount, m_sConnectionString), "CurrencyValue");
                    //}
                    else
                    {
                        this.CreateAndSetElement(objOptionXmlElement, "InvoiceAmount", p_objFundsAutoSplit.InvoiceAmount.ToString("c"), CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, p_objFundsAutoSplit.InvoiceAmount, m_sConnectionString, base.ClientId), "CurrencyValue");
                    }
                }
                else
                {
                    this.CreateAndSetElement(objOptionXmlElement, "InvoiceAmount", p_objFundsAutoSplit.InvoiceAmount.ToString("c"), CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, p_objFundsAutoSplit.InvoiceAmount, m_sConnectionString, base.ClientId), "CurrencyValue");
                }
                //Deb Multi Currency
				this.CreateAndSetElement( objOptionXmlElement , "InvoiceDate" , Conversion.GetDBDateFormat( p_objFundsAutoSplit.InvoiceDate , "d" ));					
				this.CreateAndSetElement( objOptionXmlElement , "PoNumber" , p_objFundsAutoSplit.PoNumber );					
				this.CreateAndSetElement( objOptionXmlElement , "Tag" , p_objFundsAutoSplit.Tag.ToString() );					
				this.CreateAndSetElement( objOptionXmlElement , "reservebalance" , dReserveBalance.ToString() );
                //BOB
                bCarrierClaims = Conversion.ConvertStrToBool(p_objFundsAutoSplit.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString());
                //if (bCarrierClaims)
                //{
                //    iCoverageId = p_objFundsAutoSplit.CoverageId;
                //    iResult = GetPolicyIDOrCoverageCode(iCoverageId, out iCoveTypeCode);
                //    objPolicy = (Policy)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Policy", false);
                //    objPolicy.MoveTo(iResult);
                //    this.CreateAndSetElement(objOptionXmlElement, "PolicyID", objPolicy.PolicyName.ToString(), objPolicy.PolicyId.ToString(), "value");
                //    this.objCache.GetCodeInfo(iCoveTypeCode, ref sShortCode, ref sCodeDesc);
                //    this.CreateAndSetElement(objOptionXmlElement, "CoverageTypeCode", sShortCode + " " + sCodeDesc, iCoveTypeCode.ToString(), "codeid");
                //    if (objOptionXmlElement.SelectNodes("//CoverageTypeCode").Count > 1)
                //    {
                //        objCoverageType = objOptionXmlElement.SelectNodes("//CoverageTypeCode").Item(1);
                //    }
                //    else
                //    {
                //        objCoverageType = objOptionXmlElement.SelectSingleNode("//CoverageTypeCode");
                //    }
                //    XmlAttribute tabAttr = base.SysEx.CreateAttribute("tablename");
                //    tabAttr.Value = "COVERAGE_TYPE";
                //    objCoverageType.Attributes.Append(tabAttr);
                //}
                //rupal:start, r8 unit changes
                if (bCarrierClaims)
                {
                    long lPolicyUnitRowId;
                    int iPolicyID;
                    //Mona: Maintaining relation b/w FUNDS_TRANS_SPLIT and RESERVE_CURRENT through RC_ROW_ID 
                    //iCoverageId = p_objFundsAutoSplit.CoverageId;
                    iCoverageId = CommonFunctions.GetCovRowIdFromRCRowId(FundsAutoBatch.Context.DbConn.ConnectionString, p_objFundsAutoSplit.RCRowId, base.ClientId);
                    //Ankit : Start Policy System Interface
                    //GetPolicyDetails(iCoverageId, out iPolicyID, out lPolicyUnitRowId, out iCoveTypeCode);
                    GetPolicyDetails(iCoverageId, out iPolicyID, out lPolicyUnitRowId, out iCoveTypeCode, out sCovgSeqNum);
                    objPolicy = (Policy)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Policy", false);
                    objPolicy.MoveTo(iPolicyID);

                    this.CreateAndSetElement(objOptionXmlElement, "PolicyID", objPolicy.PolicyName.ToString(), objPolicy.PolicyId.ToString(), "value");
					//RMA-16311-naresh -start
                    if (p_objFundsAutoSplit.AutoTransId > 0)
                    {
                        base.ResetSysExData("PolID", objPolicy.PolicyId.ToString());
                        base.ResetSysExData("PolIDDesc", objPolicy.PolicyName);
                    }
					//RMA-16311-naresh -end
                    this.CreateAndSetElement(objOptionXmlElement, "UnitID", GetSelectedUnit(objPolicy.PolicyId, iCoverageId, lPolicyUnitRowId), lPolicyUnitRowId.ToString(), "value");
					//RMA-16311-naresh -start
                    if (p_objFundsAutoSplit.AutoTransId > 0)
                    {
                        base.ResetSysExData("PolUnitRowID", lPolicyUnitRowId.ToString());
                        base.ResetSysExData("PolUnitDesc", GetSelectedUnit(objPolicy.PolicyId, iCoverageId, lPolicyUnitRowId));
                    }
					//RMA-16311-naresh -end
                    this.CreateAndSetElement(objOptionXmlElement, "CovgSeqNum", sCovgSeqNum, sCovgSeqNum, "value");

                    this.objCache.GetCodeInfo(iCoveTypeCode, ref sShortCode, ref sCodeDesc, base.Adaptor.userLogin.objUser.NlsCode); //Aman Ml Change
					//RMA-16311-naresh -start
                    if (objPolicy.PolicySystemId > 0)
                    {
                        this.CreateAndSetElement(objOptionXmlElement, "CoverageTypeCode", GetCoverageText(Convert.ToInt32(lPolicyUnitRowId), iCoverageId, iCoveTypeCode), iCoveTypeCode.ToString(), "codeid");
                        if (p_objFundsAutoSplit.AutoTransId > 0)
                        {
                            base.ResetSysExData("CovID", iCoveTypeCode.ToString());
                            base.ResetSysExData("CovIDDesc", GetCoverageText(Convert.ToInt32(lPolicyUnitRowId), iCoverageId, iCoveTypeCode));
                        }
                    }
                    else
                    {
                        this.CreateAndSetElement(objOptionXmlElement, "CoverageTypeCode", sShortCode + " " + sCodeDesc, iCoveTypeCode.ToString(), "codeid");

                        if (p_objFundsAutoSplit.AutoTransId > 0)
                        {
                            base.ResetSysExData("CovID", iCoveTypeCode.ToString());
                            base.ResetSysExData("CovIDDesc", sShortCode + " " + sCodeDesc);
                        }
                    }
					//RMA-16311-naresh -end
                    if (objOptionXmlElement.SelectNodes("//CoverageTypeCode").Count > 1)
                    {
                        objCoverageType = objOptionXmlElement.SelectNodes("//CoverageTypeCode").Item(1);
                    }
                    else
                    {
                        objCoverageType = objOptionXmlElement.SelectSingleNode("//CoverageTypeCode");
                    }
                    XmlAttribute tabAttr = base.SysEx.CreateAttribute("tablename");
                    tabAttr.Value = "COVERAGE_TYPE";
                    objCoverageType.Attributes.Append(tabAttr);
                    string sLossDesc = string.Empty;
                    string sLossCode = string.Empty;
                    int iDisabilityCat=0;
                    int iCvgLossId=0;
                    int iLossCodeId = CommonFunctions.GetLossType(m_sConnectionString, p_objFundsAutoSplit.RCRowId, ref iDisabilityCat, ref  iCvgLossId, base.ClientId);
                    p_objFundsAutoSplit.Context.LocalCache.GetCodeInfo(iLossCodeId, ref sLossCode, ref sLossDesc,base.Adaptor.userLogin.objUser.NlsCode); //Aman Ml Change
                    Claim objClaim = (Claim)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Claim", false);

                    if (Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimId").InnerText) > 0)
                    {
                        objClaim.MoveTo(Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimId").InnerText));
                        //    if(
                        if (objClaim.LineOfBusCode == 243)
                        {
                            objCache.GetCodeInfo(p_objFundsAutoSplit.Context.LocalCache.GetRelatedCodeId(iLossCodeId), ref sShortCode, ref sCodeDesc, base.Adaptor.userLogin.objUser.NlsCode);
                            this.CreateAndSetElement(objOptionXmlElement, "DisabilityCatCode", sShortCode + " " + sCodeDesc, ref objXmlElement);
                            objXmlElement.SetAttribute("codeid", p_objFundsAutoSplit.Context.LocalCache.GetRelatedCodeId(iLossCodeId).ToString());
                            //MITS 31466 : appended sLossCode with sLossDesc
                            this.CreateAndSetElement(objOptionXmlElement, "DisabilityTypeCode", sLossCode+" "+ sLossDesc, ref objXmlElement);
                            objXmlElement.SetAttribute("codeid", iLossCodeId.ToString());

                           
                        }
                        else
                        {
                            //MITS 31466 : appended sLossCode with sLossDesc
                            this.CreateAndSetElement(objOptionXmlElement, "LossTypeCode", sLossCode+" "+ sLossDesc, ref objXmlElement);
                            objXmlElement.SetAttribute("codeid", iLossCodeId.ToString());
							//RMA-16311-naresh -start
                            if (p_objFundsAutoSplit.AutoTransId > 0)
                            {
                                base.ResetSysExData("LossID", iLossCodeId.ToString());
                                base.ResetSysExData("LossDesc", sLossCode + " " + sLossDesc);
                            }
						//RMA-16311-naresh -end
                        }
                        //Start:added by nitin goel,01/28/2013,MITS 30910
                        //Start -  Changed by Nikhil.
                        // this.CreateAndSetElement(objOptionXmlElement, "IsOverrideDedProcessing", Convert.ToString(p_objFundsAutoSplit.IsOverrideDedProcessing));
                       

                      
                        if (objClaim.LineOfBusCode > 0)
                        {
                            if (objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ApplyDedToPaymentsFlag == -1 && objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface)
                            {
                                this.CreateAndSetElement(objOptionXmlElement, "IsOverrideDedProcessing", Convert.ToString(p_objFundsAutoSplit.IsOverrideDedProcessing));
                            }
                        }
                        //end -  Changed by Nikhil.

                                                                
                        //end: nitin goel
                    }

                    
            
                       this.CreateAndSetElement(objOptionXmlElement, "RCRowId", p_objFundsAutoSplit.RCRowId.ToString());

                       // Start - Added by Nikhil
                     
                  
                       int iLOB = 0;
                       iLOB = p_objFundsAutoSplit.Context.DbConn.ExecuteInt(string.Format("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = {0}",p_objFundsAuto.ClaimId));
                       if (iLOB > 0)
                       {
                           bIsDedProcAllowed = objClaim.Context.RMUser.IsAllowedEx(DED_PROC_SHOW);//ddhiman 10/14/2014, SMS Settings for deductible Procesing
                           if (p_objFundsAuto.Context.InternalSettings.ColLobSettings[iLOB].ApplyDedToPaymentsFlag == -1 && p_objFundsAuto.Context.InternalSettings.SysSettings.UsePolicyInterface && bIsDedProcAllowed)//ddhiman 10/14/2014, SMS Settings for deductible Procesing
                           {
                               bOverrideDedFlagVisible = true;
                           }
                           else
                           {
                               bOverrideDedFlagVisible = false;
                           }
                       }
                       this.CreateAndSetElement(objOptionXmlElement, "OverrideDedFlagVisible", bOverrideDedFlagVisible ? "-1" : "0");
                    
                    //End -  Added by Nikhil.


                       objClaim = null;
                }
               
                //rupal:end
                //Deb Multi Currency
                if (p_objFundsAutoSplit.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                {
                    if (p_objFundsAutoSplit.Context.InternalSettings.SysSettings.BaseCurrencyType > 0)
                    {
                        sBaseCurrCulture = p_objFundsAutoSplit.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + p_objFundsAutoSplit.Context.InternalSettings.SysSettings.BaseCurrencyType);
                        this.CreateAndSetElement(objOptionXmlElement, "BaseCurrencyType", sBaseCurrCulture.Split('|')[1]);
                        this.CreateAndSetElement(objOptionXmlElement, "UseMultiCurrency", Boolean.TrueString);
                    }
                    else
                    {
                        Errors.Add(Globalization.GetString("BaseCurrency.Error", base.ClientId), "Please set the Base Currency in the Utilities", BusinessAdaptorErrorType.Error);
                    }
                }
                else
                {
                    if (p_objFundsAutoSplit.Context.InternalSettings.SysSettings.BaseCurrencyType > 0)
                    {
                        sBaseCurrCulture = p_objFundsAutoSplit.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + p_objFundsAutoSplit.Context.InternalSettings.SysSettings.BaseCurrencyType);
                        this.CreateAndSetElement(objOptionXmlElement, "BaseCurrencyType", sBaseCurrCulture.Split('|')[1]);
                        this.CreateAndSetElement(objOptionXmlElement, "UseMultiCurrency", Boolean.FalseString);
                    }
                }
                //Deb Multi Currency
            }
			finally
			{
				objOptionXmlElement = null;
				objXmlElement = null;				
			}			
		}
        private string GetCoverageText(int iPolicyUnitRowId, int iPolicyCvgRowId, int iCoverageTypeCode)
        {
            string sCoverageText = string.Empty;
            string sShortCode = string.Empty;
            StringBuilder sSQL = null;
            try
            {
                sSQL = new StringBuilder();
				//smahajan22 - mits 35587 start
                //sSQL.Append(" SELECT DISTINCT SHORT_CODE,");
                //sSQL.Append(" CASE(CNT)");
                //sSQL.Append(" WHEN 1 THEN A.CODE_DESC ");
                //sSQL.Append(" ELSE");
                //if (DbFactory.IsOracleDatabase(m_sConnectionString))
                //{
                //    sSQL.Append(" CASE(NVL(COVERAGE_TEXT,''))");
                //}
                //else
                //{
                //    sSQL.Append(" CASE(ISNULL(COVERAGE_TEXT,''))");
                //}
                //sSQL.Append(" WHEN '' THEN A.CODE_DESC ");
                //sSQL.Append(" ELSE COVERAGE_TEXT  ");
                //sSQL.Append(" END  ");
                //sSQL.Append(" END COVERAGE_TEXT");
                //sSQL.Append(" FROM ");
                //sSQL.Append(" ( ");
                //sSQL.Append(" SELECT SHORT_CODE,CODE_DESC, COVERAGE_TEXT, C.COVERAGE_TYPE_CODE ,CVG_SEQUENCE_NO,B.CNT FROM ");
                //sSQL.Append(" ( ");
                //sSQL.Append(" SELECT COVERAGE_TYPE_CODE,COUNT(*) CNT FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + iPolicyUnitRowId + " AND COVERAGE_TYPE_CODE=" + iCoverageTypeCode + " GROUP BY COVERAGE_TYPE_CODE");
                //sSQL.Append(" )B,");
                //sSQL.Append(" (");
                //sSQL.Append(" SELECT POLICY_X_CVG_TYPE.POLCVG_ROW_ID, CODE_DESC, COVERAGE_TEXT, SHORT_CODE,COVERAGE_TYPE_CODE ,CVG_SEQUENCE_NO");
                //sSQL.Append(" FROM POLICY_X_CVG_TYPE,CODES_TEXT");
                //sSQL.Append(" WHERE");
                //sSQL.Append(" POLICY_UNIT_ROW_ID =" + iPolicyUnitRowId + " AND POLCVG_ROW_ID=" + iPolicyCvgRowId);
                //sSQL.Append(" AND CODES_TEXT.CODE_ID=POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE	");
                //sSQL.Append(" ) C WHERE B.COVERAGE_TYPE_CODE = C.COVERAGE_TYPE_CODE");
                //sSQL.Append(" )A ");

                sSQL.Append("SELECT COVERAGE_TEXT FROM POLICY_X_CVG_TYPE WHERE  POLCVG_ROW_ID = " + iPolicyCvgRowId);
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString()))
                {
                    if (objReader.Read())
                    {
                        sCoverageText = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }
                }
                return sCoverageText;
			//smahajan22 - mits 35587 end

            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetReservesBOB.GenericError", base.ClientId), objException);
            }
            finally
            {
                sSQL = null;
            }
        }

        private int GetPolicyIDOrCoverageCode(int iCoverageId, out int iCoveTypeCode)
        {
            int iResult = 0;
            DbReader objReader = null;
            DbConnection objConn = null;
            string sSql = string.Empty;
            iCoveTypeCode = 0;
            try
            {
                //objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT " + sOutData + " FROM RESERVE_CURRENT WHERE RC_ROW_ID =" + iRowId);
                //if (objReader.Read())
                //    iResult = objReader.GetInt32(0);
                //objReader.Close();
                sSql = string.Format("SELECT POLICY_ID,COVERAGE_TYPE_CODE FROM POLICY_X_CVG_TYPE WHERE POLCVG_ROW_ID = {0}", iCoverageId);
                if (iCoverageId != 0)
                {
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                    if (objReader.Read())
                    {
                        iResult = objReader.GetInt32(0);
                        iCoveTypeCode = objReader.GetInt32(1);
                    }
                    objReader.Close();
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("AutoClaimChecksForm.CheckClaimTypeChangeOption.Error", base.ClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
            return iResult;
        }
		/* Vaibhav Kaushik 11 Feb 2006
		 *	This function has been re-written to use the "XmlForFundsAutoSplit()" function to serialize the Object.
		 *	This method creates data for Future Payments Grid.
		 * 
		 * (Essentially each grid row is the FundsAutoSplit data of Payee FundsAuto object)
		 * <FuturePayments>
		 *		<listhead>
		 *			<TransactionType/>
		 *			.
		 *			.		
		 *		</listhead>
		 *		<option ref="/Instance/UI/FormVariables/SysExData/FuturePayments/option[0]">
		 *			<TransactionType/>
		 *			.
		 *			. 
		 *		</option>
		 *			.
		 *			.
		 *			.
		 *			.
		 *		<option ref="/Instance/UI/FormVariables/SysExData/FuturePayments/option[n]" type="new" >
		 *			<TransactionType/>
		 *			.
		 *			. 
		 *		</option>
		 * </FuturePayments>
		 * 
		 * Nodes under <listhead> are the Grid headers. Grid rows are represented by <option> tags.
		 * Since this grid is editable, we need to provide 'ref' attributes for each row, that are picked
		 * by the xsl to render controls for each element of grid bound to the specific node under
		 * <option>. An extra <option> node is provided for capturing data for a new grid row.
		 * 
		 * Ideally, we would like to serialize each FundsAutoSplit object using DataModel, but the
		 * current grid implementation in xforms-controls.xsl picks up columns for display				
		 * as they are available in the <option> tag. Need to enhance the xsl for the Grid			
		 * so that it picks up specified columns only, and that too in order.	
		 * 
		 *   
			The following piece of code is currently not in use. See comments below on 
			constraints in the current Grid implementation.			
			
			XmlDocument objSerializationConfigXmlDoc = new XmlDocument();
			objSerializationConfigXmlDoc.LoadXml("<FundsAutoSplit/>"); 

			string sFundsAutoSplitData = string.Empty;
			XmlDocument objFundsAutoSplitXmlDoc = null;
						
			Ideally, we would like to serialize this object using DataModel, but the
			current grid implementation in xforms-controls.xsl picks up columns for display
			as they are available in the <option> tag. Need to enhance the xsl for the Grid
			so that it picks up specified columns only, and that too in order.
			
			sFundsAutoSplitData = objFundsAutoSplit.SerializeObject(objSerializationConfigXmlDoc);

			objFundsAutoSplitXmlDoc = new XmlDocument();
			objFundsAutoSplitXmlDoc.LoadXml(sFundsAutoSplitData);  

			XmlNode objFundsAutoSplitNode = objFuturePaymentsXmlDoc.ImportNode(objFundsAutoSplitXmlDoc.SelectSingleNode("//FundsAutoSplit"),true);
			objOptionXmlElement.InnerXml = objFundsAutoSplitNode.InnerXml;							                    
		*/
		private void AppenedFuturePaymentsGridData( SortedList m_objNonThiredPartyPaymentList )
		{
			XmlElement objFuturePaymentsElement = null ;
			XmlElement objListHeadXmlElement = null ;
			FundsAutoSplit objFundsAutoSplitTemp = null ;

			int iFuturePaymentCount = 0;			
			int iAutoSplitCount = 0 ;
            //rsharma220 MITS 32723 start
            string sSQL = string.Empty;
            int iLobCode = 0;
            object oClaim = null;
            bool bClaimFound = false;
            //rsharma220 MITS 32723 end

            try
			{
            
                objFundsAutoSplitTemp = (FundsAutoSplit)this.FundsAutoBatch.Context.Factory.GetDataModelObject("FundsAutoSplit", false);
                bCarrierClaims = Conversion.ConvertStrToBool(objFundsAutoSplitTemp.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString());

				if( m_objNonThiredPartyPaymentList.Count > 0 )
					iAutoSplitCount = FirstPayment( m_objNonThiredPartyPaymentList ).AutoSplitList.Count ;
			
				objFuturePaymentsElement = (XmlElement)base.SysEx.SelectSingleNode( "/SysExData/FuturePayments");
				if( objFuturePaymentsElement != null )
					objFuturePaymentsElement.ParentNode.RemoveChild( objFuturePaymentsElement );

				this.CreateElement( base.SysEx.DocumentElement , "FuturePayments" , ref objFuturePaymentsElement );			
				objFuturePaymentsElement.SetAttribute( "count" , iAutoSplitCount.ToString() );

                base.ResetSysExData("FuturePaymentsCount", iAutoSplitCount.ToString());

				// Create HEADER nodes for Grid.
				this.CreateElement( objFuturePaymentsElement , "listhead" , ref objListHeadXmlElement );
                //this.CreateAndSetElement( objListHeadXmlElement , "TransactionType" , "Transaction Type" );
                //this.CreateAndSetElement( objListHeadXmlElement , "ReserveType" , "Reserve Type" );
                this.CreateAndSetElement(objListHeadXmlElement, "TransTypeCode", "Transaction Type");
				this.CreateAndSetElement( objListHeadXmlElement , "ReserveTypeCode" , "Reserve Type" );
                
				this.CreateAndSetElement( objListHeadXmlElement , "Amount" , "Amount" );
				this.CreateAndSetElement( objListHeadXmlElement , "FromDate" , "From Date" );
				this.CreateAndSetElement( objListHeadXmlElement , "ToDate" , "To Date" );
                //BOB
                if (bCarrierClaims)
                {
                    this.CreateAndSetElement(objListHeadXmlElement, "PolicyID", "Policy");
                    //rupal:r8 unit change
                    this.CreateAndSetElement(objListHeadXmlElement, "UnitID", "Unit");                    
                    this.CreateAndSetElement(objListHeadXmlElement, "CoverageTypeCode", "Coverage Type");
                    Claim objClaim = (Claim)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Claim", false);
                    //rsharma220 MITS 32723 start
                    if (base.CurrentAction.ToString().Equals("Delete"))
                    {
                        if (!string.IsNullOrEmpty(base.SysEx.SelectSingleNode("//ThisClaimNumber").InnerText))
                        {

                           // int iLobCode = Conversion.ConvertObjToInt(this.FundsAutoBatch.Context.DbConn.ExecuteScalar("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_NUMBER='" + base.SysEx.SelectSingleNode("//ThisClaimNumber").InnerText.ToString() + "'"), base.ClientId);
                            //    if(
                            Dictionary<string, string> dictParams = new Dictionary<string, string>();
                            sSQL = String.Format("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_NUMBER={0}", "~CLAIMNUMBR~");
                            dictParams.Add("CLAIMNUMBR", base.SysEx.SelectSingleNode("//ThisClaimNumber").InnerText);
                            oClaim = DbFactory.ExecuteScalar(this.FundsAutoBatch.Context.DbConn.ConnectionString, sSQL, dictParams);
                            if (oClaim != null)
                            {
                                iLobCode = Conversion.CastToType<int>(oClaim.ToString(), out bClaimFound);

                                if (iLobCode == 243)
                                {
                                    //this.CreateAndSetElement(objListHeadXmlElement, "DisabilityCatCode", "Disability Category");
                                    this.CreateAndSetElement(objListHeadXmlElement, "DisabilityTypeCode", "Loss Type");
                                }
                                else
                                {

                                    this.CreateAndSetElement(objListHeadXmlElement, "LossTypeCode", "Loss Type");
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimId").InnerText) > 0)
                        {
                            objClaim.MoveTo(Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimId").InnerText));
                            //    if(
                            if (objClaim.LineOfBusCode == 243)
                            {
                                //this.CreateAndSetElement(objListHeadXmlElement, "DisabilityCatCode", "Disability Category");
                                this.CreateAndSetElement(objListHeadXmlElement, "DisabilityTypeCode", "Loss Type");
                            }
                            else
                            {

                                this.CreateAndSetElement(objListHeadXmlElement, "LossTypeCode", "Loss Type");
                            }
                        }
                    }
                    //rsharma220 MITS 32723 end
                    //Start:added by nitin goel,01/28/2013,MITS 30910


                    //Start -  Changed by Nikhil.
                    //     this.CreateAndSetElement(objListHeadXmlElement, "IsOverrideDedProcessing", "Override Deductible"); 
                   

                   
                    if (objClaim.LineOfBusCode > 0)
                    {
                        if (objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ApplyDedToPaymentsFlag == -1 && objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface )
                        {
                            this.CreateAndSetElement(objListHeadXmlElement, "IsOverrideDedProcessing", "Override Deductible"); 
                        }
                    }
                    //end -  Changed by Nikhil.
                
                    //end: nitin goel
                    objClaim = null;

                    
                }

                int iLastReserveID = 0;
                int iCurrentReserveID = 0;
                int iSetReserveID = 0;
                bool bSetReserveID = false;
                bool bFoundReserveID = false;

                if (SysEx.SelectSingleNode("/SysExData/ReserveID") != null)
                {
                    if ((SysEx.SelectSingleNode("/SysExData/ReserveID").InnerXml != "") && (SysEx.SelectSingleNode("/SysExData/ReserveID").InnerXml != "0"))
                    {
                        iCurrentReserveID = Conversion.ConvertStrToInteger(SysEx.SelectSingleNode("/SysExData/ReserveID").InnerXml);
                    }
                }
				// Create option node for each split entry.
                if (m_objNonThiredPartyPaymentList.Count > 0)
                {
                    foreach (FundsAutoSplit objFundsAutoSplit in FirstPayment(m_objNonThiredPartyPaymentList).AutoSplitList)
                    {
                        this.XmlForFundsAutoSplit(objFuturePaymentsElement, objFundsAutoSplit, ++iFuturePaymentCount, FirstPayment(m_objNonThiredPartyPaymentList));

                        if (bFoundReserveID == false)
                        {
                            if (iCurrentReserveID == objFundsAutoSplit.ReserveTypeCode)
                            {
                                bFoundReserveID = true;
                            }
                            if ((iCurrentReserveID != objFundsAutoSplit.ReserveTypeCode) && (iLastReserveID == 0))
                            {
                                iLastReserveID = objFundsAutoSplit.ReserveTypeCode;
                                bSetReserveID = true;
                            }
                        }
                    }
                    if ((iCurrentReserveID == 0) && (iLastReserveID != 0))
                    {
                        bSetReserveID = true;
                    }
                    
                    if ((!bFoundReserveID) && (bSetReserveID == true))
                    {
                        base.ResetSysExData("ReserveID", iLastReserveID.ToString());                       
                        base.ResetSysExData("ReserveIDDesc", GetReserveIDDesc(iLastReserveID));
                        
         
                    }
                }
				
                // Add an Extra Funds-Auto-Split Node for new data grid binding. 
                this.XmlForFundsAutoSplit(objFuturePaymentsElement, objFundsAutoSplitTemp, ++iFuturePaymentCount, FirstPayment(m_objNonThiredPartyPaymentList));	
			
				// Reset the Grid Flags.
				base.ResetSysExData("FuturePaymentSelectedId",""); 
				base.ResetSysExData("FuturePaymentsGrid_RowDeletedFlag","false"); 
				base.ResetSysExData("FuturePaymentsGrid_RowAddedFlag", "false");
				base.ResetSysExData("FuturePaymentsGrid_RowEditFlag", "false");
			}
			finally
			{
				objFuturePaymentsElement = null ;
				objListHeadXmlElement = null ;
				objFundsAutoSplitTemp = null ;
			}
		}

		#endregion 

		#region Appened Data for Thired Party Funds-Auto-Paymnet Collection.
		/*
		 * Vaibhav Kaushik 10 Feb 2006
		 * This function is used to serialize the FundsAuto Object for *****Thired Party Payments***** Only.
		 * 
		 * It take care ihe case if Funds-Auto object is a ****NEW**** Thired Party Payment.
		 * 
		 * We use this function to serialize the Xml in place of DataModel serialize function, cause we need the 
		 * data in a specific format to disply the content using the *****Grid control***** of xforms-control.xsl .
		 * 
		 * 
		 * Vaibhav Kaushik 11 Feb 2006 
		 * 
			We are passing the NonThiredPartyPaymentList only and only to calculate "Deduct Against Payee Flag".
			This Flag does not have the direct Database property and caculated by looking at the NonThiredPartyPaymentList
				
			NonThiredPartyPaymentList MUST not be used except caculating "Deduct Against Payee Flag".
				
			For all other purposes, objThiredPartyPaymentList MUST be used.
		 */ 
		private void XmlForFundsAuto( XmlElement p_objRootElement , FundsAuto p_objFundsAuto , int p_iCount , SortedList p_objNonThiredPartyPaymentList , bool p_bIsNew )
		{
			XmlElement objOptionXmlElement = null;
			XmlElement objXmlElement = null;
			FundsAutoSplit objFundsAutoSplit = null;
				
			string sTransTypeCodeDescription = string.Empty; 
			string sReserveTypeCodeDescription = string.Empty ;
			string sGlAccountCodeDescription = string.Empty ;
			string sPayeeEidDescription = string.Empty ;
			string sStateIdDescription = string.Empty ;
			string sDeductAgainstPayeeFlag = string.Empty ;
			string bDeductAgainstPayeeFlag = string.Empty ;
            Policy objPolicy = null;
            int iResult = 0;
            int iCoverageId = 0;
            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;
            int iCoveTypeCode = 0;
            XmlNode objCoverageType = null;
            string sCovgSeqNum = string.Empty;  //Ankit : Policy System Interface
			try
			{
				if( p_objFundsAuto.AutoSplitList.Count > 0 )
				{
					foreach( FundsAutoSplit objFundsAutoSplitTemp in p_objFundsAuto.AutoSplitList )
					{
						// Thired party payments have only a single split entry, Get the first one.
						objFundsAutoSplit = objFundsAutoSplitTemp ;
						break ;
					}
					if( objFundsAutoSplit.AutoSplitId != 0 )
					{
                        //Change by kuladeep for mits:33564 Start
						//sTransTypeCodeDescription = this.objCache.GetCodeDesc(objFundsAutoSplit.TransTypeCode);
						//sReserveTypeCodeDescription = this.objCache.GetCodeDesc(objFundsAutoSplit.ReserveTypeCode);
						//sGlAccountCodeDescription = this.objCache.GetCodeDesc(objFundsAutoSplit.GlAccountCode);

                        sTransTypeCodeDescription = this.objCache.GetCodeDesc(objFundsAutoSplit.TransTypeCode, base.Adaptor.userLogin.objUser.NlsCode);
                        sReserveTypeCodeDescription = this.objCache.GetCodeDesc(objFundsAutoSplit.ReserveTypeCode, base.Adaptor.userLogin.objUser.NlsCode);
                        sGlAccountCodeDescription = this.objCache.GetCodeDesc(objFundsAutoSplit.GlAccountCode, base.Adaptor.userLogin.objUser.NlsCode);
                        //Change by kuladeep for mits:33564 Start
					}
				}
				else
				{
					objFundsAutoSplit = ( FundsAutoSplit ) this.FundsAutoBatch.Context.Factory.GetDataModelObject( "FundsAutoSplit", false );
					if( p_bIsNew )
						objFundsAutoSplit.Tag = this.GetNextTagValue( p_objNonThiredPartyPaymentList );
					p_objFundsAuto.AutoSplitList.Add( objFundsAutoSplit );
				}
					
				if( p_objFundsAuto.PayeeEid != 0 )
					sPayeeEidDescription = this.objCache.GetCodeDesc(p_objFundsAuto.PayeeEid);
                string sStateCode = string.Empty;
                if (p_objFundsAuto.StateId != 0)
                //sStateIdDescription = this.objCache.GetCodeDesc(p_objFundsAuto.StateId );
                //MITS 16415 : Umesh
                {
                    this.objCache.GetStateInfo(p_objFundsAuto.StateId, ref sStateCode, ref sStateIdDescription);
                    sStateIdDescription = sStateCode + " " + sStateIdDescription;
                }
                //MITS 16415 End
				this.CreateElement( p_objRootElement , "option" , ref objOptionXmlElement );
				objOptionXmlElement.SetAttribute( "ref" , INSTANCE_SYSEXDATA_PATH + "/" + objOptionXmlElement.ParentNode.LocalName  + "/option[" + p_iCount.ToString() + "]" );
				if( p_objFundsAuto.AutoTransId == 0 )
					objOptionXmlElement.SetAttribute( "type" , "new" );

				this.CreateAndSetElement( objOptionXmlElement , "LastName" , p_objFundsAuto.LastName );
				this.CreateAndSetElement( objOptionXmlElement , "FirstName" , p_objFundsAuto.FirstName );
				this.CreateAndSetElement( objOptionXmlElement , "PercentNumber" , p_objFundsAuto.PercentNumber.ToString() );
                //Deb Multi Currency
				//this.CreateAndSetElement( objOptionXmlElement , "Amount" , objFundsAutoSplit.Amount.ToString() );
                if (this.FundsAutoBatch.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                {
                    if (p_objFundsAuto.PmtCurrencyType > 0)
                    {
                        this.CreateAndSetElement(objOptionXmlElement, "Amount", objFundsAutoSplit.PmtCurrencyAmount.ToString("c"), CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.PmtCurrencyType, objFundsAutoSplit.PmtCurrencyAmount, m_sConnectionString, base.ClientId), "CurrencyValue");
                    }
                    else
                    {
                        this.CreateAndSetElement(objOptionXmlElement, "Amount", objFundsAutoSplit.Amount.ToString("c"), CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, objFundsAutoSplit.Amount, m_sConnectionString, base.ClientId), "CurrencyValue");
                    }
                }
                else
                {
                    this.CreateAndSetElement(objOptionXmlElement, "Amount", objFundsAutoSplit.Amount.ToString("c"), CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, objFundsAutoSplit.Amount, m_sConnectionString, base.ClientId), "CurrencyValue");
                }
                //Deb Multi Currency
                //BOB
                bCarrierClaims = Conversion.ConvertStrToBool(objFundsAutoSplit.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString());
                /*
                if (bCarrierClaims)
                {
                    iCoverageId = objFundsAutoSplit.CoverageId;
                    iResult = GetPolicyIDOrCoverageCode(iCoverageId, out iCoveTypeCode);
                    objPolicy = (Policy)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Policy", false);
                    objPolicy.MoveTo(iResult);
                    this.CreateAndSetElement(objOptionXmlElement, "PolicyID", objPolicy.PolicyName.ToString(), objPolicy.PolicyId.ToString(), "value");
                    this.objCache.GetCodeInfo(iCoveTypeCode, ref sShortCode, ref sCodeDesc);
                    this.CreateAndSetElement(objOptionXmlElement, "CoverageTypeCode", sShortCode + " " + sCodeDesc, iCoveTypeCode.ToString(), "codeid");
                    if (objOptionXmlElement.SelectNodes("//CoverageTypeCode").Count > 1)
                    {
                        objCoverageType = objOptionXmlElement.SelectNodes("//CoverageTypeCode").Item(1);
                    }
                    else
                    {
                        objCoverageType = objOptionXmlElement.SelectSingleNode("//CoverageTypeCode");
                    }
                    XmlAttribute tabAttr = base.SysEx.CreateAttribute("tablename");
                    tabAttr.Value = "COVERAGE_TYPE";
                    objCoverageType.Attributes.Append(tabAttr);
                }*/
                //rupal:start, r8 unit implementation
                if (bCarrierClaims)
                {
                    long lPolicyUnitRowId;
                    int iPolicyID;
                    //Mona: Maintaining relation b/w FUNDS_TRANS_SPLIT and RESERVE_CURRENT through RC_ROW_ID 
                    //iCoverageId = objFundsAutoSplit.CoverageId;
                    iCoverageId = CommonFunctions.GetCovRowIdFromRCRowId(FundsAutoBatch.Context.DbConn.ConnectionString, objFundsAutoSplit.RCRowId, base.ClientId);
                    GetPolicyDetails(iCoverageId, out iPolicyID, out lPolicyUnitRowId, out iCoveTypeCode, out sCovgSeqNum);
                    objPolicy = (Policy)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Policy", false);
                    objPolicy.MoveTo(iPolicyID);

                    this.CreateAndSetElement(objOptionXmlElement, "PolicyID", objPolicy.PolicyName.ToString(), objPolicy.PolicyId.ToString(), "value");
                    this.CreateAndSetElement(objOptionXmlElement, "UnitID", GetSelectedUnit(objPolicy.PolicyId, iCoverageId, lPolicyUnitRowId), lPolicyUnitRowId.ToString(), "value");

                    this.CreateAndSetElement(objOptionXmlElement, "CovgSeqNum", sCovgSeqNum, sCovgSeqNum, "value");

                    this.objCache.GetCodeInfo(iCoveTypeCode, ref sShortCode, ref sCodeDesc, base.Adaptor.userLogin.objUser.NlsCode); //Aman Ml Change
                    if (objPolicy.PolicySystemId > 0)
                        this.CreateAndSetElement(objOptionXmlElement, "CoverageTypeCode", GetCoverageText(Convert.ToInt32(lPolicyUnitRowId), iCoverageId, iCoveTypeCode), iCoveTypeCode.ToString(), "codeid");
                    else
                    this.CreateAndSetElement(objOptionXmlElement, "CoverageTypeCode", sShortCode + " " + sCodeDesc, iCoveTypeCode.ToString(), "codeid");

                    if (objOptionXmlElement.SelectNodes("//CoverageTypeCode").Count > 1)
                    {
                        objCoverageType = objOptionXmlElement.SelectNodes("//CoverageTypeCode").Item(1);
                    }
                    else
                    {
                        objCoverageType = objOptionXmlElement.SelectSingleNode("//CoverageTypeCode");
                    }
                    XmlAttribute tabAttr = base.SysEx.CreateAttribute("tablename");
                    tabAttr.Value = "COVERAGE_TYPE";
                    objCoverageType.Attributes.Append(tabAttr);
                    string sLossDesc = string.Empty;
                    string sLossCode = string.Empty;
                    int iDisabilityCat = 0;
                    int iCvgLossId = 0;

                    int iLossId = CommonFunctions.GetLossType(m_sConnectionString, objFundsAutoSplit.RCRowId, ref iDisabilityCat, ref  iCvgLossId, base.ClientId);

                    this.CreateAndSetElement(objOptionXmlElement, "RCRowId", objFundsAutoSplit.RCRowId.ToString());

                    objFundsAutoSplit.Context.LocalCache.GetCodeInfo(iLossId, ref sLossCode, ref sLossDesc, base.Adaptor.userLogin.objUser.NlsCode); //Aman Ml Change
                    Claim objClaim = (Claim)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Claim", false);

                    if (Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimId").InnerText) > 0)
                    {
                        objClaim.MoveTo(Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimId").InnerText));
                        //    if(
                        if (objClaim.LineOfBusCode == 243)
                        {
                            objCache.GetCodeInfo(objCache.GetRelatedCodeId(iLossId), ref sShortCode, ref sCodeDesc, base.Adaptor.userLogin.objUser.NlsCode);
                            this.CreateAndSetElement(objOptionXmlElement, "DisabilityCatCode", sShortCode + " " + sCodeDesc, ref objXmlElement);
                            objXmlElement.SetAttribute("codeid", objFundsAutoSplit.Context.LocalCache.GetRelatedCodeId(iLossId).ToString());
                            //MITS 31466 : appended sLossCode with sLossDesc
                            this.CreateAndSetElement(objOptionXmlElement, "DisabilityTypeCode", sLossCode+" "+ sLossDesc, ref objXmlElement);
                            objXmlElement.SetAttribute("codeid", iLossId.ToString());
                        }
                        else
                        {
                            //MITS 31466 : appended sLossCode with sLossDesc
                            this.CreateAndSetElement(objOptionXmlElement, "LossTypeCode", sLossCode+" "+ sLossDesc, ref objXmlElement);
                            objXmlElement.SetAttribute("codeid", iLossId.ToString());
                        }
                    }

                    //tanwar2 - mits 30910 - start
                    //Start -  Changed by Nikhil.
                    //       this.CreateAndSetElement(objOptionXmlElement, "IsOverrideDedProcessing", Convert.ToString(objFundsAutoSplit.IsOverrideDedProcessing));
                 
                    if (objClaim.LineOfBusCode > 0)
                    {
                      
                        if (objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ApplyDedToPaymentsFlag == -1 && objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface )
                        {
                            this.CreateAndSetElement(objOptionXmlElement, "IsOverrideDedProcessing", Convert.ToString(objFundsAutoSplit.IsOverrideDedProcessing));
                        }

                    }

                    //end -  Changed by Nikhil.


                    //tanwar2 - mits 30910 - end

                    objClaim = null;
                }
                //rupal:end
	
				this.CreateAndSetElement( objOptionXmlElement , "TransTypeCode" , sTransTypeCodeDescription , ref objXmlElement );
				objXmlElement.SetAttribute( "codeid" , objFundsAutoSplit.TransTypeCode.ToString() );

				this.CreateAndSetElement( objOptionXmlElement , "ReserveTypeCode" , sReserveTypeCodeDescription , ref objXmlElement );
				objXmlElement.SetAttribute( "codeid" , objFundsAutoSplit.ReserveTypeCode.ToString() );
					
				/******************* Hidden Grid Columns ************************/
				this.CreateAndSetElement( objOptionXmlElement , "AutoSplitId" , objFundsAutoSplit.AutoSplitId.ToString() );

                this.CreateAndSetElement(objOptionXmlElement, "PayeeEid", p_objFundsAuto.PayeeEid.ToString(), ref objXmlElement);
                //this.CreateAndSetElement(objOptionXmlElement, "PayeeEid", sPayeeEidDescription, ref objXmlElement);
                //objXmlElement.SetAttribute( "codeid" , p_objFundsAuto.PayeeEid.ToString() );
					
				this.CreateAndSetElement( objOptionXmlElement , "MiddleName" , p_objFundsAuto.MiddleName );
				this.CreateAndSetElement( objOptionXmlElement , "TaxId" , p_objFundsAuto.PayeeEntity.TaxId );
				this.CreateAndSetElement( objOptionXmlElement , "Addr1" , p_objFundsAuto.Addr1 );
				this.CreateAndSetElement( objOptionXmlElement , "Addr2" , p_objFundsAuto.Addr2 );
                this.CreateAndSetElement(objOptionXmlElement, "Addr3", p_objFundsAuto.Addr3);// JIRA 6420 pkandhari
                this.CreateAndSetElement(objOptionXmlElement, "Addr4", p_objFundsAuto.Addr4);// JIRA 6420 pkandhari
				this.CreateAndSetElement( objOptionXmlElement , "City" , p_objFundsAuto.City );

                //this.CreateAndSetElement(objOptionXmlElement, "StateId", p_objFundsAuto.StateId.ToString(), ref objXmlElement);
                this.CreateAndSetElement(objOptionXmlElement, "StateId", sStateIdDescription, ref objXmlElement);
                objXmlElement.SetAttribute("codeid", p_objFundsAuto.StateId.ToString());
					
				this.CreateAndSetElement( objOptionXmlElement , "ZipCode" , p_objFundsAuto.ZipCode );
				this.CreateAndSetElement( objOptionXmlElement , "CheckMemo" , p_objFundsAuto.CheckMemo );
				this.CreateAndSetElement( objOptionXmlElement , "EnclosureFlag" , p_objFundsAuto.EnclosureFlag.ToString() );
				this.CreateAndSetElement( objOptionXmlElement , "AutoTransId" , p_objFundsAuto.AutoTransId.ToString() );
					
				// There is no field in Database for 'DeductAgainstPayeeFlag'. The only way to get this
				// value is to determine is there is any Payee Split with same 'Tag' as this Tpp Split.	
				if( !p_bIsNew )
					sDeductAgainstPayeeFlag = this.DeductAgainstPayee( objFundsAutoSplit.Tag , p_objNonThiredPartyPaymentList ) ? "True" : "False" ;
				else
					sDeductAgainstPayeeFlag = "True" ;

				this.CreateAndSetElement( objOptionXmlElement , "DeductAgainstPayeeFlag" , sDeductAgainstPayeeFlag );					
				this.CreateAndSetElement( objOptionXmlElement , "FromDate" , Conversion.GetDBDateFormat( objFundsAutoSplit.FromDate, "d" ) );
				this.CreateAndSetElement( objOptionXmlElement , "ToDate" , Conversion.GetDBDateFormat( objFundsAutoSplit.ToDate, "d" ) );
                //Deb Multi Currency	
                //this.CreateAndSetElement( objOptionXmlElement , "InvoiceAmount" , objFundsAutoSplit.InvoiceAmount.ToString() );
                if (p_objFundsAuto.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                {
                    if (p_objFundsAuto.PmtCurrencyType > 0)
                    { 
                        this.CreateAndSetElement(objOptionXmlElement, "InvoiceAmount", objFundsAutoSplit.PmtCurrencyInvAmount.ToString("c"), CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.PmtCurrencyType, objFundsAutoSplit.PmtCurrencyInvAmount, m_sConnectionString, base.ClientId), "CurrencyValue");
                    }
                    else
                    {
                        this.CreateAndSetElement(objOptionXmlElement, "InvoiceAmount", objFundsAutoSplit.InvoiceAmount.ToString("c"), CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, objFundsAutoSplit.InvoiceAmount, m_sConnectionString, base.ClientId), "CurrencyValue");
                    }
                } 
                else
                {
                    this.CreateAndSetElement(objOptionXmlElement, "InvoiceAmount", objFundsAutoSplit.InvoiceAmount.ToString("c"), CommonFunctions.ConvertByCurrencyCode(p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType, objFundsAutoSplit.InvoiceAmount, m_sConnectionString, base.ClientId), "CurrencyValue");
                }
                //Deb Multi Currency
					
               
				this.CreateAndSetElement( objOptionXmlElement , "GlAccountCode" , sGlAccountCodeDescription , ref objXmlElement );
				objXmlElement.SetAttribute( "codeid" , objFundsAutoSplit.GlAccountCode.ToString() );
					
				this.CreateAndSetElement( objOptionXmlElement , "InvoicedBy" , objFundsAutoSplit.InvoicedBy );
				this.CreateAndSetElement( objOptionXmlElement , "InvoiceDate" , Conversion.GetDBDateFormat( objFundsAutoSplit.InvoiceDate, "d" ) );
				this.CreateAndSetElement( objOptionXmlElement , "InvoiceNumber" , objFundsAutoSplit.InvoiceNumber.ToString() );
				this.CreateAndSetElement( objOptionXmlElement , "PoNumber" , objFundsAutoSplit.PoNumber.ToString() );
				this.CreateAndSetElement( objOptionXmlElement , "Tag" , objFundsAutoSplit.Tag.ToString() );
                //Added by amitosh for EFT
                if (CommonFunctions.hasEFTBankInfo(p_objFundsAuto.PayeeEid, objCache.GetCodeId("A", "EFT_BANKING_STATUS"), m_sConnectionString))
                {
                    this.CreateAndSetElement(objOptionXmlElement, "hasEntityEFtbankInfo", "true");
                }
                else
                {
                    this.CreateAndSetElement(objOptionXmlElement, "hasEntityEFtbankInfo", "false");
                }
                //End Amitosh
                //Deb Multi Currency
                if (p_objFundsAuto.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                {
                    if (p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType > 0)
                    {
                        sBaseCurrCulture = p_objFundsAuto.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType);
                        this.CreateAndSetElement(objOptionXmlElement, "BaseCurrencyType", sBaseCurrCulture.Split('|')[1]);
                        this.CreateAndSetElement(objOptionXmlElement, "UseMultiCurrency", Boolean.TrueString);
                    }
                    else
                    {
                        Errors.Add(Globalization.GetString("BaseCurrency.Error", base.ClientId), "Please set the Base Currency in the Utilities", BusinessAdaptorErrorType.Error);
                    }
                }
                else
                {
                    if (p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType > 0)
                    {
                        sBaseCurrCulture = p_objFundsAuto.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + p_objFundsAuto.Context.InternalSettings.SysSettings.BaseCurrencyType);
                        this.CreateAndSetElement(objOptionXmlElement, "BaseCurrencyType", sBaseCurrCulture.Split('|')[1]);
                        this.CreateAndSetElement(objOptionXmlElement, "UseMultiCurrency", Boolean.FalseString);
                    }
                }
                //Deb Multi Currency
			}

			finally
			{
				objOptionXmlElement = null;
				objXmlElement = null;	
				objFundsAutoSplit = null;
			}			
		}

        //rupal:start. r8 changes for Unit
        private int GetPolicyDetails(int iCoverageId, out int iPolicyID, out long lPolUnitRowId, out int iCoveTypeCode, out string sCovgSeqNum)
        {
            int iResult = 0;
            DbReader objReader = null;
            DbConnection objConn = null;
            string sSql = string.Empty;
            iCoveTypeCode = 0;
            iPolicyID = 0;
            lPolUnitRowId = 0;
            sCovgSeqNum = string.Empty;
            try
            {
                //Ankit : Start Policy System Interface
                //sSql = string.Format("SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID,COVERAGE_TYPE_CODE , POLICY_X_UNIT.POLICY_ID FROM POLICY_X_CVG_TYPE , POLICY_X_UNIT WHERE POLCVG_ROW_ID = {0} AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID", iCoverageId);
                sSql = string.Format("SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID,COVERAGE_TYPE_CODE , POLICY_X_UNIT.POLICY_ID, POLICY_X_CVG_TYPE.CVG_SEQUENCE_NO FROM POLICY_X_CVG_TYPE , POLICY_X_UNIT WHERE POLCVG_ROW_ID = {0} AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID", iCoverageId);
                //Ankit : End

                if (iCoverageId != 0)
                {
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                    if (objReader.Read())
                    {
                        lPolUnitRowId = objReader.GetInt32(0);
                        iCoveTypeCode = objReader.GetInt32(1);
                        iPolicyID = objReader.GetInt32(2);
                        sCovgSeqNum = Conversion.ConvertObjToStr(objReader.GetValue(3));
                    }
                    objReader.Close();
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("AutoClaimChecksForm.CheckClaimTypeChangeOption.Error", base.ClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
            return iResult;
        }

        private void Initialize()
        {
            try
            {
                m_sDsnName = base.m_fda.userLogin.objRiskmasterDatabase.DataSourceName;
                m_sUserName = base.m_fda.userLogin.LoginName;
                m_sPassword = base.m_fda.userLogin.Password;
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, base.ClientId);
                m_sConnectionString = base.m_fda.connectionString;
               
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AutoClaimChecksForm.Initialize.ErrorInit", base.ClientId), p_objEx);//rkaur27
            }
        }

        /// <summary>
        /// Get the list of units attached to Policy for a Claim.
        /// </summary>
        /// <param name="objXmlDoc">XmlDocument Object</param>
        public string GetSelectedUnit(long lPolicyId, long lUnitCvgRowId, long lPolicyUnitRowId)
        {
            
            string sUnit = "";
            StringBuilder sbSQL;
            string sDBType = string.Empty;
            SysSettings objSysSetting = new SysSettings(m_sConnectionString, ClientId); //Ash - cloud

            try
            {
                //rupal:start, policy system interface changes,
                //formatted the query and appended joins in the query for site unit and other unit
                sbSQL = new StringBuilder();

                //changes for MITS 31997 start
                if (m_objDataModelFactory == null)
                    this.Initialize();
                sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString();
                //changes for MITS 31997 end

                if (objSysSetting.MultiCovgPerClm == -1 )
                {
                    if( sDBType == Constants.DB_SQLSRVR)
                    sbSQL = sbSQL.Append("SELECT  ISNULL(VEHICLE.VEH_DESC,VEHICLE.VIN) Unit");
                    else if (sDBType == Constants.DB_ORACLE)
                    sbSQL = sbSQL.Append("SELECT  nvl(VEHICLE.VEH_DESC,VEHICLE.VIN) Unit");
                }
                else
                {
                      sbSQL = sbSQL.Append("SELECT  VEHICLE.VIN Unit");
                }

                sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT");
                sbSQL = sbSQL.Append(" INNER JOIN VEHICLE ON VEHICLE.UNIT_ID = POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE = 'V' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString() + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN RESERVE_CURRENT ON RESERVE_CURRENT.POLCVG_ROW_ID = " + lUnitCvgRowId.ToString());
                
                sbSQL = sbSQL.Append(" UNION");
                sbSQL = sbSQL.Append(" SELECT  PROPERTY_UNIT.PIN Unit");
                sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT");
                sbSQL = sbSQL.Append(" INNER JOIN PROPERTY_UNIT ON PROPERTY_UNIT.PROPERTY_ID = POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='P' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString() + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN RESERVE_CURRENT ON RESERVE_CURRENT.POLCVG_ROW_ID = " + lUnitCvgRowId.ToString());

                // as of now site and other unit will be available for policy system interface only                        

                sbSQL = sbSQL.Append(" UNION");

                //SITE UNIT - S
                sbSQL = sbSQL.Append(" SELECT  SITE_UNIT.NAME Unit");
                sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT ");
                sbSQL = sbSQL.Append(" INNER JOIN SITE_UNIT ON SITE_UNIT.SITE_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='S' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString() + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN RESERVE_CURRENT ON RESERVE_CURRENT.POLCVG_ROW_ID = " + lUnitCvgRowId.ToString());

                //OTHER UNIT - SU
                sbSQL = sbSQL.Append(" UNION");
                //changes for MITS 31997 start
                if(sDBType ==Constants.DB_SQLSRVR)
                    //dbisht6 start for mits 35655
                    sbSQL = sbSQL.Append(" SELECT POINT_UNIT_DATA.STAT_UNIT_NUMBER + ' - ' + ISNULL(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((ISNULL(ENTITY.FIRST_NAME,'') + ' '+ ISNULL(ENTITY.MIDDLE_NAME,'') +' '+ ISNULL(ENTITY.LAST_NAME,''))))) Unit");
                // sbSQL = sbSQL.Append(" SELECT (ISNULL(ENTITY.FIRST_NAME,'') + ' '+ ISNULL(ENTITY.MIDDLE_NAME,'') +' '+ ISNULL(ENTITY.LAST_NAME,'')) Unit");
                //dbisht6 end

                else if (sDBType ==Constants.DB_ORACLE)
                    //dbisht6 start for mits 35655
                    sbSQL = sbSQL.Append(" SELECT POINT_UNIT_DATA.STAT_UNIT_NUMBER || ' - ' || nvl(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((nvl(ENTITY.FIRST_NAME,'') || ' '|| nvl(ENTITY.MIDDLE_NAME,'') ||' '|| nvl(ENTITY.LAST_NAME,''))))) Unit");
                //dbisht6 end
                //changes for MITS 31997 end
                sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT ");
                sbSQL = sbSQL.Append(" INNER JOIN OTHER_UNIT ON OTHER_UNIT.OTHER_UNIT_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='SU' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString() + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN ENTITY ON ENTITY.ENTITY_ID=OTHER_UNIT.ENTITY_ID AND OTHER_UNIT.UNIT_TYPE='SU'");
                //dbisht6 start mits of 35655
                sbSQL = sbSQL.Append(" INNER JOIN POINT_UNIT_DATA ON	OTHER_UNIT.OTHER_UNIT_ID = POINT_UNIT_DATA.UNIT_ID and POINT_UNIT_DATA.UNIT_TYPE = OTHER_UNIT.UNIT_TYPE ");
                //dbisht6 end

                sbSQL = sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN RESERVE_CURRENT ON RESERVE_CURRENT.POLCVG_ROW_ID = " + lUnitCvgRowId.ToString());

                //FORMATTED THIS QUERY ABOVE
                //sSql = string.Format("SELECT v.VIN UNIT FROM VEHICLE v , POLICY_X_UNIT p , RESERVE_CURRENT,POLICY_X_CVG_TYPE WHERE (p.UNIT_TYPE = 'V' AND v.UNIT_ID = p.UNIT_ID AND RESERVE_CURRENT.POLCVG_ROW_ID = {1} AND p.POLICY_UNIT_ROW_ID={2} AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = P.POLICY_UNIT_ROW_ID  AND p.POLICY_ID = {0} ) UNION SELECT DISTINCT u.PIN UNIT FROM PROPERTY_UNIT u , POLICY_X_UNIT p , RESERVE_CURRENT,POLICY_X_CVG_TYPE WHERE (p.UNIT_TYPE = 'P' AND u.PROPERTY_ID = p.UNIT_ID AND RESERVE_CURRENT.POLCVG_ROW_ID = {1} AND p.POLICY_UNIT_ROW_ID = {2} AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = P.POLICY_UNIT_ROW_ID  AND p.POLICY_ID ={0}) ", lPolicyId.ToString(), lUnitCvgRowId.ToString(), lPolicyUnitRowId.ToString());
                //RUPAL:END, POLICY SYSTEM INTERFACE CHANGES
                
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            sUnit = objReader.GetString("Unit");

                        }
                    }
                }
            }

            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CheckUtility.Error", base.ClientId), p_objEx);
            }
            finally
            {
                sbSQL = null;
                objSysSetting = null;
            }
            return sUnit;

        }
        //rupal:end. r8 changes for Unit

		private bool DeductAgainstPayee( int p_iTag , SortedList p_objNonThiredPartyPaymentList)
		{
			/* 
			 * Looking the Non-Thired-Party Payment collection, for the same Tag number. 
			 */

			if( p_objNonThiredPartyPaymentList.Count <= 0 )
				return false ;

			foreach( FundsAutoSplit objFundsAutoSplit in FirstPayment( p_objNonThiredPartyPaymentList ).AutoSplitList )
				if( objFundsAutoSplit.Tag == p_iTag ) 
					return(true) ;
			return false ;			
		}
		

		private int GetNextTagValue( SortedList p_objNonThiredPartyPaymentList )
		{
			int iMaxTagValue = 1 ;
			
			if( p_objNonThiredPartyPaymentList.Count <= 0 )
				return( iMaxTagValue ) ;

			foreach( FundsAutoSplit objFundsAutoSplit in FirstPayment( p_objNonThiredPartyPaymentList ).AutoSplitList )
				if( objFundsAutoSplit.Tag >= iMaxTagValue ) 
					iMaxTagValue = objFundsAutoSplit.Tag + 1 ;
			return(iMaxTagValue);	
		}


		/* This method creates data for Third Party Payments Grid.
			 * (Essentially each grid row has the FundsAutoSplit and FundsAuto data for a Third Party Payment)
			 * <ThirdPartyPayments>
			 *		<listhead>
			 *			<LastName/>
			 *			.
			 *			.		
			 *		</listhead>
			 *		<option ref="/Instance/UI/FormVariables/SysExData/ThirdPartyPayments/option[0]">
			 *			<LastName/>
			 *			.
			 *			. 
			 *		</option>
			 *			.
			 *			.
			 *			.
			 *			.
			 *		<option ref="/Instance/UI/FormVariables/SysExData/ThirdPartyPayments/option[n]" type="new" >
			 *			<LastName/>
			 *			.
			 *			. 
			 *		</option>
			 * </ThirdPartyPayments>
			 * 
			 * Nodes under <listhead> are the Grid headers. Grid rows are represented by <option> tags.
			 * Since this grid is editable, we need to provide 'ref' attributes for each row, that are picked
			 * by the xsl to render controls for each element of grid bound to the specific node under
			 * <option>. An extra <option> node is provided for capturing data for a new grid row.
			 * 

				Ideally, we would like to serialize each FundsAutoSplit and FundsAuto object using DataModel, 
				but the current grid implementation in xforms-controls.xsl picks up columns for display
				as they are available in the <option> tag. Need to enhance the xsl for the Grid
				so that it picks up specified columns only, and that too in order.		
								
				Ideally, we would like to serialize this object using DataModel, but the
				current grid implementation in xforms-controls.xsl picks up columns for display
				as they are available in the <option> tag. Need to enhance the xsl for the Grid
				so that it picks up specified columns only, and that too in order.										
									
				objXmlElement = objThirdPartyPaymentsXmlDoc.CreateElement("PayeeName");
				objXmlElement.InnerText = objFundsAuto.PayeeEntity.GetLastFirstName();
				objOptionXmlElement.AppendChild(objXmlElement); 
				objXmlElement = null;
				
				Vaibhav Kaushik 10 Feb 2006
				This function has been re-written to use the "XmlForFundsAuto()" function to serialize the Object.
							
				Vaibhav Kaushik 11 Feb 2006 
				We are passing the NonThiredPartyPaymentList only and only to calculate "Deduct Against Payee Flag".
				This Flag does not have the direct Database property and caculated by looking at the NonThiredPartyPaymentList
				 
				NonThiredPartyPaymentList MUST not be used except caculating "Deduct Against Payee Flag".
				 
				For all other purposes, objThiredPartyPaymentList MUST be used.
				
			*/
		private void AppenedThirdPartyPaymentsGridData( SortedList p_objThiredPartyPaymentList , SortedList p_objNonThiredPartyPaymentList )
		{
			XmlElement objThirdPartyPaymentsElement = null ;
			XmlElement objListHeadXmlElement = null ;
			FundsAuto objFundsAutoTemp = null ;
			
			int iPaymentRecordCount = 0 ;
            //rsharma220 MITS 32723 start
            string sSQL = string.Empty;
            int iLobCode = 0;
            object oClaim = null;
            bool bClaimFound = false;
            //rsharma220 MITS 32723 end
			
			try
			{
                objFundsAutoTemp = (FundsAuto)this.FundsAutoBatch.Context.Factory.GetDataModelObject("FundsAuto", false);
                bCarrierClaims = Conversion.ConvertStrToBool(objFundsAutoTemp.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString());

				objThirdPartyPaymentsElement = (XmlElement)base.SysEx.SelectSingleNode( "/SysExData/ThirdPartyPayments");
				if( objThirdPartyPaymentsElement != null )
					objThirdPartyPaymentsElement.ParentNode.RemoveChild( objThirdPartyPaymentsElement );
				this.CreateElement( base.SysEx.DocumentElement , "ThirdPartyPayments" , ref objThirdPartyPaymentsElement );			
			
				// Create HEADER nodes for Grid.
				this.CreateElement( objThirdPartyPaymentsElement , "listhead" , ref objListHeadXmlElement );
				this.CreateAndSetElement( objListHeadXmlElement , "LastName" , "Last Name" );
				this.CreateAndSetElement( objListHeadXmlElement , "FirstName" , "First Name" );
				this.CreateAndSetElement( objListHeadXmlElement , "PercentNumber" , "Percent" );
				this.CreateAndSetElement( objListHeadXmlElement , "Amount" , "Amount" );
                this.CreateAndSetElement(objListHeadXmlElement, "TransTypeCode", "Transaction Type");
                this.CreateAndSetElement(objListHeadXmlElement, "ReserveTypeCode", "Reserve Type");

                //BOB
                if (bCarrierClaims)
                {
                    this.CreateAndSetElement(objListHeadXmlElement, "PolicyID", "Policy");
                    this.CreateAndSetElement(objListHeadXmlElement, "UnitID", "Unit");
                    this.CreateAndSetElement(objListHeadXmlElement, "CoverageTypeCode", "Coverage Type");
                    Claim objClaim = (Claim)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Claim", false);
                    //rsharma220 MITS 32723 start
                    if (base.CurrentAction.ToString().Equals("Delete"))
                    {
                        if (!string.IsNullOrEmpty(base.SysEx.SelectSingleNode("//ThisClaimNumber").InnerText))
                        {

                           // int iLobCode = Conversion.ConvertObjToInt(this.FundsAutoBatch.Context.DbConn.ExecuteScalar("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_NUMBER='" + base.SysEx.SelectSingleNode("//ThisClaimNumber").InnerText.ToString() + "'"), base.ClientId);
                            //    if(
                            Dictionary<string, string> dictParams = new Dictionary<string, string>();
                            sSQL = String.Format("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_NUMBER={0}", "~CLAIMNUMBER~");
                            dictParams.Add("CLAIMNUMBER", base.SysEx.SelectSingleNode("//ThisClaimNumber").InnerText);
                            oClaim = DbFactory.ExecuteScalar(this.FundsAutoBatch.Context.DbConn.ConnectionString, sSQL, dictParams);
                            if (oClaim != null)
                            {
                                iLobCode = Conversion.CastToType<int>(oClaim.ToString(), out bClaimFound);

                                if (iLobCode == 243)
                                {
                                    //this.CreateAndSetElement(objListHeadXmlElement, "DisabilityCatCode", "Disability Category");
                                    this.CreateAndSetElement(objListHeadXmlElement, "DisabilityTypeCode", "Loss Type");
                                }
                                else
                                {

                                    this.CreateAndSetElement(objListHeadXmlElement, "LossTypeCode", "Loss Type");
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimId").InnerText) > 0)
                        {
                        objClaim.MoveTo(Conversion.ConvertStrToInteger(base.SysEx.SelectSingleNode("/SysExData/FundsAuto/ClaimId").InnerText));
                        //    if(
                        if (objClaim.LineOfBusCode == 243)
                        {
                            //this.CreateAndSetElement(objListHeadXmlElement, "DisabilityCatCode", "Disability Category");
                            this.CreateAndSetElement(objListHeadXmlElement, "DisabilityTypeCode", "Loss Type");
                        }
                        else
                        {

                            this.CreateAndSetElement(objListHeadXmlElement, "LossTypeCode", "Loss Type");
                        }
                        }
                    }
                    //rsharma220 MITS 32723 end
                    //tanwar2 - mits 30910 - start
                    //Start -  Changed by Nikhil.
                   // this.CreateAndSetElement(objListHeadXmlElement, "IsOverrideDedProcessing", "Override Deductible");
               
                    if (objClaim.LineOfBusCode > 0)
                    {
                        if (objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ApplyDedToPaymentsFlag == -1 && objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface )
                        {
                            this.CreateAndSetElement(objListHeadXmlElement, "IsOverrideDedProcessing", "Override Deductible");
                        }
                    }
                    //end -  Changed by Nikhil.

                    //tanwar2 - mits 30910 - end
                    objClaim = null;
                }

				for( int iIndex = 0 ; iIndex < p_objThiredPartyPaymentList.Count ; iIndex++ ) 
				{
					FundsAuto objFundsAuto = ( FundsAuto ) p_objThiredPartyPaymentList.GetByIndex( iIndex );
					if( objFundsAuto.PayNumber != this.FirstPayment( p_objThiredPartyPaymentList ).PayNumber )
						continue ;
									
					this.XmlForFundsAuto( objThirdPartyPaymentsElement , objFundsAuto , ++iPaymentRecordCount , p_objNonThiredPartyPaymentList , false );
				}

				// Add an Extra Funds-Auto Node for new data grid binding. 
								
				this.XmlForFundsAuto( objThirdPartyPaymentsElement , objFundsAutoTemp , ++iPaymentRecordCount , p_objNonThiredPartyPaymentList , true );	
			
				// Reset the Grid Flags.
				base.ResetSysExData("ThirdPartyPaymentSelectedId",""); 
				base.ResetSysExData("ThirdPartyPaymentsGrid_RowDeletedFlag", "false");
				base.ResetSysExData("ThirdPartyPaymentsGrid_RowAddedFlag", "false");
                
			}
			finally
			{
				objThirdPartyPaymentsElement = null ;
				objListHeadXmlElement = null ;
				objFundsAutoTemp = null ;
			}
			
		}

		#endregion 

		#region Appened Data for Payee Type Combobox List
        private void AppendPayeeTypeList(int p_iPayeeTypeCode, int p_iPayeeEid)
		{
			XmlDocument objSysExDataXmlDoc = base.SysEx;
			XmlNode objOldPayeeTypeListNode = null;
			XmlElement objNewPayeeTypeListNode = null;
			
			XmlElement objOptionXmlElement = null; 
			XmlAttribute objXmlAttribute = null;
			XmlCDataSection objCData = null;

			objOldPayeeTypeListNode = objSysExDataXmlDoc.SelectSingleNode("//PayeeTypeList");
			objNewPayeeTypeListNode = objSysExDataXmlDoc.CreateElement("PayeeTypeList");
			
			// Blank value for the combo box
			objOptionXmlElement = objSysExDataXmlDoc.CreateElement("option");
			objXmlAttribute = objSysExDataXmlDoc.CreateAttribute("value");
			objOptionXmlElement.Attributes.Append(objXmlAttribute);
			objNewPayeeTypeListNode.AppendChild(objOptionXmlElement);
			objXmlAttribute = null;
			objOptionXmlElement = null;

			objOptionXmlElement = objSysExDataXmlDoc.CreateElement("option");
			objCData = objSysExDataXmlDoc.CreateCDataSection("C-Claimant");
			objOptionXmlElement.AppendChild(objCData);			
			objXmlAttribute = objSysExDataXmlDoc.CreateAttribute("value");
			objXmlAttribute.Value = this.objCache.GetCodeId("C", "PAYEE_TYPE").ToString();  
			objOptionXmlElement.Attributes.Append(objXmlAttribute);
			objNewPayeeTypeListNode.AppendChild(objOptionXmlElement);
			objCData = null;
			objXmlAttribute = null;
			objOptionXmlElement = null;

			int iOtherPayeeTypeCodeId = this.objCache.GetCodeId("O", "PAYEE_TYPE");

            objOptionXmlElement = objSysExDataXmlDoc.CreateElement("option");
            objCData = objSysExDataXmlDoc.CreateCDataSection("O - Other Payees (People & Entities)");
            objOptionXmlElement.AppendChild(objCData);
            objXmlAttribute = objSysExDataXmlDoc.CreateAttribute("value");
            objXmlAttribute.Value = "e" + iOtherPayeeTypeCodeId.ToString();
            objOptionXmlElement.Attributes.Append(objXmlAttribute);
            objNewPayeeTypeListNode.AppendChild(objOptionXmlElement);
            objCData = null;
            objXmlAttribute = null;
            objOptionXmlElement = null;
            //avipinsrivas Start : Worked for Issue 4634 - Epic 340
            //Bkuzhanthaim : MITS-36026/RMA-338: add OrgHierarchy to Payee Type dropdown if "AddOrgHierarchyAsPayeeType" is true or a fund is already saved with payee type as "OrgHierarchy"
            //string sSQL = "SELECT GLOSSARY.GLOSSARY_TYPE_CODE FROM ENTITY, GLOSSARY WHERE ENTITY.ENTITY_ID="
            //            + p_iPayeeEid + " AND ENTITY.ENTITY_TABLE_ID=GLOSSARY.TABLE_ID";
            //int iTableType = this.FundsAutoBatch.Context.DbConnLookup.ExecuteInt(sSQL);
            iOtherPayeeTypeCodeId = this.objCache.GetCodeId("H", "PAYEE_TYPE");
            if (this.objData.Context.InternalSettings.SysSettings.AddOrgHierarchyAsPayeeType || (p_iPayeeEid != 0 && p_iPayeeTypeCode == iOtherPayeeTypeCodeId))
            {
                objOptionXmlElement = objSysExDataXmlDoc.CreateElement("option");
                objCData = objSysExDataXmlDoc.CreateCDataSection("O - Other Payees (Org. Hierarchy)");
                objOptionXmlElement.AppendChild(objCData);
                objXmlAttribute = objSysExDataXmlDoc.CreateAttribute("value");
                objXmlAttribute.Value = "o" + iOtherPayeeTypeCodeId.ToString();
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                objNewPayeeTypeListNode.AppendChild(objOptionXmlElement);
                objCData = null;
                objXmlAttribute = null;
                objOptionXmlElement = null;
            }
            //rupal:r8 enh to add person involved as payee type   
            //add Person Involved to Payee Type dropdown if "AddPersonInvolvedAsPayeeType" is true or a fund is already saved with payee type as "Person Involved"
            int iPIPayeeTypeCodeId = this.objCache.GetCodeId("P", "PAYEE_TYPE");
            if (this.objData.Context.InternalSettings.SysSettings.AddPersonInvolvedAsPayeeType || (p_iPayeeEid != 0 && p_iPayeeTypeCode == iPIPayeeTypeCodeId))
            {
                objOptionXmlElement = objSysExDataXmlDoc.CreateElement("option");
                objCData = objSysExDataXmlDoc.CreateCDataSection("P - Person Involved");
                objOptionXmlElement.AppendChild(objCData);
                objXmlAttribute = objSysExDataXmlDoc.CreateAttribute("value");
                objXmlAttribute.Value = iPIPayeeTypeCodeId.ToString();
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                objNewPayeeTypeListNode.AppendChild(objOptionXmlElement);
                objCData = null;
                objXmlAttribute = null;
                objOptionXmlElement = null;
            }
            //rupal:end
			if(objOldPayeeTypeListNode !=null)
				objSysExDataXmlDoc.DocumentElement.ReplaceChild(objNewPayeeTypeListNode,objOldPayeeTypeListNode);
			else
				objSysExDataXmlDoc.DocumentElement.AppendChild(objNewPayeeTypeListNode);

			//Clean up
			objOldPayeeTypeListNode = null;
			objNewPayeeTypeListNode = null;
			objCData = null;	
			objSysExDataXmlDoc = null;
		}
		
		#endregion 

		#region Appened Data for BankAccount Combobox List
		private void AppendBankAccountList()
		{
			//Variable declarations
			ArrayList arrAcctList = new ArrayList();
			XmlDocument objXML = base.SysEx;
			XmlNode objOld=null;
			XmlElement objNew=null;
			XmlCDataSection objCData=null;
            //Parijat: Mits 9378---since the fundManager was not being populated with the claims data.
            string claimNumber = base.GetSysExDataNodeText("//SysExternalParam/ClaimNumber");
            int claimId = base.GetSysExDataNodeInt("//SysExternalParam/ClaimId");
            string sDSNName = base.m_fda.userLogin.objRiskmasterDatabase.DataSourceName;
            string sUserName = base.m_fda.userLogin.LoginName;
            string sPassword = base.m_fda.userLogin.Password;
            int iUserId = base.m_fda.userLogin.UserId;
            int iGroupId = base.m_fda.userLogin.GroupId;
            //abisht MITS 10875
            //mcapps2 MITS 19769
            string sAccountName = string.Empty;
            int iThisAccountId = 0;
            int iThisSubAccountId = 0;
            bool bThisAccountIdFound = false;
            string sEFTAccountIds = string.Empty;
            bool bCombinedPayAccIDFound = false;  //MITS 27024
            int iCombinedPaySubAccID = 0; //MITS 27024
            int iCombinedPayAccID = 0; //MITS 27024
            int iPayeeEID = 0; //MITS 27024

            if (claimId == 0)
            {
                claimId = base.GetSysExDataNodeInt("//FundsAuto/ClaimId");
            }

            iThisAccountId = base.GetSysExDataNodeInt("//FundsAuto/AccountId");
            iThisSubAccountId = base.GetSysExDataNodeInt("//FundsAuto/SubAccId");

            //MITS 27024 Start
            FundsAuto FundsAutoObject = null;
            foreach (FundsAuto objAuto in this.FundsAutoBatch.FundsAutoList)
            {
                if (objAuto != null)
                {
                    FundsAutoObject = objAuto;
                    break;
                }
            }
            if (FundsAutoObject != null)
            {
                iPayeeEID = FundsAutoObject.PayeeEid;
            }
            if ((iPayeeEID > 0) && (base.GetSysExDataNodeText("AddCombPayAccount") == "Add")) //MITS 27024
            {
                getCombinedPayeeAccountID(iPayeeEID, ref iCombinedPayAccID, ref iCombinedPaySubAccID);
            }
            //MITS 27024 End

            FundManager m_objFundMngr = new FundManager(sDSNName, sUserName, sPassword, iUserId, iGroupId, claimId,base.ClientId);
            //Retrieve the complete Bank Account List and store it in the ArrayList
            arrAcctList = m_objFundMngr.GetAccounts(this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc);
            m_objFundManager = null;
            
			//Create the necessary SysExData to be used in ref binding
			objOld = objXML.SelectSingleNode("//AccountList");
			objNew = objXML.CreateElement("AccountList");

			XmlElement xmlOption = null;
			XmlAttribute xmlOptionAttrib = null;

            if (this.objData.Context.InternalSettings.SysSettings.FundsAccountLink != true) //MITS 22227 - mcapps2 - 09/08/2010
            {
                // Blank value for the combo box
                xmlOption = objXML.CreateElement("option");
                xmlOptionAttrib = objXML.CreateAttribute("value");
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
                xmlOptionAttrib = null;
                xmlOption = null;
            }
			//Loop through and create all the option values for the combobox control
			foreach (FundManager.AccountDetail item in arrAcctList)
			{
				xmlOption = objXML.CreateElement("option");
				objCData = objXML.CreateCDataSection(item.AccountName);
				xmlOption.AppendChild(objCData);
				xmlOptionAttrib = objXML.CreateAttribute("value");

                if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                {
                    xmlOptionAttrib.Value = item.SubRowId.ToString();
                }
                else
                {
                    xmlOptionAttrib.Value = item.AccountId.ToString();
                }
				xmlOption.Attributes.Append(xmlOptionAttrib);
				objNew.AppendChild(xmlOption);
                if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                {
                    if (iThisSubAccountId == item.SubRowId)
                    {
                        bThisAccountIdFound = true;
                    }
                    if (iCombinedPayAccID > 0) //MITS 27024
                    {
                        if (iCombinedPaySubAccID == item.SubRowId)
                        {
                            bCombinedPayAccIDFound = true;
                        }
                    } //MITS 27024 End
                }
                else
                {
                    if (iThisAccountId == item.AccountId)
                    {
                        bThisAccountIdFound = true;
                    }
                    if (iCombinedPayAccID > 0)  //MITS 27024
                    {
                        if (iCombinedPayAccID == item.AccountId)
                        {
                            bCombinedPayAccIDFound = true;
                        }
                    } //MITS 27024 End
                }
                //Added by Amitosh For EFT Payment
                //if (item.IsEFtAccount)
                //{
                //    if (string.IsNullOrEmpty(sEFTAccountIds))
                //    {
                //        sEFTAccountIds = item.AccountId.ToString();
                //    }
                //    else
                //    {
                //        sEFTAccountIds = sEFTAccountIds + "," + item.AccountId.ToString();
                //    }
                //}
                //Modifed by Manika for EFT Payment : MITS 26821

                if (item.IsEFtAccount)
                {
                    if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                    {
                        if (string.IsNullOrEmpty(sEFTAccountIds))
                        {
                            sEFTAccountIds = item.SubRowId.ToString();
                        }
                        else
                        {
                            sEFTAccountIds = sEFTAccountIds + "," + item.SubRowId.ToString();
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sEFTAccountIds))
                        {
                            sEFTAccountIds = item.AccountId.ToString();
                        }
                        else
                        {
                            sEFTAccountIds = sEFTAccountIds + "," + item.AccountId.ToString();
                        }
                    }
                }
            }//end foreach
            base.ResetSysExData("EFTBanks", sEFTAccountIds);
            if ((!bThisAccountIdFound) && (iThisAccountId != 0))
            {
                sAccountName = sGetAccountName(iThisAccountId, iThisSubAccountId, this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc);
                xmlOption = objXML.CreateElement("option");
                objCData = objXML.CreateCDataSection(sAccountName);
                xmlOption.AppendChild(objCData);
                xmlOptionAttrib = objXML.CreateAttribute("value");

                if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                {
                    xmlOptionAttrib.Value = iThisSubAccountId.ToString();
                }
                else
                {
                    xmlOptionAttrib.Value = iThisAccountId.ToString();
                }
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
            }
            if ((!bCombinedPayAccIDFound) && (iCombinedPayAccID != 0))  //MITS 27024 Start
            {
                sAccountName = string.Empty;
                sAccountName = sGetAccountName(iCombinedPayAccID, iCombinedPaySubAccID, this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc);
                xmlOption = objXML.CreateElement("option");
                objCData = objXML.CreateCDataSection(sAccountName);
                xmlOption.AppendChild(objCData);
                xmlOptionAttrib = objXML.CreateAttribute("value");

                if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                {
                    xmlOptionAttrib.Value = iCombinedPaySubAccID.ToString();
                    if ((FundsAutoObject != null) && (base.GetSysExDataNodeText("AddCombPayAccount") == "Add"))//MITS 27024
                    {
                        FundsAutoObject.SubAccId = iCombinedPaySubAccID;
                    }
                }
                else
                {
                    xmlOptionAttrib.Value = iCombinedPayAccID.ToString();
                    if ((FundsAutoObject != null) && (base.GetSysExDataNodeText("AddCombPayAccount") == "Add"))//MITS 27024
                    {
                        FundsAutoObject.AccountId = iCombinedPayAccID;
                    }
                }
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
                //ref="/Instance/UI/FormVariables/SysExData/FundsAuto/CombinedPayFlag"
                if ((FundsAutoObject != null) && (base.GetSysExDataNodeText("AddCombPayAccount") == "Add"))//MITS 27024
                {
                    FundsAutoObject.CombinedPayFlag = true;
                    base.ResetSysExData("AddCombPayAccount", "Set");//MITS 27024
                }
            }  //MITS 27024 End
			//Add the XML to the SysExData 
			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);


			objOld = objXML.SelectSingleNode("//ControlAppendAttributeList");
			objNew = objXML.CreateElement("ControlAppendAttributeList");
			XmlElement objElem=null;
			XmlElement objChild=null;
		
			objElem=objXML.CreateElement("bankaccount");
			objChild=objXML.CreateElement("ref");
			if( this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc )
				objChild.SetAttribute("value","/Instance/UI/FormVariables/SysExData/FundsAuto/SubAccId");	
			else
				objChild.SetAttribute("value","/Instance/UI/FormVariables/SysExData/FundsAuto/AccountId");

			objElem.AppendChild(objChild);;
			objNew.AppendChild(objElem);

			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);
				
			//Clean up
			arrAcctList = null;
			objOld = null;
			objNew = null;
			objCData = null;
			objXML = null;
            //Parijat: Mits 9378
            m_objFundMngr.Dispose();
            FundsAutoObject = null;
		}
        //MITS 27024 Start
        private void getCombinedPayeeAccountID(int iPayeeEID, ref int iCombinedPayAccID, ref int iCombinedPaySubAccID)
        {
            string sSQL = string.Empty;

            sSQL = "SELECT ACCOUNT_ID, SUB_ACCOUNT_ID FROM COMBINED_PAYMENTS WHERE COMBINED_PAY_EID = " + iPayeeEID;

            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
            {
                if (objReader != null && objReader.Read())
                {
                    iCombinedPayAccID = objReader.GetInt("ACCOUNT_ID");
                    iCombinedPaySubAccID = objReader.GetInt("SUB_ACCOUNT_ID");
                }
            }
        }
        //MITS 27024 End
		#endregion 

		#endregion 
		
		#region Common XML functions	
		/*
		 * Vaibhav Kaushik 11 Feb 2006
		 * 
		 * Following functions are used to create the Xml document.
		 */
		private void StartDocument( ref XmlDocument objXmlDocument , ref XmlElement p_objRootNode, string p_sRootNodeName )
		{
			objXmlDocument = new XmlDocument();
			p_objRootNode = objXmlDocument.CreateElement( p_sRootNodeName );
			objXmlDocument.AppendChild( p_objRootNode );							
		}

        private string sGetAccountName(int iAccountID, int iSubAccountId, bool bUseSubAccounts)
        {
            string sTemp = string.Empty;
            string sSQL = string.Empty;

            if (bUseSubAccounts == true)
            {
                sSQL = "SELECT SUB_ACC_NAME FROM BANK_ACC_SUB WHERE ACCOUNT_ID = " + iAccountID + " AND SUB_ROW_ID = " + iSubAccountId;
            }
            else
            {
                sSQL = "SELECT ACCOUNT_NAME FROM ACCOUNT WHERE ACCOUNT_ID = " + iAccountID;
            }
            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
            {
                if (objReader != null && objReader.Read())
                {
                    if (bUseSubAccounts == true)
                    {
                        sTemp = objReader.GetString("SUB_ACC_NAME");
                    }
                    else
                    {
                        sTemp = objReader.GetString("ACCOUNT_NAME");
                    }
                }
            }
            return sTemp;
        }

		private void CreateElement( XmlElement p_objParentNode , string p_sNodeName )
		{
			XmlElement objChildNode = null ;
			objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
			p_objParentNode.AppendChild( objChildNode );
			objChildNode = null ;			
		}	
	

		private void CreateElement( XmlElement p_objParentNode , string p_sNodeName , ref XmlElement p_objChildNode )
		{
			p_objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
			p_objParentNode.AppendChild( p_objChildNode );			
		}
		

		private void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string p_sText )
		{
			XmlElement objChildNode = null ;
			this.CreateAndSetElement( p_objParentNode , p_sNodeName , p_sText , ref objChildNode );			
		}


		private void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string p_sText , ref XmlElement p_objChildNode )
		{
			CreateElement( p_objParentNode , p_sNodeName , ref p_objChildNode );
			p_objChildNode.InnerText = p_sText ;						
		}

        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode, string p_sAttValue,string p_AttName)
        {
            CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
            p_objChildNode.InnerText = p_sText;
            p_objChildNode.SetAttribute(p_AttName, p_sAttValue);
        }

        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, string p_sAttValue, string p_AttName)
        {
            XmlElement objChildNode = null;
            this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode, p_sAttValue,p_AttName);
        }
		#endregion 

        #region MITS 7293 
        private void CheckFreezePaymentPemission()
        {
            try
            {
                if (SysEx.DocumentElement.SelectSingleNode("CheckFreeze") == null)
                {
                    CreateSysExData("CheckFreeze");

                }
                if (!m_fda.userLogin.IsAllowedEx(RMB_FUNDS_AUTOCHK + RMB_ALLOW_FREEZE_AUTOCHK_BATCH, RMO_ACCESS))
                {
                    base.ResetSysExData("CheckFreeze", "0");
                }
                else
                {
                    base.ResetSysExData("CheckFreeze", "1");
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
                
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AutoClaimChecksForm.Validate.Error", base.ClientId), p_objEx);
                
            }
            
        }
        #endregion

        //AA 7199
        public override void BeforeSave(ref bool Cancel)
        {
            if (IsDupeAddress())
            {
                Cancel = true;
                return;
            }
        }
        public bool IsDupeAddress()
        {

            string sSql = string.Empty;
            XmlNode nodeDup = null;
            XmlNode nodeAddress = null;
            XmlNode nodeSearchString = null;
            int sAddressId = 0;
            bool bIsReturn = false;

            FundsAutoList objFundsAutoList = this.FundsAutoBatch.FundsAutoList;
            FundsAuto objFundsAuto = null;
            foreach (FundsAuto fundsAuto in objFundsAutoList)
            {
                if (fundsAuto.MailToAddress.AddressId <= 0 && (fundsAuto.MailToAddress.Isdupeoverride != "IsSaveDup" && fundsAuto.MailToAddress.Isdupeoverride != "Cancel") && fundsAuto.MailToAddress.SearchString != "0" && (SysEx.SelectSingleNode("/SysExData/HdnEditAddress") != null && SysEx.SelectSingleNode("/SysExData/HdnEditAddress").InnerXml == "true"))
                {
                    sAddressId = CommonFunctions.CheckAddressDuplication(fundsAuto.MailToAddress.SearchString, fundsAuto.MailToAddress.Context.DbConn.ConnectionString, fundsAuto.MailToAddress.Context.ClientId);

                    if (SysEx.SelectSingleNode("/SysExData/HdnEditAddress") != null)
                        SysEx.SelectSingleNode("/SysExData/HdnEditAddress").InnerXml = "";

                    if (sAddressId <= 0)
                    {
                        bIsReturn = false;
                    }
                    else
                    {
                        base.ResetSysExData("HdnIsDupAddr", "");
                        nodeDup = this.SysEx.SelectSingleNode("//HdnIsDupAddr");
                        nodeDup.InnerText = "1";

                        base.ResetSysExData("HdnSearchString", "");
                        nodeSearchString = this.SysEx.SelectSingleNode("//HdnSearchString");
                        nodeSearchString.InnerText = fundsAuto.MailToAddress.SearchString;

                        base.ResetSysExData("HdnDupAddrId", "");
                        nodeAddress = this.SysEx.SelectSingleNode("//HdnDupAddrId");
                        nodeAddress.InnerText = Convert.ToString(sAddressId);
                        bIsReturn = true;
                        return true;
                    }
                }

            }

            return bIsReturn;
        }

        #region After Save and OFAC Implementation
        //Added After Save for the Implementation of OFAC Check
        public override void AfterSave()
        {
            //AA 7199
            base.ResetSysExData("dupeoverride", "");
            base.ResetSysExData("HdnIsDupAddr", "");
            base.ResetSysExData("HdnDupAddrId", "");
            base.ResetSysExData("HdnSearchString", "");

            //rsushilaggar MITS 27286 Date 03/01/2012
            FundsAutoList objFundsAutoList = this.FundsAutoBatch.FundsAutoList;
            FundsAuto objFundsAuto = null;
            foreach (FundsAuto fundsAuto in objFundsAutoList)
            { 
			//mbahl3 mits 31774
                if (( fundsAuto.PayeeEid != 0) && (fundsAuto.ThirdPartyFlag ==0))
                {
                    objFundsAuto = fundsAuto;
                    break;
                }
            }

            foreach (FundsAuto fundsAuto in objFundsAutoList)
			//mbahl3 mits 31774
            {
                if (objFundsAuto != null && fundsAuto.ThirdPartyFlag == 0)      //avipinsrivas start : Worked on JIRA - 15376 (Added null check for objFundsAuto).
				//mbahl3 mits 31774
                {
                    fundsAuto.PayeeEid = objFundsAuto.PayeeEid;

                    fundsAuto.LastName = objFundsAuto.LastName;
                    fundsAuto.FirstName = objFundsAuto.FirstName;
                    fundsAuto.Addr1 = objFundsAuto.Addr1;
                    fundsAuto.Addr2 = objFundsAuto.Addr2;
                    fundsAuto.Addr3 = objFundsAuto.Addr3;// JIRA 6420 pkandhari
                    fundsAuto.Addr4 = objFundsAuto.Addr4;// JIRA 6420 pkandhari
                    fundsAuto.City = objFundsAuto.City;
                    fundsAuto.StateId = objFundsAuto.StateId;
                    fundsAuto.ZipCode = objFundsAuto.ZipCode;

                    fundsAuto.Save();
                }
            }
            //End rsushilaggar
            if (FundsAutoBatch.Context.InternalSettings.SysSettings.DoOfacCheck)
            {
                PerformOFACCheck();
            }
        }
        protected void PerformOFACCheck()
        {
            FundsAuto FundsAutoObject = null;

            foreach (FundsAuto objAuto in this.FundsAutoBatch.FundsAutoList)
            {
                if (objAuto != null)
                {
                    FundsAutoObject = objAuto;
                    break;
                }
            }

            if ((String.Equals(FundsAutoObject.LastName.ToString(),FundsAutoObject.PayeeEntity.LastName.ToString())) && (String.Equals(FundsAutoObject.FirstName.ToString(),FundsAutoObject.PayeeEntity.FirstName.ToString())))
            {
                if ((FundsAutoObject.PayeeEntity.OverrideOfacCheck == false) && (FundsAutoObject.PayeeEntity.FreezePayments == false))
                {
                    Riskmaster.Application.PatriotProtectorWrapper.PatriotProtectorWrapper obj = new Riskmaster.Application.PatriotProtectorWrapper.PatriotProtectorWrapper(FundsAutoObject.PayeeEntity.Context.RMDatabase.DataSourceName, FundsAutoObject.PayeeEntity.Context.RMUser.LoginName, FundsAutoObject.PayeeEntity.Context.RMUser.Password, base.ClientId);//rkaur27
                    obj.AsynchPatriotProtectorCall(FundsAutoObject.PayeeEntity.FirstName, FundsAutoObject.PayeeEntity.LastName, FundsAutoObject.Table, FundsAutoObject.AutoTransId, FundsAutoObject.PayeeEntity, -1, FundsAutoObject.AutoTransId, FundsAutoObject.AutoBatchId,true,FundsAutoObject.AutoBatchId.ToString());
                }
            }
            else
            {
                if ((FundsAutoObject.PayeeEntity.OverrideOfacCheck == false) && (FundsAutoObject.PayeeEntity.FreezePayments == false))
                {
                    Riskmaster.Application.PatriotProtectorWrapper.PatriotProtectorWrapper obj = new Riskmaster.Application.PatriotProtectorWrapper.PatriotProtectorWrapper(FundsAutoObject.PayeeEntity.Context.RMDatabase.DataSourceName, FundsAutoObject.PayeeEntity.Context.RMUser.LoginName, FundsAutoObject.PayeeEntity.Context.RMUser.Password, base.ClientId);//rkaur27
                    obj.AsynchPatriotProtectorCall(FundsAutoObject.FirstName, FundsAutoObject.LastName, FundsAutoObject.Table, FundsAutoObject.AutoTransId, FundsAutoObject.PayeeEntity, -1, FundsAutoObject.AutoTransId, FundsAutoObject.AutoBatchId, false, FundsAutoObject.AutoBatchId.ToString());
                   // obj.AsynchPatriotProtectorCall(FundsAutoObject.PayeeEntity.FirstName, FundsAutoObject.PayeeEntity.LastName, FundsAutoObject.PayeeEntity.Table, FundsAutoObject.PayeeEntity.EntityId, FundsAutoObject.PayeeEntity, -1, -1, -1, true);
                }
            }
        }
        #endregion

        private void ResetResRowId(ref XmlDocument oDoc, FundsAuto objPayeeFundsAuto)
        {
            XmlNode objTransId = null;
            XmlNode objPolicyId = null;
            XmlNode objCoverageTypeCode = null;
            XmlNode objCovgSeqNum = null;   //Ankit : Start Policy System Interface

            //start - Added by Nikhil
            XmlNode objDedVisibleFlag = null;
            //End - Added by Nikhil

            DbReader objReader = null;
            XmlNode objUnit = null;
            StringBuilder sbSql = new StringBuilder();
            string sClaimId = string.Empty;
            string sCoverageTypeCode = string.Empty;
            string sPolicyId = string.Empty;
            string sPolCvgRowId = string.Empty;
            string sClaimantEid = string.Empty;
            string sReserveTypeCode = string.Empty;
            string sRcRowId = string.Empty;
            XmlElement objXml = null;
            //rupal:r8 unit changes
            string sPolUnitRowId = string.Empty;
            bCarrierClaims = Conversion.ConvertStrToBool(objPayeeFundsAuto.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString());
            Policy objPolicy = null;
            string sCovgSeqNum = string.Empty;
            bool bConverted = false;
            try
            {
                if (bCarrierClaims)
                {
                    sClaimId = oDoc.SelectSingleNode("//ClaimId").InnerText;
                    sClaimantEid = oDoc.SelectSingleNode("//ClaimantEid").Attributes["codeid"].Value;
                    if (string.IsNullOrEmpty(sClaimId) || string.IsNullOrEmpty(sClaimantEid))
                    {
                        objTransId = oDoc.SelectSingleNode("//AutoTransId");
                        sbSql = sbSql.Append(string.Format("SELECT CLAIM_ID,CLAIMANT_EID FROM FUNDS WHERE TRANS_ID ={0}", objTransId.InnerText));
                        using (objReader = objPayeeFundsAuto.Context.DbConn.ExecuteReader(sbSql.ToString()))
                        {
                            if (objReader.Read())
                            {
                                sClaimId = objReader["CLAIM_ID"].ToString();
                                sClaimantEid = objReader["CLAIMANT_EID"].ToString();
                            }
                        }
                    }
                    foreach (XmlNode objNode in oDoc.SelectNodes("//AutoSplitList/FundsAutoSplit"))
                    {
                        sCoverageTypeCode = objNode.SelectSingleNode("//CoverageTypeCode").Attributes["codeid"].Value;
                        sPolicyId = objNode.SelectSingleNode("//PolicyID").Attributes["value"].Value;
                        //rupal:r8 unit changes
                        sPolUnitRowId = objNode.SelectSingleNode("//UnitID").Attributes["value"].Value;

                        sReserveTypeCode = objNode.SelectSingleNode("//ReserveTypeCode").Attributes["codeid"].Value;
                        sbSql.Length = 0;
                        sCovgSeqNum = oDoc.SelectSingleNode("//CovgSeqNum").InnerText;//Ankit : Policy System Interface
                        //sbSql = sbSql.Append(string.Format("SELECT POLICY_X_CVG_TYPE.POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE WHERE POLICY_X_CVG_TYPE.POLICY_ID = {0} AND POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE = {1}", sPolicyId, sCoverageTypeCode));
                        //Ankit : Start Policy System Interface
                        objPolicy = (Policy)this.FundsAutoBatch.Context.Factory.GetDataModelObject("Policy", false);
                        objPolicy.MoveTo(Conversion.CastToType<int>(sPolicyId, out bConverted));
                        if (objPolicy.PolicySystemId > 0)
                        {
                            sbSql = sbSql.Append(string.Format("SELECT POLICY_X_CVG_TYPE.POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE WHERE POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = {0} AND POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE = {1} AND CVG_SEQUENCE_NO = '{2}' ", sPolUnitRowId, sCoverageTypeCode, sCovgSeqNum));
                        }
                        else
                        {
                            sbSql = sbSql.Append(string.Format("SELECT POLICY_X_CVG_TYPE.POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE WHERE POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = {0} AND POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE = {1}", sPolUnitRowId, sCoverageTypeCode));
                        }
                        using (objReader = objPayeeFundsAuto.Context.DbConn.ExecuteReader(sbSql.ToString()))
                        {
                            if (objReader.Read())
                                sPolCvgRowId = objReader["POLCVG_ROW_ID"].ToString();
                        }
                        sbSql.Length = 0;
                        //sbSql = sbSql.Append(string.Format("SELECT RC_ROW_ID FROM RESERVE_CURRENT WHERE CLAIM_ID = {0} AND CLAIMANT_EID = {1} AND POLICY_ID = {2} AND POLCVG_ROW_ID = {3} AND RESERVE_TYPE_CODE = {4}", sClaimId, sClaimantEid, sPolicyId, sPolCvgRowId, sReserveTypeCode));
                        //using (objReader = FundsObject.Context.DbConn.ExecuteReader(sbSql.ToString()))
                        //{
                        //    if (objReader.Read())
                        //        sRcRowId = objReader["RC_ROW_ID"].ToString();
                        //}
                        //BOB
                        this.CreateAndSetElement((XmlElement)objNode, "CoverageId", sPolCvgRowId.ToString(), ref objXml);
                        
                        objPolicyId = objNode.SelectSingleNode("//PolicyID");
                        if (objPolicyId != null)
                            objNode.RemoveChild(objPolicyId);
                        
                        objCoverageTypeCode = objNode.SelectSingleNode("//CoverageTypeCode");
                        if (objCoverageTypeCode != null)
                            objNode.RemoveChild(objCoverageTypeCode);
                        
                        //rupal:start, r8 unit implementation
                        objUnit = objNode.SelectSingleNode("//UnitID");
                        if (objUnit != null)
                            objNode.RemoveChild(objUnit);
                        //rupal:end
                        objCovgSeqNum = objNode.SelectSingleNode("CovgSeqNum");
                        if (objCovgSeqNum != null)
                            objNode.RemoveChild(objCovgSeqNum);
                        //start - Added by Nikhil.
                        objDedVisibleFlag = objNode.SelectSingleNode("OverrideDedFlagVisible");
                        if (objDedVisibleFlag != null)
                            objNode.RemoveChild(objDedVisibleFlag);
                        //End - Added by Nikhil.
                    }                            
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                objTransId = null;
                objPolicyId = null;
                objCoverageTypeCode = null;
                objUnit = null;
                objReader = null;
                sbSql = null;
                if (objPolicy != null)
                    objPolicy.Dispose();
            }
        }

        /// <summary> R8 Funds utility enhancement
        /// Neha: Changes the print date sa per Holiset set up in schedule dates
        /// </summary>
        /// <param name="bAdjustPrintDatesFlag"></param>
        /// <param name="p_dtToDate"></param>
        /// <returns>date time</returns>
        private DateTime AdjustPrintDate(bool bAdjustPrintDatesFlag, DateTime p_dtToDate)
        {
            string sSQL = string.Empty;
            bool bAutoCheck = false;
            string sScheduleToDay = string.Empty;
            string sAdjustPrintDatesFlag = string.Empty;
            ArrayList sHolidayList = new ArrayList();
            bool bIsHoliday = false;
            DateTime dtPrintDate;
            dtPrintDate = p_dtToDate;
            bAutoCheck = CommonFunctions.IsScheduleDatesOn("SCH_DATE_AUTOCHECK", m_sConnectionString, base.ClientId);


            if (bAutoCheck)
            {
                if (bAdjustPrintDatesFlag)
                {
                    sScheduleToDay = CommonFunctions.GetWeekendSchedule(m_sConnectionString, base.ClientId);
                    sSQL = "SELECT SCHEDULE_DATE FROM SCHEDULED_DATES";
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        while (objReader.Read())
                        {
                            sHolidayList.Add(objReader.GetString("SCHEDULE_DATE"));
                        }
                    }
                    dtPrintDate = CheckHolidayWeekend(dtPrintDate, sScheduleToDay, sHolidayList);
                }
            }
            return dtPrintDate;
        }

        /// <summary>
        /// neha R8 Funds utilty enhancement loops through all days till 
        /// </summary>
        /// <param name="p_date"></param>
        /// <param name="sScheduleToDay"></param>
        /// <param name="sHolidayList"></param>
        /// <returns></returns>
        private DateTime CheckHolidayWeekend(DateTime p_date, string sScheduleToDay, ArrayList sHolidayList)
        {
            bool bIsHoliday = false;
            foreach (string sScheduledate in sHolidayList)
            {
                if (String.Compare(p_date.ToString("yyyyMMdd"), sScheduledate) == 0)
                {
                    bIsHoliday = true;
                }
            }

            if (p_date.DayOfWeek == DayOfWeek.Sunday)
            {
                switch (sScheduleToDay)
                {
                    case "Monday":
                        p_date = p_date.AddDays(1);
                        break;
                    case "Thursday":
                        p_date = p_date.AddDays(-3);
                        break;

                    case "Saturday":
                        p_date = p_date.AddDays(-1);
                        break;
                    default://and friday will be default case as well
                        p_date = p_date.AddDays(-2);
                        break;
                }
                p_date = CheckHolidayWeekend(p_date, sScheduleToDay, sHolidayList);
            }
            else if (p_date.DayOfWeek == DayOfWeek.Saturday && sScheduleToDay != "Saturday")
            {
                switch (sScheduleToDay)
                {
                    case "Monday":
                        p_date = p_date.AddDays(2);
                        break;
                    case "Thursday":
                        p_date = p_date.AddDays(-2);
                        break;
                    default://and friday will be default case as well
                        p_date = p_date.AddDays(-1);
                        break;
                }
                p_date = CheckHolidayWeekend(p_date, sScheduleToDay, sHolidayList);
            }
            else if (bIsHoliday)
            {
                //rupal:start, mits 28850
                if (this.FundsAutoBatch.PaymentInterval == this.objCache.GetCodeId("PD", "PERIOD_TYPES"))
                {
                    p_date = p_date.AddDays(1);
                }
                else
                {
                    p_date = p_date.AddDays(-1);
                }
                //rupal:end, mits 28850
                p_date = CheckHolidayWeekend(p_date, "", sHolidayList);
            }
            return p_date;
        }
        //amitosh R8 comb Pay
        private void RenderPayeesList(ref XmlDocument objXML)
        {
            XmlElement objPayeeListElement = null;
            string sCodeId = string.Empty;
           // Entity objEntity = (Entity)m_fda.Factory.GetDataModelObject("Entity", false);
            DbReader rdr = null;
            XmlElement objXMLFundsAutoBatchId = null;
            //skhare7 R8 Payee Typecode
            int sTableId = 0;
            Entity objEntity = null;
            CombinedPayment objCombinedPayment = null;
            string sSQL = string.Empty;
            string sPayeeTypeCodeDesc = string.Empty;
            string sSelectedPayeeTypeCode = string.Empty;
            int iTableType = 0;
            int iOtherPayeeTypeCodeId = 0;
            int iCombinedPayeeId = 0;
            DbReader DBRdr = null;
            bool isFirstVal = true;
            try
            {
                objPayeeListElement = (XmlElement)objXML.SelectSingleNode("/SysExData/FundsAutoXPayeeList");
                objXMLFundsAutoBatchId = ((XmlElement)objXML.SelectSingleNode("/SysExData/FundsAuto/AutoBatchId"));
                if (objPayeeListElement != null)
                    objPayeeListElement.ParentNode.RemoveChild(objPayeeListElement);

                CreateElement(objXML.DocumentElement, "FundsAutoXPayeeList", ref objPayeeListElement);
                if (array != null)
                {
                    try
                    {
                        foreach (string sTemp in array)
                        {
                            if (sTemp != string.Empty)
                            {
                            arrayPayeeType = sTemp.Split("#".ToCharArray()[0]);
                            int iEnt = Convert.ToInt32(arrayPayeeType[0]);
                           
                            if (iEnt != 0)
                            {
                                //Ankit Start : Financial Enhancement - Prefix and Suffix Changes
                                if (isFirstVal)
                                    m_sEntityIDs = iEnt.ToString();
                                else
                                    m_sEntityIDs = m_sEntityIDs + ", " + iEnt.ToString();

                                if (isFirstVal)
                                    isFirstVal = false;
                                //Ankit End

                                //Amitosh for combined Payee
                                if (isCombinedPayee(iEnt, ref iCombinedPayeeId))
                                {

                                    objCombinedPayment = (CombinedPayment)m_fda.Factory.GetDataModelObject("CombinedPayment", false);

                                    objCombinedPayment.MoveTo(iCombinedPayeeId);
                                    //sTableId = objCombinedPayment.EntityTableId;
                                    CreateAndSetElement(objPayeeListElement, "Item", objCombinedPayment.LastName + "|" + objCombinedPayment.FirstName + "|" + objCombinedPayment.TaxId, objCombinedPayment.LastName + "|" + objCombinedPayment.FirstName + "|" + objCombinedPayment.TaxId + "_" + objCombinedPayment.EntityId);
                                    objCombinedPayment = null;

                                }
                                else
                                {
                                    objEntity = (Entity)m_fda.Factory.GetDataModelObject("Entity", false);

                                    objEntity.MoveTo(Convert.ToInt32(arrayPayeeType[0]));
                                    sTableId = objEntity.EntityTableId;
                                    CreateAndSetElement(objPayeeListElement, "Item", objEntity.LastName + "|" + objEntity.FirstName + "|" + objEntity.TaxId, objEntity.LastName + "|" + objEntity.FirstName + "|" + objEntity.TaxId + "_" + objEntity.EntityId);
                                    objEntity = null;
                                }
                                //End Amitosh

//                                objEntity.MoveTo(Convert.ToInt32(arrayPayeeType[0]));
  //                              CreateAndSetElement(objPayeeListElement, "Item", objEntity.LastName + "|" + objEntity.FirstName + "|" + objEntity.TaxId, objEntity.LastName + "|" + objEntity.FirstName + "|" + objEntity.TaxId + "_" + objEntity.EntityId);
                            }
                            if (string.IsNullOrEmpty(sCodeId) && iEnt != 0)
                            {
                                sCodeId = sTemp.ToString() + "#" + arrayPayeeType[1];
                            }
                            else if (iEnt != 0)
                            {
                                sCodeId = string.Concat(sCodeId + "  " + sTemp.ToString() + "#" + arrayPayeeType[1]);
                            }
                        }
                        }
                        array = null;
                    }
                    catch (Exception ex)
                    {
                        array = null;
                    }
                }
                if (objXMLFundsAutoBatchId != null && objPayeeListElement.ChildNodes.Count == 0)
                {
                   
                 //   int iPayeeId = 0;//MITS 27687
                    //MITS 27687
                    if (!string.IsNullOrEmpty(objXMLFundsAutoBatchId.InnerText) && Convert.ToInt32(objXMLFundsAutoBatchId.InnerText) > 0)
                    {
                        //using (rdr = DbFactory.ExecuteReader(this.m_sConnectionString, "SELECT distinct b.PAYEE_EID,b.PAYEE_TYPE_CODE FROM FUNDS_AUTO a,FUNDS_AUTO_X_PAYEE b WHERE a.AUTO_TRANS_ID=b.FUNDS_AUTO_TRANS_ID AND  AUTO_BATCH_ID =" + objXMLFundsAutoBatchId.InnerText.Trim()))
                        using (rdr = DbFactory.ExecuteReader(this.m_sConnectionString, "SELECT distinct b.PAYEE_EID,b.PAYEE_TYPE_CODE,b.ORDER_BY FROM FUNDS_AUTO a,FUNDS_AUTO_X_PAYEE b WHERE a.AUTO_TRANS_ID=b.FUNDS_AUTO_TRANS_ID AND  AUTO_BATCH_ID =" + objXMLFundsAutoBatchId.InnerText.Trim() + " ORDER BY ORDER_BY"))//rkotak:mits 29024
                        {
                            while (rdr.Read())
                            {
                                int iEnt = Convert.ToInt32(rdr.GetValue(0));
                                sPayeeTypeCodeDesc = this.objCache.GetShortCode(Convert.ToInt32(rdr.GetValue(1)));
                               // if (iPayeeId != iEnt)//MITS 27687
                               // {
                                    if (iEnt != 0)
                                    {
                                        //Ankit Start : Financial Enhancement - Prefix and Suffix Changes
                                        if (isFirstVal)
                                            m_sEntityIDs = iEnt.ToString();
                                        else
                                            m_sEntityIDs = m_sEntityIDs + ", " + iEnt.ToString();

                                        if (isFirstVal)
                                            isFirstVal = false;
                                        //Ankit End

                                    //  iPayeeId = iEnt;
                                    iOtherPayeeTypeCodeId = this.objCache.GetCodeId(sPayeeTypeCodeDesc, "PAYEE_TYPE");
                                    if (sPayeeTypeCodeDesc == "O")
                                    {
                                        //using (DBRdr = DbFactory.ExecuteReader(this.m_sConnectionString, "SELECT GLOSSARY.GLOSSARY_TYPE_CODE FROM ENTITY, GLOSSARY WHERE ENTITY.ENTITY_ID="
                                        //            + iEnt + " AND ENTITY.ENTITY_TABLE_ID=GLOSSARY.TABLE_ID"))
                                        //{
                                        //    while (DBRdr.Read())
                                        //    {
                                        //        iTableType = Convert.ToInt32(DBRdr.GetValue(0));
                                        //        iOtherPayeeTypeCodeId = this.objCache.GetCodeId("O", "PAYEE_TYPE");



                                        //        if (iTableType == 7 || iTableType == 4) // People
                                        //        {
                                        //            sSelectedPayeeTypeCode = "e" + iOtherPayeeTypeCodeId;
                                        //        }
                                        //        else if (iTableType == 5) // Org Hierarchy
                                        //        {
                                        //            sSelectedPayeeTypeCode = "o" + iOtherPayeeTypeCodeId;
                                        //        }
                                        //        else // If PAYEE_EID is 0 (often case in conversions)
                                        //        {
                                        //            sSelectedPayeeTypeCode = "o" + iOtherPayeeTypeCodeId;
                                        //        }
                                        //    }
                                        //}
                                        if (int.Equals(iEnt, 0))        //(often case during conversions)
                                            sSelectedPayeeTypeCode = "o" + iOtherPayeeTypeCodeId;
                                        else
                                            sSelectedPayeeTypeCode = "e" + iOtherPayeeTypeCodeId;
                                    }
                                    else if (sPayeeTypeCodeDesc == "H")
                                        sSelectedPayeeTypeCode = "o" + iOtherPayeeTypeCodeId;
                                    //avipinsrivas End
                                    else if (sPayeeTypeCodeDesc == "C")
                                    {
                                        sSelectedPayeeTypeCode = this.objCache.GetCodeId("C", "PAYEE_TYPE").ToString();
                                    }
                                    //start:rupal, r8 enh to inculde person involved as payee type
                                    else if (sPayeeTypeCodeDesc == "P")
                                    {
                                        sSelectedPayeeTypeCode = this.objCache.GetCodeId("P", "PAYEE_TYPE").ToString();
                                    }//end:rupal
                                    else
                                    {
                                        sSelectedPayeeTypeCode = string.Empty;
                                    }
                                    //Amitosh for combined Payee
                                    if (isCombinedPayee(iEnt, ref iCombinedPayeeId))
                                    {

                                        objCombinedPayment = (CombinedPayment)m_fda.Factory.GetDataModelObject("CombinedPayment", false);

                                        objCombinedPayment.MoveTo(iCombinedPayeeId);
                                        //sTableId = objCombinedPayment.EntityTableId;
                                        CreateAndSetElement(objPayeeListElement, "Item", objCombinedPayment.LastName + "|" + objCombinedPayment.FirstName + "|" + objCombinedPayment.TaxId, objCombinedPayment.LastName + "|" + objCombinedPayment.FirstName + "|" + objCombinedPayment.TaxId + "_" + objCombinedPayment.EntityId);
                                        objCombinedPayment = null;

                                    }
                                    else
                                    {
                                        objEntity = (Entity)m_fda.Factory.GetDataModelObject("Entity", false);

                                            objEntity.MoveTo(Convert.ToInt32(rdr.GetValue(0)));
                                            sTableId = objEntity.EntityTableId;
                                            CreateAndSetElement(objPayeeListElement, "Item", objEntity.LastName + "|" + objEntity.FirstName + "|" + objEntity.TaxId, objEntity.LastName + "|" + objEntity.FirstName + "|" + objEntity.TaxId + "_" + objEntity.EntityId);
                                            objEntity = null;
                                        }
                                        //End Amitosh

                                        //   objEntity.MoveTo(Convert.ToInt32(rdr.GetValue(0)));
                                        // CreateAndSetElement(objPayeeListElement, "Item", objEntity.LastName + "|" + objEntity.FirstName + "|" + objEntity.TaxId, objEntity.LastName + "|" + objEntity.FirstName + "|" + objEntity.TaxId + "_" + objEntity.EntityId);
                                    }
                                    if (string.IsNullOrEmpty(sCodeId) && iEnt != 0)
                                    {
                                        sCodeId = iEnt.ToString() + "#" + sSelectedPayeeTypeCode;
                                    }
                                    else if (iEnt != 0)
                                    {
                                        //rupal:start, mits 29024
                                        //sCodeId = string.Concat(sCodeId + " " + iEnt.ToString() + "#" + sSelectedPayeeTypeCode);
                                        sCodeId = string.Concat(sCodeId + "  " + iEnt.ToString() + "#" + sSelectedPayeeTypeCode);
                                        //rupal:end,mits 29024
                                    }
                                   
                               // }
                            }
                        }
                    }
                }
                objPayeeListElement.SetAttribute("codeid", sCodeId);

            }
            finally
            {
                objPayeeListElement = null;
                objXMLFundsAutoBatchId = null;
                if(objEntity!=null)
                  objEntity.Dispose();
                if (rdr != null)
                    rdr = null;
                if (objCombinedPayment != null)
                    objCombinedPayment = null;
                if (objEntity != null)
                    objEntity = null;
            }
        }
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, string p_iRecordid)
        {

            XmlElement objChildNode = null;
            this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
            objChildNode.SetAttribute("value", p_iRecordid.ToString());
        }
        //amitosh R8 comb Pay

        //Added by Amitosh for EFT
        private bool IsEftBank(int p_iAccId)
             {
                 bool bReturnValue = false;
                 try
                 {
                     using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT IS_EFT_ACCOUNT FROM ACCOUNT WHERE ACCOUNT_ID =" + p_iAccId))
                     {
                         if (objReader.Read())
                         {
                             bReturnValue = Conversion.ConvertObjToBool(objReader.GetValue(0), base.ClientId);
                         }
                     }
                 }
                 catch
                 {
                 }
                 return bReturnValue;
             }
        //End Amitosh
        private void AppendPayeePhraseList()
        {
            string sSQL = string.Empty;
            XmlDocument objSysExDataXmlDoc = base.SysEx;
            XmlNode objOldPayeePhraseListNode = null;
            XmlElement objNewPayeePhraseListNode = null;

            XmlElement objOptionXmlElement = null;
            XmlCDataSection objCData = null;

            try
            {
                objOldPayeePhraseListNode = objSysExDataXmlDoc.SelectSingleNode("//PayeePhraseList");
                objNewPayeePhraseListNode = objSysExDataXmlDoc.CreateElement("PayeePhraseList");

                sSQL = "SELECT DISTINCT CODES.CODE_ID, CODE_DESC ";
                sSQL = sSQL + "FROM CODES, CODES_TEXT, GLOSSARY ";
                sSQL = sSQL + "WHERE GLOSSARY.SYSTEM_TABLE_NAME='PAYEE_PHRASE_LIST' AND ";
                sSQL = sSQL + "GLOSSARY.TABLE_ID = CODES.TABLE_ID AND ";
                sSQL = sSQL + "CODES.CODE_ID = CODES_TEXT.CODE_ID AND ";
                sSQL = sSQL + "(CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ";
                sSQL = sSQL + "CODES_TEXT.LANGUAGE_CODE = 1033 ";
                sSQL = sSQL + " AND UPPER(CODES.SHORT_CODE) = 'AND'";
                //Aman ML Change
                StringBuilder sbSQL = new StringBuilder();
                sbSQL = CommonFunctions.PrepareMultilingualQuery(sSQL, "PAYEE_PHRASE_LIST", base.Adaptor.userLogin.objUser.NlsCode);

                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))  //Aman ML Change
                {
                    while (objReader.Read())
                    {
                        objOptionXmlElement = objSysExDataXmlDoc.CreateElement("value");
                        objCData = objSysExDataXmlDoc.CreateCDataSection(Convert.ToString(objReader.GetValue("CODE_DESC")));
                        objOptionXmlElement.AppendChild(objCData);
                        objNewPayeePhraseListNode.AppendChild(objOptionXmlElement);
                        objCData = null;
                        objOptionXmlElement = null;
                    }
                }

                if (objOldPayeePhraseListNode != null)
                    objSysExDataXmlDoc.DocumentElement.ReplaceChild(objNewPayeePhraseListNode, objOldPayeePhraseListNode);
                else
                    objSysExDataXmlDoc.DocumentElement.AppendChild(objNewPayeePhraseListNode);

            }
            finally
            {
                objOldPayeePhraseListNode = null;
                objNewPayeePhraseListNode = null;
                objCData = null;
                objSysExDataXmlDoc = null;

            }
        }

        //Amitosh
        private bool isCombinedPayee(int p_iEntityId, ref int iCombinedPayeeId)
        {
            bool bReturnValue = false;


            try
            {

                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT COMBINED_PAY_ROW_ID FROM COMBINED_PAYMENTS WHERE COMBINED_PAY_EID =" + p_iEntityId))
                {
                    if (objReader.Read())
                    {
                        iCombinedPayeeId = Conversion.ConvertStrToInteger(objReader.GetValue("COMBINED_PAY_ROW_ID").ToString());


                        bReturnValue = true;
                    }

                }

            }
            catch
            {
            }
            return bReturnValue;
        }



    }
}
