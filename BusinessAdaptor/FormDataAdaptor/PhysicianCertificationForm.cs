﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PhysicianCertification Screen.
	/// </summary>
	public class PhysicianCertificationForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PhysicianCertification";
		private PhysicianCertification PhysicianCertification{get{return objData as PhysicianCertification;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "PhysEid";
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
		
		/// <summary>
		/// Class constructor which initializes Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public PhysicianCertificationForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = 	CLASS_NAME;	
            sLangCode = Convert.ToString(fda.userLogin.objUser.NlsCode);

		}//end Class constructor

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();

			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlNode objPhysEid=null;

			try
			{
				objPhysEid = base.SysEx.GetElementsByTagName(FILTER_KEY_NAME)[0];
			}//end try
			catch
			{
			}; //end catch
			
			if(objPhysEid !=null)
				this.m_ParentId = Conversion.ConvertObjToInt(objPhysEid.InnerText, base.ClientId);


			PhysicianCertification.PhysEid = this.m_ParentId;

			//Filter by this PhysEid if Present.
			if(this.m_ParentId != 0)
			{
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId;
			}
			
		}//end method InitNew()

        // npadhy MITS 14200 Validation for Privilege Start Date and Privilege End Date as Per RMWorld
        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;

            if (!string.IsNullOrEmpty(PhysicianCertification.IntDate) && !(string.IsNullOrEmpty(PhysicianCertification.EndDate)))
            {
                if (PhysicianCertification.IntDate.CompareTo(PhysicianCertification.EndDate) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                            String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.EndDate", base.ClientId, sLangCode), Conversion.ToDate(PhysicianCertification.IntDate).ToShortDateString() + "(" + Globalization.GetString("Field.StartDate", base.ClientId, sLangCode) + ")"),
                            BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }

            // Return true if there were validation errors
            Cancel = bError;
        }

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
		}
	}
}
