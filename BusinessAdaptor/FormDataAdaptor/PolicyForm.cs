﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Application.FundManagement;
using System.Text;
using Riskmaster.Settings;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for Policy Screen.
	/// </summary>
	public class PolicyForm : DataEntryFormBase
	{
		const string CLASS_NAME = "Policy";
		private Policy Policy{get{return objData as Policy;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
        private string TodeleteUnitsCoverages = "";
		private bool killbackbutton=true;
        int iUsePolicyUnit = 0;
		public PolicyForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		public override void InitNew()
		{
			base.InitNew();

			//Nikhil Garg		Dated: 20-Jan-2006
			//check whether we need to provide the back button
			//back button means that the user has come here by clicking the "Open" button on some page.
//			if (base.m_fda.SafeFormVariableParamText("SysCmd")=="0" && base.m_fda.SafeParamText("SysFormId")!="0")
//			{
				string displayBackButton=base.GetSysExDataNodeText("/SysExData/DisplayBackButton");

				if (displayBackButton=="true")
					killbackbutton=false;
//			}
//			else
//				killbackbutton=true;
            iUsePolicyUnit = Policy.Context.InternalSettings.SysSettings.MultiCovgPerClm;
		}
        internal void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", base.ClientId), p_objEx);
            }

        }
        internal void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode)
        {
            try
            {
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
                p_objChildNode.InnerText = p_sText;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", base.ClientId), p_objEx);
            }
        }
        internal void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, string  p_iPolicyId)
        {
            try
            {
                XmlElement objChildNode = null;
                this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
                objChildNode.SetAttribute("value", p_iPolicyId.ToString());
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", base.ClientId), p_objEx);
            }
        }
        private void CreateUnits()
        {
            string[] array = null;
            XmlDocument objXML = base.SysEx;
            
            if (Policy.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
            {
                if (objXML.SelectSingleNode("//PolicyUnitList/@codeid") != null)
                {
                    string sClaimPolicyList = objXML.SelectSingleNode("//PolicyUnitList").Attributes["codeid"].Value;
                    array = sClaimPolicyList.Split(" ".ToCharArray()[0]);
                }
                if (array != null && array[0]!="")
                {
                    for (int count = 0; count <= array.Length - 1; count++)
                    {
                        bool bAdd = true;
                        string[] arrTemp=null;
                        arrTemp = array[count].Split("_".ToCharArray()[0]);
                        foreach (PolicyXUnit OPolicyXUnit in Policy.PolicyXUnitList)
                        {

                            if ((arrTemp[0] + "_" + arrTemp[1]) == (OPolicyXUnit.UnitType + "_" + OPolicyXUnit.UnitId.ToString()))
                            {
                                bAdd = false;
                                break;
                            }                      
                        }
                        if (bAdd)
                        {
                            PolicyXUnit objPolicyXUnit = Policy.PolicyXUnitList.AddNew();
                            objPolicyXUnit.UnitId = Conversion.ConvertStrToInteger(arrTemp[1]);
                            objPolicyXUnit.UnitType = arrTemp[0];                            
                        }
                    }
                }
                foreach (PolicyXUnit OPolicyXUnit in Policy.PolicyXUnitList)
                {
                    bool bDelete = true;
                    if (array != null && array[0] != "")
                    {
                       string[] arrTemp = null;
                        for (int count = 0; count <= array.Length - 1; count++)
                        {
                            arrTemp = array[count].Split("_".ToCharArray()[0]);
                            if ((arrTemp[0] + "_" + arrTemp[1]) == (OPolicyXUnit.UnitType + "_" + OPolicyXUnit.UnitId.ToString()))
                            {
                                bDelete = false;
                                break;
                            }
                        }
                    }
                    if (bDelete)
                    {
                        Policy.PolicyXUnitList.Remove(OPolicyXUnit.PolicyUnitRowId);
                        TodeleteUnitsCoverages += OPolicyXUnit.PolicyUnitRowId.ToString() + ",";
                    }
                }
            }
            if (TodeleteUnitsCoverages!="")
            TodeleteUnitsCoverages = TodeleteUnitsCoverages.Substring(0, TodeleteUnitsCoverages.Length - 1);
        }
        
        private void DeleteAssociatedCoverages()
        {
            try
            {
                if (TodeleteUnitsCoverages == "")
                    return;

                //Delete all Activity Records (if any)
                string SQL = "DELETE FROM POLICY_X_CVG_TYPE WHERE Policy_Unit_Row_Id in (" + TodeleteUnitsCoverages +")";

                if (Policy.Context.DbTrans == null)
                    Policy.Context.DbConn.ExecuteNonQuery(SQL);
                else
                    Policy.Context.DbConn.ExecuteNonQuery(SQL, Policy.Context.DbTrans);

            }
            catch (Exception e) { throw new Riskmaster.ExceptionTypes.DataModelException(Riskmaster.Common.Globalization.GetString("Claim.DeleteAssociatedDiaries.Exception", base.ClientId), e); }
        }
        private void RenderUnitList(ref XmlDocument objXML)
        {
            XmlElement objPolicyUnitElement = null;
            string sCodeId = string.Empty;
            //Policy objPolicy = (Policy)m_fda.Factory.GetDataModelObject("Policy", false);
            Vehicle objVehicle = (Vehicle)m_fda.Factory.GetDataModelObject("Vehicle", false);
            PropertyUnit objProperty = (PropertyUnit)m_fda.Factory.GetDataModelObject("PropertyUnit", false);
            SiteUnit objSiteUnit = (SiteUnit)m_fda.Factory.GetDataModelObject("SiteUnit", false);
            OtherUnit objOtherUnit = (OtherUnit)m_fda.Factory.GetDataModelObject("OtherUnit", false);
            SysSettings objSysSettings = null;
            try
            {
                objPolicyUnitElement = (XmlElement)objXML.SelectSingleNode("/SysExData/PolicyUnitList");
                if (objPolicyUnitElement != null)
                    objPolicyUnitElement.ParentNode.RemoveChild(objPolicyUnitElement);
                
                CreateElement(objXML.DocumentElement, "PolicyUnitList", ref objPolicyUnitElement);
                objSysSettings = new SysSettings(Policy.Context.DbConn.ConnectionString, ClientId); //Ash - cloud
               
                foreach (PolicyXUnit objPolicyXUnit in Policy.PolicyXUnitList)
                {
                    if (string.IsNullOrEmpty(sCodeId) && objPolicyXUnit.PolicyId != 0)
                    {
                        sCodeId = objPolicyXUnit.UnitType + "_" + objPolicyXUnit.UnitId+"_"+objPolicyXUnit.PolicyUnitRowId.ToString();
                    }
                    else if (objPolicyXUnit.PolicyId != 0)
                    {
                        sCodeId = string.Concat(sCodeId + " " + objPolicyXUnit.UnitType + "_" + objPolicyXUnit.UnitId+"_"+objPolicyXUnit.PolicyUnitRowId.ToString());
                    }
                    if (objPolicyXUnit.PolicyId != 0)
                    {
                        if (objPolicyXUnit.UnitType=="P")
                        {
                            objProperty.MoveTo(objPolicyXUnit.UnitId);
                            CreateAndSetElement(objPolicyUnitElement, "Item", "Property Unit: " + objProperty.Pin , "P_" + objPolicyXUnit.UnitId + "_" + objPolicyXUnit.PolicyUnitRowId.ToString()); 
                        }
                        else if (objPolicyXUnit.UnitType == "V")
                        {
                            objVehicle.MoveTo(objPolicyXUnit.UnitId);
                            if (objSysSettings.MultiCovgPerClm == -1)
                            {
                                string sTemp = objVehicle.VehDesc == string.Empty ? objVehicle.Vin : objVehicle.VehDesc;
                                CreateAndSetElement(objPolicyUnitElement, "Item", "Vehicle Unit: " + sTemp, "V_" + objPolicyXUnit.UnitId + "_" + objPolicyXUnit.PolicyUnitRowId.ToString()); 
                            }
                            else
                            {
                                CreateAndSetElement(objPolicyUnitElement, "Item", "Vehicle Unit: " + objVehicle.Vin, "V_" + objPolicyXUnit.UnitId + "_" + objPolicyXUnit.PolicyUnitRowId.ToString()); 
                            }
                            
                        }
                        else if (objPolicyXUnit.UnitType == "S")
                        {
                            objSiteUnit.MoveTo(objPolicyXUnit.UnitId);
                            CreateAndSetElement(objPolicyUnitElement, "Item", "Site Unit: " + objSiteUnit.SiteNumber,"S_"+objPolicyXUnit.UnitId+"_"+objPolicyXUnit.PolicyUnitRowId.ToString()); 
                        }
                        else if (objPolicyXUnit.UnitType == "SU")
                        {
                            objOtherUnit.MoveTo(objPolicyXUnit.UnitId);
                             Entity objEntity = (Entity)m_fda.Factory.GetDataModelObject("Entity", false);
                            objEntity.MoveTo(objOtherUnit.EntityId);
                            CreateAndSetElement(objPolicyUnitElement, "Item", "Stat Unit: " + objEntity.LastName, "SU_" + objPolicyXUnit.UnitId + "_" + objPolicyXUnit.PolicyUnitRowId.ToString() + "_" + objEntity.EntityId);
                        }
                    }
                }
                //objPolicyListElement = (XmlElement)objXML.SelectSingleNode("/SysExData/ClaimPolicyList");
                objPolicyUnitElement.SetAttribute("codeid", sCodeId);
                CreateElement(objXML.DocumentElement, "PolicyUnitListAll", ref objPolicyUnitElement);

            }
            finally
            {
                objPolicyUnitElement = null;
                if (objVehicle != null)
                {
                    objVehicle.Dispose();
                    objVehicle = null;
                }
                if (objProperty != null)
                {
                    objProperty.Dispose();
                    objProperty = null;
                }
                objSysSettings = null;
            }
        }
        //Ankit Start : Worked on MITS - 34333
        private void ResetSysExForPolicyName(int iPolicySystemID)
        {
            string sSQL = null;
            string strPolicyName = string.Empty;

            string strPolicySysType = string.Empty;
            sSQL = string.Concat(" SELECT POLICY_SYSTEM_NAME, POLICY_SYSTEM_CODE FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID = ", iPolicySystemID);
            DbReader reader = Policy.Context.DbConn.ExecuteReader(sSQL);
            if (reader.Read())
            {
                strPolicyName = Convert.ToString(reader.GetValue(0));
                strPolicySysType = Convert.ToString(reader.GetValue(1));
                base.CreateSysExData("PolicySystemName", strPolicyName);
                base.CreateSysExData("PolicySystemType", strPolicySysType);
            }
            if (reader != null)
                reader.Dispose();
        }
        //Ankit End
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			//Changes made here to add Policy name to form -Ratheen(#2113)
			string sName = "";


            XmlNode objOld = null;
            XmlElement objNew = null;

			//Call the base implementation of OnUpdateForm from the FormBase class
			base.OnUpdateForm ();

            //Ankit Start : Worked on MITS - 34333
            ResetSysExForPolicyName(Policy.PolicySystemId);
            //Ankit End

            //abahl3 Mits 22370 : to update the rmxref at runtime
            objOld = base.SysEx.SelectSingleNode("//ControlAppendAttributeList");
            objNew = base.SysEx.CreateElement("ControlAppendAttributeList");
            //ijha Mits 24391 : to change bank account to sub bank account when use sub bank account field is  made compulsary
                ArrayList singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                base.AddElementToList(ref singleRow, "id", "lbl_bankaccount");

            if (Policy.Context.InternalSettings.SysSettings.UseFundsSubAcc)
            {
               // this.AppendRuntimeAttribute(objNew, "bankaccount", "ref", "/Instance/Policy/SubAccRowId");
                this.AppendRuntimeAttribute(objNew, "bankaccount", "ref", "/Instance/Policy/SubAccRowId");
                base.AddElementToList(ref singleRow, "Text", "Sub Bank Account");
                base.m_ModifiedControls.Add(singleRow);

            }
            else
            {
                this.AppendRuntimeAttribute(objNew, "bankaccount", "ref", "/Instance/Policy/BankAccId");
                base.AddElementToList(ref singleRow, "Text", "Bank Account");
            }
            //ijha end
            if (objOld != null)
                base.SysEx.DocumentElement.ReplaceChild(objNew, objOld);
            else
                base.SysEx.DocumentElement.AppendChild(objNew);

            //abahl3 end
			//Create the BankAccountList necessary to populate the DropDownList on the FDM XML form
			AppendBankAccountList();
            //Parag
            XmlDocument objXML = base.SysEx ;
            if (iUsePolicyUnit == -1)
            {
                RenderUnitList(ref objXML);
                base.AddKillNode("btnCov");
            }
            else
            {
                base.ResetSysExData("UsePolicyUnit", "false");
                base.AddKillNode("unitinfo");
            }
            //Ashish Ahuja: Claims Made Jira 1342
            if (Policy.Context.InternalSettings.SysSettings.UsePolicyInterface)
            {
                
                base.AddKillNode("div_triggerclaimflag");
            }
            else
                base.AddKillNode("PolicyLimits");
            //Ashish Ahuja: Claims Made Jira 1342
            //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim.
            if (Policy.PolicyId > 0 && Policy.Context.InternalSettings.SysSettings.MultiCovgPerClm != 0 && !Policy.Context.InternalSettings.SysSettings.UsePolicyInterface)
            {
                int iClaimCount = 0;
                iClaimCount = Policy.Context.DbConnLookup.ExecuteInt("SELECT COUNT(CLAIM_ID) FROM CLAIM_X_POLICY WHERE POLICY_ID = " + Policy.PolicyId);
                if (iClaimCount >= 1)
                {
                    base.AddKillNode("delete");
                }
            }
            //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim.

            //skhare7 Point Policy Interface
           
            if (Policy.PolicySystemId <= 0)
            {
                base.AddKillNode("policyinterest");
                base.AddKillNode("policydriver");
            }
           

            else
                RenderDriverIntList(ref objXML);
            base.ResetSysExData("PolicyLOB",Policy.PolicyLOB.ToString());//skhare7 changed or Policy interface
			if (killbackbutton)
				base.AddKillNode("btnBack");
			//Ratheen
			sName = this.Policy.PolicyName.ToString();
            if (sName != "")
            {//	this.SysView.SelectSingleNode("//form/@title").InnerText = this.SysView.SelectSingleNode("//form/@title").InnerText + "[ "+ sName+" ]";
               // ArrayList singleRow = new ArrayList();            ijha Mits 24391 : The line commented to give single arraylist.
                singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                base.AddElementToList(ref singleRow, "id", "formsubtitle");
                base.AddElementToList(ref singleRow, "Text", "[ " + sName + " ]");
                base.m_ModifiedControls.Add(singleRow);
            }

            // Pradyumna 1/3/2013 - WWIG Gap 16: Modified If condition - Start
            if (Policy.PolicySystemId == 0)    //Start aaggarwal29: Point Policy interface; make additional details button  and form data button visible only in case of external policy
            {
                base.AddKillNode("btnAdditionalDetails");
                base.AddKillNode("btnAdditionalDetailsUnit");
                base.AddKillNode("btnAdditionalDetailsDriver");
                base.AddKillNode("btnAdditionalDetailsInterest");
                base.AddKillNode("btnFormDataUnit");
                base.AddKillNode("btnFormDataPolicy");
            }                                   //End aaggarwal29: Point Policy interface; 
            base.ResetSysExData("PostBackAction", "");
            // Pradyumna 1/3/2013 - WWIG Gap 16: Modified If condition - Ends
            
			// Mihika 01/12/2007 MITS 8656
			// If the utility setting for 'Multiple Insurer' is unchecked, remove the 'Insurer' button

                if (!objData.Context.InternalSettings.SysSettings.MultipleInsurer)
                {
                    
                    if (Policy.PolicySystemId > 0)    //akaur9 MITS 25163- Policy Interface Implementation
                    {
                        base.AddReadOnlyNode("readOnlyPage");
                        base.AddKillNode("save");
                        base.AddKillNode("delete");
                    }
                    
                    base.AddKillNode("btnInsurer");
                }
                else
                    if (Policy.PolicySystemId > 0)    //akaur9 MITS 25163- Policy Interface Implementation
                    {                       
                    //    //if (objData.Context.InternalSettings.SysSettings.ShowJurisdiction != 0)
                    //    //{
                    //    //    base.AddDisplayNode("allstatesflag");
                    //    //    base.AddDisplayNode("States");
                    //    //}
                    //    //else
                    //    //{
                    //    //    base.AddKillNode("allstatesflag");
                    //    //    base.AddKillNode("States");
                    //    //}
                        base.AddReadOnlyNode("readOnlyPage");   //akaur9 MITS 25163- Policy Interface Implementation
                        base.AddKillNode("save");
                        base.AddKillNode("delete");
                    }
                   else
                    {

                if (!Policy.IsNew)
                {
                    base.AddReadOnlyNode("inslastname");
                    base.AddReadOnlyNode("insphone1");
                    base.AddReadOnlyNode("inscontact");
                    base.AddReadOnlyNode("insphone2");
                    base.AddReadOnlyNode("insaddr1");
                    base.AddReadOnlyNode("insfaxnumber");
                    base.AddReadOnlyNode("insaddr2");
                    base.AddReadOnlyNode("insaddr3");// JIRA 6420 pkandhari
                    base.AddReadOnlyNode("insaddr4");// JIRA 6420 pkandhari
                    base.AddReadOnlyNode("inscity");
                    base.AddReadOnlyNode("insstateid");
                    base.AddReadOnlyNode("inszipcode");
                    base.AddReadOnlyNode("inscountrycode");

	//Gagan Safeway Retrofit Policy Jursidiction : START
                    base.AddReadOnlyNode("insemailaddress"); //abansal23 on 12/15/2009 MITS 19059 :Data in Email Address fields not populating from entity table 

//Gagan Safeway Retrofit Policy Jursidiction : END

                    //added by rkaur7 on 05/15/2009 - MITS 16668          
                    if (Policy.PolicyId > 0 && Policy.AllStatesFlag.ToString().ToLower() == "true")
                    {
                        base.AddReadOnlyNode("States");
                    }
                    //code end rkaur7
                }
                else
                {
                    base.AddReadWriteNode("inslastname");
                    base.AddReadWriteNode("insphone1");
                    base.AddReadWriteNode("inscontact");
                    base.AddReadWriteNode("insphone2");
                    base.AddReadWriteNode("insaddr1");
                    base.AddReadWriteNode("insfaxnumber");
                    base.AddReadWriteNode("insaddr2");
                    base.AddReadWriteNode("insaddr3");// JIRA 6420 pkandhari
                    base.AddReadWriteNode("insaddr4");// JIRA 6420 pkandhari
                    base.AddReadWriteNode("inscity");
                    base.AddReadWriteNode("insstateid");
                    base.AddReadWriteNode("inszipcode");
                    base.AddReadWriteNode("inscountrycode");

//Gagan Safeway Retrofit Policy Jursidiction : START
                    base.AddReadWriteNode("insemailaddress");//abansal23 on 12/15/2009 MITS 19059 :Data in Email Address fields not populating from entity table 


//Gagan Safeway Retrofit Policy Jursidiction : END
                }


            //Gagan Safeway Retrofit Policy Jursidiction : START
            /*if (objData.Context.InternalSettings.SysSettings.ShowJurisdiction != 0)
            {
                base.AddDisplayNode("alljurisdiction");
                base.AddDisplayNode("Jurisdictions");           
            }
            else
            {
                base.AddKillNode("alljurisdiction");
                base.AddKillNode("Jurisdictions");
            }*/
            //Gagan Safeway Retrofit Policy Jursidiction : END

            //rkaur7 MITS 16668 : START
            if (objData.Context.InternalSettings.SysSettings.ShowJurisdiction != 0)
            {
                base.AddDisplayNode("allstatesflag");
                base.AddDisplayNode("States");
            }
            else
            {
                base.AddKillNode("allstatesflag");
                base.AddKillNode("States");
            }
            }
                //Added for MITS 31085, Policy LOB field should not be a mandatory field on Policy Tracking when Carrier Claim setting is OFF.
                if (objData.Context.InternalSettings.SysSettings.MultiCovgPerClm != -1)
                {//base.AddKillNode("policyLOBCode");
                    ArrayList oArrayList = new ArrayList();
                    base.AddElementToList(ref oArrayList, "ControlType", enummodifiedcontrolType.Labels.ToString());
                    base.AddElementToList(ref oArrayList, "id", "lbl_policytype");
                    base.AddElementToList(ref oArrayList, "Class", "label");
                    base.m_ModifiedControls.Add(oArrayList);

                    base.AddSysRequiredFields("policytype_codelookup_cid");

                }
                if (Policy.PolicySystemId == 0)
                {
                  //  base.AddKillNode("policytype");
                    base.AddKillNode("policyexternalid");
                    base.AddKillNode("policysymbol");
                  //  dbisht6 start for mits
                    base.AddKillNode("mastercompany");
                    base.AddKillNode("locationcompany");
                // dbisht6 end
                    //dbisht6 start for mits 35331
                    base.AddKillNode("btnopeninsuredentity");
                    //dbisht6 end
                }
            //rkaur7 MITS 16668 : END

                if (Policy.PolicySystemId > 0)
                {
                    base.AddKillNode("btnClonePolicy");
                }
            AppendUnitTypeList();

            base.ResetSysExData("WCPolicyEmployee", CommonFunctions.GetWcPolicyEmployeeDetails(Policy.Context.DbConn.ConnectionString)); //Amitosh
            base.ResetSysExData("UseEntityRole", Policy.Context.InternalSettings.SysSettings.UseEntityRole.ToString().ToUpper());        //avipinsrivas start : Worked for JIRA - 14188 (For Epic 7767 and Story 13197)
		}


        public override void OnUpdateObject()
        {            
            base.OnUpdateObject();
            string sPostBackAction = string.Empty;
            
            sPostBackAction = base.GetSysExDataNodeText("PostBackAction");

            if (sPostBackAction == "CLONE")
            {
                //Shruti for 11244
                if (this.Policy.Context.RMUser.IsAllowedEx(this.SecurityId, RMO_POLICY_CLONE))
                {
                    ClonePolicy();
                }
                else
                {
                    Errors.Add(Globalization.GetString("PermissionFailure", base.ClientId),
                    string.Format(Globalization.GetString("Permission.FullPermissionMsg", base.ClientId), "(No Clone Permission)"),
                    BusinessAdaptorErrorType.Error);
                }
                //Shruti for 11244 ends
            }
            // npadhy Start MITS 18111 Moved the Code of Adding the Insurer to onUpdateobject as we are updating the object and 
            // This code should not be called in case of Cloning. So added the code here.
            else
            {
                if (this.Policy.PolicyId <= 0) // Insert a new record in the Policy_X_INSURER table.
                {
                    //nadim for boeing..count should not increase if insurer is not added
                    if (this.Policy.InsurerEid != 0)
                    {
                        PolicyXInsurer objPolIns = this.Policy.PolicyXInsurerList.AddNew(); ;
                        objPolIns.InsurerCode = this.Policy.InsurerEid;
                        objPolIns.PolicyId = this.Policy.PolicyId;
                        objPolIns.ResPercentage = 100.0;
                        objPolIns.PrimaryInsurer = true;
                        //MGaba2:MITS 16441:Count was not showing 1 when a new policy record is created with insurer:start
                        //objPolIns.Save();
                    }
                    //nadim for boeing..count should not increase if insurer is not added
                }
            }
            // npadhy End MITS 18111 Moved the Code of Adding the Insurer to onUpdateobject as we are updating the object and 
            // This code should not be called in case of Cloning. So added the code here.

            //abahl3 Mits 22370
            if (Policy.Context.InternalSettings.SysSettings.UseFundsSubAcc)
            {

                BankAccSub objBankAccSub = (BankAccSub)Policy.Context.Factory.GetDataModelObject("BankAccSub", false);
                objBankAccSub.MoveTo(Policy.SubAccRowId);
                Policy.BankAccId = objBankAccSub.AccountId;
            }
            //abahl3 End
            //parag
            CreateUnits();
        }


        /// <summary>
        /// Clones the current policy
        /// </summary>
        private void ClonePolicy()
        {
            Policy objPolicy;
            DataModelFactory objDataModelFactory = new DataModelFactory(this.Policy.Context.RMDatabase.DataSourceName,this.Policy.Context.RMUser.LoginName, this.Policy.Context.RMUser.Password, base.ClientId);
            XmlDocument PropertyStore = new XmlDocument();
            PolicyXMco objPolicyXMcoNew = null;
            PolicyXCvgType objPolicyXCvgTypeNew = null;
            PolicyXInsurer objPolicyXInsurerNew = null;
            // npadhy Start MITS 18111 Copy the Insurer and Reinsurer Data into new object while Cloning
            PolicyXReInsurer objPolicyXReInsurerNew = null;
            // npadhy End MITS 18111 Copy the Insurer and Reinsurer Data into new object while Cloning
            string sProp = string.Empty;
            string strCovgSupp = String.Empty;
            
            // npadhy Start MITS 18111 Copy the Insurer and Reinsurer Data into new object while Cloning
            string strPolicyInsurerSupp = String.Empty;
            string strPolicyReInsurerSupp = String.Empty;
            // npadhy End MITS 18111 Copy the Insurer and Reinsurer Data into new object while Cloning

            objPolicy = (Policy)objDataModelFactory.GetDataModelObject("Policy", false);                                       
            objPolicy.AddedByUser = this.Policy.Context.RMUser.LoginName;
            objPolicy.BankAccId = this.Policy.BankAccId;
            objPolicy.BrokerEid = this.Policy.BrokerEid;            
            objPolicy.CancelDate = this.Policy.CancelDate;
            objPolicy.Comments = this.Policy.Comments;
            objPolicy.DttmRcdAdded = Conversion.ToDbDateTime(DateTime.Now);
            objPolicy.DttmRcdLastUpd = Conversion.ToDbDateTime(DateTime.Now);
            objPolicy.EffectiveDate = this.Policy.EffectiveDate;
            objPolicy.ExpirationDate = this.Policy.ExpirationDate;
            objPolicy.HTMLComments = this.Policy.HTMLComments;
            objPolicy.InsurerEid = this.Policy.InsurerEid;          
            objPolicy.IssueDate = this.Policy.IssueDate;
            objPolicy.PolicyName = "Copy of " + this.Policy.PolicyName;
            objPolicy.PolicyNumber = this.Policy.PolicyNumber;
            objPolicy.PolicyStatusCode = this.Policy.PolicyStatusCode;
            objPolicy.PolicySystemId = 0;  //akaur9 MITS 25163- Policy Interface Implementation
            
            foreach (PolicyXCvgType objPolicyXCvgType in this.Policy.PolicyXCvgTypeList)
            {
                objPolicyXCvgTypeNew = objPolicy.PolicyXCvgTypeList.AddNew();
                objPolicyXCvgTypeNew.PolicyId = 0;

                objPolicyXCvgTypeNew.BrokerName = objPolicyXCvgType.BrokerName ;
                objPolicyXCvgTypeNew.CancelNoticeDays = objPolicyXCvgType.CancelNoticeDays;
                objPolicyXCvgTypeNew.ClaimLimit = objPolicyXCvgType.ClaimLimit;
                objPolicyXCvgTypeNew.CoverageTypeCode = objPolicyXCvgType.CoverageTypeCode;
                objPolicyXCvgTypeNew.Exceptions = objPolicyXCvgType.Exceptions;
                objPolicyXCvgTypeNew.NextPolicyId = objPolicyXCvgType.NextPolicyId;
                objPolicyXCvgTypeNew.NotificationUid = objPolicyXCvgType.NotificationUid;
                objPolicyXCvgTypeNew.OccurrenceLimit = objPolicyXCvgType.OccurrenceLimit;                
                objPolicyXCvgTypeNew.PolicyLimit = objPolicyXCvgType.PolicyLimit ;
                objPolicyXCvgTypeNew.Remarks = objPolicyXCvgType.Remarks;
                objPolicyXCvgTypeNew.SelfInsureDeduct = objPolicyXCvgType.SelfInsureDeduct;
                //Start: Added by Nitin Goel for NI requirment, MITS 30910,01/10/2012
                objPolicyXCvgTypeNew.DeductibleType = objPolicyXCvgType.DeductibleType;
                //End: Added by Nitin Goel for NI requirment, MITS 30910,01/10/2012
                // npadhy Start MITS 18111 Copy the Section Code into new object while Cloning
                objPolicyXCvgTypeNew.SectionNumCode = objPolicyXCvgType.SectionNumCode;
                // npadhy End MITS 18111 Copy the Section Code Data into new object while Cloning

                objPolicyXCvgTypeNew.TotalPayments = objPolicyXCvgType.TotalPayments;

                //Copy Coverages Supplemental
                strCovgSupp = objPolicyXCvgType.Supplementals.SerializeObject();
                if (strCovgSupp != String.Empty)
                {
                    PropertyStore.LoadXml(strCovgSupp);

                    foreach (XmlElement item in PropertyStore.SelectNodes("/Instance/*"))
                    {
                        sProp = item.LocalName;

                        if (sProp == "Supplementals")
                        {
                            if (objPolicyXCvgTypeNew.GetType().GetProperty("Supplementals") != null)
                                if (objPolicyXCvgTypeNew.Supplementals != null)
                                    objPolicyXCvgTypeNew.Supplementals.PopulateObject(Utilities.XmlElement2XmlDoc(item), false);
                        }
                    }
                }                                           

            }
                                   
            if (this.Policy.PolicyXInsured != null)
            {
                foreach (System.Collections.DictionaryEntry objDictionaryEntry in this.Policy.PolicyXInsured)
                {
                    objPolicy.PolicyXInsured.Add(objDictionaryEntry.Value);
                }
            }

            //added by rkaur7 on 05/14/2009 -MITS 16668
            if (this.Policy.PolicyXState != null)
            {
                foreach (System.Collections.DictionaryEntry objDictionaryEntry in this.Policy.PolicyXState)
                {
                    objPolicy.PolicyXState.Add(objDictionaryEntry.Value);
                }
            }
            //code end rkaur7

            foreach (PolicyXInsurer objPolicyXInsurer in this.Policy.PolicyXInsurerList)
            {

                objPolicyXInsurerNew = objPolicy.PolicyXInsurerList.AddNew();
                objPolicyXInsurerNew.PolicyId = 0;
                objPolicyXInsurerNew.InsurerCode = objPolicyXInsurer.InsurerCode;
                objPolicyXInsurerNew.PrimaryInsurer = objPolicyXInsurer.PrimaryInsurer;
                objPolicyXInsurerNew.ResPercentage = objPolicyXInsurer.ResPercentage;

                // npadhy Start MITS 18111 Copy the Insurer and Reinsurer Data into new object while Cloning
                objPolicyXInsurerNew.PreAmount = objPolicyXInsurer.PreAmount;
                objPolicyXInsurerNew.PortionPartLayer = objPolicyXInsurer.PortionPartLayer;
                objPolicyXInsurerNew.PartTotalLayer = objPolicyXInsurer.PartTotalLayer;
                objPolicyXInsurerNew.PaymentAmount = objPolicyXInsurer.PaymentAmount;
                objPolicyXInsurerNew.ScheduleCode = objPolicyXInsurer.ScheduleCode;
                objPolicyXInsurerNew.SpecialCircularMemo = objPolicyXInsurer.SpecialCircularMemo;
                objPolicyXInsurerNew.LayerNumCode = objPolicyXInsurer.LayerNumCode;
                objPolicyXInsurerNew.OccuranceLimit = objPolicyXInsurer.OccuranceLimit;
                objPolicyXInsurerNew.CoverageDesc = objPolicyXInsurer.CoverageDesc;
                objPolicyXInsurerNew.CommentMemo = objPolicyXInsurer.CommentMemo;

                strPolicyInsurerSupp = objPolicyXInsurer.Supplementals.SerializeObject();
                if (strPolicyInsurerSupp != String.Empty)
                {
                    PropertyStore = new XmlDocument();
                    PropertyStore.LoadXml(strPolicyInsurerSupp);

                    foreach (XmlElement item in PropertyStore.SelectNodes("/Instance/*"))
                    {
                        sProp = item.LocalName;

                        if (sProp == "Supplementals")
                        {
                            if (objPolicyXInsurerNew.GetType().GetProperty("Supplementals") != null)
                                if (objPolicyXInsurerNew.Supplementals != null)
                                    objPolicyXInsurerNew.Supplementals.PopulateObject(Utilities.XmlElement2XmlDoc(item), false);
                        }
                    }
                }
                foreach (PolicyXReInsurer objPolicyXReInsurer in objPolicyXInsurer.PolicyXReInsurerList)
                {
                    objPolicyXReInsurerNew = objPolicyXInsurerNew.PolicyXReInsurerList.AddNew();
                    objPolicyXReInsurerNew.ReInsurerRowID = 0;
                    objPolicyXReInsurerNew.InRowID = objPolicyXReInsurer.InRowID;
                    objPolicyXReInsurerNew.ReInsurerEID = objPolicyXReInsurer.ReInsurerEID;


                    objPolicyXReInsurerNew.PreAmount = objPolicyXReInsurer.PreAmount;
                    objPolicyXReInsurerNew.PortionPartLayer = objPolicyXReInsurer.PortionPartLayer;
                    objPolicyXReInsurerNew.PartTotalLayer = objPolicyXReInsurer.PartTotalLayer;
                    objPolicyXReInsurerNew.PaymentAmount = objPolicyXReInsurer.PaymentAmount;
                    objPolicyXReInsurerNew.ScheduleCode = objPolicyXReInsurer.ScheduleCode;
                    objPolicyXReInsurerNew.SpecialCircularMemo = objPolicyXReInsurer.SpecialCircularMemo;
                    objPolicyXReInsurerNew.LayerNumCode = objPolicyXReInsurer.LayerNumCode;
                    objPolicyXReInsurerNew.OccuranceLimit = objPolicyXReInsurer.OccuranceLimit;
                    objPolicyXReInsurerNew.CoverageDesc = objPolicyXReInsurer.CoverageDesc;
                    objPolicyXReInsurerNew.CertificateNumber = objPolicyXReInsurer.CertificateNumber;
                    objPolicyXReInsurerNew.CommentMemo = objPolicyXReInsurer.CommentMemo;

                    strPolicyReInsurerSupp = objPolicyXReInsurer.Supplementals.SerializeObject();
                    if (strPolicyReInsurerSupp != String.Empty)
                    {
                        PropertyStore = new XmlDocument();
                        PropertyStore.LoadXml(strPolicyReInsurerSupp);

                        foreach (XmlElement item in PropertyStore.SelectNodes("/Instance/*"))
                        {
                            sProp = item.LocalName;

                            if (sProp == "Supplementals")
                            {
                                if (objPolicyXReInsurerNew.GetType().GetProperty("Supplementals") != null)
                                    if (objPolicyXReInsurerNew.Supplementals != null)
                                        objPolicyXReInsurerNew.Supplementals.PopulateObject(Utilities.XmlElement2XmlDoc(item), false);
                            }
                        }
                    }
                }
            }

            // npadhy End MITS 18111 Copy the Insurer and Reinsurer Data into new object while Cloning
            foreach (PolicyXMco objPolicyXMco in this.Policy.PolicyXMcoList)
            {
                objPolicyXMcoNew = objPolicy.PolicyXMcoList.AddNew();
                objPolicyXMcoNew.PolicyId = 0;
                objPolicyXMcoNew.McoBeginDate = objPolicyXMco.McoBeginDate;
                objPolicyXMcoNew.McoEid = objPolicyXMco.McoEid;
                objPolicyXMcoNew.McoEndDate = objPolicyXMco.McoEndDate;             
            }


            objPolicy.Premium = this.Policy.Premium;
            objPolicy.PrimaryPolicyFlg = this.Policy.PrimaryPolicyFlg;
            objPolicy.RenewalDate = this.Policy.RenewalDate;
            objPolicy.ReviewDate = this.Policy.ReviewDate;
            objPolicy.SubAccRowId = this.Policy.SubAccRowId;

            //Copy Policy Supplementals
            if (base.m_fda.HasParam("SysPropertyStore"))
                PropertyStore.LoadXml(base.m_fda.SafeParam("SysPropertyStore").InnerXml);            

            foreach (XmlElement item in PropertyStore.SelectNodes("/Instance/Policy/*"))
            {
                sProp = item.LocalName;

                if (sProp == "Supplementals")
                {
                    if (objPolicy.GetType().GetProperty("Supplementals") != null)
                        if (objPolicy.Supplementals != null)
                            objPolicy.Supplementals.PopulateObject(Utilities.XmlElement2XmlDoc(item), false);
                }
            }

            objPolicy.TriggerClaimFlag = this.Policy.TriggerClaimFlag;
            //added by rkaur7 on 05/14/2009 -MITS 16668
            objPolicy.AllStatesFlag = this.Policy.AllStatesFlag;
            //end rkaur7 
            objPolicy.UpdatedByUser = this.Policy.Context.RMUser.LoginName;

            objPolicy.Save();
            this.MoveLast();

            if (objPolicy != null)
            {
                objPolicy.Dispose();
                objPolicy = null;
            }

            if (objDataModelFactory != null)
            {
                objDataModelFactory.Dispose();
                objDataModelFactory = null;
            }

            PropertyStore = null;

            if(objPolicyXMcoNew != null)
            {
                objPolicyXMcoNew.Dispose();
                objPolicyXMcoNew = null;
            }

            if(objPolicyXCvgTypeNew != null)
            {
                objPolicyXCvgTypeNew.Dispose();
                objPolicyXCvgTypeNew = null;
            }

            if (objPolicyXInsurerNew != null)
            {
                objPolicyXInsurerNew.Dispose();
                objPolicyXInsurerNew = null;
            }
            if (objPolicyXReInsurerNew != null)
            {
                objPolicyXReInsurerNew.Dispose();
                objPolicyXReInsurerNew = null;
            }

        }

        private void AppendUnitTypeList()
        {
            XmlDocument objXML = base.SysEx;
            XmlElement objNew = null;
            XmlNode objOld = null;
            XmlElement xmlOption = null;
            objOld = objXML.SelectSingleNode("//UnitTypeList");
            XmlAttribute xmlOptionAttrib = null;
            objNew = objXML.CreateElement("UnitTypeList");
            xmlOption = objXML.CreateElement("option");
            xmlOptionAttrib = objXML.CreateAttribute("value");
            xmlOptionAttrib.InnerText = "V";
            xmlOption.InnerText = "Vehicle";
            xmlOption.Attributes.Append(xmlOptionAttrib);
            objNew.AppendChild(xmlOption);
            xmlOption = objXML.CreateElement("option");
            xmlOption.InnerText = "Property";
            xmlOptionAttrib = objXML.CreateAttribute("value");
            xmlOptionAttrib.InnerText = "P";
            xmlOption.Attributes.Append(xmlOptionAttrib);
            objNew.AppendChild(xmlOption);

            //Added by Amitosh for 27459
            xmlOption = objXML.CreateElement("option");
            xmlOption.InnerText = "Site";
            xmlOptionAttrib = objXML.CreateAttribute("value");
            xmlOptionAttrib.InnerText = "S";
            xmlOption.Attributes.Append(xmlOptionAttrib);
            objNew.AppendChild(xmlOption);
            //To Do for the time Unit Type SU is not supported except external policy
            if (this.Policy.PolicySystemId > 0)
            {
                xmlOption = objXML.CreateElement("option");
                xmlOption.InnerText = "Stat Unit";
                xmlOptionAttrib = objXML.CreateAttribute("value");
                xmlOptionAttrib.InnerText = "SU";
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
            }
            //End Amitosh
            xmlOptionAttrib = null;
            xmlOption = null;
            //Add the XML to the SysExData 
            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);
        }

		/// <summary>
		/// Adds the necessary Bank Account List information to SysExData
		/// </summary>
		/// <remarks>The Bank Account List information is created in a manner
		/// that can be used to populate DropDownLists.  The XPath to the SysExData
		/// will be used for binding to the itemset ref tag in the FDM XML Document
		/// </remarks>
		private void AppendBankAccountList()
		{
			//Variable declarations
			ArrayList arrAcctList = new ArrayList();
			XmlDocument objXML = base.SysEx;
			XmlNode objOld=null;
			XmlElement objNew=null;
			XmlCDataSection objCData=null;

			//Retrieve the security credential information to pass to the FundManager object
			string sDSNName = base.m_fda.userLogin.objRiskmasterDatabase.DataSourceName;
			string sUserName = base.m_fda.userLogin.LoginName;
			string sPassword = base.m_fda.userLogin.Password;
			int iUserId = base.m_fda.userLogin.UserId;
			int iGroupId = base.m_fda.userLogin.GroupId;

            using (FundManager objFundMgr = new FundManager(sDSNName, sUserName, sPassword, iUserId, iGroupId, base.ClientId))
            {
                arrAcctList = objFundMgr.GetAccounts(Policy.Context.InternalSettings.SysSettings.UseFundsSubAcc);
            }

			//Create the necessary SysExData to be used in ref binding
			objOld = objXML.SelectSingleNode("//AccountList");
			objNew = objXML.CreateElement("AccountList");

            // Start Naresh MITS 10009 Blank Value Added in Combo
            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;

            // Blank value for the combo box
            xmlOption = objXML.CreateElement("option");
            xmlOptionAttrib = objXML.CreateAttribute("value");
            xmlOption.Attributes.Append(xmlOptionAttrib);
            objNew.AppendChild(xmlOption);
            xmlOptionAttrib = null;
            xmlOption = null;
            // End Naresh Blank Value Added in Combo

			//Loop through and create all the option values for the combobox control
			foreach (FundManager.AccountDetail item in arrAcctList)
			{
				xmlOption = objXML.CreateElement("option");
				//Create a CData section to handle any possible strange characters
				//in the Account Name that may cause problems with the output XML
				objCData = objXML.CreateCDataSection(item.AccountName);
				xmlOption.AppendChild(objCData);
				xmlOptionAttrib = objXML.CreateAttribute("value");
                //abahl3 Mits 22370
                if (this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                    xmlOptionAttrib.Value = item.SubRowId.ToString();
                else
                    xmlOptionAttrib.Value = item.AccountId.ToString();
                //abahl3 End

              //xmlOptionAttrib.Value = item.AccountId.ToString();
				xmlOption.Attributes.Append(xmlOptionAttrib);
				objNew.AppendChild(xmlOption);
			}//end foreach

			//Add the XML to the SysExData 
			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);

			//Clean up
			arrAcctList = null;
			objOld = null;
			objNew = null;
			objCData = null;
		}//end method AppendBankAccountList()

		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;

			if (  Policy.ExpirationDate.CompareTo(Policy.EffectiveDate) < 0)
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
					Globalization.GetString("Validation.ExpirationDateMustBeGreaterThanEffectiveDate", base.ClientId),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}			

            //Charanpreet for Mits 11323 : Start
            if (Policy.PolicyXInsured.Count != 0)
            { 
                bool bValerror = false;
                bValerror = OrgHierarchyValidate(ref bValerror);
                if (bValerror)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        "Selected Insured(s) Not Within Effective Date Range",
                        BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            //Charanpreet for Mits 11323 : End

			// Return true if there were validation errors
			Cancel = bError;
		}



        //Gagan Safeway Retrofit Policy Jursidiction : START
        /*public override void AfterSave()
        {
            base.AfterSave();

            String sSQL = string.Empty;
            bool bAddAllStates = false;

            if( objData.Context.InternalSettings.SysSettings.ShowJurisdiction != 0  )
            {
                if (Policy.PolicyId > 0 && Policy.AllJurisdictionFlg.ToString().ToLower() == "true")
                {
                    bAddAllStates = true;
                }

                if (bAddAllStates)
                {
                    sSQL = "DELETE FROM POLICY_X_STATE WHERE POLICY_ID=" + Policy.PolicyId;
                    Policy.Context.DbConn.ExecuteNonQuery(sSQL);
                    Policy.PolicyXState.ClearAll();

                    sSQL = "SELECT STATE_ROW_ID FROM STATES WHERE STATE_ROW_ID >0 ORDER BY STATE_ID";
                    using (DbReader objReader = Policy.Context.DbConnLookup.ExecuteReader(sSQL))
                    {
                        while (objReader.Read())
                        {

                            sSQL = "INSERT INTO POLICY_X_STATE (POLICY_ID, STATE_ID) VALUES (" + Policy.PolicyId + ", " + objReader.GetValue("STATE_ROW_ID").ToString() + ")";
                            Policy.Context.DbConn.ExecuteNonQuery(sSQL);
                            //By Vaibhav for Policy MITS#13334
                            Policy.PolicyXState.Add(objReader.GetValue("STATE_ROW_ID").ToString());

                        }
                    }
                }
            }

        }*/
        //Gagan Safeway Retrofit Policy Jursidiction : END

        //added by rkaur7 on 05/15/2009 -MITS 16668
        public override void AfterSave()
        {
            base.AfterSave();

            String sSQL = string.Empty;
            bool bAddAllStates = false;

            if( objData.Context.InternalSettings.SysSettings.ShowJurisdiction != 0  )
            {
                if (Policy.PolicyId > 0 && Policy.AllStatesFlag.ToString().ToLower() == "true")
                {
                    bAddAllStates = true;
                }

                if (bAddAllStates)
                {
                    sSQL = "DELETE FROM POLICY_X_STATE WHERE POLICY_ID=" + Policy.PolicyId;
                    Policy.Context.DbConn.ExecuteNonQuery(sSQL);
                    Policy.PolicyXState.ClearAll();

                    sSQL = "SELECT STATE_ROW_ID FROM STATES WHERE STATE_ROW_ID >0 ORDER BY STATE_NAME";
                    using (DbReader objReader = Policy.Context.DbConnLookup.ExecuteReader(sSQL))
                    {
                        while (objReader.Read())
                        {
                            sSQL = "INSERT INTO POLICY_X_STATE (POLICY_ID, STATE_ID) VALUES (" + Policy.PolicyId + ", " + objReader.GetValue("STATE_ROW_ID").ToString() + ")";
                            Policy.Context.DbConn.ExecuteNonQuery(sSQL);                        
                            Policy.PolicyXState.Add(objReader.GetValue("STATE_ROW_ID").ToString());

                        }
                    }
                }
            }
            DeleteAssociatedCoverages();
        }
        //end rkaur7
        //skhare7 Point Policy Interface
        private void RenderDriverIntList(ref XmlDocument objXML)
        {
            XmlElement objPolicyDriverElement = null;
            XmlElement objPolicyIntElement = null;
            string sCodeId = string.Empty;
            string sCodeIdDr = string.Empty;
            Driver objDriver = (Driver)m_fda.Factory.GetDataModelObject("Driver", false);
            Entity objEntity = (Entity)m_fda.Factory.GetDataModelObject("Entity", false);
            int iTableid = 0, iDriverId = 0;
            bool bIsDriverFound = false;
         
            try
            {

                objPolicyDriverElement = (XmlElement)objXML.SelectSingleNode("/SysExData/PolicyDriverList");
                if (objPolicyDriverElement != null)
                    objPolicyDriverElement.ParentNode.RemoveChild(objPolicyDriverElement);

                CreateElement(objXML.DocumentElement, "PolicyDriverList", ref objPolicyDriverElement);

                objPolicyIntElement = (XmlElement)objXML.SelectSingleNode("/SysExData/PolicyInterestList");
                if (objPolicyIntElement != null)
                    objPolicyIntElement.ParentNode.RemoveChild(objPolicyIntElement);

                CreateElement(objXML.DocumentElement, "PolicyInterestList", ref objPolicyIntElement);
                foreach (PolicyXEntity objPolicyXEntity in Policy.PolicyXEntityList)
                {

                   
                    if (objPolicyXEntity.PolicyId != 0 && objPolicyXEntity.PolicyUnitRowid == 0)
                    {
                        objEntity.MoveTo(objPolicyXEntity.EntityId);
                      
                        //SELECT GT.TABLE_NAME, G.TABLE_ID 
                        //    FROM GLOSSARY_TEXT GT, GLOSSARY G 
                        //    WHERE GT.TABLE_ID=G.TABLE_ID AND G.GLOSSARY_TYPE_CODE=7  ORDER BY GT.TABLE_NAME AND G.TABLE_ID=
                        if (objCache.GetTableName(objPolicyXEntity.TypeCode).ToUpper().CompareTo("DRIVERS") == 0)
                        {
                            //Policy.Context.DbConnLookup.ExecuteReader(
                            //using (DbReader objRdr = DbFactory.GetDbReader(connectionString, "SELECT DRIVER_ROW_ID FROM DRIVER WHERE DRIVER_EID=" + objPolicyXEntity.EntityId))
                            using (DbReader objRdr =Policy.Context.DbConnLookup.ExecuteReader( "SELECT DRIVER_ROW_ID FROM DRIVER WHERE DRIVER_EID=" + objPolicyXEntity.EntityId))
                            {
                                if (objRdr.Read())
                                    iDriverId = Conversion.ConvertObjToInt(objRdr.GetValue(0), base.ClientId);
                            }

                            objDriver.MoveTo(iDriverId);
                            if (string.IsNullOrEmpty(sCodeIdDr) && objPolicyXEntity.PolicyId != 0)
                            {
                                sCodeIdDr = objPolicyXEntity.EntityId + "_" + objDriver.DriverRowId.ToString();
                            }
                            else if (objPolicyXEntity.PolicyId != 0)
                            {
                                sCodeIdDr = string.Concat(sCodeIdDr + " " + objPolicyXEntity.EntityId + "_" + objDriver.DriverRowId.ToString());
                            }

                            CreateAndSetElement(objPolicyDriverElement, "Item", objEntity.FirstName + " " + objEntity.LastName + ":" + objDriver.DriversLicNo.ToString(), objPolicyXEntity.EntityId + "_" + objDriver.DriverRowId.ToString());
                            bIsDriverFound = true;
                        }
                        else
                        {
                           
                            if (string.IsNullOrEmpty(sCodeId) && objPolicyXEntity.PolicyId != 0)
                            {
                                sCodeId = objPolicyXEntity.EntityId.ToString();
                            }
                            else if (objPolicyXEntity.PolicyId != 0)
                            {
                                sCodeId = string.Concat(sCodeId + " " + objPolicyXEntity.EntityId.ToString());
                            }
                          

                            iTableid = objCache.GetTableId(objCache.GetTableName(objPolicyXEntity.TypeCode));
                            CreateAndSetElement(objPolicyIntElement, "Item", objEntity.FirstName + " " + objEntity.LastName + ":" + objCache.GetUserTableName(iTableid), objPolicyXEntity.EntityId.ToString());

                        }
                    }
                }
                if (!bIsDriverFound)
                {
                    base.AddKillNode("policydriver");
                }
               
             
                //objPolicyListElement = (XmlElement)objXML.SelectSingleNode("/SysExData/ClaimPolicyList");
                if (objPolicyDriverElement != null)
                {
                    objPolicyDriverElement.SetAttribute("codeid", sCodeIdDr);
                    CreateElement(objXML.DocumentElement, "PolicyDriverAll", ref objPolicyDriverElement);
                }
                if (objPolicyIntElement != null)
                {
                    objPolicyIntElement.SetAttribute("codeid", sCodeId);
                    CreateElement(objXML.DocumentElement, "PolicyInterestAll", ref objPolicyIntElement);
                }

            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            finally
            {
                objPolicyIntElement = null;
                objPolicyDriverElement = null;

                if (objEntity != null)
                {
                    objEntity.Dispose();
                    objEntity = null;
                }
                if (objDriver != null)
                {
                    objDriver.Dispose();
                    objDriver = null;
                }
            }
        }


        #region Org Hierarchy Validate On Filter Date
        // Charanpreet for MITS 11323 : Start
        private bool OrgHierarchyValidate(ref bool bCancel)
        {
            string sSql = "";
            string sOrg_Trig = "";
            string sSys_dt = "";
            string sEff_start_dt = "";
            string sEff_end_dt = "";

            string p_sInsuredEid = Conversion.ConvertObjToStr(Policy.PolicyXInsured);
            string sInsParentEid = GetParentEid(p_sInsuredEid);

            sSql = "SELECT TRIGGER_DATE_FIELD,EFF_START_DATE,EFF_END_DATE FROM ENTITY WHERE TRIGGER_DATE_FIELD IS NOT NULL AND ENTITY_ID IN (" + sInsParentEid + ")";
            using (DbReader objReader = DbFactory.GetDbReader(Policy.Context.DbConn.ConnectionString, sSql))
            {
                while (objReader.Read())
                {
                    sOrg_Trig = objReader.GetValue(0).ToString();
                    if (sOrg_Trig != "")
                    {
                        sEff_start_dt = objReader.GetValue(1).ToString();
                        sEff_end_dt = objReader.GetValue(2).ToString();
                        if (!(sEff_start_dt == "" && sEff_end_dt == ""))
                        {
                            switch (sOrg_Trig)
                            {
                                case "POLICY.EFFECTIVE_DATE,SYSTEM_DATE":
                                    if (sEff_start_dt != "")
                                            if (Policy.EffectiveDate.CompareTo(sEff_start_dt) < 0)
                                            return (true);
                                    if (sEff_end_dt != "")
                                            if (Policy.EffectiveDate.CompareTo(sEff_end_dt) > 0)
                                            return (true);
                                    break;
                                case "SYSTEM_DATE":
                                    sSys_dt = Conversion.GetDate(DateTime.Now.ToShortDateString());
                                    if (sEff_start_dt != "")
                                        if (sSys_dt.CompareTo(sEff_start_dt) < 0)
                                            return (true);
                                    if (sEff_end_dt != "")
                                        if (sSys_dt.CompareTo(sEff_end_dt) > 0)
                                            return (true);
                                    break;
                            }
                        }
                    }
                }
            }
            //}
            return (false);
        }
        #endregion

        #region Get ParentEids
        /// Name		: GetParentEid
        /// Author		: Prashant J Singh         
        /// Date Created: 05/18/09	
        /// MITS        : 11323
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Fetches the Table Id of the Org Hierarchy
        /// </summary>
        /// <param name="sInsEid">List of Insured</param>
        /// <returns>Entity Ids of parents of insureds</returns>

        private string GetParentEid(string sInsEid)
        {
            StringBuilder sParentEid = new StringBuilder();
            string sTempIns = string.Empty;
            string sParent = string.Empty;
            string sRet = string.Empty;

            string sSQL = string.Empty;	//sql string
            DbReader objReader = null;	//Data reader object

            string[] sInsList = sInsEid.Split(' ');


            try
            {
                foreach (string sInsId in sInsList)
                {
                    sTempIns = sInsId;

                    if (sTempIns != "0" && !String.IsNullOrEmpty(sTempIns))
                    {
                        sParentEid.Append(sTempIns);
                        sParentEid.Append(" ,");
                    }

                    while (sTempIns != "0" && !String.IsNullOrEmpty(sTempIns))
                    {
                        sSQL.Remove(0, sSQL.Length);
                        sSQL = "SELECT PARENT_EID FROM ENTITY WHERE ENTITY_ID =" + sTempIns;

                        //Fetch the Details for this Department
                        objReader = DbFactory.GetDbReader(Policy.Context.DbConn.ConnectionString, sSQL);

                        if (objReader.Read())
                        {
                            sParent = Conversion.ConvertObjToStr(objReader.GetValue("PARENT_EID"));
                            if (sParent != "0" && !String.IsNullOrEmpty(sParent))
                            {
                                sParentEid.Append(sParent);
                                sTempIns = sParent;
                                sParentEid.Append(" ,");
                            }
                            else if (sParent == "0")
                                sTempIns = sParent;
                        }

                        objReader.Close();
                    }
                }
                sRet = sParentEid.ToString().TrimEnd(',');
            }
    
            catch (DataModelException p_objExp)
            {
                throw p_objExp;
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("FormDataAdaptor.PolicyEnh.GetParentEid.Error", base.ClientId), p_objExp);
            }
            finally
            {
                if (objReader != null) objReader.Dispose();
            }

            return sRet;
        }

        #endregion


        /// Name		: AppendRuntimeAttribute
        /// Author		: Amitosh Bahl       
        /// Date Created: 01/13/2011	
        /// MITS        : 22370
        /// <summary>
        /// appends the runtime ref in the xml
        /// </summary>
        /// <param name="p_objParentNode"></param>
        /// <param name="p_sControlName"></param>
        /// <param name="p_sAttributeName"></param>
        /// <param name="p_sValue"></param>
        private void AppendRuntimeAttribute(XmlElement p_objParentNode, string p_sControlName, string p_sAttributeName, string p_sValue)
        {
            XmlElement objControlNode = null;
            XmlElement objAttributeNode = null;

            objControlNode = p_objParentNode.OwnerDocument.CreateElement(p_sControlName);
            objAttributeNode = p_objParentNode.OwnerDocument.CreateElement(p_sAttributeName);
            objAttributeNode.SetAttribute("value", p_sValue);
            objControlNode.AppendChild(objAttributeNode);
            p_objParentNode.AppendChild(objControlNode);
        }

       
	}
}
