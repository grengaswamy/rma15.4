using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
    /// Summary description for Leave Plan Screen
	/// </summary>
	public class LeavePlanForm : DataEntryFormBase
	{
		const string CLASS_NAME = "LeavePlan";
		const string FILTER_KEY_NAME = "LPRowId";
		private LeavePlan LeavePlan{get{return objData as LeavePlan;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
	
		public LeavePlanForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		public override void InitNew()
		{
			base.InitNew();

            //Set Filter For LPRowId
			LeavePlan.LPRowId =base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME);
			if (LeavePlan.LPRowId >0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + LeavePlan.LPRowId;
		}
		
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
            //Add Leave Plan name to form
            string sName = string.Empty; 
            //Call the base implementation of OnUpdateForm from the FormBase class
			base.OnUpdateForm ();
            if (this.LeavePlan.LeavePlanName != null)
                sName = " [ " + this.LeavePlan.LeavePlanName.ToString() + " ]";

            if (sName != "")
            {//    this.SysView.SelectSingleNode("//form/@title").InnerText = this.SysView.SelectSingleNode("//form/@title").InnerText + " [ " + sName + " ]";

                ArrayList singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                base.AddElementToList(ref singleRow, "id", "formsubtitle");
                base.AddElementToList(ref singleRow, "Text", sName);
                base.m_ModifiedControls.Add(singleRow);
            }

            if (!(LeavePlan.LPRowId > 0) && LeavePlan.IsNew)
            {
                int iDayCodeId = 0;
                iDayCodeId = objCache.GetCodeId("D", "DURATION_TYPE");
                this.LeavePlan.BenePeriodType = iDayCodeId;
                this.LeavePlan.PerPeriodType = iDayCodeId;
                this.LeavePlan.AllowedPeriodType = iDayCodeId;
            }
            //Deb:added for mediaview
            if (LeavePlan.Context.InternalSettings.SysSettings.UseMediaViewInterface)
            {
                base.AddKillNode("attachdocuments");
                base.AddKillNode("attach");
            }
            //Deb:added for mediaview
		}
		
	}
}