﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Text;


namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PiEmployee Screen.
	/// </summary>
	public class PiEmployeeForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PiEmployee";
		private PiEmployee PiEmployee{get{return objData as PiEmployee;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "EventId";
        const string FILTER_KEY_NAME2 = "ParentRowId";
        const string FILTER_KEY_NAME3 = "ParentTableName";
		private int m_iEventId = 0;
        private int m_iParentRowId = 0;
        private string m_sParentTableName = string.Empty;
        //Neha Start
       int iUseCaseMgmt = 0;
       //neha End
		private string sConnectionString = null; // Mob apps
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
		private bool IsDuplicateSSN()
		{//MGaba2:MITS 9052
			XmlNode eltDupe =null;
			string sSQL="";        


			//User already saw dup popup and is choosing to proceed.
            if(base.SysEx.SelectSingleNode("//DupSSN")!=null && base.SysEx.SelectSingleNode("//DupSSN").InnerText.Trim()=="AfterIgnore")
				return false;
            DbConnection objDbConnection = null;

            if(PiEmployee.PiEntity.EntityId.ToString()!="0")
            {
                sSQL = "SELECT TAX_ID FROM ENTITY WHERE ENTITY_ID = " + PiEmployee.PiEntity.EntityId.ToString();
			    objDbConnection = DbFactory.GetDbConnection(PiEmployee.Context.DbConn.ConnectionString);
			    if(objDbConnection != null)
			    	objDbConnection.Open();
                string sTaxId = Conversion.ConvertObjToStr(objDbConnection.ExecuteScalar(sSQL));
                if (sTaxId == PiEmployee.PiEntity.TaxId)
                    return false;
                 objDbConnection.Close();
            }
            if (PiEmployee.PiEntity.TaxId.Trim() != "")//Neha It will be empty in case we are calling the injury info from Injury screen
            {
                //sSQL = "SELECT TAX_ID FROM ENTITY WHERE TAX_ID = " + Utilities.FormatSqlFieldValue(PiEmployee.PiEntity.TaxId.Trim()) + " AND ENTITY_ID <> " + PiEmployee.PiEntity.EntityId.ToString();
                sSQL = "SELECT TAX_ID FROM ENTITY WHERE TAX_ID = " + Utilities.FormatSqlFieldValue(PiEmployee.PiEntity.TaxId.Trim()) + " AND ENTITY_ID <> " + PiEmployee.PiEntity.EntityId.ToString() + " AND DELETED_FLAG = 0 ";
                objDbConnection = DbFactory.GetDbConnection(PiEmployee.Context.DbConn.ConnectionString);
                if (objDbConnection != null)
                    objDbConnection.Open();
                Object objDuplicate = objDbConnection.ExecuteScalar(sSQL);
                objDbConnection.Close();
                if (objDuplicate != null)
                {
                    base.ResetSysExData("DupSSN", "");
                    eltDupe = this.SysEx.SelectSingleNode("//DupSSN");
                    eltDupe.InnerText = "BeforeIgnore";
                    return true;
                }
            }
           
			return false;
			
		}
        public override void Init()
        {
            base.Init();
            iUseCaseMgmt = Conversion.ConvertStrToInteger(PiEmployee.Context.DbConn.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'CASE_MGT_FLAG'"));
        }
		public override void InitNew()
		{
            string sFilter = string.Empty;
			base.InitNew(); 

            //rsushilaggar JITA 11736  
            //this.m_iEventId = base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);           
            this.m_iParentRowId = base.GetSysExDataNodeInt("/SysExData/ParentRowId", false);
            this.m_sParentTableName = base.GetFormVarNodeText("SysFormPForm");

            this.PiEmployee.ParentRowId = this.m_iParentRowId;

            //-----sgupta320:Jira 15676
            switch ((this.m_sParentTableName).ToUpper())
            {
                case "CLAIMGC":
                    m_SecurityId = RMO_GC_PERSONINVOLVED;
                    this.PiEmployee.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiEmployee.ParentTableName + "' ";
                    break;
                case "CLAIMDI":
                    m_SecurityId = RMO_DI_PERSONINVOLVED;
                    this.PiEmployee.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiEmployee.ParentTableName + "' ";
                    break;
                case "CLAIMPC":
                    m_SecurityId = RMO_PC_PERSONINVOLVED;
                    this.PiEmployee.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiEmployee.ParentTableName + "' ";
                    break;
                case "CLAIMWC":
                    m_SecurityId = RMO_WC_PERSONINVOLVED;
                    this.PiEmployee.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiEmployee.ParentTableName + "' ";
                    break;
                case "CLAIMVA":
                    m_SecurityId = RMO_VA_PERSONINVOLVED;
                    this.PiEmployee.ParentTableName = "CLAIM";
                    m_iEventId = CommonFunctions.GetEventIdByClaimId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiEmployee.ParentTableName + "' ";
                    break;
                case "CLAIMXPOLICY":
                    m_SecurityId = RMO_POLICY_PERSONINVOLVED;
                    this.PiEmployee.ParentTableName = "POLICY";
                    m_iEventId = CommonFunctions.GetEventIdByPolicyId(this.m_iParentRowId, this.sConnectionString);
                    sFilter = objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiEmployee.ParentTableName + "' ";
                    break;
                default:
                    // Defaults to Event's person involved
                    m_iEventId = m_iParentRowId;
                    this.PiEmployee.ParentTableName = "EVENT";
                    sFilter = "(" + objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_iEventId.ToString() + " OR (" + objData.PropertyNameToField(FILTER_KEY_NAME2) + "=" + this.m_iParentRowId + " AND " + objData.PropertyNameToField(FILTER_KEY_NAME3) + " = '" + this.PiEmployee.ParentTableName + "'))";
                    break;
            }
			//Nikhil Garg	03/02/2006 
			//Moved from problematic Code below (Not to save before user presses Save button)
			int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid",false);
			sFilter += " AND PI_EID=" + iPiEid;

			(objData as INavigation).Filter = sFilter;
			
			this.PiEmployee.EventId = this.m_iEventId;
			this.PiEmployee.PiEid = iPiEid;

			if (iPiEid>0) //Neha
			{
                //this is  to check if front end is employee or injury.
                string sSQL = "SELECT EMPLOYEE_EID FROM EMPLOYEE where EMPLOYEE_EID =" + iPiEid;
                 int iEmplEID = 0;
                 iEmplEID = this.PiEmployee.Context.DbConnLookup.ExecuteInt(sSQL);

                 if (iEmplEID > 0)
                 {
                     Employee objEmployee =
                         (Employee)this.PiEmployee.Context.Factory.GetDataModelObject("Employee", false);

                     objEmployee.MoveTo(iEmplEID);
                     objEmployee.CopyToPiEmployee(this.PiEmployee);
                     objEmployee = null;
                 }
			}//Neh end

            //rsushilaggar JIRA 7767
            if (PiEmployee.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (PiEmployee.PiEntity.EntityId <= 0 && PiEmployee.PiEntity.NameType <= 0)
                    PiEmployee.PiEntity.NameType = objCache.GetCodeId("IND", "ENTITY_NAME_TYPE");
            }
		}



        public PiEmployeeForm(FormDataAdaptor fda)
            : base(fda)
		{
			base.m_ClassName = CLASS_NAME;
			sConnectionString = fda.connectionString; // Ishan Mobile Apps
            sLangCode = Convert.ToString(fda.userLogin.objUser.NlsCode);
		}

        //Shruti for 10779
        public override void BeforeSave(ref bool Cancel)
        {
            int iRoleTableId = this.PiEmployee.Context.LocalCache.GetTableId(Globalization.IndividualGlossaryTableNames.EMPLOYEES.ToString());
            this.m_iParentRowId = base.GetSysExDataNodeInt("/SysExData/ParentRowId", false);
            if ((this.SecurityId == RMB_WC_PI && base.CurrentAction == enumFormActionType.Save && 
                !(this.m_objData as PiEmployee).IsNew && !PiEmployee.Context.RMUser.IsAllowedEx(RMO_WC_EMPLOYEE, RMO_UPDATE)) ||
                (this.SecurityId == RMB_WC_PI && base.CurrentAction == enumFormActionType.Save &&
                !(this.m_objData as PiEmployee).IsNew && !PiEmployee.Context.RMUser.IsAllowedEx(RMPermissions.RMO_EMPLOYEE, RMO_UPDATE)))
            {
                Cancel = true;
                LogSecurityError(RMO_UPDATE);
                return;
            }
            // Anjaneya : MITS 9941
            int iPiEid = base.GetSysExDataNodeInt("/SysExData/PiEid", false);
            // Anjaneya : MITS 12476
            if (iPiEid > 0 && (this.m_objData as PiEmployee).IsNew)
            {
                //avipinsrivas Start : Worked for Jira-340
                //string sSQL = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE EVENT_ID=" + this.m_iEventId + " AND PI_EID=" + iPiEid;
                string sSQL = string.Empty;
                if (this.PiEmployee.Context.InternalSettings.SysSettings.UseEntityRole)
                    sSQL = string.Format("SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PARENT_ROW_ID= {0} AND PI_EID= {1} AND ROLE_TABLE_ID= {2} AND  PARENT_TABLE_NAME = '{3}' ", this.m_iParentRowId, iPiEid, iRoleTableId, this.PiEmployee.ParentTableName);
                else
                    sSQL = string.Format("SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PARENT_ROW_ID= {0} AND PI_EID= {1} AND  PARENT_TABLE_NAME = '{2}' ", this.m_iParentRowId, iPiEid, this.PiEmployee.ParentTableName);
                  
                int iPiRowId = 0;
                iPiRowId = this.PiEmployee.Context.DbConnLookup.ExecuteInt(sSQL);
                if (iPiRowId > 0)
                {
                    // PI Record already exists	
                    Cancel = true;
                    // pgupta215: RMA-18997
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), string.Format(Globalization.GetString("Validation.EmployeeAlreadyExistsAsPI", base.ClientId, sLangCode), this.PiEmployee.ParentTableName.ToLower()), BusinessAdaptorErrorType.Error);
                    return;
                }
               // int iPiErRowID = 0;
                //if (this.PiEmployee.Context.InternalSettings.SysSettings.UseEntityRole)
                //{
                //    int iEntityTableID = this.PiEmployee.Context.LocalCache.GetTableId(Globalization.PersonInvolvedGlossaryTableNames.EMPLOYEE.ToString());
                //  //  iPiErRowID = this.PiEmployee.PiEntity.IsEntityRoleExists(this.PiEmployee.PiEntity.EntityXRoleList, iEntityTableID);
                //}
                //if (base.CheckPIEntity(this.m_iEventId, iPiEid, iPiErRowID, Globalization.PersonInvolvedGlossaryTableNames.EMPLOYEE.ToString(), this.PiEmployee.Context.DbConn.ConnectionString, this.PiEmployee.Context.InternalSettings.SysSettings.UseEntityRole, sLangCode))
                //{
                //    Cancel = true;
                //    return;
                //}
                //avipinsrivas End
            }
            // Anjaneya : MITS 9941 ends
            //MITS 9052:Start
           // if (PiEmployee.IsNew && IsDuplicateSSN())
            if (IsDuplicateSSN())   //MGaba2:MITS 9052
            {
                Cancel = true;
                return;
            }
            //MITS 9052:End            
        }
        //Shruti for 10779 ends
        public override void AfterSave()
        {
            //MGaba2:MITS 9052
            base.ResetSysExData("DupSSN", "");            
        }

        
       
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
			XmlDocument objXML = null;
			XmlElement objWeeksPerMonth = null;       



			if (this.SecurityId!=RMB_WC_PI && this.SecurityId!=RMB_DI_PI)
			{
				if(!PiEmployee.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_PI_EMPLOYEE))
				{
					LogSecurityError(RMO_ACCESS);
					return;
				}
			}
            //Shruti for 10779
            //----sgupta320 Jira-17835 remove condition
            if ((this.SecurityId == RMB_WC_PI && base.CurrentAction == enumFormActionType.AddNew &&
                (this.m_objData as PiEmployee).EmployeeNumber == "" && !PiEmployee.Context.RMUser.IsAllowedEx(RMO_WC_EMPLOYEE, RMO_CREATE)) || 
                (this.SecurityId == RMB_WC_PI && base.CurrentAction == enumFormActionType.AddNew &&
                (this.m_objData as PiEmployee).EmployeeNumber == "" && !PiEmployee.Context.RMUser.IsAllowedEx(RMPermissions.RMO_EMPLOYEE, RMO_CREATE))) // added for RMA-15715
            {
                LogSecurityError(RMO_CREATE);
                return;
            }
            
            //Shruti for 10779 ends

			XmlDocument objSysExDataXmlDoc = base.SysEx;

			string sSubTitleText = string.Empty; 
			
			// Add EventNumber node, if already not present, to avoid Orbeon 'ref' error 			
			XmlNode objEventNumberNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/EventNumber");

			if(objEventNumberNode==null)
			{
                //Deb MITS 27711 
                if (this.PiEmployee.Context.InternalSettings.SysSettings.UseAcrosoftInterface)
                {
                    string sEventnumber = this.PiEmployee.Context.DbConnLookup.ExecuteString("SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + this.PiEmployee.EventId);
                    base.CreateSysExData("EventNumber", sEventnumber);
                }
                else
                {
                    base.CreateSysExData("EventNumber");
                }
                //Deb MITS 27711 
			}
			else
			{
				if(objEventNumberNode.InnerText.Length>0)
					sSubTitleText = " [" + objEventNumberNode.InnerText + "]"; 
				objEventNumberNode = null;
			}

            //Commented for RMA:9558 --Starts
            //Payal: RMA-9148 --Starts
            //if (this.PiEmployee.Context.InternalSettings.SysSettings.EditWCEmpNum) 
            //       base.AddReadWriteNode("employeenumber");
            //else
            //       base.AddReadOnlyNode("employeenumber");
            //Payal: RMA-9148 --Ends
            //Commented for RMA:9558 --Ends

			// Add ClaimNumber node, if already not present, to avoid Orbeon 'ref' error
			XmlNode objClaimNumberNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/ClaimNumber");

			if(objClaimNumberNode==null)
			{
				base.CreateSysExData("ClaimNumber"); 
			}			
			else
			{
				if(objClaimNumberNode.InnerText.Length>0)
					sSubTitleText = " [" + objClaimNumberNode.InnerText + "]"; 
				objClaimNumberNode = null;
			}

			// TODO - To find a better way to ascertain which is our starting page (Event or Claim)
			// As of now, first event number and then claim number is checked for subtitle text.
			base.ResetSysExData("SubTitle",sSubTitleText);
            
            //MGaba2:MITS 22114:Comments screen showing title as "Piemployee Comments undefined" :Start
            //Shifting the code below as event number is required
            //mona
            //ArrayList singleRow = new ArrayList();
            //base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            //base.AddElementToList(ref singleRow, "id", "formsubtitle");
            //base.AddElementToList(ref singleRow, "Text", sSubTitleText);
            //base.m_ModifiedControls.Add(singleRow);
            //MGaba2:MITS 22114:End
           //Mits 14165:Asif Start
            if (this.PiEmployee != null && Convert.ToString(this.PiEmployee.PiEid) != "")
            {
                if (this.PiEmployee.PiEid > 0)
                {
                    this.CurrentAction = enumFormActionType.ApplyLookupData;
                }
            }
            //Mits 14165:Asif End
			// Defect no. 1468 week days should get checked automatically, according to the utilities settings
			if(this.CurrentAction == enumFormActionType.AddNew)
			{
				PiEmployee.WorkFriFlag=PiEmployee.Context.InternalSettings.SysSettings.WorkFri;
				PiEmployee.WorkMonFlag=PiEmployee.Context.InternalSettings.SysSettings.WorkMon;
				PiEmployee.WorkSatFlag=PiEmployee.Context.InternalSettings.SysSettings.WorkSat;
				PiEmployee.WorkSunFlag=PiEmployee.Context.InternalSettings.SysSettings.WorkSun;
				PiEmployee.WorkThuFlag=PiEmployee.Context.InternalSettings.SysSettings.WorkThu;
				PiEmployee.WorkTueFlag=PiEmployee.Context.InternalSettings.SysSettings.WorkTue;
				PiEmployee.WorkWedFlag=PiEmployee.Context.InternalSettings.SysSettings.WorkWed;
			}

			// Set WorkFlags Node
			string sWorkFlags = string.Empty; 

			sWorkFlags =		PiEmployee.WorkSunFlag + ","
							+	PiEmployee.WorkMonFlag + "," 
							+	PiEmployee.WorkTueFlag + "," 
							+	PiEmployee.WorkWedFlag + "," 
							+	PiEmployee.WorkThuFlag + "," 
							+	PiEmployee.WorkFriFlag + "," 
							+	PiEmployee.WorkSatFlag;

			base.ResetSysExData("WorkFlags",sWorkFlags);							

            //objSysExDataXmlDoc = null;

			CheckAutoGenerateEmpNo();

			// Shruti 16-Mar-2007 Mits 9052
            //MITS 9052:Start          
            if (base.SysEx.SelectSingleNode("//DupSSN") == null) //MGaba2:MITS 9052      
                base.ResetSysExData("DupSSN", "");            
            //MITS 9052:End
//            base.ResetSysExData("Entityid", PiEmployee.PiEid.ToString());

            //Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.  
            Event objEvent = (Event)this.PiEmployee.Context.Factory.GetDataModelObject("Event", false);
            string sEventDateFromDB = string.Empty;
            if (objEvent != null)
            {
                int iEventId = PiEmployee.EventId;
                if (iEventId != 0)
                {
                    objEvent.MoveTo(iEventId);
                    sEventDateFromDB = objEvent.DateOfEvent;
                    if (!String.IsNullOrEmpty(sEventDateFromDB))
                    {
                        if (base.SysEx.SelectSingleNode("DateOfEvent") == null)
                        {
                            string sUIEventDate = string.Empty;
                            sUIEventDate = Conversion.ToDate(sEventDateFromDB).ToString("MM/dd/yyyy");
                            base.CreateSysExData("DateOfEvent", sUIEventDate);
                        }
                    }
                    //MGaba2:MITS 22114:Comments screen showing title as "Piemployee Comments undefined" :Start
                   // sSubTitleText = string.Format(" [{0} * {1} {2}]", objEvent.EventNumber, PiEmployee.PiEntity.FirstName, PiEmployee.PiEntity.LastName);
                    switch (PiEmployee.ParentTableName.ToUpper())  //ijain4 : JIRA 17480 : 20/11/2015
                    {
                        //case "CLAIMGC":
                        //case "CLAIMDI":
                        //case "CLAIMPC":
                        //case "CLAIMWC":
                        case "CLAIM":
                            Claim objClaim = (Claim)this.PiEmployee.Context.Factory.GetDataModelObject("Claim", false);
                            objClaim.MoveTo(PiEmployee.ParentRowId);
                            sSubTitleText = string.Format(" [{0} * {1} {2}]", objClaim.ClaimNumber, PiEmployee.PiEntity.FirstName, PiEmployee.PiEntity.LastName);
                            break;
                        case "POLICY":
                            Policy objPolicy = (Policy)this.PiEmployee.Context.Factory.GetDataModelObject("Policy", false);
                            objPolicy.MoveTo(PiEmployee.ParentRowId);
                            sSubTitleText = string.Format(" [{0} * {1} {2}]", objPolicy.PolicyNumber, PiEmployee.PiEntity.FirstName, PiEmployee.PiEntity.LastName);
                            break;
                        default:
                            sSubTitleText = string.Format(" [{0} * {1} {2}]", objEvent.EventNumber, PiEmployee.PiEntity.FirstName, PiEmployee.PiEntity.LastName);
                            break;
                    }

                    //ijain4 : JIRA 17480 : 20/11/2015 End
                    ArrayList singleRow = new ArrayList();
                    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                    base.AddElementToList(ref singleRow, "id", "formsubtitle");
                    base.AddElementToList(ref singleRow, "Text", sSubTitleText);
                    base.m_ModifiedControls.Add(singleRow);
                    //MGaba2:MITS 22114:End
                }
            }
            //Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.  
			if (PiEmployee.PiEntity.BirthDate!=string.Empty)
				base.ResetSysExData("EmployeeAge",Utilities.CalculateAgeInYears(PiEmployee.PiEntity.BirthDate,sEventDateFromDB)); //Added sEventDateFromDB for MITS 19315
			else
				base.ResetSysExData("EmployeeAge","");

			if(!PiEmployee.Context.RMUser.IsAllowedEx(m_SecurityId,FormBase.RMO_VIEW_HOSPITAL_INFO))
				base.AddKillNode("hospitallist");
			if(!PiEmployee.Context.RMUser.IsAllowedEx(m_SecurityId,FormBase.RMO_VIEW_PHYSICIAN_INFO))
				base.AddKillNode("physicianslist");
			if(!PiEmployee.Context.RMUser.IsAllowedEx(m_SecurityId,FormBase.RMO_VIEW_DIAGNOSIS_INFO))
				base.AddKillNode("diagnosislist");
			if(!PiEmployee.Context.RMUser.IsAllowedEx(m_SecurityId,FormBase.RMO_VIEW_TREATMENT_INFO))
				base.AddKillNode("treatmentlist");
            //smishra25: MITS 24395, MITS 13748 implementation was missing
            //nadim for 13748 hide/unhide ssn field
            //pyadav25 - MITS 31338 - 04/24/2013 Add support for View SSN permission
            if (PiEmployee.PiEid > 0 && !PiEmployee.Context.RMUser.IsAllowedEx(m_SecurityId, FormBase.RMO_PI_EMP_VIEW_SSN))
                base.AddKillNode("taxid");

            //nadim for 13748 hide/unhide ssn field

            if (!PiEmployee.Context.RMUser.IsAllowedEx(m_SecurityId, FormBase.RMO_ENABLE_SALARY))
            {
                base.AddKillNode("paytypecode");
                base.AddKillNode("payamount");
                base.AddKillNode("hourlyrate");
                base.AddKillNode("weeklyhours");
                base.AddKillNode("weeklyrate");
                base.AddKillNode("monthlyrate");
                //nkaranam2 - MITS : 34643
                base.AddKillNode("dailyhours");
                base.AddKillNode("dailyrate");
                base.AddKillNode("collateralsources");
                base.AddKillNode("collateraloffset");
                //nkaranam2 - MITS : 34643
            }

           
			if (this.PiEmployee.PiRowId > 0 || this.PiEmployee.PiEid <= 0)
				base.AddKillNode("javascript_SetDirtyFlag");

            //MITS 20237 Add support for View SSN permission
            if (PiEmployee.PiEid > 0 && !PiEmployee.Context.RMUser.IsAllowedEx(RMPermissions.RMO_EMPLOYEE, RMPermissions.RMO_EMPLOYEE_VIEW_SSN))
            {
                base.AddKillNode("taxid");
            }

			//Check for People Maintenance permission
			PeoplePermissionChecks4PI("piemployee", m_SecurityId+RMO_UPDATE);
            //neha 
            if (iUseCaseMgmt == -1)
            {
            }
            else
            {
                base.AddKillNode("casemanagementlist");
            }
			//Raman Bhatia - LTD Phase 3 changes : Weeks Per Month should be passed via a node in SYSEX
			string sSQL =	"SELECT NO_WEEKS_PER_MONTH FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = 844";

			DbReader objReader = null;
            using (objReader = objData.Context.DbConnLookup.ExecuteReader(sSQL))
            {

                objXML = base.SysEx;
                objWeeksPerMonth = objXML.CreateElement("weekspermonth");
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        objWeeksPerMonth.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }

                }
            }
			objXML.DocumentElement.AppendChild(objWeeksPerMonth);
            //if(objReader!=null)
            //    objReader.Close();
            // Add DiaryMessage node, if already not present, to avoid Orbeon 'ref' error
           
            XmlNode objDiaryMessageNode = objSysExDataXmlDoc.SelectSingleNode("/SysExData/DiaryMessage");

            if (objDiaryMessageNode == null)
            {
                base.CreateSysExData("DiaryMessage");
            }
			//Shruti Choudhary updated code for mits 10384
            //if (this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[2]/SysFormName") != null)
            //{
            //    ((XmlElement)base.SysView.SelectSingleNode("//internal[@name='SysFormPForm']")).SetAttribute("value",
            //        this.SysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[2]/SysFormName").InnerText);
            //}

            objSysExDataXmlDoc = null;

            //MITS 28903: Raman Bhatia
            //Injury info has been moved to new screen under piemployee.. hence removing tab
            base.AddKillNode("injuryinfo");

            if (PiEmployee.PolicyUnitRowId != 0)
            {
                DisplayUnitNo();

            }
            else
            {
                base.AddKillNode("Unitno");
            
            }
            //added by neha goel MITS# 36916: PMC gap 7 CLUE fields - RMA - 5499
            if (!PiEmployee.Context.InternalSettings.SysSettings.UseCLUEReportingFields)
            {
                base.AddKillNode("ClueSubjIdentifier");
                base.AddKillNode("ClueSubjRemovalIdentifier");
            }
            else
            {
                base.AddDisplayNode("ClueSubjIdentifier");
                base.AddDisplayNode("ClueSubjRemovalIdentifier");
            }
            //end by neha goel MITS#36916 PMC CLUE gap 7- RMA - 5499
            //rsushilaggar JIRA 7767
            if (this.PiEmployee.Context.InternalSettings.SysSettings.UseEntityRole)
            {
                if (this.PiEmployee != null && this.PiEmployee.PiEntity != null && this.PiEmployee.PiEntity.NameType > 0)
                    this.ResetSysExData("EntityType", objCache.GetCodeDesc(this.PiEmployee.PiEntity.NameType));
                else
                    this.ResetSysExData("EntityType", objCache.GetCodeDesc(objCache.GetCodeId("IND", "ENTITY_NAME_TYPE")));
            }
            else
            {
                this.AddKillNode("entitytype");
            }
            //end rushilaggar

            //----sgupta320: Jira-17668
            if (base.FormVariables.SelectSingleNode("//SysSid") != null)
                base.FormVariables.SelectSingleNode("//SysSid").InnerText = Convert.ToString(m_SecurityId);
		}

		internal override bool ValidateRequiredViewFields(XmlDocument propertyStore)
		{
			CheckAutoGenerateEmpNo();

			return base.ValidateRequiredViewFields (propertyStore);
		}

		private void CheckAutoGenerateEmpNo()
		{
			//Bug No: 636,649,672. For Autogenerating Employee Number
			//Nikhil Garg		Dated: 20/Dec/2005
			XmlElement objNotReqNew = null;
			string sNotReqNew=string.Empty;
			if (objData.Context.InternalSettings.SysSettings.AutoNumWCEmp)
			{
                objNotReqNew = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysNotReqNew");
                sNotReqNew = objNotReqNew.InnerText;
				if (sNotReqNew.IndexOf("employeenumber")==-1)
					sNotReqNew=sNotReqNew + "employeenumber|";
                objNotReqNew.InnerText = sNotReqNew;
			}
		}
		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;
			string sSQL = string.Empty;
			DbConnection objDbConnection=null;

			Event objEvent = (Event) this.PiEmployee.Context.Factory.GetDataModelObject("Event",false);    
			objEvent.MoveTo(this.m_iEventId);   			
			string sEventDateFromDB = objEvent.DateOfEvent;  
			
			// Ideally this date formatting should be done by presentation server
			// depending upon user locale. We are formatting it for validation error message display. 
			string sEventDate = Conversion.ToDate(sEventDateFromDB).ToString("MM/dd/yyyy");

			string sToday = Conversion.ToDbDate(System.DateTime.Now);
			string sTime = Conversion.GetTime(System.DateTime.Now.ToShortTimeString());
            //rsushilaggar MITS 18991 Date 10/05/2010 
            string sHiredDateText = Globalization.GetString("Field.DateHired", base.ClientId, sLangCode) + ": " + Conversion.ToDate(PiEmployee.DateHired).ToString("MM/dd/yyyy");
            string sBirthDateText = Globalization.GetString("Field.BirthDate", base.ClientId, sLangCode) + ": " + Conversion.ToDate(PiEmployee.PiEntity.BirthDate).ToString("MM/dd/yyyy");
			
			if(PiEmployee.PiEntity.BirthDate!="")
			{
				if(PiEmployee.PiEntity.BirthDate.CompareTo(sToday)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId, sLangCode), Globalization.GetString("Field.BirthDate", base.ClientId, sLangCode), System.DateTime.Now.ToShortDateString()),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				if(PiEmployee.PiEntity.BirthDate.CompareTo(sEventDateFromDB)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanEventDate", base.ClientId, sLangCode), Globalization.GetString("Field.BirthDate", base.ClientId, sLangCode), sEventDate),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}
			
			
			if(PiEmployee.DateHired!="")
			{
				if(PiEmployee.DateHired.CompareTo(sToday)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId, sLangCode), Globalization.GetString("Field.DateHired", base.ClientId, sLangCode), System.DateTime.Now.ToShortDateString()),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				if(PiEmployee.DateHired.CompareTo(PiEmployee.PiEntity.BirthDate)<0)
				{
                    //rsushilaggar MITS 18991 Date 10/05/2010
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.DateHired", base.ClientId, sLangCode), sBirthDateText),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				if(PiEmployee.DateHired.CompareTo(sEventDateFromDB)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanEventDate", base.ClientId, sLangCode), Globalization.GetString("Field.DateHired", base.ClientId, sLangCode), sEventDate),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}

			if(PiEmployee.TermDate!="")
			{
				if(PiEmployee.TermDate.CompareTo(sToday)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId, sLangCode), Globalization.GetString("Field.TermDate", base.ClientId, sLangCode), System.DateTime.Now.ToShortDateString()),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				if(PiEmployee.TermDate.CompareTo(PiEmployee.PiEntity.BirthDate)<0)
				{
                    //rsushilaggar MITS 18991 Date 10/05/2010
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.TermDate", base.ClientId, sLangCode), sBirthDateText),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				if(PiEmployee.TermDate.CompareTo(PiEmployee.DateHired)<0)
				{
                    //rsushilaggar MITS 18991 Date 10/05/2010
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.TermDate", base.ClientId, sLangCode), sHiredDateText),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				if(PiEmployee.TermDate.CompareTo(sEventDateFromDB)<0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate", base.ClientId, sLangCode), Globalization.GetString("Field.TermDate", base.ClientId, sLangCode), sEventDate),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				
			}
			
			if(PiEmployee.DateOfDeath!="")
			{
				if(PiEmployee.DateOfDeath.CompareTo(sToday)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId, sLangCode), Globalization.GetString("Field.DateOfDeath", base.ClientId, sLangCode), System.DateTime.Now.ToShortDateString()),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				if(PiEmployee.DateOfDeath.CompareTo(PiEmployee.PiEntity.BirthDate)<0)
				{
                    //rsushilaggar MITS 18991 Date 10/05/2010
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.DateOfDeath", base.ClientId, sLangCode), sBirthDateText),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				if(PiEmployee.DateOfDeath.CompareTo(PiEmployee.DateHired)<0)
				{
                    //rsushilaggar MITS 18991 Date 10/05/2010
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.DateOfDeath", base.ClientId, sLangCode), sHiredDateText),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				
			}

			if(PiEmployee.DateDriverslicexp!="")
			{				
				if(PiEmployee.DateDriverslicexp.CompareTo(PiEmployee.PiEntity.BirthDate)<0)
				{
                    //rsushilaggar MITS 18991 Date 10/05/2010
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.DateDriverslicexp", base.ClientId, sLangCode), sBirthDateText),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}

			if(PiEmployee.LastVerifiedDate!="")
			{
				if(PiEmployee.LastVerifiedDate.CompareTo(sToday)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId, sLangCode), Globalization.GetString("Field.LastVerifiedDate", base.ClientId, sLangCode), System.DateTime.Now.ToShortDateString()),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
				if(PiEmployee.LastVerifiedDate.CompareTo(PiEmployee.PiEntity.BirthDate)<0)
				{
                    //rsushilaggar MITS 18991 Date 10/05/2010
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), sBirthDateText),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}

			if(PiEmployee.WorkPermitDate!="")
			{				
				if(PiEmployee.WorkPermitDate.CompareTo(PiEmployee.PiEntity.BirthDate)<0)
                {//rsushilaggar MITS 18991 Date 10/05/2010
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.WorkPermitDate", base.ClientId, sLangCode), sBirthDateText),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}

            //Neha begin
            //akaur9 Field.FileClosingdate changed to Field.FileClosingDate 
            if (PiEmployee.FileClosingdate != "")
            {
                if (PiEmployee.FileClosingdate.CompareTo(sToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId, sLangCode), Globalization.GetString("Field.FileClosingDate", base.ClientId, sLangCode), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
                if (PiEmployee.FileClosingdate.CompareTo(PiEmployee.PiEntity.BirthDate) < 0)
                {
                    //rsushilaggar MITS 18991 Date 10/05/2010
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.FileClosingDate", base.ClientId, sLangCode), sBirthDateText),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
                if (PiEmployee.FileClosingdate.CompareTo(sEventDateFromDB) <0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate", base.ClientId, sLangCode), Globalization.GetString("Field.FileClosingDate", base.ClientId, sLangCode), sEventDate),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }

            switch (objData.Context.InternalSettings.CacheFunctions.GetShortCode(PiEmployee.DisabilityCode))
            {
                case "ILL":
                    if (PiEmployee.IllnessCode == 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                            String.Format(Globalization.GetString("Validation.IllnessCodeIsRequired", base.ClientId, sLangCode), Globalization.GetString("Field.IllnessCode", base.ClientId, sLangCode)),
                            BusinessAdaptorErrorType.Error);

                        bError = true;
                    }
                    break;
                case "INJ":
                    if (PiEmployee.InjuryCodes.Count == 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                            String.Format(Globalization.GetString("Validation.InjuryTypeCodeIsRequired", base.ClientId, sLangCode), Globalization.GetString("Field.InjuryCodes", base.ClientId, sLangCode)),
                            BusinessAdaptorErrorType.Error);

                        bError = true;
                    }
                    break;
            }
            //Neha End

			//shruti, 14th mar 2007, MITS 7863 starts
			try
			{

                //if (PiEmployee.IsNew) Commented By Charanpreet for MITS 9052 - rework
                if (PiEmployee != null && PiEmployee.PiEid == 0)  // Added  MITS 9052
                {


                    sSQL = "SELECT EMPLOYEE_NUMBER FROM EMPLOYEE WHERE EMPLOYEE_NUMBER = " + Utilities.FormatSqlFieldValue(PiEmployee.EmployeeNumber.Trim());
                    objDbConnection = DbFactory.GetDbConnection(PiEmployee.Context.DbConn.ConnectionString);
                    if (objDbConnection != null)
                        objDbConnection.Open();
                    Object objDuplicate = objDbConnection.ExecuteScalar(sSQL);
                    if (objDuplicate != null)
                    {
						//akaur9 08/23/2011 Changed the code for Mobile Adjuster Employee Number Issue start------
						//added the node to detect the emp number conflict in the upload service
						XmlElement objCaller = null;
						objCaller = (XmlElement)base.FormVariables.SelectSingleNode("//caller");
						if (objCaller != null)
						{
							if (objCaller.InnerText == "MobileAdjuster" && base.SysEx.SelectSingleNode("//DupEmpNumber") != null)
							{                                
								base.SysEx.SelectSingleNode("//DupEmpNumber").InnerText = "True";
							}
						}
						//akaur9 End----------
                       // dbisht6 jira 1131 adding hack to avoid chinese character in the error message
					    string[] dataseprator = {"~*~"};
                        string sReturnMsg = null;
                        string[] errormsg = Globalization.GetString("Field.EmployeeNumber", base.ClientId, sLangCode).Split(dataseprator, StringSplitOptions.None);
                       // string[] errormsg = Regex.Split(Globalization.GetString("Field.EmployeeNumber"),dataseprator, StringSplitOptions.None);
                       
                        UserLogin userlgn = new UserLogin(base.ClientId) ;
                        string iUserLangCode = userlgn.objUser.NlsCode.ToString();
                        foreach (string s in errormsg)
                        {
                            //if (s.Contains(iUserLangCode))//vkumar258 RMA-14072 
                            //{
                                sReturnMsg = s;
                                break;
                            //}
                        }
                        if (!string.IsNullOrEmpty(sReturnMsg))
                        {
                            Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                                String.Format(Globalization.GetString("Validation.Duplicate", base.ClientId, sLangCode), sReturnMsg, PiEmployee.EmployeeNumber),
                                BusinessAdaptorErrorType.Error);
                        }
						// jira 1131 ends
                        bError = true;
                    }

                    objDbConnection.Close();
                }
                //else      Commented By Charanpreet for MITS 9052 - rework
                //{
                //    sSQL = "SELECT EMPLOYEE_NUMBER FROM EMPLOYEE WHERE EMPLOYEE_NUMBER = " + Utilities.FormatSqlFieldValue(PiEmployee.EmployeeNumber.Trim());
                //    sSQL += "AND EMPLOYEE_EID <> " + PiEmployee.PiEid.ToString();
                //    objDbConnection = DbFactory.GetDbConnection(PiEmployee.Context.DbConn.ConnectionString);
                //    if (objDbConnection != null)
                //        objDbConnection.Open();
                //    Object objDuplicate = objDbConnection.ExecuteScalar(sSQL);
                //    if (objDuplicate != null)
                //    {
                //        Errors.Add(Globalization.GetString("ValidationError"),
                //            String.Format(Globalization.GetString("Validation.Duplicate"), Globalization.GetString("Field.EmployeeNumber"), PiEmployee.EmployeeNumber),
                //            BusinessAdaptorErrorType.Error);
                //        bError = true;
                //    }

                //    objDbConnection.Close();
                //}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				if(objDbConnection != null)
					objDbConnection.Dispose();
			}
			objEvent = null;

			// Return true if there were validation errors
			Cancel = bError;
		}

        public override void BeforeDelete(ref bool Cancel)
        {
            try
            {
                // MGaba2:MITS 15632: Primary employee cannot be deleted:start
                string sSql = "SELECT EVENT_ID FROM CLAIM WHERE CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT WHERE CLAIMANT_EID = " + PiEmployee.PiEntity.EntityId + " AND PRIMARY_CLMNT_FLAG = -1)";
                using (DbReader objReader = DbFactory.GetDbReader(PiEmployee.Context.DbConn.ConnectionString, sSql))
                {
                    while (objReader.Read())
                    {
                        if (PiEmployee.EventId == Conversion.ConvertObjToInt64(objReader.GetValue("EVENT_ID"), base.ClientId))
                        {
                            string sFullName = "";
                            if (PiEmployee.PiEntity.FirstName != "")
                            {
                                sFullName = PiEmployee.PiEntity.LastName + "," + PiEmployee.PiEntity.FirstName;
                            }
                            else
                            {
                                sFullName = PiEmployee.PiEntity.LastName;
                            }
                            objReader.Close();
                            objReader.Dispose();
                            Cancel = true;
                            Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                                "Employee " + sFullName + " can't be deleted as it is Primary Employee for this claim.",
                                BusinessAdaptorErrorType.Error);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
 
            }
            // MGaba2:MITS 15632:End

        }
      
        //Added Rakhi for R7:Add Emp Data Elements
        public override void OnUpdateObject()
        {
            base.OnUpdateObject();
            XmlDocument objXML = base.SysEx;
			XmlElement objCaller = null;
                       
			// Ishan Mobile Apps
			objCaller = (XmlElement)base.FormVariables.SelectSingleNode("//caller");
			if (objCaller != null)
			{
				if (objCaller.InnerText == "MobileAdjuster")
				{
					string sSQL = " SELECT EVENT_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + base.GetSysExDataNodeText("//ClaimNumber") + "'";
					using (DbReader objReader = DbFactory.ExecuteReader(sConnectionString, sSQL))
					{
						if (objReader.Read())
						{
							PiEmployee.EventId = Conversion.ConvertObjToInt(objReader.GetValue("EVENT_ID"), base.ClientId);
							this.m_iEventId = PiEmployee.EventId;
							//base.FormVariables.SelectSingleNode("//SysExData//EventId").InnerText = objReader.GetValue("EVENT_ID").ToString();
							if (base.FormVariables.SelectSingleNode("//SysExData//EventId") != null)
							{
								base.FormVariables.SelectSingleNode("//SysExData//EventId").InnerText = objReader.GetValue("EVENT_ID").ToString();

							}

						}
					}					
				}
			}
		   // Ishan end
           if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
            {
                PiEmployee.PiEntity.FormName = "PiEmployeeForm"; //Mits 22497

                #region "Updating Address Info Object"

                string sAddr1 = PiEmployee.PiEntity.Addr1;
                string sAddr2 = PiEmployee.PiEntity.Addr2;
                string sAddr3 = PiEmployee.PiEntity.Addr3;// JIRA 6420 pkandhari
                string sAddr4 = PiEmployee.PiEntity.Addr4;// JIRA 6420 pkandhari
                string sCity = PiEmployee.PiEntity.City;
                int iCountryCode = PiEmployee.PiEntity.CountryCode;
                int iStateId = PiEmployee.PiEntity.StateId;
                string sEmailAddress = PiEmployee.PiEntity.EmailAddress;
                string sFaxNumber = PiEmployee.PiEntity.FaxNumber;
                string sCounty = PiEmployee.PiEntity.County;
                string sZipCode = PiEmployee.PiEntity.ZipCode;
                //RMA-8753 nshah28(Added by ashish)
                string sSearchString = string.Empty;
                 //RMA-8753 nshah28(Added by ashish) END
                if (
                        sAddr1 != string.Empty || sAddr2 != string.Empty || sAddr3 != string.Empty || sAddr4 != string.Empty ||
                        sCity != string.Empty || iCountryCode != 0 || iStateId != 0 || sEmailAddress != string.Empty ||
                        sFaxNumber != string.Empty || sCounty != string.Empty || sZipCode != string.Empty
                    )
                {
                    if (PiEmployee.PiEntity.EntityXAddressesList.Count == 0)
                    {


                        EntityXAddresses objEntityXAddressesInfo = PiEmployee.PiEntity.EntityXAddressesList.AddNew();
						//RMA-8753 nshah28(Added by ashish) START
                        objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                        objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                        objEntityXAddressesInfo.Address.Addr3 = sAddr3;// JIRA 6420 pkandhari
                        objEntityXAddressesInfo.Address.Addr4 = sAddr4;// JIRA 6420 pkandhari
                        objEntityXAddressesInfo.Address.City = sCity;
                        objEntityXAddressesInfo.Address.Country = iCountryCode;
                        objEntityXAddressesInfo.Address.State = iStateId;
                        objEntityXAddressesInfo.Email = sEmailAddress;
                        objEntityXAddressesInfo.Fax = sFaxNumber;
                        objEntityXAddressesInfo.Address.County = sCounty;
                        objEntityXAddressesInfo.Address.ZipCode = sZipCode;
						//RMA-8753 nshah28(Added by ashish) END
                        objEntityXAddressesInfo.EntityId = PiEmployee.PiEntity.EntityId;
                        objEntityXAddressesInfo.PrimaryAddress = -1;
                        //objEntityXAddressesInfo.AddressId = -1;
                        AddressForm objAddressForm = new AddressForm(m_fda);
                        sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                        objEntityXAddressesInfo.Address.SearchString = sSearchString;
                        objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, PiEmployee.Context.RMDatabase.ConnectionString, base.ClientId);
                        objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;

                    }
                    else
                    {
                        foreach (EntityXAddresses objEntityXAddressesInfo in PiEmployee.PiEntity.EntityXAddressesList)
                        {
                            if (objEntityXAddressesInfo.PrimaryAddress == -1)
                            {
								//RMA-8753 nshah28(Added by ashish) START
                                objEntityXAddressesInfo.Address.Addr1 = PiEmployee.PiEntity.Addr1;
                                objEntityXAddressesInfo.Address.Addr2 = PiEmployee.PiEntity.Addr2;
                                objEntityXAddressesInfo.Address.Addr3 = PiEmployee.PiEntity.Addr3;// JIRA 6420 pkandhari
                                objEntityXAddressesInfo.Address.Addr4 = PiEmployee.PiEntity.Addr4;// JIRA 6420 pkandhari
                                objEntityXAddressesInfo.Address.City = PiEmployee.PiEntity.City;
                                objEntityXAddressesInfo.Address.Country = PiEmployee.PiEntity.CountryCode;
                                objEntityXAddressesInfo.Address.State = PiEmployee.PiEntity.StateId;
                                objEntityXAddressesInfo.Email = PiEmployee.PiEntity.EmailAddress;
                                objEntityXAddressesInfo.Fax = PiEmployee.PiEntity.FaxNumber;
                                objEntityXAddressesInfo.Address.County = PiEmployee.PiEntity.County;
                                objEntityXAddressesInfo.Address.ZipCode = PiEmployee.PiEntity.ZipCode;
                                AddressForm objAddressForm = new AddressForm(m_fda);
                                sSearchString = objAddressForm.SearchStringForAddress(objEntityXAddressesInfo.Address, objData.Context.LocalCache);
                                objEntityXAddressesInfo.Address.SearchString = sSearchString;
                                objEntityXAddressesInfo.AddressId = CommonFunctions.CheckAddressDuplication(sSearchString, PiEmployee.Context.RMDatabase.ConnectionString, base.ClientId);
                                objEntityXAddressesInfo.Address.AddressId = objEntityXAddressesInfo.AddressId;
								//RMA-8753 nshah28(Added by ashish) END
                                break;
                            }

                        }
                    }
                }
                else
                {
                    if (PiEmployee.PiEntity.EntityXAddressesList.Count > 0)
                    {
                        foreach (EntityXAddresses objEntityXAddressesInfo in PiEmployee.PiEntity.EntityXAddressesList)
                        {
                            if (objEntityXAddressesInfo.PrimaryAddress == -1)
                            {
                               // PiEmployee.PiEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                                PiEmployee.PiEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.RowId); //RMA-8753 nshah28(Added by ashish)
                                break;
                            }

                        }
                    }

                }
            #endregion

                #region Added for updating Phone Numbers

                string sOfficePhone = PiEmployee.PiEntity.Phone1.Trim();
                string sHomePhone = PiEmployee.PiEntity.Phone2.Trim();
                int iHomeCode = objCache.GetCodeId("h", "PHONES_CODES");
                int iOfficeCode = objCache.GetCodeId("o", "PHONES_CODES");
                bool bOfficePhoneEntered = false;
                AddressXPhoneInfo objAddressesInfo = null;
                bool bOfficeCodeExists = false;
                bool bHomePhoneEntered = false;
                bool bHomeCodeExists = false;

                if (sOfficePhone != string.Empty)
                    bOfficePhoneEntered = true;
                if (sHomePhone != string.Empty)
                    bHomePhoneEntered = true;

                if (PiEmployee.PiEntity.AddressXPhoneInfoList.Count == 0)
                {
                    if (bOfficePhoneEntered)
                    {
                        objAddressesInfo = PiEmployee.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iOfficeCode;
                        objAddressesInfo.PhoneNo = sOfficePhone;
                    }
                    if (bHomePhoneEntered)
                    {
                        objAddressesInfo = PiEmployee.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iHomeCode;
                        objAddressesInfo.PhoneNo = sHomePhone;
                    }
                }
                else
                {
                    foreach (AddressXPhoneInfo objXAddressesInfo in PiEmployee.PiEntity.AddressXPhoneInfoList)
                    {
                        if (objXAddressesInfo.PhoneCode == iOfficeCode)
                        {
                            if (bOfficePhoneEntered)
                            {
                                objXAddressesInfo.PhoneNo = sOfficePhone;
                                bOfficeCodeExists = true;
                            }
                            else
                            {
                                PiEmployee.PiEntity.AddressXPhoneInfoList.Remove(objXAddressesInfo.PhoneId);
                            }
                            continue;
                        }
                        else if (objXAddressesInfo.PhoneCode == iHomeCode)
                        {
                            if (bHomePhoneEntered)
                            {
                                objXAddressesInfo.PhoneNo = sHomePhone;
                                bHomeCodeExists = true;
                            }
                            else
                            {
                                PiEmployee.PiEntity.AddressXPhoneInfoList.Remove(objXAddressesInfo.PhoneId);
                            }
                        }
                    }
                    if (bOfficePhoneEntered && !bOfficeCodeExists)
                    {
                        objAddressesInfo = PiEmployee.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iOfficeCode;
                        objAddressesInfo.PhoneNo = sOfficePhone;
                    }
                    if (bHomePhoneEntered && !bHomeCodeExists)
                    {
                        objAddressesInfo = PiEmployee.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iHomeCode;
                        objAddressesInfo.PhoneNo = sHomePhone;
                    }
                }

                #endregion
          
            }
        }
        private void DisplayUnitNo()
        {

            string sStaUnitNo = string.Empty;
            string sRiskLoc = string.Empty;
            string sSubLoc = string.Empty;

            StringBuilder sbSql = new StringBuilder();
            // RMA-10039: Ash, fixed error but in general this is incorrect handling for POINT, Integral, Staging
            sbSql.Append("SELECT POINT_UNIT_DATA.STAT_UNIT_NUMBER, POINT_UNIT_DATA.UNIT_RISK_LOC, POINT_UNIT_DATA.UNIT_RISK_SUB_LOC from POLICY_X_UNIT,POINT_UNIT_DATA,PERSON_INVOLVED ");
            sbSql.Append(" WHERE POLICY_X_UNIT.POLICY_UNIT_ROW_ID=PERSON_INVOLVED.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.UNIT_ID=POINT_UNIT_DATA.UNIT_ID ");
            sbSql.Append(" AND POINT_UNIT_DATA.UNIT_TYPE=POLICY_X_UNIT.UNIT_TYPE AND PERSON_INVOLVED.PI_ROW_ID=" + PiEmployee.PiRowId);

            DbReader objReader = null;
            objReader = DbFactory.GetDbReader(base.m_fda.connectionString, sbSql.ToString());
            if (objReader != null)
            {
                if (objReader.Read())
                {
                    sStaUnitNo = objReader.GetValue("STAT_UNIT_NUMBER").ToString();
                    sRiskLoc = objReader.GetValue("UNIT_RISK_LOC").ToString();
                    if (sRiskLoc == string.Empty)
                        sRiskLoc = "-";

                    sSubLoc = objReader.GetValue("UNIT_RISK_SUB_LOC").ToString();
                    if (sSubLoc == string.Empty)
                        sSubLoc = "-";

                    base.ResetSysExData("UnitNo", sStaUnitNo + "/" + sRiskLoc + "/" + sSubLoc);


                }

            }


        }
      
	}
}
