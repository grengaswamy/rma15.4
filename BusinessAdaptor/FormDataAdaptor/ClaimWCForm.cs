﻿///********************************************************************
/// Amendment History
///********************************************************************
/// Date Amended   *            Amendment               *    Author
/// 03/06/2014         MITS 34260 - duplicate claim         Ngupta36
/// 05/22/2014         MITS 33573 - claims Made Handling    dagrawal
/// 08/20/2014         RMA 1209 -   ICD 10 Code not getting deleted achouhan3
/// 08/22/2014         RMA 1308 -   ICD 9 Code was replaceed by ICD10   achouhan3
///********************************************************************
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using Riskmaster.Security.RMApp;
using System.Data;
using System.Text;
using Riskmaster.BusinessAdaptor.AutoFroiAcord;
using System.Collections.Generic;
using Riskmaster.Application.PolicySystemInterface;
using Riskmaster.Application.ImagRightWrapper;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// Summary description for ClaimWC Screen.
    /// </summary>
    public class ClaimWCForm : DataEntryFormBase
    {
        const string CLASS_NAME = "Claim";
        private int m_LOB;
        private LobSettings objLobSettings = null;
        private bool bCloseDiaries = false;
        //Changed by Gagan for MITS 11451 : Start
        bool bDeleteAutoChecks = false;
        //Changed by Gagan for MITS 11451 : End
        string sUserCloseDiary = "false";
        string m_ClaimLimitFilter = "";
        string[] sDaysOfWeek = new string[7];
        int iClaimId = 0;
        int iUseCaseMgmt = 0;
        //BOB Enhancement Reserves : Start
        int iUseUnitStat = 0;
        //BOB Enhancement Reserves : End

        int iCaseMgmtHisId = 0;
        //Mridul 05/28/09 MITS 16745-Chubb ACK Letter Enhancement
        bool bTriggerLetter = false;
        UserLoginLimits objLimits = null;
        private SysSettings objSysSettings = null;
        private ArrayList arrPoliciesDeleted = new ArrayList();
        //tanwar2 - mits 30910 - start
        HashSet<int> hSetPoliciesAdded = null;
        //tanwar2 - mits 30910 - end

        //vsharma205 Jira-111623 Starts
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
        //vsharma205 Jira-111623 ends

        int ClmStatus = 0;//JIRA RMA-11122 ajohari2 
        public struct Holidays
        {
            internal string sDate;
            internal string sDesc;
            internal long[] lOrgID;
        }
        public Holidays[] SYS_HOLIDAYS;

        private Claim objClaim { get { return objData as Claim; } }
        private LocalCache objCache { get { return objData.Context.LocalCache; } }

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="fda"></param>
        public ClaimWCForm(FormDataAdaptor fda)
            : base(fda)
        {
            base.m_ClassName = CLASS_NAME;
            m_LOB = fda.Factory.Context.LocalCache.GetCodeId("WC", "LINE_OF_BUSINESS");
            base.m_PreInitHandler = new PreInitHandler(this.CustomPreInitHandler);
            objLimits = base.GetUserLoginLimits();
        }

        //BSB 10.10.2006 If present, this handler will be called by DM during object construction 
        // just prior to invoking any client initialization scripts.
        public void CustomPreInitHandler(object objSrc, Riskmaster.DataModel.InitObjEventArgs e)
        {
            Claim objClaim = (objSrc as Claim);
            objClaim.LineOfBusCode = m_LOB;
            this.m_ParentId = base.GetSysExDataNodeInt("/SysExData/EventId");
            objClaim.EventId = m_ParentId;
            //BSB 01.25.2007 Defeat the built-in PrimaryPI selection of DM
            //by force loading it.
            objClaim.HookPrimaryPiEmployeeByPiRowId(base.GetSysExDataNodeInt("/SysExData/PiRowId"));

            //Log.Write("CustomPreInitHandler Called");
            // Now DM will go off and run customer coded Init Scripts with the same information
            // that RMWorld used to provide.
        }
        public override Hashtable GetTrimHashTable()
        {
            Hashtable ht = new Hashtable();
            ht.Add("Comments", "Comments");
            ht.Add("HTMLComments", "HTMLComments");
            ht.Add("AdjusterList", "AdjusterList");
            ht.Add("ClaimantList", "ClaimantList");
            ht.Add("LitigationList", "LitigationList");
            ht.Add("DefendantList", "DefendantList");
            ht.Add("UnitList", "UnitList");
            ht.Add("ClaimStatusHistList", "ClaimStatusHistList");
            ht.Add("TimeAndExpenseList", "TimeAndExpenseList");
            ht.Add("FundsList", "FundsList");
            ht.Add("ReserveCurrentList", "ReserveCurrentList");
            ht.Add("ProgressNoteList", "ProgressNoteList");
            return ht;
        }
        public override void InitNew()
        {
            base.InitNew();

            m_LOB = objClaim.Context.LocalCache.GetCodeId("WC", "LINE_OF_BUSINESS");
            objLobSettings = objClaim.Context.InternalSettings.ColLobSettings[m_LOB];
            objSysSettings = objClaim.Context.InternalSettings.SysSettings;
            this.m_ParentId = base.GetSysExDataNodeInt("/SysExData/EventId");

            objClaim.LineOfBusCode = m_LOB;
            objClaim.NavType = Riskmaster.DataModel.ClaimNavType.ClaimNavLOB;

            if (m_ParentId > 0)
                (objData as INavigation).Filter = "EVENT_ID=" + m_ParentId;

            objClaim.EventId = m_ParentId;
        }

        /// <summary>
        /// Author: Sumit Kumar
        /// Date: 09/23/2010
        /// MITS: 21919
        /// </summary>
        /// <returns></returns>
        public override bool AddNew()
        {
            try
            {
                bool bIsClaimWCEnabled = objClaim.Context.InternalSettings.SysSettings.UseWCLOB;
                if (bIsClaimWCEnabled)
                {
                    base.AddNew();
                }
                else
                {
                    //base.LogSecurityError(FormBase.RMO_ACCESS); - May use this if decided in future.
                    Errors.Add(Globalization.GetString("PermissionFailure", base.ClientId), Globalization.GetString("Claim.AccessPermission.error", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Claim.GetUtilitySetting.error", base.ClientId), p_objException);
            }
            return true;
        }

        // BSB 11.09.2005 Work Around for ClaimId jumping to -1 when this object creates a new parent and 
        // is added to it's claimlist.  We would like client side javascript to be able to assume
        // that only "ClaimId==0" means new record.
        public override void AfterAddNew()
        {
            base.AfterAddNew();
            objClaim.ClaimId = 0;
        }
        public override void BeforeDelete(ref bool Cancel)  // Added by csingh7 R6 Claim Comment Enhancement
        {
            iClaimId = objClaim.ClaimId;
        }
        //Mukul Added 2/2/07 MITS 8760
        public override void AfterDelete()
        {
            base.AfterDelete();
            if (iClaimId != 0)
            {
                objClaim.Context.DbConnLookup.ExecuteNonQuery(
                   String.Format(@"DELETE FROM COMMENTS_TEXT 
                WHERE ATTACH_RECORDID = {0} AND ATTACH_TABLE = 'CLAIM'", iClaimId));
                //  Start -  Added by Nikhil on 07/14/14 to delete deductable details on claim delete
                DeleteClaimDeductiblesForDeletedClaim(iClaimId);
                //  End -  Added by Nikhil on 07/14/14 to delete deductable details on claim delete

            }
            base.ResetSysExData("EventId", "0");
            objClaim.EventId = 0;
        }

        public override void Init()
        {
            base.Init();

            //Filter Innaccessible Claim types based on UserLimit Security.
            if (objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ClmAccLmtFlag)//If UserLimits Enabled...
            {
                if (objLimits != null)
                {
                    string[,] arr = objLimits.ClaimStatusTypeAccessList(objClaim.LineOfBusCode);

                    if (arr.Length != 0)
                    {
                        m_ClaimLimitFilter = "";
                        string sTypeFilter = "";
                        string sTypeFilter1 = "";
                        string sTypeFilter2 = "";

                        for (int i = 0; i < Convert.ToInt32(arr.Length / 2); i++)
                        {
                            if (arr[i, 0] != "0" && arr[i, 1] != "0")
                            {
                                if (i > 0 && sTypeFilter != "")
                                    sTypeFilter += " AND ";
                                sTypeFilter += "NOT(CLAIM_TYPE_CODE =" + arr[i, 0];
                                sTypeFilter += " AND ";
                                sTypeFilter += "CLAIM_STATUS_CODE =" + arr[i, 1];
                                sTypeFilter += ")";
                            }
                            else if (arr[i, 0] != "0" || arr[i, 1] != "0")
                            {
                                if (arr[i, 0] != "0")
                                {
                                    if (i > 0 && sTypeFilter1 != "")
                                        sTypeFilter1 += ",";
                                    sTypeFilter1 += arr[i, 0];
                                }
                                else
                                {
                                    if (i > 0 && sTypeFilter2 != "")
                                        sTypeFilter2 += ",";
                                    sTypeFilter2 += arr[i, 1];
                                }
                            }
                        }
                        if (sTypeFilter != "")
                            m_ClaimLimitFilter += sTypeFilter;

                        if (sTypeFilter1 != "")
                        {
                            if (m_ClaimLimitFilter != "")
                                m_ClaimLimitFilter += " AND " + "CLAIM_TYPE_CODE NOT IN (" + sTypeFilter1 + ")";
                            else
                                m_ClaimLimitFilter += "CLAIM_TYPE_CODE NOT IN (" + sTypeFilter1 + ")";
                        }
                        if (sTypeFilter2 != "")
                        {
                            if (m_ClaimLimitFilter != "")
                                m_ClaimLimitFilter += " AND " + "CLAIM_STATUS_CODE NOT IN (" + sTypeFilter2 + ")";
                            else
                                m_ClaimLimitFilter += "CLAIM_STATUS_CODE NOT IN (" + sTypeFilter2 + ")";
                        }
                    }

                    if (m_ClaimLimitFilter != "")
                    {
                        if (m_ParentId > 0) //Geeta 09/19/07 : Modified for MITS 10377
                        {
                            (objClaim as DataModel.INavigation).Filter += " AND " + m_ClaimLimitFilter;
                        }
                        else
                        {
                            (objClaim as DataModel.INavigation).Filter += m_ClaimLimitFilter;
                        }
                    }
                }
            }


            //rsushilaggar Changes made for performance improvement
            //iUseCaseMgmt = Conversion.ConvertStrToInteger(objClaim.Context.DbConn.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'CASE_MGT_FLAG'"));
            iUseCaseMgmt = objClaim.Context.InternalSettings.SysSettings.CaseMgmtFlag;

            //BOB Reserve Enhancement
            //iUseUnitStat = Conversion.ConvertStrToInteger(objClaim.Context.DbConn.ExecuteString("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'UNIT_STAT'"));
            iUseUnitStat = objClaim.Context.InternalSettings.SysSettings.UnitStat;
            if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == 0)
                iUseUnitStat = 0;
            //rsushilaggar 
            //			This Code has been moved into LoadPiEmployeeChilds() under Primary Case Manager Region;
            //			foreach(CmXCmgrHist objCmXCmgrHist in objClaim.CaseManagement.CaseMgrHistList)
            //			{
            //				if(objCmXCmgrHist.PrimaryCmgrFlag == -1)
            //				{
            //					iCaseMgmtHisId=objCmXCmgrHist.CmcmhRowId;
            //				}
            //			}
        }


        //Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
        public override void OnUpdateForm()
        {

            base.OnUpdateForm();

            XmlDocument objXmlCustom = null;                  // Umesh
            XmlNode objSessionDSN = null;                       // Umesh
            XmlDocument objXML = base.SysView;                  //Umesh
            XmlDocument objSysViewSec = null;
            objSysViewSec = base.SysViewSection;
            base.CreateSysExData("CheckPolicyValidation", "");
            //For new claim, InitCClaim custom script could set value for Comments.
            //If LegacyComments is true, we need to bring value back
            XmlDocument objSysExData = base.SysEx;
            objClaim.GetCommentsXml(ref objSysExData, objClaim.ClaimId);

            //Geeta 05/07/07 Starts :Mits 9310 
            XmlNode objPVJuris = null;
            XmlNode objJuris = null;
            //Start by Shivendu for AWW Calc
            XmlElement objJurisDiction = null;
            string sJurisCodeId = null;
            //if (base.m_fda.HasParam("SysPropertyStore"))
            //    objJurisDiction = base.m_fda.SafeParam("SysPropertyStore");
            //Mridul 05/28/09 MITS 16745 - Pass value of Template for Claim Letter 
            base.CreateSysExData("ClaimLetterTmplId", "0");

            //smishra54: Policy Interface
            base.ResetSysExData("DownloadedEntitiesIds", "0");
            //smishra54:End

            base.ResetSysExData("hdnEditClaimEvtDate", objClaim.Context.InternalSettings.SysSettings.EditclaimEvtDate.ToString());//sharishkumar jira 12444
            if (objClaim != null)
            {

                // sJurisCodeId = objJurisDiction.SelectSingleNode("//Instance/Claim/FilingStateId").Attributes["codeid"].Value;
                sJurisCodeId = objClaim.FilingStateId.ToString();

                //Code changed for R5..Raman Bhatia
                /*
                objJurisDiction = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empweeklyrate1']");
                if (objJurisDiction != null)
                {
                    //Arizona,Colorado,Nevada and Wyoming should have monthly rate not weekly 
                    if (sJurisCodeId == "4" || sJurisCodeId == "8" || sJurisCodeId == "31" || sJurisCodeId == "60")
                    {
                        objJurisDiction.Attributes["title"].Value = "Monthly Rate";
                    }
                }
                */

                //Arizona,Colorado,Nevada and Wyoming should have monthly rate not weekly 
                if (sJurisCodeId == "4" || sJurisCodeId == "8" || sJurisCodeId == "31" || sJurisCodeId == "60")
                {

                    ArrayList singleRow = new ArrayList();
                    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                    base.AddElementToList(ref singleRow, "id", "lbl_empweeklyrate1");
                    base.AddElementToList(ref singleRow, "Text", "Monthly Rate");
                    base.m_ModifiedControls.Add(singleRow);
                }
                else
                {
                    ArrayList singleRow = new ArrayList();
                    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                    base.AddElementToList(ref singleRow, "id", "lbl_empweeklyrate1");
                    base.AddElementToList(ref singleRow, "Text", "Weekly Rate");
                    base.m_ModifiedControls.Add(singleRow);
                }

            }
            //End by Shivendu for AWW Calc

            if (this.SysView.SelectSingleNode("//form/@viewid") != null)
            {

                objPVJuris = objXML.SelectSingleNode("//group[@name='pvjurisgroup']");
                objJuris = objSysViewSec.SelectSingleNode("//group[@name='jurisgroup']");

                if (objPVJuris != null && objJuris != null)
                {
                    {
                        objJuris.Attributes["title"].InnerText = objPVJuris.Attributes["title"].InnerText;
                    }

                    objPVJuris.ParentNode.RemoveChild(objPVJuris);
                }
                else if (objPVJuris != null)
                {
                    objPVJuris.ParentNode.RemoveChild(objPVJuris);
                }
                else if (objJuris != null)
                {
                    objJuris.ParentNode.RemoveChild(objJuris);
                }
            }
            //Geeta Mits 9310 Ends

            // 03/01/2007 REM  to customize the page flow : Umesh
            objXmlCustom = new XmlDocument();
            XmlElement objXmlElement = null;
            //using (DbReader objRdr = RMSessionManager.GetCustomizedContent("customize_custom"))//R5 PS2 Merge
            string strContent = RMSessionManager.GetCustomContent(base.ClientId);//R5 PS2 Merge
            if (!string.IsNullOrEmpty(strContent))//R5 PS2 Merge
            {
                //if (objRdr.Read())//R5 PS2 Merge
                //{//R5 PS2 Merge
                //objXmlCustom.LoadXml(objRdr.GetString("CONTENT"));//R5 PS2 Merge
                objXmlCustom.LoadXml(strContent);//R5 PS2 Merge
                //objRdr.Dispose();
                //Added by Shivendu to do a null check
                if (objXmlCustom.SelectSingleNode("//SpecialSettings/Show_AdjusterList") != null)
                    if (objXmlCustom.SelectSingleNode("//SpecialSettings/Show_AdjusterList").InnerText == "-1")
                    {
                        //Raman Bhatia: Commented code for R5..
                        //Customizations would be handled in a different manner now at Page level

                        //objXmlElement = objXML.SelectSingleNode("//button[@name='btnAdjusters']") as XmlElement;
                        //if (objXmlElement != null)
                        //    objXmlElement.Attributes["param"].Value = "SysFormPIdName=claimid&SysFormName=claimadjusterlist&SysCmd=1&SysViewType=controlsonly&SysFormIdName=adjrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber";
                        //objXmlElement = null;

                        ArrayList singleRow = new ArrayList();
                        base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.FormButton.ToString());
                        base.AddElementToList(ref singleRow, "id", "btnAdjusters");
                        //base.AddElementToList(ref singleRow, "param", "SysFormPIdName=claimid&SysFormName=claimadjusterlist&SysCmd=1&SysViewType=controlsonly&SysFormIdName=adjrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber");
                        base.AddElementToList(ref singleRow, "param", "SysFormPIdName=claimid&SysFormName=claimadjusterlist&SysCmd=1&SysViewType=controlsonly&SysFormIdName=adjrowid&SysEx=ClaimId|ClaimNumber&SysExMapCtl=ClaimId|ClaimNumber");
                        base.m_ModifiedControls.Add(singleRow);

                    }
                //}//R5 PS2 Mergex
            }
            // 03/01/2007 REM End

            // Naresh Code Changed for Saving the Enhanced Policy Ids attached with the Claim
            //objXmlElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='primarypolicyid']");
            //if (objXmlElement != null)
            //{
            //Start:Nitin Goel,Check for specific LOB. 01/13/10, MITS#18229
            //if (objClaim.Context.InternalSettings.SysSettings.UseEnhPolFlag == -1)
            if (objLobSettings.UseEnhPolFlag == -1)
            //End:Nitin Goel,Check for specific LOB. 01/13/10: MITS#18229
            {
                //Raman Bhatia: Commented code for R5..
                //Customizations would be handled in a different manner now at Page level

                //objXmlElement.SetAttribute("idref", "/Instance/Claim/PrimaryPolicyIdEnh");
                //// Start Naresh MITS 9996 Set the Ref for Policy Management
                //objXmlElement.SetAttribute("ref", "/Instance/Claim/PrimaryPolicyIdEnh/@defaultdetail");
                //objXmlElement.SetAttribute("param", "SysFormPIdName=claimid&SysFormName=policyenh&SysCmd=0&SysFormIdName=policyid&SysFormId=%primarypolicyid_cid%&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber");

                ArrayList singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.PolicyLookup.ToString());
                base.AddElementToList(ref singleRow, "id", "primarypolicyid");
                base.AddElementToList(ref singleRow, "idref", "/Instance/Claim/PrimaryPolicyIdEnh");
                base.AddElementToList(ref singleRow, "ref", "/Instance/Claim/PrimaryPolicyIdEnh/@defaultdetail");
                //MGaba2:MITS 14169:Setting onclientclick attribute in .aspx.cs..So no need of this param attribute
                // base.AddElementToList(ref singleRow, "param", "SysFormPIdName=claimid&SysFormName=policyenh&SysCmd=0&SysFormIdName=policyid&SysFormId=%primarypolicyid_cid%&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber");
                base.m_ModifiedControls.Add(singleRow);
                //MGaba2:MITS 14169: Need for opening Enhanced Policy/Policy Screen through MDI when Open is clicked
                base.ResetSysExData("UseEnhPolFlag", "-1");
                base.ResetSysExData("UseAdvancedClaim", "0");
            }
            else
            {
                //objXmlElement.SetAttribute("idref", "/Instance/Claim/PrimaryPolicyId");
                // Start Naresh MITS 9996 Set the Ref for Policy Tracking
                //objXmlElement.SetAttribute("ref", "/Instance/Claim/PrimaryPolicyId/@defaultdetail");
                //objXmlElement.SetAttribute("param", "SysFormPIdName=claimid&SysFormName=policy&SysCmd=0&SysFormIdName=policyid&SysFormId=%primarypolicyid_cid%&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber");
                if (objSysSettings.MultiCovgPerClm == -1)
                {
                    ArrayList singleRow = new ArrayList();
                    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.PolicyLookup.ToString());
                    base.AddElementToList(ref singleRow, "id", "primarypolicyid");
                    base.AddElementToList(ref singleRow, "idref", "/Instance/Claim/PrimaryPolicyId");
                    base.AddElementToList(ref singleRow, "ref", "/Instance/UI/FormVariables/SysExData/ClaimPolicyList");
                    base.m_ModifiedControls.Add(singleRow);
                    base.ResetSysExData("UseAdvancedClaim", "-1");
                }
                else
                {
                    ArrayList singleRow = new ArrayList();
                    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.PolicyLookup.ToString());
                    base.AddElementToList(ref singleRow, "id", "primarypolicyid");
                    base.AddElementToList(ref singleRow, "idref", "/Instance/Claim/PrimaryPolicyId");
                    base.AddElementToList(ref singleRow, "ref", "/Instance/Claim/PrimaryPolicyId/@defaultdetail");
                    //MGaba2:MITS 14169:Setting onclientclick attribute in .aspx.cs..So no need of this param attribute
                    // base.AddElementToList(ref singleRow, "param", "SysFormPIdName=claimid&SysFormName=policyenh&SysCmd=0&SysFormIdName=policyid&SysFormId=%primarypolicyid_cid%&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber");
                    base.m_ModifiedControls.Add(singleRow);
                    //MGaba2:MITS 14169: Need for opening Enhanced Policy/Policy Screen through MDI when Open is clicked
                    base.ResetSysExData("UseEnhPolFlag", "0");
                    base.ResetSysExData("UseAdvancedClaim", "0");
                }
            }
            //}
            //MITS 11779:Asif Code added for making wc claim time fields mandatory
            if (this.CurrentAction == enumFormActionType.AddNew)
            {
                if (objClaim.Context.InternalSettings.SysSettings.DefaultTimeFlag == -1)
                {
                    XmlNode xnode = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysNotReqNew");
                    if (xnode != null)
                    {
                        xnode.InnerText = xnode.InnerText + "|timerequired";
                    }
                }
            }
            //MITS 11779 END
            objXML = null;
            objXML = base.SysEx;
            XmlCDataSection objCData = null;
            int iOrgLevel = 0;
            string sCaption = "";
            if (this.CurrentAction == enumFormActionType.AddNew)
            {
                //We're adding a new claim - check to see if we are supposed to put it 
                //under an existing event...
                XmlNode objEventNode = this.SysEx.SelectSingleNode("/SysExData/EventId");
                if (objEventNode != null && objClaim.EventId == 0)
                {
                    objClaim.EventId = Conversion.ConvertStrToInteger(objEventNode.InnerText);
                }
                //Start Bug no. 001219(For a new record automatic selection/checking work week)

                //rsushilaggar changes made for Performance Improvement
                PiEmployee piEmployee = objClaim.PrimaryPiEmployee;
                piEmployee.WorkFriFlag = objClaim.Context.InternalSettings.SysSettings.WorkFri;
                piEmployee.WorkMonFlag = objClaim.Context.InternalSettings.SysSettings.WorkMon;
                piEmployee.WorkSatFlag = objClaim.Context.InternalSettings.SysSettings.WorkSat;
                piEmployee.WorkSunFlag = objClaim.Context.InternalSettings.SysSettings.WorkSun;
                piEmployee.WorkThuFlag = objClaim.Context.InternalSettings.SysSettings.WorkThu;
                piEmployee.WorkTueFlag = objClaim.Context.InternalSettings.SysSettings.WorkTue;
                piEmployee.WorkWedFlag = objClaim.Context.InternalSettings.SysSettings.WorkWed;
                //objClaim.PrimaryPiEmployee.WorkFriFlag=objClaim.Context.InternalSettings.SysSettings.WorkFri;
                //objClaim.PrimaryPiEmployee.WorkMonFlag=objClaim.Context.InternalSettings.SysSettings.WorkMon;
                //objClaim.PrimaryPiEmployee.WorkSatFlag=objClaim.Context.InternalSettings.SysSettings.WorkSat;
                //objClaim.PrimaryPiEmployee.WorkSunFlag=objClaim.Context.InternalSettings.SysSettings.WorkSun;
                //objClaim.PrimaryPiEmployee.WorkThuFlag=objClaim.Context.InternalSettings.SysSettings.WorkThu;
                //objClaim.PrimaryPiEmployee.WorkTueFlag=objClaim.Context.InternalSettings.SysSettings.WorkTue;
                //objClaim.PrimaryPiEmployee.WorkWedFlag=objClaim.Context.InternalSettings.SysSettings.WorkWed;
                //End Bug no. 001219(For a new record automatic selection/checking work week)
                //End rsushilaggar
                base.ResetSysExData("NewClaim", "1");

            }

            //Umesh MITS_8093
            else
                base.ResetSysExData("NewClaim", "0");
            //	Umesh MITS_8093

            base.ResetSysExData("AllCodes", m_fda.Factory.Context.LocalCache.GetAllParentCodesForClosedClaims(243, m_fda.Factory.Context.RMDatabase.DataSourceId));
            objClaim.CaseManagement.ClaimId = objClaim.ClaimId;


            //Place Full Parent(Event) Information into SysExData for XML Binding
            XmlNode objOld = null;
            XmlElement objNew = null;
            int l = 0;

            objOld = objXML.SelectSingleNode("//Parent");
            objNew = objXML.CreateElement("Parent");
            objNew.InnerXml = (objClaim.Parent as DataObject).SerializeObject();

            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);

            string sNewClaim = base.GetSysExDataNodeText("/SysExData/NewClaim");

            //Check UserLimit Security.
            if (objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmAccLmtFlag)//If UserLimits Enabled...
            {
                if (objLimits != null)
                    //Asharma326 MITS 30874 add parameter objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmAccLmtUsrFlag
                    if ((!objClaim.IsNew && !objLimits.ClaimAccessAllowed(objClaim.LineOfBusCode, objClaim.ClaimTypeCode, objClaim.ClaimStatusCode, objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmAccLmtUsrFlag) && sNewClaim != "1"))
                    {
                        //Umesh MITS_8093
                        int iClaim_Id;
                        int iDirection = (int)this.CurrentAction;
                        //gagnihotri MITS 18851
                        if (sNewClaim == "0" && iDirection != 9 && iDirection != 6)
                        {
                            switch (iDirection.ToString())
                            {

                                case "1":
                                    MoveNext();
                                    break;

                                case "2":
                                    MovePrevious();
                                    break;

                                case "3":
                                    iClaim_Id = objClaim.Context.DbConnLookup.ExecuteInt("SELECT MAX(CLAIM_ID) FROM CLAIM WHERE (LINE_OF_BUS_CODE=" + objClaim.LineOfBusCode + ")");
                                    if (objClaim.ClaimId == iClaim_Id)
                                        MovePrevious();
                                    else
                                        MoveNext();
                                    break;

                                case "4":
                                    iClaim_Id = objClaim.Context.DbConnLookup.ExecuteInt("SELECT MIN(CLAIM_ID) FROM CLAIM WHERE (LINE_OF_BUS_CODE=" + objClaim.LineOfBusCode + ")");
                                    if (objClaim.ClaimId == iClaim_Id)
                                        MoveNext();
                                    else
                                        MovePrevious();
                                    break;


                            }
                        }
                        //Umesh MITS_8093
                        else
                        {
                            base.ResetSysExData("NewClaim", "0");
                            Errors.Add(Globalization.GetString("LimitError", base.ClientId),
                                String.Format(Globalization.GetString("Access.Claim.UserLimitFailed", base.ClientId), objCache.GetCodeDesc(objClaim.ClaimTypeCode, base.Adaptor.userLogin.objUser.NlsCode), objCache.GetCodeDesc(objClaim.ClaimStatusCode, base.Adaptor.userLogin.objUser.NlsCode)),
                                BusinessAdaptorErrorType.SystemError); //Aman ML Change
                            return;
                        }
                    }
            }

            //add the Current Adjuster Name Node
            ClaimAdjuster objAdj = objClaim.CurrentAdjuster;
            if (objAdj != null)
            {
                if (objAdj.CurrentAdjFlag)
                    base.ResetSysExData("CurrentAdjuster", objAdj.AdjusterEntity.Default);
                else
                    base.ResetSysExData("CurrentAdjuster", "");
            }
            else
                base.ResetSysExData("CurrentAdjuster", "");

            //add the Claimant Attorney node
            objOld = objXML.SelectSingleNode("//ClaimantAttorney");
            objNew = objXML.CreateElement("ClaimantAttorney");

            Claimant objPrimaryClaimant = objClaim.PrimaryClaimant;
            if (objPrimaryClaimant.AttorneyEntity != null)
            {
                // Mihika 11/25/2005 Defect no. 604. Removed the stray comma in Attorney, when no data is present.
                if (objPrimaryClaimant.AttorneyEntity.LastName != string.Empty && objPrimaryClaimant.AttorneyEntity.FirstName != string.Empty)
                    objCData = objXML.CreateCDataSection(String.Format("{0}, {1}", objPrimaryClaimant.AttorneyEntity.LastName, objPrimaryClaimant.AttorneyEntity.FirstName));
                else
                    objCData = objXML.CreateCDataSection(String.Format("{0}{1}", objPrimaryClaimant.AttorneyEntity.LastName, objPrimaryClaimant.AttorneyEntity.FirstName));
                objNew.AppendChild(objCData);
                objNew.SetAttribute("codeid", objPrimaryClaimant.AttorneyEid.ToString());
            }

            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);

            //JIRA RMA-11122 ajohari2  : Start
            //if (m_CurrentAction != enumFormActionType.Save && m_CurrentAction != enumFormActionType.Delete && m_CurrentAction != enumFormActionType.Refresh && m_CurrentAction != enumFormActionType.MoveTo)
            if (m_CurrentAction.Equals(enumFormActionType.AddNew))
            {
                if (ClmStatus.Equals(0))
                {
                    ClmStatus = CommonFunctions.GetChildIDFromParent(objClaim.Context.LocalCache.GetTableId("CLAIMANT_STATUS"), objClaim.Context.LocalCache.GetCodeId("O", "CLAIMANT_STATUS_PARENT"), objClaim.Context.DbConn.ConnectionString, base.ClientId);

                }

                if (!ClmStatus.Equals(0))
                {
                    objPrimaryClaimant.ClaimantStatusCode = ClmStatus;
                }
            }
            objOld = objXML.SelectSingleNode("//ClaimantStatusCode");
            objNew = objXML.CreateElement("ClaimantStatusCode");

            if (objPrimaryClaimant != null)
            {
                objCData = objXML.CreateCDataSection(String.Format("{0}", objClaim.Context.LocalCache.GetCodeDesc(objPrimaryClaimant.ClaimantStatusCode)));
                objNew.AppendChild(objCData);
                objNew.SetAttribute("codeid", objPrimaryClaimant.ClaimantStatusCode.ToString());
            }

            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);

            //JIRA RMA-11122 ajohari2  : End
            //add the folder node
            string sFolder = RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, "pdf-forms");

            objOld = objXML.SelectSingleNode("//folder");
            objNew = objXML.CreateElement("folder");

            objCData = objXML.CreateCDataSection(sFolder);
            objNew.AppendChild(objCData);

            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);

            Riskmaster.Settings.LobSettings objLob = null;
            //add the sub title node
            //objOld = objXML.SelectSingleNode("//SubTitle");
            //objNew = objXML.CreateElement("SubTitle");

            //zmohammad MITs 34436 - Unifying the function used to apply form subtitle across all LOBs.
            ApplyFormTitle();
            //if (!objClaim.IsNew)
            //{
            //    objLob = objData.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode];
            //    iOrgLevel = objLob.CaptionLevel;
            //    sCaption = objData.Context.InternalSettings.CacheFunctions.GetOrgParent((objClaim.Parent as Event).DeptEid, iOrgLevel, 1, ref l);
            //    sCaption = sCaption.Substring(0, Math.Min(sCaption.Length, 25));
            //    objNew.InnerText = " [" + objClaim.ClaimNumber + " * ";
            //    Entity objPiEntity = objClaim.PrimaryPiEmployee.PiEntity;
            //    objNew.InnerText = " [" + objClaim.ClaimNumber + " * " + sCaption
            //        + " * " +
            //        //nadim for 15014-Start
            //        //objPiEntity.FirstName + " " + objPiEntity.LastName + "] ";
            //        objPiEntity.LastName + " " + objPiEntity.FirstName + "] ";
            //    //nadim for 15014-End
            //    ArrayList singleRow = new ArrayList();
            //    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            //    base.AddElementToList(ref singleRow, "id", "formsubtitle");
            //    base.AddElementToList(ref singleRow, "Text", objNew.InnerText);
            //    base.m_ModifiedControls.Add(singleRow);
            //}



            //if (objOld != null)
            //    objXML.DocumentElement.ReplaceChild(objNew, objOld);
            //else
            //    objXML.DocumentElement.AppendChild(objNew);

            Event objEvent = objClaim.Parent as Event;

            //Handle Locked down Toolbar buttons.
            if (!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_SEARCH, FormBase.RMO_CLAIM_SEARCH))
                base.AddKillNode("search");
            if (!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_LOOKUP, FormBase.RMO_CLAIM_SEARCH))
                base.AddKillNode("lookup");

            //MITS 20237 Add support for View SSN permission
            //if (!objClaim.Context.RMUser.IsAllowedEx(RMPermissions.RMO_EMPLOYEE, RMPermissions.RMO_EMPLOYEE_VIEW_SSN))
            //{
            //    base.AddKillNode("emptaxid");
            //} ------ smishra25:Commented for MITS 24395.Behaviour should be similar to 13748

            //nadim for 13748,added to hide/unhide SSN field
            if (!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_WC_EMPLOYEE, FormBase.RMO_VIEW_SSN))
            {
                base.AddKillNode("emptaxid");

            }

            //nadim for 13748

            int iAWWFormOption = -1;
            string sStateId = string.Empty;


            //Remove button if this claim does not have any state WC forms.
            if (objClaim.FilingStateId != 0)
            {
                using (DbReader objReader = objClaim.Context.DbConnLookup.ExecuteReader("SELECT STATE_ID, AWW_FORM_OPTION FROM STATES WHERE STATE_ROW_ID=" + objClaim.FilingStateId))
                {
                    if (objReader.Read())
                    {
                        sStateId = Conversion.ConvertObjToStr(objReader.GetValue("STATE_ID"));
                        if (sStateId.Trim() == string.Empty)
                        {
                            base.AddKillNode("btnFROI");
                        }
                        else
                        {
                            base.AddDisplayNode("btnFROI");
                        }
                        // Yatharth : MITS 18097: Added the Module permission check for WC-PDF Forms.
                        //if (objClaim.Context.RMUser.IsAllowed(FormBase.RMO_ALLOW_WC_PDF_FORMS))
                        if (objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_ALLOW_WC_PDF_FORMS))
                        {
                            if (sStateId.Trim() == string.Empty || !System.IO.Directory.Exists(sFolder + @"\" + sStateId))
                                base.AddKillNode("btnWCForms");
                            else
                                base.AddDisplayNode("btnWCForms");
                        }
                        else
                            base.AddKillNode("btnWCForms");

                        string sAWWFormOption = Conversion.ConvertObjToStr(objReader.GetValue("AWW_FORM_OPTION"));
                        if (sAWWFormOption.Trim() != string.Empty)
                            iAWWFormOption = Conversion.ConvertStrToInteger(sAWWFormOption);

                        //base.AddDisplayNode("btnWCForms");// Yatharth : MITS 18097: Commented as it was unnecessary
                        base.AddDisplayNode("btnFROI");
                    }
                    else
                    {
                        base.AddKillNode("btnWCForms");
                        base.AddKillNode("btnFROI");
                    }
                }
                //base.AddDisplayNode("btnWCForms");//Yatharth : MITS 18097: Commented as it was unnecessary
                base.AddDisplayNode("btnFROI");
            }
            else
            {
                base.AddKillNode("btnWCForms");
                base.AddKillNode("btnFROI");
            }
            SetAWWSysExNodes(iAWWFormOption);

            //Start - VSS enable setting check
            //if (!objClaim.Context.InternalSettings.SysSettings.EnableVSS.Equals(-1))
            //{
            //    base.AddKillNode("vssclaimind");
            //}
            //else
            //{
            //    base.AddDisplayNode("vssclaimind");
            //}
            //End - VSS enable setting check

            //Raman Bhatia .. As per email from Mike, Making changes in Progress Notes Design
            //Kill Comments or Enhanced Notes node depending on SYS_PARMS_LOB settings

            try
            {
                //rsushilaggar: Get the flag from the LobSetings Objects
                //int iUseClaimProgressNotes = objClaim.Context.DbConnLookup.ExecuteInt(
                //    "SELECT USE_CLAIM_PROGRESS_NOTES FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode.ToString());
                int iUseClaimProgressNotes = objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmProgressNotesFlag;

                if (iUseClaimProgressNotes == 0)
                {
                    base.AddKillNode("enhancednotes");
                }
                else
                {
                    base.AddDisplayNode("enhancednotes");
                }

                //rsushilaggar: Get the flag from the LobSetings Objects
                //int iUseClaimComments = objClaim.Context.DbConnLookup.ExecuteInt(
                //    "SELECT USE_CLAIM_COMMENTS FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode.ToString());
                int iUseClaimComments = objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmCommentsFlag;

                if (iUseClaimComments == 0)
                {
                    base.AddKillNode("comments");
                }
                else
                {
                    base.AddDisplayNode("comments");
                }
                //rsushilaggar: ISO Claim Search : Start
                //int iUseISOSubmission = objClaim.Context.DbConnLookup.ExecuteInt(
                //    "SELECT USE_ISO_SUBMISSION FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode.ToString());
                int iUseISOSubmission = objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmISOSubmissionFlag;
                if (iUseISOSubmission == 0 || iUseISOSubmission.ToString() == null)
                {
                    base.AddKillNode("iso");
                }
                else
                {
                    base.AddDisplayNode("iso");
                }
                //rsushilaggar: ISO Claim Search : End	
                if (!objClaim.Context.InternalSettings.SysSettings.ClaimActivityLog)
                {
                    base.AddKillNode("claimActLog");
                }
                else
                {
                    base.AddDisplayNode("claimActLog");
                }
                //added by swati agarwal For PMC Gap 6 DCI fields MITS#36997 - RMA - 5497
                if (!objClaim.Context.InternalSettings.SysSettings.UseDCIReportingFields)
                {
                    base.AddKillNode("feinnumber");
                    base.AddKillNode("dailycomprate");
                    base.AddKillNode("postweeklywage");
                    base.AddKillNode("dailycomprate1");
                    base.AddKillNode("postweeklywage1");
                    base.AddKillNode("nccilosstypelosscode");
                    base.AddKillNode("nccilosstyperecovcode");
                    base.AddKillNode("impairmentpercentcode");
                    base.AddKillNode("medextinguishind");
                    base.AddKillNode("returntoworkind");
                    base.AddKillNode("losseventclmind");
                    base.AddKillNode("dcivallvlcode");
                }
                else
                {
                    base.AddDisplayNode("feinnumber");
                    base.AddDisplayNode("dailycomprate");
                    base.AddDisplayNode("postweeklywage");
                    base.AddDisplayNode("dailycomprate1");
                    base.AddDisplayNode("postweeklywage1");
                    base.AddDisplayNode("nccilosstypelosscode");
                    base.AddDisplayNode("nccilosstyperecovcode");
                    base.AddDisplayNode("impairmentpercentcode");
                    base.AddDisplayNode("medextinguishind");
                    base.AddDisplayNode("returntoworkind");
                    base.AddDisplayNode("losseventclmind");
                    base.AddDisplayNode("dcivallvlcode");
                }
                //change end here by swati - RMA - 5497

                if (!objClaim.Context.InternalSettings.SysSettings.OpenPointPolicy)
                {
                    base.AddKillNode("multipolicyidbtnopenpointpolicy");
                }
                else
                {
                    base.AddDisplayNode("multipolicyidbtnopenpointpolicy");
                }
                //MITS:33573 Starts
                if (objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface)
                {
                    base.AddDisplayNode("noticedate");
                    base.AddDisplayNode("errordate");
                    base.AddDisplayNode("underlyinglossdate");
                }
                else
                {
                    base.AddKillNode("noticedate");
                    base.AddKillNode("errordate");
                    base.AddKillNode("underlyinglossdate");
                }
                //MITS:33573 Ends
            }
            catch
            {
            }


            //Nitesh MITS 7656 Starts
            //Event Number Locking Logic
            //			if(objClaim.IsNew && (objClaim.Parent as Event).EventId ==0 && !objClaim.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_ALLOW_ENTRY_EVENT_NUM))
            //				AddReadOnlyNode("ev_eventnumber");
            //			
            //			if(objClaim.IsNew && (objClaim.Parent as Event).EventId !=0 && !objClaim.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_ALLOW_EDIT_EVENT_NUM))
            //				AddReadOnlyNode("ev_eventnumber");
            //
            //			if(!objClaim.IsNew && !objClaim.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_ALLOW_EDIT_EVENT_NUM))
            //				AddReadOnlyNode("ev_eventnumber");
            //Nitesh MITS 7656 Ends

            // ABhateja, 05.07.2007 -START-
            // MITS 9980, Commented the code for location and event description 
            // freeze flags as it is being checked in the editor.cs file.
            //if(objEvent.Context.InternalSettings.SysSettings.FreezeEventDesc!=0)
            //    base.AddReadOnlyNode("ev_eventdescription");

            //if(objEvent.Context.InternalSettings.SysSettings.FreezeLocDesc!=0)
            //    base.AddReadOnlyNode("ev_locationareadesc");
            // ABhateja, 05.07.2007 -END-

            if ((objClaim.IsNew && !objClaim.Context.RMUser.IsAllowedEx(this.SecurityId, RMO_CLAIM_NUMBER_ALLOW_ENTRY))
                || (!objClaim.IsNew && !objClaim.Context.RMUser.IsAllowedEx(this.SecurityId, RMO_CLAIM_NUMBER_ALLOW_EDIT)))
                base.AddReadOnlyNode("claimnumber");
            else
                base.AddReadWriteNode("claimnumber");

            // Mihika 07-Jan-2006 Defect no. 1279
            // Making 'Employee Number' readonly in case the corresponding utilities entry is set.
            if (!objClaim.IsNew && !objClaim.Context.InternalSettings.SysSettings.EditWCEmpNum)
                base.AddReadOnlyNode("empemployeenumber");
            else
                base.AddReadWriteNode("empemployeenumber");

            // Mihika 07-Jan-2006 Defect no. 1231 Added a SysEx node for 'DefaultDeptFlag'
            base.ResetSysExData("DefaultDeptFlag", objClaim.Context.InternalSettings.SysSettings.DefaultDeptFlag.ToString());

            objOld = objXML.SelectSingleNode("//ControlAppendAttributeList");
            objNew = objXML.CreateElement("ControlAppendAttributeList");
            XmlElement objElem = null;
            XmlElement objChild = null;

            objElem = objXML.CreateElement("ev_eventnumber");
            if (!objClaim.IsNew)
            {
                //Commented by pmittal5 Mits 16471 - linktobutton has been removed for Event Number 
                //objChild=objXML.CreateElement("linktobutton");
                //objChild.SetAttribute("value","1");
                //Nitesh MITS 7656 Starts
                //objElem.AppendChild(objChild);
                if (!objEvent.Context.RMUser.IsAllowedEx(this.SecurityId, RMO_ALLOW_EDIT_EVENT_NUM))
                {
                    objChild = objXML.CreateElement("readonlyText");
                    objChild.SetAttribute("value", "true");
                    objElem.AppendChild(objChild);
                    //base.AddReadOnlyNode("eventnumber");
                }
                //Nitesh MITS 7656 Ends
            }
            else
            {
                // MITS 9558  Need to remove. Button doesn't work correctly.  objChild=objXML.CreateElement("lookupbutton");
                // MITS 9558  objChild.SetAttribute("value","1");
                //Nitesh MITS 7656 Starts
                // MITS 9558  objElem.AppendChild(objChild);
                if ((objEvent.EventId == 0 && !objEvent.Context.RMUser.IsAllowedEx(this.SecurityId, RMO_ALLOW_ENTRY_EVENT_NUM))
                    || (objEvent.EventId != 0 && !objEvent.Context.RMUser.IsAllowedEx(this.SecurityId, RMO_ALLOW_EDIT_EVENT_NUM)))
                {
                    objChild = objXML.CreateElement("readonlyText");
                    objChild.SetAttribute("value", "true");
                    objElem.AppendChild(objChild);
                    //base.AddReadOnlyNode("eventnumber");
                }


            }

            //objElem.AppendChild(objChild);
            //Nitesh MITS 7656 Ends
            objNew.AppendChild(objElem);
            //To change the onclientclick during runtime.
            //objSysSettings = new SysSettings(objClaim.Context.DbConn.ConnectionString);
            if (!objSysSettings.PhysicianSearchFlag)
            {
                objElem = objXML.CreateElement("physicianslist1btn");
                objChild = objXML.CreateElement("onclientclick");
                objChild.SetAttribute("value", "return lookupData('physicianslist1','PHYSICIANS',4,'physicianslist1',3)");
                objElem.AppendChild(objChild);
                objNew.AppendChild(objElem);
                //rjhamb-Mits 21281:Medical Info Physician search not working when Case Mgmt is on but utility setting "Include Physician Med. and Employee in Physician Search" is off
                objElem = objXML.CreateElement("physicianslist2btn");
                objChild = objXML.CreateElement("onclientclick");
                objChild.SetAttribute("value", "return lookupData('physicianslist2','PHYSICIANS',4,'physicianslist2',3)");
                objElem.AppendChild(objChild);
                objNew.AppendChild(objElem);
                //rjhamb-Mits 21281:Medical Info Physician search not working when Case Mgmt is on but utility setting "Include Physician Med. and Employee in Physician Search" is off
            }
            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);

            //Show/Clear DateTimeStamp of Record Closing if record is/is not closed.
            if (8 != objClaim.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(objClaim.ClaimStatusCode))
                objClaim.DttmClosed = "";

            //create nodes in sysExData for Claim Status History Field
            base.CreateSysExData("StatusApprovedBy");
            base.CreateSysExData("StatusDateChg");
            base.CreateSysExData("StatusReason");
            //MGaba2:MITS 10241:Start
            base.CreateSysExData("systemcurdate", Conversion.GetUIDate(DateTime.Now.ToShortDateString(), LanguageCode, ClientId));//vkumar258 ML Changes
            base.CreateSysExData("BackdtClaimSetting", "1");
            if (!m_fda.userLogin.IsAllowedEx(this.SecurityId, FormBase.RMO_WC_BKDATE_CLAIM_STATUS_HISTORY))
            {
                base.ResetSysExData("BackdtClaimSetting", "0");
            }
            //MGaba2:MITS 10241:End
            //rsushilaggar Changes made for performance improvement
            //string sWorkFlags = string.Empty;
            StringBuilder sbWorkFlags = new StringBuilder();
            PiEmployee objEmployee = objClaim.PrimaryPiEmployee as PiEmployee;

            if (objEmployee != null)
                sbWorkFlags.Append(objEmployee.WorkSunFlag + ",");
            sbWorkFlags.Append(objEmployee.WorkMonFlag + ",");
            sbWorkFlags.Append(objEmployee.WorkTueFlag + ",");
            sbWorkFlags.Append(objEmployee.WorkWedFlag + ",");
            sbWorkFlags.Append(objEmployee.WorkThuFlag + ",");
            sbWorkFlags.Append(objEmployee.WorkFriFlag + ",");
            sbWorkFlags.Append(objEmployee.WorkSatFlag);

            base.ResetSysExData("WorkFlags", sbWorkFlags.ToString());

            sDaysOfWeek = sbWorkFlags.ToString().Split(",".ToCharArray());
            //End rsushilaggar
            // Loading all the childs of Primary Employee. Added By Rahul Sharma.
            LoadPiEmployeeChilds(ref objXML);
            LoadUnitStat(ref objXML);
            if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
                RenderPolicyList(ref objXML);
            //End of Childs of Primary Employee.
            //skhare7 Point Policy Interface
            if (objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface)
            {
                if (objClaim.Context.InternalSettings.SysSettings.PolicyCvgType == 0)
                    base.ResetSysExData("PointClaimEventSetting", "true");

                else if (objClaim.Context.InternalSettings.SysSettings.PolicyCvgType == 1)
                    base.ResetSysExData("PointClaimEventSetting", "false");
            }
            //skhare7 Point Policy Interface End
            CheckAutoGenerateEmpNo();
            //Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.  
            string sEventDateFromDB = string.Empty;
            if (objEvent != null)
            {
                if (objEvent.EventId != 0)
                {
                    sEventDateFromDB = objEvent.DateOfEvent;
                    if (!String.IsNullOrEmpty(sEventDateFromDB))
                    {
                        if (base.SysEx.SelectSingleNode("DateOfEvent") == null)
                        {
                            string sUIEventDate = string.Empty;
                            sUIEventDate = Conversion.ToDate(sEventDateFromDB).ToString("MM/dd/yyyy");
                            base.CreateSysExData("DateOfEvent", sUIEventDate);
                        }
                    }
                }
            }
            //Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.
            //rsushilaggar changes made for Performance Improvement
            string sBirthDate = objClaim.PrimaryPiEmployee.PiEntity.BirthDate;
            if (sBirthDate != string.Empty)
                base.ResetSysExData("EntityAge", Utilities.CalculateAgeInYears(sBirthDate, sEventDateFromDB)); //Added sEventDateFromDB for MITS 19315
            else
                base.ResetSysExData("EntityAge", "");

            // Mihika 6-Jan-2006 Defect no. 1189
            if (base.m_CurrentAction != enumFormActionType.Save)
                base.ResetSysExData("dupeoverride", "");

            //for Displaying Back button on Policy Maintenance Screen
            base.ResetSysExData("DisplayBackButton", "true");
            //for Closing Diary or not -- Rahul 6th Feb 2006
            base.ResetSysExData("CloseDiary", "false");

            //Nikhil Garg		17-Feb-2006
            //check whether open diaries exist for this claim or not
            if (HasOpenDiaries)
                base.ResetSysExData("ContainsOpenDiaries", "true");
            else
                base.ResetSysExData("ContainsOpenDiaries", "false");

            //Changed by Gagan for MITS 11451 : Start

            //Initializing Delete Auto-check flag
            base.ResetSysExData("DeleteAutoCheck", "false");
            base.ResetSysExData("ClaimStatusCode", objClaim.ClaimStatusCode.ToString());

            //check whether auto checks diaries exist for this claim or not
            string sSQL = "SELECT CLAIM_ID FROM FUNDS_AUTO WHERE CLAIM_ID = " + objClaim.ClaimId.ToString();
            using (DbReader objReaderAutoChecks = objClaim.Context.DbConn.ExecuteReader(sSQL))
            {
                if (objReaderAutoChecks.Read())
                    base.ResetSysExData("ContainsAutoChecks", "true");
                else
                    base.ResetSysExData("ContainsAutoChecks", "false");
            }
            //Changed by Gagan for MITS 11451 : End

            //Changed by Gagan for MITS 14770
            if (objClaim.Context.InternalSettings.SysSettings.DeleteAllClaimDiaries == 0)
            {
                base.ResetSysExData("DeleteAllClaimDiaries", "false");
            }
            else
            {
                base.ResetSysExData("DeleteAllClaimDiaries", "true");
            }

            if (!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_WC_EMPLOYEE, FormBase.RMO_VIEW_HOSPITAL_INFO))
            {
                base.AddKillNode("hospitallist1");
                base.AddKillNode("hospitallist2");
            }
            if (!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_WC_EMPLOYEE, FormBase.RMO_VIEW_PHYSICIAN_INFO))
            {
                base.AddKillNode("physicianslist1");
                base.AddKillNode("physicianslist2");
            }
            if (!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_WC_EMPLOYEE, FormBase.RMO_VIEW_DIAGNOSIS_INFO))
            {
                base.AddKillNode("diagnosislist1");
                base.AddKillNode("diagnosislist2");
            }
            if (!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_WC_EMPLOYEE, FormBase.RMO_VIEW_TREATMENT_INFO))
            {
                base.AddKillNode("treatmentlist1");
                base.AddKillNode("treatmentlist2");
            }
            if (!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_WC_EMPLOYEE, FormBase.RMO_ENABLE_SALARY))
            {
                base.AddKillNode("emppaytypecode1");
                base.AddKillNode("emppaytypecode2");
                base.AddKillNode("emppayamount");
                base.AddKillNode("emppayamount_1"); //jramkumar MITS 32175
                base.AddKillNode("emphourlyrate1");
                base.AddKillNode("emphourlyrate2");
                base.AddKillNode("empweeklyhours1");
                base.AddKillNode("empweeklyhours2");
                base.AddKillNode("empweeklyrate1");
                base.AddKillNode("empweeklyrate2");
                base.AddKillNode("empmonthlyrate");
                base.AddKillNode("empmonthlyrate_1"); //jramkumar MITS 32175
            }

            //check for hiding FROI button
            if (!objClaim.Context.RMUser.IsAllowedEx(this.SecurityId + FormBase.JUR_OFFSET, FormBase.RMO_PRINT_JURISDICTIONAL_FORMS))
                base.AddKillNode("btnFROI");

            //Shruti for 10779 - starts
            //skhare7:MITS 22365
            //    if (!Adaptor.userLogin.IsAllowedEx(RMO_WC_EMPLOYEE + RMO_CREATE) || !Adaptor.userLogin.IsAllowedEx(RMPermissions.RMO_EMPLOYEE + RMO_CREATE))
            if (!Adaptor.userLogin.IsAllowedEx(RMO_WC_EMPLOYEE + RMO_CREATE))
            {
                //XmlNode objCreatableNode = base.SysView.SelectSingleNode("//form[@name='claimwc']//control[@creatable='1']");
                //if(objCreatableNode != null )
                //{
                //    objCreatableNode.Attributes.RemoveNamedItem("creatable");
                //}
                base.AddKillNode("empemployeenumber_creatable");
            }
            //skhare7 MITS 18069
            if (!Adaptor.userLogin.IsAllowedEx(RMPermissions.RMO_WC_MMSEA, RMPermissions.RMO_VIEW))
            {

                base.AddKillNode("btnMMSEAData");
            }
            else
            {
                base.AddDisplayNode("btnMMSEAData");
            }

            //skhare7 MITS 18069 end
            //pmittal5 08/24/09 Mits 13724 - In case there is no record in CLAIM_AWW for this claim, Hourly Rate and other fields come as editable
            //They should be readonly, along with "AWW Rate Calculations" button if Module Permissions are not given
            //Commented and copied the code below.

            //if (!Adaptor.userLogin.IsAllowedEx(FormBase.RMO_WC_EMPLOYEE, FormBase.RMO_UPDATE) || !Adaptor.userLogin.IsAllowedEx(RMO_EMPLOYEE + RMO_UPDATE))
            //{
            //    DisableEmpFields();
            //}

            //Shruti for 10779 - ends


            //Nikhil Garg		14-Mar-2006
            //check whether to disable Hourly Rate, Hours Per Week, & Weekly Rate fields
            sSQL = "SELECT Claim_ID FROM CLAIM_AWW WHERE Claim_ID= " + objClaim.ClaimId.ToString();
            using (DbReader objClaimAWW = objClaim.Context.DbConn.ExecuteReader(sSQL))
            {
                if (objClaimAWW.Read())
                {
                    base.AddReadOnlyNode("emphourlyrate1");
                    base.AddReadOnlyNode("empweeklyhours1");
                    base.AddReadOnlyNode("empweeklyrate1");
                    base.AddReadOnlyNode("emphourlyrate2");
                    base.AddReadOnlyNode("empweeklyhours2");
                    base.AddReadOnlyNode("empweeklyrate2");
                }
                else
                {
                    base.AddReadWriteNode("emphourlyrate1");
                    base.AddReadWriteNode("empweeklyhours1");
                    base.AddReadWriteNode("empweeklyrate1");
                    base.AddReadWriteNode("emphourlyrate2");
                    base.AddReadWriteNode("empweeklyhours2");
                    base.AddReadWriteNode("empweeklyrate2");
                }
            }

            //pmittal5 Mits 13724 08/24/09 - Copied above commented code
            //commented by shobhana
            // if (!Adaptor.userLogin.IsAllowedEx(FormBase.RMO_WC_EMPLOYEE, FormBase.RMO_UPDATE) || !Adaptor.userLogin.IsAllowedEx(RMPermissions.RMO_EMPLOYEE + RMO_UPDATE))
            if (!Adaptor.userLogin.IsAllowedEx(FormBase.RMO_WC_EMPLOYEE, FormBase.RMO_UPDATE))
            {
                DisableEmpFields();
            }

            base.AddReadOnlyNode("comprate");
            base.AddReadOnlyNode("comprate1");

            //Add by kuladeep for mits:30926
            base.AddReadOnlyNode("averagewage");
            base.AddReadOnlyNode("averagewage1");

            //Nitesh(09/27/2006): LSS Merge Setup changes Starts

            try
            {
                if (!objClaim.Context.InternalSettings.SysSettings.RMXLSSEnable)
                {
                    base.AddKillNode("lssclaimind");
                }
                //start Mits 35472
                //else
                //{
                //    base.AddDisplayNode("lssclaimind");
                //}
                //End Mits 35472

            }
            catch
            {
                base.AddKillNode("lssclaimind");
            }
            //// asingh263 Added for MITS 31673 Starts
            if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == 0)
            {
                using (Policy objPolicy = (Policy)m_fda.Factory.GetDataModelObject("Policy", false))
                {
                    if (objClaim.PrimaryPolicyId != default(int))
                    {
                        objPolicy.MoveTo(objClaim.PrimaryPolicyId);

                        if (!string.IsNullOrEmpty(objPolicy.EffectiveDate))
                        {
                            string primaryPolicyEffectiveDate = Conversion.ToDate(objPolicy.EffectiveDate).ToShortDateString();
                            base.ResetSysExData("PrimaryPolicyEffectiveDate", primaryPolicyEffectiveDate);
                        }
                        else
                        {
                            base.ResetSysExData("PrimaryPolicyEffectiveDate", string.Empty);
                        }

                        if (!string.IsNullOrEmpty(objPolicy.ExpirationDate))
                        {
                            string primaryPolicyExpirationDate = Conversion.ToDate(objPolicy.ExpirationDate).ToShortDateString();
                            base.ResetSysExData("PrimaryPolicyExpirationDate", primaryPolicyExpirationDate);
                        }
                        else
                        {
                            base.ResetSysExData("PrimaryPolicyExpirationDate", string.Empty);
                        }
                    }
                    else
                    {
                        base.ResetSysExData("PrimaryPolicyExpirationDate", string.Empty);
                        base.ResetSysExData("PrimaryPolicyEffectiveDate", string.Empty);
                    }
                    bool claimTriggerClaimFlag = objPolicy.TriggerClaimFlag;
                    base.ResetSysExData("ClaimTriggerClaimFlag", claimTriggerClaimFlag.ToString());
                }
            }
            //// asingh263 Added for MITS 31673 Ends
            //Added by sharishkumar for Mits 35472
            if ((objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1))
            {
                base.AddKillNode("lssclaimind");
            }
            //End Mits 35472
            //Start: Added by Sumit Agarwal, 08/05/2014, MITS 33588
            // ddhiman updated UseClaimDept to UseInsuredClaimDept MITS 33588
            if (objClaim.Context.InternalSettings.SysSettings.UseInsuredClaimDept != -1)
            {
                base.AddKillNode("clm_insdepteid");
                //End ddhiman
            }
            base.ResetSysExData("InsuredClaimDeptFlag", objClaim.Context.InternalSettings.SysSettings.UseInsuredClaimDept.ToString());  //Added by Sumit Agarwal to set the value for InsuredClaimDeptFlag: 09/23/2014: MITS 33588
            //End: Added by Sumit Agarwal, 08/05/2014, MITS 33588

            //Nitesh(09/27/2006): LSS Merge Setup changes Ends

            //Shruti for EMI changes
            if (objClaim.Context.InternalSettings.SysSettings.UseFLMaxRate == -1)
            {

                base.AddKillNode("comprateHide");
                base.AddKillNode("comprate1Hide");
                base.AddDisplayNode("comprate");
                base.AddDisplayNode("comprate1");

                base.AddKillNode("averagewageHide");
                base.AddKillNode("averagewage1Hide");
                base.AddDisplayNode("averagewage");//Add by kuladeep for mits:30926
                base.AddDisplayNode("averagewage1");
            }
            else
            {
                base.AddKillNode("comprate");
                base.AddKillNode("comprate1");
                base.AddDisplayNode("comprateHide");
                base.AddDisplayNode("comprate1Hide");

                base.AddKillNode("averagewage");
                base.AddKillNode("averagewage1");
                base.AddDisplayNode("averagewageHide");//Add by kuladeep for mits:30926
                base.AddDisplayNode("averagewage1Hide");
            }

            //MGaba2:MITS 15642:creating the node so that it doesnt come in missing refs:start
            CreateSysExData("EventOnPremiseChecked", "false");
            //tkr mits 10267.  need entire org hierarchy to hide/show group assoc supp fields
            bool isOhSet = false;
            StringBuilder strB = null;
            int iResCount = 0;
            int iResCurrCount = 0;//pgupta93: JIRA-16476
            if (objClaim.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
            {
                iResCount = objClaim.Context.DbConn.ExecuteInt("SELECT COUNT(1) FROM FUNDS_AUTO WHERE CLAIM_ID=" + objClaim.ClaimId);
                iResCurrCount = objClaim.Context.DbConn.ExecuteInt("SELECT COUNT(1) FROM RESERVE_CURRENT WHERE CLAIM_ID=" + objClaim.ClaimId);//pgupta93:JIRA-16476
            }
            if (objEvent.DeptEid > 0)
            {
                using (DbReader rdr = objClaim.Context.DbConnLookup.ExecuteReader("SELECT FACILITY_EID,LOCATION_EID,DIVISION_EID,REGION_EID,OPERATION_EID,COMPANY_EID,CLIENT_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid.ToString()))
                {
                    while (rdr.Read())
                    {
                        isOhSet = true;
                        base.ResetSysExData("OH_FACILITY_EID", rdr.GetInt("FACILITY_EID").ToString());
                        base.ResetSysExData("OH_LOCATION_EID", rdr.GetInt("LOCATION_EID").ToString());
                        base.ResetSysExData("OH_DIVISION_EID", rdr.GetInt("DIVISION_EID").ToString());
                        base.ResetSysExData("OH_REGION_EID", rdr.GetInt("REGION_EID").ToString());
                        base.ResetSysExData("OH_OPERATION_EID", rdr.GetInt("OPERATION_EID").ToString());
                        base.ResetSysExData("OH_COMPANY_EID", rdr.GetInt("COMPANY_EID").ToString());
                        base.ResetSysExData("OH_CLIENT_EID", rdr.GetInt("CLIENT_EID").ToString());
                        if (objClaim.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)//Deb Multi Currency
                        {
                            strB = new StringBuilder(objEvent.DeptEid.ToString());
                            if (strB.ToString() != "")
                                strB.Append(",");
                            strB.Append(rdr.GetInt("FACILITY_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("LOCATION_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("DIVISION_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("REGION_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("OPERATION_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("COMPANY_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("CLIENT_EID").ToString());
                        }
                    }
                }
                if (objClaim.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                {
                    string sCurrCode = string.Empty;
                    string[] arr = strB.ToString().Split(',');
                    for (int i = 0; i < arr.Length; i++)
                    {
                        int orgCurrCode = objClaim.Context.DbConnLookup.ExecuteInt("SELECT ORG_CURR_CODE FROM ENTITY WHERE ENTITY_ID =" + arr[i]);
                        if (orgCurrCode > 0)
                        {
                            sCurrCode = orgCurrCode.ToString();
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(sCurrCode))
                    {
                        if (m_CurrentAction != enumFormActionType.Save && m_CurrentAction != enumFormActionType.Delete && m_CurrentAction != enumFormActionType.Refresh && m_CurrentAction != enumFormActionType.MoveTo && iResCount <= 0)
                            objClaim.CurrencyType = Convert.ToInt32(sCurrCode);//Deb Multi Currency
                    }
                }
            }
            if (!isOhSet)
            {
                base.ResetSysExData("OH_FACILITY_EID", "0");
                base.ResetSysExData("OH_LOCATION_EID", "0");
                base.ResetSysExData("OH_DIVISION_EID", "0");
                base.ResetSysExData("OH_REGION_EID", "0");
                base.ResetSysExData("OH_OPERATION_EID", "0");
                base.ResetSysExData("OH_COMPANY_EID", "0");
                base.ResetSysExData("OH_CLIENT_EID", "0");
            }

            //Raman: MITS 16269
            // Store count of PI employees so we can throw up popup to pick employee when creating claim
            if (objClaim.EventId != 0)
            {
                int iCount = objClaim.Context.DbConnLookup.ExecuteInt("SELECT COUNT(*) FROM PERSON_INVOLVED WHERE PARENT_ROW_ID = " + objClaim.EventId.ToString() + " AND UPPER(PARENT_TABLE_NAME) LIKE 'EVENT' AND PI_TYPE_CODE = 237");  // JIRA 17807 pgupta215
                base.ResetSysExData("PiEmpCount", iCount.ToString());
            }
            else
            {
                base.ResetSysExData("PiEmpCount", "0");
            }
            CreateSysExData("UseLegacyComments", Riskmaster.Common.Conversion.ConvertObjToStr(objClaim.Context.InternalSettings.SysSettings.UseLegacyComments));
            //Ashish Ahuja: Claims Made Jira 1342
            CreateSysExData("ClaimRptDateType", Riskmaster.Common.Conversion.ConvertObjToStr(objClaim.Context.InternalSettings.SysSettings.ClaimDateRptType));
            //BOB Enhancement 
            base.ResetSysExData("isAutoPopulateDpt", objClaim.Context.InternalSettings.SysSettings.AutoPopulateDpt.ToString());
            base.ResetSysExData("hdnEmpSelected", "False");	//MGaba2:MITS 20184
            GetFilteredListForClaimType();
            //Deb Multi Currency
            int iBaseCurrCode = objClaim.Context.InternalSettings.SysSettings.BaseCurrencyType;
            if (objClaim.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
            {
                if (objClaim.IsNew)
                {
                    if (iBaseCurrCode > 0 && objClaim.CurrencyType <= 0)
                    {
                        string sCurrency = objClaim.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode);
                        base.ResetSysExData("BaseCurrencyType", sCurrency.Split('|')[1]);
                        objClaim.CurrencyType = iBaseCurrCode;
                    }
                    else if (objClaim.CurrencyType <= 0)
                    {
                        Errors.Add(Globalization.GetString("BaseCurrency.Error", base.ClientId), "Please set the Base Currency in the Utilities", BusinessAdaptorErrorType.Error);
                    }
                }
                else
                {
                    if (iBaseCurrCode > 0)
                    {
                        string sCurrency = objClaim.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode);
                        base.ResetSysExData("BaseCurrencyType", sCurrency.Split('|')[1]);
                        //rupal:start,multicurrency
                        //commented because for existing claim, if claim currency is not set then we will ask user to manually set
                        /*
                        if (objClaim.CurrencyType <= 0)
                        {
                            objClaim.CurrencyType = iBaseCurrCode;
                        }
                        */
                        //rupal:end                    
                    }
                    else
                    {
                        Errors.Add(Globalization.GetString("BaseCurrency.Error", base.ClientId), "Please set the Base Currency in the Utilities", BusinessAdaptorErrorType.Error);
                    }
                    //pgupta93:JIRA-16476 START
                    if (iResCurrCount > 0 || objClaim.ReserveCurrentList.Count > 0)
                    {
                        base.AddReadOnlyNode("currencytypetext");
                    }
                    //pupta93:JIRA-16476 END
                    if (iResCount > 0 || objClaim.FundsList.Count > 0)
                    {
                        base.AddReadOnlyNode("currencytypetext");
                    }
                }
            }
            else
            {
                if (iBaseCurrCode > 0)
                {
                    string sCurrency = objClaim.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode);
                    base.ResetSysExData("BaseCurrencyType", sCurrency.Split('|')[1]);
                    if (objClaim.CurrencyType <= 0)
                    {
                        objClaim.CurrencyType = iBaseCurrCode;
                    }
                    base.AddKillNode("currencytypetext");
                }
            }
            //Deb Multi Currency
            //Added by Amitosh for Policy interface
            if (!objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface || !m_fda.userLogin.IsAllowedEx(RMO_SEARCH + RMO_POLICY_SEARCH))
            {
                base.AddKillNode("multipolicyidPSDownloadbtn");
            }
            //Added by rupal for Policy interface
            if (!objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface)
            {
                base.AddKillNode("btnOtherunitloss");
            }
            //Start - averma62 MITS 25163- Policy Interface Implementation
            if (objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface && !objClaim.Context.InternalSettings.SysSettings.AllowPolicySearch)
            {
                base.AddKillNode("multipolicyidbtn");
            }
            //End - averma62 MITS 25163- Policy Interface Implementation

            //if (objClaim.IsNew)
            //    base.AddReadOnlyNode("multipolicyid");
            //End Amitosh
            //nsachdeva2 - 7/9/2012 - PolicySave
            int iTransCount = 0;
            ////Ashish Ahuja: Claims Made Jira 1342
            string sTransStatus = "false";
            //if (objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface && objClaim.ClaimId > 0)  //MITS 30212; aaggarwal29; ClaimID might come as 0 or -1 in case of new claim, so below query is not required for new claim. By default remove policy button should come for new WC claim
            if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1 && objClaim.ClaimId > 0)  //MITS 30212; aaggarwal29; ClaimID might come as 0 or -1 in case of new claim, so below query is not required for new claim. By default remove policy button should come for new WC claim
            {
                iTransCount = objClaim.Context.DbConn.ExecuteInt("SELECT COUNT(1) FROM FUNDS WHERE FUNDS.CLAIM_ID =" + objClaim.ClaimId);
                if (iTransCount > 0)
                {
                    base.AddKillNode("multipolicyidbtndel");
                    if (objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface)
                        sTransStatus = "true";//Ashish Ahuja: Claims Made Jira 1342
                }
                iTransCount = objClaim.Context.DbConn.ExecuteInt("SELECT COUNT(1) FROM RESERVE_CURRENT WHERE CLAIM_ID =" + objClaim.ClaimId);
                if (iTransCount > 0)
                {
                    base.AddKillNode("multipolicyidbtndel");
                    if (objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface)
                        sTransStatus = "true";////Ashish Ahuja: Claims Made Jira 1342
                }
            }

            //if (objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface && objClaim.ClaimId > 0)   //MITS 30212; aaggarwal29; ClaimID might come as 0 or -1 in case of new claim, so below query is not required for new claim. By default remove policy button should come for new WC claim
            if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1 && objClaim.ClaimId > 0)   //MITS 30212; aaggarwal29; ClaimID might come as 0 or -1 in case of new claim, so below query is not required for new claim. By default remove policy button should come for new WC claim
            {
                iTransCount = objClaim.Context.DbConn.ExecuteInt("SELECT COUNT(1) FROM FUNDS_AUTO WHERE FUNDS_AUTO.CLAIM_ID =" + objClaim.ClaimId);
                if (iTransCount > 0)
                {
                    base.AddKillNode("multipolicyidbtndel");
                    if (objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface)
                        sTransStatus = "true";////Ashish Ahuja: Claims Made Jira 1342
                }
            }
            ////Ashish Ahuja: Claims Made Jira 1342
            CreateSysExData("TransStatus", sTransStatus);
            //JIRA 1342 ajohari2: Start
            //sharishkumar Jira 12444 starts
            //if (String.Compare(sTransStatus, "true", true) == 0)
            if (String.Compare(sTransStatus, "true", true) == 0 && !objClaim.Context.InternalSettings.SysSettings.EditclaimEvtDate)
            //sharishkumar Jira 12444 ends
            {
                base.AddReadOnlyNode("ev_dateofevent");
                base.AddReadOnlyNode("dateofclaim");
                base.AddReadOnlyNode("clm_datereported");
            }
            //JIRA 1342 ajohari2: End

            if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm != -1)
            {//base.AddKillNode("policyLOBCode");
                ArrayList singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                base.AddElementToList(ref singleRow, "id", "lbl_policyLOBCode");
                base.AddElementToList(ref singleRow, "Class", "label");
                base.m_ModifiedControls.Add(singleRow);

                base.AddSysRequiredFields("policyLOBCode_codelookup_cid");

            }
            //PSHEKHAWAT : MITS-23383 : Hide/display control 'Email FROI to' (ID=emailaddresses) based on the value of AutoFROIACORDFlag in utilities          
            if (objSysSettings.AutoFROIACORDFlag == false)
                base.AddKillNode("emailaddresses");

            //Amandeep Catastrophe Enhancement MITS 28528--start
            if (!objClaim.IsNew)
                base.ResetSysExData("CatastropheNumber", GetCatastropheNumber());
            else
                base.ResetSysExData("CatastropheNumber", "");
            //Amandeep Catastrophe Enhancement MITS 28528--end

            //tanwar2 - ImageRight - start

            if (objClaim.Context.InternalSettings.SysSettings.UseImgRight == false)
            {
                base.AddKillNode("openImageRight");
                base.AddKillNode("generateFUP");
            }
            //NameValueCollection nvCol = RMConfigurationManager.GetNameValueSectionSettings("ImageRight");
            //if (nvCol != null && !string.IsNullOrEmpty(nvCol["IR_Drawer"]))
            //{
            //    base.ResetSysExData("irdrawer", nvCol["IR_Drawer"]);
            //}
            int iIRCodeId = 0;
            bool bSuccess = false;
            if (objClaim.Supplementals["IR_DRAWER_CODE"] != null)
            {
                iIRCodeId = Conversion.CastToType<int>(objClaim.Supplementals["IR_DRAWER_CODE"].Value.ToString(), out bSuccess);
                base.ResetSysExData("irdrawer", objClaim.Context.LocalCache.GetCodeDesc(iIRCodeId));
            }
            if (objClaim.Supplementals["IR_FILE_TYPE_CODE"] != null)
            {
                iIRCodeId = Conversion.CastToType<int>(objClaim.Supplementals["IR_FILE_TYPE_CODE"].Value.ToString(), out bSuccess);
                base.ResetSysExData("irfiletype", objClaim.Context.LocalCache.GetCodeDesc(iIRCodeId));
            }
            if (string.Compare(base.GetSysExDataNodeText("/SysExData/generateFUPFile"), "-1") == 0)
            {
                base.ResetSysExData("generateFUPFile", "-1");
            }
            else
            {
                base.ResetSysExData("generateFUPFile", "0");
            }

            //tanwar2 - ImageRight - end
                //added by gmallick JIRA 12965 Start
                if (!objClaim.Context.RMUser.IsAllowedEx(this.SecurityId, FormBase.RMO_UPDATE))
                {
                    base.ResetSysExData("updateflag", "false");
                }
                else
                {
                    base.ResetSysExData("updateflag", "true");
                }
            //added by gmallick JIRA 12965 End
        }
        
        private string GetCatastropheNumber()
        {
            string sSQL = "SELECT CAT_NUMBER FROM CATASTROPHE WHERE CATASTROPHE_ROW_ID = " + objClaim.CatastropheRowId;
            string sReturnValue = string.Empty;
            try
            {
                //using (DbReader objRdr = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                using (DbReader objRdr = objClaim.Context.DbConn.ExecuteReader(sSQL))
                {
                    if (objRdr.Read())
                        sReturnValue = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                }
            }
            catch (Exception e)
            {
                sReturnValue = string.Empty;
            }
            return sReturnValue;
        }
        //Amandeep Catastrophe Enhancement MITS 28528--end

        private void DisableEmpFields()
        {
            XmlElement objTempElement = null;
            XmlAttribute tempAttr = null;

            //employee info
            base.AddReadOnlyNode("emplastname");
            base.AddReadOnlyNode("emptaxid");
            base.AddReadOnlyNode("empfirstname");

            //Raman Bhatia: commented for R5

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empbirthdate']");//date
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            ArrayList singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Date.ToString());
            base.AddElementToList(ref singleRow, "id", "empbirthdate");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);


            base.AddReadOnlyNode("empmiddlename");
            base.AddReadOnlyNode("empabbreviation");
            base.AddReadOnlyNode("empalsoknownas");
            base.AddReadOnlyNode("empphone1");
            base.AddReadOnlyNode("empaddr1");
            base.AddReadOnlyNode("empphone2");
            base.AddReadOnlyNode("empaddr2");
            base.AddReadOnlyNode("empaddr3");// JIRA 6420 pkandhari
            base.AddReadOnlyNode("empaddr4");// JIRA 6420 pkandhari
            base.AddReadOnlyNode("empfaxnumber");
            base.AddReadOnlyNode("empcity");
            base.AddReadOnlyNode("empsexcode");
            base.AddReadOnlyNode("stateid");//Shruti for 11740
            base.AddReadOnlyNode("emptitle");
            base.AddReadOnlyNode("empzipcode");
            base.AddReadOnlyNode("empcostcentercode");
            base.AddReadOnlyNode("empcounty");
            base.AddReadOnlyNode("claimantattorney");
            base.AddReadOnlyNode("empcountrycode");

            //employement info
            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empdatehired1']");//date
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Date.ToString());
            base.AddElementToList(ref singleRow, "id", "empdatehired1");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);


            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empdateofdeath_empinfo']");//date
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Date.ToString());
            base.AddElementToList(ref singleRow, "id", "empdateofdeath_empinfo");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='emptermdate1']");//date
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Date.ToString());
            base.AddElementToList(ref singleRow, "id", "emptermdate1");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            base.AddReadOnlyNode("empmaritalstatcode_empinfo");
            base.AddReadOnlyNode("emppositioncode1");

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empworkpermitdate_empinfo']");//date
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Date.ToString());
            base.AddElementToList(ref singleRow, "id", "empworkpermitdate_empinfo");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            base.AddReadOnlyNode("empdeptassignedeid1");
            base.AddReadOnlyNode("empworkpermitnumber_empinfo");
            base.AddReadOnlyNode("empsupervisoreid1");
            base.AddReadOnlyNode("empncciclasscode_empinfo");
            base.AddReadOnlyNode("emppaytypecode1");
            //base.AddReadOnlyNode("jobclassification");  //pmittal5 12/15/08 -Commented for Mits 13724
            base.AddReadOnlyNode("empmonthlyrate");

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empworksunflag1']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empworksunflag1");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empworkthuflag1']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empworkthuflag1");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empworkmonflag1']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empworkmonflag1");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empworkfriflag1']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empworkfriflag1");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empworktueflag1']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empworktueflag1");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empworksatflag1']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empworksatflag1");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empworkwedflag1']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empworkwedflag1");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            base.AddReadOnlyNode("emppayamount");
            objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empactiveflag1']");//checkbox
            if (objTempElement != null)
            {
                if (objTempElement.Attributes["readonly"] != null)
                    objTempElement.Attributes["readonly"].Value = "true";
                else
                {
                    tempAttr = base.SysView.CreateAttribute("readonly");
                    objTempElement.Attributes.Append(tempAttr);
                    objTempElement.Attributes["readonly"].Value = "true";
                }
            }

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empactiveflag1");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            base.AddReadOnlyNode("emphourlyrate1");
            base.ReadWriteNodes.Remove("emphourlyrate1k"); //pmittal5 08/24/09 Mits 13724

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empfulltimeflag1']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empfulltimeflag1");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            base.AddReadOnlyNode("empweeklyhours1");
            base.ReadWriteNodes.Remove("empweeklyhours1k"); //pmittal5 08/24/09 Mits 13724 - Remove the node from ReadWriteNodes list, if present there

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empexemptstatusflag1']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empexemptstatusflag1");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            base.AddReadOnlyNode("empweeklyrate1");
            base.ReadWriteNodes.Remove("empweeklyrate1k"); //pmittal5 08/24/09 Mits 13724 

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='emphiredinsteflag1']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "emphiredinsteflag1");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            //employement info(case management)
            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empdatehired2']");//date
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Date.ToString());
            base.AddElementToList(ref singleRow, "id", "empdatehired2");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='emptermdate2']");//date
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Date.ToString());
            base.AddElementToList(ref singleRow, "id", "emptermdate2");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            base.AddReadOnlyNode("emppositioncode2");
            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empactiveflag2']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empactiveflag2");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            base.AddReadOnlyNode("empdeptassignedeid2");
            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empfulltimeflag2']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empfulltimeflag2");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            base.AddReadOnlyNode("empsupervisoreid2");
            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empexemptstatusflag2']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empexemptstatusflag2");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            base.AddReadOnlyNode("emppaytypecode2");
            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='emphiredinsteflag2']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "emphiredinsteflag2");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            base.AddReadOnlyNode("empmonthlyrate_1");  //pmittal5 Mits 13724 08/24/09 - Name of the control changed
            base.AddReadOnlyNode("emptimeworkdaybegan");
            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empworksunflag2']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empworksunflag2");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empworkmonflag2']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empworkmonflag2");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);
            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empworktueflag2']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empworktueflag2");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empworkwedflag2']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empworkwedflag2");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empworkthuflag2']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empworkthuflag2");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empworkfriflag2']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empworkfriflag2");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            //objTempElement = (XmlElement)base.SysView.SelectSingleNode("//control[@name='empworksatflag2']");//checkbox
            //if (objTempElement != null)
            //{
            //    if (objTempElement.Attributes["readonly"] != null)
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    else
            //    {
            //        tempAttr = base.SysView.CreateAttribute("readonly");
            //        objTempElement.Attributes.Append(tempAttr);
            //        objTempElement.Attributes["readonly"].Value = "true";
            //    }
            //}
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Checkbox.ToString());
            base.AddElementToList(ref singleRow, "id", "empworksatflag2");
            base.AddElementToList(ref singleRow, "readonly", "true");
            base.m_ModifiedControls.Add(singleRow);

            base.AddReadOnlyNode("emppayamount_1");  //pmittal5 Mits 13724 08/24/09 - Name of the control changed
            base.AddReadOnlyNode("emphourlyrate2");
            base.AddReadOnlyNode("empweeklyhours2");
            base.AddReadOnlyNode("empweeklyrate2");

            //pmittal5 Mits 13724 08/24/09 
            base.AddReadOnlyNode("calcaww1");   //Disable "AWW Rate Calculations" button
            base.AddReadOnlyNode("calcaww4");
            base.AddReadOnlyNode("calcaww2");   //Disable "Indemnity Rate Calculations" button
            base.AddReadOnlyNode("calcaww3");
            base.ReadWriteNodes.Remove("emphourlyrate2k");
            base.ReadWriteNodes.Remove("empweeklyhours2k");
            base.ReadWriteNodes.Remove("empweeklyrate2k");
            //End - pmittal5
        }

        internal override bool ValidateRequiredViewFields(XmlDocument propertyStore)
        {
            CheckAutoGenerateEmpNo();

            return base.ValidateRequiredViewFields(propertyStore);
        }

        private void CheckAutoGenerateEmpNo()
        {
            //Bug No: 636,649,672. For Autogenerating Employee Number
            //Nikhil Garg		Dated: 20/Dec/2005
            XmlElement objNotReqNew = null;
            string sNotReqNew = string.Empty;
            if (objData.Context.InternalSettings.SysSettings.AutoNumWCEmp)
            {
                objNotReqNew = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysNotReqNew");
                sNotReqNew = objNotReqNew.InnerText;
                if (sNotReqNew.IndexOf("empemployeenumber") == -1)
                    sNotReqNew = sNotReqNew + "|empemployeenumber|";
                objNotReqNew.InnerText = sNotReqNew;
            }
        }

        #region Verify AWW Calculator
        private int GetAWWCalculator()
        {
            int iAWW = -1;
            iAWW = objClaim.Context.DbConnLookup.ExecuteInt("SELECT AWW_FORM_OPTION FROM STATES WHERE STATE_ID = " + objClaim.FilingStateId + " AND AWW_FORM_OPTION > 0");
            return iAWW;
        }
        #endregion
        #region Load Primary Employee Childs at runtime
        //Get the Work Loss days count between 2 dates.
        private void subInitHolidayInfo()
        {
            string sDate1 = string.Empty;
            string sDate2 = string.Empty;
            int i = 0;
            int j = 0;
            int count = 1;

            using (DbReader objCount = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, "SELECT COUNT(*) COUNTI FROM SYS_HOLIDAYS"))
            {
                while (objCount.Read())
                {
                    count = Conversion.ConvertObjToInt(objCount.GetValue("COUNTI"), base.ClientId);
                    SYS_HOLIDAYS = new Holidays[count];
                }
            }

            using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, "SELECT HOL_DATE,ORG_EID FROM SYS_HOLIDAYS ORDER BY HOL_DATE,ORG_EID"))
            {
                while (objReader.Read())
                {

                    sDate1 = objReader.GetString("HOL_DATE");
                    if (sDate1 != sDate2)
                    {
                        if (sDate2 != "")
                            i = i + 1;
                        SYS_HOLIDAYS[i].sDate = sDate1;
                        j = 0;
                        SYS_HOLIDAYS[i].lOrgID = new long[count];
                        SYS_HOLIDAYS[i].lOrgID[j] = Conversion.ConvertObjToInt64(objReader.GetValue("ORG_EID"), base.ClientId);
                        sDate2 = sDate1;
                    }
                    else
                    {
                        j = j + 1;
                        SYS_HOLIDAYS[i].lOrgID[j] = Conversion.ConvertObjToInt64(objReader.GetValue("ORG_EID"), base.ClientId);
                    }
                }
            }
        }
        private int GetWorkDay(DateTime dDate1, DateTime dDate2, string[] sDaysOfWeek)
        {
            int iDays = 0;
            int iDayOfWeekStart = 0;
            int iDayOfWeekEnd = 0;
            int iNewDays = 0;
            string sParent = string.Empty;
            TimeSpan objDays;

            objDays = dDate2.Subtract(dDate1);
            iDays = objDays.Days + 1;
            iDayOfWeekStart = (int)dDate1.DayOfWeek;
            iDayOfWeekEnd = (int)dDate2.DayOfWeek;
            for (int i = 1; i <= iDays; i++)
            {
                if (sDaysOfWeek[(int)dDate1.AddDays(i - 1).DayOfWeek] == "True")
                    iNewDays = iNewDays + 1;
            }
            return iNewDays;
        }
        private int GetWorkLossDayCount(DateTime sLastWorkDate, DateTime sReturnedToWorkDate, string[] sDaysOfWeek)
        {
            DateTime objLastWorkedDate = System.DateTime.Today;
            DateTime objReturnedToWorkDate = System.DateTime.Today;
            TimeSpan objDays;
            int iDayOfWeekStart = 0;
            int iDayOfWeekEnd = 0;
            int iNewDays = 0;
            int iDays = 0;
            string sParent = string.Empty;

            objLastWorkedDate = sLastWorkDate.AddDays(1);
            objReturnedToWorkDate = sReturnedToWorkDate;
            objDays = sReturnedToWorkDate.Subtract(objLastWorkedDate);
            iDays = objDays.Days;
            iDayOfWeekStart = (int)objLastWorkedDate.DayOfWeek;
            iDayOfWeekEnd = (int)objReturnedToWorkDate.DayOfWeek;
            subInitHolidayInfo();
            for (int i = 1; i <= iDays; i++)
            {
                if (sDaysOfWeek[(int)objLastWorkedDate.AddDays(i - 1).DayOfWeek] == "True")
                    iNewDays = iNewDays + 1;
            }
            return iNewDays;

        }

        private void RenderPolicyList(ref XmlDocument objXML)
        {
            XmlElement objPolicyListElement = null;
            string sCodeId = string.Empty;
            Policy objPolicy = (Policy)m_fda.Factory.GetDataModelObject("Policy", false);
            string sDataToDisplay = string.Empty;//skhare7 Policy Interface
            try
            {

                objPolicyListElement = (XmlElement)objXML.SelectSingleNode("/SysExData/ClaimPolicyList");
                if (objPolicyListElement != null)
                    objPolicyListElement.ParentNode.RemoveChild(objPolicyListElement);

                CreateElement(objXML.DocumentElement, "ClaimPolicyList", ref objPolicyListElement);

                foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
                {
                    if (string.IsNullOrEmpty(sCodeId) && objClaimXPolicy.PolicyId != 0)
                    {
                        sCodeId = objClaimXPolicy.PolicyId.ToString();
                    }
                    else if (objClaimXPolicy.PolicyId != 0)
                    {
                        sCodeId = string.Concat(sCodeId + " " + objClaimXPolicy.PolicyId.ToString());
                    }
                    if (objClaimXPolicy.PolicyId != 0)
                    {

                        objPolicy.MoveTo(objClaimXPolicy.PolicyId);
                        if (objPolicy.PolicySystemId > 0)
                        {
                            //Fixed review comments for MITS- 34260/RMA-4362 :pgupta93: START
                            //sDataToDisplay = CommonForm.PointPolicyDataToDisplay(objPolicy.Context.DbConn.ConnectionString, objClaimXPolicy.PolicyId);//skhare7 Policy Interface
                            sDataToDisplay = objPolicy.PolicyName;
                            //Fixed review comments for MITS- 34260/RMA-4362 :pgupta93: END
                            if (sDataToDisplay != string.Empty)
                                CreateAndSetElement(objPolicyListElement, "Item", sDataToDisplay, objPolicy.PolicyId);
                            else
                                CreateAndSetElement(objPolicyListElement, "Item", objPolicy.PolicySymbol + "|" + objPolicy.PolicyNumber + "|" + objPolicy.Module, objPolicy.PolicyId);
                        }
                        else
                            CreateAndSetElement(objPolicyListElement, "Item", objPolicy.PolicyName, objPolicy.PolicyId);
                    }
                }
                //objPolicyListElement = (XmlElement)objXML.SelectSingleNode("/SysExData/ClaimPolicyList");
                objPolicyListElement.SetAttribute("codeid", sCodeId);

            }
            finally
            {
                objPolicyListElement = null;
            }
        }

        /// <summary>
        /// Create Element
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_sNodeName">Child Node Name</param>
        /// <param name="p_objChildNode">Child Node</param>
        internal void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", base.ClientId), p_objEx);
            }

        }
        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Text</param>
        /// <param name="p_iPolicyId">Attribute name</param>
        internal void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, int p_iPolicyId)
        {
            try
            {
                XmlElement objChildNode = null;
                this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
                objChildNode.SetAttribute("value", p_iPolicyId.ToString());
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", base.ClientId), p_objEx);
            }
        }
        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Text</param>
        /// <param name="p_objChildNode">Child Node</param>
        internal void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode)
        {
            try
            {
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
                p_objChildNode.InnerText = p_sText;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", base.ClientId), p_objEx);
            }
        }

        private void LoadUnitStat(ref XmlDocument objXML)
        {
            XmlElement objElem = null;
            XmlElement objElemTmp = null;
            UnitStat objUnitStat = null;
            XmlNode objOldNode = null;
            string sXml = "";
            XmlDocument objDocument = new XmlDocument();

            if (iUseUnitStat == -1)
            {
                objOldNode = null;
                objOldNode = objXML.SelectSingleNode("//UnitStat");

                //Load Unit Stat                            
                objUnitStat = objClaim.UnitStat;
                objElem = objXML.CreateElement("UnitStat");
                sXml = objUnitStat.SerializeObject();

                objDocument.LoadXml(sXml);
                objElem.InnerXml = objDocument.SelectSingleNode("//UnitStat").InnerXml;

                if (objOldNode != null)
                    objXML.DocumentElement.ReplaceChild(objElem, objOldNode);
                else
                    objXML.DocumentElement.AppendChild(objElem);




                base.AddDisplayNode("unitstat");
                base.ResetSysExData("UseUnitStat", "true");
            }
            else
            {
                base.ResetSysExData("UseUnitStat", "false");
                base.AddKillNode("unitstat");
            }
        }


        private void LoadPiEmployeeChilds(ref XmlDocument objXML)
        {
            //Creates Node PiXDiagHist for Diagnosis History for Medical Info and Case Management.
            //Added By Rahul.
            XmlNode objOldNode = objXML.SelectSingleNode("//PiXDiagHistList");
            XmlDocument objTempDoc = new XmlDocument();
            XmlElement objElem = null;

            objElem = null;
            #region Load MMI History
            //Create XML for MMI History . Added by Rahul Sharma
            objOldNode = null;
            objOldNode = objXML.SelectSingleNode("//PiXMMIHistList");
            objElem = null;
            int iMaxPiXMMIHistid = 0;
            PiXMMIHist objXMMIHist = null;
            //rsushilaggar changes made for performance improvement
            PiXMMIHistList objPiXMMIHistList = objClaim.PrimaryPiEmployee.PiXMMIHistList;
            if (objPiXMMIHistList.Count != 0)
            {
                //Finding the max. row id to display MMI History.
                foreach (PiXMMIHist objPiXMMIHist in objPiXMMIHistList)
                {
                    if (objPiXMMIHist.PiXMmirowId > iMaxPiXMMIHistid)
                    {
                        //sgoel6 04/28/2009 MITS 14887
                        if (objPiXMMIHist.DeletedFlag != -1)
                            iMaxPiXMMIHistid = objPiXMMIHist.PiXMmirowId;
                    }
                }
                objElem = objXML.CreateElement("PiXMMIHistList");
                if (iMaxPiXMMIHistid == 0)
                    objXMMIHist = (PiXMMIHist)objClaim.Context.Factory.GetDataModelObject("PiXMMIHist", false);
                else
                    objXMMIHist = objPiXMMIHistList[iMaxPiXMMIHistid];

                //Creating XML for MMI History
                objElem.InnerXml = objXMMIHist.SerializeObject();
            }
            else
            {
                objElem = objXML.CreateElement("PiXMMIHistList");
                objXMMIHist = (PiXMMIHist)objClaim.Context.Factory.GetDataModelObject("PiXMMIHist", false);
                objElem.InnerXml = objXMMIHist.SerializeObject();
            }
            if (objOldNode != null)
                objXML.DocumentElement.ReplaceChild(objElem, objOldNode);
            else
                objXML.DocumentElement.AppendChild(objElem);
            //End of MMI History.
            #endregion
            if (iUseCaseMgmt == -1)
            {
                #region Load the Highest Diagnosis History
                PiXDiagHist objDiaHist = null;
                PiXDiagHist objDiaHist10 = null;
                int iMaxDays = 0;
                int iMinDays = 0;
                int iOptDays = 0;
                objOldNode = objXML.SelectSingleNode("//PiXDiagHistList");
                string pirowid = objClaim.PrimaryPiEmployee.PiRowId.ToString();    //MITS 32423 :Praveen ICD-10 Changes
                objDiaHist = objClaim.PrimaryPiEmployee.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHistbyPiRowId(pirowid);    //MITS 32423 :Praveen ICD-10 Changes
                //objDiaHist10 = objClaim.PrimaryPiEmployee.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHistICD10byPiRowId(pirowid);

                if (objDiaHist != null)
                {
                    iMaxDays = objDiaHist.DisMaxDuration;
                    iMinDays = objDiaHist.DisMinDuration;
                    iOptDays = objDiaHist.DisOptDuration;
                }
                else
                    objDiaHist = objClaim.Context.Factory.GetDataModelObject("PiXDiagHist", false) as PiXDiagHist;

                objElem = objXML.CreateElement("PiXDiagHistList");
                objElem.InnerXml = objDiaHist.SerializeObject();

                if (objOldNode != null)
                    objXML.DocumentElement.ReplaceChild(objElem, objOldNode);
                else
                    objXML.DocumentElement.AppendChild(objElem);

                //create dummy sysex nodes for Binding Min/Max/Opt values


                //MITS 32423 : Praveen Load ICD10 Diagonosis start
                objElem = null;


                //tkatsarski: 11/12/14 Start changes for RMA-3912
                objOldNode = objXML.SelectSingleNode("//PiXDiagHistICD10List");
                objDiaHist10 = objClaim.PrimaryPiEmployee.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHistICD10byPiRowId(pirowid);
                if (objDiaHist10 != null)
                {
                    //Can modify data here
                }
                else
                    objDiaHist10 = objClaim.Context.Factory.GetDataModelObject("PiXDiagHist", false) as PiXDiagHist;

                objElem = objXML.CreateElement("PiXDiagHistICD10List");
                objElem.InnerXml = objDiaHist10.SerializeObject();

                if (objOldNode != null)
                    objXML.DocumentElement.ReplaceChild(objElem, objOldNode);
                else
                    objXML.DocumentElement.AppendChild(objElem);
                //tkatsarski: 11/12/14 End changes for RMA-3912

                base.ResetSysExData("ICDFlag", "0");
                base.ResetSysExData("hdnicdSelected", "0");

                //string latestICD = string.Empty;

                //if (objDiaHist.PiXDiagrowId > objDiaHist10.PiXDiagrowId)
                //{
                //    if (!string.IsNullOrEmpty(objDiaHist.DisMdaTopic))
                //    {
                //        latestICD = "ICD9";
                //    }
                //    else
                //    {
                //        latestICD = "ICD10";
                //    }
                //}
                //else
                //{
                //    if (!string.IsNullOrEmpty(objDiaHist10.DisMdaTopic))
                //    {
                //        latestICD = "ICD10";
                //    }
                //    else
                //    {
                //        latestICD = "ICD9";
                //    }
                //}

                //if (objDiaHist.PiXDiagrowId > objDiaHist10.PiXDiagrowId && !string.IsNullOrEmpty(objDiaHist.DisMdaTopic))
                if (objDiaHist.PiXDiagrowId > objDiaHist10.PiXDiagrowId)
                {
                    base.ResetSysExData("PiXDiagHist_DisMaxDuration", objDiaHist.DisMaxDuration.ToString());
                    base.ResetSysExData("PiXDiagHist_DisMinDuration", objDiaHist.DisMinDuration.ToString());
                    base.ResetSysExData("PiXDiagHist_DisOptDuration", objDiaHist.DisOptDuration.ToString());
                    base.ResetSysExData("DisMdaTopic", objDiaHist.DisMdaTopic.ToString());
                    base.ResetSysExData("DisFactor", objDiaHist.DisFactor.ToString());
                }
                else
                {
                    base.ResetSysExData("PiXDiagHist_DisMaxDuration", objDiaHist10.DisMaxDuration.ToString());
                    base.ResetSysExData("PiXDiagHist_DisMinDuration", objDiaHist10.DisMinDuration.ToString());
                    base.ResetSysExData("PiXDiagHist_DisOptDuration", objDiaHist10.DisOptDuration.ToString());
                    base.ResetSysExData("DisMdaTopic", objDiaHist10.DisMdaTopic.ToString());
                    base.ResetSysExData("DisFactor", objDiaHist10.DisFactor.ToString());
                }

                objDiaHist = null;
                objDiaHist10 = null;
                //MITS 32423 : Praveen Load ICD10 Diagonosis End
                //zmohammad MITs 35169 : Checking for SMS permission.
                if (!objClaim.Context.RMUser.IsAllowedEx(this.SecurityId, FormBase.RMO_ALLOW_EDIT_MDA_DURATIONS))
                {
                    base.AddReadOnlyNode("mdaminimumdays1");
                    base.AddReadOnlyNode("mdaoptimumdays1");
                    base.AddReadOnlyNode("mdamaximumdays1");
                }
                //zmohammad MITs 35169 : Checking for SMS permission.
                #endregion

                #region Load Primary Case Manager
                //Creating XML for case Management History -- Added By Rahul Sharma.
                CmXCmgrHist objCmMgrHist = null;
                objOldNode = null;
                objOldNode = objXML.SelectSingleNode("//CmXCmgrHistList");
                objElem = null;
                // Code moved from Init() and added here to get Primary Case Manager . 
                //Its manadatory for displaying data of Primary Case Manager in Case Management Tab.
                //Start.
                CmXCmgrHistList objCaseMgrHistList = objClaim.CaseManagement.CaseMgrHistList;
                foreach (CmXCmgrHist objCmXCmgrHist in objCaseMgrHistList)
                {
                    if (objCmXCmgrHist.PrimaryCmgrFlag)
                    {
                        iCaseMgmtHisId = objCmXCmgrHist.CmcmhRowId;
                    }
                }
                //End.
                if (objCaseMgrHistList.Count != 0)
                {
                    if (iCaseMgmtHisId != 0)
                    {
                        objCmMgrHist = objCaseMgrHistList[iCaseMgmtHisId];

                        int iCaseMgrDays = 0;
                        TimeSpan objDateDiff;
                        objElem = objXML.CreateElement("CmXCmgrHistList");
                        objElem.InnerXml = objCmMgrHist.SerializeObject();
                        if (objCmMgrHist.DateClosed == "" || objCmMgrHist.DateClosed == null)
                        {
                            if (objCmMgrHist.RefDate == "" || objCmMgrHist.RefDate == null)
                                iCaseMgrDays = 0;
                            else
                            {
                                objDateDiff = System.DateTime.Today.Subtract(Conversion.ToDate(objCmMgrHist.RefDate));
                                iCaseMgrDays = objDateDiff.Days;
                            }
                        }
                        else
                        {
                            if (objCmMgrHist.RefDate == "" || objCmMgrHist.RefDate == null)
                                iCaseMgrDays = 0;
                            else
                            {
                                objDateDiff = Conversion.ToDate(objCmMgrHist.DateClosed).Subtract(Conversion.ToDate(objCmMgrHist.RefDate));
                                iCaseMgrDays = objDateDiff.Days;
                            }
                        }
                        if (iCaseMgrDays < 0)
                            base.ResetSysExData("CaseMgrDays", "");
                        else
                            base.ResetSysExData("CaseMgrDays", iCaseMgrDays.ToString());
                    }
                    else
                    {
                        objElem = objXML.CreateElement("CmXCmgrHistList");
                        objCmMgrHist = (CmXCmgrHist)objClaim.Context.Factory.GetDataModelObject("CmXCmgrHist", false);
                        objElem.InnerXml = objCmMgrHist.SerializeObject();
                        base.ResetSysExData("CaseMgrDays", "");
                    }
                }
                else
                {
                    objElem = objXML.CreateElement("CmXCmgrHistList");
                    objCmMgrHist = (CmXCmgrHist)objClaim.Context.Factory.GetDataModelObject("CmXCmgrHist", false);
                    objElem.InnerXml = objCmMgrHist.SerializeObject();
                    base.ResetSysExData("CaseMgrDays", "");
                }
                if (objOldNode != null)
                    objXML.DocumentElement.ReplaceChild(objElem, objOldNode);
                else
                    objXML.DocumentElement.AppendChild(objElem);
                #endregion

                #region Load Work Loss
                objOldNode = null;
                objOldNode = objXML.SelectSingleNode("//PiXWorkLossList");
                objElem = null;
                int iPiXWorkLoss = 0;
                PiXWorkLoss objPiX_WorkLoss = null;
                DateTime objDate1 = System.DateTime.Today;
                DateTime objDate2 = System.DateTime.Today;
                PiXWorkLossList objPiXWorkLossList = objClaim.PrimaryPiEmployee.PiXWorkLossList;
                if (objPiXWorkLossList.Count != 0)
                {

                    //Start rsushilaggar MITS 21965 Date 12/08/2010
                    PiXWorkLoss tempPiWorkLoss = null;
                    foreach (PiXWorkLoss objPiXWorkLoss in objPiXWorkLossList)
                    {
                        if (tempPiWorkLoss != null)
                        {
                            if (tempPiWorkLoss.DateReturned.CompareTo(objPiXWorkLoss.DateReturned) < 0)
                            {
                                tempPiWorkLoss = objPiXWorkLoss;
                            }
                        }
                        else
                        {
                            tempPiWorkLoss = objPiXWorkLoss;
                        }
                    }

                    iPiXWorkLoss = tempPiWorkLoss.PiWlRowId;
                    //End rsushilaggar


                    objPiX_WorkLoss = objPiXWorkLossList[iPiXWorkLoss];
                    if (objClaim.CaseManagement.EstRetWorkDate != "")
                        objDate2 = Conversion.ToDate(objClaim.CaseManagement.EstRetWorkDate);
                    objDate1 = Conversion.ToDate(objPiX_WorkLoss.DateLastWorked);
                    if (objClaim.PrimaryPiEmployee.JobClassCode != 0)
                    {
                        DateTime objMaxDate = System.DateTime.Today;
                        DateTime objMinDate = System.DateTime.Today;
                        DateTime objOptDate = System.DateTime.Today;
                        if (objPiX_WorkLoss.DateLastWorked != "")
                        {
                            objMaxDate = Conversion.ToDate(objPiX_WorkLoss.DateLastWorked).AddDays(Conversion.ConvertObjToDouble(iMaxDays, base.ClientId) + 1);
                            base.ResetSysExData("WL_MaxDate", objMaxDate.ToShortDateString());
                            objMinDate = Conversion.ToDate(objPiX_WorkLoss.DateLastWorked).AddDays(Conversion.ConvertObjToDouble(iMinDays, base.ClientId) + 1);
                            base.ResetSysExData("WL_MinDate", objMinDate.ToShortDateString());
                            objOptDate = Conversion.ToDate(objPiX_WorkLoss.DateLastWorked).AddDays(Conversion.ConvertObjToDouble(iOptDays, base.ClientId) + 1);
                            base.ResetSysExData("WL_OptDate", objOptDate.ToShortDateString());
                        }
                        else
                        {
                            base.ResetSysExData("WL_MaxDate", "");
                            base.ResetSysExData("WL_MinDate", "");
                            base.ResetSysExData("WL_OptDate", "");
                        }
                    }
                    else
                    {
                        base.ResetSysExData("WL_MaxDate", "");		// Nikhil Garg 01.16.2006   Needs to get created if not already
                        base.ResetSysExData("WL_MinDate", "");		// Nikhil Garg 01.16.2006   Needs to get created if not already
                        base.ResetSysExData("WL_OptDate", "");		// Nikhil Garg 01.16.2006   Needs to get created if not already
                    }
                    int iTotalLostDays = 0;
                    int iEstDisability = 0;
                    if (objPiX_WorkLoss.DateReturned == "")
                    {
                        base.ResetSysExData("ChkWorkLoss", "True");

                        iEstDisability = GetWorkLossDayCount(Conversion.ToDate(objPiX_WorkLoss.DateLastWorked), objDate2, sDaysOfWeek);
                        base.ResetSysExData("EstDisability", iEstDisability.ToString() + " Days");
                        int i = 0;
                        foreach (PiXWorkLoss objPiXWorkLoss in objPiXWorkLossList)
                        {
                            if (i > 0)
                            {
                                iTotalLostDays = iTotalLostDays + objPiXWorkLoss.Duration;
                            }
                            ++i;
                        }
                        iTotalLostDays = iTotalLostDays + GetWorkLossDayCount(Conversion.ToDate(objPiX_WorkLoss.DateLastWorked), System.DateTime.Today, sDaysOfWeek);
                    }
                    else
                    {
                        base.ResetSysExData("ChkWorkLoss", "False");
                        foreach (PiXWorkLoss objPiXWorkLoss in objPiXWorkLossList)
                            iTotalLostDays = iTotalLostDays + objPiXWorkLoss.Duration;

                        //						base.ResetSysExData("EstDisability","");	// JP 12.20.2005   Needs to get created if not already
                        //						base.ResetSysExData("WL_MaxDate","");		// JP 12.20.2005   Needs to get created if not already
                        //						base.ResetSysExData("WL_MinDate","");		// JP 12.20.2005   Needs to get created if not already
                        //						base.ResetSysExData("WL_OptDate","");		// JP 12.20.2005   Needs to get created if not already
                    }
                    base.ResetSysExData("TotalLostDays", iTotalLostDays.ToString() + " Days");
                    objElem = objXML.CreateElement("PiXWorkLossList");
                    objElem.InnerXml = objPiX_WorkLoss.SerializeObject();
                }
                else
                {
                    objElem = objXML.CreateElement("PiXWorkLossList");
                    objPiX_WorkLoss = (PiXWorkLoss)objClaim.Context.Factory.GetDataModelObject("PiXWorkLoss", false);
                    objElem.InnerXml = objPiX_WorkLoss.SerializeObject();
                    base.ResetSysExData("TotalLostDays", "");
                    base.ResetSysExData("ChkWorkLoss", "False");
                    base.ResetSysExData("WL_MaxDate", "");
                    base.ResetSysExData("WL_MinDate", "");
                    base.ResetSysExData("WL_OptDate", "");
                    base.ResetSysExData("EstDisability", "");
                }
                if (objOldNode != null)
                    objXML.DocumentElement.ReplaceChild(objElem, objOldNode);
                else
                    objXML.DocumentElement.AppendChild(objElem);
                #endregion

                #region Load Restriction
                // Creating XML for PI Restriction--- Added By Rahul Sharma.
                objOldNode = null;
                objOldNode = objXML.SelectSingleNode("//PiXRestrictList");
                objElem = null;
                int iPiXRestrict = 0;
                PiXRestrict objPiX_Restrict = null;
                PiXRestrictList objPiXRestrictList = objClaim.PrimaryPiEmployee.PiXRestrictList;
                if (objPiXRestrictList.Count != 0)
                {
                    foreach (PiXRestrict objPiXRestrict in objPiXRestrictList)
                    {
                        iPiXRestrict = objPiXRestrict.PiRestrictRowId;
                        break;
                    }
                    string sDateLastRes = "False";
                    int iEstLenDis = 0;
                    int iTotalRestDays = 0;
                    objPiX_Restrict = objPiXRestrictList[iPiXRestrict];
                    objElem = objXML.CreateElement("PiXRestrictList");
                    objElem.InnerXml = objPiX_Restrict.SerializeObject();

                    DateTime sDate2 = System.DateTime.Today;

                    if (objClaim.CaseManagement.EstRelRstDate != "")
                        sDate2 = Conversion.ToDate(objClaim.CaseManagement.EstRelRstDate);
                    DateTime sDate1 = System.DateTime.Today;
                    sDate1 = Conversion.ToDate(objPiX_Restrict.DateFirstRestrct);

                    if (objPiX_Restrict.DateLastRestrct == "")
                    {
                        sDateLastRes = "True";
                        int i = 0;
                        foreach (PiXRestrict objPiXRestrict in objPiXRestrictList)
                        {
                            if (i > 0)
                            {
                                iTotalRestDays = iTotalRestDays + objPiXRestrict.Duration;
                            }
                            ++i;
                        }
                        iEstLenDis = GetWorkDay(Conversion.ToDate(objPiX_Restrict.DateFirstRestrct), sDate2, sDaysOfWeek);
                        base.ResetSysExData("EstLenDis", iEstLenDis.ToString() + " Days");
                        iTotalRestDays = iTotalRestDays + GetWorkDay(Conversion.ToDate(objPiX_Restrict.DateFirstRestrct), System.DateTime.Today, sDaysOfWeek);
                        base.ResetSysExData("TotalResDays", iTotalRestDays.ToString() + " Days");
                    }
                    else
                    {
                        sDateLastRes = "False";
                        foreach (PiXRestrict objPiXRestrict in objPiXRestrictList)
                            iTotalRestDays = iTotalRestDays + objPiXRestrict.Duration;
                        base.ResetSysExData("EstLenDis", "");
                        base.ResetSysExData("TotalResDays", iTotalRestDays.ToString() + " Days");
                    }
                    base.ResetSysExData("ChkCurrResDays", sDateLastRes);
                    if (objClaim.PrimaryPiEmployee.JobClassCode != 0)
                    {
                        DateTime objMaxDate = System.DateTime.Today;
                        DateTime objMinDate = System.DateTime.Today;
                        DateTime objOptDate = System.DateTime.Today;
                        if (objPiX_Restrict.DateFirstRestrct != "")
                        {
                            objMaxDate = Conversion.ToDate(objPiX_Restrict.DateFirstRestrct).AddDays(Conversion.ConvertObjToDouble(iMaxDays, base.ClientId));
                            base.ResetSysExData("RW_MaxDate", objMaxDate.ToShortDateString());
                            objMinDate = Conversion.ToDate(objPiX_Restrict.DateFirstRestrct).AddDays(Conversion.ConvertObjToDouble(iMinDays, base.ClientId));
                            base.ResetSysExData("RW_MinDate", objMinDate.ToShortDateString());
                            objOptDate = Conversion.ToDate(objPiX_Restrict.DateFirstRestrct).AddDays(Conversion.ConvertObjToDouble(iOptDays, base.ClientId));
                            base.ResetSysExData("RW_OptDate", objOptDate.ToShortDateString());
                        }
                        else
                        {
                            base.ResetSysExData("RW_MaxDate", "");
                            base.ResetSysExData("RW_MinDate", "");
                            base.ResetSysExData("RW_OptDate", "");
                        }
                    }
                    else
                    {
                        base.ResetSysExData("RW_MaxDate", "");
                        base.ResetSysExData("RW_MinDate", "");
                        base.ResetSysExData("RW_OptDate", "");
                    }
                }
                else
                {
                    objElem = objXML.CreateElement("PiXRestrictList");
                    objPiX_Restrict = (PiXRestrict)objClaim.Context.Factory.GetDataModelObject("PiXRestrict", false);
                    objElem.InnerXml = objPiX_Restrict.SerializeObject();
                    base.ResetSysExData("ChkCurrResDays", "False");
                    base.ResetSysExData("RW_MaxDate", "");
                    base.ResetSysExData("RW_MinDate", "");
                    base.ResetSysExData("RW_OptDate", "");
                    base.ResetSysExData("EstLenDis", "");
                    base.ResetSysExData("TotalResDays", "");
                }
                if (objOldNode != null)
                    objXML.DocumentElement.ReplaceChild(objElem, objOldNode);
                else
                    objXML.DocumentElement.AppendChild(objElem);
                #endregion

                base.AddKillNode("employmentinfowocasemgmt");
                base.AddKillNode("employeeeventdetail");
                base.AddDisplayNode("medicalinfo");
                base.AddDisplayNode("casemgt");
                base.AddDisplayNode("employmentinfo");
                base.AddDisplayNode("empworkdaystarttime");
                base.AddDisplayNode("empregularjobflag1");
                //base.AddDisplayNode("empregularjobflag2");    //gagnihotri MITS 14319 
                base.AddDisplayNode("emplostconsciousness");
                base.AddDisplayNode("emposharecordable");
                base.AddDisplayNode("emposhaaccdesc");
                base.ResetSysExData("UseCaseMgmt", "true");
            }
            else
            {
                base.ResetSysExData("UseCaseMgmt", "false");
                base.AddKillNode("medicalinfo");
                base.AddKillNode("casemgt");
                base.AddKillNode("employmentinfo");
                base.AddKillNode("empworkdaystarttime");
                base.AddKillNode("empregularjobflag1");
                //base.AddKillNode("empregularjobflag2");       //gagnihotri MITS 14319
                base.AddKillNode("emplostconsciousness");
                base.AddKillNode("emposharecordable");
                base.AddKillNode("emposhaaccdesc");
                base.AddDisplayNode("employmentinfowocasemgmt");
                base.AddDisplayNode("employeeeventdetail");

            }
        }
        #endregion
        private bool DiagHistIsModified(PiXDiagHist objOrig, PiXDiagHist objNew)
        {
            if (objNew == null)
                return false;
            if (objOrig == null)
            {
                if (objNew.DiagnosisCode != 0)
                    return true;
                else
                    return false;
            }
            if (objOrig.DiagnosisCode != objNew.DiagnosisCode) return true;
            if (objOrig.DateChanged != objNew.DateChanged) return true;
            if (objOrig.ChgdByUser != objNew.ChgdByUser) return true;
            //  npadhy MITS 18552 MDA Topic not getting saved
            if (objOrig.DisMdaTopic != objNew.DisMdaTopic) return true;
            if (objOrig.DisFactor != objNew.DisFactor) return true;
            if (objOrig.ApprovedByUser != objNew.ApprovedByUser) return true;
            if (objOrig.Reason != objNew.Reason) return true;
            //zmohammad : MITs 35169 start
            if (objOrig.DisMinDuration != objNew.DisMinDuration) return true;
            if (objOrig.DisOptDuration != objNew.DisOptDuration) return true;
            if (objOrig.DisMaxDuration != objNew.DisMaxDuration) return true;
            //zmohammad : MITs 35169 end
            return false;
        }
        //update the claim object with data for Event which is in SysEx node
        // Makes it an appropriate place to re-populate any additional "non-child" Xml object data.
        public override void OnUpdateObject()
        {
            //tanwar2 - mits 30910 - start
            hSetPoliciesAdded = new HashSet<int>();
            //tanwar2 - mits 30910 - end
            XmlDocument objXML = base.SysEx;
            string[] array = null;
            string strExternalPolicyID = string.Empty;      //Start : Ankit on 31_Oct_2012 : Policy Interface Changes - Setting IsInsured Value after External Policy Deletion
            //For new claim, InitCClaim custom script could set value for Comments.
            //If LegacyComments is true, we need to bring value back
            objClaim.UpdateObjectComments(objXML, objClaim.ClaimId);

            //Raman 08/28/2008 : We need to copy SysEx from Event

            //Storing Current SysEx in an xmldocument
            Event parentEvent = (objClaim.Parent as Event);
            XmlElement oCurSysExele = (XmlElement)base.SysEx.SelectSingleNode("//Parent");
            if (oCurSysExele != null)
            {
                string sCurrentSysEx = oCurSysExele.InnerXml;
                XmlDocument oCurrentSysEx = new XmlDocument();
                oCurrentSysEx.InnerXml = sCurrentSysEx;

                //Preparing SysEx from Event

                base.SysEx.SelectSingleNode("//Parent").InnerXml = parentEvent.SerializeObject();
                base.CopyNodes(ref objXML, oCurrentSysEx, "Event");
            }

            base.OnUpdateObject();

            // BSB 06.15.2006 If the claim and event are "out of sync" after the 
            // populateObject call made by default against the objClaim object with the 
            // XML from propertyStore, we may need to handle this manually.
            // This is a possibility on new claims as a result of Init Scripts loading the parent event
            // and then a UI lookup requesting a different (already existing) parent explicitly.
            // This situation is only applicable to the claim screens.  It happens when the default "screen" 
            // represented object is not the logical "root".  (Here it's logically event but implemented as claim)

            if (objClaim.EventId != parentEvent.EventId)
            {
                objClaim.Parent = null;
                parentEvent = (objClaim.Parent as Event);
            }
            parentEvent.PopulateObject(Utilities.XPointerDoc("//Parent/Instance", base.SysEx), true);
            parentEvent.EventId = objClaim.EventId;
            if (objSysSettings.MultiCovgPerClm == -1)
            {
                if (objXML.SelectSingleNode("//ClaimPolicyList/@codeid") != null)
                {
                    string sClaimPolicyList = objXML.SelectSingleNode("//ClaimPolicyList").Attributes["codeid"].Value;
                    array = sClaimPolicyList.Split(" ".ToCharArray()[0]);
                }
                if (array != null)
                {
                    for (int count = 0; count <= array.Length - 1; count++)
                    {
                        if (array[count] != "") //RMA-16974 pgupta215
                        {
                        //Start:added by Nitin goel, JIRA 7659
                        bool bSuccess = false;
                        int iPolicyId = 0;
                        iPolicyId = Conversion.CastToType<int>(array[count], out bSuccess);
                        //End:added by Nitin goel, JIRA 7659
                        bool bAdd = true;
                        foreach (ClaimXPolicy OClaimXPolicy in objClaim.ClaimPolicyList)
                        {
                            if (Conversion.ConvertStrToInteger(array[count]) == OClaimXPolicy.PolicyId)
                            {
                                bAdd = false;
                                break;
                            }
                        }
                        if (bAdd)
                        {
                            ClaimXPolicy objClaimXPolicy = objClaim.ClaimPolicyList.AddNew();
                            objClaimXPolicy.PolicyId = Conversion.ConvertStrToInteger(array[count]);
                            //tanwar2 - mits 30910 - start
                            hSetPoliciesAdded.Add(objClaimXPolicy.PolicyId);
                            //tanwar2 - mts 30910 - end
                        }
                        //Start:added by Nitin goel, JIRA 7659
                        else if (!bAdd && objClaim.ClaimId > 0)
                        {
                            if (bValidateNewCoverageDwnloadedOnClaim(objClaim.ClaimId, iPolicyId))
                            {
                                hSetPoliciesAdded.Add(iPolicyId);
                            }
                        }
                        //end:added by Nitin goel, JIRA 7659
                        }
                    }
                }
                foreach (ClaimXPolicy OClaimXPolicy in objClaim.ClaimPolicyList)
                {
                    bool bDelete = true;
                    for (int count = 0; count <= array.Length - 1; count++)
                    {
                        if (Conversion.ConvertStrToInteger(array[count]) == OClaimXPolicy.PolicyId)
                        {
                            bDelete = false;
                            break;
                        }
                    }
                    if (bDelete)
                    {
                        objClaim.ClaimPolicyList.Remove(OClaimXPolicy.RowId);
                        arrPoliciesDeleted.Add(OClaimXPolicy.PolicyId);

                        //Start : Ankit on 31_Oct_2012 : Policy Interface Changes - Setting IsInsured Value after External Policy Deletion
                        if (string.IsNullOrEmpty(strExternalPolicyID))
                            strExternalPolicyID = OClaimXPolicy.PolicyId.ToString();
                        else
                            strExternalPolicyID = strExternalPolicyID + ", " + OClaimXPolicy.PolicyId.ToString();
                        //End : Ankit 
                    }
                }

                //Start : Ankit on 31_Oct_2012 : Policy Interface Changes - Setting IsInsured Value after External Policy Deletion
                if (!string.IsNullOrEmpty(strExternalPolicyID))
                    UpdateClaimUnitsForRemovedPolicy(objClaim.ClaimId, strExternalPolicyID);
                //End : Ankit 
            }
            //update Claimnant Attorney for the Claim
            if (objClaim.PrimaryClaimant != null)
            {
                if (objXML.SelectSingleNode("//ClaimantAttorney/@codeid") != null)
                    objClaim.PrimaryClaimant.AttorneyEid = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//ClaimantAttorney").Attributes["codeid"].Value);
                else
                    objClaim.PrimaryClaimant.AttorneyEid = 0;

                //JIRA RMA-11122 ajohari2 : Start
                if (objXML.SelectSingleNode("//ClaimantStatusCode/@codeid") != null)
                    objClaim.PrimaryClaimant.ClaimantStatusCode = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//ClaimantStatusCode").Attributes["codeid"].Value);
                //JIRA RMA-11122 ajohari2 : End
            }

            //update Claim Status History Fields from SysExData
            objClaim.StatusApprovedBy = objXML.SelectSingleNode("//StatusApprovedBy").InnerText;
            objClaim.StatusDateChg = objXML.SelectSingleNode("//StatusDateChg").InnerText;
            objClaim.StatusReason = objXML.SelectSingleNode("//StatusReason").InnerText;

            // BSB 05.15.2007 The desired behavior here is a "Journaling" effect.  
            // In the Hist table we add a record only if the data from the client is different from the 
            // data in the most recently committed hist record.
            //XmlDocument objDoc = new XmlDocument();
            //MITS 32423 : Praveen ICD10 Changes start
            try
            {
                bool addICD10 = false;
                bool addICD9 = false;
                string ICDFlag = string.Empty;
                if (objXML.SelectSingleNode("//SysExData//ICDFlag") != null)
                    ICDFlag = objXML.SelectSingleNode("//SysExData//ICDFlag").InnerXml.ToString();
                else
                    ICDFlag = "0";
                PiXDiagHist objPiXDiagHist = null;
                PiXDiagHist objPiXDiagHist10 = null;
                if (iUseCaseMgmt == -1)
                {
                    XmlElement objDiagHistElem = (XmlElement)objXML.SelectSingleNode("//SysExData//PiXDiagHistList");
                    if (objDiagHistElem != null)
                    {
                        XmlDocument objDoc = new XmlDocument();
                        objDoc.LoadXml(objXML.SelectSingleNode("//SysExData//PiXDiagHistList").InnerXml);
                        objPiXDiagHist = (PiXDiagHist)objClaim.Context.Factory.GetDataModelObject("PiXDiagHist", false);
                        objPiXDiagHist.PopulateObject(objDoc);
                        //srajindersin MITS 36903 12/08/2014 - Claim Save performance
                        PiXDiagHist objMostRecent = objClaim.PrimaryPiEmployee.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHistbyPiRowId(Convert.ToString(objClaim.PrimaryPiEmployee.PiRowId));

                        //if (ICDFlag == "1" || ICDFlag == "3")
                        // {
                        //     //srajindersin MITS 36903 12/08/2014 - Claim Save performance
                        //     if(DiagHistIsModified(objMostRecent, objPiXDiagHist))
                        //     {
                        //     objPiXDiagHist.IsICD10 = 0;
                        //     objPiXDiagHist.PiXDiagrowId = -1;
                        //     objPiXDiagHist.ChgdByUser = objClaim.Context.RMUser.LoginName;
                        //   //  objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist);
                        //     addICD9 = true;
                        //     }
                        // }
                        //else if(ICDFlag == "0")
                        //{
                        if (DiagHistIsModified(objMostRecent, objPiXDiagHist))
                        {
                            //objPiXDiagHist.PiXDiagrowId = -1;
                            objPiXDiagHist.IsICD10 = 0;
                            objPiXDiagHist.ChgdByUser = objClaim.Context.RMUser.LoginName;
                            //    objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist);
                            addICD9 = true;
                        }
                        //}


                        objDoc = null;
                        //    objPiXDiagHist = null;
                        objMostRecent = null;
                    }
                    XmlElement objDiagHistICD10Elem = (XmlElement)objXML.SelectSingleNode("//SysExData//PiXDiagHistICD10List");
                    //RMA-1209 Starts - ICD 10 Code not getting deleted after adding once
                    if (objDiagHistICD10Elem != null)
                    {
                        //achouhan3  RMA 1308 Starts : copied to maintain Above added ICD-9 otherwise reference clears the previous records
                        //PiEmployee objICD9Employee = (PiEmployee)objClaim.Context.Factory.GetDataModelObject("PiEmployee", false);
                        //if (ICDFlag == "0" || ICDFlag == "3")
                        //{
                        //    foreach (PiXDiagHist objICD9 in objClaim.PrimaryPiEmployee.PiXDiagHistList)
                        //    {
                        //        objICD9Employee.PiXDiagHistList.Add(objICD9);
                        //    }
                        //}
                        //achouhan3  RMA 1308 Ends : copied to maintain Above added ICD-9 otherwise reference clears the previous records

                        XmlDocument objDoc = new XmlDocument();
                        objDoc.LoadXml(objXML.SelectSingleNode("//SysExData//PiXDiagHistICD10List").InnerXml);
                        objPiXDiagHist10 = (PiXDiagHist)objClaim.Context.Factory.GetDataModelObject("PiXDiagHist", false);
                        objPiXDiagHist10.PopulateObject(objDoc);
                        //if (ICDFlag == "2" || ICDFlag == "3")
                        //{
                        //    //srajindersin MITS 36903 12/08/2014 - Claim Save performance                               
                        //    PiXDiagHist objICD10MostRecent = objClaim.PrimaryPiEmployee.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHistICD10byPiRowId(Convert.ToString(objClaim.PrimaryPiEmployee.PiRowId));
                        //    if (DiagHistIsModified(objICD10MostRecent, objPiXDiagHist10))
                        //    {
                        //        if (ICDFlag == "3")
                        //            objPiXDiagHist10.PiXDiagrowId = -2;
                        //        else
                        //            objPiXDiagHist10.PiXDiagrowId = -1;

                        //        objPiXDiagHist10.ChgdByUser = objClaim.Context.RMUser.LoginName;
                        //        objPiXDiagHist10.IsICD10 = -1;
                        //        //   objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist10);
                        //        addICD10 = true;
                        //    }
                        //    objICD10MostRecent = null;
                        //    //objDoc = null;  //RMA-1209 commented
                        //    //objPiXDiagHist = null;  ////RMA-1209 Moved it down to work for if and else both clause
                        //}
                        //else if (ICDFlag == "0") ////RMA-1209 new if block. Added to delete code and Changed GetRecent record picking
                        //{
                        PiXDiagHist objICD10MostRecent = objClaim.PrimaryPiEmployee.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHistICD10byPiRowId(Convert.ToString(objClaim.PrimaryPiEmployee.PiRowId));
                        if (DiagHistIsModified(objICD10MostRecent, objPiXDiagHist10))
                        {
                            //objPiXDiagHist10.PiXDiagrowId = -2;
                            objPiXDiagHist10.ChgdByUser = objClaim.Context.RMUser.LoginName;
                            objPiXDiagHist10.IsICD10 = -1;
                            //    objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist10);
                            addICD10 = true;
                        }
                        objICD10MostRecent = null;
                        //}
                        ////achouhan3  RMA 1308 Starts : To merge all records and save
                        //if (ICDFlag == "0" || ICDFlag == "3")
                        //{
                        //    foreach (PiXDiagHist objICD9 in objICD9Employee.PiXDiagHistList)
                        //    {
                        //        objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objICD9);
                        //    }
                        //}
                        //achouhan3  RMA 1308 Starts : To merge all records and save
                        objDoc = null;
                        // objPiXDiagHist10 = null;
                    }

                    //if (object.ReferenceEquals(objPiXDiagHist10, null) && !string.IsNullOrEmpty(objPiXDiagHist10.DisMdaTopic))
                    //{
                    //    if (addICD10 == true)
                    //    {
                    //        objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist10);
                    //    }

                    //    if (addICD9 == true)
                    //    {
                    //        objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist);
                    //    }
                    //}
                    //else
                    //{
                    //    if (addICD9 == true)
                    //    {
                    //        objPiXDiagHist.PiXDiagrowId = -2;
                    //        objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist);
                    //    }
                    //    if (addICD10 == true)
                    //    {
                    //        objPiXDiagHist10.PiXDiagrowId = -1;
                    //        objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist10);
                    //    }
                    //}
                    //RMA-1209 Ends

                    switch (ICDFlag)
                    {
                        case "0":
                            if (objPiXDiagHist.PiXDiagrowId > objPiXDiagHist10.PiXDiagrowId && addICD9)
                            {
                                objPiXDiagHist.PiXDiagrowId = -1;
                                objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist);
                            }
                            else if (addICD10)
                            {
                                objPiXDiagHist10.PiXDiagrowId = -1;
                                objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist10);
                            }
                            break;
                        case "1":
                            if (addICD9)
                            {
                                objPiXDiagHist.PiXDiagrowId = -1;
                                objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist);
                            }
                            break;
                        case "2":
                            if (addICD10)
                            {
                                objPiXDiagHist10.PiXDiagrowId = -1;
                                objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist10);
                            }
                            break;
                        case "3":
                            if (addICD10)
                            {
                                objPiXDiagHist10.PiXDiagrowId = -1;
                                objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist10);
                            }

                            if (addICD9)
                            {
                                objPiXDiagHist.PiXDiagrowId = -1;
                                objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist);
                            }
                            break;
                        case "4":
                            if (addICD9)
                            {
                                objPiXDiagHist.PiXDiagrowId = -1;
                                objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist);
                            }

                            if (addICD10)
                            {
                                objPiXDiagHist10.PiXDiagrowId = -1;
                                objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist10);
                            }
                            break;
                    }

                    objDiagHistElem = null;
                    objDiagHistICD10Elem = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //MITS 32423 : Praveen ICD10 Changes End
            //Added By Rahul for saving MMI History.
            // BSB - This is a difficult pattern to get correct.
            // Rather, children of PersonInolved from Event should probably be serialized from the top down
            // and included in the Event Serialization. If enhancement to the DM is required for this then we 
            // should probably work on that instead of creating client code like this. 
            //Commented by Mohit for Bug No. 001264
            /*XmlElement objMMIHistElem=(XmlElement)objXML.SelectSingleNode("//SysExData//PiXMMIHistList");
            if (objMMIHistElem!=null)
            {
                objDoc=new XmlDocument();
                objDoc.LoadXml(objXML.SelectSingleNode("//SysExData/PiXMMIHistList").InnerXml);*/
            //BSB TODO This is a temporary (and incomplete) fix.
            // If the data has not been changed from the "empty" object default values then 
            //NO RECORD SHOULD BE SAVED. 
            // So, ensure that key fields (Maintained by DM) do not end up triggering "DataChanged".
            /*PiXMMIHist objPiXMMIHist = objClaim.PrimaryPiEmployee.PiXMMIHistList.AddNew();
            (objDoc.SelectSingleNode("//PiXMmirowId") as XmlElement).InnerText = objPiXMMIHist.PiXMmirowId.ToString();
            (objDoc.SelectSingleNode("//PiRowId") as XmlElement).InnerText = objPiXMMIHist.PiRowId.ToString();*/
            //Now pop in any user selections. 
            //(The matching linkage fields from above will no longer trigger DataChanged)
            /*objPiXMMIHist.PopulateObject(objDoc);
        }*/

            //Nikhil Garg		Dated: 22-Dec-2005
            //If Case Management is On the we should update the Case Management Table
            //In case of new claim we need to create a new row in Case Management Table for the new claim
            //If Case Management has bee turned on just now then also we need to create a new row in 
            //CaseManagement Table if it does not already exists

            //BOB Reserves Enhancement
            if (iUseCaseMgmt == -1)
                objClaim.UnitStat.ClaimId = objClaim.ClaimId;


            //tkr mits 8764.  auto-populate event location fields
            //MGaba2:MITS 15642:creating the node so that it doesnt come in missing refs:start
            //CreateSysExData("EventOnPremiseChecked", "false");
            //Reload the department data if this node is true
            if (base.GetSysExDataNodeText("/SysExData/EventOnPremiseChecked") == "true")
            {//MGaba2:MITS 15642:End
                // MAC : This is breaking the build, please declare 'objEvent'
                PopulateEventDetailLocation(parentEvent);
                base.ResetSysExData("EventOnPremiseChecked", "false");//MGaba2:MITS 15642
            }

            //Nikhil Garg		14-Mar-2006
            //set the claim id for aww if needed
            string sSQLString = "SELECT Claim_ID FROM CLAIM_AWW WHERE Claim_ID= " + objClaim.ClaimId.ToString();
            using (DbReader objClaimAWW = objClaim.Context.DbConn.ExecuteReader(sSQLString))
            {
                if (objClaimAWW.Read())
                    if (objClaim.ClaimAWW.ClaimId <= 0)
                        objClaim.ClaimAWW.ClaimId = objClaim.ClaimId;
            }
            //Shruti for EMI changes

            //MITS 30386 Start
            if (!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_WC_EMPLOYEE, FormBase.RMO_VIEW_SSN))
            {
                if (!objClaim.PrimaryPiEmployee.PiEntity.IsNew)
                {
                    string sTAXSQLString = "SELECT TAX_ID FROM ENTITY WHERE ENTITY_ID= " + objClaim.PrimaryPiEmployee.PiEntity.EntityId.ToString();
                    using (DbReader objClaimAWW = objClaim.Context.DbConn.ExecuteReader(sTAXSQLString))
                    {
                        if (objClaimAWW.Read())
                        {
                            string oldTaxid = Convert.ToString(objClaimAWW[0]);
                            objClaim.PrimaryPiEmployee.PiEntity.TaxId = oldTaxid;
                        }
                    }
                }
            }
            //MITS 30386 ends

            double dMaxRate = 0;
            if (parentEvent.DateOfEvent != string.Empty)
            {
                sSQLString = "SELECT MAX_RATE_AMT FROM WC_MAX_RATE_CALC WHERE MAX_YEAR_TEXT = " + parentEvent.DateOfEvent.Substring(0, 4);
                using (DbReader objMaxRate = objClaim.Context.DbConn.ExecuteReader(sSQLString))
                {
                    if (objMaxRate.Read())
                    {
                        dMaxRate = objMaxRate.GetDouble("MAX_RATE_AMT");
                    }
                    //objClaim.CompRate = objClaim.PrimaryPiEmployee.WeeklyRate * 0.6667;
                    //objClaim.CompRate = Math.Round(objClaim.CompRate, 2, MidpointRounding.AwayFromZero);
                    //Ankit Start : MITS - 32385
                    objClaim.CompRate = Math.Round((objClaim.ClaimAWW.Aww * 0.6667), 2, MidpointRounding.AwayFromZero);
                    //Ankit End

                    if (objClaim.CompRate > dMaxRate)
                    {
                        objClaim.CompRate = dMaxRate;
                    }
                }
            }
            //Added Rakhi for R7:Add Emp Data Elements
            if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
            {

                objClaim.PrimaryPiEmployee.PiEntity.FormName = "ClaimWCForm"; //Mits 22497

                #region "Updating Address Info Object"

                string sAddr1 = objClaim.PrimaryPiEmployee.PiEntity.Addr1;
                string sAddr2 = objClaim.PrimaryPiEmployee.PiEntity.Addr2;
                string sAddr3 = objClaim.PrimaryPiEmployee.PiEntity.Addr3;// JIRA 6420 pkandhari
                string sAddr4 = objClaim.PrimaryPiEmployee.PiEntity.Addr4;// JIRA 6420 pkandhari
                string sCity = objClaim.PrimaryPiEmployee.PiEntity.City;
                int iCountryCode = objClaim.PrimaryPiEmployee.PiEntity.CountryCode;
                int iStateId = objClaim.PrimaryPiEmployee.PiEntity.StateId;
                string sEmailAddress = objClaim.PrimaryPiEmployee.PiEntity.EmailAddress;
                string sFaxNumber = objClaim.PrimaryPiEmployee.PiEntity.FaxNumber;
                string sCounty = objClaim.PrimaryPiEmployee.PiEntity.County;
                string sZipCode = objClaim.PrimaryPiEmployee.PiEntity.ZipCode;
                //Ashish Ahuja - Address Master
                string sStateDesc = string.Empty;
                string sStateCode = string.Empty;
                string sCountryDesc = string.Empty;
                string sCountryCode = string.Empty;
                string sSearchString = string.Empty;

                if (
                        sAddr1 != string.Empty || sAddr2 != string.Empty || sAddr3 != string.Empty || sAddr4 != string.Empty ||
                        sCity != string.Empty || iCountryCode != 0 || iStateId != 0 || sEmailAddress != string.Empty ||
                        sFaxNumber != string.Empty || sCounty != string.Empty || sZipCode != string.Empty
                    )
                {

                    if (objClaim.PrimaryPiEmployee.PiEntity.EntityXAddressesList.Count == 0)
                    {


                        EntityXAddresses objEntityXAddressesInfo = objClaim.PrimaryPiEmployee.PiEntity.EntityXAddressesList.AddNew();
                        //RMA-8753 nshah28(Added by ashish) START
                        objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                        objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                        objEntityXAddressesInfo.Address.Addr3 = sAddr3;// JIRA 6420 pkandhari
                        objEntityXAddressesInfo.Address.Addr4 = sAddr4;// JIRA 6420 pkandhari
                        objEntityXAddressesInfo.Address.City = sCity;
                        objEntityXAddressesInfo.Address.Country = iCountryCode;
                        objEntityXAddressesInfo.Address.State = iStateId;
                        objEntityXAddressesInfo.Email = sEmailAddress;
                        objEntityXAddressesInfo.Fax = sFaxNumber;
                        objEntityXAddressesInfo.Address.County = sCounty;
                        objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                        //RMA-8753 nshah28(Added by ashish) END
                        objEntityXAddressesInfo.EntityId = objClaim.PrimaryPiEmployee.PiEntity.EntityId;
                        objEntityXAddressesInfo.PrimaryAddress = -1;
                        objEntityXAddressesInfo.AddressId = 0;
                        objEntityXAddressesInfo.Address.AddressId = 0;
                        objCache.GetStateInfo(iStateId, ref sStateCode, ref sStateDesc);
                        objCache.GetCodeInfo(iCountryCode, ref sCountryCode, ref sCountryDesc);
                        sSearchString = (sAddr1 + sAddr2 + sAddr3 + sAddr4 + sCity + sStateDesc + sCountryDesc + sZipCode + "0").ToLower().Replace(" ", ""); //Remove "County"
                        objEntityXAddressesInfo.Address.SearchString = sSearchString;

                    }
                    else
                    {
                        foreach (EntityXAddresses objEntityXAddressesInfo in objClaim.PrimaryPiEmployee.PiEntity.EntityXAddressesList)
                        {
                            if (objEntityXAddressesInfo.PrimaryAddress == -1)
                            {
                                //RMA-8753 nshah28(Added by ashish) START
                                objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                                objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                                objEntityXAddressesInfo.Address.Addr3 = sAddr3;// JIRA 6420 pkandhari
                                objEntityXAddressesInfo.Address.Addr4 = sAddr4;// JIRA 6420 pkandhari
                                objEntityXAddressesInfo.Address.City = sCity;
                                objEntityXAddressesInfo.Address.Country = iCountryCode;
                                objEntityXAddressesInfo.Address.State = iStateId;
                                objEntityXAddressesInfo.Email = sEmailAddress;
                                objEntityXAddressesInfo.Fax = sFaxNumber;
                                objEntityXAddressesInfo.Address.County = sCounty;
                                objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                                objEntityXAddressesInfo.AddressId = 0;
                                objEntityXAddressesInfo.Address.AddressId = 0;
                                objCache.GetStateInfo(iStateId, ref sStateCode, ref sStateDesc);
                                objCache.GetCodeInfo(iCountryCode, ref sCountryCode, ref sCountryDesc);
                                sSearchString = (sAddr1 + sAddr2 + sAddr3 + sAddr4 + sCity + sStateDesc + sCountryDesc + sZipCode + "0").ToLower().Replace(" ", ""); //Remove "County"
                                objEntityXAddressesInfo.Address.SearchString = sSearchString;
                                //RMA-8753 nshah28(Added by ashish) END
                                break;
                            }
                        }
                    }
                }
                else
                {
                    if (objClaim.PrimaryPiEmployee.PiEntity.EntityXAddressesList.Count > 0)
                    {
                        foreach (EntityXAddresses objEntityXAddressesInfo in objClaim.PrimaryPiEmployee.PiEntity.EntityXAddressesList)
                        {
                            if (objEntityXAddressesInfo.PrimaryAddress == -1)
                            {
                                objClaim.PrimaryPiEmployee.PiEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                                break;
                            }

                        }
                    }

                }

                #endregion

                #region Added for updating Phone Numbers

                string sOfficePhone = objClaim.PrimaryPiEmployee.PiEntity.Phone1.Trim();
                string sHomePhone = objClaim.PrimaryPiEmployee.PiEntity.Phone2.Trim();
                int iHomeCode = objCache.GetCodeId("H", "PHONES_CODES");
                int iOfficeCode = objCache.GetCodeId("O", "PHONES_CODES");
                bool bOfficePhoneEntered = false;
                AddressXPhoneInfo objAddressesInfo = null;
                bool bOfficeCodeExists = false;
                bool bHomePhoneEntered = false;
                bool bHomeCodeExists = false;

                if (sOfficePhone != string.Empty)
                    bOfficePhoneEntered = true;
                if (sHomePhone != string.Empty)
                    bHomePhoneEntered = true;

                if (objClaim.PrimaryPiEmployee.PiEntity.AddressXPhoneInfoList.Count == 0)
                {
                    if (bOfficePhoneEntered)
                    {
                        objAddressesInfo = objClaim.PrimaryPiEmployee.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iOfficeCode;
                        objAddressesInfo.PhoneNo = sOfficePhone;
                    }
                    if (bHomePhoneEntered)
                    {
                        objAddressesInfo = objClaim.PrimaryPiEmployee.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iHomeCode;
                        objAddressesInfo.PhoneNo = sHomePhone;
                    }
                }
                else
                {
                    foreach (AddressXPhoneInfo objXAddressesInfo in objClaim.PrimaryPiEmployee.PiEntity.AddressXPhoneInfoList)
                    {
                        if (objXAddressesInfo.PhoneCode == iOfficeCode)
                        {
                            if (bOfficePhoneEntered)
                            {
                                objXAddressesInfo.PhoneNo = sOfficePhone;
                                bOfficeCodeExists = true;
                            }
                            else
                            {
                                objClaim.PrimaryPiEmployee.PiEntity.AddressXPhoneInfoList.Remove(objXAddressesInfo.PhoneId);
                            }
                            continue;
                        }
                        else if (objXAddressesInfo.PhoneCode == iHomeCode)
                        {
                            if (bHomePhoneEntered)
                            {
                                objXAddressesInfo.PhoneNo = sHomePhone;
                                bHomeCodeExists = true;
                            }
                            else
                            {
                                objClaim.PrimaryPiEmployee.PiEntity.AddressXPhoneInfoList.Remove(objXAddressesInfo.PhoneId);
                            }
                        }
                    }
                    if (bOfficePhoneEntered && !bOfficeCodeExists)
                    {
                        objAddressesInfo = objClaim.PrimaryPiEmployee.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iOfficeCode;
                        objAddressesInfo.PhoneNo = sOfficePhone;
                    }
                    if (bHomePhoneEntered && !bHomeCodeExists)
                    {
                        objAddressesInfo = objClaim.PrimaryPiEmployee.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iHomeCode;
                        objAddressesInfo.PhoneNo = sHomePhone;
                    }
                }

                #endregion
            }
            //Added Rakhi for R7:Add Emp Data Elements
            //MGaba2:12/08/2010:MITS 20184:All Employee information was not getting saved in powerview in case field doesnt exist in powerview of claim WC
            string sEmpSelected = base.GetSysExDataNodeText("/SysExData/hdnEmpSelected");
            if (sEmpSelected == "True")
            {
                int iPiEid = objClaim.PrimaryPiEmployee.PiEid;
                if (iPiEid > 0)
                {
                    Employee objEmployee = (Employee)objClaim.Context.Factory.GetDataModelObject("Employee", false);

                    objEmployee.MoveTo(iPiEid);
                    if (objEmployee.EmployeeNumber != objClaim.PrimaryPiEmployee.EmployeeNumber)
                        objEmployee.EmployeeNumber = objClaim.PrimaryPiEmployee.EmployeeNumber;//skhare7 27895
                    objEmployee.CopyToPiEmployee(objClaim.PrimaryPiEmployee);
                    objEmployee = null;
                }
                base.ResetSysExData("hdnEmpSelected", "False");
            }
            //tanwar2 - ImageRight - start
            //Generate FUP if text of this node is -1
            if (string.Compare(base.GetSysExDataNodeText("/SysExData/generateFUPFile"), "-1") == 0)
            {
                FUPGenerator oFUPGenerator = new FUPGenerator(base.Adaptor.userLogin, base.ClientId);
                oFUPGenerator.GenerateFUP(objClaim.ClaimId, objClaim);
            }
            //tanwar2 - ImageRight - end
        }
        //tanwar2 - mits 30910 - start
        private void UpdateClaimDeductiblesForAddedPolicy(int p_ClaimId)
        {
            ClaimXPolDed objClmXPolDed = null;
            PolicyXInslineGroup oPolicyXInsLineGroup = null;
            Policy objPolicy = null;
            int iClaimantRowId = Int32.MaxValue;
            int iClaimantEid = 0;
            string sCustomerNumber = string.Empty;
            string sInsLine = string.Empty;
            string sProdLine = string.Empty;
            string sAslLine = string.Empty;
            int iCvgCode = 0;
            int iPolicyXInsLineGroupId = 0;
            //start: added by Nitin goel , MITs 34153 added a case so duplicate entry should not go into deductible table.
            int iDedRecExist = 0;
            int iUnitRowId = 0;
            int iPolCovRowId = 0;
            int iUnitId = 0;
            string sUnitType = string.Empty;
            int iUnitStateRowId = 0;
            string sCovClassCode = string.Empty;
            string sCovSubLineCode = string.Empty;
            string sStatUnitNumber = string.Empty;
            int iDeductiblePerEventEnabled = 0;
            //END:added by nitin goel
            int sPolicySystemCode = 0;
            string sPolicySystemType = string.Empty;
            bool bSuccess;

            if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1
                    && objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ApplyDedToPaymentsFlag == -1)
            {
                foreach (Claimant objClaimant in objClaim.ClaimantList)
                {
                    if (iClaimantRowId > objClaimant.ClaimantRowId)
                    {
                        iClaimantRowId = objClaimant.ClaimantRowId;
                        iClaimantEid = objClaimant.ClaimantEid;
                    }

                    objClaimant.Dispose();
                }

                objPolicy = objClaim.Context.Factory.GetDataModelObject("Policy", false) as Policy;

                foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
                {
                    if (hSetPoliciesAdded.Contains(objClaimXPolicy.PolicyId))
                    {
                        objPolicy.MoveTo(objClaimXPolicy.PolicyId);

                        // Start RMA-15937
                        if (objPolicy.PolicySystemId > 0)
                        {
                            PolicySystemInterface objpolicyInterface = new PolicySystemInterface(objClaim.Context.DbConnLookup.ConnectionString, base.ClientId);
                            sCustomerNumber = objpolicyInterface.GetDownLoadXMLNodeSpecificData("POLICY", objPolicy.PolicyId, objPolicy.PolicyId, "CustPermId", false, string.Empty);
                            sPolicySystemCode = Conversion.CastToType<Int32>(objpolicyInterface.GetPolicySystemParamteres("POLICY_SYSTEM_CODE", objPolicy.PolicySystemId), out bSuccess);
                            sPolicySystemType = objClaim.Context.LocalCache.GetShortCode(sPolicySystemCode);
                        }
                        if (objPolicy.PolicySystemId > 0)
                        {
                            switch (CommonFunctions.GetPolicySystemTypeIndicator(sPolicySystemType))
                            {
                                case Constants.POLICY_SYSTEM_TYPE.STAGING:
                                    using (DbReader oDbReader = DbFactory.GetDbReader(objClaim.Context.DbConnLookup.ConnectionString,
                                        string.Format("SELECT CVG.DED_TYPE_CODE, CVG.SELF_INSURE_DEDUCT, CVG.POLCVG_ROW_ID, PXU.POLICY_UNIT_ROW_ID FROM POLICY_X_CVG_TYPE CVG INNER JOIN POLICY_X_UNIT PXU ON CVG.POLICY_UNIT_ROW_ID=PXU.POLICY_UNIT_ROW_ID WHERE PXU.POLICY_ID={0} ", objClaimXPolicy.PolicyId)))
                                    {
                                        while (oDbReader.Read())
                                        {
                                            objClmXPolDed = objClaim.Context.Factory.GetDataModelObject("ClaimXPolDed", false) as ClaimXPolDed;
                                            objClmXPolDed.PolicyId = objPolicy.PolicyId;
                                            objClmXPolDed.ClaimId = p_ClaimId;
                                            objClmXPolDed.ClaimantEid = iClaimantEid;
                                            objClmXPolDed.AddedByUser = "SYSTEM";
                                            objClmXPolDed.UpdatedByUser = "SYSTEM";
                                            objClmXPolDed.PolicyLobCode = objPolicy.PolicyLOB;
                                            objClmXPolDed.DedTypeCode = oDbReader.GetInt("DED_TYPE_CODE"); // 'None' code if fixed for staging
                                            objClmXPolDed.UnitRowId = oDbReader.GetInt("POLICY_UNIT_ROW_ID");
                                            objClmXPolDed.PolcvgRowId = oDbReader.GetInt("POLCVG_ROW_ID");
                                            objClmXPolDed.SirDedAmt = oDbReader.GetDouble("SELF_INSURE_DEDUCT");
                                            //objClmXPolDed.CustomerNum = sCustomerNumber;
                                            //objClmXPolDed.PolicyXInslineGroupId = iPolicyXInsLineGroupId;
                                            objClmXPolDed.EventId = (objClaim.Parent as Event).EventId;
                                            objClmXPolDed.UnitStateRowId = iUnitStateRowId;
                                            objClmXPolDed.Save();
                                        }
                                    }
                                    break;
                                case Constants.POLICY_SYSTEM_TYPE.POINT:

                                    using (DbReader oDbReader = DbFactory.GetDbReader(objClaim.Context.DbConnLookup.ConnectionString,
                            string.Format("SELECT PUD.STAT_UNIT_NUMBER,CVG.COVERAGE_CLASS_CODE, CVG.SUB_LINE,CVG.COVERAGE_TYPE_CODE, CVG.DED_TYPE_CODE, CVG.SELF_INSURE_DEDUCT, CVG.POLCVG_ROW_ID, CVG.PRODLINE, CVG.ASLINE, PXU.UNIT_ID, PXU.UNIT_TYPE, PXU.POLICY_UNIT_ROW_ID, PUD.INS_LINE FROM POLICY_X_CVG_TYPE CVG INNER JOIN POLICY_X_UNIT PXU ON CVG.POLICY_UNIT_ROW_ID=PXU.POLICY_UNIT_ROW_ID INNER JOIN POINT_UNIT_DATA PUD ON PXU.UNIT_ID=PUD.UNIT_ID AND PXU.UNIT_TYPE=PUD.UNIT_TYPE WHERE PXU.POLICY_ID={0} ", objClaimXPolicy.PolicyId)))
                                    {
                                        while (oDbReader.Read())
                                        {
                                            iPolicyXInsLineGroupId = 0;
                                            sInsLine = oDbReader.GetString("INS_LINE");
                                            sProdLine = oDbReader.GetString("PRODLINE");
                                            sAslLine = oDbReader.GetString("ASLINE");
                                            iCvgCode = oDbReader.GetInt("COVERAGE_TYPE_CODE");
                                            //start: added by Nitin goel , MITs 34153 added a case so duplicate entry should not go into deductible table.
                                            iUnitRowId = oDbReader.GetInt("POLICY_UNIT_ROW_ID");
                                            iPolCovRowId = oDbReader.GetInt("POLCVG_ROW_ID");
                                            iUnitId = oDbReader.GetInt("UNIT_ID");
                                            sUnitType = oDbReader.GetString("UNIT_TYPE");
                                            iUnitStateRowId = GetUnitStateRowId(iUnitId, sUnitType);
                                            sCovClassCode = oDbReader.GetString("COVERAGE_CLASS_CODE");
                                            sCovSubLineCode = oDbReader.GetString("SUB_LINE");
                                            sStatUnitNumber = oDbReader.GetString("STAT_UNIT_NUMBER");
                                            iDedRecExist = DbFactory.ExecuteAsType<int>(objClaim.Context.DbConnLookup.ConnectionString, string.Format("SELECT COUNT(*) FROM CLAIM_X_POL_DED WHERE CLAIM_ID={0} AND POLICY_ID={1} AND POLICY_UNIT_ROW_ID={2} AND POLCVG_ROW_ID={3}", new object[] { p_ClaimId, objPolicy.PolicyId, iUnitRowId, iPolCovRowId }));
                                            if (iDedRecExist == 0)
                                            {
                                                //end:added by nitin goel
                                                oPolicyXInsLineGroup = objClaim.Context.Factory.GetDataModelObject("PolicyXInslineGroup", false) as PolicyXInslineGroup;
                                                //iPolicyXInsLineGroupId = DbFactory.ExecuteAsType<int>(objPolicy.Context.DbConnLookup.ConnectionString, string.Format("SELECT POLICY_X_INSLINE_GROUP_ID FROM POLICY_X_INSLINE_GROUP WHERE POLICY_SYSTEM_ID={0} AND POLICY_NUMBER='{1}' AND POLICY_SYMBOL='{2}' AND MODULE='{3}' AND MASTER_COMPANY='{4}' AND LOCATION_COMPANY='{5}' AND (INS_LINE='{6}' OR INS_LINE IS NULL) AND (PRODLINE='{7}' OR PRODLINE IS NULL) AND (ASLINE='{8}' OR ASLINE IS NULL) AND COVERAGE_CODE={9}", new object[] { objPolicy.PolicySystemId, objPolicy.PolicyNumber, objPolicy.PolicySymbol, objPolicy.Module, objPolicy.MasterCompany, objPolicy.LocationCompany, sInsLine, sProdLine, sAslLine, iCvgCode }));
                                                iPolicyXInsLineGroupId = DbFactory.ExecuteAsType<int>(objPolicy.Context.DbConnLookup.ConnectionString, string.Format("SELECT POLICY_X_INSLINE_GROUP_ID FROM POLICY_X_INSLINE_GROUP WHERE POLICY_SYSTEM_ID={0} AND POLICY_NUMBER='{1}' AND POLICY_SYMBOL='{2}' AND MODULE='{3}' AND MASTER_COMPANY='{4}' AND LOCATION_COMPANY='{5}' AND (INS_LINE='{6}' OR INS_LINE IS NULL) AND (PRODLINE='{7}' OR PRODLINE IS NULL) AND (ASLINE='{8}' OR ASLINE IS NULL) AND COVERAGE_CODE={9} AND STAT_UNIT_NUMBER ='{10}' AND (COVERAGE_CLASS_CODE='{11}' OR COVERAGE_CLASS_CODE IS NULL) AND (COVERAGE_SUBLINE_CODE='{12}' OR COVERAGE_SUBLINE_CODE IS NULL) AND UNIT_STATE_ROW_ID={13}  ", new object[] { objPolicy.PolicySystemId, objPolicy.PolicyNumber, objPolicy.PolicySymbol, objPolicy.Module, objPolicy.MasterCompany, objPolicy.LocationCompany, sInsLine, sProdLine, sAslLine, iCvgCode, sStatUnitNumber, sCovClassCode, sCovSubLineCode, iUnitStateRowId }));
                                                if (iPolicyXInsLineGroupId == 0)
                                                {

                                                    oPolicyXInsLineGroup.PolicySystemId = objPolicy.PolicySystemId;
                                                    oPolicyXInsLineGroup.PolicyNumber = objPolicy.PolicyNumber;
                                                    oPolicyXInsLineGroup.PolicySymbol = objPolicy.PolicySymbol;
                                                    oPolicyXInsLineGroup.Module = objPolicy.Module;
                                                    oPolicyXInsLineGroup.MasterCompany = objPolicy.MasterCompany;
                                                    oPolicyXInsLineGroup.LocationCompany = objPolicy.LocationCompany;
                                                    oPolicyXInsLineGroup.InsLine = sInsLine;
                                                    oPolicyXInsLineGroup.ProdLine = sProdLine;
                                                    oPolicyXInsLineGroup.AslLine = sAslLine;
                                                    oPolicyXInsLineGroup.Coverage = iCvgCode;
                                                    oPolicyXInsLineGroup.StatUnitNumber = sStatUnitNumber;
                                                    oPolicyXInsLineGroup.UnitStateRowId = iUnitStateRowId;
                                                    oPolicyXInsLineGroup.CoverageClassCode = sCovClassCode;
                                                    oPolicyXInsLineGroup.CoverageSubLineCode = sCovSubLineCode;
                                                    oPolicyXInsLineGroup.ClaimLineOfBusiness = objClaim.LineOfBusCode;
                                                    oPolicyXInsLineGroup.Save();
                                                    iPolicyXInsLineGroupId = oPolicyXInsLineGroup.PolicyXInslineGroupId;

                                                }

                                                oPolicyXInsLineGroup.MoveTo(iPolicyXInsLineGroupId);

                                                objClmXPolDed = objClaim.Context.Factory.GetDataModelObject("ClaimXPolDed", false) as ClaimXPolDed;
                                                objClmXPolDed.PolicyId = objPolicy.PolicyId;
                                                objClmXPolDed.ClaimId = p_ClaimId;
                                                objClmXPolDed.ClaimantEid = iClaimantEid;
                                                objClmXPolDed.AddedByUser = "SYSTEM";
                                                objClmXPolDed.UpdatedByUser = "SYSTEM";
                                                objClmXPolDed.PolicyLobCode = objPolicy.PolicyLOB;
                                                objClmXPolDed.DedTypeCode = oDbReader.GetInt("DED_TYPE_CODE");
                                                objClmXPolDed.UnitRowId = oDbReader.GetInt("POLICY_UNIT_ROW_ID");
                                                objClmXPolDed.PolcvgRowId = oDbReader.GetInt("POLCVG_ROW_ID");
                                                objClmXPolDed.SirDedAmt = oDbReader.GetDouble("SELF_INSURE_DEDUCT");
                                                objClmXPolDed.CustomerNum = sCustomerNumber;
                                                //objClmXPolDed.InsLine = sInsLine;
                                                objClmXPolDed.PolicyXInslineGroupId = iPolicyXInsLineGroupId;
                                                objClmXPolDed.EventId = (objClaim.Parent as Event).EventId;
                                                objClmXPolDed.UnitStateRowId = iUnitStateRowId;
                                                objClmXPolDed.Save();

                                                if (oPolicyXInsLineGroup.CovGroupId > 0 && objClmXPolDed.DedTypeCode != objCache.GetCodeId("FP", "DEDUCTIBLE_TYPE"))
                                                {
                                                    //Start: added by nitin goel, for PCR Changes, deductible amount should only be overridden when deductible per event is enabled for the coverage group.
                                                    // iDeductiblePerEventEnabled = DbFactory.ExecuteAsType<int>(objPolicy.Context.DbConnLookup.ConnectionString, "SELECT USE_SHARED_DED_FLAG FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID= " + oPolicyXInsLineGroup.CovGroupId);
                                                    iDeductiblePerEventEnabled = DbFactory.ExecuteAsType<int>(objPolicy.Context.DbConnLookup.ConnectionString, string.Concat("SELECT USE_SHARED_DED_FLAG FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID= ", oPolicyXInsLineGroup.CovGroupId));
                                                    if (iDeductiblePerEventEnabled == -1)
                                                    {
                                                        //end:added by nitin goel.
                                                        objClmXPolDed.MoveTo(objClmXPolDed.ClmXPolDedId);
                                                        //objClmXPolDed.SirDedAmt = DbFactory.ExecuteAsType<double>(objPolicy.Context.DbConnLookup.ConnectionString, "SELECT SIR_DED_PEREVENT_AMT FROM CLAIM_X_POL_DED_AGG_LIMIT WHERE COV_GROUP_CODE=" + oPolicyXInsLineGroup.CovGroupId);
                                                        objClmXPolDed.SirDedAmt = DbFactory.ExecuteAsType<double>(objPolicy.Context.DbConnLookup.ConnectionString, string.Concat("SELECT SIR_DED_PEREVENT_AMT FROM CLAIM_X_POL_DED_AGG_LIMIT WHERE COV_GROUP_CODE=", oPolicyXInsLineGroup.CovGroupId));
                                                        objClmXPolDed.UpdatedByUser = "SYSTEM";
                                                        objClmXPolDed.Save();
                                                        //Start: added by nitin goel, for PCR Changes, 
                                                    }
                                                    //end:added by nitin goel.
                                                }
                                                oPolicyXInsLineGroup.Dispose();
                                                objClmXPolDed.Dispose();
                                                //Start:added by Nitin goel, MITS 34153 ,10/23/2013
                                            }
                                            //End:added by Nitin goel, MITS 34153 ,10/23/2013
                                        }
                                    }
                                    break;
                            }
                        }// End RMA-15937
                    }
                    objClaimXPolicy.Dispose();
                }
                objPolicy.Dispose();
            }
        }
        //start:Added By Nitin Goel, For Fetching State of the unit.
        private int GetUnitStateRowId(int iUnitId, string sUnitType)
        {
            int iStateRowId = 0;
            string sSql = string.Empty;
            try
            {
                if (String.Compare(sUnitType, "V", true) == 0)
                {
                    // sSql = "SELECT STATE_ROW_ID FROM VEHICLE WHERE UNIT_ID= " + iUnitId;
                    sSql = string.Concat("SELECT STATE_ROW_ID FROM VEHICLE WHERE UNIT_ID= ", iUnitId);
                }
                else if (String.Compare(sUnitType, "SU", true) == 0)
                {
                    //sSql = "SELECT E.STATE_ID FROM ENTITY E INNER JOIN OTHER_UNIT O ON E.ENTITY_ID = O.ENTITY_ID WHERE O.OTHER_UNIT_ID= " + iUnitId;
                    sSql = string.Concat("SELECT E.STATE_ID FROM ENTITY E INNER JOIN OTHER_UNIT O ON E.ENTITY_ID = O.ENTITY_ID WHERE O.OTHER_UNIT_ID= ", iUnitId);
                }
                else if (String.Compare(sUnitType, "P", true) == 0)
                {
                    //sSql = " SELECT STATE_ID FROM PROPERTY_UNIT WHERE PROPERTY_ID= " + iUnitId;
                    sSql = string.Concat(" SELECT STATE_ID FROM PROPERTY_UNIT WHERE PROPERTY_ID= ", iUnitId);
                }
                else if (String.Compare(sUnitType, "S", true) == 0)
                {
                    //   sSql = "SELECT STATE_ID FROM SITE_UNIT WHERE SITE_ID= " + iUnitId;
                    sSql = string.Concat("SELECT STATE_ID FROM SITE_UNIT WHERE SITE_ID= ", iUnitId);
                }

                if (sSql.Length > 0)
                {
                    iStateRowId = DbFactory.ExecuteAsType<int>(objClaim.Context.DbConnLookup.ConnectionString, sSql);
                }
            }
            catch
            {
                throw;
            }
            return iStateRowId;
        }
        private void UpdateClaimDeductiblesForRemovedPolicy(int ClaimId, string PolicyIDs)
        {
            //To Delete the history of deductibles
            DbFactory.ExecuteNonQuery(objClaim.Context.DbConnLookup.ConnectionString,
                string.Format("DELETE FROM CLAIM_X_POL_DED_HIST WHERE CLM_X_POL_DED_ID IN (SELECT CLM_X_POL_DED_ID FROM CLAIM_X_POL_DED WHERE CLAIM_ID={0} AND POLICY_ID IN({1}))", ClaimId, PolicyIDs));
            DbFactory.ExecuteNonQuery(objClaim.Context.DbConnLookup.ConnectionString,
                string.Format("DELETE FROM CLAIM_X_POL_DED WHERE CLAIM_ID={0} AND POLICY_ID IN({1})", ClaimId, PolicyIDs));
        }
        //tanwar2 - mits 30910 - end
        //Start : Ankit on 31_Oct_2012 : Policy Interface Changes - Setting IsInsured Value after External Policy Deletion
        private void UpdateClaimUnitsForRemovedPolicy(int ClaimID, string PolicyIDs)
        {
            string strSUnitID = string.Empty;
            string strSUUnitID = string.Empty;
            try
            {
                using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, "SELECT UNIT_ID, UNIT_TYPE FROM POLICY_X_UNIT WHERE POLICY_ID IN (" + PolicyIDs + ")"))
                {
                    while (objReader.Read())
                    {
                        switch (Conversion.ConvertObjToStr(objReader.GetValue("UNIT_TYPE")).ToUpper())
                        {
                            case "S":
                                if (string.IsNullOrEmpty(strSUnitID))
                                    strSUnitID = Conversion.ConvertObjToStr(objReader.GetValue("UNIT_ID"));
                                else
                                    strSUnitID = strSUnitID + ", " + Conversion.ConvertObjToStr(objReader.GetValue("UNIT_ID"));
                                break;
                            case "SU":
                                if (string.IsNullOrEmpty(strSUUnitID))
                                    strSUUnitID = Conversion.ConvertObjToStr(objReader.GetValue("UNIT_ID"));
                                else
                                    strSUUnitID = strSUUnitID + ", " + Conversion.ConvertObjToStr(objReader.GetValue("UNIT_ID"));
                                break;
                        }
                    }

                    if (!string.IsNullOrEmpty(strSUnitID))
                        UpdateInsuredFlag(ClaimID, strSUnitID, "CLAIM_X_SITELOSS", "ISINSURED", "SITE_ID");
                    if (!string.IsNullOrEmpty(strSUUnitID))
                        UpdateInsuredFlag(ClaimID, strSUUnitID, "CLAIM_X_OTHERUNIT", "ISINSURED", "OTHER_UNIT_ID");

                }
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
        }
        private void UpdateInsuredFlag(int ClaimID, string UnitID, string TableName, string UpdatedColumnName, string WhereColumnName)
        {
            DbConnection objConn = null;
            StringBuilder sbSQL = null;

            sbSQL = new StringBuilder();
            objConn = DbFactory.GetDbConnection(objClaim.Context.DbConn.ConnectionString);
            objConn.Open();

            sbSQL.Append(" UPDATE " + TableName + " ");
            sbSQL.Append(" SET " + UpdatedColumnName + " = -2");
            sbSQL.Append(" WHERE CLAIM_ID = " + ClaimID);
            sbSQL.Append(" AND " + WhereColumnName + " IN (" + UnitID + ")");


            objConn.ExecuteNonQuery(sbSQL.ToString());

            if (objConn != null)
            {
                objConn.Dispose();
                objConn = null;
            }
        }
        //End : Ankit

        public override void BeforeSave(ref bool Cancel)
        {
            base.BeforeSave(ref Cancel);
            Event objEvent = objClaim.Parent as Event;
            // JIRA 17911 start
            if (objClaim.PrimaryPiEmployee.PiEid > 0 && (objClaim.PrimaryPiEmployee as PiEmployee).IsNew)
            {
                string sSQLQuery = string.Empty;
                string sParentTableName = "EVENT";
                int iPiRowId = 0;
                int iRoleTableId = this.objClaim.Context.LocalCache.GetTableId(Globalization.IndividualGlossaryTableNames.EMPLOYEES.ToString());
                if (objClaim.Context.InternalSettings.SysSettings.UseEntityRole)
                    sSQLQuery = string.Format("SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PARENT_ROW_ID= {0} AND PI_EID= {1} AND ROLE_TABLE_ID= {2} AND  PARENT_TABLE_NAME = '{3}' ", objClaim.EventId, objClaim.PrimaryPiEmployee.PiEid, iRoleTableId, sParentTableName);
                else
                    sSQLQuery = string.Format("SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PARENT_ROW_ID= {0} AND PI_EID= {1} AND  PARENT_TABLE_NAME = '{2}' ", objClaim.EventId, objClaim.PrimaryPiEmployee.PiEid, sParentTableName);
             
                iPiRowId = this.objClaim.Context.DbConnLookup.ExecuteInt(sSQLQuery);
                if (iPiRowId > 0)
                {
                    // PI Record already exists	
                    Cancel = true;
                    // pgupta215: RMA-18997
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), string.Format(Globalization.GetString("Validation.EmployeeAlreadyExistsAsPI", base.ClientId, sLangCode), sParentTableName.ToLower()), BusinessAdaptorErrorType.Error);

                    return;
                }
                // JIRA 17911 End
            }
                //Gagan Safeway Retrofit Policy Jurisidiction : START / added by rkaur7 on 06/04/2009-  MITS 16668
                int iNumPolicy = 0;
                string sShowPolPopUp = string.Empty;
                //Gagan Safeway Retrofit Policy Jurisidiction : END / 

                //MITS 34260 : Duplicate Claim Check : start
                bool bFlag = false;  //bFlag set to true in case any new policy added that was not already existing on the claim.
                int prevClaimPolicyCount;

                //This loop checks if there is any new policy attached other than previous ones.
                foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
                {
                    prevClaimPolicyCount = objClaim.Context.DbConnLookup.ExecuteInt("SELECT COUNT(CLAIM_X_POLICY.ROW_ID) FROM CLAIM , CLAIM_X_POLICY WHERE CLAIM_X_POLICY.CLAIM_ID = CLAIM.CLAIM_ID AND CLAIM.CLAIM_ID=" + objClaim.ClaimId + " AND CLAIM_X_POLICY.POLICY_ID = " + objClaimXPolicy.PolicyId);
                    if (prevClaimPolicyCount == 0)
                    {
                        bFlag = true;
                        break;
                    }
                }
                //MITS 34260 : Duplicate Claim Check: end

                //Check UserLimit Security.
                if (objLobSettings.ClmAccLmtFlag)//If UserLimits Enabled...
                {
                    if (objLimits != null)
                        //asharma326 MITS 30874 Add Parameter objLobSettings.ClmAccLmtUsrFlag
                        if ((objClaim.IsNew && !objLimits.ClaimCreateAllowed(objClaim.LineOfBusCode, objClaim.ClaimTypeCode, objClaim.ClaimStatusCode, objLobSettings.ClmAccLmtUsrFlag)) ||
                            !objClaim.IsNew && !objLimits.ClaimCreateAllowed(objClaim.LineOfBusCode, objClaim.ClaimTypeCode, objClaim.ClaimStatusCode, objLobSettings.ClmAccLmtUsrFlag))
                        {
                            Errors.Add(Globalization.GetString("SaveError", base.ClientId),
                                String.Format(Globalization.GetString("Save.Claim.UserLimitFailed", base.ClientId), objCache.GetCodeDesc(objClaim.ClaimTypeCode, base.Adaptor.userLogin.objUser.NlsCode), objCache.GetCodeDesc(objClaim.ClaimStatusCode, base.Adaptor.userLogin.objUser.NlsCode)),
                                BusinessAdaptorErrorType.Error); //Aman ML Change
                            Cancel = true;
                            return;
                        }
                }

                //Check Permission to Event Number.
                if (base.ModuleSecurityEnabled)//Is Security Enabled
                {
                    //Existing Claim
                    if (!objClaim.IsNew)
                    {
                        if (!m_fda.userLogin.IsAllowedEx(this.SecurityId, FormBase.RMO_ALLOW_EDIT_EVENT_NUM))
                        {
                            string sPrev = objEvent.Context.DbConnLookup.ExecuteString("SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + objEvent.EventId);
                            objEvent.EventNumber = sPrev;
                            objClaim.EventNumber = sPrev;
                        }
                        //Start:Sumit (10/28/2010) - MITS# 22849 - Update Claim Event number when edited.
                        else
                        {
                            objClaim.EventNumber = objEvent.EventNumber;
                        }
                        //End:Sumit
                    }
                    else
                    {
                        //New Claim, new Event
                        if (objEvent.EventId == 0)
                        {
                            if (!m_fda.userLogin.IsAllowedEx(this.SecurityId, FormBase.RMO_ALLOW_ENTRY_EVENT_NUM))
                            {
                                // JP 10.25.2005    string sPrev = objEvent.Context.DbConnLookup.ExecuteString("SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + objEvent.EventId);
                                // JP 10.25.2005    objEvent.EventNumber = sPrev;
                                // JP 10.25.2005    objClaim.EventNumber = sPrev;
                                objEvent.EventNumber = string.Empty;
                                objClaim.EventNumber = string.Empty;
                            }
                        }
                        else //New Claim, existing Event.
                        {
                            if (!m_fda.userLogin.IsAllowedEx(this.SecurityId, FormBase.RMO_ALLOW_EDIT_EVENT_NUM))
                            {
                                string sPrev = objEvent.Context.DbConnLookup.ExecuteString("SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + objEvent.EventId);
                                objEvent.EventNumber = sPrev;
                                objClaim.EventNumber = sPrev;
                            }
                        }
                    }
                    //Check Permissions to Claim Number
                    if (objClaim.IsNew && !m_fda.userLogin.IsAllowedEx(this.SecurityId, FormBase.RMO_CLAIM_NUMBER_ALLOW_ENTRY))
                        // JP 10.25.2005    objClaim.ClaimNumber = objEvent.Context.DbConnLookup.ExecuteString("SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID=" + objClaim.ClaimId);
                        objClaim.ClaimNumber = "";
                    else if (!objClaim.IsNew && !m_fda.userLogin.IsAllowedEx(this.SecurityId, FormBase.RMO_CLAIM_NUMBER_ALLOW_EDIT))
                        objClaim.ClaimNumber = objEvent.Context.DbConnLookup.ExecuteString("SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID=" + objClaim.ClaimId);

                    //Check Permission to change Department. --This one is considered fatal.
                    if (objEvent.EventId != 0 && objEvent.DeptEid != objEvent.Context.DbConnLookup.ExecuteInt("SELECT DEPT_EID FROM EVENT WHERE EVENT_ID=" + objEvent.EventId))
                        if (!m_fda.userLogin.IsAllowedEx(this.SecurityId, FormBase.RMO_CLAIM_CHANGE_DEPARTMENT))
                        {
                            Errors.Add(Globalization.GetString("SaveError", base.ClientId),
                                String.Format(Globalization.GetString("Save.DeptChangeFailed", base.ClientId)),
                                BusinessAdaptorErrorType.Error);
                            Cancel = true;
                            return;
                        }

                    //Check Permission to change MCO.
                    if (m_fda.userLogin.objRiskmasterDatabase.Status)//Is Security Enabled
                        if (!objClaim.IsNew && objClaim.McoEid != objClaim.Context.DbConnLookup.ExecuteInt("SELECT MCO_EID FROM CLAIM WHERE CLAIM_ID=" + objClaim.ClaimId))
                            if (!m_fda.userLogin.IsAllowedEx(this.SecurityId, FormBase.RMO_POLICY_MCO_CHANGE))
                            {
                                Errors.Add(Globalization.GetString("SaveError", base.ClientId),
                                    String.Format(Globalization.GetString("Save.MCOChangeFailedClaim", base.ClientId)),
                                    BusinessAdaptorErrorType.Error);
                                Cancel = true;
                                return;
                            }

                    //Check Permission on Claim Status Change
                    // Decide if we should close open event diaries after the save...
                    if (!objClaim.IsNew)
                    {
                        int prevCode = objClaim.Context.DbConnLookup.ExecuteInt("SELECT CLAIM_STATUS_CODE FROM CLAIM WHERE CLAIM_ID=" + objClaim.ClaimId);
                        int prevParent = objClaim.Context.LocalCache.GetRelatedCodeId(prevCode);
                        int curParent = objClaim.Context.LocalCache.GetRelatedCodeId(objClaim.ClaimStatusCode);

                        if (prevParent != curParent && curParent == 8)
                            if (!m_fda.userLogin.IsAllowedEx(this.SecurityId, FormBase.RMO_CLAIM_ALLOW_CLOSE))
                            {
                                Errors.Add(Globalization.GetString("SaveError", base.ClientId),
                                    String.Format(Globalization.GetString("Save.ClaimClosePermissionFailed", base.ClientId)),
                                    BusinessAdaptorErrorType.Error);
                                Cancel = true;
                                return;
                            }

                        if (prevParent != curParent && curParent == 9)
                            if (!m_fda.userLogin.IsAllowedEx(this.SecurityId, FormBase.RMO_CLAIM_ALLOW_REOPEN))
                            {
                                Errors.Add(Globalization.GetString("SaveError", base.ClientId),
                                    String.Format(Globalization.GetString("Save.ClaimReOpenPermissionFailed", base.ClientId)),
                                    BusinessAdaptorErrorType.Error);
                                Cancel = true;
                                return;
                            }

                        //Nikhil Garg		Dated: 27-Jan-06
                        //Checking for Permission to update a closed claim
                        if (prevParent == curParent && curParent == 8)
                            if (!m_fda.userLogin.IsAllowedEx(this.SecurityId, FormBase.RMO_CLAIM_UPDATE_CLOSED))
                            {
                                Errors.Add(Globalization.GetString("SaveError", base.ClientId),
                                    String.Format(Globalization.GetString("Save.ClaimCloseUpdatePermissionFailed", base.ClientId)),
                                    BusinessAdaptorErrorType.Error);
                                Cancel = true;
                                return;
                            }
                    }
                    //bCloseDiaries = (objCache.GetRelatedCodeId(objClaim.ClaimStatusCode)==8);

                }//End "If Module Security"

                //Default the Time of Event & Time Reported
                if (objEvent.TimeOfEvent == "" && objClaim.Context.InternalSettings.SysSettings.DefaultTimeFlag != -1)
                    objEvent.TimeOfEvent = "12:00 AM";
                if (objClaim.TimeOfClaim == "" && objClaim.Context.InternalSettings.SysSettings.DefaultTimeFlag != -1)
                    objClaim.TimeOfClaim = "12:00 AM";
                if (objEvent.TimeReported == "" && objClaim.Context.InternalSettings.SysSettings.DefaultTimeFlag != -1)
                    objEvent.TimeReported = "12:00 AM";
                //Nikhil Garg		Dated: 23-Dec-05	Bug No:1069
                //As per the RMWorld behaviour, there is no need to initialize Time Reported
                //			if(objEvent.TimeReported=="" && objEvent.DateReported!="")
                //				objEvent.TimeReported= "12:00 AM";

                //			// Update Claim Closed Status History and Date Stamp if necessary.
                //			ClaimStatusHist objStatusHist = objClaim.ClaimStatusHistList.LastStatusChange();
                //			if(objStatusHist !=null)
                //				if(objClaim.ClaimStatusCode!=objStatusHist.StatusCode)
                //				{
                //					ClaimStatusHist objNew  = objClaim.ClaimStatusHistList.AddNew();
                //					objNew.StatusCode = objClaim.Context.DbConnLookup.ExecuteInt("SELECT CLAIM_STATUS_CODE FROM CLAIM WHERE CLAIM_ID=" + objClaim.ClaimId);
                //					objNew.StatusChgdBy = m_fda.userLogin.LoginName;
                //					objNew.DateStatusChgd = Conversion.ToDbDate(DateTime.Now);
                //					
                //					if(	objClaim.Context.LocalCache.GetRelatedCodeId(objClaim.ClaimStatusCode)==8 ||
                //						objClaim.ClaimStatusCode==8)
                //					{
                //						bCloseDiaries = true;
                //						if(objClaim.DttmClosed=="")
                //							objClaim.DttmClosed = objNew.DateStatusChgd;
                //					}
                //				}

                // Update Close Diaries Flag for later if necessary.
                int prevStatusCode = objClaim.Context.DbConnLookup.ExecuteInt("SELECT CLAIM_STATUS_CODE FROM CLAIM WHERE CLAIM_ID=" + objClaim.ClaimId);

                //Changed by Saurabh Arora for MITS 19061:Start
                /* bool bWasClosed = (objClaim.Context.LocalCache.GetRelatedCodeId(prevStatusCode) == 8 || prevStatusCode == 8);
                bool bIsClosed = (objClaim.Context.LocalCache.GetRelatedCodeId(objClaim.ClaimStatusCode) == 8 || objClaim.ClaimStatusCode == 8);
                if (!bWasClosed && bIsClosed)
                    bCloseDiaries = true; */
                int bClosedCodeId = objClaim.Context.LocalCache.GetCodeId("C", "Status");
                bCloseDiaries = (objClaim.Context.LocalCache.GetRelatedCodeId(objClaim.ClaimStatusCode) == bClosedCodeId);

                //Changed by Saurabh Arora for MITS 19061:End

                //Mridul 05/28/09 MITS 16745-Chubb ACK Letter Enhancement
                if (prevStatusCode != objClaim.ClaimStatusCode)
                {
                    //Commented By Navdeep, Change in table, Closed Status Mapped to SYS_UTIL_CLM_LTR
                    //string sStatInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT CLOSED_STATUS FROM SYS_PARMS_CLM_LTR WHERE CLOSED_STATUS LIKE '%," + objClaim.ClaimStatusCode + ",%'"));                
                    string sStatInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT COLUMN_DESC FROM SYS_UTIL_CLM_LTR WHERE ROW_ID = 3 AND CODE_EID IN (" + objClaim.ClaimStatusCode + ")"));
                    //Check if Current status is among those in the list
                    if (sStatInList != "")
                        bTriggerLetter = true;
                }
                //Duplicate Claim Check
                // Mihika 6-Jan-2006 Defect no. 1189
                if (objClaim.IsNew && IsDupeClaim())
                {
                    //				Errors.Add(Globalization.GetString("SaveError"),
                    //					String.Format(Globalization.GetString("Save.DuplicateClaimWarning")),
                    //					BusinessAdaptorErrorType.Error);
                    Cancel = true;
                    return;
                }
                //MITS 34260 start - adding existing claim condition for dupcriteria 4.
                else if ((!objClaim.IsNew) && bFlag && (objLobSettings.DupPayCriteria == 4) && IsDupeClaim())
                {
                    Cancel = true;
                    return;
                }
                //MITS 34260 end
                base.ResetSysExData("dupeoverride", "");
                bool bCheckPolicyValidation = false; ;
                if (base.SysEx.SelectSingleNode("//CheckPolicyValidation") != null && base.SysEx.SelectSingleNode("//CheckPolicyValidation").InnerText != "")
                    bCheckPolicyValidation = Conversion.ConvertStrToBool(base.SysEx.SelectSingleNode("//CheckPolicyValidation").InnerText);

                // Mihika 16-Jan-2006 Defect no. 1232
                // Autoselect Policy if the corresponding Utilities Setting is checked
                int iPolicyId = 0;
                //nsachdeva2:  MITS 25163- Policy Interface Implementation - 1/6/2012
                if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == 0)
                {
                    //Start:Nitin Goel,Check for specific LOB. 01/13/10, MITS#18229
                    //if (objClaim.Context.InternalSettings.SysSettings.UseEnhPolFlag == -1)
                    if (objLobSettings.UseEnhPolFlag == -1)
                    //End:Nitin Goel,Check for specific LOB. 01/13/10: MITS#18229
                    {
                        if (!ValidatePolicy(objClaim.PrimaryPolicyIdEnh))
                            objClaim.PrimaryPolicyIdEnh = 0;
                    }
                    else
                    {
                        if (bCheckPolicyValidation && !ValidatePolicy(objClaim.PrimaryPolicyId))
                            objClaim.PrimaryPolicyId = 0;
                    }
                }
                //Gagan Safeway Retrofit Policy Jurisidiction : START / added by rkaur7 on 06/04/2009-  MITS 16668
                //sysex nodes created to capture Number of policy satisfying Auto Policy Select Criterion 
                //and flag for whether to select policy pop up or not
                base.ResetSysExData("CheckPolicyValidation", "false");
                base.CreateSysExData("NumOfPolicy");
                base.CreateSysExData("ShowPolicyPopUp");
                //Gagan Safeway Retrofit Policy Jurisidiction : END / End rkaur7

                //Sumit kumar, 07/01/2010 - Disable autoselect functionality when Policy mgmt is selected for WC
                if (objClaim.Context.InternalSettings.SysSettings.AutoSelectPolicy && objClaim.Context.InternalSettings.ColLobSettings[m_LOB].UseEnhPolFlag != -1)
                {
                    //Gagan Safeway Retrofit Policy Jurisidiction :  passed iNumPolicy in the AutoFillPolicy function to capture the number of policy satisfying autofill criteria / added by rkaur7 on 06/04/2009-  MITS 16668
                    AutoFillPolicy(ref iPolicyId, ref iNumPolicy);
                    //Start:Nitin Goel,Check for specific LOB. 01/13/10, MITS#18229
                    // Start Naresh MITS 9996 Policy Attach with Claim for Policy Management
                    //Gagan Safeway Retrofit Policy Jurisidiction :  Added NumOfPolicy also in the if loop / added by rkaur7 on 06/04/2009-  MITS 16668
                    //if (objClaim.Context.InternalSettings.SysSettings.UseEnhPolFlag == -1 && objClaim.PrimaryPolicyIdEnh == 0 && iNumPolicy<=1)
                    if (objLobSettings.UseEnhPolFlag == -1 && objClaim.PrimaryPolicyIdEnh == 0 && iNumPolicy <= 1)
                    //End:Nitin Goel,Check for specific LOB. 01/13/10: MITS#18229
                    {
                        objClaim.PrimaryPolicyIdEnh = iPolicyId;
                    }
                    // End Naresh MITS 9996 Policy Attach with Claim for Policy Management
                    //Start:Nitin Goel,Check for specific LOB. 01/13/10, MITS#18229
                    // Start Naresh MITS 9996 Policy Attach with Claim for Policy Tracking
                    //Gagan Safeway Retrofit Policy Jurisidiction : START - Added NumOfPolicy also in the if loop / added by rkaur7 on 06/04/2009-  MITS 16668
                    //else if (objClaim.Context.InternalSettings.SysSettings.UseEnhPolFlag != -1 && objClaim.PrimaryPolicyId == 0 && iNumPolicy<=1)
                    else if (objLobSettings.UseEnhPolFlag != -1 && objClaim.PrimaryPolicyId == 0 && iNumPolicy <= 1)
                    //End:Nitin Goel,Check for specific LOB. 01/13/10: MITS#18229
                    {
                        objClaim.PrimaryPolicyId = iPolicyId;
                    }
                    // End Naresh MITS 9996 Policy Attach with Claim for Policy Tracking
                }
                sUserCloseDiary = base.SysEx.SelectSingleNode("//CloseDiary").InnerText;
                //Gagan Safeway Retrofit Policy Jurisidiction : START / added by rkaur7 on 06/04/2009-  MITS 16668
                if (base.GetSysExDataNodeText("//ShowPolicyPopUp").ToString() == "")
                {
                    sShowPolPopUp = "True";
                }
                else
                {
                    sShowPolPopUp = "";
                }
                base.SysEx.SelectSingleNode("//NumOfPolicy").InnerText = iNumPolicy.ToString();
                base.SysEx.SelectSingleNode("//ShowPolicyPopUp").InnerText = sShowPolPopUp;
                //Gagan Safeway Retrofit Policy Jurisidiction : END / added by rkaur7 on 06/04/2009-  MITS 16668
                //Changed by Gagan for MITS 11451 : Start
                if (base.SysEx.SelectSingleNode("//DeleteAutoCheck").InnerText != "")
                    bDeleteAutoChecks = Conversion.ConvertStrToBool(base.SysEx.SelectSingleNode("//DeleteAutoCheck").InnerText);
                //Changed by Gagan for MITS 11451 : End

                //check for duplicate claim number
                if (objClaim.ClaimNumber.Trim() != string.Empty)
                {
                    //string sSQL = "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER='" + objClaim.ClaimNumber + "' AND CLAIM_ID<>" + objClaim.ClaimId.ToString();
                    //using (DbReader objReader = objClaim.Context.DbConn.ExecuteReader(sSQL))
                    //{
                    //Deb:Commented above line used parameterized input for sql query for MITS 25141

                    Dictionary<string, string> dictParams = new Dictionary<string, string>();
                    string sSQL = string.Format("SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER={0} AND CLAIM_ID <> {1}", "~CLAIMNUMBER~", "~CLAIMID~");
                    dictParams.Add("CLAIMNUMBER", objClaim.ClaimNumber);
                    dictParams.Add("CLAIMID", objClaim.ClaimId.ToString());
                    using (DbReader objReader = DbFactory.ExecuteReader(objClaim.Context.DbConn.ConnectionString, sSQL, dictParams))
                    {
                        if (objReader.Read())
                        {
                            Errors.Add(Globalization.GetString("SaveError", base.ClientId),
                                String.Format(Globalization.GetString("Save.DuplicateClaimNumberWarning", base.ClientId)),
                                BusinessAdaptorErrorType.Error);
                            Cancel = true;
                            objReader.Close();
                            objReader.Dispose();
                            return;
                    }
                }
            }
        }
        private bool ValidatePolicy(int p_iPolicyId)
        {
            string sSQL = CreateSqlForSelectingPolicy(p_iPolicyId, true);
            using (DbReader objReader = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
            {
                if (objReader.Read())
                {
                    return true;

                }
                else { return false; }
            }


        }
        public override void AfterSave()
        {
            base.AfterSave();

            //Added by Navdeep for Auto FROI ACORD - Chubb
            AutoFroiAcordAdaptor objAutoFroiAcord = null;
            //R7 Perf Imp
            //objAutoFroiAcord = new AutoFroiAcordAdaptor();
            string sReason = string.Empty;
            int iRowID = 0;
            //End Navdeep for Auto FROI ACORD - Chubb

            //Mridul. MITS 16745-Chubb Enhancement
            #region ClaimLetter
            int iLtrGenerated = 0;
            int iEmailAck = 0;
            Event objEvent = null;
            StringBuilder sbSql = null;
            StringBuilder sOrgEid = null;
            DbReader objReader = null;
            int iTempId = 0;
            //Check from LIST instead of below TODO
            base.ResetSysExData("ClaimLetterTmplId", "");

            if (objData.Context.InternalSettings.SysSettings.ClaimLetterFlag)
            {
                iTempId = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar("SELECT CLLTR_TMPL_ID FROM SYS_PARMS_CLM_LTR"), base.ClientId);
                if (bTriggerLetter && objClaim.CurrentAdjuster.AdjusterEid.ToString() != "" && iTempId != 0)
                {
                    //If generated before
                    iLtrGenerated = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar("SELECT LTR_FLAG FROM CLM_LTR_LOG WHERE LTR_FLAG = 1 AND CLAIM_ID = " + objClaim.ClaimId.ToString()), base.ClientId);
                    if (iLtrGenerated == 0)
                    {
                        //Commented BY Navdeep - Table changed to SYS_UTIL_CLM_LTR instead of SYS_PARMS_CLM_LTR for Claim Type IDS
                        //string sTypeNotInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT DTTM_RCD_LAST_UPD FROM SYS_PARMS_CLM_LTR WHERE X_CLAIM_TYPE_CODE NOT LIKE '%," + objClaim.ClaimTypeCode + ",%'"));
                        string sTypeNotInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT COLUMN_DESC FROM SYS_UTIL_CLM_LTR WHERE ROW_ID = 2 AND CODE_EID IN (" + objClaim.ClaimTypeCode + ")"));
                        //Check if Claim Type is in Included List
                        //Commented by Code - Change due to above modified Query
                        //if (sTypeNotInList != "")
                        if (string.IsNullOrEmpty(sTypeNotInList))
                        {
                            //Commented BY Navdeep - Table changed to SYS_UTIL_CLM_LTR instead of SYS_PARMS_CLM_LTR for Adjuster EIDs
                            //string sAdjNotInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT ADJUSTERS FROM SYS_PARMS_CLM_LTR WHERE ADJUSTERS LIKE '%," + objClaim.CurrentAdjuster.AdjusterEid + ",%'"));
                            string sAdjNotInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT COLUMN_DESC FROM SYS_UTIL_CLM_LTR WHERE ROW_ID = 1 AND CODE_EID IN (" + objClaim.CurrentAdjuster.AdjusterEid + ")"));
                            //Check if Current Adjuster is in Included List
                            //if (sAdjNotInList != "")
                            if (!string.IsNullOrEmpty(sAdjNotInList))
                            {
                                //Check if ACK should be generated
                                objEvent = (Event)m_fda.Factory.GetDataModelObject("Event", false);
                                objEvent.MoveTo(objClaim.EventId);
                                sbSql = new StringBuilder();
                                sOrgEid = new StringBuilder();
                                //rsushilaggar Modified the file to improve performance
                                sbSql.Append("SELECT FACILITY_EID,LOCATION_EID,DIVISION_EID,REGION_EID,OPERATION_EID,COMPANY_EID,CLIENT_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid);
                                using (objReader = objClaim.Context.DbConn.ExecuteReader(sbSql.ToString()))
                                {
                                    if (objReader.Read())
                                        sOrgEid.Append(objEvent.DeptEid + "," + objReader.GetInt("FACILITY_EID") + "," + objReader.GetInt("LOCATION_EID") + "," + objReader.GetInt("DIVISION_EID") + "," + objReader.GetInt("REGION_EID") + "," + objReader.GetInt("OPERATION_EID") + "," + objReader.GetInt("COMPANY_EID") + "," + objReader.GetInt("CLIENT_EID"));
                                }
                                //Geeta : Mits 18718 for Acord Comma Seperated List Issue
                                iEmailAck = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar("SELECT COUNT(EMAIL_ACK) FROM ENT_X_CONTACTINFO EXC,CONTACT_LOB WHERE EXC.EMAIL_ACK = -1 AND EXC.ENTITY_ID IN (" + sOrgEid.ToString() + ") AND EXC.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode), base.ClientId);
                                //Send CLOSED Letter Signal
                                if (iEmailAck > 0)
                                    base.ResetSysExData("ClaimLetterTmplId", "CL");
                            }
                        }
                    }
                }
            }
            bTriggerLetter = false;
            #endregion ClaimLetter
            //navdeep - Auto FROI ACORD For Chubb - Triggering Criteria
            #region Auto FROI ACORD
            if (objData.Context.InternalSettings.SysSettings.AutoFROIACORDFlag)
            {
                if (objClaim.CurrentAdjuster != null)
                {
                    //R7 Perf Imp
                    objAutoFroiAcord = new AutoFroiAcordAdaptor(base.ClientId);
                    if (objClaim.CurrentAdjuster.CurrentAdjFlag == true && objClaim.MailSent == "")
                    {
                        //Commented By Navdeep - Table now changed to ADJUSTER_FROI_ACORD
                        //iRowID = Riskmaster.Common.Conversion.ConvertObjToInt(objCon.ExecuteScalar("SELECT ROW_ID FROM EMAIL_FROI_ACORD WHERE ADJUSTERS LIKE '%," + objClaim.CurrentAdjuster.AdjusterEid + ",%'"), base.ClientId);
                        iRowID = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar("SELECT ROW_ID FROM ADJUSTER_FROI_ACORD WHERE ADJUSTER_EID = " + objClaim.CurrentAdjuster.AdjusterEid), base.ClientId);
                        if (iRowID > 0)
                        {
                            // Geeta 19341  : Split architecture issue  ACORD and FROI
                            XmlNode objHostNode = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/RequestHost");
                            if (objHostNode != null)
                            {
                                objAutoFroiAcord.RequestHost = objHostNode.InnerText;
                            }

                            sReason = objAutoFroiAcord.generateFROIACORD(objClaim);
                            objAutoFroiAcord.FROIACORDERRLOG(objClaim, sReason);
                        }
                        else
                        {
                            objAutoFroiAcord.FROIACORDERRLOG(objClaim, "Current Adjuster was not found in Assigned Adjuster List");
                        }
                    }
                    else if (objClaim.CurrentAdjuster.CurrentAdjFlag == false && objClaim.MailSent == "")
                    {
                        objAutoFroiAcord.FROIACORDERRLOG(objClaim, "Triggering criteria not met.");
                    }
                }
            }
            //navdeep - End 
            #endregion Auto FROI ACORD
            //Check and close Diaries
            // sUserCloseDiary applied by Rahul 6th Feb 2006.
            //            if(bCloseDiaries && sUserCloseDiary=="true")
            //                objClaim.Context.DbConnLookup.ExecuteNonQuery(
            //                    String.Format(@"UPDATE WPA_DIARY_ENTRY SET STATUS_OPEN = 0 
            //					WHERE ATTACH_RECORDID = {0} AND ATTACH_TABLE = 'CLAIM'",objClaim.ClaimId));
            //            bCloseDiaries =false;

            //pmittal5 MITS 11653 4/3/2008 -Start          
            //pmittal5 MITS 11653 4/3/2008 -End

            //BOB Reserves Enhancement : start
            if (iUseUnitStat == -1)
            {
                objClaim.UnitStat.ClaimId = objClaim.ClaimId;
                objClaim.UnitStat.Save();
            }
            //BOB Reserves Enhancement :  end


            CloseAllDiaries();

            //Changed by Gagan for MITS 11451 : Start
            //Delete auto checks
            if (bDeleteAutoChecks)
                DeleteAutoChecks();
            //Changed by Gagan for MITS 11451 : End

            //pmahli MITS 10997 - start 
            if (objClaim.PrimaryClaimant.ClaimantEid != objClaim.PrimaryPiEmployee.PiEid)
            {
                if (objClaim.PrimaryPiEmployee.PiEid != null)
                {
                    objClaim.PrimaryClaimant.ClaimantEid = objClaim.PrimaryPiEmployee.PiEid;
                    objClaim.PrimaryClaimant.Save();
                }
            }
            //pmahli MITS 10811 - End
            //pmittal5 MITS 11653 4/3/2008 -Start
            //Neha 14/09/2011 R8 changes moved the code to last so that making parent null does not affect the other child objects.
            if (iUseCaseMgmt == -1)
            {
                objClaim.CaseManagement.Parent = null;
                objClaim.CaseManagement.PiRowId = objClaim.PrimaryPiEmployee.PiRowId;//
                objClaim.CaseManagement.ClaimId = objClaim.ClaimId;
                objClaim.CaseManagement.Save();
            }
            //pmittal5 MITS 11653 4/3/2008 -End

            //added by Amitosh for Policy interface
            //	changes for MITS 32702: start
            if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
            {
                AddExternalPolicyEntities();
                // need to attach units of internal policy to claim, for insured setting, no change required, so adding policy interface check here: MITS 32702
                if (objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface)
                    AddExternalPolicyInsureds();
            }
            // changes for MITS 32702: end
            //tanwar2 - mits 30910 - start
            string sDeletedPolicyIds = string.Empty;

            for (int i = 0; i < arrPoliciesDeleted.Count; i++)
            {
                //sDeletedPolicyIds += string.IsNullOrEmpty(sDeletedPolicyIds) ? arrPoliciesDeleted[i] : "," + arrPoliciesDeleted[i];
                sDeletedPolicyIds = string.Concat(sDeletedPolicyIds, string.IsNullOrEmpty(sDeletedPolicyIds) ? arrPoliciesDeleted[i] : string.Concat(",", arrPoliciesDeleted[i]));
            }

            if (!string.IsNullOrEmpty(sDeletedPolicyIds))
            {
                UpdateClaimDeductiblesForRemovedPolicy(objClaim.ClaimId, sDeletedPolicyIds);
            }

            if (hSetPoliciesAdded != null && hSetPoliciesAdded.Count > 0)
            {
                UpdateClaimDeductiblesForAddedPolicy(objClaim.ClaimId);
            }
            //tanwar2 - mits 30910 - end
        }

        private bool IsDupeClaim()
        {
            Event objEvent = (objClaim.Parent as Event);

            //			string sTmp="";
            XmlNode eltDupe = null;
            //			XmlNode eltClaim =null;

            StringBuilder sSql = new StringBuilder();
            int iDupNumDays;
            int iDupPayCriteria;

            if (!objLobSettings.DuplicateFlag)
                return false;

            //User already saw dup popup and is choosing to proceed.
            //if(base.SysEx.SelectSingleNode("//dupeoverride").InnerText.Trim()!="")    csingh7 : Commented for MITS 19840
            if (base.SysEx.SelectSingleNode("//dupeoverride").InnerText.Trim() == "IsSaveDup")  //csingh7 MITS 19840 : If user saves Duplicate Claim.
                return false;

            iDupNumDays = objLobSettings.DupNumDays;
            iDupPayCriteria = objLobSettings.DupPayCriteria;

            switch (iDupPayCriteria)
            {
                case 0: //No Additional Criteria
                    sSql.Append(" SELECT CLAIM.CLAIM_ID ");
                    sSql.Append(" FROM CLAIM, EVENT, CLAIMANT ");
                    sSql.Append(" WHERE CLAIM.LINE_OF_BUS_CODE =" + objClaim.LineOfBusCode);
                    sSql.Append(" AND CLAIM.CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode);
                    sSql.Append(" AND CLAIM.EVENT_ID = EVENT.EVENT_ID ");
                    sSql.Append(" AND EVENT.DEPT_EID = " + objEvent.DeptEid);
                    sSql.Append(" AND CLAIM.FILING_STATE_ID = " + objClaim.FilingStateId);
                    sSql.Append(" AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID ");
                    sSql.Append(" AND CLAIMANT.CLAIMANT_EID = " + objClaim.PrimaryPiEmployee.PiEid);
                    sSql.Append(" AND EVENT.DATE_OF_EVENT = '" + objEvent.DateOfEvent + "'");
                    break;
                case 1: //Department and Event Date within X Days
                    sSql.Append(" SELECT CLAIM.CLAIM_ID ");
                    sSql.Append(" FROM CLAIM, EVENT, CLAIMANT ");
                    sSql.Append(" WHERE CLAIM.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode);
                    sSql.Append(" AND CLAIM.EVENT_ID = EVENT.EVENT_ID ");
                    sSql.Append(" AND EVENT.DEPT_EID = " + objEvent.DeptEid);
                    sSql.Append(" AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID ");
                    sSql.Append(" AND EVENT.DATE_OF_EVENT >= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(-iDupNumDays)) + "'");
                    sSql.Append(" AND EVENT.DATE_OF_EVENT <= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(iDupNumDays)) + "'");
                    break;
                case 2: //Operation Org Level, Clmnt Last Name and Event Date within X Days
                    string sAbbreviation = string.Empty;
                    string sName = string.Empty;
                    int iParentId = 0;
                    objCache.GetParentOrgInfo(objEvent.DeptEid, 1007, ref sAbbreviation, ref sName, ref iParentId);

                    sSql.Append(" SELECT CLAIM.CLAIM_ID");
                    sSql.Append(" FROM CLAIM, EVENT, CLAIMANT, ENTITY, ORG_HIERARCHY ");
                    sSql.Append(" WHERE CLAIM.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode);
                    sSql.Append(" AND CLAIM.EVENT_ID = EVENT.EVENT_ID ");
                    sSql.Append(" AND EVENT.DEPT_EID = ORG_HIERARCHY.DEPARTMENT_EID ");
                    sSql.Append(" AND ORG_HIERARCHY.OPERATION_EID = " + iParentId);
                    sSql.Append(" AND EVENT.DATE_OF_EVENT >= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(-iDupNumDays)) + "'");
                    sSql.Append(" AND EVENT.DATE_OF_EVENT <= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(iDupNumDays)) + "'");
                    sSql.Append(" AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID ");
                    sSql.Append(" AND CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID ");
                    //+ " AND ENTITY.LAST_NAME = '" + objClaim.PrimaryPiEmployee.PiEntity.LastName + "'";
                    //abisht MITS 12032
                    //If Employee Name in workers Compensation claim contains Apostophe
                    //then to avoid Sql Exception
                    sSql.Append(" AND ENTITY.LAST_NAME = '" + objClaim.PrimaryPiEmployee.PiEntity.LastName.Replace("'", "''") + "'");
                    break;
                case 3: //Dept, Clm Type, Clmnt SS#, and Event Date within X Days
                    sSql.Append(" SELECT CLAIM.CLAIM_ID ");
                    sSql.Append(" FROM CLAIM, EVENT, CLAIMANT, ENTITY ");
                    sSql.Append(" WHERE CLAIM.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode);
                    sSql.Append(" AND CLAIM.CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode);
                    sSql.Append(" AND CLAIM.EVENT_ID = EVENT.EVENT_ID ");
                    sSql.Append(" AND EVENT.DEPT_EID = " + objEvent.DeptEid);
                    sSql.Append(" AND CLAIM.FILING_STATE_ID = " + objClaim.FilingStateId);
                    sSql.Append(" AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID ");
                    sSql.Append(" AND CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID");
                    sSql.Append(" AND REPLACE(ENTITY.TAX_ID, '-', '') = '" + objClaim.PrimaryPiEmployee.PiEntity.TaxId.Replace("-", "") + "'");
                    sSql.Append(" AND EVENT.DATE_OF_EVENT >= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(-iDupNumDays)) + "'");
                    sSql.Append(" AND EVENT.DATE_OF_EVENT <= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(iDupNumDays)) + "'");
                    break;
                //MITS 34260 start
                case 4:
                    string sClaimPolicyList = string.Empty;
                    int iprevClaimPolicyCount;

                    foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
                    {
                        iprevClaimPolicyCount = objClaim.Context.DbConnLookup.ExecuteInt("SELECT Count(CLAIM_X_POLICY.ROW_ID) FROM CLAIM , CLAIM_X_POLICY WHERE CLAIM_X_POLICY.Claim_ID = Claim.Claim_ID AND Claim.CLAIM_ID=" + objClaim.ClaimId + " AND CLAIM_X_POLICY.POLICY_ID = " + objClaimXPolicy.PolicyId);
                        if (iprevClaimPolicyCount == 0)
                            sClaimPolicyList += objClaimXPolicy.PolicyId + ",";
                    }

                    if (sClaimPolicyList.EndsWith(","))
                        sClaimPolicyList = sClaimPolicyList.Substring(0, sClaimPolicyList.Length - 1);

                    sSql.Append(" SELECT DISTINCT CLAIM.CLAIM_ID As CLAIM_ID, POLICY.POLICY_SYMBOL AS SYMBOL, POLICY.POLICY_NUMBER AS POL_NBR, POLICY.MODULE AS MODULE FROM CLAIM, EVENT, CLAIM_X_POLICY, POLICY WHERE CLAIM.LINE_OF_BUS_CODE =" + objClaim.LineOfBusCode);
                    sSql.Append(" AND CLAIM.CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode);
                    sSql.Append(" AND CLAIM.EVENT_ID = EVENT.EVENT_ID AND CLAIM.CLAIM_ID = CLAIM_X_POLICY.CLAIM_ID AND CLAIM_X_POLICY.POLICY_ID = POLICY.POLICY_ID ");
                    sSql.Append(" AND POLICY.POLICY_NUMBER IN (SELECT DISTINCT(POLICY_NUMBER) FROM POLICY WHERE POLICY_ID in ('" + sClaimPolicyList + "')) ");
                    sSql.Append(" AND EVENT.DATE_OF_EVENT >= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(-iDupNumDays)) + "'");
                    sSql.Append(" AND EVENT.DATE_OF_EVENT <= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(iDupNumDays)) + "'");
                    break;
                //MITS 34260 end
                default:
                    sSql.Append(" SELECT CLAIM.CLAIM_ID ");
                    sSql.Append(" FROM CLAIM, EVENT, CLAIMANT ");
                    sSql.Append(" WHERE CLAIM.LINE_OF_BUS_CODE =" + objClaim.LineOfBusCode);
                    sSql.Append(" AND CLAIM.CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode);
                    sSql.Append(" AND CLAIM.EVENT_ID = EVENT.EVENT_ID ");
                    sSql.Append(" AND EVENT.DEPT_EID = " + objEvent.DeptEid);
                    sSql.Append(" AND CLAIM.FILING_STATE_ID = " + objClaim.FilingStateId);
                    sSql.Append(" AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID ");
                    sSql.Append(" AND CLAIMANT.CLAIMANT_EID = " + objClaim.PrimaryPiEmployee.PiEid);
                    sSql.Append(" AND EVENT.DATE_OF_EVENT = '" + objEvent.DateOfEvent + "'");
                    break;
            }

            //			SQL =	@"SELECT CLAIM.CLAIM_ID, CLAIM.CLAIM_NUMBER, CLAIM.CLAIM_STATUS_CODE, EVENT.EVENT_NUMBER, EVENT.EVENT_DESCRIPTION, EVENT.EVENT_ID
            //						FROM CLAIM, EVENT 
            //						WHERE CLAIM.LINE_OF_BUS_CODE =" + objClaim.LineOfBusCode 
            //				+ " AND CLAIM.CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode 
            //				+ " AND CLAIM.EVENT_ID = EVENT.EVENT_ID "
            //				+ " AND EVENT.DEPT_EID = " + objEvent.DeptEid 
            //				+ " AND EVENT.DATE_OF_EVENT = '" + objEvent.DateOfEvent + "'"
            //				+ " AND CLAIM.DATE_OF_CLAIM = '" + objClaim.DateOfClaim + "'";


            using (DbReader rdr = objClaim.Context.DbConnLookup.ExecuteReader(sSql.ToString()))
            {
                if (!rdr.Read())
                    return false;

                //Clear Dupe SysEx node.
                base.ResetSysExData("dupeoverride", "");
                eltDupe = this.SysEx.SelectSingleNode("//dupeoverride");
                //Set Info from the Current Claim.
                //				this.SafeSetAttribute(eltDupe, "formname","claimwc");
                //				this.SafeSetAttribute(eltDupe, "claimtype",objClaim.Context.LocalCache.GetCodeDesc(objClaim.ClaimTypeCode));
                //				this.SafeSetAttribute(eltDupe, "department",objClaim.Context.LocalCache.GetEntityLastFirstName(objEvent.DeptEid));
                //				this.SafeSetAttribute(eltDupe, "eventdate",Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString());
                //				this.SafeSetAttribute(eltDupe, "claimdate",Conversion.ToDate(objClaim.DateOfClaim).ToShortDateString());

                string sClaimIds = string.Empty;
                //MITS 34260 start
                string sPolNum = "";
                if (iDupPayCriteria == 4)
                    sPolNum = string.Concat(string.Concat(rdr.GetString("SYMBOL").ToString(), " "), rdr.GetString("POL_NBR").ToString(), " ", rdr.GetValue(3).ToString());
                //MITS 34260 end
                StringBuilder sbClaimIDs = new StringBuilder();
                //Get Info From Potential Duplicate Matched Claims.
                do
                {
                    //rsushilaggar chamnges made for performance improvement
                    //sClaimIds = sClaimIds + rdr.GetInt("CLAIM_ID").ToString() + ",";
                    sbClaimIDs.Append(rdr.GetInt("CLAIM_ID").ToString() + ",");
                    //					eltClaim = eltDupe.OwnerDocument.CreateElement("claim");
                    //					this.SafeSetAttribute(eltClaim, "claimid",rdr.GetInt("CLAIM_ID").ToString());
                    //					this.SafeSetAttribute(eltClaim, "claimnumber",rdr.GetString("CLAIM_NUMBER"));
                    //					this.SafeSetAttribute(eltClaim, "claimstatus",objClaim.Context.LocalCache.GetCodeDesc(rdr.GetInt("CLAIM_STATUS_CODE")));
                    //					this.SafeSetAttribute(eltClaim, "eventid",rdr.GetInt("EVENT_ID").ToString());
                    //					this.SafeSetAttribute(eltClaim, "eventnumber",rdr.GetString("EVENT_NUMBER"));
                    //					SQL = rdr.GetString("EVENT_DESCRIPTION").Replace('\n',' ').Replace('\r',' ').Replace("'","\'");
                    //					if(SQL.Length >47)
                    //						SQL = SQL.Substring(0,47)+"...";
                    //					this.SafeSetAttribute(eltClaim, "eventdescription",SQL);
                    //					eltDupe.AppendChild(eltClaim);
                } while (rdr.Read());
                sClaimIds = sbClaimIDs.ToString().Substring(0, sbClaimIDs.ToString().Length - 1);

                sClaimIds = sClaimIds
                    + ";" + Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()
                //MITS 34260 start + ";" + objClaim.Context.LocalCache.GetEntityLastFirstName(objEvent.DeptEid);
                     + ";" + objClaim.Context.LocalCache.GetEntityLastFirstName(objEvent.DeptEid) + ";" + sPolNum;
                //MITS 34260 end
                eltDupe.InnerText = sClaimIds;
            }

            //Fetch First Claimant's Name (if available) for each Claim.

            //			foreach(XmlNode eltTemp in eltDupe.SelectNodes("./claim"))
            //			{
            //				SQL = 
            //					"SELECT ENTITY.LAST_NAME,ENTITY.FIRST_NAME FROM ENTITY,CLAIMANT WHERE " +
            //					" ENTITY.ENTITY_ID = CLAIMANT.CLAIMANT_EID AND CLAIMANT.CLAIM_ID = " + eltTemp.Attributes["claimid"].Value;
            //				using(DbReader rdr = objClaim.Context.DbConnLookup.ExecuteReader(SQL))
            //				{
            //					if(!rdr.Read())
            //						sTmp = "[none]";
            //					else
            //						sTmp = rdr.GetString("LAST_NAME").Trim() + " " + rdr.GetString("FIRST_NAME").Trim();
            //					this.SafeSetAttribute(eltTemp, "claimant",sTmp.Trim());
            //				}
            //				
            //			}

            return true;

        }

        //validate data according to the Business Rules
        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;
            Event objEvent = objClaim.Parent as Event;
            PiEmployee objEmp = objClaim.PrimaryPiEmployee as PiEmployee;

            // Perform data validation
            //Time Zone Enhancements - R7- yatharth
            string sToday = Conversion.ToDbDate(System.DateTime.Now);
            string sTime = Conversion.GetTime(System.DateTime.Now.ToShortTimeString()); // Current server Time
            string sOrgLevelToday = Conversion.ToDbDate(System.DateTime.Now); //Current OrgLevel Date
            string sOrgLevelTime = Conversion.GetTime(System.DateTime.Now.ToShortTimeString()); //Current OrgLevel Time
            int iOrgLevelID = 0;
            //int iDateChange = 0; //0-> No Change in date, 1-> Subtract 1 day from date, 2-> Add 1 Day to date
            DateTime dtOrgLevelCurrentDateTime = System.DateTime.Now;//For storing Current Date time of OrgLevel
            string sOrgLevelCurrentDate = string.Empty; //Current Date in OrgLevel
            string sOrgLevelCurrentTime = string.Empty; //Current Time in OrgLevel
            DateTime dtTemp = System.DateTime.Now.ToUniversalTime(); //DateTime in GMT
            string sSQL = string.Empty;
            int iTimeZoneTracking = 0; //-1 if Time Zone is enabled 
            //int iDayLightSavings = 0; //-1 if Day Light Savings is enabled
            //bool bDayLightSavings = false; //True for Day Light Savings and vice-versa
            string sTimeZone = string.Empty;//For storing TimeZone of OrgLevel
            int OrgTimeZoneLevel = 0;

            //pmittal5 Mits 18751 09/20/10 - If existing Event Number is used
            int iEventId = 0;
            object oEvent = null;
            bool bEventFound = false;
            //Parag R7 : We will check for Duplicate EventNumber only in case when Generate AutoEventNumber is OFF
            if (!objEvent.EventNumber.Equals(""))
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                if (objEvent.EventId == 0)
                {

                    //sSQL = String.Format("SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = '{0}'", objEvent.EventNumber);
                    sSQL = String.Format("SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = {0}", "~EVENTNUMBER~");
                    dictParams.Add("EVENTNUMBER", objEvent.EventNumber);
                }
                else
                {
                    //sSQL = String.Format("SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = '{0}' and EVENT_ID <> {1} ", objEvent.EventNumber, objEvent.EventId);
                    sSQL = String.Format("SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = {0} and EVENT_ID <> {1} ", "~EVENTNUMBER~", "~EVENTID~");
                    dictParams.Add("EVENTNUMBER", objEvent.EventNumber);
                    dictParams.Add("EVENTID", objEvent.EventId.ToString());
                }
                //oEvent = objClaim.Context.DbConn.ExecuteScalar(sSQL);//Deb MITS 25141
                oEvent = DbFactory.ExecuteScalar(objEvent.Context.DbConn.ConnectionString, sSQL, dictParams);

                if (oEvent != null)
                {
                    iEventId = Conversion.CastToType<int>(oEvent.ToString(), out bEventFound);
                    if (iEventId != 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                                    String.Format(Globalization.GetString("Validation.EventAlreadyExists", base.ClientId), objEvent.EventNumber.ToString()),
                                    BusinessAdaptorErrorType.Error);
                        bError = true;
                    }
                }
            }
            //End - pmittal5
            //sSQL = @"SELECT ORG_TIMEZONE_LEVEL FROM SYS_PARMS";
            //OrgTimeZoneLevel = Conversion.ConvertStrToInteger(objClaim.Context.DbConn.ExecuteScalar(sSQL).ToString());
            //BOB Enhancement  :  Ukusvaha
            if (objEvent.DeptEid == 0)
            {
                if (objEvent.Context.InternalSettings.SysSettings.AutoPopulateDpt == -1)
                {

                    objEvent.DeptEid = objEvent.Context.InternalSettings.SysSettings.AutoFillDpteid;
                }
            }
            //End BOB Enhancement 
            OrgTimeZoneLevel = objClaim.Context.InternalSettings.SysSettings.OrgTimeZoneLevel;
            //Parag R7 : We will check for Time Validation only in case when Time Validation is ON
            if (OrgTimeZoneLevel != 0)
            {
                sSQL = @"SELECT ";

                switch (OrgTimeZoneLevel)
                {
                    case 1005:
                        sSQL = sSQL + "CLIENT_EID";
                        break;
                    case 1006:
                        sSQL = sSQL + "COMPANY_EID";
                        break;
                    case 1007:
                        sSQL = sSQL + "OPERATION_EID";
                        break;
                    case 1008:
                        sSQL = sSQL + "REGION_EID";
                        break;
                    case 1009:
                        sSQL = sSQL + "DIVISION_EID";
                        break;
                    case 1010:
                        sSQL = sSQL + "LOCATION_EID";
                        break;
                    case 1011:
                        sSQL = sSQL + "FACILITY_EID";
                        break;
                    default:
                        sSQL = sSQL + "DEPARTMENT_EID";
                        break;
                }


                //BOB Enhancement  :  Ukusvaha
                if (objEvent.DeptEid == 0)
                {
                    if (objEvent.Context.InternalSettings.SysSettings.AutoPopulateDpt == -1)
                    {

                        objEvent.DeptEid = objEvent.Context.InternalSettings.SysSettings.AutoFillDpteid;
                    }
                }
                //End BOB Enhancement 
                sSQL = sSQL + " FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid;

                //END
                iOrgLevelID = Conversion.ConvertStrToInteger(objClaim.Context.DbConn.ExecuteScalar(sSQL).ToString());

                //To get Time Zone of the OrgLevel is enabled or not
                sSQL = @"SELECT TIME_ZONE_TRACKING FROM ENTITY WHERE ENTITY_ID = " + iOrgLevelID.ToString();
                iTimeZoneTracking = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar(sSQL), base.ClientId);

                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {

                    sSQL = @"SELECT SHORT_CODE FROM CODES WHERE CODE_ID=
                        (SELECT TIME_ZONE_CODE FROM ENTITY WHERE ENTITY_ID=" + iOrgLevelID.ToString() + ")";
                    sTimeZone = objClaim.Context.DbConn.ExecuteScalar(sSQL).ToString();

                    //To see Day light saving is enabled or not for OrgLevel
                    //sSQL = @"SELECT DAY_LIGHT_SAVINGS FROM ENTITY WHERE ENTITY_ID = " + iOrgLevelID.ToString();
                    //iDayLightSavings = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar(sSQL), base.ClientId);

                    ////If Daylight savings are enabled
                    //if (iDayLightSavings != 0)
                    //{
                    //    bDayLightSavings = true;
                    //}
                    ////If Daylight savings are disabled
                    //else
                    //{
                    //    bDayLightSavings = false;
                    //}

                    //Current Time in OrgLevel
                    // sOrgLevelTime = Conversion.ConvertGMTToOrgHierarchyTime(sTimeZone, Conversion.ToDbTime(System.DateTime.Now.ToUniversalTime()), bDayLightSavings, ref iDateChange);
                    dtOrgLevelCurrentDateTime = Conversion.ConvertGMTToOrgHierarchyTime(sTimeZone, dtTemp);
                    //if (iDateChange != 0)
                    //{
                    //    if (iDateChange == 1)
                    //    {
                    //        dtTemp = dtTemp.AddDays(-1);
                    //    }
                    //    else
                    //    {
                    //        dtTemp = dtTemp.AddDays(1);
                    //    }
                    //}
                    //Current Date in OrgLevel
                    sOrgLevelToday = Conversion.ToDbDate(dtOrgLevelCurrentDateTime);
                    //Date in OrgLevel in mm/dd/yy format for displaying validation error message
                    //sOrgLevelCurrentDate = dtTemp.ToShortDateString();
                    //Time in OrgLevel for displaying validation error message
                    sOrgLevelCurrentTime = Conversion.ToDbTime(dtOrgLevelCurrentDateTime);
                    //dtOrgLevelCurrentDateTime = Convert.ToDateTime(sOrgLevelCurrentDate + " " + sOrgLevelCurrentTime);
                }

            }

            //Advance claim Validation -- start
            if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
            {
                int iRecordCount = 0;
                foreach (int iPolicyId in arrPoliciesDeleted)
                {
                    sSQL = "SELECT COUNT(*) FROM RESERVE_CURRENT, POLICY_X_CVG_TYPE WHERE RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND RESERVE_CURRENT.CLAIM_ID = " + objClaim.ClaimId + " AND POLICY_X_CVG_TYPE.POLICY_ID = " + iPolicyId;
                    iRecordCount = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar(sSQL), base.ClientId);
                    if (iRecordCount > 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Field.Policies", base.ClientId)),
                            BusinessAdaptorErrorType.Error);

                        bError = true;
                        break;
                    }
                }
            }

            //Validation in case of TimeZone
            if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
            {
                if (objEvent.DateOfEvent.CompareTo(sOrgLevelToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.EventDate", base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                    BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            //Validation without TimeZone
            else
            {
                if (objEvent.DateOfEvent.CompareTo(sToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.EventDate", base.ClientId), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }


            //Validation in case of TimeZone
            if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
            {

                if (objClaim.DateOfClaim.CompareTo(sOrgLevelToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.ClaimDate", base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }

            else
            {
                //Validation without TimeZone
                if (objClaim.DateOfClaim.CompareTo(sToday) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.ClaimDate", base.ClientId), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }

            if (objClaim.DateOfClaim.CompareTo(objEvent.DateOfEvent) < 0)
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate", base.ClientId), Globalization.GetString("Field.ClaimDate", base.ClientId), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
                    BusinessAdaptorErrorType.Error);

                bError = true;
            }



            //vsharma205 ::RMA- 11623  starts
            if (objClaim.PrimaryPiEmployee.HrangeEndDate != "" && objClaim.PrimaryPiEmployee.HrangeStartDate != "")
            {
                if (objClaim.PrimaryPiEmployee.HrangeStartDate.CompareTo(objClaim.PrimaryPiEmployee.HrangeEndDate) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                  String.Format(Globalization.GetString("Validation.MustBeLessThanHospitalizationToDate", base.ClientId, sLangCode), Globalization.GetString("Field.HRangeStartDate", base.ClientId, sLangCode), Conversion.ToDate(objClaim.PrimaryPiEmployee.HrangeEndDate).ToShortDateString()),
                  BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            //vsharma205 ::RMA- 11623 ends


            //Validation for the Time of event reported on the claim screen START
            if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
            {
                if (objEvent.DateReported.CompareTo(sOrgLevelToday) == 0 && objEvent.TimeReported.CompareTo(sOrgLevelCurrentTime) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                      String.Format(Globalization.GetString("Field.TimeReported", base.ClientId), dtOrgLevelCurrentDateTime.ToLongTimeString()),
                      BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }
            else
            {
                if (objEvent.DateReported.CompareTo(sToday) == 0 && objEvent.TimeReported.CompareTo(sTime) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                      String.Format(Globalization.GetString("Field.TimeReported", base.ClientId), System.DateTime.Now.ToLongTimeString()),
                      BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }
            //Validation for the Time of event reported on the claim screen END



            if (objEvent.DateOfEvent.CompareTo(objClaim.DateOfClaim) == 0 && objClaim.TimeOfClaim.CompareTo(objEvent.TimeOfEvent) < 0)
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventTime", base.ClientId), Globalization.GetString("Field.ClaimTime", base.ClientId), Conversion.ToDate(objEvent.TimeOfEvent).ToLongTimeString()),
                    BusinessAdaptorErrorType.Error);

                bError = true;
            }
            //Validation for the Time of Claim on the claim screen START
            if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
            {
                if (objClaim.DateOfClaim.CompareTo(sOrgLevelToday) == 0 && objClaim.TimeOfClaim.CompareTo(sOrgLevelCurrentTime) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                      String.Format(Globalization.GetString("Field.TimeOfClaim", base.ClientId), dtOrgLevelCurrentDateTime.ToLongTimeString()),
                      BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }
            else
            {
                if (objClaim.DateOfClaim.CompareTo(sToday) == 0 && objClaim.TimeOfClaim.CompareTo(sTime) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                      String.Format(Globalization.GetString("Field.TimeOfClaim", base.ClientId), System.DateTime.Now.ToLongTimeString()),
                      BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }

            if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
            {
                if (objEvent.DateOfEvent.CompareTo(sOrgLevelToday) == 0 && objEvent.TimeOfEvent.CompareTo(sOrgLevelCurrentTime) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                      String.Format(Globalization.GetString("Field.TimeOfEvent", base.ClientId), dtOrgLevelCurrentDateTime.ToLongTimeString()),
                      BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }
            else
            {
                if (objEvent.DateOfEvent.CompareTo(sToday) == 0 && objEvent.TimeOfEvent.CompareTo(sTime) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                      String.Format(Globalization.GetString("Field.TimeOfEvent", base.ClientId), System.DateTime.Now.ToLongTimeString()),
                      BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }

            //Validations of the Time Fields present on the claim screen. END

            if (objClaim.DttmClosed != "")
            { //Time Zone Validation
                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {
                    //nsachdeva2 - MITS: 28069 - 4/4/2012
                    //if (objClaim.DttmClosed.Substring(0, 8).CompareTo(sOrgLevelToday) > 0 &&
                    if (Conversion.ToDbDate(Conversion.ConvertGMTToOrgHierarchyTime(sTimeZone, Conversion.ToDate(objClaim.DttmClosed))).CompareTo(sOrgLevelToday) > 0 &&
                        objData.Context.InternalSettings.CacheFunctions.GetShortCode(objData.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(objClaim.ClaimStatusCode)) == "C")
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.DttmClosed", base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);

                        bError = true;
                    }
                }
                //Validation without TimeZone
                else
                {
                    if (objClaim.DttmClosed.Substring(0, 8).CompareTo(sToday) > 0 &&
                        objData.Context.InternalSettings.CacheFunctions.GetShortCode(objData.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(objClaim.ClaimStatusCode)) == "C")
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.DttmClosed", base.ClientId), System.DateTime.Now.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);

                        bError = true;
                    }
                }

                if (objClaim.DttmClosed.Substring(0, 8).CompareTo(objClaim.DateOfClaim) < 0 &&
                    objData.Context.InternalSettings.CacheFunctions.GetShortCode(objData.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(objClaim.ClaimStatusCode)) == "C")
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThanClaimDate", base.ClientId), Globalization.GetString("Field.DttmClosed", base.ClientId), Conversion.ToDate(objClaim.DateOfClaim).ToShortDateString()),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }

            if (objEvent.DateReported != "")
            {

                //Validation in case of TimeZone
                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {

                    if (objEvent.DateReported.CompareTo(sOrgLevelToday) > 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.DateReported", base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);

                        bError = true;
                    }
                }
                //Validation without TimeZone
                else
                {
                    if (objEvent.DateReported.CompareTo(sToday) > 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.DateReported", base.ClientId), System.DateTime.Now.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);

                        bError = true;
                    }

                }
                if (objEvent.DateReported.CompareTo(objEvent.DateOfEvent) < 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate", base.ClientId), Globalization.GetString("Field.DateReported", base.ClientId), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }

                //Nikhil Garg		Dated: 23-Dec-05	Bug No:1069
                //Checking if the TimeReported is Empty before comparing it with EventTime
                if (objEvent.DateOfEvent.CompareTo(objEvent.DateReported) == 0 && objEvent.TimeReported != string.Empty && objEvent.TimeReported.CompareTo(objEvent.TimeOfEvent) < 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventTime", base.ClientId), Globalization.GetString("Field.EventTimeReported", base.ClientId), Conversion.ToDate(objEvent.DateOfEvent + objEvent.TimeOfEvent).ToLongTimeString()),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }
            if (objClaim.DateRptdToRm != string.Empty)
            {

                //Validation in case of TimeZone
                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {
                    if (objClaim.DateRptdToRm.CompareTo(sOrgLevelToday) > 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.DateReported", base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);

                        bError = true;
                    }
                }
                else
                {
                    if (objClaim.DateRptdToRm.CompareTo(sToday) > 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.DateReported", base.ClientId), System.DateTime.Now.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);

                        bError = true;
                    }
                }
                //if (objClaim.DateRptdToRm.CompareTo(objEvent.DateOfEvent) < 0)
                //{
                //    Errors.Add(Globalization.GetString("ValidationError"),
                //        String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate"), Globalization.GetString("Field.DateReported"), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
                //        BusinessAdaptorErrorType.Error);

                //    bError = true;
                //}
                if (objClaim.DateRptdToRm.CompareTo(objClaim.DateOfClaim) < 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThanClaimDate", base.ClientId), Globalization.GetString("Field.DateReported", base.ClientId), Conversion.ToDate(objClaim.DateOfClaim).ToShortDateString()),
                        BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }
            // npadhy MITS 14200 Validation for Injury From Date and Injury To Date as Per RMWorld
            if (!string.IsNullOrEmpty(objEvent.InjuryFromDate) && !(string.IsNullOrEmpty(objEvent.InjuryToDate)))
            {
                if (objEvent.InjuryFromDate.CompareTo(objEvent.InjuryToDate) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanInjuryToDate", base.ClientId), Globalization.GetString("Field.InjuryFromDate", base.ClientId), Conversion.ToDate(objEvent.InjuryToDate).ToShortDateString()),
                            BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }

            if (objEmp != null)
            {
                if (objEmp.DateHired != "")
                {

                    //Validation in case of TimeZone
                    if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                    {
                        if (objEmp.DateHired.CompareTo(sOrgLevelToday) > 0)
                        {
                            Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                                String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.DateHired", base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                                BusinessAdaptorErrorType.Error);

                            bError = true;
                        }
                        else if (objEmp.DateHired.CompareTo(objEvent.DateOfEvent) > 0 && objEvent.DateOfEvent != "")
                        {
                            Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                                String.Format(Globalization.GetString("Validation.MustBeLessThanEventDate", base.ClientId), Globalization.GetString("Field.DateHired", base.ClientId), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
                                BusinessAdaptorErrorType.Error);

                            bError = true;
                        }
                    }
                    //Validation without timezone
                    else
                    {
                        if (objEmp.DateHired.CompareTo(sToday) > 0)
                        {
                            Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                                String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.DateHired", base.ClientId), System.DateTime.Now.ToShortDateString()),
                                BusinessAdaptorErrorType.Error);

                            bError = true;
                        }
                        else if (objEmp.DateHired.CompareTo(objEvent.DateOfEvent) > 0 && objEvent.DateOfEvent != "")
                        {
                            Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                                String.Format(Globalization.GetString("Validation.MustBeLessThanEventDate", base.ClientId), Globalization.GetString("Field.DateHired", base.ClientId), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
                                BusinessAdaptorErrorType.Error);

                            bError = true;
                        }
                    }

                }
                if (objEmp.DateOfDeath != "")
                {

                    //Validation in case of TimeZone
                    if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                    {

                        if (objEmp.DateOfDeath.CompareTo(sOrgLevelToday) > 0)
                        {
                            Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                                String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.DateOfDeath", base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                                BusinessAdaptorErrorType.Error);

                            bError = true;
                        }
                        else if (objEmp.DateOfDeath.CompareTo(objEvent.DateOfEvent) < 0)
                        {
                            Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                                String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate", base.ClientId), Globalization.GetString("Field.DateOfDeath", base.ClientId), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
                                BusinessAdaptorErrorType.Error);

                            bError = true;
                        }
                    }
                    //Validation without timezone
                    else
                    {
                        if (objEmp.DateOfDeath.CompareTo(sToday) > 0)
                        {
                            Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                                String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.DateOfDeath", base.ClientId), System.DateTime.Now.ToShortDateString()),
                                BusinessAdaptorErrorType.Error);

                            bError = true;
                        }
                        else if (objEmp.DateOfDeath.CompareTo(objEvent.DateOfEvent) < 0)
                        {
                            Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                                String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate", base.ClientId), Globalization.GetString("Field.DateOfDeath", base.ClientId), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
                                BusinessAdaptorErrorType.Error);

                            bError = true;
                        }
                    }
                }


                //Validation in case of TimeZone
                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {

                    if (objEmp.PiEntity.BirthDate != "" && objEmp.PiEntity.BirthDate.CompareTo(sOrgLevelToday) > 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);

                        bError = true;
                    }
                }
                //Validation without timezone
                else
                {
                    if (objEmp.PiEntity.BirthDate != "" && objEmp.PiEntity.BirthDate.CompareTo(sToday) > 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId), System.DateTime.Now.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);

                        bError = true;
                    }
                }

                //pmittal5 MITS:12360  06/16/08
                if (objEmp.TermDate != "")
                {

                    //Validation in case of TimeZone
                    if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                    {
                        if (objEmp.TermDate.CompareTo(sOrgLevelToday) > 0)
                        {
                            Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                                String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.TermDate", base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                                BusinessAdaptorErrorType.Error);
                            bError = true;
                        }
                    }
                    //else case normal validation
                    else
                    {
                        if (objEmp.TermDate.CompareTo(sToday) > 0)
                        {
                            Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                                String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.TermDate", base.ClientId), System.DateTime.Now.ToShortDateString()),
                                BusinessAdaptorErrorType.Error);
                            bError = true;
                        }
                    }
                    if (objEmp.TermDate.CompareTo(objEmp.DateHired) < 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.TermDate", base.ClientId), Globalization.GetString("Field.DateHired", base.ClientId)),
                            BusinessAdaptorErrorType.Error);
                        bError = true;
                    }
                    if (objEmp.TermDate.CompareTo(objEmp.PiEntity.BirthDate) < 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.TermDate", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId)),
                            BusinessAdaptorErrorType.Error);
                        bError = true;
                    }
                }
                //End - pmittal5
            }

            switch (objData.Context.InternalSettings.CacheFunctions.GetShortCode(objEmp.DisabilityCode))
            {
                case "ILL":
                    if (objEmp.IllnessCode == 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.IllnessCodeIsRequired", base.ClientId), Globalization.GetString("Field.IllnessCode", base.ClientId)),
                            BusinessAdaptorErrorType.Error);

                        bError = true;
                    }
                    break;
                case "INJ":
                    if (objEmp.InjuryCodes.Count == 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.InjuryTypeCodeIsRequired", base.ClientId), Globalization.GetString("Field.InjuryCodes", base.ClientId)),
                            BusinessAdaptorErrorType.Error);

                        bError = true;
                    }
                    break;
            }

            //Charanpreet for MITS 12174 : Start
            if (objEvent.DeptEid > 0)
            {
                bool bValerror = false;
                bValerror = objClaim.OrgHierarchyValidate();
                if (bValerror)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        "Selected Department Not Within Effective Date Range",
                        BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            //Charanpreet for Mits 12174 : End

            //nsachdeva2:  MITS 25163- Policy Interface Implementation - 1/6/2012
            if ((objClaim.ClaimPolicyList != null) && (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm != 0))
            {
                StringBuilder sbPolicyList = new StringBuilder();
                DbReader oReader = null;
                ArrayList objEffectivePolicies = new ArrayList();
                StringBuilder sbFailedPolicy = new StringBuilder();
                string sFaiedPolicyNames = string.Empty;
                string sTaggedPolicies = string.Empty;
                ValidatePolicy objPolicyValidate = null;
                //string sSql = string.Empty;
                foreach (ClaimXPolicy cp in objClaim.ClaimPolicyList)
                {

                    if (cp.PolicyId != 0)
                    {
                        if (sbPolicyList.Length > 0) sbPolicyList.Append(",");

                        sbPolicyList.Append(cp.PolicyId);
                        objEffectivePolicies.Add(cp.PolicyId.ToString());
                    }
                }
                try
                {
                    if (sbPolicyList.Length != 0)
                    {
                        // Pradyumna - MITS 36013 - Start
                        //objPolicyValidate = new ValidatePolicy(objClaim.Context.DbConn.ConnectionString);
                        objPolicyValidate = new ValidatePolicy(base.Adaptor.userLogin, base.ClientId);
                        // Pradyumna - MITS 36013 - End
                        //nsachdeva2 - 7/26/2012
                        //oReader = objPolicyValidate.PolicyValidation(sbPolicyList.ToString(), objEvent.DateOfEvent, objClaim.DateOfClaim, objClaim.CurrencyType.ToString());
                        //oReader = objPolicyValidate.PolicyValidation(sbPolicyList.ToString(), objEvent.DateOfEvent, objClaim.DateOfClaim, objClaim.CurrencyType.ToString(), objClaim.PolicyLOBCode.ToString());

                        oReader = objPolicyValidate.PolicyValidation(sbPolicyList.ToString(), objEvent.DateOfEvent, objClaim.DateOfClaim, objClaim.CurrencyType.ToString(), objClaim.PolicyLOBCode.ToString());

                        while (oReader.Read())
                        {
                            if (objEffectivePolicies.Contains(Conversion.ConvertObjToStr(oReader.GetValue("POLICY_ID"))))
                            {
                                objEffectivePolicies.Remove(Conversion.ConvertObjToStr(oReader.GetValue("POLICY_ID")));

                            }

                        }

                        oReader = null;
                        if (objEffectivePolicies.Count > 0)
                        {
                            foreach (string sTemp in objEffectivePolicies)
                            {
                                if (sbFailedPolicy.Length > 0)
                                    sbFailedPolicy.Append(",");

                                sbFailedPolicy.Append(sTemp);
                            }

                            oReader = objClaim.Context.DbConn.ExecuteReader("SELECT POLICY_NAME FROM POLICY WHERE POLICY_ID IN (" + sbFailedPolicy.ToString() + ")");
                            while (oReader.Read())
                            {
                                if (string.IsNullOrEmpty(sFaiedPolicyNames))
                                    sFaiedPolicyNames = Conversion.ConvertObjToStr(oReader.GetValue(0));
                                else
                                    sFaiedPolicyNames = sFaiedPolicyNames + "," + Conversion.ConvertObjToStr(oReader.GetValue(0));
                            }
                            if (!string.IsNullOrEmpty(sFaiedPolicyNames))
                                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                                GetValidationFailedMessage(sFaiedPolicyNames), BusinessAdaptorErrorType.Error);
                            bError = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (oReader != null)
                    {
                        oReader.Dispose();
                    }
                    if (objPolicyValidate != null)
                    {
                        objPolicyValidate = null;
                    }
                    objEffectivePolicies = null;
                    sbPolicyList = null;
                    sbFailedPolicy = null;
                }
            }
            // End MITS 25163- Policy Interface Implementation 

            // rrachev JIRA 5121 Begin
            if (bCloseDiaries)
            {
                string sContainsOpenDiariesOnFormOpen = GetSysExDataNodeText("ContainsOpenDiaries");
                if (sContainsOpenDiariesOnFormOpen == "false" && HasOpenDiaries)
                {
                    this.ResetSysExData("ContainsOpenDiaries", "true");
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.DiariesWereOpening", base.ClientId), BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            // rrachev JIRA 5121 End

            // Return true if there were validation errors
            Cancel = bError;
        }


        // BSB InitNew on 1:1 case causes navigation to be aborted if we slap in the key
        // of the parent because the value is shared as the key of the current record.
        // This makes navigation silently stop becuase the assumption is that you are already
        // looking at the record data you've requested.  As a work-around for only the 
        // 1:1 Cases let's zero this back out before the navigation call.
        public override void BeforeAction(enumFormActionType eActionType, ref bool Cancel)
        {
            base.BeforeAction(eActionType, ref Cancel);
            switch (eActionType)
            {
                case enumFormActionType.MoveFirst:
                case enumFormActionType.MoveLast:
                case enumFormActionType.MoveTo:
                    objClaim.EventId = 0;
                    break;
            }
        }

        //BSB 1:1 Hack Let's throw the proper parent key back on in case the MoveFirst\Last request
        // didn't find any records (we want to be sure the eventId stays set when creating a new
        // record.
        public override void AfterAction(enumFormActionType eActionType)
        {
            base.AfterAction(eActionType);
            switch (eActionType)
            {
                case enumFormActionType.MoveFirst:
                case enumFormActionType.MoveLast:
                case enumFormActionType.MoveTo:
                    if (objClaim.EventId == 0) //No record found - treat as new.
                        objClaim.EventId = this.m_ParentId;
                    break;
            }
        }

        private void SetAWWSysExNodes(int p_iAWWFormOption)
        {
            int iBaseArrayNumber = 0;

            //set Base Array Number
            switch (p_iAWWFormOption)
            {
                case -1:
                    //Commented by Shivendu for AWW Calculator
                    //base.AddKillNode("calcaww1");
                    base.AddKillNode("includezeros1");
                    base.AddKillNode("constantwage1");
                    base.AddKillNode("bonuses1");
                    base.AddKillNode("lastworkweek1");
                    //Commented by Shivendu for AWW Calculator
                    //base.AddKillNode("calcaww2");
                    base.AddKillNode("includezeros2");
                    base.AddKillNode("constantwage2");
                    base.AddKillNode("bonuses2");
                    base.AddKillNode("lastworkweek2");
                    break;
                case 0:
                case 52:
                case 529:
                    iBaseArrayNumber = 0;
                    break;
                case 13:
                case 149:
                    iBaseArrayNumber = 39;
                    break;
                case 26:
                    iBaseArrayNumber = 26;
                    break;
                case 61: //Florida
                    iBaseArrayNumber = 38;
                    break;
                //Start by Shivendu for AWW Calc case 12 and 8
                case 12:
                    iBaseArrayNumber = 40;
                    break;
                case 8:
                    iBaseArrayNumber = 44;
                    break;
                    //End by Shivendu for AWW Calc case 12 and 8
            }
            base.ResetSysExData("BaseArrayNumber", iBaseArrayNumber.ToString());

            //set Week Array
            string sWeekArray = string.Empty;

            ClaimAWW objAWW = objClaim.ClaimAWW;
            for (int i = iBaseArrayNumber; i < 52; i++)
                sWeekArray = sWeekArray + "," + objAWW.GetWeek(i);

            sWeekArray = sWeekArray.Substring(1);

            base.ResetSysExData("WeekArray", sWeekArray);

            base.ResetSysExData("IncludeZeros", objClaim.ClaimAWW.IncludeZeros.ToString());
            base.ResetSysExData("ConstantWage", objClaim.ClaimAWW.ConstantWage.ToString());
            base.ResetSysExData("Bonuses", objClaim.ClaimAWW.Bonuses.ToString());
            base.ResetSysExData("LastWorkWeek", objClaim.ClaimAWW.LastWorkWeek);
            base.ResetSysExData("Aww", objClaim.ClaimAWW.Aww.ToString());
        }

        // Mihika 16-Jan-2006 Defect. 1232 AutoUpdatePolicy Functionality
        // This is the same query implementation as it in Riskmaster.Application.ClaimLookups.LookupClaimPolicy class
        //Gagan Safeway Retrofit Policy Jurisidiction / added by rkaur7 on 06/04/2009-  MITS 16668
        //NumPolicy added in the parameter and number of policy satisfying Auto Select Criteria captured in the variable
        private void AutoFillPolicy(ref int p_iPolicyId, ref int p_iNumPolicy)
        {
            string sSQL = CreateSqlForSelectingPolicy(p_iPolicyId, false);
            DataSet objDataSet = null;
            DataRow[] objRow = null;
            objDataSet = DbFactory.GetDataSet(objClaim.Context.DbConn.ConnectionString, sSQL, base.ClientId);//rkaur27
            objRow = objDataSet.Tables[0].Select();

            p_iNumPolicy = objDataSet.Tables[0].Rows.Count;
            if (p_iNumPolicy != 0)
            {
                p_iPolicyId = Conversion.ConvertObjToInt(objRow[0]["POLICY_ID"], base.ClientId);

            }
            if (objDataSet != null)
                objDataSet = null;
            if (objRow != null)
                objRow = null;
            //pmahli Safeway Policy Tracking Enhancement 
            //Code commented as Dataset used instead of Reader to capture the row count
            //using (DbReader objReader = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
            //{
            //    if (objReader.Read())
            //    {
            //        p_iPolicyId = objReader.GetInt("POLICY_ID");

            //    }
            //}
        }

        private string CreateSqlForSelectingPolicy(int p_iPolicyId, bool p_bIncludePolicyID)
        {
            string sSQL = string.Empty;
            string sSQLSelect = string.Empty;
            string sSQLFrom = string.Empty;
            string sSQLWhere = string.Empty;
            string sSQLOrder = string.Empty;

            //rsushilaggar changes made for Performance Improvement
            StringBuilder sbSQLSelect = new StringBuilder();
            StringBuilder sbSQLWhere = new StringBuilder();
            int iCN_POLICY_REVOKED = 0;
            int iCN_POLICY_IN_EFFECT = 0;
            int iCN_POLICY_EXPIRED = 0;
            int iCN_POLICY_CANCELED = 0;
            int iCoverageTypeCode = 0;
            int iClaimTypeCode = 0;
            string sGetOrgForDept = string.Empty;
            //Gagan Safeway Retrofit Policy Jurisidiction 
            int iJurisdictionId = 0;
            int iPolicyDepartment = 0; //Added by Sumit Agarwal, 08/05/2014, MITS 33588
            const string SC_POLICY_REVOKED = "R";
            const string SC_POLICY_IN_EFFECT = "I";
            const string SC_POLICY_EXPIRED = "E";
            const string SC_POLICY_CANCELED = "C";

            Event objEvent = (objClaim.Parent as Event);

            iCN_POLICY_REVOKED = objCache.GetCodeId(SC_POLICY_REVOKED, "POLICY_STATUS");
            iCN_POLICY_IN_EFFECT = objCache.GetCodeId(SC_POLICY_IN_EFFECT, "POLICY_STATUS");
            iCN_POLICY_EXPIRED = objCache.GetCodeId(SC_POLICY_EXPIRED, "POLICY_STATUS");
            iCN_POLICY_CANCELED = objCache.GetCodeId(SC_POLICY_CANCELED, "POLICY_STATUS");
            iClaimTypeCode = objClaim.ClaimTypeCode;
            iCoverageTypeCode = objCache.GetRelatedCodeId(iClaimTypeCode);
            //Start: Added by Sumit Agarwal, 08/05/2014, MITS 33588
            if (objClaim.Context.InternalSettings.SysSettings.UseInsuredClaimDept == -1)
            {
                iPolicyDepartment = objClaim.InsuredClaimDeptEid;
            }
            else
            {
                iPolicyDepartment = objEvent.DeptEid;
            }
            //Start: Change the following code by Sumit Agarwal: 10/08/2014: MITS 33588
            //sGetOrgForDept = GetOrgListForDept(iPolicyDepartment);
            if (iPolicyDepartment == 0)
            {
                sGetOrgForDept = "0";
            }
            else
            {
                sGetOrgForDept = GetOrgListForDept(iPolicyDepartment);
            }
            //End: Change the following code by Sumit Agarwal: 10/08/2014: MITS 33588
            //End: Added by Sumit Agarwal, 08/05/2014, MITS 33588
            //Gagan Safeway Retrofit Policy Jurisidiction : START 
            iJurisdictionId = objClaim.FilingStateId;
            //Mukul Added 5/29/07 MITS 9424
            //Start:Nitin Goel,Check for specific LOB. 01/13/10, MITS#18229
            //if (objClaim.Context.InternalSettings.SysSettings.UseEnhPolFlag == -1)
            if (objLobSettings.UseEnhPolFlag == -1)
            //End:Nitin Goel,Check for specific LOB. 01/13/10: MITS#18229
            {
                sbSQLSelect = new StringBuilder();
                sbSQLWhere = new StringBuilder();
                //Build select for looking up policies from enhanced policy system for this claim
                //Start: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588
                //sbSQLSelect.Append("SELECT POLICY_ENH.POLICY_ID,POLICY_X_TERM_ENH.POLICY_NUMBER,POLICY_X_CVG_ENH.NEXT_POLICY_ID,POLICY_ENH.POLICY_NAME,POLICY_X_TERM_ENH.EFFECTIVE_DATE,POLICY_X_TERM_ENH.EXPIRATION_DATE,POLICY_X_CVG_ENH.OCCURRENCE_LIMIT");
                //sbSQLSelect.Append(",POLICY_X_CVG_ENH.POLICY_LIMIT,POLICY_X_CVG_ENH.TOTAL_PAYMENTS,ENTITY.LAST_NAME");
                //sbSQLSelect.Append(",POLICY_X_TERM_ENH.CANCEL_DATE,POLICY_X_TERM_ENH.REINSTATEMENT_DATE,POLICY_X_TERM_ENH.SEQUENCE_ALPHA,POLICY_X_TERM_ENH.TERM_NUMBER,POLICY_ENH.TRIGGER_CLAIM_FLAG");
                sbSQLSelect.Append("SELECT DISTINCT POLICY_ENH.POLICY_ID ");
                //End: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588
                sSQLFrom = " FROM POLICY_ENH, POLICY_X_TERM_ENH, POLICY_X_CVG_ENH, POLICY_X_INSRD_ENH, ENTITY";
                sbSQLWhere.Append(" WHERE (POLICY_ENH.POLICY_ID = POLICY_X_INSRD_ENH.POLICY_ID");
                sbSQLWhere.Append(" AND POLICY_ENH.POLICY_ID = POLICY_X_CVG_ENH.POLICY_ID AND POLICY_ENH.INSURER_EID = ENTITY.ENTITY_ID");
                sbSQLWhere.Append(" AND POLICY_ENH.POLICY_ID = POLICY_X_TERM_ENH.POLICY_ID AND POLICY_ENH.POLICY_ID = POLICY_X_INSRD_ENH.POLICY_ID)");

                sbSQLWhere.Append(" AND (POLICY_X_CVG_ENH.COVERAGE_TYPE_CODE IN (" + iCoverageTypeCode + ", " + iClaimTypeCode + ")");
                sbSQLWhere.Append(" AND POLICY_X_INSRD_ENH.INSURED_EID IN (" + sGetOrgForDept + "))");
                sbSQLWhere.Append(" AND ((POLICY_X_TERM_ENH.EFFECTIVE_DATE <= '" + objEvent.DateOfEvent + "'");
                sbSQLWhere.Append(" AND POLICY_X_TERM_ENH.EXPIRATION_DATE >= '" + objEvent.DateOfEvent + "'");
                sbSQLWhere.Append(" AND POLICY_ENH.TRIGGER_CLAIM_FLAG = 0)");
                //sbSQLWhere.Append(" OR (POLICY_X_TERM_ENH.EFFECTIVE_DATE <= '" + objClaim.DateOfClaim + "'");
                //sbSQLWhere.Append(" AND POLICY_X_TERM_ENH.EXPIRATION_DATE >= '" + objClaim.DateOfClaim + "'");
                //sbSQLWhere.Append(" AND POLICY_ENH.TRIGGER_CLAIM_FLAG <> 0))");
                sbSQLWhere.Append(" OR(POLICY_ENH.TRIGGER_CLAIM_FLAG <> 0))");
                sbSQLWhere.Append(" AND (POLICY_ENH.POLICY_INDICATOR = " + objCache.GetCodeId("P", "POLICY_INDICATOR") + ")");
                sbSQLWhere.Append(" AND PRIMARY_POLICY_FLG <> 0 ");
                if (p_bIncludePolicyID)
                    sbSQLWhere.Append(" AND POLICY_ENH.POLICY_ID =" + p_iPolicyId);
                //sSQLOrder = " ORDER BY POLICY_NUMBER ";       //Commented by Sumit Agarwal as we are only selecting the Policy_Id: 10/01/2014: MITS 33588
            }
            //rkaur7- It works based on utility setting of Show Jursidiction and Event State radio button
            else if (objClaim.Context.InternalSettings.SysSettings.ShowJurisdiction != 0 && objClaim.Context.InternalSettings.SysSettings.StateEval == 0)
            {
                //Condition added by rkaur7 to check whether Location state field is empty or not - MITS 16668
                if (objEvent.StateId == 0)
                {
                    sbSQLSelect = new StringBuilder();
                    sbSQLWhere = new StringBuilder();
                    //Start: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588
                    //sbSQLSelect.Append("SELECT DISTINCT(POLICY.POLICY_ID),POLICY_NUMBER,POLICY_X_CVG_TYPE.NEXT_POLICY_ID,POLICY_NAME,EFFECTIVE_DATE,EXPIRATION_DATE,OCCURRENCE_LIMIT,POLICY_LIMIT,TOTAL_PAYMENTS,ENTITY.LAST_NAME");
                    sbSQLSelect.Append("SELECT DISTINCT POLICY.POLICY_ID ");
                    //End: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588

                    sSQLFrom = " FROM POLICY, POLICY_X_CVG_TYPE, POLICY_X_INSURED, ENTITY ";

                    sbSQLWhere.Append(" WHERE (POLICY.POLICY_ID = POLICY_X_INSURED.POLICY_ID");
                    sbSQLWhere.Append(" AND POLICY.POLICY_ID = POLICY_X_CVG_TYPE.POLICY_ID AND POLICY.INSURER_EID = ENTITY.ENTITY_ID)");

                    sbSQLWhere.Append(" AND (POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE = " + iCoverageTypeCode);
                    sbSQLWhere.Append("      AND POLICY_X_INSURED.INSURED_EID IN (" + sGetOrgForDept + "))");

                    sbSQLWhere.Append(" AND ((POLICY.POLICY_STATUS_CODE IN (" + iCN_POLICY_IN_EFFECT + "," + iCN_POLICY_EXPIRED + "))");
                    sbSQLWhere.Append("      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED + " ");
                    sbSQLWhere.Append("          AND POLICY.CANCEL_DATE > '" + objClaim.DateOfClaim + "'");
                    sbSQLWhere.Append("          AND POLICY.TRIGGER_CLAIM_FLAG <> 0)");
                    sbSQLWhere.Append("      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED);
                    sbSQLWhere.Append("          AND POLICY.CANCEL_DATE > '" + objEvent.DateOfEvent + "'");
                    sbSQLWhere.Append("          AND POLICY.TRIGGER_CLAIM_FLAG = 0))");
                    sbSQLWhere.Append(" AND ((POLICY.EFFECTIVE_DATE <= '" + objEvent.DateOfEvent + "'");
                    sbSQLWhere.Append("       AND POLICY.EXPIRATION_DATE >= '" + objEvent.DateOfEvent + "'");
                    sbSQLWhere.Append("       AND POLICY.TRIGGER_CLAIM_FLAG = 0)");
                    sbSQLWhere.Append("      OR (POLICY.EFFECTIVE_DATE <= '" + objClaim.DateOfClaim + "'");
                    sbSQLWhere.Append("          AND POLICY.EXPIRATION_DATE >= '" + objClaim.DateOfClaim + "'");
                    sbSQLWhere.Append("          AND POLICY.TRIGGER_CLAIM_FLAG <> 0))");

                    sbSQLWhere.Append(" AND PRIMARY_POLICY_FLG <> 0 ");
                    if (p_bIncludePolicyID)
                        sbSQLWhere.Append(" AND POLICY.POLICY_ID =" + p_iPolicyId);

                    //sSQLOrder = " ORDER BY POLICY_NUMBER ";       //Commented by Sumit Agarwal as we are only selecting the Policy_Id: 10/01/2014: MITS 33588
                }
                //end
                else
                {
                    sbSQLSelect = new StringBuilder();
                    sbSQLWhere = new StringBuilder();
                    //Start: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588
                    //sbSQLSelect.Append("SELECT DISTINCT(POLICY.POLICY_ID),POLICY_NUMBER,POLICY_X_CVG_TYPE.NEXT_POLICY_ID,POLICY_NAME,EFFECTIVE_DATE,EXPIRATION_DATE,OCCURRENCE_LIMIT,POLICY_LIMIT,TOTAL_PAYMENTS,ENTITY.LAST_NAME");
                    sbSQLSelect.Append("SELECT DISTINCT POLICY.POLICY_ID ");
                    //End: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588

                    sSQLFrom = " FROM POLICY, POLICY_X_CVG_TYPE, POLICY_X_INSURED, ENTITY,POLICY_X_STATE"; //modified by rkaur7 on 06/04/2009-added POLICY_X_STATE table to query-  MITS 16668

                    sbSQLWhere.Append(" WHERE (POLICY.POLICY_ID = POLICY_X_INSURED.POLICY_ID");
                    sbSQLWhere.Append(" AND POLICY.POLICY_ID = POLICY_X_CVG_TYPE.POLICY_ID AND POLICY.INSURER_EID = ENTITY.ENTITY_ID)");

                    sbSQLWhere.Append(" AND (POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE = " + iCoverageTypeCode);
                    sbSQLWhere.Append("      AND POLICY_X_INSURED.INSURED_EID IN (" + sGetOrgForDept + "))");

                    sbSQLWhere.Append(" AND ((POLICY.POLICY_STATUS_CODE IN (" + iCN_POLICY_IN_EFFECT + "," + iCN_POLICY_EXPIRED + "))");
                    sbSQLWhere.Append("      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED + " ");
                    sbSQLWhere.Append("          AND POLICY.CANCEL_DATE > '" + objClaim.DateOfClaim + "'");
                    sbSQLWhere.Append("          AND POLICY.TRIGGER_CLAIM_FLAG <> 0)");
                    sbSQLWhere.Append("      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED);
                    sbSQLWhere.Append("          AND POLICY.CANCEL_DATE > '" + objEvent.DateOfEvent + "'");
                    sbSQLWhere.Append("          AND POLICY.TRIGGER_CLAIM_FLAG = 0))");
                    sbSQLWhere.Append(" AND ((POLICY.EFFECTIVE_DATE <= '" + objEvent.DateOfEvent + "'");
                    sbSQLWhere.Append("       AND POLICY.EXPIRATION_DATE >= '" + objEvent.DateOfEvent + "'");
                    sbSQLWhere.Append("       AND POLICY.TRIGGER_CLAIM_FLAG = 0)");
                    sbSQLWhere.Append("      OR (POLICY.EFFECTIVE_DATE <= '" + objClaim.DateOfClaim + "'");
                    sbSQLWhere.Append("          AND POLICY.EXPIRATION_DATE >= '" + objClaim.DateOfClaim + "'");
                    sbSQLWhere.Append("          AND POLICY.TRIGGER_CLAIM_FLAG <> 0))");

                    sbSQLWhere.Append(" AND PRIMARY_POLICY_FLG <> 0 ");
                    //added by rkaur7 on 06/04/2009-  MITS 16668 - filter for jurisdiction also added in auto select policy


                    sbSQLWhere.Append(" AND POLICY.POLICY_ID = POLICY_X_STATE.POLICY_ID ");
                    sbSQLWhere.Append(" AND POLICY_X_STATE.STATE_ID =" + objEvent.StateId + " ");

                    if (p_bIncludePolicyID)
                        sbSQLWhere.Append(" AND POLICY.POLICY_ID =" + p_iPolicyId);

                    //sSQLOrder = " ORDER BY POLICY_NUMBER ";       //Commented by Sumit Agarwal as we are only selecting the Policy_Id: 10/01/2014: MITS 33588
                }
            }
            else if (objClaim.Context.InternalSettings.SysSettings.ShowJurisdiction != 0 && objClaim.Context.InternalSettings.SysSettings.StateEval == 1)
            {
                sbSQLSelect = new StringBuilder();
                sbSQLWhere = new StringBuilder();
                //Start: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588
                //sbSQLSelect.Append("SELECT DISTINCT(POLICY.POLICY_ID),POLICY_NUMBER,POLICY_X_CVG_TYPE.NEXT_POLICY_ID,POLICY_NAME,EFFECTIVE_DATE,EXPIRATION_DATE,OCCURRENCE_LIMIT,POLICY_LIMIT,TOTAL_PAYMENTS,ENTITY.LAST_NAME");
                sbSQLSelect.Append("SELECT DISTINCT POLICY.POLICY_ID ");
                //End: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588

                sSQLFrom = " FROM POLICY, POLICY_X_CVG_TYPE, POLICY_X_INSURED, ENTITY,POLICY_X_STATE"; //modified by rkaur7 on 06/04/2009-added POLICY_X_STATE table to query-  MITS 16668

                sbSQLWhere.Append(" WHERE (POLICY.POLICY_ID = POLICY_X_INSURED.POLICY_ID");
                sbSQLWhere.Append(" AND POLICY.POLICY_ID = POLICY_X_CVG_TYPE.POLICY_ID AND POLICY.INSURER_EID = ENTITY.ENTITY_ID)");

                sbSQLWhere.Append(" AND (POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE = " + iCoverageTypeCode);
                sbSQLWhere.Append("      AND POLICY_X_INSURED.INSURED_EID IN (" + sGetOrgForDept + "))");

                sbSQLWhere.Append(" AND ((POLICY.POLICY_STATUS_CODE IN (" + iCN_POLICY_IN_EFFECT + "," + iCN_POLICY_EXPIRED + "))");
                sbSQLWhere.Append("      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED + " ");
                sbSQLWhere.Append("          AND POLICY.CANCEL_DATE > '" + objClaim.DateOfClaim + "'");
                sbSQLWhere.Append("          AND POLICY.TRIGGER_CLAIM_FLAG <> 0)");
                sbSQLWhere.Append("      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED);
                sbSQLWhere.Append("          AND POLICY.CANCEL_DATE > '" + objEvent.DateOfEvent + "'");
                sbSQLWhere.Append("          AND POLICY.TRIGGER_CLAIM_FLAG = 0))");
                sbSQLWhere.Append(" AND ((POLICY.EFFECTIVE_DATE <= '" + objEvent.DateOfEvent + "'");
                sbSQLWhere.Append("       AND POLICY.EXPIRATION_DATE >= '" + objEvent.DateOfEvent + "'");
                sbSQLWhere.Append("       AND POLICY.TRIGGER_CLAIM_FLAG = 0)");
                sbSQLWhere.Append("      OR (POLICY.EFFECTIVE_DATE <= '" + objClaim.DateOfClaim + "'");
                sbSQLWhere.Append("          AND POLICY.EXPIRATION_DATE >= '" + objClaim.DateOfClaim + "'");
                sbSQLWhere.Append("          AND POLICY.TRIGGER_CLAIM_FLAG <> 0))");

                sbSQLWhere.Append(" AND PRIMARY_POLICY_FLG <> 0 ");
                //added by rkaur7 on 06/04/2009-  MITS 16668 - filter for jurisdiction also added in auto select policy


                sbSQLWhere.Append(" AND POLICY.POLICY_ID = POLICY_X_STATE.POLICY_ID ");
                sbSQLWhere.Append(" AND POLICY_X_STATE.STATE_ID =" + iJurisdictionId + " ");

                if (p_bIncludePolicyID)
                    sbSQLWhere.Append(" AND POLICY.POLICY_ID =" + p_iPolicyId);

                //sSQLOrder = " ORDER BY POLICY_NUMBER ";       //Commented by Sumit Agarwal as we are only selecting the Policy_Id: 10/01/2014: MITS 33588
            }





            else
            {
                sbSQLSelect = new StringBuilder();
                sbSQLWhere = new StringBuilder();
                //Start: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588
                //sbSQLSelect.Append("SELECT DISTINCT(POLICY.POLICY_ID),POLICY_NUMBER,POLICY_X_CVG_TYPE.NEXT_POLICY_ID,POLICY_NAME,EFFECTIVE_DATE,EXPIRATION_DATE,OCCURRENCE_LIMIT,POLICY_LIMIT,TOTAL_PAYMENTS,ENTITY.LAST_NAME");
                sbSQLSelect.Append("SELECT DISTINCT POLICY.POLICY_ID ");
                //End: Change the following code by Sumit Agarwal to only select the Policy_ID as this is only required: 10/01/2014: MITS 33588

                sSQLFrom = " FROM POLICY, POLICY_X_CVG_TYPE, POLICY_X_INSURED, ENTITY ";

                /* R5 PS2 merge
                            sSQLSelect = "SELECT POLICY.POLICY_ID,POLICY_NUMBER,POLICY_X_CVG_TYPE.NEXT_POLICY_ID,POLICY_NAME,EFFECTIVE_DATE,EXPIRATION_DATE,OCCURRENCE_LIMIT,POLICY_LIMIT,TOTAL_PAYMENTS,ENTITY.LAST_NAME";

                            //Gagan Safeway Retrofit Policy Jurisidiction
                            sSQLFrom = " FROM POLICY, POLICY_X_CVG_TYPE, POLICY_X_INSURED, ENTITY,POLICY_X_STATE";*/

                sbSQLWhere.Append(" WHERE (POLICY.POLICY_ID = POLICY_X_INSURED.POLICY_ID");
                sbSQLWhere.Append(" AND POLICY.POLICY_ID = POLICY_X_CVG_TYPE.POLICY_ID AND POLICY.INSURER_EID = ENTITY.ENTITY_ID)");

                sbSQLWhere.Append(" AND (POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE = " + iCoverageTypeCode);
                sbSQLWhere.Append("      AND POLICY_X_INSURED.INSURED_EID IN (" + sGetOrgForDept + "))");

                sbSQLWhere.Append(" AND ((POLICY.POLICY_STATUS_CODE IN (" + iCN_POLICY_IN_EFFECT + "," + iCN_POLICY_EXPIRED + "))");
                sbSQLWhere.Append("      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED + " ");
                sbSQLWhere.Append("          AND POLICY.CANCEL_DATE > '" + objClaim.DateOfClaim + "'");
                sbSQLWhere.Append("          AND POLICY.TRIGGER_CLAIM_FLAG <> 0)");
                sbSQLWhere.Append("      OR (POLICY.POLICY_STATUS_CODE = " + iCN_POLICY_CANCELED);
                sbSQLWhere.Append("          AND POLICY.CANCEL_DATE > '" + objEvent.DateOfEvent + "'");
                sbSQLWhere.Append("          AND POLICY.TRIGGER_CLAIM_FLAG = 0))");
                sbSQLWhere.Append(" AND ((POLICY.EFFECTIVE_DATE <= '" + objEvent.DateOfEvent + "'");
                sbSQLWhere.Append("       AND POLICY.EXPIRATION_DATE >= '" + objEvent.DateOfEvent + "'");
                sbSQLWhere.Append("       AND POLICY.TRIGGER_CLAIM_FLAG = 0)");
                sbSQLWhere.Append("      OR (POLICY.EFFECTIVE_DATE <= '" + objClaim.DateOfClaim + "'");
                sbSQLWhere.Append("          AND POLICY.EXPIRATION_DATE >= '" + objClaim.DateOfClaim + "'");
                sbSQLWhere.Append("          AND POLICY.TRIGGER_CLAIM_FLAG <> 0))");

                sbSQLWhere.Append(" AND PRIMARY_POLICY_FLG <> 0 ");
                /* R5 PS2 merge
                //Gagan Safeway Retrofit Policy Jurisidiction : START - filter for jurisdiction also added in auto select policy
                if (objClaim.Context.InternalSettings.SysSettings.ShowJurisdiction != 0)
                {
                    sSQLWhere = sSQLWhere + " AND POLICY.POLICY_ID = POLICY_X_STATE.POLICY_ID ";
                    sSQLWhere = sSQLWhere + " AND POLICY_X_STATE.STATE_ID =" + iJurisdictionId + " ";
                }
                //Gagan Safeway Retrofit Policy Jurisidiction : END*/
                if (p_bIncludePolicyID)
                    sbSQLWhere.Append(" AND POLICY.POLICY_ID =" + p_iPolicyId);

                //sSQLOrder = " ORDER BY POLICY_NUMBER ";       //Commented by Sumit Agarwal as we are only selecting the Policy_Id: 10/01/2014: MITS 33588
            }
            sSQL = sbSQLSelect.ToString() + sSQLFrom + sbSQLWhere.ToString() + sSQLOrder;
            //End rsushilaggar
            return sSQL;
        }

        private string GetOrgListForDept(int p_iDeptEId)
        {
            //rsushilaggar changes made for Performance Improvement
            StringBuilder sbSQL = new StringBuilder();
            //string sGetOrgListForDept=string.Empty;
            StringBuilder sbGetOrgListForDept = new StringBuilder();

            sbSQL.Append("SELECT ENTITY.ENTITY_ID, E1.ENTITY_ID, E2.ENTITY_ID, E3.ENTITY_ID, E4.ENTITY_ID, E5.ENTITY_ID, E6.ENTITY_ID, E7.ENTITY_ID ");
            sbSQL.Append(" FROM ENTITY, ENTITY E1, ENTITY E2, ENTITY E3, ENTITY E4, ENTITY E5, ENTITY E6, ENTITY E7 ");
            sbSQL.Append(" WHERE ENTITY.ENTITY_ID=" + p_iDeptEId + " AND E1.ENTITY_ID=ENTITY.PARENT_EID AND ");
            sbSQL.Append(" E2.ENTITY_ID=E1.PARENT_EID AND E3.ENTITY_ID=E2.PARENT_EID AND E4.ENTITY_ID=E3.PARENT_EID ");
            sbSQL.Append(" AND E5.ENTITY_ID=E4.PARENT_EID AND E6.ENTITY_ID=E5.PARENT_EID AND E7.ENTITY_ID=E6.PARENT_EID");

            using (DbReader objReader = objClaim.Context.DbConnLookup.ExecuteReader(sbSQL.ToString()))
            {
                if (objReader.Read())
                    sbGetOrgListForDept.Append(p_iDeptEId.ToString() + ",");
                sbGetOrgListForDept.Append(Conversion.ConvertObjToStr(objReader.GetValue(0)) + ",");
                sbGetOrgListForDept.Append(Conversion.ConvertObjToStr(objReader.GetValue(1)) + ",");
                sbGetOrgListForDept.Append(Conversion.ConvertObjToStr(objReader.GetValue(2)) + ",");
                sbGetOrgListForDept.Append(Conversion.ConvertObjToStr(objReader.GetValue(3)) + ",");
                sbGetOrgListForDept.Append(Conversion.ConvertObjToStr(objReader.GetValue(4)) + ",");
                sbGetOrgListForDept.Append(Conversion.ConvertObjToStr(objReader.GetValue(5)) + ",");
                sbGetOrgListForDept.Append(Conversion.ConvertObjToStr(objReader.GetValue(6)) + ",");
                sbGetOrgListForDept.Append(Conversion.ConvertObjToStr(objReader.GetValue(7)));
            }
            //End rushilaggar
            return sbGetOrgListForDept.ToString();

        }

        //Changed by Gagan for MITS 11451 : Start

        /// <summary>
        /// Deletes all auto checks for a claim
        /// </summary>
        /// <param name="p_iClaimID"></param>
        private void DeleteAutoChecks()
        {
            string sSQL;

            sSQL = "DELETE FROM FUNDS_AUTO_SPLIT WHERE AUTO_TRANS_ID IN (SELECT AUTO_TRANS_ID FROM FUNDS_AUTO WHERE CLAIM_ID = " + objClaim.ClaimId.ToString() + ")";
            objClaim.Context.DbConnLookup.ExecuteNonQuery(sSQL);

            sSQL = "DELETE FROM FUNDS_AUTO_BATCH WHERE AUTO_BATCH_ID IN (SELECT AUTO_BATCH_ID FROM FUNDS_AUTO WHERE CLAIM_ID = " + objClaim.ClaimId.ToString() + ")";
            objClaim.Context.DbConnLookup.ExecuteNonQuery(sSQL);

            sSQL = "DELETE FROM FUNDS_AUTO WHERE CLAIM_ID = " + objClaim.ClaimId.ToString();
            objClaim.Context.DbConnLookup.ExecuteNonQuery(sSQL);
        }

        //Changed by Gagan for MITS 11451 : End


        private void CloseAllDiaries()
        {
            SysSettings objSysSettings = null;
            objSysSettings = new SysSettings(objClaim.Context.DbConn.ConnectionString, base.ClientId); //Ash - cloud
            if (bCloseDiaries && sUserCloseDiary == "true")
            {
                objClaim.Context.DbConnLookup.ExecuteNonQuery(
                    String.Format(@"UPDATE WPA_DIARY_ENTRY SET STATUS_OPEN = 0 
					WHERE ATTACH_RECORDID = {0} AND ATTACH_TABLE = 'CLAIM'", objClaim.ClaimId));

                // Deleting all diaries
                //Check and close Diaries
                // sUserCloseDiary applied by Rahul 6th Feb 2006.
                if (objSysSettings.DeleteAllClaimDiaries != 0)
                {
                    int iCount = 0;
                    Event objEvent = null;
                    int i = 0;

                    string sSQL = "SELECT TRANS_ID FROM FUNDS WHERE CLAIM_ID=" + objClaim.ClaimId;

                    using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                    {
                        while (objReader.Read())
                        {
                            //sClaimDetails[i,0]=objReader.GetValue("TRANS_ID").ToString();
                            //sClaimDetails[i,1]="FUNDS";
                            iCount++;
                        }
                    }

                    iCount = iCount + objClaim.AdjusterList.Count;
                    iCount = iCount + objClaim.ClaimantList.Count;
                    iCount = iCount + objClaim.DefendantList.Count;
                    iCount = iCount + objClaim.LitigationList.Count;
                    objEvent = (objClaim.Parent as Event);
                    iCount = iCount + objEvent.PiList.Count;

                    string[,] sClaimDetails = new string[iCount, 2];

                    foreach (ClaimAdjuster objAdjuster in objClaim.AdjusterList)
                    {
                        sClaimDetails[i, 0] = objAdjuster.AdjRowId.ToString();
                        sClaimDetails[i, 1] = "ADJUSTER";
                        i++;
                    }
                    foreach (Claimant objClaimant in objClaim.ClaimantList)
                    {
                        sClaimDetails[i, 0] = objClaimant.ClaimantEid.ToString();
                        sClaimDetails[i, 1] = "CLAIMANT";
                        i++;
                    }
                    foreach (Defendant objDefendant in objClaim.DefendantList)
                    {
                        sClaimDetails[i, 0] = objDefendant.AttorneyEid.ToString();
                        sClaimDetails[i, 1] = "DEFENDANT";
                        i++;
                    }
                    foreach (ClaimXLitigation objLitigation in objClaim.LitigationList)
                    {
                        sClaimDetails[i, 0] = objLitigation.JudgeEid.ToString();
                        sClaimDetails[i, 1] = "LITIGATION";
                        i++;
                    }
                    foreach (PersonInvolved objPersons in objEvent.PiList)
                    {
                        int piTypeCode = objPersons.PiTypeCode;
                        string sTable = string.Empty;
                        switch (objClaim.Context.LocalCache.GetShortCode(piTypeCode))
                        {
                            case "E":
                                sTable = "PiEmployee";
                                break;
                            case "MED":
                                sTable = "PiMedicalStaff";
                                break;
                            case "O":
                                sTable = "PiOther";
                                break;
                            case "P":
                                sTable = "PiPatient";
                                break;
                            case "PHYS":
                                sTable = "PiPhysician";
                                break;
                            case "W":
                                sTable = "PiWitness";
                                break;
                        }
                        sTable = sTable.ToUpper();
                        sClaimDetails[i, 0] = objPersons.PiRowId.ToString();
                        sClaimDetails[i, 1] = sTable;
                        i++;
                    }

                    using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                    {
                        while (objReader.Read())
                        {
                            sClaimDetails[i, 0] = objReader.GetValue("TRANS_ID").ToString();
                            sClaimDetails[i, 1] = "FUNDS";
                            i++;
                        }
                    }

                    for (int j = 0; j < iCount; j++)
                    {
                        objClaim.Context.DbConnLookup.ExecuteNonQuery(
                            String.Format(@"UPDATE WPA_DIARY_ENTRY SET STATUS_OPEN = 0 
					WHERE ATTACH_RECORDID = {0} AND ATTACH_TABLE = '{1}'", sClaimDetails[j, 0], sClaimDetails[j, 1]));
                    }

                }
            }
            bCloseDiaries = false;
        }

        private void GetFilteredListForClaimType()
        {
            string sQuery = null;
            DbReader objRdr = null;

            ArrayList iCodeArray = new ArrayList();

            try
            {
                sQuery = "SELECT CODE2 FROM CODE_X_CODE WHERE  CODE1 = " + objClaim.ClaimTypeCode + " AND DELETED_FLAG = 0";
                objRdr = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sQuery);
                while (objRdr.Read())
                {
                    iCodeArray.Add(Conversion.ConvertStrToInteger(objRdr.GetValue("CODE2").ToString()));


                }

                if (!iCodeArray.Contains(objClaim.Context.LocalCache.GetCodeId("LL", "LOSS_COMPONENT")))
                {
                    base.AddKillNode("btnLiabilityloss");
                }


            }


            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objRdr != null)
                {
                    objRdr.Close();
                    objRdr.Dispose();
                }
            }
        }

        //Added by Amitosh for adding External Policy Entities and Units with the claim.
        //private void AddExternalPolicyEntities()
        //{
        //    string sSQL = string.Empty;
        //    string sInsuredIds = string.Empty;
        //    XmlNode objDownloadedIds = null;
        //    string sDownloadedEntitiesIds = string.Empty;

        //    foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
        //    {
        //        Policy objPolicy = (Policy)objClaim.Context.Factory.GetDataModelObject("Policy", false);
        //        objPolicy.MoveTo(objClaimXPolicy.PolicyId);
        //        if (objPolicy.PolicySystemId > 0)
        //        {
        //            objDownloadedIds = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysExData/DownloadedEntitiesIds");
        //            if (objDownloadedIds != null)
        //            {
        //                foreach (string strIds in objDownloadedIds.InnerText.Split(';'))
        //                {
        //                    if (sDownloadedEntitiesIds.Equals(string.Empty))
        //                        sDownloadedEntitiesIds = strIds.Remove(0, strIds.IndexOf('|') + 1);
        //                    else
        //                        sDownloadedEntitiesIds = sDownloadedEntitiesIds + "," + strIds.Remove(0, strIds.IndexOf('|') + 1);
        //                }
        //            }
        //            sSQL = "SELECT INSURED_EID FROM POLICY_X_INSURED WHERE POLICY_ID= " + objPolicy.PolicyId + " AND INSURED_EID IN(" + sDownloadedEntitiesIds + ")";

        //            using (DbReader oReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
        //            {
        //                while (oReader.Read())
        //                {
        //                    if (string.IsNullOrEmpty(sInsuredIds))
        //                        sInsuredIds = Conversion.ConvertObjToStr(oReader.GetValue("INSURED_EID"));
        //                    else
        //                        sInsuredIds = sInsuredIds + "," + Conversion.ConvertObjToStr(oReader.GetValue("INSURED_EID"));
        //                }
        //            }

        //            if (!string.IsNullOrEmpty(sInsuredIds))
        //            {
        //                foreach (string sEntityId in sInsuredIds.Split(','))
        //                {
        //                    CreateSubTypeEntity(sEntityId);
        //                }
        //            }
        //        }

        //        objPolicy.Dispose();

        //    }
        //}

        private void AddExternalPolicyEntities()
        {

            foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
            {
                Policy objPolicy = (Policy)objClaim.Context.Factory.GetDataModelObject("Policy", false);
                objPolicy.MoveTo(objClaimXPolicy.PolicyId);

                // removed the external Policy system id check: MITS 32702
                foreach (PolicyXUnit objPolicXUnit in objPolicy.PolicyXUnitList)
                {
                    //Start By Ankit on 31_Oct_2012 : Policy Intrface - Commented as Vehicle("V") and Property("P") is not part of Worker Compensation Claim
                    //if (string.Equals(objPolicXUnit.UnitType, "V", StringComparison.InvariantCultureIgnoreCase))
                    //{
                    //    int iUnitRowId = 0;
                    //    using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, "SELECT UNIT_ROW_ID FROM UNIT_X_CLAIM WHERE CLAIM_ID = " + objClaim.ClaimId + " AND UNIT_ID = " + objPolicXUnit.UnitId))
                    //    {
                    //        if (objReader.Read())
                    //            iUnitRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                    //    }

                    //    if (iUnitRowId == 0)
                    //    {
                    //        UnitXClaim objUnitXClaim = (UnitXClaim)objClaim.Context.Factory.GetDataModelObject("UnitXClaim", false);
                    //        Vehicle objVehicle = (Vehicle)objClaim.Context.Factory.GetDataModelObject("Vehicle", false);
                    //        //if (iUnitRowId > 0)
                    //        //    objUnitXClaim.MoveTo(iUnitRowId);

                    //        objVehicle.MoveTo(objPolicXUnit.UnitId);

                    //        objUnitXClaim.ClaimId = objClaim.ClaimId;
                    //        objUnitXClaim.UnitId = objVehicle.UnitId;
                    //        objUnitXClaim.Vin = objVehicle.Vin;
                    //        objUnitXClaim.VehicleMake = objVehicle.VehicleMake;
                    //        objUnitXClaim.VehicleYear = objVehicle.VehicleYear;
                    //        objUnitXClaim.StateRowId = objVehicle.StateRowId;
                    //        objUnitXClaim.HomeDeptEid = objVehicle.HomeDeptEid;
                    //        objUnitXClaim.LicenseNumber = objVehicle.LicenseNumber;
                    //        objUnitXClaim.UnitTypeCode = objVehicle.UnitTypeCode;
                    //        objUnitXClaim.Save();

                    //        objVehicle.Dispose();
                    //        objUnitXClaim.Dispose();
                    //    }

                    //}
                    //else if (string.Equals(objPolicXUnit.UnitType, "P", StringComparison.InvariantCultureIgnoreCase))
                    //{

                    //    int iUnitRowId = 0;
                    //    using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, "SELECT ROW_ID FROM CLAIM_X_PROPERTYLOSS WHERE CLAIM_ID = " + objClaim.ClaimId + " AND PROPERTY_ID = " + objPolicXUnit.UnitId))
                    //    {
                    //        if (objReader.Read())
                    //            iUnitRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);

                    //    }
                    //    if (iUnitRowId == 0)
                    //    {

                    //        ClaimXPropertyLoss objClaimXPropertyLoss = (ClaimXPropertyLoss)objClaim.Context.Factory.GetDataModelObject("ClaimXPropertyLoss", false);
                    //        //if (iUnitRowId > 0)
                    //        //    objClaimXPropertyLoss.MoveTo(iUnitRowId);

                    //        objClaimXPropertyLoss.ClaimId = objClaim.ClaimId;
                    //        objClaimXPropertyLoss.PropertyID = objPolicXUnit.UnitId;
                    //        objClaimXPropertyLoss.Save();

                    //        objClaimXPropertyLoss.Dispose();
                    //    }
                    //}
                    //End By Ankit on 31_Oct_2012 : Policy Intrface - Commented as Vehicle("V") and Property("P") is not part of Worker Compensation Claim
                    if (string.Equals(objPolicXUnit.UnitType, "S", StringComparison.InvariantCultureIgnoreCase))
                    {

                        int iUnitRowId = 0;
                        using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, "SELECT ROW_ID FROM CLAIM_X_SITELOSS WHERE CLAIM_ID = " + objClaim.ClaimId + " AND SITE_ID = " + objPolicXUnit.UnitId))
                        {
                            if (objReader.Read())
                                iUnitRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);

                        }
                        if (iUnitRowId == 0)
                        {

                            ClaimXSiteLoss objClaimXSiteLoss = (ClaimXSiteLoss)objClaim.Context.Factory.GetDataModelObject("ClaimXSiteLoss", false);
                            //if (iUnitRowId > 0)
                            //    objClaimXPropertyLoss.MoveTo(iUnitRowId);

                            objClaimXSiteLoss.ClaimId = objClaim.ClaimId;
                            objClaimXSiteLoss.SiteId = objPolicXUnit.UnitId;
                            //Start : Ankit on 31_Oct_2012 : Policy Interface Changes
                            objClaimXSiteLoss.Insured = true;
                            //End : Ankit
                            objClaimXSiteLoss.Save();

                            objClaimXSiteLoss.Dispose();
                        }
                    }
                    //changed the if-else clause for MITS 32702
                    else if (string.Equals(objPolicXUnit.UnitType, "SU", StringComparison.InvariantCultureIgnoreCase))
                    {

                        int iUnitRowId = 0;
                        using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, "SELECT ROW_ID FROM CLAIM_X_OTHERUNIT WHERE CLAIM_ID = " + objClaim.ClaimId + " AND OTHER_UNIT_ID = " + objPolicXUnit.UnitId))
                        {
                            if (objReader.Read())
                                iUnitRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);

                        }
                        if (iUnitRowId == 0)
                        {

                            ClaimXOtherUnit objClaimXOtherUnit = (ClaimXOtherUnit)objClaim.Context.Factory.GetDataModelObject("ClaimXOtherUnit", false);
                            //if (iUnitRowId > 0)
                            //    objClaimXPropertyLoss.MoveTo(iUnitRowId);

                            objClaimXOtherUnit.ClaimId = objClaim.ClaimId;
                            objClaimXOtherUnit.OtherUnitId = objPolicXUnit.UnitId;
                            //Start : Ankit on 31_Oct_2012 : Policy Interface Changes
                            objClaimXOtherUnit.Insured = true;
                            //End : Ankit
                            objClaimXOtherUnit.Save();

                            objClaimXOtherUnit.Dispose();
                        }
                    }
                }

                foreach (PolicyXEntity objPolicyEntity in objPolicy.PolicyXEntityList)
                {
                        CreateSubTypeEntity(objPolicyEntity.EntityId.ToString(), objPolicyEntity.PolicyUnitRowid, objPolicyEntity.TypeCode, objPolicyEntity.PolicyId); //Added objPolicyEntity.TypeCode forRMA:7909 by Payal
                }

            }
            //aaggarwal29: MITS 32102 start
            UpdateInsuredFlagForDuplicateClaimUnits(objClaim.ClaimId.ToString());
            //aaggarwal29: MITS 32102 end

            //string sSQL = string.Empty;
            //string sInsuredIds = string.Empty;
            //XmlNode objDownloadedIds = null;
            //string sDownloadedEntitiesIds = string.Empty;
            //string sDownloadedVehicleIds = string.Empty;
            //string sDownloadedPropertyIds = string.Empty;

            //objDownloadedIds = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysExData/DownloadedVehicleIds");
            //if (objDownloadedIds != null)
            //{
            //    sDownloadedVehicleIds = objDownloadedIds.InnerText;
            //}

            //objDownloadedIds = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysExData/DownloadedPropertyIds");
            //if (objDownloadedIds != null)
            //{
            //    sDownloadedPropertyIds = objDownloadedIds.InnerText;
            //}

            //foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
            //{
            //    Policy objPolicy = (Policy)objClaim.Context.Factory.GetDataModelObject("Policy", false);
            //    objPolicy.MoveTo(objClaimXPolicy.PolicyId);
            //    if (objPolicy.PolicySystemId > 0)
            //    {
            //        foreach (PolicyXUnit objPolicXUnit in objPolicy.PolicyXUnitList)
            //        {
            //            if (string.Equals(objPolicXUnit.UnitType, "V", StringComparison.InvariantCultureIgnoreCase))
            //            {
            //                if (IsDownloadedPolicyObject(objClaimXPolicy.PolicyId, sDownloadedVehicleIds.Split(';'), objPolicXUnit.UnitId.ToString()))
            //                {
            //                    int iUnitRowId = 0;
            //                    using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, "SELECT UNIT_ROW_ID FROM UNIT_X_CLAIM WHERE CLAIM_ID = " + objClaim.ClaimId + " AND UNIT_ID = " + objPolicXUnit.UnitId))
            //                    {
            //                        if (objReader.Read())
            //                            iUnitRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
            //                    }

            //                    UnitXClaim objUnitXClaim = (UnitXClaim)objClaim.Context.Factory.GetDataModelObject("UnitXClaim", false);
            //                    Vehicle objVehicle = (Vehicle)objClaim.Context.Factory.GetDataModelObject("Vehicle", false);
            //                    if (iUnitRowId > 0)
            //                        objUnitXClaim.MoveTo(iUnitRowId);

            //                    objVehicle.MoveTo(objPolicXUnit.UnitId);

            //                    objUnitXClaim.ClaimId = objClaim.ClaimId;
            //                    objUnitXClaim.UnitId = objVehicle.UnitId;
            //                    objUnitXClaim.Vin = objVehicle.Vin;
            //                    objUnitXClaim.VehicleMake = objVehicle.VehicleMake;
            //                    objUnitXClaim.VehicleYear = objVehicle.VehicleYear;
            //                    objUnitXClaim.StateRowId = objVehicle.StateRowId;
            //                    objUnitXClaim.HomeDeptEid = objVehicle.HomeDeptEid;
            //                    objUnitXClaim.LicenseNumber = objVehicle.LicenseNumber;
            //                    objUnitXClaim.UnitTypeCode = objVehicle.UnitTypeCode;
            //                    objUnitXClaim.Save();

            //                    objVehicle.Dispose();
            //                    objUnitXClaim.Dispose();
            //                }
            //            }
            //            else
            //            {
            //                if (IsDownloadedPolicyObject(objClaimXPolicy.PolicyId, sDownloadedPropertyIds.Split(';'), objPolicXUnit.UnitId.ToString()))
            //                {
            //                    int iUnitRowId = 0;
            //                    using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, "SELECT ROW_ID FROM CLAIM_X_PROPERTYLOSS WHERE CLAIM_ID = " + objClaim.ClaimId + " AND PROPERTY_ID = " + objPolicXUnit.UnitId))
            //                    {
            //                        if (objReader.Read())
            //                            iUnitRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);

            //                    }

            //                    ClaimXPropertyLoss objClaimXPropertyLoss = (ClaimXPropertyLoss)objClaim.Context.Factory.GetDataModelObject("ClaimXPropertyLoss", false);
            //                    if (iUnitRowId > 0)
            //                        objClaimXPropertyLoss.MoveTo(iUnitRowId);

            //                    objClaimXPropertyLoss.ClaimId = objClaim.ClaimId;
            //                    objClaimXPropertyLoss.PropertyID = objPolicXUnit.UnitId;
            //                    objClaimXPropertyLoss.Save();

            //                    objClaimXPropertyLoss.Dispose();
            //                }
            //            }
            //        }

            //        objDownloadedIds = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysExData/DownloadedEntitiesIds");
            //        if (objDownloadedIds != null)
            //        {
            //            foreach(string strIds in  objDownloadedIds.InnerText.Split(';'))
            //            {
            //                if (sDownloadedEntitiesIds.Equals(string.Empty))
            //                    sDownloadedEntitiesIds = strIds.Remove(0, strIds.IndexOf('|') + 1);
            //                else
            //                    sDownloadedEntitiesIds = sDownloadedEntitiesIds + "," + strIds.Remove(0, strIds.IndexOf('|') + 1);
            //            }
            //        }
            //        sSQL = "SELECT INSURED_EID FROM POLICY_X_INSURED WHERE POLICY_ID= " + objPolicy.PolicyId + " AND INSURED_EID IN(" + sDownloadedEntitiesIds + ")";

            //        using (DbReader oReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
            //        {
            //            while (oReader.Read())
            //            {
            //                if (string.IsNullOrEmpty(sInsuredIds))
            //                    sInsuredIds = Conversion.ConvertObjToStr(oReader.GetValue("INSURED_EID"));
            //                else
            //                    sInsuredIds = sInsuredIds + "," + Conversion.ConvertObjToStr(oReader.GetValue("INSURED_EID"));
            //            }
            //        }
            //        if (!string.IsNullOrEmpty(sInsuredIds))
            //        {
            //            foreach (string sEntityId in sInsuredIds.Split(','))
            //            {
            //                CreateSubTypeEntity(sEntityId);
            //            }
            //        }
            //    }

            //    objPolicy.Dispose();
            //}
        }

        private string GetValidationFailedMessage(string sFailedPoliciesNames)
        {
            StringBuilder sbMessage = new StringBuilder("</p>Policy validation failed for Policies " + sFailedPoliciesNames + ": Please verify following parameters-<br/>");
            sbMessage.Append("Policy status should be In-Effect <br/>");
            sbMessage.Append("Policy should be primary policy <br/>");
            sbMessage.Append("Date Of Claim/Event should be between Policy effective and expiration date <br/>");

            if (int.Equals(objClaim.Context.InternalSettings.SysSettings.UseMultiCurrency, -1))
                sbMessage.Append("Claim currency should be same as Policy currency");

            sbMessage.Append("</p>");

            return sbMessage.ToString();
        }

        private void CreateSubTypeEntity(string sEntityId, int iPolicyUnitRowId, int iEntityTableId, int iPolicyId) // Added iEntityTableId for RMA:7909 by PAYAL
        {
            string sSQL = string.Empty;
            string entityTableName = string.Empty;
            bool blnSuccess = false;
            int iEntityId = 0;
            Entity objEntity = null;
            Employee objEmployee = null;
            PiEmployee objPiEmployee = null;
            Patient objPatient = null;
            PiPatient objPiPatient = null;
            MedicalStaff objMedicalStaff = null;
            Physician objPhysician = null;
            int iEntityRoleId = 0; //Payal; RMA:7909
            //EntityXRole objentityRole = null; //Payal; RMA:7909
            string sPolicyTable = "POLICY";

            try
            {
                iEntityId = Conversion.CastToType<Int32>(sEntityId, out blnSuccess);
                objEntity = (Entity)objClaim.Context.Factory.GetDataModelObject("Entity", false);
                objEntity.MoveTo(iEntityId);
                entityTableName = objCache.GetTableName(objEntity.EntityTableId);

                switch (entityTableName.ToUpper())
                {
                    case "EMPLOYEES":
                        string sEmpNumber = string.Empty;
                        sSQL = string.Format("SELECT EMPLOYEE_EID, EMPLOYEE_NUMBER FROM EMPLOYEE WHERE EMPLOYEE_EID ={0} ", iEntityId);
                        using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                        {
                            if (objReader.Read())
                            {
                                sEmpNumber = Conversion.ConvertObjToStr(objReader.GetValue(1));
                            }
                        }

                        //    sSQL = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE EVENT_ID = " + objClaim.EventId + " AND PI_EID = " + iEntityId + " AND PI_ER_ROW_ID =" + iEntityRoleId + " AND PI_TYPE_CODE=" + objCache.GetCodeId("E", "PERSON_INV_TYPE"); //Added PI_ER_ROW_ID for RMA:7909 by Payal
                        if (this.objClaim.Context.InternalSettings.SysSettings.UseEntityRole)
                        {
                            sSQL = string.Format("SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PARENT_TABLE_NAME='POLICY'  AND PARENT_ROW_ID = {0} AND PI_EID = {1}  AND ROLE_TABLE_ID = {2}  AND PI_TYPE_CODE= {3}",
                         iPolicyId, iEntityId, iEntityTableId, objCache.GetCodeId("E", "PERSON_INV_TYPE")); // pgupta215 JIRA 11730  
                        }
                        else
                        {
                            sSQL = string.Format("SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PARENT_TABLE_NAME='POLICY'  AND PARENT_ROW_ID = {0} AND PI_EID = {1}  AND PI_TYPE_CODE= {2}",
                        iPolicyId, iEntityId, objCache.GetCodeId("E", "PERSON_INV_TYPE")); // pgupta215 JIRA 11730  
                        }

                        using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                        {
                            objPiEmployee = (PiEmployee)objClaim.Context.Factory.GetDataModelObject("PiEmployee", false);
                            if (objReader.Read())
                            {
                                objPiEmployee.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                            }
                            else
                            {
                                objPiEmployee.EventId = objClaim.EventId;
                                objPiEmployee.PiEid = iEntityId;
                            }
                            if (!string.IsNullOrEmpty(sEmpNumber))
                                objPiEmployee.EmployeeNumber = sEmpNumber;
                            objPiEmployee.PiTypeCode = objCache.GetCodeId("E", "PERSON_INV_TYPE");
                            objPiEmployee.PolicyUnitRowId = iPolicyUnitRowId;
                            // pgupta215 Jira 11736 Entity role enh start
                            objPiEmployee.RoleTableId = (iEntityTableId == 0) ? objCache.GetTableId(Globalization.IndividualGlossaryTableNames.EMPLOYEES.ToString()) : iEntityTableId;
                            objPiEmployee.ParentTableName = sPolicyTable;
                            objPiEmployee.ParentRowId = iPolicyId;
                            // pgupta215 Jira 11736 Entity role enh End
                            //objPiEmployee.PiErRowID = iEntityRoleId;  //Payal; RMA:7909
                            objPiEmployee.Save();
                            objPiEmployee.Dispose();
                        }
                        break;
                    case "PATIENTS":
                        int iPatientId = 0;
                        sSQL = string.Format("SELECT PATIENT_ID FROM PATIENT WHERE PATIENT_EID = {0} ", iEntityId);
                        using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                        {
                            if (objReader.Read())
                            {
                                iPatientId = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                            }

                        }
                        //sSQL = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE EVENT_ID = " + objClaim.EventId + " AND PI_EID = " + iEntityId +  " AND PI_ER_ROW_ID =" + iEntityRoleId + " AND PI_TYPE_CODE=" + objCache.GetCodeId("P", "PERSON_INV_TYPE"); //Added PI_ER_ROW_ID for RMA:7909 by Payal-- commented by prianca2512
                        if (this.objClaim.Context.InternalSettings.SysSettings.UseEntityRole)
                        {
                            sSQL = string.Format("SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PARENT_TABLE_NAME='POLICY'  AND PARENT_ROW_ID = {0} AND PI_EID = {1} AND ROLE_TABLE_ID = {2} AND PI_TYPE_CODE= {3}"
                           , iPolicyId, iEntityId, iEntityTableId, objCache.GetCodeId("P", "PERSON_INV_TYPE")); // pgupta215 JIRA 11736
                        }
                        else
                        {
                            sSQL = string.Format("SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PARENT_TABLE_NAME='POLICY'  AND PARENT_ROW_ID = {0} AND PI_EID = {1} AND PI_TYPE_CODE= {2}"
                            , iPolicyId, iEntityId, objCache.GetCodeId("P", "PERSON_INV_TYPE")); // pgupta215 JIRA 11736
                        }
                        using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                        {
                            objPiPatient = (PiPatient)objClaim.Context.Factory.GetDataModelObject("PiPatient", false);
                            if (objReader.Read())
                            {
                                objPiPatient.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                            }
                            else
                            {
                                objPiPatient.EventId = objClaim.EventId;
                                objPiPatient.PiEid = iEntityId;
                            }
                            objPiPatient.PatientId = iPatientId;
                            objPiPatient.PiTypeCode = objCache.GetCodeId("P", "PERSON_INV_TYPE");
                            objPiPatient.PolicyUnitRowId = iPolicyUnitRowId;
                            //objPiPatient.PiErRowID = iEntityRoleId; //Payal; RMA:7909
                            // pgupta215 Jira 11736 Entity role enh start
                            objPiPatient.RoleTableId = (iEntityTableId == 0) ? objCache.GetTableId(Globalization.IndividualGlossaryTableNames.PATIENTS.ToString()) : iEntityTableId;
                            objPiPatient.ParentTableName = sPolicyTable;
                            objPiPatient.ParentRowId = iPolicyId;
                            // pgupta215 Jira 11736 Entity role enh End
                            objPiPatient.Save();
                            objPiPatient.Dispose();
                        }
                        break;
                    case "MEDICAL_STAFF":
                        CreatePersonInvolvedEntities(iEntityId, "MED", iPolicyUnitRowId, iEntityTableId, iPolicyId); //Added iEntityRoleId for RMA:7909 by Payal
                        break;
                    case "PHYSICIANS":
                        CreatePersonInvolvedEntities(iEntityId, "PHYS", iPolicyUnitRowId, iEntityTableId, iPolicyId); //Added iEntityRoleId for RMA:7909 by Payal
                        break;
                    case "WITNESS":
                        CreatePersonInvolvedEntities(iEntityId, "W", iPolicyUnitRowId, iEntityTableId, iPolicyId); //Added iEntityRoleId for RMA:7909 by Payal
                        break;
                    case "DRIVERS":
                        CreatePersonInvolvedEntities(iEntityId, "D", iPolicyUnitRowId, iEntityTableId, iPolicyId); //Added iEntityRoleId for RMA:7909 by Payal
                        break;
                    default:
                        CreatePersonInvolvedEntities(iEntityId, "O", iPolicyUnitRowId, iEntityTableId, iPolicyId); //Added iEntityRoleId for RMA:7909 by Payal
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objEntity != null)
                    objEntity.Dispose();
                if (objEmployee != null)
                    objEmployee.Dispose();
                if (objPiEmployee != null)
                    objPiEmployee.Dispose();
                if (objPatient != null)
                    objPatient.Dispose();
                if (objPiPatient != null)
                    objPiPatient.Dispose();
                if (objMedicalStaff != null)
                    objMedicalStaff.Dispose();
                if (objPhysician != null)
                    objPhysician.Dispose();

            }
        }

        private void AddExternalPolicyInsureds()
        {
            string sSQL = string.Empty;
            int iInsuredId = 0;
            string sDownloadedEntitiesIds = string.Empty;
            string sClaimantSQL = string.Empty;
            Policy objPolicy = null;
           // Claimant objClaimant = null;
          //  EntityXRole objEntityRole = null; //Payal; RMA:7909
           // Entity objEntity = null; //Payal; RMA:7909
            int iEntityRoleId = 0; //Payal; RMA:7909
            try
            {
                foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
                {
                    objPolicy = (Policy)objClaim.Context.Factory.GetDataModelObject("Policy", false);
                    objPolicy.MoveTo(objClaimXPolicy.PolicyId);
                    if (objPolicy.PolicySystemId > 0)
                    {
                        sSQL = "SELECT INSURED_EID FROM POLICY_X_INSURED WHERE POLICY_ID= " + objPolicy.PolicyId;

                        using (DbReader oReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                        {
                            while (oReader.Read())
                            {

                                iInsuredId = Conversion.ConvertObjToInt(oReader.GetValue("INSURED_EID"), base.ClientId);
                                //Payal; RMA-7909--Starts
                                //objEntity = (Entity)objClaim.Context.Factory.GetDataModelObject("Entity", false);
                                //objEntity.MoveTo(iInsuredId);
                                //if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
                                //{
                                //    iEntityRoleId = objEntity.IsEntityRoleExists(objEntity.EntityXRoleList, objEntity.Context.LocalCache.GetTableId("POLICY_INSURED"));
                                //    if (iEntityRoleId < 0)
                                //    {
                                //        objEntityRole = (EntityXRole)objClaim.Context.Factory.GetDataModelObject("EntityXRole", false);
                                //        objEntityRole.EntityTableId = objEntity.Context.LocalCache.GetTableId("POLICY_INSURED");
                                //        objEntityRole.EntityId = iInsuredId;
                                //        objEntityRole.Save();
                                //        iEntityRoleId = objEntityRole.ERRowId;
                                //    }
                                //}
                                //sClaimantSQL = "SELECT CLAIMANT_ROW_ID FROM CLAIMANT WHERE CLAIM_ID= " + objClaim.ClaimId + " AND CLAIMANT_EID =" + iInsuredId;


                                //using (DbReader objRdr = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                                //{
                                //    if (objRdr.Read())
                                //    {
                                //        objClaimant = (Claimant)objClaim.Context.Factory.GetDataModelObject("Claimant", false);
                                //        objClaimant.MoveTo(Conversion.ConvertObjToInt(objRdr.GetValue("CLAIMANT_ROW_ID"), base.ClientId));
                                //        objClaimant.ClaimantEid = iInsuredId;
                                //        objClaimant.ClaimantErRowId = iEntityRoleId;
                                //        objClaimant.Save();
                                //        objClaimant.Dispose();
                                //Payal; RMA-7909--Ends
                                //sClaimantSQL = "SELECT CLAIMANT_ROW_ID FROM CLAIMANT WHERE CLAIM_ID= " + objClaim.ClaimId + " AND CLAIMANT_EID =" + iInsuredId;

                                //using (DbReader objRdr = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                                // {
                                //if (!objRdr.Read())
                                //{
                                //     objClaimant = (Claimant)objClaim.Context.Factory.GetDataModelObject("Claimant", false);
                                //    objClaimant.ClaimantEid = iInsuredId;
                                //    objClaimant.Save();
                                //    objClaimant.Dispose();
                                CreateSubTypeEntity(iInsuredId.ToString(), 0, objClaim.Context.LocalCache.GetTableId("POLICY_INSURED"), objPolicy.PolicyId); //Added objClaim.Context.LocalCache.GetTableId("POLICY_INSURED") for RMA:7909 by Payal--- Pgupta215 JIRa 11736 added PolicyId
                                                                                                                                         //    CreatePersonInvolvedEntities(iInsuredId, "O");
                                                                                                                                         //  }
                                                                                                                                         //}
                            }
                        }
                    }


                    objPolicy.Dispose();

                }
            }
            finally
            {
                if(objPolicy!=null)
                objPolicy.Dispose();
              
            }
            
        }

        private void CreatePersonInvolvedEntities(int iEntityId, string sPiType, int iPolicyUnitRowId, int iEntityRoleId, int iPolicyId) //Added ientityRoleId for RMA:7909 by Payal-- pgupta215 JIRA 11736 added PolicyId
        {
            PersonInvolved objPersonInvloved;
            string sSQL = string.Empty;
            string entityTableName = string.Empty;
            int iPiRowId = 0, iDriverRowId = 0;

            //sSQL = "SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PARENT_TABLE_NAME='POLICY'  AND PARENT_ROW_ID = " + iPolicyId
            //    + " AND PI_EID = " + iEntityId + " AND ROLE_TABLE_ID =" + iEntityRoleId + " AND PI_TYPE_CODE=" + objCache.GetCodeId(sPiType, "PERSON_INV_TYPE"); //Added PI_ER_ROW_ID for RMA:7909 by Payal // commented by pgupta215
            if (this.objClaim.Context.InternalSettings.SysSettings.UseEntityRole) // pgupta215 JIRA 11736 
            {
                sSQL = string.Format("SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PARENT_TABLE_NAME='POLICY'  AND PARENT_ROW_ID = {0}"
                   + " AND PI_EID = {1}  AND ROLE_TABLE_ID ={2}  AND PI_TYPE_CODE= {3}", iPolicyId, iEntityId, iEntityRoleId, objCache.GetCodeId(sPiType, "PERSON_INV_TYPE"));   //pgupta215 Jira 11736 Enity role enh 
            }
            else
            {
                sSQL = string.Format("SELECT PI_ROW_ID FROM PERSON_INVOLVED WHERE PARENT_TABLE_NAME='POLICY'  AND PARENT_ROW_ID = {0}"
                      + " AND PI_EID = {1}   AND PI_TYPE_CODE= {2}", iPolicyId, iEntityId, objCache.GetCodeId(sPiType, "PERSON_INV_TYPE"));   //pgupta215 Jira 11736 Enity role enh 
            }
            using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
            {
                if (objReader.Read())
                {
                    iPiRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                }
            }

            objPersonInvloved = (PersonInvolved)objClaim.Context.Factory.GetDataModelObject("PersonInvolved", false);
            if (iPiRowId > 0)
                objPersonInvloved.MoveTo(iPiRowId);

            //for driver only
            if (sPiType.Trim().ToUpper() == "D" && iPiRowId == 0)
            {
                sSQL = string.Format("SELECT DRIVER_ROW_ID FROM DRIVER WHERE DRIVER_EID = {0}", iEntityId);
                using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                {
                    if (objReader.Read())
                    {
                        iDriverRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                        objPersonInvloved.DriverRowId = iDriverRowId;
                    }
                }
            }

            objPersonInvloved.EventId = objClaim.EventId;
            objPersonInvloved.PiEid = iEntityId;
            objPersonInvloved.PiTypeCode = objCache.GetCodeId(sPiType, "PERSON_INV_TYPE");
            objPersonInvloved.PolicyUnitRowId = iPolicyUnitRowId;
            //pgupta215 Jira 11736 Enity role enh start
            switch (sPiType)
            {
                case "MED":
                    objPersonInvloved.RoleTableId = (iEntityRoleId == 0) ? objCache.GetTableId(Globalization.IndividualGlossaryTableNames.MEDICAL_STAFF.ToString()) : iEntityRoleId;
                    break;
                case "PHYS":
                    objPersonInvloved.RoleTableId = (iEntityRoleId == 0) ? objCache.GetTableId(Globalization.IndividualGlossaryTableNames.PHYSICIANS.ToString()) : iEntityRoleId;
                    break;
                case "W":
                    objPersonInvloved.RoleTableId = (iEntityRoleId == 0) ? objCache.GetTableId(Globalization.IndividualGlossaryTableNames.WITNESS.ToString()) : iEntityRoleId;
                    break;
                case "D":
                    objPersonInvloved.RoleTableId = (iEntityRoleId == 0) ? objCache.GetTableId(Globalization.IndividualGlossaryTableNames.DRIVERS.ToString()) : iEntityRoleId;
                    break;
                default:
                    objPersonInvloved.RoleTableId = (iEntityRoleId == 0) ? objCache.GetTableId(Globalization.IndividualGlossaryTableNames.OTHER_PEOPLE.ToString()) : iEntityRoleId;
                    break;
            }
            objPersonInvloved.ParentTableName = "POLICY";
            objPersonInvloved.ParentRowId = iPolicyId;
            //pgupta215 Jira 11736 Enity role enh end
            objPersonInvloved.Save();
            objPersonInvloved.Dispose();
        }
        //aaggarwal29: MITS 32102 update insured flag to -2  for units not attached to policy but present in claim
        private void UpdateInsuredFlagForDuplicateClaimUnits(string p_sClaimID)
        {
            string sSql = string.Empty;
            string sUnitIdList = string.Empty;
            int iClaimId = Conversion.ConvertStrToInteger(p_sClaimID);

            try
            {
                //check for site unit
                sSql = "SELECT SITE_ID FROM CLAIM_X_SITELOSS WHERE SITE_ID NOT IN ( ";
                sSql = sSql + "SELECT UNIT_ID FROM POLICY_X_UNIT, CLAIM_X_POLICY WHERE CLAIM_ID = " + p_sClaimID + " AND CLAIM_X_POLICY.POLICY_ID = POLICY_X_UNIT.POLICY_ID";
                sSql = sSql + " AND POLICY_X_UNIT.UNIT_TYPE='S' ) AND CLAIM_ID = " + p_sClaimID;
                using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSql))
                {
                    while (objReader.Read())
                    {
                        if (sUnitIdList == string.Empty)
                            sUnitIdList = Conversion.ConvertObjToStr(objReader.GetValue("SITE_ID"));
                        else
                            sUnitIdList = sUnitIdList + "," + Conversion.ConvertObjToStr(objReader.GetValue("SITE_ID"));
                    }
                }
                // update ISINSURED flag to -2 if the unit is not part of policy_x_unit
                if (sUnitIdList.Trim() != string.Empty)
                    UpdateInsuredFlag(iClaimId, sUnitIdList, "CLAIM_X_SITELOSS", "ISINSURED", "SITE_ID");


                // check for other unit(stat unit) 
                sSql = "SELECT OTHER_UNIT_ID FROM CLAIM_X_OTHERUNIT WHERE OTHER_UNIT_ID NOT IN ( ";
                sSql = sSql + "SELECT UNIT_ID FROM POLICY_X_UNIT, CLAIM_X_POLICY WHERE CLAIM_ID = " + p_sClaimID + " AND CLAIM_X_POLICY.POLICY_ID = POLICY_X_UNIT.POLICY_ID";
                sSql = sSql + " AND POLICY_X_UNIT.UNIT_TYPE='SU' ) AND CLAIM_ID = " + p_sClaimID;
                using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSql))
                {
                    while (objReader.Read())
                    {
                        if (sUnitIdList == string.Empty)
                            sUnitIdList = Conversion.ConvertObjToStr(objReader.GetValue("OTHER_UNIT_ID"));
                        else
                            sUnitIdList = sUnitIdList + "," + Conversion.ConvertObjToStr(objReader.GetValue("OTHER_UNIT_ID"));
                    }
                }
                // update ISINSURED flag to -2 if the unit is not part of policy_x_unit
                if (sUnitIdList.Trim() != string.Empty)
                    UpdateInsuredFlag(iClaimId, sUnitIdList, "CLAIM_X_OTHERUNIT", "ISINSURED", "OTHER_UNIT_ID");


            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
        }
        // aaggarwal29 : MITS 32102 end
        private void ApplyFormTitle()
        {
            string sCaption = "";
            int captionLevel = 0;
            string sTmp = "";
            string sDiaryMessage = "";
            Event objEvent = (objClaim.Parent as Event);
            //Handle User Specific Caption Level
            if (!objClaim.IsNew || objClaim.PrimaryClaimant.ClaimantRowId != 0)
            {

                if (objLobSettings.CaptionLevel != 0)
                    captionLevel = objLobSettings.CaptionLevel;
                else
                    captionLevel = 1006;
                //nadim for 15014
                if (captionLevel < 1005 || captionLevel > 1012)
                    captionLevel = 0;

                if (captionLevel != 1012)
                    sCaption = objClaim.Context.InternalSettings.CacheFunctions.GetOrgParent(objEvent.DeptEid, captionLevel, 0, ref captionLevel);//captionLevel discarded
                else
                {
                    objClaim.Context.InternalSettings.CacheFunctions.GetOrgInfo(objEvent.DeptEid, ref sCaption, ref sTmp);
                    sCaption += " - " + sTmp;
                }
                sCaption = sCaption.Substring(0, Math.Min(sCaption.Length, 25));

                if (objClaim.EventId != 0 && objClaim.PrimaryClaimant != null && objClaim.PrimaryClaimant.ClaimantEntity != null)
                {
                    sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * " +
                        objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName() + "]";
                    sDiaryMessage = objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
                }
                else
                {
                    sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * - ]";
                    sDiaryMessage = "";
                }
            }
            //Pass this subtitle value to view (ref'ed from @valuepath).
            if (objClaim.IsNew)
                sCaption = "";
            base.ResetSysExData("SubTitle", sCaption);

            //Raman: Adding SubTitle to modified controls list
            ArrayList singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", sCaption);
            base.m_ModifiedControls.Add(singleRow);

            base.ResetSysExData("DiaryMessage", sDiaryMessage);
            base.ResetSysExData("ClaimNumber", objClaim.ClaimNumber);
        }

        private bool HasOpenDiaries
        {
            get
            {
                //Nikhil Garg		17-Feb-2006
                //check whether open diaries exist for this claim or not
                string sSQL = "SELECT COUNT(*) FROM WPA_DIARY_ENTRY WHERE STATUS_OPEN<>0 AND ATTACH_RECORDID = " + objClaim.ClaimId.ToString() + " AND ATTACH_TABLE = 'CLAIM'";
                return objClaim.Context.DbConn.ExecuteInt(sSQL) > 0;
            }
        }
        // Start -  Added by Nikhil on 07/14/14 to delete deductable details on claim delete
        private void DeleteClaimDeductiblesForDeletedClaim(int ClaimId)
        {
            //To Delete the history of deductibles
            DbFactory.ExecuteNonQuery(objClaim.Context.DbConnLookup.ConnectionString,
                string.Format("DELETE FROM CLAIM_X_POL_DED_HIST WHERE CLM_X_POL_DED_ID IN (SELECT CLM_X_POL_DED_ID FROM CLAIM_X_POL_DED WHERE CLAIM_ID={0})", ClaimId));
            DbFactory.ExecuteNonQuery(objClaim.Context.DbConnLookup.ConnectionString,
                string.Format("DELETE FROM CLAIM_X_POL_DED WHERE CLAIM_ID={0}", ClaimId));
        }
        // End -  Added by Nikhil on 07/14/14 to delete deductable details on claim delete
        // Start -  Added by Nitin goel, for JIRA IDs 7659
        private bool bValidateNewCoverageDwnloadedOnClaim(int iClaimId, int iPolicyId)
        {
            bool bNewCoverage = false;
            int iCountCoverageonClaim = 0;
            int iCountCoverageonClaimXPolDed = 0;

            iCountCoverageonClaim = DbFactory.ExecuteAsType<int>(objClaim.Context.DbConnLookup.ConnectionString,
                string.Format("SELECT COUNT(POLCVG_ROW_ID) FROM POLICY_X_CVG_TYPE PXCT INNER JOIN POLICY_X_UNIT PXU ON PXU.POLICY_UNIT_ROW_ID=PXCT.POLICY_UNIT_ROW_ID INNER JOIN CLAIM_X_POLICY CXP ON CXP.POLICY_ID=PXU.POLICY_ID WHERE PXU.POLICY_ID= {0} AND CXP.CLAIM_ID={1}", iPolicyId, iClaimId));
            iCountCoverageonClaimXPolDed = DbFactory.ExecuteAsType<int>(objClaim.Context.DbConnLookup.ConnectionString,
                string.Format("SELECT COUNT(POLCVG_ROW_ID) FROM CLAIM_X_POL_DED CXPD WHERE CXPD.POLICY_ID= {0} AND CXPD.CLAIM_ID={1}", iPolicyId, iClaimId));

            if (iCountCoverageonClaim > iCountCoverageonClaimXPolDed)
            {
                bNewCoverage = true;
            }
            return bNewCoverage;
        }
        //end: JIRA ID 7659
    }
}
