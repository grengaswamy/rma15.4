using System;

using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Text;
using System.Collections.Generic;
using Riskmaster.Cache;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for EventScreen.
	/// </summary>
	public class QMInitialReviewForm : DataEntryFormBase
	{
		const string CLASS_NAME = "EventQM";

		private EventQM EventQM{get{return objData as EventQM;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
        const string FORM_NAME = "qminitialreview";
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
		
		public override void InitNew()
		{
			base.InitNew();
			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlDocument objXML = base.SysEx;
			XmlNode objEventId=null;
			try{objEventId = objXML.GetElementsByTagName("EventId")[0];}
			catch{};
			
			//Filter by this EventId if Present.
			if(objEventId !=null)
			{
				(objData as INavigation).Filter = "EVENT_ID=" + objEventId.InnerText;
				// Mihika 11/25/2005 Defect no.395 Assigning the value of eventid to parentid
				this.m_ParentId = Conversion.ConvertStrToInteger(objEventId.InnerText);
			}
		}

		public QMInitialReviewForm(FormDataAdaptor fda):base(fda)
		{
			this.m_ClassName = CLASS_NAME;
            sLangCode = Convert.ToString(fda.userLogin.objUser.NlsCode);
		}

		// BSB Hack due to unique 1:1 addition situation where this child is being created on the fly.
		public override void BeforeSave(ref bool Cancel)
		{
			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlDocument objXML = base.SysEx;
			XmlNode objEventId=null;
			try{objEventId = objXML.GetElementsByTagName("EventId")[0];}
			catch{};
			
			//Filter by this EventId if Present.
			if(objEventId !=null)
				if(this.EventQM.EventId ==0)
					this.EventQM.EventId = Conversion.ConvertStrToInteger(objEventId.InnerText);
            
			base.BeforeSave (ref Cancel);
		}

		// Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		// Also handles Extended Screen Elements for which not all UI strings are available direct from 
		// DataModel. 
		public override void OnUpdateForm()
		{          
			base.OnUpdateForm ();
			XmlDocument objXML = base.SysEx;
			
			//*********************
			//Get Derived Fields
			//*********************

			//Give user friendly values to System User Fields.
			string userName ="";
			
			//BSB Noted from RMWorld - EventQM.IrReviewerName is not used - Uid is used instead.
            userName = objCache.GetSystemLoginName(EventQM.IrReviewerUid, objData.Context.RMDatabase.DataSourceId, SecurityDatabase.GetSecurityDsn(base.ClientId), base.ClientId);
			
			XmlNode objNew = objXML.CreateElement("IrReviewerUid");
			XmlNode objOld = objXML.SelectSingleNode("//IrReviewerUid");

			XmlAttribute objAtt = objXML.CreateAttribute("tablename");
			objAtt.Value = "RM_SYS_USERS";
			objNew.Attributes.Append(objAtt);

            objAtt = objXML.CreateAttribute("codeid");
            objAtt.Value = EventQM.IrReviewerUid.ToString();
            objNew.Attributes.Append(objAtt);

			XmlCDataSection objCData = objXML.CreateCDataSection(String.Format("{0}",userName));
			objNew.AppendChild(objCData);

			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);
			base.ResetSysExData("Duration",CalculateDuration());
		}
		private string CalculateDuration()
		{
			TimeSpan objDays;
			string sDuration = string.Empty;
            StringBuilder sbSQL = new StringBuilder();
            DbReader objReader = null;
            Dictionary<string, string> strDictResourceValues = new Dictionary<string, string>();
            string sResKeys="'lblNA','lblDays'";
			Event objEvent = (Event) EventQM.Context.Factory.GetDataModelObject("Event",false);
			objEvent.MoveTo(EventQM.EventId);

            sbSQL = sbSQL.Append(string.Format(@"SELECT LR.RESOURCE_KEY, LR.RESOURCE_VALUE FROM LOCAL_RESOURCE LR
                                                INNER JOIN PAGE_INFO PI ON PI.PAGE_ID=LR.PAGE_ID 
                                            WHERE PI.PAGE_NAME='{0}' AND LR.LANGUAGE_ID={1} AND LR.RESOURCE_KEY IN ({2})", FORM_NAME + ".aspx" ,Convert.ToInt32(sLangCode), sResKeys));
            objReader = DbFactory.ExecuteReader(ConfigurationInfo.GetViewConnectionString(base.ClientId), sbSQL.ToString());
            while (objReader.Read())
            {
                strDictResourceValues.Add(objReader.GetString(0), objReader.GetString(1));
            }
            objReader.Close();
            sbSQL.Clear();

            if (strDictResourceValues.Count == 0) // fall back
            {
                sbSQL = sbSQL.Append(string.Format(@"SELECT LR.RESOURCE_KEY, LR.RESOURCE_VALUE FROM LOCAL_RESOURCE LR
                                                INNER JOIN PAGE_INFO PI ON PI.PAGE_ID=LR.PAGE_ID 
                                            WHERE PI.PAGE_NAME='{0}' AND LR.LANGUAGE_ID={1} AND LR.RESOURCE_KEY IN ({2})", FORM_NAME + ".aspx", Convert.ToInt32(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0]), sResKeys));
                objReader = DbFactory.ExecuteReader(ConfigurationInfo.GetViewConnectionString(base.ClientId), sbSQL.ToString());
                while (objReader.Read())
                {
                    strDictResourceValues.Add(objReader.GetString(0), objReader.GetString(1));
                }
                objReader.Close();
            }

			if(EventQM.IrReviewDate.Trim()!="")
			{
				objDays = Conversion.ToDate(EventQM.IrReviewDate).Subtract(Conversion.ToDate(objEvent.DateOfEvent));
				if(objDays.Days > 1000 || objDays.Days < 0)
					//sDuration = " NA";
                    sDuration = strDictResourceValues["lblNA"];
				else
					//sDuration = objDays.Days.ToString()+" Days";
                    sDuration = objDays.Days.ToString() + strDictResourceValues["lblDays"];
			}
			else
				//sDuration = " NA";
                sDuration = strDictResourceValues["lblNA"];
			return sDuration;
		}

		// Mihika - Defect No.395 - 11/25/2005
		// throwing the proper parent key back on in case the MoveFirst\Last request
		// didn't find any records.
		public override void AfterAction(enumFormActionType eActionType)
		{
			base.AfterAction(eActionType);
			switch(eActionType)
			{
				case enumFormActionType.MoveFirst:
				case enumFormActionType.MoveLast:
				case enumFormActionType.MoveTo:
					if(EventQM.EventId==0) //No record found - treat as new.
						EventQM.EventId = this.m_ParentId;
					break;
			}
		}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;
			Event objEvent = (Event) EventQM.Context.Factory.GetDataModelObject("Event",false);
			objEvent.MoveTo(EventQM.EventId);

			if(EventQM.IrReviewDate.Trim()!="")
			{
				if(objEvent.DateOfEvent.CompareTo(EventQM.IrReviewDate)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.ReviewDateMustBeGreaterThanEqualToEventDate", base.ClientId, sLangCode), Globalization.GetString("Field.ReviewDate", base.ClientId, sLangCode), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
						BusinessAdaptorErrorType.Error);
                    //Mits 35163 by sharishkumar
					bError = true;
				}
			}
			if(EventQM.IrFollowUpDate.Trim()!="")
			{
				if(objEvent.DateOfEvent.CompareTo(EventQM.IrFollowUpDate)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.FollowUpDateMustBeGreaterThanEqualToEventDate", base.ClientId, sLangCode), Globalization.GetString("Field.FollowUpDate", base.ClientId, sLangCode), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
			}

			//Defect No: 002512. Nikhil Garg	Dated: 16-Mar-2006
			//check for IR Review date < PA Review Date
			if(EventQM.IrReviewDate.Trim()!="" && EventQM.PaReviewDate.Trim()!="")
			{
				if(EventQM.IrReviewDate.CompareTo(EventQM.PaReviewDate)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.PhysicianAdvisorReviewDate", base.ClientId, sLangCode), Globalization.GetString("Field.InitialReviewReviewDate", base.ClientId, sLangCode)),
						BusinessAdaptorErrorType.Error);

					bError = true;
					Cancel=bError;
					return;
				}
			}

			if (EventQM.IrReviewDate.Trim()!="" && EventQM.CdReviewDate.Trim()!="")	//check for IR Review date < Cd Review Date
			{
				if(EventQM.IrReviewDate.CompareTo(EventQM.CdReviewDate)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.CommitteeDepartmentReviewDate", base.ClientId, sLangCode), Globalization.GetString("Field.InitialReviewReviewDate", base.ClientId, sLangCode)),
						BusinessAdaptorErrorType.Error);

					bError = true;
					Cancel=bError;
					return;
				}
			}

			if (EventQM.IrReviewDate.Trim()!="" && EventQM.QmReviewDate.Trim()!="")	//check for IR Review date < Qm Review Date
			{
				if(EventQM.IrReviewDate.CompareTo(EventQM.QmReviewDate)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId, sLangCode),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId, sLangCode), Globalization.GetString("Field.QualityManagerReviewDate", base.ClientId, sLangCode), Globalization.GetString("Field.InitialReviewReviewDate", base.ClientId, sLangCode)),
						BusinessAdaptorErrorType.Error);

					bError = true;
					Cancel=bError;
					return;
				}
			}

			Cancel=bError;
		}
	}
}
