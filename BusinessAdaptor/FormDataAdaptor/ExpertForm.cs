using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Expert Screen.
	/// </summary>
	public class ExpertForm : DataEntryFormBase
	{
		const string CLASS_NAME = "Expert";
		const string FILTER_KEY_NAME = "LitigationRowId";

		private Expert objExpert{get{return objData as Expert;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}

		public ExpertForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

        private string m_Caption = string.Empty;
            //nnorouzi; MITS 19478
        public override string GetCaption()
        {
            return base.GetCaption();
        }

		public override void InitNew()
		{
			base.InitNew();

			//set filter for LitigationRowId
			objExpert.LitigationRowId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			if (objExpert.LitigationRowId>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + objExpert.LitigationRowId;
		}
        //Payal starts for RMA:18125
        public override void OnUpdateObject()
        {
            base.OnUpdateObject();
            objExpert.ExpertEntity.Abbreviation = objExpert.ExpertEntity.LastName.Substring(0, 1);
            if (objExpert.ExpertEntity.FirstName!="")
                objExpert.ExpertEntity.Abbreviation += objExpert.ExpertEntity.FirstName.Substring(0, 1);
        }
        //Payal ends for RMA:18125

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();

            //Geeta 08/12/09 : Mits 14081 Start
            int iLitigationRowId = objExpert.LitigationRowId;
            ClaimXLitigation oLitigation = (ClaimXLitigation)objExpert.Context.Factory.GetDataModelObject("ClaimXLitigation", false);
            oLitigation.MoveTo(iLitigationRowId);
            Claim objClaimTemp = (Claim)objExpert.Context.Factory.GetDataModelObject("Claim", false);
            objClaimTemp.MoveTo(oLitigation.ClaimId);

            switch (objClaimTemp.LineOfBusCode)
            {
                case 241:
                    m_SecurityId = RMO_GC_LITIGATION_EXPERT ;
                    break;
                case 242:
                    m_SecurityId = RMO_VA_LITIGATION_EXPERT;
                    break;
                case 243:
                    m_SecurityId = RMO_WC_LITIGATION_EXPERT;
                    break;
                case 844:
                    m_SecurityId = RMO_DI_LITIGATION_EXPERT;
                    break;
                //Start-Mridul Bansal. 01/04/10. MITS#19314. Support for Property Claims.
                case 845:
                    m_SecurityId = RMO_PC_LITIGATION_EXPERT;
                    break;
                //End-Mridul Bansal. 01/04/10. MITS#19314. Support for Property Claims.
            }
            
            //Setting Security Id 
            XmlElement objSysSid = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysSid");
            objSysSid.InnerText = m_SecurityId.ToString();                  

            if (objExpert.ExpertRowId == 0)
            {
                if (!objExpert.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_CREATE))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_CREATE);
                    return;
                }
            }
            else
            {                            
                if (!objClaimTemp.Context.RMUser.IsAllowedEx(m_SecurityId, RMO_VIEW))
                {
                    m_CurrentAction = enumFormActionType.None;
                    LogSecurityError(RMO_VIEW);
                    return;
                }
            }

            //Geeta 08/12/09 : Mits 14081 End


            //nnorouzi; MITS 19478
            m_Caption = this.GetCaption();
            base.ResetSysExData("SubTitle", m_Caption);
            ArrayList singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", m_Caption);
            base.m_ModifiedControls.Add(singleRow);

			//Check for People Maintenance permission
			PeoplePermissionChecks4Other("expert", m_SecurityId+RMO_UPDATE);
		}
	}
}