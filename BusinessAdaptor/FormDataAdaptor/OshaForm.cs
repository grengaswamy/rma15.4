using System.Xml;
using Riskmaster.Common;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Event Osha Screen.
	/// </summary>
	public class OshaForm : DataEntryFormBase
	{
		const string CLASS_NAME = "EventXOsha";
		const string FILTER_KEY_NAME = "EventId";
		private const int RMO_OSHA_301 = 31;
		private const int RMO_OSHA_WIZARD = 32;
		
		private int m_preNavKeyValue = 0;

		private EventXOsha EventXOsha{get{return objData as EventXOsha;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		
		/// <summary>
		/// Class constructor which initializes Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public OshaForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();
            //If condition added by Shivendu for MITS 11679
           if (base.m_fda.SafeFormVariableParamText("SysCmd") != "0" || base.m_fda.SafeParamText("SysFormId") == "0")
            {
                //set filter for EventId
                EventXOsha.EventId = base.GetSysExDataNodeInt(FILTER_KEY_NAME, true);
                if (EventXOsha.EventId > 0)
                    (objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + EventXOsha.EventId;
            }

		}//end method InitNew()
		public override void Init()
		{
			base.Init ();
			//Update the Instance Document with the necessary SysExData
			base.CreateSysExData("EventNumber");
			base.CreateSysExData("ClaimNumber");
			base.CreateSysExData("ClaimId");

		}

		// BSB Hack because navigation call into DataModel to move to a record is stubbed if
		// the keyvalue to target is the same as the current one.
		// Only a problem where the key field is shared with a 1:1 parent.
		public override void BeforeAction(enumFormActionType eActionType, ref bool Cancel)
		{
			base.BeforeAction (eActionType, ref Cancel);
			switch (this.CurrentAction)
			{
				case enumFormActionType.MoveFirst:
				case enumFormActionType.MoveLast:
				case enumFormActionType.MoveNext:
				case enumFormActionType.MovePrevious:
				case enumFormActionType.MoveTo:
				{
					this.m_preNavKeyValue = this.objData.KeyFieldValue;
					this.objData.KeyFieldValue = 0;
					break;
				}
			}
		}
		// BSB Hack because navigation call into DataModel to move to a record is stubbed if
		// the keyvalue to target is the same as the current one.
		// Only a problem where the key field is shared with a 1:1 parent.
		public override void AfterAction(enumFormActionType eActionType)
		{
			base.AfterAction(eActionType);
			switch (this.CurrentAction)
			{
				case enumFormActionType.MoveFirst:
				case enumFormActionType.MoveLast:
				case enumFormActionType.MoveNext:
				case enumFormActionType.MovePrevious:
				case enumFormActionType.MoveTo:
				{
					//If navigation not accomplished, just leave the old key field on the current record.
					if(this.objData.KeyFieldValue==0)
						this.objData.KeyFieldValue = this.m_preNavKeyValue;
					
					this.m_preNavKeyValue=0;
					break;
				}
			}

		}

		
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
			if( EventXOsha.RecordableFlag )
			{
				base.ResetSysExData( "IsOshaCreated" , "true" );
			}
			else
			{
				base.ResetSysExData( "IsOshaCreated" , "false" );
			}
			
			//TR #2299,2303 Pankaj 02/28/06 OSHA security
			if(base.ModuleSecurityEnabled)//Is Security Enabled
			{
				if(!this.objData.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_OSHA_301))
					base.AddKillNode("Print301");

				if(!this.objData.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_OSHA_WIZARD))
					base.AddKillNode("OSHARecordabilityWizard2");
			}
		}
//		/// <summary>
//		/// Overrides the base class OnUpdateForm method to handle EventNumber in the 
//		/// instance data for ref binding.  
//		/// </summary>
//		/// <remarks>This instance data is specifically held in the SysExData section
//		/// of the Instance document.
//		/// </remarks>
//		/// <example> 
//		///		<control name="eventnumber" tabindex="2" type="id" ref="Instance/UI/FormVariables/SysExData/EventNumber"/>
//		/// </example>
//		public override void OnUpdateForm()
//		{
//			
//			base.OnUpdateForm ();
//
////			//Update the Instance Document with the necessary SysExData
////			base.CreateSysExData("EventNumber");
////			base.CreateSysExData("ClaimNumber");
////			base.CreateSysExData("ClaimId");
//
//			
////			//Determine if the record exists
////			if (EventXOsha.KeyFieldValue != 0)
////			{
////				//Load the Parent object to make the parent object's attributes available
////				EventXOsha.LoadParent();
////				
////				//Replace the empty EventNumber node with the EventNumber value
////				base.ResetSysExData("EventNumber", (EventXOsha.Parent as Event).EventNumber);
////			}//end if statement
//
//
//		}//end method OnUpdateForm()

//		// BSB Hack due to unique 1:1 addition situation where this child is being created on the fly.
//		public override void BeforeSave(ref bool Cancel)
//		{
//			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
//			//originally from ScreenFlow\SysExData.
//			XmlDocument objXML = base.SysEx;
//			XmlNode objEventId=null;
//			try{objEventId = objXML.GetElementsByTagName("EventId")[0];}
//			catch{};
//			
//			//Filter by this EventId if Present.
//			if(objEventId !=null)
//				if(this.EventXOsha.EventId ==0)
//					this.EventXOsha.EventId = Conversion.ConvertStrToInteger(objEventId.InnerText);
//
//			base.BeforeSave (ref Cancel);
//
//		}
	}
}
