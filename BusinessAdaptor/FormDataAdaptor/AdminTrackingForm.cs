using System;
using System.Collections;
using Riskmaster.DataModel;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using Riskmaster.Db; //pmittal5
namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Class implements common screen operations. Any extended operation  
	/// can be done in specific derived screen classes.                       
	/// Author: Brian Battah, 04/28/2005             
	/// </summary>
	public class AdminTrackingForm : DataEntryFormBase
	{
		private string m_TableName="";
		const string CLASS_NAME = "ADMTable";
        bool newflag = false; //zmohammad MITs 33695

		public AdminTrackingForm(FormDataAdaptor fda):base(fda)
		{
			//Stash "Parent" Reference.
			m_fda = fda;
			m_ClassName = CLASS_NAME;
		}

		public override void InitNew()
		{
			m_objData = this.m_fda.Factory.GetDataModelObject("ADMTable",false) as  ADMTable;
            
            //MITS 12028 Enable this object to call custom script
            ((DataRoot)m_objData).FiringScriptFlag = 2; 
			
            objAdm.TableName = m_TableName;
            
            //Parijat: Mits 11785
            if (m_fda.SafeFormVariableParamText("SysCmd") == "6")
            {
                if (m_fda.SafeFormVariableParam("SysFormId") != null && m_fda.SafeFormVariableParam("SysFormId").InnerText!="")
                {
                    //objAdm.KeyValue = Convert.ToInt32(m_fda.SafeFormVariableParam("SysFormId").Attributes.GetNamedItem("RECORD_ID").Value);
                    objAdm.KeyValue = Convert.ToInt32(m_fda.SafeFormVariableParam("SysFormId").InnerText);
                }
                else if (m_fda.SafeFormVariableParam("SysFormId").Attributes.Count != 0)
                {
                    objAdm.KeyValue = Convert.ToInt32(m_fda.SafeFormVariableParam("SysFormId").Attributes.GetNamedItem("RECORD_ID").Value);
                }
            }
		}
		public override void Init()
		{
			string sFormName = m_fda.SafeFormVariableParamText("SysFormName");
			if(sFormName.IndexOf('|')>=0)
				sFormName=sFormName.Split('|')[1];
			this.m_TableName = sFormName;
			InitNew();
		}
		internal ADMTable objAdm{get{return m_objData as ADMTable;}}

        //zmohammad MITs 33695 : Overriding Addnew() for Admin tracking specifically
        public override bool AddNew()
        {
            bool bCancel = false;
            try
            {
                m_CurrentAction = enumFormActionType.AddNew;


                BeforeAddNew(ref bCancel);
                if (!bCancel)
                    BeforeAction(m_CurrentAction, ref bCancel);

                if (bCancel)
                {
                    this.Cancelled = true;
                    m_CurrentAction = enumFormActionType.None;
                    return false;
                }

                ((DataRoot)m_objData).InitializeScriptData();

                AfterAddNew();
                AfterAction(m_CurrentAction);
                newflag = true;
                UpdateForm();
            }
            catch (Exception e)
            {
                Errors.Add(e, Globalization.GetString("FormDataAdaptor.ScreenBase.AddNew.Exception",base.ClientId), Common.BusinessAdaptorErrorType.Error);
                m_CurrentAction = enumFormActionType.None;
                return false;
            }

            m_CurrentAction = enumFormActionType.None;
            return true;
        }

		override public void UpdateObject()
		{
			try
			{
//				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_UPDATE))
//				{
//					LogSecurityError(RMO_UPDATE);
//					return;
//				}
				OnUpdateObject();
				m_DataChanged |= m_DataChanged || objAdm.AnyDataChanged;
			}
			catch(Exception e)
			{
				Errors.Add(e,Globalization.GetString("FormDataAdaptor.ScreenBase.UpdateObject.Exception",base.ClientId),Common.BusinessAdaptorErrorType.Error);
				return;
			}
		}		

		override public void UpdateForm()
		{
			try
			{
//				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_VIEW))
//				{
//					LogSecurityError(RMO_VIEW);
//					return;
//				}

				//tr#479 Powerview
                //bool bIsPowerView = (Conversion.ConvertStrToInteger(((XmlElement)SysView.SelectSingleNode("//form")).GetAttribute("viewid"))!=0);
				

                ////Send back a view section suitable to present the ADM table's data fields.
                //XmlElement objElem = SysViewSection.SelectSingleNode(@"//section[@name='admdata']") as XmlElement;
                //if(objElem == null)
                //{
                //    objElem = SysViewSection.CreateElement("section");
                //    objElem.SetAttribute("name","admdata");
                //    objElem.SetAttribute("selected","1");
                //    SysViewSection.DocumentElement.AppendChild(objElem);
                //}
                ////tr#479 Powerview
                //if(objAdm != null)
                //    if(!bIsPowerView)
                //        objElem.InnerXml = objAdm.ViewXml.OuterXml;
				

				//TODO:  Also send enough meta-data to populate necessary hidden FormVariables.
				//
				XmlDocument objXML = base.SysEx;
				XmlNode objNew = objXML.CreateElement("AdmTableName");
				XmlNode objOld = objXML.SelectSingleNode("//AdmTableName");
				
				XmlCDataSection objCData = objXML.CreateCDataSection(objAdm.TableName);
				objNew.AppendChild(objCData);
				
				if(objOld !=null)
					objXML.DocumentElement.ReplaceChild(objNew,objOld);
				else
					objXML.DocumentElement.AppendChild(objNew);
                //pmittal5 Mits 20679 07/08/2010 - Display Grid data in Records Summary window
                if (SysView.SelectSingleNode("//form") != null)
                {
                    string sSql = string.Empty;
                    int iColCount = 0;
                    string sFieldName = string.Empty;
                    int iFieldId = 0;
                    XmlAttribute objAtt = null;
                    XmlDocument objGridXml = null;
                    XmlElement objXMLElem = null;
                    XmlElement objXMLTitleElement = null;
                    XmlElement objXMLRowElement = null;
                    XmlElement objXMLCellElement = null;
                    XmlNode objXMLFieldsNode = null;
                    XmlNode objXMLRowNode = null;
                    XmlNode objXMLRowsNode = null;
                    bool bRow = false;
                    bool flag = true;

                    //SysViewSection.InnerXml = SysView.OuterXml;
                    XmlNodeList objGridNodeList = SysView.SelectNodes(@"//control[@type='ZapatecGrid']");
                    string sPath = RMConfigurator.AppFilesPath + "/Supp/columns.xml";
                    foreach (XmlNode objGridNode in objGridNodeList)
                    {
                        sFieldName = objGridNode.Attributes["name"].Value;
                        sSql = "SELECT FIELD_ID FROM SUPP_DICTIONARY WHERE SYS_FIELD_NAME = '" + sFieldName + "'";
                        using(DbReader objRdr = DbFactory.GetDbReader(this.objAdm.Context.DbConn.ConnectionString, sSql))
                        {
                            if(objRdr.Read())
                            {
                                iFieldId = objRdr.GetInt(0);
                            }
                        }

                        objAtt = SysView.CreateAttribute("gridXML");
                        objGridXml = new XmlDocument();
                        objGridXml.Load(sPath);

                        sSql = "SELECT * FROM SUPP_GRID_DICT WHERE FIELD_ID = '" + iFieldId + "' AND DELETED_FLAG <> -1 ORDER BY GRID_DICT_ROW_ID ASC";
                        using (DbReader objRdr = DbFactory.GetDbReader(this.objAdm.Context.DbConn.ConnectionString, sSql))
                        {
                            while (objRdr.Read())
                            {
                                iColCount++;
                                objXMLElem = objGridXml.CreateElement("field");
                                objXMLTitleElement = objGridXml.CreateElement("title");
                                objXMLTitleElement.InnerText = objRdr["COL_HEADER"].ToString();
                                objXMLElem.SetAttribute("width", Convert.ToString(objRdr["COL_HEADER"].ToString().Length * 8) + "px");
                                objXMLElem.AppendChild(objXMLTitleElement);
                                objXMLFieldsNode = objGridXml.SelectSingleNode("/grid/table/fields");
                                objXMLFieldsNode.AppendChild(objXMLElem);
                            }
                        }

                        sSql = "SELECT * FROM SUPP_GRID_VALUE WHERE FIELD_ID = '" + iFieldId + "' AND RECORD_ID = '" + objAdm.KeyValue + "' ORDER BY ROW_NUM,COL_NUM";
                        using (DbReader objRdr = DbFactory.GetDbReader(this.objAdm.Context.DbConn.ConnectionString, sSql))
                        {
                            objXMLRowNode = objGridXml.SelectSingleNode("/grid/table/rows/row");
                            objGridXml.SelectSingleNode("/grid/table/rows").RemoveChild(objXMLRowNode);
                            long lRowNoPrev = 0;
                            long lRow = 0;
                            long lCol = 0;
                            long lColNo = 0;
                            objXMLRowElement = objGridXml.CreateElement("row");
                            while (flag)
                            {
                                if (objRdr.Read())
                                {
                                    lCol = Convert.ToInt64(objRdr["COL_NUM"].ToString());
                                    lRow = Convert.ToInt64(objRdr["ROW_NUM"].ToString());
                                    if (lRowNoPrev != lRow)
                                    {
                                        for (int i = Convert.ToInt32(lColNo); i < iColCount; i++)
                                        {
                                            objXMLCellElement = objGridXml.CreateElement("cell");
                                            objXMLCellElement.InnerText = "";
                                            objXMLRowElement.AppendChild(objXMLCellElement);
                                            objXMLRowsNode = objGridXml.SelectSingleNode("/grid/table/rows");
                                            objXMLRowsNode.AppendChild(objXMLRowElement);
                                            bRow = true;
                                        }
                                        lColNo = 0;
                                        objXMLRowElement = objGridXml.CreateElement("row");
                                    }
                                    objXMLCellElement = objGridXml.CreateElement("cell");
                                    objXMLCellElement.InnerText = objRdr["CELL_VALUE"].ToString();

                                    objXMLRowElement.AppendChild(objXMLCellElement);
                                    objXMLRowsNode = objGridXml.SelectSingleNode("/grid/table/rows");
                                    objXMLRowsNode.AppendChild(objXMLRowElement);
                                    bRow = true;
                                }
                                else
                                {
                                    for (int i = Convert.ToInt32(lColNo); i < iColCount; i++)
                                    {

                                        objXMLCellElement = objGridXml.CreateElement("cell");
                                        objXMLCellElement.InnerText = "";
                                        objXMLRowElement.AppendChild(objXMLCellElement);
                                        objXMLRowsNode = objGridXml.SelectSingleNode("/grid/table/rows");
                                        objXMLRowsNode.AppendChild(objXMLRowElement);
                                        bRow = true;
                                    }
                                    break;
                                }
                                lColNo = lColNo + 1;
                                lRowNoPrev = lRow;
                            }
                        }

                        if (!bRow)
                        {
                            objXMLRowsNode = objGridXml.SelectSingleNode("/grid/table/rows");
                            objXMLRowsNode.AppendChild(objXMLRowElement);

                        }
                        objAtt.Value = objGridXml.InnerXml;
                        objGridNode.Attributes.Append(objAtt);
                    }
                }
                //End - pmittal5
                //zmohammad MITS 33695 Start
                else if (newflag == true)
                {
                    string SQL = "";
                    foreach (SupplementalField objSupp in objAdm)
                    {
                        if (objSupp.FieldType == (SupplementalFieldTypes)17)
                        {

                            SQL = "SELECT * FROM GRID_SYS_PARMS";
                            using (DbReader objRdr = DbFactory.GetDbReader(objAdm.Context.DbConn.ConnectionString, SQL))
                            {
                                while (objRdr.Read())
                                {
                                    objSupp.minRows = objRdr["MIN_ROWS"].ToString();
                                    objSupp.maxRows = objRdr["MAX_ROWS"].ToString();

                                }
                            }
                            string path = RMConfigurator.AppFilesPath + "/Supp/columns.xml";
                            XmlElement objXMLElem = null;
                            XmlNode objXMLFieldsNode = null;
                            XmlElement objXMLTitleElement = null;
                            objSupp.gridXML = new XmlDocument();
                            objSupp.gridXML.Load(path);
                            SQL = string.Concat("SELECT * FROM SUPP_GRID_DICT WHERE FIELD_ID = '"
                                , objSupp.FieldId
                                , "' AND DELETED_FLAG <> -1 ORDER BY GRID_DICT_ROW_ID ASC");
                            using (DbReader objRdr = DbFactory.GetDbReader(objAdm.Context.DbConn.ConnectionString, SQL))
                            {
                                while (objRdr.Read())
                                {
                                    objXMLElem = objSupp.gridXML.CreateElement("field");
                                    objXMLTitleElement = objSupp.gridXML.CreateElement("title");
                                    objXMLTitleElement.InnerText = objRdr["COL_HEADER"].ToString();
                                    objXMLElem.SetAttribute("width", Convert.ToString(objRdr["COL_HEADER"].ToString().Length * 8) + "px");
                                    objXMLElem.AppendChild(objXMLTitleElement);
                                    objXMLFieldsNode = objSupp.gridXML.SelectSingleNode("/grid/table/fields");
                                    objXMLFieldsNode.AppendChild(objXMLElem);
                                }
                            }
                        }
                    }
                }
                //zmohammad MITS 33695 Ends
				OnUpdateForm();

                //MITS 11296 -Abhishek if record count is 0 than add a warining
                if (objAdm.RecordCount == 0 && objAdm.KeyValue != 0)
                {
                    Errors.Add("WarningScriptError", Globalization.GetString("AdminTrackingForm.RecordNotFound.Exception",base.ClientId), Common.BusinessAdaptorErrorType.Warning);
                }
                
                objNew = objXML.CreateElement("ADMRecordCount");
                objCData = objXML.CreateCDataSection(objAdm.RecordCount.ToString());
                objNew.AppendChild(objCData);
                objXML.DocumentElement.AppendChild(objNew);

                // non fatal warning errors for MCIC
                foreach (System.Collections.DictionaryEntry objError in this.objAdm.Context.ScriptWarningErrors)
                    Errors.Add("WarningScriptError", objError.Value.ToString(), Common.BusinessAdaptorErrorType.Warning);

				//BSB Hack to include position information like Record (5 of 10)
				base.ResetSysExData("NavToRecordId",objAdm.KeyValue.ToString());
				//base.ResetSysExData("CurrentRecordCount",objAdm.Context.DbConnLookup.ExecuteInt(String.Format("SELECT COUNT(*) FROM {0} WHERE {1}<{2}",objAdm.TableName,objAdm.KeyName,objAdm.KeyValue)).ToString());

				m_DataChanged = false;

				//gagnihotri MITS 13502 11/05/2008
                //Raman Bhatia - Acrosoft Phase 1 changes.. Document Management Not available for Employee if Acrosoft is enabled
                //bool bUseAcrosoftInterface = objAdm.Context.InternalSettings.SysSettings.UseAcrosoftInterface;
                //if(bUseAcrosoftInterface) base.AddKillNode("attachdocuments");

                //rbhatia4:R8: Use Media View Setting : July 11 2011
                if (objAdm.Context.InternalSettings.SysSettings.UseMediaViewInterface)
                {
                    base.AddKillNode("attachdocuments");
                    base.AddKillNode("attach");
                }

                // akaushik5 Added for MITS 37383 Starts
                this.ImplementSMSPermissions(); 
                // akaushik5 Added for MITS 37383 Ends
			}
			catch(Exception e)
			{
				Errors.Add(e,Globalization.GetString("FormDataAdaptor.ScreenBase.UpdateForm.Exception",base.ClientId),Common.BusinessAdaptorErrorType.Error);
				return;
			}
		}
		override public bool Validate()
		{
			bool bCancel = false;
			bool bRet = true;
			try
			{
				//			UpdateObject();
				if(m_DataChanged)
				{
					
					//Do Any Custom Validation Stuff
					OnValidate(ref bCancel);
					bRet = bRet && !bCancel;
					
					//Run Validation from DataModel
					if(pIPer != null)
					{
						bRet &= pIPer.Validate();
                        foreach (System.Collections.DictionaryEntry objError in this.objAdm.Context.ScriptValidationErrors)
                            Errors.Add("ValidationScriptError", objError.Value.ToString(), Common.BusinessAdaptorErrorType.Error);
					}
					//No Children to validate so we're done.	
				}

			}
			catch(Exception e)
			{
				Errors.Add(e,Globalization.GetString("FormDataAdaptor.ScreenBase.Validate.Exception",base.ClientId),Common.BusinessAdaptorErrorType.Error);
				return false;
			}
			return bRet;
		}
        /// <summary>
        /// Added By: Parijat : Mits 11785
        /// The function performs the after delete operation on the xml
        /// </summary>
        public override void AfterDelete()
        {

            if (base.m_fda.HasParam("SysPropertyStore") && base.m_fda.SafeParam("SysPropertyStore").SelectSingleNode("//ADMTable") != null)
                base.m_fda.SafeParam("SysPropertyStore").SelectSingleNode("//ADMTable").Attributes.GetNamedItem("RECORD_ID").Value = "";
            objAdm.KeyValue = 0;

        }
		public override void OnSecurityIdChanged()
		{
			if(this.SecurityId ==13000)
				base.m_SecurityId = (5000000 + objAdm.TableId * 20);
			base.OnSecurityIdChanged ();
		}

		override public bool Refresh(){return Refresh(false);} 
		override public bool Refresh(bool viewOnly) 
		{
			bool bCancel = false;
			try
			{
				m_CurrentAction = enumFormActionType.Refresh;
				
				int RMO_PERM;
				if(pIPer !=null && pIPer.IsNew)
					RMO_PERM = RMO_CREATE;
				else
					RMO_PERM= RMO_VIEW;

				if(!Adaptor.userLogin.IsAllowedEx(m_SecurityId, RMO_PERM))
				{
					m_CurrentAction = enumFormActionType.None;
					LogSecurityError(RMO_PERM);
					return false;
				}
				
				BeforeRefresh(ref bCancel);
				if(!bCancel)
					BeforeAction(m_CurrentAction,ref bCancel);
				
				if(bCancel)
				{
					this.Cancelled=true;
					m_CurrentAction = enumFormActionType.None;
					return false;
				}

				if(!viewOnly && pIPer !=null)
					pIPer.Refresh();
				
				UpdateForm();
				m_DataChanged = false;

				AfterRefresh();
				AfterAction(m_CurrentAction);
			}				
			catch(Exception e)
			{
				Errors.Add(e,Globalization.GetString("FormDataAdaptor.ScreenBase.Refresh.Exception",base.ClientId),Common.BusinessAdaptorErrorType.Error);
				m_CurrentAction = enumFormActionType.None;
				return false;
			}
			m_CurrentAction = enumFormActionType.None;
			return true;

		}
		//BSB TODO - This function enforced fields set as required in REQ_FIELDS database table.
		// In World this is meaningfull. In RMNet these settings are simply ignored.  The question is:
		// What to do about it in Wizard.  Obviously a direct port is impossible since the linkage from
		// record to screen control uses Visual Basic control names which are not available in the GUI
		// any more...
		// Stubbed for now.
		private bool UnivValidate()
		{
			return true;
		}

        // akaushik5 Added for MITS 37383 Starts
        /// <summary>
        /// Implements the SMS permissions.
        /// </summary>
        private void ImplementSMSPermissions()
        {
            if (!this.objAdm.Context.RMUser.IsAllowedEx(this.m_SecurityId, FormBase.RMO_ADM_EMAIL_NOTIFICATION))
            {
                base.AddKillNode("sendmail");
            }
        }
        // akaushik5 Added for MITS 37383 Ends
	}
}
