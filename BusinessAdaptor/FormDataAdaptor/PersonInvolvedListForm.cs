﻿
using System;
using Riskmaster.DataModel;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Application.SupportScreens;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.BusinessAdaptor.Common;
namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PersonInvolvedList.
	/// </summary>
	public class PersonInvolvedListForm: ListFormBase
	{
		const string CLASS_NAME = "PersonInvolvedList";
		public override void Init()
		{
			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlDocument objXML = base.SysEx;
			XmlNode objEventId=null;
            //start rsushilaggar JIRA 11736
            XmlNode objParentId = null;
            string sParentTable = null;
            try
            {
                objEventId = objXML.GetElementsByTagName("EventId")[0];
                objParentId = objXML.GetElementsByTagName("ParentId")[0];
                sParentTable = base.GetFormVarNodeText("SysFormPForm"); ;
            }
            catch { };
			
			//Filter by this EventId if Present.
            if (objEventId != null)
            {
                if (objParentId != null && string.IsNullOrEmpty(objParentId.InnerText))
                {
                    objParentId.InnerText = objEventId.InnerText;
                }
            }
            if (objParentId != null)
            {
                    if ((sParentTable).ToUpper() == "CLAIMGC" || (sParentTable).ToUpper() == "CLAIMDI" || (sParentTable).ToUpper() == "CLAIMPC" || (sParentTable).ToUpper() == "CLAIMWC" || (sParentTable).ToUpper() == "CLAIMVA")
                    {
                        objDataList.BulkLoadFlag = true;
                        objDataList.SQLFilter = "PARENT_ROW_ID=" + objParentId.InnerText + " AND PARENT_TABLE_NAME ='CLAIM'";
                    }
                    else if (sParentTable.ToUpper() == "EVENT")
                    {
                        objDataList.BulkLoadFlag = true;
                        objDataList.SQLFilter = "PARENT_ROW_ID=" + objParentId.InnerText + " AND PARENT_TABLE_NAME ='EVENT'";
                    }
                    else if (sParentTable.ToUpper() == "CLAIMXPOLICY")
                    {
                        objDataList.BulkLoadFlag = true;
                        objDataList.SQLFilter = "PARENT_ROW_ID=" + objParentId.InnerText + " AND PARENT_TABLE_NAME ='POLICY'";
                    }
            } //End rsuhsilaggar JIRA 11736
			else
				throw new Riskmaster.ExceptionTypes.CollectionException(String.Format(Globalization.GetString("ListForm.MissingExpectedFilterKey",base.ClientId),"EventId"));
			OnUpdateForm();
		}


		public PersonInvolvedListForm(FormDataAdaptor fda):base(fda)
		{
			this.objDataList = fda.Factory.GetDataModelObject(CLASS_NAME,false) as DataCollection;
		}
		private LocalCache objCache{get{return objDataList.Context.LocalCache;}}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm() //Only called from base classes on Refresh()caused by PostBack.
		{
			
			base.OnUpdateForm ();
			base.CreateSysExData("PiEid","");


			XmlDocument objXML = base.SysEx;
			XmlNode objNew;
			XmlNode objOld ;
			HipaaLog objLog=null;
            DbReader objReader = null;
            string sSQL = string.Empty;
			objNew = objXML.CreateElement("Screen");
			objNew.Attributes.Append(objXML.CreateAttribute("type"));
			objNew.Attributes["type"].Value="";

			objOld = objXML.SelectSingleNode("//Screen");
			if(objOld !=null)
				;
				//Let it Ride from the Client... 
				//Will be acted on in the Form XSL
				//No Need to clear since next page request should hit a new
				//form altogether anyhow.
				//objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);


			//Clear any existing tablename nodes.
			foreach(XmlNode nd in objXML.SelectNodes("//EntityTableName"))
				nd.ParentNode.RemoveChild(nd);
            
            //Added Rakhi for Mits 13082-START:Able to hide the Add and Delete  button for PI
                CheckSecuritySettings(ref objXML);
            //Added Rakhi for Mits 13082-END:Able to hide the Add and Delete  button for PI
			//*********************
			// Parallel Array to the PIList to be serialized...
			//*********************
			objLog=new HipaaLog(objDataList.Context.DbConn.ConnectionString, base.ClientId);
            //XmlDocument objSysFormStack=  base.SysFormStack;
            //XmlNode objNode=objSysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[position()=last()]");
            //string sFormName="";
            //string sFormId="0";
			string sEventId="0";
			string sClaimId="0";
            //bool bIsPatient=false;
            //if (objNode!=null)
            //{
            //    sFormName=objNode.SelectSingleNode("SysFormName").InnerText;
            //    sFormId=objNode.SelectSingleNode("SysFormId").InnerText;
            //    sFormName=GetSearchStringFromFormName(sFormName,sFormId,ref sClaimId,ref sEventId);
            //}
            //objNode=objSysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[position()=1]");
            //if (objNode!=null)
            //{
            //    if (objNode.SelectSingleNode("SysFormName").InnerText=="pipatient")
            //        bIsPatient=true; //When coming from BackToPersonInvolved-NOT in R5
            //}
            //MITS 15918 : Umesh 
            //We can remove this condition without any impact as we are using foreach loop.
            //if (base.m_fda.SafeFormVariableParamText("SysCmd")=="1" )
            //{
				//if (bIsPatient==false)
				//{
					foreach(PersonInvolved objPI in objDataList)
					{
						//Entity Table Name - Used as fall-back for Blank Entity Name.
						if (objPI.PiEntity.EntityTableId==1061)
						{
							if (objPI.PiEntity.EntityId!=0)
							{
								if (sEventId=="0")
								{
									sEventId=objPI.EventId.ToString();
								}
								//objLog.LogHippaInfo(objPI.PiEntity.EntityId,sEventId,sClaimId,sFormName,"VW",objDataList.Context.RMUser.LoginName);
                                objLog.LogHippaInfo(objPI.PiEntity.EntityId, sEventId, sClaimId, "Event Occurrence/Person Involved", "VW", objDataList.Context.RMUser.LoginName);
							}
						}
                        //avipinsrivas Start : Worked for Jira-340
                        string sTableName = objCache.GetUserTableName(objPI.PiEntity.EntityTableId);
                        //string sTableName = objCache.GetUserTableName(objPI.PiEntity.GetEntityTableID(objPI.PiErRowID));
                        //avipinsrivas End
						objNew = objXML.CreateElement("EntityTableName");
						objNew.AppendChild(objXML.CreateCDataSection(sTableName));
                        objNew.Attributes.Append(objXML.CreateAttribute("EntityId"));
                        objNew.Attributes["EntityId"].Value = objPI.PiEntity.EntityId.ToString();
                        objNew.Attributes.Append(objXML.CreateAttribute("EntitySysTableName"));
						objXML.DocumentElement.AppendChild(objNew);

                        // jira 17807 pgupta215- Show Other person type in screeen
                        string sRoleTableName = objCache.GetUserTableName(objPI.RoleTableId);
                        objNew = objXML.CreateElement("RoleTableName");
                        objNew.AppendChild(objXML.CreateCDataSection(sRoleTableName));
                        objNew.Attributes.Append(objXML.CreateAttribute("RoleTableId"));
                        objNew.Attributes["RoleTableId"].Value = Conversion.ConvertObjToStr(objPI.RoleTableId);
                        objNew.Attributes.Append(objXML.CreateAttribute("RoleSysTableName"));
                        objNew.Attributes["RoleSysTableName"].Value = sRoleTableName;
                        objXML.DocumentElement.AppendChild(objNew);
                        // jira 17807 end
					}
					
				//}
            //}
            
           //Geeta 03/13/07 : Modified for fixing mits number 8351
            if (objXML.SelectSingleNode("//PersonInvolvedTypeList") == null)
            {

                sSQL = "SELECT SHORT_CODE,CODE_DESC FROM CODES_TEXT WHERE CODE_ID IN" + "(" + objCache.GetCodeId("E", "PERSON_INV_TYPE") + ",";
                sSQL = sSQL + objCache.GetCodeId("MED", "PERSON_INV_TYPE") + "," + objCache.GetCodeId("O", "PERSON_INV_TYPE") + ",";
                sSQL = sSQL + objCache.GetCodeId("P", "PERSON_INV_TYPE") + "," + objCache.GetCodeId("PHYS", "PERSON_INV_TYPE") + ",";
                sSQL = sSQL + objCache.GetCodeId("W", "PERSON_INV_TYPE") + "," + objCache.GetCodeId("D", "PERSON_INV_TYPE") + ")"; //mbahl3
                                
                objNew = objXML.CreateElement("PersonInvolvedTypeList");

                using (objReader = DbFactory.GetDbReader(base.m_fda.connectionString, sSQL))
                {

                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            objNew.AppendChild(objXML.CreateElement(Conversion.ConvertObjToStr(objReader["SHORT_CODE"])));
                            objXML.DocumentElement.AppendChild(objNew);
                            XmlNode objNode = objXML.SelectSingleNode("//PersonInvolvedTypeList/" + Conversion.ConvertObjToStr(objReader["SHORT_CODE"]));
                            objNode.InnerText = Conversion.ConvertObjToStr(objReader["CODE_DESC"]);
                        }

                        //objReader.Close();
                        //objReader = null;

                    }
                }
           }
           
		}
		public override void BeforeRemove(ref bool Cancel,XmlDocument p_objSysPropertyStore)
		{
            HipaaLog objLog=new HipaaLog(objDataList.Context.DbConn.ConnectionString, base.ClientId);
            //XmlDocument objSysFormStack=  base.SysFormStack;
            //XmlNode objNode=objSysFormStack.SelectSingleNode("//ScreenFlowStack/ScreenFlow[position()=last()]");
            //string sFormName="";
            //string sFormId="0";
			string sEventId="0";
			string sClaimId="0";
            //pmahli MITS 11143 1/7/2008    -start
            string sSQL = string.Empty;
            DbReader objReader = null;
            int iClaimID = 0;
            int iClaimantID = 0;
            string sClaimant = string.Empty;
            //pmahli MITS 11143 1/7/2008    -End
            // try catch block used because MITS 11143 needed to capture some exception
            try
            {
                //if (objNode != null)
                //{
                //    sFormName = objNode.SelectSingleNode("SysFormName").InnerText;
                //    sFormId = objNode.SelectSingleNode("SysFormId").InnerText;
                //    sFormName = GetSearchStringFromFormName(sFormName, sFormId, ref sClaimId, ref sEventId);
                //}
                XmlNodeList objList = p_objSysPropertyStore.SelectNodes("//PersonInvolved[@remove='True']");
                //pmahli MITS 11143 1/7/2008 - start 
                //Claimant information 
                iClaimID = base.GetSysExDataNodeInt("/SysExData/ClaimId");
                sSQL = "SELECT CLAIMANT_EID FROM CLAIMANT WHERE PRIMARY_CLMNT_FLAG = -1 AND CLAIM_ID=" + iClaimID;
                objReader = DbFactory.GetDbReader(base.m_fda.connectionString, sSQL);
                if (objReader.Read())
                {
                    iClaimantID = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                    sClaimant = objDataList.Context.LocalCache.GetEntityLastFirstName(iClaimantID);
                }
                
                //pmahli MITS 11143 1/7/2008 -End
                foreach (XmlNode objTmpNode in objList)
                {
                    //pmahli MITS 11143 1/7/2008-start
                    //Throws exception when Person involved to be removed is the primary employee for the claim
                    if (Conversion.ConvertObjToInt(objTmpNode.SelectSingleNode("PiEid").Attributes["codeid"].Value, base.ClientId) == iClaimantID)
                    {
                        throw new RMAppException("Employee " + sClaimant + " can't be deleted as it is Primary Employee for this claim.");
                    }
                    //pmahli MITS 11143 1/7/2008- End
                    else if (objTmpNode.SelectSingleNode("PiTypeCode/@codeid").InnerText.Equals(objDataList.Context.LocalCache.GetCodeId("P", "PERSON_INV_TYPE").ToString()))
                    {
                        int iPatientEid = Conversion.ConvertObjToInt(objTmpNode.SelectSingleNode("PiEid").Attributes["codeid"].Value, base.ClientId);
                        //objLog.LogHippaInfo(iPatientEid, sEventId, sClaimId, sFormName, "DL", objDataList.Context.RMUser.LoginName);
                        objLog.LogHippaInfo(iPatientEid, sEventId, sClaimId, "Event Occurrence/Person Involved", "DL", objDataList.Context.RMUser.LoginName);
                    }
              }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            finally
            {

            }
		}

		public string GetSearchStringFromFormName(string p_sFormName,
			string p_sFormId,ref string p_sClaimId,ref string p_sEventId)
		{
			string sSearchString="";
			switch(p_sFormName)
			{
				case "event":
					sSearchString="Event Occurrence/Person Involved"; 
					p_sEventId=p_sFormId;
					break;
				case "claimgc":
					sSearchString="General Claims/Person Involved"; 
					p_sClaimId=p_sFormId;
					break;
				case "claimwc":
					sSearchString="Workers' Compensation /Person Involved"; 
					p_sClaimId=p_sFormId;
					break;
				case "claimdi":
					sSearchString="Non-Occupational Claims/Person Involved"; 
					p_sClaimId=p_sFormId;
					break;
				case "claimva":
					sSearchString="Vehicle Accident Claims/Person Involved"; 
					p_sClaimId=p_sFormId;
					break;
				default:
					sSearchString="Person Involved List"; 
					break;
					
			}
			return sSearchString;
		}
        #region Added Rakhi for Mits 13082-START:Able to hide the Add and Delete  button for PI
        private void CheckSecuritySettings(ref XmlDocument objXML)
        {
            //bool bIsView = false;
            bool bIsAddNew = false;
            bool bIsDelete = false;
            string sSysSid = Adaptor.SafeFormVariableParamText("SysSid");
            int SecurityId = Conversion.ConvertStrToInteger(sSysSid);
            XmlNode objSec = null;
            //bIsView = Adaptor.userLogin.IsAllowedEx(SecurityId, FormBase.RMO_VIEW);
            bIsAddNew = Adaptor.userLogin.IsAllowedEx(SecurityId, RMO_CREATE);
            bIsDelete = Adaptor.userLogin.IsAllowedEx(SecurityId, FormBase.RMO_DELETE);
            objSec = objXML.SelectSingleNode("//Add");
            if (objSec == null)
            {
                if (bIsAddNew)
                {
                    objSec = objXML.CreateElement("Add");
                    objSec.Attributes.Append(objXML.CreateAttribute("type"));
                    objSec.Attributes["type"].Value = "true";
                    objXML.DocumentElement.AppendChild(objSec);
                }
                else
                {
                    objSec = objXML.CreateElement("Add");
                    objSec.Attributes.Append(objXML.CreateAttribute("type"));
                    objSec.Attributes["type"].Value = "false";
                    objXML.DocumentElement.AppendChild(objSec);

                }
            }
            objSec = objXML.SelectSingleNode("//Delete");
            if (objSec == null)
            {
                if (bIsDelete)
                {
                    objSec = objXML.CreateElement("Delete");
                    objSec.Attributes.Append(objXML.CreateAttribute("type"));
                    objSec.Attributes["type"].Value = "true";
                    objXML.DocumentElement.AppendChild(objSec);
                }
                else
                {
                    objSec = objXML.CreateElement("Delete");
                    objSec.Attributes.Append(objXML.CreateAttribute("type"));
                    objSec.Attributes["type"].Value = "false";
                    objXML.DocumentElement.AppendChild(objSec);
                }
            }
        }
      #endregion Added Rakhi for Mits 13082-END:Able to hide the Add and Delete  button for PI

        
	}
    
	
}
