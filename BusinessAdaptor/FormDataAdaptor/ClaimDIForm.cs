﻿///********************************************************************
/// Amendment History
///********************************************************************
/// 08/20/2014         RMA 1209 -   ICD 10 Code not getting deleted     achouhan3
/// 08/22/2014         RMA 1308 -   ICD 9 Code was replaceed by ICD10   achouhan3
///********************************************************************
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using Riskmaster.Security.RMApp;
using System.Text;
using System.Collections.Generic;
using Riskmaster.Application.ImagRightWrapper;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for ClaimDI Screen.
	/// </summary>
	public class ClaimDIForm : DataEntryFormBase
	{
		const string CLASS_NAME = "Claim";
		private int m_LOB;
		private LobSettings objLobSettings = null;
		private bool bCloseDiaries = false;
        //Changed by Gagan for MITS 11451 : Start
        bool bDeleteAutoChecks = false;
        //Changed by Gagan for MITS 11451 : End
		string sUserCloseDiary = "false";
		string m_ClaimLimitFilter = "";
		string[] sDaysOfWeek=new string[7];
        int iClaimId = 0;
		int iUseCaseMgmt=0;
		int iCaseMgmtHisId=0;
        //Mridul 05/28/09 MITS 16745-Chubb ACK Letter Enhancement
        bool bTriggerLetter = false;
        UserLoginLimits objLimits = null;

        int ClmStatus = 0;//JIRA RMA-11122 ajohari2 
		public struct Holidays
		{
			internal string sDate;
			internal string sDesc;
			internal long[] lOrgID;
		}
		public Holidays[] SYS_HOLIDAYS;

		private Claim objClaim{get{return objData as Claim;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		
		/// <summary>
		/// Class constructor
		/// </summary>
		/// <param name="fda"></param>
		public ClaimDIForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
			m_LOB = fda.Factory.Context.LocalCache.GetCodeId("DI", "LINE_OF_BUSINESS");
			base.m_PreInitHandler = new PreInitHandler(this.CustomPreInitHandler);
            objLimits = base.GetUserLoginLimits();
		}

		//BSB 10.10.2006 If present, this handler will be called by DM during object construction 
		// just prior to invoking any client initialization scripts.
		public void CustomPreInitHandler(object objSrc,Riskmaster.DataModel.InitObjEventArgs e)
		{
			Claim objClaim = (objSrc as Claim);
			objClaim.LineOfBusCode=m_LOB;
			this.m_ParentId = base.GetSysExDataNodeInt("/SysExData/EventId");
			objClaim.EventId = m_ParentId;
			
			//BSB 01.25.2007 Defeat the built-in PrimaryPI selection of DM
			//by force loading it.
			objClaim.HookPrimaryPiEmployeeByPiRowId(base.GetSysExDataNodeInt("/SysExData/PiRowId"));
			//Log.Write("CustomPreInitHandler Called");
			// Now DM will go off and run customer coded Init Scripts with the same information
			// that RMWorld used to provide.
		}
		// BSB 11.09.2005 Work Around for ClaimId jumping to -1 when this object creates a new parent and 
		// is added to it's claimlist.  We would like client side javascript to be able to assume
		// that only "ClaimId==0" means new record.
		public override void AfterAddNew()
		{
			base.AfterAddNew ();
			objClaim.ClaimId=0;
		}
		public override void InitNew()
		{
			base.InitNew();

			m_LOB = objClaim.Context.LocalCache.GetCodeId("DI", "LINE_OF_BUSINESS");
			objLobSettings = objClaim.Context.InternalSettings.ColLobSettings[m_LOB];
			
			this.m_ParentId = base.GetSysExDataNodeInt("/SysExData/EventId");

			objClaim.LineOfBusCode = m_LOB;
			objClaim.NavType = Riskmaster.DataModel.ClaimNavType.ClaimNavLOB;

			if(m_ParentId >0)
				(objData as INavigation).Filter = "EVENT_ID=" + m_ParentId;
			if(m_ClaimLimitFilter !="")
            {
                if (m_ParentId > 0) //Geeta 09/19/07 : Modified for MITS 10377
                {
                    (objClaim as DataModel.INavigation).Filter += " AND " + m_ClaimLimitFilter;
                }
                else
                {
                    (objClaim as DataModel.INavigation).Filter += m_ClaimLimitFilter;
                }
            }
			
			objClaim.EventId = m_ParentId;
            //Added by Shivendu for MITS 9047
            if (SysEx.DocumentElement.SelectSingleNode("NonOccClaimFutureDate") == null)
            {
                CreateSysExData("NonOccClaimFutureDate");
                base.ResetSysExData("NonOccClaimFutureDate", "");
            }
           
            //Added by Shivendu for MITS 9047
            //MGaba2 : MITS 11989
            if (SysEx.DocumentElement.SelectSingleNode("NonOccEventFutureDate") == null)
            {
                CreateSysExData("NonOccEventFutureDate");
                base.ResetSysExData("NonOccEventFutureDate", "");
            }
		}
        /// <summary>
        /// Author: Sumit Kumar
        /// Date: 09/23/2010
        /// MITS: 21919
        /// </summary>
        /// <returns></returns>
        public override bool AddNew()
        {
            try
            {
                bool bIsClaimDIEnabled = objClaim.Context.InternalSettings.SysSettings.UseDILOB;
                if (bIsClaimDIEnabled)
                {
                    base.AddNew();
                }
                else
                {
                    //base.LogSecurityError(FormBase.RMO_ACCESS); - May use this if decided in future.
                    Errors.Add(Globalization.GetString("PermissionFailure",base.ClientId), Globalization.GetString("Claim.AccessPermission.error",base.ClientId), BusinessAdaptorErrorType.Error);//psharma206
                    return false;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Claim.GetUtilitySetting.error",base.ClientId), p_objException);//psharma206
            }
            return true;
        }

        public override Hashtable GetTrimHashTable()
        {
            Hashtable ht = new Hashtable();
            ht.Add("Comments", "Comments");
            ht.Add("HTMLComments", "HTMLComments");
            ht.Add("AdjusterList", "AdjusterList");
            ht.Add("ClaimantList", "ClaimantList");
            ht.Add("LitigationList", "LitigationList");
            ht.Add("DefendantList", "DefendantList");
            ht.Add("UnitList", "UnitList");
            ht.Add("ClaimStatusHistList", "ClaimStatusHistList");
            ht.Add("TimeAndExpenseList", "TimeAndExpenseList");
            ht.Add("FundsList", "FundsList");
            ht.Add("ReserveCurrentList", "ReserveCurrentList");
            ht.Add("ProgressNoteList", "ProgressNoteList");
            return ht;
        }

        public override void BeforeDelete(ref bool Cancel)  // Added by csingh7 R6 Claim Comment Enhancement
        {
            iClaimId = objClaim.ClaimId;
        }
		//Mukul Added 2/2/07 MITS 8760
		public override void AfterDelete()
		{
			base.AfterDelete ();
            if (iClaimId != 0)
            {
                objClaim.Context.DbConnLookup.ExecuteNonQuery(
                   String.Format(@"DELETE FROM COMMENTS_TEXT 
                WHERE ATTACH_RECORDID = {0} AND ATTACH_TABLE = 'CLAIM'", iClaimId));
            }
			base.ResetSysExData("EventId","0");
			objClaim.EventId=0;
		}

		public override void Init()
		{
			base.Init();
			
			//Filter Innaccessible Claim types based on UserLimit Security.
			if(objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ClmAccLmtFlag)//If UserLimits Enabled...
			{
				if(objLimits != null)
				{
					string[,] arr = objLimits.ClaimStatusTypeAccessList(objClaim.LineOfBusCode);

					if(arr.Length!=0)
					{
						m_ClaimLimitFilter = "";
						string sTypeFilter = "";
						string sTypeFilter1 = "";
						string sTypeFilter2 = "";

						for(int i=0; i<Convert.ToInt32(arr.Length/2); i++)
						{
							if(arr[i,0]!="0" && arr[i,1]!="0")
							{
								if(i>0 && sTypeFilter!="")
									sTypeFilter += " AND ";
								sTypeFilter += "NOT(CLAIM_TYPE_CODE =" + arr[i,0];
								sTypeFilter += " AND ";
								sTypeFilter += "CLAIM_STATUS_CODE =" + arr[i,1];
								sTypeFilter += ")";
							}
							else if(arr[i,0]!="0" || arr[i,1]!="0")
							{
								if(arr[i,0]!="0")
								{									
									if(i>0 && sTypeFilter1!="")
										sTypeFilter1 += ",";
									sTypeFilter1 += arr[i,0];
								}
								else
								{									
									if(i>0 && sTypeFilter2!="")
										sTypeFilter2 += ",";
									sTypeFilter2 += arr[i,1];
								}
							}
						}
						if(sTypeFilter!="")
							m_ClaimLimitFilter += sTypeFilter;

						if(sTypeFilter1!="")
						{	
							if(m_ClaimLimitFilter!="")
								m_ClaimLimitFilter += " AND " + "CLAIM_TYPE_CODE NOT IN (" + sTypeFilter1 + ")";
							else
								m_ClaimLimitFilter += "CLAIM_TYPE_CODE NOT IN (" + sTypeFilter1 + ")";
						}
						if(sTypeFilter2!="")
						{	
							if(m_ClaimLimitFilter!="")
								m_ClaimLimitFilter += " AND " + "CLAIM_STATUS_CODE NOT IN (" + sTypeFilter2 + ")";
							else
								m_ClaimLimitFilter += "CLAIM_STATUS_CODE NOT IN (" + sTypeFilter2 + ")";
						}
					}
				}
			}

			string sSQL="SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'CASE_MGT_FLAG'";
            using (DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
            {
                while (objReader.Read())
                {
                    iUseCaseMgmt = Conversion.ConvertObjToInt(objReader.GetValue("STR_PARM_VALUE"), base.ClientId);
                }
            }
            //It is already prsent in LoadPiemployee
            //foreach(CmXCmgrHist objCmXCmgrHist in objClaim.CaseManagement.CaseMgrHistList)
            //{
            //    if(objCmXCmgrHist.PrimaryCmgrFlag)
            //    {
            //        iCaseMgmtHisId=objCmXCmgrHist.CmcmhRowId;
            //    }
            //}
		}

		
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
            XmlDocument objXmlCustom = null;                  // Umesh
            XmlDocument objXML = base.SysView;                  //Umesh
			XmlCDataSection objCData=null;
			XmlElement objElem=null;
			XmlElement objChild=null;
			XmlElement objWeeksPerMonth = null;
            // 03/01/2007 REM  to customize the page flow : Umesh
             ArrayList singleRow = null;
            objXmlCustom = new XmlDocument();

            //For new claim, InitCClaim custom script could set value for Comments.
            //If LegacyComments is true, we need to bring value back
            XmlDocument objSysExData = base.SysEx;
            objClaim.GetCommentsXml(ref objSysExData, objClaim.ClaimId);

            //Mridul 05/28/09 MITS 16745 - Pass value of Template for Claim Letter 
            base.CreateSysExData("ClaimLetterTmplId", "0");
            //using (DbReader objRdr = RMSessionManager.GetCustomizedContent("customize_custom"))//R5 PS2 Merge
            string strContent = RMSessionManager.GetCustomContent(base.ClientId);//R5 PS2 Merge
            if (!string.IsNullOrEmpty(strContent))//R5 PS2 Merge
            {
                objXmlCustom.LoadXml(strContent);//R5 PS2 Merge
                //if (objRdr.Read())//R5 PS2 Merge
                //{
                //    objXmlCustom.LoadXml(objRdr.GetString("CONTENT"));//R5 PS2 Merge
                    //objRdr.Dispose();
                    //Added by Shivendu to do a null check
                    if (objXmlCustom.SelectSingleNode("//SpecialSettings/Show_AdjusterList") != null)
                        if (objXmlCustom.SelectSingleNode("//SpecialSettings/Show_AdjusterList").InnerText == "-1")
                        {
                        //    XmlElement objXmlElement = null;
                        //    objXmlElement = objXML.SelectSingleNode("//button[@name='btnAdjuster']") as XmlElement;
                        //    if (objXmlElement != null)
                        //        objXmlElement.Attributes["param"].Value = "SysFormPIdName=claimid&SysFormName=claimadjusterlist&SysCmd=1&SysViewType=controlsonly&SysFormIdName=adjrowid&SysEx=/Instance/Claim/ClaimId | /Instance/Claim/ClaimNumber";
                        //    objXmlElement = null;
                            
                            singleRow = new ArrayList();
                            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.FormButton.ToString());
                            base.AddElementToList(ref singleRow, "id", "btnAdjusters");
                            base.AddElementToList(ref singleRow, "param", "SysFormPIdName=claimid&SysFormName=claimadjusterlist&SysCmd=1&SysViewType=controlsonly&SysFormIdName=adjrowid&SysEx=ClaimId|ClaimNumber&SysExMapCtl=ClaimId|ClaimNumber");
                            base.m_ModifiedControls.Add(singleRow);
                        }
                //}//R5 PS2 Merge
            }
            // 03/01/2007 REM End
            //MITS 11779:Asif code added for making time fields mandatory for Non occupational claim form.

            if (this.CurrentAction == enumFormActionType.AddNew)
            {
                if (objClaim.Context.InternalSettings.SysSettings.DefaultTimeFlag == -1)
                {
                    XmlNode xnode = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysNotReqNew");
                    if (xnode != null)
                    {
                        xnode.InnerText = xnode.InnerText + "|timerequired";
                    }
                }
            }
            //MITS 11779 end
            
            objXML = null;
            objXML = base.SysEx;
			
			if(this.CurrentAction == enumFormActionType.AddNew)
			{
				//We're adding a new claim - check to see if we are supposed to put it 
				//under an existing event...
				XmlNode objEventNode = this.SysEx.SelectSingleNode("/SysExData/EventId");
				if(objEventNode!=null && objClaim.EventId==0)
				{
					objClaim.EventId = Conversion.ConvertStrToInteger(objEventNode.InnerText);
				}
				//Start Bug no. 001219(For a new record automatic selection/checking work week)
				objClaim.PrimaryPiEmployee.WorkFriFlag=objClaim.Context.InternalSettings.SysSettings.WorkFri;
				objClaim.PrimaryPiEmployee.WorkMonFlag=objClaim.Context.InternalSettings.SysSettings.WorkMon;
				objClaim.PrimaryPiEmployee.WorkSatFlag=objClaim.Context.InternalSettings.SysSettings.WorkSat;
				objClaim.PrimaryPiEmployee.WorkSunFlag=objClaim.Context.InternalSettings.SysSettings.WorkSun;
				objClaim.PrimaryPiEmployee.WorkThuFlag=objClaim.Context.InternalSettings.SysSettings.WorkThu;
				objClaim.PrimaryPiEmployee.WorkTueFlag=objClaim.Context.InternalSettings.SysSettings.WorkTue;
				objClaim.PrimaryPiEmployee.WorkWedFlag=objClaim.Context.InternalSettings.SysSettings.WorkWed;
				//End Bug no. 001219(For a new record automatic selection/checking work week)

				base.ResetSysExData("NewClaim","1");
			}

			//Umesh MITS_8093
			else
				base.ResetSysExData("NewClaim","0");
			//Umesh MITS_8093

			base.ResetSysExData("AllCodes",m_fda.Factory.Context.LocalCache.GetAllParentCodesForClosedClaims(844,m_fda.Factory.Context.RMDatabase.DataSourceId));
			//Place Full Parent(Event) Information into SysExData for XML Binding
			XmlNode objOld=null;
			XmlElement objNew=null;
			int l=0;

			objOld = objXML.SelectSingleNode("//Parent");
			objNew = objXML.CreateElement("Parent");
			objNew.InnerXml = (objClaim.Parent as DataObject).SerializeObject();

			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);

			string sNewClaim=base.GetSysExDataNodeText("/SysExData/NewClaim");

			//Check UserLimit Security.
			if(objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmAccLmtFlag)//If UserLimits Enabled...
			{
				if(objLimits != null)
                    //Asharma326 MITS 30874 Add parameter objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmAccLmtUsrFlag
                    if ((!objClaim.IsNew && !objLimits.ClaimAccessAllowed(objClaim.LineOfBusCode, objClaim.ClaimTypeCode, objClaim.ClaimStatusCode, objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmAccLmtUsrFlag) && sNewClaim != "1"))
					{
						//Umesh MITS_8093
						int iClaim_Id;
						int iDirection=(int)this.CurrentAction;
                        //gagnihotri MITS 18851
                        if (sNewClaim == "0" && iDirection != 9 && iDirection != 6)
						{ 
							switch(iDirection.ToString())
							{
									
								case "1" : 
									MoveNext();
									break;

								case "2" :
									MovePrevious();
									break;

								case "3" :
									iClaim_Id=objClaim.Context.DbConnLookup.ExecuteInt("SELECT MAX(CLAIM_ID) FROM CLAIM WHERE (LINE_OF_BUS_CODE=" + objClaim.LineOfBusCode + ")" );
									if(objClaim.ClaimId==iClaim_Id)
										MovePrevious();
									else
										MoveNext();
									break;

								case "4" :
									iClaim_Id=objClaim.Context.DbConnLookup.ExecuteInt("SELECT MIN(CLAIM_ID) FROM CLAIM WHERE (LINE_OF_BUS_CODE=" + objClaim.LineOfBusCode + ")" );
									if(objClaim.ClaimId==iClaim_Id)
										MoveNext();
									else
										MovePrevious();
									break;
								
								
							}
						}//Umesh MITS_8093

						else
						{
							base.ResetSysExData("NewClaim","0");
							Errors.Add(Globalization.GetString("LimitError",base.ClientId),
								String.Format(Globalization.GetString("Access.Claim.UserLimitFailed",base.ClientId), objCache.GetCodeDesc(objClaim.ClaimTypeCode,base.Adaptor.userLogin.objUser.NlsCode),objCache.GetCodeDesc(objClaim.ClaimStatusCode,base.Adaptor.userLogin.objUser.NlsCode)),
								BusinessAdaptorErrorType.SystemError); //Aman ML Change added another parameter in the GetCodeDesc function 
							return;
						}
					}
			}
	
			//add ClassList for populating the Class Name Combo box
			objOld = objXML.SelectSingleNode("//ClassList");
			objNew = objXML.CreateElement("ClassList");
			
			if (objClaim.DisabilityPlan.ClassList!=null && objClaim.DisabilityPlan.ClassList.Count>0)
			{
				objNew.SetAttribute("codeid",objClaim.ClassRowId.ToString());

                //Abhishek MITS 11119: Appending blank value in drop down
                objElem = objXML.CreateElement("option");
                objElem.SetAttribute("value", "0");
                objElem.InnerText = string.Empty;
                objNew.AppendChild(objElem);
               
				foreach(DisabilityClass o in objClaim.DisabilityPlan.ClassList)
				{
					objElem=objXML.CreateElement("option");
					objElem.SetAttribute("value", o.ClassRowId.ToString());
					objCData = objXML.CreateCDataSection(o.ClassName);
					objElem.AppendChild(objCData);
					objNew.AppendChild(objElem);
				}
			}
			else
			{
				objNew.SetAttribute("codeid",string.Empty);
				objElem=objXML.CreateElement("option");
				objElem.SetAttribute("value","0");
				objElem.InnerText=string.Empty;
				objNew.AppendChild(objElem);
			}

			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);

			//add the Current Adjuster Name Node
			ClaimAdjuster objAdj = objClaim.CurrentAdjuster;
			if (objAdj!=null)
			{
				if (objAdj.CurrentAdjFlag)
					base.ResetSysExData("CurrentAdjuster",objAdj.AdjusterEntity.Default);
				else
					base.ResetSysExData("CurrentAdjuster","");
			}
			else
				base.ResetSysExData("CurrentAdjuster","");

			//add the Claimant Attorney node
			objOld = objXML.SelectSingleNode("//ClaimantAttorney");
			objNew = objXML.CreateElement("ClaimantAttorney");
			
			Claimant objPrimaryClaimant = objClaim.PrimaryClaimant;
			if (objPrimaryClaimant.AttorneyEntity!=null)
			{
				// Nikhil 01/06/2006 Defect no. 604. Removed the stray comma in Attorney, when no data is present.
				if (objPrimaryClaimant.AttorneyEntity.LastName != string.Empty && objPrimaryClaimant.AttorneyEntity.FirstName != string.Empty)
					objCData = objXML.CreateCDataSection(String.Format("{0}, {1}",objPrimaryClaimant.AttorneyEntity.LastName,objPrimaryClaimant.AttorneyEntity.FirstName));
				else
					objCData = objXML.CreateCDataSection(String.Format("{0}{1}",objPrimaryClaimant.AttorneyEntity.LastName,objPrimaryClaimant.AttorneyEntity.FirstName));
				objNew.AppendChild(objCData);
				objNew.SetAttribute("codeid",objPrimaryClaimant.AttorneyEid.ToString());
			}
			
			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);

            //JIRA RMA-11122 ajohari2  : Start
            //if (m_CurrentAction != enumFormActionType.Save && m_CurrentAction != enumFormActionType.Delete && m_CurrentAction != enumFormActionType.Refresh && m_CurrentAction != enumFormActionType.MoveTo)
            if(m_CurrentAction.Equals(enumFormActionType.AddNew))
            {
                if (ClmStatus.Equals(0))
                {
                    ClmStatus = CommonFunctions.GetChildIDFromParent(objClaim.Context.LocalCache.GetTableId("CLAIMANT_STATUS"), objClaim.Context.LocalCache.GetCodeId("O", "CLAIMANT_STATUS_PARENT"), objClaim.Context.DbConn.ConnectionString, base.ClientId);
                }

                if (!ClmStatus.Equals(0))
                {
                    objPrimaryClaimant.ClaimantStatusCode = ClmStatus;
                }
            }
            objOld = objXML.SelectSingleNode("//ClaimantStatusCode");
            objNew = objXML.CreateElement("ClaimantStatusCode");

            
            if (objPrimaryClaimant != null)
            {
                objCData = objXML.CreateCDataSection(String.Format("{0}", objClaim.Context.LocalCache.GetCodeDesc(objPrimaryClaimant.ClaimantStatusCode)));
                objNew.AppendChild(objCData);
                objNew.SetAttribute("codeid", objPrimaryClaimant.ClaimantStatusCode.ToString());
            }

            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);

            //JIRA RMA-11122 ajohari2  : End
			Riskmaster.Settings.LobSettings objLob=null;
			//add the sub title node
            //objOld = objXML.SelectSingleNode("//SubTitle");
            //objNew = objXML.CreateElement("SubTitle");

            //zmohammad MITs 34436 - Unifying the function used to apply form subtitle across all LOBs.
            ApplyFormTitle();
            //if (!objClaim.IsNew)
            //{
            //    objLob = objData.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode];
            //    objNew.InnerText=" [" + objClaim.ClaimNumber + " * " +
            //        objData.Context.InternalSettings.CacheFunctions.GetOrgParent((objClaim.Parent as Event).DeptEid, objLob.CaptionLevel, 1,ref l) + " * " +
            //        objClaim.PrimaryPiEmployee.PiEntity.LastName + ", " + objClaim.PrimaryPiEmployee.PiEntity.FirstName +"] ";
            //    singleRow = new ArrayList();
            //    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            //    base.AddElementToList(ref singleRow, "id", "formsubtitle");
            //    base.AddElementToList(ref singleRow, "Text", objNew.InnerText);
            //    base.m_ModifiedControls.Add(singleRow);
            //}
            //if(objOld !=null)
            //    objXML.DocumentElement.ReplaceChild(objNew,objOld);
            //else
            //    objXML.DocumentElement.AppendChild(objNew);

			Event objEvent=objClaim.Parent as Event;

			//Handle Locked down Toolbar buttons.
			if(!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_SEARCH,FormBase.RMO_CLAIM_SEARCH))
				base.AddKillNode("search");
			if(!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_LOOKUP,FormBase.RMO_CLAIM_SEARCH))
				base.AddKillNode("lookup");

            //MITS 20237 Add support for View SSN permission
            //if (!objClaim.Context.RMUser.IsAllowedEx(RMPermissions.RMO_EMPLOYEE, RMPermissions.RMO_EMPLOYEE_VIEW_SSN))
            //{
            //    base.AddKillNode("emptaxid");
            //}------ smishra25:Commented for MITS 24395.Behaviour should be similar to 13748

            //nadim for 13748,added to hide/unhide SSN field
            if (!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_DI_EMPLOYEE, FormBase.RMO_VIEW_SSN))
            {
                base.AddKillNode("emptaxid");

            }

            //nadim for 13748


			int iAWWFormOption=-1;

			//Remove button if this claim does not have any state WC forms.
			if (objClaim.FilingStateId!=0)
			{
                using (DbReader objReader = objClaim.Context.DbConnLookup.ExecuteReader("SELECT STATE_ID, AWW_FORM_OPTION FROM STATES WHERE STATE_ROW_ID=" + objClaim.FilingStateId))
                {
                    if (objReader.Read())
                    {
                        string sAWWFormOption = Conversion.ConvertObjToStr(objReader.GetValue("AWW_FORM_OPTION"));
                        if (sAWWFormOption.Trim() != string.Empty)
                            iAWWFormOption = Conversion.ConvertStrToInteger(sAWWFormOption);
                    }
                    objReader.Close();
                }
			}

			SetAWWSysExNodes(iAWWFormOption);

			//Raman Bhatia .. As per email from Mike, Making changes in Progress Notes Design
			//Kill Comments or Enhanced Notes node depending on SYS_PARMS_LOB settings

			try
			{
                //rsushilaggar: Get the flag from the LobSetings Objects
                //int iUseClaimProgressNotes = objClaim.Context.DbConnLookup.ExecuteInt(
                //    "SELECT USE_CLAIM_PROGRESS_NOTES FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode.ToString());
                int iUseClaimProgressNotes = objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmProgressNotesFlag;
				if(iUseClaimProgressNotes == 0)
				{
					base.AddKillNode("enhancednotes");
				}

                //rsushilaggar: Get the flag from the LobSetings Objects
                //int iUseClaimComments = objClaim.Context.DbConnLookup.ExecuteInt(
                //    "SELECT USE_CLAIM_COMMENTS FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode.ToString());
                int iUseClaimComments = objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmCommentsFlag;

				if(iUseClaimComments == 0)
				{
					base.AddKillNode("comments");
				}
                //rsushilaggar: ISO Claim Search : Start
                int iUseISOSubmission = objClaim.Context.InternalSettings.ColLobSettings[m_LOB].ClmISOSubmissionFlag;
                if (iUseISOSubmission == 0 || iUseISOSubmission.ToString() == null)
                {
                    base.AddKillNode("iso");
                }
                //rsushilaggar: ISO Claim Search : End					
                if (!objClaim.Context.InternalSettings.SysSettings.ClaimActivityLog)
                {
                    base.AddKillNode("claimActLog");
                }
                else
                {
                    base.AddDisplayNode("claimActLog");
                }
			}
			catch
			{				
			}
            //Start - VSS enable setting check

            //if (!objClaim.Context.InternalSettings.SysSettings.EnableVSS.Equals(-1))
            //{
            //    base.AddKillNode("vssclaimind");
            //}
            //else
            //{
            //    base.AddDisplayNode("vssclaimind");
            //}
            //End - VSS enable setting check
			
			//Event Number Locking Logic
			//Nitesh MITS 7656 Starts
//			if(objClaim.IsNew && (objClaim.Parent as Event).EventId ==0 && !objClaim.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_ALLOW_ENTRY_EVENT_NUM))
//				AddReadOnlyNode("ev_eventnumber");
//			
//			if(objClaim.IsNew && (objClaim.Parent as Event).EventId !=0 && !objClaim.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_ALLOW_EDIT_EVENT_NUM))
//				AddReadOnlyNode("ev_eventnumber");
//
//			if(!objClaim.IsNew && !objClaim.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_ALLOW_EDIT_EVENT_NUM))
//				AddReadOnlyNode("ev_eventnumber");
			//Nitesh MITS 7656 Ends

            // ABhateja, 05.07.2007 -START-
            // MITS 9980, Commented the code for location and event description 
            // freeze flags as it is being checked in the editor.cs file.
            //if(objEvent.Context.InternalSettings.SysSettings.FreezeEventDesc!=0)
            //    base.AddReadOnlyNode("ev_eventdescription");

            //if(objEvent.Context.InternalSettings.SysSettings.FreezeLocDesc!=0)
            //    base.AddReadOnlyNode("ev_locationareadesc");
            // ABhateja, 05.07.2007 -END-
            
			if ((objClaim.IsNew && !objClaim.Context.RMUser.IsAllowedEx(this.SecurityId, RMO_CLAIM_NUMBER_ALLOW_ENTRY))
				|| (!objClaim.IsNew && !objClaim.Context.RMUser.IsAllowedEx(this.SecurityId, RMO_CLAIM_NUMBER_ALLOW_EDIT)))
				base.AddReadOnlyNode("claimnumber");	

        

        
			// Mihika 07-Jan-2006 Defect no. 1279
			// Making 'Employee Number' readonly in case the corresponding utilities entry is set.
			if (!objClaim.IsNew && !objClaim.Context.InternalSettings.SysSettings.EditWCEmpNum)
				base.AddReadOnlyNode("empemployeenumber");
            else
                base.AddReadWriteNode("empemployeenumber");

			// Mihika 07-Jan-2006 Defect no. 1231 Added a SysEx node for 'DefaultDeptFlag'
			base.ResetSysExData("DefaultDeptFlag", objClaim.Context.InternalSettings.SysSettings.DefaultDeptFlag.ToString());

			objOld = objXML.SelectSingleNode("//ControlAppendAttributeList");
			objNew = objXML.CreateElement("ControlAppendAttributeList");
			
			objElem=objXML.CreateElement("ev_eventnumber");
			if (!objClaim.IsNew)
			{
                //Commented by pmittal5 Mits 16471 - linktobutton has been removed for Event Number 
				//objChild=objXML.CreateElement("linktobutton");
				//objChild.SetAttribute("value","1");
				//Nitesh  MITS 7656 Starts
				//objElem.AppendChild(objChild);
				if (!objEvent.Context.RMUser.IsAllowedEx(this.SecurityId, RMO_ALLOW_EDIT_EVENT_NUM))
				{
					objChild=objXML.CreateElement("readonlyText");
					objChild.SetAttribute("value","true");
					objElem.AppendChild(objChild);
					//base.AddReadOnlyNode("eventnumber");
				}
				//Nitesh  MITS 7656 Ends
			}
			else
			{
				// MITS 9558  Need to remove. Button doesn't work correctly.   objChild=objXML.CreateElement("lookupbutton");
				// MITS 9558  objChild.SetAttribute("value","1");
				//Nitesh MITS 7656 Starts
				// MITS 9558  objElem.AppendChild(objChild);
				if(	(objEvent.EventId ==0 && !objEvent.Context.RMUser.IsAllowedEx(this.SecurityId, RMO_ALLOW_ENTRY_EVENT_NUM))
					||	(objEvent.EventId !=0 && !objEvent.Context.RMUser.IsAllowedEx(this.SecurityId, RMO_ALLOW_EDIT_EVENT_NUM)))
				{
					objChild=objXML.CreateElement("readonlyText");
					objChild.SetAttribute("value","true");
					objElem.AppendChild(objChild);
					//base.AddReadOnlyNode("eventnumber");
				}
			}

			objNew.AppendChild(objElem);
//Nitesh RM/RMX Gaps MITS 5653 Starts
			if (!objEvent.Context.RMUser.IsAllowedEx(623600, 0))
			{		
					objElem=objXML.CreateElement("disguidelines");
					objChild=objXML.CreateElement("disabled");
					objChild.SetAttribute("value","true");
					objElem.AppendChild(objChild);
					objNew.AppendChild(objElem);
			}
//Nitesh RM/RMX Gaps MITS 5653 Ends
			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);
		
			//Show/Clear DateTimeStamp of Record Closing if record is/is not closed.
			if(8!=objClaim.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(objClaim.ClaimStatusCode))
				objClaim.DttmClosed="";

			//create nodes in sysExData for Claim Status History Field
			base.CreateSysExData("StatusApprovedBy");
			base.CreateSysExData("StatusDateChg");
			base.CreateSysExData("StatusReason");
            //MGaba2:MITS 10241:Start
            base.ResetSysExData("systemcurdate",Conversion.GetUIDate(DateTime.Now.ToShortDateString(),LanguageCode,ClientId));//vkumar258 ML Changes
            base.ResetSysExData("BackdtClaimSetting", "1");
            if (!m_fda.userLogin.IsAllowedEx(this.SecurityId, FormBase.RMO_DI_BKDATE_CLAIM_STATUS_HISTORY))
            {
                base.ResetSysExData("BackdtClaimSetting", "0");
            }
            //MGaba2:MITS 10241:End
			string sWorkFlags = string.Empty; 
			PiEmployee objEmployee=objClaim.PrimaryPiEmployee as PiEmployee; 

			if (objEmployee!=null)
				sWorkFlags =	objEmployee.WorkSunFlag + ","
					+	objEmployee.WorkMonFlag + "," 
					+	objEmployee.WorkTueFlag + "," 
					+	objEmployee.WorkWedFlag + "," 
					+	objEmployee.WorkThuFlag + "," 
					+	objEmployee.WorkFriFlag + "," 
					+	objEmployee.WorkSatFlag;

			base.ResetSysExData("WorkFlags",sWorkFlags);
			sDaysOfWeek=sWorkFlags.Split(",".ToCharArray());

			// Loading all the childs of Primary Employee. Added By Rahul Sharma.
			LoadPiEmployeeChilds(ref objXML);
			//End of Childs of Primary Employee.

			CheckAutoGenerateEmpNo();

			// Mihika 6-Jan-2006 Defect no. 1189
			if(base.m_CurrentAction != enumFormActionType.Save)
				base.ResetSysExData("dupeoverride","");
            //Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.  
            string sEventDateFromDB = string.Empty;
            if (objEvent != null)
            {
                if (objEvent.EventId != 0)
                {
                    sEventDateFromDB = objEvent.DateOfEvent;
                    if (!String.IsNullOrEmpty(sEventDateFromDB))
                    {
                        if (base.SysEx.SelectSingleNode("DateOfEvent") == null)
                        {
                            string sUIEventDate = string.Empty;
                            sUIEventDate = Conversion.ToDate(sEventDateFromDB).ToString("MM/dd/yyyy");
                            base.CreateSysExData("DateOfEvent", sUIEventDate);
                        }
                    }
                }
            }
            //Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.
			if (objClaim.PrimaryPiEmployee.PiEntity.BirthDate!=string.Empty)
                base.ResetSysExData("EntityAge", Utilities.CalculateAgeInYears(objClaim.PrimaryPiEmployee.PiEntity.BirthDate, sEventDateFromDB)); //Added sEventDateFromDB for MITS 19315
			else
				base.ResetSysExData("EntityAge","");
            //Added by Shivendu MITS 9047
            base.ResetSysExData("NonOccClaimFutureDate", "");
            //MGaba2 : MITS 11989 
            base.ResetSysExData("NonOccEventFutureDate", "");
            //Added by Shivendu MITS 9047
			//for Displaying Back button on Policy Maintenance Screen
			base.ResetSysExData("DisplayBackButton","true");
			//for Closing Diary or not -- Rahul 6th Feb 2006
			base.ResetSysExData("CloseDiary","false");

			base.ResetSysExData("DisabilityPlanName",objClaim.DisabilityPlan.PlanName);
           
			//Nikhil Garg		17-Feb-2006
			//check whether open diaries exist for this claim or not
            if (HasOpenDiaries)
                base.ResetSysExData("ContainsOpenDiaries", "true");
            else
                base.ResetSysExData("ContainsOpenDiaries", "false");

            //Changed by Gagan for MITS 11451 : Start

            //Initializing Delete Auto-check flag
            base.ResetSysExData("DeleteAutoCheck", "false");
            base.ResetSysExData("ClaimStatusCode", objClaim.ClaimStatusCode.ToString());

            //check whether auto checks diaries exist for this claim or not
            string sSQL = "SELECT CLAIM_ID FROM FUNDS_AUTO WHERE CLAIM_ID = " + objClaim.ClaimId.ToString();
            using (DbReader objReaderAutoChecks = objClaim.Context.DbConn.ExecuteReader(sSQL))
            {
                if (objReaderAutoChecks.Read())
                    base.ResetSysExData("ContainsAutoChecks", "true");
                else
                    base.ResetSysExData("ContainsAutoChecks", "false");
            }
            //Changed by Gagan for MITS 11451 : End

            //Changed by Gagan for MITS 14770
            if (objClaim.Context.InternalSettings.SysSettings.DeleteAllClaimDiaries == 0)
            {
                base.ResetSysExData("DeleteAllClaimDiaries", "false");
            }
            else
            {
                base.ResetSysExData("DeleteAllClaimDiaries", "true");
            }

			if(!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_DI_EMPLOYEE,FormBase.RMO_VIEW_HOSPITAL_INFO))
				base.AddKillNode("hospitallist");
			if(!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_DI_EMPLOYEE,FormBase.RMO_VIEW_PHYSICIAN_INFO))
				base.AddKillNode("physicianslist");
			if(!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_DI_EMPLOYEE,FormBase.RMO_VIEW_DIAGNOSIS_INFO))
				base.AddKillNode("diagnosislist");
			if(!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_DI_EMPLOYEE,FormBase.RMO_VIEW_TREATMENT_INFO))
				base.AddKillNode("treatmentlist");
			if(!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_DI_EMPLOYEE,FormBase.RMO_ENABLE_SALARY))
			{
				base.AddKillNode("emppaytypecode");
				base.AddKillNode("emppayamount");
				base.AddKillNode("emphourlyrate");
				base.AddKillNode("empweeklyhours");
				base.AddKillNode("empweeklyrate");
				base.AddKillNode("empmonthlyrate");
			}

			//Check Employee permission
            if (!Adaptor.userLogin.IsAllowedEx(RMPermissions.RMO_EMPLOYEE + RMO_CREATE))
			{
                //XmlNode objCreatableNode = base.SysView.SelectSingleNode("//form[@name='claimdi']//control[@creatable='1']");
                //if(objCreatableNode != null )
                //{
                //    objCreatableNode.Attributes.RemoveNamedItem("creatable");
                //}
                 base.AddKillNode("empemployeenumber_creatable");
			}

			//Nikhil Garg		14-Mar-2006
			//check whether to disable Hourly Rate, Hours Per Week, & Weekly Rate fields
			sSQL="SELECT Claim_ID FROM CLAIM_AWW WHERE Claim_ID= " + objClaim.ClaimId.ToString();
            using (DbReader objClaimAWW = objClaim.Context.DbConn.ExecuteReader(sSQL))
            {
                if (objClaimAWW.Read())
                {
                    base.AddReadOnlyNode("emphourlyrate");
                    base.AddReadOnlyNode("empweeklyhours");
                    base.AddReadOnlyNode("empweeklyrate");
                }
                objClaimAWW.Close();
            }

			//Nitesh(09/27/2006): LSS Merge Setup changes Starts
			
			try
			{
				if(!objClaim.Context.InternalSettings.SysSettings.RMXLSSEnable)
				{ 
					base.AddKillNode("lssclaimind");
				}
			}
			catch
			{
				base.AddKillNode("lssclaimind");
			}
			
			//Nitesh(09/27/2006): LSS Merge Setup changes Ends
			//Raman Bhatia - LTD Phase 3 changes : Weeks Per Month should be passed via a node in SYSEX
			sSQL =	"SELECT NO_WEEKS_PER_MONTH FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = 844";
            objWeeksPerMonth = objXML.CreateElement("weekspermonth");
            using (DbReader objReader = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
            {
                if (objReader.Read())
                {
                    objWeeksPerMonth.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(0));
                }
            }
			
			objXML.DocumentElement.AppendChild(objWeeksPerMonth);

            //MGaba2:MITS 15642:creating the node so that it doesnt come in missing refs:start
            CreateSysExData("EventOnPremiseChecked", "false");
            //tkr mits 10267.  need entire org hierarchy to hide/show group assoc supp fields
            bool isOhSet = false;
            StringBuilder strB = null;//apandey21 Curr Mull
            int iResCount = 0;
            int iResCurrCount = 0;//pgupta93:JIRA-16476
            if (objClaim.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
            {
                iResCount = objClaim.Context.DbConn.ExecuteInt("SELECT COUNT(1) FROM FUNDS_AUTO WHERE CLAIM_ID=" + objClaim.ClaimId);
                iResCurrCount = objClaim.Context.DbConn.ExecuteInt("SELECT COUNT(1) FROM RESERVE_CURRENT WHERE CLAIM_ID=" + objClaim.ClaimId);//pgupta93:JIRA-16476
            }
            if (objEvent.DeptEid > 0)
            {
                using (DbReader rdr = objClaim.Context.DbConnLookup.ExecuteReader("SELECT * FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid.ToString()))
                {
                    while (rdr.Read())
                    {
                        isOhSet = true;
                        base.ResetSysExData("OH_FACILITY_EID", rdr.GetInt("FACILITY_EID").ToString());
                        base.ResetSysExData("OH_LOCATION_EID", rdr.GetInt("LOCATION_EID").ToString());
                        base.ResetSysExData("OH_DIVISION_EID", rdr.GetInt("DIVISION_EID").ToString());
                        base.ResetSysExData("OH_REGION_EID", rdr.GetInt("REGION_EID").ToString());
                        base.ResetSysExData("OH_OPERATION_EID", rdr.GetInt("OPERATION_EID").ToString());
                        base.ResetSysExData("OH_COMPANY_EID", rdr.GetInt("COMPANY_EID").ToString());
                        base.ResetSysExData("OH_CLIENT_EID", rdr.GetInt("CLIENT_EID").ToString());
                        if (objClaim.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)//Deb Multi Currency
                        {
                            strB = new StringBuilder(objEvent.DeptEid.ToString());
                            if (strB.ToString() != "")
                                strB.Append(",");
                            strB.Append(rdr.GetInt("FACILITY_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("LOCATION_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("DIVISION_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("REGION_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("OPERATION_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("COMPANY_EID").ToString() + ",");
                            strB.Append(rdr.GetInt("CLIENT_EID").ToString());
                        }
                    }
                }
                if (objClaim.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                {
                    string sCurrCode = string.Empty;
                    string[] arr = strB.ToString().Split(',');
                    for (int i = 0; i < arr.Length; i++)
                    {
                        int orgCurrCode = objClaim.Context.DbConnLookup.ExecuteInt("SELECT ORG_CURR_CODE FROM ENTITY WHERE ENTITY_ID =" + arr[i]);
                        if (orgCurrCode > 0)
                        {
                            sCurrCode = orgCurrCode.ToString();
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(sCurrCode))
                    {
                        if (m_CurrentAction != enumFormActionType.Save && m_CurrentAction != enumFormActionType.Delete && m_CurrentAction != enumFormActionType.Refresh && m_CurrentAction != enumFormActionType.MoveTo && iResCount <= 0)
                            objClaim.CurrencyType = Convert.ToInt32(sCurrCode);//Deb Multi Currency
                    }
                }
            }
            if (!isOhSet)
            {
                base.ResetSysExData("OH_FACILITY_EID", "0");
                base.ResetSysExData("OH_LOCATION_EID", "0");
                base.ResetSysExData("OH_DIVISION_EID", "0");
                base.ResetSysExData("OH_REGION_EID", "0");
                base.ResetSysExData("OH_OPERATION_EID", "0");
                base.ResetSysExData("OH_COMPANY_EID", "0");
                base.ResetSysExData("OH_CLIENT_EID", "0");
            }

            //Raman: MITS 16269
            // Store count of PI employees so we can throw up popup to pick employee when creating claim
            if (objClaim.EventId != 0)
            {
                int iCount = objClaim.Context.DbConnLookup.ExecuteInt("SELECT COUNT(*) FROM PERSON_INVOLVED WHERE EVENT_ID = " + objClaim.EventId.ToString() + " AND PI_TYPE_CODE = 237");
                base.ResetSysExData("PiEmpCount", iCount.ToString());
            }
            else
            {
                base.ResetSysExData("PiEmpCount", "0");
            }
            CreateSysExData("UseLegacyComments", Riskmaster.Common.Conversion.ConvertObjToStr(objClaim.Context.InternalSettings.SysSettings.UseLegacyComments));
            //BOB Enhancement 
            base.ResetSysExData("isAutoPopulateDpt", objClaim.Context.InternalSettings.SysSettings.AutoPopulateDpt.ToString());
            //Deb Multi Currency
            int iBaseCurrCode = objClaim.Context.InternalSettings.SysSettings.BaseCurrencyType;
            if (objClaim.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
            {
                if (objClaim.IsNew)
                {
                    if (iBaseCurrCode > 0 && objClaim.CurrencyType <= 0)
                    {
                        string sCurrency = objClaim.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode);
                        base.ResetSysExData("BaseCurrencyType", sCurrency.Split('|')[1]);
                        objClaim.CurrencyType = iBaseCurrCode;
                    }
                    else if (objClaim.CurrencyType <= 0)
                    {
                        Errors.Add(Globalization.GetString("BaseCurrency.Error",base.ClientId), "Please set the Base Currency in the Utilities", BusinessAdaptorErrorType.Error);//psharma206
                    }
                }
                else
                {
                    if (iBaseCurrCode > 0)
                    {
                        string sCurrency = objClaim.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode);
                        base.ResetSysExData("BaseCurrencyType", sCurrency.Split('|')[1]);
                         //rupal:start,multicurrency
                        //commented because for existing claim, if claim currency is not set then we will ask user to manually set
                        /*
                        if (objClaim.CurrencyType <= 0)
                        {
                            objClaim.CurrencyType = iBaseCurrCode;
                        }
                        */
                        //rupal:end 
                    }
                    else
                    {
                        Errors.Add(Globalization.GetString("BaseCurrency.Error", base.ClientId), "Please set the Base Currency in the Utilities", BusinessAdaptorErrorType.Error);//psharma206
                    }
                    //pgupta93:JIRA-16476 START
                    if (iResCurrCount > 0 || objClaim.ReserveCurrentList.Count > 0)
                    {
                        base.AddReadOnlyNode("currencytypetext");
                    }
                    //pgupta93:JIRA-16476 END
                    if (iResCount > 0 || objClaim.FundsList.Count > 0)
                    {
                        base.AddReadOnlyNode("currencytypetext");
                    }
                }
            }
            else
            {
                if (iBaseCurrCode > 0)
                {
                    string sCurrency = objClaim.Context.DbConn.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode);
                    base.ResetSysExData("BaseCurrencyType", sCurrency.Split('|')[1]);
                    if (objClaim.CurrencyType <= 0)
                    {
                        objClaim.CurrencyType = iBaseCurrCode;
                    }
                    base.AddKillNode("currencytypetext");
                }
            }
            //Deb Multi Currency

            //nsachdeva2 - 7/9/2012 - PolicySave
            int iTransCount = 0;
            if (objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface)
            {
                iTransCount = objClaim.Context.DbConn.ExecuteInt("SELECT COUNT(1) FROM FUNDS,FUNDS_AUTO WHERE FUNDS.CLAIM_ID = FUNDS_AUTO.CLAIM_ID AND FUNDS.CLAIM_ID =" + objClaim.ClaimId);
                if (iTransCount == 0)
                {
                    base.AddKillNode("multipolicyidbtndel");
                }
            }

            //Amandeep Catastrophe Enhancement MITS 28528--start
            if (!objClaim.IsNew)
                base.ResetSysExData("CatastropheNumber", GetCatastropheNumber());
            else
                base.ResetSysExData("CatastropheNumber", "");

			//tanwar2 - ImageRight - start           
                if (objClaim.Context.InternalSettings.SysSettings.UseImgRight == false)
                {
                    base.AddKillNode("openImageRight");
                    base.AddKillNode("generateFUP");
                }
                //NameValueCollection nvCol = RMConfigurationManager.GetNameValueSectionSettings("ImageRight");
                //if (nvCol != null && !string.IsNullOrEmpty(nvCol["IR_Drawer"]))
                //{
                //    base.ResetSysExData("irdrawer", nvCol["IR_Drawer"]);
                //}
                int iIRCodeId = 0;
                bool bSuccess = false;
                if (objClaim.Supplementals["IR_DRAWER_CODE"] != null)
                {
                    iIRCodeId = Conversion.CastToType<int>(objClaim.Supplementals["IR_DRAWER_CODE"].Value.ToString(), out bSuccess);
                    base.ResetSysExData("irdrawer", objClaim.Context.LocalCache.GetCodeDesc(iIRCodeId));
                }
                if (objClaim.Supplementals["IR_FILE_TYPE_CODE"] != null)
                {
                    iIRCodeId = Conversion.CastToType<int>(objClaim.Supplementals["IR_FILE_TYPE_CODE"].Value.ToString(), out bSuccess);
                    base.ResetSysExData("irfiletype", objClaim.Context.LocalCache.GetCodeDesc(iIRCodeId));
                }
                if (string.Compare(base.GetSysExDataNodeText("/SysExData/generateFUPFile"), "-1") == 0)
                {
                    base.ResetSysExData("generateFUPFile", "-1");
                }
                else
                {
                    base.ResetSysExData("generateFUPFile", "0");
                }
           
            //tanwar2 - ImageRight - end

                // gmallick JIRA 12965 Start
                if (!objClaim.Context.RMUser.IsAllowedEx(this.SecurityId, FormBase.RMO_UPDATE))
                {
                    base.ResetSysExData("updateflag", "false");
                }
                else
                {
                    base.ResetSysExData("updateflag", "true");
                }
                //gmallick JIRA 12965 End
            
		}

        


       
        private string GetCatastropheNumber()
        {
            string sSQL = "SELECT CAT_NUMBER FROM CATASTROPHE WHERE CATASTROPHE_ROW_ID = " + objClaim.CatastropheRowId;
            string sReturnValue = string.Empty;
            try
            {
                //using (DbReader objRdr = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                using (DbReader objRdr = objClaim.Context.DbConn.ExecuteReader(sSQL))
                {
                    if (objRdr.Read())
                        sReturnValue = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                }
            }
            catch (Exception e)
            {
                sReturnValue = string.Empty;
            }
            return sReturnValue;
        }
        //Amandeep Catastrophe Enhancement MITS 28528--end

		internal override bool ValidateRequiredViewFields(XmlDocument propertyStore)
		{
			CheckAutoGenerateEmpNo();

			return base.ValidateRequiredViewFields (propertyStore);
		}

		private void CheckAutoGenerateEmpNo()
		{
			//Bug No: 636,649,672. For Autogenerating Employee Number
			//Nikhil Garg		Dated: 20/Dec/2005
			XmlElement objNotReqNew = null;
			string sNotReqNew=string.Empty;
			if (objData.Context.InternalSettings.SysSettings.AutoNumWCEmp)
			{
                objNotReqNew = (XmlElement)base.FormVariables.SelectSingleNode("/FormVariables/SysNotReqNew");
				sNotReqNew=objNotReqNew.InnerText;
				if (sNotReqNew.IndexOf("empemployeenumber")==-1)
					sNotReqNew=sNotReqNew + "|empemployeenumber|";
                objNotReqNew.InnerText = sNotReqNew;
			}
		}
		
		#region Load Primary Employee Childs at runtime
		//Get the Work Loss days count between 2 dates.
		private void subInitHolidayInfo()
		{
			string sDate1=string.Empty;
			string sDate2=string.Empty;
			int i=0;
			int j=0;
			int count=1;

			using(DbReader objCount=DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString,"SELECT COUNT(*) COUNTI FROM SYS_HOLIDAYS"))
			{
				while (objCount.Read())
				{
					count=Conversion.ConvertObjToInt(objCount.GetValue("COUNTI"), base.ClientId);
					SYS_HOLIDAYS=new Holidays[count];
				}
			}

            using(DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, "SELECT HOL_DATE,ORG_EID FROM SYS_HOLIDAYS ORDER BY HOL_DATE,ORG_EID"))
			{
				while(objReader.Read())
				{
					
					sDate1=objReader.GetString("HOL_DATE");
					if(sDate1 != sDate2)
					{
						if(sDate2 != "")
							i=i+1;
						SYS_HOLIDAYS[i].sDate=sDate1;
						j=0;
						SYS_HOLIDAYS[i].lOrgID=new long[count];
                        SYS_HOLIDAYS[i].lOrgID[j] = Conversion.ConvertObjToInt64(objReader.GetValue("ORG_EID"), base.ClientId);
						sDate2=sDate1;
					}
					else
					{
						j=j+1;
                        SYS_HOLIDAYS[i].lOrgID[j] = Conversion.ConvertObjToInt64(objReader.GetValue("ORG_EID"), base.ClientId);
					}
				}
			}
		}
		private int GetWorkDay(DateTime dDate1, DateTime dDate2,string[] sDaysOfWeek)
		{
			int iDays=0;
			int iDayOfWeekStart=0;
			int iDayOfWeekEnd=0;
			int iNewDays=0;
			int iReturnEid=0;
			bool bCount;
			string sParent=string.Empty;
			TimeSpan objDays;

			objDays=dDate2.Subtract(dDate1);
			iDays=objDays.Days+1;
			iDayOfWeekStart = (int)dDate1.DayOfWeek ;
			iDayOfWeekEnd=(int)dDate2.DayOfWeek ;
			for(int i=1; i<=iDays; i++)
			{
				if(sDaysOfWeek[(int) dDate1.AddDays(i-1).DayOfWeek]=="True")
				{
					/*if(objClaim.Context.InternalSettings.SysSettings.ExclHolidays==false)
					{
						bCount=false;
						for(int j=0; j<=SYS_HOLIDAYS.GetUpperBound(SYS_HOLIDAYS.Length-1); j++)
						{
							if(dDate1.AddDays(i-1).ToShortDateString()== SYS_HOLIDAYS[j].sDate)
							{
								for(int k=0; k<= SYS_HOLIDAYS.GetUpperBound(SYS_HOLIDAYS.Length-1); k++)
								{
									iReturnEid=0;
									sParent=objClaim.Context.InternalSettings.CacheFunctions.GetOrgParent(0,Conversion.ConvertObjToInt(SYS_HOLIDAYS[j].lOrgID[k], base.ClientId),0,ref iReturnEid);
									if(SYS_HOLIDAYS[j].lOrgID[k]== iReturnEid)
										bCount=true;
								}
							}
						}
						if(bCount==false)
							iNewDays=iNewDays+1;
					}
					else
					{*/
						iNewDays=iNewDays+1;
					//}
				}
			}
			return iNewDays;
		}
		private int GetWorkLossDayCount(DateTime sLastWorkDate, DateTime sReturnedToWorkDate, string[] sDaysOfWeek)
		{
			DateTime objLastWorkedDate=System.DateTime.Today;
			DateTime objReturnedToWorkDate=System.DateTime.Today;
			TimeSpan objDays;
			int iDayOfWeekStart=0;
			int iDayOfWeekEnd=0;
			int iNewDays=0;
			int iDays=0;
			bool bCount;
			int iReturnEid=0;
			string sParent=string.Empty;
			objLastWorkedDate=sLastWorkDate.AddDays(1);
			objReturnedToWorkDate=sReturnedToWorkDate;
			objDays=sReturnedToWorkDate.Subtract(objLastWorkedDate);
			iDays=objDays.Days;
			iDayOfWeekStart = (int)objLastWorkedDate.DayOfWeek ;
			iDayOfWeekEnd=(int)objReturnedToWorkDate.DayOfWeek ;
			subInitHolidayInfo();
			for(int i=1; i<=iDays; i++)
			{
				if(sDaysOfWeek[(int) objLastWorkedDate.AddDays(i-1).DayOfWeek]=="True")
				{
					/*if(objClaim.Context.InternalSettings.SysSettings.ExclHolidays==false)
					{
						bCount=false;
						for(int j=0; j<=SYS_HOLIDAYS.GetUpperBound(SYS_HOLIDAYS.Length-1); j++)
						{
							if(objLastWorkedDate.AddDays(i-1).ToShortDateString()== SYS_HOLIDAYS[j].sDate)
							{
								for(int k=0; k<= SYS_HOLIDAYS.GetUpperBound(SYS_HOLIDAYS.Length-1); k++)
								{
									iReturnEid=0;
									sParent=objClaim.Context.InternalSettings.CacheFunctions.GetOrgParent(0,Conversion.ConvertObjToInt(SYS_HOLIDAYS[j].lOrgID[k], base.ClientId),0,ref iReturnEid);
									if(SYS_HOLIDAYS[j].lOrgID[k]== iReturnEid)
										bCount=true;
								}
							}
						}
						if(bCount==false)
							iNewDays=iNewDays+1;
					}
					else
					{*/
						iNewDays=iNewDays+1;
					//}
				}
			}
			return iNewDays;

		}

		private void LoadPiEmployeeChilds(ref XmlDocument objXML)
		{
			//Creates Node PiXDiagHist for Diagnosis History for Medical Info and Case Management.
			//Added By Rahul.
			XmlNode objOldNode = null;
			XmlDocument objTempDoc=new XmlDocument();
			XmlElement objElem=null;
			objElem=null;
			
//			string sSQL="SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'CASE_MGT_FLAG'";
//			objReader=DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString,sSQL);
//			if(objReader!=null)
//			{
//				while(objReader.Read())
//				{
//					iUseCaseMgmt=Conversion.ConvertObjToInt(objReader.GetValue("STR_PARM_VALUE"), base.ClientId);
//				}
//			}
			objOldNode = objXML.SelectSingleNode("//PiXDiagHistList");
			PiXDiagHist objDiaHist=null;
            PiXDiagHist objDiaHist10 = null;
			#region Load the Highest Diagnosis History
			int iMaxPiXDiagrowId=0;
			int iMaxDays=0;
			int iMinDays=0;
			int iOptDays=0;
            string pirowid = objClaim.PrimaryPiEmployee.PiRowId.ToString();    //MITS 32423 :Praveen ICD-10 Changes
            objDiaHist = objClaim.PrimaryPiEmployee.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHistbyPiRowId(pirowid);    //MITS 32423 :Praveen ICD-10 Changes
            objDiaHist10 = objClaim.PrimaryPiEmployee.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHistICD10byPiRowId(pirowid);

			//if(objClaim.PrimaryPiEmployee.PiXDiagHistList.Count!=0)
            //if (objDiaHist != null)
            //{
            //    foreach(PiXDiagHist objPixDiagHist in objClaim.PrimaryPiEmployee.PiXDiagHistList)
            //    {
            //        if(objPixDiagHist.PiXDiagrowId > iMaxPiXDiagrowId)
            //        {
            //            iMaxPiXDiagrowId=objPixDiagHist.PiXDiagrowId;
            //        }
            //    }
            //    objElem=objXML.CreateElement("PiXDiagHistList");
            //    if (iMaxPiXDiagrowId==0)
            //        objDiaHist=(PiXDiagHist)objClaim.Context.Factory.GetDataModelObject("PiXDiagHist",false);
            //    else
            //        objDiaHist=objClaim.PrimaryPiEmployee.PiXDiagHistList[iMaxPiXDiagrowId];
            //    objElem.InnerXml=objDiaHist.SerializeObject();
            //    iMaxDays=objDiaHist.DisMaxDuration;
            //    iMinDays=objDiaHist.DisMinDuration;
            //    iOptDays=objDiaHist.DisOptDuration;
            //}
            //else
            //{
            //    objDiaHist=(PiXDiagHist)objClaim.Context.Factory.GetDataModelObject("PiXDiagHist",false);
            //    objElem=objXML.CreateElement("PiXDiagHistList");
            //    objElem.InnerXml=objDiaHist.SerializeObject();
            //}
            //if( objOldNode != null )
            //    objXML.DocumentElement.ReplaceChild(objElem,objOldNode);
            //else
            //    objXML.DocumentElement.AppendChild(objElem);


            if (objDiaHist != null)
            {
                iMaxDays = objDiaHist.DisMaxDuration;
                iMinDays = objDiaHist.DisMinDuration;
                iOptDays = objDiaHist.DisOptDuration;
            }
            else
                objDiaHist = objClaim.Context.Factory.GetDataModelObject("PiXDiagHist", false) as PiXDiagHist;

            objElem = objXML.CreateElement("PiXDiagHistList");
            objElem.InnerXml = objDiaHist.SerializeObject();

            if (objOldNode != null)
                objXML.DocumentElement.ReplaceChild(objElem, objOldNode);
            else
                objXML.DocumentElement.AppendChild(objElem);

			//create dummy sysex nodes for Binding Min/Max/Opt values
            //base.ResetSysExData("PiXDiagHist_DisMaxDuration", objDiaHist.DisMaxDuration.ToString());
            //base.ResetSysExData("PiXDiagHist_DisMinDuration", objDiaHist.DisMinDuration.ToString());
            //base.ResetSysExData("PiXDiagHist_DisOptDuration", objDiaHist.DisOptDuration.ToString());
    //MITS 32423 : Praveen Load ICD10 Diagonosis start
            objElem = null;
           // objDiaHist = null;

            //objDiaHist10 = objClaim.PrimaryPiEmployee.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHistICD10byPiRowId(pirowid);
            if (objDiaHist10 != null)
            {
                //Can modify data here
            }
            else
                objDiaHist10 = objClaim.Context.Factory.GetDataModelObject("PiXDiagHist", false) as PiXDiagHist;

            //tkatsarski: 20/01/15 Start changes for RMA-3912
            objOldNode = objXML.SelectSingleNode("//PiXDiagHistICD10List");
            objElem = objXML.CreateElement("PiXDiagHistICD10List");
            objElem.InnerXml = objDiaHist10.SerializeObject();

            if (objOldNode != null)
                objXML.DocumentElement.ReplaceChild(objElem, objOldNode);
            else
                objXML.DocumentElement.AppendChild(objElem);
            //tkatsarski: 20/01/15 End changes for RMA-3912

            base.ResetSysExData("ICDFlag", "0");

            string latestICD = string.Empty;

            //if (objDiaHist.PiXDiagrowId > objDiaHist10.PiXDiagrowId)
            //{
            //    if (!string.IsNullOrEmpty(objDiaHist.DisMdaTopic))
            //    {
            //        latestICD = "ICD9";
            //    }
            //    else
            //    {
            //        latestICD = "ICD10";
            //    }
            //}
            //else
            //{
            //    if (!string.IsNullOrEmpty(objDiaHist10.DisMdaTopic))
            //    {
            //        latestICD = "ICD10";
            //    }
            //    else
            //    {
            //        latestICD = "ICD9";
            //    }
            //}

           //if (objDiaHist.PiXDiagrowId > objDiaHist10.PiXDiagrowId && !string.IsNullOrEmpty(objDiaHist.DisMdaTopic))
            if (objDiaHist.PiXDiagrowId > objDiaHist10.PiXDiagrowId)
            {
                base.ResetSysExData("PiXDiagHist_DisMaxDuration", objDiaHist.DisMaxDuration.ToString());
                base.ResetSysExData("PiXDiagHist_DisMinDuration", objDiaHist.DisMinDuration.ToString());
                base.ResetSysExData("PiXDiagHist_DisOptDuration", objDiaHist.DisOptDuration.ToString());
                base.ResetSysExData("DisMdaTopic", objDiaHist.DisMdaTopic.ToString());
                base.ResetSysExData("DisFactor", objDiaHist.DisFactor.ToString());
            }
            else
            {
                base.ResetSysExData("PiXDiagHist_DisMaxDuration", objDiaHist10.DisMaxDuration.ToString());
                base.ResetSysExData("PiXDiagHist_DisMinDuration", objDiaHist10.DisMinDuration.ToString());
                base.ResetSysExData("PiXDiagHist_DisOptDuration", objDiaHist10.DisOptDuration.ToString());
                base.ResetSysExData("DisMdaTopic", objDiaHist10.DisMdaTopic.ToString());
                base.ResetSysExData("DisFactor", objDiaHist10.DisFactor.ToString());
            }

            objDiaHist10 = null;
            objDiaHist = null;
            //MITS 32423 : Praveen Load ICD10 Diagonosis End
            //zmohammad MITs 35169 : Checking for SMS permission.
            if (!objClaim.Context.RMUser.IsAllowedEx(this.SecurityId, FormBase.RMO_ALLOW_EDIT_MDA_DURATIONS))
            {
                base.AddReadOnlyNode("mdaminimumdays1");
                base.AddReadOnlyNode("mdaoptimumdays1");
                base.AddReadOnlyNode("mdamaximumdays1");
            }
            //zmohammad MITs 35169 : Checking for SMS permission.

			#endregion

			#region Load MMI History
			//Create XML for MMI History . Added by Rahul Sharma
			objOldNode=null;
			objOldNode = objXML.SelectSingleNode("//PiXMMIHistList");
			objElem=null;
			int iMaxPiXMMIHistid=0;
			PiXMMIHist objXMMIHist=null;
			if(objClaim.PrimaryPiEmployee.PiXMMIHistList.Count!=0)
			{
				//Finding the max. row id to display MMI History.
				foreach(PiXMMIHist objPiXMMIHist in objClaim.PrimaryPiEmployee.PiXMMIHistList)
				{
					if(objPiXMMIHist.PiXMmirowId > iMaxPiXMMIHistid)
					{
                        //sgoel6 04/28/2009 MITS 14887
                        if (objPiXMMIHist.DeletedFlag != -1)
							iMaxPiXMMIHistid=objPiXMMIHist.PiXMmirowId;
					}
				}					
				objElem=objXML.CreateElement("PiXMMIHistList");
				if (iMaxPiXMMIHistid==0)
					objXMMIHist=(PiXMMIHist)objClaim.Context.Factory.GetDataModelObject("PiXMMIHist",false);
				else
					objXMMIHist=objClaim.PrimaryPiEmployee.PiXMMIHistList[iMaxPiXMMIHistid];

				//Creating XML for MMI History
				objElem.InnerXml=objXMMIHist.SerializeObject();
			}
			else
			{
				objElem=objXML.CreateElement("PiXMMIHistList");
				objXMMIHist=(PiXMMIHist)objClaim.Context.Factory.GetDataModelObject("PiXMMIHist",false);
				objElem.InnerXml=objXMMIHist.SerializeObject();
			}
			if( objOldNode != null )
				objXML.DocumentElement.ReplaceChild(objElem,objOldNode);
			else
				objXML.DocumentElement.AppendChild(objElem);
			//End of MMI History.
			#endregion
			if(iUseCaseMgmt==-1)
			{

				#region Load Primary Case Manager
				//Creating XML for case Management History -- Added By Rahul Sharma.
				CmXCmgrHist objCmMgrHist=null;
				objOldNode=null;
				objOldNode = objXML.SelectSingleNode("//CmXCmgrHistList");
				objElem=null;
				int iCaseMgmtHisId=0;
				if(objClaim.CaseManagement.CaseMgrHistList.Count!=0)
				{
					foreach(CmXCmgrHist objCmXCmgrHist in objClaim.CaseManagement.CaseMgrHistList)
					{
						if(objCmXCmgrHist.PrimaryCmgrFlag)
						{
							iCaseMgmtHisId=objCmXCmgrHist.CmcmhRowId;
						}
					}		
					if (iCaseMgmtHisId!=0)
					{
						objCmMgrHist=objClaim.CaseManagement.CaseMgrHistList[iCaseMgmtHisId];

						int iCaseMgrDays=0;
						TimeSpan objDateDiff;
						objElem=objXML.CreateElement("CmXCmgrHistList");
						objElem.InnerXml=objCmMgrHist.SerializeObject();
						if(objCmMgrHist.DateClosed=="" || objCmMgrHist.DateClosed==null)
						{
							if(objCmMgrHist.RefDate=="" || objCmMgrHist.RefDate==null)
								iCaseMgrDays=0;
							else
							{
								objDateDiff=System.DateTime.Today.Subtract(Conversion.ToDate(objCmMgrHist.RefDate));
								iCaseMgrDays=objDateDiff.Days;
							}
						}
						else
						{
							if(objCmMgrHist.RefDate=="" || objCmMgrHist.RefDate==null)
								iCaseMgrDays=0;
							else
							{
								objDateDiff=Conversion.ToDate(objCmMgrHist.DateClosed).Subtract(Conversion.ToDate(objCmMgrHist.RefDate));
								iCaseMgrDays=objDateDiff.Days;
							}
						}
						if(iCaseMgrDays<0)
							base.ResetSysExData("CaseMgrDays","");
						else
							base.ResetSysExData("CaseMgrDays",iCaseMgrDays.ToString());
					}
					else
					{
						objElem=objXML.CreateElement("CmXCmgrHistList");
						objCmMgrHist=(CmXCmgrHist)objClaim.Context.Factory.GetDataModelObject("CmXCmgrHist",false);
						objElem.InnerXml=objCmMgrHist.SerializeObject();
						base.ResetSysExData("CaseMgrDays","");
					}
				}
				else
				{
					objElem=objXML.CreateElement("CmXCmgrHistList");
					objCmMgrHist=(CmXCmgrHist)objClaim.Context.Factory.GetDataModelObject("CmXCmgrHist",false);
					objElem.InnerXml=objCmMgrHist.SerializeObject();
					base.ResetSysExData("CaseMgrDays","");
				}
				if( objOldNode != null )
					objXML.DocumentElement.ReplaceChild(objElem,objOldNode);
				else
					objXML.DocumentElement.AppendChild(objElem);
				#endregion

				#region Load Work Loss
				objOldNode=null;
				objOldNode = objXML.SelectSingleNode("//PiXWorkLossList");
				objElem=null;
				int iPiXWorkLoss=0;
				PiXWorkLoss objPiX_WorkLoss=null;
				if(objClaim.PrimaryPiEmployee.PiXWorkLossList.Count!=0)
				{
					DateTime objDate1=System.DateTime.Today;
					DateTime objDate2=System.DateTime.Today;
					foreach(PiXWorkLoss objPiXWorkLoss in objClaim.PrimaryPiEmployee.PiXWorkLossList)
					{
						iPiXWorkLoss=objPiXWorkLoss.PiWlRowId;
						break;
					}
					objPiX_WorkLoss=objClaim.PrimaryPiEmployee.PiXWorkLossList[iPiXWorkLoss];
					if(objClaim.CaseManagement.EstRetWorkDate!="")
						objDate2=Conversion.ToDate(objClaim.CaseManagement.EstRetWorkDate);
					objDate1=Conversion.ToDate(objPiX_WorkLoss.DateLastWorked);
					if(objClaim.PrimaryPiEmployee.JobClassCode!=0)
					{
						DateTime objMaxDate=System.DateTime.Today;
						DateTime objMinDate=System.DateTime.Today;
						DateTime objOptDate=System.DateTime.Today;
						if(objPiX_WorkLoss.DateLastWorked!="")
						{
							objMaxDate= Conversion.ToDate(objPiX_WorkLoss.DateLastWorked).AddDays(Conversion.ConvertObjToDouble(iMaxDays, base.ClientId)+1);
							base.ResetSysExData("WL_MaxDate",objMaxDate.ToShortDateString());
							objMinDate= Conversion.ToDate(objPiX_WorkLoss.DateLastWorked).AddDays(Conversion.ConvertObjToDouble(iMinDays, base.ClientId)+1);
							base.ResetSysExData("WL_MinDate",objMinDate.ToShortDateString());
							objOptDate= Conversion.ToDate(objPiX_WorkLoss.DateLastWorked).AddDays(Conversion.ConvertObjToDouble(iOptDays, base.ClientId)+1);
							base.ResetSysExData("WL_OptDate",objOptDate.ToShortDateString());
						}
						else
						{
							base.ResetSysExData("WL_MaxDate","");
							base.ResetSysExData("WL_MinDate","");
							base.ResetSysExData("WL_OptDate","");
						}
					}
					else
					{
						base.ResetSysExData("EstDisability","");	// Nikhil Garg 01.16.2006   Needs to get created if not already
						base.ResetSysExData("WL_MaxDate","");		// Nikhil Garg 01.16.2006   Needs to get created if not already
						base.ResetSysExData("WL_MinDate","");		// Nikhil Garg 01.16.2006   Needs to get created if not already
						base.ResetSysExData("WL_OptDate","");		// Nikhil Garg 01.16.2006   Needs to get created if not already
					}
					int iTotalLostDays=0;
					int iEstDisability=0;
					if(objPiX_WorkLoss.DateReturned=="")
					{
						base.ResetSysExData("ChkWorkLoss","True");
//						DateTime sDate2= System.DateTime.Today;
//						if(objClaim.CaseManagement.EstRetWorkDate!="")
//							sDate2=Conversion.ToDate(objClaim.CaseManagement.EstRetWorkDate);
						iEstDisability = GetWorkLossDayCount(Conversion.ToDate(objPiX_WorkLoss.DateLastWorked),objDate2,sDaysOfWeek);
						base.ResetSysExData("EstDisability",iEstDisability.ToString()+ " Days");
						int i=0;
						foreach(PiXWorkLoss objPiXWorkLoss in objClaim.PrimaryPiEmployee.PiXWorkLossList)
						{
							if(i>0)
								iTotalLostDays=iTotalLostDays +objPiXWorkLoss.Duration;
							++i;
						}
						iTotalLostDays=iTotalLostDays + GetWorkLossDayCount(Conversion.ToDate(objPiX_WorkLoss.DateLastWorked),System.DateTime.Today,sDaysOfWeek);
					}
					else
					{
						base.ResetSysExData("ChkWorkLoss","False");	
						
						foreach(PiXWorkLoss objPiXWorkLoss in objClaim.PrimaryPiEmployee.PiXWorkLossList)
							iTotalLostDays=iTotalLostDays +objPiXWorkLoss.Duration;

//						base.ResetSysExData("EstDisability","");   // JP 12.20.2005   Needs to get created if not already.
//						base.ResetSysExData("WL_MaxDate","");		// JP 12.20.2005   Needs to get created if not already.
//						base.ResetSysExData("WL_MinDate","");		// JP 12.20.2005   Needs to get created if not already.
//						base.ResetSysExData("WL_OptDate","");		// JP 12.20.2005   Needs to get created if not already.
					}
					base.ResetSysExData("TotalLostDays",iTotalLostDays.ToString()+" Days");
					objElem=objXML.CreateElement("PiXWorkLossList");
					objElem.InnerXml=objPiX_WorkLoss.SerializeObject();
				}
				else
				{
					objElem=objXML.CreateElement("PiXWorkLossList");
					objPiX_WorkLoss=(PiXWorkLoss)objClaim.Context.Factory.GetDataModelObject("PiXWorkLoss",false);
					objElem.InnerXml=objPiX_WorkLoss.SerializeObject();
					base.ResetSysExData("TotalLostDays","");
					base.ResetSysExData("ChkWorkLoss","False");
					base.ResetSysExData("WL_MaxDate","");
					base.ResetSysExData("WL_MinDate","");
					base.ResetSysExData("WL_OptDate","");
					base.ResetSysExData("EstDisability","");
				}
				if( objOldNode != null )
					objXML.DocumentElement.ReplaceChild(objElem,objOldNode);
				else
					objXML.DocumentElement.AppendChild(objElem);
				#endregion

				#region Load Restriction
				// Creating XML for PI Restriction--- Added By Rahul Sharma.
				objOldNode=null;
				objOldNode = objXML.SelectSingleNode("//PiXRestrictList");
				objElem=null;
				int iPiXRestrict=0;
				PiXRestrict objPiX_Restrict=null;
				if(objClaim.PrimaryPiEmployee.PiXRestrictList.Count!=0)
				{
					foreach(PiXRestrict objPiXRestrict in objClaim.PrimaryPiEmployee.PiXRestrictList)
					{
						iPiXRestrict=objPiXRestrict.PiRestrictRowId;
						break;
					}
					string sDateLastRes="False";
					int iEstLenDis=0;
					int iTotalRestDays=0;					
					objPiX_Restrict=objClaim.PrimaryPiEmployee.PiXRestrictList[iPiXRestrict];				
					objElem=objXML.CreateElement("PiXRestrictList");
					objElem.InnerXml=objPiX_Restrict.SerializeObject();					
										
					if(objPiX_Restrict.DateLastRestrct=="")
					{
						int i=0;
						foreach(PiXRestrict objPiXRestrict in objClaim.PrimaryPiEmployee.PiXRestrictList)
						{
							if(i>0)
								iTotalRestDays=iTotalRestDays +objPiXRestrict.Duration;
							++i;
						}
						DateTime sDate2=System.DateTime.Today;
						sDateLastRes="True";
						if(objClaim.CaseManagement.EstRelRstDate!="")
							sDate2=Conversion.ToDate(objClaim.CaseManagement.EstRelRstDate);
						iEstLenDis=GetWorkDay(Conversion.ToDate(objPiX_Restrict.DateFirstRestrct),sDate2,sDaysOfWeek);
						base.ResetSysExData("EstLenDis",iEstLenDis.ToString()+" Days");
						iTotalRestDays = iTotalRestDays + GetWorkDay(Conversion.ToDate(objPiX_Restrict.DateFirstRestrct),System.DateTime.Today,sDaysOfWeek);
						base.ResetSysExData("TotalResDays",iTotalRestDays.ToString() + " Days");
					}
					else
					{
						foreach(PiXRestrict objPiXRestrict in objClaim.PrimaryPiEmployee.PiXRestrictList)
							iTotalRestDays=iTotalRestDays +objPiXRestrict.Duration;
						base.ResetSysExData("EstLenDis","");
						base.ResetSysExData("TotalResDays",iTotalRestDays.ToString() + " Days");
					}
					base.ResetSysExData("ChkCurrResDays",sDateLastRes);
					if(objClaim.PrimaryPiEmployee.JobClassCode!=0)
					{
						DateTime objMaxDate=System.DateTime.Today;
						DateTime objMinDate=System.DateTime.Today;
						DateTime objOptDate=System.DateTime.Today;
						if(objPiX_Restrict.DateFirstRestrct!="")
						{
                            objMaxDate = Conversion.ToDate(objPiX_Restrict.DateFirstRestrct).AddDays(Conversion.ConvertObjToDouble(iMaxDays, base.ClientId));
							base.ResetSysExData("RW_MaxDate",objMaxDate.ToShortDateString());
                            objMinDate = Conversion.ToDate(objPiX_Restrict.DateFirstRestrct).AddDays(Conversion.ConvertObjToDouble(iMinDays, base.ClientId));
							base.ResetSysExData("RW_MinDate",objMinDate.ToShortDateString());
                            objOptDate = Conversion.ToDate(objPiX_Restrict.DateFirstRestrct).AddDays(Conversion.ConvertObjToDouble(iOptDays, base.ClientId));
							base.ResetSysExData("RW_OptDate",objOptDate.ToShortDateString());
						}
						else
						{
							base.ResetSysExData("RW_MaxDate","");
							base.ResetSysExData("RW_MinDate","");
							base.ResetSysExData("RW_OptDate","");
						}
					}
					else
					{
						base.ResetSysExData("RW_MaxDate","");		// Nikhil Garg 01.16.2006   Needs to get created if not already
						base.ResetSysExData("RW_MinDate","");		// Nikhil Garg 01.16.2006   Needs to get created if not already
						base.ResetSysExData("RW_OptDate","");		// Nikhil Garg 01.16.2006   Needs to get created if not already
					}
				}
				else
				{
					objElem=objXML.CreateElement("PiXRestrictList");
					objPiX_Restrict=(PiXRestrict)objClaim.Context.Factory.GetDataModelObject("PiXRestrict",false);
					objElem.InnerXml=objPiX_Restrict.SerializeObject();
					base.ResetSysExData("ChkCurrResDays","False");
					base.ResetSysExData("RW_MaxDate","");
					base.ResetSysExData("RW_MinDate","");
					base.ResetSysExData("RW_OptDate","");
					base.ResetSysExData("EstLenDis","");
					base.ResetSysExData("TotalResDays","");
				}
				if( objOldNode != null )
					objXML.DocumentElement.ReplaceChild(objElem,objOldNode);
				else
					objXML.DocumentElement.AppendChild(objElem);
				#endregion			
				//ybhaskar: MITS 15274: Added nodes workloss and restriction to kill node list
				base.AddKillNode("employeeeventdetail");
                base.AddKillNode("employeerestrictions1");
                base.AddKillNode("employeeworkloss1");
			}
			else
			{
				base.AddKillNode("mdatopic");
				base.AddKillNode("mdafactor");
				base.AddKillNode("casemgt");				
                //ybhaskar: MITS 15274: Added casemanager, treatment plan, medicalmgmtsavings to killnode list
                base.AddKillNode("casemanagerhist1");
                base.AddKillNode("treatmentplan");
                base.AddKillNode("medicalmgmtsavings");
			}
		}
		#endregion

		//update the claim object with data for Event 
		//which is in SysEx node
		public override void OnUpdateObject()
		{
			XmlDocument objXML = base.SysView;
           
            //Raman: MITS 10780 
            //if Disability To Date has been changed by user then it makes sense to clean Payment End date as they need to be
            //recalculated according to new values
            //if (Conversion.ToDbDate(Conversion.ToDate(objXML.SelectSingleNode("//control[@name = 'distodate']").InnerText)) != objClaim.DisabilToDate)
            //{
            //    objClaim.BenCalcPayTo = null;
            //}

            //For new claim, InitCClaim custom script could set value for Comments.
            //If LegacyComments is true, we need to bring value back
            objClaim.UpdateObjectComments(base.SysEx, objClaim.ClaimId);

            //Raman 08/28/2008 : We need to copy SysEx from Event
            //PJS 09-04-2008 : Chamges Mad for R5- not used Sysview
            if (m_fda.HasParam("SysPropertyStore"))
                objXML.InnerXml = m_fda.SafeParam("SysPropertyStore").InnerXml;
            if (objXML.SelectSingleNode("/Instance/Claim/DisabilToDate") != null)  //pmittal5 Mits 16419 05/13/09
            {
                if (Conversion.ToDbDate(Conversion.ToDate(objXML.SelectSingleNode("/Instance/Claim/DisabilToDate").InnerText)) != objClaim.DisabilToDate)
                {
                    objClaim.BenCalcPayTo = null;
                }
            }

            //Raman 08/28/2008 : We need to copy SysEx from Event

            //Storing Current SysEx in an xmldocument
            Event objEvent = (objClaim.Parent as Event);
            objXML = base.SysEx;
            XmlElement oCurSysExele = (XmlElement)base.SysEx.SelectSingleNode("//Parent");
            if (oCurSysExele != null)
            {
                string sCurrentSysEx = oCurSysExele.InnerXml;
                XmlDocument oCurrentSysEx = new XmlDocument();
                oCurrentSysEx.InnerXml = sCurrentSysEx;

                //Preparing SysEx from Event

                base.SysEx.SelectSingleNode("//Parent").InnerXml = objEvent.SerializeObject();
                base.CopyNodes(ref objXML, oCurrentSysEx, "Event");
            }

            base.OnUpdateObject();

            // BSB 06.15.2006 If the claim and event are "out of sync" after the 
			// populateObject call made by default against the objClaim object with the 
			// XML from propertyStore, we may need to handle this manually.
			// This is a possibility on new claims as a result of Init Scripts loading the parent event
			// and then a UI lookup requesting a different (already existing) parent explicitly.
			// This situation is only applicable to the claim screens.  It happens when the default "screen" 
			// represented object is not the logical "root".  (Here it's logically event but implemented as claim)

            
            if(objClaim.EventId != objEvent.EventId)
			{
				objClaim.Parent=null;
				objEvent=(objClaim.Parent as Event);
			}
			objEvent.PopulateObject(Utilities.XPointerDoc("//Parent/Instance",base.SysEx),true);

			//update Claimnant Attorney for the Claim
			if (objClaim.PrimaryClaimant!=null)
            {
                if (objXML.SelectSingleNode("//ClaimantAttorney") != null)   //pmittal5 Mits 16419 05/13/09
			        objClaim.PrimaryClaimant.AttorneyEid=Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//ClaimantAttorney").Attributes["codeid"].Value);
                //JIRA RMA-11122 ajohari2 : Start
                    if (objXML.SelectSingleNode("//ClaimantStatusCode/@codeid") != null)
                        objClaim.PrimaryClaimant.ClaimantStatusCode = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//ClaimantStatusCode").Attributes["codeid"].Value);
                //JIRA RMA-11122 ajohari2 : End
            }

			//Raman: MITS 10780
            //if Class has been changed by user then it makes sense to clean Benefit Calculation End dates as they need to be
            //recalculated according to new class
            if (Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//ClassList").Attributes["codeid"].Value) != objClaim.ClassRowId)
            {
                objClaim.BenCalcPayStart = null;
                objClaim.BenCalcPayTo = null;
                objClaim.BenefitsThrough = null;
                objClaim.BenefitsStart = null;

            }
                
            //update classId for this claim
			objClaim.ClassRowId=Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//ClassList").Attributes["codeid"].Value);
            //MITS 12902 : Umesh 
            DataModelFactory objDataModelFactory =null;
            objDataModelFactory = new DataModelFactory(base.Adaptor.userLogin,base.ClientId);//psharma206
            DisabilityClass objDisabilityClass = null;
            objDisabilityClass = objDataModelFactory.GetDataModelObject("DisabilityClass",false) as DisabilityClass;
            if (objClaim.ClassRowId != 0)
                objDisabilityClass.MoveTo(objClaim.ClassRowId);
            //pmittal5 Mits 13522 04/16/09 - Set Offset Amounts only if they are not on Non-Occ Payments screen
            if (objClaim.PensionAmt == 0 && objClaim.SsAmt == 0 && objClaim.OtherAmt == 0 && objClaim.OtherOffset1 == 0 && objClaim.OtherOffset2 == 0 && objClaim.OtherOffset3 == 0 && objClaim.PostTaxDed1 == 0 && objClaim.PostTaxDed2 == 0)
            {
                objClaim.PensionAmt = objDisabilityClass.PensionAmt;
                objClaim.SsAmt = objDisabilityClass.SsAmt;
                objClaim.OtherAmt = objDisabilityClass.OtherAmt;
                //Animesh Inserted For GHS Enhancement  //pmittal5 Mits 14841
                objClaim.OtherOffset1 = objDisabilityClass.OtherOffset1;
                objClaim.OtherOffset2 = objDisabilityClass.OtherOffset2;
                objClaim.OtherOffset3 = objDisabilityClass.OtherOffset3;
                objClaim.PostTaxDed1 = objDisabilityClass.PostTaxDed1;
                objClaim.PostTaxDed2 = objDisabilityClass.PostTaxDed2;
                //Animesh Insertion Ends
            }
            //MITS 12902 : Umesh
			//update Claim Status History Fields from SysExData
			objClaim.StatusApprovedBy=objXML.SelectSingleNode("//StatusApprovedBy").InnerText;
			objClaim.StatusDateChg=objXML.SelectSingleNode("//StatusDateChg").InnerText;
			objClaim.StatusReason=objXML.SelectSingleNode("//StatusReason").InnerText;

			//Added By Rahul for saving Diagnosis History

       //MITS 32423 : Praveen ICD10 Changes start
            
            XmlDocument objDoc=new XmlDocument();
            //////DataModelFactory m_objDataModelFactory = null;
            //////m_objDataModelFactory = objClaim.Context.Factory;

            //////XmlElement objDiagHistElem=(XmlElement)objXML.SelectSingleNode("//SysExData//PiXDiagHistList");
            //////if (objDiagHistElem!=null)
            //////{
            //////    objDoc.LoadXml(objXML.SelectSingleNode("//SysExData//PiXDiagHistList").InnerXml);
            //////    PiXDiagHist objPiXDiagHist = (PiXDiagHist)objClaim.Context.Factory.GetDataModelObject( "PiXDiagHist" , false );
            //////    objPiXDiagHist.PopulateObject(objDoc);
            //////    PiXDiagHist objMostRecent = objClaim.PrimaryPiEmployee.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHist();
            //////    //  npadhy MITS 18552 MDA Topic not getting saved
            //////    if (DiagHistIsModified(objMostRecent, objPiXDiagHist))
            //////    {
            //////        objPiXDiagHist.PiXDiagrowId = -1;
            //////        objPiXDiagHist.ChgdByUser = m_objDataModelFactory.Context.RMUser.LoginName;
            //////        // npadhy Start MITS 18544 Do not default the Date to today's date. Rather We should save the Date which is passed from UI
            //////        //objPiXDiagHist.DateChanged = System.DateTime.Today.ToShortDateString();
            //////        // npadhy End MITS 18544 
            //////        objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist);
            //////    }
            //////}

            try
            {
                bool addICD10 = false;
                bool addICD9 = false;
                string ICDFlag = string.Empty;
                if (objXML.SelectSingleNode("//SysExData//ICDFlag") != null)
                    ICDFlag = objXML.SelectSingleNode("//SysExData//ICDFlag").InnerXml.ToString();
                else
                    ICDFlag = "0";

                PiXDiagHist objPiXDiagHist = null;
                PiXDiagHist objPiXDiagHist10 = null;

                    XmlElement objDiagHistElem = (XmlElement)objXML.SelectSingleNode("//SysExData//PiXDiagHistList");
                    if (objDiagHistElem != null)
                    {
                        objDoc.LoadXml(objXML.SelectSingleNode("//SysExData//PiXDiagHistList").InnerXml);
                        objPiXDiagHist = (PiXDiagHist)objClaim.Context.Factory.GetDataModelObject("PiXDiagHist", false);
                        objPiXDiagHist.PopulateObject(objDoc);
                        //srajindersin MITS 36903 12/08/2014 - Claim Save performance
                        PiXDiagHist objMostRecent = objClaim.PrimaryPiEmployee.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHistbyPiRowId(Convert.ToString(objClaim.PrimaryPiEmployee.PiRowId));

                        //if (ICDFlag == "1" || ICDFlag == "3")
                        //{
                        //    //srajindersin MITS 36903 12/08/2014 - Claim Save performance
                        //    if(DiagHistIsModified(objMostRecent, objPiXDiagHist))
                        //    {
                        //    objPiXDiagHist.IsICD10 = 0;
                        //    objPiXDiagHist.PiXDiagrowId = -1;
                        //    objPiXDiagHist.ChgdByUser = objClaim.Context.RMUser.LoginName;
                        //    //  objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist);
                        //    addICD9 = true;
                        //    }
                        //}
                        //else if (ICDFlag == "0")
                        //{
                        if (DiagHistIsModified(objMostRecent, objPiXDiagHist))
                        {
                            //objPiXDiagHist.PiXDiagrowId = -1;
                            objPiXDiagHist.IsICD10 = 0;
                            objPiXDiagHist.ChgdByUser = objClaim.Context.RMUser.LoginName;
                            //    objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist);
                            addICD9 = true;
                            //}
                        }


                        objDoc = null;
                        //    objPiXDiagHist = null;
                        objMostRecent = null;
                    }
                    XmlElement objDiagHistICD10Elem = (XmlElement)objXML.SelectSingleNode("//SysExData//PiXDiagHistICD10List");
                  //RMA-1209 Starts - ICD 10 Code not getting deleted after adding once
                    if (objDiagHistICD10Elem != null)
                    {
                        //achouhan3  RMA 1308 Starts : copied to maintain Above added ICD-9 otherwise reference clears the previous records
                        //PiEmployee objICD9Employee = (PiEmployee)objClaim.Context.Factory.GetDataModelObject("PiEmployee", false);
                        //if (ICDFlag == "0" || ICDFlag == "3")
                        //{
                        //    foreach (PiXDiagHist objICD9 in objClaim.PrimaryPiEmployee.PiXDiagHistList)
                        //    {
                        //        objICD9Employee.PiXDiagHistList.Add(objICD9);
                        //    }
                        //}
                        //achouhan3  RMA 1308 Ends : copied to maintain Above added ICD-9 otherwise reference clears the previous records
                        objDoc = new XmlDocument();
                        objDoc.LoadXml(objXML.SelectSingleNode("//SysExData//PiXDiagHistICD10List").InnerXml);
                        objPiXDiagHist10 = (PiXDiagHist)objClaim.Context.Factory.GetDataModelObject("PiXDiagHist", false);
                        objPiXDiagHist10.PopulateObject(objDoc);
                        //if (ICDFlag == "2" || ICDFlag == "3")
                        //{
                        //    //srajindersin MITS 36903 12/08/2014 - Claim Save performance
                        //    // RMA 1308 Changed to fetch only ICD-10 records
                        //    PiXDiagHist objICD10MostRecent = objClaim.PrimaryPiEmployee.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHistICD10byPiRowId(Convert.ToString(objClaim.PrimaryPiEmployee.PiRowId));
                        //    if (DiagHistIsModified(objICD10MostRecent, objPiXDiagHist10))
                        //    {
                        //        if (ICDFlag == "3")
                        //            objPiXDiagHist10.PiXDiagrowId = -2;
                        //        else
                        //            objPiXDiagHist10.PiXDiagrowId = -1;

                        //        objPiXDiagHist10.ChgdByUser = objClaim.Context.RMUser.LoginName;
                        //        objPiXDiagHist10.IsICD10 = -1;
                        //        //   objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist10);
                        //        addICD10 = true;
                        //    }
                        //    objICD10MostRecent = null;
                        //    //objDoc = null;  //RMA-1209 commented
                        //    //objPiXDiagHist = null;  ////RMA-1209 Moved it down to work for if and else both clause
                        //}
                        //else if (ICDFlag == "0") ////RMA-1209 new if block. Added to delete code and Changed GetRecent ICD10 record picking
                        //{
                            PiXDiagHist objICD10MostRecent = objClaim.PrimaryPiEmployee.PiXDiagHistList.GetMostRecentlyCommittedPiXDiagHistICD10byPiRowId(Convert.ToString(objClaim.PrimaryPiEmployee.PiRowId));
                            if (DiagHistIsModified(objICD10MostRecent, objPiXDiagHist10))
                            {
                                //objPiXDiagHist10.PiXDiagrowId = -2;
                                objPiXDiagHist10.ChgdByUser = objClaim.Context.RMUser.LoginName;
                                objPiXDiagHist10.IsICD10 = -1;
                                //    objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist10);
                                addICD10 = true;
                            }
                            objICD10MostRecent = null;
                        //}
                        //achouhan3  RMA 1308 Starts : To merge all records and save
                        //if (ICDFlag == "0" || ICDFlag == "3")
                        //{
                        //    foreach (PiXDiagHist objICD9 in objICD9Employee.PiXDiagHistList)
                        //    {
                        //        objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objICD9);
                        //    }
                        //}
                        //achouhan3  RMA 1308 Ends : To merge all records and save

                        objDoc = null;
                        //objPiXDiagHist10 = null;
                    }

                    //if (objPiXDiagHist == null || string.IsNullOrEmpty(objPiXDiagHist.DisMdaTopic))
                    //{
                    //    if (addICD10 == true)
                    //    {
                    //        objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist10);
                    //    }
                    //    if (addICD9 == true)
                    //    {
                    //        objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist);
                    //    }
                    //}
                    //else
                    //{
                    //    if (addICD9 == true)
                    //    {
                    //        objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist);
                    //    }
                    //    if (addICD10 == true)
                    //    {
                    //        objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist10);
                    //    }
                    //}
                    switch (ICDFlag)
                    {
                        case "0":
                            if (objPiXDiagHist.PiXDiagrowId > objPiXDiagHist10.PiXDiagrowId && addICD9)
                            {
                                objPiXDiagHist.PiXDiagrowId = -1;
                                objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist);
                            }
                            else if (addICD10)
                            {
                                objPiXDiagHist10.PiXDiagrowId = -1;
                                objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist10);
                            }
                            break;
                        case "1":
                            if (addICD9)
                            {
                                objPiXDiagHist.PiXDiagrowId = -1;
                                objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist);
                            }
                            break;
                        case "2":
                            if (addICD10)
                            {
                                objPiXDiagHist10.PiXDiagrowId = -1;
                                objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist10);
                            }
                            break;
                        case "3":
                            if (addICD10)
                            {
                                objPiXDiagHist10.PiXDiagrowId = -1;
                                objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist10);
                            }

                            if (addICD9)
                            {
                                objPiXDiagHist.PiXDiagrowId = -1;
                                objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist);
                            }
                            break;
                        case "4":
                            if (addICD9)
                            {
                                objPiXDiagHist.PiXDiagrowId = -1;
                                objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist);
                            }

                            if (addICD10)
                            {
                                objPiXDiagHist10.PiXDiagrowId = -1;
                                objClaim.PrimaryPiEmployee.PiXDiagHistList.Add(objPiXDiagHist10);
                            }
                            break;
                    }
                        //RMA-1209 Ends
                        objDiagHistElem = null;
                        objDiagHistICD10Elem = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
      //MITS 32423 : Praveen ICD10 Changes End

			//Added By Rahul for saving MMI History.
			XmlElement objMMIHistElem=(XmlElement)objXML.SelectSingleNode("//SysExData//PiXMMIHistList");
			if (objMMIHistElem!=null)
			{
				objDoc=new XmlDocument();
				objDoc.LoadXml(objXML.SelectSingleNode("//SysExData/PiXMMIHistList").InnerXml);
				PiXMMIHist objPiXMMIHist = (PiXMMIHist)objClaim.Context.Factory.GetDataModelObject("PiXMMIHist",false);
				objPiXMMIHist.PopulateObject(objDoc);
				objPiXMMIHist.PiXMmirowId=0;
				objClaim.PrimaryPiEmployee.PiXMMIHistList.Add(objPiXMMIHist);
			}
			// Added By Rahul for Case Management.
			if(objClaim.CaseManagement.CaseMgrHistList.Count>0 && iCaseMgmtHisId>0)
			{
				XmlElement objCaseMgmtElem=(XmlElement)objXML.SelectSingleNode("//SysExData//CmXCmgrHistList");
				if (objCaseMgmtElem!=null)
				{
					objDoc=new XmlDocument();
					objDoc.LoadXml(objXML.SelectSingleNode("//SysExData/CmXCmgrHistList").InnerXml);
					CmXCmgrHist objCmMgrHist=null;
					objCmMgrHist=objClaim.CaseManagement.CaseMgrHistList[iCaseMgmtHisId];
					objCmMgrHist.PopulateObject(objDoc);						
				}
			}

			//Nikhil Garg		Dated: 22-Dec-2005
			//If Case Management is On the we should update the Case Management Table
			//In case of new claim we need to create a new row in Case Management Table for the new claim
			//If Case Management has bee turned on just now then also we need to create a new row in 
			//CaseManagement Table if it does not already exists
			//if(iUseCaseMgmt==-1)
				//objClaim.CaseManagement.ClaimId=objClaim.ClaimId;

			objClaim.PlanId=objClaim.DisabilityPlan.PlanId;

            //tkr mits 8764.  auto-populate event location fields
            //MGaba2:MITS 15642:creating the node so that it doesnt come in missing refs:start
            //CreateSysExData("EventOnPremiseChecked", "false");
            //Reload the department data if this node is true
            if (base.GetSysExDataNodeText("/SysExData/EventOnPremiseChecked") == "true")
            {//MGaba2:MITS 15642:End
                PopulateEventDetailLocation(objEvent);
                base.ResetSysExData("EventOnPremiseChecked", "false");//MGaba2:MITS 15642
            }

			//Nikhil Garg		14-Mar-2006
			//set the claim id for aww if needed
			string sSQLString="SELECT Claim_ID FROM CLAIM_AWW WHERE Claim_ID= " + objClaim.ClaimId.ToString();
            using (DbReader objClaimAWW = objClaim.Context.DbConn.ExecuteReader(sSQLString))
            {
                if (objClaimAWW.Read())
                    if (objClaim.ClaimAWW.ClaimId <= 0)
                        objClaim.ClaimAWW.ClaimId = objClaim.ClaimId;
                objClaimAWW.Close();
            }

            //MITS 30386 Start
            if (!objClaim.Context.RMUser.IsAllowedEx(FormBase.RMO_DI_EMPLOYEE, FormBase.RMO_VIEW_SSN))
            {
                if (!objClaim.PrimaryPiEmployee.PiEntity.IsNew)
                {
                    string sTAXSQLString = "SELECT TAX_ID FROM ENTITY WHERE ENTITY_ID= " + objClaim.PrimaryPiEmployee.PiEntity.EntityId.ToString();
                    using (DbReader objClaimAWW = objClaim.Context.DbConn.ExecuteReader(sTAXSQLString))
                    {
                        if (objClaimAWW.Read())
                        {
                            string oldTaxid = Convert.ToString(objClaimAWW[0]);
                            objClaim.PrimaryPiEmployee.PiEntity.TaxId = oldTaxid;
                        }

                    }

                }
            }
            //MITS 30386 End
            //Added Rakhi for R7:Add Emp Data Elements 
            if (base.m_fda.SafeFormVariableParamText("SysCmd") == "5")
            {

                objClaim.PrimaryPiEmployee.PiEntity.FormName = "ClaimDIForm"; //Mits 22497

                
                #region "Updating Address Info Object"

                string sAddr1 = objClaim.PrimaryPiEmployee.PiEntity.Addr1;
                string sAddr2 = objClaim.PrimaryPiEmployee.PiEntity.Addr2;
                string sAddr3 = objClaim.PrimaryPiEmployee.PiEntity.Addr3;// JIRA 6420 pkandhari
                string sAddr4 = objClaim.PrimaryPiEmployee.PiEntity.Addr4;// JIRA 6420 pkandhari
                string sCity = objClaim.PrimaryPiEmployee.PiEntity.City;
                int iCountryCode = objClaim.PrimaryPiEmployee.PiEntity.CountryCode;
                int iStateId = objClaim.PrimaryPiEmployee.PiEntity.StateId;
                string sEmailAddress = objClaim.PrimaryPiEmployee.PiEntity.EmailAddress;
                string sFaxNumber = objClaim.PrimaryPiEmployee.PiEntity.FaxNumber;
                string sCounty = objClaim.PrimaryPiEmployee.PiEntity.County;
                string sZipCode = objClaim.PrimaryPiEmployee.PiEntity.ZipCode;
                //Ashish Ahuja - Address Master
                string sStateDesc = string.Empty;
                string sStateCode = string.Empty;
                string sCountryDesc = string.Empty;
                string sCountryCode = string.Empty;
                string sSearchString = string.Empty;

                if (
                        sAddr1 != string.Empty || sAddr2 != string.Empty || sAddr3 != string.Empty || sAddr4 != string.Empty ||
                        sCity != string.Empty || iCountryCode != 0 || iStateId != 0 || sEmailAddress != string.Empty ||
                        sFaxNumber != string.Empty || sCounty != string.Empty || sZipCode != string.Empty
                    )
                {

                    if (objClaim.PrimaryPiEmployee.PiEntity.EntityXAddressesList.Count == 0)
                    {


                        EntityXAddresses objEntityXAddressesInfo = objClaim.PrimaryPiEmployee.PiEntity.EntityXAddressesList.AddNew();
						//RMA-8753 nshah28(Added by ashish) START
                        objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                        objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                        objEntityXAddressesInfo.Address.Addr3 = sAddr3;// JIRA 6420 pkandhari
                        objEntityXAddressesInfo.Address.Addr4 = sAddr4;// JIRA 6420 pkandhari
                        objEntityXAddressesInfo.Address.City = sCity;
                        objEntityXAddressesInfo.Address.Country = iCountryCode;
                        objEntityXAddressesInfo.Address.State = iStateId;
                        objEntityXAddressesInfo.Email = sEmailAddress;
                        objEntityXAddressesInfo.Fax = sFaxNumber;
                        objEntityXAddressesInfo.Address.County = sCounty;
                        objEntityXAddressesInfo.Address.ZipCode = sZipCode;
						//RMA-8753 nshah28(Added by ashish) END
                        objEntityXAddressesInfo.EntityId = objClaim.PrimaryPiEmployee.PiEntity.EntityId;
                        objEntityXAddressesInfo.PrimaryAddress = -1;
                        objEntityXAddressesInfo.AddressId = 0;
                        objEntityXAddressesInfo.Address.AddressId = 0;
                        objCache.GetStateInfo(iStateId, ref sStateCode, ref sStateDesc);
                        objCache.GetCodeInfo(iCountryCode, ref sCountryCode, ref sCountryDesc);
                        sSearchString = (sAddr1 + sAddr2 + sAddr3 + sAddr4 + sCity + sStateDesc + sCountryDesc + sZipCode + "0").ToLower().Replace(" ", ""); //Remove "County"
                        objEntityXAddressesInfo.Address.SearchString = sSearchString;

                    }
                    else
                    {
                        foreach (EntityXAddresses objEntityXAddressesInfo in objClaim.PrimaryPiEmployee.PiEntity.EntityXAddressesList)
                        {
                            if (objEntityXAddressesInfo.PrimaryAddress == -1)
                            {
							//RMA-8753 nshah28(Added by ashish) START
                                objEntityXAddressesInfo.Address.Addr1 = sAddr1;
                                objEntityXAddressesInfo.Address.Addr2 = sAddr2;
                                objEntityXAddressesInfo.Address.Addr3 = sAddr3;// JIRA 6420 pkandhari
                                objEntityXAddressesInfo.Address.Addr4 = sAddr4;// JIRA 6420 pkandhari
                                objEntityXAddressesInfo.Address.City = sCity;
                                objEntityXAddressesInfo.Address.Country = iCountryCode;
                                objEntityXAddressesInfo.Address.State = iStateId;
                                objEntityXAddressesInfo.Email = sEmailAddress;
                                objEntityXAddressesInfo.Fax = sFaxNumber;
                                objEntityXAddressesInfo.Address.County = sCounty;
                                objEntityXAddressesInfo.Address.ZipCode = sZipCode;
                                objEntityXAddressesInfo.AddressId = 0;
                                objEntityXAddressesInfo.Address.AddressId = 0;
                                objCache.GetStateInfo(iStateId, ref sStateCode, ref sStateDesc);
                                objCache.GetCodeInfo(iCountryCode, ref sCountryCode, ref sCountryDesc);
                                sSearchString = (sAddr1 + sAddr2 + sAddr3 + sAddr4 + sCity + sStateDesc + sCountryDesc + sZipCode + "0").ToLower().Replace(" ", ""); //Remove "County"
                                objEntityXAddressesInfo.Address.SearchString = sSearchString;
								//RMA-8753 nshah28(Added by ashish) END
                                break;
                            }
                        }
                    }
                }
                else
                {
                    if (objClaim.PrimaryPiEmployee.PiEntity.EntityXAddressesList.Count > 0)
                    {
                        foreach (EntityXAddresses objEntityXAddressesInfo in objClaim.PrimaryPiEmployee.PiEntity.EntityXAddressesList)
                        {
                            if (objEntityXAddressesInfo.PrimaryAddress == -1)
                            {
                                objClaim.PrimaryPiEmployee.PiEntity.EntityXAddressesList.Remove(objEntityXAddressesInfo.AddressId);
                                break;
                            }

                        }
                    }

                }
                #endregion

                #region Added for updating Phone Numbers

                string sOfficePhone = objClaim.PrimaryPiEmployee.PiEntity.Phone1.Trim();
                string sHomePhone = objClaim.PrimaryPiEmployee.PiEntity.Phone2.Trim();
                int iHomeCode = objCache.GetCodeId("H", "PHONES_CODES");
                int iOfficeCode = objCache.GetCodeId("O", "PHONES_CODES");
                bool bOfficePhoneEntered = false;
                AddressXPhoneInfo objAddressesInfo = null;
                bool bOfficeCodeExists = false;
                bool bHomePhoneEntered = false;
                bool bHomeCodeExists = false;

                if (sOfficePhone != string.Empty)
                    bOfficePhoneEntered = true;
                if (sHomePhone != string.Empty)
                    bHomePhoneEntered = true;

                if (objClaim.PrimaryPiEmployee.PiEntity.AddressXPhoneInfoList.Count == 0)
                {
                    if (bOfficePhoneEntered)
                    {
                        objAddressesInfo = objClaim.PrimaryPiEmployee.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iOfficeCode;
                        objAddressesInfo.PhoneNo = sOfficePhone;
                    }
                    if (bHomePhoneEntered)
                    {
                        objAddressesInfo = objClaim.PrimaryPiEmployee.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iHomeCode;
                        objAddressesInfo.PhoneNo = sHomePhone;
                    }
                }
                else
                {
                    foreach (AddressXPhoneInfo objXAddressesInfo in objClaim.PrimaryPiEmployee.PiEntity.AddressXPhoneInfoList)
                    {
                        if (objXAddressesInfo.PhoneCode == iOfficeCode)
                        {
                            if (bOfficePhoneEntered)
                            {
                                objXAddressesInfo.PhoneNo = sOfficePhone;
                                bOfficeCodeExists = true;
                            }
                            else
                            {
                                objClaim.PrimaryPiEmployee.PiEntity.AddressXPhoneInfoList.Remove(objXAddressesInfo.PhoneId);
                            }
                            continue;
                        }
                        else if (objXAddressesInfo.PhoneCode == iHomeCode)
                        {
                            if (bHomePhoneEntered)
                            {
                                objXAddressesInfo.PhoneNo = sHomePhone;
                                bHomeCodeExists = true;
                            }
                            else
                            {
                                objClaim.PrimaryPiEmployee.PiEntity.AddressXPhoneInfoList.Remove(objXAddressesInfo.PhoneId);
                            }
                        }
                    }
                    if (bOfficePhoneEntered && !bOfficeCodeExists)
                    {
                        objAddressesInfo = objClaim.PrimaryPiEmployee.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iOfficeCode;
                        objAddressesInfo.PhoneNo = sOfficePhone;
                    }
                    if (bHomePhoneEntered && !bHomeCodeExists)
                    {
                        objAddressesInfo = objClaim.PrimaryPiEmployee.PiEntity.AddressXPhoneInfoList.AddNew();
                        objAddressesInfo.PhoneCode = iHomeCode;
                        objAddressesInfo.PhoneNo = sHomePhone;
                    }
                }

                #endregion
            }
            //Added Rakhi for R7:Add Emp Data Elements

            //tanwar2 - ImageRight - start
            //Generate FUP if text of this node is -1
            if (string.Compare(base.GetSysExDataNodeText("/SysExData/generateFUPFile"), "-1") == 0)
            {
                FUPGenerator oFUPGenerator = new FUPGenerator(base.Adaptor.userLogin, base.ClientId);
                oFUPGenerator.GenerateFUP(objClaim.ClaimId, objClaim);
            }
            //tanwar2 - ImageRight - end
		}//method: OnUpdateObject()

        //  npadhy MITS 18552 MDA Topic not getting saved
        private bool DiagHistIsModified(PiXDiagHist objOrig, PiXDiagHist objNew)
        {
            if (objNew == null)
                return false;
            if (objOrig == null)
            {
                if (objNew.DiagnosisCode != 0)
                    return true;
                else
                    return false;
            }
            if (objOrig.DiagnosisCode != objNew.DiagnosisCode) return true;
            if (objOrig.DateChanged != objNew.DateChanged) return true;
            if (objOrig.ChgdByUser != objNew.ChgdByUser) return true;
            if (objOrig.DisMdaTopic != objNew.DisMdaTopic) return true;
            if (objOrig.DisFactor != objNew.DisFactor) return true;
            if (objOrig.ApprovedByUser != objNew.ApprovedByUser) return true;
            if (objOrig.Reason != objNew.Reason) return true;
            //zmohammad  : MITs 35169 start
            if (objOrig.DisMinDuration != objNew.DisMinDuration) return true;
            if (objOrig.DisOptDuration != objNew.DisOptDuration) return true;
            if (objOrig.DisMaxDuration != objNew.DisMaxDuration) return true;
            // zmohammad : MITs 35169 end
            return false;
        }
		public override void BeforeSave(ref bool Cancel)
		{
            bool bIsSuccess = false;//Add by kuladeep for object reference error mits:25548 07/26/2011
			base.BeforeSave (ref Cancel);
			Event objEvent = objClaim.Parent as Event;

			//Check UserLimit Security.
			if(objLobSettings.ClmAccLmtFlag)//If UserLimits Enabled...
			{
				if(objLimits != null)
                    //Asharma326 MITS 30874 Add Parameter objLobSettings.ClmAccLmtUsrFlag
                    if ((objClaim.IsNew && !objLimits.ClaimCreateAllowed(objClaim.LineOfBusCode, objClaim.ClaimTypeCode, objClaim.ClaimStatusCode, objLobSettings.ClmAccLmtUsrFlag)) ||
                        !objClaim.IsNew && !objLimits.ClaimCreateAllowed(objClaim.LineOfBusCode, objClaim.ClaimTypeCode, objClaim.ClaimStatusCode, objLobSettings.ClmAccLmtUsrFlag))
					{
						Errors.Add(Globalization.GetString("SaveError",base.ClientId),
							String.Format(Globalization.GetString("Save.Claim.UserLimitFailed",base.ClientId), objCache.GetCodeDesc(objClaim.ClaimTypeCode,base.Adaptor.userLogin.objUser.NlsCode),objCache.GetCodeDesc(objClaim.ClaimStatusCode,base.Adaptor.userLogin.objUser.NlsCode)),
							BusinessAdaptorErrorType.Error);  //Aman ML Change
						Cancel = true;
						return;
					}
			}

			//Check Permission to Event Number.
			if(base.ModuleSecurityEnabled)//Is Security Enabled
			{
				if(!objClaim.IsNew)
				{
					if(!m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_ALLOW_EDIT_EVENT_NUM))
					{
						string sPrev = objEvent.Context.DbConnLookup.ExecuteString("SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + objEvent.EventId);
						objEvent.EventNumber = sPrev;
						objClaim.EventNumber = sPrev;
					}
                    //Start:Sumit (10/28/2010) - MITS# 22849 - Update Claim Event number when edited.
                    else
                    {
                        objClaim.EventNumber = objEvent.EventNumber;
                    }
                    //End:Sumit
				}
				else
				{
					if(objEvent.EventId==0)
					{
						if(!m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_ALLOW_ENTRY_EVENT_NUM))
						{
							// JP 10.25.2005    string sPrev = objEvent.Context.DbConnLookup.ExecuteString("SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + objEvent.EventId);
							// JP 10.25.2005    objEvent.EventNumber = sPrev;
							// JP 10.25.2005    objClaim.EventNumber = sPrev;
							objEvent.EventNumber = "";
							objClaim.EventNumber = "";
						}
					}
					else //New Claim, existing Event.
					{
						if(!m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_ALLOW_EDIT_EVENT_NUM))
						{
							string sPrev = objEvent.Context.DbConnLookup.ExecuteString("SELECT EVENT_NUMBER FROM EVENT WHERE EVENT_ID=" + objEvent.EventId);
							objEvent.EventNumber = sPrev;
							objClaim.EventNumber = sPrev;
						}
					}
				}
				//Check Permissions to Claim Number
				if(objClaim.IsNew && !m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_CLAIM_NUMBER_ALLOW_ENTRY))
					// JP 10.25.2005   objClaim.ClaimNumber = objEvent.Context.DbConnLookup.ExecuteString("SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID=" + objClaim.ClaimId);
					objClaim.ClaimNumber = "";
				else if(!objClaim.IsNew && !m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_CLAIM_NUMBER_ALLOW_EDIT))
					objClaim.ClaimNumber = objEvent.Context.DbConnLookup.ExecuteString("SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID=" + objClaim.ClaimId);

				//Check Permission to change Department. --This one is considered fatal.
				if(objEvent.EventId!=0 && objEvent.DeptEid != objEvent.Context.DbConnLookup.ExecuteInt("SELECT DEPT_EID FROM EVENT WHERE EVENT_ID=" + objEvent.EventId))
					if(!m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_CLAIM_CHANGE_DEPARTMENT))
					{
						Errors.Add(Globalization.GetString("SaveError",base.ClientId),
							String.Format(Globalization.GetString("Save.DeptChangeFailed",base.ClientId)),
							BusinessAdaptorErrorType.Error);
						Cancel = true;
						return;
					}
								
				//Check Permission on Claim Status Change
				// Decide if we should close open event diaries after the save...
				if(!objClaim.IsNew)
				{
					int prevCode= objClaim.Context.DbConnLookup.ExecuteInt("SELECT CLAIM_STATUS_CODE FROM CLAIM WHERE CLAIM_ID=" + objClaim.ClaimId);
					int prevParent= objClaim.Context.LocalCache.GetRelatedCodeId(prevCode);
					int curParent = objClaim.Context.LocalCache.GetRelatedCodeId(objClaim.ClaimStatusCode);
					
					if(prevParent!=curParent && curParent==8)
						if(!m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_CLAIM_ALLOW_CLOSE))
						{
							Errors.Add(Globalization.GetString("SaveError",base.ClientId),
								String.Format(Globalization.GetString("Save.ClaimClosePermissionFailed",base.ClientId)),
								BusinessAdaptorErrorType.Error);
							Cancel = true;
							return;
						}

					if(prevParent!=curParent && curParent==9)
						if(!m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_CLAIM_ALLOW_REOPEN))
						{
							Errors.Add(Globalization.GetString("SaveError",base.ClientId),
								String.Format(Globalization.GetString("Save.ClaimReOpenPermissionFailed",base.ClientId)),
								BusinessAdaptorErrorType.Error);
							Cancel = true;
							return;
						}

					//Nikhil Garg		Dated: 27-Jan-06
					//Checking for Permission to update a closed claim
					if (prevParent==curParent && curParent==8)
						if(!m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_CLAIM_UPDATE_CLOSED))
						{
							Errors.Add(Globalization.GetString("SaveError",base.ClientId),
								String.Format(Globalization.GetString("Save.ClaimCloseUpdatePermissionFailed",base.ClientId)),
								BusinessAdaptorErrorType.Error);
							Cancel = true;
							return;
						}
				}
				//bCloseDiaries = (objCache.GetRelatedCodeId(objClaim.ClaimStatusCode)==8);

			}//End "If Module Security"

			//Default the Time of Event & Time Reported
			if(objEvent.TimeOfEvent=="" && objClaim.Context.InternalSettings.SysSettings.DefaultTimeFlag !=-1)
				objEvent.TimeOfEvent= "12:00 AM";
			if(objClaim.TimeOfClaim=="" && objClaim.Context.InternalSettings.SysSettings.DefaultTimeFlag !=-1)
				objClaim.TimeOfClaim= "12:00 AM";
			if(objEvent.TimeReported=="" && objClaim.Context.InternalSettings.SysSettings.DefaultTimeFlag !=-1)
				objEvent.TimeReported= "12:00 AM";
			//Nikhil Garg		Dated: 23-Dec-05	Bug No:1069
			//As per the RMWorld behaviour, there is no need to initialize Time Reported
//			if(objEvent.TimeReported=="" && objEvent.DateReported!="")
//				objEvent.TimeReported= "12:00 AM";

//			// Update Claim Closed Status History and Date Stamp if necessary.
//			ClaimStatusHist objStatusHist = objClaim.ClaimStatusHistList.LastStatusChange();
//			if(objStatusHist !=null)
//				if(objClaim.ClaimStatusCode!=objStatusHist.StatusCode)
//				{
//					ClaimStatusHist objNew  = objClaim.ClaimStatusHistList.AddNew();
//					objNew.StatusCode = objClaim.Context.DbConnLookup.ExecuteInt("SELECT CLAIM_STATUS_CODE FROM CLAIM WHERE CLAIM_ID=" + objClaim.ClaimId);
//					objNew.StatusChgdBy = m_fda.userLogin.LoginName;
//					objNew.DateStatusChgd = Conversion.ToDbDate(DateTime.Now);
//					
//					if(	objClaim.Context.LocalCache.GetRelatedCodeId(objClaim.ClaimStatusCode)==8 ||
//						objClaim.ClaimStatusCode==8)
//					{
//						bCloseDiaries = true;
//						if(objClaim.DttmClosed=="")
//							objClaim.DttmClosed = objNew.DateStatusChgd;
//					}
//				}

			// Update Close Diaries Flag for later if necessary.
            int prevStatusCode = objClaim.Context.DbConnLookup.ExecuteInt("SELECT CLAIM_STATUS_CODE FROM CLAIM WHERE CLAIM_ID=" + objClaim.ClaimId);

            //Changed by Saurabh Arora for MITS 19061:Start
            /* bool bWasClosed = (objClaim.Context.LocalCache.GetRelatedCodeId(prevStatusCode) == 8 || prevStatusCode == 8);
            bool bIsClosed = (objClaim.Context.LocalCache.GetRelatedCodeId(objClaim.ClaimStatusCode) == 8 || objClaim.ClaimStatusCode == 8);
            if (!bWasClosed && bIsClosed)
                bCloseDiaries = true; */
            int bClosedCodeId = objClaim.Context.LocalCache.GetCodeId("C", "Status");
            bCloseDiaries = (objClaim.Context.LocalCache.GetRelatedCodeId(objClaim.ClaimStatusCode) == bClosedCodeId);
            
            //Changed by Saurabh Arora for MITS 19061:End

            //Mridul 05/28/09 MITS 16745-Chubb ACK Letter Enhancement
            if (prevStatusCode != objClaim.ClaimStatusCode)
            {                
                //Commented By Navdeep, Change in table, Closed Status Mapped to SYS_UTIL_CLM_LTR
                //string sStatInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT CLOSED_STATUS FROM SYS_PARMS_CLM_LTR WHERE CLOSED_STATUS LIKE '%," + objClaim.ClaimStatusCode + ",%'"));                
                string sStatInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT COLUMN_DESC FROM SYS_UTIL_CLM_LTR WHERE ROW_ID = 3 AND CODE_EID IN (" + objClaim.ClaimStatusCode + ")"));
                //Check if Current status is among those in the list
                if (sStatInList != "")
                    bTriggerLetter = true;
            }
			//Duplicate Claim Check
			// Mihika 6-Jan-2006 Defect no. 1189
			if(objClaim.IsNew && IsDupeClaim())
			{
//				Errors.Add(Globalization.GetString("SaveError"),
//					String.Format(Globalization.GetString("Save.DuplicateClaimWarning")),
//					BusinessAdaptorErrorType.Error);
				Cancel = true;
				return;
			}
            //pmittal5 Mits 15752 06/12/09 - Detatch already selected Plan if criteria not met
            if (!ValidatePlan(objClaim.PlanId))
                objClaim.PlanId = 0;
            //End - pmittal5

            //Added by Shivendu for MITS 9047
            string sToday = Conversion.ToDbDate(System.DateTime.Now);
            //MGaba2 : MITS 11989-start
            //R7 Time Zone :Yatharth START
            //Now we can have any Time Zone
            string sTime = Conversion.GetTime(System.DateTime.Now.ToShortTimeString()); // Current server Time
            string sOrgLevelToday = Conversion.ToDbDate(System.DateTime.Now); //Current OrgLevel Date
            string sOrgLevelTime = Conversion.GetTime(System.DateTime.Now.ToShortTimeString()); //Current OrgLevel Time
            int iOrgLevelID = 0;
            //int iDateChange = 0; //0-> No Change in date, 1-> Subtract 1 day from date, 2-> Add 1 Day to date
            DateTime dtOrgLevelCurrentDateTime = System.DateTime.Now;//For storing Current Date time of OrgLevel
            string sOrgLevelCurrentDate = string.Empty; //Current Date in OrgLevel
            string sOrgLevelCurrentTime = string.Empty; //Current Time in OrgLevel
            DateTime dtTemp = System.DateTime.Now.ToUniversalTime(); //DateTime in GMT
            string sSQL = string.Empty;
            int iTimeZoneTracking = 0; //-1 if Time Zone is enabled 
            //int iDayLightSavings = 0; //-1 if Day Light Savings is enabled
            //bool bDayLightSavings = false; //True for Day Light Savings and vice-versa
            string sTimeZone = string.Empty;//For storing TimeZone of OrgLevel
            int OrgTimeZoneLevel = 0;


            OrgTimeZoneLevel = objClaim.Context.InternalSettings.SysSettings.OrgTimeZoneLevel;

            sSQL = @"SELECT ";
            if (OrgTimeZoneLevel != 0)
            {
                switch (OrgTimeZoneLevel)
                {
                    case 1005:
                        sSQL = sSQL + "CLIENT_EID";
                        break;
                    case 1006:
                        sSQL = sSQL + "COMPANY_EID";
                        break;
                    case 1007:
                        sSQL = sSQL + "OPERATION_EID";
                        break;
                    case 1008:
                        sSQL = sSQL + "REGION_EID";
                        break;
                    case 1009:
                        sSQL = sSQL + "DIVISION_EID";
                        break;
                    case 1010:
                        sSQL = sSQL + "LOCATION_EID";
                        break;
                    case 1011:
                        sSQL = sSQL + "FACILITY_EID";
                        break;
                    default:
                        sSQL = sSQL + "DEPARTMENT_EID";
                        break;
                }

                sSQL = sSQL + " FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid;

                //END
                //Change by kuladeep for object reference error mits:25548 Start 07/26/2011
                //iOrgLevelID = Conversion.ConvertStrToInteger(objClaim.Context.DbConn.ExecuteScalar(sSQL).ToString());
                iOrgLevelID = Conversion.CastToType<int>(Convert.ToString(objClaim.Context.DbConn.ExecuteScalar(sSQL)), out bIsSuccess);
                //Change by kuladeep for object reference error mits:25548 End 07/26/2011

                //To get Time Zone of the OrgLevel is enabled or not
                sSQL = @"SELECT TIME_ZONE_TRACKING FROM ENTITY WHERE ENTITY_ID = " + iOrgLevelID.ToString();
                iTimeZoneTracking = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar(sSQL), base.ClientId);

                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {

                    sSQL = @"SELECT SHORT_CODE FROM CODES WHERE CODE_ID=
                        (SELECT TIME_ZONE_CODE FROM ENTITY WHERE ENTITY_ID=" + iOrgLevelID.ToString() + ")";
                    sTimeZone = objClaim.Context.DbConn.ExecuteScalar(sSQL).ToString();

                    ////To see Day light saving is enabled or not for OrgLevel
                    //sSQL = @"SELECT DAY_LIGHT_SAVINGS FROM ENTITY WHERE ENTITY_ID = " + iOrgLevelID.ToString();
                    //iDayLightSavings = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar(sSQL), base.ClientId);

                    ////If Daylight savings are enabled
                    //if (iDayLightSavings != 0)
                    //{
                    //    bDayLightSavings = true;
                    //}
                    ////If Daylight savings are disabled
                    //else
                    //{
                    //    bDayLightSavings = false;
                    //}

                    //Current Time in OrgLevel
                    dtOrgLevelCurrentDateTime = Conversion.ConvertGMTToOrgHierarchyTime(sTimeZone, dtTemp);

                    //if (iDateChange != 0)
                    //{
                    //    if (iDateChange == 1)
                    //    {
                    //        dtTemp = dtTemp.AddDays(-1);
                    //    }
                    //    else
                    //    {
                    //        dtTemp = dtTemp.AddDays(1);
                    //    }
                    //}
                    sOrgLevelToday = Conversion.ToDbDate(dtOrgLevelCurrentDateTime);
                    //Date in OrgLevel in mm/dd/yy format for displaying validation error message
                    sOrgLevelCurrentDate = dtTemp.ToShortDateString();
                    //Time in OrgLevel for displaying validation error message
                    sOrgLevelCurrentTime = Conversion.ToDbTime(dtOrgLevelCurrentDateTime);
                    //dtOrgLevelCurrentDateTime = Convert.ToDateTime(sOrgLevelCurrentDate + " " + sOrgLevelCurrentTime);
                }
            }
            //Condition added if the time zone tracking is ON then we need to check the date of event and claim
            //with the date of time zone in which the Org Level Resides.

            if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
            {
                if ((objClaim.DateOfClaim.CompareTo(sOrgLevelToday) > 0) && (objEvent.DateOfEvent.CompareTo(sOrgLevelToday) > 0))
                {
                    if ((base.SysEx.SelectSingleNode("//NonOccClaimFutureDate").InnerText == "") && (base.SysEx.SelectSingleNode("//NonOccEventFutureDate").InnerText == ""))
                    {
                        base.m_CurrentAction = enumFormActionType.None;
                        base.ResetSysExData("NonOccClaimFutureDate", "1");
                        base.ResetSysExData("NonOccEventFutureDate", "1");
                        Cancel = true;
                        return;
                    }
                }
                else if (objClaim.DateOfClaim.CompareTo(sOrgLevelToday) > 0)
                {
                    if (base.SysEx.SelectSingleNode("//NonOccClaimFutureDate").InnerText == "")
                    {
                        base.m_CurrentAction = enumFormActionType.None;
                        base.ResetSysExData("NonOccClaimFutureDate", "1");
                        Cancel = true;
                        return;
                    }
                } 
                else if (objEvent.DateOfEvent.CompareTo(sOrgLevelToday) > 0) 
                {
                    if (base.SysEx.SelectSingleNode("//NonOccEventFutureDate").InnerText == "")
                    {
                        base.m_CurrentAction = enumFormActionType.None;
                        base.ResetSysExData("NonOccEventFutureDate", "1");
                        Cancel = true;
                        return;
                    }
                }
                else if (objClaim.DateOfClaim.CompareTo(sOrgLevelToday) == 0 && objEvent.DateOfEvent.CompareTo(sOrgLevelToday) == 0 && objClaim.TimeOfClaim.CompareTo(sOrgLevelCurrentTime) > 0 && objEvent.TimeOfEvent.CompareTo(sOrgLevelCurrentTime) > 0)
                {
                    if ((base.SysEx.SelectSingleNode("//NonOccClaimFutureDate").InnerText == "") && (base.SysEx.SelectSingleNode("//NonOccEventFutureDate").InnerText == ""))
                    {
                        base.m_CurrentAction = enumFormActionType.None;
                        base.ResetSysExData("NonOccClaimFutureDate", "1");
                        base.ResetSysExData("NonOccEventFutureDate", "1");
                        Cancel = true;
                        return;
                    }
                }
                else if (objClaim.DateOfClaim.CompareTo(sOrgLevelToday) == 0 && objClaim.TimeOfClaim.CompareTo(sOrgLevelCurrentTime) > 0)
                {
                    if (base.SysEx.SelectSingleNode("//NonOccClaimFutureDate").InnerText == "")
                    {
                        base.m_CurrentAction = enumFormActionType.None;
                        base.ResetSysExData("NonOccClaimFutureDate", "1");
                        Cancel = true;
                        return;
                    }
                }
                else if (objEvent.DateOfEvent.CompareTo(sOrgLevelToday) == 0 && objEvent.TimeOfEvent.CompareTo(sOrgLevelCurrentTime) > 0)
                {
                    if (base.SysEx.SelectSingleNode("//NonOccEventFutureDate").InnerText == "")
                    {
                        base.m_CurrentAction = enumFormActionType.None;
                        base.ResetSysExData("NonOccEventFutureDate", "1");
                        Cancel = true;
                        return;
                    }
                }
            }
            else
            {
                if ((objClaim.DateOfClaim.CompareTo(sToday) > 0) && (objEvent.DateOfEvent.CompareTo(sToday) > 0))
                {
                    if ((base.SysEx.SelectSingleNode("//NonOccClaimFutureDate").InnerText == "") && (base.SysEx.SelectSingleNode("//NonOccEventFutureDate").InnerText == ""))
                    {
                        base.m_CurrentAction = enumFormActionType.None;
                        base.ResetSysExData("NonOccClaimFutureDate", "1");
                        base.ResetSysExData("NonOccEventFutureDate", "1");
                        Cancel = true;
                        return;
                    }
                }//MGaba2 : MITS 11989-end
                else if (objClaim.DateOfClaim.CompareTo(sToday) > 0)//Added by Shivendu for MITS 9047
                {
                    if (base.SysEx.SelectSingleNode("//NonOccClaimFutureDate").InnerText == "")
                    {
                        base.m_CurrentAction = enumFormActionType.None;
                        base.ResetSysExData("NonOccClaimFutureDate", "1");
                        Cancel = true;
                        return;
                    }
                } //Added by Shivendu for MITS 9047
                else if (objEvent.DateOfEvent.CompareTo(sToday) > 0) //MGaba2 : MITS 11989-start
                {
                    if (base.SysEx.SelectSingleNode("//NonOccEventFutureDate").InnerText == "")
                    {
                        base.m_CurrentAction = enumFormActionType.None;
                        base.ResetSysExData("NonOccEventFutureDate", "1");
                        Cancel = true;
                        return;
                    }
                }
                else if (objClaim.DateOfClaim.CompareTo(sToday) == 0 && objEvent.DateOfEvent.CompareTo(sToday) == 0 && objClaim.TimeOfClaim.CompareTo(sTime) > 0 && objEvent.TimeOfEvent.CompareTo(sTime) > 0)
                {
                    if ((base.SysEx.SelectSingleNode("//NonOccClaimFutureDate").InnerText == "") && (base.SysEx.SelectSingleNode("//NonOccEventFutureDate").InnerText == ""))
                    {
                        base.m_CurrentAction = enumFormActionType.None;
                        base.ResetSysExData("NonOccClaimFutureDate", "1");
                        base.ResetSysExData("NonOccEventFutureDate", "1");
                        Cancel = true;
                        return;
                    }
                }
                else if (objClaim.DateOfClaim.CompareTo(sToday) == 0 && objClaim.TimeOfClaim.CompareTo(sTime) > 0)
                {
                    if (base.SysEx.SelectSingleNode("//NonOccClaimFutureDate").InnerText == "")
                    {
                        base.m_CurrentAction = enumFormActionType.None;
                        base.ResetSysExData("NonOccClaimFutureDate", "1");
                        Cancel = true;
                        return;
                    }
                }
                else if (objEvent.DateOfEvent.CompareTo(sToday) == 0 && objEvent.TimeOfEvent.CompareTo(sTime) > 0)
                {
                    if (base.SysEx.SelectSingleNode("//NonOccEventFutureDate").InnerText == "")
                    {
                        base.m_CurrentAction = enumFormActionType.None;
                        base.ResetSysExData("NonOccEventFutureDate", "1");
                        Cancel = true;
                        return;
                    }
                }

            }//MGaba2 : MITS 11989-end

            //R7 Time Zone :Yatharth END
            base.ResetSysExData("NonOccClaimFutureDate", "");    //Added by Shivendu for MITS 9047
            base.ResetSysExData("NonOccEventFutureDate", ""); //MGaba2 : MITS 11989
			
            base.ResetSysExData("dupeoverride","");
			sUserCloseDiary = base.SysEx.SelectSingleNode("//CloseDiary").InnerText;

            //Changed by Gagan for MITS 11451 : Start
            if (base.SysEx.SelectSingleNode("//DeleteAutoCheck").InnerText != "")
                bDeleteAutoChecks = Conversion.ConvertStrToBool(base.SysEx.SelectSingleNode("//DeleteAutoCheck").InnerText);
            //Changed by Gagan for MITS 11451 : End

			//check for duplicate claim number
			if (objClaim.ClaimNumber.Trim() != string.Empty)
			{
                //sSQL="SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER='" + objClaim.ClaimNumber + "' AND CLAIM_ID<>" + objClaim.ClaimId.ToString();
                //using (DbReader objReader = objClaim.Context.DbConn.ExecuteReader(sSQL))
                //{
                //Deb:Commented above line used parameterized input for sql query for MITS 25141

                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                sSQL = string.Format("SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER={0} AND CLAIM_ID <> {1}", "~CLAIMNUMBER~", "~CLAIMID~");
                dictParams.Add("CLAIMNUMBER", objClaim.ClaimNumber);
                dictParams.Add("CLAIMID", objClaim.ClaimId.ToString());
                using (DbReader objReader = DbFactory.ExecuteReader(objClaim.Context.DbConn.ConnectionString, sSQL, dictParams))
                {
                    if (objReader.Read())
                    {
                        Errors.Add(Globalization.GetString("SaveError",base.ClientId),
                            String.Format(Globalization.GetString("Save.DuplicateClaimNumberWarning",base.ClientId)),
                            BusinessAdaptorErrorType.Error);
                        Cancel = true;
                        objReader.Close();
                        return;
                    }
                }
			}
		}

		public override void AfterSave()
		{
			base.AfterSave ();

			//Check and close Diaries
			// sUserCloseDiary applied by Rahul 6th Feb 2006.
//            if(bCloseDiaries && sUserCloseDiary=="true")
//                objClaim.Context.DbConnLookup.ExecuteNonQuery(
//                    String.Format(@"UPDATE WPA_DIARY_ENTRY SET STATUS_OPEN = 0 
//					WHERE ATTACH_RECORDID = {0} AND ATTACH_TABLE = 'CLAIM'",objClaim.ClaimId));
//            bCloseDiaries =false;
            ////Psarin2 Start :MITS 14695
            //if (iUseCaseMgmt == -1)//Neha Moved to end
            //{
            //    objClaim.CaseManagement.ClaimId = objClaim.ClaimId;
            //    objClaim.CaseManagement.Save();
            //}
            //Psarin2 End :MITS 14695
            CloseAllDiaries();
            
            //Changed by Gagan for MITS 11451 : Start
            //Delete auto checks
            if (bDeleteAutoChecks)
                DeleteAutoChecks();
            //Changed by Gagan for MITS 11451 : End

            //Raman: 05/14/2009 Claimant Eid becomes out of sync after updation of primary employee id.
            //This MITS was fixed for WC but needs to be done for DI too

            //pmahli MITS 10997 - start 
            if (objClaim.PrimaryClaimant.ClaimantEid != objClaim.PrimaryPiEmployee.PiEid)
            {
                objClaim.PrimaryClaimant.ClaimantEid = objClaim.PrimaryPiEmployee.PiEid;
                objClaim.PrimaryClaimant.Save();

            }
            //pmahli MITS 10811 - End
            //Mridul. MITS 16745-Chubb Enhancement
            #region ClaimLetter
            int iLtrGenerated = 0;
            int iEmailAck = 0;
            Event objEvent = null;
            StringBuilder sbSql = null;
            StringBuilder sOrgEid = null;
            DbReader objReader = null;
            int iTempId = 0;
            //Check from LIST instead of below TODO
            base.ResetSysExData("ClaimLetterTmplId", "");
            if (objData.Context.InternalSettings.SysSettings.ClaimLetterFlag)
            {
                iTempId = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar("SELECT CLLTR_TMPL_ID FROM SYS_PARMS_CLM_LTR"), base.ClientId);
                if (bTriggerLetter && objClaim.CurrentAdjuster.AdjusterEid.ToString() != "" && iTempId != 0)
                {
                    //If generated before
                    iLtrGenerated = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar("SELECT LTR_FLAG FROM CLM_LTR_LOG WHERE LTR_FLAG = 1 AND CLAIM_ID = " + objClaim.ClaimId.ToString()), base.ClientId);
                    if (iLtrGenerated == 0)
                    {                        
                        //Commented BY Navdeep - Table changed to SYS_UTIL_CLM_LTR instead of SYS_PARMS_CLM_LTR for Claim Type IDS
                        //string sTypeNotInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT DTTM_RCD_LAST_UPD FROM SYS_PARMS_CLM_LTR WHERE X_CLAIM_TYPE_CODE NOT LIKE '%," + objClaim.ClaimTypeCode + ",%'"));
                        string sTypeNotInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT COLUMN_DESC FROM SYS_UTIL_CLM_LTR WHERE ROW_ID = 2 AND CODE_EID IN (" + objClaim.ClaimTypeCode + ")"));
                        //Check if Claim Type is in Included List
                        //Commented by Code - Change due to above modified Query
                        //if (sTypeNotInList != "")
                        if (string.IsNullOrEmpty(sTypeNotInList))
                        {
                            //Commented BY Navdeep - Table changed to SYS_UTIL_CLM_LTR instead of SYS_PARMS_CLM_LTR for Adjuster EIDs
                            //string sAdjNotInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT ADJUSTERS FROM SYS_PARMS_CLM_LTR WHERE ADJUSTERS LIKE '%," + objClaim.CurrentAdjuster.AdjusterEid + ",%'"));
                            string sAdjNotInList = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT COLUMN_DESC FROM SYS_UTIL_CLM_LTR WHERE ROW_ID = 1 AND CODE_EID IN (" + objClaim.CurrentAdjuster.AdjusterEid + ")"));
                            //Check if Current Adjuster is in Included List
                            //if (sAdjNotInList != "")
                            if (!string.IsNullOrEmpty(sAdjNotInList))
                            {
                                //Check if ACK should be generated
                                objEvent = (Event)m_fda.Factory.GetDataModelObject("Event", false);
                                objEvent.MoveTo(objClaim.EventId);
                                sbSql = new StringBuilder();
                                sOrgEid = new StringBuilder();
                                sbSql.Append("SELECT * FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid);
                                using (objReader = objClaim.Context.DbConn.ExecuteReader(sbSql.ToString()))
                                {
                                    if (objReader.Read())
                                        sOrgEid.Append(objReader.GetInt("DEPARTMENT_EID") + "," + objReader.GetInt("FACILITY_EID") + "," + objReader.GetInt("LOCATION_EID") + "," + objReader.GetInt("DIVISION_EID") + "," + objReader.GetInt("REGION_EID") + "," + objReader.GetInt("OPERATION_EID") + "," + objReader.GetInt("COMPANY_EID") + "," + objReader.GetInt("CLIENT_EID"));
                                }

                                //Geeta : Mits 18718 for Acord Comma Seperated List Issue
                                iEmailAck = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar("SELECT COUNT(EMAIL_ACK) FROM ENT_X_CONTACTINFO EXC,CONTACT_LOB WHERE EXC.EMAIL_ACK = -1 AND EXC.ENTITY_ID IN (" + sOrgEid.ToString() + ") AND EXC.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode ), base.ClientId);
                                //Send CLOSED Letter Signal
                                if (iEmailAck > 0)
                                    base.ResetSysExData("ClaimLetterTmplId", "CL");
                            }
                        }
                    }
                }
            }
            bTriggerLetter = false;
            #endregion ClaimLetter
            //Neha 14/09/2011 R8 changes moved the code to last so that making parent null does not affect the other child objects.
            if (iUseCaseMgmt == -1)
            {
                objClaim.CaseManagement.Parent = null;
                objClaim.CaseManagement.PiRowId = objClaim.PrimaryPiEmployee.PiRowId;//
                objClaim.CaseManagement.ClaimId = objClaim.ClaimId;
                objClaim.CaseManagement.Save();
            }
		}

		private bool IsDupeClaim()
		{
			Event objEvent = (objClaim.Parent as Event);
			string SQL="";
//			string sTmp="";
			XmlNode eltDupe =null;
//			XmlNode eltClaim =null;

			int iDupNumDays;
			int iDupPayCriteria;

			if(!objLobSettings.DuplicateFlag)
				return false;
			
			//User already saw dup popup and is choosing to proceed.
			//if(base.SysEx.SelectSingleNode("//dupeoverride").InnerText.Trim()!="")    csingh7 : Commented for MITS 19840
            if (base.SysEx.SelectSingleNode("//dupeoverride").InnerText.Trim() == "IsSaveDup")  //csingh7 MITS 19840 : If user saves Duplicate Claim.
                return false;

			iDupNumDays = objLobSettings.DupNumDays;
			iDupPayCriteria = objLobSettings.DupPayCriteria;

			switch(iDupPayCriteria)
			{
				case 0: //No Additional Criteria
					SQL = @"SELECT CLAIM.CLAIM_ID
						FROM CLAIM, EVENT, CLAIMANT
						WHERE CLAIM.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode
						+ " AND CLAIM.CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode
						+ " AND CLAIM.EVENT_ID = EVENT.EVENT_ID "
						+ " AND EVENT.DEPT_EID = " + objEvent.DeptEid
						+ " AND CLAIM.FILING_STATE_ID = " + objClaim.FilingStateId
						+ " AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID "
						+ " AND CLAIMANT.CLAIMANT_EID = " + objClaim.PrimaryPiEmployee.PiEid
						+ " AND EVENT.DATE_OF_EVENT = '" + objEvent.DateOfEvent + "'";
					break;
				case 1: //Department and Event Date within X Days
					SQL = @"SELECT CLAIM.CLAIM_ID
						FROM CLAIM, EVENT, CLAIMANT
						WHERE CLAIM.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode
						+ " AND CLAIM.EVENT_ID = EVENT.EVENT_ID "
						+ " AND EVENT.DEPT_EID = " + objEvent.DeptEid
						+ " AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID "
						+ " AND EVENT.DATE_OF_EVENT >= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(-iDupNumDays)) + "'"
						+ " AND EVENT.DATE_OF_EVENT <= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(iDupNumDays)) + "'";
					break;
				case 2: //Operation Org Level and Event Date within X Days
					string sAbbreviation = string.Empty;
					string sName = string.Empty;
					int iParentId = 0;
					objCache.GetParentOrgInfo(objEvent.DeptEid, 1007, ref sAbbreviation, ref sName, ref iParentId);

					SQL = @"SELECT CLAIM.CLAIM_ID
						FROM CLAIM, EVENT, CLAIMANT, ENTITY, ORG_HIERARCHY
						WHERE CLAIM.LINE_OF_BUS_CODE =" + objClaim.LineOfBusCode
						+ " AND CLAIM.EVENT_ID = EVENT.EVENT_ID"
						+ " AND EVENT.DEPT_EID = ORG_HIERARCHY.DEPARTMENT_EID "
						+ " AND ORG_HIERARCHY.OPERATION_EID = " + iParentId
						+ " AND EVENT.DATE_OF_EVENT >= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(-iDupNumDays)) + "'"
						+ " AND EVENT.DATE_OF_EVENT <= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(iDupNumDays)) + "'"
						+ " AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID "
						+ " AND CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID "
						+ " AND ENTITY.LAST_NAME = '" + objClaim.PrimaryPiEmployee.PiEntity.LastName + "'";
					break;
				case 3: //Dept, Clm Type and Event Date within X Days
					SQL = @"SELECT CLAIM.CLAIM_ID
						FROM CLAIM, EVENT, CLAIMANT, ENTITY
						WHERE CLAIM.LINE_OF_BUS_CODE =" + objClaim.LineOfBusCode
						+ " AND CLAIM.CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode
						+ " AND CLAIM.EVENT_ID = EVENT.EVENT_ID"
						+ " AND EVENT.DEPT_EID = " + objEvent.DeptEid
						+ " AND CLAIM.FILING_STATE_ID = " + objClaim.FilingStateId
						+ " AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID"
						+ " AND CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID"
						+ " AND REPLACE(ENTITY.TAX_ID, '-', '') = '" + objClaim.PrimaryPiEmployee.PiEntity.TaxId.Replace("-", "") + "'"
						+ " AND EVENT.DATE_OF_EVENT >= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(-iDupNumDays)) + "'"
						+ " AND EVENT.DATE_OF_EVENT <= '" + Conversion.ToDbDate(Conversion.ToDate(objEvent.DateOfEvent).AddDays(iDupNumDays)) + "'";
					break;
				default:
					SQL = @"SELECT CLAIM.CLAIM_ID
						FROM CLAIM, EVENT, CLAIMANT
						WHERE CLAIM.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode
						+ " AND CLAIM.CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode
						+ " AND CLAIM.EVENT_ID = EVENT.EVENT_ID "
						+ " AND EVENT.DEPT_EID = " + objEvent.DeptEid
						+ " AND CLAIM.FILING_STATE_ID = " + objClaim.FilingStateId
						+ " AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID "
						+ " AND CLAIMANT.CLAIMANT_EID = " + objClaim.PrimaryPiEmployee.PiEid
						+ " AND EVENT.DATE_OF_EVENT = '" + objEvent.DateOfEvent + "'";
					break;
			}
			
//			SQL =	@"SELECT CLAIM.CLAIM_ID, CLAIM.CLAIM_NUMBER, CLAIM.CLAIM_STATUS_CODE, EVENT.EVENT_NUMBER, EVENT.EVENT_DESCRIPTION, EVENT.EVENT_ID
//						FROM CLAIM, EVENT 
//						WHERE CLAIM.LINE_OF_BUS_CODE =" + objClaim.LineOfBusCode 
//				+ " AND CLAIM.CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode 
//				+ " AND CLAIM.EVENT_ID = EVENT.EVENT_ID "
//				+ " AND EVENT.DEPT_EID = " + objEvent.DeptEid 
//				+ " AND EVENT.DATE_OF_EVENT = '" + objEvent.DateOfEvent + "'"
//				+ " AND CLAIM.DATE_OF_CLAIM = '" + objClaim.DateOfClaim + "'";
			
			
			using(DbReader rdr = objClaim.Context.DbConnLookup.ExecuteReader(SQL))
			{
				if(!rdr.Read())
					return false;
				
				//Clear Dupe SysEx node.
				base.ResetSysExData("dupeoverride","");
				eltDupe = this.SysEx.SelectSingleNode("//dupeoverride");
				//Set Info from the Current Claim.
//				this.SafeSetAttribute(eltDupe, "formname","claimdi");
//				this.SafeSetAttribute(eltDupe, "claimtype",objClaim.Context.LocalCache.GetCodeDesc(objClaim.ClaimTypeCode));
//				this.SafeSetAttribute(eltDupe, "department",objClaim.Context.LocalCache.GetEntityLastFirstName(objEvent.DeptEid));
//				this.SafeSetAttribute(eltDupe, "eventdate",Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString());
//				this.SafeSetAttribute(eltDupe, "claimdate",Conversion.ToDate(objClaim.DateOfClaim).ToShortDateString());
				
				string sClaimIds = string.Empty;
				//Get Info From Potential Duplicate Matched Claims.
				do
				{
					sClaimIds = sClaimIds + rdr.GetInt("CLAIM_ID").ToString() + ",";
//					eltClaim = eltDupe.OwnerDocument.CreateElement("claim");
//					this.SafeSetAttribute(eltClaim, "claimid",rdr.GetInt("CLAIM_ID").ToString());
//					this.SafeSetAttribute(eltClaim, "claimnumber",rdr.GetString("CLAIM_NUMBER"));
//					this.SafeSetAttribute(eltClaim, "claimstatus",objClaim.Context.LocalCache.GetCodeDesc(rdr.GetInt("CLAIM_STATUS_CODE")));
//					this.SafeSetAttribute(eltClaim, "eventid",rdr.GetInt("EVENT_ID").ToString());
//					this.SafeSetAttribute(eltClaim, "eventnumber",rdr.GetString("EVENT_NUMBER"));
//					SQL = rdr.GetString("EVENT_DESCRIPTION").Replace('\n',' ').Replace('\r',' ').Replace("'","\'");
//					if(SQL.Length >47)
//						SQL = SQL.Substring(0,47)+"...";
//					this.SafeSetAttribute(eltClaim, "eventdescription",SQL);
//					eltDupe.AppendChild(eltClaim);
				}while(rdr.Read());
				sClaimIds = sClaimIds.Substring(0,sClaimIds.Length - 1);

				sClaimIds = sClaimIds 
					+ ";" + Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()
					+ ";" + objClaim.Context.LocalCache.GetEntityLastFirstName(objEvent.DeptEid);
				eltDupe.InnerText = sClaimIds;
			}

			//Fetch First Claimant's Name (if available) for each Claim.
			
//			foreach(XmlNode eltTemp in eltDupe.SelectNodes("./claim"))
//			{
//				SQL = 
//					"SELECT ENTITY.LAST_NAME,ENTITY.FIRST_NAME FROM ENTITY,CLAIMANT WHERE " +
//					" ENTITY.ENTITY_ID = CLAIMANT.CLAIMANT_EID AND CLAIMANT.CLAIM_ID = " + eltTemp.Attributes["claimid"].Value;
//				using(DbReader rdr = objClaim.Context.DbConnLookup.ExecuteReader(SQL))
//				{
//					if(!rdr.Read())
//						sTmp = "[none]";
//					else
//						sTmp = rdr.GetString("LAST_NAME").Trim() + " " + rdr.GetString("FIRST_NAME").Trim();
//					this.SafeSetAttribute(eltTemp, "claimant",sTmp.Trim());
//				}
//				
//			}

			return true;
			
		}

		//validate data according to the Business Rules
		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;
			Event objEvent=objClaim.Parent as Event;
			PiEmployee objEmp= objClaim.PrimaryPiEmployee as PiEmployee;

			// Perform data validation
            //Time Zone Code for R7: yatharth
            string sToday = Conversion.ToDbDate(System.DateTime.Now); // Current server Date
            string sTime = Conversion.GetTime(System.DateTime.Now.ToShortTimeString()); // Current server Time
            string sOrgLevelToday = Conversion.ToDbDate(System.DateTime.Now); //Current OrgLevel Date
            string sOrgLevelTime = Conversion.GetTime(System.DateTime.Now.ToShortTimeString()); //Current OrgLevel Time
            int iOrgLevelID = 0;
            //int iDateChange = 0; //0-> No Change in date, 1-> Subtract 1 day from date, 2-> Add 1 Day to date
            DateTime dtOrgLevelCurrentDateTime = System.DateTime.Now;//For storing Current Date time of OrgLevel
            string sOrgLevelCurrentDate = string.Empty; //Current Date in OrgLevel
            string sOrgLevelCurrentTime = string.Empty; //Current Time in OrgLevel
            DateTime dtTemp = System.DateTime.Now.ToUniversalTime(); //DateTime in GMT
            string sSQL = string.Empty;
            int iTimeZoneTracking = 0; //-1 if Time Zone is enabled 
            //int iDayLightSavings = 0; //-1 if Day Light Savings is enabled
            //bool bDayLightSavings = false; //True for Day Light Savings and vice-versa
            string sTimeZone = string.Empty;//For storing TimeZone of OrgLevel
            int OrgTimeZoneLevel = 0;
            //pmittal5 Mits 18751 09/20/10 - If existing Event Number is used
            int iEventId = 0;
            object oEvent = null;
            bool bEventFound = false;
            //Parag R7 : We will check for Duplicate EventNumber only in case when Generate AutoEventNumber is OFF
            if (!objEvent.EventNumber.Equals(""))
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                if (objEvent.EventId == 0)
                {

                    //sSQL = String.Format("SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = '{0}'", objEvent.EventNumber);
                    sSQL = String.Format("SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = {0}", "~EVENTNUMBER~");
                    dictParams.Add("EVENTNUMBER", objEvent.EventNumber);
                }
                else
                {
                    //sSQL = String.Format("SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = '{0}' and EVENT_ID <> {1} ", objEvent.EventNumber, objEvent.EventId);
                    sSQL = String.Format("SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER = {0} and EVENT_ID <> {1} ", "~EVENTNUMBER~", "~EVENTID~");
                    dictParams.Add("EVENTNUMBER", objEvent.EventNumber);
                    dictParams.Add("EVENTID", objEvent.EventId.ToString());
                }
                //oEvent = objClaim.Context.DbConn.ExecuteScalar(sSQL);//Deb MITS 25141
                oEvent = DbFactory.ExecuteScalar(objEvent.Context.DbConn.ConnectionString, sSQL, dictParams);
                if (oEvent != null)
                {
                    iEventId = Conversion.CastToType<int>(oEvent.ToString(), out bEventFound);
                    if (iEventId != 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                                    String.Format(Globalization.GetString("Validation.EventAlreadyExists",base.ClientId), objEvent.EventNumber.ToString()),
                                    BusinessAdaptorErrorType.Error);//psharma206
                        bError = true;
                    }
                }
            }
            //End - pmittal5
            //MGaba2: MITS 11989-Commenting following check
           /*if (objEvent.DateOfEvent.CompareTo(sToday) > 0)
			{
				Errors.Add(Globalization.GetString("ValidationError"),
					String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate"), Globalization.GetString("Field.EventDate"), System.DateTime.Now.ToShortDateString()),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}*/
            //Modified by Shivendu for MITS 9047
            //if (objClaim.DateOfClaim.CompareTo(sToday) > 0)
            //{
            //   
            ////    Errors.Add(Globalization.GetString("ValidationError"),
            ////        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate"), Globalization.GetString("Field.ClaimDate"), System.DateTime.Now.ToShortDateString()),
            ////        BusinessAdaptorErrorType.Error);

            ////    bError = true;
            
            //}
            //Modified by Shivendu for MITS 9047
            //BOB Enhancement  :  Ukusvaha
            if (objEvent.DeptEid == 0)
            {
                if (objEvent.Context.InternalSettings.SysSettings.AutoPopulateDpt == -1)
                {

                    objEvent.DeptEid = objEvent.Context.InternalSettings.SysSettings.AutoFillDpteid;
                }
            }
            //End BOB Enhancement 
            //sSQL = @"SELECT ORG_TIMEZONE_LEVEL FROM SYS_PARMS";
            //OrgTimeZoneLevel = Conversion.ConvertStrToInteger(objClaim.Context.DbConn.ExecuteScalar(sSQL).ToString());
            OrgTimeZoneLevel = objClaim.Context.InternalSettings.SysSettings.OrgTimeZoneLevel;
             //Parag R7 : We will check for Time Validation only in case when Time Validation is ON
            if (OrgTimeZoneLevel != 0)
            {
                sSQL = @"SELECT ";

                switch (OrgTimeZoneLevel)
                {
                    case 1005:
                        sSQL = sSQL + "CLIENT_EID";
                        break;
                    case 1006:
                        sSQL = sSQL + "COMPANY_EID";
                        break;
                    case 1007:
                        sSQL = sSQL + "OPERATION_EID";
                        break;
                    case 1008:
                        sSQL = sSQL + "REGION_EID";
                        break;
                    case 1009:
                        sSQL = sSQL + "DIVISION_EID";
                        break;
                    case 1010:
                        sSQL = sSQL + "LOCATION_EID";
                        break;
                    case 1011:
                        sSQL = sSQL + "FACILITY_EID";
                        break;
                    default:
                        sSQL = sSQL + "DEPARTMENT_EID";
                        break;
                }


                //END
                sSQL = sSQL + " FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid;
                iOrgLevelID = Conversion.ConvertStrToInteger(objClaim.Context.DbConn.ExecuteScalar(sSQL).ToString());

                //To get Time Zone of the OrgLevel is enabled or not
                sSQL = @"SELECT TIME_ZONE_TRACKING FROM ENTITY WHERE ENTITY_ID = " + iOrgLevelID.ToString();
                iTimeZoneTracking = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar(sSQL), base.ClientId);

                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {

                    sSQL = @"SELECT SHORT_CODE FROM CODES WHERE CODE_ID=
                        (SELECT TIME_ZONE_CODE FROM ENTITY WHERE ENTITY_ID=" + iOrgLevelID.ToString() + ")";
                    sTimeZone = objClaim.Context.DbConn.ExecuteScalar(sSQL).ToString();

                    //To see Day light saving is enabled or not for OrgLevel
                //sSQL = @"SELECT DAY_LIGHT_SAVINGS FROM ENTITY WHERE ENTITY_ID = " + iOrgLevelID.ToString();
                //iDayLightSavings = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar(sSQL), base.ClientId);

                ////If Daylight savings are enabled
                //if (iDayLightSavings != 0)
                //{
                //    bDayLightSavings = true;
                //}
                ////If Daylight savings are disabled
                //else
                //{
                //    bDayLightSavings = false;
                //}

                //Current Time in OrgLevel
                //sOrgLevelTime = Conversion.ConvertGMTToOrgHierarchyTime(sTimeZone, Conversion.ToDbTime(System.DateTime.Now.ToUniversalTime()), bDayLightSavings, ref iDateChange);
                dtOrgLevelCurrentDateTime = Conversion.ConvertGMTToOrgHierarchyTime(sTimeZone, dtTemp);
                //if (iDateChange != 0)
                //{
                //    if (iDateChange == 1)
                //    {
                //        dtTemp = dtTemp.AddDays(-1);
                //    }
                //    else
                //    {
                //        dtTemp = dtTemp.AddDays(1);
                //    }
                //}
                //Current Date in OrgLevel
                sOrgLevelToday = Conversion.ToDbDate(dtOrgLevelCurrentDateTime);
                //Date in OrgLevel in mm/dd/yy format for displaying validation error message
                sOrgLevelCurrentDate = dtTemp.ToShortDateString();
                //Time in OrgLevel for displaying validation error message
                sOrgLevelCurrentTime = Conversion.ToDbTime(dtOrgLevelCurrentDateTime);
                //dtOrgLevelCurrentDateTime = Convert.ToDateTime(sOrgLevelCurrentDate + " " + sOrgLevelCurrentTime);
                }
            }
            //Time Zone Validation-- R7
			if (objClaim.DateOfClaim.CompareTo(objEvent.DateOfEvent) < 0)
			{
				Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
					String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate",base.ClientId), Globalization.GetString("Field.ClaimDate",base.ClientId), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
					BusinessAdaptorErrorType.Error);//psharma206

				bError = true;
			}

            if (objEvent.DateOfEvent.CompareTo(objClaim.DateOfClaim) == 0 && objClaim.TimeOfClaim.CompareTo(objEvent.TimeOfEvent) < 0)
            {
                Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventTime",base.ClientId), Globalization.GetString("Field.ClaimTime",base.ClientId), Conversion.ToDate(objEvent.TimeOfEvent).ToLongTimeString()),
                    BusinessAdaptorErrorType.Error);//psharma206

                bError = true;
            }

            //Added by csingh7 : MITS 14151 : Start
            if (objClaim.DisabilToDate != "" && objClaim.DisabilToDate != null)
            {
                if (objClaim.DisabilToDate.CompareTo(objClaim.DisabilFromDate) < 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThanDisabilyFromDate",base.ClientId), Globalization.GetString("Field.DisabilityToDate",base.ClientId), Conversion.ToDate(objClaim.DisabilFromDate).ToShortDateString()),
                        BusinessAdaptorErrorType.Error);//psharma206

                    bError = true;
                }
            }
            //Added by csingh7 : MITS 14151 : End

            


			if (objClaim.DttmClosed!="")
			{
                //Validation in case of TimeZone
                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {
                    //nsachdeva2 - MITS: 28069 - 4/4/2012
                    //if (objClaim.DttmClosed.Substring(0, 8).CompareTo(sOrgLevelToday) > 0 &&
                    if (Conversion.ToDbDate(Conversion.ConvertGMTToOrgHierarchyTime(sTimeZone, Conversion.ToDate(objClaim.DttmClosed))).CompareTo(sOrgLevelToday) > 0 &&
                        objData.Context.InternalSettings.CacheFunctions.GetShortCode(objData.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(objClaim.ClaimStatusCode)) == "C")
                    {
                        Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.DttmClosed",base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);//psharma206

                        bError = true;
                    }
                }
                //Validation without TimeZone
                else
                {
				if(objClaim.DttmClosed.Substring(0,8).CompareTo(sToday) > 0 &&
					objData.Context.InternalSettings.CacheFunctions.GetShortCode(objData.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(objClaim.ClaimStatusCode))=="C")
				{
					Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
						String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.DttmClosed",base.ClientId), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);//psharma206

					bError = true;
                    }
				}

				if(objClaim.DttmClosed.Substring(0,8).CompareTo(objClaim.DateOfClaim) < 0 &&
					objData.Context.InternalSettings.CacheFunctions.GetShortCode(objData.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(objClaim.ClaimStatusCode))=="C")
				{
					Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
						String.Format(Globalization.GetString("Validation.MustBeGreaterThanClaimDate",base.ClientId), Globalization.GetString("Field.DttmClosed",base.ClientId), Conversion.ToDate(objClaim.DateOfClaim).ToShortDateString()),
                        BusinessAdaptorErrorType.Error);//psharma206

					bError = true;
				}
			}
            if (objClaim.DateRptdToRm != string.Empty)
            {
                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {
                    if (objEvent.DateOfEvent.CompareTo(sOrgLevelToday) <= 0)
                    {
                        if (objClaim.DateRptdToRm.CompareTo(sOrgLevelToday) > 0)
                        {
                            Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                                String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.DateReported",base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                                BusinessAdaptorErrorType.Error);//psharma206

                            bError = true;
                        }
                    }
                }
                else
                {
                if (objEvent.DateOfEvent.CompareTo(sToday) <= 0)
                {
                    if (objClaim.DateRptdToRm.CompareTo(sToday) > 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.DateReported",base.ClientId), System.DateTime.Now.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);//psharma206

                        bError = true;
                        }
                    }
                }
                // igupta3 MITS:25047 Changes start
                //Claim Reported Date can be less than claim date in case claim date is future date
                if (objClaim.DateOfClaim.CompareTo(sToday) <= 0)
                {
                    if (objClaim.DateRptdToRm.CompareTo(objClaim.DateOfClaim) < 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeGreaterThanClaimDate",base.ClientId), Globalization.GetString("Field.DateReported",base.ClientId), Conversion.ToDate(objClaim.DateOfClaim).ToShortDateString()),
                            BusinessAdaptorErrorType.Error);//psharma206

                        bError = true;
                    }
                }
                //igupta3 MITS:25047 Changes ends             
            }
			if (objEvent.DateReported!="") 
			{
                //Validation in case of TimeZone
                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {

                    if (objEvent.DateReported.CompareTo(sOrgLevelToday) > 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.DateReported",base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);//psharma206

                        bError = true;
                    }
                }
                //Validation without TimeZone
                else
                {
				if (objEvent.DateReported.CompareTo(sToday) > 0)
				{
					Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
						String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.DateReported",base.ClientId), System.DateTime.Now.ToShortDateString()),
                        BusinessAdaptorErrorType.Error);//psharma206

					bError = true;
				}
                }
                //Validation in case of TimeZone
                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {
                //MGaba2 : MITS 11989-Reported Date should be allowed to be less than event date in case
                //event date is in future.So added an if condition
                    if (objEvent.DateOfEvent.CompareTo(sOrgLevelToday) <= 0)
                    {
                        if (objEvent.DateReported.CompareTo(objEvent.DateOfEvent) < 0)
                        {
                            Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                                String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate",base.ClientId), Globalization.GetString("Field.DateReported",base.ClientId), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
                                BusinessAdaptorErrorType.Error);//psharma206

                            bError = true;
                        }
                    }
                }
                //Validation without TimeZone
                else
                {
                    //MGaba2 : MITS 11989-Reported Date should be allowed to be less than event date in case
                    //event date is in future.So added an if condition
                if (objEvent.DateOfEvent.CompareTo(sToday) <= 0)
                {
				if (objEvent.DateReported.CompareTo(objEvent.DateOfEvent) < 0)
				{
					Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
						String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate",base.ClientId), Globalization.GetString("Field.DateReported",base.ClientId), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
                        BusinessAdaptorErrorType.Error);//psharma206

					bError = true;
                        }
				}
                }
				//Nikhil Garg		Dated: 23-Dec-05	Bug No:1069
				//Checking if the TimeReported is Empty before comparing it with EventTime
				if (objEvent.DateOfEvent.CompareTo(objEvent.DateReported) == 0 && objEvent.TimeReported!=string.Empty && objEvent.TimeReported.CompareTo(objEvent.TimeOfEvent) < 0)
				{
					Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventTime",base.ClientId), Globalization.GetString("Field.EventTimeReported",base.ClientId), Conversion.ToDate(objEvent.DateOfEvent + objEvent.TimeOfEvent).ToLongTimeString()),
                        BusinessAdaptorErrorType.Error);//psharma206

					bError = true;
				}
			}

            // npadhy MITS 14200 Validation for Injury From Date and Injury To Date as Per RMWorld
            if (!string.IsNullOrEmpty(objEvent.InjuryFromDate) && !(string.IsNullOrEmpty(objEvent.InjuryToDate)))
            {
                if (objEvent.InjuryFromDate.CompareTo(objEvent.InjuryToDate) > 0)
                {
                    Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanInjuryToDate",base.ClientId), Globalization.GetString("Field.InjuryFromDate",base.ClientId), Conversion.ToDate(objEvent.InjuryToDate).ToShortDateString()),
                            BusinessAdaptorErrorType.Error);//psharma206

                    bError = true;
                }
            }

			if(objEmp != null)
			{
				if (objEmp.DateHired!= "")
				{
                    //Validation in case of TimeZone
                    if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                    {
                        if (objEmp.DateHired.CompareTo(sOrgLevelToday) > 0)
                        {
                            Errors.Add(Globalization.GetString("ValidationError",base.ClientId),
                                String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate",base.ClientId), Globalization.GetString("Field.DateHired",base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                                BusinessAdaptorErrorType.Error);//psharma206

                            bError = true;
                        }
                        else if (objEmp.DateHired.CompareTo(objEvent.DateOfEvent) > 0 && objEvent.DateOfEvent != "")
                        {
                            Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                                String.Format(Globalization.GetString("Validation.MustBeLessThanEventDate", base.ClientId), Globalization.GetString("Field.DateHired", base.ClientId), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
                                BusinessAdaptorErrorType.Error);

                            bError = true;
                        }
                    }
                    //Validation without timezone
                    else
                    {

					if (objEmp.DateHired.CompareTo(sToday) > 0)
					{
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.DateHired", base.ClientId), System.DateTime.Now.ToShortDateString()),
							BusinessAdaptorErrorType.Error);

						bError = true;
					}
					else if (objEmp.DateHired.CompareTo(objEvent.DateOfEvent)>0 && objEvent.DateOfEvent!="")
					{
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanEventDate", base.ClientId), Globalization.GetString("Field.DateHired", base.ClientId), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
							BusinessAdaptorErrorType.Error);

						bError = true;
					}
				}
				}

				if (objEmp.DateOfDeath!= "")
				{
                    //Validation in case of TimeZone
                    if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                    {

                        if (objEmp.DateOfDeath.CompareTo(sOrgLevelToday) > 0)
                        {
                            Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                                String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.DateOfDeath", base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                                BusinessAdaptorErrorType.Error);

                            bError = true;
                        }
                        else if (objEmp.DateOfDeath.CompareTo(objEvent.DateOfEvent) < 0)
                        {
                            Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                                String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate", base.ClientId), Globalization.GetString("Field.DateOfDeath", base.ClientId), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
                                BusinessAdaptorErrorType.Error);

                            bError = true;
                        }
                    }
                    //Validation without timezone
                    else
                    {

					if (objEmp.DateOfDeath.CompareTo(sToday) > 0)
					{
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.DateOfDeath", base.ClientId), System.DateTime.Now.ToShortDateString()),
							BusinessAdaptorErrorType.Error);

						bError = true;
					}
					else if (objEmp.DateOfDeath.CompareTo(objEvent.DateOfEvent) < 0)
					{
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeGreaterThanEventDate", base.ClientId), Globalization.GetString("Field.DateOfDeath", base.ClientId), Conversion.ToDate(objEvent.DateOfEvent).ToShortDateString()),
							BusinessAdaptorErrorType.Error);

						bError = true;
					}
				}
				}

                //Validation in case of TimeZone
                if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                {

                    if (objEmp.PiEntity.BirthDate != "" && objEmp.PiEntity.BirthDate.CompareTo(sOrgLevelToday) > 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);

                        bError = true;
                    }
                }
                //Validation without timezone
                else
                {
				if (objEmp.PiEntity.BirthDate!="" && objEmp.PiEntity.BirthDate.CompareTo(sToday)>0)
				{
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId), System.DateTime.Now.ToShortDateString()),
						BusinessAdaptorErrorType.Error);

					bError = true;
				}
                }

                //pmittal5 MITS:12360  06/16/08
                if (objEmp.TermDate != "")
                {   //Validation in case of TimeZone
                    if (iTimeZoneTracking != 0 && OrgTimeZoneLevel != 0)
                    {
                        if (objEmp.TermDate.CompareTo(sOrgLevelToday) > 0)
                        {
                            Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                                String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.TermDate", base.ClientId), dtOrgLevelCurrentDateTime.ToShortDateString()),
                                BusinessAdaptorErrorType.Error);
                            bError = true;
                        }
                    }
                    //Validation without timezone
                    else
                    {
                    if (objEmp.TermDate.CompareTo(sToday) > 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeLessThanTodaysDate", base.ClientId), Globalization.GetString("Field.TermDate", base.ClientId), System.DateTime.Now.ToShortDateString()),
                            BusinessAdaptorErrorType.Error);
                        bError = true;
                        }
                    }
                    if (objEmp.TermDate.CompareTo(objEmp.DateHired) < 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.TermDate", base.ClientId), Globalization.GetString("Field.DateHired", base.ClientId)),
                            BusinessAdaptorErrorType.Error);
                        bError = true;
                    }
                    if (objEmp.TermDate.CompareTo(objEmp.PiEntity.BirthDate) < 0)
                    {
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.TermDate", base.ClientId), Globalization.GetString("Field.BirthDate", base.ClientId)),
                            BusinessAdaptorErrorType.Error);
                        bError = true;
                    }
                }
                //End - pmittal5
			}

			switch(objData.Context.InternalSettings.CacheFunctions.GetShortCode(objEmp.DisabilityCode))
			{
				case "ILL":
					if (objEmp.IllnessCode==0)
					{
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.IllnessCodeIsRequired", base.ClientId), Globalization.GetString("Field.IllnessCode", base.ClientId)),
							BusinessAdaptorErrorType.Error);

						bError = true;
					}
					break;
				case "INJ":
					if (objEmp.InjuryCodes.Count==0)
					{
                        Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                            String.Format(Globalization.GetString("Validation.InjuryTypeCodeIsRequired", base.ClientId), Globalization.GetString("Field.InjuryCodes", base.ClientId)),
							BusinessAdaptorErrorType.Error);

						bError = true;
					}
					break;
			}
            //Charanpreet for MITS 12174 : Start
            if (objEvent.DeptEid > 0)
            {
                bool bValerror = false;
                bValerror = objClaim.OrgHierarchyValidate();
                if (bValerror)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                        "Selected Department Not Within Effective Date Range",
                        BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            //Charanpreet for Mits 12174 : End

            // rrachev JIRA 5121 Begin
            if (bCloseDiaries)
            {
                string sContainsOpenDiariesOnFormOpen = GetSysExDataNodeText("ContainsOpenDiaries");
                if (sContainsOpenDiariesOnFormOpen == "false" && HasOpenDiaries)
                {
                    this.ResetSysExData("ContainsOpenDiaries", "true");
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("Validation.DiariesWereOpening", base.ClientId), BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }
            // rrachev JIRA 5121 End

			// Return true if there were validation errors
			Cancel = bError;
		}


		// BSB InitNew on 1:1 case causes navigation to be aborted if we slap in the key
		// of the parent because the value is shared as the key of the current record.
		// This makes navigation silently stop becuase the assumption is that you are already
		// looking at the record data you've requested.  As a work-around for only the 
		// 1:1 Cases let's zero this back out before the navigation call.
		public override void BeforeAction(enumFormActionType eActionType, ref bool Cancel)
		{
			base.BeforeAction (eActionType, ref Cancel);
			switch(eActionType)
			{
				case enumFormActionType.MoveFirst:
				case enumFormActionType.MoveLast:
				case enumFormActionType.MoveTo:
					objClaim.EventId=0;
					break;
			}
		}	
		
		//BSB 1:1 Hack Let's throw the proper parent key back on in case the MoveFirst\Last request
		// didn't find any records (we want to be sure the eventId stays set when creating a new
		// record.
		public override void AfterAction(enumFormActionType eActionType)
		{
			base.AfterAction(eActionType);
			switch(eActionType)
			{
				case enumFormActionType.MoveFirst:
				case enumFormActionType.MoveLast:
				case enumFormActionType.MoveTo:
					if(objClaim.EventId==0) //No record found - treat as new.
						objClaim.EventId = this.m_ParentId;
					break;
			}
		}

		private void SetAWWSysExNodes(int p_iAWWFormOption)
		{
			int iBaseArrayNumber=0;

			//set Base Array Number
			switch (p_iAWWFormOption)
			{
				case -1:
					break;
				case 0:
				case 52:
				case 529:
					iBaseArrayNumber=0;
					break;
				case 13:
				case 149:
					iBaseArrayNumber = 39;
					break;
				case 26:
					iBaseArrayNumber = 26;
					break;
				case 61: //Florida
					iBaseArrayNumber = 38;
					break;
			}
			base.ResetSysExData("BaseArrayNumber",iBaseArrayNumber.ToString());

			//set Week Array
			string sWeekArray=string.Empty;

			for (int i=iBaseArrayNumber;i<52;i++)
				sWeekArray=sWeekArray + "," + objClaim.ClaimAWW.GetWeek(i);

			sWeekArray=sWeekArray.Substring(1);
			
			base.ResetSysExData("WeekArray",sWeekArray);

			base.ResetSysExData("IncludeZeros",objClaim.ClaimAWW.IncludeZeros.ToString());
			base.ResetSysExData("ConstantWage",objClaim.ClaimAWW.ConstantWage.ToString());
			base.ResetSysExData("Bonuses",objClaim.ClaimAWW.Bonuses.ToString());
			base.ResetSysExData("LastWorkWeek",objClaim.ClaimAWW.LastWorkWeek);
			base.ResetSysExData("Aww",objClaim.ClaimAWW.Aww.ToString());
		}

        //Changed by Gagan for MITS 11451 : Start

        /// <summary>
        /// Deletes all auto checks for a claim
        /// </summary>
        /// <param name="p_iClaimID"></param>
        private void DeleteAutoChecks()
        {
            string sSQL;

            sSQL = "DELETE FROM FUNDS_AUTO_SPLIT WHERE AUTO_TRANS_ID IN (SELECT AUTO_TRANS_ID FROM FUNDS_AUTO WHERE CLAIM_ID = " + objClaim.ClaimId.ToString() + ")";
            objClaim.Context.DbConnLookup.ExecuteNonQuery(sSQL);

            sSQL = "DELETE FROM FUNDS_AUTO_BATCH WHERE AUTO_BATCH_ID IN (SELECT AUTO_BATCH_ID FROM FUNDS_AUTO WHERE CLAIM_ID = " + objClaim.ClaimId.ToString() + ")";
            objClaim.Context.DbConnLookup.ExecuteNonQuery(sSQL);

            sSQL = "DELETE FROM FUNDS_AUTO WHERE CLAIM_ID = " + objClaim.ClaimId.ToString();
            objClaim.Context.DbConnLookup.ExecuteNonQuery(sSQL);
        }

        //Changed by Gagan for MITS 11451 : End



        private void CloseAllDiaries()
        {
            SysSettings objSysSettings = null;
            objSysSettings = new SysSettings(objClaim.Context.DbConn.ConnectionString, base.ClientId); //Ash - cloud
            if (bCloseDiaries && sUserCloseDiary == "true")
            {
                objClaim.Context.DbConnLookup.ExecuteNonQuery(
                    String.Format(@"UPDATE WPA_DIARY_ENTRY SET STATUS_OPEN = 0 
					WHERE ATTACH_RECORDID = {0} AND ATTACH_TABLE = 'CLAIM'", objClaim.ClaimId));

                // Deleting all diaries
                //Check and close Diaries
                // sUserCloseDiary applied by Rahul 6th Feb 2006.
                if (objSysSettings.DeleteAllClaimDiaries != 0)
                {
                    int iCount = 0;
                    Event objEvent = null;
                    int i = 0;

                    string sSQL = "SELECT TRANS_ID FROM FUNDS WHERE CLAIM_ID=" + objClaim.ClaimId;

                    using(DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                    {
                        while (objReader.Read())
                        {
                            //sClaimDetails[i,0]=objReader.GetValue("TRANS_ID").ToString();
                            //sClaimDetails[i,1]="FUNDS";
                            iCount++;
                        }
                    }

                    iCount = iCount + objClaim.AdjusterList.Count;
                    iCount = iCount + objClaim.ClaimantList.Count;
                    iCount = iCount + objClaim.DefendantList.Count;
                    iCount = iCount + objClaim.LitigationList.Count;
                    objEvent = (objClaim.Parent as Event);
                    iCount = iCount + objEvent.PiList.Count;

                    string[,] sClaimDetails = new string[iCount, 2];

                    foreach (ClaimAdjuster objAdjuster in objClaim.AdjusterList)
                    {
                        sClaimDetails[i, 0] = objAdjuster.AdjRowId.ToString();
                        sClaimDetails[i, 1] = "ADJUSTER";
                        i++;
                    }
                    foreach (Claimant objClaimant in objClaim.ClaimantList)
                    {
                        sClaimDetails[i, 0] = objClaimant.ClaimantEid.ToString();
                        sClaimDetails[i, 1] = "CLAIMANT";
                        i++;
                    }
                    foreach (Defendant objDefendant in objClaim.DefendantList)
                    {
                        sClaimDetails[i, 0] = objDefendant.AttorneyEid.ToString();
                        sClaimDetails[i, 1] = "DEFENDANT";
                        i++;
                    }
                    foreach (ClaimXLitigation objLitigation in objClaim.LitigationList)
                    {
                        sClaimDetails[i, 0] = objLitigation.JudgeEid.ToString();
                        sClaimDetails[i, 1] = "LITIGATION";
                        i++;
                    }
                    foreach (PersonInvolved objPersons in objEvent.PiList)
                    {
                        int piTypeCode = objPersons.PiTypeCode;
                        string sTable = string.Empty;
                        switch (objClaim.Context.LocalCache.GetShortCode(piTypeCode))
                        {
                            case "E":
                                sTable = "PiEmployee";
                                break;
                            case "MED":
                                sTable = "PiMedicalStaff";
                                break;
                            case "O":
                                sTable = "PiOther";
                                break;
                            case "P":
                                sTable = "PiPatient";
                                break;
                            case "PHYS":
                                sTable = "PiPhysician";
                                break;
                            case "W":
                                sTable = "PiWitness";
                                break;
                        }
                        sTable = sTable.ToUpper();
                        sClaimDetails[i, 0] = objPersons.PiRowId.ToString();
                        sClaimDetails[i, 1] = sTable;
                        i++;
                    }

                    using(DbReader objReader = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                    {
                        while (objReader.Read())
                        {
                            sClaimDetails[i, 0] = objReader.GetValue("TRANS_ID").ToString();
                            sClaimDetails[i, 1] = "FUNDS";
                            i++;
                        }
                    }
                    for (int j = 0; j < iCount; j++)
                    {
                        objClaim.Context.DbConnLookup.ExecuteNonQuery(
                            String.Format(@"UPDATE WPA_DIARY_ENTRY SET STATUS_OPEN = 0 
					WHERE ATTACH_RECORDID = {0} AND ATTACH_TABLE = '{1}'", sClaimDetails[j, 0], sClaimDetails[j, 1]));
                    }

                }
            }
            bCloseDiaries = false;
        }

        //Created by pmittal5 Mits 15752 06/12/09 - Check if already selected Plan meets the claim's criteria
        private bool ValidatePlan(int p_iPlanId)
        {
            string sSQL = CreateSqlForSelectingPlan(p_iPlanId);
            using (DbReader objReader = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
            {
                if (objReader.Read())
                {
                    return true;

                }
                else { return false; }
            }
        }

        //Created by pmittal5 Mits 15752 06/12/09 - Check if already selected Plan meets the claim's criteria
        private string CreateSqlForSelectingPlan(int p_iPlanId)
        {
            string sSQL = string.Empty;
            string sGetOrgForDept = string.Empty;
            int iCN_PLAN_IN_EFFECT = 0;

            Event objEvent = (objClaim.Parent as Event);

            iCN_PLAN_IN_EFFECT = objCache.GetCodeId("I", "PLAN_STATUS");

            sGetOrgForDept = GetOrgListForDept(objEvent.DeptEid);

            sSQL = "SELECT DISABILITY_PLAN.PLAN_ID,PLAN_NUMBER,PLAN_NAME,EFFECTIVE_DATE,EXPIRATION_DATE,DTTM_RCD_LAST_UPD";
            sSQL = sSQL + " FROM DISABILITY_PLAN, DIS_PLAN_X_INSURED";
            sSQL = sSQL + " WHERE DISABILITY_PLAN.PLAN_ID = DIS_PLAN_X_INSURED.PLAN_ID";
            sSQL = sSQL + "      AND DIS_PLAN_X_INSURED.INSURED_EID IN (" + sGetOrgForDept + ")";
            sSQL = sSQL + " AND ((DISABILITY_PLAN.PLAN_STATUS_CODE = " + iCN_PLAN_IN_EFFECT + ")";
            sSQL = sSQL + " AND ((DISABILITY_PLAN.EFFECTIVE_DATE <= '" + objEvent.DateOfEvent + "'";
            sSQL = sSQL + "       AND DISABILITY_PLAN.EXPIRATION_DATE >= '" + objEvent.DateOfEvent + "')";
            sSQL = sSQL + "      OR (DISABILITY_PLAN.EFFECTIVE_DATE <= '" + objClaim.DateOfClaim + "'";
            sSQL = sSQL + "          AND DISABILITY_PLAN.EXPIRATION_DATE >= '" + objClaim.DateOfClaim + "')))";
            sSQL = sSQL + " AND DISABILITY_PLAN.PLAN_ID = " + p_iPlanId;
            sSQL = sSQL + " ORDER BY PLAN_NUMBER ";

            return sSQL;
        }

        //Created by pmittal5 Mits 15752 06/12/09 - Check if already selected Plan meets the claim's criteria
        private string GetOrgListForDept(int p_iDeptEId)
        {
            string sSQL = string.Empty;
            string sGetOrgListForDept = string.Empty;
            sSQL = "SELECT ENTITY.ENTITY_ID, E1.ENTITY_ID, E2.ENTITY_ID, E3.ENTITY_ID, E4.ENTITY_ID, E5.ENTITY_ID, E6.ENTITY_ID, E7.ENTITY_ID " +
                 " FROM ENTITY, ENTITY E1, ENTITY E2, ENTITY E3, ENTITY E4, ENTITY E5, ENTITY E6, ENTITY E7 " +
                 " WHERE ENTITY.ENTITY_ID=" + p_iDeptEId + " AND E1.ENTITY_ID=ENTITY.PARENT_EID AND " +
                 " E2.ENTITY_ID=E1.PARENT_EID AND E3.ENTITY_ID=E2.PARENT_EID AND E4.ENTITY_ID=E3.PARENT_EID " +
                 " AND E5.ENTITY_ID=E4.PARENT_EID AND E6.ENTITY_ID=E5.PARENT_EID AND E7.ENTITY_ID=E6.PARENT_EID";

            using (DbReader objReader = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
            {

                if (objReader.Read())
                    sGetOrgListForDept = p_iDeptEId.ToString() + "," +
                                           Conversion.ConvertObjToStr(objReader.GetValue(0)) + "," +
                                           Conversion.ConvertObjToStr(objReader.GetValue(1)) + "," +
                                           Conversion.ConvertObjToStr(objReader.GetValue(2)) + "," +
                                           Conversion.ConvertObjToStr(objReader.GetValue(3)) + "," +
                                           Conversion.ConvertObjToStr(objReader.GetValue(4)) + "," +
                                           Conversion.ConvertObjToStr(objReader.GetValue(5)) + "," +
                                           Conversion.ConvertObjToStr(objReader.GetValue(6)) + "," +
                                           Conversion.ConvertObjToStr(objReader.GetValue(7));
            }
            return sGetOrgListForDept;


        }

        private void ApplyFormTitle()
        {
            string sCaption = "";
            int captionLevel = 0;
            string sTmp = "";
            string sDiaryMessage = "";
            Event objEvent = (objClaim.Parent as Event);
            //Handle User Specific Caption Level
            if (!objClaim.IsNew || objClaim.PrimaryClaimant.ClaimantRowId != 0)
            {

                if (objLobSettings.CaptionLevel != 0)
                    captionLevel = objLobSettings.CaptionLevel;
                else
                    captionLevel = 1006;
                //nadim for 15014
                if (captionLevel < 1005 || captionLevel > 1012)
                    captionLevel = 0;

                if (captionLevel != 1012)
                    sCaption = objClaim.Context.InternalSettings.CacheFunctions.GetOrgParent(objEvent.DeptEid, captionLevel, 0, ref captionLevel);//captionLevel discarded
                else
                {
                    objClaim.Context.InternalSettings.CacheFunctions.GetOrgInfo(objEvent.DeptEid, ref sCaption, ref sTmp);
                    sCaption += " - " + sTmp;
                }
                sCaption = sCaption.Substring(0, Math.Min(sCaption.Length, 25));

                if (objClaim.EventId != 0 && objClaim.PrimaryClaimant != null && objClaim.PrimaryClaimant.ClaimantEntity != null)
                {
                    sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * " +
                        objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName() + "]";
                    sDiaryMessage = objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
                }
                else
                {
                    sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * - ]";
                    sDiaryMessage = "";
                }
            }
            //Pass this subtitle value to view (ref'ed from @valuepath).
            if (objClaim.IsNew)
                sCaption = "";
            base.ResetSysExData("SubTitle", sCaption);

            //Raman: Adding SubTitle to modified controls list
            ArrayList singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "formsubtitle");
            base.AddElementToList(ref singleRow, "Text", sCaption);
            base.m_ModifiedControls.Add(singleRow);

            base.ResetSysExData("DiaryMessage", sDiaryMessage);
            base.ResetSysExData("ClaimNumber", objClaim.ClaimNumber);
        }

        private bool HasOpenDiaries
        {
            get
            {
                //Nikhil Garg		17-Feb-2006
                //check whether open diaries exist for this claim or not
                string sSQL = "SELECT COUNT(*) FROM WPA_DIARY_ENTRY WHERE STATUS_OPEN<>0 AND ATTACH_RECORDID = " + objClaim.ClaimId.ToString() + " AND ATTACH_TABLE = 'CLAIM'";
                return objClaim.Context.DbConn.ExecuteInt(sSQL) > 0;
            }
        }
	}
}
