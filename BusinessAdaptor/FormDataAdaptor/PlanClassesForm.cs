﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Application.FundManagement;
using System.Text;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// PlanClasses Screen.
	/// </summary>
	public class PlanClassesForm : DataEntryFormBase
	{
		const string CLASS_NAME = "DisabilityClass";
		const string FILTER_KEY_NAME = "PlanId";

		private DisabilityClass objDisabilityClass{get{return objData as DisabilityClass;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}

		private FundManager m_objFundManager = null ;	
        ArrayList singleRow = null;//mona
	
		public PlanClassesForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		public override void InitNew()
		{
			base.InitNew();

			//set filter for Plan Id
			objDisabilityClass.PlanId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			if (objDisabilityClass.PlanId>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + objDisabilityClass.PlanId;

		}

		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();

			AddComboboxList("BenePrdType","DURATION_TYPE");
			AddComboboxList("BeneFromDtType","HIRED_PERIOD_TYPE");
			AddComboboxList("DisClndrWrkCode","CALENDAR_WORK");
			AddComboboxList("DisPrdType","DURATION_TYPE");
			AddComboboxList("DisMaxPrdType","DURATION_TYPE");
			AddComboboxList("FromDisPrdType","BENE_START_PERIOD");
			AddComboboxList("BeneBasedCode","RATE_TYPE");

			AddNodesForRadioButtons();
			
			//11/20/2006 Raman Bhatia: LTD Module Implementation
			//Changes in form due to new radio buttons in "Determine Work Period"		
			
			string sSQL =	"SELECT PREF_PAY_SCH_CODE FROM DISABILITY_PLAN WHERE PLAN_ID = " + objDisabilityClass.PlanId;

			DbReader objReader = null;
            string sSchedule = "";
            string sDesc = "";
            XmlDocument objXML = null;
            XmlElement objPreferredPaymentSchedule = null;
            XmlElement objSupplementTableDataSum = null;
            XmlElement objReserveTypeElement = null;
            XmlElement objXmlElement = null;

            using (objReader = objDisabilityClass.Context.DbConnLookup.ExecuteReader(sSQL))
            {
                if (objReader != null)
                {
                    //Loop through and create all the option values for the combobox control
                    while (objReader.Read())
                    {
                        objDisabilityClass.Context.LocalCache.GetCodeInfo(objReader.GetInt32("PREF_PAY_SCH_CODE"), ref sSchedule, ref sDesc);  //Aman No need for the langugae oode here b'coz PERIOD_TYPES is the system table
                    } // while
                }
            }
            //if(objReader!=null)
            //{
            //    objReader.Close();
            //}

			//Changing captions in xml in case of long term disability
			
			if(sSchedule.ToUpper() == "PM")
			{
                //objXmlElement = (XmlElement) base.SysView.SelectSingleNode("//control[@name='bencap']");
                //objXmlElement.SetAttribute("title" , "Monthly Benefit Cap(Leave 0 for None)");

				//objXmlElement = (XmlElement) base.SysView.SelectSingleNode("//control[@name='weeklyben']");
				//objXmlElement.SetAttribute("pvtitle" , "Percentage of Monthly Wage");
				//objXmlElement.SetAttribute("label" , "Percentage of Monthly Wage");
         
                //objXmlElement = (XmlElement) base.SysView.SelectSingleNode("//control[@name='offsetsflatamounts']");
                //objXmlElement.SetAttribute("pvtitle" , "Offsets - Monthly Flat Amounts");
				//objXmlElement.InnerText =  "Offsets - Monthly Flat Amounts";


                //Mona:Start


                singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                base.AddElementToList(ref singleRow, "id", "lbl_bencap");
                base.AddElementToList(ref singleRow, "Text", "Monthly Benefit Cap (Leave 0 for None)");
                base.m_ModifiedControls.Add(singleRow);
                //singleRow.Clear();
                singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.RadioButton.ToString());
                base.AddElementToList(ref singleRow, "id", "weeklyben1");
                base.AddElementToList(ref singleRow, "Text", "Percentage of Monthly Wage");
                base.m_ModifiedControls.Add(singleRow);

                singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                base.AddElementToList(ref singleRow, "id", "offsetsflatamounts");
                base.AddElementToList(ref singleRow, "Text", "Offsets - Monthly Flat Amounts");
                base.m_ModifiedControls.Add(singleRow);
                singleRow = new ArrayList();
                base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                base.AddElementToList(ref singleRow, "id", "PstTaxDedT");
                base.AddElementToList(ref singleRow, "Text", "Post Tax Deductions : Monthly Flat Amounts");
                base.m_ModifiedControls.Add(singleRow);
                 
                //Mona-End
			}
			
            //objXmlElement = (XmlElement) base.SysView.SelectSingleNode("//control[@name='weeklybenlabel']");
            //objXmlElement.SetAttribute("pvtitle" , "Benefit Calculation");
            //objXmlElement.InnerText = "Benefit Calculation:";
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "weeklybenlabel");
            base.AddElementToList(ref singleRow, "Text", "Benefit Calculation:");
            base.m_ModifiedControls.Add(singleRow);

			//Add the Supplement/Buy Up option section

			//Fill the Bank Account combobox
			AppendBankAccountList();
			//this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc
			//objXmlElement = (XmlElement) base.SysView.SelectSingleNode("//control[@name='BankAccount']");
            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.ComboBox.ToString());
            base.AddElementToList(ref singleRow, "id", "BankAccount");
			if(this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
            {//objXmlElement.SetAttribute("ref" , "/Instance/DisabilityClass/SuppSubAccId");
                base.AddElementToList(ref singleRow, "ref", "/Instance/DisabilityClass/SuppSubAccId");
            }
            else
            {//   objXmlElement.SetAttribute("ref", "/Instance/DisabilityClass/SuppBankAccId");
                base.AddElementToList(ref singleRow, "ref", "/Instance/DisabilityClass/SuppBankAccId");
            }
            base.m_ModifiedControls.Add(singleRow);
			//Fill the Transaction Type Combobox

			LoadDisTransTypes();

			//Fill the Reserve Type textbox

			int iRelatedCodeID = objDisabilityClass.Context.LocalCache.GetRelatedCodeId(objDisabilityClass.SuppTrTypeCode);
			string sReserveType = objDisabilityClass.Context.LocalCache.GetCodeDesc(iRelatedCodeID,base.Adaptor.userLogin.objUser.NlsCode); //Aman ML Change
			
			objXML = base.SysEx;
			objReserveTypeElement = objXML.CreateElement("ReserveType");
			objReserveTypeElement.InnerText = sReserveType;
			objXML.DocumentElement.AppendChild(objReserveTypeElement);

			
			// SETTING A NODE IN SYSEX for Preferred Payment Schedule Flag
			
			objXML = base.SysEx;
			objPreferredPaymentSchedule = objXML.CreateElement("PreferredPaymentSchedule");
			objPreferredPaymentSchedule.InnerText = sSchedule;
			objXML.DocumentElement.AppendChild(objPreferredPaymentSchedule);
			
			//Setting a node in SYSEX for TD Supplement Amount Total

			sSQL =	"select sum(supp_amt) from dis_class_td where class_id = " + objDisabilityClass.ClassRowId;

            using (objReader = objDisabilityClass.Context.DbConnLookup.ExecuteReader(sSQL))
            {

                objXML = base.SysEx;
                objSupplementTableDataSum = objXML.CreateElement("SupplementTableDataSum");
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        objSupplementTableDataSum.InnerText = objReader.GetValue(0).ToString();

                    }
                }
            }
			
            //if(objReader!=null)
            //{
            //    objReader.Close();
            //}
			objXML.DocumentElement.AppendChild(objSupplementTableDataSum);
			
		}

		public override void OnUpdateObject()
		{
			base.OnUpdateObject();
			
			string sWorkWeek=string.Empty;
			string sWeeklyBen=string.Empty;
			int iSubAccountID = 0;
			string sSQL = "";
			DbReader objReader = null;

			XmlDocument objXML=base.SysEx;
			
			if(this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
			{
				iSubAccountID = objDisabilityClass.SuppSubAccId;
				if (iSubAccountID==0)
				{
					objDisabilityClass.SuppBankAccId = 0;
				}
				sSQL = "SELECT ACCOUNT_ID FROM BANK_ACC_SUB WHERE SUB_ROW_ID=" + iSubAccountID.ToString();
                using (objReader = objDisabilityClass.Context.DbConnLookup.ExecuteReader(sSQL))
                {

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            objDisabilityClass.SuppBankAccId = Conversion.ConvertObjToInt(objReader.GetValue("ACCOUNT_ID"), base.ClientId);
                        }

                    }
                }

			}
            //if(objReader!=null)
            //    objReader.Close();

			sWorkWeek=objXML.SelectSingleNode("//workweek").InnerText;
			sWeeklyBen=objXML.SelectSingleNode("//weeklyben").InnerText;

			//11/20/2006 Raman Bhatia: LTD Module Implementation
			//New radio buttons in "Determine Work Period"			
			switch (sWorkWeek)
			{
				case "ACTUAL_WORK_WEEK":
					objDisabilityClass.ActualWorkWeek=true;
					objDisabilityClass.Day7WorkWeek=false;
					objDisabilityClass.Day5WorkWeek=false;
					objDisabilityClass.ActualMonth=false;
					objDisabilityClass.Day30WorkMonth=false;
					break;
				case "DAY7_WORK_WEEK":
					objDisabilityClass.ActualWorkWeek=false;
					objDisabilityClass.Day7WorkWeek=true;
					objDisabilityClass.Day5WorkWeek=false;
					objDisabilityClass.ActualMonth=false;
					objDisabilityClass.Day30WorkMonth=false;
					break;
				case "DAY5_WORK_WEEK":
					objDisabilityClass.ActualWorkWeek=false;
					objDisabilityClass.Day7WorkWeek=false;
					objDisabilityClass.Day5WorkWeek=true;
					objDisabilityClass.ActualMonth=false;
					objDisabilityClass.Day30WorkMonth=false;
					break;
				case "ACTUAL_MONTH":
					objDisabilityClass.ActualWorkWeek=false;
					objDisabilityClass.Day7WorkWeek=false;
					objDisabilityClass.Day5WorkWeek=false;
					objDisabilityClass.ActualMonth=true;
					objDisabilityClass.Day30WorkMonth=false;
					break;
				case "DAY30_WORK_MONTH":
					objDisabilityClass.ActualWorkWeek=false;
					objDisabilityClass.Day7WorkWeek=false;
					objDisabilityClass.Day5WorkWeek=false;
					objDisabilityClass.ActualMonth=false;
					objDisabilityClass.Day30WorkMonth=true;
					break;
			}

			switch (sWeeklyBen)
			{
				case "BENE_PRCTG_FLAG":
					objDisabilityClass.BenePrctgFlag=true;
					objDisabilityClass.BeneFlatAmtFlag=false;
					objDisabilityClass.BeneTdFlag=false;
					break;
				case "BENE_FLAT_AMT_FLAG":
					objDisabilityClass.BenePrctgFlag=false;
					objDisabilityClass.BeneFlatAmtFlag=true;
					objDisabilityClass.BeneTdFlag=false;
					break;
				case "BENE_TD_FLAG":
					objDisabilityClass.BenePrctgFlag=false;
					objDisabilityClass.BeneFlatAmtFlag=false;
					objDisabilityClass.BeneTdFlag=true;
					break;
			}

		}

		private FundManager FundManagerObject
		{
			get
			{
				if( m_objFundManager == null )
				{
					//Retrieve the security credential information to pass to the FundManager object
					string sDSNName = base.m_fda.userLogin.objRiskmasterDatabase.DataSourceName;
					string sUserName = base.m_fda.userLogin.LoginName;
					string sPassword = base.m_fda.userLogin.Password;
					int iUserId = base.m_fda.userLogin.UserId;
					int iGroupId = base.m_fda.userLogin.GroupId;

					//Create and return the instance of the FundManager object
					m_objFundManager = new FundManager(sDSNName,sUserName, sPassword, iUserId, iGroupId, base.ClientId);
				}
				return m_objFundManager ;
			}
		}

		private void AppendBankAccountList()
		{
			//Variable declarations
			ArrayList arrAcctList = new ArrayList();
			XmlDocument objXML = base.SysEx;
			XmlNode objOld=null;
			XmlElement objNew=null;
			XmlCDataSection objCData=null;

            //Retrieve the complete Bank Account List and store it in the ArrayList
            using (FundManager oFundManager = FundManagerObject)
            {
                arrAcctList = oFundManager.GetAccounts(this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc);
            }
            m_objFundManager = null;

			//Create the necessary SysExData to be used in ref binding
			objOld = objXML.SelectSingleNode("//AccountList");
			objNew = objXML.CreateElement("AccountList");

			XmlElement xmlOption = null;
			XmlAttribute xmlOptionAttrib = null;

			// Blank value for the combo box
			xmlOption = objXML.CreateElement("option");
			xmlOptionAttrib = objXML.CreateAttribute("value");
			xmlOption.Attributes.Append(xmlOptionAttrib);
			objNew.AppendChild(xmlOption);
			xmlOptionAttrib = null;
			xmlOption = null;

			//Loop through and create all the option values for the combobox control
			foreach (FundManager.AccountDetail item in arrAcctList)
			{
				xmlOption = objXML.CreateElement("option");
				objCData = objXML.CreateCDataSection(item.AccountName);
				xmlOption.AppendChild(objCData);
				xmlOptionAttrib = objXML.CreateAttribute("value");
				
				if( this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc )
					xmlOptionAttrib.Value = item.SubRowId.ToString();
				else
					xmlOptionAttrib.Value = item.AccountId.ToString();

				xmlOption.Attributes.Append(xmlOptionAttrib);
				objNew.AppendChild(xmlOption);
			}//end foreach

			//Add the XML to the SysExData 
			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);


			objOld = objXML.SelectSingleNode("//ControlAppendAttributeList");
			objNew = objXML.CreateElement("ControlAppendAttributeList");
			XmlElement objElem=null;
			XmlElement objChild=null;
		
			objElem=objXML.CreateElement("bankaccount");
			objChild=objXML.CreateElement("ref");
			if( this.objData.Context.InternalSettings.SysSettings.UseFundsSubAcc )
            //pmittal5 Mits 16223 05/05/09 - Corrected the "ref" for Bank Account
				//objChild.SetAttribute("value","/Instance/UI/FormVariables/SysExData/FundsAuto/SubAccId");	
                objChild.SetAttribute("value", "/Instance/DisabilityClass/SuppSubAccId");
			else
				//objChild.SetAttribute("value","/Instance/UI/FormVariables/SysExData/FundsAuto/AccountId");
                objChild.SetAttribute("value", "/Instance/DisabilityClass/SuppBankAccId");

			objElem.AppendChild(objChild);;
			objNew.AppendChild(objElem);

			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);
				
			//Clean up
			arrAcctList = null;
			objOld = null;
			objNew = null;
			objCData = null;
			objXML = null;
		}
		
		private void AddComboboxList(string p_sTagName, string p_sTableName)
		{
			string sSQL=string.Empty;
			XmlDocument objXML = base.SysEx;
			XmlNode objOld=null;
			XmlElement objNew=null;
			XmlCDataSection objCData=null;
			DbReader objReader=null;
			XmlElement xmlOption=null;
			XmlAttribute xmlOptionAttrib=null;

			//Create the necessary SysExData to be used in ref binding
			objOld = objXML.SelectSingleNode("//"+p_sTagName);
			objNew = objXML.CreateElement(p_sTagName);

			if(objDisabilityClass.Context.DbConn.DatabaseType==eDatabaseType.DBMS_IS_ORACLE)
			{
				sSQL =	"SELECT CODES.CODE_ID, CODES.SHORT_CODE || ' - ' || CODES_TEXT.CODE_DESC" ;
			}
			else
			{
				sSQL =	"SELECT CODES.CODE_ID, CODES.SHORT_CODE + ' - ' + CODES_TEXT.CODE_DESC" ;
			}
			sSQL = sSQL +
				" FROM CODES, CODES_TEXT, GLOSSARY" +
				" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID" +
				" AND CODES.TABLE_ID = GLOSSARY.TABLE_ID" +
                " AND CODES_TEXT.LANGUAGE_CODE = 1033 " +                //Aman ML Change
				" AND GLOSSARY.SYSTEM_TABLE_NAME = '" + p_sTableName + "'" +
				" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)";
            //Aman ML Change
            StringBuilder sbSQL = new StringBuilder();
            sbSQL = CommonFunctions.PrepareMultilingualQuery(sSQL, p_sTableName, base.Adaptor.userLogin.objUser.NlsCode);
            using (objReader = objDisabilityClass.Context.DbConnLookup.ExecuteReader(sbSQL.ToString()))  //Aman ML Change
            {

                if (objReader != null)
                {
                    //Loop through and create all the option values for the combobox control
                    while (objReader.Read())
                    {
                        xmlOption = objXML.CreateElement("option");
                        objCData = objXML.CreateCDataSection(Conversion.ConvertObjToStr(objReader[1]));
                        xmlOption.AppendChild(objCData);
                        xmlOptionAttrib = objXML.CreateAttribute("value");
                        xmlOptionAttrib.Value = Conversion.ConvertObjToInt(objReader[0], base.ClientId).ToString();
                        xmlOption.Attributes.Append(xmlOptionAttrib);
                        objNew.AppendChild(xmlOption);
                    }//end foreach

                    //objReader.Close();
                    //objReader = null;
                }
            }

			//Add the XML to the SysExData 
			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);
		}

		private void LoadDisTransTypes()
		{
			string sSQL=string.Empty;
			XmlDocument objXML = base.SysEx;
			XmlNode objOld=null;
			XmlElement objNew=null;
			DbReader objReader=null;
			XmlElement xmlOption = null;
			XmlCDataSection objCData = null;
			XmlAttribute xmlOptionAttrib = null;
			int iRelatedCodeID = 0;
			XmlElement objReserveType = null;
            ArrayList singleRow = new ArrayList();//mona
			//Create the necessary SysExData to be used in ref binding
			objOld = objXML.SelectSingleNode("//TransactionType");
			objNew = objXML.CreateElement("TransactionType");
            //Aman ML Change
			 sSQL = "SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC,TRIGGER_DATE_FIELD,EFF_START_DATE,EFF_END_DATE,ORG_GROUP_EID,LINE_OF_BUS_CODE FROM CODES,CODES_TEXT WHERE CODES.TABLE_ID = " + objDisabilityClass.Context.LocalCache.GetTableId("TRANS_TYPES") + " AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = 1033 AND CODES.RELATED_CODE_ID IN (SELECT SYS_LOB_RESERVES.RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES,CODES " 
				 + "WHERE SYS_LOB_RESERVES.LINE_OF_BUS_CODE = 844 AND SYS_LOB_RESERVES.RESERVE_TYPE_CODE = CODES.CODE_ID AND CODES.RELATED_CODE_ID = 1066) AND CODES.DELETED_FLAG = 0" 
			     + " AND CODES.CODE_ID NOT IN (SELECT TRANS_TYPE_CODE_ID FROM TAX_MAPPING)";

             StringBuilder sbSQL = new StringBuilder();
             sbSQL = CommonFunctions.PrepareMultilingualQuery(sSQL, "TRANS_TYPES", base.Adaptor.userLogin.objUser.NlsCode);
             using (objReader = objDisabilityClass.Context.DbConnLookup.ExecuteReader(sbSQL.ToString()))  //Aman ML Change
             {

                 // Blank value for the combo box
                 xmlOption = objXML.CreateElement("option");
                 xmlOptionAttrib = objXML.CreateAttribute("value");
                 xmlOption.Attributes.Append(xmlOptionAttrib);
                 xmlOptionAttrib = objXML.CreateAttribute("ReserveType");
                 xmlOption.Attributes.Append(xmlOptionAttrib);
                 objNew.AppendChild(xmlOption);
                 xmlOptionAttrib = null;
                 xmlOption = null;

                 if (objReader != null)
                 {
                     //Loop through and create all the option values for the combobox control
                     while (objReader.Read())
                     {
                         xmlOption = objXML.CreateElement("option");
                         objCData = objXML.CreateCDataSection(Conversion.ConvertObjToStr(objReader[1]) + " - " + Conversion.ConvertObjToStr(objReader[2]));
                         xmlOption.AppendChild(objCData);
                         xmlOptionAttrib = objXML.CreateAttribute("value");
                         xmlOptionAttrib.Value = Conversion.ConvertObjToInt(objReader[0], base.ClientId).ToString();
                         xmlOption.Attributes.Append(xmlOptionAttrib);
                         xmlOptionAttrib = objXML.CreateAttribute("ReserveType");
                         iRelatedCodeID = objDisabilityClass.Context.LocalCache.GetRelatedCodeId(Conversion.ConvertObjToInt(objReader[0], base.ClientId));
                         xmlOptionAttrib.Value = objDisabilityClass.Context.LocalCache.GetCodeDesc(iRelatedCodeID,base.Adaptor.userLogin.objUser.NlsCode); //Aman ML Change
                         xmlOption.Attributes.Append(xmlOptionAttrib);
                         objNew.AppendChild(xmlOption);

                         //Creating a Hidden control for keeping reserve type 
                         //mona-comment:start
                         //objReserveType = base.SysView.CreateElement("control");
                         //objReserveType.SetAttribute("name", ("ReserveType_" + Conversion.ConvertObjToStr(objReader[0])));
                         //objReserveType.SetAttribute("id", ("ReserveType_" + Conversion.ConvertObjToStr(objReader[0])));
                         //objReserveType.SetAttribute("type", "hidden");
                         //string sReserveTypeXpathString = "//control[@name='ReserveType_" + Conversion.ConvertObjToStr(objReader[0]) + "']";
                         //objReserveType.SetAttribute("ref", sReserveTypeXpathString);
                         //objReserveType.InnerText = objDisabilityClass.Context.LocalCache.GetCodeDesc(iRelatedCodeID);

                         //XmlElement objParentGroup = (XmlElement)base.SysView.SelectSingleNode("//group[@name='bencalc']");
                         //XmlElement objChild = base.SysView.CreateElement("displaycolumn");
                         //objChild.AppendChild(objReserveType);
                         //objParentGroup.AppendChild(objChild);
                         //mona-comment:End
                         singleRow = new ArrayList();
                         string sReserveTypeXpathString = "ReserveType_" + Conversion.ConvertObjToStr(objReader[0]) ;
                         base.CreateSysExData(sReserveTypeXpathString, objDisabilityClass.Context.LocalCache.GetCodeDesc(iRelatedCodeID,base.Adaptor.userLogin.objUser.NlsCode)); //Aman ML Change
                         base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Hidden.ToString());
                         base.AddElementToList(ref singleRow, "id", "ReserveType_" + Conversion.ConvertObjToStr(objReader[0]));
                         //base.AddElementToList(ref singleRow, "name", "ReserveType_" + Conversion.ConvertObjToStr(objReader[0]));
                         base.AddElementToList(ref singleRow, "Text", objDisabilityClass.Context.LocalCache.GetCodeDesc(iRelatedCodeID,base.Adaptor.userLogin.objUser.NlsCode));  //Aman ML Change
                         base.AddElementToList(ref singleRow, "NewControl", "true");
                         base.AddElementToList(ref singleRow, "ref", "/Instance/UI/FormVariables/SysExData/" + sReserveTypeXpathString);


                         base.m_ModifiedControls.Add(singleRow);

                     }//end foreach

                     //objReader.Close();
                     //objReader = null;
                 }
             }

			//Add the XML to the SysExData 
			if(objOld !=null)
				objXML.DocumentElement.ReplaceChild(objNew,objOld);
			else
				objXML.DocumentElement.AppendChild(objNew);
		}

		
		
		private void AddNodesForRadioButtons()
		{
			if (objDisabilityClass.ActualWorkWeek)
				base.ResetSysExData("workweek","ACTUAL_WORK_WEEK");
			else if (objDisabilityClass.Day7WorkWeek)
				base.ResetSysExData("workweek","DAY7_WORK_WEEK");
			else if (objDisabilityClass.Day5WorkWeek)
				base.ResetSysExData("workweek","DAY5_WORK_WEEK");
				//11/20/2006 Raman Bhatia: LTD Module Implementation
				//New radio buttons in "Determine Work Period"
			else if (objDisabilityClass.ActualMonth)
				base.ResetSysExData("workweek","ACTUAL_MONTH");
			else if (objDisabilityClass.Day30WorkMonth)
				base.ResetSysExData("workweek","DAY30_WORK_MONTH");
			else
				base.ResetSysExData("workweek","ACTUAL_WORK_WEEK");

			if (objDisabilityClass.BenePrctgFlag)
				base.ResetSysExData("weeklyben","BENE_PRCTG_FLAG");
			else if (objDisabilityClass.BeneFlatAmtFlag)
				base.ResetSysExData("weeklyben","BENE_FLAT_AMT_FLAG");
			else if (objDisabilityClass.BeneTdFlag)
				base.ResetSysExData("weeklyben","BENE_TD_FLAG");
			else
				base.ResetSysExData("weeklyben","BENE_PRCTG_FLAG");

		}
	}
}
