﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using System.Text;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using Riskmaster.Security.RMApp;
using Riskmaster.Application.FundManagement;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// NonOccForm Screen.
	/// </summary>
	public class NonOccForm : DataEntryFormBase
	{
		internal struct TaxRate
		{
			public double FED_TAX_RATE;
			public double SS_TAX_RATE;
			public double MEDICARE_TAX_RATE;
			public double STATE_TAX_RATE;
			public int iFedTaxEID; 
			public int iSSTaxEID;
			public int iMedicareTaxEID;
			public int iStateTaxEID;
			public int iFedTransTypeCodeID;
			public int iSSTransTypeCodeID;
			public int iMedicareTransTypeCodeID;
			public int iStateTransTypeCodeID;
			public bool bFed;
			public bool bSS;
			public bool bMed;
			public bool bState;
			public double dblTaxablePercent;
		}

		const string CLASS_NAME = "Claim";
		const string FILTER_KEY_NAME = "ClaimId";
		private int m_LOB;
        private bool m_bEligible = false;
        private string m_sEligTitle = string.Empty;
		private int [] iDaysOfWeek = new int[7];
		private LobSettings objLobSettings = null;

		private TaxRate m_dtpTaxes=new TaxRate();
		private bool m_bAllowCalc=true;	//set false to disable Calculate Payments button

		private XmlNode objOldAppendAttributeList = null;
		private XmlElement objNewAppendAttributeList =null;
		XmlElement objElemAppendAttributeList=null;
		XmlElement objChildAppendAttributeList=null;

        DateTime dThisPayPeriodStart = DateTime.MinValue;
        DateTime dThisPayPeriodEnd = DateTime.MinValue;
        int iDaysWorkingInMonth = 0;
        int iDaysWorkingInWeek = 0;
        int ilblDaysIncluded = 0;
        int iPrefSched = 0;
        double dGrossPayment = 0;
        double dGrossSupplement = 0;
        double m_dGrossPayment = 0; //MITS 14186  It is used to get calculation Factor
        double m_dGrossSupplement = 0; //MITS 14186  It is used to get calculation Factor
		int iOffsetCalc = 0;
        bool b30DayMonthFlag = false;
        double dTaxablePercent = 0;
        string m_sBenCalcPayStart = "";
        string m_sBenCalcPayTo = "";
        private string m_sConnectionString = "";//Neha R8 Funds utility enhacement
		//Defect 002285 Fixed by Neelima, 
		//added security for View/Edit/delete payments in Non-Occupational Claims Payment
		private const int RMO_NONOCC_PAYMENTS = 65002;

		private Claim objClaim{get{return objData as Claim;}}
        
       
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
        ArrayList singleRow = null;
		public NonOccForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
            m_sConnectionString = fda.connectionString;//Neha R8 Funds utility enhacement
		}

		public override void InitNew()
		{
			base.InitNew();

			m_LOB = objClaim.Context.LocalCache.GetCodeId("DI", "LINE_OF_BUSINESS");
			objLobSettings = objClaim.Context.InternalSettings.ColLobSettings[m_LOB];
			
			this.m_ParentId = base.GetSysExDataNodeInt("/SysExData/EventId");

			objClaim.LineOfBusCode = m_LOB;
			objClaim.NavType = Riskmaster.DataModel.ClaimNavType.ClaimNavLOB;

			if(m_ParentId >0)
				(objData as INavigation).Filter = "EVENT_ID=" + m_ParentId;
			
			objClaim.EventId = m_ParentId;
		}
       
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
        public override void OnUpdateForm()
        {
            base.OnUpdateForm();

            XmlDocument objXML = base.SysEx;
            XmlNode objOld = null;
            XmlElement objNew = null;
            int l = 0;
            DisabilityClass objDisClass = null;
            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;
            string sPrdType = string.Empty;
            string sEligDate = string.Empty;
            double dblHourlyRate = 0;
            double dblWeeklyRate = 0;
            string sWage = string.Empty;
            string sWageTitle = string.Empty;
            double dblTemp = 0;
            string sCalcMethod = string.Empty;
            double dblWeeklyBenefit = 0;
            string sStateTitle = string.Empty;
            bool[] bTaxes = new bool[] { false, false, false, false };
            string sTaxMappings = string.Empty;
            int iTaxFlags = 0;
            bool bWorkDay = false;
            DateTime dTemp = DateTime.Now;
            int iTypeOfWage = 0;
            int iPayType = 0;
            bool bUseVoucher = false;
            string sEndDisDate = string.Empty;
            int iAge = 0;
            int iLowerAgeLimit = 0;
            int iUpperAgeLimit = 0;
            int iTempAge = 0;
            bool bLAgeLimit = false;
            bool bUAgeLimit = false;
            int iBenDisType = 0;
            int m_iDefaultDistributionType = 0;
            TimeSpan objSpan;

            string sBenCalcPayTo = string.Empty;
            string sBenefitsThrough = string.Empty;
            string sMaxPeriodDayWeeks = string.Empty;
            string sBenefitStartDate = string.Empty;
            string sBenCalcPayStart = string.Empty;
            string sBenefitType = string.Empty;
            string sSql = string.Empty;
            string sDefaultDistributionTypeShortCode = string.Empty;
            string sDefaultDistributionTypeCodeDesc = string.Empty;

            dblWeeklyRate = objClaim.PrimaryPiEmployee.WeeklyRate;
            dblHourlyRate = objClaim.PrimaryPiEmployee.HourlyRate;
            double dblMonthlyRate = objClaim.PrimaryPiEmployee.MonthlyRate;

            //Defect 002285 Fixed by Neelima, 
            //added security for View/Edit/delete payments in Non-Occupational Claims Payment
            if (base.ModuleSecurityEnabled)//Is Security Enabled
            {
                if (!m_fda.userLogin.IsAllowedEx(RMO_NONOCC_PAYMENTS, RMO_ACCESS))
                {
                    base.AddKillNode("printedpaym");
                    base.AddKillNode("futurepaym");
                }
            }

            objOldAppendAttributeList = objXML.SelectSingleNode("//ControlAppendAttributeList");
            objNewAppendAttributeList = objXML.CreateElement("ControlAppendAttributeList");

            //Neha R8 Funds utility enhancement
            if (!(CommonFunctions.IsScheduleDatesOn("SCH_DATE_NONOCC_PMT", m_sConnectionString, base.ClientId)))
            {
                //base.ResetSysExData("AdjustPrintDatesFlag", objClaim.AdjustPrintDatesFlag.ToString());
                base.AddKillNode("AdjustPrintDatesFlag");
            }

            //check for required objects and fields
            if (objClaim.DisabilityPlan == null)
            {
                Errors.Add(Globalization.GetString("FillNonOccDomError", base.ClientId),
                    String.Format(Globalization.GetString("UpdateForm.FillNonOccDomFailed.DisabilityPlanNotFound", base.ClientId), objClaim.ClaimId.ToString()),
                    BusinessAdaptorErrorType.Error);
                DisableFormFields(); //pmittal5 Mits 16284 05/08/09 - Disable all fields if error is thrown
                return;
            }

            if ((objClaim.PrimaryPiEmployee as PiEmployee).PiRowId == 0)
            {
                Errors.Add(Globalization.GetString("FillNonOccDomError", base.ClientId),
                    String.Format(Globalization.GetString("UpdateForm.FillNonOccDomFailed.EmployeeNotFound", base.ClientId), objClaim.ClaimId.ToString()),
                    BusinessAdaptorErrorType.SystemError);
                DisableFormFields(); //pmittal5 Mits 16284 05/08/09 - Disable all fields if error is thrown
                return;
            }

            //add the sub title node
            objOld = objXML.SelectSingleNode("//SubTitle");
            objNew = objXML.CreateElement("SubTitle");

            if (objClaim.ClaimId != 0)
            {
                objLobSettings = objData.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode];
                if (objClaim.PrimaryClaimant.ClaimantRowId != 0)
                {
                    objNew.InnerText = " [" + objClaim.ClaimNumber + " * " +
                        objData.Context.InternalSettings.CacheFunctions.GetOrgParent((objClaim.Parent as Event).DeptEid, objLobSettings.OrgTableId, 1, ref l) + " * " +
                        objClaim.PrimaryClaimant.ClaimantEntity.LastName + ", " + objClaim.PrimaryClaimant.ClaimantEntity.FirstName + "] ";
                }
                else
                {
                    objNew.InnerText = " [" + objClaim.ClaimNumber + " * " +
                        objData.Context.InternalSettings.CacheFunctions.GetOrgParent((objClaim.Parent as Event).DeptEid, objLobSettings.OrgTableId, 1, ref l);
                }
            }
            if (objOld != null)
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            else
                objXML.DocumentElement.AppendChild(objNew);


            if (objClaim.PrimaryPiEmployee.DateHired.Trim() == "")
            {
                m_bAllowCalc = false;
                Errors.Add(Globalization.GetString("FillNonOccDomError", base.ClientId),
                    String.Format(Globalization.GetString("UpdateForm.FillNonOccDomFailed.DateHired", base.ClientId), objClaim.PrimaryPiEmployee.EmployeeNumber),
                    BusinessAdaptorErrorType.SystemError);
                DisableFormFields(); //pmittal5 Mits 16284 05/08/09 - Disable all fields if error is thrown
                return;
            }

            //Raman Bhatia: adding more validations
            if (objClaim.DisabilFromDate == "")
            {
                m_bAllowCalc = false;
                Errors.Add(Globalization.GetString("FillNonOccDomError", base.ClientId),
                    String.Format(Globalization.GetString("UpdateForm.FillNonOccDomFailed.DisabilityFromDate", base.ClientId), objClaim.PrimaryPiEmployee.EmployeeNumber),
                    BusinessAdaptorErrorType.SystemError);
                DisableFormFields(); //pmittal5 Mits 16284 05/08/09 - Disable all fields if error is thrown
                return;
            }
            // Lookup class object record 
            foreach (DisabilityClass objClass in objClaim.DisabilityPlan.ClassList)
            {
                if (objClass.ClassRowId == objClaim.ClassRowId)
                {
                    objDisClass = objClass;
                    break;
                }
            }
            if (objDisClass == null)
            {
                Errors.Add(Globalization.GetString("FillNonOccDomError", base.ClientId),
                    String.Format(Globalization.GetString("UpdateForm.FillNonOccDomFailed.DisClassNotFound", base.ClientId), objClaim.ClaimId.ToString(), objClaim.DisabilityPlan.PlanId.ToString()),
                    BusinessAdaptorErrorType.SystemError);
                DisableFormFields(); //pmittal5 Mits 16284 05/08/09 - Disable all fields if error is thrown
                return;
            }
            // Pull the preferred schedule selected on the disability plan
            // Added by Rahul, as per RMWorld.
            if (objData.Context.InternalSettings.CacheFunctions.GetShortCode(objClaim.DisabilityPlan.PrefPaySchCode).ToLower() == "pw")
            {
                iPrefSched = 1;
                sBenefitType = "Weekly Benefit";
            }
            if (objData.Context.InternalSettings.CacheFunctions.GetShortCode(objClaim.DisabilityPlan.PrefPaySchCode).ToLower() == "pb")
            {
                iPrefSched = 2;
                sBenefitType = "Weekly Benefit";
            }
            if (objData.Context.InternalSettings.CacheFunctions.GetShortCode(objClaim.DisabilityPlan.PrefPaySchCode).ToLower() == "pm")
            {
                iPrefSched = 3;
                sBenefitType = "Monthly Benefit";
            }
            //End of preferred schedule selected on the disability plan
            //Get the calculation type
            if (objDisClass.BenePrctgFlag)
                iTypeOfWage = 1; //Percent of Wage
            if (objDisClass.BeneFlatAmtFlag)
                iTypeOfWage = 2; //Flat Rate
            if (objDisClass.BeneTdFlag)
                iTypeOfWage = 3; //Table Driven
            //End of 
            //Now check if weekly or hourly rate entered
            string sBeneBasedCodeDesc = "";
            string sBeneBasedCodeShortCode = "";
            objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(objDisClass.BeneBasedCode, ref sBeneBasedCodeShortCode, ref sBeneBasedCodeDesc);

            AppendBankAccountList();

            XmlNodeList objNodeList = null;
            objNodeList = objXML.SelectNodes("//AccountList//option");
            if (objNodeList.Count == 0)
            {
                m_bAllowCalc = false;
                Errors.Add(Globalization.GetString("FillNonOccDomError", base.ClientId), Globalization.GetString("UpdateForm.FillNonOccDomFailed.AccountReq", base.ClientId),
                    BusinessAdaptorErrorType.SystemError);
                DisableFormFields(); //pmittal5 Mits 16284 05/08/09 - Disable all fields if error is thrown
                return;
            }

            //Tax Deduction checkboxes
            objData.Context.InternalSettings.CacheFunctions.GetStateInfo(objClaim.PrimaryClaimant.ClaimantEntity.StateId, ref sShortCode, ref sCodeDesc);
            sStateTitle = "Deduct " + sShortCode + " Tax:";
            //MITS 12902 : Umesh
            //GetTaxes(objClaim.PrimaryClaimant.ClaimantEntity.StateId, ref bTaxes);
            GetTaxes(objClaim, ref bTaxes);

            //alert but allow calculation if tax mapping not been completed


            if (!bTaxes[0] || !bTaxes[1] || !bTaxes[2] || !bTaxes[3])
            {
                if (!bTaxes[0]) sTaxMappings = ", Federal";
                if (!bTaxes[1]) sTaxMappings = sTaxMappings + ", Social Security";
                if (!bTaxes[2]) sTaxMappings = sTaxMappings + ", Medicare";
                if (!bTaxes[3]) sTaxMappings = sTaxMappings + ", State";

                sTaxMappings = sTaxMappings.Substring(2);
                //commenting code as the alert is anyways not visible on screen
                /*
				Errors.Add(Globalization.GetString("FillNonOccDomError"),
					String.Format(Globalization.GetString("UpdateForm.FillNonOccDomFailed.TaxMapping"), sTaxMappings),
					BusinessAdaptorErrorType.Message);
                 */
            }


            iTaxFlags = objClaim.TaxFlags;

            if (iTaxFlags == 0)	//mwc only set flags if they have not been previously saved.  1 means has been saved
            {
                m_dtpTaxes.bFed = objDisClass.WithholdFedItax;
                m_dtpTaxes.bSS = objDisClass.WithholdFica;
                m_dtpTaxes.bMed = objDisClass.WithholdMedicare;
                m_dtpTaxes.bState = objDisClass.WithholdState;
            }
            else
            {
                //objClaim.taxflags is bitmask: fed = 2,ss = 4,medicare = 8,state = 16
                m_dtpTaxes.bFed = ((2 | iTaxFlags) == iTaxFlags);
                m_dtpTaxes.bSS = ((4 | iTaxFlags) == iTaxFlags);
                m_dtpTaxes.bMed = ((8 | iTaxFlags) == iTaxFlags);
                m_dtpTaxes.bState = ((16 | iTaxFlags) == iTaxFlags);
            }
            m_dtpTaxes.dblTaxablePercent = objDisClass.TaxablePercent / 100;

            //days worked: raise alert if no days worked, cannot calc paycheck
            if (!(objDisClass.Day5WorkWeek || objDisClass.Day7WorkWeek))
            {
                bWorkDay = objClaim.PrimaryPiEmployee.WorkSunFlag ||
                            objClaim.PrimaryPiEmployee.WorkMonFlag ||
                            objClaim.PrimaryPiEmployee.WorkTueFlag ||
                            objClaim.PrimaryPiEmployee.WorkWedFlag ||
                            objClaim.PrimaryPiEmployee.WorkThuFlag ||
                            objClaim.PrimaryPiEmployee.WorkFriFlag ||
                            objClaim.PrimaryPiEmployee.WorkSatFlag;

                if (!bWorkDay)
                {
                    m_bAllowCalc = false;
                    Errors.Add(Globalization.GetString("FillNonOccDomError", base.ClientId), Globalization.GetString("UpdateForm.FillNonOccDomFailed.WorkDay", base.ClientId),
                        BusinessAdaptorErrorType.SystemError);
                    DisableFormFields(); //pmittal5 Mits 16284 05/08/09 - Disable all fields if error is thrown
                    return;
                }
            }
            if (iTypeOfWage == 3)
            {
                objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(objDisClass.BeneBasedCode, ref sShortCode, ref sCodeDesc);	//Days: Weeks
                switch (sShortCode.ToUpper())
                {
                    case "W":
                        sWage = string.Format("{0:0.00}", objClaim.PrimaryPiEmployee.WeeklyRate);
                        sWageTitle = "Weekly Wage";
                        sCalcMethod = "Table Driven (Weekly Wage)";
                        break;
                    case "H":
                        sWage = string.Format("{0:0.00}", objClaim.PrimaryPiEmployee.HourlyRate);
                        sWageTitle = "Hourly Wage";
                        sCalcMethod = "Table Driven (Hourly Wage)";
                        break;
                    case "M":
                        sWage = string.Format("{0:0.00}", objClaim.PrimaryPiEmployee.MonthlyRate);
                        sWageTitle = "Monthly Wage";
                        sCalcMethod = "Table Driven (Monthly Wage)";
                        break;
                    default:
                        Errors.Add(Globalization.GetString("FillNonOccDomError", base.ClientId),
                            String.Format(Globalization.GetString("UpdateForm.FillNonOccDomFailed.UnknownBeneCode", base.ClientId), objDisClass.ClassRowId.ToString()),
                            BusinessAdaptorErrorType.SystemError);
                        return;
                }
                if (sBeneBasedCodeDesc.ToLower().Trim() == "hourly rate")
                {
                    iPayType = 1;
                    if (objClaim.PrimaryPiEmployee.HourlyRate == Conversion.ConvertStrToDouble("0.00"))
                    {
                        Errors.Add(Globalization.GetString("FillNonOccDomError", base.ClientId),
                            String.Format(Globalization.GetString("UpdateForm.FillNonOccDomFailed.HourlyRateMissing", base.ClientId)),
                            BusinessAdaptorErrorType.SystemError);
                        DisableFormFields(); //pmittal5 Mits 16284 05/08/09 - Disable all fields if error is thrown
                        return;
                    }
                }
                else
                {
                    if (sBeneBasedCodeDesc.ToLower().Trim() == "monthly rate")
                    {
                        iPayType = 3;
                        if (objClaim.PrimaryPiEmployee.MonthlyRate == Conversion.ConvertStrToDouble("0.00"))
                        {
                            Errors.Add(Globalization.GetString("FillNonOccDomError", base.ClientId),
                                String.Format(Globalization.GetString("UpdateForm.FillNonOccDomFailed.MonthlyRateMissing", base.ClientId)),
                                BusinessAdaptorErrorType.SystemError);
                            DisableFormFields(); //pmittal5 Mits 16284 05/08/09 - Disable all fields if error is thrown
                            return;
                        }
                    }
                    else
                    {
                        iPayType = 2;
                        if (objClaim.PrimaryPiEmployee.WeeklyRate == Conversion.ConvertStrToDouble("0.00"))
                        {
                            Errors.Add(Globalization.GetString("FillNonOccDomError", base.ClientId),
                                String.Format(Globalization.GetString("UpdateForm.FillNonOccDomFailed.WeeklyRateMissing", base.ClientId)),
                                BusinessAdaptorErrorType.SystemError);
                            DisableFormFields(); //pmittal5 Mits 16284 05/08/09 - Disable all fields if error is thrown
                        }
                    }
                }
                //parijat: Mits 11942
                //dblWeeklyBenefit = dblWeeklyRate;	//dblWeeklyBenefit overridden in For loop if match is found
                //foreach (DisClassTd objDisClassTd in objDisClass.TableList)
                //{
                //    if (dblWeeklyRate <= objDisClassTd.WagesTo)
                //    {
                //        if (dblWeeklyRate >= objDisClassTd.WagesFrom)
                //        {
                //            dblWeeklyBenefit = objDisClassTd.WeeklyBenefit;
                //            break;
                //        }
                //    }
                //}
                //
            }
            else
            {
                if (iTypeOfWage == 1)
                {
                    dblTemp = objDisClass.BenePerAmt;
                    if (dblTemp == 0)
                        dblTemp = 100;   //0 means leave at 100%
                    if (iPrefSched == 3)
                    {
                        sWage = string.Format("{0:0.00}", dblMonthlyRate);
                        sWageTitle = "Monthly Wage";
                        sCalcMethod = "% of Monthly Wage (" + string.Format("{0:###.00}", dblTemp) + "%)";
                        dblWeeklyBenefit = objClaim.PrimaryPiEmployee.MonthlyRate * dblTemp / 100; //pmittal5 Mits 17772
                        if (objClaim.PrimaryPiEmployee.MonthlyRate == Conversion.ConvertStrToDouble("0.00"))
                        {
                            Errors.Add(Globalization.GetString("FillNonOccDomError", base.ClientId),
                                String.Format(Globalization.GetString("UpdateForm.FillNonOccDomFailed.MonthlyRateMissing", base.ClientId)),
                                BusinessAdaptorErrorType.SystemError);
                            DisableFormFields(); //pmittal5 Mits 16284 05/08/09 - Disable all fields if error is thrown
                            return;
                        }
                    }
                    else
                    {
                        sWage = string.Format("{0:0.00}", dblWeeklyRate);
                        sWageTitle = "Weekly Wage";
                        sCalcMethod = "% of Weekly Wage (" + string.Format("{0:###.00}", dblTemp) + "%)";
                        dblWeeklyBenefit = objClaim.PrimaryPiEmployee.WeeklyRate * dblTemp / 100; //pmittal5 Mits 17772
                        if (iPrefSched == 2 || iPrefSched == 1)
                        {
                            if (objClaim.PrimaryPiEmployee.WeeklyRate == Conversion.ConvertStrToDouble("0.00"))
                            {
                                Errors.Add(Globalization.GetString("FillNonOccDomError", base.ClientId),
                                    String.Format(Globalization.GetString("UpdateForm.FillNonOccDomFailed.WeeklyRateMissing", base.ClientId)),
                                    BusinessAdaptorErrorType.SystemError);
                                DisableFormFields(); //pmittal5 Mits 16284 05/08/09 - Disable all fields if error is thrown
                                return;
                            }
                        }
                    }
                    dGrossPayment = (Conversion.ConvertStrToDouble(sWage) * dblTemp) / 100;
                    if (objDisClass.SuppPercent > 0)
                    {
                        dGrossSupplement = (Conversion.ConvertStrToDouble(sWage) * objDisClass.SuppPercent) / 100;
                    }
                }
                else
                {
                    if (iTypeOfWage == 2)
                    {
                        if (iPrefSched != 3) //weekly
                        {
                            sWage = string.Format("{0:0.00}", dblWeeklyRate);
                            sWageTitle = "Weekly Wage";
                        }
                        else
                        {
                            //monthly
                            sWage = string.Format("{0:0.00}", objClaim.PrimaryPiEmployee.MonthlyRate);
                            sWageTitle = "Monthly Wage";
                        }

                        sCalcMethod = "Flat Amount (" + string.Format("{0:c}", objDisClass.BeneFlatAmt) + ")";
                    }
                    else
                    {
                        sWage = string.Format("{0:0.00}", dblWeeklyRate);
                        sWageTitle = "Weekly Wage";
                        sCalcMethod = "Flat Amount (" + string.Format("{0:c}", objDisClass.BeneFlatAmt) + ")";
                        dblWeeklyBenefit = objDisClass.BeneFlatAmt;
                    }
                }
            }
            //Eligibility Date calculation type
            // Added By Rahul
            if (objClaim.PrimaryPiEmployee.EligDisBenFlag == true)
            {
                Errors.Add(Globalization.GetString("FillNonOccDomError", base.ClientId),
                    String.Format(Globalization.GetString("UpdateForm.FillNonOccDomFailed.NotEligibleFlag", base.ClientId)),
                    BusinessAdaptorErrorType.SystemError);
                DisableFormFields(); //pmittal5 Mits 16284 05/08/09 - Disable all fields if error is thrown
                return;
            }
            // End (Eligibility Date calculation type).

            //**************************************************************************************************//
            //////			objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(objDisClass.BenePrdType,ref sShortCode, ref sCodeDesc); //Days : Weeks : Months
            //////			sPrdType=sShortCode.ToLower();
            //////			if (sPrdType=="w")
            //////				sPrdType="ww";
            //////			
            //Code added by Rahul -- Eligibility Criteria.
            foreach (DisabilityClass objClass in objClaim.DisabilityPlan.ClassList)
            {
                if (objClass.ClassRowId == objClaim.ClassRowId)
                {
                    bUseVoucher = objClass.UseVouchers;
                    objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(objClass.BenePrdType, ref sShortCode, ref sCodeDesc); //Days : Weeks : Months
                    sPrdType = sShortCode.ToLower();
                    if (sPrdType == "w")
                        sPrdType = "ww";

                    objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(objClass.DisMaxPrdType, ref sShortCode, ref sCodeDesc);
                    sMaxPeriodDayWeeks = sShortCode.ToLower().Trim();
                    if (sMaxPeriodDayWeeks == "w")
                        sMaxPeriodDayWeeks = "ww";
                    //MITS 10780
                    //Wrong property objClass.DisPrdType was picked from the database.Now checking it for objClass.FromDisPrdType
                    //Commented the earlier code.
                    //objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(objClass.DisPrdType,ref sShortCode, ref sCodeDesc);
                    //objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(objClass.FromDisPrdType, ref sShortCode, ref sCodeDesc);
                    //if(objClaim.DisabilFromDate.Trim() != "")
                    //{
                    //    if(sShortCode.ToUpper() == "D")
                    //        sEndDisDate = AddPeriodToDate(objClaim.DisabilFromDate,sMaxPeriodDayWeeks,objClass.DisMaxPrd);
                    //    else
                    //    {
                    //        if(objClaim.BenefitsStart != "")
                    //            sEndDisDate = AddPeriodToDate(objClaim.BenefitsStart,sMaxPeriodDayWeeks,objClass.DisMaxPrd);
                    //        else
                    //            sEndDisDate = "";
                    //    }
                    //}
                    objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(objClass.DisPrdType, ref sShortCode, ref sCodeDesc);

                    string sBeneFromDtType = string.Empty;
                    sBeneFromDtType = objData.Context.InternalSettings.CacheFunctions.GetShortCode(objClass.BeneFromDtType);
                    if (sBeneFromDtType.ToUpper().Trim() == "DH")
                    {
                        if (objClaim.PrimaryPiEmployee.DateHired != "")
                            sEligDate = AddPeriodToDate(objClaim.PrimaryPiEmployee.DateHired, sPrdType, objClass.EligBenePrd);
                        else
                            sEligDate = AddPeriodToDate(Conversion.ToDbDate(System.DateTime.Today), sPrdType, objClass.EligBenePrd);
                    }
                    else
                    {
                        if (objClaim.PrimaryPiEmployee.DateHired != "")
                        {
                            string sDate = string.Empty;
                            int iMonth = 0;
                            string sMonth = string.Empty;
                            int iYear = 0;
                            string sYear = string.Empty;
                            DateTime objDate = new DateTime(0);
                            sDate = objClaim.PrimaryPiEmployee.DateHired;
                            objDate = Conversion.ToDate(objClaim.PrimaryPiEmployee.DateHired);
                            if ((int)objDate.DayOfWeek != 0)
                            {
                                iMonth = objDate.Month;
                                iYear = objDate.Year;
                                if (iMonth == 12)
                                {
                                    sMonth = "01";
                                    iYear = iYear + 1;
                                }
                                else
                                    iMonth = iMonth + 1;

                                //sDate = iYear.ToString()+ string.Format( "00" , iMonth ) +"01";
                                DateTime objModifiedDate = new DateTime(iYear, iMonth, 1);
                                sDate = Conversion.ToDbDate(objModifiedDate);
                            }
                            sDate = AddPeriodToDate(sDate, sPrdType, objClass.EligBenePrd);
                            sEligDate = sDate;
                        }
                    }
                    int iSelectedDisabilityType = 0;
                    int iWaitPeriod = 0;
                    bool bFoundDisType = false;
                    string sDisClassWaitPrdType = string.Empty;
                    if (objClaim.DisTypeCode > 0 && objClass.WaitList.Count > 0)
                    {
                        iSelectedDisabilityType = objClaim.DisTypeCode;
                        bFoundDisType = false;
                        foreach (DisClassWait objClassWait in objDisClass.WaitList)
                        {
                            iBenDisType = objClaim.DisTypeCode;
                            if (iBenDisType == objClassWait.DisTypeCode)
                            {
                                objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(objClassWait.DisClndrWrkCode, ref sShortCode, ref sCodeDesc);
                                if (sCodeDesc.ToLower().Trim() == "work")
                                {
                                    sDisClassWaitPrdType = objData.Context.InternalSettings.CacheFunctions.GetShortCode(objClassWait.DisPrdType);
                                    if (sDisClassWaitPrdType.ToLower().Trim() == "d")
                                    {
                                        iWaitPeriod = objClassWait.DisWaitPrd;
                                        GetWorkWeek(objClass);
                                        //sBenefitStartDate = AddPeriodToDate(objClaim.DisabilFromDate,"d",iWaitPeriod);
                                        //Parijat: Mits 11550 :Adding work period here
                                        sBenefitStartDate = AddWorkPeriodToDate(objClaim.DisabilFromDate, "d", iWaitPeriod);
                                    }//Parijat: Mits 11820
                                    else if (sDisClassWaitPrdType.ToLower().Trim() == "m")
                                    {
                                        iWaitPeriod = objClassWait.DisWaitPrd;
                                        //sBenefitStartDate = AddPeriodToDate(objClaim.DisabilFromDate,"m",iWaitPeriod);
                                        //Parijat: Mits 11550 :Adding work period here
                                        GetWorkWeek(objClass);
                                        sBenefitStartDate = AddWorkPeriodToDate(objClaim.DisabilFromDate, "m", iWaitPeriod);
                                    }
                                    else
                                    {
                                        iWaitPeriod = objClassWait.DisWaitPrd;
                                        //sBenefitStartDate = AddPeriodToDate(objClaim.DisabilFromDate,"ww",iWaitPeriod);
                                        //Parijat: Mits 11550 :Adding work period here
                                        GetWorkWeek(objClass);
                                        sBenefitStartDate = AddWorkPeriodToDate(objClaim.DisabilFromDate, "ww", iWaitPeriod);
                                    }
                                }
                                else
                                {
                                    iWaitPeriod = objClassWait.DisWaitPrd;
                                    sPrdType = objData.Context.InternalSettings.CacheFunctions.GetShortCode(objClassWait.DisPrdType);
                                    if (sPrdType == "w")
                                        sPrdType = "ww";
                                    sBenefitStartDate = AddPeriodToDate(objClaim.DisabilFromDate, sPrdType, iWaitPeriod);
                                }
                                bFoundDisType = true;
                                break;
                            }
                        }
                        if (!bFoundDisType)
                        {
                            objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(objDisClass.DisClndrWrkCode, ref sShortCode, ref sCodeDesc);
                            if (sCodeDesc.ToLower().Trim() == "work")
                            {
                                iWaitPeriod = objClass.DisWaitPrd;
                                //pawan added to lower here in code below for 10780 mits
                                if (objData.Context.InternalSettings.CacheFunctions.GetShortCode(objClass.DisPrdType).ToLower() == "d")
                                {
                                    GetWorkWeek(objClass);
                                    //sBenefitStartDate = AddPeriodToDate(objClaim.DisabilFromDate,"d",iWaitPeriod);
                                    //Parijat: Mits 11550 :Adding work period here
                                    sBenefitStartDate = AddWorkPeriodToDate(objClaim.DisabilFromDate, "d", iWaitPeriod);
                                }//Parijat: Mits 11820
                                else if (objData.Context.InternalSettings.CacheFunctions.GetShortCode(objClass.DisPrdType).ToLower() == "m")
                                {
                                    //sBenefitStartDate = AddPeriodToDate(objClaim.DisabilFromDate, "m", iWaitPeriod);
                                    //Parijat: Mits 11550 :Adding work period here
                                    GetWorkWeek(objClass);
                                    sBenefitStartDate = AddWorkPeriodToDate(objClaim.DisabilFromDate, "m", iWaitPeriod);
                                }
                                else
                                {
                                    //sBenefitStartDate = AddPeriodToDate(objClaim.DisabilFromDate, "ww", iWaitPeriod);
                                    //Parijat: Mits 11550 :Adding work period here
                                    GetWorkWeek(objClass);
                                    sBenefitStartDate = AddWorkPeriodToDate(objClaim.DisabilFromDate, "ww", iWaitPeriod);
                                }
                            }
                            else
                            {
                                iWaitPeriod = objDisClass.DisWaitPrd;
                                sBenefitStartDate = AddPeriodToDate(objClaim.DisabilFromDate, sPrdType, iWaitPeriod);
                            }
                        }
                    }
                    else
                    {
                        objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(objDisClass.DisClndrWrkCode, ref sShortCode, ref sCodeDesc);
                        if (sCodeDesc.ToLower().Trim() == "work")
                        {
                            iWaitPeriod = objClass.DisWaitPrd;
                            if (objData.Context.InternalSettings.CacheFunctions.GetShortCode(objClass.DisPrdType).ToLower() == "d")
                            {
                                GetWorkWeek(objClass);

                                //Parijat: Mits 11550 :Adding work period here
                                sBenefitStartDate = AddWorkPeriodToDate(objClaim.DisabilFromDate, "d", iWaitPeriod);
                                //sBenefitStartDate = AddPeriodToDate(objClaim.DisabilFromDate,"d",iWaitPeriod);
                            }//Parijat : Mits 11820
                            else if (objData.Context.InternalSettings.CacheFunctions.GetShortCode(objClass.DisPrdType).ToLower() == "m")
                            {
                                //sBenefitStartDate = AddPeriodToDate(objClaim.DisabilFromDate, "m", iWaitPeriod);
                                //Parijat: Mits 11550 :Adding work period here
                                GetWorkWeek(objClass);
                                sBenefitStartDate = AddWorkPeriodToDate(objClaim.DisabilFromDate, "m", iWaitPeriod);
                            }
                            else
                            {
                                //sBenefitStartDate = AddPeriodToDate(objClaim.DisabilFromDate, "ww", iWaitPeriod);
                                //Parijat: Mits 11550 :Adding work period here
                                GetWorkWeek(objClass);
                                sBenefitStartDate = AddWorkPeriodToDate(objClaim.DisabilFromDate, "ww", iWaitPeriod);
                            }
                        }
                        else
                        {
                            string sTmp2 = string.Empty;
                            iWaitPeriod = objDisClass.DisWaitPrd;
                            if (objData.Context.InternalSettings.CacheFunctions.GetShortCode(objClass.DisPrdType).ToLower() == "d")
                                sTmp2 = "d";
                            else if (objData.Context.InternalSettings.CacheFunctions.GetShortCode(objClass.DisPrdType).ToLower() == "m")
                                sTmp2 = "m";//Parijat : Mits 11820
                            else
                                sTmp2 = "ww";
                            sBenefitStartDate = AddPeriodToDate(objClaim.DisabilFromDate, sTmp2, iWaitPeriod);
                        }
                    }

                    //MITS 10780
                    //Wrong property objClass.DisPrdType was picked from the database.Now checking it for objClass.FromDisPrdType
                    //Commented the earlier code.
                    //objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(objClass.DisPrdType,ref sShortCode, ref sCodeDesc);
                    objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(objClass.FromDisPrdType, ref sShortCode, ref sCodeDesc);
                    if (objClaim.DisabilFromDate.Trim() != "")
                    {
                        if (sShortCode.ToUpper() == "D")
                            sEndDisDate = AddPeriodToDate(objClaim.DisabilFromDate, sMaxPeriodDayWeeks, objClass.DisMaxPrd);
                        else
                        {
                            if (objClaim.BenefitsStart != "")
                            {
                                sEndDisDate = AddPeriodToDate(objClaim.BenefitsStart, sMaxPeriodDayWeeks, objClass.DisMaxPrd);
                            }
                            else
                            {
                                if (sBenefitStartDate != "")
                                {
                                    sEndDisDate = AddPeriodToDate(sBenefitStartDate, sMaxPeriodDayWeeks, objClass.DisMaxPrd);
                                }
                                else
                                {
                                    sEndDisDate = "";
                                }
                            }
                        }
                    }
                    if (objClaim.PrimaryPiEmployee.PiEntity.BirthDate == "")
                        iAge = 0;
                    else
                    {
                        objSpan = Conversion.ToDate((objClaim.Parent as Event).DateOfEvent).Subtract(Conversion.ToDate(objClaim.PrimaryPiEmployee.PiEntity.BirthDate));
                        iAge = (int)((objSpan.Days) / (365.25));
                    }
                    if (iAge < 0)
                        iAge = 0;
                    if (objClass.LowerAgeLimit > 0 && iAge > 0)
                    {
                        iLowerAgeLimit = objClass.LowerAgeLimit;
                        bLAgeLimit = true;
                    }
                    else
                        bLAgeLimit = false;
                    if (objClass.UpperAgeLimit > 0 && iAge > 0)
                    {
                        iUpperAgeLimit = objClass.UpperAgeLimit;
                        bUAgeLimit = true;
                    }

                }
            }
            if (sEligDate.Trim() != "" && objClaim.DisabilFromDate.Trim() != "" && sBenefitStartDate.Trim() != "")
            {
                if (Conversion.ToDate(sEligDate).CompareTo(Conversion.ToDate(objClaim.DisabilFromDate)) > 0)
                {
                    m_bEligible = false;
                    m_sEligTitle = "NOTE: Employee Not Eligible For Benefits. Eligibility Date is: " + Conversion.ToDate(sEligDate).ToShortDateString();
                }
                else
                {
                    if (bLAgeLimit && bUAgeLimit)
                    {
                        if (iAge > iUpperAgeLimit && iAge <= iLowerAgeLimit)
                        {
                            m_bEligible = false;
                            m_sEligTitle = "NOTE: Employee Not Eligible For Benefits. Age " + iAge.ToString() + " Not Within Limits.";
                        }
                        else
                        {
                            m_bEligible = true;
                            m_sEligTitle = "Employee Eligible for Benefits. Eligible On: " + Conversion.ToDate(sEligDate).ToShortDateString();
                        }
                    }
                    else
                    {
                        if (bUAgeLimit == true)
                        {
                            if (iAge > iUpperAgeLimit)
                            {
                                m_bEligible = false;
                                m_sEligTitle = "NOTE: Employee Not Eligible For Benefits. Age " + iAge.ToString() + " Not Within Limits.";
                            }
                            else
                            {
                                m_bEligible = true;
                                m_sEligTitle = "Employee Eligible for Benefits. Eligible On: " + Conversion.ToDate(sEligDate).ToShortDateString();
                            }
                        }
                        else
                        {
                            if (bLAgeLimit)
                            {
                                if (iAge <= iLowerAgeLimit)
                                {
                                    m_bEligible = false;
                                    m_sEligTitle = "NOTE: Employee Not Eligible For Benefits. Age " + iAge.ToString() + " Not Within Limits.";
                                }
                                else
                                {
                                    m_bEligible = true;
                                    m_sEligTitle = "Employee Eligible for Benefits. Eligible On: " + Conversion.ToDate(sEligDate).ToShortDateString();
                                }
                            }
                            else
                            {
                                m_bEligible = true;
                                m_sEligTitle = "Employee Eligible for Benefits. Eligible On: " + Conversion.ToDate(sEligDate).ToShortDateString();
                            }
                        }
                    }
                }
            }
            if (sEndDisDate.Trim() != "")
            {
                if (objClaim.BenefitsThrough.Trim() == "")
                    //AA 10/10/2006 - MITS 7952 The Benefits period displayed on the Non-occ payments page is NOT calculated correctly
                    //srajindersin MITS 32468 05/07/2013
                    //sBenefitsThrough = sEndDisDate; //AddPeriodToDate(sEndDisDate,"d",-1);
                    sBenefitsThrough = Conversion.ToDbDate(Conversion.ToDate(sEndDisDate).AddDays(-1));
                if (objClaim.BenCalcPayTo.Trim() == "")
                {
                    //AA 10/10/2006 - MITS 7952 The Benefits period displayed on the Non-occ payments page is NOT calculated correctly
                    if (objClaim.DisabilToDate.Trim() != "" && sEndDisDate.Trim() != "")
                    {
                        if (int.Parse(sEndDisDate.Trim()) < int.Parse(objClaim.DisabilToDate.Trim()))
                            //srajindersin MITS 32468 05/07/2013
                            //sBenCalcPayTo = sEndDisDate; //AddPeriodToDate(sEndDisDate,"d",-1);
                            sBenCalcPayTo = Conversion.ToDbDate(Conversion.ToDate(sEndDisDate).AddDays(-1));
                        else
                            sBenCalcPayTo = objClaim.DisabilToDate;
                    }
                    else
                    {
                        //sBenCalcPayTo = sEndDisDate;
                        sBenCalcPayTo = Conversion.ToDbDate(Conversion.ToDate(sEndDisDate).AddDays(-1));//igupta3 MITS 32822 
                    }
                }
            }



            //////			dTemp=Conversion.ToDate(objClaim.PrimaryPiEmployee.DateHired);
            //////
            //////			objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(objDisClass.BeneFromDtType,ref sShortCode,ref sCodeDesc); //Immediate : 1st Of Next Month
            //////
            //////			if (sShortCode.ToUpper()!="DH")	//DH means eligible from date hired  + ELIG_BENE_PRD
            //////			{
            //////				dTemp=dTemp.AddMonths(1);
            //////				dTemp=Conversion.ToDate(string.Format("{0:0000}",dTemp.Year) + string.Format("{0:00}",dTemp.Month) + "01");
            //////			}
            //////			sEligDate=AddPeriodToDate(Conversion.ToDbDate(dTemp),sPrdType,objDisClass.EligBenePrd);
            //////
            //////			if (sEligDate.CompareTo(objClaim.DisabilFromDate)>0)
            //////			{
            //////				m_bAllowCalc = false;
            //////				m_sEligTitle = "Employee Not Eligible For Benefits. Eligibility Date:";
            //////				Errors.Add(Globalization.GetString("FillNonOccDomError"),
            //////					String.Format(Globalization.GetString("UpdateForm.FillNonOccDomFailed.NotEligForBenefits"),Conversion.GetDBDateFormat(sEligDate,"d")),
            //////					BusinessAdaptorErrorType.Message);
            //////			}
            //////			else
            //////				m_sEligTitle = "Employee Eligible for Benefits On:";
            //////
            //////			//Benefit Start Date & period
            //////			GetBenefitPeriod(objDisClass,ref sBenefitStartDate,ref sBenCalcPayStart,ref sBenCalcPayTo);
            //////    
            //////			//Calculation Method, weekly or hourly wage, weekly benefit
            //////			dblHourlyRate = objClaim.PrimaryPiEmployee.HourlyRate;
            //////			if (objClaim.ClaimAWW == null)
            //////				dblWeeklyRate=objClaim.PrimaryPiEmployee.WeeklyRate;
            //////			else
            //////			{
            //////				dblWeeklyRate=objClaim.ClaimAWW.Aww;
            //////				if (dblWeeklyRate==0)
            //////					dblWeeklyRate=objClaim.PrimaryPiEmployee.WeeklyRate;
            //////			}
            //////
            //////			if (objDisClass.BenePrctgFlag)	//Percentage of Weekly Wage
            //////			{
            //////				sWage=string.Format("{0:0.00}",dblWeeklyRate);
            //////				sWageTitle = "Weekly Wage";
            //////				dblTemp = objDisClass.BenePerAmt;
            //////				if (dblTemp == 0)
            //////					dblTemp = 100;   //0 means leave at 100%
            //////				sCalcMethod = "% of Weekly Wage (" + string.Format("{0:###.00}",dblTemp) + "%)";
            //////				dblWeeklyBenefit = objClaim.PrimaryPiEmployee.WeeklyRate * (dblTemp / 100);
            //////			}
            //////			else if (objDisClass.BeneFlatAmtFlag)
            //////			{
            //////				sWage=string.Format("{0:0.00}",dblWeeklyRate);
            //////				sWageTitle = "Weekly Wage";
            //////				sCalcMethod = "Flat Amount (" + string.Format("{0:c}",objDisClass.BeneFlatAmt) + ")";
            //////				dblWeeklyBenefit = objDisClass.BeneFlatAmt;
            //////			}
            //////			else if (objDisClass.BeneTdFlag)  //Table Driven (based on Weekly Rate or Hourly Rate)
            //////			{
            //////				objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(objDisClass.BeneBasedCode,ref sShortCode, ref sCodeDesc);	//Days: Weeks
            //////				switch (sShortCode.ToUpper())
            //////				{
            //////					case "W":
            //////						sWage = string.Format("{0:0.00}",dblWeeklyRate );
            //////						sWageTitle = "Weekly Wage";
            //////						sCalcMethod = "Table Driven (Weekly Wage)";
            //////						break;
            //////					case "H":
            //////						sWage = string.Format("{0:0.00}",dblHourlyRate );
            //////						sWageTitle = "Hourly Wage";
            //////						sCalcMethod = "Table Driven (Hourly Rate)";
            //////						break;
            //////					default:
            //////						Errors.Add(Globalization.GetString("FillNonOccDomError"),
            //////							String.Format(Globalization.GetString("UpdateForm.FillNonOccDomFailed.UnknownBeneCode"), objDisClass.ClassRowId.ToString()),
            //////							BusinessAdaptorErrorType.Error);
            //////						return;
            //////				}
            //////				dblWeeklyBenefit = dblWeeklyRate;	//dblWeeklyBenefit overridden in For loop if match is found
            //////				foreach(DisClassTd objDisClassTd in objDisClass.TableList)
            //////				{
            //////					if (dblWeeklyRate <= objDisClassTd.WagesTo)
            //////					{
            //////						if (dblWeeklyRate >= objDisClassTd.WagesFrom)
            //////						{
            //////							dblWeeklyBenefit = objDisClassTd.WeeklyBenefit;
            //////							break;
            //////						}
            //////					}
            //////				}
            //////			}
            //////
            //////			// apply weekly cap
            //////			if (objDisClass.WeeklyBeneCap > 0)
            //////			{
            //////				if (dblWeeklyBenefit > objDisClass.WeeklyBeneCap)
            //////					dblWeeklyBenefit = objDisClass.WeeklyBeneCap;
            //////			}
            //////
            //////			AppendBankAccountList();
            //////
            //////			XmlNodeList objNodeList=null;
            //////			objNodeList=objXML.SelectNodes("//AccountList//option");
            //////			if (objNodeList.Count==0)
            //////			{
            //////				 m_bAllowCalc = false;
            //////				Errors.Add(Globalization.GetString("FillNonOccDomError"),Globalization.GetString("UpdateForm.FillNonOccDomFailed.AccountReq"),
            //////					BusinessAdaptorErrorType.Error);
            //////			}
            //////
            //////			//Tax Deduction checkboxes
            //////			objData.Context.InternalSettings.CacheFunctions.GetStateInfo(objClaim.PrimaryClaimant.ClaimantEntity.StateId, ref sShortCode, ref sCodeDesc);
            //////			sStateTitle="Deduct " + sShortCode + " Tax:";
            //////    
            //////			GetTaxes(objClaim.PrimaryClaimant.ClaimantEntity.StateId, ref bTaxes);
            //////	   
            //////		    //alert but allow calculation if tax mapping not been completed
            //////			if (!bTaxes[0] || !bTaxes[1] || !bTaxes[2] || !bTaxes[3])
            //////			{
            //////				if (!bTaxes[0]) sTaxMappings = ", Federal";
            //////				if (!bTaxes[1]) sTaxMappings = sTaxMappings + ", Social Security";
            //////				if (!bTaxes[2]) sTaxMappings = sTaxMappings + ", Medicare";
            //////				if (!bTaxes[3]) sTaxMappings = sTaxMappings + ", State";
            //////
            //////				sTaxMappings=sTaxMappings.Substring(2);
            //////
            //////				Errors.Add(Globalization.GetString("FillNonOccDomError"),
            //////					String.Format(Globalization.GetString("UpdateForm.FillNonOccDomFailed.TaxMapping"), sTaxMappings),
            //////					BusinessAdaptorErrorType.Message);
            //////			}
            //////			
            //////			iTaxFlags = objClaim.TaxFlags;
            //////
            //////			if (iTaxFlags==0)	//mwc only set flags if they have not been previously saved.  1 means has been saved
            //////			{
            //////				m_dtpTaxes.bFed = objDisClass.WithholdFedItax;
            //////				m_dtpTaxes.bSS = objDisClass.WithholdFica;
            //////				m_dtpTaxes.bMed = objDisClass.WithholdMedicare;
            //////				m_dtpTaxes.bState = objDisClass.WithholdState;		
            //////			}
            //////			else
            //////			{
            //////				//objClaim.taxflags is bitmask: fed = 2,ss = 4,medicare = 8,state = 16
            //////				m_dtpTaxes.bFed = ((2 | iTaxFlags)==iTaxFlags);
            //////				m_dtpTaxes.bSS = ((4 | iTaxFlags)==iTaxFlags);
            //////				m_dtpTaxes.bMed = ((8 | iTaxFlags)==iTaxFlags);
            //////				m_dtpTaxes.bState = ((16 | iTaxFlags)==iTaxFlags);
            //////			}
            //////			m_dtpTaxes.dblTaxablePercent = objDisClass.TaxablePercent/100;
            //////
            //////			//days worked: raise alert if no days worked, cannot calc paycheck
            //////			if (!(objDisClass.Day5WorkWeek || objDisClass.Day7WorkWeek))
            //////			{
            //////				bWorkDay =	objClaim.PrimaryPiEmployee.WorkSunFlag ||
            //////							objClaim.PrimaryPiEmployee.WorkMonFlag ||
            //////							objClaim.PrimaryPiEmployee.WorkTueFlag ||
            //////							objClaim.PrimaryPiEmployee.WorkWedFlag ||
            //////							objClaim.PrimaryPiEmployee.WorkThuFlag ||
            //////							objClaim.PrimaryPiEmployee.WorkFriFlag ||
            //////							objClaim.PrimaryPiEmployee.WorkSatFlag;
            //////
            //////				if (!bWorkDay)
            //////				{
            //////					m_bAllowCalc=false;
            //////					Errors.Add(Globalization.GetString("FillNonOccDomError"),Globalization.GetString("UpdateForm.FillNonOccDomFailed.WorkDay"),
            //////						BusinessAdaptorErrorType.Message);
            //////				}
            //////			}

            //Raman Bhatia LTD : peforming disability calculations

            if (objClaim.BenCalcPayStart.Trim() != "")
            {
                base.ResetSysExData("BenCalcPayStart", Conversion.ToDate(objClaim.BenCalcPayStart).ToShortDateString());
            }
            else
            {
                if (objClaim.BenefitsStart.Trim() != "")
                {
                    base.ResetSysExData("BenCalcPayStart", Conversion.ToDate(objClaim.BenefitsStart).ToShortDateString());
                }
                else
                {
                    base.ResetSysExData("BenCalcPayStart", Conversion.ToDate(sBenefitStartDate).ToShortDateString());
                }
            }

            if (objClaim.BenCalcPayTo.Trim() != "")
            {
                base.ResetSysExData("BenCalcPayTo", Conversion.ToDate(objClaim.BenCalcPayTo).ToShortDateString());
            }
            else
            {
                base.ResetSysExData("BenCalcPayTo", Conversion.ToDate(sBenCalcPayTo).ToShortDateString());
            }

            XmlElement objElement = (XmlElement)objXML.SelectSingleNode("//BenCalcPayStart");
            if (objElement != null && objElement.InnerText != "")
                sBenCalcPayStart = objElement.InnerText;
            objElement = (XmlElement)objXML.SelectSingleNode("//BenCalcPayTo");
            if (objElement != null && objElement.InnerText != "")
                sBenCalcPayTo = objElement.InnerText;
            m_sBenCalcPayStart = sBenCalcPayStart;
            m_sBenCalcPayTo = sBenCalcPayTo;
            
            sSql = "SELECT DEF_DSTRBN_TYPE_CODE FROM CHECK_OPTIONS";
            m_iDefaultDistributionType = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, sSql), base.ClientId);


            if (m_iDefaultDistributionType != 0)
            {
                objCache.GetCodeInfo(m_iDefaultDistributionType, ref sDefaultDistributionTypeShortCode, ref sDefaultDistributionTypeCodeDesc);
                base.ResetSysExData("DstrbnType", sDefaultDistributionTypeShortCode + " " + sDefaultDistributionTypeCodeDesc);
            }
            else
            {
                base.ResetSysExData("DstrbnType", string.Empty);
            }
            objElement = (XmlElement)objXML.SelectSingleNode("//DstrbnType");
            if (objElement != null)
            {
                objElement.SetAttribute("codeid", m_iDefaultDistributionType.ToString());
            }

            performDisabilityCalculations(sBenefitStartDate, sBenCalcPayStart, sBenCalcPayTo, ref dblWeeklyBenefit);

            //Raman Bhatia: Calculating Totals

            CalculateTotals();

            string sReserveType = AppendTransactionTypeCombo(objClaim.ClaimId);//skhare7 27920

            base.ResetSysExData("ClaimId", objClaim.ClaimId.ToString());
            base.ResetSysExData("PiEid", objClaim.PrimaryPiEmployee.PiEid.ToString());
            base.ResetSysExData("ClaimNumber", objClaim.ClaimNumber);
            base.ResetSysExData("DisabilityFromDate", Conversion.GetDBDateFormat(objClaim.DisabilFromDate, "d"));
            base.ResetSysExData("AllowCalc", m_bAllowCalc.ToString());
            base.ResetSysExData("TaxArray", m_dtpTaxes.FED_TAX_RATE + "," + m_dtpTaxes.iFedTaxEID + "," +
                                            m_dtpTaxes.iFedTransTypeCodeID + "," + m_dtpTaxes.MEDICARE_TAX_RATE + "," +
                                            m_dtpTaxes.iMedicareTaxEID + "," + m_dtpTaxes.iMedicareTransTypeCodeID + "," +
                                            m_dtpTaxes.SS_TAX_RATE + "," + m_dtpTaxes.iSSTaxEID + "," +
                                            m_dtpTaxes.iSSTransTypeCodeID + "," + m_dtpTaxes.STATE_TAX_RATE + "," +
                                            m_dtpTaxes.iStateTaxEID + "," + m_dtpTaxes.iStateTransTypeCodeID);
            base.ResetSysExData("EligDate", m_sEligTitle);

            objXML = base.SysEx;
            if (objXML.SelectSingleNode("//IsEligible") != null)
            {
                base.ResetSysExData("IsEligible", m_bEligible.ToString());
            }
            else
            {
                objElement = objXML.CreateElement("IsEligible");
                objElement.InnerText = m_bEligible.ToString();
                objXML.DocumentElement.AppendChild(objElement);
            }
            if (m_bEligible)
            {
                base.ResetSysExData("BenefitStartDateReadOnly", Conversion.GetDBDateFormat(sBenefitStartDate, "d"));
            }
            else
            {
                base.ResetSysExData("BenefitStartDateReadOnly", "");
            }
            base.ResetSysExData("PlanName", objClaim.DisabilityPlan.PlanName);
            base.ResetSysExData("CalcMethod", sCalcMethod);
            base.ResetSysExData("Wage", sWage);
            //mona
            //Raman Bhatia - LTD Phase 3 changes : change the title of node 
            //XmlDocument objSysViewXML = base.SysView;
            //objElement = (XmlElement) objSysViewXML.SelectSingleNode("//control[@name='wage']");
            //objElement.SetAttribute("title" , sWageTitle);

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "lbl_wage");
            base.AddElementToList(ref singleRow, "Text", sWageTitle);
            base.m_ModifiedControls.Add(singleRow);


            if ((objDisClass.WeeklyBeneCap < dblWeeklyBenefit) && (objDisClass.WeeklyBeneCap != 0))
                dblWeeklyBenefit = objDisClass.WeeklyBeneCap;
            //base.ResetSysExData("WeeklyBenefit",string.Format("{0:0.00}",dblWeeklyBenefit));
            //base.ResetSysExData("WeeklyBenefit", Math.Round(dblWeeklyBenefit, 2).ToString());
            base.ResetSysExData("WeeklyBenefit", Math.Round(dblWeeklyBenefit, 2, MidpointRounding.AwayFromZero).ToString());
            //mona
            //objElement = (XmlElement) objSysViewXML.SelectSingleNode("//control[@name='weeklybenefit']");
            //objElement.SetAttribute("title" , sBenefitType);

            singleRow = new ArrayList();
            base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
            base.AddElementToList(ref singleRow, "id", "lbl_weeklybenefit");
            base.AddElementToList(ref singleRow, "Text", sBenefitType);
            base.m_ModifiedControls.Add(singleRow);

            if (objClaim.BenefitsStart.Trim() != "")
                base.ResetSysExData("BenefitStartDate", objClaim.BenefitsStart);
            else
                base.ResetSysExData("BenefitStartDate", sBenefitStartDate);
            if (objClaim.BenefitsThrough.Trim() != "")
                base.ResetSysExData("BenefitsThrough", Conversion.ToDate(objClaim.BenefitsThrough).ToShortDateString());
            else
                base.ResetSysExData("BenefitsThrough", sBenefitsThrough);
            if (objClaim.BenCalcPayStart.Trim() != "")
            {
                base.ResetSysExData("BenCalcPayStart", Conversion.ToDate(objClaim.BenCalcPayStart).ToShortDateString());
            }
            else
            {
                if (objClaim.BenefitsStart.Trim() != "")
                {
                    base.ResetSysExData("BenCalcPayStart", Conversion.ToDate(objClaim.BenefitsStart).ToShortDateString());
                }
                else
                {
                    base.ResetSysExData("BenCalcPayStart", Conversion.ToDate(sBenefitStartDate).ToShortDateString());
                }
            }
            if (objClaim.BenCalcPayTo.Trim() != "")
            {
                base.ResetSysExData("BenCalcPayTo", Conversion.ToDate(objClaim.BenCalcPayTo).ToShortDateString());
            }
            else
            {
                base.ResetSysExData("BenCalcPayTo", sBenCalcPayTo);
            }
            base.ResetSysExData("ChkFederalTax", m_dtpTaxes.bFed.ToString());
            base.ResetSysExData("ChkSSTax", m_dtpTaxes.bSS.ToString());
            base.ResetSysExData("ChkMedTax", m_dtpTaxes.bMed.ToString());
            base.ResetSysExData("ChkStateTax", m_dtpTaxes.bState.ToString());

            //Changed for MITS 10728 by Gagan: Start
            if (objXML.SelectSingleNode("//ReserveType") != null)
                base.ResetSysExData("ReserveType", objXML.SelectSingleNode("//ReserveType").InnerText);
            else
                base.ResetSysExData("ReserveType", "");
            base.ResetSysExData("ReserveTypeArray", sReserveType);
            //Changed for MITS 10728 by Gagan: End

            if (objOldAppendAttributeList != null)
                objXML.DocumentElement.ReplaceChild(objNewAppendAttributeList, objOldAppendAttributeList);
            else
                objXML.DocumentElement.AppendChild(objNewAppendAttributeList);

            objNewAppendAttributeList = objXML.CreateElement("PensionOffset");
            objNewAppendAttributeList.InnerText = objClaim.PensionAmt.ToString();
            objXML.DocumentElement.AppendChild(objNewAppendAttributeList);

            objNewAppendAttributeList = objXML.CreateElement("SocialSecurityOffset");
            objNewAppendAttributeList.InnerText = objClaim.SsAmt.ToString();
            objXML.DocumentElement.AppendChild(objNewAppendAttributeList);

            objNewAppendAttributeList = objXML.CreateElement("OtherIncomeOffset");
            objNewAppendAttributeList.InnerText = objClaim.OtherAmt.ToString();
            objXML.DocumentElement.AppendChild(objNewAppendAttributeList);

            //Animesh Inserted For GHS Enhancement  //pmittal5 Mits 14841
            //Other Offset1
            objNewAppendAttributeList = objXML.CreateElement("OtherOffset1");
            objNewAppendAttributeList.InnerText = objClaim.OtherOffset1.ToString();
            objXML.DocumentElement.AppendChild(objNewAppendAttributeList);
            //Other Offset 2
            objNewAppendAttributeList = objXML.CreateElement("OtherOffset2");
            objNewAppendAttributeList.InnerText = objClaim.OtherOffset2.ToString();
            objXML.DocumentElement.AppendChild(objNewAppendAttributeList);
            // Other Offset 3
            objNewAppendAttributeList = objXML.CreateElement("OtherOffset3");
            objNewAppendAttributeList.InnerText = objClaim.OtherOffset3.ToString();
            objXML.DocumentElement.AppendChild(objNewAppendAttributeList);
            //Post Tax Deduction 1  
            objNewAppendAttributeList = objXML.CreateElement("PostTaxDed1");
            objNewAppendAttributeList.InnerText = objClaim.PostTaxDed1.ToString();
            objXML.DocumentElement.AppendChild(objNewAppendAttributeList);
            //Post Tax Deduction 2 
            objNewAppendAttributeList = objXML.CreateElement("PostTaxDed2");
            objNewAppendAttributeList.InnerText = objClaim.PostTaxDed2.ToString();
            objXML.DocumentElement.AppendChild(objNewAppendAttributeList);
            //Animesh Insertion Ends
            

            base.ResetSysExData("ClaimBenefitStartDate", objClaim.BenefitsStart); //pmittal5 Mits 17436 09/15/09

            if (CommonFunctions.IsScheduleDatesOn("SCH_DATE_NONOCC_PMT", m_sConnectionString, base.ClientId))
            {
                base.ResetSysExData("AdjustPrintDatesFlag", objClaim.AdjustPrintDatesFlag.ToString());
            }
 
   		}


		public override void OnUpdateObject()
        {		
            XmlDocument objXML = base.SysEx;
            //Mgaba2
            // npadhy start In case of NonOcc Payments Screen , all the controls are bound with Sysex. So Propertystore is blank.
            // So, we are getting the Claim Object once again, and refilling its values, which are changed in onUpdateObject
            int iTempClaimId = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("/SysExData/ClaimId").InnerText.ToString());
            objClaim.MoveTo(iTempClaimId);



			base.OnUpdateObject();
			

			// Verify Security
			//for non-occ only check non-occ security id, is no separate create/update permission
			if(base.ModuleSecurityEnabled)//Is Security Enabled
			{
				if(!m_fda.userLogin.IsAllowedEx(this.SecurityId,FormBase.RMO_UPDATE))
				{
                    Errors.Add(Globalization.GetString("SaveError", base.ClientId),
                        String.Format(Globalization.GetString("Save.NonOccSaveFailedClaim", base.ClientId)),
						BusinessAdaptorErrorType.Error);
					return;
				}
			}

			//TaxFlags is bitmask:  1=has been set,fed=2,ss=4,med=8,state=16
			int iTaxFlags=1;	//1 means .TaxFlags has been set

			//Benefits Start
			objClaim.BenefitsStart=objXML.SelectSingleNode("/SysExData/BenefitStartDate").InnerText;
			//Benefits Through
			objClaim.BenefitsThrough=objXML.SelectSingleNode("/SysExData/BenefitsThrough").InnerText;
			//Payments Beginning
			objClaim.BenCalcPayStart=objXML.SelectSingleNode("/SysExData/BenCalcPayStart").InnerText;
			//Payments Through
			objClaim.BenCalcPayTo=objXML.SelectSingleNode("/SysExData/BenCalcPayTo").InnerText;
			//Deduct Federal Tax
			if (Conversion.ConvertStrToBool(objXML.SelectSingleNode("/SysExData/ChkFederalTax").InnerText))
				iTaxFlags=iTaxFlags+2;
			//Deduct Soc Sec Tax
			if (Conversion.ConvertStrToBool(objXML.SelectSingleNode("/SysExData/ChkSSTax").InnerText))
				iTaxFlags=iTaxFlags+4;
			//Deduct Medicare Tax
			if (Conversion.ConvertStrToBool(objXML.SelectSingleNode("/SysExData/ChkMedTax").InnerText))
				iTaxFlags=iTaxFlags+8;
			//Deduct State Tax
			if (Conversion.ConvertStrToBool(objXML.SelectSingleNode("/SysExData/ChkStateTax").InnerText))
				iTaxFlags=iTaxFlags+16;

			objClaim.TaxFlags=iTaxFlags;

            //Raman Bhatia: LTD changes
            //objClaim.PensionAmt = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("/SysExData/PensionOffset").InnerText);
            //objClaim.SsAmt = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("/SysExData/SocialSecurityOffset").InnerText);
            //objClaim.OtherAmt = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("/SysExData/OtherIncomeOffset").InnerText);
           
            //MITS : 13027 Umesh
            objClaim.PensionAmt = Conversion.ConvertStrToDouble(objXML.SelectSingleNode("/SysExData/PensionOffset").InnerText);
            objClaim.SsAmt = Conversion.ConvertStrToDouble(objXML.SelectSingleNode("/SysExData/SocialSecurityOffset").InnerText);
            objClaim.OtherAmt = Conversion.ConvertStrToDouble(objXML.SelectSingleNode("/SysExData/OtherIncomeOffset").InnerText);
            //MITS : 13027 End

            //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
            objClaim.OtherOffset1 = Conversion.ConvertStrToDouble(objXML.SelectSingleNode("/SysExData/OtherOffset1").InnerText);
            objClaim.OtherOffset2 = Conversion.ConvertStrToDouble(objXML.SelectSingleNode("/SysExData/OtherOffset2").InnerText);
            objClaim.OtherOffset3 = Conversion.ConvertStrToDouble(objXML.SelectSingleNode("/SysExData/OtherOffset3").InnerText);
            objClaim.PostTaxDed1 = Conversion.ConvertStrToDouble(objXML.SelectSingleNode("/SysExData/PostTaxDed1").InnerText);
            objClaim.PostTaxDed2 = Conversion.ConvertStrToDouble(objXML.SelectSingleNode("/SysExData/PostTaxDed2").InnerText);
           //Animesh Insertion Ends
            //neha R8 Funds utility enhancement
            if (objXML.SelectSingleNode("/SysExData/AdjustPrintDatesFlag") != null) //rupal: added if not null condition
            {
                objClaim.AdjustPrintDatesFlag = Conversion.ConvertStrToBool(objXML.SelectSingleNode("/SysExData/AdjustPrintDatesFlag").InnerText);
            }
            
		}

		//validate data according to the Business Rules
		public override void OnValidate(ref bool Cancel)
		{
			bool bError = false;
			
			// Perform data validation
			if (objClaim.BenCalcPayStart.CompareTo(objClaim.BenefitsStart) < 0)
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeGreaterThan", base.ClientId), Globalization.GetString("Field.PaymentsStartDate", base.ClientId), Conversion.ToDate(objClaim.BenefitsStart).ToShortDateString()),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}

			// Perform data validation
			if (objClaim.BenCalcPayTo.CompareTo(objClaim.BenefitsThrough) > 0)
			{
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    String.Format(Globalization.GetString("Validation.MustBeLessThan", base.ClientId), Globalization.GetString("Field.PaymentsThroughDate", base.ClientId), Conversion.ToDate(objClaim.BenefitsThrough).ToShortDateString()),
					BusinessAdaptorErrorType.Error);

				bError = true;
			}

			// Return true if there were validation errors
			Cancel = bError;
		}

		#region Private helper functions
        /// <summary>
        /// Added By: Parijat : Mits 11550 : Adding work period functionality
        /// </summary>
        /// <param name="p_sBaseDate"></param>
        /// <param name="p_sPrdType"></param>
        /// <param name="p_iPrd"></param>
        /// <returns></returns>
        private string AddWorkPeriodToDate(string p_sBaseDate, string p_sPrdType, int p_iPrd)
        {
            int iWaitPeriod = 0;
            switch (p_sPrdType.ToLower())
            {
                case "yyyy":
                    iWaitPeriod = p_iPrd * 365;
                    break;
                case "q":
                    //return Conversion.ToDbDate(Conversion.ToDate(p_sBaseDate).AddMonths((p_iPrd - 1) * 3));
                    iWaitPeriod = (p_iPrd *3*30);
                    break;
                case "m":
                    iWaitPeriod = (p_iPrd* 30);
                    break;
                case "y":
                    iWaitPeriod = (p_iPrd * 365);
                    break;
                case "d":
                    iWaitPeriod = p_iPrd;
                    break;
                case "ww":
                    iWaitPeriod = p_iPrd *7;
                    break;
            }
            //
            //Parijat: MITS 11550 Work Days
            int dOW = (int)Conversion.ToDate(p_sBaseDate).DayOfWeek;
            int count = 0;
            DateTime dt = Conversion.ToDate(p_sBaseDate);
            while (count < iWaitPeriod)
            {
                if (dOW < 6)
                    dOW += 1;
                else
                    dOW = 0;

                if (iDaysOfWeek[dOW] == -1)
                {
                    dt = dt.AddDays(1);
                    count += 1;
                }
                else
                {
                    dt = dt.AddDays(1);
                }
            }
            return Conversion.ToDbDate(dt);  
           
        }
		private string AddPeriodToDate(string p_sBaseDate,string p_sPrdType,int p_iPrd)
		{
			switch(p_sPrdType.ToLower())
			{
				case "yyyy":
					return Conversion.ToDbDate(Conversion.ToDate(p_sBaseDate).AddYears(p_iPrd));
				case "q":
					return Conversion.ToDbDate(Conversion.ToDate(p_sBaseDate).AddMonths((p_iPrd-1)*3));
				case "m":
					return Conversion.ToDbDate(Conversion.ToDate(p_sBaseDate).AddMonths(p_iPrd));
				case "y":
					return Conversion.ToDbDate(Conversion.ToDate(p_sBaseDate).AddYears(p_iPrd));
				case "d":
					return Conversion.ToDbDate(Conversion.ToDate(p_sBaseDate).AddDays(p_iPrd));
				//case "w":
				//	break;
				case "ww":
                    //abisht
                    //MITS 27963 mcapps2 03/28/2012 - We should not deduct a day from the week calculation.  We want the date to be the next day
					//return Conversion.ToDbDate(Conversion.ToDate(p_sBaseDate).AddDays(((p_iPrd)*7) - 1));
                    return Conversion.ToDbDate(Conversion.ToDate(p_sBaseDate).AddDays(p_iPrd * 7));
				//case "h":
				//	break;
				//case "n":
				//	break;
				//case "s":
				//	break;
			}
			return "";
		}


		private void AppendBankAccountList()
		{
			//Variable declarations
			ArrayList arrAcctList = new ArrayList();
			XmlDocument objXML = base.SysEx;
            //nadim for 13133,XMLElement created instead of Xmlnode
			//XmlNode objOld=null;            
            XmlElement objOldElement = null;
             //nadim 13133
			XmlElement objNew=null;
			XmlCDataSection objCData=null;

			//Retrieve the security credential information to pass to the FundManager object
			string sDSNName = base.m_fda.userLogin.objRiskmasterDatabase.DataSourceName;
			string sUserName = base.m_fda.userLogin.LoginName;
			string sPassword = base.m_fda.userLogin.Password;
			int iUserId = base.m_fda.userLogin.UserId;
			int iGroupId = base.m_fda.userLogin.GroupId;

            using (FundManager objFundMgr = new FundManager(sDSNName, sUserName, sPassword, iUserId, iGroupId, objClaim.ClaimId, base.ClientId))
            {
                arrAcctList = objFundMgr.GetAccountsEx(true, true, 0);
            }

			//Create the necessary SysExData to be used in ref binding
            //code added by nadim for 13133
		//	objOld = objXML.SelectSingleNode("//AccountList");            
           objOldElement = (XmlElement)objXML.SelectSingleNode("//AccountList");
            //nadim 13133
			objNew = objXML.CreateElement("AccountList");

			objElemAppendAttributeList=objXML.CreateElement("bankaccount");
			objChildAppendAttributeList=objXML.CreateElement("title");
			if (objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
			{
				objNew.SetAttribute("codeid",objClaim.DisabilityPlan.SubAccRowId.ToString());
				objChildAppendAttributeList.SetAttribute("value","Sub Bank Account");
			}
			else
			{
				objNew.SetAttribute("codeid",objClaim.DisabilityPlan.BankAccId.ToString());
				objChildAppendAttributeList.SetAttribute("value","Bank Account");
			}
			objElemAppendAttributeList.AppendChild(objChildAppendAttributeList);;
			objNewAppendAttributeList.AppendChild(objElemAppendAttributeList);

			//Loop through and create all the option values for the combobox control
			foreach (FundManager.AccountDetail item in arrAcctList)
			{
				XmlElement xmlOption = objXML.CreateElement("option");
				objCData = objXML.CreateCDataSection(item.AccountName);
				xmlOption.AppendChild(objCData);
				XmlAttribute xmlOptionAttrib = objXML.CreateAttribute("value");
                //MITS 13347 : Umesh
                if (objData.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                {
                    xmlOptionAttrib.Value = item.SubRowId.ToString();
                }
                else
                {
                    xmlOptionAttrib.Value = item.AccountId.ToString();
                }
                //MITS 13347 : End
				xmlOption.Attributes.Append(xmlOptionAttrib);
				objNew.AppendChild(xmlOption);
			}//end foreach

            
            //Add the XML to the SysExData 
            //code added by nadim for 13133
            //if (objOld != null)
            if(objOldElement!=null)
            {                
                 //objNew.SetAttribute("codeid", ((XmlElement)objOld).GetAttribute("codeid"));
                objNew.SetAttribute("codeid", objOldElement.GetAttribute("codeid"));                           
                objXML.DocumentElement.ReplaceChild(objNew, objOldElement);
            }
            else
                objXML.DocumentElement.AppendChild(objNew);            

			//Clean up
			arrAcctList = null;
            //nadim 13133
			//objOld = null;
			objNew = null;
			objCData = null;
            //nadim 13133
            objOldElement = null;
		}

        //MITS 12902 : Umesh
		//private void GetTaxes(int p_iStateId, ref bool[] p_bTaxes)
        private void GetTaxes(Claim p_objClaim, ref bool[] p_bTaxes)
		{
            int iStateId = 0;
			string sSQL=string.Empty;
			DbReader objReader=null;

            if (p_objClaim != null)
                iStateId = p_objClaim.PrimaryClaimant.ClaimantEntity.StateId;
    
			sSQL = "SELECT * FROM TAX_MAPPING WHERE STATE_ID = -1";
            using (objReader = objData.Context.DbConnLookup.ExecuteReader(sSQL))
            {
                while (objReader.Read())
                {
                    switch (Conversion.ConvertObjToStr(objReader.GetValue("TAX_NAME")).Trim().ToUpper())
                    {
                        case "FEDERAL":
                            p_bTaxes[0] = true;
                            if (p_objClaim.PrimaryPiEmployee.CustomFedTaxPer > 0)
                            {
                                m_dtpTaxes.FED_TAX_RATE = (p_objClaim.PrimaryPiEmployee.CustomFedTaxPer) / 100;
                                m_dtpTaxes.FED_TAX_RATE = Math.Round(m_dtpTaxes.FED_TAX_RATE, 4, MidpointRounding.AwayFromZero);
                            }
                            else
                            {
                                m_dtpTaxes.FED_TAX_RATE = Conversion.ConvertObjToDouble(objReader.GetValue("TAX_PERCENTAGE"), base.ClientId) / 100;
                                m_dtpTaxes.FED_TAX_RATE = Math.Round(m_dtpTaxes.FED_TAX_RATE, 4, MidpointRounding.AwayFromZero);
                            }
                            m_dtpTaxes.iFedTaxEID = Conversion.ConvertObjToInt(objReader.GetValue("TAX_EID"), base.ClientId);
                            m_dtpTaxes.iFedTransTypeCodeID = Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE_ID"), base.ClientId);
                            break;
                        case "SOCIAL_SECURITY":
                            p_bTaxes[1] = true;
                            if (p_objClaim.PrimaryPiEmployee.CustomSSTaxPer > 0)
                            {
                                m_dtpTaxes.SS_TAX_RATE = (p_objClaim.PrimaryPiEmployee.CustomSSTaxPer) / 100;
                                m_dtpTaxes.SS_TAX_RATE = Math.Round(m_dtpTaxes.SS_TAX_RATE, 4, MidpointRounding.AwayFromZero);
                            }
                            else
                            {
                                m_dtpTaxes.SS_TAX_RATE = Conversion.ConvertObjToDouble(objReader.GetValue("TAX_PERCENTAGE"), base.ClientId) / 100;
                                m_dtpTaxes.SS_TAX_RATE = Math.Round(m_dtpTaxes.SS_TAX_RATE, 4, MidpointRounding.AwayFromZero);
                            }
                            
                            m_dtpTaxes.iSSTaxEID = Conversion.ConvertObjToInt(objReader.GetValue("TAX_EID"), base.ClientId);
                            m_dtpTaxes.iSSTransTypeCodeID = Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE_ID"), base.ClientId);
                            break;
                        case "MEDICARE":
                            p_bTaxes[2] = true;
                            if (p_objClaim.PrimaryPiEmployee.CustomMedTaxPer > 0)
                            {
                                m_dtpTaxes.MEDICARE_TAX_RATE = (p_objClaim.PrimaryPiEmployee.CustomMedTaxPer) / 100;
                                m_dtpTaxes.MEDICARE_TAX_RATE = Math.Round(m_dtpTaxes.MEDICARE_TAX_RATE, 4, MidpointRounding.AwayFromZero);
                            }
                            else
                            {
                                m_dtpTaxes.MEDICARE_TAX_RATE = Conversion.ConvertObjToDouble(objReader.GetValue("TAX_PERCENTAGE"), base.ClientId) / 100;
                                m_dtpTaxes.MEDICARE_TAX_RATE = Math.Round(m_dtpTaxes.MEDICARE_TAX_RATE, 4, MidpointRounding.AwayFromZero);
                            }
                           
                            m_dtpTaxes.iMedicareTaxEID = Conversion.ConvertObjToInt(objReader.GetValue("TAX_EID"), base.ClientId);
                            m_dtpTaxes.iMedicareTransTypeCodeID = Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE_ID"), base.ClientId);
                            break;
                    }
                }
            }
            //objReader.Close();
            //objReader.Dispose();

			if (iStateId>0)
			{
				sSQL = "SELECT * FROM TAX_MAPPING WHERE STATE_ID = " + iStateId.ToString();
                using (objReader = objData.Context.DbConnLookup.ExecuteReader(sSQL))
                {
                    if (objReader.Read())
                    {
                        p_bTaxes[3] = true;
                        if (p_objClaim.PrimaryPiEmployee.CustomSTTaxPer > 0)
                        {
                            m_dtpTaxes.STATE_TAX_RATE = (p_objClaim.PrimaryPiEmployee.CustomSTTaxPer) / 100;
                            m_dtpTaxes.STATE_TAX_RATE = Math.Round(m_dtpTaxes.STATE_TAX_RATE, 4, MidpointRounding.AwayFromZero);
                        }
                        else
                        {
                            m_dtpTaxes.STATE_TAX_RATE = Conversion.ConvertObjToDouble(objReader.GetValue("TAX_PERCENTAGE"), base.ClientId) / 100;
                            m_dtpTaxes.STATE_TAX_RATE = Math.Round(m_dtpTaxes.STATE_TAX_RATE, 4, MidpointRounding.AwayFromZero);
                        }
                       
                        m_dtpTaxes.iStateTaxEID = Conversion.ConvertObjToInt(objReader.GetValue("TAX_EID"), base.ClientId);
                        m_dtpTaxes.iStateTransTypeCodeID = Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE_ID"), base.ClientId);
                    }
                }
                //objReader.Close();
                //objReader.Dispose();
			}
		}


		private string AppendTransactionTypeCombo(int iClaimId)
		{
			string sSQL=string.Empty;
			XmlDocument objXML=base.SysEx;
			bool bValueFound=false;
			XmlCDataSection objCData=null;
			DbReader objReader=null;
			XmlElement xmlOption=null;
			XmlAttribute xmlOptionAttrib=null;
			XmlNode objOld=null;
			XmlElement objNew=null;
			string sReserveType=string.Empty;
            string sReserveStatus = string.Empty;//skhare7 27920
          

            //Changed by Gagan for MITS 10901: Start
            objLobSettings = objData.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode];

			if(objClaim.Context.DbConn.DatabaseType==eDatabaseType.DBMS_IS_ORACLE)
			{
				sSQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE || ' - ' || CODES_TEXT.CODE_DESC";
			}
			else
			{
				sSQL = "SELECT CODES.CODE_ID, CODES.SHORT_CODE + ' - ' + CODES_TEXT.CODE_DESC";
			}
            sSQL = sSQL + " FROM CODES, CODES_TEXT, GLOSSARY";
            sSQL = sSQL + " WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID";
            sSQL = sSQL + " AND CODES.TABLE_ID = GLOSSARY.TABLE_ID";
            sSQL = sSQL + " AND GLOSSARY.SYSTEM_TABLE_NAME = 'TRANS_TYPES'";
            sSQL = sSQL + " AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)";
            
            if (objLobSettings.ResByClmType == true)
            {
                sSQL = sSQL + " AND CODES.RELATED_CODE_ID IN (	SELECT SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES,CODES WHERE SYS_CLM_TYPE_RES.LINE_OF_BUS_CODE = 844 AND SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE = CODES.CODE_ID AND CODES.RELATED_CODE_ID = 1066 AND SYS_CLM_TYPE_RES.CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode + ")";
                sSQL = sSQL + " AND CODES.CODE_ID NOT IN (SELECT TRANS_TYPE_CODE_ID FROM TAX_MAPPING)";
            }
            else
            {
                sSQL = sSQL + " AND CODES.RELATED_CODE_ID IN (SELECT SYS_LOB_RESERVES.RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES,CODES WHERE SYS_LOB_RESERVES.LINE_OF_BUS_CODE = 844 AND SYS_LOB_RESERVES.RESERVE_TYPE_CODE = CODES.CODE_ID AND CODES.RELATED_CODE_ID = 1066)";
                sSQL = sSQL + " AND CODES.CODE_ID NOT IN (SELECT TRANS_TYPE_CODE_ID FROM TAX_MAPPING)";
            }    
            //pmittal5 Mits 14344 04/07/09
            sSQL = sSQL + " AND (CODES.LINE_OF_BUS_CODE = 844 OR CODES.LINE_OF_BUS_CODE = 0 OR CODES.LINE_OF_BUS_CODE IS NULL)";

            //Changed by Gagan for MITS 10901: End
            using (objReader = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
            {

                //Create the necessary SysExData to be used in ref binding
                objOld = objXML.SelectSingleNode("//TransactionType");
                objNew = objXML.CreateElement("TransactionType");

                objNew.SetAttribute("codeid", "");
                //			bool b=false;
                //			long l=0;
                int iCodeId = 0;
                string sShortCode = string.Empty;
                string sShortCodeStatus = string.Empty;
                string sCodeDesc = string.Empty;
                object oReserveStatus = null;//skhare7
                //create an empty node
                xmlOption = objXML.CreateElement("option");
                xmlOption.InnerText = "";
                xmlOptionAttrib = objXML.CreateAttribute("value");
                xmlOptionAttrib.Value = "0";
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);

                if (objReader != null)
                {
                    //				b = objData.Context.InternalSettings.ColLobSettings["844"].ResByClmType;

                    //Loop through and create all the option values for the combobox control
                    while (objReader.Read())
                    {
                        bValueFound = true;
                        xmlOption = objXML.CreateElement("option");
                        objCData = objXML.CreateCDataSection(Conversion.ConvertObjToStr(objReader[1]));
                        xmlOption.AppendChild(objCData);
                        xmlOptionAttrib = objXML.CreateAttribute("value");
                        xmlOptionAttrib.Value = Conversion.ConvertObjToInt(objReader[0], base.ClientId).ToString();
                        xmlOption.Attributes.Append(xmlOptionAttrib);
                        objNew.AppendChild(xmlOption);

                        //get reserve type for each transaction.  reservetype label set in nonocc.js setReserveType() onchange event added in above line
                        iCodeId = objData.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(Conversion.ConvertObjToInt(objReader[0], base.ClientId));
                        //					s = lCodeId + "k";  //key is [ReserveTypeCode]k
                        //if reserved by claim type, key is [ReserveTypeCode]k[ClaimTypeCode]k  see uglyhack in RMSetUtil
                        //					if (b) 
                        //						s = s + objClaim.ClaimTypeCode + "k";
                        //					l = objData.Context.InternalSettings.ColLobSettings["844"].ColReserveTypes(s);
                        objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(iCodeId, ref sShortCode, ref sCodeDesc);

                        xmlOptionAttrib = objXML.CreateAttribute("reservetype");
                        xmlOptionAttrib.Value = iCodeId.ToString();
                        //sReserveType=sReserveType + iCodeId.ToString() + ",";
                        sReserveType = sReserveType + sCodeDesc + ",";
                        xmlOption.Attributes.Append(xmlOptionAttrib);
                        //skhare7 MITS 27920
                   

                         oReserveStatus = DbFactory.ExecuteScalar(this.m_sConnectionString,"SELECT RES_STATUS_CODE FROM RESERVE_CURRENT WHERE CLAIM_ID=" + iClaimId + " AND RESERVE_TYPE_CODE=" + iCodeId);
                         if (oReserveStatus != null && oReserveStatus != string.Empty)
                         {
                             oReserveStatus = Conversion.ConvertObjToStr(oReserveStatus);
                             sShortCodeStatus = objData.Context.InternalSettings.CacheFunctions.GetShortCode(Conversion.ConvertObjToInt(oReserveStatus, base.ClientId));

                             sReserveType = sReserveType + sCodeDesc + "_" + sShortCodeStatus + ",";
                         }
                    }//end foreach

                    //objReader.Close();
                    //objReader.Dispose();
                }
                if (oReserveStatus != null)
                    oReserveStatus = null;
            }

            //Changed for MITS 10728 by Gagan: Start
            //Add the XML to the SysExData 
            if (objOld != null)
            {
                objNew.SetAttribute("codeid", objOld.Attributes["codeid"].Value.ToString());
                objXML.DocumentElement.ReplaceChild(objNew, objOld);
            }
            else
                objXML.DocumentElement.AppendChild(objNew);
            //Changed for MITS 10728 by Gagan: End


			if (bValueFound==false)
			{
				m_bAllowCalc=false;
                Errors.Add(Globalization.GetString("FillNonOccDomError", base.ClientId), Globalization.GetString("UpdateForm.FillNonOccDomFailed.TransTypeReq", base.ClientId),
					BusinessAdaptorErrorType.Error);
			}

			objElemAppendAttributeList=objXML.CreateElement("transactiontypecode");
			objChildAppendAttributeList=objXML.CreateElement("onchange");
            objChildAppendAttributeList.SetAttribute("value", "setReserveType('transactiontypecode');");	//bank acct/transcode not saved to database

			objElemAppendAttributeList.AppendChild(objChildAppendAttributeList);;
			objNewAppendAttributeList.AppendChild(objElemAppendAttributeList);

			//sReserveType=" "+ "," + sReserveType.Trim(new char[]{','});    averma62 MITS 31325 - No need to do this as it is already done above.

            sReserveType = sReserveType.Trim(new char[] { ',' });  //averma62 MITS 31325
			return sReserveType;
		}

		
		private void GetBenefitPeriod(DisabilityClass p_objDisClass, ref string p_sBenefitStartDate, ref string p_sBenCalcPayStart, ref string p_sBenCalcPayTo)
		{
			bool bFoundDisType = false;
			int iBenDisType=0;
			string sShortCode=string.Empty;
			string sCodeDesc=string.Empty;
			DisClassWait objDisClassWait=null;
			string sTempDate=string.Empty;

			//Benefit Start Date
			if (p_objDisClass.WaitList.Count > 0) //alternate waiting periods exist
			{
				iBenDisType=objClaim.DisTypeCode;
				foreach(DisClassWait objClassWait in p_objDisClass.WaitList)
				{
					if (iBenDisType==objClassWait.DisTypeCode)
					{
						objDisClassWait=objClassWait;
						bFoundDisType=true;	//found an alternate waiting period that matches claim's benefit disability type
						break;
					}
				}
			}

			if (bFoundDisType)	//found an objClassWait (alternate waiting period) that matches claim's Benefit Disabilty Type
			{
				objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(objDisClassWait.DisPrdType,ref sShortCode,ref sCodeDesc); //Days: Weeks
				sShortCode = sShortCode.ToLower();
				if (sShortCode == "w")
					sShortCode = "ww";
				p_sBenefitStartDate = AddPeriodToDate(objClaim.DisabilFromDate,sShortCode, objDisClassWait.DisWaitPrd);
				}
			else	//alternate waiting periods do not exist, or did not find an objClassWait (alternate waiting period) that matches claim's Benefit Disabilty Type
			{
				objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(p_objDisClass.DisPrdType,ref sShortCode,ref sCodeDesc); //Days: Weeks
				sShortCode = sShortCode.ToLower();
				if (sShortCode == "w")
					sShortCode = "ww";
				p_sBenefitStartDate =AddPeriodToDate(objClaim.DisabilFromDate,sShortCode, p_objDisClass.DisWaitPrd);
			}

			if (objClaim.BenCalcPayStart.Trim()!="")
				p_sBenCalcPayStart=objClaim.BenCalcPayStart;
			else if (objClaim.BenefitsStart.Trim()!="")
				p_sBenCalcPayStart=objClaim.BenefitsStart;
			else
				p_sBenCalcPayStart=p_sBenefitStartDate;

			if (objClaim.BenCalcPayTo.Trim()!="")
				p_sBenCalcPayTo=objClaim.BenCalcPayTo;
			else if (objClaim.BenefitsThrough.Trim()!="")
				p_sBenCalcPayTo=objClaim.BenefitsThrough;
			else
			{
				//use plan class max duration of disability (if any) added to benefit start date
				objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(p_objDisClass.FromDisPrdType,ref sShortCode,ref sCodeDesc); //Days: Weeks
				if (sShortCode.ToUpper()=="B")
					sTempDate=p_sBenefitStartDate;
				else
					sTempDate=objClaim.DisabilFromDate;

				//add days/months/weeks?
				objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(p_objDisClass.DisMaxPrdType,ref sShortCode,ref sCodeDesc); //Days: Weeks
				sShortCode = sShortCode.ToLower();
				if (sShortCode == "w")
					sShortCode = "ww";

				//objClass.DIS_MAX_PRD is # of days/months/weeks
				sTempDate = AddPeriodToDate(sTempDate,sShortCode, p_objDisClass.DisMaxPrd);
				p_sBenCalcPayTo = sTempDate;
			}
		}
		private void GetWorkWeek(DisabilityClass objClass)
		{
			if(objClass.Day7WorkWeek)
			{
				for(int i=0;i<7;i++)
				{
					iDaysOfWeek[i] = -1;
				}
			}
			else
			{
				if(objClass.Day5WorkWeek)
				{
					for(int i=0;i<7;i++)
					{
                        //if(i==0 && i==6) Commented and updated by Pawan for mits 11996
						if(i==0 || i==6)
							iDaysOfWeek[i] = 0;
						else
							iDaysOfWeek[i] = -1;
					}
				}
				else
				{
					if(objClass.ActualWorkWeek)
					{
						if(objClaim.PrimaryPiEmployee.WorkSunFlag)
							iDaysOfWeek[0]=-1;
						else
							iDaysOfWeek[0]=0;
						if(objClaim.PrimaryPiEmployee.WorkMonFlag)
							iDaysOfWeek[1]=-1;
						else
							iDaysOfWeek[1]=0;
						if(objClaim.PrimaryPiEmployee.WorkTueFlag)
							iDaysOfWeek[2]=-1;
						else
							iDaysOfWeek[2]=0;
						if(objClaim.PrimaryPiEmployee.WorkWedFlag)
							iDaysOfWeek[3]=-1;
						else
							iDaysOfWeek[3]=0;
						if(objClaim.PrimaryPiEmployee.WorkThuFlag)
							iDaysOfWeek[4]=-1;
						else
							iDaysOfWeek[4]=0;
						if(objClaim.PrimaryPiEmployee.WorkFriFlag)
							iDaysOfWeek[5]=-1;
						else
							iDaysOfWeek[5]=0;
						if(objClaim.PrimaryPiEmployee.WorkSatFlag)
							iDaysOfWeek[6]=-1;
						else
							iDaysOfWeek[6]=0;
					}
					else
					{
						if(objClaim.PrimaryPiEmployee.WorkSunFlag)
							iDaysOfWeek[0]=-1;
						else
							iDaysOfWeek[0]=0;
						if(objClaim.PrimaryPiEmployee.WorkMonFlag)
							iDaysOfWeek[1]=-1;
						else
							iDaysOfWeek[1]=0;
						if(objClaim.PrimaryPiEmployee.WorkTueFlag)
							iDaysOfWeek[2]=-1;
						else
							iDaysOfWeek[2]=0;
						if(objClaim.PrimaryPiEmployee.WorkWedFlag)
							iDaysOfWeek[3]=-1;
						else
							iDaysOfWeek[3]=0;
						if(objClaim.PrimaryPiEmployee.WorkThuFlag)
							iDaysOfWeek[4]=-1;
						else
							iDaysOfWeek[4]=0;
						if(objClaim.PrimaryPiEmployee.WorkFriFlag)
							iDaysOfWeek[5]=-1;
						else
							iDaysOfWeek[5]=0;
						if(objClaim.PrimaryPiEmployee.WorkSatFlag)
							iDaysOfWeek[6]=-1;
						else
							iDaysOfWeek[6]=0;
					}
				}
			}
		}
        private void performDisabilityCalculations(string p_sBenefitStartDate, string p_sBenCalcPayStart , string p_sBenCalcPayTo , ref double p_dblWeeklyBenefit)
        {
            int iTypeOfWage = 0; 
            int iPayType = 0; //kdb 03/04/2005  Hourly = 1, Weekly = 2, Monthly = 3
            int i = 0;
            DisabilityClass objDisabilityClass = null;
            double dWages = 0;
            double dNewWeeklyWage;
            double dHigh;
            double dLow;
            DisClassTd  objThisTableRow ;
            double dPayment; //kdb 04/13/2005
            double dSupp = 0; //kdb 04/13/2005
            double dWeeklyCap = 0;
            bool bUseVouchers = false;
            bool bFoundTableValue = true;
            double dDailySuppAmount = 0; //kdb 05/02/2005
            int iWeekCode = 0;
            string sBenefitType = "";
            bool[] bTaxes = new bool[] { false, false, false, false };
            long lPensionOffTransTypeCodeID = 0;
            long lSSOffTransTypeCodeID = 0;
            long lOtherOffTransTypeCodeID = 0;
            StringBuilder sSql = new StringBuilder();
            DbReader objReader = null;
            string sConnectionString = base.m_fda.connectionString;
            bool bProRateOffsetFlag = false;
            bool bFullPayOnlyFlag = false;
            string sShortCode = "";
            string sCodeDesc = "";
            double dAWWAmount = 0;
            string lblLabel = "";
            double dDailyAmount = 0;
            XmlDocument objXML = null;

            //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
            long lOthOff1TransTypeCodeID = 0;
            long lOthOff2TransTypeCodeID = 0;
            long lOthOff3TransTypeCodeID = 0;
            long lPostTaxDed1TransTypeCodeID = 0;
            long lPostTaxDed2TransTypeCodeID = 0;
            //Animesh Insertion Ends
          
            // Pull the preferred schedule selected on the disability plan
            // Added by Rahul, as per RMWorld.
            if (objData.Context.InternalSettings.CacheFunctions.GetShortCode(objClaim.DisabilityPlan.PrefPaySchCode).ToLower() == "pw")
            {
                iPrefSched = 1;
                sBenefitType = "Weekly Benefit";
            }
            if (objData.Context.InternalSettings.CacheFunctions.GetShortCode(objClaim.DisabilityPlan.PrefPaySchCode).ToLower() == "pb")
            {
                iPrefSched = 2;
                sBenefitType = "Weekly Benefit";
            }
            if (objData.Context.InternalSettings.CacheFunctions.GetShortCode(objClaim.DisabilityPlan.PrefPaySchCode).ToLower() == "pm")
            {
                iPrefSched = 3;
                sBenefitType = "Monthly Benefit";
            }
            //End of preferred schedule selected on the disability plan

            //MITS 12902 : Umesh
            //GetTaxes(objClaim.PrimaryClaimant.ClaimantEntity.StateId, ref bTaxes);
            GetTaxes(objClaim, ref bTaxes);

            //Raman Bhatia: LTD changes: Get the Transaction Types for Offsets

            sSql.Append("SELECT * FROM OFFSET_MAPPING");
            using (objReader = DbFactory.GetDbReader(sConnectionString, sSql.ToString()))
            {
                //loop and get OFFSET trans type code
                while (objReader.Read())
                {
                    if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "PENSION")
                        lPensionOffTransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), base.ClientId);
                    if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "SOCIAL_SECURITY")
                        lSSOffTransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), base.ClientId);
                    if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHER_INCOME")
                        lOtherOffTransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), base.ClientId);
                    //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                    if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHEROFFSET1")
                        lOthOff1TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), base.ClientId);
                    if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHEROFFSET2")
                        lOthOff2TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), base.ClientId);
                    if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "OTHEROFFSET3")
                        lOthOff3TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), base.ClientId);
                    if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "POST_TAX_DED1")
                        lPostTaxDed1TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), base.ClientId);
                    if (Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")) == "POST_TAX_DED2")
                        lPostTaxDed2TransTypeCodeID = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_TYPE_CODE_ID"), base.ClientId);
                    //Animesh Insertion Ends
                }
            }
            //objReader.Close();
            sSql.Remove(0, sSql.Length);

            double dblWeeklyRate = objClaim.PrimaryPiEmployee.WeeklyRate;
			double dblHourlyRate = objClaim.PrimaryPiEmployee.HourlyRate;
			double dblMonthlyRate = objClaim.PrimaryPiEmployee.MonthlyRate;
            string sDTGCalBenefitStart = "";
            string sDTGCalBenefitEnd = "";
                        
            if (objClaim.DisabilityPlan.PlanId != 0)
            {
                if ((!GetPayPeriod(iPrefSched , ref dThisPayPeriodStart , ref dThisPayPeriodEnd)) || (objClaim.DisabilityPlan.ClassList.Count == 0)) 
                {
                    objXML = base.SysEx;
                    XmlElement objElement = objXML.CreateElement("GrossCalculatedPayment");
                    objElement.InnerText = "0";
                    objXML.DocumentElement.AppendChild(objElement);

                    objElement = objXML.CreateElement("GrossCalculatedSupplement");
                    objElement.InnerText = "0";
                    objXML.DocumentElement.AppendChild(objElement);

                    objElement = objXML.CreateElement("Supplement");
                    objElement.InnerText = "0";
                    objXML.DocumentElement.AppendChild(objElement);

                    objElement = objXML.CreateElement("DailyAmount");
                    objElement.InnerText = dDailyAmount.ToString();
                    objXML.DocumentElement.AppendChild(objElement);

                    objElement = objXML.CreateElement("DailySuppAmount");
                    objElement.InnerText = dDailySuppAmount.ToString();
                    objXML.DocumentElement.AppendChild(objElement);

                    objElement = objXML.CreateElement("iDaysWorkingInMonth");
                    objElement.InnerText = iDaysWorkingInMonth.ToString();
                    objXML.DocumentElement.AppendChild(objElement);

                    objElement = objXML.CreateElement("iDaysWorkingInWeek");
                    objElement.InnerText = iDaysWorkingInWeek.ToString();
                    objXML.DocumentElement.AppendChild(objElement);

                    m_bEligible = false;
                    m_sEligTitle = "NOTE: Employee Not Eligible For Benefits.";
                    
                    return;
                }
                /*
                if (objClaim.DisabilityPlan.ClassList.Count == 0)
                {
                    return;
                }
                 */ 
                if (p_sBenCalcPayStart == "")
                {
                    if (objClaim.BenCalcPayStart.Trim() != "")
                    {
                        sDTGCalBenefitStart = Conversion.ToDate(objClaim.BenCalcPayStart).ToShortDateString();
                    }
                    else
                    {
                        if (objClaim.BenefitsStart.Trim() != "")
                        {
                            sDTGCalBenefitStart = Conversion.ToDate(objClaim.BenefitsStart).ToShortDateString();
                        }
                        else
                        {
                            sDTGCalBenefitStart = Conversion.ToDate(p_sBenefitStartDate).ToShortDateString();
                        }
                    }
                }
                else
                    sDTGCalBenefitStart = p_sBenCalcPayStart;
                if (p_sBenCalcPayTo == "")
                {
                    if (objClaim.BenCalcPayTo.Trim() != "")
                    {
                        sDTGCalBenefitEnd = Conversion.ToDate(objClaim.BenCalcPayTo).ToShortDateString();
                    }
                    else
                    {
                        sDTGCalBenefitEnd = p_sBenCalcPayTo;
                    }
                }
                else
                    sDTGCalBenefitEnd = p_sBenCalcPayTo;
                foreach (DisabilityClass objClass in objClaim.DisabilityPlan.ClassList)
                {
                    if (objClass.ClassRowId == objClaim.ClassRowId)
                    {
                        //MWC Get Weekly Cap
                        dWeeklyCap = objClass.WeeklyBeneCap;
                        //MWC Get Taxable Percent
                        dTaxablePercent = objClass.TaxablePercent;
                        dTaxablePercent = (dTaxablePercent / 100);
                        //MWC Get Voucher Info
                        bUseVouchers = objClass.UseVouchers;
                        //mwc get pay schedule code: Weekly : Bi-Weekly : Monthly
                        iWeekCode = objClaim.DisabilityPlan.PrefPaySchCode;
                        //kdb 04/20/2005  get the pro-rate offset flag value
                        bProRateOffsetFlag = objClass.ProrateOffFlag;
                        //kdb 04/25/2005 get the full pay only flag value
                        bFullPayOnlyFlag = objClass.FullpayOnlyFlag;
                        //kdb 04/28/2005 Get the calc type for offset
                        iOffsetCalc = GetOffsetCalc(bProRateOffsetFlag, bFullPayOnlyFlag);
                        //Get the appropriate wage
                        if (objClass.BenePrctgFlag)
                            iTypeOfWage = 1; //Percent of Wage
                        else if (objClass.BeneFlatAmtFlag)
                            iTypeOfWage = 2; //Flat Rate
                        else if (objClass.BeneTdFlag)
                        {
                            iTypeOfWage = 3; //Table Driven
                            //check for hourly, weekly, or monthly kdb 03/04/2005
                            objData.Context.InternalSettings.CacheFunctions.GetCodeInfo(objClass.BeneBasedCode, ref sShortCode, ref sCodeDesc);
                            if (sCodeDesc.ToLower() == "hourly rate")
                                iPayType = 1; //Hourly
                            else if (sCodeDesc.ToLower() == "weekly rate")//Parijat:Mits 11942
                                iPayType = 2; //Weekly
                            else
                                iPayType = 3; //Monthly
                        }

                        //See what Taxes are being withheld
                        if (objClaim.TaxFlags == 0) // mwc only set default flags if they have not been previously saved
                        {
                            m_dtpTaxes.bFed = objClass.WithholdFedItax;
                            m_dtpTaxes.bSS = objClass.WithholdFica;
                            m_dtpTaxes.bMed = objClass.WithholdMedicare;
                            m_dtpTaxes.bState = objClass.WithholdState;
                        }
                        objDisabilityClass = objClass;
                        continue;
                    }

                    
                }
                if (i > objClaim.DisabilityPlan.ClassList.Count)
                {
                    return;
                }
                 //kdb 04/04/2005  If/Then to for preferred payment schedule
                if (iPrefSched == 1 || iPrefSched == 2)
                {
                    if(objClaim.ClaimAWW!=null) //ClaimAWW exists
                    {
                        if(objClaim.ClaimAWW.Aww > 0) //kdb 04/04/2005 added in iPayType clause
                        {
                            if (iTypeOfWage == 3 && iPayType == 1) //kdb LCase(sGetCodeDesc(m_claim.Plan.PlanXClassList(i).BENE_BASED_CODE)) = "hourly rate" Then
                            {
                                dAWWAmount = dblHourlyRate;
                                lblLabel = "Hourly Wage:";
                                dWages = objClaim.ClaimAWW.Aww;
                            }
                            else
                            {
                                dAWWAmount = objClaim.ClaimAWW.Aww;
                                lblLabel = "Weekly Wage:";
                                dWages = objClaim.ClaimAWW.Aww;
                            }
                        }
                        else
                        {
                            if (iTypeOfWage == 3 && iPayType == 1) //kdb LCase(sGetCodeDesc(m_claim.Plan.PlanXClassList(i).BENE_BASED_CODE)) = "hourly rate" Then
                            {
                                dAWWAmount = dblHourlyRate;
                                lblLabel = "Hourly Wage:";
                                dWages = dblHourlyRate;
                            }
                            else
                            {
                                dAWWAmount = dblWeeklyRate;
                                lblLabel = "Weekly Wage:";
                                dWages = dblWeeklyRate;
                            }
                        }
                    }
                    else //m_claim.ClaimAWW = Nothing
                    if (iTypeOfWage == 3 && iPayType == 1)
                    {
                        dAWWAmount = dblHourlyRate;
                        lblLabel = "Hourly Wage:";
                        dWages = dblHourlyRate;
                    }
                    else
                    {
                        dAWWAmount = dblWeeklyRate;
                        lblLabel = "Weekly Wage:";
                        dWages = dblWeeklyRate;
                    }
                }
                else if (iPrefSched == 3) //monthly
                    {
                          if (iTypeOfWage == 3 && iPayType == 1)
                          {
                                dAWWAmount = dblHourlyRate;
                                lblLabel = "Hourly Wage:";
                                dWages = dblHourlyRate;
                          }
                          else if(iTypeOfWage == 3 && iPayType == 2)
                          {
                                dAWWAmount = dblWeeklyRate;
                                lblLabel = "Weekly Wage:";
                                dWages = dblWeeklyRate;
                          }
                          else
                          {
                              dAWWAmount = dblMonthlyRate;
                              dWages = dblMonthlyRate;
                              lblLabel = "Monthly Wage";
                          }
                    }
                if(iTypeOfWage > 0)
                {
                    switch (iTypeOfWage)
                    {
                        case 1: if (objDisabilityClass.BenePerAmt != 0)
                            {
                                if (iPrefSched != 3)
                                {
                                    dAWWAmount = dblWeeklyRate;
                                }
                                else
                                {
                                    dAWWAmount = dblMonthlyRate;
                                }
                                dGrossPayment = dAWWAmount * objDisabilityClass.BenePerAmt / 100; //Mits 17772
                            }
                            else
                            {
                                if (iPrefSched != 3)
                                {
                                    dAWWAmount = dblWeeklyRate;
                                }
                                else
                                {
                                    dAWWAmount = dblMonthlyRate;
                                }
                                dGrossPayment = dAWWAmount;
                            }
                            if (objDisabilityClass.SuppPercent > 0)
                            {
                                dGrossSupplement = (dAWWAmount * objDisabilityClass.SuppPercent) / 100;
                            }
                            break;

                        case 2: //Flat Rate
                            dGrossPayment = objDisabilityClass.BeneFlatAmt;
                            dAWWAmount = dGrossPayment;
                            if (objDisabilityClass.SuppFlatAmt > 0)
                            {
                                 //Changed by Shivendu for MITS 11943
                                //dGrossSupplement = (dAWWAmount * objDisabilityClass.SuppFlatAmt) / 100;
                                dGrossSupplement = objDisabilityClass.SuppFlatAmt;
                            }
                            break;

                        case 3: //Table Driven
                            switch (iPayType)
                            {
                                case 2: bFoundTableValue = false;
                                    foreach (DisClassTd objTheTableRow in objDisabilityClass.TableList)
                                    {
                                        if (dWages <= objTheTableRow.WagesTo)
                                        {
                                            if (dWages >= objTheTableRow.WagesFrom)//parijat: Mits 11942
                                            {
                                                dGrossPayment = objTheTableRow.WeeklyBenefit;
                                                dAWWAmount = dGrossPayment;
                                                bFoundTableValue = true;
                                                if (objTheTableRow.SuppAmt > 0)
                                                {
                                                    dGrossSupplement = objTheTableRow.SuppAmt;
                                                    dSupp = dGrossSupplement;
                                                }
                                                break;//Parijat: Mits 11942
                                            }
                                        }
                                        dGrossPayment = dWages;
                                        dAWWAmount = dWages;
                                    }
                                    if (bFoundTableValue == false)
                                    {
                                        dGrossPayment = 0;
                                        dGrossSupplement = 0;
                                        dSupp = 0;
                                        dAWWAmount = 0;
                                    }
                                    break;
                                case 1: // Hourly Rate
                                    bFoundTableValue = false;
                                    foreach (DisClassTd objTheTableRow in objDisabilityClass.TableList)
                                    {
                                        if (dblHourlyRate <= objTheTableRow.WagesTo)
                                        {
                                            if (dblHourlyRate >= objTheTableRow.WagesFrom)//Parijat: Mits 11942
                                            {
                                                dNewWeeklyWage = objTheTableRow.WeeklyBenefit;
                                                dGrossPayment = objTheTableRow.WeeklyBenefit;
                                                dAWWAmount = dGrossPayment;
                                                bFoundTableValue = true;
                                                if (objTheTableRow.SuppAmt > 0)
                                                {
                                                    dGrossSupplement = objTheTableRow.SuppAmt;
                                                    dSupp = dGrossSupplement;
                                                }
                                                break;//Parijat: Mits 11942
                                            }
                                        }

                                    }
                                    if (bFoundTableValue == false)
                                    {
                                        dGrossPayment = 0;
                                        dGrossSupplement = 0;
                                        dSupp = 0;
                                        dAWWAmount = 0;
                                    }
                                    break;
                                case 3: //Monthly Rate
                                    bFoundTableValue = false;
                                    foreach (DisClassTd objTheTableRow in objDisabilityClass.TableList)
                                    {
                                        if (dblMonthlyRate <= objTheTableRow.WagesTo)
                                        {
                                            if (dblMonthlyRate >= objTheTableRow.WagesFrom)//Parijat: Mits 11942
                                            {
                                                dGrossPayment = objTheTableRow.WeeklyBenefit;
                                                dAWWAmount = dGrossPayment;
                                                bFoundTableValue = true;
                                                if (objTheTableRow.SuppAmt > 0)
                                                {
                                                    dGrossSupplement = objTheTableRow.SuppAmt;
                                                    dSupp = dGrossSupplement;
                                                }
                                                break;//Parijat: Mits 11942
                                            }
                                        }
                                        dGrossPayment = dAWWAmount;
                                        dAWWAmount = dWages;
                                    }
                                    if (bFoundTableValue == false)
                                    {
                                        dGrossPayment = 0;
                                        dGrossSupplement = 0;
                                        dSupp = 0;
                                        dAWWAmount = 0;
                                    }
                                    break;
                            }
                            break;
                    }
               }
               //mwc 02/06/2003 If we are dealing with a biweekly payment scheme we need to double the amount cap
               if(objClaim.DisabilityPlan.PrefPaySchCode == 1737) //And chkFirstWeek.Value Then
               {
                    dWeeklyCap = dWeeklyCap * 2;
                    dGrossPayment = dGrossPayment * 2;
                    dGrossSupplement = dGrossSupplement * 2; //kdb 04/13/2005
                    dSupp = dGrossSupplement;
               }
        
                //mwc 02/05/2003 Apply cap
                if(dWeeklyCap != 0)
                {
                    if(objDisabilityClass.SuppPercent > 0 || objDisabilityClass.SuppFlatAmt > 0) //'kdb 04/13/2005 Check for Supplement
                    {
                        if(dWeeklyCap < (dGrossPayment + dSupp))
                        {
                            if (dGrossPayment < dWeeklyCap)
                                dGrossSupplement = dWeeklyCap - dGrossPayment;
                            else
                            {
                                dGrossPayment = dWeeklyCap;
                                dGrossSupplement = 0;
                            }
                        }
                    }
                    else //If no supplement
                    if(dWeeklyCap < dGrossPayment)
                        dGrossPayment = dWeeklyCap;
               
                }
                if(iPrefSched == 1 || iPrefSched == 2)
                {
                    if(objDisabilityClass.Day7WorkWeek)
                    {
                        for(int j=0 ; j<7;j++)
                            iDaysOfWeek[j] = -1;
                    }
                    else if(objDisabilityClass.Day5WorkWeek)
                    {
                        for(int j=0 ; j<7;j++)
                        {
                            if(j==0 || j==6)
                                iDaysOfWeek[j] = 0;
                            else
                                iDaysOfWeek[j] = -1;
                        }
                    }
                    else if(objDisabilityClass.ActualWorkWeek)
                    {
                        iDaysOfWeek[0] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkSunFlag, base.ClientId);
                        iDaysOfWeek[1] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkMonFlag, base.ClientId);
                        iDaysOfWeek[2] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkTueFlag, base.ClientId);
                        iDaysOfWeek[3] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkWedFlag, base.ClientId);
                        iDaysOfWeek[4] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkThuFlag, base.ClientId);
                        iDaysOfWeek[5] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkFriFlag, base.ClientId);
                        iDaysOfWeek[6] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkSatFlag, base.ClientId);
                        
                    }   
                    else
                    {
                        iDaysOfWeek[0] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkSunFlag, base.ClientId);
                        iDaysOfWeek[1] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkMonFlag, base.ClientId);
                        iDaysOfWeek[2] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkTueFlag, base.ClientId);
                        iDaysOfWeek[3] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkWedFlag, base.ClientId);
                        iDaysOfWeek[4] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkThuFlag, base.ClientId);
                        iDaysOfWeek[5] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkFriFlag, base.ClientId);
                        iDaysOfWeek[6] = Conversion.ConvertBoolToInt(objClaim.PrimaryPiEmployee.WorkSatFlag, base.ClientId);
                        
                    }
          
                    iDaysWorkingInWeek = 0;
                    for(int j = 0;j<7;j++)
                        if(iDaysOfWeek[j] != 0) 
                           iDaysWorkingInWeek = iDaysWorkingInWeek + 1;
                }
                else if(iPrefSched == 3)
                {     //kdb 04/04/2005 Monthly options
                    if (objDisabilityClass.Day30WorkMonth)
                        b30DayMonthFlag = true;
                    else if(objDisabilityClass.ActualMonth)
                    {
                        int iMonth = dThisPayPeriodStart.Month;
                        int iYear = dThisPayPeriodStart.Year;

                        if (iMonth == 12)
                        {
                            iMonth = 0;
                            iYear = iYear + 1;
                        }
                        //DateTime dTempDate = new DateTime(dThisPayPeriodStart.Year, dThisPayPeriodStart.Month + 1, 1);
                        DateTime dTempDate = new DateTime(iYear, iMonth + 1, 1);
                        iDaysWorkingInMonth = dTempDate.AddDays(-1).Day;
                    }
                }
                       
                //if(objClaim.DisabilityPlan.PrefPaySchCode == 1737)
                   // iDaysWorkingInWeek = iDaysWorkingInWeek * 2;

                char[] sSeperator = new char[2];
                sSeperator[0] = '/';
                sSeperator[1] = '/';
                
                string[] sSplitsStart  = sDTGCalBenefitStart.Split(sSeperator , 3);
                string[] sSplitsEnd = sDTGCalBenefitEnd.Split(sSeperator, 3);
                if (sSplitsStart[1].Length == 1)
                    sSplitsStart[1] = "0" + sSplitsStart[1];
                if (sSplitsStart[0].Length == 1)
                    sSplitsStart[0] = "0" + sSplitsStart[0];
                if (sSplitsEnd[1].Length == 1)
                    sSplitsEnd[1] = "0" + sSplitsEnd[1];
                if (sSplitsEnd[0].Length == 1)
                    sSplitsEnd[0] = "0" + sSplitsEnd[0];

                if (iPrefSched == 3)
                {   //MITS 14022 : Umesh
                    if(dThisPayPeriodEnd <= Conversion.ToDate(sSplitsEnd[2] + sSplitsEnd[0] + sSplitsEnd[1]))
                        ilblDaysIncluded = GetDaysIncludedMonth(Conversion.ToDate(sSplitsStart[2] + sSplitsStart[0] + sSplitsStart[1]), dThisPayPeriodEnd);
                    else
                        ilblDaysIncluded = GetDaysIncludedMonth(Conversion.ToDate(sSplitsStart[2] + sSplitsStart[0] + sSplitsStart[1]), Conversion.ToDate(sSplitsEnd[2] + sSplitsEnd[0] + sSplitsEnd[1]));
                }
                else if (iPrefSched == 1 || iPrefSched == 2)
                    ilblDaysIncluded = GetDaysIncluded(Conversion.ToDate(sSplitsStart[2] + sSplitsStart[0] + sSplitsStart[1]), Conversion.ToDate(sSplitsEnd[2] + sSplitsEnd[0] + sSplitsEnd[1]), iDaysOfWeek, true, false);
               
               //kdb 04/04/2005 - use days/week unless schedule is monthly
                if (iPrefSched == 1 )  
                {
                    // Compute Gross Payment
                    if (iDaysWorkingInWeek > 0)
                    {
                        dDailyAmount = dGrossPayment / iDaysWorkingInWeek;
                        dDailySuppAmount = dGrossSupplement / iDaysWorkingInWeek;  //kdb 04/13/2005
                    }
                }
                else if (iPrefSched == 2)
                {
                    // Compute Gross Payment
                    if (iDaysWorkingInWeek > 0)
                    {
                        dDailyAmount = dGrossPayment / (iDaysWorkingInWeek * 2);
                        dDailySuppAmount = dGrossSupplement / (iDaysWorkingInWeek * 2);  //kdb 04/13/2005
                    }
                }
                else if (iPrefSched == 3)
                {
                    if (b30DayMonthFlag)
                    {
                        dDailyAmount = dGrossPayment / 30;
                        dDailySuppAmount = dGrossSupplement / 30;
                    }
                    else
                    {
                        if(iDaysWorkingInMonth > 0)
                        {
                            dDailyAmount = dGrossPayment / iDaysWorkingInMonth;
                            dDailySuppAmount = dGrossSupplement / iDaysWorkingInMonth;
                        }
                        else
                        {
                            dDailyAmount = 0;
                            dDailySuppAmount = 0;
                        }
                    }
                }

                dSupp = dGrossSupplement;
                dPayment = dGrossPayment;
                string sSupplementCaption = "";
                double dSupprate = 0;

                dGrossPayment = dDailyAmount * ilblDaysIncluded;
                dGrossSupplement = dDailySuppAmount * ilblDaysIncluded; //kdb 04/13/2005d

                if(iPrefSched == 2) //m_claim.Plan.PrefPaySchCode = 1737 Then
                {
                    sSupplementCaption = "Weekly Supplement";
                    p_dblWeeklyBenefit = dPayment / 2;
                     //if(objDisabilityClass.SuppPercent > 0 || objDisabilityClass.SuppFlatAmt > 0)//Parijat: Mits 11942
                     //{   
                         dSupprate = dSupp / 2;
                     //}
                         m_dGrossPayment = p_dblWeeklyBenefit; //MITS 14186
                         m_dGrossSupplement = dSupprate; // MITS 14186
                }
                else if(iPrefSched == 1)
                {
                    sSupplementCaption = "Weekly Supplement";
                    p_dblWeeklyBenefit = dPayment;
                    //if(objDisabilityClass.SuppPercent > 0 || objDisabilityClass.SuppFlatAmt > 0)//Parijat: Mits 11942
                    // {   
                         dSupprate = dSupp;
                     //}
                         m_dGrossPayment = p_dblWeeklyBenefit; //MITS 14186
                         m_dGrossSupplement = dSupprate; // MITS 14186
                }
                else
                {
                    sSupplementCaption = "Monthly Supplement";
                    p_dblWeeklyBenefit = dPayment;
                    //if(objDisabilityClass.SuppPercent > 0 || objDisabilityClass.SuppFlatAmt > 0)//Parijat: Mits 11942
                    // {   
                         dSupprate = dSupp;
                     //}
                         m_dGrossPayment = p_dblWeeklyBenefit; //MITS 14186
                         m_dGrossSupplement = dSupprate; // MITS 14186
                }
                
                //Adding GrossPayment , GrossSupplement , Supplement , dDailyAmount , dDailySuppAmount in Sysex
                objXML = base.SysEx;
                //pmittal5 Mits 14126 12/23/08
                //if (base.m_fda.SafeFormVariableParamText("SysCmd") != "7")
                if (base.m_fda.SafeFormVariableParamText("SysCmd") != "7" && base.m_fda.SafeFormVariableParamText("SysCmd") != "5")
                {
                    XmlElement objElement = objXML.CreateElement("GrossCalculatedPayment");
                    //objElement.InnerText = Math.Round(dGrossPayment, 2).ToString();
                    objElement.InnerText = Math.Round(dGrossPayment, 2, MidpointRounding.AwayFromZero).ToString();

                    objXML.DocumentElement.AppendChild(objElement);

                    objElement = objXML.CreateElement("GrossCalculatedSupplement");
                    //objElement.InnerText = Math.Round(dGrossSupplement, 2).ToString();
                    objElement.InnerText = Math.Round(dGrossSupplement, 2, MidpointRounding.AwayFromZero).ToString();
                    objXML.DocumentElement.AppendChild(objElement);

                    objElement = objXML.CreateElement("Supplement");
                    objElement.InnerText = Math.Round(dSupprate, 2, MidpointRounding.AwayFromZero).ToString();
                    objXML.DocumentElement.AppendChild(objElement);

                    objElement = objXML.CreateElement("DailyAmount");
                    objElement.InnerText = dDailyAmount.ToString();
                    objXML.DocumentElement.AppendChild(objElement);

                    objElement = objXML.CreateElement("DailySuppAmount");
                    objElement.InnerText = dDailySuppAmount.ToString();
                    objXML.DocumentElement.AppendChild(objElement);

                    objElement = objXML.CreateElement("iDaysWorkingInMonth");
                    objElement.InnerText = iDaysWorkingInMonth.ToString();
                    objXML.DocumentElement.AppendChild(objElement);

                    objElement = objXML.CreateElement("iDaysWorkingInWeek");
                    objElement.InnerText = iDaysWorkingInWeek.ToString();
                    objXML.DocumentElement.AppendChild(objElement);

                    //XmlDocument objSysView = base.SysView;
                    //objElement = (XmlElement)objSysView.SelectSingleNode("//control[@name = 'supplement']");
                    //objElement.InnerText = Math.Round(dSupprate, 2).ToString();
                    //if(sSupplementCaption != "")
                    //    objElement.SetAttribute("title", sSupplementCaption);


                    singleRow = new ArrayList();
                    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.TextBox.ToString());
                    base.AddElementToList(ref singleRow, "id", "supplement");
                    base.AddElementToList(ref singleRow, "Text", Math.Round(dSupprate, 2, MidpointRounding.AwayFromZero).ToString());
                    base.m_ModifiedControls.Add(singleRow);

                    if (sSupplementCaption != "")
                    {
                        singleRow = new ArrayList();
                        base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                        base.AddElementToList(ref singleRow, "id", "lbl_supplement");
                        base.AddElementToList(ref singleRow, "Text", sSupplementCaption);
                        base.m_ModifiedControls.Add(singleRow);
                    }
                    
                }
                else
                {
                    base.ResetSysExData("GrossCalculatedPayment", Math.Round(dGrossPayment, 2, MidpointRounding.AwayFromZero).ToString());
                    base.ResetSysExData("GrossCalculatedSupplement", Math.Round(dGrossSupplement, 2, MidpointRounding.AwayFromZero).ToString());
                    base.ResetSysExData("Supplement", Math.Round(dSupprate, 2, MidpointRounding.AwayFromZero).ToString());
                    base.ResetSysExData("DailyAmount", dDailyAmount.ToString());
                    base.ResetSysExData("DailySuppAmount", dDailySuppAmount.ToString());
                    base.ResetSysExData("iDaysWorkingInWeek", iDaysWorkingInWeek.ToString());
                    base.ResetSysExData("iDaysWorkingInMonth", iDaysWorkingInMonth.ToString());

                    //XmlDocument objSysView = base.SysView;
                    //XmlElement objElement = (XmlElement)objSysView.SelectSingleNode("//control[@name = 'supplement']");
                    //objElement.InnerText = Math.Round(dSupprate, 2).ToString();
                    //objElement.SetAttribute("title", sSupplementCaption);
                    singleRow = new ArrayList();
                    base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.TextBox.ToString());
                    base.AddElementToList(ref singleRow, "id", "supplement");
                    base.AddElementToList(ref singleRow, "Text", Math.Round(dSupprate, 2, MidpointRounding.AwayFromZero).ToString());
                    base.m_ModifiedControls.Add(singleRow);

                    
                        singleRow = new ArrayList();
                        base.AddElementToList(ref singleRow, "ControlType", enummodifiedcontrolType.Labels.ToString());
                        base.AddElementToList(ref singleRow, "id", "lbl_supplement");
                        base.AddElementToList(ref singleRow, "Text", sSupplementCaption);
                        base.m_ModifiedControls.Add(singleRow);
                   
                }
               
            }
            
        }
       
        public bool GetPayPeriod(int p_iPrefSched, ref DateTime dThisPayPeriodStart, ref DateTime dThisPayPeriodEnd)
        {
            DateTime dBeneStart = DateTime.MinValue;
            DateTime dPlanPayStart = DateTime.MinValue;
            DateTime dPlanPayEnd = DateTime.MinValue;
            int iTmp = 0;
            int iLastDayOfMonth = 0 ;
            bool bFoundDate = false;
            DateTime dCheckDate = DateTime.MinValue;
            bool bGetPayPeriod = false;
            int i;

            bFoundDate = false;
            
            if (objClaim.DisabilityPlan.StartPayPeriod != "")
                dPlanPayStart = Conversion.ToDate(objClaim.DisabilityPlan.StartPayPeriod);

            if (objClaim.BenefitsStart != "")
                dBeneStart = Conversion.ToDate(objClaim.BenCalcPayStart);
            else
                dBeneStart = Convert.ToDateTime(m_sBenCalcPayStart);

            if (dPlanPayStart != DateTime.MinValue && dBeneStart != DateTime.MinValue)
            {
                if (objClaim.DisabilityPlan.PrefPaySchCode == 1737)
                {
                    iTmp = 14;
                    dPlanPayEnd = dPlanPayStart.AddDays(iTmp - 1);
                }
                else if (p_iPrefSched == 1)
                {
                    iTmp = 7;
                    dPlanPayEnd = dPlanPayStart.AddDays(iTmp - 1);
                }
                else
                {
                    dPlanPayEnd = dPlanPayStart.AddMonths(1).AddDays(-1);
                }


                while (bFoundDate == false)
                {
                    if (dPlanPayStart <= dBeneStart)
                    {
                        if (dPlanPayStart <= dBeneStart && dPlanPayEnd >= dBeneStart)
                        {
                            dThisPayPeriodStart = dPlanPayStart;
                            dThisPayPeriodEnd = dPlanPayEnd;
                            bFoundDate = true;
                            bGetPayPeriod = true;
                        }
                        else if (p_iPrefSched == 3) //monthly kdb 04/05/2005
                        {
                            dPlanPayStart = dPlanPayEnd.AddDays(1);
                            dPlanPayEnd = dPlanPayStart.AddMonths(1).AddDays(-1);
                        }
                        else //weekly/bi-weekly
                        {
                            dPlanPayStart = dPlanPayStart.AddDays(iTmp);
                            dPlanPayEnd = dPlanPayEnd.AddDays(iTmp);
                        }
                    }
                    else
                        if ((dPlanPayStart < dBeneStart) && (dPlanPayEnd > dBeneStart))
                        {
                            dThisPayPeriodStart = dPlanPayStart;
                            dThisPayPeriodEnd = dPlanPayEnd;
                            bFoundDate = true;
                        }
                        else if (p_iPrefSched == 3) //monthly kdb 04/05/2005
                        {
                            dPlanPayStart = dPlanPayStart.AddMonths(-1);
                            dPlanPayEnd = dPlanPayStart.AddMonths(1).AddDays(-1);
                        }
                        else //weekly/bi-weekly
                        {
                            dPlanPayStart = dPlanPayStart.AddDays(iTmp * -1);
                            dPlanPayEnd = dPlanPayEnd.AddDays(iTmp * -1);
                        }
                }
            }
            else if (dBeneStart != DateTime.MinValue)
            {
                if (p_iPrefSched == 1 || p_iPrefSched == 2)
                {
                    dCheckDate = dBeneStart;
                    for (i = 1; i <= 7; i++)
                    {
                        //if ((int)dCheckDate.DayOfWeek != 1)
                        if ((int)dCheckDate.DayOfWeek != 0)
                        {
                            dCheckDate = dCheckDate.AddDays(-1);
                        }
                        else
                        {
                            dPlanPayStart = dCheckDate;
                            continue;
                        }
                    }
                }
                if (objClaim.DisabilityPlan.PrefPaySchCode == 1737)
                {
                    iTmp = 13;
                    dPlanPayEnd = dPlanPayStart.AddDays(iTmp); //MITS 5650 kdb 5/02/2005 took out tmp - 1
                }
                else if (p_iPrefSched == 1)
                {
                    iTmp = 6;
                    dPlanPayEnd = dPlanPayStart.AddDays(iTmp); //MITS 5650 kdb 5/02/2005 took out tmp-1
                }
                else if (p_iPrefSched == 3) //kdb 03/07/2005 Monthly
                {
                    dCheckDate = new DateTime(dBeneStart.Year, dBeneStart.Month, 1);
                    dPlanPayStart = dCheckDate;
                    dPlanPayEnd = dPlanPayStart.AddMonths(1).AddDays(-1);
                }
            }

           if (dPlanPayStart != DateTime.MinValue && dPlanPayEnd != DateTime.MinValue)
           {
               dThisPayPeriodStart = dPlanPayStart;
               dThisPayPeriodEnd = dPlanPayEnd;
               bGetPayPeriod = true;
           }
           else
               bGetPayPeriod = false;
   
            return bGetPayPeriod;
              
        }

        public bool IsDate(string inValue)
        {
            bool result;
            try
            {
                DateTime myDT = DateTime.Parse(inValue);
                result = true;
            }
            catch (FormatException e)
            {
            result = false;
            }
          
            return result;
            
        }
        //kdb 04/28/2005
        //Return the type of offset calc to be used in the disability benefit calculation
        int GetOffsetCalc(bool p_bProRateFlag , bool p_bFullPayFlag )
        {
            int iGetOffsetCalc = 0;
            if (p_bProRateFlag == false && p_bFullPayFlag == false) 
            {
                //Deduct full amount for full periods; pro-rate for partial
                iGetOffsetCalc = 1;
            }
            else if (p_bProRateFlag == true && p_bFullPayFlag == false)
            {    
                //Deduct pro-rated offsets for all pay periods
                iGetOffsetCalc = 2;
            }
            else if (p_bProRateFlag == false && p_bFullPayFlag == true)
            {
                //Deduct full amount of offset for only full pay periods
                iGetOffsetCalc = 3;
            }
            else 
            {
                //both true
                //Deduct pro-rated offset for only full pay periods
                iGetOffsetCalc = 4;
            }
            return iGetOffsetCalc;
        }

        //KDB 04/06/2005  Calculate the days included in the month pay
        int GetDaysIncludedMonth(DateTime p_dStartDate , DateTime p_dEndDate)
        {
            int iDays = 0;
            int iNumberOfDays = 0;
            int iGetDaysIncludedMonth = 0;

            if(p_dStartDate == DateTime.MinValue)
            {
                iGetDaysIncludedMonth = 0;
                return iGetDaysIncludedMonth;
            }
            if(p_dEndDate == DateTime.MinValue)
            {
                iGetDaysIncludedMonth = 0;
                return iGetDaysIncludedMonth;
            }
        
            if(p_dEndDate.Month == p_dStartDate.Month)
            {
                TimeSpan d = p_dEndDate - p_dStartDate;
                iGetDaysIncludedMonth = d.Days + 1;
            }
            else
            {
                string  sMonth = p_dStartDate.Month.ToString();
               // int iMonth = p_dStartDate.Month + 1;
                int iMonth = 0;
                int iYear = p_dStartDate.Year;
                //MGaba2: changing the sequence of instructions so that it works fine when value of imonth is 9
               // if (iMonth >= 1 && iMonth <= 9)
                if (p_dStartDate.Month >= 1 && p_dStartDate.Month <= 9)
                {
                    sMonth = "0" + p_dStartDate.Month.ToString();
                }
                if (p_dStartDate.Month == 12)
                {
                    iMonth = 1;
                    iYear = iYear + 1;
                }
                else
                {
                    iMonth = p_dStartDate.Month + 1;
                }


                iDays = Conversion.ToDate(p_dStartDate.Year.ToString() + sMonth + "01").AddDays(-1).Day;
                iGetDaysIncludedMonth = (iDays - p_dStartDate.Day) + 1 + p_dEndDate.Day;
            }
            return iGetDaysIncludedMonth;
        }

        int GetPartialPayDays(DateTime sStartDate, DateTime sEndDate, bool bLastPay)
        {
            int i = 0;
            int iEnd = 0;
            int iStart = 0;
            string sTestDate = "";
            int iGetPartialPayDays = 0;

            if (!bLastPay)
            {
                if (sStartDate != DateTime.MinValue && dThisPayPeriodStart != DateTime.MinValue)
                    if (dThisPayPeriodStart > sStartDate)
                        iStart = (dThisPayPeriodStart.Subtract(sStartDate)).Days + 1;
                    else
                        iStart = (sStartDate.Subtract(dThisPayPeriodStart)).Days + 1;
                else
                    iStart = 1;
                if (sEndDate != DateTime.MinValue && dThisPayPeriodEnd != DateTime.MinValue)
                    if (sEndDate < dThisPayPeriodEnd)
                        iEnd = (sEndDate.Subtract(sStartDate)).Days + 1;
                    else
                        iEnd = (dThisPayPeriodEnd.Subtract(sStartDate)).Days + 1;
                else
                    iEnd = 1;
                i = iEnd;
            }
            else if (sEndDate != DateTime.MinValue && dThisPayPeriodEnd != DateTime.MinValue)
                if (sEndDate > dThisPayPeriodEnd)
                    iEnd = (sEndDate.Subtract(dThisPayPeriodEnd)).Days + 1;
                else
                    iStart = (dThisPayPeriodEnd.Subtract(sEndDate)).Days + 1;
            else
                iEnd = 1;

            iGetPartialPayDays = i;

            return iGetPartialPayDays;
        }

        int GetDaysIncluded(DateTime sStartDate, DateTime sEndDate, int[] iWorkWeek, bool bPartialWeek , bool bLastPay )
        {
            int iGetDaysIncluded = 0;
            int i = 0;
            int iNumberOfDays = 0;
            DateTime dCheckDate = DateTime.MinValue;
            int iCount = 0;
            int iDaysToWeekend = 0;
            
            if(sStartDate == DateTime.MinValue)
            {
                iGetDaysIncluded = 0;
                return iGetDaysIncluded;
            }
            if(sEndDate == DateTime.MinValue)
            {
                iGetDaysIncluded = 0;
                return iGetDaysIncluded;
            }
            else
                iNumberOfDays = 1;
            
            if(bPartialWeek)
            {
                if(objClaim.DisabilityPlan.PrefPaySchCode == 1737)
                {
                    if(iNumberOfDays != 0)
                    {
                        iDaysToWeekend = GetPartialPayDays(sStartDate , sEndDate , bLastPay);
                        iNumberOfDays = iDaysToWeekend;
                    }
                }
                else
                {
                     if(iNumberOfDays != 0)
                    {
                        iDaysToWeekend = GetPartialPayDays(sStartDate , sEndDate , bLastPay);
                        iNumberOfDays = iDaysToWeekend;
                    }
                }

            }
            else if(objClaim.DisabilityPlan.PrefPaySchCode == 1737)
                iNumberOfDays = 14;
            else
                iNumberOfDays = 7;
            
            if(iNumberOfDays != 0)
            {
                dCheckDate = sStartDate;
                i = 0;
                for(iCount = 1 ; iCount<=iNumberOfDays;iCount++)
                {
                    if (iWorkWeek[(int)dCheckDate.DayOfWeek] == 1 || iWorkWeek[(int)dCheckDate.DayOfWeek] == -1)
                        i = i + 1;
                    if(iNumberOfDays > 1)
                        dCheckDate = dCheckDate.AddDays(1);
                }
            }
            else
            {
              dCheckDate = sStartDate;
              if (iWorkWeek[(int)dCheckDate.DayOfWeek] == 1 || iWorkWeek[(int)dCheckDate.DayOfWeek] == -1)
                i = 1;
              else
                i = 0;
            }
    
            iGetDaysIncluded = i;
            return iGetDaysIncluded;
        }

        //kdb 04/28/2005  Return whether or not the pay is partial
        bool PartialPay(int iDaysInCurrent)
        {
            int iFullPayDays = 0;
    
            if(iPrefSched == 3)
            {
                if(b30DayMonthFlag)
                    iFullPayDays = 30;
                else
                    iFullPayDays = iDaysWorkingInMonth;
            }
            else if(iPrefSched == 2)
                iFullPayDays = iDaysWorkingInWeek * 2;
            else if(iPrefSched == 1)
                iFullPayDays = iDaysWorkingInWeek;
            
            if(iDaysInCurrent < iFullPayDays)
                return true;
            else
                return false;
            
        }


        void CalculateTotals()
        {
            //Animesh Modified for GHS Enhancement //pmittal5 Mits 14841 
            //double dTotalOffsetAmount = objClaim.PensionAmt + objClaim.SsAmt + objClaim.OtherAmt;
            double dTotalOffsetAmount = objClaim.PensionAmt + objClaim.SsAmt + objClaim.OtherAmt 
                                        +objClaim.OtherOffset1 + objClaim.OtherOffset2 + objClaim.OtherOffset3;
            //Animesh modification ends
            double dPension = 0;
            double dSS = 0;
            double dOther = 0;
            double dLocalGrossPayment = 0;
            double dPercent = 0;
            double dTotalWithholding = 0;
            double dFederalTax = 0;
            double dSocialSecurityAmount = 0;
            double dMedicareAmount = 0;
            double dStateAmount = 0;
            double dNetAmount = 0;
            double dPensionAmt = 0;
            double dSSAmt = 0;
            double dOtherAmt = 0;

            //Animesh Inserted For GHS Enhancement  //pmittal5 Mits 14841
            double dOthOffset1Amt = 0;
            double dOthOffset2Amt = 0;
            double dOthOffset3Amt = 0;
            double dPostTaxDed1Amt = 0;
            double dPostTaxDed2Amt = 0;
            double dOthOffset1 = 0;
            double dOthOffset2 = 0;
            double dOthOffset3 = 0;
            double dPostTaxDed1 = 0;
            double dPostTaxDed2 = 0;
            double dTotalPostTaxOffsetAmt = objClaim.PostTaxDed1 + objClaim.PostTaxDed2;    
            //Animesh Insertion Ends

            double dCalcFactor = 0;  //MITS 14186 
            dGrossPayment = Math.Round(dGrossPayment, 2, MidpointRounding.AwayFromZero);//MITS 28425 - mcapps2 05/17/2012
            //dGrossPayment = Math.Round(dGrossPayment, 2); //MITS 28425 - mcapps2 05/17/2012
            dGrossSupplement = Math.Round(dGrossSupplement, 2, MidpointRounding.AwayFromZero); //MITS 28425 - mcapps2 05/17/2012
            //dGrossSupplement = Math.Round(dGrossSupplement, 2); //MITS 14186  //MITS 28425 - mcapps2 05/17/2012
            dLocalGrossPayment = dGrossPayment;
            //kdb 04/20/2005  calculate pro-rated offset amount for first payment
            //MITS 14186
            if (m_dGrossPayment != 0 && dGrossPayment != 0)
            {

                dCalcFactor = dGrossPayment / m_dGrossPayment;
                dGrossSupplement = m_dGrossSupplement * dCalcFactor;
            }
            else
            {
                if (m_dGrossSupplement != 0 && dGrossSupplement != 0)

                    dCalcFactor = dGrossSupplement / m_dGrossSupplement;
                    
            }
            // End MITS 14186
            //MITS 28425 - mcapps2 05/17/2012 Start
            dPensionAmt = Math.Round(objClaim.PensionAmt, 2, MidpointRounding.AwayFromZero);
            //dPensionAmt = Math.Round(objClaim.PensionAmt,2);
            dSSAmt = Math.Round(objClaim.SsAmt, 2, MidpointRounding.AwayFromZero);
            //dSSAmt = Math.Round(objClaim.SsAmt ,2);
            dOtherAmt = Math.Round(objClaim.OtherAmt, 2, MidpointRounding.AwayFromZero);
            //dOtherAmt = Math.Round(objClaim.OtherAmt ,2);
            //Animesh Inserted For GHS Enhancement  //pmittal5 Mits 14841
            dOthOffset1Amt = Math.Round(objClaim.OtherOffset1, 2, MidpointRounding.AwayFromZero);
            //dOthOffset1Amt = Math.Round(objClaim.OtherOffset1,2);
            dOthOffset2Amt = Math.Round(objClaim.OtherOffset2, 2, MidpointRounding.AwayFromZero);
            //dOthOffset2Amt = Math.Round(objClaim.OtherOffset2,2);
            dOthOffset3Amt = Math.Round(objClaim.OtherOffset3, 2, MidpointRounding.AwayFromZero);
            //dOthOffset3Amt = Math.Round(objClaim.OtherOffset3,2);
            dPostTaxDed1Amt = Math.Round(objClaim.PostTaxDed1, 2, MidpointRounding.AwayFromZero);
            //dPostTaxDed1Amt = Math.Round(objClaim.PostTaxDed1,2);
            dPostTaxDed2Amt = Math.Round(objClaim.PostTaxDed2, 2, MidpointRounding.AwayFromZero);
            //dPostTaxDed2Amt = Math.Round(objClaim.PostTaxDed2,2);   
            //Animesh Insertion Ends
            //MITS 28425 - mcapps2 05/17/2012 End
            if(iPrefSched == 1 )
            {
                if(iDaysWorkingInWeek > 0)//kdb 05/06/2005 quick fix to division by zero error
                {
                    dPension = dPensionAmt / iDaysWorkingInWeek;
                    dSS = dSSAmt / iDaysWorkingInWeek;
                    dOther = dOtherAmt / iDaysWorkingInWeek;
                    //Animesh Inserted For GHS Enhancement //pmittal5 Mits 14841
                    dOthOffset1 = dOthOffset1Amt / iDaysWorkingInWeek;
                    dOthOffset2 = dOthOffset2Amt / iDaysWorkingInWeek;
                    dOthOffset3 = dOthOffset3Amt / iDaysWorkingInWeek;
                    //dPostTaxDed1 = dPostTaxDed1Amt / iDaysWorkingInWeek ;
                    dPostTaxDed1 = dPostTaxDed1Amt;
                    //dPostTaxDed2  = dPostTaxDed2Amt  / iDaysWorkingInWeek ;
                    dPostTaxDed2 = dPostTaxDed2Amt ;
                    //dTotalPostTaxOffsetAmt = (dTotalPostTaxOffsetAmt / iDaysWorkingInWeek) * ilblDaysIncluded;   
                    //Animesh Insertion ends
                    //dTotalOffsetAmount = (dTotalOffsetAmount / iDaysWorkingInWeek) * ilblDaysIncluded;
                    dTotalOffsetAmount = Math.Round(((dPensionAmt * dCalcFactor) + (dSSAmt * dCalcFactor) + (dOtherAmt * dCalcFactor) + (dOthOffset1Amt * dCalcFactor) + (dOthOffset2Amt * dCalcFactor) + (dOthOffset3Amt * dCalcFactor)), 2, MidpointRounding.AwayFromZero); //MITS 28425 - mcapps2 05/17/2012
                    //dTotalOffsetAmount = Math.Round(dPensionAmt * dCalcFactor, 2) + Math.Round(dSSAmt * dCalcFactor, 2) + Math.Round(dOtherAmt * dCalcFactor, 2)+Math.Round(dOthOffset1Amt * dCalcFactor,2)+Math.Round(dOthOffset2Amt * dCalcFactor,2)+Math.Round(dOthOffset3Amt * dCalcFactor,2); //MITS 28425 - mcapps2 05/17/2012
                }
                
                if(PartialPay(ilblDaysIncluded))
                {
                    if(iOffsetCalc == 3 || iOffsetCalc == 4)
                        dTotalOffsetAmount = 0;
                    //pmittal5 12620 11/17/08 - If Pro-Rate flag is OFF then deduct flat offsets
                    else if (iOffsetCalc == 1)
                        {
                        //dTotalOffsetAmount = dPensionAmt + dSSAmt + dOtherAmt;
                        dTotalOffsetAmount = dPensionAmt + dSSAmt + dOtherAmt + dOthOffset1Amt + dOthOffset2Amt + dOthOffset3Amt;
                        //Animesh Inserted For GHS Enhancment //pmittal5 Mits 14841
                        dTotalPostTaxOffsetAmt = dPostTaxDed1Amt + dPostTaxDed2Amt; 
                        //Animesh Insertion Ends
						}

                }
                else
                {
                     if(iOffsetCalc == 1 || iOffsetCalc == 4)
                        //dTotalOffsetAmount = dPensionAmt + dSSAmt + dOtherAmt;
                        dTotalOffsetAmount = dPensionAmt + dSSAmt + dOtherAmt + dOthOffset1Amt + dOthOffset2Amt + dOthOffset3Amt;
                     //Animesh Inserted For GHS Enhancment //pmittal5 Mits 14841
                        dTotalPostTaxOffsetAmt = dPostTaxDed1Amt + dPostTaxDed2Amt; 
                        //Animesh Insertion Ends
                }
            }
            else if (iPrefSched == 2)
            {
                if (iDaysWorkingInWeek > 0)//kdb 05/06/2005 quick fix to division by zero error
                {
                    dPension = dPensionAmt / (iDaysWorkingInWeek * 2);
                    dSS = dSSAmt / (iDaysWorkingInWeek * 2);
                    dOther = dOtherAmt / (iDaysWorkingInWeek * 2);
                    //Animesh Inserted for GHS Enhancment  //pmittal5 Mits 14841
                    dOthOffset1 = dOthOffset1Amt / (iDaysWorkingInWeek * 2);
                    dOthOffset2 = dOthOffset2Amt / (iDaysWorkingInWeek * 2);
                    dOthOffset3 = dOthOffset3Amt / (iDaysWorkingInWeek * 2);
                    //dPostTaxDed1 = dPostTaxDed1Amt / (iDaysWorkingInWeek * 2) ;
                    dPostTaxDed1 = dPostTaxDed1Amt ;
                    //dPostTaxDed2 = dPostTaxDed2Amt / (iDaysWorkingInWeek * 2) ;
                    dPostTaxDed2 = dPostTaxDed2Amt ;
                    //dTotalPostTaxOffsetAmt = (dTotalPostTaxOffsetAmt / (iDaysWorkingInWeek * 2)) * ilblDaysIncluded ;
                    //Animesh Insertion ends
                    //dTotalOffsetAmount = (dTotalOffsetAmount / (iDaysWorkingInWeek * 2)) * ilblDaysIncluded;
                    dTotalOffsetAmount = Math.Round(((dPensionAmt * dCalcFactor) + (dSSAmt * dCalcFactor) + (dOtherAmt * dCalcFactor) + (dOthOffset1Amt * dCalcFactor) + (dOthOffset2Amt * dCalcFactor) + (dOthOffset3Amt * dCalcFactor)), 2, MidpointRounding.AwayFromZero); //MITS 28425 - mcapps2 05/17/2012
                    //dTotalOffsetAmount = Math.Round(dPensionAmt * dCalcFactor, 2) + Math.Round(dSSAmt * dCalcFactor, 2) + Math.Round(dOtherAmt * dCalcFactor, 2) + Math.Round(dOthOffset1Amt * dCalcFactor, 2) + Math.Round(dOthOffset2Amt * dCalcFactor, 2) + Math.Round(dOthOffset3Amt * dCalcFactor, 2); //MITS 28425 - mcapps2 05/17/2012
                }
                if(PartialPay(ilblDaysIncluded))
                {
                    if(iOffsetCalc == 3 || iOffsetCalc == 4)
                        dTotalOffsetAmount = 0;
                    //pmittal5 12620 11/17/08 - If Pro-Rate flag is OFF then deduct flat offsets
                    else if (iOffsetCalc == 1)
                        {
                        //dTotalOffsetAmount = dPensionAmt + dSSAmt + dOtherAmt;
                        //added by rkaur on 4/1/2009 for GHS 
                            dTotalOffsetAmount = (dPensionAmt + dSSAmt + dOtherAmt + dOthOffset1Amt + dOthOffset2Amt + dOthOffset3Amt) * 2;
                        //Animesh Inserted For GHS Enhancment  //pmittal5 Mits 14841
                        dTotalPostTaxOffsetAmt = dPostTaxDed1Amt + dPostTaxDed2Amt; 
                        //Animesh Insertion Ends
						}
                }
                else
                {
                     if(iOffsetCalc == 1 || iOffsetCalc == 4)
                         //Animesh Modified for GHS Enhancement  //pmittal5 Mits 14841
                         //dTotalOffsetAmount = dPensionAmt + dSSAmt + dOtherAmt;
                         dTotalOffsetAmount = (dPensionAmt + dSSAmt + dOtherAmt + dOthOffset1Amt + dOthOffset2Amt + dOthOffset3Amt)*2 ;
                        //Animesh Inserted For GHS Enhancment
                        dTotalPostTaxOffsetAmt = dPostTaxDed1Amt + dPostTaxDed2Amt; 
                        //Animesh Insertion Ends
                }
            }
            else if(iPrefSched == 3)
            {
                if(b30DayMonthFlag)
                {
                    dPension = dPensionAmt / 30;
                    dSS = dSSAmt / 30;
                    dOther = dOtherAmt / 30;
                    //Animesh Inserted For GHS Enhancement  //pmittal5 Mits 14841
                    dOthOffset1 = dOthOffset1Amt / 30;
                    dOthOffset2 = dOthOffset2Amt / 30;
                    dOthOffset3 = dOthOffset3Amt / 30;
                    //dPostTaxDed1 = dPostTaxDed1Amt / 30 ;
                    dPostTaxDed1 = dPostTaxDed1Amt;
                    //dPostTaxDed2 = dPostTaxDed2Amt / 30 ;
                    dPostTaxDed2 = dPostTaxDed2Amt;
                    //dTotalPostTaxOffsetAmt = (dTotalPostTaxOffsetAmt / 30) * ilblDaysIncluded;
                    //Animesh Insertion Ends
                    //dTotalOffsetAmount = (dTotalOffsetAmount / 30) * ilblDaysIncluded;
                    dTotalOffsetAmount = Math.Round(((dPensionAmt * dCalcFactor) + (dSSAmt * dCalcFactor) + (dOtherAmt * dCalcFactor) + (dOthOffset1Amt * dCalcFactor) + (dOthOffset2Amt * dCalcFactor) + (dOthOffset3Amt * dCalcFactor)), 2, MidpointRounding.AwayFromZero); //MITS 28425 - mcapps2 05/17/2012
                    //dTotalOffsetAmount = Math.Round(dPensionAmt * dCalcFactor, 2) + Math.Round(dSSAmt * dCalcFactor, 2) + Math.Round(dOtherAmt * dCalcFactor, 2) + Math.Round(dOthOffset1Amt * dCalcFactor, 2) + Math.Round(dOthOffset2Amt * dCalcFactor, 2) + Math.Round(dOthOffset3Amt * dCalcFactor, 2); //MITS 28425 - mcapps2 05/17/2012
                }
                else if(iDaysWorkingInMonth > 0)
                {
                    dPension = dPensionAmt / iDaysWorkingInMonth;
                    dSS = dSSAmt / iDaysWorkingInMonth;
                    dOther = dOtherAmt / iDaysWorkingInMonth;
                    //Animesh Inserted For GHS Enhancement  //pmittal5 Mits 14841
                    dOthOffset1 = dOthOffset1Amt / iDaysWorkingInMonth;
                    dOthOffset2 = dOthOffset2Amt / iDaysWorkingInMonth;
                    dOthOffset3 = dOthOffset3Amt / iDaysWorkingInMonth;
                    //dPostTaxDed1 = dPostTaxDed1Amt / iDaysWorkingInMonth ;
                    dPostTaxDed1 = dPostTaxDed1Amt;
                    //dPostTaxDed2 = dPostTaxDed2Amt / iDaysWorkingInMonth ;
                    dPostTaxDed2 = dPostTaxDed2Amt ;
                    //dTotalPostTaxOffsetAmt = (dTotalPostTaxOffsetAmt / iDaysWorkingInMonth) * ilblDaysIncluded;
                    //Animesh Insertion Ends
                    //dTotalOffsetAmount = (dTotalOffsetAmount / iDaysWorkingInMonth) * ilblDaysIncluded;
                    dTotalOffsetAmount = Math.Round(((dPensionAmt * dCalcFactor) + (dSSAmt * dCalcFactor) + (dOtherAmt * dCalcFactor) + (dOthOffset1Amt * dCalcFactor) + (dOthOffset2Amt * dCalcFactor) + (dOthOffset3Amt * dCalcFactor)), 2, MidpointRounding.AwayFromZero); //MITS 28425 - mcapps2 05/17/2012
                    //dTotalOffsetAmount = Math.Round(dPensionAmt * dCalcFactor, 2) + Math.Round(dSSAmt * dCalcFactor, 2) + Math.Round(dOtherAmt * dCalcFactor, 2) + Math.Round(dOthOffset1Amt * dCalcFactor, 2) + Math.Round(dOthOffset2Amt * dCalcFactor, 2) + Math.Round(dOthOffset3Amt * dCalcFactor, 2); //MITS 28425 - mcapps2 05/17/2012
                }
                if(PartialPay(ilblDaysIncluded))
                {
                    if(iOffsetCalc == 3 || iOffsetCalc == 4)
                        dTotalOffsetAmount = 0;
                    //pmittal5 12620 11/17/08 - If Pro-Rate flag is OFF then deduct flat offsets
                    else if (iOffsetCalc == 1)
                        {
                        //dTotalOffsetAmount = dPensionAmt + dSSAmt + dOtherAmt;
                        dTotalOffsetAmount = dPensionAmt + dSSAmt + dOtherAmt + dOthOffset1Amt + dOthOffset2Amt + dOthOffset3Amt;
                        //Animesh Inserted For GHS Enhancment //pmittal5 Mits 14841
                        dTotalPostTaxOffsetAmt = dPostTaxDed1Amt + dPostTaxDed2Amt; 
                        //Animesh Insertion Ends
						}
                }
                else
                {//pmittal5 Mits 16043 05/01/09 - For 31 Day month, if Prorate is ON then Offsets for 1 extra day are also calculated 
                     //if(iOffsetCalc == 1 || iOffsetCalc == 4)
                     if (iOffsetCalc == 1 || iOffsetCalc == 3) 
                         //Animesh Modified for GHS Enhancement   //pmittal5 Mits 14841  
                         //dTotalOffsetAmount = dPensionAmt + dSSAmt + dOtherAmt;
                         dTotalOffsetAmount = dPensionAmt + dSSAmt + dOtherAmt + dOthOffset1Amt + dOthOffset2Amt + dOthOffset3Amt ;
                         //Animesh Modification ends
                         //Animesh Inserted for GHS Enhancment //pmittal5 Mits 14841
                        dTotalPostTaxOffsetAmt = dPostTaxDed1Amt + dPostTaxDed2Amt; 
                        //Animesh Insertion Ends
                }
            }
            //Bi-Weekly
            if(iPrefSched == 2)
            {
                dPension = dPension * 2;
                dSS = dSS * 2;
                dOther = dOther * 2;
                //Animesh inserted for GHS Enhancment //pmittal5 Mits 14841
                dOthOffset1 = dOthOffset1 * 2;
                dOthOffset2 = dOthOffset2 * 2 ;
                dOthOffset3 = dOthOffset3 * 2 ;
                //dPostTaxDed1 = dPostTaxDed1Amt * 2 ;
                dPostTaxDed1 = dPostTaxDed1Amt;
                //dPostTaxDed2 = dPostTaxDed2Amt * 2 ;
                dPostTaxDed2 = dPostTaxDed2Amt;
               // dTotalOffsetAmount = dTotalOffsetAmount * 2;  //MITS 14186
                //Animesh Insertion Ends
                //dTotalOffsetAmount = dTotalOffsetAmount * 2;
            }
            double dlblTotOffsets = dTotalOffsetAmount;
            dLocalGrossPayment = Math.Round((dLocalGrossPayment + dGrossSupplement - dTotalOffsetAmount), 2, MidpointRounding.AwayFromZero); //MITS 28425 - mcapps2 05/17/2012
            //dLocalGrossPayment = Math.Round((dLocalGrossPayment + dGrossSupplement - dTotalOffsetAmount),2);  //MITS 14186  //MITS 28425 - mcapps2 05/17/2012
            double dTempPayment = dLocalGrossPayment;   //pmittal5 Mits 14253 01/19/09
            //MITS 14186
            //As discussed with Mike Hamann , if  dPayment - dTotalOffset = 0.01
            //deduct 1 cent from the offset having maximum value and make  net payment = 0

            if (dTempPayment < 0 && dTempPayment >= -0.01)
                dTempPayment = 0.0;
            if(dLocalGrossPayment < 0)
                dLocalGrossPayment = 0;
   
            //double dTotNetOffsets = dLocalGrossPayment;
            double dTotNetOffsets = dTempPayment;
            if(m_dtpTaxes.bFed)
            {
                if (dTaxablePercent > 0)
                {
                    dFederalTax = Math.Round((dLocalGrossPayment * dTaxablePercent * m_dtpTaxes.FED_TAX_RATE), 2, MidpointRounding.AwayFromZero); //MITS 28425 - mcapps2 05/17/2012
                    //dFederalTax = Math.Round(dLocalGrossPayment * dTaxablePercent * m_dtpTaxes.FED_TAX_RATE, 2); //MITS 28425 - mcapps2 05/17/2012
                }
                else
                {
                    dFederalTax = Math.Round((dLocalGrossPayment * m_dtpTaxes.FED_TAX_RATE), 2, MidpointRounding.AwayFromZero); //MITS 28425 - mcapps2 05/17/2012
                    //dFederalTax = Math.Round(dLocalGrossPayment * m_dtpTaxes.FED_TAX_RATE, 2); //MITS 28425 - mcapps2 05/17/2012
                }
            }
            else
                dFederalTax = 0;

            if (m_dtpTaxes.bSS)
            {
                if (dTaxablePercent > 0)
                {
                    dSocialSecurityAmount = Math.Round((dLocalGrossPayment * dTaxablePercent * m_dtpTaxes.SS_TAX_RATE), 2, MidpointRounding.AwayFromZero);
                    //dSocialSecurityAmount = Math.Round(dLocalGrossPayment * dTaxablePercent * m_dtpTaxes.SS_TAX_RATE, 2);
                }
                else
                {
                    dSocialSecurityAmount = Math.Round((dLocalGrossPayment * m_dtpTaxes.SS_TAX_RATE), 2, MidpointRounding.AwayFromZero);
                    //dSocialSecurityAmount = Math.Round(dLocalGrossPayment * m_dtpTaxes.SS_TAX_RATE, 2);
                }
            }
            else
                dSocialSecurityAmount = 0;

            if (m_dtpTaxes.bMed)
            {
                if (dTaxablePercent > 0)
                {
                    dMedicareAmount = Math.Round((dLocalGrossPayment * dTaxablePercent * m_dtpTaxes.MEDICARE_TAX_RATE), 2, MidpointRounding.AwayFromZero); //MITS 28425 - mcapps2 05/17/2012
                    //dMedicareAmount = Math.Round(dLocalGrossPayment * dTaxablePercent * m_dtpTaxes.MEDICARE_TAX_RATE, 2); //MITS 28425 - mcapps2 05/17/2012
                }
                else
                {
                    dMedicareAmount = Math.Round((dLocalGrossPayment * m_dtpTaxes.MEDICARE_TAX_RATE), 2, MidpointRounding.AwayFromZero); //MITS 28425 - mcapps2 05/17/2012
                    //dMedicareAmount =Math.Round( dLocalGrossPayment * m_dtpTaxes.MEDICARE_TAX_RATE ,2); //MITS 28425 - mcapps2 05/17/2012
                }
            }
            else
            {
                dMedicareAmount = 0;
            }
            if (m_dtpTaxes.bState)
            {
                if (dTaxablePercent > 0)
                {
                    dStateAmount = Math.Round((dLocalGrossPayment * dTaxablePercent * m_dtpTaxes.STATE_TAX_RATE), 2, MidpointRounding.AwayFromZero); //MITS 28425 - mcapps2 05/17/2012
                    //dStateAmount = Math.Round(dLocalGrossPayment * dTaxablePercent * m_dtpTaxes.STATE_TAX_RATE, 2);//MITS 28425 - mcapps2 05/17/2012
                }
                else
                {
                    dStateAmount = Math.Round((dLocalGrossPayment * m_dtpTaxes.STATE_TAX_RATE), 2, MidpointRounding.AwayFromZero); //MITS 28425 - mcapps2 05/17/2012
                    //dStateAmount = Math.Round(dLocalGrossPayment * m_dtpTaxes.STATE_TAX_RATE, 2); //MITS 28425 - mcapps2 05/17/2012
                }
            }
            else
            {
                dStateAmount = 0;
            }
            dTotalWithholding = Math.Round((dFederalTax + dSocialSecurityAmount + dMedicareAmount + dStateAmount), 2, MidpointRounding.AwayFromZero); //MITS 28425 - mcapps2 05/17/2012
            //dTotalWithholding = Math.Round((dFederalTax + dSocialSecurityAmount + dMedicareAmount + dStateAmount),2); //MITS 14186 //MITS 28425 - mcapps2 05/17/2012
            //pmittal5 Mits 14253 01/19/09
            //dNetAmount = dLocalGrossPayment - dTotalWithholding;
            dNetAmount = dTempPayment - dTotalWithholding;
            //Animesh Inserted for GHS Enhancment  //pmittal5 Mits 14841
            //Logic to deduct the Post Tax Deduction offset  Once the Taxes are calcualted and deducted
            dNetAmount = dNetAmount - dTotalPostTaxOffsetAmt;
            //commented by rkaur on 4/1/2009 
            //if (dNetAmount < 0)
            //{
            //    dNetAmount = 0;
            //}
            //Animesh Insertion ends
            CheckLimits(dNetAmount);

            //Adding Gross Total Net Offsets and Net Payment in Sysex
            XmlDocument objXML = base.SysEx;
            //pmittal5 Mits 14126 12/23/08
            //if (base.m_fda.SafeFormVariableParamText("SysCmd") != "7")
            if (base.m_fda.SafeFormVariableParamText("SysCmd") != "7" && base.m_fda.SafeFormVariableParamText("SysCmd") != "5")
            {
                XmlElement objElement = objXML.CreateElement("GrossTotalNetOffsets");
                //objElement.InnerText = Math.Round(dTotNetOffsets, 2).ToString();
                objElement.InnerText = dTotNetOffsets.ToString();
                objXML.DocumentElement.AppendChild(objElement);

                objElement = objXML.CreateElement("NetPayment");
                //objElement.InnerText = Math.Round(dNetAmount, 2).ToString();
                objElement.InnerText = dNetAmount.ToString();
                objXML.DocumentElement.AppendChild(objElement);

                objElement = objXML.CreateElement("FederalTax");
               // objElement.InnerText = Math.Round(dFederalTax, 2).ToString();
                objElement.InnerText = dFederalTax.ToString();
                objXML.DocumentElement.AppendChild(objElement);

                objElement = objXML.CreateElement("MedicareAmount");
                //objElement.InnerText = Math.Round(dMedicareAmount, 2).ToString();
                objElement.InnerText = dMedicareAmount.ToString();
                objXML.DocumentElement.AppendChild(objElement);

                objElement = objXML.CreateElement("SocialSecurityAmount");
               // objElement.InnerText = Math.Round(dSocialSecurityAmount, 2).ToString();
                objElement.InnerText = dSocialSecurityAmount.ToString();
                objXML.DocumentElement.AppendChild(objElement);

                objElement = objXML.CreateElement("StateAmount");
                //objElement.InnerText = Math.Round(dStateAmount, 2).ToString();
                objElement.InnerText = dStateAmount.ToString();
                objXML.DocumentElement.AppendChild(objElement);

                objElement = objXML.CreateElement("dPension");
                //objElement.InnerText = Math.Round(dPension, 2).ToString();
                objElement.InnerText = dPension.ToString();
                objXML.DocumentElement.AppendChild(objElement);

                objElement = objXML.CreateElement("dSS");
               // objElement.InnerText = Math.Round(dSS, 2).ToString();
                objElement.InnerText = dSS.ToString();
                objXML.DocumentElement.AppendChild(objElement);

                objElement = objXML.CreateElement("dOther");
               // objElement.InnerText = Math.Round(dOther, 2).ToString();
                objElement.InnerText = dOther.ToString();
                objXML.DocumentElement.AppendChild(objElement);

                objElement = objXML.CreateElement("iOffsetCalc");
                objElement.InnerText = iOffsetCalc.ToString();
                objXML.DocumentElement.AppendChild(objElement);

                objElement = objXML.CreateElement("ilblDaysIncluded");
                objElement.InnerText = ilblDaysIncluded.ToString();
                objXML.DocumentElement.AppendChild(objElement);

                //Animesh Inserted for GHS Enhancment  //pmittal5 Mits 14841
                objElement = objXML.CreateElement("dOtherOffset1");
                objElement.InnerText = dOthOffset1.ToString();
                objXML.DocumentElement.AppendChild(objElement);
                objElement = objXML.CreateElement("dOtherOffset2");
                objElement.InnerText = dOthOffset2.ToString();
                objXML.DocumentElement.AppendChild(objElement);
                objElement = objXML.CreateElement("dOtherOffset3");
                objElement.InnerText = dOthOffset3.ToString();
                objXML.DocumentElement.AppendChild(objElement);
                objElement = objXML.CreateElement("dPostTaxDed1");
                objElement.InnerText = dPostTaxDed1.ToString();
                objXML.DocumentElement.AppendChild(objElement);
                objElement = objXML.CreateElement("dPostTaxDed2");
                objElement.InnerText = dPostTaxDed2.ToString();
                objXML.DocumentElement.AppendChild(objElement);
                //Animesh Insertion Ends 
            }
            else
            { 
                base.ResetSysExData("GrossTotalNetOffsets", Math.Round(dTotNetOffsets, 4, MidpointRounding.AwayFromZero).ToString());
                base.ResetSysExData("NetPayment", Math.Round(dNetAmount, 4, MidpointRounding.AwayFromZero).ToString());
                base.ResetSysExData("FederalTax", Math.Round(dFederalTax, 4, MidpointRounding.AwayFromZero).ToString());
                base.ResetSysExData("MedicareAmount", Math.Round(dMedicareAmount, 4, MidpointRounding.AwayFromZero).ToString());
                base.ResetSysExData("SocialSecurityAmount", Math.Round(dSocialSecurityAmount, 4, MidpointRounding.AwayFromZero).ToString());
                base.ResetSysExData("StateAmount", Math.Round(dStateAmount, 4, MidpointRounding.AwayFromZero).ToString());
                base.ResetSysExData("dPension", Math.Round(dPension, 4, MidpointRounding.AwayFromZero).ToString());
                base.ResetSysExData("dSS", Math.Round(dSS, 4, MidpointRounding.AwayFromZero).ToString());
                base.ResetSysExData("dOther", Math.Round(dOther, 4, MidpointRounding.AwayFromZero).ToString());
                base.ResetSysExData("iOffsetCalc", iOffsetCalc.ToString());
                base.ResetSysExData("ilblDaysIncluded", ilblDaysIncluded.ToString());
                //Animesh Inserted For GHS Enhancment  //pmittal5 Mits 14841
                base.ResetSysExData("dOtherOffset1", Math.Round(dOthOffset1, 4, MidpointRounding.AwayFromZero).ToString());
                base.ResetSysExData("dOtherOffset2", Math.Round(dOthOffset2, 4, MidpointRounding.AwayFromZero).ToString());
                base.ResetSysExData("dOtherOffset3", Math.Round(dOthOffset3, 4, MidpointRounding.AwayFromZero).ToString());
                base.ResetSysExData("dPostTaxDed1", Math.Round(dPostTaxDed1, 4, MidpointRounding.AwayFromZero).ToString());
                base.ResetSysExData("dPostTaxDed2", Math.Round(dPostTaxDed2, 4, MidpointRounding.AwayFromZero).ToString());
                //Animesh Insertion Ends
            }
            if (!m_bEligible)
            {
                base.ResetSysExData("GrossTotalNetOffsets", "0");
                base.ResetSysExData("NetPayment", "0");
                base.ResetSysExData("FederalTax", "0");
                base.ResetSysExData("MedicareAmount", "0");
                base.ResetSysExData("SocialSecurityAmount", "0");
                base.ResetSysExData("StateAmount", "0");
                base.ResetSysExData("dPension", "0");
                base.ResetSysExData("dSS", "0");
                base.ResetSysExData("dOther", "0");
                base.ResetSysExData("iOffsetCalc", "0");
                base.ResetSysExData("ilblDaysIncluded", "0");
                //Animesh Inserted For GHS Enhancment //pmittal5 Mits 14841
                base.ResetSysExData("dOtherOffset1", "0");
                base.ResetSysExData("dOtherOffset2", "0");
                base.ResetSysExData("dOtherOffset3", "0");
                base.ResetSysExData("dPostTaxDed1", "0");
                base.ResetSysExData("dPostTaxDed2", "0");
                //Animesh Insertion Ends
            }

        }

        double CheckLimits(double dNetAmount)
        {
            foreach (DisabilityClass objClass in objClaim.DisabilityPlan.ClassList)
            {
                if (objClass.ClassRowId == objClaim.ClassRowId)
                {
                    if (objClaim.DisabilityPlan.PrefPaySchCode == 1737) //And chkFirstWeek.Value Then
                    {
                        if ((dNetAmount > objClass.WeeklyBeneCap) && (objClass.WeeklyBeneCap != 0))
                        {
                            dNetAmount = objClass.WeeklyBeneCap * 2;
                            return dNetAmount;
                        }
                    }
                    else
                    {
                        if ((dNetAmount > objClass.WeeklyBeneCap) && (objClass.WeeklyBeneCap != 0))
                        {
                            dNetAmount = objClass.WeeklyBeneCap;
                            return dNetAmount;
                        }
                    }
                }
            }
            return dNetAmount;
        }
   
        //pmittal5 Mits 16284 - Disable all form fields when error is thrown.
        private void DisableFormFields()
        {
            base.AddReadOnlyNode("calculatepaym");
            base.AddReadOnlyNode("futurepaym");
            base.AddReadOnlyNode("printedpaym");
            base.AddReadOnlyNode("collection");
            base.AddReadOnlyNode("benstartdate");
            base.AddReadOnlyNode("benthroughdate");
            base.AddReadOnlyNode("paymentsstartdate");
            base.AddReadOnlyNode("paymentsthroughdate");
            base.AddReadOnlyNode("chkfederaltax");
            base.AddReadOnlyNode("chksstax");
            base.AddReadOnlyNode("chkmedtax");
            base.AddReadOnlyNode("chkstatetax");
            base.AddReadOnlyNode("transactiontypecode");
            base.AddReadOnlyNode("bankaccount");
            base.AddReadOnlyNode("pensionoffset");
            base.AddReadOnlyNode("socialsecurityoffset");
            base.AddReadOnlyNode("otherincomeoffset");
            base.AddReadOnlyNode("OtherOffset1");
            base.AddReadOnlyNode("OtherOffset2");
            base.AddReadOnlyNode("OtherOffset3");
            base.AddReadOnlyNode("PostTaxDed1");
            base.AddReadOnlyNode("PostTaxDed2");
        }//End - pmittal5
    
        #endregion
	}
}
