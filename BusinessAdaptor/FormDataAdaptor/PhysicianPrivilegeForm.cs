﻿
using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

//TODO: Auto-Generated Form - migrate and verify behaviors from Legacy RMNet form.

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PhysicianPrivilege Screen.
	/// </summary>
	public class PhysicianPrivilegeForm : DataEntryFormBase
	{
		const string CLASS_NAME = "PhysicianPrivilege";
		private PhysicianPrivilege PhysicianPrivilege{get{return objData as PhysicianPrivilege;}}
		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		const string FILTER_KEY_NAME = "PhysEid";
		
		/// <summary>
		/// Class constructor which initializes Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public PhysicianPrivilegeForm(FormDataAdaptor fda):base(fda)
		{
			this.m_ClassName = CLASS_NAME;
		}

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();

			//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
			//originally from ScreenFlow\SysExData.
			XmlNode objPhysEid=null;

			try
			{
				objPhysEid = base.SysEx.GetElementsByTagName(FILTER_KEY_NAME)[0];
			}//end try
			catch
			{
			}; //end catch
			if(objPhysEid !=null)
				this.m_ParentId = Conversion.ConvertObjToInt(objPhysEid.InnerText, base.ClientId);

		
			PhysicianPrivilege.PhysEid = this.m_ParentId;

			//Filter by this PhysEid if Present.
			if(this.m_ParentId != 0)
			{
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + this.m_ParentId;
			}
			
		}//end method InitNew()

        // npadhy MITS 14200 Validation for Privilege Start Date and Privilege End Date as Per RMWorld
        public override void OnValidate(ref bool Cancel)
        {
            bool bError = false;            
            if (!string.IsNullOrEmpty(PhysicianPrivilege.IntDate) && !(string.IsNullOrEmpty(PhysicianPrivilege.EndDate)))
            {
                if (PhysicianPrivilege.IntDate.CompareTo(PhysicianPrivilege.EndDate) > 0)
                {
                    //Errors.Add(Globalization.GetString("ValidationError"),
                    //        String.Format(Globalization.GetString("Validation.MustBeGreaterThan"), Globalization.GetString("Field.EndDate"), Conversion.ToDate(PhysicianPrivilege.IntDate).ToShortDateString() + "(" + Globalization.GetString("Field.StartDate") + ")"),
                    //        BusinessAdaptorErrorType.Error);
                    //Aman MITS 31122
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("StartDateMustlessthanEndDate", base.ClientId), BusinessAdaptorErrorType.Error);
                    bError = true;
                }
            }

            // Return true if there were validation errors
            Cancel = bError;
        }
	
		
		//Handle Extended Screen Elements not directly applicable to database fields\datamodel properties.
		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
		}
	}
}
