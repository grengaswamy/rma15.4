﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Application.FundManagement;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// Deductible Form to make the Dedcutible pages into FDM
    /// </summary>
    public class DeductibleForm : DataEntryFormBase
    {
        /// <summary>
        /// Data Model Class Name
        /// </summary>
        const string CLASS_NAME = "ClaimXPolDed";
        
        /// <summary>
        /// Return the instance of the datamodel object
        /// </summary>
        private ClaimXPolDed ClaimXPolDed { get { return objData as ClaimXPolDed; } }
        private LocalCache objCache { get { return objData.Context.LocalCache; } }
        const string FILTER_KEY_NAME = "PolcvgRowId";
        public override void Init()
        {
            XmlDocument objXML = base.SysEx;
            XmlNode objPolcvgRowId = null;
            try { objPolcvgRowId = objXML.GetElementsByTagName(FILTER_KEY_NAME)[0]; }
            catch { };

            this.m_ParentId = Conversion.ConvertStrToInteger(objPolcvgRowId.InnerText);
        }

        public DeductibleForm(FormDataAdaptor fda)
            : base(fda)
		{
			base.m_ClassName = CLASS_NAME;
            this.objData = fda.Factory.GetDataModelObject(CLASS_NAME, false) as DataObject;
		}

        public override bool Validate()
        {
            //return base.Validate();
            int iCovGroupId = 0;

            double dOrigDedSirAmt = 0d;
            double dOrigDimPercentNum = 0d;
            double dGroupAggrLimit = 0d;
            bool bError = false;
            bool bIsDiminishingTypeValid = false;
            bool bIsReserveUpdateSuccessful = false;
            bool bBaseClassValidation = false;

            string sSql = string.Empty;

            iCovGroupId = base.GetSysExDataNodeInt("coveragegroupid");

            if (ClaimXPolDed.SirDedAmt < 0)
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    Globalization.GetString("DeductibleManager.AmountLessThanZero.Error", base.ClientId), BusinessAdaptorErrorType.Error);

                bError = true;

            }

            sSql = string.Format("SELECT SIR_DED_AMT, DIM_PERCENT_NUM FROM CLAIM_X_POL_DED WHERE CLM_X_POL_DED_ID = {0}", ClaimXPolDed.ClmXPolDedId);
            using (DbReader getOrigValues = ClaimXPolDed.Context.DbConn.ExecuteReader(sSql))
            {
                if (getOrigValues.Read())
                {
                    dOrigDedSirAmt = getOrigValues.GetDouble("SIR_DED_AMT");
                    dOrigDimPercentNum = getOrigValues.GetDouble("DIM_PERCENT_NUM");
                }
            }

            if (ClaimXPolDed.SirDedAmt != dOrigDedSirAmt && iCovGroupId > 0)
            {
                sSql = string.Format("SELECT AGG_LIMIT_AMT FROM CLAIM_X_POL_DED_AGG_LIMIT WHERE COV_GROUP_CODE = {0}", iCovGroupId);
                dGroupAggrLimit = ClaimXPolDed.Context.DbConn.ExecuteDouble(sSql);

                if (dGroupAggrLimit > 0 && ClaimXPolDed.SirDedAmt > dGroupAggrLimit)
                {
                    Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    Globalization.GetString("DeductibleManager.DedAmountLimit.Error", base.ClientId), BusinessAdaptorErrorType.Error);

                    bError = true;
                }
            }

            // Logic for DiminishingTypeValid

            bIsDiminishingTypeValid = IsDiminishingTypeValid();
            if (!bIsDiminishingTypeValid)
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    Globalization.GetString("DeductibleManager.InvalidDiminishingType", base.ClientId), BusinessAdaptorErrorType.Error);
                bError = true;
            }


            bIsReserveUpdateSuccessful = CheckIfReserveUpdateIsSuccessful(dOrigDedSirAmt, dOrigDimPercentNum);
            if (!bIsReserveUpdateSuccessful)
                bError = true;

            if (ClaimXPolDed.DedTypeCode != ClaimXPolDed.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE") && ClaimXPolDed.DiminishingTypeCode > 0)
            {
                Errors.Add(Globalization.GetString("ValidationError", base.ClientId),
                    Globalization.GetString("DiminishingDeductible.NonFirstParty.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                bError = true;
            }

            bBaseClassValidation = base.Validate();

            if(!bBaseClassValidation)
                bError = true;
            return !bError;
           
        }

        private bool IsDiminishingTypeValid()
        {
            string sSql = string.Empty;
            int iBasicDim = 0;
            int iEnhancedDim = 0;
            int iDimTypeToCheck = 0;
            int iCount = 0;

            iBasicDim = ClaimXPolDed.Context.LocalCache.GetCodeId("B", "DIMINISHING_TYPE");
            iEnhancedDim = ClaimXPolDed.Context.LocalCache.GetCodeId("E", "DIMINISHING_TYPE");

            if (ClaimXPolDed.DiminishingTypeCode == iBasicDim)
            {
                iDimTypeToCheck = iEnhancedDim;
            }
            else if (ClaimXPolDed.DiminishingTypeCode == iEnhancedDim)
            {
                iDimTypeToCheck = iBasicDim;
            }
            else
            {
                return true;
            }

            sSql = String.Format("SELECT COUNT(*) FROM CLAIM_X_POL_DED WHERE DIMNSHNG_TYPE_CODE = {0} AND CLAIM_ID = {1} AND POLICY_UNIT_ROW_ID = {2}", iDimTypeToCheck, ClaimXPolDed.ClaimId, ClaimXPolDed.UnitRowId);

            iCount = ClaimXPolDed.Context.DbConn.ExecuteInt(sSql);

            if (iCount > 0)
            {
                return false;
            }

            return true;
        }
        public override void OnUpdateObject()
        {
            base.OnUpdateObject();
            string sPostBackAction = string.Empty;

            int iPrevDiminishingType = 0;
            int iFPTypeCode = 0;

            sPostBackAction = base.GetSysExDataNodeText("\\PostBackAction");
            iFPTypeCode = ClaimXPolDed.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE");
            iPrevDiminishingType = base.GetSysExDataNodeInt("\\DiminishingTypeCode");

            switch (sPostBackAction)
            {
                case "LoadDeductibleData":
                    if (ClaimXPolDed.DedTypeCode == iFPTypeCode)
                    {
                        ClaimXPolDed.DiminishingTypeCode = iPrevDiminishingType;
                    }
                    
                    break;
            }
        }

        public override void OnUpdateForm()
        {
            int isPayment = 0;
            int iFPTypeCode = 0;
            int iCovGroupId = 0;
            int iTPTypeCode = 0;
            int iBaseCurrCode = 0;
            int iPreventEditDedPerEvent = 0;

            Double dRemainingAmt = 0d;
            Double dReducedAmt = 0d;

            bool bIsDedTypEditAllowed = false;
            bool bIsDedAmtEditAllowed = false;
            bool bIsDimTypEditAllowed = false;
            bool bIsDimPerEditAllowed = false;
            bool bCoverageGroupExists = false;

            string sUnit = string.Empty;
            string sDiminishingEvalDate = string.Empty;
            string sCoverageGroup = string.Empty;
            string sBaseCurr = string.Empty;
            string sSharedAggDedThirdParty = string.Empty;
            string sDiminishingFlag = string.Empty;
            string sPostBackAction = string.Empty;
              double dCvgSirDedAmt = 0d;
            StringBuilder sbSql = null;
            string sDBType = string.Empty;//skhare7 JIRA 14056
            DbReader objReader = null;
            DeductibleManager objDeductibleManager = null;
            Claim objClaim = null;
            base.OnUpdateForm();

            sbSql = new StringBuilder();
            objDeductibleManager = new DeductibleManager(ClaimXPolDed.Context.DbConn.ConnectionString, base.Adaptor.userLogin, base.ClientId);
            objClaim = m_fda.Factory.GetDataModelObject("Claim", false) as Claim;
            //skhare7 JIRA 14056 start
            sDBType = m_fda.Factory.Context.DbConn.DatabaseType.ToString();
            if (sDBType == Constants.DB_SQLSRVR)
            {
                sbSql.Append("SELECT P.POLICY_SYMBOL+'|'+P.POLICY_NUMBER+'|'+P.MODULE AS POLICY_NAME, C.CLAIM_NUMBER, CVG.DED_TYPE_CODE, CVG.SELF_INSURE_DEDUCT, CVG.POLCVG_ROW_ID, CVG.COVERAGE_TEXT, CVG.COVERAGE_TYPE_CODE, PXU.UNIT_ID, PXU.UNIT_TYPE, PUD.INS_LINE ");
                sbSql.Append(", CASE PXU.UNIT_TYPE WHEN 'V' THEN (SELECT 'Vehicle Unit: ' + COALESCE(V.VIN , '') FROM VEHICLE V WHERE V.UNIT_ID = PXU.UNIT_ID)");
                sbSql.Append(" WHEN 'SU' THEN (SELECT 'Stat Unit: ' + COALESCE(E.LAST_NAME, '') FROM ENTITY E INNER JOIN OTHER_UNIT O ON E.ENTITY_ID = O.ENTITY_ID WHERE O.OTHER_UNIT_ID = PXU.UNIT_ID)");
                sbSql.Append(" WHEN 'P' THEN (SELECT 'Property Unit: ' + Coalesce(P.PIN , '') FROM PROPERTY_UNIT P WHERE P.PROPERTY_ID = PXU.UNIT_ID)");
                sbSql.Append(" WHEN 'S' THEN (SELECT 'Site Unit: ' + Coalesce(S.SITE_NUMBER , '') FROM SITE_UNIT S WHERE S.SITE_ID = PXU.UNIT_ID)");
            }
            else if (sDBType == Constants.DB_ORACLE)
            {

                sbSql.Append("SELECT P.POLICY_SYMBOL || '|' || P.POLICY_NUMBER || '|' || P.MODULE AS POLICY_NAME, C.CLAIM_NUMBER, CVG.DED_TYPE_CODE, CVG.SELF_INSURE_DEDUCT, CVG.POLCVG_ROW_ID, CVG.COVERAGE_TEXT, CVG.COVERAGE_TYPE_CODE, PXU.UNIT_ID, PXU.UNIT_TYPE, PUD.INS_LINE ");
                sbSql.Append(", CASE PXU.UNIT_TYPE WHEN 'V' THEN (SELECT 'Vehicle Unit: ' || COALESCE(V.VIN , '') FROM VEHICLE V WHERE V.UNIT_ID = PXU.UNIT_ID)");
                sbSql.Append(" WHEN 'SU' THEN (SELECT 'Stat Unit: ' || COALESCE(E.LAST_NAME, '') FROM ENTITY E INNER JOIN OTHER_UNIT O ON E.ENTITY_ID = O.ENTITY_ID WHERE O.OTHER_UNIT_ID = PXU.UNIT_ID)");
                sbSql.Append(" WHEN 'P' THEN (SELECT 'Property Unit: ' || Coalesce(P.PIN , '') FROM PROPERTY_UNIT P WHERE P.PROPERTY_ID = PXU.UNIT_ID)");
                sbSql.Append(" WHEN 'S' THEN (SELECT 'Site Unit: ' || Coalesce(S.SITE_NUMBER , '') FROM SITE_UNIT S WHERE S.SITE_ID = PXU.UNIT_ID)");
            
            }
            //skhare7 JIRA 14056 end
            sbSql.Append(" END AS UNIT_NAME ");
            sbSql.Append("FROM POLICY_X_CVG_TYPE CVG ");
            sbSql.Append("INNER JOIN POLICY_X_UNIT PXU ON CVG.POLICY_UNIT_ROW_ID = PXU.POLICY_UNIT_ROW_ID ");
            sbSql.Append("INNER JOIN POLICY P ON PXU.POLICY_ID = P.POLICY_ID ");
            sbSql.Append("INNER JOIN CLAIM_X_POLICY CXP ON P.POLICY_ID = CXP.POLICY_ID ");
            sbSql.Append("INNER JOIN CLAIM C ON C.CLAIM_ID = CXP.CLAIM_ID ");
            sbSql.Append("INNER JOIN POINT_UNIT_DATA PUD ON PUD.UNIT_ID=PXU.UNIT_ID AND PUD.UNIT_TYPE = PXU.UNIT_TYPE ");
            sbSql.Append("WHERE CVG.POLCVG_ROW_ID = ");
            sbSql.Append(ClaimXPolDed.PolcvgRowId);

            // TODO write query for Oracle
            using (objReader = DbFactory.GetDbReader(ClaimXPolDed.Context.DbConn.ConnectionString, sbSql.ToString()))
            {

                if (objReader.Read())
                {
                    base.ResetSysExData("UnitName", objReader["UNIT_NAME"].ToString());
                    base.ResetSysExData("ClaimNumber", objReader["CLAIM_NUMBER"].ToString());
                    base.ResetSysExData("PolicyName", objReader["POLICY_NAME"].ToString());
                    base.ResetSysExData("CoverageText", objReader["COVERAGE_TEXT"].ToString());
                    base.ResetSysExData("InsuranceLine", objReader["INS_LINE"].ToString());
                }
            }
            base.AddReadOnlyNode("dimreducedamount");
            base.AddReadOnlyNode("remainingamount");
            base.AddReadOnlyNode("policylob");
            if (ClaimXPolDed.DiminishingTypeCode > 0)
            {
                sDiminishingEvalDate = objDeductibleManager.GetEvaluationDate(ClaimXPolDed.ClaimId, ClaimXPolDed.UnitRowId,
                    ClaimXPolDed.DiminishingTypeCode, ClaimXPolDed.DimEvaluationDate);

                if (!string.IsNullOrEmpty(sDiminishingEvalDate) && ClaimXPolDed.DedTypeCode != iFPTypeCode)
                {
                    base.ResetSysExData("DiminishingEvaluationDate", sDiminishingEvalDate);
                }
                else
                {
                    base.ResetSysExData("DiminishingEvaluationDate", string.Empty);
                }
            }

            iFPTypeCode = ClaimXPolDed.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE");
            iCovGroupId = DbFactory.ExecuteAsType<int>(ClaimXPolDed.Context.DbConnLookup.ConnectionString, "SELECT COV_GROUP_CODE FROM POLICY_X_INSLINE_GROUP WHERE POLICY_X_INSLINE_GROUP_ID=" + ClaimXPolDed.PolicyXInslineGroupId);
            if (ClaimXPolDed.DedTypeCode == iFPTypeCode)
            {
                sCoverageGroup = "NA";
            }
            else
            {
                sCoverageGroup = objDeductibleManager.GetCovGroupInfo(iCovGroupId.ToString());
                if (sCoverageGroup.Trim() != string.Empty)
                {
                    bCoverageGroupExists = true;
                }
            }
            base.ResetSysExData("CoverageGroup", sCoverageGroup);
            base.ResetSysExData("CoverageGroupId", iCovGroupId.ToString());

            base.ResetSysExData("FirstParty", iFPTypeCode.ToString());
            
            iBaseCurrCode = ClaimXPolDed.Context.InternalSettings.SysSettings.BaseCurrencyType;
            sBaseCurr = ClaimXPolDed.Context.DbConnLookup.ExecuteString("SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode.ToString());

            base.ResetSysExData("BaseCurrencyType", sBaseCurr.ToString().Split('|')[1]);
            iTPTypeCode = ClaimXPolDed.Context.LocalCache.GetCodeId("TP", "DEDUCTIBLE_TYPE");
            base.ResetSysExData("ThirdParty", iTPTypeCode.ToString());

            dReducedAmt = ClaimXPolDed.CalculateReducedDeductibleAmount(ClaimXPolDed.ClaimId, ClaimXPolDed.DiminishingTypeCode,
                ClaimXPolDed.DedTypeCode, ClaimXPolDed.SirDedAmt,
                ClaimXPolDed.DimPercentNum, ClaimXPolDed.ClmXPolDedId);

            if (ClaimXPolDed.DiminishingTypeCode != 0)
                base.ResetSysExData("ReducedAmt", String.Format("{0:c}", dReducedAmt));
            else
                base.ResetSysExData("ReducedAmt", "NA");

            //Different method for calculating remaining amount when coverage group is to be applied or otherwise
            if (iCovGroupId == 0 || ClaimXPolDed.DedTypeCode == 0 || ClaimXPolDed.DedTypeCode == iFPTypeCode)
            {
                dRemainingAmt = ClaimXPolDed.CalculateRemainingAmount(dReducedAmt, ClaimXPolDed.ClaimId, ClaimXPolDed.PolcvgRowId, ClaimXPolDed.DedTypeCode);
            }
            else
            {
                iPreventEditDedPerEvent = DbFactory.ExecuteAsType<int>(ClaimXPolDed.Context.DbConn.ConnectionString, "SELECT USE_SHARED_DED_FLAG FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID= " + iCovGroupId);
                if (iPreventEditDedPerEvent == 0)
                    dRemainingAmt = ClaimXPolDed.CalculateRemainingAmount(dReducedAmt, ClaimXPolDed.ClaimId, ClaimXPolDed.PolcvgRowId, ClaimXPolDed.DedTypeCode, iCovGroupId, false);
                else
                {
                    dRemainingAmt = ClaimXPolDed.CalculateRemainingAmount(dReducedAmt, ClaimXPolDed.ClaimId, iCovGroupId, false);
                    dCvgSirDedAmt = DbFactory.ExecuteAsType<double>(ClaimXPolDed.Context.DbConn.ConnectionString, "SELECT SIR_DED_PEREVENT_AMT FROM CLAIM_X_POL_DED_AGG_LIMIT WHERE COV_GROUP_CODE=" + iCovGroupId);
                }
            }
            base.ResetSysExData("RemainingAmt", dRemainingAmt.ToString());


            objClaim.MoveTo(ClaimXPolDed.ClaimId);
            switch (objClaim.Context.LocalCache.GetShortCode(objClaim.LineOfBusCode))
            {
                case "GC":
                    //SMS Settings for deductible fields for GC
                    bIsDedTypEditAllowed = base.Adaptor.userLogin.IsAllowedEx(GC_DED, GC_DED_TYP_EDIT);
                    bIsDedAmtEditAllowed = base.Adaptor.userLogin.IsAllowedEx(GC_DED, GC_DED_AMT_EDIT);
                    bIsDimTypEditAllowed = base.Adaptor.userLogin.IsAllowedEx(GC_DED, GC_DIM_TYP_EDIT);
                    bIsDimPerEditAllowed = base.Adaptor.userLogin.IsAllowedEx(GC_DED, GC_DIM_PER_EDIT);
                    m_SecurityId = GC_DED_SUPP;//skhare7 JIRAa 14055
                    //End ddhiman
                    break;
                case "WC":
                    //SMS Settings for deductible fields for WC
                    bIsDedTypEditAllowed = base.Adaptor.userLogin.IsAllowedEx(WC_DED, WC_DED_TYP_EDIT);
                    bIsDedAmtEditAllowed = base.Adaptor.userLogin.IsAllowedEx(WC_DED, WC_DED_AMT_EDIT);
                    bIsDimTypEditAllowed = base.Adaptor.userLogin.IsAllowedEx(WC_DED, WC_DIM_TYP_EDIT);//skhare7 variable chnged
                    bIsDimPerEditAllowed = base.Adaptor.userLogin.IsAllowedEx(WC_DED, WC_DIM_PER_EDIT);
                    m_SecurityId = WC_DED_SUPP;//skhare7 JIRAa 14055
                    break;
            }
            if (bIsDedTypEditAllowed)
                base.AddReadWriteNode("deductibletype");
            else
                base.AddReadOnlyNode("deductibletype");

            // If the Permission to Edit the DeductibleAmount and Deductible is not Event based then only we allow it to be edited
            if (bIsDedAmtEditAllowed && iPreventEditDedPerEvent == 0)
                base.AddReadWriteNode("dedsiramount");
            else
                base.AddReadOnlyNode("dedsiramount");

            // As per the current functionality, Diminishing type can be edited only if Deductible Type is editable.
            // So for a user to edit Dimishing Type, he/she should have rights to edit both Deductible and Dminishing Type
            if (bIsDimTypEditAllowed && bIsDedTypEditAllowed)
                base.AddReadWriteNode("diminishingtype");
            else
                base.AddReadOnlyNode("diminishingtype");

            if (bIsDimPerEditAllowed && ClaimXPolDed.DiminishingTypeCode != 0)
                base.AddReadWriteNode("diminishingpercent");
            else
                base.AddReadOnlyNode("diminishingpercent");

            if (bCoverageGroupExists)
                base.AddReadOnlyNode("excludeexpensepayments");
            else
                base.AddReadWriteNode("excludeexpensepayments");

            base.ResetSysExData("IsDedPerEventEnabled", Convert.ToString(iPreventEditDedPerEvent));
            sSharedAggDedThirdParty = objClaim.Context.InternalSettings.ColLobSettings[objDeductibleManager.GetLOB(objClaim.ClaimId)].SharedAggDedFlag.ToString();
            sDiminishingFlag = objClaim.Context.InternalSettings.ColLobSettings[objDeductibleManager.GetLOB(objClaim.ClaimId)].DimnishingFlag.ToString();

            base.ResetSysExData("SharedAggDedFlag", sSharedAggDedThirdParty);
            base.ResetSysExData("DiminishingFlag", sDiminishingFlag);

            if (sSharedAggDedThirdParty != "-1")
            {
                base.AddKillNode("coveragegroup");
            }
            if (sDiminishingFlag != "-1")
            {
                base.AddKillNode("diminishingtype");
                base.AddKillNode("diminishingpercent");
                base.AddKillNode("cvgevaluationdate");
                base.AddKillNode("dimevaluationdate");
            }

            // Reset all the Previous controls. As save has happened or for initial load these controls should be blank
            base.ResetSysExData("PrevDiminishingTypeCode", string.Empty);
            base.ResetSysExData("PrevDiminishingTypeCodeDesc", string.Empty);
            base.ResetSysExData("PrevDiminishingPercent", string.Empty);
            base.ResetSysExData("PrevCvgEvaluationDate", string.Empty);
            base.ResetSysExData("PrevDimEvaluationDate", string.Empty);

        }


        private bool CheckIfReserveUpdateIsSuccessful(double dOrigSirAmt, double dOrigDimPercentNum)
        {
            DeductibleManager objDeductibleManager = null;
            bool bSuccess = true;

            objDeductibleManager = new DeductibleManager(ClaimXPolDed.Context.DbConn.ConnectionString, base.Adaptor.userLogin, base.ClientId);

            if (ClaimXPolDed.SirDedAmt != dOrigSirAmt || ClaimXPolDed.DimPercentNum != dOrigDimPercentNum)
            {
                bSuccess = objDeductibleManager.ModifyRecoveryreserves(ClaimXPolDed.SirDedAmt, ClaimXPolDed.ClaimId, ClaimXPolDed.PolcvgRowId, ClaimXPolDed.PolicyId);
            }
            return bSuccess;
        }


        
    }
}
