using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Event Medwatch
	/// </summary>
	public class MedWatchForm : DataEntryFormBase
	{
		const string CLASS_NAME = "EventMedwatch";
		const string FILTER_KEY_NAME = "EventId"; 
		private const int RMO_EVENT_MEDWATCH_PRINT = 30;
		private const int RMO_EVENT_MEDWATCH_TEST = 12200;
		private const int RMO_EVENT_MEDWATCH_CONCOMITANT = 12350;

		private EventMedwatch EventMedwatch{get{return objData as EventMedwatch;}}

		private LocalCache objCache{get{return objData.Context.LocalCache;}}
		
		/// <summary>
		/// Class constructor to initialize Class Name information
		/// </summary>
		/// <param name="fda"></param>
		public MedWatchForm(FormDataAdaptor fda):base(fda)
		{
			base.m_ClassName = CLASS_NAME;
		}

		/// <summary>
		/// Initializes the class and sets the parent ID for the class
		/// that can be used for parent-child navigation
		/// </summary>
		public override void InitNew()
		{
			base.InitNew();

			//set filter for EventId
			EventMedwatch.EventId=base.GetSysExDataNodeInt("/SysExData/" + FILTER_KEY_NAME,false);
			if (EventMedwatch.EventId>0)
				(objData as INavigation).Filter = objData.PropertyNameToField(FILTER_KEY_NAME) + "=" + EventMedwatch.EventId;

			this.m_ParentId=EventMedwatch.EventId;

		}//end method InitNew()

		// BSB InitNew on 1:1 case causes navigation to be aborted if we slap in the key
		// of the parent because the value is shared as the key of the current record.
		// This makes navigation silently stop becuase the assumption is that you are already
		// looking at the record data you've requested.  As a work-around for only the 
		// 1:1 Cases let's zero this back out before the navigation call.
		public override void BeforeAction(enumFormActionType eActionType, ref bool Cancel)
		{
			base.BeforeAction (eActionType, ref Cancel);
			switch(eActionType)
			{
				case enumFormActionType.MoveFirst:
				case enumFormActionType.MoveLast:
				case enumFormActionType.MoveTo:
					EventMedwatch.EventId=0;
					break;
			}
		}	
		
		//BSB 1:1 Hack Let's throw the proper parent key back on in case the MoveFirst\Last request
		// didn't find any records (we want to be sure the eventId stays set when creating a new
		// record.
		public override void AfterAction(enumFormActionType eActionType)
		{
			base.AfterAction(eActionType);
			switch(eActionType)
			{
				case enumFormActionType.MoveFirst:
				case enumFormActionType.MoveLast:
				case enumFormActionType.MoveTo:
					if(EventMedwatch.EventId==0) //No record found - treat as new.
						EventMedwatch.EventId = this.m_ParentId;
					break;
			}
		}

		public override void BeforeApplyLookupData(ref bool Cancel, ArrayList arrToSaveFields)
		{
			string sAttachNodePath=base.SysLookup.SelectSingleNode("/SysLookup/SysLookupAttachNodePath").InnerText;
			if (sAttachNodePath.IndexOf("ManufacturerEntity") > 0 )
				arrToSaveFields.Add("EntityTableId");
		}

		public override void OnUpdateForm()
		{
			base.OnUpdateForm ();
			//TR #2299,2303 Pankaj 02/28/06 medwatch security
			if(base.ModuleSecurityEnabled)//Is Security Enabled
			{
				if(!this.objData.Context.RMUser.IsAllowedEx(this.SecurityId,RMO_EVENT_MEDWATCH_PRINT))
					base.AddKillNode("btnPrintMedwatch");

				if(!this.objData.Context.RMUser.IsAllowedEx(RMO_EVENT_MEDWATCH_TEST))
					base.AddKillNode("btnMedwatchTest");

				if(!this.objData.Context.RMUser.IsAllowedEx(RMO_EVENT_MEDWATCH_CONCOMITANT))
					base.AddKillNode("btnConcomProd");

//				//Check for People Maintenance permission
//				if( !Adaptor.userLogin.IsAllowed(RMO_PEOPLE+RMO_CREATE) )
//				{
//					XmlNode objCreatableNode = base.SysView.SelectSingleNode("//form[@name='medwatch']//control[@creatable='1']");
//					if(objCreatableNode != null )
//					{
//						objCreatableNode.Attributes.RemoveNamedItem("creatable");
//					}
//				}

				//Check for People Maintenance permission
				PeoplePermissionChecks4Other("medwatch", m_SecurityId+RMO_UPDATE);

			}
		}

//				// BSB Hack due to unique 1:1 addition situation where this child is being created on the fly.
//				public override void BeforeSave(ref bool Cancel)
//				{
//					//If we were sent here from a parent(event) then we'll have an eventid in the SysEx
//					//originally from ScreenFlow\SysExData.
//					XmlDocument objXML = base.SysEx;
//					XmlNode objEventId=null;
//					try{objEventId = objXML.GetElementsByTagName("EventId")[0];}
//					catch{};
//					
//					//Apply EventId if Present.
//					if(objEventId !=null)
//						if(this.EventMedwatch.EventId ==0)
//							this.EventMedwatch.EventId = Conversion.ConvertStrToInteger(objEventId.InnerText);
//		
//					base.BeforeSave (ref Cancel);
//		
//				}
//		

//		/// <summary>
//		/// Overrides the base class OnUpdateForm method to handle EventNumber in the 
//		/// instance data for ref binding.  
//		/// </summary>
//		/// <remarks>This instance data is specifically held in the SysExData section
//		/// of the Instance document.
//		/// </remarks>
//		/// <example> 
//		///		<control name="eventnumber" tabindex="2" type="id" ref="Instance/UI/FormVariables/SysExData/EventNumber"/>
//		/// </example>
//		public override void OnUpdateForm()
//		{
//			
//			base.OnUpdateForm ();
//
////			//Update the Instance Document with the necessary SysExData
////			base.CreateSysExData("EventNumber");
////
////			//Determine if the record exists
////			if (EventMedwatch.KeyFieldValue != 0)
////			{
////				//Load the Parent object to make the parent object's attributes available
////				EventMedwatch.LoadParent();
////				
////				//Replace the empty EventNumber node with the EventNumber value
////				base.ResetSysExData("EventNumber", (EventMedwatch.Parent as Event).EventNumber);
////			}//end if statement
//		}//end method OnUpdateForm()
	}
}