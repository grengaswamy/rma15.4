﻿using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using Riskmaster.Application.FundManagement;
using Riskmaster.Application.EnhancePolicy;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Text;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// Author  : Mridul Bansal
    /// Date    : 11/23/09.
    /// Replica of PolicyEnfForm.cs. Written afresh as now it is tied to a particular LOB.
    /// </summary>
    public class PolicyEnhGLForm : PolicyEnhForm
    {                    
        
        #region Global Varaibles and Properties
        const string CLASS_NAME = "PolicyEnh";
        public PolicyEnhGLForm(FormDataAdaptor fda) : base(fda)
        {
            base.m_ClassName = CLASS_NAME;
        }
        private UserLogin UserLogin { get { return PolicyEnh.Context.RMUser; } }
        #endregion 

        //umesh
        public override void InitNew()
        {
            base.InitNew();
            PolicyEnh.PolicyType = EnhancePolicyManager.LocalCache.GetCodeId("GL", "POLICY_LOB");        
        }
        //End Umesh

        #region Update Policy/Quote Detials

        protected override void SetEntityFieldsDetails(string p_sPolicyStatus, string p_sPolicyType, int p_iTransNumber)
        {
            DbReader objReader = null;
            DbReader objReader2 = null;
            string sSQL = string.Empty;
            bool bDisableAll = false;

            int iGreatestTrans = 0;
            string sOtherType = string.Empty;
            string sOtherStatus = string.Empty;
            string sTransactionStatus = string.Empty;

            bool bEditBroker = false;
            bool bEditInsurer = false;

            // Check authority for insure and broker.
            if (EntityAccessCheck())
            {
                if (PolicyEnh.InsurerEntity.EntityId == 0)
                {
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_PEOPMAINT, RMO_CREATE))
                        bEditInsurer = true;
                }
                else
                {
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_PEOPMAINT, RMO_UPDATE))
                        bEditInsurer = true;
                }
            }
            if (EntityAccessCheck())
            {
                if (PolicyEnh.BrokerEntity.EntityId == 0)
                {
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_PEOPMAINT, RMO_CREATE))
                        bEditBroker = true;
                }
                else
                {
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_PEOPMAINT, RMO_UPDATE))
                        bEditBroker = true;
                }
            }

            structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
            structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = false;
            structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
            structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = false;
            structEnhancePolicyUI.PolicyUI.PolicyStatusEnabled = false;

            // Need to find the most recent txn that is not an audit.
            // If that is not tran num we have then we need to disable all fields.

            if (!(p_iTransNumber == 0) && !(PolicyEnh.PolicyXTransEnhList.Count == 0) && !(PolicyEnh.PolicyId == 0))
            {
                sSQL = " SELECT * FROM POLICY_X_TRANS_ENH WHERE POLICY_ID=" + PolicyEnh.PolicyId
                    + " ORDER BY TRANSACTION_ID DESC ";
                objReader = PolicyEnh.Context.DbConnLookup.ExecuteReader(sSQL);

                while (objReader.Read())
                {
                    iGreatestTrans = Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_ID"), base.ClientId);
                    sOtherType = EnhancePolicyManager.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_TYPE"), base.ClientId));
                    sOtherStatus = EnhancePolicyManager.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_STATUS"), base.ClientId));

                    if (sOtherType == "AU")
                    {
                        if (p_iTransNumber != iGreatestTrans)
                        {
                            if (sOtherStatus == "PR")
                            {
                                structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                                bDisableAll = true;
                                break;
                            }
                        }
                        else
                        {
                            p_sPolicyStatus = "AU";
                            break;
                        }
                    }
                    else
                    {
                        if (p_iTransNumber != iGreatestTrans)
                        {
                            if (sOtherStatus != "PR")
                            {
                                //Start:Added by Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                                //structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = true;
                                structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                                //End:Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                            }
                            else
                                structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                            bDisableAll = true;
                            break;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                objReader.Close();
            }

            if (bDisableAll)
            {
                #region Disabled All
                structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                structEnhancePolicyUI.PolicyUI.RetroDateEnabled = true;

                structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;
                // Shruti
                structEnhancePolicyUI.PolicyUI.MCOEnabled = false;
                structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;
                structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = false;
                structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = false;
                structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = false;
                structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = false;
                structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = false;
                structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = true;
                structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = false;
                structEnhancePolicyUI.ExposureUI.EditExposureEnabled = true;
                structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                structEnhancePolicyUI.McoUI.EditMcoEnabled = true;

                structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;

                structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;
                //csingh7 for MITS 21670 : Start
                if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_VIEW))
                    structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_VIEW))
                    structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                //csingh7 for MITS 21670 : End

                structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                //MGaba2:MITS 11802
                //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                if (p_sPolicyType == "EN")
                {
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = true;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = true;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = true;
                }
                else
                {
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                }
                structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;

                #endregion
                return;
            }

            foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
            {
                if (objPolicyXTransEnh.TransactionId == p_iTransNumber)
                {
                    sTransactionStatus = EnhancePolicyManager.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionStatus);
                    break;
                }
            }

            switch (p_sPolicyStatus)
            {
                // New 
                case "":
                    #region New
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = true;
                    //Mridul 11/19/09 MITS:18229. Commented as LOB will be hard coded.
                    //structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = false;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = true;

                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = true;
                    //else
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_CREATE); //Added by csingh7 : MITS 20427
                    structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_DELETE);
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_UPDATE);
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_VIEW))
                        structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = true;
                    //else
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                    structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_DELETE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_UPDATE);
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_VIEW))
                        structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                    //structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_DELETE);Commented by csingh7 MITS 20427
                    //structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);Commented by csingh7 MITS 20427

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //Mridul. 11/24/09. MITS:18229. Since this is GL policy so we need to set Expense Modification Factor to FALSE by default.
                    //if( EnhancePolicyManager.CCacheFunctions.GetShortCode( PolicyEnh.PolicyType ) == "GL" )
                    //    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //else
                    //    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = true;
                    break;
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    #endregion
                // Audit
                case "AU":
                    #region  Audit
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    if (sTransactionStatus == "PR")
                    {
                        structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                        structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                        //Commented by csingh7 : MITS 20427 Start
                        //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_CREATE))
                        //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = true;
                        //else
                        //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                        //Commented by csingh7 : MITS 20427 End
                        structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_CREATE); //Added by csingh7 : MITS 20427
                        structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_DELETE);
                        structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_UPDATE);
                        if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_VIEW))
                            structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                        //Commented by csingh7 : MITS 20427 Start
                        //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE))
                        //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = true;
                        //else
                        //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                        //Commented by csingh7 : MITS 20427 End
                        structEnhancePolicyUI.ExposureUI.AddExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                        structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_DELETE);
                        structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_UPDATE);
                        if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_VIEW))
                            structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                        //structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_DELETE);Commented by csingh7 MITS 20427
                        //structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);Commented by csingh7 MITS 20427

                        structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = true;
                        structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = true;
                        structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = true;
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        //Mridul. 11/24/09. MITS:18229. Since this is GL policy so we need to set Expense Modification Factor to FALSE by default.
                        //if (EnhancePolicyManager.CCacheFunctions.GetShortCode(PolicyEnh.PolicyType) == "GL")
                        //    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                        //else
                        //    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = true;
                        structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    }
                    else
                    {
                        structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                        structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = false;
                        structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                        structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                        structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = false;
                        structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = false;
                        structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = true;
                        //Mridul. 11/24/09. MITS:18229. Since this is GL policy so we need to disable the AUDIT functionality.
                        //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                        //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                        //else
                        //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        //Start:Added by Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                        //structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                        //End:Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                        structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                        //csingh7 for MITS 21670 : Start
                        if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_VIEW))
                            structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                        if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_VIEW))
                            structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                        //csingh7 for MITS 21670 : End
                    }

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = false;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    // structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    break;
                    #endregion
                // Quote
                case "Q":
                    #region Quote
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = true;
                    //Mridul 11/19/09 MITS:18229. Commented as LOB will be hard coded.
                    //structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_CONVERT);
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = false;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = true;

                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = true;
                    //else
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_CREATE); //Added by csingh7 : MITS 20427
                    structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_DELETE);
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_UPDATE);
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_VIEW))
                        structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = true;
                    //else
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_CREATE); //Added by csingh7 : MITS 20427
                    structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_DELETE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_UPDATE);
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_VIEW))
                        structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                    //structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_DELETE);Commented by csingh7 : MITS 20427
                    //structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);Commented by csingh7 : MITS 20427

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //Mridul. 11/24/09. MITS:18229. Since this is GL policy so we need to set Expense Modification Factor to FALSE by default.
                    //if (EnhancePolicyManager.CCacheFunctions.GetShortCode(PolicyEnh.PolicyType) == "GL")
                    //    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //else
                    //    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = true;
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    break;
                    #endregion
                // Converted Quote
                case "CV":
                    #region Converted Quote
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;

                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = false;
                    structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = false;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = true;
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = true;
                    //csingh7 for MITS 21670 : Start
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_VIEW))
                        structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_VIEW))
                        structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                    //csingh7 for MITS 21670 : End

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = false;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = false;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = false;
                    //Divya -03/23/2007 Bank Account should be enabled for coverted quote
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;

                    break;
                    #endregion
                // In Effect
                case "I":
                    #region In Effect
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    if (p_sPolicyType == "AU")
                        structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;
                    else
                        structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = true;
                    //Mridul. 11/24/09. MITS:18229. Since this is GL policy so need to disable the AUDIT functionality.
                    //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //else
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    //Start:Added by Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                    //structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //End:Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;

                    if (sTransactionStatus == "PR")
                    {
                        structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                        structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                        //Commented by csingh7 : MITS 20427 Start
                        //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_CREATE))
                        //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = true;
                        //else
                        //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                        //Commented by csingh7 : MITS 20427 End
                        structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_CREATE); //Added by csingh7 : MITS 20427
                        structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_DELETE);
                        structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_UPDATE);
                        if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_VIEW))
                            structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                        //Commented by csingh7 : MITS 20427 Start
                        //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE))
                        //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = true;
                        //else
                        //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                        //Commented by csingh7 : MITS 20427 End
                        structEnhancePolicyUI.ExposureUI.AddExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                        structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_DELETE);
                        structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_UPDATE);
                        if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_VIEW))
                            structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                        //structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_DELETE);Commented by csingh7 MITS 20427
                        //structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);Commented by csingh7 MITS 20427

                        structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = true;
                        structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = true;
                        structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = true;
                        structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = true;
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    }
                    else
                    {
                        structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                        structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = false;
                        structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                        structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = false;
                        //pmahli MITS 9773 Coverage and Exposure Edit button enabled in case of acccepted transaction
                        structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_UPDATE);
                        structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_UPDATE);
                        //csingh7 for MITS 21670 : Start
                        if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_VIEW))
                            structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                        if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_VIEW))
                            structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                        //csingh7 for MITS 21670 : End
                        if (p_sPolicyType != "AU")
                        {
                            structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AMEND);
                            //MGaba2:MITS 15382
                            //structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AMEND);
                            structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_CANCEL);
                        }
                        else
                        {
                            structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                            structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;
                        }

                        structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = false;
                        structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = false;

                        if (p_sPolicyType != "AU")
                        {
                            if (PolicyEnh.NonRenewFlag)
                                structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                            else
                                structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_RENEW);
                        }
                        else
                        {
                            sSQL = " SELECT TERM_NUMBER FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID = " + p_iTransNumber;
                            objReader = PolicyEnh.Context.DbConnLookup.ExecuteReader(sSQL);
                            if (objReader.Read())
                            {
                                sSQL = " SELECT * FROM POLICY_X_TERM_ENH WHERE POLICY_ID=" + PolicyEnh.PolicyId
                                    + " AND TERM_NUMBER=" + (Conversion.ConvertObjToInt(objReader.GetValue("TERM_NUMBER"), base.ClientId) + 1);

                                objReader2 = PolicyEnh.Context.DbConnLookup.ExecuteReader(sSQL);
                                if (objReader2.Read())
                                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                                else
                                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_RENEW);

                                objReader2.Close();
                            }
                            objReader.Close();
                        }
                        if (p_sPolicyType == "EN" || p_sPolicyType == "AU")
                        {
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = true;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = true;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = true;
                        }
                        else
                        {
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                            structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                        }
                        //Mridul. 11/24/09. MITS:18229. Since this is GL policy so need to disable the AUDIT functionality.
                        //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                        //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                        //else
                        //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                        //Start:Added by Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                        // structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;  
                        structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                        //End:Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                    }
                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //Mukul(07/09/2007) Added MITS 9973
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    break;
                    #endregion
                // Revoked
                case "R":
                    #region Revoked
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = true;

                    //Mridul. 11/24/09. MITS:18229. Since this is GL policy so need to disable the AUDIT functionality.
                    //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //}
                    //else
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    //}
                    //Start:Added by Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                    //structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false; 
                    // structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false; 
                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //End:Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671

                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;

                    if (EnhancePolicyManager.LocalCache.GetShortCode(EnhancePolicyManager.GetLatestTran(PolicyEnh, true).TransactionStatus) == "PR")
                    {
                        structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                        structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;
                    }
                    else
                    {//MGaba2:MITS 15382:Start
                        // structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_RENEW);
                        // structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AMEND);
                        structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AMEND);
                        structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_CANCEL);
                        //MGaba2:MITS 15382:End                        
                    }
                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    break;
                    #endregion
                case "E":
                    #region Expired
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    //Mridul. 11/24/09. MITS:18229. Since this is GL policy so need to disable the AUDIT functionality.
                    //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //}
                    //else
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    //}
                    //Start:Added by Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                    //structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false; 
                    //structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //End:Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671

                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    break;
                    #endregion
                case "PR":
                    #region Provisionally Renewed
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    //Mridul. 11/24/09. MITS:18229. Since this is GL policy so need to disable the AUDIT functionality.
                    //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //}
                    //else
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    //}
                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //Start:Added by Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                    // structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //End:Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = true;
                    //else
                    //    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.CoverageUI.AddCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_CREATE); //Added by csingh7 : MITS 20427
                    structEnhancePolicyUI.CoverageUI.DeleteCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_DELETE);
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_UPDATE);
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_VIEW))
                        structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                    //Commented by csingh7 : MITS 20427 Start
                    //if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_ACCESS) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_VIEW) || UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_CREATE))
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = true;
                    //else
                    //    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = false;
                    //Commented by csingh7 : MITS 20427 End
                    structEnhancePolicyUI.ExposureUI.AddExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_CREATE);   //Added by csingh7 : MITS 20427
                    structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_DELETE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_UPDATE);
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_VIEW))
                        structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                    //structEnhancePolicyUI.ExposureUI.DeleteExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_DELETE);Commented by csingh7 MITS 20427
                    //structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGT_EXP_ENH, RMO_UPDATE);Commented by csingh7 MITS 20427

                    structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = true;
                    structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = true;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //Mridul. 11/24/09. MITS:18229. Since this is GL policy so we need to set Expense Modification Factor to FALSE by default.
                    //if (EnhancePolicyManager.CCacheFunctions.GetShortCode(PolicyEnh.PolicyType) == "GL")
                    //    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //else
                    //    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = true;
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;

                    break;
                    #endregion
                // Cancelled
                case "C":
                case "CPR":
                case "CF":
                    #region Cancelled
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = false;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    //Mridul. 11/24/09. MITS:18229. Since this is GL policy so need to disable the AUDIT functionality.
                    //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //}
                    //else
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    //}
                    //Start:Added by Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                    // structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_AUDIT);
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //End:Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_REINSTATE);
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = false;
                    structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = false;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //pmahli MITS 9773
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_UPDATE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_UPDATE);
                    //csingh7 for MITS 21670 : Start
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_VIEW))
                        structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_VIEW))
                        structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                    //csingh7 for MITS 21670 : End

                    break;
                    #endregion
                // Provisional Cancel
                case "PCF":
                case "PCP":
                    #region Provisional Cancel
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = true;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    //Mridul. 11/24/09. MITS:18229. Since this is GL policy so need to disable the AUDIT functionality.
                    //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //}
                    //else
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    //}
                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //Start:Added by Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                    // structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //End:Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = true;
                    structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = true;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //pmahli MITS 9773
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_UPDATE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_UPDATE);
                    //csingh7 for MITS 21670 : Start
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_VIEW))
                        structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_VIEW))
                        structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                    //csingh7 for MITS 21670 : End

                    break;
                    #endregion
                // Provisional Reinstate
                case "PRN":
                case "PRL":
                    #region Provisional Reinstate
                    structEnhancePolicyUI.PolicyUI.PolicyNameEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyNumberEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyTypeEnabled = false;
                    structEnhancePolicyUI.PolicyUI.PolicyStateEnabled = false;

                    structEnhancePolicyUI.PolicyUI.IssueDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ReviewDateEnabled = true;
                    structEnhancePolicyUI.PolicyUI.EffectiveDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ExpirationDateEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RetroDateEnabled = true;

                    structEnhancePolicyUI.PolicyUI.CancelReasonEnabled = true;
                    structEnhancePolicyUI.PolicyUI.NonRenewReasonEnabled = false;

                    //Mridul. 11/24/09. MITS:18229. Since this is GL policy so need to disable the AUDIT functionality.
                    //if (EnhancePolicyManager.LocalCache.GetShortCode(PolicyEnh.PolicyType) == "WC")
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //}
                    //else
                    //{
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;
                    //}
                    structEnhancePolicyUI.PolicyUI.AuditButtonEnabled = false;
                    //Start:Added by Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                    //structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = false;  
                    structEnhancePolicyUI.PolicyUI.AuditButtonVisbile = true;
                    //End:Nitin Goel:Adding functionality for Audit Policy for GL Policy,05/07/2010,MITS#20671
                    // Shruti
                    structEnhancePolicyUI.PolicyUI.MCOEnabled = true;
                    structEnhancePolicyUI.PolicyUI.ConvertQuoteEnabled = false;
                    structEnhancePolicyUI.PolicyUI.ReinstatePolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.AmendPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.RenewPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CancelPolicyEnabled = false;

                    structEnhancePolicyUI.PolicyUI.BrokerEnabled = bEditBroker;
                    structEnhancePolicyUI.PolicyUI.InsurerEnabled = bEditInsurer;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumEnabled = true;
                    structEnhancePolicyUI.PolicyUI.CalculateEarnedPremiumVisible = true;
                    structEnhancePolicyUI.PolicyUI.PrimaryPolicyEnabled = false;
                    structEnhancePolicyUI.PolicyUI.CoverageEnabled = false;

                    structEnhancePolicyUI.PolicyUI.AcceptTransacationEnabled = true;
                    structEnhancePolicyUI.PolicyUI.DeleteTransactionEnabled = true;

                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled = false;
                    structEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible = false;
                    structEnhancePolicyUI.McoUI.AddMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.DeleteMcoEnabled = true;
                    structEnhancePolicyUI.McoUI.EditMcoEnabled = true;
                    structEnhancePolicyUI.PolicyUI.BankAccountEnabled = true;
                    //Start:Nitin Goel:Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    //MGaba2:MITS 11802
                    //structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = true;
                    structEnhancePolicyUI.PolicyUI.PolicyPrintEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_ENH, EnhancePolicyManager.RMB_POLMGT_ENH_PRINT);
                    //End:Nitin Goel,Use Print Policy SMS setting of Enhance Policy for VACo ,04/05/2010
                    structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled = false;
                    //pmahli MITS 9773 
                    structEnhancePolicyUI.CoverageUI.EditCoverageEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_UPDATE);
                    structEnhancePolicyUI.ExposureUI.EditExposureEnabled = UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_UPDATE);
                    //csingh7 for MITS 21670 : Start
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_COV_ENH, RMO_VIEW))
                        structEnhancePolicyUI.CoverageUI.CoverageEnabled = true;
                    if (UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_ACCESS) && UserLogin.IsAllowedEx(EnhancePolicyManager.RMB_POLMGTGL_EXP_ENH, RMO_VIEW))
                        structEnhancePolicyUI.ExposureUI.ExposureEnabled = true;
                    //csingh7 for MITS 21670 : End
                    break;
                    #endregion
            }

            // Set Defaults which need to be READONLY in each case. 
            structEnhancePolicyUI.PolicyUI.CancelDateEnabled = false;

        }

        #endregion        
     
    }
}
