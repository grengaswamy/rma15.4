using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Application.MMIHistorySummary;
using Riskmaster.DataModel;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: MMIHistorySummaryAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 09-September-2005
	///* $Author	: Mohit Yadav
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to call the application layer component for MMI History Summary
	///	which implements the functionality of MMI History. 
	/// </summary>
	public class MMIHistorySummaryAdaptor : BusinessAdaptorBase
	{
		
		#region Constructor
		/// <summary>
		/// Constructor for the class.
		/// </summary>
		public MMIHistorySummaryAdaptor()
		{
		}
		#endregion

		#region Public Methods

		#region "SaveMMIHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)"
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.MMIHistorySummary.MMIHistorySummary.SaveMMIHistory() method.
		///		Saves the MMI History
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of XML document would be:
		///		<Document>
		///			<MMIHistorySummary>
		///				<ClaimId>Claim Id</ClaimId>
		///				<EmployeeId>Employee Id</EmployeeId>
		///				<MMIDate>MMI Date</MMIDate>
		///				<DateExam>Date of Exam</DateExam>
		///				<ChangedBy>Changed By</ChangedBy>
		///				<ApprovedBy>Approved By</ApprovedBy>
		///				<Disability>Disability %</Disability>
		///				<Reason>Reason</Reason>
		///				<Physician>Physician</Physician>
		///				<Physician_cid>Physician Entity Id</Physician_cid>
		///			</MMIHistorySummary>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool SaveMMIHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.MMIHistorySummary.MMIHistorySummary  objMMIHistorySummary=null; //Application layer component

			int iClaimId = 0;
			int iEmployeeId = 0;
            int iPiRowId = 0;
			XmlElement objElement=null;					//used for parsing the input xml

			try
			{
				//check existence of Employee Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EmployeeId");
				if (objElement==null)
				{
					p_objErrOut.Add("MMIHistorySummaryAdaptor.SaveMMIHistory.Error",Globalization.GetString("MMIHistorySummaryAdaptor.SaveMMIHistory.IdMissing", base.ClientId),BusinessAdaptorErrorType.PopupMessage);
					return false;
				}
				iEmployeeId=Conversion.ConvertStrToInteger(objElement.InnerText);
				
				//check existence of Claim Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ClaimId");
				if (objElement==null)
				{
                    p_objErrOut.Add("MMIHistorySummaryAdaptor.SaveMMIHistory.Error", Globalization.GetString("MMIHistorySummaryAdaptor.SaveMMIHistory.IdMissing", base.ClientId), BusinessAdaptorErrorType.PopupMessage);
					return false;
				}
				iClaimId=Conversion.ConvertStrToInteger(objElement.InnerText);

                //neha start added Pi Id to open the pop from injury acreen
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PiRowId");
                if (objElement == null)
                {
                    p_objErrOut.Add("MMIHistorySummaryAdaptor.LoadListMMIHistory.Error", Globalization.GetString("MMIHistorySummaryAdaptor.LoadListMMIHistory.IdMissing", base.ClientId), BusinessAdaptorErrorType.PopupMessage);
                    return false;
                }
                iPiRowId = Conversion.ConvertStrToInteger(objElement.InnerText); //neha end
				objMMIHistorySummary = new Riskmaster.Application.MMIHistorySummary.MMIHistorySummary(userLogin.objRiskmasterDatabase.DataSourceName,userLogin.LoginName,userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);

                p_objXmlOut = objMMIHistorySummary.SaveMMIHistory(iClaimId, iEmployeeId, iPiRowId,p_objXmlIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("MMIHistorySummaryAdaptor.SaveMMIHistory.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				if (objMMIHistorySummary != null)
					objMMIHistorySummary.Dispose(); 
				objElement=null;
			}
		}

		#endregion

		#region "LoadListMMIHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)"
		/// <summary>
		///		This method is a wrapper to Riskmaster.Application.MMIHistorySummary.MMIHistorySummary.LoadListMMIHistory() method.
		///		Gets the MMI History in XML format
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of XML document would be:
		///		<Document>
		///			<MMIHistorySummary>
		///				<ClaimId>Claim Id</ClaimId>
		///				<EmployeeId>Employee Id</EmployeeId>
		///			</MMIHistorySummary>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of the output XML document would be
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool LoadListMMIHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			Riskmaster.Application.MMIHistorySummary.MMIHistorySummary  objMMIHistorySummary=null; //Application layer component

			int iClaimId = 0;
			int iEmployeeId = 0;//neha
            int iPiRowId = 0;//Neha
			XmlElement objElement=null;					//used for parsing the input xml

			try
			{
				//check existence of Employee Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EmployeeId");
				if (objElement==null)
				{
                    p_objErrOut.Add("MMIHistorySummaryAdaptor.LoadListMMIHistory.Error", Globalization.GetString("MMIHistorySummaryAdaptor.LoadListMMIHistory.IdMissing", base.ClientId), BusinessAdaptorErrorType.PopupMessage);
					return false;
				}
				iEmployeeId=Conversion.ConvertStrToInteger(objElement.InnerText);
				
				//check existence of Claim Id which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ClaimId");
				if (objElement==null)
				{
                    p_objErrOut.Add("MMIHistorySummaryAdaptor.LoadListMMIHistory.Error", Globalization.GetString("MMIHistorySummaryAdaptor.LoadListMMIHistory.IdMissing", base.ClientId), BusinessAdaptorErrorType.PopupMessage);
					return false;
				}
				iClaimId=Conversion.ConvertStrToInteger(objElement.InnerText);
                //neha start added Pi Id to open the pop from injury acreen
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PiRowId");
                if (objElement == null)
                {
                    p_objErrOut.Add("MMIHistorySummaryAdaptor.LoadListMMIHistory.Error", Globalization.GetString("MMIHistorySummaryAdaptor.LoadListMMIHistory.IdMissing", base.ClientId), BusinessAdaptorErrorType.PopupMessage);
                    return false;
                }
                iPiRowId = Conversion.ConvertStrToInteger(objElement.InnerText); //neha end
				objMMIHistorySummary = new Riskmaster.Application.MMIHistorySummary.MMIHistorySummary(userLogin.objRiskmasterDatabase.DataSourceName,userLogin.LoginName,userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);

                p_objXmlOut = objMMIHistorySummary.LoadListMMIHistory(iClaimId, iEmployeeId, iPiRowId);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("MMIHistorySummaryAdaptor.LoadListMMIHistory.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (objMMIHistorySummary != null)
                    objMMIHistorySummary.Dispose();
				objElement=null;
			}
		}

		#endregion

        #region DeleteMMIHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        //sgoel6 MITS 04/27/2009
        /// <summary>
        ///	This method is a wrapper to Riskmaster.Application.MMIHistorySummary.MMIHistorySummary.DeleteMMIHistory() method.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool DeleteMMIHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Riskmaster.Application.MMIHistorySummary.MMIHistorySummary objMMIHistorySummary = null; //Application layer component

            string sMMIRowIds = string.Empty;
            XmlElement objElement = null;					//used for parsing the input xml

            try
            {
                //check existence of MMI Row Id which is required
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//MMIRowId");
                if (objElement == null)
                {
                    p_objErrOut.Add("MMIHistorySummaryAdaptor.DeleteMMIHistory.Error", Globalization.GetString("MMIHistorySummaryAdaptor.DeleteMMIHistory.IdMissing", base.ClientId), BusinessAdaptorErrorType.PopupMessage);
                    return false;
                }
                sMMIRowIds = objElement.InnerText;
                sMMIRowIds = sMMIRowIds.Replace("!,#", ",");

                objMMIHistorySummary = new Riskmaster.Application.MMIHistorySummary.MMIHistorySummary(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);                
                p_objXmlOut = objMMIHistorySummary.DeleteMMIHistory(sMMIRowIds, p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("MMIHistorySummaryAdaptor.SaveMMIHistory.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objMMIHistorySummary != null)
                    objMMIHistorySummary.Dispose();
                objElement = null;
            }
        }
        #endregion

        #endregion
	}
}
