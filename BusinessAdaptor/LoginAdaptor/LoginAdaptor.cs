using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security.RMApp;    // JP 6/15/2005
using Riskmaster.Application.SecurityManagement ;
using Riskmaster.Application.FundManagement;
using Riskmaster.Security.Encryption;    //Arnab: MITS-8489 - Added reference


namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class LoginAdaptor: BusinessAdaptorBase
	{

		private XmlDocument m_req = null;
		private XmlDocument m_resp = null;
		private BusinessAdaptorErrors m_err = null;
        private bool blnSuccess = false;

		#region BSB Standard  XML Parameter Parsing Routines

		static XmlNamespaceManager _nsmgr =null;
		static XmlNameTable _nt = null;
		const string PARAM_XPATH = "/ParamList/Param";
		static LoginAdaptor()
		{
			_nt = new NameTable();
			_nsmgr = new XmlNamespaceManager(_nt);
			_nsmgr.AddNamespace("m",AppConstants.APP_NAMESPACE);
			_nsmgr.AddNamespace("soap",@"http://schemas.xmlsoap.org/soap/envelope/");
		}
		private bool HasParam(string paramName){return(SafeParam(paramName) !=null);}
		private XmlElement SafeParam(string paramName)
		{
			return (SafeXPath(String.Format("{0}[@name='{1}']",PARAM_XPATH,paramName)) as XmlElement);
		}		
		private string SafeParamText(string paramName)
		{
			try{return (SafeParam(paramName) as XmlElement).InnerText;}
			catch{return "";}																																
		}
		
		private XmlNode SafeXPath(string xPath)
		{
			try
			{
				//return m_req.SelectSingleNode(xPath, _nsmgr);
				return m_req.SelectSingleNode(xPath);
			}
			catch
			{
				return null;
			}
		}
		private bool SafeSaveParamText(string paramName, string paramValue)
		{
			try{ (SafeParam(paramName) as XmlElement).InnerText = paramValue;return true;}
			catch{return false;}																																
		}
		private bool SafeSaveParamXml(string paramName, string paramValue)
		{
			try{ (SafeParam(paramName) as XmlElement).InnerXml = paramValue.Replace("&","&amp;");return true;}
			catch{return false;}																																
		}

		#endregion

		public LoginAdaptor()
		{
			m_resp = new XmlDocument(_nt);
			m_err = new BusinessAdaptorErrors(base.ClientId);
		}


		/* Sample Login XML Message 
		  
		  
		 REQUEST:
		<m:message>
				<m:call>
					<m:webservice></m:webservice>
					<m:function>AuthenticateUser</m:function>
				</m:call>
				<m:document>
					<m:ParamList>
						<m:Param name="LoginName"></m:Param>
						<m:Param name="CurrentDSN"></m:Param>
						<m:Param name="AvailableDSNList"></m:Param>
						<m:Param name="CurrentView"></m:Param>
						<m:Param name="AvailableViewList"></m:Param>
						<m:Param name="Action"></m:Param>
						<m:Param name="AuthorizationToken"></m:Param>
					</m:ParamList>
				</m:document>
			</m:message>
		
		RESPONSE:
		<m:message>
				<m:call>
					<m:webservice></m:webservice>
					<m:function>AuthenticateUser</m:function>
				</m:call>
				<m:document>
					<m:ParamList>
						<m:Param name="LoginName"></m:Param>
						<m:Param name="CurrentDSN"></m:Param>
						<m:Param name="AvailableDSNList"></m:Param>
						<m:Param name="CurrentView"></m:Param>
						<m:Param name="AvailableViewList"></m:Param>
						<m:Param name="Action"></m:Param>
						<m:Param name="AuthorizationToken"></m:Param>
					</m:ParamList>
				</m:document>
			</m:message>
		*/

		private string GetSecret(string loginName, string sDSN)
		{
			string ret = "";
			string sSQL = "SELECT USER_DETAILS_TABLE.PASSWORD " +
						"FROM USER_DETAILS_TABLE INNER JOIN " +
						"DATA_SOURCE_TABLE ON USER_DETAILS_TABLE.DSNID = DATA_SOURCE_TABLE.DSNID " +
						"WHERE (USER_DETAILS_TABLE.LOGIN_NAME = '" + loginName + "') " +
						"AND (DATA_SOURCE_TABLE.DSN = '" + sDSN + "')";
			using(DbReader reader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(base.ClientId),String.Format(sSQL)))
			{
				if(! reader.Read())
					ret = "";
				string sCryptPwd = reader.GetString(0);
                if (sCryptPwd == "")
                    ret = "";
                else
                    ret = RMCryptography.DecryptString(sCryptPwd);
				reader.Close();
			}
			return ret;
		}

		//Presentation Entry Point
		//Needs "LoginName"
		//Populates "AvailableDSNList"
		public bool GetAvailableDSNListTrusted(XmlDocument xmlIn, ref XmlDocument xmlResponse,ref BusinessAdaptorErrors  errOut)
		{
			m_req = xmlIn;
			errOut = m_err;

            Login objLogin = new Login(base.ClientId);
			string[] list = objLogin.GetUserDatabases(SafeParamText("LoginName"));
			string sValue = "";
			foreach(string DSN in list)
				sValue += String.Format("<Item value=\"{0}\">{0}</Item>",DSN);

			SafeSaveParamXml("AvailableDSNList",	sValue);
			
			//SafeSaveParamText("PwdExpiredStatus","") ;
			if(PublicFunctions.IsPwdPolEnabled())
			{
				this .CheckPasswordExpr(m_req); 
			}
			m_resp = m_req;
			xmlResponse = m_resp;
			return true; 		
		}
		//Presentation Entry Point
		public bool GetAvailableDSNList(XmlDocument xmlIn, ref XmlDocument xmlResponse,ref BusinessAdaptorErrors  errOut)
		{
			m_req = xmlIn;
			errOut = m_err;

            Login objLogin = new Login(base.ClientId);
			string[] list = objLogin.GetUserDatabases(base.loginName);
			string sValue = "";
			foreach(string DSN in list)
				sValue += DSN + ",";
			sValue = sValue.TrimEnd(',');

			SafeSaveParamText("AvailableDSNList",	sValue);

			m_resp = m_req;
			xmlResponse = m_resp;
			return true; 
		}		


		//Presentation Entry Point
		//Needs "LoginName","CurrentDSN"
		//Populates "AvailableViewList"
		public bool GetAvailableViewListTrusted(XmlDocument xmlIn, ref XmlDocument xmlResponse,ref BusinessAdaptorErrors  errOut)
		{
            String sValue = string.Empty;

            
			try
			{
				m_req = xmlIn;
				errOut = m_err;

				UserPermissions temp = (new UserPermissions(base.ClientId));					
				ArrayList pvList = temp.GetListOfPowerViews();				
				int[] perm = temp.GetPermissionsArray();


                foreach (string View in GetAvailableViewListImpl(base.userLogin))
				{
					//as it happens logic for going to fist page on clicking login button needs to
					// be same as for building Nav tree, 
					// therefore moving the screen to first record is essential, in case 'create permission' is not there
					// MITS 8730

					sValue += String.Format("<Item value=\"{0}\" home=\"{2}\">{1}</Item>",View.Split(',')[0],View.Split(',')[1]
						,temp.ApplySecurityOnViewPath(
						View.Split(',')[2]
						,base.userLogin
						,pvList
						,perm
						));
				}
				SafeSaveParamXml("AvailableViewList",sValue);
				//TR#1209-01/13/06-Pankaj-Tab/Topdown layout settings. Set to ViewType node
				SafeSaveParamXml("ViewType",GetSysViewType(SafeParamText("CurrentDSN"),base.userLogin));
				m_resp = m_req;
				xmlResponse = m_resp;
			}
			catch(Exception p_objException)
			{   /*Tanuj- Trap BES enabled exception and add it in error collection.
				 * 
				 * */
				if(p_objException is Riskmaster.ExceptionTypes.OrgSecAccessViolationException)
				{
					m_err.Add("99991", p_objException.Message, BusinessAdaptorErrorType.Error);
					m_resp = m_req;
					xmlResponse = m_resp;
					return false;	
				}
                if (p_objException is Riskmaster.ExceptionTypes.LoginException)
                {
                    m_err.Add("99991", p_objException.Message, BusinessAdaptorErrorType.Error);
                    m_resp = m_req;
                    xmlResponse = m_resp;
                    return false;
                }
				throw p_objException ;
				
			}
			return true; 		
		}		
		
		
		private ArrayList GetAvailableViewListImpl(UserLogin objUserLogin)
		{
		string SQL = "SELECT NET_VIEWS.* FROM NET_VIEWS_MEMBERS,NET_VIEWS WHERE ((MEMBER_ID=" + objUserLogin.UserId + " AND ISGROUP=0) OR (MEMBER_ID=" + objUserLogin.GroupId + " AND ISGROUP<>0)) AND NET_VIEWS_MEMBERS.VIEW_ID=NET_VIEWS.VIEW_ID ORDER BY ISGROUP";
		ArrayList  arrQE = null;
		DbReader reader=null; 
		DbConnection cn  = null;
		try
		{
		reader = DbFactory.GetDbReader(objUserLogin.objRiskmasterDatabase.ConnectionString,SQL);
		int i = 0;
		string sViewItem = "";
		arrQE = new ArrayList();
		while(reader.Read()) //Pick up	all defined views.
		{  
		sViewItem = String.Format("{0},{1},{2}",reader.GetInt("VIEW_ID"),reader.GetString("VIEW_NAME"),reader.GetString("HOME_PAGE"));
			//-- ABhateja 08.17.2006 -START-
			//-- MITS 7631, Check if a view already exists in the collection.
			if(!arrQE.Contains(sViewItem))
			{
				arrQE.Add(sViewItem);
				i++;
			}
			//-- ABhateja 08.17.2006 -END-
		}
		}
		finally
		{
            if (cn != null)
            {
                cn.Close();
                cn.Dispose();
            }
        if (reader != null)
        {
            reader.Close();
            reader.Dispose();
        }
		}

		return arrQE;
		}

//		bool SelectView(string viewName, int viewId, string viewHome)
//		{
//			m_session[AppConstants.SESSION_QUICKENTRY]="" + viewId;
//			m_session[AppConstants.SESSION_QUICKENTRYVIEWNAME]="" +  viewName;
//			try{ m_session[AppConstants.SESSION_QUICKENTRYHOMEPAGE] =	viewHome;}
//			catch{}
//			SafeSaveParamText("CurrentView",	viewName);
//			m_resp = m_req;
//			return true;
//		}


		/// <summary>
		/// Tanuj; This function would check if admin is trying to login. 
		/// </summary>
		/// <param name="p_sLoginName"></param>
		/// <param name="p_sPwd"></param>
		/// <returns></returns>
		private bool IsValidAdminUser(string p_sLoginName , string p_sPwd)
		{
			SMSUser objSMSUser= null ;
			try
			{
               objSMSUser = new SMSUser(p_sLoginName, base.ClientId);
               if(!objSMSUser.IsAdminUser)
				   return false;
               else
               return objSMSUser.ValidAdminCredentials(p_sPwd) ;
			   
			}
			finally
			{
				objSMSUser = null;
			}
		}
		
		private bool DefaultResponse()
		{
			m_resp = m_req;
			return true;
		}

		//TR#1209-01/13/06-Pankaj-Tab/Topdown layout settings picked from the status page and set into session
		private string GetSysViewType(string sDsnName, UserLogin objLogin)
		{
			string sViewType="2"; //Defaulted to tab-layout when nothing is found. 1 is top-down
            using (DbReader reader = DbFactory.GetDbReader(objLogin.objRiskmasterDatabase.ConnectionString,
                "SELECT LAYOUT_ID FROM NET_LAYOUTS_MEMBERS WHERE MEMBER_ID=" + objLogin.UserId))
            {
                if (reader.Read())
                    sViewType = reader.GetInt32(0).ToString();
               // if (reader != null)
                    //reader.Dispose();
                reader.Close();
            }
			return sViewType;
		}

		public bool GetBISSSOString(XmlDocument xmlIn, ref XmlDocument xmlResponse,ref BusinessAdaptorErrors  errOut)
		{
			m_req = xmlIn;
			errOut = m_err;

			string sBISLogin;

			// Create user id/password string for BIS
			string sAuth = SafeParamText("AuthPassword");
			if (sAuth == "BISRMAUTHKEY0306")
			{

				sBISLogin = RMCryptography.EncryptString("USERID=" + RMCryptography.EncryptString(m_userLogin.LoginName) + "##" + "PWD=" + RMCryptography.EncryptString(m_userLogin.Password));

				SafeSaveParamXml("LoginStr",sBISLogin);
			}
			else
				SafeSaveParamXml("LoginStr","");

			// Send back path to BIS from riskmaster.config
            string sBISURL = RMConfigurationManager.GetAppSetting("BISURL");
			SafeSaveParamXml("BISURL", sBISURL);

			m_resp = m_req;
			xmlResponse = m_resp;
			return true; 		
		}		

		/// Name		: AuthorizeUserAccount
		/// Author		: Anurag Agarwal
		/// Date Created	: 20 Jul 2006		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Function will check 
		/// </summary>
		/// <param name="p_objXMLDoc">
		/// Input xml after authentication.
		///		<ParamList>
		///			<Param name="LoginName">csc</Param> 
		///			<Param name="Password">********</Param> 
		///			<Param name="Authenticated">true</Param>
		///			<Param name="Status" value="*">Message</Param>
		///		</ParamList>
		/// </param>
		/// <returns>XML with User Account status, which includes 
		/// account lockout, expiration etc</returns>
		private XmlDocument AuthorizeUserAccount(XmlDocument p_objXMLDoc)
		{
			bool bIsAuthenticated = false;
			string sLoginName = "";
			int iUserID = 0;
			string sQuery = "";
			int iLockOutDur = 0;
			int iAllowedLogin = 0;
			DbConnection objConn = null;
			bool bLockAccount = false;
            
			try
			{
                
				bIsAuthenticated = Conversion.CastToType<bool>(p_objXMLDoc.SelectSingleNode("//Param[@name='Authenticated']").InnerText, out blnSuccess); 
				sLoginName = p_objXMLDoc.SelectSingleNode("//Param[@name='LoginName']").InnerText;

                using (DbReader objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(base.ClientId), String.Format("SELECT USER_ID FROM USER_DETAILS_TABLE WHERE LOGIN_NAME='{0}'", sLoginName)))
				{
					if(objReader.Read())
					{
						iUserID = Conversion.CastToType<int>(objReader["USER_ID"].ToString(), out blnSuccess);
						objReader.Close();
					}
				}

				if(iUserID <= 0)
					return p_objXMLDoc;

                objConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(base.ClientId));
				objConn.Open();
				if(bIsAuthenticated)
				{
					objConn.ExecuteNonQuery("INSERT INTO LOGIN_HISTORY (USER_ID, DTTM_LOGIN, STATUS)" + 
						" VALUES(" + iUserID + ",'" + Conversion.ToDbDateTime(DateTime.Now) + "',1)");
				}
				else
					objConn.ExecuteNonQuery("INSERT INTO LOGIN_HISTORY (USER_ID, DTTM_LOGIN, STATUS)" + 
						" VALUES(" + iUserID + ",'" + Conversion.ToDbDateTime(DateTime.Now) + "',0)");
				objConn.Close();
            
				SMSUser objSMSUser = null;
				bool bIsAdminUser = false;
				try
				{
                    objSMSUser = new SMSUser(sLoginName, base.ClientId);
                    bIsAdminUser = objSMSUser.IsSMSAdminUser(false, sLoginName);
				}
				finally
				{
					objSMSUser = null;
				}

				if(bIsAdminUser)
				{
                   return p_objXMLDoc;
				}
				sQuery = "SELECT * FROM USER_TABLE WHERE USER_ID = " + iUserID;
                using (DbReader objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(base.ClientId), sQuery))
				{
					if(objReader.Read())
					{
						if(Conversion.CastToType<int>(objReader["ACCOUNT_LOCK_STATUS"].ToString(), out blnSuccess) == 1)
						{
							p_objXMLDoc.SelectSingleNode("//Param[@name='Status']").Attributes["value"].Value = "1";
							p_objXMLDoc.SelectSingleNode("//Param[@name='Status']").InnerText = Globalization.GetString("LoginAdaptor.AuthorizeUserAccount.UserAccountLocked", base.ClientId);
							return p_objXMLDoc;
						}
						objReader.Close();
					}
				}

				sQuery = "SELECT * FROM PASSWORD_POLICY_PARMS";
                using (DbReader objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(base.ClientId), sQuery))
				{
					if(objReader.Read())
					{
						iLockOutDur = Conversion.CastToType<int>(objReader["PWD_LOCKOUT_DUR"].ToString(), out blnSuccess);
						iAllowedLogin = Conversion.CastToType<int>(objReader["ALLOWED_LOGIN_ATTEMPTS"].ToString(), out blnSuccess);
						objReader.Close();
					}
				}

				DateTime dtDate = DateTime.Now.AddHours(-1 * iLockOutDur);
				int iLoopCount = 0;
				bLockAccount = false;
				
				string sDate = Conversion.ToDbDateTime(dtDate);    
				sQuery = "SELECT STATUS FROM LOGIN_HISTORY WHERE USER_ID = " + iUserID + " AND DTTM_LOGIN > '" + sDate  + "' ORDER BY DTTM_LOGIN desc";
                using (DbReader objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(base.ClientId), sQuery))
				{
				
					iLoopCount = 0;
				
					while(objReader.Read())
					{
						if(Conversion.CastToType<int>(objReader["STATUS"].ToString(), out blnSuccess) == 1)
						{
							bLockAccount = false;
							//Setting status for Success, no error message
							p_objXMLDoc.SelectSingleNode("//Param[@name='Status']").Attributes["value"].Value = "-1";
							break;						
						}

						iLoopCount++;
						if( iLoopCount >= iAllowedLogin )
						{
							bLockAccount = true;
							break;
						}
					}
					if(objReader != null)
						objReader.Close();
				}

				if(bLockAccount)
				{
					objConn.Open() ;
					objConn.ExecuteNonQuery("UPDATE USER_TABLE SET ACCOUNT_LOCK_STATUS = 1 WHERE USER_ID = " + iUserID);
					objConn.Close() ;
					p_objXMLDoc.SelectSingleNode("//Param[@name='Status']").Attributes["value"].Value = "1";
                    p_objXMLDoc.SelectSingleNode("//Param[@name='Status']").InnerText = Globalization.GetString("LoginAdaptor.AuthorizeUserAccount.UserAccountLocked", base.ClientId);
					return p_objXMLDoc;
				}
			}
			finally
			{
				if( objConn!=null)
				{
                    objConn.Close() ;
					objConn.Dispose() ;
                    objConn = null;
				}
			}
			return p_objXMLDoc;
		}

		/// Name		: CheckPasswordExpr
		/// Author		: Anurag Agarwal
		/// Date Created	: 24 Jul 2006		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Function will check 
		/// </summary>
		/// <param name="p_objXMLDoc">
		/// Input xml after authentication.
		///		<ParamList>
		///			<Param name="LoginName">csc</Param> 
		///			<Param name="Password">********</Param> 
		///			<Param name="Authenticated">true</Param>
		///			<Param name="Status" value="*">Message</Param>
		///		</ParamList>
		/// </param>
		/// <returns>XML with User Account status, which includes 
		/// account lockout, expiration etc</returns>
		private XmlDocument CheckPasswordExpr(XmlDocument p_objXMLDoc)
		{
			string sLoginName = "";
			string sPassword = "";
			int iUserID = 0;
			int iForceChangePwd = 0;
			string sQuery = "";
			int iPwdExpDays = 0;
			int iPwdExprWarnDays = 0;
			DateTime dtPwdSMSExpr = DateTime.Now;
			try
			{
				sLoginName = p_objXMLDoc.SelectSingleNode("//Param[@name='LoginName']").InnerText;

                using (DbReader objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(base.ClientId), String.Format("SELECT * FROM USER_DETAILS_TABLE WHERE LOGIN_NAME='{0}'", sLoginName)))
				{
					if(objReader.Read())
					{
						iUserID = Conversion.CastToType<int>(objReader["USER_ID"].ToString(), out blnSuccess);
						sPassword = PublicFunctions.Decrypt(objReader["PASSWORD"].ToString());
						iForceChangePwd = Conversion.CastToType<int>(objReader["FORCE_CHANGE_PWD"].ToString(), out blnSuccess);
						dtPwdSMSExpr = Conversion.ToDate(objReader["PASSWD_EXPIRE"].ToString());
						objReader.Close();

						if (dtPwdSMSExpr != DateTime.MinValue && dtPwdSMSExpr < System.DateTime.Now)
                            throw new PermissionExpiredException(Globalization.GetString("Login.AuthUser.ForcedPwdExpired", base.ClientId));
						if(dtPwdSMSExpr == DateTime.MinValue)
							dtPwdSMSExpr = DateTime.MaxValue;
					}
				}
                
				SMSUser objSMSUser = null;
				bool bIsAdminUser = false;
				try
				{
                    objSMSUser = new SMSUser(sLoginName, base.ClientId);
                    bIsAdminUser = objSMSUser.IsSMSAdminUser(false, sLoginName);
				}
				finally
				{
					objSMSUser = null;
				}
				if( iUserID <= 0  || bIsAdminUser )
					return p_objXMLDoc;

				XmlNode objXMLNode = (XmlNode)p_objXMLDoc.CreateElement("Item"); 
				XmlAttribute objXmlAttribute = p_objXMLDoc.CreateAttribute("status");

				if(iForceChangePwd != 0)
				{
					objXmlAttribute.Value = "0";
                    objXMLNode.InnerText = Globalization.GetString("LoginAdaptor.CheckPasswordExpr.UserPwdExpired", base.ClientId); 
					objXMLNode.Attributes.Append(objXmlAttribute);
					p_objXMLDoc.SelectSingleNode("//Param[@name='PwdExpiredStatus']").AppendChild(objXMLNode);
					return p_objXMLDoc;
				}

				sQuery = "SELECT * FROM PASSWORD_POLICY_PARMS";
                using (DbReader objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(base.ClientId), sQuery))
				{
					if(objReader.Read())
					{
                        bool blnSuccess = false;
						iPwdExpDays = Conversion.CastToType<int>(objReader["PWD_EXPR_DAYS"].ToString(), out blnSuccess);
						iPwdExprWarnDays = Conversion.CastToType<int>(objReader["PWD_EXPR_WARNING_DAYS"].ToString(), out blnSuccess);
						objReader.Close();
					}
				}
		
				sQuery = String.Format("SELECT DTTM_CREATED FROM PASSWORD_HISTORY WHERE USER_ID = {0} ORDER BY DTTM_CREATED desc", iUserID);
                using (DbReader objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(base.ClientId), sQuery))
				{			
					if(objReader.Read())
					{
						DateTime dtDatabaseDate = Conversion.ToDate(objReader["DTTM_CREATED"].ToString());
						DateTime DTPwdExprDate = dtDatabaseDate.AddDays(iPwdExpDays);
						DateTime DTPwdExprWarnDate = dtDatabaseDate.AddDays(iPwdExpDays - iPwdExprWarnDays);
				
						
						if(DTPwdExprDate <= DateTime.Now ||  dtPwdSMSExpr <= DateTime.Now)
						{
							//Setting status & Error message for password expiration.
							objXmlAttribute.Value = "0";
                            objXMLNode.InnerText = Globalization.GetString("LoginAdaptor.CheckPasswordExpr.UserPwdExpired", base.ClientId);  
						}
						else if(DTPwdExprWarnDate <= DateTime.Now)
						{
							int iDiff =  ((TimeSpan)DTPwdExprDate.Subtract(dtPwdSMSExpr)).Days;

							if(iDiff >= 0)
								iDiff =  ((TimeSpan)dtPwdSMSExpr.Subtract(DateTime.Now)).Days;
							else if(iDiff < 0)
								iDiff =  ((TimeSpan)DTPwdExprDate.Subtract(DateTime.Now)).Days;

							//Setting status & Error message for warning of password expiration.
							objXmlAttribute.Value = "1";
							if(iDiff == 0)
								iDiff = 1;
                            objXMLNode.InnerText = String.Format(Globalization.GetString("LoginAdaptor.CheckPasswordExpr.UserPwdExpireWarningMsg", base.ClientId), iDiff);
						}
						  
					}
					else
					{ 
						PublicFunctions.UpdatePasswordHistory(iUserID,0,sPassword,"",false, base.ClientId); 
					}

					objXMLNode.Attributes.Append(objXmlAttribute);
					p_objXMLDoc.SelectSingleNode("//Param[@name='PwdExpiredStatus']").AppendChild(objXMLNode);

					if(objReader != null)
						objReader.Close();
				}
			}
			finally
			{
				
			}
			return p_objXMLDoc;
		}
//01/17/2007 REM Umesh

		/// Name		: GetUserDetails
		/// Author		: Umesh Kusvaha
		/// Date Created	: 17 Jan 2007		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		///  This function would get the Last Name and First Name of the User 
		/// </summary>
		/// <param name="p_sUserName"></param>
		/// <param name="p_sDSN"></param>
		/// <returns></returns>

		private void GetUserDetails(string p_sUserName,string p_sDSN)
		{
			string sSql="";
			string sFirstName = "";
			string sLastName = "";

			DbConnection db = null; 
			//DbReader objReader=null;
            db = DbFactory.GetDbConnection(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(base.ClientId));
			try
			{
				db.Open();
				sSql ="SELECT LAST_NAME,FIRST_NAME" ;
				sSql += " FROM USER_TABLE,USER_DETAILS_TABLE,DATA_SOURCE_TABLE";
				sSql += " WHERE USER_TABLE.USER_ID=USER_DETAILS_TABLE.USER_ID";
				sSql += " AND USER_DETAILS_TABLE.DSNID=DATA_SOURCE_TABLE.DSNID"; 
				sSql += " AND USER_DETAILS_TABLE.LOGIN_NAME='"+p_sUserName+"'";
				sSql += " AND DATA_SOURCE_TABLE.DSN='"+p_sDSN+"'";
              using(  DbReader objReader = db.ExecuteReader(sSql))
              {
				if(objReader.Read())
				{
					sLastName = objReader.GetString("LAST_NAME");
					sFirstName=objReader.GetString("FIRST_NAME");
				}
                objReader.Close();
             }
				
				SafeSaveParamXml("LastName",sLastName);
				SafeSaveParamXml("FirstName",sFirstName);
			}
			finally
			{
					if (db!=null)
                    {   db.Close();
                        db.Dispose();
                    }
					sSql="";
                    //if (objReader != null)
                    //    objReader.Close();
			}
		
		}
		//End REM Umesh

        /* Arnab: MITS-8489 - Created new method for the PaymentNotification method call */

        /// Name: GetPaymentNotificationStatus(SessionManager)
        /// Author : Arnab Mukherjee
        /// Date Craeted : 18th September' 2007
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// This method first creates the UserLogin object using SessionManager object and 
        /// extracts the ConnectionString and UserID. Then it calls PaymentNotification.NotifyUserOfFuturePayments()
        /// which returns the result XML document. Now search for the 
        /// </summary>
        /// <param name="sConnectionString">Connection String</param>
        /// <param name="iUserID">User ID</param>        
        private void GetPaymentNotificationStatus(string sConnectionString, int iUserID)
        {
            string sRecordFoundStatus = "false";            
            XmlDocument objPaymentXMLDocument = null;
            XmlElement xmlTotalNumChecks = null;           // Will point to TotalNumChecks in XMLDocument
            try
            {               
                //Callling PaymentNotification.NotifyUserOfFuturePayments() for XML document
                //PaymentNotification objPaymentNotification = new PaymentNotification(sConnectionString);
                PaymentNotification objPaymentNotification = new PaymentNotification(sConnectionString, base.ClientId);
                objPaymentXMLDocument = objPaymentNotification.NotifyUserOfFuturePayments(iUserID);
                xmlTotalNumChecks = (XmlElement)objPaymentXMLDocument.SelectSingleNode("/PaymentNotification/TotalNumChecks");

                if (xmlTotalNumChecks != null && xmlTotalNumChecks.InnerText != "0") //both of these conditions may arise for a not having the xml
                    sRecordFoundStatus = "true";
                else
                    sRecordFoundStatus = "false";

                SafeSaveParamXml("CheckPrintStatus", sRecordFoundStatus); //Appending value in node <CheckPrintStatus/> in the XML
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
            finally
            {
                sRecordFoundStatus = "false";
            }
        }
        //MITS-8489 - Method End
	}
}
