﻿using System;
using System.Xml;
using Riskmaster.Application.SupportScreens;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    /// Author  :   Gurpreet Singh Bindra
    /// Dated   :   15th, Apr 2014
    /// Purpose :   MITS#35365 to handle the person involved MDI menu.
    /// </summary>
    /// <returns></returns>
    public class PersonInvMenuConfigAdaptor : BusinessAdaptorBase
    {
        #region Public methods
        /// <summary>
        ///		This function sets the list of person involved
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml</param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool SetPersonInvMenuConfig(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PersonInvMenu objPersonInvMenu = null;
            try
            {
                objPersonInvMenu = new PersonInvMenu(base.connectionString,base.ClientId);
                p_objXmlOut = objPersonInvMenu.SetPersonInvMenuConfig(p_objXmlIn);

                return true;
            }
            catch (InvalidValueException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PersonInvMenuConfigAdaptor.SetPersonInvMenuConfig.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
        }

        /// <summary>
        ///		This function gets the list of person involved
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml</param>
        /// <param name="p_objXmlOut">Result as Output xml</param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
        public bool GetPersonInvMenuConfig(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            PersonInvMenu objPersonInvMenu = null;
            try
            {
                objPersonInvMenu = new PersonInvMenu(base.connectionString,base.ClientId);
                p_objXmlOut = objPersonInvMenu.GetPersonInvMenuConfig(p_objXmlIn);

                return true;
            }
            catch (InvalidValueException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PersonInvMenuConfigAdaptor.GetPersonInvMenuConfig.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
        }

        #endregion
    }
}
