using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.LSSInterface;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for LSSInterfaceAdaptor.
	/// </summary>
	public class LSSInterfaceAdaptor: BusinessAdaptorBase
	{

		/// Name		: LSSInterfaceAdaptor
		/// Author		: Manoj Agrawal
		/// Date Created: 09/08/2006
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		///	<summary>
		/// Constructor for the Class
		/// </summary>
		public LSSInterfaceAdaptor()
		{
		}

		/// Name		: Execute
		/// Author		: Manoj Agrawal
		/// Date Created: 09/08/2006
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		///	<summary>
		/// This method is called from Web Service
		/// </summary>
		public bool Execute(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ImportProcessFactory objImportProcessFactory = null;
			IImportProcess objImportProcess = null;

			try
			{
				objImportProcessFactory = new ImportProcessFactory(userLogin.objRiskmasterDatabase.ConnectionString, userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName,base.ClientId);
				objImportProcess = objImportProcessFactory.CreateImportProcess();

				p_objXmlOut = objImportProcess.Execute(p_objXmlIn);
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("LSSInterfaceAdaptor.Execute.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objImportProcessFactory = null;
				objImportProcess = null;
			}
			return true;
		}
	}
}
