﻿using System;
using Riskmaster.ExceptionTypes;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Application.SecurityManagement;
using Riskmaster.Db;
using System.Collections;
using Riskmaster.Security;
using System.Collections.Generic;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: RMWinSecurityAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 23-Mar-2005
	///* $Author	: Raman Bhatia
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************

	/// <summary>
	/// This class is used to call the application layer component for Riskmaster.Application.SecurityManagement.RMWinSecurity class 
	/// which contains general functions related to Security.
	/// </summary>
	/// 
	public class RMWinSecurityAdaptor :BusinessAdaptorBase
	{
		#region Constructor
		/// Name		: RMWinSecurityAdaptor
		/// Author		: Raman Bhatia
		/// Date Created: 23-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		///	<summary>
		/// Constructor for the Class
		/// </summary>
		
		public RMWinSecurityAdaptor()
		{
		}
		#endregion

		#region Public Methods

		/// Name		: ClearLicenseHistory
		/// Author		: Raman Bhatia
		/// Date Created: 23-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	This method is a wrapper to ClearLicenseHistory method which 
		///	clears the License History.
		/// </summary>
		/// <param name="p_objDocIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<Code>Code required to clear the License History.</code>
		///			</RMWinSecurity>
		///		</Document> 
		/// 
		/// <param name="p_objXmlOut">XML contains the output document
		///		The structure of output XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<Message>
		///					Message, represents whether the License history	has been cleared or not.
		///				</Message>
		///				<IsLicenseHistoryCleared>Flag for checking whether License History has been cleared</IsLicenseHistoryCleared>
		///			</RMWinSecurity>
		///		</Document>
		///	</param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool ClearLicenseHistory(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			RMWinSecurity objRMWinSecurity = null;					//object of RMWinSecurity Class
			string sCode = "";
			string sMessage= "";
			bool bIsLicenceHistoryCleared = false;
            
			try
			{
				sCode =((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='licensecode']")).InnerText;
                objRMWinSecurity = new RMWinSecurity(base.ClientId);
				//calling the ClearLicenseHistory method to clear the history.	
				bIsLicenceHistoryCleared = objRMWinSecurity.ClearLicenseHistory(sCode , ref sMessage);
				//Creating the output XML containing the required information
				p_objDocIn.SelectSingleNode("//license").Attributes["postback"].Value="true";
				if(!bIsLicenceHistoryCleared)
				{
                 p_objDocIn.SelectSingleNode("//license").Attributes["iserror"].Value="Yes";
				 //p_objDocIn.SelectSingleNode("//buttonscript[@name='btnBack']").Attributes["functionname"].Value="SetWizardAction(this,'ClearLicenseHistory')";
				}				
                 p_objDocIn.SelectSingleNode("//license").Attributes["message"].Value=sMessage;
				p_objXmlOut=p_objDocIn;
				return true;
			}
			catch(Exception p_objException)
			{
				p_objDocIn.SelectSingleNode("//license").Attributes["postback"].Value="true";
				p_objDocIn.SelectSingleNode("//license").Attributes["iserror"].Value="Yes";
				p_objDocIn.SelectSingleNode("//license").Attributes["message"].Value=p_objException.Message;
				p_objXmlOut=p_objDocIn;
				return true;
			}
			finally
			{
				objRMWinSecurity = null;
			}
          
		}

		/// Name		: LoadODBCDrivers
		/// Author		: Raman Bhatia
		/// Date Created: 23-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	This method is a wrapper to LoadODBCDrivers method which 
		///	loads the ODBC Drivers for DB Connection Wizard.
		/// </summary>
		/// <param name="p_objDocIn">No Input parameter is required hence no input XML is needed
		///		
		/// <param name="p_objXmlOut">XML contains the output document
		///		The structure of output XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<ODBCDrivers>
		///					<ODBCDriver></ODBCDriver>
		///					<ODBCDriver></ODBCDriver>
		///				</ODBCDrivers>
		///			</RMWinSecurity>
		///		</Document>
		///		ODBCDrivers would be a collction of all the ODBCDriver.
		///	</param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool LoadODBCDrivers(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			RMWinSecurity objRMWinSecurity = null;			//object of RMWinSecurity Class
			XmlElement objParentElement = null;				//used for Xml manipulation
            XmlElement objChildElement = null;				//used for Xml manipulation
            List<string> arrlstODBCDrivers = new List<string>();	//used to store the ODBCDrivers collection
			string sXmlOut = "<Document><RMWinSecurity><ODBCDrivers></ODBCDrivers></RMWinSecurity></Document>";
            
			try
			{
                objRMWinSecurity = new RMWinSecurity(base.ClientId);
				
				//calling the LoadODBCDrivers method to loads the ODBC Drivers for DB Connection Wizard.	
                arrlstODBCDrivers = objRMWinSecurity.LoadODBCDrivers();

				//Creating the output XML containing the required information
				
				p_objXmlOut.InnerXml = sXmlOut;
				objParentElement = (XmlElement)p_objXmlOut.SelectSingleNode("//ODBCDrivers");
	
				//extracting the ODBCDrivers information from the collection and adding information to output XML
				foreach(string sODBCDriver in arrlstODBCDrivers)
				{
					objChildElement = p_objXmlOut.CreateElement("ODBCDriver");
					objChildElement.InnerText = sODBCDriver.ToString();
					objParentElement.AppendChild(objChildElement);
				}
			
				return true;
			}
		
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("RMWinSecurityAdaptor.LoadODBCDrivers.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objRMWinSecurity = null;
				objChildElement = null;
				objParentElement = null;
				arrlstODBCDrivers = null;
			}
		}

		/// Name		: LoadPreDefinedDataSources
		/// Author		: Raman Bhatia
		/// Date Created: 23-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	This method is a wrapper to LoadPreDefinedDataSources method which 
		///	loads the pre-defined datasources for DB Connection Wizard.
		/// </summary>
		/// <param name="p_objDocIn">No Input parameter is required hence no input XML is needed
		/// <param name="p_objXmlOut">XML contains the output document
		///		The structure of output XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<SQLDataSources>
		///					<SQLDataSource></SQLDataSource>
		///					<SQLDataSource></SQLDataSource>
		///				</SQLDataSources>
		///			</RMWinSecurity>
		///		</Document>
		///		SQLDataSources would be a collection of all the SQLDataSource.
		///	</param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool LoadPreDefinedDataSources(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			RMWinSecurity objRMWinSecurity = null;				//object of RMWinSecurity Class
			XmlElement objParentElement = null;					//used for Xml manipulation
			XmlElement objChildElement = null;					//used for Xml manipulation
			ArrayList arrlstSQLDataSources = new ArrayList();	//used to store the collection
			string sXmlOut = "<Document><RMWinSecurity><SQLDataSources></SQLDataSources></RMWinSecurity></Document>";
            
			try
			{
                objRMWinSecurity = new RMWinSecurity(base.ClientId);
				
				//calling the LoadPreDefinedDataSources method to loads the ODBC Drivers for DB Connection Wizard.	
				objRMWinSecurity.LoadPreDefinedDataSources(ref arrlstSQLDataSources);

				//Creating the output XML containing the required information
				
				p_objXmlOut.InnerXml = sXmlOut;
				objParentElement = (XmlElement)p_objXmlOut.SelectSingleNode("//SQLDataSources");

				//extracting the SQLDataSources information from the collection and adding information to output XML
				foreach(string sSQLDataSources in arrlstSQLDataSources)
				{
					objChildElement = p_objXmlOut.CreateElement("SQLDataSource");
					objChildElement.InnerText = sSQLDataSources.ToString();
					objParentElement.AppendChild(objChildElement);
				}
			
				return true;
			}
		
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("RMWinSecurityAdaptor.LoadPreDefinedDataSources.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objRMWinSecurity = null;
				objChildElement = null;
				objParentElement = null;
				arrlstSQLDataSources = null;
			}
		}
		/// Name		: LoadDataSources
		/// Author		: Raman Bhatia
		/// Date Created: 23-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	This method is a wrapper to LoadDataSources method which 
		///	loads the datasources for DB Connection Wizard.
		/// </summary>
		/// <param name="p_objDocIn">No Input parameter is required hence no input XML is needed 
		/// <param name="p_objXmlOut">XML contains the output document
		///		The structure of output XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<DataSources>
		///					<DataSource></DataSource>
		///					<DataSource></DataSource>
		///				</DataSources>DataSources>
		///			</RMWinSecurity>
		///		</Document>
		///		DataSources would be a collection of all the DataSource.
		///	</param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool LoadDataSources(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			RMWinSecurity objRMWinSecurity = null;			//object of RMWinSecurity Class
			XmlElement objParentElement = null;				//used for Xml manipulation
			XmlElement objChildElement = null;				//used for Xml manipulation
			ArrayList arrlstDataSources = new ArrayList();	//used to store the collection
			string sXmlOut = "<Document><RMWinSecurity><DataSources></DataSources></RMWinSecurity></Document>";
            
			try
			{
                objRMWinSecurity = new RMWinSecurity(base.ClientId);
				
				//calling the Load DataSources method to loads the ODBC Drivers for DB Connection Wizard.	
				objRMWinSecurity.LoadDataSources(ref arrlstDataSources);

				//Creating the output XML containing the required information
				
				p_objXmlOut.InnerXml = sXmlOut;
				objParentElement = (XmlElement)p_objXmlOut.SelectSingleNode("//DataSources");

				//extracting the DataSources information from the collection and adding information to output XML
				foreach(string sDataSources in arrlstDataSources)
				{
					objChildElement = p_objXmlOut.CreateElement("DataSource");
					objChildElement.InnerText = sDataSources.ToString();
					objParentElement.AppendChild(objChildElement);
				}
			
				return true;
			}
		
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("RMWinSecurityAdaptor.LoadDataSources.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objRMWinSecurity = null;
				objChildElement = null;
				objParentElement = null;
				arrlstDataSources = null;
			}
		}

		/// Name		: LoadLanguages
		/// Author		: Raman Bhatia
		/// Date Created: 23-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	This method is a wrapper to LoadLanguages method which 
		///	loads the languages.
		/// </summary>
		/// <param name="p_objDocIn">No Input parameter is required hence no input XML is needed 
		/// <param name="p_objXmlOut">XML contains the output document
		///		The structure of output XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<Languages>
		///					<Language></Language>
		///					<Language></Language>
		///				</Languages>
		///			</RMWinSecurity>
		///		</Document>
		///		Languages would be a collection of all the Language.
		///	</param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool LoadLanguages(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			RMWinSecurity objRMWinSecurity = null;			//object of RMWinSecurity Class
			XmlElement objParentElement = null;				//used for Xml manipulation
			XmlElement objChildElement = null;				//used for Xml manipulation
			ArrayList arrlstLanguages = new ArrayList();	//used to store the collection
			string sXmlOut = "<Document><RMWinSecurity><Languages></Languages></RMWinSecurity></Document>";
            
			try
			{
                objRMWinSecurity = new RMWinSecurity(base.ClientId);
				
				//calling the LoadLanguages method to loads the languages.
				objRMWinSecurity.LoadLanguages(ref arrlstLanguages);

				//Creating the output XML containing the required information
				
				p_objXmlOut.InnerXml = sXmlOut;
				objParentElement = (XmlElement)p_objXmlOut.SelectSingleNode("//Languages");

				//extracting the Languages information from the collection and adding information to output XML
				foreach(string sLanguages in arrlstLanguages)
				{
					objChildElement = p_objXmlOut.CreateElement("Language");
					objChildElement.InnerText = sLanguages.ToString();
					objParentElement.AppendChild(objChildElement);
				}
			
				return true;
			}
		
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("RMWinSecurityAdaptor.LoadLanguages.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objRMWinSecurity = null;
				objChildElement = null;
				objParentElement = null;
				arrlstLanguages = null;
			}
		}
		

		/// Name		: LoadUsers
		/// Author		: Raman Bhatia
		/// Date Created: 23-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	This method is a wrapper to LoadUsers method which 
		///	loads the Users.
		/// </summary>
		/// <param name="p_objDocIn">No Input parameter is required hence no input XML is needed 
		/// <param name="p_objXmlOut">XML contains the output document
		///		The structure of output XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<Users>
		///					<User></User>
		///					<User></User>
		///				</Users>
		///			</RMWinSecurity>
		///		</Document>
		///		Users would be a collection of all the User.
		///	</param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool LoadUsers(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			RMWinSecurity objRMWinSecurity = null;			//object of RMWinSecurity Class
			XmlElement objParentElement = null;				//used for Xml manipulation
			XmlElement objChildElement = null;				//used for Xml manipulation
			ArrayList arrlstUsers = new ArrayList();		//used to store the collection
			string sXmlOut = "<Document><RMWinSecurity><Users></Users></RMWinSecurity></Document>";
            
			try
			{
                objRMWinSecurity = new RMWinSecurity(base.ClientId);
				
				//calling the LoadUsers method to loads the Users.
				objRMWinSecurity.LoadUsers(ref arrlstUsers);
					
				//Creating the output XML containing the required information
				
				p_objXmlOut.InnerXml = sXmlOut;
				objParentElement = (XmlElement)p_objXmlOut.SelectSingleNode("//Users");

				//extracting the Users information from the collection and adding information to output XML
				foreach(string sUser in arrlstUsers)
				{
					objChildElement = p_objXmlOut.CreateElement("User");
					objChildElement.InnerText = sUser.ToString();
					objParentElement.AppendChild(objChildElement);
				}
			
				return true;
			}
		
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("RMWinSecurityAdaptor.LoadUsers.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objRMWinSecurity = null;
				objChildElement = null;
				objParentElement = null;
				arrlstUsers = null;
			}
		}
		/// Name		: LoadUsersLogins
		/// Author		: Raman Bhatia
		/// Date Created: 23-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	This method is a wrapper to LoadUsersLogins method which 
		///	loads the users that are allowed to login to a datasource.
		/// </summary>
		/// <param name="p_objDocIn">No Input parameter is required hence no input XML is needed 
		/// <param name="p_objXmlOut">XML contains the output document
		///		The structure of output XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<Users>
		///					<User></User>
		///					<User></User>
		///				</Users>
		///			</RMWinSecurity>
		///		</Document>
		///		Users would be a collection of all the User who are allowed to login to a datsource.
		///	</param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool LoadUsersLogins(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			RMWinSecurity objRMWinSecurity = null;			//object of RMWinSecurity Class
			XmlElement objParentElement = null;				//used for Xml manipulation
			XmlElement objChildElement = null;				//used for Xml manipulation
			ArrayList arrlstUsersLogins = new ArrayList();  //used to store the collection
			string sXmlOut = "<Document><RMWinSecurity><Users></Users></RMWinSecurity></Document>";
            
			try
			{
                objRMWinSecurity = new RMWinSecurity(base.ClientId);
				
				//calling the LoadUsersLogins method to loads the UsersLogins.
				objRMWinSecurity.LoadUsersLogins(ref arrlstUsersLogins);

				//Creating the output XML containing the required information
				
				p_objXmlOut.InnerXml = sXmlOut;
				objParentElement = (XmlElement)p_objXmlOut.SelectSingleNode("//Users");

				//extracting the UsersLogins information from the collection and adding information to output XML
				foreach(string sUser in arrlstUsersLogins)
				{
					objChildElement = p_objXmlOut.CreateElement("User");
					objChildElement.InnerText = sUser.ToString();
					objParentElement.AppendChild(objChildElement);
				}
			
				return true;
			}
		
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("RMWinSecurityAdaptor.LoadUsersLogins.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objRMWinSecurity = null;
				objChildElement = null;
				objParentElement = null;
				arrlstUsersLogins = null;
			}
		}
		
		/// Name		: Finish
		/// Author		: Raman Bhatia
		/// Date Created: 24-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	This method is a wrapper to Finish method which is used to save the datasource
		///	related information once the user creates the datasource.
		/// </summary>
		/// <param name="p_objDocIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<UseDSN>Represents whether the ODBC Driver/Predefined datasource is being used.</UseDSN>
		///				<DSName>DS Name.</DSName>
		///				<ODBCDriverName>Driver Name.</ODBCDriverName>
		///				<UserName>User Name.</UserName>
		///				<Password>Password.</Password>
		///				<ServerName>ServerName.</ServerName>
		///				<DBFileName>DB FileName.</DBFileName>
		///				<View>Whether server/file view is being used.</View>
		///				<AdditionalParams>Additional Parameters.</AdditionalParams>
		///				<AdditionalParamsChecked>Whether Additional Parameters checked or not.</AdditionalParamsChecked>
		///				<DsnId>DSN ID.</DsnId>
		///				<NumLicenses>Number of Licenses.</NumLicenses>
		///				<datLicenseUpdtDate>License update date.</datLicenseUpdtDate>
		///			</RMWinSecurity>
		///		</Document> 
		/// 
		/// <param name="p_objXmlOut">XML contains the output document
		///		The structure of output XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<DsnId></DsnId>
		///			</RMWinSecurity>
		///		</Document>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool Finish(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			RMWinSecurity objRMWinSecurity = null;			//object of RMWinSecurity Class
			XmlElement objTargetElement = null;				//used for Xml manipulation
			string sUseDSN = "";
			string sDSName = "";
			string sODBCDriverName = "";
			string sUserName = "";
			string sPassword = "";
			string sServerName = "";
			string sDBFileName = "";
			string sView = "";
			string sAdditionalParams = "";
			string sAdditionalParamsChecked = "";
			string sXmlOut = "<Document><RMWinSecurity><DsnId></DsnId></RMWinSecurity></Document>";
			int iDsnId = 0;
			int iNumLicenses = 0;
			DateTime datLicenseUpdtDate; 

       
			try
			{
                objRMWinSecurity = new RMWinSecurity(base.ClientId);
				
				//extracting the data source information from input XML
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "UseDSN"); 
				sUseDSN = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "DSName"); 
				sDSName = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "ODBCDriverName"); 
				sODBCDriverName = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "UserName"); 
				sUserName = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "Password"); 
				sPassword = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "ServerName"); 
				sServerName = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "DBFileName"); 
				sDBFileName = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "View"); 
				sView = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "AdditionalParams"); 
				sAdditionalParams = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "AdditionalParamsChecked"); 
				sAdditionalParamsChecked = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "DsnId"); 
				iDsnId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "NumLicenses"); 
				iNumLicenses = Conversion.ConvertStrToInteger(objTargetElement.InnerText);

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "datLicenseUpdtDate");
				datLicenseUpdtDate = Conversion.ToDate(objTargetElement.InnerXml);

				//calling the Finish method to save the datasource related information
				objRMWinSecurity.Finish(sUseDSN,sDSName,sODBCDriverName,sUserName,sPassword,sServerName,sDBFileName,sView,sAdditionalParams,sAdditionalParamsChecked,ref iDsnId,iNumLicenses,datLicenseUpdtDate);
                
				//Creating the output XML containing the required information
				
				p_objXmlOut.InnerXml = sXmlOut;
				objTargetElement = (XmlElement)p_objXmlOut.SelectSingleNode("//DsnId");
				objTargetElement.InnerText = iDsnId.ToString();
				
				return true;
			}
		
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("RMWinSecurityAdaptor.Finish.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objRMWinSecurity = null;
				objTargetElement = null;
			}
		}

		/// Name		: IsConnectionValid
		/// Author		: Raman Bhatia
		/// Date Created: 24-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	This method is a wrapper to Finish method which is used to check the validity of the DSN created.
		/// </summary>
		/// <param name="p_objDocIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<UseDSN>Represents whether the ODBC Driver/Predefined datasource is being used.</UseDSN>
		///				<ODBCDriverName>Driver Name.</ODBCDriverName>
		///				<UserName>User Name.</UserName>
		///				<Password>Password.</Password>
		///				<ServerName>ServerName.</ServerName>
		///				<DBFileName>DB FileName.</DBFileName>
		///				<View>Whether server/file view is being used.</View>
		///				<CallingMethod>Represents the calling method.</CallingMethod>
		///				<AdditionalParams>Additional Parameters.</AdditionalParams>
		///				<AdditionalParamsChecked>Whether Additional Parameters checked or not.</AdditionalParamsChecked>
		///				<Message>Represents the message, if connection is valid/not</Message>
		///				<ConnectionStr>Connection string.</ConnectionStr>
		///			</RMWinSecurity>
		///		</Document> 
		/// 
		/// <param name="p_objXmlOut">XML contains the output document
		///		The structure of output XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<Message>Represents the message, if connection is valid/not</Message>
		///				<ConnectionStr></ConnectionStr>
		///				<IsConnectionValid>Flag for checking the validity of DSN created</IsConnectionValid>
		///			</RMWinSecurity>
		///		</Document>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool IsConnectionValid(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			RMWinSecurity objRMWinSecurity = null;			//object of RMWinSecurity Class
			XmlElement objTargetElement = null;				//used for Xml manipulation
			string sUseDSN = "";
			string sODBCDriverName = "";
			string sUserName = "";
			string sPassword = "";
			string sServerName = "";
			string sDBFileName = "";
			string sView = "";
			string sCallingMethod = "";
			string sAdditionalParams = "";
			string sAdditionalParamsChecked = "";
			string sMessage = "";
			string sConnectionStr = "";
			bool bIsConnectionValid = false;
			string sXmlOut = "<Document><RMWinSecurity><Message></Message><ConnectionStr></ConnectionStr><IsConnectionValid></IsConnectionValid></RMWinSecurity></Document>";
			
			try
			{
                objRMWinSecurity = new RMWinSecurity(base.ClientId);
				
				//extracting the data source information from input XML
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "UseDSN"); 
				sUseDSN = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "ODBCDriverName"); 
				sODBCDriverName = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "UserName"); 
				sUserName = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "Password"); 
				sPassword = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "ServerName"); 
				sServerName = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "DBFileName"); 
				sDBFileName = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "View"); 
				sView = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "CallingMethod"); 
				sCallingMethod = objTargetElement.InnerText;
		
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "AdditionalParams"); 
				sAdditionalParams = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "AdditionalParamsChecked"); 
				sAdditionalParamsChecked = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "Message"); 
				sMessage = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "ConnectionStr"); 
				sConnectionStr = objTargetElement.InnerText;


				//calling the IsConnectionValid method to save the datasource related information
                bIsConnectionValid = objRMWinSecurity.IsConnectionValid(sODBCDriverName, sUserName, sPassword, sServerName, sDBFileName, sView, sCallingMethod, sAdditionalParams, sAdditionalParamsChecked, ref sMessage, ref sConnectionStr);
                
				//Creating the output XML containing the required information
			
				p_objXmlOut.InnerXml = sXmlOut;
				objTargetElement = (XmlElement)p_objXmlOut.SelectSingleNode("//Message");
				objTargetElement.InnerText = sMessage;

				objTargetElement = (XmlElement)p_objXmlOut.SelectSingleNode("//ConnectionStr");
				objTargetElement.InnerText = sConnectionStr;

				objTargetElement = (XmlElement)p_objXmlOut.SelectSingleNode("//IsConnectionValid");
				objTargetElement.InnerText = bIsConnectionValid.ToString();
			
				return true;
			}
		
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("RMWinSecurityAdaptor.IsConnectionValid.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objRMWinSecurity = null;
				objTargetElement = null;
			}
		}

		/// Name		: LoadUserLoginData
		/// Author		: Raman Bhatia
		/// Date Created: 24-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	This method is a wrapper to LoadUserLoginData method which verifies the user login name & password before logging in.
		/// </summary>
		/// <param name="p_objDocIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<UserName>User Name.</UserName>
		///				<Password>Password.</Password>
		///			</RMWinSecurity>
		///		</Document> 
		/// 
		/// <param name="p_objXmlOut">XML contains the output document
		///		The structure of output XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<IsAuthorized>Flag for verifying the user credentials</IsAuthorized>
		///			</RMWinSecurity>
		///		</Document>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool LoadUserLoginData(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			RMWinSecurity objRMWinSecurity = null;			//object of RMWinSecurity Class
			XmlElement objTargetElement = null;				//used for Xml manipulation
			string sUserName = "";
			string sPassword = "";
			bool IsAuthorized = false;
			string sXmlOut = "<Document><RMWinSecurity><IsAuthorized></IsAuthorized></RMWinSecurity></Document>";
			
			try
			{
                objRMWinSecurity = new RMWinSecurity(base.ClientId);
				
				//extracting the data source information from input XML
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "UserName"); 
				sUserName = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "Password"); 
				sPassword = objTargetElement.InnerText;
				//calling the IsConnectionValid method to save the datasource related information
				IsAuthorized =  objRMWinSecurity.LoadUserLoginData(sUserName,sPassword);
                
				//Creating the output XML containing the required information
				
				p_objXmlOut.InnerXml = sXmlOut;
				objTargetElement = (XmlElement)p_objXmlOut.SelectSingleNode("//IsAuthorized");
				objTargetElement.InnerText = IsAuthorized.ToString();

				return true;
			}
		
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("RMWinSecurityAdaptor.LoadUserLoginData.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objRMWinSecurity = null;
				objTargetElement = null;
			}
		}

		/// Name		: SaveUserLoginData
		/// Author		: Raman Bhatia
		/// Date Created: 24-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	This method is a wrapper to SaveUserLoginData method which saves the user login name & password.
		/// </summary>
		/// <param name="p_objDocIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<UserName>User Name.</UserName>
		///				<Password>Password.</Password>
		///			</RMWinSecurity>
		///		</Document> 
		/// 
		/// <param name="p_objXmlOut">As the function is a wrapper to a save function 
		/// hence it would not return a XML document.
		/// </param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool SaveUserLoginData(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			RMWinSecurity objRMWinSecurity;			
			string sUserName;
			string sPassword;		
			SMSUser objAdminUser = null;
			try
			{
                objRMWinSecurity = new RMWinSecurity(base.ClientId);
				
				//extracting the data source information from input XML
				sUserName = ((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='newlogin']")).InnerText.Trim().ToLower(); 
				
				sPassword = ((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='newpasswd']")).InnerText;

                objAdminUser = new SMSUser(sUserName, base.ClientId);

				if(!objAdminUser.IsAdminUser)
				{

                    throw new RMAppException(Globalization.GetString("RMWinSecurityAdaptor.SaveUserLoginData.NotAdminError", base.ClientId));
					
				}

				objAdminUser.RmPwd =  sPassword  ;
				objAdminUser.Save() ;
				DbReader objReader = null;
				int iUserId,iDataSourceId;
				UserLogins objUserLogin= null;
                string sUserLoginName = "";      //gagnihotri MITS 11995 Changes made for Audit table
                XmlNode objXMLNode = null;   //gagnihotri MITS 11995 Changes made for Audit table
				try
				{
                    //gagnihotri MITS 11995 Changes made for Audit table Start
                    objXMLNode = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdUserLoginName']");
                    if (objXMLNode != null)
                        sUserLoginName = objXMLNode.InnerText;
                    //gagnihotri MITS 11995 Changes made for Audit table End
					//objAdminHandler.ModifySMSAdminPassword( sUserName, sPassword ) ;
					objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(base.ClientId) , "Select * from User_Details_Table where Login_name = '"+ sUserName +"'") ;
				 
					if( objReader.Read() )
					{
						iUserId = objReader.GetInt32("User_ID");
						iDataSourceId = objReader.GetInt32("DSNID") ;
						objReader.Close();
						//gagnihotri MITS 11995 Changes made for Audit table
                        //objUserLogin = new UserLogins( iUserId, iDataSourceId ) ;
                        objUserLogin = new UserLogins(iUserId, iDataSourceId, sUserLoginName, base.ClientId);
						objUserLogin.Password = sPassword ;

						objUserLogin.UpdateLoginPassword( ref objUserLogin) ;
					}

					
				}
				finally
				{
					if(objReader!=null)
					{
						if(!objReader.IsClosed)
						{
							objReader.Close();
							objReader=null;
						}
					}
					objUserLogin=null ;
					objAdminUser = null;
                    objXMLNode = null;      //gagnihotri MITS 11995 Changes made for Audit table
				}
				p_objXmlOut = p_objDocIn ;
				return true;
			}
		
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.SystemError);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("RMWinSecurityAdaptor.SaveUserLoginData.Error", base.ClientId), BusinessAdaptorErrorType.SystemError);
				return false;
			}
			finally
			{
				objRMWinSecurity = null;
				
			}

		}

		private void ValidateAdminUser(string p_sAdminLogin)
		{
			SMSUser objAdminUser = null;
			try
			{
                objAdminUser = new SMSUser(p_sAdminLogin, base.ClientId);
				if(objAdminUser.UserId.ToString().IndexOf("-")==-1)
				{
					throw new RMAppException("Specified Login Name is not a Security Admin User.") ;
				}

			}
			finally
			{
				objAdminUser = null;
			}
		}
		
		/// Name		: SaveEmailData
		/// Author		: Raman Bhatia
		/// Date Created: 24-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	This method is a wrapper to SaveEmailData method which saves the email data.
		/// </summary>
		/// <param name="p_objDocIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<SMTP>SMTP Server.</SMTP>
		///				<Email>Email Address</Email>
		///				<SecureDSN>Secure DSN/Not.</SecureDSN>
		///			</RMWinSecurity>
		///		</Document> 
		/// 
		/// <param name="p_objXmlOut">As the function is a wrapper to a save function 
		/// hence it would not return a XML document.
		/// </param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool SaveEmailData(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			RMWinSecurity objRMWinSecurity = null;			//object of RMWinSecurity Class
			XmlElement objTargetElement = null;				//used for Xml manipulation
			string sSMTP = "";
			string sEmail = "";
			bool bSecureDSN = false;
            string sUserName = "";      //gagnihotri MITS 11995 Changes made for Audit table
            //rsushilaggar Added Alternate domain & replyto domain field in the settings screen MITS 28280 
            string sAtlernateDomain = string.Empty;
            string sReplyToDomain = string.Empty;
								
			try
			{
                //gagnihotri MITS 11995 Changes made for Audit table Start
                objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdUserLoginName']");
                if (objTargetElement != null)
                    sUserName = objTargetElement.InnerText;
                //objRMWinSecurity = new RMWinSecurity();
                objRMWinSecurity = new RMWinSecurity(sUserName, base.ClientId);
                //gagnihotri MITS 11995 Changes made for Audit table End
				
				//extracting the data source information from input XML
                objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='smtpserver']");
                if (objTargetElement != null)
                    sSMTP = objTargetElement.InnerText;
                
                //sSMTP = RMConfigurationSettings.GetSMTPServer();

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='adminemailaddr']"); 
				sEmail = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='chkSecure']"); 
				bSecureDSN= Conversion.ConvertStrToBool(objTargetElement.InnerText);

                //rsushilaggar Added Alternate domain & replyto domain field in the settings screen MITS 28280 
                objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='senderalternatedomain']");
                sAtlernateDomain = objTargetElement.InnerText;

                objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='senderreplytodomain']");
                sReplyToDomain = objTargetElement.InnerText;

				//calling the SaveEmailData method to save the username and password
				objRMWinSecurity.SaveEmailData(sSMTP,sEmail,bSecureDSN,sAtlernateDomain,sReplyToDomain);
                //End rsushilaggar
                				
				return true;
			}
		
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("RMWinSecurityAdaptor.SaveEmailData.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objRMWinSecurity = null;
				objTargetElement = null;
			}
		}
        /// Name		: SaveDomainAuthenticationData
        /// Author		: Rahul Solanki
        /// Date Created: 14-Feb-2008		       

        public bool SaveDomainAuthenticationData(XmlDocument p_objDocIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            RMWinSecurity objRMWinSecurity = null;			//object of RMWinSecurity Class
            XmlElement objTargetElement = null;				//used for Xml manipulation
            string sDomainName = "";
            string sEmail = "";
            bool bDomainEnabled = false;
            string sUserName = "";      //gagnihotri MITS 11995 Changes made for Audit table

            try
            {
                //gagnihotri MITS 11995 Changes made for Audit table Start
                objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdUserLoginName']");
                if (objTargetElement != null)
                    sUserName = objTargetElement.InnerText;
                //objRMWinSecurity = new RMWinSecurity();
                objRMWinSecurity = new RMWinSecurity(sUserName, base.ClientId);
                //gagnihotri MITS 11995 Changes made for Audit table End
                                
                objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='sequenceid']");
                p_objXmlOut = p_objDocIn;
                return true;
            }

            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("RMWinSecurityAdaptor.SaveEmailData.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objRMWinSecurity = null;
                objTargetElement = null;
            }
        }

        /// Name		: AddEditDomain
        /// Author		: Rahul Solanki
        /// Date Created: 14-Feb-2008		       

        public bool AddEditDomain(XmlDocument p_objDocIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            RMWinSecurity objRMWinSecurity = null;			//object of RMWinSecurity Class
            XmlElement objTargetElement = null;				//used for Xml manipulation
            string sNewDomainName = "";
            string sOldDomainName = "";
            bool bNewDomainStatus = false;
            bool bDomainEnabled = false;
            string sUserName = "";      //gagnihotri MITS 11995 Changes made for Audit table


            try
            {
                //gagnihotri MITS 11995 Changes made for Audit table Start
                objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdUserLoginName']");
                if (objTargetElement != null)
                    sUserName = objTargetElement.InnerText;
                //objRMWinSecurity = new RMWinSecurity();
                objRMWinSecurity = new RMWinSecurity(sUserName, base.ClientId);
                //gagnihotri MITS 11995 Changes made for Audit table End

                objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//sequenceidVal");
                if (objTargetElement != null)
                    sNewDomainName = objTargetElement.InnerText;
 
                objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//oldNameVal");
                if (objTargetElement != null)
                    sOldDomainName = objTargetElement.InnerText; 

                objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//statusVal");
                if (objTargetElement != null)
                    bDomainEnabled = Conversion.ConvertStrToBool(objTargetElement.InnerText);

                objRMWinSecurity.AddEditDomain(sOldDomainName, sNewDomainName, bDomainEnabled);

                return true;
            }

            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("RMWinSecurityAdaptor.SaveEmailData.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objRMWinSecurity = null;
                objTargetElement = null;
            }
        }


		/// Name		: UpdateLicenseInfo
		/// Author		: Raman Bhatia
		/// Date Created: 24-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	This method is a wrapper to UpdateLicenseInfo method which is used to update the license information.
		/// </summary>
		/// <param name="p_objDocIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<Code>Code provided to update the information.</Code>
		///				<DsnId>DSN Id</DsnId>
		///				<Message>Message representing whether the information has been updated/not.</Message>
		///			</RMWinSecurity>
		///		</Document> 
		/// 
		/// <param name="p_objXmlOut">XML contains the output document
		///		The structure of output XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<Message></Message>
		///			</RMWinSecurity>
		///		</Document>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		

		public bool UpdateLicenseInfo(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			RMWinSecurity objRMWinSecurity = null;					//object of RMWinSecurity Class
			string sCode = "";
			string sMessage= "";
			bool bIsLicencesUpdated = false;
			int iDsnId=0;
            //gagnihotri MITS 11995 Changes made for Audit table
            string sUserName = "";
            XmlNode objXMLNode = null;
			try
			{
				//extracting the code from input Xml
				sCode =((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='licensecode']")).InnerText;
                iDsnId = Conversion.ConvertObjToInt(((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdDsnId']")).InnerText, base.ClientId); 
		        				
                //gagnihotri MITS 11995 Changes made for Audit table
                objXMLNode = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdUserLoginName']");
                if (objXMLNode != null)
                    sUserName = objXMLNode.InnerText;
                objRMWinSecurity = new RMWinSecurity(sUserName, base.ClientId);
                //objRMWinSecurity = new RMWinSecurity();
				
				//calling the ClearLicenseHistory method to clear the history.	
				bIsLicencesUpdated = objRMWinSecurity.UpdateLicenseInfo(sCode,iDsnId, ref sMessage);

				//Creating the output XML containing the required information
				p_objDocIn.SelectSingleNode("//license").Attributes["postback"].Value="true";
				if(!bIsLicencesUpdated)
				{
					p_objDocIn.SelectSingleNode("//license").Attributes["iserror"].Value="Yes";
					p_objDocIn.SelectSingleNode("//buttonscript[@name='btnUpdate']").Attributes["name"].Value="btnBack";
                    p_objDocIn.SelectSingleNode("//buttonscript[@name='btnBack']").Attributes["functionname"].Value = "SetWizardAction(this,'UpdateLicenses',event)";
					p_objDocIn.SelectSingleNode("//buttonscript[@name='btnBack']").Attributes["title"].Value="Back";
					//p_objDocIn.SelectSingleNode("//buttonscript[@name='btnBack']").Attributes["functionname"].Value="SetWizardAction(this,'Updatelicenses')";
				}
				else
				{
					p_objDocIn.SelectSingleNode("//license").Attributes["postback"].Value="true";
				}
				p_objDocIn.SelectSingleNode("//license").Attributes["message"].Value=sMessage;
				p_objXmlOut=p_objDocIn;
				return true;
			}


			catch(Exception p_objException)
			{
				p_objDocIn.SelectSingleNode("//license").Attributes["postback"].Value="true";
				p_objDocIn.SelectSingleNode("//license").Attributes["iserror"].Value="Yes";
				p_objDocIn.SelectSingleNode("//license").Attributes["message"].Value=p_objException.Message;
				p_objXmlOut=p_objDocIn;
				return true;
			}
			finally
			{
				objRMWinSecurity = null;
                objXMLNode = null;
			}
          
		}



		
		/// Name		: LoadAllModules
		/// Author		: Raman Bhatia
		/// Date Created: 23-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	This method is a wrapper to LoadAllModules method which 
		///	is used to load all the modules.
		/// </summary>
		/// <param name="p_objDocIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<RMDSN>The underlying RM DB DSN.</RMDSN>
		///				<GroupID>Group ID for which modules are to be loaded.</GroupID>
		///			</RMWinSecurity>
		///		</Document> 
		/// 
		/// <param name="p_objXmlOut">XML contains the output document
		///		The structure of output XML document would be:
		///		<Document>
		///			<RMWinSecurity>
		///				<AllModules>
		///					<Module>
		///						<ModuleInfo>Loaded module.</ModuleInfo>					
		///						<IsAllowed>An integer flag representing whether module is granted access or not</IsAllowed>
		///					</Module>
		///				</AllModules>
		///			</RMWinSecurity>
		///		</Document>
		///		AllModules would be a collection of all the Module. 
		///	</param>
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		
		public bool LoadAllModules(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			RMWinSecurity objRMWinSecurity = null;			//object of RMWinSecurity Class
			XmlElement objTargetElement = null;				//used for Xml manipulation
			XmlElement objParentElement = null;				//used for Xml manipulation	
			XmlElement objChildElement = null;				//used for Xml manipulation
			ArrayList arrlstAllModules  = new ArrayList();  //used to store the collection
			ArrayList arrlstIsAllowed = new ArrayList();    //used to store the collection
			ArrayList arrlstIsParent = new ArrayList();     //used to store the collection
			string sRMDSN = "";
			int iGroupId = 0;
			string sXmlOut = "<Document><RMWinSecurity><AllModules></AllModules></RMWinSecurity></Document>";
            
			try
			{
				objRMWinSecurity = new RMWinSecurity(base.ClientId);
				
				//extracting the user information from input XML
				
				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "RMDSN"); 
				sRMDSN = objTargetElement.InnerText;

				objTargetElement = (XmlElement)p_objDocIn.SelectSingleNode("//" + "GroupID"); 
				iGroupId = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
				
				//calling the LoadModuleGroups method to loads the languages.
				objRMWinSecurity.LoadAllModules(sRMDSN,iGroupId,ref arrlstAllModules,ref arrlstIsAllowed,ref arrlstIsParent);

				//Creating the output XML containing the required information
				
				p_objXmlOut.InnerXml = sXmlOut;
				
				//extracting the ModuleGroups information from the collection and adding information to output XML
				objParentElement = (XmlElement)p_objXmlOut.SelectSingleNode("//AllModules");

				for (int i=0;i<(arrlstAllModules.Count);i++)
				{
					objTargetElement = p_objXmlOut.CreateElement("Module");
					objParentElement.AppendChild(objTargetElement);

					objChildElement = p_objXmlOut.CreateElement("ModuleInfo");
					objChildElement.InnerText = arrlstAllModules[i].ToString();
					objTargetElement.AppendChild(objChildElement);

					objChildElement = p_objXmlOut.CreateElement("IsAllowed");
					objChildElement.InnerText = arrlstIsAllowed[i].ToString();
					objTargetElement.AppendChild(objChildElement);

					/*As in the application layer the collection arrlstIsParent is not being populated at the moment
					hence the data for the collection is not being added in the output xml.Hence the following lines
					code are commented.*/

//					objChildElement = p_objXmlOut.CreateElement("IsParent");
//					objChildElement.InnerText = arrlstIsParent[i].ToString();
//					objParentElement.AppendChild(objChildElement);

				}

				return true;
			}
		
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("RMWinSecurityAdaptor.LoadAllModules.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objRMWinSecurity = null;
				objTargetElement = null;
				objChildElement = null;
				objParentElement = null;
				arrlstAllModules  = null;
				arrlstIsAllowed = null;
				arrlstIsParent = null;
			}
		}
		#endregion

	}
}
