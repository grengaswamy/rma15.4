using System;
using Riskmaster.ExceptionTypes;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Application.SecurityManagement;
using System.Collections;

namespace Riskmaster.BusinessAdaptor
{
	/// Name		: RMModulesAdaptor
	/// Author		: Tanuj Narula
	/// Date Created: 22-Mar-2005		
	///************************************************************
	/// Amendment History
	///************************************************************
	/// Date Amended   *   Amendment   *    Author
	///************************************************************
	/// <summary>
	/// Contains the methods & properties related to modules.
	/// </summary>
	public class RMModulesAdaptor:BusinessAdaptorBase
	{
		/// Name		: RMModulesAdaptor
		/// Author		: Tanuj Narula
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default constructor
		/// </summary>
		public RMModulesAdaptor()
		{
		}
		/// Name		: Load
		/// Author		: Tanuj Narula
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Loads various modules/submodules based on certain conditions.
		/// </summary>
		/// <param name="p_objDocIn">Nothing</param>
		/// <param name="p_objXmlOut">
		/// Structure of the output xml-:
		/// <Document>
		/// <RMModule>
		/// <ID></ID>
		/// <EntryType></EntryType>
		/// <ModuleName></ModuleName>
		/// <ParentID></ParentID>
		/// </RMModule>
		/// <RMModule>
		/// <ID></ID>
		/// <EntryType></EntryType>
		/// <ModuleName></ModuleName>
		/// <ParentID></ParentID>
		/// </RMModule>
		/// </Document>
		/// </param>
		/// <param name="p_objErrOut">This collection will contain errors during the function execution.</param>
		/// <returns>True if successful, false if fails.</returns>
		public bool Load(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
           ArrayList arrlstRMModules=null;
		   XmlElement objRootElement=null;
		   XmlElement objTempElement=null;
		   RMModules objRMModules=null;
		   string sRMModule="<ID></ID><EntryType></EntryType><ModuleName></ModuleName><ParentID></ParentID>"; 
			try
			{
				arrlstRMModules=new ArrayList();
				objRMModules=new RMModules(base.connectionString, base.ClientId);
				objRMModules.Load(ref arrlstRMModules);
				objRootElement = p_objXmlOut.CreateElement("Document");
				foreach(RMModules objRMModule in arrlstRMModules)
				{
					
					objTempElement=p_objXmlOut.CreateElement("RMModule");
					objTempElement.InnerXml=sRMModule;

					objTempElement.SelectSingleNode("//"+ "ID").InnerText=Conversion.ConvertObjToStr(objRMModule.ID);
					objTempElement.SelectSingleNode("//"+ "EntryType").InnerText=Conversion.ConvertObjToStr(objRMModule.EntryType);
					objTempElement.SelectSingleNode("//"+ "ModuleName").InnerText=objRMModule.ModuleName;
					objTempElement.SelectSingleNode("//"+ "ParentID").InnerText=Conversion.ConvertObjToStr(objRMModule.ParentID);

					objRootElement.AppendChild(objTempElement);
					p_objXmlOut.AppendChild(objRootElement);
                
				}
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				arrlstRMModules=null;
				objRootElement=null;
				objTempElement=null;
				objRMModules=null;
			}
		}
	}
	
}
