﻿
using System;
using Riskmaster.ExceptionTypes;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Application.SecurityManagement;
using System.Collections;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: ModuleGroupsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 22-Mar-2005
	///* $Author	: Tanuj Narula
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>
	///  Contains the methods & properties related to ModuleGroups.
	/// </summary>
	public class ModuleGroupsAdaptor: BusinessAdaptorBase
	{
		#region Constructor
		/// Name		: ModuleGroupsAdaptor
		/// Author		: Tanuj Narula
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default constructor
		/// </summary>
		public ModuleGroupsAdaptor()
		{
		}
		#endregion
		/// Name		: AddPermissions
		/// Author		: Tanuj Narula
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function is used to get the allowed modules from the 
		/// GROUP_PERMISSIONS table.
		/// </summary>
		/// <param name="p_objDocIn">
		/// Structure of the input xml-:
		/// <Document>
		/// <GroupID>
		/// </GroupID>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">
		/// Structure of the output xml-:
		/// <AddPermissions>
		///   <AllowedModules>
		///   </AllowedModules>
		/// </AddPermissions>
		/// </param>
		/// <param name="p_objErrOut">This collection will contain errors during the function execution.</param>
		/// <returns>True if successful, false if fails.</returns>
		public bool AddPermissions(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			string sAllowedModules="";
			ModuleGroups objGroups=null;
			XmlElement objRootElement=null;
			XmlElement objTempElement=null;
			try
			{
                objGroups = new ModuleGroups(base.connectionString, base.ClientId);
                objGroups.AddPermissions(Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//" + "GroupID"), base.ClientId), ref sAllowedModules);
				objRootElement=p_objXmlOut.CreateElement("AddPermissions");
				objTempElement=p_objXmlOut.CreateElement("AllowedModules");
				objTempElement.InnerText=sAllowedModules;
				objRootElement.AppendChild(objTempElement);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ModuleGroupsAdaptor.AddPermissions.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				 objGroups=null;
				 objRootElement=null;
				 objTempElement=null;
			}
		}
		/// Name		: Remove
		/// Author		: Tanuj Narula
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Used to remove a particular group from all the related tables.
		/// </summary>
		/// <param name="p_objDocIn">
		///  Structure of the input xml-:
		/// <Document>
		/// <ModuleGroupID>
		/// </ModuleGroupID>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">Nothing</param>
		/// <param name="p_objErrOut">This collection will contain errors during the function execution.</param>
		/// <returns>True if successful, false if fails.</returns>
		public bool Remove(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ModuleGroups objGroups=null;
			try
			{
                objGroups = new ModuleGroups(base.connectionString, base.ClientId);
                objGroups.ModuleGroupID = Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//" + "ModuleGroupID"), base.ClientId);
				objGroups.Remove();
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ModuleGroupsAdaptor.Remove.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				objGroups=null;
			}
		}
		/// Name		: Load
		/// Author		: Tanuj Narula
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Used to load all the module groups.
		/// </summary>
		/// <param name="p_objDocIn"></param>
		/// <param name="p_objXmlOut">
		/// <Document>
		///  <Modules>
		///    <Module>
		///    </Module>
		///    <Module>
		///    </Module>
		/// </Modules>
		/// <Groups>
		///   <Group>
		///    <GroupId>
		///    </GroupId>
		///    <GroupName>
		///    </GroupName>
		///   </Group>
		///   <Group>
		///   </Group>
		/// </Groups>
		/// </Document>
		/// </param>
		/// <param name="p_objErrOut">This collection will contain errors during the function execution.</param>
		/// <returns>True if successful, false if fails.</returns>
		public bool Load(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ModuleGroups objGroups=null;
		    ArrayList arrlstModules=null;
			ArrayList arrlstColGroups=null;
			string sRetXml="<Document>"+
				           "<Modules></Modules>" +
                           "<Groups></Groups>"+
				           "</Document>";
			XmlElement objTempElement=null;
			try
			{
				p_objXmlOut.LoadXml(sRetXml);
				arrlstModules=new ArrayList();
				arrlstColGroups=new ArrayList();
                objGroups = new ModuleGroups(base.connectionString, base.ClientId);
				objGroups.Load(ref arrlstModules,ref arrlstColGroups);
				foreach(string sModule in arrlstModules)
				{
					objTempElement=p_objXmlOut.CreateElement("Module");
					objTempElement.InnerText=sModule;
					p_objXmlOut.SelectSingleNode("//" + "Modules").AppendChild(objTempElement);
				}

				foreach(ModuleGroups objMdGroup in arrlstColGroups)
				{
					objTempElement=p_objXmlOut.CreateElement("Group");
					objTempElement.InnerXml="<GroupId></GroupId><GroupName></GroupName>";
					objTempElement.SelectSingleNode("//" + "GroupId").InnerText=objMdGroup.ModuleGroupID.ToString();
					objTempElement.SelectSingleNode("//" + "GroupName").InnerText=objMdGroup.GroupName;
					p_objXmlOut.SelectSingleNode("//" + "Groups").AppendChild(objTempElement);

				}
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
			   objGroups=null;
			   arrlstModules=null;
			   arrlstColGroups=null;
			   objTempElement=null;
			}
		}


        //abansal23: Clone for module security groups Starts
        /// <summary>
        /// This method is a wrapper to Riskmaster.Application.SecurityManagement.ModuleGroups.Get() method.	
        /// </summary>
        /// <param name="p_objDocIn">Input XML document</param>
        /// <param name="p_objXmlOut">XML containing the results</param>
        /// <param name="p_objErrOut">Collection of Errors/Messages</param>
        /// <returns>True/False for success or failure of the function</returns>
        public bool Get(XmlDocument p_objDocIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ModuleGroups objGroups = null;
            string sDsnId = "";
            string connString = "";

            try
            {
                sDsnId = ((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdDBId']")).InnerText;
                connString = PublicFunctions.GetConnStr(Conversion.ConvertObjToInt(sDsnId, base.ClientId), base.ClientId);
                objGroups = new ModuleGroups(connString, base.ClientId);
                p_objXmlOut = objGroups.Get(p_objDocIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;

            }
            finally
            {
                objGroups = null;
            }


        }

        //abansal23: Clone for module security groups Ends




		/// Name		: Add
		/// Author		: Tanuj Narula
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Used to add a module group.
		/// </summary>
		/// <param name="p_objDocIn">
		///  Structure of the input xml-:
		/// <Document>
		/// <GroupName>
		/// </GroupName>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">Nothing</param>
		/// <param name="p_objErrOut">This collection will contain errors during the function execution.</param>
		/// <returns>True if successful, false if fails.</returns>
		public bool Add(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ModuleGroups objGroups=null;
            string sUserName = "";      //gagnihotri MITS 11995 Changes made for Audit table
            XmlNode objXMLNode = null;  //gagnihotri MITS 11995 Changes made for Audit table
			try
			{
                //gagnihotri MITS 11995 Changes made for Audit table Start
                objXMLNode = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdUserLoginName']");
                if (objXMLNode != null)
                    sUserName = objXMLNode.InnerText;
				//objGroups=new ModuleGroups(base.connectionString);
                objGroups = new ModuleGroups(base.connectionString, sUserName, base.ClientId);
                //gagnihotri MITS 11995 Changes made for Audit table End
				objGroups.Add(p_objDocIn.SelectSingleNode("//" + "GroupName").InnerText);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				objGroups=null;
                objXMLNode = null;
			}
		}
		/// Name		: GetMaxGroupID
		/// Author		: Tanuj Narula
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Used to get the maximum group ID from USER_GROUPS table.
		/// </summary>
		/// <param name="p_objDocIn">
		/// Structure of the input xml-:
		/// <Document>
		/// <ModuleGroupID>
		/// </ModuleGroupID>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">
		/// <MaxGroupID>
		/// </MaxGroupID>
		/// </param>
		/// <param name="p_objErrOut">This collection will contain errors during the function execution.</param>
		/// <returns>True if successful, false if fails.</returns>
		public bool GetMaxGroupID(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ModuleGroups objGroups=null;
			XmlElement objRoot=null;
			int iMaxId=0;
			try
			{
                objGroups = new ModuleGroups(base.connectionString, base.ClientId);
				objGroups.GetMaxGroupID(ref iMaxId);
				objRoot=p_objXmlOut.CreateElement("MaxGroupID");
				objRoot.InnerText=iMaxId.ToString();
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				 objGroups=null;
				 objRoot=null;
			}
		}
			/// Name		: SaveUsers
			/// Author		: Tanuj Narula
			/// Date Created: 22-Mar-2005		
			///************************************************************
			/// Amendment History
			///************************************************************
			/// Date Amended   *   Amendment   *    Author
			///************************************************************
	        /// <summary>
		    /// Used to save the users, that are permitted under a given module group.
		    /// </summary>
		    /// <param name="p_objDocIn">
		    /// Structure of the input xml-:
		    /// <Document>
		    ///   <GroupID>
		    ///   </GroupID>
		    ///   <UserID>
		    ///   </UserID>
		    /// </Document>
		    /// </param>
		    /// <param name="p_objXmlOut">Nothing</param>
		    /// <param name="p_objErrOut">This collection will contain errors during the function execution.</param>
		    /// <returns>True if successful, false if fails.</returns>
		public bool SaveUsers(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
			{
				ModuleGroups objGroups=null;
                string sUserName = "";      //gagnihotri MITS 11995 Changes made for Audit table
                XmlNode objXMLNode = null;  //gagnihotri MITS 11995 Changes made for Audit table
				try
                {
                    //gagnihotri MITS 11995 Changes made for Audit table Start
                    objXMLNode = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdUserLoginName']");
                    if (objXMLNode != null)
                        sUserName = objXMLNode.InnerText;
                    //objGroups=new ModuleGroups(base.connectionString);
                    objGroups = new ModuleGroups(base.connectionString, sUserName, base.ClientId);
                    //gagnihotri MITS 11995 Changes made for Audit table End
					objGroups.SaveUsers(Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//" + "GroupID").InnerText, base.ClientId),
                                        Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//" + "UserID").InnerText, base.ClientId));
					return true;
				}
				catch(RMAppException p_objException)
				{
					p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
					return false;
				}
				catch(Exception p_objException)
				{
                    p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;

				}
				finally
				{
					objGroups=null;
				}
			}
		/// Name		: SaveObject
		/// Author		: Tanuj Narula
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///Called from Add(), used to add a module group.
		/// </summary>
		/// <param name="p_objDocIn">
		/// Structure of the input xml-:
		/// <Document>
		///   <MGName>
		///   </MGName>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">Nothing</param>
		/// <param name="p_objErrOut">This collection will contain errors during the function execution.</param>
		/// <returns>True if successful, false if fails.</returns>
		public bool SaveObject(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ModuleGroups objGroups=null;
			string sEntityType="";
			string sEntityId="";
			string sConnStr="";
			string sMGrpName="";
			int iAddedGrpId=0;
			int iDsnId;
            string sUserName = "";      //gagnihotri MITS 11995 Changes made for Audit table
            XmlNode objXMLNode = null;  //gagnihotri MITS 11995 Changes made for Audit table
            string sCloneId = "";       //abansal23: Clone for module security groups 
			try
			{ 
				sEntityType=((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='entitytype']")).InnerText;
				sEntityId=((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='selectedentityid']")).InnerText;
                sMGrpName=((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='groupname']")).InnerText;
				switch (sEntityType)
				{
					case "Datasource":case "NoData":
                        sConnStr = PublicFunctions.GetConnStr(Conversion.ConvertObjToInt(sEntityId, base.ClientId), base.ClientId);
						 iDsnId=Conversion.ConvertObjToInt(sEntityId, base.ClientId);
						break;
					case "Root Node Module Security Groups":
                    case "Module Security Group":
                    case "GrpModuleAccessPermission":
						
						string[] arrIds=null;
						string sDsnId="";
						try
						{
							arrIds =PublicFunctions.BreakDbId(sEntityId);
							sDsnId= arrIds[0];
                            sConnStr = PublicFunctions.GetConnStr(Conversion.ConvertObjToInt(sDsnId, base.ClientId), base.ClientId);
							iDsnId=Conversion.ConvertObjToInt(sDsnId, base.ClientId);
	
						}
						catch(Exception p_objException)
						{
							throw p_objException;
						}
						finally
						{
							arrIds=null;
							sDsnId=null;
						}
				
						break;
					   default:
                       throw new RMAppException("Select a valid entity for adding a module group.");
						
					
				}
                //gagnihotri MITS 11995 Changes made for Audit table Start
                objXMLNode = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdUserLoginName']");
                if (objXMLNode != null)
                    sUserName = objXMLNode.InnerText;
				//objGroups=new ModuleGroups(sConnStr);
                objGroups = new ModuleGroups(sConnStr, sUserName, base.ClientId);
                //gagnihotri MITS 11995 Changes made for Audit table End
				bool bRetVal=objGroups.ModuleGrpAlreadyExists(sMGrpName.Trim());
				if(!bRetVal)
				{
					objGroups.SaveObject(sMGrpName,ref iAddedGrpId);
                    //abansal23: Clone for module security groups Starts
                    sCloneId = ((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdCloneModuleId']")).InnerText;
                    if (sCloneId != "")
                    {
                        GroupModulePermission oGMPermission = new GroupModulePermission(sUserName, base.ClientId); //Ash - cloud
                        oGMPermission.CopyClonePermissions(p_objDocIn, Convert.ToInt16(sCloneId), iAddedGrpId);
                    }
                    
                    //abansal23: Clone for module security groups Ends
					((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdNewDBId']")).InnerText=iAddedGrpId.ToString();
					((XmlElement)p_objDocIn.SelectSingleNode("//form")).Attributes["postback"].Value="true";
					((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='selectedentityid']")).InnerText=iDsnId.ToString()+","+iAddedGrpId.ToString();
				}
				else
				{  					
					((XmlElement)p_objDocIn.SelectSingleNode("//form")).Attributes["postback"].Value="true";
					((XmlElement)p_objDocIn.SelectSingleNode("//form")).Attributes["iserror"].Value="Yes";
					((XmlElement)p_objDocIn.SelectSingleNode("//form")).Attributes["closeonpostbackifnoerr"].Value="false";
					((XmlElement)p_objDocIn.SelectSingleNode("//form")).Attributes["message"].Value="Group with same name is already defined.\n Please specify a different name.";
					 //p_objDocIn.SelectSingleNode("//buttonscript[@name='btnOk']").Attributes["name"].Value= "btnBack";
					//p_objDocIn.SelectSingleNode("//buttonscript[@name='btnBack']").Attributes["title"].Value= "< Back";//"SetWizardAction(this,'ClearLicenseCall')";
				}

				
                 p_objXmlOut=p_objDocIn;
				return true;
			}
			catch(Exception p_objException)
			{
				((XmlElement)p_objDocIn.SelectSingleNode("//form")).Attributes["postback"].Value="true";
				((XmlElement)p_objDocIn.SelectSingleNode("//form")).Attributes["iserror"].Value="Yes";
				((XmlElement)p_objDocIn.SelectSingleNode("//form")).Attributes["closeonpostbackifnoerr"].Value="false";
				((XmlElement)p_objDocIn.SelectSingleNode("//form")).Attributes["message"].Value=p_objException.Message;
				p_objDocIn.SelectSingleNode("//buttonscript[@name='btnOk']").Attributes["name"].Value= "btnBack";
				p_objDocIn.SelectSingleNode("//buttonscript[@name='btnBack']").Attributes["title"].Value= "< Back";
				p_objXmlOut=p_objDocIn;
				return true;

			}
			finally
			{
				objGroups=null;
                objXMLNode=null;
			}
		}
		/// Name		: DeleteUsers
		/// Author		: Tanuj Narula
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Used to delete the user permitted under a group.
		/// </summary>
		/// <param name="p_objDocIn">
		/// Structure of the input xml-:
		/// <Document>
		///   <GroupID>
		///   </GroupID>
		///   <UserID>
		///   </UserID>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">Nothing</param>
		/// <param name="p_objErrOut">This collection will contain errors during the function execution.</param>
		/// <returns>True if successful, false if fails.</returns>
		public bool DeleteUsers(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ModuleGroups objGroups=null;
			try
			{
                objGroups = new ModuleGroups(base.connectionString, base.ClientId);
				objGroups.DeleteUsers(Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//" + "GroupID").InnerText, base.ClientId),
                    Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//" + "UserID").InnerText, base.ClientId));
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				objGroups=null;
			}
		}
		
		/// Name		: IsParent
		/// Author		: Tanuj Narula
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Used to determine whether the p_iID passed to this function has any parent.
		/// If yes, p_iParentID which is passed by ref gives that parent ID.
		/// </summary>
		/// <param name="p_objDocIn">
		/// Structure of the input xml-:
		/// <Document>
		/// <Id>
		/// </Id>
		/// <ChildNodes>
		/// <ChildId>
		/// </ChildId>
		/// <ChildId>
		/// </ChildId>
		/// </ChildNodes>
		/// <ParentNodes>
		/// <ParentId>
		/// </ParentId>
		/// <ParentId>
		/// </ParentId>
		/// </ParentNodes>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">
		/// Structure of the output xml-:
		/// <Document>
		///   <IsParent>
		///   </IsParent>
		///   <ParentID>
		///   </ParentID>
		/// </Document>
		/// </param>
		/// <param name="p_objErrOut">This collection will contain errors during the function execution.</param>
		/// <returns>True if successful, false if fails.</returns>
		public bool IsParent(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ModuleGroups objGroups=null;
			bool bRetVal=false;
			string[] arrChild=null;
			string[] arrParent=null;
			int iParentID=0;
			int iChildNodes=0;
            int iParentNodes=0;
			try
			{
				iChildNodes=p_objDocIn.SelectSingleNode("//"+"ChildNodes").SelectNodes("//"+ "ChildId").Count;
				iParentNodes=p_objDocIn.SelectSingleNode("//"+"ParentNodes").SelectNodes("//"+ "ParentId").Count;
                
				arrChild=new string[iChildNodes];
				arrParent=new string[iParentNodes];
                
				for(int iTmp=0;iTmp<iChildNodes;iTmp++)
				{
					arrChild[iTmp]=p_objDocIn.SelectSingleNode("//"+"ChildNodes").SelectNodes("//"+ "ChildId")[iTmp].InnerText;
				}
				for(int iTmp=0;iTmp<iParentNodes;iTmp++)
				{
					arrParent[iTmp]=p_objDocIn.SelectSingleNode("//"+"ParentNodes").SelectNodes("//"+ "ParentId")[iTmp].InnerText;
				}
                objGroups = new ModuleGroups(base.connectionString, base.ClientId);
                bRetVal = objGroups.IsParent(Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//" + "Id").InnerText, base.ClientId),
											arrChild,
											arrParent,
											ref iParentID);

		     	p_objXmlOut.LoadXml("<Document><IsParent></IsParent><ParentID></ParentID></Document>");

				if(bRetVal)
				{
                 p_objXmlOut.SelectSingleNode("//" + "IsParent").InnerText="1";
				}
				else
				{
					p_objXmlOut.SelectSingleNode("//" + "IsParent").InnerText="0";
				}
				p_objXmlOut.SelectSingleNode("//" + "ParentID").InnerText=iParentID.ToString();
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				objGroups=null;
				arrChild=null;
				arrParent=null;
			}
		}
		/// Name		: Rename
		/// Author		: Tanuj Narula
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Used to rename the module group.
		/// </summary>
		/// <param name="p_objDocIn">
		/// Structure of the input xml-:
		/// <Document>
		///   <NewMGName>
		///   </NewMGName>
		///   <GroupID>
		///   </GroupID>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut"></param>
		/// <param name="p_objErrOut">This collection will contain errors during the function execution.</param>
		/// <returns>True if successful, false if fails.</returns>
		public bool Rename(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ModuleGroups objGroups;
			string sEntityType;
			string sEntityId;
			string sConnStr;
			string sNewMGrpName;
			string[] arrIds;
			string sDsnId;
            string sUserName = "";      //gagnihotri MITS 11995 Changes made for Audit table
            XmlNode objXMLNode = null;  //gagnihotri MITS 11995 Changes made for Audit table
			try
			{ 
				
				sEntityType=((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='entitytype']")).InnerText;
				sEntityId=((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='selectedentityid']")).InnerText;
				sNewMGrpName=((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='groupname']")).InnerText;
				if(sEntityType!="Module Security Group" && sEntityType!="GrpModuleAccessPermission")
				{
					throw new RMAppException("Select Module Group entity to rename.");
				}
			
				arrIds =PublicFunctions.BreakDbId(sEntityId);
				sDsnId= arrIds[0];
                sConnStr = PublicFunctions.GetConnStr(Conversion.ConvertObjToInt(sDsnId, base.ClientId), base.ClientId);
                //gagnihotri MITS 11995 Changes made for Audit table Start
                objXMLNode = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdUserLoginName']");
                if (objXMLNode != null)
                    sUserName = objXMLNode.InnerText;
                //objGroups = new ModuleGroups(sConnStr);
                objGroups = new ModuleGroups(sConnStr, sUserName, base.ClientId);
                //gagnihotri MITS 11995 Changes made for Audit table End
				if(objGroups.ModuleGrpAlreadyExists(sNewMGrpName))
				{
                    throw new RMAppException(Globalization.GetString("ModuleGroupsAdaptor.Rename.Error", base.ClientId));
				}
				
				objGroups.Rename(sNewMGrpName,Conversion.ConvertObjToInt(arrIds[1], base.ClientId));
				((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdNewDBId']")).InnerText=arrIds[1];
				((XmlElement)p_objDocIn.SelectSingleNode("//form")).Attributes["postback"].Value="true";
				((XmlElement)p_objDocIn.SelectSingleNode("//control[@name='selectedentityid']")).InnerText=sDsnId+","+arrIds[1];
				 p_objXmlOut=p_objDocIn;
				return true;
			}
            //Parijat---MITS 11694
            //catch(RMAppException p_objException)
            //{
            //    p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.SystemError);
            //    return false;
            //}
			catch(Exception p_objException)
			{
                //Parijat---MITS 11694
                ((XmlElement)p_objDocIn.SelectSingleNode("//form")).Attributes["postback"].Value = "true";
                ((XmlElement)p_objDocIn.SelectSingleNode("//form")).Attributes["iserror"].Value = "Yes";
                ((XmlElement)p_objDocIn.SelectSingleNode("//form")).Attributes["closeonpostbackifnoerr"].Value = "false";
                ((XmlElement)p_objDocIn.SelectSingleNode("//form")).Attributes["message"].Value = p_objException.Message;
                //p_objDocIn.SelectSingleNode("//buttonscript[@name='btnOk']").Attributes["name"].Value = "btnBack";
                //p_objDocIn.SelectSingleNode("//buttonscript[@name='btnBack']").Attributes["title"].Value = "< Back";
                p_objXmlOut = p_objDocIn;
                //p_objErrOut.Add(p_objException,Globalization.GetString("OSHALibAdapter.RunReport.Error"),BusinessAdaptorErrorType.SystemError);
                //return false;
                return true;

			}
			finally
			{
				objGroups=null;
				arrIds=null;
                objXMLNode=null;
			}
		
		
			
		}
		/// Name		: UpdateModuleAccessPermissions
		/// Author		: Tanuj Narula
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///  Updates the module access permissions.
		/// </summary>
		/// <param name="p_objDocIn">
		/// Structure of the input xml-:
		/// <Document>
		///   <NewMGName>
		///   </NewMGName>
		///   <GroupID>
		///   </GroupID>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">Nothing</param>
		/// <param name="p_objErrOut">This collection will contain errors during the function execution.</param>
		/// <returns>True if successful, false if fails.</returns>
		public bool UpdateModuleAccessPermissions(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
            ModuleGroups objGroups = null;
            string sUserName = "";      //gagnihotri MITS 11995 Changes made for Audit table
            XmlNode objXMLNode = null;  //gagnihotri MITS 11995 Changes made for Audit table
			try
            {
                //gagnihotri MITS 11995 Changes made for Audit table Start
                objXMLNode = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdUserLoginName']");
                if (objXMLNode != null)
                    sUserName = objXMLNode.InnerText;
				//objGroups=new ModuleGroups(base.connectionString);
                objGroups = new ModuleGroups(base.connectionString, sUserName, base.ClientId);
                //gagnihotri MITS 11995 Changes made for Audit table End

                objGroups.UpdateModuleAccessPermissions(Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//" + "GroupID").InnerText, base.ClientId));

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				objGroups=null;
                objXMLNode = null;
			}
		}
		/// Name		: SetChildNodes
		/// Author		: Tanuj Narula
		/// Date Created: 22-Mar-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function is used to set the child nodes for every node in the module
		/// access permissions.
		/// </summary>
		/// <param name="p_objDocIn">
		/// Structure of the input xml-:
		/// <Document>
		/// <ID>
		/// </ID>
		/// <AllId>
		/// <Id>
		/// </Id>
		/// <Id>
		/// </Id>
		/// </AllId>
		/// <AllParentID>
		/// <Id>
		/// </Id>
		/// <Id>
		/// </Id>
		/// </AllParentID>
		/// </Document>
		/// </param>
		/// <param name="p_objXmlOut">
		/// Structur of the output xml-:
		/// <Modules>
		/// </Modules>
		/// </param>
		/// <param name="p_objErrOut">This collection will contain errors during the function execution.</param>
		/// <returns>True if successful, false if fails.</returns>
		public bool SetChildNodes(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{	
			ModuleGroups objGroups=null;
			int iID=0;
			int[] arrAllID;
			int[] arrAllParentID;
			int iAllId=0;
			int iAllParentId=0;
			XmlElement objRootNode=null;
			string sModules="";
			try
			{
                iID = Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//" + "ID").InnerText, base.ClientId);
				iAllId = p_objDocIn.SelectSingleNode("//"+"AllId").SelectNodes("//"+ "Id").Count;
				iAllParentId = p_objDocIn.SelectSingleNode("//"+"AllParentID").SelectNodes("//"+ "Id").Count;
                
				arrAllID=new int[iAllId];
				arrAllParentID=new int[iAllParentId];
                
				for(int iTmp=0;iTmp<iAllId;iTmp++)
				{
                    arrAllID[iTmp] = Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//" + "AllId").SelectNodes("//" + "Id")[iTmp].InnerText, base.ClientId);
				}
				for(int iTmp=0;iTmp<iAllParentId;iTmp++)
				{
                    arrAllParentID[iTmp] = Conversion.ConvertObjToInt(p_objDocIn.SelectSingleNode("//" + "AllParentID").SelectNodes("//" + "Id")[iTmp].InnerText, base.ClientId);
				}
                objGroups = new ModuleGroups(base.connectionString, base.ClientId); //Ash - cloud
				objGroups.SetChildNodes(iID, arrAllID,arrAllParentID,ref sModules);

				objRootNode=p_objXmlOut.CreateElement("Modules");
				objRootNode.InnerText = sModules;
				p_objXmlOut.AppendChild(objRootNode);
				
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				 objGroups=null;
				 arrAllID=null;
				 arrAllParentID=null;
				 objRootNode=null;
			}
		}

        /// <summary>
        /// get the permissions for child function nodes.
        /// </summary>
        /// <param name="p_objDocIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetChildPermissions(XmlDocument p_objDocIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            try
            {
                GroupModulePermission oGMPermission = new GroupModulePermission(base.ClientId); //Ash - cloud
                oGMPermission.GetChildPermissions(p_objDocIn, ref p_objXmlOut);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ModuleGroupsAdaptor.GetModulePermision.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Update the permissions for the module.
        /// </summary>
        /// <param name="p_objDocIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool UpdateModulePermissions(XmlDocument p_objDocIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sUserName = "";      //gagnihotri MITS 11995 Changes made for Audit table
            XmlNode objXMLNode = null;  //gagnihotri MITS 11995 Changes made for Audit table
            try
            {
                //gagnihotri MITS 11995 Changes made for Audit table Start
                objXMLNode = (XmlElement)p_objDocIn.SelectSingleNode("//control[@name='hdUserLoginName']");
                if (objXMLNode != null)
                    sUserName = objXMLNode.InnerText;
                //GroupModulePermission oGMPermission = new GroupModulePermission();
                GroupModulePermission oGMPermission = new GroupModulePermission(sUserName, base.ClientId); //Ash - cloud
                //gagnihotri MITS 11995 Changes made for Audit table End
                oGMPermission.UpdateModulePermissions(p_objDocIn, ref p_objXmlOut);
                oGMPermission.GetChildPermissions(p_objDocIn, ref p_objXmlOut);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ModuleGroupsAdaptor.UpdateModulePermision.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objXMLNode = null;
            }
            return true;
        }
	}
}
