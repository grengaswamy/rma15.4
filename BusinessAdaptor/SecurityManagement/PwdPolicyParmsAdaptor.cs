using System;
using System.Xml;
using Riskmaster.ExceptionTypes;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.SecurityManagement;
using Riskmaster.Common;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for PwdPolicyParmsAdaptor.
	/// </summary>
	public class PwdPolicyParmsAdaptor: BusinessAdaptorBase

	{
		public PwdPolicyParmsAdaptor()
		{
		}

		public bool LoadParmsData(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{   
			Riskmaster.Application.SecurityManagement.PwdPolicyParms objPwdPolicyParms=null;
			try
			{
                objPwdPolicyParms = new PwdPolicyParms(base.ClientId);
				p_objXmlOut = objPwdPolicyParms.LoadParmsData(p_objDocIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PwdPolicyParmsAdaptor.LoadParmsData.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				
			}
		}

		public bool SaveParmsData(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{   
			Riskmaster.Application.SecurityManagement.PwdPolicyParms objPwdPolicyParms=null;
			try
			{
                objPwdPolicyParms = new PwdPolicyParms(base.ClientId);
				p_objXmlOut = objPwdPolicyParms.SaveParmsData(p_objDocIn);
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PwdPolicyParmsAdaptor.SaveParmsData.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				
			}
		}

		public bool GetUsersList(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{   
			Riskmaster.Application.SecurityManagement.PwdPolicyParms objPwdPolicyParms=null;
			try
			{
                objPwdPolicyParms = new PwdPolicyParms(base.ClientId);
				objPwdPolicyParms.GetUsersList(ref p_objDocIn);
				p_objXmlOut = p_objDocIn;
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PwdPolicyParmsAdaptor.GetUsersList.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				
			}
		}

		public bool UnlockUserAccount(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{   
			Riskmaster.Application.SecurityManagement.PwdPolicyParms objPwdPolicyParms=null;
			try
			{
				objPwdPolicyParms=new PwdPolicyParms(base.ClientId);
				objPwdPolicyParms.UnlockUserAccount(ref p_objDocIn);
				p_objXmlOut = p_objDocIn;
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PwdPolicyParmsAdaptor.UnlockUserAccount.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				
			}
		}

		public bool LockUserAccount(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{   
			Riskmaster.Application.SecurityManagement.PwdPolicyParms objPwdPolicyParms=null;
			try
			{
                objPwdPolicyParms = new PwdPolicyParms(base.ClientId);
				objPwdPolicyParms.LockUserAccount(ref p_objDocIn);
				p_objXmlOut = p_objDocIn;
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PwdPolicyParmsAdaptor.LockUserAccount.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
				
			}
		}
	}
}
