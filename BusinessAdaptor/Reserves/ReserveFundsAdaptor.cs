/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 12/06/2013 | 34082   | pgupta93   | Changes req for Add and edit Reserve for Multicurrency
 * 19/03/2014 | 34275  | abhadouria |  RMA Swiss Re Financial Summary 
 * 14/01/2015 | GAP-12 | achouhan3  | Method to save User Preferences
 **********************************************************************************************/
using System;
using Riskmaster.Application.Reserves;
using Riskmaster.BusinessAdaptor.Common;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using Riskmaster.Application.VSSInterface;


namespace Riskmaster.BusinessAdaptor
{

	///************************************************************** 
	///* $File		: ReserveFundsAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 18-Feb-2005
	///* $Author	: Tanuj Narula
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	A class representing the Reserve Funds adaptor. It exposes various methods for retrieving
	/// and modifying the reserve amounts. 
	/// </summary>
	public class ReserveFundsAdaptor: BusinessAdaptorBase
	{
		#region Constructor
		/// <summary>
		/// This is the default constructor.
		/// </summary>
		public ReserveFundsAdaptor()
		{	
		}

		#endregion

		#region Public functions
		/// <summary>
		/// This function is a wrapper over Riskmaster.Application.Reserves.ReserveFunds.ReserveGetClaimReserves() function. 
		/// This method is basically used for getting the Claim reserves associated with a 
		/// particular claim.
		/// </summary>
		/// <param name="p_objXmlIn">Input xml document.
		/// Following is the structure for input xml-:
		// <Document>
		//	<ReserveFunds>
		//	    <ClaimNumber> 
		//	    </ClaimNumber>
		//	    <ParentSecurityId>
		//	    </ParentSecurityId>
		//	    <SecurityId>
		//	    </SecurityId>
		//		<ClaimId>
		//		</ClaimId>
		//		<ClaimantEID>
		//		</ClaimantEID>
		//		<UnitID>
		//		</UnitID>
		//		<LOB>
		//		</LOB>
		//	    <IsTandE>
		//	    </IsTandE>
		//	    <ReserveTypeCode>
		//	    </ReserveTypeCode>
		//	    <DateEntered> 
		//	    </DateEntered> 
		//	    <Amount>
		//	    </Amount> 
		//	    <StatusCodeID>
		//	    </StatusCodeID>
		//	    <USer>
		//	    </USer>
		//	    <Reason>
		//	    </Reason>
		//	    <UserId>
		//	    </UserId>
		//	    <GroupId>
		//	    </GroupId>
		//      <Reserve>
		//      </Reserve>
		//	    <ChequeDate>
		//	    </ChequeDate>
		//	    <LoginName>
		//	    </LoginName>
		//	   </ReserveFunds>
		//	   </Document>
		/// </param>
		/// <param name="p_objXmlOut">Xml document containing the output xml.
		//	Output structure is as follows-:
        //  <ReserveFunds>
		//  <claimreserves claimid="" claimant="" unit="" istande="" claimnumber="" caption="">
		//    <reserves claimid="" claimant="" unit="">
		//	  <reserve reservetypecode="" reservename="" reservestatus="" balance="" incurred="" paid="" collected="" /> 
		//	  <reserve reservetypecode="" reservename="" reservestatus="" balance="" incurred="" paid="" collected="" /> 
		//	  <reserve reservetypecode="" reservename="" reservestatus="" balance="" incurred="" paid="" collected="" /> 
		//	  <reserve reservetypecode="" reservename="" reservestatus="" balance="" incurred="" paid="" collected="" /> 
		//	  <reserve reservetypecode="" reservename="" reservestatus="" balance="" incurred="" paid="" collected="" /> 
		//	  </reserves>
		//	  <totals balanceamount="" incurredamount="" paidtotal="" collectedtotal="" /> 
		//	</claimreserves>
		// </ReserveFunds>
		///</param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool GetClaimReserves(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{

			ReserveFunds objReserveFunds = null;
            ReserveCurrent objReserveCurrent = null;
			int iClaimId = 0;
			int iClaimantEID = 0;
			int iUnitID = 0;
			int iLOB = 0;
            int iUnitRowId = 0;
			bool bIsTandE = false;
            //pmittal5 MITS 9906 04/11/08
            string sFormTitle = "";
            XmlElement p_objXmlElement = null;
            int iCurrType = 0;
			try
			{
                GetDetailTrackingRequiredInfo(ref p_objXmlIn);

				iClaimId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText);
				iClaimantEID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimantEID").InnerText);
				iUnitID =Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//UnitID").InnerText);
                iUnitRowId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//UnitRowID").InnerText);
                iCurrType= Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//CurrencyType").InnerText);//Deb
                if (iUnitID == 0)
                {
                    if (iUnitRowId > 0)
                    {
                        iUnitID = iGetUnitIDFromUnitRowID(iUnitRowId);
                    }
                }
				iLOB=Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//LOB").InnerText);
                //pmittal5 MITS 9906 04/11/08
                sFormTitle = p_objXmlIn.SelectSingleNode("//FormTitle").InnerText;
				//Initialize the application layer ReserveFunds class
				objReserveFunds=InitializeReserves(ref p_objXmlIn,ref p_objXmlOut,ref p_objErrOut);

                bIsTandE = Conversion.ConvertObjToBool(p_objXmlIn.SelectSingleNode("//IsTandE").InnerText, base.ClientId);
                objReserveFunds.LanguageCode = userLogin.objUser.NlsCode; //Aman ML Change
				if(bIsTandE)
				{
                    //pmittal5 MITS 9906 04/11/08 
					//p_objXmlOut.DocumentElement.InnerXml=objReserveFunds.GetClaimReserves(iClaimId,iClaimantEID,iUnitID,iLOB,bIsTandE).OuterXml;
                    p_objXmlOut.DocumentElement.InnerXml = objReserveFunds.GetClaimReserves(iClaimId, iClaimantEID, iUnitID, iLOB, sFormTitle, bIsTandE, iCurrType).OuterXml;
				}
				else
				{
                    //pmittal5 MITS 9906 04/11/08
					//p_objXmlOut.DocumentElement.InnerXml=objReserveFunds.GetClaimReserves(iClaimId,iClaimantEID,iUnitID,iLOB,bIsTandE).OuterXml;
                    p_objXmlOut.DocumentElement.InnerXml = objReserveFunds.GetClaimReserves(iClaimId, iClaimantEID, iUnitID, iLOB, sFormTitle, bIsTandE, iCurrType).OuterXml;
				}
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.GetClaimReserves.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objReserveFunds=null;
			}
			
		}

        /// <summary>
        /// GetClaimants
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetClaimants(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            int iClaimId = 0;         
            ReserveFunds objReserveFunds = null;

            try
            {              
                iClaimId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText);
                
                objReserveFunds = new ReserveFunds(base.connectionString, this.m_userLogin, base.ClientId);//rkaur27
                p_objXmlOut = new XmlDocument();
                
                p_objXmlOut = objReserveFunds.GetClaimants(iClaimId);

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.GetClaimants.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objReserveFunds = null;
            }
        }
        //skhare7
        public bool GetBOBReserves(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {

            ReserveFunds objReserveFunds = null;
            ReserveCurrent objReserveCurrent = null;
            int iClaimId = 0;
            int iClaimantEID = 0;
            int iUnitID = 0;
            int iLOB = 0;
            int iUnitRowId = 0;
            bool bIsTandE = false;
            //pmittal5 MITS 9906 04/11/08
            string sFormTitle = "";
            XmlElement p_objXmlElement = null;
            try
            {
                GetDetailTrackingRequiredInfo(ref p_objXmlIn);

                iClaimId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText);
                iClaimantEID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimantEID").InnerText);
               
              
                iLOB = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//LOB").InnerText);
                //pmittal5 MITS 9906 04/11/08
                sFormTitle = p_objXmlIn.SelectSingleNode("//FormTitle").InnerText;
                //Initialize the application layer ReserveFunds class
                objReserveFunds = InitializeReservesBOB(ref p_objXmlIn, ref p_objXmlOut, ref p_objErrOut);
                objReserveFunds.LanguageCode = base.userLogin.objUser.NlsCode; //Aman ML Change
                {
                    //pmittal5 MITS 9906 04/11/08
                    //p_objXmlOut.DocumentElement.InnerXml=objReserveFunds.GetClaimReserves(iClaimId,iClaimantEID,iUnitID,iLOB,bIsTandE).OuterXml;
                    p_objXmlOut.DocumentElement.InnerXml = objReserveFunds.GetBOBReserves(iClaimId, iClaimantEID, iLOB, sFormTitle).OuterXml;
                }
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.GetClaimReserves.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objReserveFunds = null;
            }

        }

        // akaushik5 Added for MITS 33577 Starts
        /// <summary>
        /// Gets the claim reserve summary BOB.
        /// </summary>
        /// <param name="p_objXmlIn">The p_obj XML in.</param>
        /// <param name="p_objXmlOut">The p_obj XML out.</param>
        /// <param name="p_objErrOut">The p_obj err out.</param>
        /// <returns></returns>
        public bool GetClaimReserveSummaryBOB(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ReserveFunds objReserveFunds = null;
            int iClaimId = 0;
            int iLOB = 0;
            string sFormTitle = "";
            bool success;
            try
            {
                iClaimId = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText, out success);
                iLOB = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("//LOB").InnerText, out success);
                sFormTitle = p_objXmlIn.SelectSingleNode("//FormTitle").InnerText;
                objReserveFunds = InitializeReservesBOB(ref p_objXmlIn, ref p_objXmlOut, ref p_objErrOut);
                objReserveFunds.LanguageCode = base.userLogin.objUser.NlsCode;
                {
                    p_objXmlOut.DocumentElement.InnerXml = objReserveFunds.GetClaimReservesSummaryBOB(iClaimId, iLOB, sFormTitle).OuterXml;
                }
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.GetClaimReserves.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objReserveFunds = null;
            }

        }
        // akaushik5 Added for MITS 33577 Ends
        
        /// <summary>
        /// GetClaimReservesBOB
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetClaimReservesBOB(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {

            ReserveFunds objReserveFunds = null;            
            int iClaimId = 0;
            int iClaimantEID = 0;
            int iCurrType = 0;
            string sGridId = string.Empty; //RMA-11468 pgupta93
            string sPageName = string.Empty;//RMA-11468 pgupta93

            try
            {               
                
                iClaimId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText);
                iClaimantEID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimantEID").InnerText);//RMA-11468 pgupta93
                //rupal:start,r8 multicurrency
                if (p_objXmlIn.SelectSingleNode("//CurrencyType") != null)
                    iCurrType = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//CurrencyType").InnerText);
                sGridId = (p_objXmlIn.SelectSingleNode("//GridId").InnerText);
                sPageName = (p_objXmlIn.SelectSingleNode("//PageName").InnerText);
                //rupal:end
                //Initialize the application layer ReserveFunds class       
                objReserveFunds = new ReserveFunds(base.connectionString, this.m_userLogin, base.ClientId);//rkaur27
                p_objXmlOut = new XmlDocument();
                objReserveFunds.LanguageCode = base.userLogin.objUser.NlsCode; //Aman ML Change
               // p_objXmlOut = objReserveFunds.GetReservesBOB(iClaimId, iCurrType, sPageName, sGridId);//rma 11468
                p_objXmlOut = objReserveFunds.GetReservesBOB(iClaimId, iCurrType, sPageName, sGridId, iClaimantEID);//RMA-8490



                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.GetClaimReservesBOB.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objReserveFunds = null;
            }

        }



        
        /// <summary>
        /// Gets Payments made for a particular claim, claimant, policy , coverage and reserve
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetPaymentsBOB(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ReserveFunds objReserveFunds = null;         
            int iClaimId = 0;
            int iClaimantEID = 0;
            int iPolicyId = 0;
            int iPolicyCvgRowID = 0;
            int iReserveTypeCode = 0;
            int iRC_ROW_ID = 0;
            int iCgLossId = 0;
            //rupal:r8 multicurrency
            int iCurrType = 0;

            try
            {               
                iClaimId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText);
                iClaimantEID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimantEID").InnerText);
                //iPolicyId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//PolicyID").InnerText);
                iPolicyCvgRowID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//PolCvgID").InnerText);
                iReserveTypeCode = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ReserveTypeCode").InnerText);
                //rupal:start,r8 multicurrency
                if (p_objXmlIn.SelectSingleNode("//CurrencyType") != null)
                    iCurrType = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//CurrencyType").InnerText);
                //rupal:end
                if (p_objXmlIn.SelectSingleNode("//CvgLossId") != null)
                    iCgLossId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//CvgLossId").InnerText);

                if (p_objXmlIn.SelectSingleNode("//RCRowID") != null)
                iRC_ROW_ID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//RCRowID").InnerText);
                objReserveFunds = new ReserveFunds(base.connectionString, this.m_userLogin, base.ClientId);//rkaur27
                p_objXmlOut = new XmlDocument();

                //p_objXmlOut = objReserveFunds.GetPaymentsBOB(iClaimId, iClaimantEID, iPolicyId, iPolicyCvgRowID, iReserveTypeCode,iRC_ROW_ID);
                p_objXmlOut = objReserveFunds.GetPaymentsBOB(iClaimId, iClaimantEID, iPolicyCvgRowID, iReserveTypeCode, iCurrType,iRC_ROW_ID, iCgLossId);

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.GetPaymentsBOB.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objReserveFunds = null;
            }

        }



        /// <summary>
        /// Gets Lookup Values for Policy , Coverage and Reserve combo boxes
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetLookupXml(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ReserveFunds objReserveFunds = null;
            int iClaimId = 0;
            //Ankit Start : Financial Enhancement - Reserve Sub Type Changes
            int iReserveID = 0;
            //Ankit End

            try
            {
                iClaimId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText);

                //Ankit Start : Financial Enhancement - Reserve Sub Type Changes
                if (p_objXmlIn.SelectSingleNode("//ReserveType") != null && !string.IsNullOrEmpty(p_objXmlIn.SelectSingleNode("//ReserveType").InnerText))
                    iReserveID = Convert.ToInt32(p_objXmlIn.SelectSingleNode("//ReserveType").InnerText);
                //Ankit End

                objReserveFunds = new ReserveFunds(base.connectionString, this.m_userLogin, base.ClientId);//rkaur27
                p_objXmlOut = new XmlDocument();
                objReserveFunds.LanguageCode = base.userLogin.objUser.NlsCode; //Aman ML Change
                p_objXmlOut = objReserveFunds.GetLookupXml(iClaimId, iReserveID);


                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.GetLookupXml.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objReserveFunds = null;
            }

        }

        //MITS 34275- RMA Swiss Re Financial Summary START
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetPolicyUnitCoverageSummary(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ReserveFunds objReserveFunds = null;
            int iselectedvalue = 1;
            int iPolicyID = 0;
            int  iCoverageID = 0;
            int iClaimID = 0;
             int iLOB = 0;
            string sFormTitle = "";
            string spreviousvalue = "";
            string sselectedvalue = "";
            string sClaimCurrency = "";
            bool success;
            int iUnitID = 0;
            try
            {
                iClaimID = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText, out success);
                iLOB = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("//LOB").InnerText, out success);
                iPolicyID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//PolicyID").InnerText);
                iCoverageID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//CoverageID").InnerText);
                iselectedvalue = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//Selected").InnerText);
                spreviousvalue = (p_objXmlIn.SelectSingleNode("//previouscurrencytype").InnerText);
                sselectedvalue = (p_objXmlIn.SelectSingleNode("//selectedcurrencytype").InnerText);
                iUnitID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//UnitID").InnerText);
                iLOB = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("//LOB").InnerText, out success);
                sClaimCurrency = (p_objXmlIn.SelectSingleNode("//claimcurrency").InnerText);
                sFormTitle = p_objXmlIn.SelectSingleNode("//FormTitle").InnerText;
                int iClaimantListClaimantEID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimantEID").InnerText);
                objReserveFunds = InitializeReservesBOB(ref p_objXmlIn, ref p_objXmlOut, ref p_objErrOut);
                objReserveFunds.LanguageCode = base.userLogin.objUser.NlsCode;
                {
                    p_objXmlOut.DocumentElement.InnerXml = objReserveFunds.GetPolicyUnitCoverageSummary(iClaimID, iLOB, iPolicyID, iCoverageID, iselectedvalue, iUnitID, sFormTitle, spreviousvalue, sselectedvalue, sClaimCurrency, iClaimantListClaimantEID).OuterXml;
                }
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.GetClaimReserves.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objReserveFunds = null;
            }
        }
        // MITS 34275- RMA Swiss Re Financial Summary END
		
        /// <summary>
        /// Adds a new Reserve for BOB
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool AddReserve(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ReserveFunds objReserveFunds = null;
            int iPolicyID = 0;
            int iCoverageID = 0;
            int iReserveTypeCode = 0;
            double dAmount = 0;
            int iClaimID = 0;
            int iClaimantEID = 0;
            int iStatusCode = 0;
            int iAdjusterEID = 0;        //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            string sReason = String.Empty;
            int iCvgLossId=0;
            int iLossTypeCode=0;
            int iReserveCat=0;
            int iDisabilityCat=0;
			int iRcRowId = 0; //averma62 - VSS - rmA Integration
            string sReserveCurrencyType = string.Empty;   //MITS:34082 added variable for Reserve Currency Type    
            System.Collections.Hashtable objErrors = new System.Collections.Hashtable();//MITS 30380
            //added by nitin goel,MITS 30910,02/04/2012
            bool bValidate;
            bValidate=true;
            bool IsDeductibleReserveApplicable= false;
            DataModelFactory oDmf = null;
            //end:nitin goel

            try
            {
                iPolicyID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//PolicyID").InnerText);
                iCoverageID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//CoverageID").InnerText);
                iReserveTypeCode = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ReserveTypeCode").InnerText);

                dAmount = Conversion.ConvertStrToDouble(p_objXmlIn.SelectSingleNode("//Amount").InnerText);
                iClaimID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText); ;
                iClaimantEID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimantEID").InnerText); ;

                if( p_objXmlIn.SelectSingleNode("//Status") != null )
                    iStatusCode = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//Status").InnerText);

                //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                if (p_objXmlIn.SelectSingleNode("//AssignAdjusterEid") != null && !string.IsNullOrEmpty(p_objXmlIn.SelectSingleNode("//AssignAdjusterEid").InnerText))
                    iAdjusterEID = Convert.ToInt32(p_objXmlIn.SelectSingleNode("//AssignAdjusterEid").InnerText);
                //Ankit End

                if (p_objXmlIn.SelectSingleNode("//Reason") != null)
                    sReason = p_objXmlIn.SelectSingleNode("//Reason").InnerText;

                //if (p_objXmlIn.SelectSingleNode("//Reason") != null)
                // iCvgLossId =Conversion.ConvertStrToInteger( p_objXmlIn.SelectSingleNode("//Reason").InnerText);

                if (p_objXmlIn.SelectSingleNode("//DisablityCat") != null)
                iDisabilityCat =Conversion.ConvertStrToInteger( p_objXmlIn.SelectSingleNode("//DisablityCat").InnerText);

                if (p_objXmlIn.SelectSingleNode("//ReserveCat") != null)
                iReserveCat = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ReserveCat").InnerText);

                if (p_objXmlIn.SelectSingleNode("//LossTypeCode") != null)
                iLossTypeCode =Conversion.ConvertStrToInteger( p_objXmlIn.SelectSingleNode("//LossTypeCode").InnerText);

                objReserveFunds = new ReserveFunds(base.connectionString, this.m_userLogin, base.ClientId);//rkaur27
                p_objXmlOut = new XmlDocument();
                //MITS:34082 MultiCurrency START
                if (p_objXmlIn.SelectSingleNode("//CurrencyType") != null)
                    sReserveCurrencyType = p_objXmlIn.SelectSingleNode("//CurrencyType").InnerText;
     			bValidate = ValidateReserve(iClaimID, iReserveTypeCode, ref p_objErrOut, ref IsDeductibleReserveApplicable);
                if (bValidate)
                {
                p_objXmlOut = objReserveFunds.AddReserve(ref objErrors, iClaimID, iClaimantEID, iPolicyID, iCoverageID, iReserveTypeCode, iAdjusterEID, dAmount, ref iStatusCode, sReason, iLossTypeCode, iDisabilityCat, iReserveCat, sReserveCurrencyType, out iRcRowId);//30380
                //MITS:34082 MultiCurrency END
                        //Start:Added by Nitin Goel, For NI PCR Changes, Recovery reserve should only be created when coverage is not part of the group and if coverage is part of coverage, it should be applicable for the reserve type.
                        ////tanwar2/ngoel9,01/25/2013, mITS 30910 - mits - NIS
                if (IsDeductibleReserveApplicable)
                    {
                        oDmf = new DataModelFactory(base.userLogin, base.ClientId);
                        if (oDmf.Context.LocalCache.GetRelatedCodeId(iStatusCode) == oDmf.Context.LocalCache.GetCodeId("O", "RESERVE_STATUS_PARENT"))
                        {
                            objReserveFunds.AddRecoveryReserve(iClaimID, iClaimantEID, iPolicyID, iCoverageID, dAmount, iLossTypeCode, iDisabilityCat, iReserveTypeCode, iAdjusterEID, sReserveCurrencyType, iStatusCode);
                        }
                        //tanwar2 - validate reserve status - start
                        //end by Nitin goel, issue with different pol coverage sequence number
                    }
                }

                //Start - averma62 - Check wheather claim is marked for vss or not
                #region VSS Adjuster Export
                string sVssFlag = string.Empty;
                string sSQL = string.Empty;
                //sSQL = "SELECT VSS_CLAIM_IND FROM CLAIM WHERE CLAIM_ID = " + iClaimID;
                //use utility check intead of claim check.
                sSQL = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='ENABLE_VSS'";
                sVssFlag=Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_connectionString, sSQL));

                if (sVssFlag.Equals("-1"))
                {
                    VssExportAsynCall objVss = new VssExportAsynCall(m_userLogin.objRiskmasterDatabase.DataSourceName, m_userLogin.LoginName, m_userLogin.Password, base.ClientId);
                    objVss.AsynchVssReserveExport(iClaimID, iClaimantEID, iRcRowId, 0, 0, "", "", "", "", "Reserve");
                }
                #endregion
                //End - averma62 - Check wheather claim is marked for vss or not
                //MITS # 34609 tanwar2 - Adding warnings - start
                //tanwar2 - In case of errors: control should go to catch.
                //This Implies if control reaches here and objErrors contains value <- These are warnings.
                foreach (System.Collections.DictionaryEntry objWarnings in objErrors)
                {
                    p_objErrOut.Add((p_objErrOut.Count + 1).ToString(), objWarnings.Value.ToString(), Common.BusinessAdaptorErrorType.Warning);
                }
                //MITS # 34609 tanwar2 - Adding warnings - end
                 //return true;
                return bValidate;
                //end:Nitin goel
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                foreach (System.Collections.DictionaryEntry objError in objErrors)//MITS 30380 starts
                    p_objErrOut.Add("ValidationScriptError", objError.Value.ToString(), Common.BusinessAdaptorErrorType.Error);//MITS 30380 ends
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.AddReserve.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (oDmf!=null)
                {
                    oDmf.Dispose();
                    oDmf = null;
                }
                objReserveFunds = null;
            }

        }
     /// <summary>
        ///  Added by Nitin goel, MITS 30910,01/28/2013,changes done for National Interstate
     /// </summary>
     /// <param name="iClaimID"></param>
     /// <param name="iReserveTypeCode"></param>
     /// <param name="p_objErrOut"></param>
     /// <returns></returns>
        private bool ValidateReserve(int iClaimID, int iReserveTypeCode, ref BusinessAdaptorErrors p_objErrOut, ref bool IsDeductibleReserveApplicable)
        {
            DataModelFactory objDatamodel;
           
            objDatamodel = null;
            
            try
            {                
                int iDedReserverTypeCode;
                int iTocheckDedSettings;
               
                int iSettingCarrierClaim;
                int iLineofBusCode;
                iDedReserverTypeCode = 0;
                iTocheckDedSettings = 0;
            
                iSettingCarrierClaim=0;
                iLineofBusCode = 0;
                IsDeductibleReserveApplicable = false;
                objDatamodel = new DataModelFactory(m_userLogin, base.ClientId);
                
                iLineofBusCode = objDatamodel.Context.DbConnLookup.ExecuteInt("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + iClaimID);
               
                iDedReserverTypeCode = objDatamodel.Context.InternalSettings.ColLobSettings[iLineofBusCode].DedRecReserveType;
                iTocheckDedSettings = objDatamodel.Context.InternalSettings.ColLobSettings[iLineofBusCode].ApplyDedToPaymentsFlag;
         
                iSettingCarrierClaim = objDatamodel.Context.InternalSettings.SysSettings.MultiCovgPerClm;

                if (objDatamodel.Context.LocalCache.GetRelatedCodeId(iReserveTypeCode) != objDatamodel.Context.LocalCache.GetCodeId("R", "MASTER_RESERVE") && iTocheckDedSettings == -1 && iSettingCarrierClaim == -1)
                {
                    IsDeductibleReserveApplicable = true;
                }
            
               if (iReserveTypeCode == iDedReserverTypeCode && iTocheckDedSettings == -1 && iSettingCarrierClaim == -1)
                {
                    p_objErrOut.Add(new RMAppException(Globalization.GetString("ReserveFundsAdaptor.AddDedRecReserve.Error", base.ClientId)), BusinessAdaptorErrorType.Error);
                    return false;                                       
                }
                return true;
            }
          
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            finally
            {
                if (objDatamodel != null)
                {
                    objDatamodel.Dispose();
                    objDatamodel = null;
                }
                       
            }
        }


        /// <summary>
        /// Adds a new Reserve for BOB
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool EditReserve(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ReserveFunds objReserveFunds = null;
            int iPolicyID = 0;
            int iCoverageID = 0;            
            double dAmount = 0;
            int iClaimID = 0;
            int iClaimantEID = 0;
            int iReserveTypeCode = 0;
            int iStatusCode = 0;
            int iAdjusterEID = 0;        //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            string sReason = String.Empty;
            int iCvgLossId = 0;
			int iRcRowId = 0; //averma62 - VSS - rmA Integration
            string sReserveCurrencyType = string.Empty;   //MITS:34082 added variable for Reserve Currency Type;
            System.Collections.Hashtable objErrors = new System.Collections.Hashtable();//MITS 30380
            //added by nitin goel,MITS 30910,02/04/2012
            bool bValidate;
            bValidate = true;
             bool IsDeductibleReserveApplicable= false;
             DataModelFactory oDataModelFactory = null;
            //end:nitin goel

            try
            {
                
                iClaimID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText);
                iClaimantEID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimantEID").InnerText);
                

                iPolicyID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//PolicyID").InnerText);
                iCoverageID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//CoverageID").InnerText);
                iReserveTypeCode = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ReserveTypeCode").InnerText);

                dAmount = Conversion.ConvertStrToDouble(p_objXmlIn.SelectSingleNode("//Amount").InnerText);
                //iClaimID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText); ;
                //iClaimantEID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimantEID").InnerText); ;

                if (p_objXmlIn.SelectSingleNode("//Status") != null)
                    iStatusCode = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//Status").InnerText);

                //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                if (p_objXmlIn.SelectSingleNode("//AssignAdjusterEid") != null && !string.IsNullOrEmpty(p_objXmlIn.SelectSingleNode("//AssignAdjusterEid").InnerText))
                    iAdjusterEID = Convert.ToInt32(p_objXmlIn.SelectSingleNode("//AssignAdjusterEid").InnerText);
                //Ankit End

                if (p_objXmlIn.SelectSingleNode("//Reason") != null)
                    sReason = p_objXmlIn.SelectSingleNode("//Reason").InnerText;

                if(p_objXmlIn.SelectSingleNode("//CoverageLossID")!=null)

                iCvgLossId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//CoverageLossID").InnerText);
                objReserveFunds = new ReserveFunds(base.connectionString, this.m_userLogin, base.ClientId);//rkaur27
                //MITS:34082 MultiCurrency START
                if (p_objXmlIn.SelectSingleNode("//CurrencyType") != null)
                    sReserveCurrencyType = p_objXmlIn.SelectSingleNode("//CurrencyType").InnerText;
                //MITS:34082 MultiCurrency END
                p_objXmlOut = new XmlDocument();
                //MITS:34082 MultiCurrency START
                //p_objXmlOut = objReserveFunds.EditReserve(ref objErrors, iClaimID, iClaimantEID, iPolicyID, iCoverageID, iReserveTypeCode, iAdjusterEID, dAmount, iStatusCode, sReason, iCvgLossId, out iRcRowId);//30380
        //Start:added by nitin goel, MITS 30910,02/04/2013
                bValidate = ValidateReserve(iClaimID, iReserveTypeCode, ref p_objErrOut, ref IsDeductibleReserveApplicable);
                if (bValidate)
                {
                p_objXmlOut = objReserveFunds.EditReserve(ref objErrors, iClaimID, iClaimantEID, iPolicyID, iCoverageID, iReserveTypeCode, iAdjusterEID, dAmount, ref iStatusCode, sReason, iCvgLossId, sReserveCurrencyType, out iRcRowId);//30380
                //MITS:34082 MultiCurrency END
                    if (IsDeductibleReserveApplicable)
                    {
                        oDataModelFactory = new DataModelFactory(base.userLogin, base.ClientId);
                        if (oDataModelFactory.Context.LocalCache.GetRelatedCodeId(iStatusCode) == oDataModelFactory.Context.LocalCache.GetCodeId("O", "RESERVE_STATUS_PARENT"))
                        {
                            //Start:added by Nitin goel
                            objReserveFunds.EditRecoveryReserve(ref objErrors, iClaimID, iClaimantEID, iPolicyID, iCoverageID, iReserveTypeCode, iAdjusterEID, dAmount, iStatusCode, sReason, iCvgLossId, sReserveCurrencyType);
                            // end: added by Nitin goel
                        }
                    }
                }

                //Start - averma62 - Check wheather claim is marked for vss or not
                #region VSS Adjuster Export
                string sVssFlag = string.Empty;
                string sSQL = string.Empty;
                //sSQL = "SELECT VSS_CLAIM_IND FROM CLAIM WHERE CLAIM_ID = " + iClaimID;
                //use utility check instead of claim check.
                sSQL = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='ENABLE_VSS'";
                sVssFlag = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_connectionString, sSQL));

                if (sVssFlag.Equals("-1"))
                {
                    VssExportAsynCall objVss = new VssExportAsynCall(m_userLogin.objRiskmasterDatabase.DataSourceName, m_userLogin.LoginName, m_userLogin.Password, base.ClientId);
                    objVss.AsynchVssReserveExport(iClaimID, iClaimantEID, iRcRowId, 0, 0, "", "", "", "", "Reserve");
                }
                #endregion
                //End - averma62 - Check wheather claim is marked for vss or not
                
                //tanwar2 - Adding warnings - start
                //tanwar2 - In case of errors: control should go to catch.
                //This Implies if control reaches here and objErrors contains value <- These are warnings.
                foreach (System.Collections.DictionaryEntry objWarnings in objErrors)
                {
                    p_objErrOut.Add((p_objErrOut.Count + 1).ToString(), objWarnings.Value.ToString(), Common.BusinessAdaptorErrorType.Warning);
                }
                //tanwar2 - Adding warnings - end
                 //return true;
                return bValidate;
                //end:Nitin goel
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                foreach (System.Collections.DictionaryEntry objError in objErrors)//MITS 30380 starts
                    p_objErrOut.Add("ValidationScriptError", objError.Value.ToString(), Common.BusinessAdaptorErrorType.Error);//MITS 30380 ends

                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.EditReserve.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (oDataModelFactory!=null)
                {
                    oDataModelFactory.Dispose();
                }
                objReserveFunds = null;
            }

        }



        /// <summary>
        /// Get related data for detail level tracking. From MDI, it'll pass in claimant row id and unit row id
        /// but detail level reserve tracking need claimant eid and unit id.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        private void GetDetailTrackingRequiredInfo(ref XmlDocument p_objXmlIn)
        {
            int iClaimId=0;
            int iClaimantRowID = 0;
			int iClaimantEID=0;
			int iUnitID=0;
            int iUnitRowID = 0;
			int iLOB=0;
			string iClaimNumber = string.Empty; // Mobile Apps
			bool bIsTandE=false;
            string sSQL = string.Empty;
            string sClaimNumber = string.Empty;
			XmlNode oParent = null;
			XmlNode objCaller = null; //Mobile Apps

            try
            {
				// ijha: Reserve Xml for mobility using claim number as input
				objCaller = p_objXmlIn.SelectSingleNode("//caller");
                if (objCaller == null || (objCaller.InnerText != "MobileAdjuster" && objCaller.InnerText != "MobilityAdjuster"))
                {
					{
                iClaimId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText);
                iClaimantRowID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimantRowID").InnerText);
                iUnitRowID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//UnitRowID").InnerText);

						oParent = p_objXmlIn.SelectSingleNode("//ReserveFunds");

                //Get LOB, CLAIM_NUMBER from UnitRowID
                sSQL = string.Format("{0} {1}", "SELECT CLAIM_NUMBER, LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID=", iClaimId);
                using (DbReader oDbReader = DbFactory.ExecuteReader(m_connectionString, sSQL))
                {
                    if (oDbReader.Read())
                    {
                        iLOB = oDbReader.GetInt32("LINE_OF_BUS_CODE");
                        sClaimNumber = oDbReader.GetString("CLAIM_NUMBER");
							}
						}
                        XmlNode oLOBNode = p_objXmlIn.SelectSingleNode("//LOB");
                        if (oLOBNode == null)
                        {
                            XmlElement oLOBElement = p_objXmlIn.CreateElement("LOB");
                            oParent.AppendChild(oLOBElement);
                        } 
                        oLOBNode.InnerText = iLOB.ToString();

                        XmlNode oClaimNumberNode = p_objXmlIn.SelectSingleNode("//ClaimNumber");
                        if (oClaimNumberNode == null)
                        {
                            XmlElement oClaimNumberElement = p_objXmlIn.CreateElement("ClaimNumber");
                            oParent.AppendChild(oClaimNumberElement);
                        }
                        oClaimNumberNode.InnerText = sClaimNumber;
                    }
                }
				else
				{
                    if (objCaller.InnerText == "MobileAdjuster" || objCaller.InnerText == "MobilityAdjuster")
                    {
						iClaimNumber = p_objXmlIn.SelectSingleNode("//ClaimNumber").InnerText;
						iClaimantRowID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimantRowID").InnerText);
						iUnitRowID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//UnitRowID").InnerText);

						oParent = p_objXmlIn.SelectSingleNode("//ReserveFunds");

						sSQL = "SELECT CLAIM_ID, CLAIM_NUMBER, LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_NUMBER='" + iClaimNumber.Trim() + "'";
						using (DbReader oDbReader = DbFactory.ExecuteReader(m_connectionString, sSQL))
						{
							if (oDbReader.Read())
							{
								iClaimId = oDbReader.GetInt32("CLAIM_ID");
								p_objXmlIn.SelectSingleNode("//ClaimID").InnerText = iClaimId + "";
								iLOB = oDbReader.GetInt32("LINE_OF_BUS_CODE");
								sClaimNumber = oDbReader.GetString("CLAIM_NUMBER");
								
							}
						}

						XmlNode oLOBNode = p_objXmlIn.SelectSingleNode("//LOB");
						if (oLOBNode == null)
						{
							XmlElement oLOBElement = p_objXmlIn.CreateElement("LOB");
							oParent.AppendChild(oLOBElement);
						}
						oLOBNode.InnerText = iLOB.ToString();

						XmlNode oClaimIDNode = p_objXmlIn.SelectSingleNode("//ClaimID");
						if (oClaimIDNode == null)
						{
							XmlElement oClaimNumberElement = p_objXmlIn.CreateElement("ClaimNumber");
							oParent.AppendChild(oClaimNumberElement);
						}
						oClaimIDNode.InnerText = iClaimId.ToString();
					}
				}
				// ijha : end
                //Get UnitID from UnitRowID
                if (iUnitRowID != 0)
                {
                    sSQL = string.Format("{0} {1}", "SELECT UNIT_ID FROM UNIT_X_CLAIM WHERE UNIT_ROW_ID=", iUnitRowID);
                    using (DbReader oDbReader = DbFactory.ExecuteReader(m_connectionString, sSQL))
                    {
                        if (oDbReader.Read())
                        {
                            iUnitID = oDbReader.GetInt32("UNIT_ID");
                            XmlNode oUnitIDNode = p_objXmlIn.SelectSingleNode("//UnitID");
                            if (oUnitIDNode == null)
                            {
                                XmlElement oUnitIDElement = p_objXmlIn.CreateElement("UnitID");
                                oParent.AppendChild(oUnitIDElement);
                            }
                            oUnitIDNode.InnerText = iUnitID.ToString();
                        }
                    }
                }

                //Get Claimant EID from ClaimantRowID
                if (iClaimantRowID != 0)
                {
                    sSQL = string.Format("{0} {1}", "SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIMANT_ROW_ID=", iClaimantRowID);
                    using (DbReader oDbReader = DbFactory.ExecuteReader(m_connectionString, sSQL))
                    {
                        if (oDbReader.Read())
                        {
                            iClaimantEID = oDbReader.GetInt32("CLAIMANT_EID");
                            XmlNode oClaimantEIDNode = p_objXmlIn.SelectSingleNode("//ClaimantEID");
                            if (oClaimantEIDNode == null)
                            {
                                XmlElement oClaimantEIDElement = p_objXmlIn.CreateElement("ClaimantEID");
                                oParent.AppendChild(oClaimantEIDElement);
                            }
                            oClaimantEIDNode.InnerText = iClaimantEID.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }        


		/// <summary>
		/// This function is a wrapper over Riskmaster.Application.Reserves.ReserveFunds.ModifyReserves() function. 
		/// This method is basically used for adding/modifying the Claim reserves associated with a 
		/// particular claim
		/// </summary>
		/// <param name="p_objXmlIn"> Input xml doocument.
		/// Following is the structure for input xml-:
		// <Document>
		//	<ReserveFunds>
		//	    <ClaimNumber> 
		//	    </ClaimNumber>
		//	    <ParentSecurityId>
		//	    </ParentSecurityId>
		//	    <SecurityId>
		//	    </SecurityId>
		//		<ClaimId>
		//		</ClaimId>
		//		<ClaimantEID>
		//		</ClaimantEID>
		//		<UnitID>
		//		</UnitID>
		//		<LOB>
		//		</LOB>
		//	    <IsTandE>
		//	    </IsTandE>
		//	    <ReserveTypeCode>
		//	    </ReserveTypeCode>
		//	    <DateEntered> 
		//	    </DateEntered> 
		//	    <Amount>
		//	    </Amount> 
		//	    <StatusCodeID>
		//	    </StatusCodeID>
		//	    <USer>
		//	    </USer>
		//	    <Reason>
		//	    </Reason>
		//	    <UserId>
		//	    </UserId>
		//	    <GroupId>
		//	    </GroupId>
		//      <Reserve>
		//     </Reserve>
		//	    <ChequeDate>
		//	    </ChequeDate>
		//	    <LoginName>
		//	    </LoginName>
		//	</ReserveFunds>
		//	</Document>
		/// </param>
		/// <param name="p_objXmlOut">Nothing.</param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool ModifyReserves(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
            ReserveCurrent objReserveCurrent = null;
            int iClaimId = 0;
            int iClaimantEID = 0;
            int iUnitID = 0;
            int iReserveTypeCode = 0;
            string sDateEntered = "";
            double dAmount = 0;
            int iStatusCodeID = 0;
            string sUser = "";
            string sReason = "";
            int iUserId = 0;
            int iGroupId = 0;
            XmlElement objXmlElement = null;
            DataModelFactory m_objDataModelFactory = null;
            int iPolicyID = 0;
            int iPolCvgLossID = 0;
            int iAdjusterEID = 0;       //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement

            //akaur9 11/02/2011 MITS 26522 --start
            LocalCache objCache = null;  
            int iResStatusCode = 0;            
            string sResShortCode = string.Empty;
            string sResCodeDesc = string.Empty;
            //akaur9 11/02/2011 MITS 26522 --end

            bool bIsSuccess = false;
            

			try
			{   //Initialize the application layer ReserveFunds class
				//objReserveFunds=InitializeReserves(ref p_objXmlIn,ref p_objXmlOut,ref p_objErrOut);

                //objReserveCurrent = PopulateReserveCurrentObject(ref p_objXmlIn);
                p_objXmlOut = new XmlDocument();      //akaur9 11/02/2011 MITS 26522 

                objCache = new LocalCache(base.connectionString, base.ClientId); //akaur9 11/02/2011 MITS 26522 
                m_objDataModelFactory = new DataModelFactory(m_userLogin, base.ClientId);
                objReserveCurrent = (ReserveCurrent)m_objDataModelFactory.GetDataModelObject("ReserveCurrent", true);
                objReserveCurrent.FiringScriptFlag = 2;

                iClaimId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText);
                iClaimantEID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "ClaimantEID").InnerText);
                iUnitID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "UnitID").InnerText);

                iReserveTypeCode = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "Reserve").InnerText);

                //pmittal5 Mits 18559 11/16/09 - Some nodes are missing in input Xml while Printing Reserves
                objXmlElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "DateEntered");
                if (objXmlElement != null)
                    sDateEntered = objXmlElement.InnerText;

                objXmlElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "Amount");
                if (objXmlElement != null)
                    dAmount = Conversion.ConvertStrToDouble(objXmlElement.InnerText);

                objXmlElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "StatusCodeID");
                if (objXmlElement != null)
                    iStatusCodeID = Conversion.ConvertStrToInteger(objXmlElement.InnerText);

                objXmlElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "PolicyID");
                if (objXmlElement != null)
                    iPolicyID = Conversion.ConvertStrToInteger(objXmlElement.InnerText);


                objXmlElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "PolCvgID");
                if (objXmlElement != null)
                    iPolCvgLossID = Conversion.ConvertStrToInteger(objXmlElement.InnerText);

                //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                objXmlElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "AssignAdjusterEid");
                //Smoke issue - Start - Govind
                //if (objXmlElement != null)
                    //iAdjusterEID = Convert.ToInt32(objXmlElement.InnerText);
                if (objXmlElement != null && !string.IsNullOrEmpty(objXmlElement.InnerText))    
                    iAdjusterEID = Conversion.CastToType<Int32>(objXmlElement.InnerText, out bIsSuccess);
                //Smoke issue - End - Govind
                //Ankit End

                //sUser=p_objXmlIn.SelectSingleNode("//" + "User").InnerText;Reserves/@LoginUser
                sUser = p_objXmlIn.SelectSingleNode("//" + "Reserves").Attributes["LoginUser"].Value;

                objXmlElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "Reason");
                string sReserveCurrencyType = string.Empty;//MITS:34082 MultiCurrency
                if (objXmlElement != null)
                    sReason = objXmlElement.InnerText;
                //End- pmittal5

                iUserId = base.userID;
                iGroupId = base.groupID;

                objReserveCurrent.ClaimId = iClaimId;
                objReserveCurrent.ClaimantEid = iClaimantEID;
                objReserveCurrent.UnitId = iUnitID;
                objReserveCurrent.ReserveTypeCode = iReserveTypeCode;
                objReserveCurrent.DateEntered = sDateEntered;
                //Deb Multi Currency
                int iReserveCurrCode = 0;//MITS:34082 MultiCurrency
                int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(iClaimId, connectionString);
                //MITS:34082 MultiCurrency START
                //if (p_objXmlIn.SelectSingleNode("//CurrencyType") != null)
                //{
                //    sReserveCurrencyType = p_objXmlIn.SelectSingleNode("//CurrencyType").InnerText;
                //    iReserveCurrCode = CommonFunctions.iGetReserveCurrencyCode(sReserveCurrencyType, connectionString);
                //}
                if (p_objXmlIn.SelectSingleNode("//CurrencyTypeID") != null)
                {
                   iReserveCurrCode = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//CurrencyTypeID").InnerText);
                }
                else iReserveCurrCode = iClaimCurrCode;


                if (iReserveCurrCode == 0)
                {
                    iReserveCurrCode = iClaimCurrCode;
                }
                //MITS:34082 MultiCurrency END
                //RMA-3887 Reserve supplemetals starts
                objReserveCurrent.ClaimCurrReserveAmt = dAmount;
                //ajohari2 RMA-15925 Start
                if (iReserveCurrCode > 0)
                    objReserveCurrent.iReserveCurrCode = iReserveCurrCode;
                //ajohari2 RMA-15925 END
                //if (iClaimCurrCode > 0)
                //{
                //    //double dExhRateClaimToBase = objReserveCurrent.Context.DbConn.ExecuteDouble(string.Format("SELECT EXCHANGE_RATE FROM CURRENCY_RATE WHERE SOURCE_CURRENCY_CODE={0} AND DESTINATION_CURRENCY_CODE={1}", iClaimCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType));
                //    double dExhRateClaimToBase = CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, connectionString, base.ClientId);
                //    //MITS:34082 MultiCurrency START
                //    //objReserveCurrent.ReserveAmount = dAmount * dExhRateClaimToBase;
                //    if (iReserveCurrCode > 0)
                //    {
                //        double dExcRateReserveToClaim = CommonFunctions.ExchangeRateSrc2Dest(iReserveCurrCode, iClaimCurrCode, connectionString, base.ClientId);
                //        double dExcRateReserveToBase = CommonFunctions.ExchangeRateSrc2Dest(iReserveCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, connectionString, base.ClientId);
                //        double dExcRateBaseToReserve = CommonFunctions.ExchangeRateSrc2Dest(objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, iReserveCurrCode, connectionString, base.ClientId);
                //        objReserveCurrent.dReserveToClaimCurRate = dExcRateReserveToClaim;
                //        objReserveCurrent.dReserveToBaseCurRate = dExcRateReserveToBase;
                //        objReserveCurrent.dBaseToReserveCurrRate = dExcRateBaseToReserve;
                //        objReserveCurrent.iReserveCurrCode = iReserveCurrCode;
                //        objReserveCurrent.dReserveCurReserveAmount = dAmount;
                //        objReserveCurrent.ReserveAmount = dAmount * dExcRateReserveToBase;
                //        objReserveCurrent.dReserveCurrReserveAmount = dAmount;

                //    }
                //    //MITS:34082 MultiCurrency END
                //}
                //else
                //{
                //    objReserveCurrent.ReserveAmount = dAmount;
                //}
                //RMA-3887 Reserve supplemetals ends.
                //Deb Multi Currency
                objReserveCurrent.ResStatusCode = iStatusCodeID;
                objReserveCurrent.Reason = sReason;
                objReserveCurrent.AssignAdjusterEid = iAdjusterEID;
                objReserveCurrent.sUser = sUser;
                objReserveCurrent.iUserId = iUserId;
                objReserveCurrent.iGroupId = iGroupId;
                objReserveCurrent.sUpdateType = "Reserves";

                //BOB Reserve enahncement
                //objReserveCurrent.PolicyId = iPolicyID;
                objReserveCurrent.CoverageLossId = iPolCvgLossID;                             

                objReserveCurrent.Save();

                //Start - averma62 - Check wheather claim is marked for vss or not
                #region VSS Adjuster Export
                //string sVssFlag = string.Empty;
                //sVssFlag = Conversion.ConvertObjToStr(objReserveCurrent.Context.DbConn.ExecuteScalar("SELECT VSS_CLAIM_IND FROM CLAIM WHERE CLAIM_ID = " + objReserveCurrent.ClaimId));
                //Use Utility Settings for checking Enable VSS
                if (objReserveCurrent.Context.InternalSettings.SysSettings.EnableVSS.Equals(-1))
                {
                    //if (sVssFlag.Equals("-1"))
                    //{
                    VssExportAsynCall objVss = new VssExportAsynCall(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password, base.ClientId);
                        objVss.AsynchVssReserveExport(objReserveCurrent.ClaimId, objReserveCurrent.ClaimantEid, objReserveCurrent.RcRowId, 0, 0, "", "", "", "", "Reserve");
                    //}
                }
                #endregion
                //End - averma62 - Check wheather claim is marked for vss or not

                 //akaur9 11/02/2011 MITS 26522 --start
                iResStatusCode  = objReserveCurrent.ResStatusCode;
                sResShortCode = objCache.GetShortCode(iResStatusCode);
                sResCodeDesc = objCache.GetCodeDesc(iResStatusCode);
                
                //sachin tendulkar
                string sHoldReason = string.Empty;
                if (objCache.GetRelatedCodeId(iResStatusCode) == objCache.GetCodeId("H", "RESERVE_STATUS_PARENT"))
                {
                    foreach (HoldReason oHoldReason in objReserveCurrent.HoldReasonList)
                    {
                        if (iResStatusCode == oHoldReason.HoldStatusCode)
                        {
                            if (string.IsNullOrWhiteSpace(sHoldReason))
                            {
                                sHoldReason = objCache.GetCodeDesc(oHoldReason.HoldReasonCode, m_LangCode);
                            }
                            else
                            {
                                sHoldReason = sHoldReason + ", " + objCache.GetCodeDesc(oHoldReason.HoldReasonCode, m_LangCode);
                            }
                        }
                    }
                }
                //sachin

                objXmlElement = p_objXmlOut.CreateElement("claimreserve");

                p_objXmlOut.AppendChild(objXmlElement);
                objXmlElement.SetAttribute("reservestatus", sResShortCode);
                objXmlElement.SetAttribute("reservestatusdesc", sResCodeDesc);
                //akaur9 11/02/2011 MITS 26522 --end
                objXmlElement.SetAttribute("reserveholdreasondesc", sHoldReason);//sachin 6385

                objReserveCurrent.Dispose();
                foreach (System.Collections.DictionaryEntry objError in objReserveCurrent.Context.ScriptWarningErrors)
                    p_objErrOut.Add("WarningScriptError", objError.Value.ToString(), Common.BusinessAdaptorErrorType.Warning);

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
                foreach (System.Collections.DictionaryEntry objError in objReserveCurrent.Context.ScriptValidationErrors)
                    p_objErrOut.Add("ValidationScriptError", objError.Value.ToString(), Common.BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.ModifyReserves.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
                if (m_objDataModelFactory != null)
                    m_objDataModelFactory.Dispose();
                if (objReserveCurrent != null)
                    objReserveCurrent.Dispose();
			}
		}

        /// <summary>
        /// MGaba2:R8: Supervisory Approval
        /// </summary>
        public bool GetReservesForApproval(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {

            ReserveFunds objReserveFunds = null;
         try
         {

             objReserveFunds = new ReserveFunds(base.connectionString, this.m_userLogin, base.ClientId);//rkaur27
             p_objXmlOut = new XmlDocument();

             p_objXmlOut = objReserveFunds.GetReservesForApproval(p_objXmlIn);
             return true;
         }
         catch (RMAppException p_objException)
         {
             p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
             return false;
         }
         catch (Exception p_objException)
         {
             p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.GetReservesForApproval.Error", base.ClientId), BusinessAdaptorErrorType.Error);
             return false;
         }
         finally
         {
             objReserveFunds = null;
         }
        }

    /// <summary>
        /// MGaba2:R8: Supervisory Approval
        /// </summary>
        public bool ApproveOrRejectReserve(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {

            ReserveFunds objReserveFunds = null;
            XmlElement oElement = null;
            BusinessAdaptorError objTemperr = null; 
            //tanwar2 - mits 30910 - Deductible implementaion - start
            bool bReserveApproved = false;
            int iClaimId = 0;
            int iReserveTypeCode = 0;
            int iClaimantEid = 0;
            int iCvgId = 0;
            double dAmt = 0d;
            int iLossTypeCode = 0;
            int iDisabilityCat = 0;
            int iAdjusterEid = 0;
            int iReserveCurrencyType = 0;
            int iReserveStatusCode = 0;
            bool bSuccess = false;
            bool bIsDeductibleApplicable = false;
            bool bMsg = false;
            string IsJson = String.Empty; // RMA-10217 Added to display multiple error
            //tanwar2 - mits 30910 - Deductible implementaion - end
            string strResvHistID = String.Empty; // RMA-5566 achouhan3  Multiple Selection on Reserve Approval screen
         try
         {

             objReserveFunds = new ReserveFunds(base.connectionString, this.m_userLogin, base.ClientId);//rkaur27
             p_objXmlOut = new XmlDocument();

             //tanwar2 - mits 30910 - Deductible implementaion - start 
             //objReserveFunds.ApproveOrRejectReserve(p_objXmlIn, out bReserveApproved);
             //RMA-5566     achouhan3       Changed to process multiple reserve at a time Starts
             XmlElement oResvElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ResHistRowId");
             XmlElement oJsonElement = (XmlElement)p_objXmlIn.SelectSingleNode("//IsJson");
             if (oJsonElement != null)
                 IsJson = oJsonElement.InnerText;
             if (oResvElement != null && !String.IsNullOrEmpty(oResvElement.InnerText))
             {
                 strResvHistID = oResvElement.InnerText;
                 foreach (string strResvID in oResvElement.InnerText.Split(','))
                 {
                     try
                     {
                         oResvElement.InnerText = strResvID;
                         objReserveFunds.ApproveOrRejectReserve(p_objXmlIn, bMsg, out bReserveApproved, out dAmt, out iLossTypeCode, out iDisabilityCat, out iAdjusterEid, out iReserveCurrencyType, out iReserveStatusCode
                  , out iClaimId, out iReserveTypeCode, out iClaimantEid, out iCvgId);
                         if (!bReserveApproved)
                         {
                             dAmt = 0;
                         }

                         ValidateReserve(iClaimId, iReserveTypeCode, ref p_objErrOut, ref bIsDeductibleApplicable);
                         if (bIsDeductibleApplicable)
                         {
                             //PolicyId = 0 as it is no more required
                             objReserveFunds.AddRecoveryReserve(iClaimId, iClaimantEid, 0, iCvgId, dAmt, iLossTypeCode, iDisabilityCat, iReserveTypeCode, iAdjusterEid, iReserveCurrencyType.ToString(), iReserveStatusCode);
                         }
                     }
                     catch (RMAppException p_objException)
                     {
                         // RMA-10217 Added to display multiple error
                        // string errmsghold = "Exceeded Reserve Limit and Exceeded Incurred limit";
                         string errmsghold = p_objException.Message;
                         if (!String.IsNullOrEmpty(IsJson) && IsJson.ToUpper() == "TRUE")
                         {
                             p_objException = new RMAppException(strResvID + '~' + errmsghold);
                         }
                         p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                     }
                 }
             }
             //RMA-5566     achouhan3       Changed to process multiple reserve at a time Starts
             else // RMA-12087  achouhan3   Edit Reserve was not working from Claim Reserve Screen
             {
                 bMsg = true;
                 objReserveFunds.ApproveOrRejectReserve(p_objXmlIn, bMsg, out bReserveApproved, out dAmt, out iLossTypeCode, out iDisabilityCat, out iAdjusterEid, out iReserveCurrencyType, out iReserveStatusCode
                  , out iClaimId, out iReserveTypeCode, out iClaimantEid, out iCvgId);
             }
             //tanwar2 - mits 30910 - Deductible implementaion - end

             return true;
         }
         catch (RMAppException p_objException)
         {
             p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
             return false;
         }
         catch (Exception p_objException)
         {
             p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.ApproveOrRejectReserve.Error", base.ClientId), BusinessAdaptorErrorType.Error);
             return false;
         }
         finally
         {
            
             oElement = (XmlElement)p_objXmlIn.SelectSingleNode("//RsvRowId");
             if (oElement != null)
             {//From Modify Reserve Screen
                 
                 if (p_objErrOut.Count != 0)
                 {
                     objTemperr = new BusinessAdaptorError();
                     objTemperr = p_objErrOut[0];
                 }

                 GetReservesXML(p_objXmlIn, ref p_objXmlOut, ref p_objErrOut);
                 if (objTemperr != null)
                 {
                     p_objErrOut.Add(objTemperr);
                     //rkulavil : RMA-9896 starts
                     if (p_objXmlOut != null && p_objXmlOut.SelectSingleNode("//ReserveFunds/Reserves") != null)
                     {
                         XmlElement ele = p_objXmlOut.SelectSingleNode("//ReserveFunds/Reserves") as XmlElement;

                         if (ele.HasAttribute("ReserveDesc"))
                         {
                             ele.SetAttribute("ReserveDesc", string.Empty);
                         }
                     }
                     //rkulavil : RMA-9896 ends
                 }
             }
             else
             {
                 oElement = (XmlElement)p_objXmlIn.SelectSingleNode("//BOB");
                 if (oElement != null)
                 {//Editing Reserve when BOB is ON
                      if (p_objErrOut.Count != 0)
                      {
                          p_objXmlOut = p_objXmlIn;
                      }
                 }
                 else
                 {
                     p_objXmlOut = objReserveFunds.GetReservesForApproval(p_objXmlIn);
                 }
             }
             objTemperr = null;
             objReserveFunds = null;
         }
        }

        /// <summary>
        ///  This function returns the unit id for a given unit row id.
        /// </summary>
        private int iGetUnitIDFromUnitRowID(int iThisUnitRowID)
        {
            int iUnitID = 0;
            string sSQL = string.Empty;

            sSQL = "SELECT UNIT_ID FROM UNIT_X_CLAIM WHERE UNIT_ROW_ID = " + iThisUnitRowID;
            using (DbReader oDbReader = DbFactory.ExecuteReader(m_connectionString, sSQL))
            {
                if (oDbReader.Read())
                {
                    iUnitID = oDbReader.GetInt32("UNIT_ID");
                }
            }
            return iUnitID;
        }


        /// <summary>
        ///  This is to populate Reserve current Data Model properties.
        ///  It can be used when a new form is created or saved.
        ///  Custom scripts can then be fired against this Data Model object; after it is returned.
        /// </summary>
        private ReserveCurrent PopulateReserveCurrentObject(ref XmlDocument p_objXmlIn)
        {
            int iClaimId = 0;
            int iClaimantEID = 0;
            int iUnitID = 0;
            int iReserveTypeCode = 0;
            string sDateEntered = "";
            double dAmount = 0;
            int iStatusCodeID = 0;
            string sUser = "";
            string sReason = "";
            int iUserId = 0;
            int iGroupId = 0;
            XmlElement objXmlElement = null;
            try
            {
                DataModelFactory m_objDataModelFactory = new DataModelFactory(m_userLogin, base.ClientId);
                ReserveCurrent objRC = (ReserveCurrent)m_objDataModelFactory.GetDataModelObject("ReserveCurrent", true);
                objRC.FiringScriptFlag = 2;

                iClaimId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText);
                iClaimantEID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "ClaimantEID").InnerText);
                iUnitID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "UnitID").InnerText);

                iReserveTypeCode = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "Reserve").InnerText);
                
                //pmittal5 Mits 18559 11/16/09 - Some nodes are missing in input Xml while Printing Reserves
                objXmlElement =(XmlElement)p_objXmlIn.SelectSingleNode("//" + "DateEntered"); 
                if(objXmlElement != null)
                    sDateEntered = objXmlElement.InnerText;

                objXmlElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "Amount");
                if(objXmlElement != null)
                    dAmount = Conversion.ConvertStrToDouble(objXmlElement.InnerText);

                objXmlElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "StatusCodeID");
                if (objXmlElement != null)
                    iStatusCodeID = Conversion.ConvertStrToInteger(objXmlElement.InnerText);

                //sUser=p_objXmlIn.SelectSingleNode("//" + "User").InnerText;Reserves/@LoginUser
                sUser = p_objXmlIn.SelectSingleNode("//" + "Reserves").Attributes["LoginUser"].Value;

                objXmlElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "Reason");
                if (objXmlElement != null)
                    sReason = objXmlElement.InnerText;
                //End- pmittal5

                iUserId = base.userID;
                iGroupId = base.groupID;

                objRC.ClaimId = iClaimId;
                objRC.ClaimantEid = iClaimantEID;
                objRC.UnitId = iUnitID;
                objRC.ReserveTypeCode = iReserveTypeCode;
                objRC.DateEntered = sDateEntered;
                objRC.ReserveAmount = dAmount;
                objRC.ResStatusCode = iStatusCodeID;
                objRC.Reason = sReason;
                objRC.sUser = sUser;
                objRC.iUserId = iUserId;
                objRC.iGroupId = iGroupId;
                objRC.sUpdateType = "Reserves";
                return objRC;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.PopulateReserveCurrent.DataErr", base.ClientId), objException);
            }
            finally
            {
                
            }
          
        }

		/// <summary>
		///  This function is a wrapper over Riskmaster.Application.Reserves.ReserveFunds.GetReservesXML() function.
		///  This method is basically used for forming the XML to be used in "Modify Reserves page" 
		/// </summary>
		/// <param name="p_objXmlIn">
		// Following is the structure for input xml-:
		// <Document>
		//	<ReserveFunds>
		//	    <ClaimNumber> 
		//	    </ClaimNumber>
		//	    <ParentSecurityId>
		//	    </ParentSecurityId>
		//	    <SecurityId>
		//	    </SecurityId>
		//		<ClaimId>
		//		</ClaimId>
		//		<ClaimantEID>
		//		</ClaimantEID>
		//		<UnitID>
		//		</UnitID>
		//		<LOB>
		//		</LOB>
		//	    <IsTandE>
		//	    </IsTandE>
		//	    <ReserveTypeCode>
		//	    </ReserveTypeCode>
		//	    <DateEntered> 
		//	    </DateEntered> 
		//	    <Amount>
		//	    </Amount> 
		//	    <StatusCodeID>
		//	    </StatusCodeID>
		//	    <USer>
		//	    </USer>
		//	    <Reason>
		//	    </Reason>
		//	    <UserId>
		//	    </UserId>
		//	    <GroupId>
		//	    </GroupId>
		//      <Reserve>
		//     </Reserve>
		//	    <ChequeDate>
		//	    </ChequeDate>
		//	    <LoginName>
		//	    </LoginName>
		//	</ReserveFunds>
		//	</Document>
		///</param>
		/// <param name="p_objXmlOut">Xml document containing the output xml.
		//Structure of the output xml is as follows:-
		//<ReserveFunds>
		// <Reserves Claim="" Claimant="" Unit="" ReserveDesc="">
		// <Dropdowns>
		// <Status>
		// <Name Value="" Text="" Selected="" /> 
		// <Name Value="" Text="" /> 
		// <Name Value="" Text="" /> 
		// </Status>
		// <Reason>
		// <Name Value="" Text="" Selected="" /> 
		// <Name Value="" Text="" /> 
		// <Name Value="" Text="" /> 
		// <Name Value="" Text=" /> 
		// <Name Value="" Text="" /> 
		// <Name Value="" Text="" /> 
		// <Name Value="" Text="" /> 
		// <Name Value="" Text="" /> 
		// <Name Value="" Text="" /> 
		// </Reason>
		// </Dropdowns>
		// <ReserveHistory /> 
		// </Reserves>
		//</ReserveFunds>
		/// </param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
								
																   
		public bool GetReservesXML(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			int iReserve=0;
			string sUser="";
            XmlElement xElement;
            ReserveFunds objReserveFunds = null;
            ReserveCurrent objRC = null;
            int iClaimId = 0;
            int iClaimantEID = 0;
            int iUnitID = 0;
            int iReserveTypeCode = 0;
            //BOB Reserve Enhancement
            int iPolicyID = 0;
            int iPolCvgID = 0;
            int iCvgLossId = 0;
            string sDateEntered = "";
            double dAmount = 0;
            int iStatusCodeID = 0;
            string sReason = "";
            int iUserId = 0;
            int iGroupId = 0;
            XmlElement objXmlElement = null;
            DataModelFactory m_objDataModelFactory=null;
			try
			{
				//Initialize the application layer ReserveFunds class
                objReserveFunds = InitializeReserves(ref p_objXmlIn, ref p_objXmlOut, ref p_objErrOut);

                //objRC = PopulateReserveCurrentObject(ref p_objXmlIn);

                m_objDataModelFactory = new DataModelFactory(m_userLogin, base.ClientId);
                objRC = (ReserveCurrent)m_objDataModelFactory.GetDataModelObject("ReserveCurrent", true);
                objRC.FiringScriptFlag = 2;

                iClaimId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText);
                iClaimantEID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "ClaimantEID").InnerText);
                iUnitID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "UnitID").InnerText);

                //BOB Reserve Enhancement
                if (p_objXmlIn.SelectSingleNode("//" + "PolicyID")!=null)
                    iPolicyID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "PolicyID").InnerText);
                if (p_objXmlIn.SelectSingleNode("//" + "CoverageID") != null)
                    iPolCvgID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "CoverageID").InnerText);
                if (p_objXmlIn.SelectSingleNode("//" + "CvgLossID") != null)
                    iCvgLossId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "CvgLossID").InnerText);
             
                iReserveTypeCode = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "Reserve").InnerText);

                //pmittal5 Mits 18559 11/16/09 - Some nodes are missing in input Xml while Printing Reserves
                objXmlElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "DateEntered");
                if (objXmlElement != null)
                    sDateEntered = objXmlElement.InnerText;

                objXmlElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "Amount");
                if (objXmlElement != null)
                    dAmount = Conversion.ConvertStrToDouble(objXmlElement.InnerText);

                objXmlElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "StatusCodeID");
                if (objXmlElement != null)
                    iStatusCodeID = Conversion.ConvertStrToInteger(objXmlElement.InnerText);

                //sUser=p_objXmlIn.SelectSingleNode("//" + "User").InnerText;Reserves/@LoginUser
                if (p_objXmlIn.SelectSingleNode("//" + "Reserves") != null)
                {
                    if (p_objXmlIn.SelectSingleNode("//" + "Reserves").Attributes["LoginUser"] != null)
                        sUser = p_objXmlIn.SelectSingleNode("//" + "Reserves").Attributes["LoginUser"].Value;
                }
                objXmlElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "CvgLossId");
                if (objXmlElement != null)
                    iCvgLossId = Conversion.ConvertStrToInteger(objXmlElement.InnerText);
                

                objXmlElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "Reason");
                if (objXmlElement != null)
                    sReason = objXmlElement.InnerText;
                //End- pmittal5

                iUserId = base.userID;
                iGroupId = base.groupID;

                objRC.ClaimId = iClaimId;
                objRC.ClaimantEid = iClaimantEID;
                objRC.UnitId = iUnitID;
                objRC.ReserveTypeCode = iReserveTypeCode;
                objRC.DateEntered = sDateEntered;
                objRC.ReserveAmount = dAmount;
                objRC.ResStatusCode = iStatusCodeID;
                objRC.Reason = sReason;
                objRC.sUser = sUser;
                objRC.iUserId = iUserId;
                objRC.iGroupId = iGroupId;
                objRC.sUpdateType = "Reserves";



                ((DataRoot)objRC).InitializeScriptData();
                foreach (System.Collections.DictionaryEntry objError in objRC.Context.ScriptWarningErrors)
                {
                    p_objErrOut.Add("WarningScriptError", objError.Value.ToString(), Common.BusinessAdaptorErrorType.Warning);
                }

                xElement = (XmlElement)p_objXmlOut.SelectSingleNode("//DateEntered");
                xElement.InnerText = objRC.DateEntered.ToString();

                xElement = (XmlElement)p_objXmlOut.SelectSingleNode("//Amount");
                xElement.InnerText = objRC.ReserveAmount.ToString();

                xElement = (XmlElement)p_objXmlOut.SelectSingleNode("//StatusCodeID");
                xElement.InnerText = objRC.ResStatusCode.ToString();

                xElement = (XmlElement)p_objXmlOut.SelectSingleNode("//LoginUser");
                if(xElement != null)
                    xElement.InnerText = objRC.sUser.ToString();

                xElement = (XmlElement)p_objXmlOut.SelectSingleNode("//Reason");
                xElement.InnerText = objRC.Reason.ToString();

				sUser = base.loginName;
				iReserve=Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "Reserve").InnerText);
                objReserveFunds.LanguageCode = base.userLogin.objUser.NlsCode;  //Aman ML Change
                objReserveFunds.GetReservesXML(ref p_objXmlOut, objRC.ClaimId, objRC.ClaimantEid, objRC.UnitId, iReserve, sUser,iPolicyID,iPolCvgID,iCvgLossId );
			
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ReservesAdaptor.GetReservesXML.Error", base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
					objReserveFunds=null;
                    if (m_objDataModelFactory != null)
                        m_objDataModelFactory.Dispose();
                    if (objRC != null)
                        objRC.Dispose();
			}
		}
		/// <summary>
		///  This function is a wrapper over Riskmaster.Application.Reserves.ReserveFunds.GetXmlForInsReserveAmount() function 
		///  This method is basically used for getting XML in case the Payment exceeds the Reserve amount.
		/// </summary>
		/// <param name="p_objXmlIn">Input xml document for this function.
		// Following is the structure for input xml-:
		//  <Document>
		//		<ReserveFunds>
		//		<ClaimNumber /> 
		//		<ParentSecurityId /> 
		//		<SecurityId /> 
		//		 <XmlForInsReserveAmount>
		//		  <insufres scr="1" userid="2" groupid="7" claimid="1431">
		//		   <unit>0</unit> 
		//		   <claimant>0</claimant> 
		//		   <balance>0</balance> 
		//		   <reserve>0</reserve> 
		//		   <payment>23333333</payment> 
		//		   <setreserve>23333333</setreserve> 
		//		   <addreserve>23333333</addreserve> 
		//		   <newamount>0</newamount> 
		//		   <rcode id="370">Medical</rcode> 
		//		 </insufres>
		//		</XmlForInsReserveAmount>
		//		</ReserveFunds>
		//	</Document>
		/// </param>
		/// <param name="p_objXmlOut">Xml document containing the output xml.
		// Following is the structure for output xml-:
        //    <ReserveFunds>  
		//     <insufres scr="" userid="" groupid="" claimid="">
		//		<unit></unit>
		//		<claimant></claimant>
		//		<balance></balance>
		//		<reserve></reserve>
		//		<payment></payment>
		//		<setreserve></setreserve>
		//		<addreserve></addreserve>
		//		<newamount></newamount>
		//		<rcode id="">Medical</rcode>
		//	   </insufres>
		//  </ReserveFunds> 
		/// </param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool GetXmlForInsReserveAmount(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			string sUser="";
			ReserveFunds objReserveFunds=null;
			try
			{   //Initialize the application layer ReserveFunds class
				objReserveFunds=InitializeReserves(ref p_objXmlIn,ref p_objXmlOut,ref p_objErrOut);

				sUser=base.loginName;
				p_objXmlOut.DocumentElement.InnerXml=objReserveFunds.GetXmlForInsReserveAmount(p_objXmlIn.SelectSingleNode("//"+ "XmlForInsReserveAmount").InnerXml,sUser,p_objXmlIn.SelectSingleNode("//"+ "Reason").InnerXml).InnerXml;	
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.GetXmlForInsReserveAmount.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
					objReserveFunds=null;
			}

		}

        /// <summary>
        ///  This function is a wrapper over Riskmaster.Application.Reserves.ReserveFunds.GetXmlForInsReserve() function 
        ///  This method is basically used for getting XML in case the Payment exceeds the Reserve amount.
        /// </summary>
        /// <param name="p_objXmlIn">Input xml document for this function.
        // Following is the structure for input xml-:
        //  <Document>
        //		<ReserveFunds>
        //		<ClaimNumber /> 
        //		<ParentSecurityId /> 
        //		<SecurityId /> 
        //		 <XmlForInsReserveAmount>
        //		  <insufres scr="1" userid="2" groupid="7" claimid="1431">
        //		   <unit>0</unit> 
        //		   <claimant>0</claimant> 
        //		   <balance>0</balance> 
        //		   <reserve>0</reserve> 
        //		   <payment>23333333</payment> 
        //		   <setreserve>23333333</setreserve> 
        //		   <addreserve>23333333</addreserve> 
        //		   <newamount>0</newamount> 
        //		   <rcode id="370">Medical</rcode> 
        //		 </insufres>
        //		</XmlForInsReserveAmount>
        //		</ReserveFunds>
        //	</Document>
        /// </param>
        /// <param name="p_objXmlOut">Xml document containing the output xml.
        // Following is the structure for output xml-:
        //    <ReserveFunds>  
        //     <insufres scr="" userid="" groupid="" claimid="">
        //		<unit></unit>
        //		<claimant></claimant>
        //		<balance></balance>
        //		<reserve></reserve>
        //		<payment></payment>
        //		<setreserve></setreserve>
        //		<addreserve></addreserve>
        //		<newamount></newamount>
        //		<rcode id="">Medical</rcode>
        //	   </insufres>
        //  </ReserveFunds> 
        /// </param>
        /// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
        /// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
        public bool GetXmlForInsReserve(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sUser = "";
            ReserveFunds objReserveFunds = null;
            try
            {   //Initialize the application layer ReserveFunds class
                objReserveFunds = InitializeReserves(ref p_objXmlIn, ref p_objXmlOut, ref p_objErrOut);

                sUser = base.loginName;
                p_objXmlOut.DocumentElement.InnerXml = objReserveFunds.GetXmlForInsReserve(p_objXmlIn.SelectSingleNode("//" + "XmlForInsReserveAmount").InnerXml, sUser, p_objXmlIn.SelectSingleNode("//" + "Reason").InnerXml, base.GetSessionObject()).InnerXml;
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.GetXmlForInsReserveAmount.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objReserveFunds = null;
            }

        }

        public bool OffsetReserves(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sUser = "";
            ReserveFunds objReserveFunds = null;
            try
            {   
                objReserveFunds = new ReserveFunds(base.connectionString, this.m_userLogin, base.ClientId);//rkaur27

                objReserveFunds.OffsetReserves(Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "TransId").InnerXml));

                return true;
            }
            catch (RMAppException p_objException)
            {
               p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.GetXmlForInsReserveAmount.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objReserveFunds = null;
            }

        }
		/// <summary>
		///  This function is a wrapper over Riskmaster.Application.Reserves.ReserveFunds.UpdateFundsReserveCurrent() function.
		///  This method is used for updating current funds reserve. 
		/// </summary>
		/// <param name="p_objXmlIn">
		// Following is the structure for input xml-:
		// <Document>
		//	<ReserveFunds> 
		//	    <ClaimNumber> 
		//	    </ClaimNumber>
		//	    <ParentSecurityId>
		//	    </ParentSecurityId>
		//	    <SecurityId>
		//	    </SecurityId>
		//		<ClaimId>
		//		</ClaimId>
		//		<ClaimantEID>
		//		</ClaimantEID>
		//		<UnitID>
		//		</UnitID>
		//		<LOB>
		//		</LOB>
		//	    <IsTandE>
		//	    </IsTandE>
		//	    <ReserveTypeCode>
		//	    </ReserveTypeCode>
		//	    <DateEntered> 
		//	    </DateEntered> 
		//	    <Amount>
		//	    </Amount> 
		//	    <StatusCodeID>
		//	    </StatusCodeID>
		//	    <USer>
		//	    </USer>
		//	    <Reason>
		//	    </Reason>
		//	    <UserId>
		//	    </UserId>
		//	    <GroupId>
		//	    </GroupId>
		//      <Reserve>
		//     </Reserve>
		//	    <ChequeDate>
		//	    </ChequeDate>
		//	    <LoginName>
		//	    </LoginName>
		//	</ReserveFunds> 
		//	</Document>
		///</param>
		/// <param name="p_objXmlOut">Nothing.</param>
		/// <param name="p_objErrOut">This will contain the errors occured during the processing of this function.</param>
		/// <returns>'True' if function call succeeds or 'false' if it fails.</returns>
		public bool UpdateFundsReserveCurrent(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			int iClaimId=0;
			int iClaimantEID=0;
			int iUnitID=0;
			int iLOB=0;
			string sChequeDate="";
			string sLoginName="";
			ReserveFunds objReserveFunds=null;
			try
			{
				//Initialize the application layer's ReserveFunds class.
				objReserveFunds=InitializeReserves(ref p_objXmlIn,ref p_objXmlOut,ref p_objErrOut);

				iClaimId=Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "ClaimId").InnerText);
				iClaimantEID=Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "ClaimantEID").InnerText);
				iUnitID=Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "UnitID").InnerText);
				iLOB=Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "LOB").InnerText);
				sChequeDate=p_objXmlIn.SelectSingleNode("//" + "ChequeDate").InnerText;
				sLoginName=p_objXmlIn.SelectSingleNode("//" + "LoginName").InnerText;
				objReserveFunds.UpdateFundsReserveCurrent(iClaimId,iClaimantEID,iUnitID,iLOB,sChequeDate,sLoginName);
				return true;

			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.UpdateFundsReserveCurrent.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;

			}
			finally
			{
					objReserveFunds=null;
			}

		}

        #region User Preferences
        //RMA-345 Start     Achouhan3   Methods tp get/set User preferences
        /// </summary>
        /// <param name="p_objXmlIn">XmlDocument</param>
        /// <param name="p_objXmlOut">XmlDocument</param>
        /// <param name="p_objErr">BusinessAdaptorErrors</param>
        /// <returns>bool</returns>
        public bool SetUserPref(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            try
            {
                Riskmaster.Common.CommonFunctions.SetUserPref(p_objXmlIn, m_userLogin.UserId, connectionString, base.ClientId);
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ExecutiveSummary.SaveUserPreference.Err", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }
        /// </summary>
        /// <param name="p_objXmlIn">XmlDocument</param>
        /// <param name="p_objXmlOut">XmlDocument</param>
        /// <param name="p_objErr">BusinessAdaptorErrors</param>
        /// <returns>bool</returns>
        public bool GetUserPrefXML(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            try
            {
                Riskmaster.Common.CommonFunctions.GetUserPrefXML(p_objXmlIn, ref p_objXmlOut, m_userLogin.UserId, connectionString, base.ClientId);
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("ExecutiveSummary.GetUserPreference.Err",base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }
        //34278 End
        #endregion
		#endregion

        //Added by sharishkumar for Mits 35472
        public bool ReserveExport(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            string iRcRowID = string.Empty;            
            string sDSN = string.Empty;
            ReserveFunds objReserveFunds = null;
            try
            {
                iRcRowID = p_objXmlIn.SelectSingleNode("//ReserveID").InnerText;
                objReserveFunds = new ReserveFunds(base.connectionString, this.m_userLogin, base.ClientId);//rkaur27               
                objReserveFunds.LSSReserveExport(iRcRowID);                
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ReservesAdaptor.ReserveExport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objReserveFunds = null;
            }
            return true;
        }
        //End Mits 35472
		#region Private functions
		/// <summary>
		/// This function will instantiate the application layer's ReserveFunds class.
		/// </summary>
		/// <param name="p_objInputDoc">Input xml containing the various parameters for ReserveFunds class instantiation.</param>
		/// <param name="p_objXmlDoc">Output xml document.</param>
		/// <param name="p_objErr">Error collection.</param>
		/// <returns>Instance of application layer's ReserveFunds class.</returns>
		private ReserveFunds InitializeReservesBOB(ref XmlDocument p_objInputDoc,ref XmlDocument p_objXmlDoc,ref BusinessAdaptorErrors p_objErr)
		{
			ReserveFunds objFunds=null;
			try
			{
				objFunds=new ReserveFunds(base.connectionString,p_objInputDoc.SelectSingleNode("//" + "ClaimNumber").InnerText, this.m_userLogin, base.ClientId);//rkaur27
                if (p_objXmlDoc == null)
                {
                    p_objXmlDoc = new XmlDocument();
                }
                p_objXmlDoc.LoadXml("<ReserveFunds><DateEntered/><Amount/><StatusCodeID/><LoginUser/><ReasonCode/><Reason/><Reserves><Dropdowns><Status/><Reason/></Dropdowns><ReserveHistory/></Reserves></ReserveFunds>");
				p_objErr=new BusinessAdaptorErrors(base.ClientId);
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			return objFunds;
		}

		private ReserveFunds InitializeReserves(ref XmlDocument p_objInputDoc,ref XmlDocument p_objXmlDoc,ref BusinessAdaptorErrors p_objErr)
		{
			ReserveFunds objFunds=null;
			try
			{
				objFunds=new ReserveFunds(base.connectionString,p_objInputDoc.SelectSingleNode("//" + "ClaimNumber").InnerText,Conversion.ConvertStrToInteger(p_objInputDoc.SelectSingleNode("//" + "ParentSecurityId").InnerText),Conversion.ConvertStrToInteger(p_objInputDoc.SelectSingleNode("//" + "SecurityId").InnerText), this.m_userLogin, base.ClientId);//rkaur27
                if (p_objXmlDoc == null)
                {
                    p_objXmlDoc = new XmlDocument();
                }
                p_objXmlDoc.LoadXml("<ReserveFunds><DateEntered/><Amount/><StatusCodeID/><LoginUser/><ReasonCode/><Reason/><Reserves><Dropdowns><Status/><Reason/></Dropdowns><ReserveHistory/></Reserves></ReserveFunds>");
				p_objErr=new BusinessAdaptorErrors(base.ClientId);
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			return objFunds;
		}
		
		
        ///// <summary>
        ///// This function will instantiate the application layer's ReserveFunds class.
        ///// </summary>
        ///// <param name="p_objInputDoc">Input xml containing the various parameters for ReserveFunds class instantiation.</param>
        ///// <param name="p_objXmlDoc">Output xml document.</param>
        ///// <param name="p_objErr">Error collection.</param>
        ///// <returns>Instance of application layer's ReserveFunds class.</returns>
        //private ReserveFunds InitializeReservesBOB(ref XmlDocument p_objInputDoc, ref XmlDocument p_objXmlDoc, ref BusinessAdaptorErrors p_objErr)
        //{
        //    ReserveFunds objFunds = null;
        //    try
        //    {
        //        objFunds = new ReserveFunds(base.connectionString, p_objInputDoc.SelectSingleNode("//" + "ClaimNumber").InnerText, Conversion.ConvertStrToInteger(p_objInputDoc.SelectSingleNode("//" + "ParentSecurityId").InnerText), Conversion.ConvertStrToInteger(p_objInputDoc.SelectSingleNode("//" + "SecurityId").InnerText), this.m_userLogin);
        //        if (p_objXmlDoc == null)
        //        {
        //            p_objXmlDoc = new XmlDocument();
        //        }
        //        p_objXmlDoc.LoadXml("<ReserveFunds><DateEntered/><Amount/><StatusCodeID/><LoginUser/><ReasonCode/><Reason/><Reserves><Dropdowns><Status/><Reason/></Dropdowns><ReserveHistory/></Reserves></ReserveFunds>");
        //        p_objErr = new BusinessAdaptorErrors();
        //    }
        //    catch (Exception p_objException)
        //    {
        //        throw p_objException;
        //    }
        //    return objFunds;
        //}

        #endregion

        //START : rkotak : JIRA: RMA-4608  - RMA Swiss Re Financial Detail History
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetFinancialDetailHistory(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ReserveFunds objReserveFunds = null;
            int iClaimID = 0;
            int iClaimantID = 0;
            int iUnitID = 0;
            int iPolicyID = 0;
            int iCoverageID = 0;
            int iSummaryLevel = 0;
            string sClaimCurrency = "";
            string spreviousvalue = "";
            string sselectedvalue = "";
            string sReserveStatus = "";
            int iCoverageLossID = 0;
            int iReserveTypeCode = 0;
            int iRCRowId = 0;
            bool success;
            string sResponseType = "JSON";
            string sGridId = "";
            try
            {
                iClaimID = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText, out success);
                iClaimantID = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("//ClaimantEID").InnerText, out success);
                iPolicyID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//PolicyID").InnerText);
                iUnitID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//UnitID").InnerText);
                iCoverageID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//CoverageID").InnerText);
                sClaimCurrency = (p_objXmlIn.SelectSingleNode("//claimcurrency").InnerText);
                iSummaryLevel = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("//SelectedLevel").InnerText, out success);
                iCoverageLossID = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("//CoverageLossID").InnerText, out success);
                iReserveTypeCode = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("//ReserveTypeCode").InnerText, out success);
                sReserveStatus = (p_objXmlIn.SelectSingleNode("//ReserveStatus").InnerText);
                iRCRowId = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("//RCrowId").InnerText, out success);
                sGridId = (p_objXmlIn.SelectSingleNode("//GridId").InnerText);
                int IsLandingFromClaimant = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("//IsLandingFromClaimant").InnerText, out success);
                //RMA-9681 Starts

                //iPageID = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("//PageID").InnerText, out success);
                string sPageName = (p_objXmlIn.SelectSingleNode("//PageName").InnerText);
                if ((p_objXmlIn.SelectSingleNode("//ContentType") != null))
                    sResponseType = (p_objXmlIn.SelectSingleNode("//ContentType").InnerText);
                //RMA-9681 Ends
                objReserveFunds = new ReserveFunds(base.connectionString, this.m_userLogin, base.ClientId);//rkaur27
                {
                    if (sResponseType == "XML")
                    {
                        //call a method which will return pure xml response
                        //since FDH is a complete new page, no method will be present which returns pure XML repsonse for existing rmA grids
                    }
                    else
                    {
                        //call a method which will return Json string embedded inside XML response. 
                        //p_objXmlOut = objReserveFunds.GetFinancialDetailHistory(iClaimID, iClaimantID, iPolicyID, iUnitID, iCoverageID, sClaimCurrency, iSummaryLevel, base.userLogin, iCoverageLossID, iReserveTypeCode, iRCRowId, sReserveStatus, sPageName, sGridId);
                        p_objXmlOut = objReserveFunds.GetFinancialDetailHistory(iClaimID, iClaimantID, iPolicyID, iUnitID, iCoverageID, sClaimCurrency, iSummaryLevel, base.userLogin, iCoverageLossID, iReserveTypeCode, iRCRowId, sReserveStatus, sPageName, sGridId, IsLandingFromClaimant);
                    }

                    //RMA-9681 Ends
                }
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ReserveFunds.GetFinancialDetailSummary.GenericError", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objReserveFunds = null;
            }
        }
        
        /// <summary>
        /// This functio is used to get default user pref for the grid
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErr"></param>
        /// <returns></returns>
        public bool RestoreDefaults(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            string sPageName = "";
            ReserveFunds objReserveFunds = null;
            string sGridId = "";
            string sUserPrefString = "";
            XmlElement oUserPrefElement;
            try
            {
                objReserveFunds = new ReserveFunds(base.connectionString, this.m_userLogin, base.ClientId);//rkaur27
                if (p_objXmlIn.SelectSingleNode("//PageName") != null)
                    sPageName = p_objXmlIn.SelectSingleNode("//PageName").InnerText.Trim();
                if (p_objXmlIn.SelectSingleNode("//GridId") != null)
                    sGridId = p_objXmlIn.SelectSingleNode("//GridId").InnerText.Trim();
                sUserPrefString = objReserveFunds.RestoreDefaultUserPref(m_userLogin.UserId, sPageName, m_userLogin.DatabaseId, sGridId);
                oUserPrefElement = p_objXmlOut.CreateElement("UserPref");
                oUserPrefElement.InnerText = sUserPrefString;
                p_objXmlOut.AppendChild(oUserPrefElement);
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("GridPreference.RestoreDefaults.Err", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }

        //END : rkotak : JIRA: RMA-4608  - RMA Swiss Re Financial Detail History
	}
}
