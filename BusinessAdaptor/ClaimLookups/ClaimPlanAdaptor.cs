using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Application.ClaimLookups;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: ClaimPlanAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 24-Aug-2005
	///* $Author	: Nikhil Kr. Garg
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for Claim Plan which 
	///	contains method to get ClaimPlan List.	
	/// </summary>
	public class ClaimPlanAdaptor: BusinessAdaptorBase
	{
		#region Constructor
		public ClaimPlanAdaptor(){}
		#endregion
		
		#region Public methods

		/// <summary>
		///		This function returns xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///		Output structure is as follows-:
		///		<plans>
		///			<plan planid="1" planname="Test Plan One Test P" plannumber="A1" effectivedate="7/2/1990" expirationdate="7/1/2010" classnames="1;Class One|7;Class Three|2;Class Two" />
		///		</plans>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool Get(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			LookupClaimPlan objLookupClaimPlan=null;
			XmlElement objElement=null;				//used for parsing the input xml
			int iClaimTypeCode=0;
			string sEventDate=string.Empty;
			string sClaimDate=string.Empty;
			int iDeptEID=0;

			try
			{
				//check existence of Claim Type Code which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ClaimTypeCode");
				if (objElement==null)
				{
					p_objErrOut.Add("ClaimPlanAdaptorParameterMissing",Globalization.GetString("ClaimPlanAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				iClaimTypeCode=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of Event Date which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//EventDate");
				if (objElement==null)
				{
					p_objErrOut.Add("ClaimPlanAdaptorParameterMissing",Globalization.GetString("ClaimPlanAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				if (objElement.InnerText.Trim()!=string.Empty)
					sEventDate=Conversion.ToDbDate(Convert.ToDateTime(objElement.InnerText));

				//check existence of Claim Date which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ClaimDate");
				if (objElement==null)
				{
					p_objErrOut.Add("ClaimPlanAdaptorParameterMissing",Globalization.GetString("ClaimPlanAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				if (objElement.InnerText.Trim()!=string.Empty)
					sClaimDate=Conversion.ToDbDate(Convert.ToDateTime(objElement.InnerText));

				//check existence of Dept EID which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//DeptEID");
				if (objElement==null)
				{
					p_objErrOut.Add("ClaimPlanAdaptorParameterMissing",Globalization.GetString("ClaimPlanAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				iDeptEID=Conversion.ConvertStrToInteger(objElement.InnerText);

				objLookupClaimPlan = new LookupClaimPlan(base.userLogin.objRiskmasterDatabase.DataSourceName,base.userLogin.LoginName,base.userLogin.Password, base.ClientId);

				//get Plan List
				p_objXmlOut=objLookupClaimPlan.GetLookupClaimPlanList(iClaimTypeCode,sEventDate,sClaimDate,iDeptEID);
				
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("ClaimPlanAdaptor.Get.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objLookupClaimPlan = null;
				objElement=null;
			}

		}

		#endregion
	}
}
