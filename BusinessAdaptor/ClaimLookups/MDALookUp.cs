using System;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using System.IO;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Application.ClaimLookups;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for MDALookUp.
	/// </summary>
	public class MDALookUpAdaptor: BusinessAdaptorBase
	{
		#region Constructor
		public MDALookUpAdaptor()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		#endregion

		#region Public methods

		/// <summary>
		///		This function returns xml containing data to be shown on screen
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///		Output structure is as follows-:
		///		<plans>
		///			<plan planid="1" planname="Test Plan One Test P" plannumber="A1" effectivedate="7/2/1990" expirationdate="7/1/2010" classnames="1;Class One|7;Class Three|2;Class Two" />
		///		</plans>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetMDATopicDetails(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			MDALookUp objMDALookUp=null;
			XmlElement objElement=null;				//used for parsing the input xml
			int iJobClassId=0;
			string sMedicalCode="";
			
			try
			{
				//check existence of Claim Type Code which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//JobClassID");
				if (objElement==null)
				{
					p_objErrOut.Add("MDALookupAdaptorParameterMissing",Globalization.GetString("MDALookUpAdaptor.GetMDATopicDetails.ParameterMissing.Job",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				iJobClassId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//check existence of Event Date which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//MedicalCode");
				if (objElement==null)
				{
					p_objErrOut.Add("MDALookupAdaptorParameterMissing",Globalization.GetString("MDALookUpAdaptor.GetMDATopicDetails.ParameterMissing.MedicalCode",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				sMedicalCode=objElement.InnerText;
				objMDALookUp = new MDALookUp(base.ClientId);

				//get MDA Topics
				p_objXmlOut=objMDALookUp.GetMDATopicDetails(base.connectionString,iJobClassId,sMedicalCode);
				
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MDALookUpAdaptor.GetMDATopicDetails.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objMDALookUp = null;
				objElement=null;
			}

		}


		/// <summary>
		/// This method is a wrapper to Riskmaster.Application.ClaimLookups.MDALookup.GetMedicalDisabilityGuideline() method.
		/// Gets the help file for the selected topic id	
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<MedicalGuideline>
		///				<TopicId></TopicId>
		///			</MedicalGuideline>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">XML containing the results
		///		The structure of output XML document would be:
		///		<Sentinel>
		///			<File Name="name of file">
		///				data for the file in Base64 encoding
		///			</File>
		///		</Sentinel>
		/// </param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool GetMedicalDisabilityGuidelineFile(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			int iTopicId=0;							//Event id passed in Input xml document
			XmlElement objOutElement=null;			//used for creating the Output Xml
			XmlElement objElement=null;				//used for parsing the input xml
			string sMDAHelpFilePath=string.Empty;	//MDA Help file path
			MemoryStream objGuidelineFile=null;		//stream of file
			MDALookUp objMDALookUp=null;

			try
			{
				//check existence of TopicId which is required
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TopicId");
				if (objElement==null)
				{
					p_objErrOut.Add("MDALookupAdaptorParameterMissing",Globalization.GetString("MDALookupAdaptorParameterMissing",base.ClientId),BusinessAdaptorErrorType.Error);
					return false;
				}
				iTopicId=Conversion.ConvertStrToInteger(objElement.InnerText);

				//read the other parameters/settings from the config file
                sMDAHelpFilePath = Path.Combine(RMConfigurator.AppFilesPath, "\\MDA\\HTML\\");

				//get the help file
				objMDALookUp = new MDALookUp(base.ClientId);
				objMDALookUp.GetMedicalDisabilityGuidelineFile(sMDAHelpFilePath, iTopicId, out objGuidelineFile);		

				//create the output xml document containing the data in Base64 format
				objOutElement=p_objXmlOut.CreateElement("GuidelineFile");
				p_objXmlOut.AppendChild(objOutElement);
				objOutElement=p_objXmlOut.CreateElement("File");
                objOutElement.SetAttribute("Name", String.Format("{0}.html", iTopicId));
				objOutElement.InnerText=Convert.ToBase64String(objGuidelineFile.ToArray());
				p_objXmlOut.FirstChild.AppendChild(objOutElement);
				return true;
			}
			catch(InvalidValueException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(FileInputOutputException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MDALookUpAdaptor.GetMedicalDisabilityGuidelineFile.Error",base.ClientId),BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objGuidelineFile = null;
				objElement=null;
				objMDALookUp=null;
			}
		}

		#endregion
	}
}
