using System;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.OSHALib;
using Riskmaster.Common;
using Riskmaster.Application.ReportInterfaces;
using System.IO;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Db;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: OSHAAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 21-Feb-2005
	///* $Author	: Tanuj Narula
	///* $Comment	: 
	///* $Source	: 
	///************************************************************** 
	/// <summary>	
	/// This class will interact with Osha application component to generate the osha reports.	
	/// </summary>
	public class OSHAAdaptor:BusinessAdaptorBase
	{
		#region Consructor
		/// <summary>
		/// This is the default constructor.
		/// </summary>
		public OSHAAdaptor()
		{
		}
		#endregion

		#region Public functions
		/// Name		: RunReport
		/// Author		: Tanuj Narula
		/// Date Created: 23-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This is the method which will be called by the web service to generate the osha reports.
		/// </summary>
		/// <param name="p_objDoc">Input xml document.</param>
		/// <param name="p_objXmlOut">
		/// Structure of output xml-:
		/// <Osha>
		/// <File Name="">
		/// </File>
		/// </Osha>
		/// </param>
		/// <param name="errOut">This collection will contain all the errors occurred during the execution of this method.</param>
		/// <returns></returns>
		public bool RunReport(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			int iReportType=0;
            string sOutputFolder = string.Empty, sReportType = string.Empty;
			MemoryStream objMs=null;
			OshaManager objMgr=null;
			XmlAttribute objXmlAttribute= null;
			try
			{
				p_objXmlOut=new XmlDocument();
				p_objErrOut=new BusinessAdaptorErrors(base.ClientId);
				objXmlAttribute =p_objDocIn.SelectSingleNode("report").Attributes["type"];
                sOutputFolder = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "OshaPdfs");
				if(string.IsNullOrEmpty(sOutputFolder) || !Directory.Exists(sOutputFolder))
				{
					p_objErrOut.Add("1",Globalization.GetString("OSHALibAdapter.RunReport.FilePathError", base.ClientId),BusinessAdaptorErrorType.Message);//rkaur27
					return false;
				}
				if(objXmlAttribute==null)
				{
					objXmlAttribute = p_objDocIn.SelectSingleNode("report").Attributes["reporttype"];
				}
				sReportType = objXmlAttribute.Value;
				if(Conversion.IsNumeric(sReportType))
				{
					iReportType = Conversion.ConvertStrToInteger(sReportType);    
				}
				GetOSHAPreparer(ref p_objDocIn);
				objMgr=new OshaManager(base.connectionString,iReportType, base.ClientId);//rkaur27
				objMgr.Init();
				objMgr.CriteriaXmlDom=p_objDocIn;
				objMgr.OutputDirectory=sOutputFolder;
				objMgr.OutputFormat=eReportFormat.Pdf;
				objMgr.RunReport();
                
				//Construct the output document.
				MakeOutputDocumentWithFileContent("Osha",ref p_objXmlOut,(string)objMgr.ReportOutput[0]);

			}
			catch(RMAppException p_objException)
			{
                p_objErrOut.Add(p_objException, p_objException.Message, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.SystemError);//rkaur27
				return false;

			}
			finally
			{

				if(objMs!=null)
				{
					objMs.Close();
					objMs=null;
				}
				objMgr=null;
			} 
			return true;
		}
		#endregion
		# region Osha Recordablility Wizard
		public bool OshaRecordabilityWizard(XmlDocument p_objDocIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			string sEventId="";
			int iEventId=0;
			XmlNode objNode=null;
			MemoryStream objMs=null;
			OshaManager objMgr=null;
			try
			{
				p_objXmlOut=new XmlDocument();
                p_objErrOut = new BusinessAdaptorErrors(base.ClientId);
				objNode =p_objDocIn.SelectSingleNode("//EventId");
				sEventId = objNode.InnerXml;
				if(Conversion.IsNumeric(sEventId))
				{
					iEventId = Conversion.ConvertStrToInteger(sEventId);    
				}
				else
				{
					p_objErrOut.Add("",Globalization.GetString("OSHALibAdaptor.OshaRecordabilityWizard.EventIdNotFound", base.ClientId),BusinessAdaptorErrorType.SystemError);//rkaur27
					return false;
				}
				objMgr=new OshaManager(base.ClientId);
				p_objXmlOut=objMgr.OshaRecordabilityWizard(sEventId,base.connectionString);
                
				//Construct the output document.
				
			}
			catch(RMAppException p_objException)
			{
                p_objErrOut.Add(p_objException, p_objException.Message, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("OSHALibAdapter.RunReport.Error", base.ClientId), BusinessAdaptorErrorType.SystemError);//rkaur27
				return false;

			}
			finally
			{

				if(objMs!=null)
				{
					objMs.Close();
					objMs=null;
				}
				objMgr=null;
			} 
			return true;
		}
		#endregion

		#region Private function
		/// Name		: MakeOutputDocumentWithFileContent
		/// Author		: Tanuj Narula
		/// Date Created: 23-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function appends the output file to the output xml document.
		/// </summary>
		/// <param name="p_sModuleName">Name of the root element of the xml document.</param>
		/// <param name="p_objXmlOut">Document to which output file to be appended.</param>
		/// <param name="p_sOutFilepath">Name of the output file.</param>
		private void MakeOutputDocumentWithFileContent(string p_sModuleName,ref XmlDocument p_objXmlOut,string p_sOutFilepath)
		{
			XmlElement objRootElement=null;
			XmlElement objElement=null;
			MemoryStream objMs=null;

			try
			{
				objRootElement=p_objXmlOut.CreateElement(p_sModuleName);
				objElement=p_objXmlOut.CreateElement("File");
				//Convert file stream to memory stream.
                objMs = Utilities.ConvertFilestreamToMemorystream(p_sOutFilepath, base.ClientId);
				
				objElement.SetAttribute("Name",p_sOutFilepath.Substring(p_sOutFilepath.LastIndexOf(@"\")+1));
                objElement.InnerText=Convert.ToBase64String(objMs.GetBuffer());
				objRootElement.AppendChild(objElement);
				p_objXmlOut.AppendChild(objRootElement);
			}
			catch(Exception p_objErr)
			{
				throw p_objErr;
			}
			finally
			{
				objRootElement=null;
				objElement=null;
				if(objMs!=null)
				{
					objMs.Close();
					objMs=null;
				}
			}
		}

		/// <summary>
		/// Retrieve the preparer infor. It's set by Utilities/Tools/FROIPrepareInfo
		/// </summary>
		private void GetOSHAPreparer(ref XmlDocument oDocIn)
		{
			string sSQL = "";
			string sPrefXML = "";
			XmlElement oElem = null;
			XmlElement oElem2 = null;
			XmlDocument oDocument = null;
			DbReader objReader = null;
			string sTemp = "";

			if( m_userLogin == null )
				return;

			try
			{
				sSQL = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = " + m_userLogin.UserId;
				objReader = DbFactory.GetDbReader(base.connectionString, sSQL);
				if( objReader.Read() )
				{
					sPrefXML = objReader.GetString("PREF_XML");
					oDocument = new XmlDocument();
					oDocument.LoadXml(sPrefXML);
					oElem = (XmlElement)oDocument.SelectSingleNode("/setting/FROIOptions");
					if( oElem != null )
					{
						//Set preparer name
						sTemp = oElem.Attributes["name"].InnerText;
						oElem2 = (XmlElement)oDocIn.SelectSingleNode("//form/group/control[@name='preparername']");
						if( oElem2 != null )
							oElem2.SetAttribute("value", sTemp);

						//Set preparer title
						sTemp = oElem.Attributes["title"].InnerText;
						oElem2 = (XmlElement)oDocIn.SelectSingleNode("//form/group/control[@name='preparertitle']");
						if( oElem2 != null )
							oElem2.SetAttribute("value", sTemp);

						//Set preparer phone
						sTemp = oElem.Attributes["phone"].InnerText;
						oElem2 = (XmlElement)oDocIn.SelectSingleNode("//form/group/control[@name='preparerphone']");
						if( oElem2 != null )
							oElem2.SetAttribute("value", sTemp);
					}
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException("OshaAdaptor.GetSSHAPreparer",p_objException);
			}
			finally
			{
				if( objReader != null )
					objReader.Close();
			}

		}
		#endregion 
	}
}
