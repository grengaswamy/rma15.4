﻿
/**********************************************************************************************
*   Date     |  MITS/JIRA      | Programmer | Description                                            *
**********************************************************************************************
* 08/08/2014 | 36030/RMA-337   | nshah28    | Changes for save mail merge setting 
**********************************************************************************************/

using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.Application.DocumentManagement;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.MailMerge;
using Riskmaster.Application.FileStorage;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Settings;
using Riskmaster.Application.AcrosoftWebserviceWrapper;
using Riskmaster.DataModel;
using Riskmaster.Application.PaperVisionWrapper;  
using Riskmaster.Application.MediaViewWrapper;

namespace Riskmaster.BusinessAdaptor
{

	///************************************************************** 
	///* $File		: MailMergeAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 18-Feb-2005
	///* $Author	: Parag Sarin
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	
	/// <summary>	
	///	This class is used to call the application layer component for Mail Merge which 
	///	contains methods to generate merged documents.	
	/// </summary>
	public class MailMergeAdaptor:BusinessAdaptorBase
	{

		private int m_iDocPathType=0;
        private string m_sDocPath = string.Empty;

#region Constants
        //spahariya MITS 28867
        public const int RMB_WMEMAIL_DET = 89;
        public const int RMB_WMEMAIL_DET_CREATE = 90;
        public const int RMB_WMEMAIL_DET_UPDATE = 92;
        public const int RMB_WMEMAIL_DET_DELETE = 91;
        //spahariya-End
#endregion
		#region Constructor
		public MailMergeAdaptor()
		{
            //Initiate all Acrosoft settings
            //TODO: Determine if this instantiation needs to be present in multiple classes
            //TODO: or if it can be instantiated and managed in a central location
            RMConfigurationManager.GetAcrosoftSettings();
            //Mona:PaperVisionMerge : Animesh Inserted 
            RMConfigurationManager.GetPaperVisionSettings();
            //Animesh Insertion Ends
        }
		#endregion

		#region Public methods

		/// <summary>
		/// This function returns custom templates list
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		/// <CustomTemplateList>
		///		<Templates>
		///			<Category Catid="" CategoryDesc="">
		///				<Template Category="" TemplateDesc="" TemplateId=""/>
		///			</Category>
		///		</Templates>
		///	</CustomTemplateList>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetCustomTemplateList(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
				objTemplateManager=new TemplateManager( m_userLogin,base.ClientId );
				
				objTemplateManager.DSN=connectionString;
				objTemplateManager.UID= userID.ToString();
				p_objXmlOut=objTemplateManager.GetCustomTemplateList();
				return true;

			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{			
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetCustomTemplateList.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objTemplateManager=null;
			}
		}

		/// <summary>
		/// Gets the information related to currently loaded template
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<TemplateId>Form Name</TemplateId>
		///			</Template>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		/// <GetCurrentTemplateInfo>
		///		<Template  CatId="0" FormFileName="" FormId="" FormName="" Prefab="" IsNew="" FormDesc=""/>
		///	</GetCurrentTemplateInfo>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetCurrentTemplateInfo(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			XmlNode objNode=null;
			
			try
			{
                //mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);

				objTemplateManager.SecurityDSN=securityConnectionString;
				objTemplateManager.DocStorageDSN = m_sDocPath;
				objTemplateManager.DSN=connectionString;
				objTemplateManager.DocPathType =m_iDocPathType;
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/TemplateId");
				if (objNode.InnerText!=null)
				{
					objTemplateManager.MoveTo(objNode.InnerText);
					p_objXmlOut=objTemplateManager.GetCurrentTemplateInfo();
				}
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetCurrentTemplateInfo.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objTemplateManager=null;
			}
		}

		/// <summary>
		/// This function converts Table name to Category id
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<TableName>TableName</TableName>
		///			</Template>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///<Template><CatId></CatId></Template>
		///</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool RMTableNameToCatId(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			MergeManager objMergeManager=null;
			XmlNode objNode=null;
			XmlNode objTemplateNode=null;
			
			try
			{
                //mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
				objMergeManager=new MergeManager(userLogin,securityConnectionString,
					userID.ToString(),base.ClientId);//dvatsa-cloud
				
				
				
				objTemplateNode=p_objXmlIn.SelectSingleNode(@"/Template/TableName");
				objNode=p_objXmlOut.CreateElement("Template");
				p_objXmlOut.AppendChild(objNode);
				objNode=p_objXmlOut.CreateElement("CatId");
				if (objNode.InnerText!=null)
				{
					objNode.InnerText=objMergeManager.RMTableNameToCatId(objTemplateNode.InnerText).ToString();
				}
				p_objXmlOut.FirstChild.AppendChild(objNode);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.RMTableNameToCatId.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objMergeManager=null;
				objTemplateNode=null;
			}
		}
        // Removed By nitika For AutoMailmerge setUp
        //private string CatIdToRMTableName(string p_sCatId)
        //{
        //    MergeManager objMergeManager=null;
        //    string sValue="";
        //    objMergeManager=new MergeManager(userLogin,securityConnectionString, userID.ToString());
        //    sValue=objMergeManager.CatIdToRMTableName(Conversion.ConvertStrToLong(p_sCatId));
        //    return sValue;
        //}
		/// <summary>
		/// This function converts Category id to Table name
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<CatId>TableName</CatId>
		///			</Template>
		///		</Document>
		///</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///<Template><Tablename></Tablename></Template>
		///</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool CatIdToRMTableName(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			MergeManager objMergeManager=null;
			XmlNode objNode=null;
			XmlNode objTemplateNode=null;
			
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
				objMergeManager=new MergeManager(userLogin,securityConnectionString,userID.ToString(),base.ClientId);//dvatsa-cloud
				
				objTemplateNode=p_objXmlIn.SelectSingleNode(@"/Template/CatId");
				objNode=p_objXmlOut.CreateElement("Template");
				p_objXmlOut.AppendChild(objNode);
				objNode=p_objXmlOut.CreateElement("Tablename");
				if (objNode.InnerText!=null)
				{
					objNode.InnerText=objMergeManager.CatIdToRMTableName(Conversion.ConvertStrToLong(objTemplateNode.InnerText));
				}
				p_objXmlOut.FirstChild.AppendChild(objNode);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.CatIdToRMTableName.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objMergeManager=null;
				objTemplateNode=null;
			}
		}

		/// <summary>
		/// This function returns the table name 
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<FormName>FormName</FormName>
		///			</Template>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///<Template><TableName></TableName></Template>
		///</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool MapFormName(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			Generic objGeneric=null;
			XmlNode objNode=null;
			
			XmlNode objTempNode=null;
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
				objGeneric=new Generic(base.ClientId);//rkaur27
				
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/FormName");
				objTempNode=p_objXmlOut.CreateElement("Template");
				p_objXmlOut.AppendChild(objTempNode);
				objTempNode=p_objXmlOut.CreateElement("TableName");
				if (objNode.InnerText!=null)
				{
					objTempNode.InnerText=Generic.MapFormName(objNode.InnerText,"");
				}
				p_objXmlOut.FirstChild.AppendChild(objTempNode);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.MapFormName.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objGeneric=null;
				objTempNode=null;
			}
		}

		/// <summary>
		/// This function returns the Table name and primary key of passed table
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<TableName>TableName</TableName>
		///			</Template>
		///		</Document>
		///</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///<TableName><MapTableName></MapTableName><PrimaryId></PrimaryId></TableName>
		///</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool MapTableName(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			Generic objGeneric=null;
			XmlNode objNode=null;
			
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
				objGeneric=new Generic(base.ClientId);//rkaur27
				
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/TableName");
				if (objNode.InnerText!=null)
				{
					p_objXmlOut.LoadXml(Generic.MapTableName(objNode.InnerText,"",""));
				}
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.MapTableName.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objGeneric=null;
			}
		}

		/// <summary>
		/// Generates XML string for a merged record
		/// Format of Xml is :
		/// <?xml-stylesheet type='text/xsl' href='records.xsl'?>
		/// <mergedata recordid='1' catid='2' docid='1157' formname='test24Nov1' docresultflag='False' >
		///       <header> 
		///       <column>Comp. Abbrev.</column> 
		///       <column>Comp. City</column>
		///       </header>
		/// </mergedata>
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<TemplateId>TemplateId</TemplateId>
		///				<RecordId>RecordId</RecordId>
		///			</Template>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///  <?xml-stylesheet type='text/xsl' href='records.xsl'?>
		///		<mergedata recordid="" catid="" docid="" formname="" docresultflag="">
		///			<header>
		///				<column></column>
		///			</header>
		///			<record selected="" rowid="">
		///				<field name=""></field>
		///			</record>
		///		</mergedata>
		///</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool FetchData(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			MergeManager objMergeManager=null;
			XmlNode objNode=null;
            XmlElement e = null;
            DataModelFactory objDataModelFactory = null;//mudabbir added 36022
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
				objMergeManager=new MergeManager(userLogin , securityConnectionString , userID.ToString(),base.ClientId);//dvatsa-cloud
				
				objMergeManager.DSN=connectionString;
				objMergeManager.UID= userID.ToString();

                if (m_sDocPath != "")
                {
                    objMergeManager.TemplatePath = m_sDocPath;
                }
				objMergeManager.DocStorageDSN=m_sDocPath;
				objMergeManager.DocPathType=m_iDocPathType;
				objMergeManager.SecurityDSN=securityConnectionString;
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/TemplateId");
				if (objNode.InnerText!=null)
				{
					objMergeManager.MoveTo(Conversion.ConvertStrToLong(objNode.InnerText));
				}
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/RecordId");
				if (objNode.InnerText!=null)
				{
					objMergeManager.CurMerge.RecordId=Conversion.ConvertStrToLong(objNode.InnerText);
                    // akaushik5 Added for RMA 5115 Starts
                    objMergeManager.CurMerge.IsAdjusterSelected = true;
                    // akaushik5 Added for RMA 5115 Ends
                    objMergeManager.CurMerge.FetchData();
					p_objXmlOut.LoadXml(objMergeManager.CurMerge.GenerateXML());
				}
                //Mudabbir added 36022
                objDataModelFactory = new DataModelFactory(m_userLogin, ClientId);
                e = p_objXmlOut.CreateElement("UseSilverlight");
                e.InnerText = objDataModelFactory.Context.InternalSettings.SysSettings.UseSilverlight.ToString().ToLower();
                p_objXmlOut.FirstChild.AppendChild(e);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.FetchData.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objMergeManager=null;
                //mudabbir disposed : 36022
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();
			}

		}
		// Moved This Function to MergeManager Class for autoMailmerge Set Up
//        private void AttachDocument(string p_sFileName,MemoryStream objMemory, string p_sCatid,string p_sSubject,
//                                    string p_sName,string p_sNotes,string p_sKeywords,string p_sClass,string p_sCategory,
//                                    string p_sType,string p_sRecordId ,int iPsid, string p_sFormName)
//        {
//            XmlDocument objDOM=null;
//            DocumentManager objDocumentManager=null;
//            XmlElement objElemParent=null;
//            XmlElement objElemChild=null;
//            XmlElement objElemTemp=null;
//            long lDocumentId=0;
//            SysSettings objSettings = null;
//            Acrosoft objAcrosoft = null;
//            BinaryReader binaryRdr = null;
//            Byte[] fileBuffer = null;
//            MediaView objMediaView = null;
//            int iRetCd = 0;
//            string sAppExcpXml = "";
//            objAcrosoft = new Acrosoft();
//            Claim objClaim = null;
//            Event objEvent = null;
//            DataModelFactory objDataModelFactory = null;
//            long lRecordId = 0;
//            Policy objPolicy = null;
//            // Start Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan
//            PolicyEnh objPolicyEnh = null;
//            // End Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan
//            ////Mona:PaperVisionMerge : Animesh
//            PaperVisionDocumentManager objPVDocManager;
//            PVWrapper objPaperVision;

//            // atavaragiri : MITS 25696 //
//            string sTemp = String.Empty;
//            XmlDocument objXmlDoc = new XmlDocument();
//            XmlNode objNode = null;
//            XmlNodeList objNodeList = null;
//            bool bEquals = false;
//            string ssubAppExcpXml = String.Empty;
//            string sFolderId = String.Empty;
//            int iTemp = 0;
//            //end:MITS 25696
                            

//            //Animesh Insertion Ends
//            try
//            {
//                objDocumentManager = new DocumentManager(m_userLogin);
//                objDocumentManager.ConnectionString = connectionString;
//                objDocumentManager.SecurityConnectionString = securityConnectionString; //Amandeep MITS 28800
//                objDocumentManager.UserLoginName = userLogin.LoginName;
//                //mgaba2:MITS 11704-04/07/2008-if doc path entered at user level-Start
//                /*if (userLogin.objRiskmasterDatabase.DocPathType == 0)
//                {
//                    objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
//                }
//                else
//                {
//                    objDocumentManager.DocumentStorageType = StorageType.DatabaseStorage;
//                }*/

                

//                if (this.userLogin.DocumentPath.Length > 0)
//                {
//                    objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
//                }
//                else
//                {
//                    if (userLogin.objRiskmasterDatabase.DocPathType == 0)
//                    {
//                        objDocumentManager.DocumentStorageType = StorageType.FileSystemStorage;
//                    }
//                    else
//                    {
//                        objDocumentManager.DocumentStorageType = StorageType.DatabaseStorage;
//                    }
//                }
//                //mgaba2:MITS 11704-04/07/2008-End
//                //mgaba2:MITS 11704-03/11/2008
//                //Document path of the DSN is overwritten by the User's Doc path(if exists)
//                //objDocumentManager.DestinationStoragePath = userLogin.objRiskmasterDatabase.GlobalDocPath;
//                if (this.userLogin.DocumentPath.Length > 0)
//                {   
//                    objDocumentManager.DestinationStoragePath = userLogin.DocumentPath;
//                }
//                else
//                {                    
//                    objDocumentManager.DestinationStoragePath = userLogin.objRiskmasterDatabase.GlobalDocPath;
//                }

//                objDOM = new XmlDocument();
//                objElemParent = objDOM.CreateElement("data");
//                objDOM.AppendChild(objElemParent);

//                objElemChild = objDOM.CreateElement("Document");

//                objElemTemp = objDOM.CreateElement("DocumentId");
//                objElemTemp.InnerText = "0";
//                objElemChild.AppendChild(objElemTemp);

//                objElemTemp = objDOM.CreateElement("FolderId");
//                objElemTemp.InnerText = "0";
//                objElemChild.AppendChild(objElemTemp);

//                objElemTemp = objDOM.CreateElement("CreateDate");
////Mona:PaperVisionMerge : Animesh
//                //objElemTemp.InnerText = Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now);
//                objElemTemp.InnerText = DateTime.Now.ToString();
//                objElemChild.AppendChild(objElemTemp);

//                objElemTemp = objDOM.CreateElement("Category");
//                objElemTemp.InnerText = p_sCategory;
//                objElemChild.AppendChild(objElemTemp);

//                objElemTemp = objDOM.CreateElement("Name");
//                objElemTemp.InnerText = p_sName;
//                objElemChild.AppendChild(objElemTemp);

//                objElemTemp = objDOM.CreateElement("Class");
//                objElemTemp.InnerText = p_sClass;
//                objElemChild.AppendChild(objElemTemp);

//                objElemTemp = objDOM.CreateElement("Subject");
//                objElemTemp.InnerText = p_sSubject;
//                objElemChild.AppendChild(objElemTemp);

//                objElemTemp = objDOM.CreateElement("Type");
//                objElemTemp.InnerText = p_sType;
//                objElemChild.AppendChild(objElemTemp);

//                objElemTemp = objDOM.CreateElement("Notes");
//                objElemTemp.InnerText = p_sNotes;
//                objElemChild.AppendChild(objElemTemp);

//                objElemTemp = objDOM.CreateElement("UserLoginName");
//                objElemTemp.InnerText = userLogin.LoginName;
//                objElemChild.AppendChild(objElemTemp);

//                objElemTemp = objDOM.CreateElement("FileName");
//                objElemTemp.InnerText = p_sFileName;
//                objElemChild.AppendChild(objElemTemp);

//                objElemTemp = objDOM.CreateElement("FilePath");
//                objElemTemp.InnerText = "";
//                objElemChild.AppendChild(objElemTemp);

//                objElemTemp = objDOM.CreateElement("Keywords");
//                objElemTemp.InnerText = p_sKeywords;
//                objElemChild.AppendChild(objElemTemp);

//                objElemTemp = objDOM.CreateElement("AttachTable");
//                //Shruti for 7918
//                if (Conversion.ConvertStrToInteger(p_sCatid) == 20)
//                {
//                    objElemTemp.InnerText = p_sFormName.ToUpper();
//                }
//                else
//                {
//                    objElemTemp.InnerText = CatIdToRMTableName(p_sCatid);
//                }
//                objElemChild.AppendChild(objElemTemp);

//                objElemTemp = objDOM.CreateElement("Subject");
//                objElemTemp.InnerText = p_sSubject;
//                objElemChild.AppendChild(objElemTemp);

//                objElemTemp = objDOM.CreateElement("AttachRecordId");
//                objElemTemp.InnerText = p_sRecordId;
//                objElemChild.AppendChild(objElemTemp);

//                //Nikhil Garg		Defect No: 2397		Dated: 03/10/2006
//                objElemTemp = objDOM.CreateElement("FormName");
//                objElemTemp.InnerText = p_sFormName;
//                objElemChild.AppendChild(objElemTemp);

//                objDOM.FirstChild.AppendChild(objElemChild);

//                //08/10/2006.. Raman Bhatia : If Acrosoft is enabled then legacy document management is not used
//                //Instead store attachment using Acrosoft

//                objSettings = new SysSettings(connectionString);
//                ////Mona:PaperVisionMerge : Animesh Inserted MITS 16697
//                if (objSettings.UsePaperVisionInterface == true)
//                {
//                    string sTableName = CatIdToRMTableName(p_sCatid);
//                    if (sTableName.ToLower() == "claim")
//                    {
//                        objPaperVision = new PVWrapper();  
//                        objPaperVision.PVEntityID = PaperVisionSection.EntityID;
//                        objPaperVision.PVLoginUrl = PaperVisionSection.LoginUrl;
//                        objPaperVision.PVProjID = PaperVisionSection.ProjectID;
//                        objPaperVision.PVServerAddress = PaperVisionSection.PVServerAddress;
//                        objPaperVision.PVShowDocLinkUrl = PaperVisionSection.DocumentLink;   
//                        //Animesh Inserted MITS 18345
//                        objPaperVision.SharedNetworkLocationPath = PaperVisionSection.SharedNetworkLocationPath;
//                        //objPaperVision.MappedDriveName = PaperVisionSection.MappedDriveName;
//                        objPaperVision.DomainName = PaperVisionSection.DomainName;
//                        objPaperVision.UserID = PaperVisionSection.UserID;
//                        objPaperVision.Password = PaperVisionSection.Password;   
//                        //Animesh Insertion ends
//                        objPVDocManager = new PaperVisionDocumentManager(m_userLogin);
//                        objPVDocManager.ConnectionString = this.connectionString;
//                        objPVDocManager.UserLoginName = this.loginName;
//                        objPVDocManager.AddDocument(objDOM.InnerXml,objMemory, iPsid, objPaperVision, out lDocumentId);
//                    }
//                    else
//                    {
//                        throw new RMAppException("PaperVision Document System is active. Attachments functionality will only be available for claims.");
//                    }

//                }
//                //Animesh Insertion Ends
//                else if (objSettings.UseAcrosoftInterface == false && objSettings.UseMediaViewInterface == false)
//                {
//                    objDocumentManager.AddDocument(objDOM.InnerXml, objMemory, iPsid, out lDocumentId);
//                } // if
//                else if(objSettings.UseAcrosoftInterface)
//                {
//                    binaryRdr = new BinaryReader(objMemory);
//                    fileBuffer = binaryRdr.ReadBytes(Convert.ToInt32(objMemory.Length));
//                    binaryRdr.Close();
                    
//                    string sTableName = CatIdToRMTableName(p_sCatid);
//                    string sPath = RMConfigurator.UserDataPath;
//                    string sFullFileName = sPath + @"\MailMerge\" + p_sFileName;
//                    //rsolanki2 :  start updates for MCM mits 19200 
//                    Boolean bSuccess = false;
//                    string sTempAcrosoftUserId = base.userLogin.LoginName;
//                    string sTempAcrosoftPassword = base.userLogin.Password;

//                    if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
//                    {
//                        if (AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName] != null
//                            && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].RmxUser))
//                        {
//                            sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].AcrosoftUserId;
//                            sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[base.userLogin.LoginName].AcrosoftPassword;
//                        }
//                        else
//                        {
//                            sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
//                            sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
//                        }
//                    }
//                    switch (sTableName.ToLower())
//                    {
//                        case "claim": objDataModelFactory = new DataModelFactory(m_userLogin);
//                            objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
//                            objClaim.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
//                            string sClaimNumber = objClaim.ClaimNumber;
//                            string sEventNumber = objClaim.EventNumber;
							
//                             //creating folder

							

//                                objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
//                                    AcrosoftSection.AcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber,
//                                    AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);

//                                // start: atavaragiri :MITS 25696:added condition to check for sub foldername from config file//
//                                // if empty,the attachment will get created for root folder //

//                                if (String.IsNullOrEmpty(AcrosoftSection.AcrosoftSubFolderName))
//                                {

//                                iRetCd = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
//                                    p_sFileName, fileBuffer, p_sName, p_sType, sEventNumber, sClaimNumber,
//                                    AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", sTempAcrosoftUserId, out sAppExcpXml);
//                                 }

//                                //creation of a sub folder at Acrosoft for mail merge attachments:MITS 25696//
//                              else
//                             {

//                                objAcrosoft.GetAttachmentFolderDocumentList(sTempAcrosoftUserId, sTempAcrosoftPassword,
//                                     sEventNumber, sClaimNumber, AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml, out sTemp); //function used here to retrieve the folder id of the parent folder

//                                objXmlDoc.LoadXml(sAppExcpXml);
								
//                                objNode = objXmlDoc.SelectSingleNode(@"/FolderDocuments/Folder/FolderId");//extract folder id of parent
//                                objNodeList = objXmlDoc.SelectNodes("//Title"); 
//                                sFolderId = objNode.InnerText;

//                                while (iTemp < objNodeList.Count)  
//                                {
//                                    bEquals = String.Equals(objNodeList[iTemp].InnerText, AcrosoftSection.AcrosoftSubFolderName);
//                                    if (bEquals == true)
//                                        break;
//                                    iTemp++;
//                                }
//                                if (bEquals == false)
//                                    // sub folder creation//
//                                    objAcrosoft.CreateSubFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, sFolderId, AcrosoftSection.AcrosoftSubFolderName, out ssubAppExcpXml);

//                                    // storing document in sub folder created //
//                                    iRetCd = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
//                                        p_sFileName, fileBuffer, p_sName, p_sType, AcrosoftSection.AcrosoftAttachmentsTypeKey, sFolderId, AcrosoftSection.AcrosoftSubFolderName, sTempAcrosoftUserId, out sAppExcpXml);
								

//                            }

//                            // end :MITS 25696 //
														
//                            //storing document
//                            //01/07/2007 Raman Bhatia: Document needs to be picked from memorystream
//                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, sFullFileName, p_sSubject, p_sType, sEventNumber, sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", m_userLogin.LoginName, out sAppExcpXml);
//                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, p_sFileName, fileBuffer, p_sSubject, p_sType, sEventNumber, sClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", m_userLogin.LoginName, out sAppExcpXml);
//                            //gagnihotri MITS 17673 Document should be saved with the given title.



													
//                            objClaim = null;
//                            objAcrosoft = null;


//                            break;
//                        case "event": objDataModelFactory = new DataModelFactory(m_userLogin);
//                            objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
//                            objEvent.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
//                            sEventNumber = objEvent.EventNumber;
							
//                            //creating folder
//                            objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
//                                AcrosoftSection.AcrosoftAttachmentsTypeKey, sEventNumber, "0", AcrosoftSection.EventFolderFriendlyName, 
//                                AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
//                            //storing document		
//                            //01/07/2007 Raman Bhatia: Document needs to be picked from memorystream
//                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, sFullFileName, p_sSubject, p_sType, sEventNumber, "0", AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", userLogin.LoginName, out sAppExcpXml);
//                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, p_sFileName, fileBuffer, p_sSubject, p_sType, sEventNumber, "0", AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", m_userLogin.LoginName, out sAppExcpXml);
//                            //gagnihotri MITS 17673 Document should be saved with the given title.
                           
//                            // atavaragiri :MITS 25696//
//                            if (String.IsNullOrEmpty(AcrosoftSection.AcrosoftSubFolderName))
//                                {

//                                    iRetCd = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword, p_sFileName,
//                                   fileBuffer, p_sName, p_sType, sEventNumber, "0", AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "",
//                                   sTempAcrosoftUserId, out sAppExcpXml);
//                                 }

//                                //creation of a sub folder at Acrosoft for mail merge attachments:MITS 25696//
//                              else
//                             {

//                                objAcrosoft.GetAttachmentFolderDocumentList(sTempAcrosoftUserId, sTempAcrosoftPassword,
//                                     sEventNumber, "", AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml, out sTemp); //function used here to retrieve the folder id of the parent folder

//                                objXmlDoc.LoadXml(sAppExcpXml);
								
//                                objNode = objXmlDoc.SelectSingleNode(@"/FolderDocuments/Folder/FolderId");//extract folder id of parent
//                                objNodeList = objXmlDoc.SelectNodes("//Title"); 
//                                sFolderId = objNode.InnerText;

//                                while (iTemp < objNodeList.Count)  
//                                {
//                                    bEquals = String.Equals(objNodeList[iTemp].InnerText, AcrosoftSection.AcrosoftSubFolderName);
//                                    if (bEquals == true)
//                                        break;
//                                    iTemp++;
//                                }
//                                if (bEquals == false)
//                                    // sub folder creation//
//                                    objAcrosoft.CreateSubFolder(sTempAcrosoftUserId, sTempAcrosoftPassword, sFolderId, AcrosoftSection.AcrosoftSubFolderName, out ssubAppExcpXml);

//                                    // storing document in sub folder created //
//                                    iRetCd = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
//                                        p_sFileName, fileBuffer, p_sName, p_sType, AcrosoftSection.AcrosoftAttachmentsTypeKey, sFolderId, AcrosoftSection.AcrosoftSubFolderName, sTempAcrosoftUserId, out sAppExcpXml);
								

//                            }

//                            // end :MITS 25696 //



//                            objEvent = null;
//                            objAcrosoft = null;
//                            objDataModelFactory = null;
//                            break;
//                        case "policy": objDataModelFactory = new DataModelFactory(m_userLogin);
//                            objPolicy = (Policy)objDataModelFactory.GetDataModelObject("Policy", false);
//                            objPolicy.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
//                            //creating folder
//                            //gagnihotri R5 MCM changes
//                            //objAcrosoft.CreatePolicyFolder(base.userLogin.LoginName, base.userLogin.Password, objConfig.GetValue("AcrosoftPolicyTypeKey"), objPolicy.PolicyName, objConfig.GetValue("PolicyFolderFriendlyName"), out sAppExcpXml);
//                            objAcrosoft.CreatePolicyFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
//                                AcrosoftSection.AcrosoftPolicyTypeKey, objPolicy.PolicyName, AcrosoftSection.PolicyFolderFriendlyName, out sAppExcpXml);
//                            //storing document		   
//                            //01/07/2007 Raman Bhatia: Document needs to be picked from memorystream
//                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, sFullFileName, p_sSubject, p_sType, objPolicy.PolicyName, objConfig.GetValue("AcrosoftPolicyTypeKey"), "", "", userLogin.LoginName, out sAppExcpXml);
//                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, p_sFileName, fileBuffer, p_sSubject, p_sType, objPolicy.PolicyName, AcrosoftSection.AcrosoftPolicyTypeKey, "", "", m_userLogin.LoginName, out sAppExcpXml);
//                            //gagnihotri MITS 17673 Document should be saved with the given title.

							
//                                iRetCd = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword, p_sFileName,
//                                                              fileBuffer, p_sName, p_sType, objPolicy.PolicyName, AcrosoftSection.AcrosoftPolicyTypeKey,
//                                                              "", "", sTempAcrosoftUserId, out sAppExcpXml);
											

//                                break;
//                        // Start Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan
//                        case "policyenh": objDataModelFactory = new DataModelFactory(m_userLogin);
//                            objPolicyEnh = (PolicyEnh)objDataModelFactory.GetDataModelObject("PolicyEnh", false);
//                            objPolicyEnh.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
//                            //creating folder
//                            //gagnihotri R5 MCM changes
//                            //objAcrosoft.CreatePolicyFolder(base.userLogin.LoginName, base.userLogin.Password, objConfig.GetValue("AcrosoftPolicyTypeKey"), objPolicy.PolicyName, objConfig.GetValue("PolicyFolderFriendlyName"), out sAppExcpXml);
//                            objAcrosoft.CreatePolicyFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
//                                AcrosoftSection.AcrosoftPolicyTypeKey, objPolicyEnh.PolicyName, AcrosoftSection.PolicyFolderFriendlyName, out sAppExcpXml);
//                            //atavaragiri :MITS 28922 :incorrect variable was being passed as a parameter for policy name//

//                            //storing document		   
//                            //01/07/2007 Raman Bhatia: Document needs to be picked from memorystream
//                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, sFullFileName, p_sSubject, p_sType, objPolicy.PolicyName, objConfig.GetValue("AcrosoftPolicyTypeKey"), "", "", userLogin.LoginName, out sAppExcpXml);
//                            //gagnihotri R5 MCM changes
//                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, p_sFileName, fileBuffer, p_sSubject, p_sType, objPolicy.PolicyName, objConfig.GetValue("AcrosoftPolicyTypeKey"), "", "", m_userLogin.LoginName, out sAppExcpXml);
//                            //iRetCd = objAcrosoft.StoreObjectBuffer(loginName, userLogin.Password, p_sFileName, fileBuffer, p_sSubject, p_sType, objPolicy.PolicyName, AcrosoftSection.AcrosoftPolicyTypeKey, "", "", m_userLogin.LoginName, out sAppExcpXml);
//                            //gagnihotri MITS 17673 Document should be saved with the given title.
//                            iRetCd = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
//                                p_sFileName, fileBuffer, p_sName, p_sType, objPolicyEnh.PolicyName,
//                                AcrosoftSection.AcrosoftPolicyTypeKey, "", "", sTempAcrosoftUserId, out sAppExcpXml);
//                            //atavaragiri :MITS 28922 :incorrect variable was being passed as a parameter for policy name//
//                            break;
//                        // End Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan  
//                        default:

//                            break;
//                    } // switch
//                    //rsolanki2 :  end updates for MCM mits 19200 
//                    //objDataModelFactory.UnInitialize();
//                } // else
//                else if (objSettings.UseMediaViewInterface)
//                {
//                    objMediaView = new MediaView(); 
//                    binaryRdr = new BinaryReader(objMemory);
//                    fileBuffer = binaryRdr.ReadBytes(Convert.ToInt32(objMemory.Length));
//                    binaryRdr.Close();
//                    string sRecordNumber = "";
//                    string sRecordType = "";
//                    string sTableName = CatIdToRMTableName(p_sCatid);
//                    string sPath = RMConfigurator.UserDataPath;
//                    string sFullFileName = sPath + @"\MailMerge\" + p_sFileName;

//                    WriteBinaryFile(sFullFileName, fileBuffer);
                    
                    
//                    switch (sTableName.ToLower())
//                    {
//                        case "claim": objDataModelFactory = new DataModelFactory(m_userLogin);
//                            objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
//                            objClaim.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
//                            sRecordNumber = objClaim.ClaimNumber;
//                            sRecordType = "Claim";
//                            break;

//                        case "event": objDataModelFactory = new DataModelFactory(m_userLogin);
//                            objEvent = (Event)objDataModelFactory.GetDataModelObject("Event", false);
//                            objEvent.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
//                            sRecordNumber = objEvent.EventNumber;
//                            sRecordType = "Event";
//                            break;

//                        case "policy": objDataModelFactory = new DataModelFactory(m_userLogin);
//                            objPolicy = (Policy)objDataModelFactory.GetDataModelObject("Policy", false);
//                            objPolicy.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
//                            sRecordNumber = objPolicy.PolicyName;
//                            sRecordType = "Policy";
//                            break;
//                        // Start Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan
//                        case "policyenh": objDataModelFactory = new DataModelFactory(m_userLogin);
//                            objPolicyEnh = (PolicyEnh)objDataModelFactory.GetDataModelObject("PolicyEnh", false);
//                            objPolicyEnh.MoveTo(Conversion.ConvertStrToInteger(p_sRecordId));
//                            sRecordNumber = objPolicyEnh.PolicyName;
//                            sRecordType = "Policy";
//                            break;
//                        // End Naresh Fixed with the MITS 9727 as discussed with Raman and Pawan  
//                        default:

//                            break;
//                    }

//                    importList oList = new importList(sRecordNumber, sRecordType, p_sName);
//                    oList.MMNotes = p_sNotes;
//                    oList.MMDescription = p_sSubject;
//                    objMediaView.MMUploadDocument(p_sFileName, sPath + @"\MailMerge", oList);
                    
//                } // else
//                }
            
//            finally
//            {
//                if (objDataModelFactory != null)
//                {
//                    objDataModelFactory.Dispose();
//                }
//                objDOM = null;
//                objDocumentManager = null;
//                objElemParent = null;
//                objElemChild = null;
//                objElemTemp = null;
//                objSettings = null;
//                if (objClaim != null)
//                {
//                    objClaim.Dispose();
//                }
//                if (objEvent != null)
//                {
//                    objEvent.Dispose();
//                }
//                if (objPolicy != null)
//                {
//                    objPolicy.Dispose();
//                }
//                if (objPolicyEnh != null)
//                {
//                    objPolicyEnh.Dispose();
//                }
//                objPaperVision = null;//Mona:PaperVisionMerge : Animesh
//                objPVDocManager = null; 
//            }
//        }

        /// <summary>
        /// This mis-named function attaches any document, not just an rtf.  It
        /// assumes that the content of p_objXmlIn.SelectSingleNode(@"/Template/FileContent").InnerText
        /// is a base64 string representing a binary file.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public  bool AttachRTF(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			XmlNode objNode=null;
			MemoryStream objMemory = null;
			string sCatid="";
			string sSubject="";
			string sName="";
			string sNotes="";
			string sKeywords = string.Empty;
			string sClass="";
			string sCategory="";
			string sType="";
			string sRecordId="";
			string sFileName = "";
			string sFormName="";
			int iPsid = 0; 
			try
			{
                //mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
				
				//Get file name
				sFileName = p_objXmlIn.SelectSingleNode(@"/Template/NewFileName").InnerText;
				int iPosition = sFileName.LastIndexOf(@"\");
				if(iPosition != -1)
				{
					sFileName = sFileName.Substring(iPosition + 1);
				}
				
                objMemory = new MemoryStream(Convert.FromBase64String(p_objXmlIn.SelectSingleNode(@"/Template/FileContent").InnerText));
			
				//Retrieve psid from request
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/psid");
				if (objNode.InnerText!=null)
				{
					iPsid=Conversion.ConvertStrToInteger(objNode.InnerText);
				}

				sCatid=p_objXmlIn.SelectSingleNode(@"/Template/CatId").InnerText;
				sSubject=p_objXmlIn.SelectSingleNode(@"/Template/Subject").InnerText;
				sNotes=p_objXmlIn.SelectSingleNode(@"/Template/Notes").InnerText;
				sClass=p_objXmlIn.SelectSingleNode(@"/Template/Class").InnerText;
				sCategory=p_objXmlIn.SelectSingleNode(@"/Template/Category").InnerText;
				sType=p_objXmlIn.SelectSingleNode(@"/Template/Type").InnerText;
				sRecordId=p_objXmlIn.SelectSingleNode(@"/Template/RecordId").InnerText;
				sName=p_objXmlIn.SelectSingleNode(@"/Template/Title").InnerText;
				sFormName=p_objXmlIn.SelectSingleNode(@"/Template/FormName").InnerText;
				sKeywords = p_objXmlIn.SelectSingleNode(@"/Template/Keywords").InnerText;
                // Changed By Nitika for Auto Miaol merge Set Up functionality - Moved this Attach Document Function in class Mergemanager Class of application//
                MergeManager objMergeManager = new MergeManager(userLogin, securityConnectionString, userID.ToString(),base.ClientId);//dvatsa-cloud
                objMergeManager.AttachDocument(sFileName, objMemory, sCatid, sSubject, sName, sNotes, sKeywords, sClass, sCategory, sType, sRecordId, iPsid, sFormName);
                // End By Nitika

				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GenerateRTF.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				//objMergeManager=null;
				//sbRowid=null;
			}
		}


        /// <summary>
        /// This mis-named function pulls both the word template and datasource into base64 strings 
        /// and returns to client for client-side word merge.
        /// </summary>
        /// <param name="p_objXmlIn">Input parameters as xml
        /// The structure of input XML document would be:
        /// 	<Document>
        ///			<Template>
        ///				<TemplateId>TemplateId</TemplateId>
        ///				<Name>Name of the template</Name>
        ///				<Attach>1 if attached, 0 otherwise</Attach>
        ///				<Subject></Subject>
        ///				<Notes></Notes>
        ///				<Class></Class>
        ///				<Category></Category>
        ///				<Type></Type>
        ///				<Class></Class>
        ///				<Category></Category>
        ///				<Comments></Comments>
        ///				<RecordId>RecordId</RecordId>
        ///				<RowIds>
        ///				  <RowId>RowId</RowId>
        ///				</RowIds>
        ///			</Template>
        ///		</Document>
        /// </param>
        /// <param name="p_objXmlOut">Result as Output xml</param>
        /// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
        /// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GenerateRTF(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			MergeManager objMergeManager=null;
			XmlNode objNode=null;
			string sFile="";			
			StringBuilder sbRowid=null;
			string sAttachDocument="";			
			string sDirectDisplay="";
            DataModelFactory objDataModelFactory = null;
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
				objMergeManager=new MergeManager(userLogin , securityConnectionString , userID.ToString(),base.ClientId);//dvatsa-cloud
				
				objMergeManager.DSN=connectionString;
				objMergeManager.UID= userID.ToString();

                if (m_sDocPath != "")
                {
                    objMergeManager.TemplatePath = m_sDocPath;
                }
				//objMergeManager.TemplatePath=@"C:\FileStore\";
				objMergeManager.DocStorageDSN=m_sDocPath;
				objMergeManager.DocPathType=m_iDocPathType;
				objMergeManager.SecurityDSN=securityConnectionString;
				sAttachDocument=p_objXmlIn.SelectSingleNode(@"/Template/Attach").InnerText;	
				sDirectDisplay = p_objXmlIn.SelectSingleNode(@"/Template/DirectDisplay").InnerText;	
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/TemplateId");
				if (objNode.InnerText!=null)
				{
					objMergeManager.MoveTo(Conversion.ConvertStrToLong(objNode.InnerText));
				}
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/RecordId");
				if (objNode.InnerText!=null)
				{
					objMergeManager.CurMerge.RecordId=Conversion.ConvertStrToLong(objNode.InnerText);
				}
				// akaushik5 Commnetd for RMA - 5115 Starts
                // objMergeManager.CurMerge.FetchData();
                // akaushik5 Commnetd for RMA - 5115 Ends

				objNode=p_objXmlIn.SelectSingleNode(@"/Template/RowIds");
				sbRowid=new StringBuilder();
				foreach (XmlNode objTemp in objNode.SelectNodes("//RowIds/RowId"))
				{
					sbRowid.Append(objTemp.InnerText);
					sbRowid.Append(",");
				}
				if (sbRowid.ToString().Length > 0)
				{
					sbRowid.Remove(sbRowid.Length-1,1);
                    // akaushik5 Added for RMA - 5115 Starts
                    objMergeManager.CurMerge.IsAdjusterSelected = true;
                    // akaushik5 Added for RMA - 5115 Ends
                }
				if( sbRowid.Length == 0 )
					sbRowid.Append("1");

                // akaushik5 Added for RMA - 5115 Starts
                objMergeManager.CurMerge.FetchData();
                // akaushik5 Added for RMA - 5115 Ends

				objNode=p_objXmlIn.SelectSingleNode(@"/Template/FileName");
				if ( objNode!=null)
				{
					if( objNode.InnerText != "" )
					{
					 objMergeManager.CurMerge.Template.FormFileName=objNode.InnerText;
					}
				}

				objNode=p_objXmlIn.SelectSingleNode(@"/Template/IsPreFab");
				if( objNode!=null && objNode.InnerText != "" )
				{
					objMergeManager.CurMerge.PreFab = true;
					objMergeManager.CurMerge.Template.Prefab=objMergeManager.CurMerge.Template.FormFileName;
				}

                //get the template
                objNode = p_objXmlOut.CreateElement("Template");
                p_objXmlOut.AppendChild(objNode);
                XmlElement e = p_objXmlOut.CreateElement("File");
                e.SetAttribute("Filename", Generic.GetServerUniqueFileName("mmt", "doc", base.ClientId));
                e.InnerText = objMergeManager.CurMerge.Template.FormFileContent;
                objNode.AppendChild(e);

                //get the datasource if user is doing a merge (PreFab means user is creating a merge template)
                //when user is creating template the merge fields are passed by wizard in a hidden field
                if (!objMergeManager.CurMerge.PreFab)
                {
                    e = p_objXmlOut.CreateElement("DataSource");
                    e.InnerText = objMergeManager.CurMerge.GetMergeDataSource( sbRowid.ToString() );
                    objNode.AppendChild(e);
                }

                //get the datasource if user is doing a merge (PreFab means user is creating a merge template)
                //when user is creating template the merge fields are passed by wizard in a hidden field
                if (!objMergeManager.CurMerge.PreFab)
                {
                    e = p_objXmlOut.CreateElement("HeaderContent");
                    e.InnerText = objMergeManager.CurMerge.Template.HeaderFileContent;
                    objNode.AppendChild(e);
                }

                //get number of records
                if (!objMergeManager.CurMerge.PreFab)
                {
                    string[] arrRowid = (sbRowid.ToString()).Split(',');
                    int iNumRecords = arrRowid.Length;
                    e = p_objXmlOut.CreateElement("NumRecords");
                    e.InnerText = iNumRecords.ToString();
                    objNode.AppendChild(e);
                }
                //mudabbir added 36022 start: retrieving silverlight settings from general system params
               
                bool buseSilverlight = false;
                objDataModelFactory = new DataModelFactory(m_userLogin, ClientId);
                buseSilverlight = objDataModelFactory.Context.InternalSettings.SysSettings.UseSilverlight;
                e = p_objXmlOut.CreateElement("UseSilverlight");
                e.InnerText = buseSilverlight.ToString().ToLower();
                p_objXmlOut.FirstChild.AppendChild(e);
                //mudabbir ends
				
                // 33465 - nkaranam2 - To Get Format of the Template stored in server
                if (!objMergeManager.CurMerge.PreFab)
                {
                    e = p_objXmlOut.CreateElement("TemplateFormat");
                    if (objMergeManager.CurMerge.Template.FormFileName.EndsWith(".doc"))
                        e.InnerText = objMergeManager.CurMerge.Template.FormFileName.Substring(objMergeManager.CurMerge.Template.FormFileName.Length - 3, 3);
                    else if (objMergeManager.CurMerge.Template.FormFileName.EndsWith(".docx"))
                        e.InnerText = objMergeManager.CurMerge.Template.FormFileName.Substring(objMergeManager.CurMerge.Template.FormFileName.Length - 4, 4);
                    objNode.AppendChild(e);
                }

				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GenerateRTF.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objMergeManager=null;
				sbRowid=null;
                //mudabbir disposed : 36022
                if (objDataModelFactory != null)
                {
                    objDataModelFactory.Dispose();  
                }
			}

		}



		/// <summary>
		/// Returns the File contents as string
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<TemplateId>TemplateId</TemplateId>
		///			</Template>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<Template><TemplateContent></TemplateContent></Template>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetTemplateContent(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			XmlNode objNode=null;
			
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
				objTemplateManager=new TemplateManager( m_userLogin,base.ClientId );
				
				objTemplateManager.DSN=connectionString;
				objTemplateManager.UID= userID.ToString();
				
				objTemplateManager.TemplatePath=m_sDocPath;
				objTemplateManager.DocStorageDSN=m_sDocPath;
				objTemplateManager.DocPathType=m_iDocPathType;
				objTemplateManager.SecurityDSN=securityConnectionString;
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/TemplateId");
				if (objNode.InnerText!=null)
				{
					objTemplateManager.MoveTo(objNode.InnerText);
				}
				objNode=p_objXmlOut.CreateElement("Template");
				p_objXmlOut.AppendChild(objNode);
				objNode=p_objXmlOut.CreateElement("TemplateContent");
				objNode.InnerText=objTemplateManager.CurTemplate.FormFileContent;
				p_objXmlOut.FirstChild.AppendChild(objNode);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetTemplateContent.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objTemplateManager=null;
			}
		}
		
		/// <summary>
		/// Returns the File contents as memory stream
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<TemplateId>TemplateId</TemplateId>
		///			</Template>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<Template><TemplateContent Filename=""></TemplateContent></Template>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetTemplateContentInMemory(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			XmlNode objNode=null;
			XmlElement objTempElement=null;
			
			MemoryStream objMemory=null;
			try
			{
                //mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objTemplateManager.DSN=connectionString;
				objTemplateManager.UID= userID.ToString();
				objTemplateManager.TemplatePath=m_sDocPath;
				objTemplateManager.DocStorageDSN=m_sDocPath;
				objTemplateManager.DocPathType=m_iDocPathType;
				objTemplateManager.SecurityDSN=securityConnectionString;
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/TemplateId");
				if (objNode.InnerText!=null)
				{
					objTemplateManager.MoveTo(objNode.InnerText);
				}
				objNode=p_objXmlOut.CreateElement("Template");
				p_objXmlOut.AppendChild(objNode);
                objTempElement=p_objXmlOut.CreateElement("TemplateContent");
                objTempElement.SetAttribute("Filename", Generic.GetServerUniqueFileName("mmt", "doc", base.ClientId));
                objTempElement.InnerText = objTemplateManager.CurTemplate.FormFileContent;
				p_objXmlOut.FirstChild.AppendChild(objTempElement);
				objTempElement=p_objXmlOut.CreateElement("TemplateHeaderContent");
				objTemplateManager.CurTemplate.GetHeaderFile(out objMemory);
				objTempElement.InnerText=Convert.ToBase64String(objMemory.ToArray());
				p_objXmlOut.FirstChild.AppendChild(objTempElement);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetTemplateContent.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objTemplateManager=null;
				objTempElement=null;
                if (objMemory != null)
                {
                    objMemory.Dispose();
                }
			}
		}
		/// <summary>
		/// This function returns the list of available templates
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<TableName>TableName</TableName>
		///				<CatId>Caterory Id</CatId>
		///			</Template>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<AvailableTemplates><Templates><Template Category="" TemplateDesc="" TemplateId=""/></Templates></AvailableTemplates>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetAvailTemplates(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			MergeManager objMergeManager=null;
			TemplateManager objTemplateManager=null;
			XmlNode objNode=null;
            XmlDocument ObjXmlDomMAilRecipientList = null;            
            XmlNode objNodeMailRecipientList = null; //spahariya MITS 28867
			string sTableName="";
			string sFormName="";
			string sShortCodeSelected="";
			long lMergeDocumentFormat=0;
			long lMergeDocumentType=0;
			long lStateId=0;
			long lLOB=0;
            bool bAllStatesSelected=false;
			int iRecordId=0;
			long lCatId=0;
            //Mona:PaperVisionMerge : Animesh Inserted 
            SysSettings objSettings = null;
            XmlElement objPVElement = null;
            PVWrapper objPaperVision = null;
            bool blnPaperVisionValidation = false;
            //Animesh Insertion Ends

            DataModelFactory objDataModelFactory = null;// mudabbir 36022

			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
				objMergeManager=new MergeManager(userLogin,securityConnectionString,userID.ToString(),base.ClientId);//dvatsa-cloud
				
				objMergeManager.DSN=connectionString;
				objMergeManager.UID= userID.ToString();

                if (m_sDocPath != "")
                {
                    objMergeManager.TemplatePath = m_sDocPath;
                }
				objMergeManager.DocStorageDSN=m_sDocPath;
				objMergeManager.DocPathType=m_iDocPathType;
				objMergeManager.SecurityDSN=securityConnectionString;
				
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/TableName");
				if (objNode.InnerText!=null)
				{
				sTableName=objNode.InnerText;
				}
				//crawford enhancements..modified by Raman Bhatia
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/AllStatesSelected");
				if (objNode.InnerText!=null)
				{
				bAllStatesSelected = Conversion.ConvertStrToBool(objNode.InnerText);
				}
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/MergeDocumentType");
				if (objNode.InnerText!=null)
				{
					lMergeDocumentType=Conversion.ConvertStrToLong(objNode.InnerText);
				}
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/MergeDocumentFormat");
				if (objNode.InnerText!=null)
				{
					lMergeDocumentFormat=Conversion.ConvertStrToLong(objNode.InnerText);
				}
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/State");
				if (objNode.InnerText!=null)
				{
					lStateId=Conversion.ConvertStrToLong(objNode.InnerText);
				}
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/FormName");
				if (objNode.InnerText!=null)
				{
					sFormName=objNode.InnerText;
				}
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/RecordId");
				if (objNode.InnerText!=null)
				{
					iRecordId = Conversion.ConvertObjToInt(objNode.InnerText, base.ClientId);
				}
				
				lCatId = objMergeManager.RMTableNameToCatId(sTableName);
				
				//CatId has to be generated by Adaptor, UI cannot provide it...modified by Raman Bhatia
				
				//objNode=p_objXmlIn.SelectSingleNode(@"/Template/CatId");
				
				//generating LOB..crawford enhancement.. modified by Raman Bhatia
				
				switch (sFormName)
				{
					case "claimgc": sShortCodeSelected = "GC";
									break;	

					case "claimva": sShortCodeSelected = "VA";
									break;
					
					case "claimwc": sShortCodeSelected = "WC";
									break;

					case "claimdi": sShortCodeSelected = "DI";
									break;
				// Ayush: Mail Merge for Property Claims:  12/14/2009,  MITS 18291 Start:
                    case "claimpc": sShortCodeSelected = "PC";  
                                    break;
				// Ayush: Mail Merge for Property Claims:  12/14/2009,  MITS 18291 End
					case "event"  : break;
						
					default:
						if( sFormName.Length > 6 )
						{
							sShortCodeSelected = sFormName.Substring(6);
						}
						break;

				}

				if(bAllStatesSelected == true) lStateId = -1;

                objTemplateManager = new TemplateManager(connectionString, m_userLogin, base.ClientId);
				objTemplateManager.DSN=connectionString;
				
				if(sFormName!="event")
					lLOB = objTemplateManager.GetLOB(sShortCodeSelected);
				else if(iRecordId!=0)
					lLOB = objTemplateManager.GetEventLOB(iRecordId);
				
					
				if (lCatId!=0)
				{
					p_objXmlOut=objMergeManager.GetAvailTemplates(lCatId , sTableName , lMergeDocumentType , lMergeDocumentFormat , lStateId , sFormName , lLOB);
				//spahariya MITS 28867
                    ObjXmlDomMAilRecipientList = objTemplateManager.GetMailRecipientList();
                    objNodeMailRecipientList = p_objXmlOut.ImportNode(ObjXmlDomMAilRecipientList.DocumentElement, true);
                    p_objXmlOut.FirstChild.AppendChild(objNodeMailRecipientList);
                    //spahariya - end
					//Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objSettings = new SysSettings(connectionString,base.ClientId);//dvatsa-cloud

					if (objSettings.UsePaperVisionInterface == true)
					{
                        objPaperVision = new PVWrapper();
                        objPaperVision.PVEntityID = PaperVisionSection.EntityID;
                        objPaperVision.PVLoginUrl = PaperVisionSection.LoginUrl;
                        objPaperVision.PVProjID = PaperVisionSection.ProjectID;
                        objPaperVision.PVShowDocLinkUrl = PaperVisionSection.DocumentLink;
                        objPaperVision.PVServerAddress = PaperVisionSection.PVServerAddress;
                        //Animesh inserted MITS 18345
                        objPaperVision.SharedNetworkLocationPath = PaperVisionSection.SharedNetworkLocationPath;
                        //objPaperVision.MappedDriveName = PaperVisionSection.MappedDriveName;    
                        objPaperVision.DomainName = PaperVisionSection.DomainName;
                        objPaperVision.UserID = PaperVisionSection.UserID;
                        objPaperVision.Password = PaperVisionSection.Password;   
                        //Animesh Insertion ends
                        blnPaperVisionValidation = objPaperVision.ValidateRequiredVariables();
                        if (blnPaperVisionValidation)
                        {
                            objPVElement = p_objXmlOut.CreateElement("PaperVisionValidation");
                            objPVElement.InnerText = "True";
                            p_objXmlOut.FirstChild.AppendChild(objPVElement);
                            objPVElement = null; 
                        }
                        else
                        {
                            objPVElement = p_objXmlOut.CreateElement("PaperVisionValidation");
                            objPVElement.InnerText = "False";
                            p_objXmlOut.FirstChild.AppendChild(objPVElement);
                            objPVElement = null; 
                        }

						objPVElement = p_objXmlOut.CreateElement("PaperVisionActive");
						objPVElement.InnerText="True";
						p_objXmlOut.FirstChild.AppendChild(objPVElement);
                        objPVElement = null; 
						//p_objXmlOut.SelectSingleNode("//PaperVisionActive").InnerText = "True";
					}
					else
					{
						//p_objXmlOut.SelectSingleNode("//PaperVisionActive").InnerText = "False";
                        objPVElement = p_objXmlOut.CreateElement("PaperVisionValidation");
                        objPVElement.InnerText = "False";
                        p_objXmlOut.FirstChild.AppendChild(objPVElement);
                        objPVElement = null; 
						objPVElement = p_objXmlOut.CreateElement("PaperVisionActive");
						objPVElement.InnerText = "False";
						p_objXmlOut.FirstChild.AppendChild(objPVElement);
                        objPVElement = null; 
					}
					//Animesh Insertion ends
				}
                //Mudabbir added 36022
                XmlElement e = null;
                objDataModelFactory = new DataModelFactory(m_userLogin, ClientId);
                e = p_objXmlOut.CreateElement("UseSilverlight");
                e.InnerText = objDataModelFactory.Context.InternalSettings.SysSettings.UseSilverlight.ToString().ToLower();
                p_objXmlOut.FirstChild.AppendChild(e);
				return true;
				
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetAvailTemplates.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objMergeManager=null;
                //Mona:PaperVisionMerge : Animesh Inserted MITS 16697
                objPaperVision = null;
                objPVElement = null;
                objSettings = null;  
                //Animesh Insertion Ends
                //mudabbir added : 36022
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();
			}
		}



		/// <summary>
		///  This function saves the Template data 
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<TemplateId>TemplateId</TemplateId>
		///				<FormInfomation>FormInfomation</FormInfomation>
		///				<FormDefinationData>
		///					<form></form>
		///				</FormDefinationData>
		///				<FormPermissionData>
		///					<form></form>
		///				</FormPermissionData>
		///				<File>File Content</File>
		///			</Template>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool StoreTemplate(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			XmlNode objNode=null;
			XmlNode objFormInfomation=null;
			XmlNode objFormDefinationData=null;
			XmlNode objFormPermissionData=null;
			XmlNode objFileName=null; //added by jasbinder on june 21, 2005
			XmlNode objFileContent=null; //added by jasbinder on june 21, 2005
			
			try
			{
				// Initializing the OutXml and creating the root node.
				p_objXmlOut = new XmlDocument();
				XmlElement objRootNode = p_objXmlOut.CreateElement( "RootElement" );
				objRootNode.InnerText = "TempText" ;
				p_objXmlOut.AppendChild( objRootNode );
                //mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objTemplateManager.DSN=connectionString;
				objTemplateManager.UID= userID.ToString();
				
				objTemplateManager.TemplatePath=m_sDocPath;
				objTemplateManager.DocStorageDSN=m_sDocPath;
				objTemplateManager.DocPathType=m_iDocPathType;
				objTemplateManager.SecurityDSN=securityConnectionString;
				objFormInfomation=p_objXmlIn.SelectSingleNode(@"/Template/FormInfomation");
				objFormDefinationData=p_objXmlIn.SelectSingleNode(@"/Template/FormDefinationData");
				objFormPermissionData=p_objXmlIn.SelectSingleNode(@"/Template/FormPermissionData");
				//objFile=p_objXmlIn.SelectSingleNode(@"/Template/File");
				objFileContent=p_objXmlIn.SelectSingleNode(@"/Template/FileContent");//added by jasbinder on june 21, 2005
				objFileName=p_objXmlIn.SelectSingleNode(@"/Template/FileName");//added by jasbinder on june 21, 2005
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/TemplateId");

                objTemplateManager.CurTemplate.FormFileName = Generic.GetServerUniqueFileName("mmt", "doc", base.ClientId);
				if (objNode.InnerText!=null)
				{	
					//added by jasbinder on 17th june 2005
					if(objNode.InnerText=="0")
					{
						objTemplateManager.AddNew();						
					}
					else
					{
						objTemplateManager.MoveTo(objNode.InnerText);
					}
					//till here
				}
				//objTemplateManager.CurTemplate.Store(objFormInfomation.OuterXml,objFormDefinationData.OuterXml,
				//	objFormPermissionData.OuterXml,objFile.InnerText,objFile.InnerText);
				objTemplateManager.CurTemplate.Store(objFormInfomation.OuterXml,objFormDefinationData.OuterXml,
					objFormPermissionData.OuterXml,objFileContent.InnerText,objFileName.InnerText);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.StoreTemplate.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objFormInfomation=null;
				objFormDefinationData=null;
				objFormPermissionData=null;
				//objFile=null;
				objFileName=null;
				objFileContent=null;
				objTemplateManager=null;
			}
		}
		/// <summary>
		///  This function saves the Template data 
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<TemplateId>TemplateId</TemplateId>
		///				<FormInfomation>FormInfomation</FormInfomation>
		///				<FormDefinationData>
		///					<form></form>
		///				</FormDefinationData>
		///				<FormPermissionData>
		///					<form></form>
		///				</FormPermissionData>
		///				<File>File Content</File>
		///			</Template>
		///		</Document>
		///</param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool StoreTemplateFromMemory(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			XmlNode objNode=null;
			XmlNode objFormInfomation=null;
			XmlNode objFormDefinationData=null;
			XmlNode objFormPermissionData=null;
			XmlNode objFile=null;
			MemoryStream objMemory=null;
			MemoryStream objHeaderFileMemory=null;
			XmlElement objTempElement=null;
			
			try
			{
                //mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objTemplateManager.DSN=connectionString;
				objTemplateManager.UID= userID.ToString();
				
				objTemplateManager.TemplatePath=m_sDocPath;
				objTemplateManager.DocStorageDSN=m_sDocPath;
				objTemplateManager.DocPathType=m_iDocPathType;
				objTemplateManager.SecurityDSN=securityConnectionString;
				objFormInfomation=p_objXmlIn.SelectSingleNode(@"/Template/FormInfomation");
				objFormDefinationData=p_objXmlIn.SelectSingleNode(@"/Template/FormDefinationData");
				objFormPermissionData=p_objXmlIn.SelectSingleNode(@"/Template/FormPermissionData");
				objFile=p_objXmlIn.SelectSingleNode(@"/Template/File");
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/TemplateId");
                objTemplateManager.CurTemplate.FormFileName = Generic.GetServerUniqueFileName("mmt", "doc", base.ClientId);
				if (objNode.InnerText!=null)
				{
					objTemplateManager.MoveTo(objNode.InnerText);
				
					objMemory=new MemoryStream(Convert.FromBase64String(objFile.InnerText));
					objTemplateManager.CurTemplate.Store(objFormInfomation.OuterXml,objFormDefinationData.OuterXml,
						objFormPermissionData.OuterXml,objMemory,out objHeaderFileMemory);
				}
				objNode=p_objXmlOut.CreateElement("Template");
				p_objXmlOut.AppendChild(objNode);
				objTempElement=p_objXmlOut.CreateElement("TemplateHeaderContent");
				objTempElement.SetAttribute("Filename",Path.GetFileName(Path.GetTempFileName()).Replace(".tmp",".HDR"));
				objTempElement.InnerText=Convert.ToBase64String(objHeaderFileMemory.ToArray());
				p_objXmlOut.FirstChild.AppendChild(objTempElement);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.StoreTemplate.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objTempElement=null;
				objFormInfomation=null;
				objFormDefinationData=null;
				objFormPermissionData=null;
				objFile=null;
                if (objMemory != null)
                {
                    objMemory.Dispose();
                }
                if (objHeaderFileMemory != null)
                {
                    objHeaderFileMemory.Dispose();
                }
				objTemplateManager=null;
			}
		}
		/// <summary>
		/// This function deletes a template
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<TemplateId>TemplateId</TemplateId>
		///			</Template>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml</param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool DeleteTemplate(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			XmlNode objNode=null;
			
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objTemplateManager.DSN=connectionString;
				objTemplateManager.UID= userID.ToString();
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/TemplateId");
				if (objNode.InnerText!=null)
				{
					
					//added by jasbinder on 9th june 2005
					string strobjNode;
					string [] strArray;										
					
					strobjNode = objNode.InnerText.ToString();				
					
					
					strArray = strobjNode.Split(' ');
					for (int i=0;i<strArray.Length;i++)
					{	
						objTemplateManager.DeleteTemplate(Conversion.ConvertStrToLong(strArray[i].ToString()));
					}
					
					//till here
					
				}
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.DeleteTemplate.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objTemplateManager=null;
			}
		}

		/// <summary>
		/// This function returns the categories list
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<GetCategoryList><Categories><Category Category="" CategoryId="" CategoryDesc="" />
		///	</Categories></GetCategoryList>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetCategoryList(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			
			try
			{
				 //mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objTemplateManager.DSN=connectionString;
				p_objXmlOut=objTemplateManager.GetCategoryList();
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetCategoryList.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objTemplateManager=null;
			}
		}

		/// <summary>
		/// This function returns the the default format code and document type code
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<GetDefaultFormatDocumentTypeCode><FormatTypeCode><FormatCode codeid /></FormatTypeCode>
		///	<DocumentTypeCode><DocumentCode codeid /></DocumntTypeCode></GetDefaultFormatDocumentTypeCode>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetDefaultFormatDocumentTypeCode(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objTemplateManager.DSN=connectionString;
				p_objXmlOut=objTemplateManager.GetDefaultFormatDocumentTypeCode();
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetCategoryList.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objTemplateManager=null;
			}
		}

		/// <summary>
		/// This function returns the existing permission list on a template
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<TemplateId>TemplateId</TemplateId>
		///			</Template>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<UsedTemplatePerm><TemplatePermItems><PermItem></PermItem></TemplatePermItems></UsedTemplatePerm>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetUsedPermList(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			XmlNode objNode=null;
			
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objTemplateManager.DSN=connectionString;
				objTemplateManager.UID= userID.ToString();
				
				objTemplateManager.SecurityDSN=securityConnectionString;
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/TemplateId");
				if (objNode.InnerText!=null)
				{
					objTemplateManager.MoveTo(objNode.InnerText);

					p_objXmlOut=objTemplateManager.CurTemplate.GetUsedPermList();
				}
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetUsedPermList.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objTemplateManager=null;
			}
		}


		/// Name		: GetServerUniqueFileName
		/// Author		: Jasbinder Singh Bali
		/// Date Created: 06/15/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function returns a unique file name
		/// </summary>
		public bool GetServerUniqueFileName(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objTemplateManager.DSN=connectionString;
				objTemplateManager.UID= userID.ToString();
				p_objXmlOut=objTemplateManager.GetServerUniqueFileName();
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetServerUniqueFileName.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objTemplateManager=null;
			}
		}



		/// <summary>
		/// This function returns the class of document
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<DocumentClass><Classes><Class codeid="" description="" />
		///	</Classes></DocumentClass>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetDocumentClasses(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objTemplateManager.DSN=connectionString;
				objTemplateManager.UID= userID.ToString();
				p_objXmlOut=objTemplateManager.GetDocumentClasses();
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetDocumentClasses.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objTemplateManager=null;
			}
		}

		private bool GetFile(string p_sFileName,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			XmlNode objNode=null;
			XmlElement objTempElement=null;
			MemoryStream objMemory=null;
			
			string sFilename=p_sFileName;
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objTemplateManager.TemplatePath=m_sDocPath;
				objTemplateManager.DocStorageDSN=m_sDocPath;
				objTemplateManager.DocPathType=m_iDocPathType;
				objTemplateManager.GetFile(p_sFileName,out objMemory);
				objNode=p_objXmlOut.CreateElement("Template");
				p_objXmlOut.AppendChild(objNode);
				objTempElement=p_objXmlOut.CreateElement("File");
				objTempElement.SetAttribute("Filename",sFilename);
				if( objMemory != null)
					objTempElement.InnerText=Convert.ToBase64String(objMemory.ToArray());
				p_objXmlOut.FirstChild.AppendChild(objTempElement);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetFile.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objTemplateManager=null;
                if (objMemory != null)
                {
                    objMemory.Dispose();
                }
				objNode=null;
				objTempElement=null;
			}
		}
		//function added by jasbinder on 5th May 2005
		//to get the string RTF instead of memory stream.
		private bool GetFileRTF(string p_sFileName,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			XmlNode objNode=null;
			XmlElement objTempElement=null;
			//MemoryStream objMemory=null;
			string objMemory=null;
			//commented by jasbinder 
			//string sFilename="";
			//added by jasbinder
			string sFilename=p_sFileName;
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objTemplateManager.TemplatePath=m_sDocPath;
				
				objTemplateManager.DocStorageDSN=m_sDocPath;
				objTemplateManager.DocPathType=m_iDocPathType;
				
				objTemplateManager.GetFileRTF(p_sFileName,out objMemory);
							
				objNode=p_objXmlOut.CreateElement("Template");
				p_objXmlOut.AppendChild(objNode);
				objTempElement=p_objXmlOut.CreateElement("File");				
				objTempElement.SetAttribute("Filename",sFilename);
				

				//objTempElement.InnerText=Convert.ToBase64String(objMemory.ToArray());
				objTempElement.InnerText = objMemory;	
				p_objXmlOut.FirstChild.AppendChild(objTempElement);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetFileRTF.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objTemplateManager=null;
				objMemory=null;
				objNode=null;
				objTempElement=null;
			}
		}
		private MemoryStream GetFile(string p_sFileName)
		{
			TemplateManager objTemplateManager=null;
			MemoryStream objMemory=null;
            try
            {
                //mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
                objTemplateManager.TemplatePath = m_sDocPath;
                objTemplateManager.DocStorageDSN = m_sDocPath;
                objTemplateManager.DocPathType = m_iDocPathType;
                objTemplateManager.GetFile(p_sFileName, out objMemory);
                return objMemory;
            }
            finally
            {
                objTemplateManager = null;
                if (objMemory != null)
                {
                    objMemory.Dispose();
                }
            }
			
		}
	
		private MemoryStream GetFile(string p_sFileName,bool blChange)
		{
			TemplateManager objTemplateManager=null;
			MemoryStream objMemory=null;
            try
            {
                //mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);

                string sPath = RMConfigurator.UserDataPath;

                if (!Directory.Exists(sPath + "\\MailMerge"))
                    Directory.CreateDirectory(sPath + "\\MailMerge");
                if (m_iDocPathType == 0)
                    objTemplateManager.TemplatePath = sPath + "\\MailMerge";
                else
                    objTemplateManager.TemplatePath = m_sDocPath;
                objTemplateManager.DocStorageDSN = m_sDocPath;
                objTemplateManager.DocPathType = m_iDocPathType;
                objTemplateManager.GetFile(p_sFileName, out objMemory);
                return objMemory;
            }
            finally
            {
                objTemplateManager = null;
                if (objMemory != null)
                {
                    objMemory.Dispose();
                }
            }
			
		}


		//added by jasbinder on 5th may 2005
		private string GetFileRTF(string p_sFileName)
		{
			TemplateManager objTemplateManager=null;
			//MemoryStream objMemory=null;
			string objMemory=null;
            try
            {
                //mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
                objTemplateManager.TemplatePath = m_sDocPath;
                objTemplateManager.DocStorageDSN = m_sDocPath;
                objTemplateManager.DocPathType = m_iDocPathType;
                objTemplateManager.GetFileRTF(p_sFileName, out objMemory);
                return objMemory;
            }
            finally
            {
                objTemplateManager = null;
                objMemory = null;
            }
			
		}
		/// <summary>
		/// This function returns the specified file in memory
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<GetFile>File Name</GetFile>
		///			</Template>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<Template><File Filename="">FileContent</File></Template>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetFile(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			XmlNode objNode=null;
			XmlElement objTempElement=null;
			MemoryStream objMemory=null;
			
			string sFilename="";
            try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/IsPrefab");
				if( objNode!=null && objNode.InnerText != "" )
				{
					objTemplateManager.TemplatePath += m_sDocPath+"\\template";
				}		
				else
					objTemplateManager.TemplatePath=m_sDocPath;

				objTemplateManager.DocStorageDSN=m_sDocPath;
				objTemplateManager.DocPathType=m_iDocPathType;
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/GetFile");
				if (objNode.InnerText!=null)
				{
					sFilename=objNode.InnerText;
					objTemplateManager.GetFile(objNode.InnerText,out objMemory);
				}
				objNode=p_objXmlOut.CreateElement("Template");
				p_objXmlOut.AppendChild(objNode);
				objTempElement=p_objXmlOut.CreateElement("File");
				objTempElement.SetAttribute("Filename",sFilename);
				objTempElement.InnerText= Convert.ToBase64String(objMemory.ToArray());
				p_objXmlOut.FirstChild.AppendChild(objTempElement);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetFile.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objTemplateManager=null;
                if (objMemory != null)
                {
                    objMemory.Dispose();
                }
				objNode=null;
				objTempElement=null;
			}
		}
		/// <summary>
		/// This function saves specified file
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///			    <FileName>FileName</FileName>
		///				<File>File Contents</GetFile>
		///			</Template>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool SaveFile(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			XmlNode objNode=null;
			MemoryStream objMemory=null;
			string sFilename="";
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				objTemplateManager.TemplatePath=m_sDocPath;
				objTemplateManager.DocStorageDSN=m_sDocPath;
				objTemplateManager.DocPathType=m_iDocPathType;
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/FileName");
				if (objNode.InnerText!=null)
				{
					sFilename=objNode.InnerText;
				}
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/File");
				objMemory=new MemoryStream(Convert.FromBase64String(objNode.InnerText));
				objTemplateManager.SaveFile(sFilename, objMemory);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.SaveFile.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objTemplateManager=null;
                if (objMemory != null)
                {
                    objMemory.Dispose();
                }
				objNode=null;
			}
		}



	//function added by jasbinder on 5th July 2005

	/// <summary>
	/// This function returns the Category list, Template list and Server Unique File Name of document
	/// </summary>
	/// <param name="p_objXmlIn">Input parameters as xml</param>
	/// <param name="p_objXmlOut">Result as Output xml
	///	Output structure is as follows-:
	/// <DocumentCategory><Categories><Category codeid="" description="" />
	/// </Categories></DocumentCategory>
	/// </param>
	/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
	/// <returns>Success -True or Failure -false in execution of the function</returns>
	public bool GetCategoryListPrefabTemplateListServerUniqueFileName(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
	{
		TemplateManager objTemplateManager=null;
		
		try
		{
			//mgaba2:MITS 11704-03/11/2008
            //Document path of the DSN is overwritten by the User's Doc path(if exists)
            //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
            //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
            if (this.userLogin.DocumentPath.Length > 0)
            {
                m_iDocPathType = 0;
                m_sDocPath = userLogin.DocumentPath;
            }
            else
            {
                m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
            }
            objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
			objTemplateManager.TemplatePath=m_sDocPath;
			objTemplateManager.DSN=connectionString;
			objTemplateManager.UID= userID.ToString();
			p_objXmlOut=objTemplateManager.GetCategoryListPrefabTemplateListServerUniqueFileName();
			return true;
		}
		catch( RMAppException p_objException )
		{
			p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
			return false;
		}
		catch(Exception p_objException)
		{
			p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetCategoryListPrefabTemplateListServerUniqueFileName.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
			return false;
		}
		finally
		{
			objTemplateManager=null;
		}
	}


//function added by jasbinder on 30th June 2005

		/// <summary>
		/// This function returns the Categories of document
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		/// <DocumentCategory><Categories><Category codeid="" description="" />
		/// </Categories></DocumentCategory>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetDocumentTypesClassesCategories(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objTemplateManager.DSN=connectionString;
				objTemplateManager.UID= userID.ToString();
				p_objXmlOut=objTemplateManager.GetDocumentTypesClassesCategories();
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetDocumentTypesClassesCategories.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objTemplateManager=null;
			}
		}





		/// <summary>
		/// This function returns the Categories of document
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		/// <DocumentCategory><Categories><Category codeid="" description="" />
		/// </Categories></DocumentCategory>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetDocumentCategories(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objTemplateManager.DSN=connectionString;
				objTemplateManager.UID= userID.ToString();
				p_objXmlOut=objTemplateManager.GetDocumentCategories();
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetDocumentCategories.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objTemplateManager=null;
			}
		}

		/// <summary>
		/// This function returns the all permissions 
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		/// <AvailableTemplatePerm><TemplatePermItems><PermItem DisplayName="" GroupId="" IsGroup="" UserId="" />
		/// </TemplatePermItems></AvailableTemplatePerm>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetAvailablePermList(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objTemplateManager.DSN=connectionString;
				objTemplateManager.SecurityDSN=securityConnectionString;
				
				p_objXmlOut=objTemplateManager.GetAvailablePermListEx(DSNID.ToString());
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetAvailablePermList.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objTemplateManager=null;
			}
		}

		/// <summary>
		/// This function returns the Admin tracking tables list
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<TemplateId>TemplateId</TemplateId>
		///			</Template>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<AvailableAdminTableList><TableNames><Table CategoryDesc="" CategoryName="" />
		///	</TableNames></AvailableAdminTableList>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetAvailableAdminTableList(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			XmlNode objNode=null;
			
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objTemplateManager.DSN=connectionString;
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/TemplateId");
				if (objNode.InnerText!=null)
				{
					objTemplateManager.MoveTo(objNode.InnerText);
				}
				p_objXmlOut=objTemplateManager.CurTemplate.GetAvailableAdminTableList();
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetAvailableAdminTableList.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objTemplateManager=null;
			}
		}

		/// <summary>
		/// This function returns distinct tables related to passed category
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<CategoryId>CategoryId</CategoryId>
		///			</Template>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<GetFieldDisplayCategoryList><Categories><Category CategoryName="" />
		///	</Categories></GetFieldDisplayCategoryList>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetFieldDisplayCategoryList(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			XmlNode objNode=null;
			
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objTemplateManager.DSN=connectionString;
				objTemplateManager.UID= userID.ToString();
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/CategoryId");
				if (objNode.InnerText!=null)
				{
					p_objXmlOut=objTemplateManager.GetFieldDisplayCategoryList(Conversion.ConvertStrToLong(objNode.InnerText));
				}
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetFieldDisplayCategoryList.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objTemplateManager=null;
			}
		}
		/// <summary>
		/// This function retrieves a file
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml</param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<DocumentType><Types><Type codeid="" description="" />
		///	</Types></DocumentType>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetDocumentTypes(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				objTemplateManager.DSN=connectionString;
				objTemplateManager.UID= userID.ToString();
				p_objXmlOut=objTemplateManager.GetDocumentTypes();
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetDocumentTypes.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objTemplateManager=null;
			}
		}
		/// <summary>
		///  This function returns the existing fields related to a template
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<TemplateId>TemplateId</TemplateId>
		///			</Template>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<UsedFieldList><Fields><Field CatId="" DisplayCat="" FieldDesc="" FieldName="" FieldId="" />
		///	</Fields></UsedFieldList>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetUsedFieldList(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			XmlNode objNode=null;
			
					
			
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				objTemplateManager.SecurityDSN=securityConnectionString;
				objTemplateManager.DSN=connectionString;
				objTemplateManager.UID= userID.ToString();
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/TemplateId");
				if (objNode.InnerText!=null)
				{
					objTemplateManager.MoveTo(objNode.InnerText);
					p_objXmlOut=objTemplateManager.CurTemplate.GetUsedFieldList();
				}
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetUsedFieldList.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objTemplateManager=null;
			}
		}

		/// <summary>
		/// This function returns field list as Xml string
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// The structure of input XML document would be:
		/// 	<Document>
		///			<Template>
		///				<CategoryId>CategoryId</CategoryId>
		///			</Template>
		///		</Document>
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<AvailableFieldsList><Fields><Field DisplayCat=""><Value FieldName="" FieldId="" FieldType="" FieldTable="" LongCodeFlag="" IsSupp="" /></Field>
		///	</Fields></AvailableFieldsList>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetAvailableFieldList(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			XmlNode objNode=null;
			
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(connectionString, m_userLogin, base.ClientId);
				objTemplateManager.SecurityDSN=securityConnectionString;
				objTemplateManager.DSN=connectionString;
				objNode=p_objXmlIn.SelectSingleNode(@"/Template/CategoryId");
				if (objNode.InnerText!=null)
				{
					p_objXmlOut=objTemplateManager.CurTemplate.CreateAvailableFieldsXml(Conversion.ConvertStrToLong(objNode.InnerText));
				}
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetAvailableFieldList.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objNode=null;
				objTemplateManager=null;
			}
		}

		/// <summary>
		/// This function returns the list of all available templates list
		/// </summary>
		/// <param name="p_objXmlIn">Input parameters as xml
		/// </param>
		/// <param name="p_objXmlOut">Result as Output xml
		///	Output structure is as follows-:
		///	<GetPrefabTemplateList><PrefabTemplates><Template PrefabTemplateDesc="" PrefabTemplateFile=""/>
		///	</PrefabTemplates></GetPrefabTemplateList>
		/// </param>
		/// <param name="p_objErrOut">Error(s) encountered while executing the function</param>
		/// <returns>Success -True or Failure -false in execution of the function</returns>
		public bool GetPrefabTemplateList(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			TemplateManager objTemplateManager=null;
			try
			{
				//mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }
                objTemplateManager = new TemplateManager(m_userLogin, base.ClientId);
				
				
				objTemplateManager.TemplatePath=m_sDocPath+"\\template";
				p_objXmlOut=objTemplateManager.GetPrefabTemplateList();
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("MailMergeAdaptor.GetPrefabTemplateList.Error",base.ClientId),BusinessAdaptorErrorType.Error);//dvatsa-cloud
				return false;
			}
			finally
			{
				objTemplateManager=null;
				
			}
		}

        /// <summary>
        ///  //Mits 23683 05/13/2011 Neha Get mail merge option from user preferences.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetMailMergeOptions(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            MergeManager objMergeManager = null;
            XmlNode objNode = null;
            string sTableName = "";
            string sRecordselect = "";
            string sAttach = "";

            //Mits:36030 JIRA RMA-337 nshah28 start
            string sAllstates = "";
            string sJurisdiction = "";
            string sMergedocumenttype = "";
            string sMergedocumentformat = "";
            string sLettername = "";
            string sSaveMySettings = "";
            //Mits:36030 JIRA RMA-337 nshah28 end

            try
            {

                objMergeManager = new MergeManager(userLogin, securityConnectionString, userID.ToString(),base.ClientId);//dvatsa-cloud

                objMergeManager.DSN = connectionString;
                objMergeManager.UID = userID.ToString();

                if (m_sDocPath != "")
                {
                    objMergeManager.TemplatePath = m_sDocPath;
                }
                objMergeManager.DocStorageDSN = m_sDocPath;
                objMergeManager.DocPathType = m_iDocPathType;
                objMergeManager.SecurityDSN = securityConnectionString;

                objNode = p_objXmlIn.SelectSingleNode(@"/Template/TableName");
                if (objNode.InnerText != null)
                {
                    sTableName = objNode.InnerText;
                }
                //crawford enhancements..modified by Raman Bhatia
                objNode = p_objXmlIn.SelectSingleNode(@"/Template/RecordSelect");
                if (objNode.InnerText != null)
                {
                    sRecordselect = objNode.InnerText;
                }
                objNode = p_objXmlIn.SelectSingleNode(@"/Template/Attach");
                if (objNode.InnerText != null)
                {
                    sAttach = objNode.InnerText;
                }

                //Mits:36030 JIRA RMA-337 nshah28 start
                objNode = p_objXmlIn.SelectSingleNode(@"/Template/AllStates");
                if (objNode.InnerText != null)
                {
                    sAllstates = objNode.InnerText;
                }
                objNode = p_objXmlIn.SelectSingleNode(@"/Template/Jurisdiction");
                if (objNode.InnerText != null)
                {
                    sJurisdiction = objNode.InnerText;
                }
                objNode = p_objXmlIn.SelectSingleNode(@"/Template/MergeDocumentType");
                if (objNode.InnerText != null)
                {
                    sMergedocumenttype = objNode.InnerText;
                }
                objNode = p_objXmlIn.SelectSingleNode(@"/Template/MergeDocumentFormat");
                if (objNode.InnerText != null)
                {
                    sMergedocumentformat = objNode.InnerText;
                }


                objNode = p_objXmlIn.SelectSingleNode(@"/Template/SaveMySettings");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        sSaveMySettings = objNode.InnerText;
                    }
                }


                objNode = p_objXmlIn.SelectSingleNode(@"/Template/Lettername");
                if (objNode.InnerText != null)
                {
                    sLettername = objNode.InnerText;
                }
                //Mits:36030 JIRA RMA-337 nshah28 end

                if (userID.ToString() != "")
                {
                    //Mits:36030 JIRA RMA-337 nshah28 start
                    p_objXmlOut = objMergeManager.GetMailMergeOptions(userID.ToString(), sTableName, sRecordselect, sAttach, sAllstates, sJurisdiction, sMergedocumenttype, sMergedocumentformat, sLettername, sSaveMySettings);
                    //Mits:36030 JIRA RMA-337 nshah28 end
                }
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("MailMergeAdaptor.GetSaveMailMergeOptions.Error",base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
                return false;
            }
            finally
            {
                objNode = null;
                objMergeManager = null;
            }
        }
        /// <summary>
        /// //Mits 23683 05/13/2011 Neha save mail merge option in user preferences.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool SaveMailMergeOptions(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            MergeManager objMergeManager = null;
            XmlNode objNode = null;
            string sTableName = "";
            string sRecordselect = "";
            string sAttach = "";

            //Mits:36030 JIRA RMA-337 nshah28 start
            string sAllstates = "";
            string sJurisdiction = "";
            string sMergedocumenttype = "";
            string sMergedocumentformat = "";
            string sSaveMySettings = "";
            string sLettername = "";
            //Mits:36030 JIRA RMA-337 nshah28 end

            try
            {

                objMergeManager = new MergeManager(userLogin, securityConnectionString, userID.ToString(),base.ClientId);//dvatsa-cloud

                objMergeManager.DSN = connectionString;
                objMergeManager.UID = userID.ToString();

                if (m_sDocPath != "")
                {
                    objMergeManager.TemplatePath = m_sDocPath;
                }
                objMergeManager.DocStorageDSN = m_sDocPath;
                objMergeManager.DocPathType = m_iDocPathType;
                objMergeManager.SecurityDSN = securityConnectionString;

                objNode = p_objXmlIn.SelectSingleNode(@"/Template/TableName");
                if (objNode.InnerText != null)
                {
                    sTableName = objNode.InnerText;
                }
                //crawford enhancements..modified by Raman Bhatia
                objNode = p_objXmlIn.SelectSingleNode(@"/Template/RecordSelect");
                if (objNode.InnerText != null)
                {
                    sRecordselect = objNode.InnerText;
                }
                objNode = p_objXmlIn.SelectSingleNode(@"/Template/Attach");
                if (objNode.InnerText != null)
                {
                    sAttach = objNode.InnerText;
                }

                //Mits:36030 JIRA RMA-337 nshah28 start
                objNode = p_objXmlIn.SelectSingleNode(@"/Template/AllStates");
                if (objNode.InnerText != null)
                {
                    sAllstates = objNode.InnerText;
                }
                objNode = p_objXmlIn.SelectSingleNode(@"/Template/Jurisdiction");
                if (objNode.InnerText != null)
                {
                    sJurisdiction = objNode.InnerText;
                }
                objNode = p_objXmlIn.SelectSingleNode(@"/Template/MergeDocumentType");
                if (objNode.InnerText != null)
                {
                    sMergedocumenttype = objNode.InnerText;
                }
                objNode = p_objXmlIn.SelectSingleNode(@"/Template/MergeDocumentFormat");
                if (objNode.InnerText != null)
                {
                    sMergedocumentformat = objNode.InnerText;
                }


                objNode = p_objXmlIn.SelectSingleNode(@"/Template/SaveMySettings");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        sSaveMySettings = objNode.InnerText;
                    }
                }


                objNode = p_objXmlIn.SelectSingleNode(@"/Template/Lettername");
                if (objNode.InnerText != null)
                {
                    sLettername = objNode.InnerText;
                }
                //Mits:36030 JIRA RMA-337 nshah28 end

                if (userID.ToString() != "")
                {
                    //Mits:36030 JIRA RMA-337 nshah28 start
                    objMergeManager.SaveMailMergeOptions(userID.ToString(), sTableName, sRecordselect, sAttach, sAllstates, sJurisdiction, sMergedocumenttype, sMergedocumentformat, sLettername, sSaveMySettings);
                    //Mits:36030 JIRA RMA-337 nshah28 end
                }
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("MailMergeAdaptor.GetSaveMailMergeOptions.Error",base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
                return false;
            }
            finally
            {
                objNode = null;
                objMergeManager = null;
            }
        }
        // Moved This Function to merge manager For AutoMailMerge Set Up
        //private void WriteBinaryFile(string fileName, byte[] fileBuffer)
        //{
        //    FileStream fileStream;
        //    BinaryWriter binaryWrtr;

        //    try
        //    {
        //        // create new or overwrite existing
        //        fileStream = new FileStream(fileName, FileMode.Create);
        //        binaryWrtr = new BinaryWriter(fileStream);
        //        binaryWrtr.Write(fileBuffer);

        //        binaryWrtr.Close();
        //        fileStream.Close();
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}
        //spahariya MITS 28867 - start ********************
        /// <summary>
        /// //Mits 28867 07/09/2012 spahariya Email the mail merge document to selected recipient.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool MailRTF(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            MergeManager objMergeManager = null;
            XmlNode objNode = null;
            MemoryStream objMemory = null;
            DocumentManager objDocumentManager = null;
            FileStorageManager objFileStorageManager = null;
            XmlElement objRetMessage = null;
            XmlElement objMessage = null;
            string sSafeFileName = string.Empty;
            string sCatid = string.Empty;
            string sName = string.Empty;
            string sTemplateId = string.Empty;
            string sKeywords = string.Empty;
            string sTable = string.Empty;
            string sType = string.Empty;
            string sRecordId = string.Empty;
            string sFileName = string.Empty;
            string sFormName = string.Empty;
            string sSendEmail = string.Empty;
            string sMailRecipient = string.Empty;
            string sRetMes = string.Empty;
            int iPsid = 0;
            try
            {
                objMergeManager = new MergeManager(userLogin, securityConnectionString, userID.ToString(),base.ClientId);//dvatsa-cloud
                //mgaba2:MITS 11704-03/11/2008
                //Document path of the DSN is overwritten by the User's Doc path(if exists)
                //m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                //m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                if (this.userLogin.DocumentPath.Length > 0)
                {
                    m_iDocPathType = 0;
                    m_sDocPath = userLogin.DocumentPath;
                }
                else
                {
                    m_iDocPathType = userLogin.objRiskmasterDatabase.DocPathType;
                    m_sDocPath = userLogin.objRiskmasterDatabase.GlobalDocPath;
                }

                //Get file name
                sFileName = p_objXmlIn.SelectSingleNode(@"/Template/NewFileName").InnerText;
                int iPosition = sFileName.LastIndexOf(@"\");
                if (iPosition != -1)
                {
                    sFileName = sFileName.Substring(iPosition + 1);
                }
                string sPath = RMConfigurator.BasePath;
                if (!Directory.Exists(sPath + @"\temp\CopyMailMerge"))
                {
                    Directory.CreateDirectory(sPath + @"\temp\CopyMailMerge");
                }
                sFileName = sPath + @"\temp\CopyMailMerge\" + sFileName;

                objMemory = new MemoryStream(Convert.FromBase64String(p_objXmlIn.SelectSingleNode(@"/Template/FileContent").InnerText));
                //Retrieve psid from request
                objNode = p_objXmlIn.SelectSingleNode(@"/Template/psid");
                if (objNode.InnerText != null)
                {
                    iPsid = Conversion.ConvertStrToInteger(objNode.InnerText);
                }

                sCatid = p_objXmlIn.SelectSingleNode(@"/Template/CatId").InnerText;
                sTemplateId = p_objXmlIn.SelectSingleNode(@"/Template/TemplateId").InnerText;
                sTable = p_objXmlIn.SelectSingleNode(@"/Template/TableName").InnerText;
                sType = p_objXmlIn.SelectSingleNode(@"/Template/Type").InnerText;
                sRecordId = p_objXmlIn.SelectSingleNode(@"/Template/RecordId").InnerText;
                sFormName = p_objXmlIn.SelectSingleNode(@"/Template/FormName").InnerText;                
                sSendEmail = p_objXmlIn.SelectSingleNode(@"/Template/SendEmail").InnerText;
                sMailRecipient = p_objXmlIn.SelectSingleNode(@"/Template/MailRecipient").InnerText;

                objDocumentManager = new DocumentManager(m_userLogin, base.ClientId);
                objDocumentManager.ConnectionString = connectionString;
                objDocumentManager.UserLoginName = userLogin.LoginName;               
               
              
                if (string.Compare(sSendEmail, "true", true) == 0 && string.Compare(sMailRecipient, "", true) != 0)
                {
                    sRetMes = objMergeManager.SendMailRTF(sFileName, objMemory, sTemplateId, sName, sTable, sType, sRecordId, iPsid, sFormName, sSendEmail, sMailRecipient);
                    if (!string.IsNullOrEmpty(sRetMes))
                    {
                        objRetMessage = p_objXmlOut.CreateElement("DispalyMessage");
                        p_objXmlOut.AppendChild(objRetMessage);
                        objMessage = p_objXmlOut.CreateElement("Message");
                        objMessage.InnerText = sRetMes;
                        p_objXmlOut.FirstChild.AppendChild(objMessage);
                        p_objErrOut.Add("", sRetMes, BusinessAdaptorErrorType.Error);
                    }                
                }
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("MailMergeAdaptor.MailRTF.Error",base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
                return false;
            }
            finally
            {
                objNode = null;
                //objMergeManager=null;
                //sbRowid=null;
            }
        }
        //spahariya MITS 28867 - start ********************
        /// <summary>
        /// //Mits 28867 07/09/2012 spahariya Get the word merge email details.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetWordMergeEmailDetailList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            WordMergeEmailDetail objWordEmailDetail = new WordMergeEmailDetail(m_connectionString, base.ClientId);//rkaur27
            XmlElement xmlElemBuutton = null;
            XmlNode objNode = null;
            string sButton = string.Empty;
            try
            {
                p_objXmlOut = objWordEmailDetail.GetWordMergeEmailDetailList(p_objXmlIn);
                if (!(base.userLogin.IsAllowedEx(RMB_WMEMAIL_DET_CREATE)))
                    sButton = "New";
                if (!(base.userLogin.IsAllowedEx(RMB_WMEMAIL_DET_UPDATE)))
                {
                    if (!string.IsNullOrEmpty(sButton))
                        sButton = sButton + "|Edit";
                    else
                        sButton = "Edit";
                }
                if (!(base.userLogin.IsAllowedEx(RMB_WMEMAIL_DET_DELETE)))
                {
                    if (!string.IsNullOrEmpty(sButton))
                        sButton = sButton + "|Delete";
                    else
                        sButton = "Delete";
                }
                if (!string.IsNullOrEmpty(sButton))
                {
                    xmlElemBuutton = (XmlElement)p_objXmlOut.SelectSingleNode("//MergeEmailDetailList");
                    xmlElemBuutton.SetAttribute("GridHideButtons", sButton);
                }

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("MailMergeAdaptor.GetWordMergeEmailDetailList.Error",base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
                return false;
            }
            finally
            {
                objWordEmailDetail=null;
            }
        }

        public bool DeleteWordMergeEmailDetail(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            WordMergeEmailDetail objWordEmailDetail = new WordMergeEmailDetail(m_connectionString, base.ClientId);//rkaur27
            try
            {               
                p_objXmlOut = objWordEmailDetail.DeleteWordMergeEmailDetail(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("MailMergeAdaptor.DeleteWordMergeEmailDetail.Error",base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
                return false;
            }
            finally
            {
                objWordEmailDetail = null;
            }
        }

        public bool GetMergeEmailDetail(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            WordMergeEmailDetail objWordEmailDetail = new WordMergeEmailDetail(m_connectionString, base.ClientId);//rkaur27
            try
            {                
                p_objXmlOut = objWordEmailDetail.GetMergeEmailDetail(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("MailMergeAdaptor.GetMergeEmailDetail.Error",base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
                return false;
            }
            finally
            {
                objWordEmailDetail = null;
            }
        }
                

        public bool SaveMergeEmailDetail(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            WordMergeEmailDetail objWordEmailDetail = new WordMergeEmailDetail(m_connectionString, base.ClientId);//rkaur27
            try
            {
                p_objXmlOut = objWordEmailDetail.SaveMergeEmailDetail(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("MailMergeAdaptor.SaveMergeEmailDetail.Error",base.ClientId), BusinessAdaptorErrorType.Error);//dvatsa-cloud
                return false;
            }
            finally
            {
                objWordEmailDetail = null;
            }
        }
		#endregion     
        
      
        //spahariya - end ***********************
	}
}
