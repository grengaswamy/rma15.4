using System;
using System.IO ;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Application.ProgressNotes;
using System.Collections;
using Riskmaster.DataModel;
using Riskmaster.Models;
using System.Collections.Generic;
using Riskmaster.Security.Authentication;
using Riskmaster.Db;
using Riskmaster.Settings;
namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: ProgressNotesAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 2-Oct-2005
	///* $Author	: Raman Bhatia and Vaibhav Kaushik
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	public class ProgressNotesAdaptor : BusinessAdaptorBase 
	{
		//Changed by Shruti on 23rdoct06 for MITS - 7949 -- starts
		#region Constants

		/// <summary>
		/// Function IDs for Progress Notes related security permission
		/// </summary>
        //pmittal5 Mits 21514 07/16/10 - Separating Enhanced Notes settings under all LOBs and Events
		private const int PROGNOTES_VIEW = 29000000;
		private const int PROGNOTES_CREATE = 29000001;       
		private const int PROGNOTES_PRINT = 29000002;
        private const int PROGNOTES_EDIT = 29000003;
        private const int PROGNOTES_DELETE = 29000004;//Added by abhishek for MITS 11403
        //Changed by Gagan for MITS 15503 : start
        private const int PROGNOTES_VIEW_ALL_NOTES = 29000005;
        //Changed by Gagan for MITS 15503 : end
        //MGaba2:MITS 22173: Separate branch for Templates
        //rsushilaggar MITS 2010 06/18/2010
        //private const int PROGNOTES_TEMPLATES = 29000006;
        //private const int PROGNOTES_TEMPVIEW = 29000008;
        //private const int PROGNOTES_TEMPCREATE = 29000007;
        //private const int PROGNOTES_TEMPEDIT = 29000009;
        //private const int PROGNOTES_TEMPDELETE = 29000010;
        //end rsushilaggar
        //MGaba2:MITS 22173:End
        private const int EVENT_PROGNOTES_VIEW = 29000000;
        private const int EVENT_PROGNOTES_CREATE = 29000001;
        private const int EVENT_PROGNOTES_PRINT = 29000002;
        private const int EVENT_PROGNOTES_EDIT = 29000003;
        private const int EVENT_PROGNOTES_DELETE = 29000004;//Added by abhishek for MITS 11403
        //Changed by Gagan for MITS 15503 : start
        private const int EVENT_PROGNOTES_VIEW_ALL_NOTES = 29000005;
        //Changed by Gagan for MITS 15503 : end
        //MGaba2:MITS 22173: Separate branch for Templates:Start
        //rsushilaggar MITS 2010 06/18/2010
        //private const int EVENT_PROGNOTES_TEMPLATES = 29000006;
        //private const int EVENT_PROGNOTES_TEMPVIEW = 29000008;
        //private const int EVENT_PROGNOTES_TEMPCREATE = 29000007;
        //private const int EVENT_PROGNOTES_TEMPEDIT = 29000009;
        //private const int EVENT_PROGNOTES_TEMPDELETE = 29000010;
        //MGaba2:MITS 22173:End
        private const int GC_PROGNOTES_VIEW = 1220400070;
        private const int DI_PROGNOTES_VIEW = 1220400090;
        private const int VA_PROGNOTES_VIEW = 1220400110;
        private const int WC_PROGNOTES_VIEW = 1220400130;
        private const int PC_PROGNOTES_VIEW = 1220400150;

        private const int PROGNOTES_CREATE_OFFSET = 1;
        private const int PROGNOTES_PRINT_OFFSET = 2;
        private const int PROGNOTES_EDIT_OFFSET = 3;
        private const int PROGNOTES_DELETE_OFFSET = 4;
        private const int PROGNOTES_VIEWALL_OFFSET = 5;
        //MGaba2:MITS 22173: Separate branch for Templates
        //private const int PROGNOTES_TEMPLATES_OFFSET = 10;
        //private const int PROGNOTES_TEMPCREATE_OFFSET = 11;
        //private const int PROGNOTES_TEMPVIEW_OFFSET = 12;
        //private const int PROGNOTES_TEMPEDIT_OFFSET = 13;
        //private const int PROGNOTES_TEMPDELETE_OFFSET = 14;
        private const int TEMPLATES = 29000006;
        private const int TEMPLATES_CREATE_OFFSET = 1;
        private const int TEMPLATES_VIEW_OFFSET = 2;
        private const int TEMPLATES_EDIT_OFFSET = 3;
        private const int TEMPLATES_DELETE_OFFSET = 4;
        //MGaba2:MITS 22173:End
        //End - pmittal5
        //williams-neha goel----start---Policy Enhanced Notes Permissions:MITS 21704
        private const int POLICY_PROGNOTES_VIEW = 9350;
        private const int POLICY_PROGNOTES_CREATE = 9351;
        private const int POLICY_PROGNOTES_PRINT = 9352;
        private const int POLICY_PROGNOTES_EDIT = 9353;
        private const int POLICY_PROGNOTES_DELETE = 9354;
        private const int POLICY_PROGNOTES_VIEW_ALL_NOTES = 9355;
        //williams-neha goel----end---Policy Enhanced Notes Permissions:MITS 21704

        private bool bIsDeleteIntegrate = false; //Mits id:34851(Smoke issue) - Govind
        /*Added by Gbindra MITS#34104 02132014 WWIG GAP15 START*/
        private const int PC_CLMNT_PROGNOTES_VIEW = 41960;
        private const int VA_CLMNT_PROGNOTES_VIEW = 7910;
        private const int GC_CLMNT_PROGNOTES_VIEW = 1960;
        /*Added by Gbindra MITS#34104 02132014 WWIG GAP15 END*/
		#endregion
		//Changed by Shruti on 23rdoct06 for MITS - 7949 -- ends

		public ProgressNotesAdaptor() 
		{
		}


		#region Public Functions
		/// Name		: SelectClaim
		/// Author		: Raman Bhatia
		/// Date Created: 04-Oct-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the claim list associated with a particular event-id
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<ProgressNotesManager>
		///				<EventID>Event ID</EventID>
		///			</ProgressNotesManager>	
		///		</Document>
		/// </param>

		/// <param name="p_objXmlOut">XML document containing Claim data.
		///		The structure of output XML document would be:
		///		<Document>
		///			<ClaimList>
		///				<Claim ClaimID="" ClaimNumber="" ClaimDate="" LastName="" FirstName="" ClaimStatusCode="" ClaimTypeCode="" LOB="" />
		///				<Claim ClaimID="" ClaimNumber="" ClaimDate="" LastName="" FirstName="" ClaimStatusCode="" ClaimTypeCode="" LOB="" />
		///			</ClaimList>
		///		</Document>
		/// </param>
		/// <param name="p_objErrOut">Error object</param>
		/// <returns>true if operation is successful else false</returns>
		public bool SelectClaim (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
				long lEventID = 0;
                string sNewRecord = ""; //Geeta 08/23/07 : MITS 10221
				XmlElement objXmlUser = null;
				ProgressNotesManager objProgressNotesManager = null;
				try
				{
                    objXmlUser = (XmlElement)p_objXmlIn.SelectSingleNode("//NewRecord");
                    if (objXmlUser != null)
                    {
                    sNewRecord = objXmlUser.InnerText;

                    //Geeta 08/23/07 : MITS 10221
                    if (sNewRecord == "false")
                    {
                        if (!(base.userLogin.IsAllowedEx(PROGNOTES_EDIT)))
                            throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesEdit, PROGNOTES_EDIT);
                    }
                    else
                    {
                        //Changed by Shruti on 23rdoct06 for MITS - 7949 -- starts
                        if (!(base.userLogin.IsAllowedEx(PROGNOTES_CREATE)))
                            throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesCreate, PROGNOTES_CREATE);
                        //Changed by Shruti on 23rdoct06 for MITS - 7949 -- ends                    
                        }
                    }

					objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode("//EventID");
					lEventID = Conversion.ConvertStrToLong (objXmlUser.InnerText);                    

					//Initialize the application layer ProgressNotesManager class
                    // Ash - cloud, overload constructor with Client ID
                    //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName  , loginName , userLogin.Password );                    
                    objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);
					p_objXmlOut = new XmlDocument();

					p_objXmlOut = objProgressNotesManager.SelectClaim(lEventID);
				}
				catch(RMAppException p_objException)
				{
					p_objErrOut.Add (p_objException, BusinessAdaptorErrorType.Error);
					return false;
				}
				catch(Exception p_objException)
				{
					p_objErrOut.Add (p_objException,Globalization.GetString ("ProgressNotesAdaptor.SelectClaim.Error",base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				finally 
				{
					objXmlUser = null;
					if( objProgressNotesManager != null )
					{
                        objProgressNotesManager.Dispose();
						objProgressNotesManager = null ;
					}	
				}
				return (true);

		}
		
		/// Name		: OnLoad
		/// Author		: Vaibhav Kaushik and Raman Bhatia
		/// Date Created: 11-Oct-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Loads the notes
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<ProgressNotesManager>
		///				<EventID>Event ID</EventID>
		///				<ClaimID>Claim ID</ClaimID>
		///				<ActivateFilter>Activate Filter Flag</ActivateFilter>
		///			</ProgressNotesManager>	
		///		</Document>
		///		The structure of output XML document would be:
		///		<Document>
		///			<ProgressNotes>
		///				<ProgressNote></ProgressNote>
		///			</ProgressNotes>	
		///		</Document>
		/// </param>
		/// </param>
		/// <param name="p_objErrOut">Error object</param>
		/// <returns>true if operation is successful else false</returns>
		

		public bool OnLoad(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut )
		{
				int iEventID = 0;
				int iClaimID = 0 ;
                //williams-neha goel--start:MITS 21704
                int iPolicyID = 0;
                //williams-neha goel--end:MITS 21704
				bool bActivateFilter = false;
				string sClaimIDList = string.Empty;
				string sEnteredByList = string.Empty;
				string sNoteTypeList = string.Empty;
                string sUserTypeList = string.Empty;
                string sActivityFromDate = string.Empty;
                string sActivityToDate = string.Empty;
                string sNotesTextContains = string.Empty;
                string sSortBy = string.Empty;
                string sFilterSQL = string.Empty;
                string sSubjectList = string.Empty; //zmohammad MITS 30218
                ////Added by akaushik5 for MITS 30789 Starts
                string sOrderBy = string.Empty;
                ////Added by akaushik5 for MITS 30789 Ends
            int iClaimantID = 0;//added by gbindra MITS#34104 WWIG GAP15
            SysSettings objSysSettings = null; //added by gbindra MITS#34104 WWIG GAP15
            bool bSuccess = false;//added by gbindra MITS#34104 WWIG GAP15

				XmlElement objTempNode = null;
				ProgressNotesManager objProgressNotesManager = null;
                try
                {
                    objSysSettings = new SysSettings(m_connectionString, base.ClientId); //Ash - cloud; //added by gbindra MITS#34104 WWIG GAP15
                    //Changed by Shruti on 23rdoct06 for MITS - 7949 -- starts
                    if (!(base.userLogin.IsAllowedEx(PROGNOTES_VIEW)))
                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, PROGNOTES_VIEW);
                    //Changed by Shruti on 23rdoct06 for MITS - 7949 -- ends	

                    objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//EventID");
                    iEventID = Conversion.ConvertStrToInteger(objTempNode.InnerText);

                    objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimID");
                    iClaimID = Conversion.ConvertStrToInteger(objTempNode.InnerText);
                    //williams-neha goel-start:MITS 21704
                    objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//PolicyId");
                    iPolicyID = Conversion.ConvertStrToInteger(objTempNode.InnerText);
                    //williams-neha goel --end:MITS 21704

                    /*added by gbindra MITS#34104 WWIG GAP15 START*/
                    if (objSysSettings.AllowNotesAtClaimant == true)
                    {
                        objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimantID");
                        iClaimantID = Conversion.CastToType<int>(objTempNode.InnerText, out bSuccess);
                    }
                    else
                    {
                        iClaimantID = -1;
                    }
                    //Initialize the application layer ProgressNotesManager class
                    // Ash - cloud, overload constructor with Client ID
                    //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName  , loginName , userLogin.Password );
                    objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);
                    p_objXmlOut = new XmlDocument();

                    //Filtering information
                    objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//ActivateFilter");
                    bActivateFilter = Conversion.ConvertStrToBool(objTempNode.InnerText);

                    if (bActivateFilter == true)
                    {
                        objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimIDList");
                        sClaimIDList = objTempNode.InnerText;

                        objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//EnteredByList");
                        sEnteredByList = objTempNode.InnerText;

                        objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//NoteTypeList ");
                        sNoteTypeList = objTempNode.InnerText;

                        objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//UserTypeList");
                        sUserTypeList = objTempNode.InnerText;

                        objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//ActivityFromDate");
                        sActivityFromDate = objTempNode.InnerText;

                        objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//ActivityToDate");
                        sActivityToDate = objTempNode.InnerText;

                        objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//NotesTextContains");
                        sNotesTextContains = objTempNode.InnerText;

                        objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//SortBy");
                        sSortBy = objTempNode.InnerText;

                        //zmohammad MITS 30218
                        objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//SubjectList");
                        sSubjectList = objTempNode.InnerText;

                        ////Changed by akaushik5 for MITS 30789 Starts
                        // sFilterSQL = objProgressNotesManager.CreateFilterString(sClaimIDList,sEnteredByList,sNoteTypeList,sUserTypeList,sActivityFromDate,sActivityToDate,sNotesTextContains,sSortBy);
                        objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//OrderBy");
                        sOrderBy = objTempNode.InnerText;

                        sFilterSQL = objProgressNotesManager.CreateFilterString(sClaimIDList, sEnteredByList, sNoteTypeList, sUserTypeList, sActivityFromDate, sActivityToDate, sNotesTextContains, sSortBy, sSubjectList, sOrderBy, iClaimantID); //Added by gbindra MITS#34104
                        ////Changed by akaushik5 for MITS 30789 Ends
                    }

                    //p_objXmlOut = objProgressNotesManager.OnLoad( iEventID , iClaimID , bActivateFilter , sFilterSQL);

                }
                catch (RMAppException p_objException)
                {
                    p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                    return false;
                }
                catch (Exception p_objException)
                {
                    p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.OnLoad.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
				finally 
				{
					objTempNode = null;
					if( objProgressNotesManager != null )
					{
                        objProgressNotesManager.Dispose();
						objProgressNotesManager = null ;
					}	
				}
		
				return (true);
        }
        #region WCF Integration --Parijat
        /// <summary>
        /// Loads the note in case of R5 where there is a separate WCF for Progress Notes.
        /// Created By: Parijat Sharma
        /// </summary>
        /// <param name="objDocument">Progress Note's Model class including the data which is needed to load progress notes</param>
        /// <param name="outDocument">Progress Note's Model class which needs to be loaded</param>
        /// <param name="p_objErrOut">Error object </param>
        /// <returns></returns>
        public bool OnLoadIntegrate(ProgressNotesType objDocument, out ProgressNotesType outDocument, ref BusinessAdaptorErrors p_objErrOut)
        {
            //tanwar2 - Print HTML Notes - start
            bool bPrintSelectedNotes = false;
            int iClaimProgressNoteId = 0;
            int iNoteTypeCodeId = 0;
            //tanwar2 - Print HTML Notes - end

            string sFilterSQL = "";
            outDocument = new ProgressNotesType();
            ProgressNotesManager objProgressNotesManager = null;
            bool bPrintAllPages = false; 
            SysSettings objSysSettings = null;//Added by gbindra MITS#34104 WWIG GAP15
            try
            {
                objSysSettings = new SysSettings(m_connectionString, base.ClientId); //Ash - cloud; //Added by gbindra MITS#34104 WWIG GAP15
                //willimas--neha goel----start:policy enhanced notes permissions:added if else
                if (!string.IsNullOrEmpty(objDocument.PolicyID.ToString()) && objDocument.PolicyID != 0)
                {
                    if (!(base.userLogin.IsAllowedEx(POLICY_PROGNOTES_VIEW)))
                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, POLICY_PROGNOTES_VIEW);
                }
                //willimas--neha goel----end:policy enhanced notes permissions
                else
                {
	                //pmittal5 Mits 21514 07/19/20
	                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_VIEW)))
	                //    throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, PROGNOTES_VIEW);
                    /*Added by gbindra MITS#34104 02132014 WWIG GAP15 START*/
                    if (objDocument.ClaimantId > 0 && objSysSettings.AllowNotesAtClaimant == true)
                    {
                        if (objSysSettings.AllowNotesAtClaimant == true)
                        {
                            switch (objDocument.LOB)
                            {
                                case "GC":
                                    if (!(base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW)))
                                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, GC_CLMNT_PROGNOTES_VIEW);
                                    break;
                                case "VA":
                                    if (!(base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW)))
                                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, VA_CLMNT_PROGNOTES_VIEW);
                                    break;
                                case "PC":
                                    if (!(base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW)))
                                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, PC_CLMNT_PROGNOTES_VIEW);
                                    break;
                            }
                        }
                    }
	                else if (objDocument.EventID != 0 && objDocument.ClaimID == 0)  //Event's Enhanced notes are opening
	                {
	                    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_VIEW)))
	                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, EVENT_PROGNOTES_VIEW);
	                }
	                else if (objDocument.ClaimID != 0)  //Claim's Enhanced notes are opening
	                {
	                    switch (objDocument.LOB)
	                    {
	                        case "GC":
	                            if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW)))
	                                throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, GC_PROGNOTES_VIEW);
	                            break;
	                        case "VA":
	                            if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW)))
	                                throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, VA_PROGNOTES_VIEW);
	                            break;
	                        case "WC":
	                            if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW)))
	                                throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, WC_PROGNOTES_VIEW);
	                            break;
	                        case "DI":
	                            if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW)))
	                                throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, DI_PROGNOTES_VIEW);
	                            break;
	                        case "PC":
	                            if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW)))
	                                throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, PC_PROGNOTES_VIEW);
	                            break;
	                    }
	                }
	                //End - pmittal5
                }

                //Initialize the application layer ProgressNotesManager class
                // Ash - cloud, overload constructor with Client ID
                //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password);
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);
                

                if(objDocument.ActivateFilter == true)
                {
                    //tanwar2 - Print HTML Notes - start
                    objProgressNotesManager.ApplyFilterFromSession(base.GetSessionObject(), objDocument);
                    //tanwar2 - Print HTML Notes - start

                    ////Changed by akaushik5 for MITS 30789 Starts
                    // sFilterSQL = objProgressNotesManager.CreateFilterString(objDocument.objFilter.ClaimIDList, objDocument.objFilter.EnteredByList, objDocument.objFilter.NoteTypeList, objDocument.objFilter.UserTypeList, objDocument.objFilter.ActivityFromDate, objDocument.objFilter.ActivityToDate, objDocument.objFilter.NotesTextContains, objDocument.objFilter.SortBy);
                    sFilterSQL = objProgressNotesManager.CreateFilterString(objDocument.objFilter.ClaimIDList, objDocument.objFilter.EnteredByList, objDocument.objFilter.NoteTypeList, objDocument.objFilter.UserTypeList, objDocument.objFilter.ActivityFromDate, objDocument.objFilter.ActivityToDate, objDocument.objFilter.NotesTextContains, objDocument.objFilter.SortBy, objDocument.objFilter.SubjectList, objDocument.objFilter.OrderBy, objDocument.objFilter.ClaimantIDList); //Added by gbindra MITS#34104 02132014 WWIG GAP15 
                    ////Changed by akaushik5 for MITS 30789 Ends
                 }

                //tanwar2 - Print HTML Notes - start
                if (!String.IsNullOrEmpty(objDocument.ClaimProgressNoteId))
                {
                    bPrintSelectedNotes = true;
                    iClaimProgressNoteId = Convert.ToInt32(objDocument.ClaimProgressNoteId);
                }

                if (!string.IsNullOrEmpty(objDocument.NoteTypeCodeId))
                {
                    iNoteTypeCodeId = Convert.ToInt32(objDocument.NoteTypeCodeId);
                    objDocument.ActivateFilter = true;
                    sFilterSQL = " AND NOTE_TYPE_CODE = " + iNoteTypeCodeId + " " + sFilterSQL;
                }
                //tanwar2 - Print HTML Notes - end

                //rsolanki2: mits 21294
                bPrintAllPages = objDocument.PrintMultiplePages;

                XmlDocument p_objXmlDocOut = new XmlDocument();
                objProgressNotesManager.m_UserLogin = base.m_userLogin; //Aman ML Change
                //williams:neha goel--start:added objDocument.PolicyID:MITS 21704
                p_objXmlDocOut = objProgressNotesManager.OnLoad(objDocument.EventID
                    , objDocument.ClaimID
                    , objDocument.ActivateFilter
                    , sFilterSQL
                    , objDocument.PageNumber
                    , objDocument.SortColumn
                    , objDocument.Direction
                    , bPrintAllPages
                    , objDocument.PolicyID
                    , objDocument.PolicyName
                    , objDocument.ViewNotesOption // mkaran2 - MITS 27038 - start 
                    , objDocument.ViewNotesTypeList // mkaran2 - MITS 27038 - end
                    , objDocument.ClaimantId  //Added by gbindra MITS#34104 02132014 WWIG GAP15 
                    );

                //srajindersin - MITS 34718 - 12/30/2013
                outDocument.PrintMultiplePages = bPrintAllPages;

                //williams-end:MITS 21704
                outDocument.ClaimID = objDocument.ClaimID;
                outDocument.EventID = objDocument.EventID;
                //williams-neha goel--start:MITS 21704
                outDocument.PolicyID = objDocument.PolicyID;
                outDocument.PolicyName = objDocument.PolicyName;
                outDocument.PolicyNumber = objDocument.PolicyNumber;
                //williams:neha goel--end:MITS 21704
                outDocument.FormName = objDocument.FormName;
                if(p_objXmlDocOut.SelectSingleNode("//Claimant")!= null)
                    outDocument.Claimant = p_objXmlDocOut.SelectSingleNode("//Claimant").InnerText;
                if (p_objXmlDocOut.SelectSingleNode("//FreezeText") != null)
                    outDocument.FreezeText = Convert.ToBoolean( p_objXmlDocOut.SelectSingleNode("//FreezeText").InnerText);
                //Changed by gagan for mits 14626 : start
                if (p_objXmlDocOut.SelectSingleNode("//ShowDateStamp") != null)
                    outDocument.ShowDateStamp = Convert.ToBoolean(p_objXmlDocOut.SelectSingleNode("//ShowDateStamp").InnerText);
                //Changed by gagan for mits 14626 : end
                
                //Start by Shivendu for MITS 18098
                if (p_objXmlDocOut.SelectSingleNode("//TotalNumberOfPages") != null)
                    outDocument.TotalNumberOfPages = Convert.ToInt32(p_objXmlDocOut.SelectSingleNode("//TotalNumberOfPages").InnerText);
               //MGaba2:MITS 18913:Deleting record was disabling links
                //outDocument.PageNumber = objDocument.PageNumber;
                
                if (p_objXmlDocOut.SelectSingleNode("//PageNumber") != null)
                    outDocument.PageNumber = Convert.ToInt32(p_objXmlDocOut.SelectSingleNode("//PageNumber").InnerText);                              
                //MGaba2:MITS 18913:End
                //End by Shivendu for MITS 18098
               
                if (p_objXmlDocOut.SelectSingleNode("//UserName") != null)
                    outDocument.UserName = p_objXmlDocOut.SelectSingleNode("//UserName").InnerText;
                if (p_objXmlDocOut.SelectSingleNode("//PrintOrder1") != null)
                    outDocument.PrintOrder1 = p_objXmlDocOut.SelectSingleNode("//PrintOrder1").InnerText;
                if (p_objXmlDocOut.SelectSingleNode("//PrintOrder2") != null)
                    outDocument.PrintOrder2 = p_objXmlDocOut.SelectSingleNode("//PrintOrder2").InnerText;
                if (p_objXmlDocOut.SelectSingleNode("//PrintOrder3") != null)
                    outDocument.PrintOrder3 = p_objXmlDocOut.SelectSingleNode("//PrintOrder3").InnerText;
                //Parijat: start Post Editable Enhanced Notes 
                if (p_objXmlDocOut.SelectSingleNode("//TimeLimit") != null)
                    outDocument.EnhancedTimeLimit = p_objXmlDocOut.SelectSingleNode("//TimeLimit").InnerText;
                if (p_objXmlDocOut.SelectSingleNode("//EnhNoteEditRight") != null)
                    outDocument.UserEditingRights = Convert.ToBoolean(p_objXmlDocOut.SelectSingleNode("//EnhNoteEditRight").InnerText);
                //Parijat: end Post Editable Enhanced Notes 
                //williams--neha goel:start--for filtring the notes code type on tne basis of parent:MITS 21704
                if (p_objXmlDocOut.SelectSingleNode("//NotesParentCodeId") != null)
                    outDocument.CodeId = p_objXmlDocOut.SelectSingleNode("//NotesParentCodeId").InnerText;
                //williams--neha goel:end:MITS 21704
                
                //tanwar2 - Print Html Notes - start
                if (!string.IsNullOrEmpty(objDocument.NoteTypeCodeId))
                {
                    if (outDocument.TotalNumberOfPages > 1)
                    {
                        outDocument.PrintMultiplePages = false;
                    }
                    else
                    {
                        outDocument.PrintMultiplePages = true;
                    }
                }
                else
                {
                    outDocument.PrintMultiplePages = objDocument.PrintMultiplePages;
                }
                //tanwar2 - Print Html Notes - end

                if(p_objXmlDocOut.GetElementsByTagName("ProgressNote").Count >0)
                {
                    for (int x = 0; x < p_objXmlDocOut.GetElementsByTagName("ProgressNote").Count; x++)
                    {

                        XmlElement xmlNode = (XmlElement)p_objXmlDocOut.GetElementsByTagName("ProgressNote")[x];

                        //tanwar2 - Print Html Notes - start
                        //if (bPrintSelectedNotes)
                        if (bPrintSelectedNotes && !bIsDeleteIntegrate) //Mits id:34851(Smoke issue) - Govind
                        {
                            if (Conversion.ConvertStrToInteger(xmlNode.SelectSingleNode("ClaimProgressNoteId").InnerText) != iClaimProgressNoteId)
                            {
                                continue;
                            }
                            outDocument.PrintMultiplePages = true;
                        }
                        //tanwar2 - Print Html Notes - end

                        ProgressNote prgNote = new ProgressNote();
                        prgNote.ClaimProgressNoteId =Conversion.ConvertStrToInteger( xmlNode.SelectSingleNode("ClaimProgressNoteId").InnerText);
                        prgNote.AttachedTo = xmlNode.SelectSingleNode("AttachedTo").InnerText;
                        prgNote.EnteredBy = xmlNode.SelectSingleNode("EnteredBy").InnerText;
                        prgNote.EnteredByName = xmlNode.SelectSingleNode("EnteredByName").InnerText;
                        prgNote.DateEntered = xmlNode.SelectSingleNode("DateEntered").InnerText;
                        prgNote.DateEnteredForSort = xmlNode.SelectSingleNode("DateEnteredForSort").InnerText;
                        prgNote.DateCreated = xmlNode.SelectSingleNode("DateCreated").InnerText;
                        prgNote.TimeCreated = xmlNode.SelectSingleNode("TimeCreated").InnerText;
                        prgNote.TimeCreatedForSort = xmlNode.SelectSingleNode("TimeCreatedForSort").InnerText;
                        prgNote.NoteTypeCode = xmlNode.SelectSingleNode("NoteTypeCode").InnerText;
                        prgNote.NoteTypeCodeId = xmlNode.SelectSingleNode("NoteTypeCode").Attributes["CodeId"].Value;
                        prgNote.UserTypeCode = xmlNode.SelectSingleNode("UserTypeCode").InnerText;
                        prgNote.UserTypeCodeId = xmlNode.SelectSingleNode("UserTypeCode").Attributes["CodeId"].Value; 
                        prgNote.NoteMemo = xmlNode.SelectSingleNode("NoteMemo").InnerText;
                        prgNote.NoteMemoCareTech = xmlNode.SelectSingleNode("NoteMemoCareTech").InnerText;
                        prgNote.IsNoteEditable = xmlNode.SelectSingleNode("IsNoteEditable").InnerText;
                        prgNote.Subject = xmlNode.SelectSingleNode("Subject").InnerText; //zmohammad MITS 30218
                        
                   
                        //Added Rakhi for R6-Progress Notes Templates
                        if(xmlNode.SelectSingleNode("TemplateId")!=null)
                        {
                            if (xmlNode.SelectSingleNode("TemplateId").InnerText == "")
                                prgNote.TemplateID = 0;
                            else
                                prgNote.TemplateID = Convert.ToInt32(xmlNode.SelectSingleNode("TemplateId").InnerText);
                        }
                        if(xmlNode.SelectSingleNode("TemplateName")!=null)
                            prgNote.TemplateName = xmlNode.SelectSingleNode("TemplateName").InnerText;
                        //Added Rakhi for R6-Progress Notes Templates
                        if (prgNote.NoteMemoCareTech.Length > 33)//Range changed by Shivendu for MITS 19164
                            prgNote.NoteMemoTrimmed = Utilities.AddHTMLTags(prgNote.NoteMemoCareTech.Substring(0, 30) + "...");
                        //prgNote.NoteMemoTrimmed = (prgNote.NoteMemoCareTech.Substring(0, 30) + "...").Replace("&", "&amp;").Replace(@"<", "&lt;").Replace(@">", "&gt;");
                        else
                            //prgNote.NoteMemoTrimmed = prgNote.NoteMemoCareTech.Replace("&", "&amp;").Replace(@"<", "&lt;").Replace(@">", "&gt;");
                            prgNote.NoteMemoTrimmed = Utilities.AddHTMLTags(prgNote.NoteMemoCareTech);

                        prgNote.AdjusterName = xmlNode.SelectSingleNode("AdjusterName").InnerText;
                        if (xmlNode.SelectSingleNode("ClaimNumber") != null)
                        {
                            prgNote.ClaimNumber = xmlNode.SelectSingleNode("ClaimNumber").InnerText;
                            //MGaba2:MITS 24066:On deleting claim notes,event notes were showing up.                            
                            if (xmlNode.SelectSingleNode("ClaimNumber").Attributes["Id"] != null)
                            {
                                prgNote.ClaimID = xmlNode.SelectSingleNode("ClaimNumber").Attributes["Id"].Value;
                            }
                        }
                        if (xmlNode.SelectSingleNode("EventNumber") != null)
                        {
                            prgNote.EventNumber = xmlNode.SelectSingleNode("EventNumber").InnerText;
                            //MGaba2:MITS 24066:On deleting claim notes,event notes were showing up.
                            if (xmlNode.SelectSingleNode("EventNumber").Attributes["Id"] != null)
                            {
                                prgNote.EventID = xmlNode.SelectSingleNode("EventNumber").Attributes["Id"].Value;
                            }
                        }
                        //williams: Added neha goel for R6-Enhanced Notes Policy--start:MITS 21704
                        if (xmlNode.SelectSingleNode("PolicyId") != null)
                        {
                            if (xmlNode.SelectSingleNode("PolicyId").InnerText == "")
                                prgNote.PolicyID = "";
                            else
                                prgNote.PolicyID = xmlNode.SelectSingleNode("PolicyId").InnerText;
                        }
                        if (xmlNode.SelectSingleNode("PolicyName") != null)
                            prgNote.PolicyName = xmlNode.SelectSingleNode("PolicyName").InnerText;
                        if (xmlNode.SelectSingleNode("PolicyNumber") != null)
                            prgNote.PolicyNumber = xmlNode.SelectSingleNode("PolicyNumber").InnerText;
                        //if (xmlNode.SelectSingleNode("CodeId") != null)
                          //  prgNote.CodeId = xmlNode.SelectSingleNode("CodeId").InnerText;
                        //williams-Added neha goel--end:MITS 21704
                        //skhare7 27285
                        outDocument.objProgressNoteList.Add(prgNote);
                        if(xmlNode.SelectSingleNode("PIName")!=null)
                        outDocument.PIName = xmlNode.SelectSingleNode("PIName").InnerText;
  
                    }
                }
               
                outDocument.ActivateFilter = true;

                //willimas--neha goel----start:policy enhanced notes permissions,added if else for policy via calim and event
                if (!string.IsNullOrEmpty(objDocument.PolicyID.ToString()) && objDocument.PolicyID != 0)
                {
                    if (!(base.userLogin.IsAllowedEx(POLICY_PROGNOTES_CREATE)))
                        outDocument.bPolicyTrackingCreatePermission = false;
                    else
                        outDocument.bPolicyTrackingCreatePermission = true;

                    if (!(base.userLogin.IsAllowedEx(POLICY_PROGNOTES_DELETE)))
                        outDocument.bPolicyTrackingDeletePermission = false;
                    else
                        outDocument.bPolicyTrackingDeletePermission = true;

                    if (!(base.userLogin.IsAllowedEx(POLICY_PROGNOTES_EDIT)))
                        outDocument.bPolicyTrackingEditPermission = false;
                    else
                        outDocument.bPolicyTrackingEditPermission = true;

                    if (!(base.userLogin.IsAllowedEx(POLICY_PROGNOTES_PRINT)))
                        outDocument.bPolicyTrackingPrintPermission = false;
                    else
                        outDocument.bPolicyTrackingPrintPermission = true;

                    if (!(base.userLogin.IsAllowedEx(POLICY_PROGNOTES_VIEW_ALL_NOTES)))
                        outDocument.bPolicyTrackingViewAllNotesPermission = false;
                    else
                        outDocument.bPolicyTrackingViewAllNotesPermission = true;
                   
                }
                //willimas--neha goel----end
                else{
                    //pmittal5 Mits 21514 07/19/10-williams retrofit neha goel--start:MITS 21704    
                    //Changed by Gagan for MITS 15503 : Start
	                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_CREATE)))
	                //    outDocument.bCreatePermission = false;
	                //else
	                //    outDocument.bCreatePermission = true;                    

	                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_DELETE)))
	                //    outDocument.bDeletePermission = false;
	                //else
	                //    outDocument.bDeletePermission = true;                                   

	                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_EDIT)))
	                //    outDocument.bEditPermission  = false;
	                //else
	                //    outDocument.bEditPermission = true;               
	                    
	                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_PRINT)))
	                //    outDocument.bPrintPermission = false;
	                //else
	                //    outDocument.bPrintPermission = true;

	                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_VIEW_ALL_NOTES)))
	                //    outDocument.bViewAllNotesPermission = false;
	                //else
	                //    outDocument.bViewAllNotesPermission = true;                              
	                    
	                ////Changed by Gagan for MITS 15503 : End
	                ////rsushilaggar MITS 21119 06/18/2010
	                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_TEMPLATES)))
	                //    outDocument.bTemplatesPermission = false;
	                //else
	                //    outDocument.bTemplatesPermission = true;                              

	                //MGaba2:MITS 22173: Separate branch for Templates
	                if (!(base.userLogin.IsAllowedEx(TEMPLATES)))
	                    outDocument.bTemplatesPermission = false;
	                else
	                    outDocument.bTemplatesPermission = true;
	                
                    /*Added by gbindra MITS#34104 WWIG GAP15 START*/
                    if (objDocument.ClaimantId > 0 && objSysSettings.AllowNotesAtClaimant == true)
                    {
                        if (objSysSettings.AllowNotesAtClaimant == true)
                        {
                            switch (objDocument.LOB)
                            {
                                case "GC":
                                    if (!(base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                                        outDocument.bCreatePermission = false;
                                    else
                                        outDocument.bCreatePermission = true;

                                    if (!(base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
                                        outDocument.bDeletePermission = false;
                                    else
                                        outDocument.bDeletePermission = true;

                                    if (!(base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                                        outDocument.bEditPermission = false;
                                    else
                                        outDocument.bEditPermission = true;

                                    if (!(base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
                                        outDocument.bPrintPermission = false;
                                    else
                                        outDocument.bPrintPermission = true;

                                    if (!(base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_VIEWALL_OFFSET)))
                                        outDocument.bViewAllNotesPermission = false;
                                    else
                                        outDocument.bViewAllNotesPermission = true;
                                    break;
                                case "VA":
                                    if (!(base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                                        outDocument.bCreatePermission = false;
                                    else
                                        outDocument.bCreatePermission = true;

                                    if (!(base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
                                        outDocument.bDeletePermission = false;
                                    else
                                        outDocument.bDeletePermission = true;

                                    if (!(base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                                        outDocument.bEditPermission = false;
                                    else
                                        outDocument.bEditPermission = true;

                                    if (!(base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
                                        outDocument.bPrintPermission = false;
                                    else
                                        outDocument.bPrintPermission = true;

                                    if (!(base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_VIEWALL_OFFSET)))
                                        outDocument.bViewAllNotesPermission = false;
                                    else
                                        outDocument.bViewAllNotesPermission = true;
                                    break;
                                case "PC":
                                    if (!(base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                                        outDocument.bCreatePermission = false;
                                    else
                                        outDocument.bCreatePermission = true;

                                    if (!(base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
                                        outDocument.bDeletePermission = false;
                                    else
                                        outDocument.bDeletePermission = true;

                                    if (!(base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                                        outDocument.bEditPermission = false;
                                    else
                                        outDocument.bEditPermission = true;

                                    if (!(base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
                                        outDocument.bPrintPermission = false;
                                    else
                                        outDocument.bPrintPermission = true;

                                    if (!(base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_VIEWALL_OFFSET)))
                                        outDocument.bViewAllNotesPermission = false;
                                    else
                                        outDocument.bViewAllNotesPermission = true;
                                    break;
                            }
                        }
                        else
                        {
                            outDocument.bCreatePermission = false;
                            outDocument.bDeletePermission = false;
                            outDocument.bEditPermission = false;
                            outDocument.bPrintPermission = false;
                            outDocument.bViewAllNotesPermission = false;
                        }
                    }
                    else if (objDocument.EventID != 0 && objDocument.ClaimID == 0)  //Event's Enhanced notes are opening
	                {
	                    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_CREATE)))
	                    outDocument.bCreatePermission = false;
	                else
	                    outDocument.bCreatePermission = true;                    

	                    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_DELETE)))
	                    outDocument.bDeletePermission = false;
	                else
	                    outDocument.bDeletePermission = true;                                   

	                    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_EDIT)))
	                    outDocument.bEditPermission  = false;
	                else
	                    outDocument.bEditPermission = true;               
	                    
	                    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_PRINT)))
	                    outDocument.bPrintPermission = false;
	                else
	                    outDocument.bPrintPermission = true;

	                    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_VIEW_ALL_NOTES)))
	                    outDocument.bViewAllNotesPermission = false;
	                else
	                    outDocument.bViewAllNotesPermission = true;                              
	                    
	                    //MGaba2:MITS 22173: Separate branch for Templates
	                    //if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_TEMPLATES)))
	                    //    outDocument.bTemplatesPermission = false;
	                    //else
	                    //    outDocument.bTemplatesPermission = true;
	                }
	                else if (objDocument.ClaimID != 0)  //Claim's Enhanced notes are opening
	                {
	                    switch (objDocument.LOB)
	                    {
	                        case "GC":
	                            if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
	                                outDocument.bCreatePermission = false;
	                            else
	                                outDocument.bCreatePermission = true;

	                            if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
	                                outDocument.bDeletePermission = false;
	                            else
	                                outDocument.bDeletePermission = true;

	                            if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
	                                outDocument.bEditPermission = false;
	                            else
	                                outDocument.bEditPermission = true;

	                            if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
	                                outDocument.bPrintPermission = false;
	                            else
	                                outDocument.bPrintPermission = true;

	                            if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_VIEWALL_OFFSET)))
	                                outDocument.bViewAllNotesPermission = false;
	                            else
	                                outDocument.bViewAllNotesPermission = true;
	                            //MGaba2:MITS 22173: Separate branch for Templates
	                            //if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_TEMPLATES_OFFSET)))
	                            //    outDocument.bTemplatesPermission = false;
	                            //else
	                            //    outDocument.bTemplatesPermission = true;

	                            break;
	                        case "VA":
	                            if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
	                                outDocument.bCreatePermission = false;
	                            else
	                                outDocument.bCreatePermission = true;

	                            if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
	                                outDocument.bDeletePermission = false;
	                            else
	                                outDocument.bDeletePermission = true;

	                            if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
	                                outDocument.bEditPermission = false;
	                            else
	                                outDocument.bEditPermission = true;

	                            if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
	                                outDocument.bPrintPermission = false;
	                            else
	                                outDocument.bPrintPermission = true;

	                            if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_VIEWALL_OFFSET)))
	                                outDocument.bViewAllNotesPermission = false;
	                            else
	                                outDocument.bViewAllNotesPermission = true;
	                            //MGaba2:MITS 22173: Separate branch for Templates
	                            //if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_TEMPLATES_OFFSET)))
	                            //    outDocument.bTemplatesPermission = false;
	                            //else
	                            //    outDocument.bTemplatesPermission = true;
	                            break;

	                        case "WC":
	                            if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
	                                outDocument.bCreatePermission = false;
	                            else
	                                outDocument.bCreatePermission = true;

	                            if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
	                                outDocument.bDeletePermission = false;
	                            else
	                                outDocument.bDeletePermission = true;

	                            if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
	                                outDocument.bEditPermission = false;
	                            else
	                                outDocument.bEditPermission = true;

	                            if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
	                                outDocument.bPrintPermission = false;
	                            else
	                                outDocument.bPrintPermission = true;

	                            if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_VIEWALL_OFFSET)))
	                                outDocument.bViewAllNotesPermission = false;
	                            else
	                                outDocument.bViewAllNotesPermission = true;
	                            //MGaba2:MITS 22173: Separate branch for Templates
	                            //if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_TEMPLATES_OFFSET)))
	                            //    outDocument.bTemplatesPermission = false;
	                            //else
	                            //    outDocument.bTemplatesPermission = true;
	                            break;

	                        case "DI":
	                            if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
	                                outDocument.bCreatePermission = false;
	                            else
	                                outDocument.bCreatePermission = true;

	                            if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
	                                outDocument.bDeletePermission = false;
	                            else
	                                outDocument.bDeletePermission = true;

	                            if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
	                                outDocument.bEditPermission = false;
	                            else
	                                outDocument.bEditPermission = true;

	                            if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
	                                outDocument.bPrintPermission = false;
	                            else
	                                outDocument.bPrintPermission = true;

	                            if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_VIEWALL_OFFSET)))
	                                outDocument.bViewAllNotesPermission = false;
	                            else
	                                outDocument.bViewAllNotesPermission = true;
	                            //MGaba2:MITS 22173: Separate branch for Templates
	                            //if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_TEMPLATES_OFFSET)))
	                            //    outDocument.bTemplatesPermission = false;
	                            //else
	                            //    outDocument.bTemplatesPermission = true;
	                            break;

	                        case "PC":
	                            if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
	                                outDocument.bCreatePermission = false;
	                            else
	                                outDocument.bCreatePermission = true;

	                            if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
	                                outDocument.bDeletePermission = false;
	                            else
	                                outDocument.bDeletePermission = true;

	                            if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
	                                outDocument.bEditPermission = false;
	                            else
	                                outDocument.bEditPermission = true;

	                            if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
	                                outDocument.bPrintPermission = false;
	                            else
	                                outDocument.bPrintPermission = true;

	                            if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_VIEWALL_OFFSET)))
	                                outDocument.bViewAllNotesPermission = false;
	                            else
	                                outDocument.bViewAllNotesPermission = true;
	                            //MGaba2:MITS 22173: Separate branch for Templates
	                            //if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_TEMPLATES_OFFSET)))
	                            //    outDocument.bTemplatesPermission = false;
	                            //else
	                            //    outDocument.bTemplatesPermission = true;
	                            break;
	                    }
	                }
	                //End - pmittal5
                }                
        	}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add (p_objException, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add (p_objException,Globalization.GetString ("ProgressNotesAdaptor.OnLoad.Error",base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally 
			{
				
				if( objProgressNotesManager != null )
				{
                    objProgressNotesManager.Dispose();
					objProgressNotesManager = null ;
				}	
			}
	
			return (true);
        }
        //rsolanki2: Enhc Notes ajax updates
        public bool OnLoadIntegratePartial(ProgressNotesType objDocument, out ProgressNotesType outDocument, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sFilterSQL = "";
            outDocument = new ProgressNotesType();
            ProgressNotesManager objProgressNotesManager = null;
			DbReader objReader = null;
            int iNotesCount = 0;
            string sSQL = string.Empty;            
            bool bSuccess=false;
            LocalCache objCache = null;
            SysSettings objSysSetting = null; //added by gbindra MITS#34104 WWIG GAP15
            try
            {
                objSysSetting = new SysSettings(m_connectionString, base.ClientId); //Ash - cloud; //added by gbindra MITS#34104 WWIG GAP15
			//mbahl3 mits 30513
                //MITS 28450:Opening Enhanced Notes from child screens of claim : In case of children of Case mgt,we do not have claim id
                if (objDocument.CaseMgtRowID != 0)
                {
                    if (objDocument.FormName == "CaseMgrNotes" )
                    {
                        if (objDocument.CaseMgrParent == "event")
                        {
                            sSQL = "SELECT PERSON_INVOLVED.EVENT_ID FROM CASE_MANAGEMENT,PERSON_INVOLVED,CM_X_CMGR_HIST WHERE  CM_X_CMGR_HIST.CASEMGT_ROW_ID = CASE_MANAGEMENT.CASEMGT_ROW_ID  AND CM_X_CMGR_HIST.CMCMH_ROW_ID= " + objDocument.CaseMgtRowID + "  AND  CASE_MANAGEMENT.PI_ROW_ID =PERSON_INVOLVED.PI_ROW_ID";
                            objDocument.EventID = Conversion.CastToType<Int32>(DbFactory.ExecuteScalar(this.connectionString, sSQL).ToString(), out bSuccess);
                        }
                        else if (objDocument.CaseMgrParent == "claim")
                        {
                            sSQL = "SELECT CASE_MANAGEMENT.CLAIM_ID FROM CASE_MANAGEMENT,CM_X_CMGR_HIST WHERE   CM_X_CMGR_HIST.CASEMGT_ROW_ID = CASE_MANAGEMENT.CASEMGT_ROW_ID AND  CM_X_CMGR_HIST.CMCMH_ROW_ID  = " + objDocument.CaseMgtRowID;
                            objDocument.ClaimID = Conversion.CastToType<Int32>(DbFactory.ExecuteScalar(this.connectionString, sSQL).ToString(), out bSuccess);
                        }
                    }

                    if (objDocument.FormName.ToLower() == "casemanagement" || objDocument.FormName == "CmXAccommodation" || objDocument.FormName == "CmXVocrehab" || objDocument.FormName == "CmXCmgrHist")
                    {
                        if (objDocument.CaseMgrParent == "event")
                        {
                            sSQL = "SELECT PERSON_INVOLVED.EVENT_ID FROM CASE_MANAGEMENT,PERSON_INVOLVED WHERE CASE_MANAGEMENT.PI_ROW_ID =PERSON_INVOLVED.PI_ROW_ID AND CASEMGT_ROW_ID = " + objDocument.CaseMgtRowID;
                            objDocument.EventID = Conversion.CastToType<Int32>(DbFactory.ExecuteScalar(this.connectionString, sSQL).ToString(), out bSuccess);
                        }
                        else if (objDocument.CaseMgrParent == "claim")
                        {
                            sSQL = "SELECT CLAIM_ID FROM CASE_MANAGEMENT WHERE CASEMGT_ROW_ID = " + objDocument.CaseMgtRowID;
                            objDocument.ClaimID = Conversion.CastToType<Int32>(DbFactory.ExecuteScalar(this.connectionString, sSQL).ToString(), out bSuccess);
                        }
                    }

                }
                if (objDocument.DemanOfferParentId != 0 && (objDocument.FormName == "demandoffer" ))
                {
                    if (string.Equals(objDocument.DemanOfferParent,"pimedstaff",StringComparison.InvariantCultureIgnoreCase) || string.Equals(objDocument.DemanOfferParent,"pidriver",StringComparison.InvariantCultureIgnoreCase)  ||string.Equals(objDocument.DemanOfferParent, "piemployee",StringComparison.InvariantCultureIgnoreCase)
                          || string.Equals(objDocument.DemanOfferParent, "PiInjury", StringComparison.InvariantCultureIgnoreCase) || string.Equals(objDocument.DemanOfferParent, "piother", StringComparison.InvariantCultureIgnoreCase) || string.Equals(objDocument.DemanOfferParent, "pipatient", StringComparison.InvariantCultureIgnoreCase) 
                          || string.Equals(objDocument.DemanOfferParent, "piphysician",StringComparison.InvariantCultureIgnoreCase)  || string.Equals(objDocument.DemanOfferParent,"piwitness",StringComparison.InvariantCultureIgnoreCase) )
                    {
                        sSQL = "SELECT PERSON_INVOLVED.EVENT_ID FROM DEMAND_OFFER,PERSON_INVOLVED WHERE DEMAND_OFFER.PARENT_ID =PERSON_INVOLVED.PI_ROW_ID AND  DEMAND_OFFER.PARENT_ID =" + objDocument.DemanOfferParentId + " AND PARENT_NAME = '" + objDocument.DemanOfferParent+"'";
                        objDocument.EventID = Conversion.CastToType<Int32>(DbFactory.ExecuteScalar(this.connectionString, sSQL).ToString(), out bSuccess);
                    }
				//mbahl3 mits 34156
                    else if (string.Equals(objDocument.DemanOfferParent, "claimxsubrogation", StringComparison.InvariantCultureIgnoreCase) 
                         ||  string.Equals(objDocument.DemanOfferParent, "claimxlitigation",StringComparison.InvariantCultureIgnoreCase)
                         || string.Equals(objDocument.DemanOfferParent, "unitxclaim", StringComparison.InvariantCultureIgnoreCase) || string.Equals(objDocument.DemanOfferParent, "claimxpropertyloss", StringComparison.InvariantCultureIgnoreCase))
                         
                    {
                        switch (objDocument.DemanOfferParent.ToLower())
                        {
                            case "unitxclaim":
                                sSQL = "SELECT CLAIM_ID FROM UNIT_X_CLAIM,DEMAND_OFFER WHERE DEMAND_OFFER.PARENT_ID = UNIT_ROW_ID AND DEMAND_OFFER.PARENT_ID = " + objDocument.DemanOfferParentId + " AND PARENT_NAME = '" + objDocument.DemanOfferParent + "'";
                                
                                break;
                            case "claimxlitigation":
                                sSQL = "SELECT CLAIM_ID FROM CLAIM_X_LITIGATION,DEMAND_OFFER WHERE DEMAND_OFFER.PARENT_ID = LITIGATION_ROW_ID AND DEMAND_OFFER.PARENT_ID = " + objDocument.DemanOfferParentId + " AND PARENT_NAME = '" + objDocument.DemanOfferParent + "'";
                                
                                break;
                            case "claimxpropertyloss":
                                sSQL = "SELECT CLAIM_ID FROM CLAIM_X_PROPERTYLOSS,DEMAND_OFFER WHERE DEMAND_OFFER.PARENT_ID =ROW_ID AND DEMAND_OFFER.PARENT_ID = " + objDocument.DemanOfferParentId + " AND PARENT_NAME = '" + objDocument.DemanOfferParent + "'";
                                
                                break;
                            case "claimxsubrogation":
                                sSQL = "SELECT CLAIM_ID FROM CLAIM_X_SUBRO,DEMAND_OFFER WHERE SUBROGATION_ROW_ID =DEMAND_OFFER.PARENT_ID AND  DEMAND_OFFER.PARENT_ID = " + objDocument.DemanOfferParentId + " AND PARENT_NAME = '" + objDocument.DemanOfferParent + "'";
                                
                                break;

                        }
                        objDocument.ClaimID = Conversion.CastToType<Int32>(DbFactory.ExecuteScalar(this.connectionString, sSQL).ToString(), out bSuccess);
                    }
                    /*gbindra   JIRA RMA-185 changes on 06/10/2014*/
                    else if(string.Equals(objDocument.DemanOfferParent, "Claimant", StringComparison.InvariantCultureIgnoreCase))
                    {
                        sSQL = "SELECT CLAIM_ID, CLAIMANT_ROW_ID FROM CLAIMANT,DEMAND_OFFER WHERE DEMAND_OFFER.PARENT_ID = CLAIMANT_ROW_ID AND DEMAND_OFFER.PARENT_ID = " + objDocument.DemanOfferParentId + " AND PARENT_NAME = '" + objDocument.DemanOfferParent + "'";
                        
                        using (objReader = DbFactory.ExecuteReader(this.connectionString, sSQL))
                        {
                            if (objReader.Read())
                            {
                                objCache = new LocalCache(this.connectionString, base.ClientId);
                                objDocument.ClaimID = Conversion.CastToType<int>(objReader.GetValue("CLAIM_ID").ToString(), out bSuccess);
                                objDocument.ClaimantId = Conversion.CastToType<int>(objReader.GetValue("CLAIMANT_ROW_ID").ToString(), out bSuccess);
                            }
                        }
                    }
                    /*gbindra   JIRA#RMA-185 changes on 06/10/2014 END*/
                }
				//mbahl3 mits 30513
                if (objDocument.bIsMobileAdjuster || objDocument.bIsMobilityAdjuster)
                {
                    //MITS 28450: Declaring the variable at the top
					//string sSQL = "SELECT CLAIM_ID,EVENT_ID FROM CLAIM WHERE CLAIM_NUMBER='" + objDocument.ClaimNumber + "'";
                    sSQL = "SELECT CLAIM_ID,EVENT_ID FROM CLAIM WHERE CLAIM_NUMBER='" + objDocument.ClaimNumber + "'";
					using (objReader = DbFactory.ExecuteReader(this.connectionString, sSQL))
					{
						if (objReader.Read())
						{
							objDocument.ClaimID = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), base.ClientId);
							objDocument.EventID = Conversion.ConvertObjToInt(objReader.GetValue("EVENT_ID"), base.ClientId);

						}
					}
				}
                 //willimas--neha goel----start:policy enhanced notes permissions:added if else
                if (!string.IsNullOrEmpty(objDocument.PolicyID.ToString()) && objDocument.PolicyID != 0)
                {
                    if (!(base.userLogin.IsAllowedEx(POLICY_PROGNOTES_VIEW)))
                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, POLICY_PROGNOTES_VIEW);
                }
                //willimas--neha goel----end:policy enhanced notes permissions
                else
                {
	                //pmittal5 Mits 21514 07/16/10 - Enhanced notes separated under LOBs and Events
	                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_VIEW)))
	                //    throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, PROGNOTES_VIEW);
                    /*Added by gbindra MITS#34104 WWIG GAP 15 START*/
                    if (objDocument.ClaimantId > 0 && objSysSetting.AllowNotesAtClaimant == true)
                    {

                        sSQL = "SELECT LINE_OF_BUS_CODE, EVENT_ID, EVENT_NUMBER FROM CLAIM WHERE CLAIM_ID = " + objDocument.ClaimID;
                        using (objReader = DbFactory.ExecuteReader(this.connectionString, sSQL))
                        {
                            if (objReader.Read())
                            {
                                objCache = new LocalCache(this.connectionString, base.ClientId);
                                objDocument.EventID = Conversion.CastToType<int>(objReader.GetValue("EVENT_ID").ToString(), out bSuccess);
                                objDocument.LOB = objCache.GetShortCode(Conversion.CastToType<int>(objReader.GetValue("LINE_OF_BUS_CODE").ToString(), out bSuccess));
                                objDocument.EventNumber = objReader.GetValue("EVENT_NUMBER").ToString();
                            }
                        }


                        switch (objDocument.LOB)
                        {
                            case "GC":
                                if (!(base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW)))
                                    throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, GC_CLMNT_PROGNOTES_VIEW);
                                break;
                            case "VA":
                                if (!(base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW)))
                                    throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, VA_CLMNT_PROGNOTES_VIEW);
                                break;
                            case "PC":
                                if (!(base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW)))
                                    throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, PC_CLMNT_PROGNOTES_VIEW);
                                break;
                        }
                    }
                    else if (objDocument.EventID != 0 && objDocument.ClaimID == 0)  //Event's Enhanced notes are opening
	                {
	                    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_VIEW)))
	                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, EVENT_PROGNOTES_VIEW);
                        //mbahl3 mits 30513 enhance notes enhancement 
                        if (objDocument.FormName == "pimedstaff" || objDocument.FormName == "pidriver" || objDocument.FormName == "piemployee"
                         || objDocument.FormName == "PiInjury" || objDocument.FormName == "piother" || objDocument.FormName == "pipatient"
                         || objDocument.FormName == "piphysician" || objDocument.FormName == "piwitness"
                     || objDocument.FormName.ToLower() == "casemanagement" || objDocument.FormName == "CmXAccommodation" || objDocument.FormName == "CmXVocrehab" || objDocument.FormName == "CmXCmgrHist"
                            || objDocument.FormName == "demandoffer"
                            || objDocument.FormName == "CaseMgrNotes" 
                            )
                        {
                            sSQL = "SELECT EVENT_ID,EVENT_NUMBER FROM CLAIM WHERE EVENT_ID = " + objDocument.EventID;
                            using (objReader = DbFactory.ExecuteReader(this.connectionString, sSQL))
                            {
                                if (objReader.Read())
                                {
                                    objCache = new LocalCache(this.connectionString, base.ClientId);

                                    objDocument.EventID = Conversion.ConvertObjToInt(objReader.GetValue("EVENT_ID"), base.ClientId);
                                    objDocument.EventNumber =Conversion.ConvertObjToStr( objReader.GetValue("EVENT_NUMBER"));

                                }
                            }       
                        }
                        //mbahl3 mits 30513 enhance notes enhancement 
	                }

                        //vsharma205 Starts-Jira-18403
                    else if (objDocument.EventID != 0 && objDocument.ClaimID != 0)  
                    {
                        if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_VIEW)))
                            throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, EVENT_PROGNOTES_VIEW);
                        //mbahl3 mits 30513 enhance notes enhancement 
                        if (objDocument.FormName == "pimedstaff" || objDocument.FormName == "pidriver" || objDocument.FormName == "piemployee"
                         || objDocument.FormName == "PiInjury" || objDocument.FormName == "piother" || objDocument.FormName == "pipatient"
                         || objDocument.FormName == "piphysician" || objDocument.FormName == "piwitness"
                     || objDocument.FormName.ToLower() == "casemanagement" || objDocument.FormName == "CmXAccommodation" || objDocument.FormName == "CmXVocrehab" || objDocument.FormName == "CmXCmgrHist"
                            || objDocument.FormName == "demandoffer"
                            || objDocument.FormName == "CaseMgrNotes"
                            )
                        {
                            sSQL = "SELECT EVENT_ID,EVENT_NUMBER FROM CLAIM WHERE EVENT_ID = " + objDocument.EventID;
                            using (objReader = DbFactory.ExecuteReader(this.connectionString, sSQL))
                            {
                                if (objReader.Read())
                                {
                                    objCache = new LocalCache(this.connectionString, base.ClientId);

                                    objDocument.EventID = Conversion.ConvertObjToInt(objReader.GetValue("EVENT_ID"), base.ClientId);
                                    objDocument.EventNumber = Conversion.ConvertObjToStr(objReader.GetValue("EVENT_NUMBER"));

                                }
                            }
                        }
                        //mbahl3 mits 30513 enhance notes enhancement 
                    }
                        //vsharma205 Ends-Jira-

	                else if (objDocument.ClaimID != 0)  //Claim's Enhanced notes are opening
	                {
                        //MITS 28450:Need LOB for module security check. In case,Enh Notes are opened from child screens of claim.
                        //Also getting event id and event number which will be used to populate Attached to drop down
                        // akaushik5 Changed for MITS 31973 Starts
                        //if (objDocument.FormName == "subrogation" || objDocument.FormName == "adjuster" || objDocument.FormName == "claimant" || objDocument.FormName == "defendant" ||
                        //    objDocument.FormName == "litigation" || objDocument.FormName == "liabilityloss" || objDocument.FormName == "arbitration" || objDocument.FormName == "unit" ||
                        //    objDocument.FormName == "propertyloss" || objDocument.FormName == "CmXAccommodation" || objDocument.FormName == "CmXVocrehab" || objDocument.FormName == "CmXCmgrHist" ||
                        //    objDocument.FormName == "ReserveListing" || objDocument.FormName == "funds" || objDocument.FormName == "autoclaimchecks" || objDocument.FormName == "ReserveListingBOB")
                        if (objDocument.FormName == "subrogation" || objDocument.FormName == "adjuster" || objDocument.FormName == "claimant" 
                            || objDocument.FormName == "defendant" || objDocument.FormName == "litigation" || objDocument.FormName == "liabilityloss" 
                            || objDocument.FormName == "arbitration" || objDocument.FormName == "unit" || objDocument.FormName == "propertyloss" 
                           ||objDocument.FormName.ToLower() == "casemanagement" || objDocument.FormName == "CmXAccommodation" || objDocument.FormName == "CmXVocrehab" || objDocument.FormName == "CmXCmgrHist"
                            || objDocument.FormName == "demandoffer"
                            || objDocument.FormName == "ReserveListing" || objDocument.FormName == "funds" || objDocument.FormName == "autoclaimchecks"
                            || objDocument.FormName == "ReserveListingBOB" || objDocument.FormName.Equals("salvage"))
                        // akaushik5 Changed for MITS 31973 Ends
                        {                            
                            sSQL = "SELECT LINE_OF_BUS_CODE,EVENT_ID,EVENT_NUMBER FROM CLAIM WHERE CLAIM_ID = " + objDocument.ClaimID;
                            using (objReader = DbFactory.ExecuteReader(this.connectionString, sSQL))
                            {
                                if (objReader.Read())
                                {
                                    objCache = new LocalCache(this.connectionString, base.ClientId);
                                    
                                    objDocument.EventID = Conversion.CastToType<Int32>(objReader.GetValue("EVENT_ID").ToString(), out bSuccess);
                                    objDocument.LOB = objCache.GetShortCode(Conversion.CastToType<Int32>(objReader.GetValue("LINE_OF_BUS_CODE").ToString(), out bSuccess));
                                    objDocument.EventNumber = objReader.GetValue("EVENT_NUMBER").ToString();
                                    
                                }
                            }                          
                        }
                            switch (objDocument.LOB)
                            {
                                case "GC":
                                    if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW)))
                                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, GC_PROGNOTES_VIEW);
                                    break;
                                case "VA":
                                    if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW)))
                                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, VA_PROGNOTES_VIEW);
                                    break;
                                case "WC":
                                    if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW)))
                                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, WC_PROGNOTES_VIEW);
                                    break;
                                case "DI":
                                    if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW)))
                                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, DI_PROGNOTES_VIEW);
                                    break;
                                case "PC":
                                    if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW)))
                                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, PC_PROGNOTES_VIEW);
                                    break;
                            }
                    }
	                //End - pmittal5
                }

                //Initialize the application layer ProgressNotesManager class
                // Ash - cloud, overload constructor with Client ID
                //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password);
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);
                objProgressNotesManager.m_UserLogin = base.m_userLogin;  //Aman ML Change

                if (objDocument.ActivateFilter == true)
                {
                    ////Changed by akaushik5 for MITS 30789 Starts
                    // sFilterSQL = objProgressNotesManager.CreateFilterString(objDocument.objFilter.ClaimIDList, objDocument.objFilter.EnteredByList, objDocument.objFilter.NoteTypeList, objDocument.objFilter.UserTypeList, objDocument.objFilter.ActivityFromDate, objDocument.objFilter.ActivityToDate, objDocument.objFilter.NotesTextContains, objDocument.objFilter.SortBy);
                    sFilterSQL = objProgressNotesManager.CreateFilterString(objDocument.objFilter.ClaimIDList, objDocument.objFilter.EnteredByList, objDocument.objFilter.NoteTypeList, objDocument.objFilter.UserTypeList, objDocument.objFilter.ActivityFromDate, objDocument.objFilter.ActivityToDate, objDocument.objFilter.NotesTextContains, objDocument.objFilter.SortBy, objDocument.objFilter.SubjectList, objDocument.objFilter.OrderBy, objDocument.objFilter.ClaimantIDList); //Added by gbindra MITS#34104 02132014 WWIG GAP15 
                    ////Changed by akaushik5 for MITS 30789 Ends
                    //tanwar2 - print html notes - start
                    objProgressNotesManager.CreateFilterSession(base.GetSessionObject(), objDocument.objFilter.ClaimIDList, objDocument.objFilter.EnteredByList, objDocument.objFilter.NoteTypeList, objDocument.objFilter.UserTypeList, objDocument.objFilter.ActivityFromDate, objDocument.objFilter.ActivityToDate, objDocument.objFilter.NotesTextContains, objDocument.objFilter.SortBy);
                    //tanwar2 - print html notes - end

                }

                XmlDocument p_objXmlDocOut = new XmlDocument();
                //williams:neha goel--start:added objDocument.PolicyID:MITS 21704
                //p_objXmlDocOut = objProgressNotesManager.OnLoadPartial(objDocument.EventID, objDocument.ClaimID, objDocument.ActivateFilter, sFilterSQL, objDocument.PageNumber, objDocument.SortColumn, objDocument.Direction);
                //p_objXmlDocOut = objProgressNotesManager.OnLoadPartial(objDocument.EventID, objDocument.ClaimID, objDocument.ActivateFilter, sFilterSQL, objDocument.PageNumber, objDocument.SortColumn, objDocument.Direction, objDocument.PolicyID, objDocument.PolicyName);
                //Changed by Amitosh for mits 23473 (05/11/2011)
                objProgressNotesManager.iCaseManagement = objDocument.CaseMgtRowID;//averma62 Mits 28988
                p_objXmlDocOut = objProgressNotesManager.OnLoadPartial(objDocument.EventID, objDocument.ClaimID, objDocument.ActivateFilter, sFilterSQL, objDocument.PageNumber, objDocument.SortColumn, objDocument.Direction, objDocument.PolicyID, objDocument.PolicyName, objDocument.bIsMobileAdjuster, objDocument.bIsMobilityAdjuster, objDocument.ClaimantId); //Added by gbindra MITS#34104 02132014 WWIG GAP15
                outDocument.SortPref = p_objXmlDocOut.SelectSingleNode("//SortPref").InnerText;

                ////Added by akaushik5 for MITS 30789 Starts
                outDocument.OrderPref = p_objXmlDocOut.SelectSingleNode("//OrderPref").InnerText;
                ////Added by akaushik5 for MITS 30789 Ends
                //williams:neha goel--end:MITS 21704

                outDocument.ClaimID = objDocument.ClaimID;
                outDocument.LOB = objDocument.LOB; //pmittal5
                outDocument.EventID = objDocument.EventID;
                //MGaba2:MITS 28450: Setting Event Number: 
                outDocument.EventNumber = objDocument.EventNumber;
                //williams-neha goel--start:MITS 21704
                outDocument.PolicyID = objDocument.PolicyID;
                //dbisht6   
                if ((objDocument.PolicyName != null) && (string.Equals(objDocument.PolicyName.ToString().Trim(), "")) && (p_objXmlDocOut.SelectSingleNode("//PolicyName")!=null))
                    outDocument.PolicyName = p_objXmlDocOut.SelectSingleNode("//PolicyName").InnerText;
                else
                    outDocument.PolicyName = objDocument.PolicyName;

                if ((objDocument.PolicyNumber != null) && (string.Equals(objDocument.PolicyNumber.ToString().Trim(), "")) && (p_objXmlDocOut.SelectSingleNode("//PolicyNumber")!=null))
                    outDocument.PolicyNumber = p_objXmlDocOut.SelectSingleNode("//PolicyNumber").InnerText;
                else
                    outDocument.PolicyNumber = objDocument.PolicyNumber;
                //dbisht6 end
                
                //outDocument.CodeId = objDocument.CodeId;
                //williams:neha goel--end::MITS 21704
                outDocument.FormName = objDocument.FormName;

                // rrachev JIRA RMA-12166 Begin
                outDocument.Claimant = GetNodeValue(p_objXmlDocOut, "//Claimant", outDocument.Claimant);
                outDocument.FreezeText = GetNodeValue(p_objXmlDocOut, "//FreezeText", outDocument.FreezeText);
                //Changed by gagan for mits 14626 : start
                outDocument.ShowDateStamp = GetNodeValue(p_objXmlDocOut, "//ShowDateStamp", outDocument.ShowDateStamp);
                //Changed by gagan for mits 14626 : end

                //Start by Shivendu for MITS 18098
                outDocument.TotalNumberOfPages = GetNodeValue(p_objXmlDocOut, "//TotalNumberOfPages", outDocument.TotalNumberOfPages);
                //MGaba2:MITS 18913:Deleting record was disabling links
                //outDocument.PageNumber = objDocument.PageNumber;

                outDocument.PageNumber = GetNodeValue(p_objXmlDocOut, "//PageNumber", outDocument.PageNumber);
                //MGaba2:MITS 18913:End
                //End by Shivendu for MITS 18098

                outDocument.UserName = GetNodeValue(p_objXmlDocOut, "//UserName", outDocument.UserName);
                outDocument.PrintOrder1 = GetNodeValue(p_objXmlDocOut, "//PrintOrder1", outDocument.PrintOrder1);
                outDocument.PrintOrder2 = GetNodeValue(p_objXmlDocOut, "//PrintOrder2", outDocument.PrintOrder2);
                outDocument.PrintOrder3 = GetNodeValue(p_objXmlDocOut, "//PrintOrder3", outDocument.PrintOrder3);
                //Parijat: start Post Editable Enhanced Notes 
                outDocument.EnhancedTimeLimit = GetNodeValue(p_objXmlDocOut, "//TimeLimit", outDocument.EnhancedTimeLimit);
                outDocument.UserEditingRights = GetNodeValue(p_objXmlDocOut, "//EnhNoteEditRight", outDocument.UserEditingRights);
                //Parijat: end Post Editable Enhanced Notes 
                //if (p_objXmlDocOut.SelectSingleNode("//Claimant") != null)
                //    outDocument.Claimant = p_objXmlDocOut.SelectSingleNode("//Claimant").InnerText;
                //if (p_objXmlDocOut.SelectSingleNode("//FreezeText") != null)
                //    outDocument.FreezeText = Convert.ToBoolean(p_objXmlDocOut.SelectSingleNode("//FreezeText").InnerText);
                ////Changed by gagan for mits 14626 : start
                //if (p_objXmlDocOut.SelectSingleNode("//ShowDateStamp") != null)
                //    outDocument.ShowDateStamp = Convert.ToBoolean(p_objXmlDocOut.SelectSingleNode("//ShowDateStamp").InnerText);
                ////Changed by gagan for mits 14626 : end

                ////Start by Shivendu for MITS 18098
                //if (p_objXmlDocOut.SelectSingleNode("//TotalNumberOfPages") != null)
                //    outDocument.TotalNumberOfPages = Convert.ToInt32(p_objXmlDocOut.SelectSingleNode("//TotalNumberOfPages").InnerText);
                ////MGaba2:MITS 18913:Deleting record was disabling links
                ////outDocument.PageNumber = objDocument.PageNumber;

                //if (p_objXmlDocOut.SelectSingleNode("//PageNumber") != null)
                //    outDocument.PageNumber = Convert.ToInt32(p_objXmlDocOut.SelectSingleNode("//PageNumber").InnerText);
                ////MGaba2:MITS 18913:End
                ////End by Shivendu for MITS 18098

                //if (p_objXmlDocOut.SelectSingleNode("//UserName") != null)
                //    outDocument.UserName = p_objXmlDocOut.SelectSingleNode("//UserName").InnerText;
                //if (p_objXmlDocOut.SelectSingleNode("//PrintOrder1") != null)
                //    outDocument.PrintOrder1 = p_objXmlDocOut.SelectSingleNode("//PrintOrder1").InnerText;
                //if (p_objXmlDocOut.SelectSingleNode("//PrintOrder2") != null)
                //    outDocument.PrintOrder2 = p_objXmlDocOut.SelectSingleNode("//PrintOrder2").InnerText;
                //if (p_objXmlDocOut.SelectSingleNode("//PrintOrder3") != null)
                //    outDocument.PrintOrder3 = p_objXmlDocOut.SelectSingleNode("//PrintOrder3").InnerText;
                ////Parijat: start Post Editable Enhanced Notes 
                //if (p_objXmlDocOut.SelectSingleNode("//TimeLimit") != null)
                //    outDocument.EnhancedTimeLimit = p_objXmlDocOut.SelectSingleNode("//TimeLimit").InnerText;
                //if (p_objXmlDocOut.SelectSingleNode("//EnhNoteEditRight") != null)
                //    outDocument.UserEditingRights = Convert.ToBoolean(p_objXmlDocOut.SelectSingleNode("//EnhNoteEditRight").InnerText);
                ////Parijat: end Post Editable Enhanced Notes 
                // rrachev JIRA RMA-12166 End

                iNotesCount= p_objXmlDocOut.GetElementsByTagName("ProgressNote").Count;
                //williams--neha goel:start--for filtring the notes code type on tne basis of parent::MITS 21704
                
                //rrachev JIRA RMA-12166 Begin
                outDocument.CodeId = GetNodeValue(p_objXmlDocOut, "//NotesParentCodeId", outDocument.CodeId);
                //if (p_objXmlDocOut.SelectSingleNode("//NotesParentCodeId") != null)
                //    outDocument.CodeId = p_objXmlDocOut.SelectSingleNode("//NotesParentCodeId").InnerText;
                //rrachev JIRA RMA-12166 END

                //williams--neha goel:end::MITS 21704
                if (iNotesCount > 0)
                {
                    XmlNodeList xmlListProgressNotes = p_objXmlDocOut.GetElementsByTagName("ProgressNote");
                    for (int x = 0; x < iNotesCount; x++)
                    {
                        // rrachev JIRA RMA-12166 Move p_objXmlDocOut.GetElementsByTagName("ProgressNote") before "for" BEGIN
                        //XmlElement xmlNode = (XmlElement)p_objXmlDocOut.GetElementsByTagName("ProgressNote")[x];\
                        XmlNode xmlNode = xmlListProgressNotes[x];
                        // rrachev JIRA RMA-12166 Move p_objXmlDocOut.GetElementsByTagName("ProgressNote") before "for" END
                        ProgressNote prgNote = new ProgressNote();
                        prgNote.ClaimProgressNoteId = Conversion.ConvertStrToInteger(xmlNode.SelectSingleNode("ClaimProgressNoteId").InnerText);
                        prgNote.AttachedTo = xmlNode.SelectSingleNode("AttachedTo").InnerText;
                        prgNote.EnteredBy = xmlNode.SelectSingleNode("EnteredBy").InnerText;
                        prgNote.EnteredByName = xmlNode.SelectSingleNode("EnteredByName").InnerText;
                        prgNote.DateEntered = xmlNode.SelectSingleNode("DateEntered").InnerText;
                        prgNote.DateEnteredForSort = xmlNode.SelectSingleNode("DateEnteredForSort").InnerText;
                        prgNote.DateCreated = xmlNode.SelectSingleNode("DateCreated").InnerText;
                        prgNote.TimeCreated = xmlNode.SelectSingleNode("TimeCreated").InnerText;
                        prgNote.TimeCreatedForSort = xmlNode.SelectSingleNode("TimeCreatedForSort").InnerText;
                        // rrachev JIRA RMA-12166 Begin
                        XmlNode xmlNoteTypeCode = xmlNode.SelectSingleNode("NoteTypeCode");
                        prgNote.NoteTypeCode = xmlNoteTypeCode.InnerText;
                        prgNote.NoteTypeCodeId = xmlNoteTypeCode.Attributes["CodeId"].Value;
                        XmlNode xmlUserTypeCode = xmlNode.SelectSingleNode("UserTypeCode");
                        prgNote.UserTypeCode = xmlUserTypeCode.InnerText;
                        prgNote.UserTypeCodeId = xmlUserTypeCode.Attributes["CodeId"].Value;
                        //prgNote.NoteTypeCode = xmlNode.SelectSingleNode("NoteTypeCode").InnerText;
                        //prgNote.NoteTypeCodeId = xmlNode.SelectSingleNode("NoteTypeCode").Attributes["CodeId"].Value;
                        //prgNote.UserTypeCode = xmlNode.SelectSingleNode("UserTypeCode").InnerText;
                        //prgNote.UserTypeCodeId = xmlNode.SelectSingleNode("UserTypeCode").Attributes["CodeId"].Value;
                        // rrachev JIRA RMA-12166 End

                        prgNote.Subject = xmlNode.SelectSingleNode("Subject").InnerText; //zmohammad MITS 30218
                                                


                        //MITS 21258: Yatharth :Removed the System.Security.SecurityElement.Escape as this method is now obsolete
                        //with respect to the usage of progressnotes. Earlier, we used to handle progress notes via XML but now
                        //we make ProgressNoteList.
                        //prgNote.NoteMemo =  System.Security.SecurityElement.Escape(xmlNode.SelectSingleNode("NoteMemo").InnerText);
                        //prgNote.NoteMemoCareTech =  System.Security.SecurityElement.Escape(xmlNode.SelectSingleNode("NoteMemoCareTech").InnerText);
                        prgNote.NoteMemo = xmlNode.SelectSingleNode("NoteMemo").InnerText;
                        prgNote.NoteMemoCareTech = xmlNode.SelectSingleNode("NoteMemoCareTech").InnerText;

                        //Added Rakhi for R6-Progress Notes Templates
                        // rrachev JIRA RMA-12166 Begin
                        XmlNode templateIdNode = xmlNode.SelectSingleNode("TemplateId");
                        if (templateIdNode != null)
                        {
                            if (templateIdNode.InnerText == "")
                                prgNote.TemplateID = 0;
                            else
                                prgNote.TemplateID = Convert.ToInt32(templateIdNode.InnerText);
                        }
                        prgNote.TemplateName = GetNodeValue(xmlNode, "TemplateName", prgNote.TemplateName);
                        //if (xmlNode.SelectSingleNode("TemplateId") != null)
                        //{
                        //    if (xmlNode.SelectSingleNode("TemplateId").InnerText == "")
                        //        prgNote.TemplateID = 0;
                        //    else
                        //        prgNote.TemplateID = Convert.ToInt32(xmlNode.SelectSingleNode("TemplateId").InnerText);
                        //}

                        //if (xmlNode.SelectSingleNode("TemplateName") != null)
                        //    prgNote.TemplateName = xmlNode.SelectSingleNode("TemplateName").InnerText;
                        // rrachev JIRA RMA-12166 End

                        //Added Rakhi for R6-Progress Notes Templates
                        if (prgNote.NoteMemoCareTech.Length > 33)//Range changed by Shivendu for MITS 19164
                            prgNote.NoteMemoTrimmed = Utilities.AddHTMLTags(prgNote.NoteMemoCareTech.Substring(0, 30) + "...");
                        //prgNote.NoteMemoTrimmed = (prgNote.NoteMemoCareTech.Substring(0, 30) + "...").Replace("&", "&amp;").Replace(@"<", "&lt;").Replace(@">", "&gt;");
                        else
                            //prgNote.NoteMemoTrimmed = prgNote.NoteMemoCareTech.Replace("&", "&amp;").Replace(@"<", "&lt;").Replace(@">", "&gt;");
                            prgNote.NoteMemoTrimmed = Utilities.AddHTMLTags(prgNote.NoteMemoCareTech);
                        //Ashish Ahuja : Mits 33124 start
                        //if (prgNote.Subject.Length > 33)
                        //    prgNote.Subject = Utilities.AddHTMLTags(prgNote.Subject.Substring(0, 30) + "...");
                        //else
                        //    prgNote.Subject = Utilities.AddHTMLTags(prgNote.Subject);
                        //Ashish Ahuja : Mits 33124 end
						//Ijha: Mobile Adjuster :note text not trimmed
                        if (objDocument.bIsMobileAdjuster || objDocument.bIsMobilityAdjuster)
                        {
							prgNote.NoteMemoTrimmed = prgNote.NoteMemoCareTech;
						}
                        prgNote.AdjusterName = xmlNode.SelectSingleNode("AdjusterName").InnerText;

                        // rrachev JIRA RMA-12166 Begin
                        XmlNode claimNumberNode = xmlNode.SelectSingleNode("ClaimNumber");
                        if (claimNumberNode != null)
                        {
                            prgNote.ClaimNumber = claimNumberNode.InnerText;
                            //rsolanki2: mits 22133: advanced search edit from event issue
                            XmlAttribute attrId = claimNumberNode.Attributes["Id"];
                            if (attrId != null)
                            {
                                prgNote.ClaimID = attrId.Value;
                            }
                        }
                        XmlNode eventNumberNode = xmlNode.SelectSingleNode("EventNumber");
                        if (eventNumberNode != null)
                        {
                            prgNote.EventNumber = eventNumberNode.InnerText;
                            //rsolanki2: mits 22133: advanced search edit from event issue
                            XmlAttribute attrId = eventNumberNode.Attributes["Id"];
                            if (attrId != null)
                            {
                                prgNote.EventID = attrId.Value;
                            }
                        }
                        //Added neha goel for R6-Enhanced Notes Policy--start
                        XmlNode policyIdNode = xmlNode.SelectSingleNode("PolicyId");
                        if (policyIdNode != null)
                        {
                            prgNote.PolicyID = policyIdNode.InnerText;
                        }
                        prgNote.PolicyName = GetNodeValue(xmlNode, "PolicyName", prgNote.PolicyName);
                        prgNote.PolicyNumber = GetNodeValue(xmlNode, "PolicyNumber", prgNote.PolicyNumber);
                        //if (xmlNode.SelectSingleNode("ClaimNumber") != null)
                        //{ 
                        //    prgNote.ClaimNumber = xmlNode.SelectSingleNode("ClaimNumber").InnerText;
                        //    //rsolanki2: mits 22133: advanced search edit from event issue
                        //    if (xmlNode.SelectSingleNode("ClaimNumber").Attributes["Id"] !=null)
                        //    {
                        //        prgNote.ClaimID = xmlNode.SelectSingleNode("ClaimNumber").Attributes["Id"].Value;
                        //    }
                        //}
                        //if (xmlNode.SelectSingleNode("EventNumber") != null)
                        //{ 
                        //    prgNote.EventNumber = xmlNode.SelectSingleNode("EventNumber").InnerText;
                        //    //rsolanki2: mits 22133: advanced search edit from event issue
                        //    if (xmlNode.SelectSingleNode("EventNumber").Attributes["Id"]!=null)
                        //    {
                        //        prgNote.EventID = xmlNode.SelectSingleNode("EventNumber").Attributes["Id"].Value; 
                        //    }
                        //}
                        ////Added neha goel for R6-Enhanced Notes Policy--start
                        //if (xmlNode.SelectSingleNode("PolicyId") != null)
                        //{
                        //    if (xmlNode.SelectSingleNode("PolicyId").InnerText == "")
                        //        prgNote.PolicyID = "";
                        //    else
                        //        prgNote.PolicyID = xmlNode.SelectSingleNode("PolicyId").InnerText;
                        //}
                        //if (xmlNode.SelectSingleNode("PolicyName") != null)
                        //    prgNote.PolicyName = xmlNode.SelectSingleNode("PolicyName").InnerText;
                        //if (xmlNode.SelectSingleNode("PolicyNumber") != null)
                        //    prgNote.PolicyNumber = xmlNode.SelectSingleNode("PolicyNumber").InnerText;
                        // rrachev JIRA RMA-12166 End
                        //if (xmlNode.SelectSingleNode("CodeId") != null)
                           // prgNote.CodeId = xmlNode.SelectSingleNode("CodeId").InnerText;
                        //Added neha goel for R6-Enhanced Notes Policy--endstart
                        outDocument.objProgressNoteList.Add(prgNote);

                    }
                }

                outDocument.ActivateFilter = true;

                //willimas--neha goel----start:policy enhanced notes permissions
                if (!string.IsNullOrEmpty(objDocument.PolicyID.ToString()) && objDocument.PolicyID != 0)
                {
                    // rrachev JIAR RMA-12166 Begin
                    outDocument.bPolicyTrackingCreatePermission = base.userLogin.IsAllowedEx(POLICY_PROGNOTES_CREATE);
                    outDocument.bPolicyTrackingDeletePermission = base.userLogin.IsAllowedEx(POLICY_PROGNOTES_DELETE);
                    outDocument.bPolicyTrackingEditPermission = base.userLogin.IsAllowedEx(POLICY_PROGNOTES_EDIT);
                    outDocument.bPolicyTrackingPrintPermission = base.userLogin.IsAllowedEx(POLICY_PROGNOTES_PRINT);
                    outDocument.bPolicyTrackingViewAllNotesPermission = base.userLogin.IsAllowedEx(POLICY_PROGNOTES_VIEW_ALL_NOTES);
                    outDocument.bTemplatesPermission = base.userLogin.IsAllowedEx(TEMPLATES);

                    //if (!(base.userLogin.IsAllowedEx(POLICY_PROGNOTES_CREATE)))
                    //    outDocument.bPolicyTrackingCreatePermission = false;
                    //else
                    //    outDocument.bPolicyTrackingCreatePermission = true;

                    //if (!(base.userLogin.IsAllowedEx(POLICY_PROGNOTES_DELETE)))
                    //    outDocument.bPolicyTrackingDeletePermission = false;
                    //else
                    //    outDocument.bPolicyTrackingDeletePermission = true;

                    //if (!(base.userLogin.IsAllowedEx(POLICY_PROGNOTES_EDIT)))
                    //    outDocument.bPolicyTrackingEditPermission = false;
                    //else
                    //    outDocument.bPolicyTrackingEditPermission = true;

                    //if (!(base.userLogin.IsAllowedEx(POLICY_PROGNOTES_PRINT)))
                    //    outDocument.bPolicyTrackingPrintPermission = false;
                    //else
                    //    outDocument.bPolicyTrackingPrintPermission = true;

                    //if (!(base.userLogin.IsAllowedEx(POLICY_PROGNOTES_VIEW_ALL_NOTES)))
                    //    outDocument.bPolicyTrackingViewAllNotesPermission = false;
                    //else
                    //    outDocument.bPolicyTrackingViewAllNotesPermission = true;

                    //if (!(base.userLogin.IsAllowedEx(TEMPLATES)))
                    //    outDocument.bTemplatesPermission = false;
                    //else
                    //    outDocument.bTemplatesPermission = true;
                    // rrachev JIAR RMA-12166 End
                   
                }
                //willimas--neha goel----start:policy enhanced notes permissions:added if else
                else
                { 
	                //pmittal5 Mits 21514 07/19/10
	                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_CREATE)))
	                //    outDocument.bCreatePermission = false;
	                //else
	                //    outDocument.bCreatePermission = true;

	                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_DELETE)))
	                //    outDocument.bDeletePermission = false;
	                //else
	                //    outDocument.bDeletePermission = true;

	                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_EDIT)))
	                //    outDocument.bEditPermission = false;
	                //else
	                //    outDocument.bEditPermission = true;

	                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_PRINT)))
	                //    outDocument.bPrintPermission = false;
	                //else
	                //    outDocument.bPrintPermission = true;

	                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_VIEW_ALL_NOTES)))
	                //    outDocument.bViewAllNotesPermission = false;
	                //else
	                //    outDocument.bViewAllNotesPermission = true;

	                //Changed by Gagan for MITS 15503 : End

	                //rsushilaggar MITS 21119 06/18/2010
	                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_TEMPLATES)))
	                //    outDocument.bTemplatesPermission = false;
	                //else
	                //    outDocument.bTemplatesPermission = true;        
	                //end rsushilaggar   

	                 //MGaba2:MITS 22173: Separate branch for Templates
	                if (!(base.userLogin.IsAllowedEx(TEMPLATES)))
	                    outDocument.bTemplatesPermission = false;
	                else
	                    outDocument.bTemplatesPermission = true;  

                    /*Added by gbindra MITS#34104 WWIG GAP15 START*/
                    if (objDocument.ClaimantId > 0 && objSysSetting.AllowNotesAtClaimant == true)
                    {
                        if (objSysSetting.AllowNotesAtClaimant == true)
                        {
                            // rrachev JIRA RMA-12166 Begin
                            int viewId = 0;
                            switch (objDocument.LOB)
                            {
                                case "GC":
                                    viewId = GC_CLMNT_PROGNOTES_VIEW;
                                    break;
                                case "VA":
                                    viewId = VA_CLMNT_PROGNOTES_VIEW;
                                    break;
                                case "PC":
                                    viewId = PC_CLMNT_PROGNOTES_VIEW;
                                    break;
                            }

                            if (viewId != 0)
                            {
                                outDocument.bCreatePermission = base.userLogin.IsAllowedEx(viewId + PROGNOTES_CREATE_OFFSET);
                                outDocument.bDeletePermission = base.userLogin.IsAllowedEx(viewId + PROGNOTES_DELETE_OFFSET);
                                outDocument.bEditPermission = base.userLogin.IsAllowedEx(viewId + PROGNOTES_EDIT_OFFSET);
                                outDocument.bPrintPermission = base.userLogin.IsAllowedEx(viewId + PROGNOTES_PRINT_OFFSET);
                                outDocument.bViewAllNotesPermission = base.userLogin.IsAllowedEx(viewId + PROGNOTES_VIEWALL_OFFSET);
                            }                            
                            //switch (objDocument.LOB)
                            //{
                            //    case "GC":
                            //        if (!(base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                            //            outDocument.bCreatePermission = false;
                            //        else
                            //            outDocument.bCreatePermission = true;
                            //        if (!(base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
                            //            outDocument.bDeletePermission = false;
                            //        else
                            //            outDocument.bDeletePermission = true;
                            //        if (!(base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                            //            outDocument.bEditPermission = false;
                            //        else
                            //            outDocument.bEditPermission = true;
                            //        if (!(base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
                            //            outDocument.bPrintPermission = false;
                            //        else
                            //            outDocument.bPrintPermission = true;
                            //        if (!(base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_VIEWALL_OFFSET)))
                            //            outDocument.bViewAllNotesPermission = false;
                            //        else
                            //            outDocument.bViewAllNotesPermission = true;
                            //        break;
                            //    case "VA":
                            //        if (!(base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                            //            outDocument.bCreatePermission = false;
                            //        else
                            //            outDocument.bCreatePermission = true;
                            //        if (!(base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
                            //            outDocument.bDeletePermission = false;
                            //        else
                            //            outDocument.bDeletePermission = true;
                            //        if (!(base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                            //            outDocument.bEditPermission = false;
                            //        else
                            //            outDocument.bEditPermission = true;
                            //        if (!(base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
                            //            outDocument.bPrintPermission = false;
                            //        else
                            //            outDocument.bPrintPermission = true;
                            //        if (!(base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_VIEWALL_OFFSET)))
                            //            outDocument.bViewAllNotesPermission = false;
                            //        else
                            //            outDocument.bViewAllNotesPermission = true;
                            //        break;
                            //    case "PC":
                            //        if (!(base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                            //            outDocument.bCreatePermission = false;
                            //        else
                            //            outDocument.bCreatePermission = true;
                            //        if (!(base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
                            //            outDocument.bDeletePermission = false;
                            //        else
                            //            outDocument.bDeletePermission = true;
                            //        if (!(base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                            //            outDocument.bEditPermission = false;
                            //        else
                            //            outDocument.bEditPermission = true;
                            //        if (!(base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
                            //            outDocument.bPrintPermission = false;
                            //        else
                            //            outDocument.bPrintPermission = true;
                            //        if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_VIEWALL_OFFSET)))
                            //            outDocument.bViewAllNotesPermission = false;
                            //        else
                            //            outDocument.bViewAllNotesPermission = true;
                            //        break;
                            //}
                            // rrachev JIRA RMA-12166 End
                        }
                        else
                        {
                            outDocument.bCreatePermission = false;
                            outDocument.bDeletePermission = false;
                            outDocument.bEditPermission = false;
                            outDocument.bPrintPermission = false;
                            outDocument.bViewAllNotesPermission = false;
                        }
                    }
                    else if (objDocument.EventID != 0 && objDocument.ClaimID == 0)  //Event's Enhanced notes are opening
	                {
                        // rrachev JIRA RMA-12166 Begin
                        outDocument.bCreatePermission = base.userLogin.IsAllowedEx(EVENT_PROGNOTES_CREATE);
                        outDocument.bDeletePermission = base.userLogin.IsAllowedEx(EVENT_PROGNOTES_DELETE);
                        outDocument.bEditPermission = base.userLogin.IsAllowedEx(EVENT_PROGNOTES_EDIT);
                        outDocument.bPrintPermission = base.userLogin.IsAllowedEx(EVENT_PROGNOTES_PRINT);
                        outDocument.bViewAllNotesPermission = base.userLogin.IsAllowedEx(EVENT_PROGNOTES_VIEW_ALL_NOTES);
                    //    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_CREATE)))
                    //        outDocument.bCreatePermission = false;
                    //    else
                    //        outDocument.bCreatePermission = true;

                    //    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_DELETE)))
                    //        outDocument.bDeletePermission = false;
                    //    else
                    //        outDocument.bDeletePermission = true;

                    //    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_EDIT)))
                    //        outDocument.bEditPermission = false;
                    //    else
                    //        outDocument.bEditPermission = true;

                    //    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_PRINT)))
                    //        outDocument.bPrintPermission = false;
                    //    else
                    //        outDocument.bPrintPermission = true;

                    //    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_VIEW_ALL_NOTES)))
                    //    outDocument.bViewAllNotesPermission = false;
                    //else
                    //    outDocument.bViewAllNotesPermission = true;
                        // rrachev JIRA RMA-12166 End
	                    //MGaba2:MITS 22173: Separate branch for Templates
	                    //if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_TEMPLATES)))
	                    //    outDocument.bTemplatesPermission = false;
	                    //else
	                    //    outDocument.bTemplatesPermission = true;     
	                }
	                else if (objDocument.ClaimID != 0)  //Claim's Enhanced notes are opening
	                {
	                    // rrachev JIRA RMA-12166 Begin
                        int viewId = 0;
                        switch (objDocument.LOB)
                        {
                            case "GC":
                                viewId = GC_PROGNOTES_VIEW;
                                break;
                            case "VA":
                                viewId = VA_PROGNOTES_VIEW;
                                break;
                            case "WC":
                                viewId = WC_PROGNOTES_VIEW;
                                break;
                            case "DI":
                                viewId = DI_PROGNOTES_VIEW;
                                break;
                            case "PC":
                                viewId = PC_PROGNOTES_VIEW;
                                break;
                        }

                        if (viewId != 0)
                        {
                            outDocument.bCreatePermission = base.userLogin.IsAllowedEx(viewId + PROGNOTES_CREATE_OFFSET);
                            outDocument.bDeletePermission = base.userLogin.IsAllowedEx(viewId + PROGNOTES_DELETE_OFFSET);
                            outDocument.bEditPermission = base.userLogin.IsAllowedEx(viewId + PROGNOTES_EDIT_OFFSET);
                            outDocument.bPrintPermission = base.userLogin.IsAllowedEx(viewId + PROGNOTES_PRINT_OFFSET);
                            outDocument.bViewAllNotesPermission = base.userLogin.IsAllowedEx(viewId + PROGNOTES_VIEWALL_OFFSET);
                        }

                        //switch (objDocument.LOB)
                        //{
                        //    case "GC":
                        //        if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                        //            outDocument.bCreatePermission = false;
                        //        else
                        //            outDocument.bCreatePermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
                        //            outDocument.bDeletePermission = false;
                        //        else
                        //            outDocument.bDeletePermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                        //            outDocument.bEditPermission = false;
                        //        else
                        //            outDocument.bEditPermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
                        //            outDocument.bPrintPermission = false;
                        //        else
                        //            outDocument.bPrintPermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_VIEWALL_OFFSET)))
                        //            outDocument.bViewAllNotesPermission = false;
                        //        else
                        //            outDocument.bViewAllNotesPermission = true;
                        //        //MGaba2:MITS 22173: Separate branch for Templates
                        //        //if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_TEMPLATES_OFFSET)))
                        //        //    outDocument.bTemplatesPermission = false;
                        //        //else
                        //        //    outDocument.bTemplatesPermission = true;  
                        //        break;
	                        
                        //    case "VA":
                        //        if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                        //            outDocument.bCreatePermission = false;
                        //        else
                        //            outDocument.bCreatePermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
                        //            outDocument.bDeletePermission = false;
                        //        else
                        //            outDocument.bDeletePermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                        //            outDocument.bEditPermission = false;
                        //        else
                        //            outDocument.bEditPermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
                        //            outDocument.bPrintPermission = false;
                        //        else
                        //            outDocument.bPrintPermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_VIEWALL_OFFSET)))
                        //            outDocument.bViewAllNotesPermission = false;
                        //        else
                        //            outDocument.bViewAllNotesPermission = true;
                        //        //MGaba2:MITS 22173: Separate branch for Templates
                        //        //if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_TEMPLATES_OFFSET)))
                        //        //    outDocument.bTemplatesPermission = false;
                        //        //else
                        //        //    outDocument.bTemplatesPermission = true;  
                        //        break;
	                        
                        //    case "WC":
                        //        if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                        //            outDocument.bCreatePermission = false;
                        //        else
                        //            outDocument.bCreatePermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
                        //            outDocument.bDeletePermission = false;
                        //        else
                        //            outDocument.bDeletePermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                        //            outDocument.bEditPermission = false;
                        //        else
                        //            outDocument.bEditPermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
                        //            outDocument.bPrintPermission = false;
                        //        else
                        //            outDocument.bPrintPermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_VIEWALL_OFFSET)))
                        //            outDocument.bViewAllNotesPermission = false;
                        //        else
                        //            outDocument.bViewAllNotesPermission = true;
                        //        //MGaba2:MITS 22173: Separate branch for Templates
                        //        //if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_TEMPLATES_OFFSET)))
                        //        //    outDocument.bTemplatesPermission = false;
                        //        //else
                        //        //    outDocument.bTemplatesPermission = true;  
                        //        break;
	                        
                        //    case "DI":
                        //        if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                        //            outDocument.bCreatePermission = false;
                        //        else
                        //            outDocument.bCreatePermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
                        //            outDocument.bDeletePermission = false;
                        //        else
                        //            outDocument.bDeletePermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                        //            outDocument.bEditPermission = false;
                        //        else
                        //            outDocument.bEditPermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
                        //            outDocument.bPrintPermission = false;
                        //        else
                        //            outDocument.bPrintPermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_VIEWALL_OFFSET)))
                        //            outDocument.bViewAllNotesPermission = false;
                        //        else
                        //            outDocument.bViewAllNotesPermission = true;
                        //        //MGaba2:MITS 22173: Separate branch for Templates
                        //        //if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_TEMPLATES_OFFSET)))
                        //        //    outDocument.bTemplatesPermission = false;
                        //        //else
                        //        //    outDocument.bTemplatesPermission = true;  
                        //        break;
	                        
                        //    case "PC":
                        //        if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                        //            outDocument.bCreatePermission = false;
                        //        else
                        //            outDocument.bCreatePermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
                        //            outDocument.bDeletePermission = false;
                        //        else
                        //            outDocument.bDeletePermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                        //            outDocument.bEditPermission = false;
                        //        else
                        //            outDocument.bEditPermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
                        //            outDocument.bPrintPermission = false;
                        //        else
                        //            outDocument.bPrintPermission = true;

                        //        if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_VIEWALL_OFFSET)))
                        //            outDocument.bViewAllNotesPermission = false;
                        //        else
                        //            outDocument.bViewAllNotesPermission = true;
                        //        //MGaba2:MITS 22173: Separate branch for Templates
                        //        //if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_TEMPLATES_OFFSET)))
                        //        //    outDocument.bTemplatesPermission = false;
                        //        //else
                        //        //    outDocument.bTemplatesPermission = true;  
                        //        break;
                        //}
                        // rrachev JIRA RMA-12166 End
	                }
	                //End - pmittal5
                }
                outDocument.ClaimantId = objDocument.ClaimantId; //Added by gbindra MITS#34104 WWIG GAP15 02122014
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.OnLoad.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {

                if (objProgressNotesManager != null)
                {
                    objProgressNotesManager.Dispose();
                    objProgressNotesManager = null;
                }
                if (objCache != null)
                    objCache.Dispose();
            }

            return (true);
        }

        private static int GetNodeValue(XmlNode parentNode, string nodePath, int defaultValue)
        {
            XmlNode xmlNode = parentNode.SelectSingleNode(nodePath);
            if (xmlNode != null) defaultValue = Convert.ToInt32(xmlNode.InnerText);
            return defaultValue;
        }

        private static string GetNodeValue(XmlNode parentNode, string nodePath, string defaultValue)
        {
            XmlNode xmlNode = parentNode.SelectSingleNode(nodePath);
            if (xmlNode != null) defaultValue = xmlNode.InnerText;
            return defaultValue;
        }

        private static bool GetNodeValue(XmlNode parentNode, string nodePath, bool defaultValue)
        {
            XmlNode xmlNode = parentNode.SelectSingleNode(nodePath);
            if (xmlNode != null) defaultValue = Convert.ToBoolean(xmlNode.InnerText);
            return defaultValue;
        }

        public bool DeleteProgressNoteIntegrate(ProgressNotesType objDocument, out ProgressNotesType outDocument, ref BusinessAdaptorErrors p_objErrOut)
        {

            ProgressNotesManager objProgressNotesManager = null;
            outDocument = new ProgressNotesType();
            SysSettings objSysSetting = null; //Added by gbindra MITS#34104 WWIG GAP15
             try
            {
                objSysSetting = new SysSettings(m_connectionString, base.ClientId); //Ash - cloud;    //Added by gbindra MITS#34104 WWIG GAP15
                //willimas--neha goel----start:policy enhanced notes permissions:added if else
                if (!string.IsNullOrEmpty(objDocument.PolicyID.ToString()) && objDocument.PolicyID != 0)
                {
                    if (!(base.userLogin.IsAllowedEx(POLICY_PROGNOTES_DELETE)))
                        throw new PermissionViolationException(RMPermissions.RMO_NOPROGRESS_NOTES_DELETE, POLICY_PROGNOTES_DELETE);
                }
                //willimas--neha goel----end:policy enhanced notes permissions
                else
                {
	                //pmittal5 Mits 21514 07/19/10
	                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_DELETE)))
	                //    throw new PermissionViolationException(RMPermissions.RMO_NOPROGRESS_NOTES_DELETE, PROGNOTES_DELETE);
                    /*Added by gbindra MITS#34104 WWIG GAP15 START*/
                    if (objDocument.ClaimantId > 0 && objSysSetting.AllowNotesAtClaimant == true)
                    {
                        switch (objDocument.LOB)
                        {
                            case "GC":
                                if (!(base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
                                    throw new PermissionViolationException(RMPermissions.RMO_NOPROGRESS_NOTES_DELETE, GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET);
                                break;
                            case "VA":
                                if (!(base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
                                    throw new PermissionViolationException(RMPermissions.RMO_NOPROGRESS_NOTES_DELETE, VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET);
                                break;
                            case "PC":
                                if (!(base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
                                    throw new PermissionViolationException(RMPermissions.RMO_NOPROGRESS_NOTES_DELETE, PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET);
                                break;
                        }
                    }
                    //if (objDocument.EventID != 0 && objDocument.ClaimID == 0)  //Event's Enhanced notes are opening
                    else if (objDocument.EventID != 0 && objDocument.ClaimID == 0)  //Event's Enhanced notes are opening
                    /*Added by gbindra MITS#34104 WWIG GAP15 END*/
	                {
	                    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_DELETE)))
	                        throw new PermissionViolationException(RMPermissions.RMO_NOPROGRESS_NOTES_DELETE, EVENT_PROGNOTES_DELETE);
	                }
	                else if (objDocument.ClaimID != 0)  //Claim's Enhanced notes are opening
	                {
	                    switch (objDocument.LOB)
	                    {
	                        case "GC":
	                            if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
	                                throw new PermissionViolationException(RMPermissions.RMO_NOPROGRESS_NOTES_DELETE, GC_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET);
	                            break;
	                        case "VA":
	                            if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
	                                throw new PermissionViolationException(RMPermissions.RMO_NOPROGRESS_NOTES_DELETE, VA_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET);
	                            break;
	                        case "WC":
	                            if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
	                                throw new PermissionViolationException(RMPermissions.RMO_NOPROGRESS_NOTES_DELETE, WC_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET);
	                            break;
	                        case "DI":
	                            if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
	                                throw new PermissionViolationException(RMPermissions.RMO_NOPROGRESS_NOTES_DELETE, DI_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET);
	                            break;
	                        case "PC":
	                            if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET)))
	                                throw new PermissionViolationException(RMPermissions.RMO_NOPROGRESS_NOTES_DELETE, PC_PROGNOTES_VIEW + PROGNOTES_DELETE_OFFSET);
	                            break;
	                    }
	                }
	                //End - pmittal5
                }
                //Initialize the application layer ProgressNotesManager class
                // Ash - cloud, overload constructor with Client ID
                //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password);
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);
                //williams-neha goel---start:added objDocument.PolicyID::MITS 21704
                //objProgressNotesManager.DeleteNote(objDocument.EventID, objDocument.ClaimID, Convert.ToInt32(objDocument.ClaimProgressNoteId));
                objProgressNotesManager.DeleteNote(objDocument.EventID, objDocument.ClaimID, Convert.ToInt32(objDocument.ClaimProgressNoteId), objDocument.PolicyID, objDocument.ClaimantId); //Added by gbindra MITS#34104 02132014 WWIG GAP15 
                //williams-neha goel:added objDocument.PolicyID---end:MITS 21704

                bIsDeleteIntegrate = true; //Mits id:34851(Smoke issue) - Govind

                return this.OnLoadIntegrate(objDocument, out outDocument, ref p_objErrOut);

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.DeleteProgressNote.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objProgressNotesManager != null)
                {
                    objProgressNotesManager.Dispose();
                    objProgressNotesManager = null;
                }

            }

        }
        public bool PrintProgressNotesIntegrate(ProgressNotesType objDocument, out ProgressNotesType outDocument, ref BusinessAdaptorErrors p_objErrOut)
        {

            ProgressNotesManager objProgressNotesManager = null;
           

            bool bReturnValue = false;
        
            bool bActivateFilter = false;
          
            string sFilterSQL = "";
            string sPdfDocPath = string.Empty;
          int iClaimProgressNoteId = 0;
          int iNoteTypeCodeId = 0;
            bool bPrintSelectedNotes = false;
            //Changed by Gagan for MITS 8697 : End
            outDocument = new ProgressNotesType();

            SysSettings objSysSetting = null;//Added by gbindra MITS#34104 WWIG GAP15
            try
            {
                objSysSetting = new SysSettings(m_connectionString, base.ClientId); //Ash - cloud;//Added by gbindra MITS#34104 WWIG GAP15
                  //willimas--neha goel----start:policy enhanced notes permissions:added if else
                if (!string.IsNullOrEmpty(objDocument.PolicyID.ToString()) && objDocument.PolicyID != 0)
                {
                    if (!(base.userLogin.IsAllowedEx(POLICY_PROGNOTES_PRINT)))
                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesPrint, POLICY_PROGNOTES_PRINT);
                }
                //willimas--neha goel----end:policy enhanced notes permissions
                else
                {
	                //pmittal5 Mits 21514 07/19/10
	               //if (!(base.userLogin.IsAllowedEx(PROGNOTES_PRINT)))
	               //     throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesPrint, PROGNOTES_PRINT);
                    /*Added by gbindra MITS#34104 WWIG GAP 15 START*/
                    if (objDocument.ClaimantId > 0 && objSysSetting.AllowNotesAtClaimant == true)
                    {
                        switch (objDocument.LOB)
                        {
                            case "GC":
                                if (!(base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
                                    throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesPrint, GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET);
                                break;
                            case "VA":
                                if (!(base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
                                    throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesPrint, VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET);
                                break;
                            case "PC":
                                if (!(base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
                                    throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesPrint, PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET);
                                break;
                        }
                    }
                    //if (objDocument.EventID != 0 && objDocument.ClaimID == 0)  //Event's Enhanced notes are opening
                    else if (objDocument.EventID != 0 && objDocument.ClaimID == 0)  //Event's Enhanced notes are opening
                    /*Added by gbindra MITS#34104 WWIG GAP 15 END*/
	                {
	                    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_PRINT)))
	                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesPrint, EVENT_PROGNOTES_PRINT);
	                }
	                else if (objDocument.ClaimID != 0)  //Claim's Enhanced notes are opening
	                {
	                    switch (objDocument.LOB)
	                    {
	                        case "GC":
	                            if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
	                                throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesPrint, GC_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET);
	                            break;
	                        case "VA":
	                            if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
	                                throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesPrint, VA_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET);
	                            break;
	                        case "WC":
	                            if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
	                                throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesPrint, WC_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET);
	                            break;
	                        case "DI":
	                            if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
	                                throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesPrint, DI_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET);
	                            break;
	                        case "PC":
	                            if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
	                                throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesPrint, PC_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET);
	                            break;
	                    }
	                }
	                //End - pmittal5
                }
                //Initialize the application layer ProgressNotesManager class
                // Ash - cloud, overload constructor with Client ID
                //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password);
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);

                if (objDocument.ClaimProgressNoteId != "")
                {
                    bPrintSelectedNotes = true;
                    iClaimProgressNoteId = Convert.ToInt32(objDocument.ClaimProgressNoteId);
                }
                //Added Rakhi for R6-Print Note by Note Type
                if (objDocument.NoteTypeCodeId != "")
                {
                    iNoteTypeCodeId = Convert.ToInt32(objDocument.NoteTypeCodeId);
                }
                //Added Rakhi for R6-Print Note by Note Type

                bActivateFilter = objDocument.ActivateFilter;

                if (bActivateFilter == true)
                {
                    ////Changed by akaushik5 for MITS 30789 Starts
                    // sFilterSQL = objProgressNotesManager.CreateFilterString(objDocument.objFilter.ClaimIDList,objDocument.objFilter.EnteredByList , objDocument.objFilter.NoteTypeList, objDocument.objFilter.UserTypeList, objDocument.objFilter.ActivityFromDate, objDocument.objFilter.ActivityToDate, objDocument.objFilter.NotesTextContains,objDocument.objFilter.SortBy);
                    sFilterSQL = objProgressNotesManager.CreateFilterString(objDocument.objFilter.ClaimIDList, objDocument.objFilter.EnteredByList, objDocument.objFilter.NoteTypeList, objDocument.objFilter.UserTypeList, objDocument.objFilter.ActivityFromDate, objDocument.objFilter.ActivityToDate, objDocument.objFilter.NotesTextContains, objDocument.objFilter.SortBy, objDocument.objFilter.SubjectList, objDocument.objFilter.OrderBy, objDocument.objFilter.ClaimantIDList); //Added by gbindra MITS#34104 02132014 WWIG GAP15 
                    ////Changed by akaushik5 for MITS 30789 Ends
               
                }

                //MITS 26763 : Client requests change to custom print process for Large Notes 
                //modified by Raman Bhatia on 12/06/2011
                objProgressNotesManager.GenerateReportThresholdExceeded = objDocument.iGenerateReportThresholdExceeded;

                //objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//SelectedChoiceThreshholdExceeded");
                //if (objElement == null)
                //{
                //    objExecSumm.GenerateReportThresholdExceeded = (int)ExecutiveSummary.GenerateReportThresholdExceededValues.DoNothing;
                //}
                //else
                //{
                //    iSelectedChoiceThreshholdExceeded = Conversion.ConvertStrToInteger(objElement.InnerText);
                //    if (iSelectedChoiceThreshholdExceeded == 1)
                //        objExecSumm.GenerateReportThresholdExceeded = (int)ExecutiveSummary.GenerateReportThresholdExceededValues.GenerateReport;
                //    else if (iSelectedChoiceThreshholdExceeded == 2)
                //        objExecSumm.GenerateReportThresholdExceeded = (int)ExecutiveSummary.GenerateReportThresholdExceededValues.ScheduleReport;
                //    else
                //        objExecSumm.GenerateReportThresholdExceeded = (int)ExecutiveSummary.GenerateReportThresholdExceededValues.DoNothing;
                //}

                //rsolanki2: execSummaryScheduler Enhacements
                objProgressNotesManager.m_sTMConnectionString = RMConfigurationManager.GetConnectionString("TaskManagerDataSource", base.ClientId);
                objProgressNotesManager.m_UserLogin = base.m_userLogin;				
                //iNoteTypeCodeId:Added By Rakhi for R6-Print Note by Note Type
                //bReturnValue = objProgressNotesManager.PrintProgressNotesReport(objDocument.EventID, objDocument.ClaimID, objDocument.ActivateFilter, sFilterSQL, ref sPdfDocPath, bPrintSelectedNotes, iClaimProgressNoteId);
                //williams-neha goel-start:added objDocument.policyID::MITS 21704
                //averma62 MITS - 27826 bReturnValue = objProgressNotesManager.PrintProgressNotesReport(objDocument.EventID, objDocument.ClaimID, objDocument.ActivateFilter, sFilterSQL, ref sPdfDocPath, bPrintSelectedNotes, iClaimProgressNoteId, iNoteTypeCodeId);
                bReturnValue = objProgressNotesManager.PrintProgressNotesReport(objDocument.EventID, objDocument.ClaimID, objDocument.ActivateFilter, sFilterSQL, ref sPdfDocPath, bPrintSelectedNotes, iClaimProgressNoteId, iNoteTypeCodeId, objDocument.PolicyID, objDocument.ClaimantId);  //averma62 MITS - 27826 - added one more parameter policy id //Added by gbindra MITS#34104 02132014 WWIG GAP15 
                //williams-neha goel-end:added objDocument.policyID::MITS 21704
                //Changed Rakhi for R6-Print Note by Note Type
                // If return value true then copy the binary content of file in the p_objXmlOut doc.
                if (bReturnValue)
                {
                    
                    this.CreatePrgNoteFileContentIntegrate(ref outDocument, sPdfDocPath);
                    return (true);
                }
                return (false);

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.PrintProgressNotesReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objProgressNotesManager != null)
                {
                    objProgressNotesManager.Dispose();
                    objProgressNotesManager = null;
                }
               
            }

        }
        private void CreatePrgNoteFileContentIntegrate( ref ProgressNotesType p_objParentNode , string p_sOutFilepath)
        {
            MemoryStream objMemoryStream = null;
            FileStream objFileStream = null;
            BinaryReader objBReader = null;
            byte[] arrRet = null;
            try
            {
                // Check the file existence.
                if (!File.Exists(p_sOutFilepath))
                    throw new RMAppException(Globalization.GetString("ProgressNotesAdaptor.PrintProgressNotesReport.Error", base.ClientId));

                // Read the file in memory stream.
                objFileStream = new FileStream(p_sOutFilepath, FileMode.Open);
                objBReader = new BinaryReader(objFileStream);
                arrRet = objBReader.ReadBytes((int)objFileStream.Length);
                objMemoryStream = new MemoryStream(arrRet);

                objFileStream.Close();

                // Delete the temp file.
                File.Delete(p_sOutFilepath);

                // Insert the memory stream as base64 string into the the attribute
                p_objParentNode.ProgressNoteReportPdf = Convert.ToBase64String(objMemoryStream.ToArray());
                
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ProgressNotesAdaptor.PrintProgressNotesReport.Error", base.ClientId), p_objException);
            }
            finally
            {
               
                if (objMemoryStream != null)
                {
                    objMemoryStream.Close();
                    objMemoryStream.Dispose();
                }
                if (objFileStream != null)
                {
                    objFileStream.Close();
                    objFileStream.Dispose();
                }
                if (objBReader != null)
                {
                    objBReader.Close();
                    objBReader = null;
                }
            }
        }
        public bool GetNoteDetailsIntegrate(GetNoteDetailsObject objDocument, out GetNoteDetailsObject outDocument, ref BusinessAdaptorErrors p_objErrOut)
        {
            ProgressNotesManager objProgressNotesManager = null;
            outDocument = new GetNoteDetailsObject();
            int iClaimProgressNoteId = 0;
            XmlElement objTempNode = null;

            try
            {

               iClaimProgressNoteId = Conversion.ConvertStrToInteger(objDocument.ClaimProgressNoteId);

                //Initialize the application layer ProgressNotesManager class
               // Ash - cloud, overload constructor with Client ID
                //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password);
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);
                  XmlDocument p_objXmlDocOut = new XmlDocument();
                  objProgressNotesManager.m_UserLogin = base.m_userLogin; //Aman ML Change for getting the notes details.
                p_objXmlDocOut = objProgressNotesManager.GetNoteDetails(iClaimProgressNoteId);

                // rrachev JIRA RMA-12166 Begin
                XmlNode xmlDocumentNode = p_objXmlDocOut.SelectSingleNode("Document");
                outDocument.ClaimProgressNoteId = xmlDocumentNode.Attributes["ProgressNoteID"].Value;
                outDocument.HTML = xmlDocumentNode.SelectSingleNode("HTML").InnerText;
                outDocument.Text = xmlDocumentNode.SelectSingleNode("Text").InnerText;
                outDocument.NoteTypeCode = xmlDocumentNode.SelectSingleNode("NoteTypeCode").InnerText;
                outDocument.NoteTypeDesc = xmlDocumentNode.SelectSingleNode("NoteTypeDesc").InnerText;
                outDocument.TemplateId = xmlDocumentNode.SelectSingleNode("TemplateId").InnerText;
                outDocument.TemplateName = xmlDocumentNode.SelectSingleNode("TemplateName").InnerText;
                outDocument.ActivityDate = xmlDocumentNode.SelectSingleNode("ActivityDate").InnerText;
                outDocument.DataChangedByInitScript = Conversion.ConvertStrToBool(xmlDocumentNode.SelectSingleNode("DataChangedByInitScript").InnerText);
                //Ashish Ahuja -Mits 33124
                outDocument.Subject = xmlDocumentNode.SelectSingleNode("Subject").InnerText;
                //outDocument.ClaimProgressNoteId = objDocument.ClaimProgressNoteId;
                //outDocument.ClaimProgressNoteId = p_objXmlDocOut.SelectSingleNode("Document").Attributes["ProgressNoteID"].Value;
                //outDocument.HTML = ((XmlElement)(p_objXmlDocOut.SelectSingleNode("Document"))).SelectSingleNode("HTML").InnerText;
                //outDocument.Text = ((XmlElement)(p_objXmlDocOut.SelectSingleNode("Document"))).SelectSingleNode("Text").InnerText;
                //outDocument.NoteTypeCode = ((XmlElement)(p_objXmlDocOut.SelectSingleNode("Document"))).SelectSingleNode("NoteTypeCode").InnerText;
                //outDocument.NoteTypeDesc = ((XmlElement)(p_objXmlDocOut.SelectSingleNode("Document"))).SelectSingleNode("NoteTypeDesc").InnerText;
                //outDocument.TemplateId = ((XmlElement)(p_objXmlDocOut.SelectSingleNode("Document"))).SelectSingleNode("TemplateId").InnerText;
                //outDocument.TemplateName = ((XmlElement)(p_objXmlDocOut.SelectSingleNode("Document"))).SelectSingleNode("TemplateName").InnerText;
                //outDocument.ActivityDate = ((XmlElement)(p_objXmlDocOut.SelectSingleNode("Document"))).SelectSingleNode("ActivityDate").InnerText;
                //outDocument.DataChangedByInitScript = Conversion.ConvertStrToBool(((XmlElement)(p_objXmlDocOut.SelectSingleNode("Document"))).SelectSingleNode("DataChangedByInitScript").InnerText);
                ////Ashish Ahuja -Mits 33124
                //outDocument.Subject = ((XmlElement)(p_objXmlDocOut.SelectSingleNode("Document"))).SelectSingleNode("Subject").InnerText;
                // rrachev JIRA RMA-12166 End
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.GetNoteDetailsIntegrate.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objProgressNotesManager != null)
                {
                    objProgressNotesManager.Dispose();
                    objProgressNotesManager = null;
                }

            }
        }
        public bool SelectClaimIntegrate(SelectClaimObject objDocument, out SelectClaimObject outDocument, ref BusinessAdaptorErrors p_objErrOut)
        {
            long lEventID = 0;
            string sNewRecord = ""; //Geeta 08/23/07 : MITS 10221
            outDocument = new SelectClaimObject();
            ProgressNotesManager objProgressNotesManager = null;
            ColLobSettings objcol = null; //mbahl3 mits 30513
            LocalCache objCache = null; //mbahl3 mits 30513
            try
            {
                if (objDocument.NewRecord != null || objDocument.NewRecord != "")
                {
                    sNewRecord = objDocument.NewRecord;
                    //MGaba2:MITS 22173: Separate branch for Templates
                    if (!(base.userLogin.IsAllowedEx(TEMPLATES, TEMPLATES_VIEW_OFFSET))) //Copied from below so that permissions are checked separately for Events and LOBs
                        outDocument.bViewTemplates = false;
                    else
                        outDocument.bViewTemplates = true;

                    //if (sNewRecord == "false")  //pmittal5 Mits 21514
                    //mbahl3 mits 30513 Enhance notes enhancement 
					if (!objDocument.NotesLevelDropdown)
                    {
                        if (sNewRecord.ToLower() == "false")
                        {
                            //willimas--neha goel----start:policy enhanced notes permissions:added if else
                            if ((!string.IsNullOrEmpty(objDocument.PolicyID)) && (Convert.ToInt32(objDocument.PolicyID) != 0))
                            {
                                if (!(base.userLogin.IsAllowedEx(POLICY_PROGNOTES_EDIT)))
                                    throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotes, POLICY_PROGNOTES_EDIT);

                                return (true);

                            }
                            else
                            {   //willimas--neha goel----end:policy enhanced notes permissions
                                //pmittal5 Mits 21514
                                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_EDIT)))
                                //    throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesEdit, PROGNOTES_EDIT);
                                if (objDocument.EventID != "0" && objDocument.EventID != "" && (objDocument.ClaimID == "0" || objDocument.ClaimID == ""))  //Event's Enhanced notes are opening
                                {
                                    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_EDIT)))
                                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesEdit, EVENT_PROGNOTES_EDIT);
                                    //MGaba2:MITS 22173: Separate branch for Templates
                                    //if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_TEMPVIEW))) //Copied from below so that permissions are checked separately for Events and LOBs
                                    //    outDocument.bViewTemplates = false;
                                    //else
                                    //    outDocument.bViewTemplates = true;
                                }
                                else if (objDocument.ClaimID != "0" && objDocument.ClaimID != "")  //Claim's Enhanced notes are opening
                                {
                                    switch (objDocument.LOB)
                                    {
                                        case "GC":
                                            if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                                                throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesEdit, GC_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET);
                                            //MGaba2:MITS 22173: Separate branch for Templates
                                            //if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET))) //Copied from below so that permissions are checked separately for Events and LOBs
                                            //    outDocument.bViewTemplates = false;
                                            //else
                                            //    outDocument.bViewTemplates = true;
                                            break;
                                        case "VA":
                                            if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                                                throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesEdit, VA_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET);
                                            //MGaba2:MITS 22173: Separate branch for Templates
                                            //if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET))) 
                                            //    outDocument.bViewTemplates = false;
                                            //else
                                            //    outDocument.bViewTemplates = true;
                                            break;
                                        case "WC":
                                            if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                                                throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesEdit, WC_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET);
                                            //MGaba2:MITS 22173: Separate branch for Templates
                                            //if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                                            //    outDocument.bViewTemplates = false;
                                            //else
                                            //    outDocument.bViewTemplates = true;
                                            break;
                                        case "DI":
                                            if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                                                throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesEdit, DI_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET);
                                            //MGaba2:MITS 22173: Separate branch for Templates
                                            //if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                                            //    outDocument.bViewTemplates = false;
                                            //else
                                            //    outDocument.bViewTemplates = true;
                                            break;
                                        case "PC":
                                            if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                                                throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesEdit, PC_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET);
                                            //MGaba2:MITS 22173: Separate branch for Templates
                                            //if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                                            //    outDocument.bViewTemplates = false;
                                            //else
                                            //    outDocument.bViewTemplates = true;
                                            break;
                                    }
                                }
                                //End - pmittal5
                            }//willimas:neha goel:else end for Policy Permissions
                        }
                        else
                        {
                            //pmittal5 Mits 21514
                            //if (!(base.userLogin.IsAllowedEx(PROGNOTES_CREATE)))
                            //    throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesCreate, PROGNOTES_CREATE);
                            if (objDocument.EventID != "0" && objDocument.EventID != "" && (objDocument.ClaimID == "0" || objDocument.ClaimID == ""))  //Event's Enhanced notes are opening
                            {
                                if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_CREATE)))
                                    throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesCreate, EVENT_PROGNOTES_CREATE);
                                //MGaba2:MITS 22173: Separate branch for Templates
                                //if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_TEMPVIEW))) 
                                //    outDocument.bViewTemplates = false;
                                //else
                                //    outDocument.bViewTemplates = true;
                            }
                            else if (objDocument.ClaimID != "0" && objDocument.ClaimID != "")  //Claim's Enhanced notes are opening
                            {
                                switch (objDocument.LOB)
                                {
                                    case "GC":
                                        if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                                            throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesCreate, GC_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET);
                                        //MGaba2:MITS 22173: Separate branch for Templates
                                        //if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                                        //    outDocument.bViewTemplates = false;
                                        //else
                                        //    outDocument.bViewTemplates = true;
                                        break;
                                    case "VA":
                                        if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                                            throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesCreate, VA_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET);
                                        //MGaba2:MITS 22173: Separate branch for Templates
                                        //if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                                        //    outDocument.bViewTemplates = false;
                                        //else
                                        //    outDocument.bViewTemplates = true;
                                        break;
                                    case "WC":
                                        if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                                            throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesCreate, WC_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET);
                                        //MGaba2:MITS 22173: Separate branch for Templates
                                        //if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                                        //    outDocument.bViewTemplates = false;
                                        //else
                                        //    outDocument.bViewTemplates = true;
                                        break;
                                    case "DI":
                                        if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                                            throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesCreate, DI_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET);
                                        //MGaba2:MITS 22173: Separate branch for Templates
                                        //if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                                        //    outDocument.bViewTemplates = false;
                                        //else
                                        //    outDocument.bViewTemplates = true;
                                        break;
                                    case "PC":
                                        if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                                            throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesCreate, PC_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET);
                                        //MGaba2:MITS 22173: Separate branch for Templates
                                        //if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                                        //    outDocument.bViewTemplates = false;
                                        //else
                                        //    outDocument.bViewTemplates = true;
                                        break;
                                }
                            }
                            //End - pmittal5
                        }
                    }
                }

               lEventID = Conversion.ConvertStrToLong(objDocument.EventID);

                //Initialize the application layer ProgressNotesManager class
                // Ash - cloud, overload constructor with Client ID
                //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password);
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);
                XmlDocument p_objXmlOut = new XmlDocument();
                objProgressNotesManager.m_UserLogin = base.m_userLogin; //Aman ML Change
                p_objXmlOut = objProgressNotesManager.SelectClaim(lEventID);
                 
                outDocument.EventID = objDocument.EventID;
                outDocument.NewRecord = objDocument.NewRecord;

                //Start rsushilaggar MITS 21119 06/18/2010
                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_TEMPVIEW)))
                //    outDocument.bViewTemplates = false;
                //else
                //    outDocument.bViewTemplates = true;
                //End rsushilaggar MITS 21119 06/18/2010

                //pmittal5 Mits 21514
                    if (outDocument.NewRecord.ToLower() == "true")
                    {
                        if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_CREATE)))
                            outDocument.SelectEvent = false;
                        else
                            outDocument.SelectEvent = objDocument.SelectEvent;
                    }
                    else if (outDocument.NewRecord.ToLower() == "false")
                    {
                        if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_EDIT)))
                            outDocument.SelectEvent = false;
                        else
                            outDocument.SelectEvent = objDocument.SelectEvent;
                    }
 				//mbahl3 mits 30513 Enhance notes enhancement 
                    if (objDocument.NotesLevelDropdown)
                    {
					
                        outDocument.SelectEvent = objDocument.SelectEvent; 
                    }
                //End - pmittal5
                //End rsushilaggar MITS 21119 06/18/2010
                outDocument.NotesLevelDropdown = objDocument.NotesLevelDropdown; //mbahl3 mits 30513
                if(p_objXmlOut.GetElementsByTagName("Claim").Count >0)
                {
                    for (int x = 0; x < p_objXmlOut.GetElementsByTagName("Claim").Count; x++)
                    {

                        XmlElement xmlNode = (XmlElement)p_objXmlOut.GetElementsByTagName("Claim")[x];
                        ClaimInfo clmInfo = new ClaimInfo();
                        clmInfo.ClaimID = xmlNode.Attributes["ClaimID"].Value;
                        clmInfo.ClaimNumber = xmlNode.Attributes["ClaimNumber"].Value;
                        clmInfo.ClaimDate = xmlNode.Attributes["ClaimDate"].Value;
                        clmInfo.ClaimStatusDesc = xmlNode.Attributes["ClaimStatusDesc"].Value;
                        clmInfo.ClaimTypeCode = xmlNode.Attributes["ClaimTypeCode"].Value;
                        clmInfo.LOB = xmlNode.Attributes["LOB"].Value;
                        clmInfo.ClaimantName = xmlNode.Attributes["ClaimantName"].Value;
						//mbahl3 mits 30513
                        if (objCache == null)
                            objCache = new LocalCache(m_connectionString, base.ClientId);

                        objcol = new ColLobSettings(m_connectionString, base.ClientId);

                        int iUseClaimProgressNotes=0 ;
                        if(objcol[Conversion.ConvertStrToInteger(clmInfo.LOB)]!=null)
                            iUseClaimProgressNotes = objcol[Conversion.ConvertStrToInteger( clmInfo.LOB)].ClmProgressNotesFlag;

							//mbahl3 mits 30513
                        //pmittal5 Mits 21514 - Populate only those Claims in dropdown list for which Create or View permissions are given
                        switch (clmInfo.LOB)
                        {
                            case "241":  //General Claims
                                if (outDocument.NewRecord.ToLower() == "advancesearch")  //For Advanced Search
                                {
                                    if ((base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW)))
                                        outDocument.objClaimList.Add(clmInfo);
                                }
                                else if (outDocument.NewRecord.ToLower() == "true")       //For New Record
                                {
                                    if ((base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                                        outDocument.objClaimList.Add(clmInfo);
                                } 
                                else if (outDocument.NotesLevelDropdown)  //mbahl3 mits 30513
                                {
                                    if ( (iUseClaimProgressNotes !=0 ) && (base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW)))
                                       
									    outDocument.objClaimList.Add(clmInfo);
                                }
                                else
                                    outDocument.objClaimList.Add(clmInfo);
                                break;
                            case "242":  //Vehicle Accident
                                if (outDocument.NewRecord.ToLower() == "advancesearch")
                                {
                                    if ((base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW)))
                                        outDocument.objClaimList.Add(clmInfo);
                                }
                                else if (outDocument.NewRecord.ToLower() == "true")
                                {
                                    if ((base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                                        outDocument.objClaimList.Add(clmInfo);
                                }
                                else if (outDocument.NotesLevelDropdown) //mbahl3 mits 30513
                                {
                                    if ((iUseClaimProgressNotes != 0) && (base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW )))
                                        outDocument.objClaimList.Add(clmInfo);
                                }
                                else
                                    outDocument.objClaimList.Add(clmInfo);
                                break;
                            case "243":  //Workers Compensation Claim
                                if (outDocument.NewRecord.ToLower() == "advancesearch")
                                {
                                    if ((base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW)))
                                        outDocument.objClaimList.Add(clmInfo);
                                }
                                else if (outDocument.NewRecord.ToLower() == "true")
                                {
                                    if ((base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                                        outDocument.objClaimList.Add(clmInfo);
                                }
                                else if (outDocument.NotesLevelDropdown) //mbahl3 mits 30513
                                {
                                    if ((iUseClaimProgressNotes != 0) && (base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW)))
                                        outDocument.objClaimList.Add(clmInfo);
                                }
                                else
                                    outDocument.objClaimList.Add(clmInfo);
                                break;
                            case "844":   //Non-Occ Claims
                                if (outDocument.NewRecord.ToLower() == "advancesearch")
                                {
                                    if ((base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW)))
                                        outDocument.objClaimList.Add(clmInfo);
                                }
                                else if (outDocument.NewRecord.ToLower() == "true")
                                {
                                    if ((base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                                        outDocument.objClaimList.Add(clmInfo);
                                }
                                else if (outDocument.NotesLevelDropdown) //mbahl3 mits 30513
                                {
                                    if ((iUseClaimProgressNotes != 0) && (base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW )))
                                        outDocument.objClaimList.Add(clmInfo);
                                }
                                else
                                    outDocument.objClaimList.Add(clmInfo);
                                break;
                            case "845":  //Property Claims
                                if (outDocument.NewRecord.ToLower() == "advancesearch")
                                {
                                    if ((base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW)))
                                        outDocument.objClaimList.Add(clmInfo);
                                }
                                else if (outDocument.NewRecord.ToLower() == "true")
                                {
                                    if ((base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW)))
                                        outDocument.objClaimList.Add(clmInfo);
                                } 
                                else if (outDocument.NotesLevelDropdown) //mbahl3 mits 30513
                                {
                                    if ((iUseClaimProgressNotes != 0) && (base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW)))
                                        outDocument.objClaimList.Add(clmInfo);
                                }
                                else
                                    outDocument.objClaimList.Add(clmInfo);
                                break;
                        }
                        
                        //outDocument.objClaimList.Add(clmInfo);
                        //End - pmittal5
                    }
               }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.SelectClaim.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objProgressNotesManager != null)
                {
                    objProgressNotesManager.Dispose();
                    objProgressNotesManager = null;
                }

                    objcol = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
            return (true);

        }
        /*GBINDRA MITS#34104 02042014 WWIG GAP15 START*/
        public bool SelectClaimantIntegrate(SelectClaimantObject objDocument, out SelectClaimantObject outDocument, ref BusinessAdaptorErrors p_objErrOut)
        {
            long lEventID = 0;
            string sNewRecord = "";
            bool bSuccess = false;
            outDocument = new SelectClaimantObject();
            ProgressNotesManager objProgressNotesManager = null;
            ColLobSettings objcol = null;
            LocalCache objCache = null;
            SysSettings objSysSetting = null;
            long lClaimID = 0;

            try
            {
                objSysSetting = new SysSettings(m_connectionString, base.ClientId); //Ash - cloud;
                if (objDocument.NewRecord != null || objDocument.NewRecord != "")
                {
                    sNewRecord = objDocument.NewRecord;
                    // Pshekhawat 6/25/2014 - JIRA RMA-200
                    //if (!objDocument.NotesLevelDropdown)
                    //{
                    //    if (sNewRecord.ToLower() == "false")
                    //    {
                    //        if (objSysSetting.AllowNotesAtClaimant == true)
                    //        {
                    //            switch (objDocument.LOB)
                    //            {
                    //                case "GC":
                    //                    if (!(base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                    //                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesEdit, GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET);
                    //                    break;
                    //                case "VA":
                    //                    if (!(base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                    //                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesEdit, VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET);
                    //                    break;
                    //                case "PC":
                    //                    if (!(base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET)))
                    //                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesEdit, PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_EDIT_OFFSET);
                    //                    break;
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (objSysSetting.AllowNotesAtClaimant == true)
                    //        {
                    //            switch (objDocument.LOB)
                    //            {
                    //                case "GC":
                    //                    if (!(base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                    //                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesCreate, GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET);
                    //                    break;
                    //                case "VA":
                    //                    if (!(base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                    //                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesCreate, VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET);
                    //                    break;
                    //                case "PC":
                    //                    if (!(base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                    //                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesCreate, PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET);
                    //                    break;
                    //            }
                    //        }
                    //    }
                    //}
                }

                lEventID = Conversion.CastToType<long>(objDocument.EventID, out bSuccess);
                lClaimID = Conversion.CastToType<long>(objDocument.ClaimID, out bSuccess);

                //Initialize the application layer ProgressNotesManager class
                // Ash - cloud, overload constructor with Client ID
                //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password);
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);
                XmlDocument p_objXmlOut = new XmlDocument();
                objProgressNotesManager.m_UserLogin = base.m_userLogin;
                p_objXmlOut = objProgressNotesManager.SelectClaimant(lEventID, lClaimID);

                outDocument.EventID = objDocument.EventID;
                outDocument.NewRecord = objDocument.NewRecord;

                outDocument.NotesLevelDropdown = objDocument.NotesLevelDropdown;
                if (p_objXmlOut.GetElementsByTagName("Claimant").Count > 0 && objSysSetting.AllowNotesAtClaimant == true)
                {
                    for (int x = 0; x < p_objXmlOut.GetElementsByTagName("Claimant").Count; x++)
                    {
                        if (objCache == null)
                            objCache = new LocalCache(m_connectionString, base.ClientId);

                        XmlElement xmlNode = (XmlElement)p_objXmlOut.GetElementsByTagName("Claimant")[x];
                        ClaimantInfo clmntInfo = new ClaimantInfo();
                        clmntInfo.ClaimID = xmlNode.Attributes["ClaimID"].Value;
                        clmntInfo.ClaimantRowId = Conversion.CastToType<int>(xmlNode.Attributes["ClaimantRowId"].Value, out bSuccess);
                        clmntInfo.ClaimantFirstName = xmlNode.Attributes["ClaimantFirstName"].Value;
                        clmntInfo.ClaimantLastName = xmlNode.Attributes["ClaimantLastName"].Value;
                        clmntInfo.ClaimNumber = xmlNode.Attributes["ClaimNumber"].Value;
                        clmntInfo.LOB = objCache.GetShortCode(Conversion.CastToType<int>(xmlNode.Attributes["LOB"].Value, out bSuccess));


                        objcol = new ColLobSettings(m_connectionString, base.ClientId);

                        //int iUseClaimProgressNotes = objcol[Conversion.CastToType<int>(xmlNode.Attributes["LOB"].Value, out bSuccess)].ClmProgressNotesFlag;//Change for JIRA-332
                        int iUseClaimProgressNotes = (Conversion.CastToType<int>(xmlNode.Attributes["LOB"].Value, out bSuccess) == 0) ? 0 :
                            objcol[Conversion.CastToType<int>(xmlNode.Attributes["LOB"].Value, out bSuccess)].ClmProgressNotesFlag;

                        switch (clmntInfo.LOB)
                        {
                            case "GC":  //General Claims
                                if (outDocument.NewRecord.ToLower() == "advancesearch")  //For Advanced Search
                                {
                                    if ((base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW)))
                                        outDocument.objClaimantList.Add(clmntInfo);
                                }
                                else if (outDocument.NewRecord.ToLower() == "true")       //For New Record
                                {
                                    if ((base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                                        outDocument.objClaimantList.Add(clmntInfo);
                                }
                                else if (outDocument.NotesLevelDropdown)
                                {
                                    if ((iUseClaimProgressNotes != 0) && (base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW)))
                                        outDocument.objClaimantList.Add(clmntInfo);
                                }
                                else
                                    if ((base.userLogin.IsAllowedEx(GC_CLMNT_PROGNOTES_VIEW)))
                                        outDocument.objClaimantList.Add(clmntInfo);
                                break;
                            case "VA":  //Vehicle Accident
                                if (outDocument.NewRecord.ToLower() == "advancesearch")
                                {
                                    if ((base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW)))
                                        outDocument.objClaimantList.Add(clmntInfo);
                                }
                                else if (outDocument.NewRecord.ToLower() == "true")
                                {
                                    if ((base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                                        outDocument.objClaimantList.Add(clmntInfo);
                                }
                                else if (outDocument.NotesLevelDropdown)
                                {
                                    if ((iUseClaimProgressNotes != 0) && (base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW)))
                                        outDocument.objClaimantList.Add(clmntInfo);
                                }
                                else
                                    if ((base.userLogin.IsAllowedEx(VA_CLMNT_PROGNOTES_VIEW)))
                                        outDocument.objClaimantList.Add(clmntInfo);
                                break;
                            case "PC":  //Property Claims
                                if (outDocument.NewRecord.ToLower() == "advancesearch")
                                {
                                    if ((base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW)))
                                        outDocument.objClaimantList.Add(clmntInfo);
                                }
                                else if (outDocument.NewRecord.ToLower() == "true")
                                {
                                    if ((base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW + PROGNOTES_CREATE_OFFSET)))
                                        outDocument.objClaimantList.Add(clmntInfo);
                                }
                                else if (outDocument.NotesLevelDropdown)
                                {
                                    if ((iUseClaimProgressNotes != 0) && (base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW)))
                                        outDocument.objClaimantList.Add(clmntInfo);
                                }
                                else
                                    if ((base.userLogin.IsAllowedEx(PC_CLMNT_PROGNOTES_VIEW)))
                                        outDocument.objClaimantList.Add(clmntInfo);
                                break;
                        }

                    }
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.SelectClaimant.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objProgressNotesManager != null)
                {
                    objProgressNotesManager.Dispose();
                    objProgressNotesManager = null;
                }

                objcol = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
            return (true);

        }
        /*GBINDRA MITS#34104 02042014 WWIG GAP15 END*/
        public bool SaveNotesIntegrate(ProgressNote objDocument, out ProgressNote outDocument, ref BusinessAdaptorErrors p_objErrOut)
        {
            int iEventID = 0;
            int iClaimID = 0;
            //williams-neha goel:start:MITS 21704
            int iPolicyID = 0;
            //williams-neha goel:end:MITS 21704
            int iNoteTypeCode = 0;
            int iEnteredBy = 0;
            int iClaimProgressNoteId = 0;
            int iUserID = 0;
            bool bNewRecord = false;
            string sNoteMemo = string.Empty;
            string sNoteMemoCareTech = string.Empty;
            string sDateEntered = string.Empty;
            string sEnteredByName = string.Empty;
            string sSubject = string.Empty; //zmohammad MITS 30218
            int iTemplateId = 0; //Added by Rakhi for R6-Save Template ID attached to a note.
            string sAttachedTable = string.Empty; //added by GBINDRA 02112014 MITS#34104 WWIG gap15
            int iAttachedRecordId = 0; //added by GBINDRA 02112014 MITS#34104 WWIG gap15

			DbReader objReader = null;

            XmlElement objXmlUser = null;
            ProgressNotesManager objProgressNotesManager = null;
            outDocument = new ProgressNote();
            try
            {
                bNewRecord = Conversion.ConvertStrToBool(objDocument.NewRecord);
				//akaur9 09/27/2011 Mobile Adjuster
                if (objDocument.bIsMobileAdjuster || objDocument.bIsMobilityAdjuster)
                {
				if (bNewRecord && Conversion.ConvertStrToInteger(objDocument.ClaimID)<0)
				{
					string sSQL = "SELECT CLAIM_ID,EVENT_ID FROM CLAIM WHERE CLAIM_NUMBER='" + objDocument.ClaimNumber + "'";

					using (objReader = DbFactory.ExecuteReader(this.connectionString, sSQL))
					{
						if (objReader.Read())
						{
							objDocument.ClaimID = Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_ID"));
							objDocument.EventID = Conversion.ConvertObjToStr(objReader.GetValue("EVENT_ID"));
							iClaimID = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), base.ClientId);
							iEventID = Conversion.ConvertObjToInt(objReader.GetValue("EVENT_ID"), base.ClientId);
						}
					}
				}
                //rsharma220 MITS 35325 Start
                else if (bNewRecord && Conversion.ConvertStrToInteger(objDocument.EventID) < 0)
                {
                    string sSQL = "SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER='" + objDocument.EventNumber + "'";

                    using (objReader = DbFactory.ExecuteReader(this.connectionString, sSQL))
                    {
                        if (objReader.Read())
                        {
                            objDocument.EventID = Conversion.ConvertObjToStr(objReader.GetValue("EVENT_ID"));
                            iEventID = Conversion.ConvertObjToInt(objReader.GetValue("EVENT_ID"), base.ClientId);
                        }
                    }

                }
                    //rsharma220 MITS 35325 End
				}
				else
				{
                iEventID = Conversion.ConvertStrToInteger(objDocument.EventID);
                iClaimID = Conversion.ConvertStrToInteger(objDocument.ClaimID);
                //williams-neha goel--start:MITS 21704
                iPolicyID = Conversion.ConvertStrToInteger(objDocument.PolicyID);
                //williams-neha goel--end:MITS 21704
				}
                iNoteTypeCode = Conversion.ConvertStrToInteger(objDocument.NoteTypeCode);
                /*added by GBINDRA 02112014 MITS#34104 WWIG gap15 START*/
                iAttachedRecordId = objDocument.AttachedRecordId;
                sAttachedTable = objDocument.AttachedTable;
                /*added by GBINDRA 02112014 MITS#34104 WWIG gap15 END*/

                sDateEntered = objDocument.DateEntered;
                sNoteMemo = objDocument.NoteMemo;
                sNoteMemoCareTech = objDocument.NoteMemoCareTech;
                sSubject = objDocument.Subject; //zmohammad MITS 30218

                if (bNewRecord == true)
                {
                    iClaimProgressNoteId = 0;
                    iUserID = base.userLogin.UserId;
                    iEnteredBy = iUserID;
                    sEnteredByName = base.userLogin.LoginName;

                    ////rsolanki2 :mits 23101 : sort by firstname + lastname issue
                    //if (! Riskmaster.Security.Authentication.PublicFunctions.GetFullNameFromLoginName(base.userLogin.LoginName, out sEnteredByName))
                    //{
                    //    sEnteredByName = base.userLogin.LoginName;
                    //}

                }
                else
                {
                    iClaimProgressNoteId = objDocument.ClaimProgressNoteId;
                }
                iTemplateId = objDocument.TemplateID;

                //Initialize the application layer ProgressNotesManager class
                // Ash - cloud, overload constructor with Client ID
                //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password);
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);
                XmlDocument p_objXmlOut = new XmlDocument();
                outDocument = objDocument;
                //iTemplateId:Added by Rakhi for R6-Progress Notes Templates
               // outDocument.ClaimProgressNoteId = objProgressNotesManager.SaveNotesIntegrate(iEventID, iClaimID, iEnteredBy, sDateEntered, iNoteTypeCode, iUserID, sNoteMemo, sNoteMemoCareTech, sEnteredByName, iClaimProgressNoteId);
                //iPolicyID:Added by Neha for williams Policy Enhanced Notes:MITS 21704
                //outDocument.ClaimProgressNoteId = objProgressNotesManager.SaveNotesIntegrate(iEventID, iClaimID, iEnteredBy, sDateEntered, iNoteTypeCode, iUserID, sNoteMemo, sNoteMemoCareTech, sEnteredByName, iClaimProgressNoteId, iTemplateId, ref p_objErrOut);
                outDocument.ClaimProgressNoteId = objProgressNotesManager.SaveNotesIntegrate(iEventID, iClaimID, iEnteredBy, sDateEntered, sSubject, iNoteTypeCode, iUserID, sNoteMemo, sNoteMemoCareTech, sEnteredByName, iClaimProgressNoteId, iTemplateId, iPolicyID, sAttachedTable, iAttachedRecordId, ref p_objErrOut); //Added by gbindra MITS#34104 02132014 WWIG GAP15 
                 //mbahl3 cloud changes for mobile
                if (objDocument.bIsMobileAdjuster || objDocument.bIsMobilityAdjuster)
                {
                    string sCreateDate=string.Empty;
                    string sCreateTime=string.Empty;

                    objProgressNotesManager.GetNotesDateTimeForMobile(outDocument.ClaimProgressNoteId, ref sCreateDate, ref sCreateTime);

                    outDocument.DateCreated = sCreateDate;
                    outDocument.TimeCreated = sCreateTime;

                }
                //mbahl3 cloud changes for mobile
                //rsolanki2: recent claim updates  MITS 18828
                if (iClaimID != 0)
                {
                    CommonFunctions.UpdateRecentRecords(this.connectionString
                        , this.userLogin.UserId.ToString()
                        , "claim"
                        , objDocument.ClaimID
                        , objDocument.ClaimNumber, base.ClientId);
                }
                else 
	            {
                    CommonFunctions.UpdateRecentRecords(this.connectionString
                        , this.userLogin.UserId.ToString()
                        , "event"
                        , objDocument.EventID
                        , objDocument.EventNumber, base.ClientId);
                }
	
            }
            catch (RMAppException p_objException)
            {
                foreach (BusinessAdaptorError objErr in p_objErrOut)
                {
                    if (objErr.oException == p_objException)
                    {
                        return false; //already added
                    }
                }
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.SaveNotes.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objXmlUser = null;
                if (objProgressNotesManager != null)
                {
                    objProgressNotesManager.Dispose();
                    objProgressNotesManager = null;
                }
            }
            return true; 
        }
        #endregion
        #region Added Rakhi for R6-Enhanced Notes Templates
        public bool SaveTemplates(ProgressNoteTemplates objDocument, out ProgressNoteTemplates outDocument, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bNewRecord = false;
            int iTemplateId = 0;
            string sTemplateName = string.Empty;
            string sTemplateMemo = string.Empty;
            string sDateAdded = string.Empty;
            string sDateUpdated = string.Empty;
            string sUpdatedBy = string.Empty;
            string sAddedBy = string.Empty;


            ProgressNotesManager objProgressNotesManager = null;
            outDocument = new ProgressNoteTemplates();
            try
            {
                bNewRecord = Convert.ToBoolean(objDocument.NewRecord);

                iTemplateId = Convert.ToInt32(objDocument.TemplateId);
               

                if (bNewRecord == true)
                {
                    iTemplateId = 0;
                    sTemplateName = objDocument.TemplateName;
                    sTemplateMemo = objDocument.TemplateMemo;
                    sAddedBy = base.userLogin.LoginName;
                    sUpdatedBy = base.userLogin.LoginName;
                    sDateAdded = objDocument.DateAdded;
                    sDateUpdated = objDocument.DateUpdated;
                }
                else
                {
                    iTemplateId = Convert.ToInt32(objDocument.TemplateId);
                    sTemplateName = objDocument.TemplateName;
                    sTemplateMemo = objDocument.TemplateMemo;
                    sUpdatedBy = base.userLogin.LoginName;
                    sDateUpdated = objDocument.DateUpdated;
                }


                //Initialize the application layer ProgressNotesManager class
                // Ash - cloud, overload constructor with Client ID
                //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password);
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);
                XmlDocument p_objXmlOut = new XmlDocument();
                outDocument.TemplateId=objProgressNotesManager.SaveTemplates(iTemplateId, sTemplateName, sTemplateMemo, sAddedBy, sUpdatedBy, sDateAdded, sDateUpdated);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.SaveNotes.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                
                if (objProgressNotesManager != null)
                {
                    objProgressNotesManager.Dispose();
                    objProgressNotesManager = null;
                }
            }
            return true; 
        }
        public bool DeleteTemplate(ProgressNoteTemplates objDocument, out ProgressNoteTemplates outDocument, ref BusinessAdaptorErrors p_objErrOut)
        {
            int iTemplateId = 0;

            ProgressNotesManager objProgressNotesManager = null;
            outDocument = new ProgressNoteTemplates();
            try
            {
                

                iTemplateId = Convert.ToInt32(objDocument.TemplateId);

                //Initialize the application layer ProgressNotesManager class
                // Ash - cloud, overload constructor with Client ID
                //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password);
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);
                outDocument = objDocument;
                objProgressNotesManager.DeleteTemplate(iTemplateId);
                return this.LoadTemplates(objDocument, out outDocument, ref p_objErrOut);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.SaveNotes.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
               
                if (objProgressNotesManager != null)
                {
                    objProgressNotesManager.Dispose();
                    objProgressNotesManager = null;
                }
            }
            //return true;
        }
        public bool GetTemplateDetails(ProgressNoteTemplates objDocument, out ProgressNoteTemplates outDocument, ref BusinessAdaptorErrors p_objErrOut)
        {
            int iTemplateId = 0;

            ProgressNotesManager objProgressNotesManager = null;
            outDocument = new ProgressNoteTemplates();
            try
            {


                iTemplateId = Convert.ToInt32(objDocument.TemplateId);

                //Initialize the application layer ProgressNotesManager class
                // Ash - cloud, overload constructor with Client ID
                //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password);
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);
                outDocument.TemplateMemo=objProgressNotesManager.GetTemplateDetails(iTemplateId);
                //start rsushilaggar MITS 21119 06/18/2010
                //pmittal5 Mits 21514 - Moved Enhanced Notes permissons under all LOBs and Event
                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_TEMPCREATE)))
                //    outDocument.bCreatePermission = false;
                //else
                //    outDocument.bCreatePermission = true;

                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_TEMPDELETE)))
                //    outDocument.bDeletePermission = false;
                //else
                //    outDocument.bDeletePermission = true;

                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_TEMPEDIT)))
                //    outDocument.bEditPermission = false;
                //else
                //    outDocument.bEditPermission = true;

                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_TEMPVIEW)))
                //    outDocument.bViewPermission = false;
                //else
                //    outDocument.bViewPermission = true;

                 //MGaba2:MITS 22173: Separate branch for Templates:Start
                if (!(base.userLogin.IsAllowedEx(TEMPLATES,TEMPLATES_CREATE_OFFSET)))
                    outDocument.bCreatePermission = false;
                else
                    outDocument.bCreatePermission = true;

                if (!(base.userLogin.IsAllowedEx(TEMPLATES,TEMPLATES_DELETE_OFFSET)))
                    outDocument.bDeletePermission = false;
                else
                    outDocument.bDeletePermission = true;

                if (!(base.userLogin.IsAllowedEx(TEMPLATES,TEMPLATES_EDIT_OFFSET)))
                    outDocument.bEditPermission = false;
                else
                    outDocument.bEditPermission = true;

                if (!(base.userLogin.IsAllowedEx(TEMPLATES,TEMPLATES_VIEW_OFFSET)))
                    outDocument.bViewPermission = false;
                else
                    outDocument.bViewPermission = true;

                //if (objDocument.EventID != 0 && objDocument.ClaimID == 0)
                //{
                //    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_TEMPCREATE)))
                //        outDocument.bCreatePermission = false;
                //    else
                //        outDocument.bCreatePermission = true;

                //    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_TEMPDELETE)))
                //        outDocument.bDeletePermission = false;
                //    else
                //        outDocument.bDeletePermission = true;

                //    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_TEMPEDIT)))
                //        outDocument.bEditPermission = false;
                //else
                //        outDocument.bEditPermission = true;

                //    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_TEMPVIEW)))
                //        outDocument.bViewPermission = false;
                //else
                //        outDocument.bViewPermission = true;
                //}
                //else if (objDocument.ClaimID != 0)
                //{
                //    switch (objDocument.LOB)
                //    {
                //        case "GC":
                //            if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_TEMPCREATE_OFFSET)))
                //                outDocument.bCreatePermission = false;
                //            else
                //                outDocument.bCreatePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_TEMPDELETE_OFFSET)))
                //                outDocument.bDeletePermission = false;
                //            else
                //                outDocument.bDeletePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_TEMPEDIT_OFFSET)))
                //                outDocument.bEditPermission = false;
                //            else
                //                outDocument.bEditPermission = true;

                //            if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                //                outDocument.bViewPermission = false;
                //            else
                //                outDocument.bViewPermission = true;
                //            break;
                    
                //        case "VA":
                //            if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_TEMPCREATE_OFFSET)))
                //                outDocument.bCreatePermission = false;
                //            else
                //                outDocument.bCreatePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_TEMPDELETE_OFFSET)))
                //                outDocument.bDeletePermission = false;
                //            else
                //                outDocument.bDeletePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_TEMPEDIT_OFFSET)))
                //                outDocument.bEditPermission = false;
                //            else
                //                outDocument.bEditPermission = true;

                //            if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                //                outDocument.bViewPermission = false;
                //            else
                //                outDocument.bViewPermission = true;
                //            break;
                        
                //        case "WC":
                //            if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_TEMPCREATE_OFFSET)))
                //                outDocument.bCreatePermission = false;
                //            else
                //                outDocument.bCreatePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_TEMPDELETE_OFFSET)))
                //                outDocument.bDeletePermission = false;
                //            else
                //                outDocument.bDeletePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_TEMPEDIT_OFFSET)))
                //                outDocument.bEditPermission = false;
                //            else
                //                outDocument.bEditPermission = true;

                //            if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                //                outDocument.bViewPermission = false;
                //            else
                //                outDocument.bViewPermission = true;
                //            break;
                        
                //        case "DI":
                //            if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_TEMPCREATE_OFFSET)))
                //                outDocument.bCreatePermission = false;
                //            else
                //                outDocument.bCreatePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_TEMPDELETE_OFFSET)))
                //                outDocument.bDeletePermission = false;
                //            else
                //                outDocument.bDeletePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_TEMPEDIT_OFFSET)))
                //                outDocument.bEditPermission = false;
                //            else
                //                outDocument.bEditPermission = true;

                //            if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                //                outDocument.bViewPermission = false;
                //            else
                //                outDocument.bViewPermission = true;
                //            break;
                        
                //        case "PC":
                //            if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_TEMPCREATE_OFFSET)))
                //                outDocument.bCreatePermission = false;
                //            else
                //                outDocument.bCreatePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_TEMPDELETE_OFFSET)))
                //                outDocument.bDeletePermission = false;
                //            else
                //                outDocument.bDeletePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_TEMPEDIT_OFFSET)))
                //                outDocument.bEditPermission = false;
                //            else
                //                outDocument.bEditPermission = true;

                //            if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                //                outDocument.bViewPermission = false;
                //            else
                //                outDocument.bViewPermission = true;
                //            break;
                //    }
                //}
                //End - pmittal5
                //MGaba2:MITS 22173: End
                //end rsushilaggar
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.SaveNotes.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {

                if (objProgressNotesManager != null)
                {
                    objProgressNotesManager.Dispose();
                    objProgressNotesManager = null;
                }
            }
            return true;
        }
        public bool LoadTemplates(ProgressNoteTemplates objDocument, out ProgressNoteTemplates outDocument, ref BusinessAdaptorErrors p_objErrOut)
        {
            outDocument = new ProgressNoteTemplates();
            ProgressNotesManager objProgressNotesManager = null;
            try
            {
               

                //Initialize the application layer ProgressNotesManager class
                // Ash - cloud, overload constructor with Client ID
                //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password);
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);
                //outDocument = objDocument;  //pmittal5 Mits 21514
                //start rsushilaggar MITS 21119 06/18/2010
                //pmittal5 Mits 21514
                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_TEMPCREATE)))
                //    outDocument.bCreatePermission = false;
                //else
                //    outDocument.bCreatePermission = true;

                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_TEMPDELETE)))
                //    outDocument.bDeletePermission = false;
                //else
                //    outDocument.bDeletePermission = true;

                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_TEMPEDIT)))
                //    outDocument.bEditPermission = false;
                //else
                //    outDocument.bEditPermission = true;

                //if (!(base.userLogin.IsAllowedEx(PROGNOTES_TEMPVIEW)))
                //    outDocument.bViewPermission = false;
                //else
                //{
                //    outDocument.bViewPermission = true;
                //    objProgressNotesManager.LoadTemplates(ref outDocument);
                //}
                //MGaba2:MITS 22173: Separate branch for Templates:Start
                if (!(base.userLogin.IsAllowedEx(TEMPLATES,TEMPLATES_CREATE_OFFSET)))
                    outDocument.bCreatePermission = false;
                else
                    outDocument.bCreatePermission = true;

                if (!(base.userLogin.IsAllowedEx(TEMPLATES,TEMPLATES_DELETE_OFFSET)))
                    outDocument.bDeletePermission = false;
                else
                    outDocument.bDeletePermission = true;

                if (!(base.userLogin.IsAllowedEx(TEMPLATES,TEMPLATES_EDIT_OFFSET)))
                    outDocument.bEditPermission = false;
                else
                    outDocument.bEditPermission = true;

                if (!(base.userLogin.IsAllowedEx(TEMPLATES,TEMPLATES_VIEW_OFFSET)))
                    outDocument.bViewPermission = false;
                else
                {
                    outDocument.bViewPermission = true;
                    objProgressNotesManager.LoadTemplates(ref outDocument);
                }
               
                //if (objDocument.EventID != 0 && objDocument.ClaimID == 0)
                //{
                //    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_TEMPCREATE)))
                //        outDocument.bCreatePermission = false;
                //    else
                //        outDocument.bCreatePermission = true;

                //    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_TEMPDELETE)))
                //        outDocument.bDeletePermission = false;
                //    else
                //        outDocument.bDeletePermission = true;

                //    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_TEMPEDIT)))
                //        outDocument.bEditPermission = false;
                //    else
                //        outDocument.bEditPermission = true;

                //    if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_TEMPVIEW)))
                //        outDocument.bViewPermission = false;
                //    else
                //    {
                //        outDocument.bViewPermission = true;
                //        objProgressNotesManager.LoadTemplates(ref outDocument);
                //    }
                //}
                //else if (objDocument.ClaimID != 0)
                //{
                //    switch (objDocument.LOB)
                //    {
                //        case "GC":
                //            if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_TEMPCREATE_OFFSET)))
                //                outDocument.bCreatePermission = false;
                //            else
                //                outDocument.bCreatePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_TEMPDELETE_OFFSET)))
                //                outDocument.bDeletePermission = false;
                //            else
                //                outDocument.bDeletePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_TEMPEDIT_OFFSET)))
                //                outDocument.bEditPermission = false;
                //            else
                //                outDocument.bEditPermission = true;

                //            if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                //                outDocument.bViewPermission = false;
                //            else
                //            {
                //                outDocument.bViewPermission = true;
                //                objProgressNotesManager.LoadTemplates(ref outDocument);
                //            }
                //            break;

                //        case "VA":
                //            if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_TEMPCREATE_OFFSET)))
                //                outDocument.bCreatePermission = false;
                //            else
                //                outDocument.bCreatePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_TEMPDELETE_OFFSET)))
                //                outDocument.bDeletePermission = false;
                //            else
                //                outDocument.bDeletePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_TEMPEDIT_OFFSET)))
                //                outDocument.bEditPermission = false;
                //            else
                //                outDocument.bEditPermission = true;

                //            if (!(base.userLogin.IsAllowedEx(VA_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                //                outDocument.bViewPermission = false;
                //            else
                //            {
                //                outDocument.bViewPermission = true;
                //                objProgressNotesManager.LoadTemplates(ref outDocument);
                //            }
                //            break;

                //        case "WC":
                //            if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_TEMPCREATE_OFFSET)))
                //                outDocument.bCreatePermission = false;
                //            else
                //                outDocument.bCreatePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_TEMPDELETE_OFFSET)))
                //                outDocument.bDeletePermission = false;
                //            else
                //                outDocument.bDeletePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_TEMPEDIT_OFFSET)))
                //                outDocument.bEditPermission = false;
                //            else
                //                outDocument.bEditPermission = true;

                //            if (!(base.userLogin.IsAllowedEx(WC_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                //                outDocument.bViewPermission = false;
                //            else
                //            {
                //                outDocument.bViewPermission = true;
                //                objProgressNotesManager.LoadTemplates(ref outDocument);
                //            }
                //            break;

                //        case "DI":
                //            if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_TEMPCREATE_OFFSET)))
                //                outDocument.bCreatePermission = false;
                //            else
                //                outDocument.bCreatePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_TEMPDELETE_OFFSET)))
                //                outDocument.bDeletePermission = false;
                //            else
                //                outDocument.bDeletePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_TEMPEDIT_OFFSET)))
                //                outDocument.bEditPermission = false;
                //            else
                //                outDocument.bEditPermission = true;

                //            if (!(base.userLogin.IsAllowedEx(DI_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                //                outDocument.bViewPermission = false;
                //            else
                //            {
                //                outDocument.bViewPermission = true;
                //                objProgressNotesManager.LoadTemplates(ref outDocument);
                //            }
                //            break;

                //        case "PC":
                //            if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_TEMPCREATE_OFFSET)))
                //                outDocument.bCreatePermission = false;
                //            else
                //                outDocument.bCreatePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_TEMPDELETE_OFFSET)))
                //                outDocument.bDeletePermission = false;
                //            else
                //                outDocument.bDeletePermission = true;

                //            if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_TEMPEDIT_OFFSET)))
                //                outDocument.bEditPermission = false;
                //            else
                //                outDocument.bEditPermission = true;

                //            if (!(base.userLogin.IsAllowedEx(PC_PROGNOTES_VIEW + PROGNOTES_TEMPVIEW_OFFSET)))
                //                outDocument.bViewPermission = false;
                //            else
                //            {
                //                outDocument.bViewPermission = true;
                //                objProgressNotesManager.LoadTemplates(ref outDocument);
                //            }
                //            break;
                //    }
                //}
                //End - pmittal5
                //MGaba2:MITS 22173: End
             }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.OnLoad.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {

                if (objProgressNotesManager != null)
                {
                    objProgressNotesManager.Dispose();
                    objProgressNotesManager = null;
                }
            }

            return (true);
        }
        public bool GetNotesHeaderOrder(ProgressNoteSettings objDocument, out ProgressNoteSettings outDocument, ref BusinessAdaptorErrors p_objErrOut)
        {
            

            ProgressNotesManager objProgressNotesManager = null;
            outDocument = new ProgressNoteSettings();
            try
            {

                //Initialize the application layer ProgressNotesManager class
                // Ash - cloud, overload constructor with Client ID
                //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password);
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);
                XmlDocument p_objXmlOut = new XmlDocument();
                outDocument = objProgressNotesManager.GetNotesHeaderOrder(objDocument,base.userID);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.SaveNotes.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {

                if (objProgressNotesManager != null)
                {
                    objProgressNotesManager.Dispose();
                    objProgressNotesManager = null;
                }
            }
            return true;
        }
        public bool SaveNotesSettings(ProgressNoteSettings objDocument, out ProgressNoteSettings outDocument, ref BusinessAdaptorErrors p_objErrOut)
        {


            ProgressNotesManager objProgressNotesManager = null;
            outDocument = new ProgressNoteSettings();
            try
            {

                //Initialize the application layer ProgressNotesManager class
                // Ash - cloud, overload constructor with Client ID
                //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password);
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);
                XmlDocument p_objXmlOut = new XmlDocument();
                //Deb MITS 30185
                //outDocument = objDocument;
                objProgressNotesManager.SaveNotesSettings(objDocument, base.userID);
                outDocument = objProgressNotesManager.GetNotesHeaderOrder(objDocument, base.userID);
                //Deb MITS 30185
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.SaveNotes.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {

                if (objProgressNotesManager != null)
                {
                    objProgressNotesManager.Dispose();
                    objProgressNotesManager = null;
                }
            }
            return true;
        }

        #endregion
		/// Name		: SaveNotes
		/// Author		: Raman Bhatia
		/// Date Created: 11-Oct-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the notes in the database
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<ProgressNotesManager>
		///				<EventID>Event ID</EventID>
		///				<ClaimID>Claim ID</ClaimID>
		///				<DateEntered>Date Entered</DateEntered>
		///				<NoteTypeCode>Note Type Code</NoteTypeCode>
		///				<NewRecord>New Record Flag</NewRecord>
		///				<NoteMemo>Note Memo</Note Memo>
		///				<NoteMemoCareTech>Note Memo Care Tech</NoteMemoCareTech>
		///				<ClaimprogressNoteId>Claim Progress Note Id</ClaimprogressNoteId>
		///			</ProgressNotesManager>	
		///		</Document>
		/// </param>

		/// </param>
		/// <param name="p_objErrOut">Error object</param>
		/// <returns>true if operation is successful else false</returns>
		public bool SaveNotes (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			int iEventID = 0;
			int iClaimID = 0;
            //williams-neha goel:start:MITS 21704
            int iPolicyID = 0;
            //williams-neha goel:end:MITS 21704
			int iNoteTypeCode = 0;
			int iEnteredBy = 0;
			int iClaimProgressNoteId = 0;
			int iUserID = 0;
			bool bNewRecord = false;
			string sNoteMemo = "";
			string sNoteMemoCareTech = "";
			string sDateEntered = "";
			string sEnteredByName = "";
					

			XmlElement objXmlUser = null;
			ProgressNotesManager objProgressNotesManager = null;
			try
			{
				objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode("//NewRecord");
				bNewRecord = Conversion.ConvertStrToBool(objXmlUser.InnerText);
				
				objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode("//EventID");
				iEventID = Conversion.ConvertStrToInteger(objXmlUser.InnerText);

				objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode("//ClaimID");
				iClaimID = Conversion.ConvertStrToInteger(objXmlUser.InnerText);
                //williams-neha goel--start:MITS 21704
                objXmlUser = (XmlElement)p_objXmlIn.SelectSingleNode("//PolicyID");
                iPolicyID = Conversion.ConvertStrToInteger(objXmlUser.InnerText);
                //williams-neha goel--end:MITS 21704

				objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode("//NoteTypeCode");
				iNoteTypeCode = Conversion.ConvertStrToInteger(objXmlUser.InnerText);

				if(bNewRecord==true)
				{
					iClaimProgressNoteId = 0;
					iUserID = base.userLogin.UserId;
					iEnteredBy = iUserID;
					sEnteredByName = base.userLogin.LoginName;
		
					objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode("//DateEntered");
					sDateEntered = objXmlUser.InnerText;

					objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode("//NoteMemo");
					sNoteMemo = objXmlUser.InnerText;

					objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode("//NoteMemoCareTech");
					sNoteMemoCareTech = objXmlUser.InnerText;
					
				}
				else
				{
                    //MITS 8784..Enhanced Notes should be editable.. Raman Bhatia
                    objXmlUser = (XmlElement)p_objXmlIn.SelectSingleNode("//DateEntered");
                    sDateEntered = objXmlUser.InnerText;

                    objXmlUser = (XmlElement)p_objXmlIn.SelectSingleNode("//NoteMemo");
                    sNoteMemo = objXmlUser.InnerText;

                    objXmlUser = (XmlElement)p_objXmlIn.SelectSingleNode("//NoteMemoCareTech");
                    sNoteMemoCareTech = objXmlUser.InnerText;
                    
                    objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode("//ClaimProgressNoteId");
					iClaimProgressNoteId = Conversion.ConvertStrToInteger(objXmlUser.InnerText);
				}

			
				//Initialize the application layer ProgressNotesManager class
                // Ash - cloud, overload constructor with Client ID
				//objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName  , loginName , userLogin.Password );
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);
				p_objXmlOut = new XmlDocument();
                //williams-neha goel--start:added iPolicyID for policy enhanced notes:MITS 21704
				//objProgressNotesManager.SaveNotes(iEventID , iClaimID , iEnteredBy , sDateEntered , iNoteTypeCode , iUserID , sNoteMemo , sNoteMemoCareTech , sEnteredByName , iClaimProgressNoteId);
                objProgressNotesManager.SaveNotes(iEventID, iClaimID, iEnteredBy, sDateEntered, iNoteTypeCode, iUserID, sNoteMemo, sNoteMemoCareTech, sEnteredByName, iClaimProgressNoteId, iPolicyID);
                //williams-neha goel--end:MITS 21704
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add (p_objException, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add (p_objException,Globalization.GetString ("ProgressNotesAdaptor.SaveNotes.Error",base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally 
			{
				objXmlUser = null;
				if( objProgressNotesManager != null )
				{
                    objProgressNotesManager.Dispose();
					objProgressNotesManager = null ;
				}	
			}
			return (true);
		}
        //Added by abhishek for MITS 11403 : Start 
        /// Name		: DeleteProgressNote
        /// Author		: Abhishek Rathore
        /// Date Created: 12-March-2008		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Deltes a note
        /// </summary>
        /// <param name="p_objXmlIn">XML containing the input parameters needed
        ///		The structure of input XML document would be:
        ///		<Document>
        ///			<ProgressNotesManager>
        ///				<EventID>Event ID</EventID>
        ///				<ClaimID>Claim ID</ClaimID>
        ///			</ProgressNotesManager>	
        ///		</Document>
        /// </param>
        /// </param>
        /// <param name="p_objErrOut">Error object</param>
        /// <returns>true if operation is successful else false</returns>
        public bool DeleteProgressNote(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {

            ProgressNotesManager objProgressNotesManager = null;
            int iEventID = 0;
            int iClaimID = 0;
            //williams-neha goel:MITS 21704
            int iPolicyID = 0;
            //williams:end:MITS 21704
            int iClaimProgressNoteId = 0;
            int iClaimantID = 0;//Added by gbindra MITS#34104 WWIG GAP15 02122014
            XmlElement objTempNode = null;
            bool bSuccess = false;//Added by gbindra MITS#34104 WWIG GAP15 02122014

            try
            {
                 //willimas--neha goel----start:policy enhanced notes permissions:added if else
                   if (!(base.userLogin.IsAllowedEx(POLICY_PROGNOTES_DELETE)))
                       throw new PermissionViolationException(RMPermissions.RMO_NOPROGRESS_NOTES_DELETE, POLICY_PROGNOTES_DELETE);                
                //willimas--neha goel----end:policy enhanced notes permissions

                if (!(base.userLogin.IsAllowedEx(PROGNOTES_DELETE)))
                    throw new PermissionViolationException(RMPermissions.RMO_NOPROGRESS_NOTES_DELETE, PROGNOTES_DELETE);

                objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//EventID");
                iEventID = Conversion.ConvertStrToInteger(objTempNode.InnerText);

                objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimID");
                iClaimID = Conversion.ConvertStrToInteger(objTempNode.InnerText);

                //williams-neha goel---start:MITS 21704
                objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//PolicyID");
                iPolicyID = Conversion.ConvertStrToInteger(objTempNode.InnerText);
                //williams-neha goel---end:MITS 21704
                
                objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimProgressNoteId");
                iClaimProgressNoteId = Conversion.ConvertStrToInteger(objTempNode.InnerText);
               
                objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimantID");
                iClaimantID = Conversion.CastToType<int>(objTempNode.InnerText, out bSuccess);
                //Initialize the application layer ProgressNotesManager class
                // Ash - cloud, overload constructor with Client ID
                //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password);
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);

                objProgressNotesManager.DeleteNote(iEventID, iClaimID, iClaimProgressNoteId, iPolicyID, iClaimantID); //Added by gbindra MITS#34104 02132014 WWIG GAP15 
                
                return this.OnLoad(p_objXmlIn, ref p_objXmlOut, ref p_objErrOut);

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.DeleteProgressNote.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objProgressNotesManager != null)
                {
                    objProgressNotesManager.Dispose();
                    objProgressNotesManager = null;
                }
                
            }

        }

        //<!--Added by abhishek for MITS 11403 : End -->

		/// Name		: PrintProgressNotesReport
		/// Author		: Vaibhav Kaushik and Raman Bhatia
		/// Date Created: 11-Oct-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Print the notes
		/// </summary>
		/// <param name="p_objXmlIn">XML containing the input parameters needed
		///		The structure of input XML document would be:
		///		<Document>
		///			<ProgressNotesManager>
		///				<EventID>Event ID</EventID>
		///				<ClaimID>Claim ID</ClaimID>
		///				<ActivateFilter>Activate Filter Flag</DateEntered>
		///			</ProgressNotesManager>	
		///		</Document>
		/// </param>
		/// </param>
		/// <param name="p_objErrOut">Error object</param>
		/// <returns>true if operation is successful else false</returns>
		
		public bool PrintProgressNotesReport( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut )
		{
			
				ProgressNotesManager objProgressNotesManager = null;
				XmlElement objRootElement = null ;
				XmlElement objTempNode = null ;
			
				bool bReturnValue = false ;
				int iEventID = 0 ;
				int iClaimID = 0 ; 
                //williams-neha goel--start:MITS 21704
                int iPolicyID = 0;  //averma62 MITS - 27826
                //williams-neha goel--end:MITS 21704
				bool bActivateFilter = false;
				string sClaimIDList = string.Empty;
				string sEnteredByList = string.Empty;
				string sNoteTypeList = string.Empty;
				string sUserTypeList = string.Empty;
				string sActivityFromDate = string.Empty;
				string sActivityToDate = string.Empty;
				string sNotesTextContains = string.Empty;
                string sSubjectList = string.Empty; //zmohammad MITS 30218
				string sSortBy = string.Empty;
				string sFilterSQL = string.Empty;
				string sPdfDocPath = string.Empty ;
                //Changed by Gagan for MITS 8697 : Start
                int iClaimProgressNoteId = 0;
                int iNoteTypeCode = 0;
                bool bPrintSelectedNotes = false;
                //Changed by Gagan for MITS 8697 : End
                ////Added by akaushik5 for MITS 30789 Starts
                string sOrderBy = string.Empty;
                ////Added by akaushik5 for MITS 30789 Ends
            int iClaimantID = 0; //gbindra MITS#34104 WWIG GAP15
            SysSettings objSysSettings = null; //added by gbindra MITS#34104 WWIG GAP15
            bool bSuccess = false;//added by gbindra MITS#34104 WWIG GAP15

				try
				{
                    objSysSettings = new SysSettings(m_connectionString, base.ClientId); //Ash - cloud; //added by gbindra MITS#34104 WWIG GAP15
                    //willimas-neha goel:added permission for policy--start
                    if (!(base.userLogin.IsAllowedEx(POLICY_PROGNOTES_PRINT)))
                        throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesPrint, POLICY_PROGNOTES_PRINT);
                    //willimas-neha goel:added permission for policy--end
					//Changed by Shruti on 23rdoct06 for MITS - 7949 -- starts
                    //if(!(base.userLogin.IsAllowedEx(PROGNOTES_PRINT)))
                    //    throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesPrint,PROGNOTES_PRINT);
					//Changed by Shruti on 23rdoct06 for MITS - 7949 -- ends
					objTempNode = (XmlElement) p_objXmlIn.SelectSingleNode("//EventID");
					iEventID = Conversion.ConvertStrToInteger (objTempNode.InnerText);
				
					objTempNode = (XmlElement) p_objXmlIn.SelectSingleNode("//ClaimID");
					iClaimID = Conversion.ConvertStrToInteger(objTempNode.InnerText);
                /*added by gbindra MITS#34104 WWIG GAP15 START*/
                if (objSysSettings.AllowNotesAtClaimant == true)
                {
                    objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimantID");
                    iClaimantID = Conversion.CastToType<int>(objTempNode.InnerText, out bSuccess);
                }
                else
                    iClaimantID = -1;
                /*added by gbindra MITS#34104 WWIG GAP15 END*/
                    //pmittal5 Mits 21514 07/19/10
                    if (iEventID != 0 && iClaimID == 0)
                    {
                        if (!(base.userLogin.IsAllowedEx(EVENT_PROGNOTES_PRINT)))
                            throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesPrint, EVENT_PROGNOTES_PRINT);
                    }
                    else if (iClaimID != 0)
                    {
                        if (!(base.userLogin.IsAllowedEx(GC_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET)))
                            throw new PermissionViolationException(RMPermissions.RMO_NoProgressNotesPrint, GC_PROGNOTES_VIEW + PROGNOTES_PRINT_OFFSET);
                    }
                    //End - pmittal5
                    //williams-neha goel--start:MITS 21704
                    objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//PolicyID");
                    iPolicyID = Conversion.ConvertStrToInteger(objTempNode.InnerText);
                    //williams-neha goel--end:MITS 21704
                    //Changed by Gagan for MITS 8697 : Start
                    objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimProgressNoteId");
                    if (objTempNode.InnerText != "")
                    {
                        bPrintSelectedNotes = true;
                        iClaimProgressNoteId = Conversion.ConvertStrToInteger(objTempNode.InnerText);
                    }
                    //Changed by Gagan for MITS 8697 : End
                    //Added By Rakhi for Notepad Screens
                    objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//NoteType");
                    if (objTempNode != null && objTempNode.InnerText != "")
                    {
                        iNoteTypeCode = Convert.ToInt32(objTempNode.InnerText);
                    }
                    //Added By Rakhi for Notepad Screens
				
					//Initialize the application layer ProgressNotesManager class
                    // Ash - cloud, overload constructor with Client ID
					//objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName  , loginName , userLogin.Password );
                    objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);
                    //rsolanki2: execSummaryScheduler Enhacements
                    objProgressNotesManager.m_sTMConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource", base.ClientId); 
                    objProgressNotesManager.m_UserLogin = base.m_userLogin;
				
					//Filtering information
					objTempNode = (XmlElement) p_objXmlIn.SelectSingleNode("//ActivateFilter");
					bActivateFilter = Conversion.ConvertStrToBool(objTempNode.InnerText);
				
					if(bActivateFilter==true)
					{
						objTempNode = (XmlElement) p_objXmlIn.SelectSingleNode("//ClaimIDList");
						sClaimIDList = objTempNode.InnerText;

						objTempNode = (XmlElement) p_objXmlIn.SelectSingleNode("//EnteredByList");
						sEnteredByList = objTempNode.InnerText;

                        //zmohammad MITS 30218
                        objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//SubjectList");
                        sEnteredByList = objTempNode.InnerText;

						objTempNode = (XmlElement) p_objXmlIn.SelectSingleNode("//NoteTypeList ");
						sNoteTypeList = objTempNode.InnerText;

						objTempNode = (XmlElement) p_objXmlIn.SelectSingleNode("//UserTypeList");
						sUserTypeList = objTempNode.InnerText;

						objTempNode = (XmlElement) p_objXmlIn.SelectSingleNode("//ActivityFromDate");
						sActivityFromDate = objTempNode.InnerText;

						objTempNode = (XmlElement) p_objXmlIn.SelectSingleNode("//ActivityToDate");
						sActivityToDate = objTempNode.InnerText;

						objTempNode = (XmlElement) p_objXmlIn.SelectSingleNode("//NotesTextContains");
						sNotesTextContains = objTempNode.InnerText;

						objTempNode = (XmlElement) p_objXmlIn.SelectSingleNode("//SortBy");
						sSortBy = objTempNode.InnerText;

                        ////Changed by akaushik5 for MITS 30789 Starts
                        // sFilterSQL = objProgressNotesManager.CreateFilterString(sClaimIDList,sEnteredByList,sNoteTypeList,sUserTypeList,sActivityFromDate,sActivityToDate,sNotesTextContains,sSortBy);
                        objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//OrderBy");
                        sOrderBy = objTempNode.InnerText;

                    sFilterSQL = objProgressNotesManager.CreateFilterString(sClaimIDList, sEnteredByList, sNoteTypeList, sUserTypeList, sActivityFromDate, sActivityToDate, sNotesTextContains, sSortBy, sSubjectList, sOrderBy, iClaimantID); //Added by gbindra MITS#34104 02132014 WWIG GAP15 
                        ////Changed by akaushik5 for MITS 30789 Ends
					
					}

                    //Changed by Gagan for MITS 8697 : Start
                    //iNoteTypeCode:Added By Rakhi for Notepad Screens
                    //bReturnValue = objProgressNotesManager.PrintProgressNotesReport(iEventID, iClaimID, bActivateFilter, sFilterSQL, ref sPdfDocPath, bPrintSelectedNotes, iClaimProgressNoteId);
                    //williams-neha goel-start:added iPolicyID for policy enhanced notes:MITS 21704
                    //averma62 MITS - 27826 bReturnValue = objProgressNotesManager.PrintProgressNotesReport(iEventID, iClaimID, bActivateFilter, sFilterSQL, ref sPdfDocPath, bPrintSelectedNotes, iClaimProgressNoteId, iNoteTypeCode);
                bReturnValue = objProgressNotesManager.PrintProgressNotesReport(iEventID, iClaimID, bActivateFilter, sFilterSQL, ref sPdfDocPath, bPrintSelectedNotes, iClaimProgressNoteId, iNoteTypeCode, iPolicyID, iClaimantID);//averma62 MITS - 27826 - added one more parameter policy id //Added by gbindra MITS#34104 02132014 WWIG GAP15 
                    //williams-neha goel-end:added iPolicyID for policy enhanced notes:MITS 21704
                    //Changed by Gagan for MITS 8697 : End

					// If return value true then copy the binary content of file in the p_objXmlOut doc.
					if( bReturnValue )
					{					
						p_objXmlOut = new XmlDocument();
						objRootElement = p_objXmlOut.CreateElement( "ProgressNotes" );
						p_objXmlOut.AppendChild( objRootElement );
						this.CreateNodeWithFileContent( "ProgressNotesReport", objRootElement , sPdfDocPath );
						return( true );
					}
					return( false );
							
				}
				catch( RMAppException p_objException )
				{
					p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
					return false;
				}
				catch( Exception p_objException )
				{
                    p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.PrintProgressNotesReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				finally
				{
					if( objProgressNotesManager != null )
					{
                        objProgressNotesManager.Dispose();
						objProgressNotesManager = null ;
					}				
					objRootElement = null ;
				}
				
		}

        /// <summary>
        /// This Function would fetch text and html contents for a given progress note
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool GetNoteDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            ProgressNotesManager objProgressNotesManager = null;
            int iClaimProgressNoteId = 0;
            XmlElement objTempNode = null;

            try
            {

                objTempNode = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimProgressNoteId");
                iClaimProgressNoteId = Conversion.ConvertStrToInteger(objTempNode.InnerText);

                //Initialize the application layer ProgressNotesManager class
                // Ash - cloud, overload constructor with Client ID
                //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password);
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password, base.ClientId);

                p_objXmlOut = objProgressNotesManager.GetNoteDetails(iClaimProgressNoteId);

                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.DeleteProgressNote.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objProgressNotesManager != null)
                {
                    objProgressNotesManager.Dispose();
                    objProgressNotesManager = null;
                }

            }
        }
		
		#endregion
			//mbahl3 enhance notes enhancement mits 30513
         public bool GetNotesCaption(ProgressNotesTypeChange objDocument, out ProgressNotesTypeChange outDocument, ref BusinessAdaptorErrors p_objErrOut)
        {
             
              bool bReturn = false;
              outDocument = new ProgressNotesTypeChange();
            ProgressNotesManager objProgressNotesManager = null;
            string sCaption = string.Empty;
            try
            {

                // Ash - cloud, overload constructor with Client ID
                //objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password);
                objProgressNotesManager = new ProgressNotesManager(base.userLogin.objRiskmasterDatabase.DataSourceName, loginName, userLogin.Password,base.ClientId);

                sCaption = objProgressNotesManager.GetNotesCaption(objDocument.RecordType, Conversion.ConvertStrToInteger(objDocument.RecordId), Conversion.ConvertStrToInteger(objDocument.PIEId));
                outDocument.RecordId = objDocument.RecordId;
                outDocument.RecordType = objDocument.RecordType;
                outDocument.CaptionText = sCaption;

                bReturn = true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                bReturn = false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("ProgressNotesAdaptor.PrintProgressNotesReport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                bReturn = false;
            }
            finally
            {
                if (objProgressNotesManager != null)
                {
                    objProgressNotesManager.Dispose();
                    objProgressNotesManager = null;
                }
            }
            return bReturn;
            
        }
	//mbahl3 enhance notes enhancement mits 30513
		#region Private Methods
		/// <summary>
		/// This function appends the output file to the output xml document.
		/// </summary>
		/// <param name="p_sNodeName">Name of the Node</param>
		/// <param name="p_objParentNode">Parent Node</param>
		/// <param name="p_sOutFilepath">Complete Path of the file.</param>
		private void CreateNodeWithFileContent( string p_sNodeName, XmlElement p_objParentNode, string p_sOutFilepath )
		{
			XmlElement objElement = null;
			MemoryStream objMemoryStream = null;
			FileStream objFileStream = null ;
			BinaryReader objBReader = null ;
			byte[] arrRet = null ;
			try
			{
				// Check the file existence.
				if( ! File.Exists( p_sOutFilepath ) )
                    throw new RMAppException(Globalization.GetString("ProgressNotesAdaptor.PrintProgressNotesReport.Error", base.ClientId));
				
				// Read the file in memory stream.
				objFileStream = new FileStream( p_sOutFilepath ,FileMode.Open);
				objBReader = new BinaryReader( objFileStream );
				arrRet = objBReader.ReadBytes( (int)objFileStream.Length );				
				objMemoryStream = new MemoryStream( arrRet );
				
				objFileStream.Close();

				// Delete the temp file.
				File.Delete( p_sOutFilepath );
			
				// Insert the memory stream as base64 string into the Xml document.
				objElement = p_objParentNode.OwnerDocument.CreateElement( "File" );											
				objElement.SetAttribute( "Name", p_sNodeName );                 																
				objElement.InnerText = Convert.ToBase64String( objMemoryStream.ToArray() );
				p_objParentNode.AppendChild( objElement );				
			}
			catch( Exception p_objException )
			{
                throw new RMAppException(Globalization.GetString("ProgressNotesAdaptor.PrintProgressNotesReport.Error", base.ClientId), p_objException);
			}
			finally
			{
				objElement = null;
				if( objMemoryStream != null ) 
				{
					objMemoryStream.Close();
					objMemoryStream.Dispose();
				}
				if( objFileStream != null )
				{
					objFileStream.Close();
                    objFileStream.Dispose();
				}
				if( objBReader != null )
				{
					objBReader.Close();
                    objBReader = null;
				}
			}
		}

		#endregion

	}
}
