/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/
using System;
using System.Xml ;
using System.IO ;
using System.Collections ;

using Riskmaster.Common ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.BusinessAdaptor.Common ;
using Riskmaster.Application.PrintChecks ;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: PrintChecksAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 11-Mar-2005
	///* $Author	: Vaibhav Kaushik
	///* $Comment	: Implements adaptor layer over the PrintChecks AL component.
	///* $Source	: 
	///* Amendment  -->
	///  1.Date		: 7 Feb 06 
	///    Desc		: Replacing RMO_ACCESS use, and changing params of permission
	///				  violation exception so it can be correctly called. 
	///    Author	: Sumit
	///**************************************************************
	///**************************************************************	
	public class PrintChecksAdaptor: BusinessAdaptorBase
	{
		private const int RMB_FUNDS = 9500;
		private const int RMO_FUNDS_TRANSACT = 150;
		private const int RMO_FUNDS_BNKACCT = 300;
		private const int RMO_FUNDS_PRINTCHK = 600;

		private const int RMB_FUNDS_PRINTCHK = 10100;
		private const int RMO_ALLOW_CHANGE_CHECK_NUMBER = 31;

		private const int RMB_FUNDS_TRANSACT = 9650;
		private const int RMO_PRINT_CHK = 30;
		private const int RMO_EDIT_PAYEE = 31;
		private const int RMO_ALLOW_MANUAL_CHECKS = 34;
		private const int RMO_ALLOW_EDIT_CTL_NUMBER = 35;
		private const int RMO_ALLOW_EDIT_CHK_NUMBER = 36;
		private const int RMO_ALLOW_PAYMENT_CLOSED_CLM =37;
		private const int RMO_ALLOW_EDIT_PRECHECK = 38;
		private const int RMO_ALLOW_EDIT_CLAIM_NUMBER = 39;
		private const int RMO_ALLOW_ENTRY_CLAIM_NUMBER = 41;
		private const int RMO_FUNDS_TRANSACT_SUPP = 50;

		#region Constructor
		///	<summary>
		/// Constructor, initializes the variables to the default value
		/// </summary>
		public PrintChecksAdaptor( ) : base()
		{						
		}		
		#endregion 

		#region Public Methods For Check Setup
		/// <summary>
		///		This method is a wrapper to CheckSetup.UpdateStatusForPrintedChecks method.
		///		This method will return the XML for Check Batch Changed event for checks. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PrintChecks>
		///				<BatchNumber>Batch Number</BatchNumber>
		///				<AccountId>Account Id</AccountId>
		///				<PrintCheckDetails></PrintCheckDetails>
		///			</PrintChecks>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// 
		///		The structure of output XML document would be:
		///		<PrintChecks>
		///			<CheckBatchChangedPost>
		///				<NumberOfPayments></NumberOfPayments> 
		///				<NumberOfChecks></NumberOfChecks> 
		///				<PrintPost></PrintPost> 
		///				<OrderPostCheck /> 
		///				<DatePostCheck></DatePostCheck> 
		///			</CheckBatchChangedPost>
		///			<CheckBatchChangedPre>
		///				<NumberOfChecks></NumberOfChecks> 
		///				<OrderPreCheck /> 
		///				<DatePreCheck></DatePreCheck> 
		///				<PrintBatch></PrintBatch> 
		///			</CheckBatchChangedPre>
		///		</PrintChecks>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool UpdateStatusForPrintedChecks( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			CheckSetup objCheckSetup = null ;
			XmlElement objTargetElement = null;
 
			try
			{
				// Create instance of the CheckSetup.
                objCheckSetup = new CheckSetup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);
                //Need to have an Xml Node like <FilesNameAndType>
                DeletePrintCheckFiles(p_objXmlIn);
				// Invoke the UpdateStatusForPrintedChecks function.
				p_objXmlOut = objCheckSetup.UpdateStatusForPrintedChecks( p_objXmlIn);
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add( p_objException, Globalization.GetString("PrintChecksAdaptor.UpdateStatusForPrintedChecks.Error", base.ClientId), BusinessAdaptorErrorType.Error );
				return false;
			}
			finally
			{
				if( objCheckSetup != null )
				{
					objCheckSetup.Dispose();
					objCheckSetup = null ;
				}
				objTargetElement = null;
			}
		}
        private bool DeletePrintCheckFiles(XmlDocument p_objXmlIn)
        {
            string[] arrFileNameAndType=null;
            string[] arrSingleFileNameAndType = null;
            if (p_objXmlIn.SelectSingleNode("//FilesNameAndType")!=null)
            {
                if (p_objXmlIn.SelectSingleNode("//FilesNameAndType").InnerText != null && p_objXmlIn.SelectSingleNode("//FilesNameAndType").InnerText != "")
                {
                    arrFileNameAndType = p_objXmlIn.SelectSingleNode("//FilesNameAndType").InnerText.Split(",".ToCharArray()[0]);
                }
            }
            if (arrFileNameAndType != null)
            {
                foreach (string sTmpFileNameAndType in arrFileNameAndType)
                {
                    arrSingleFileNameAndType = sTmpFileNameAndType.Split("~".ToCharArray()[0]);
                    if (arrSingleFileNameAndType != null)
                    {
                        string sFileType = arrSingleFileNameAndType[1];
                        string sFileName = arrSingleFileNameAndType[0];
                        string sFullFileName = string.Empty;
                        if (sFileType == "check")
                        {
                            sFullFileName = Path.Combine(RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "PrintChecks"), sFileName);
                        }
                        else if (sFileType == "eft")//Added by Amitosh For EFT Payment
                        {
                            sFullFileName = Path.Combine(RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "PrintChecks\\EFT"), sFileName);
                        }//End Amitosh
                        else
                        {
                            sFullFileName = Path.Combine(RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "EOB"), sFileName);
                        }
                        if (File.Exists(sFullFileName) && sFileType!="eft")
                        {
                            File.Delete(sFullFileName);
                        }
                    }
                }
            }
            return (true);	

        }
		/// <summary>
		///		This method is a wrapper to CheckSetup.CheckBatchChangedPost method.
		///		This method will return the XML for Check Batch Changed event for Post register checks. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PrintChecks>
		///				<BatchNumber>Batch Number</BatchNumber>
		///				<AccountId>Account Id</AccountId>
		///			</PrintChecks>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// 
		///		The structure of output XML document would be:
		///		<CheckBatchChangedPost>
		///			<NumberOfPayments></NumberOfPayments> 
		///			<NumberOfChecks></NumberOfChecks> 
		///			<PrintPost></PrintPost> 
		///			<OrderPostCheck /> 
		///			<DatePostCheck></DatePostCheck> 
		///		</CheckBatchChangedPost>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool CheckBatchChangedPost( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			CheckSetup objCheckSetup = null ;
			XmlElement objTargetElement = null;
 
			int iBatchNumebr = 0 ;
			int iAccountId = 0 ;
			// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            int iDistributionType = 0;
			// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
			try
			{
				// Get the Batch Number.
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//BatchNumber" );
				if( objTargetElement == null )
				{
					p_objErrOut.Add( "BatchNumberMissing" , Globalization.GetString("PrintChecksAdaptor.BatchNumberNotFound", base.ClientId) , BusinessAdaptorErrorType.Error );
					return false;
				}
				iBatchNumebr = Conversion.ConvertStrToInteger( objTargetElement.InnerText );

				// Get the Account Id.
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//AccountId" );
				if( objTargetElement == null )
				{
					p_objErrOut.Add( "AccountIdMissing" , Globalization.GetString("PrintChecksAdaptor.AccountIdNotFound", base.ClientId) , BusinessAdaptorErrorType.Error );
					return false;
				}
				iAccountId = Conversion.ConvertStrToInteger( objTargetElement.InnerText );
				
				// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                // Get the Distribution Type Id.
                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//DistributionType");
                if (objTargetElement == null)
                {
                    p_objErrOut.Add("DistributionTypeMissing", Globalization.GetString("PrintChecksAdaptor.DistributionTypeNotFound", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                iDistributionType = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
				// Create instance of the CheckSetup.
                objCheckSetup = new CheckSetup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);

				// Invoke the CheckBatchChangedPost function.
                p_objXmlOut = objCheckSetup.CheckBatchChangedPost(iBatchNumebr, iAccountId, iDistributionType);
				// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add( p_objException, Globalization.GetString("PrintChecksAdaptor.CheckBatchChangedPost.Error", base.ClientId), BusinessAdaptorErrorType.Error );
				return false;
			}
			finally
			{
				if( objCheckSetup != null )
				{
					objCheckSetup.Dispose();
					objCheckSetup = null ;
				}
				objTargetElement = null;
			}
		}

		/// <summary>
		///		This method is a wrapper to CheckSetup.CheckBatchChangedPre method.
		///		This method will return the XML for Check Batch Changed event for Pre register checks. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PrintChecks>
		///				<BatchNumber>Batch Number</BatchNumber>
		///				<AccountId>Account Id</AccountId>
		///			</PrintChecks>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// 
		///		The structure of output XML document would be:
		///		<CheckBatchChangedPre>
		///			<NumberOfChecks></NumberOfChecks> 
		///			<OrderPreCheck /> 
		///			<DatePreCheck></DatePreCheck> 
		///			<PrintBatch></PrintBatch> 
		///		</CheckBatchChangedPre>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool CheckBatchChangedPre( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			CheckSetup objCheckSetup = null ;
			XmlElement objTargetElement = null;
            bool bReCreate = false;
			int iBatchNumebr = 0 ;
			int iAccountId = 0 ;
			// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            int iDistributionType = 0;
			// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
			try
			{
                //if (!userLogin.IsAllowedEx(RMB_FUNDS_PRINTCHK, RMO_ALLOW_CHANGE_CHECK_NUMBER))
                //    throw new PermissionViolationException(RMPermissions.RMO_ALLOW_CHANGE_CHECK_NUMBER,RMB_FUNDS_PRINTCHK);
				
				// Get the Batch Number.
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//BatchNumber" );
				if( objTargetElement == null )
				{
					p_objErrOut.Add( "BatchNumberMissing" , Globalization.GetString("PrintChecksAdaptor.BatchNumberNotFound", base.ClientId) , BusinessAdaptorErrorType.Error );
					return false;
				}
				iBatchNumebr = Conversion.ConvertStrToInteger( objTargetElement.InnerText );

				// Get the Accoount Id.
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//AccountId" );
				if( objTargetElement == null )
				{
					p_objErrOut.Add( "AccountIdMissing" , Globalization.GetString("PrintChecksAdaptor.AccountIdNotFound", base.ClientId) , BusinessAdaptorErrorType.Error );
					return false;
				}
				iAccountId = Conversion.ConvertStrToInteger( objTargetElement.InnerText );

				// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//DistributionType");
                if (objTargetElement == null)
                {
                    p_objErrOut.Add("DistributionTypeMissing", Globalization.GetString("PrintChecksAdaptor.DistributionTypeNotFound", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                iDistributionType = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
				// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
				// Create the instance of CheckSetup.
                objCheckSetup = new CheckSetup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);
                //Ashutosh To set the isRecreate option
                if (p_objXmlIn.SelectSingleNode("//PrintChecks/IsReCreate") != null)
                {
                    if (p_objXmlIn.SelectSingleNode("//PrintChecks/IsReCreate").InnerText.ToLower() == "true")
                    {
                        bReCreate = true;
						// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                        p_objXmlOut = objCheckSetup.CheckBatchChangedPre_ForRePrint(iBatchNumebr, iAccountId, bReCreate, iDistributionType);
						// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks

                        // npadhy 11632 - If the Distribution Type is associated with Printer only then we need to show error
                        if (p_objXmlOut.SelectSingleNode("//RecreateError") != null)
                        {
                            p_objErrOut.Add("Error", p_objXmlOut.SelectSingleNode("//RecreateError").InnerText, BusinessAdaptorErrorType.Error);
                        }
                    }
                }
                else
                {
                    //Invoke the CheckBatchChangedPre function.
					// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                    p_objXmlOut = objCheckSetup.CheckBatchChangedPre(iBatchNumebr, iAccountId, iDistributionType);
					// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                }
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add( p_objException , Globalization.GetString("PrintChecksAdaptor.CheckBatchChangedPre.Error", base.ClientId), BusinessAdaptorErrorType.Error );
				return false;
			}
			finally
			{
				if( objCheckSetup != null )
				{
					objCheckSetup.Dispose();
					objCheckSetup = null ;
				}
				objTargetElement = null;
			}
		}

		/// <summary>
		///		This method is a wrapper to CheckSetup.CheckSetChanged method.
		///		This method will return the XML for Check Set Changed event. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PrintChecks>
		///				<CheckSetChanged>
		///					<AccountId>Account Id</AccountId> 
		///					<IncludeAutoPayment>Include Auto Payment Flag</IncludeAutoPayment> 
		///					<UsingSelection>1</UsingSelection>
		///					<AttachedClaimOnly>Attached Claims Only Flag</AttachedClaimOnly> 
		///					<FromDateFlag>From Date Flag</FromDateFlag> 
		///					<FromDate>From Date</FromDate>  
		///					<ToDateFlag>To Date Flag</ToDateFlag> 
		///					<ToDate>To Date</ToDate>
		///					<SelectedChecksIds>Comma Seprated List of TransIds</SelectedChecksIds>
		///					<SelectedAutoChecksIds>Comma Seprated List of Auto TransIds</SelectedAutoChecksIds>
		///			</PrintChecks>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		///		
		///		The structure of output XML document would be:
		///		 <CheckSetChanged>
		///			<NumberOfChecks>0</NumberOfChecks> 
		///			<TotalAmount>$0.00</TotalAmount> 
		///			<PrintPre>False</PrintPre> 								
		///		</CheckSetChanged>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool CheckSetChanged( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut )
		{
			CheckSetup objCheckSetup = null ;
			
			try
			{
				// Create the instance of CheckSetup.
                objCheckSetup = new CheckSetup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);

				// Invoke the CheckSetChanged function.
				p_objXmlOut = objCheckSetup.CheckSetChanged( p_objXmlIn );
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add( p_objException, Globalization.GetString("PrintChecksAdaptor.CheckSetChanged.Error", base.ClientId), BusinessAdaptorErrorType.Error );
				return false;
			}
			finally
			{
				if( objCheckSetup != null )
				{
					objCheckSetup.Dispose();
					objCheckSetup = null ;
				}
			}
		}

		/// <summary>
		///		This method is a wrapper to CheckSetup.AccountChanged method.
		///		This method will return the XML for Account Changed event. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PrintChecks>				
		///				<AccountChanged>
		///					<AccountId>Account Id</AccountId> 
		///					<IncludeAutoPayment>Include Auto Payment Flag</IncludeAutoPayment> 
		///					<AttachedClaimOnly>Attached Claim Only Flag</AttachedClaimOnly> 
		///					<FromDateFlag>From Date Flag</FromDateFlag> 
		///					<FromDate>From Date</FromDate>  
		///					<ToDateFlag>To Date Flag</ToDateFlag> 
		///					<ToDate>To Date</ToDate> 
		///				</AccountChanged>
		///			</PrintChecks>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// 
		///		The structure of output XML document would be:
		///		<AccountChanged>
		///			<Stocks Count="1">
		///				<Stock Name="aaa" Id="172" /> 
		///			</Stocks>
		///			<FirstCheckNum>25</FirstCheckNum> 
		///			<BatchNumber>57</BatchNumber> 
		///			<PayerLevel /> 
		///			<BRSInstalled>True</BRSInstalled> 
		///			<UseFundsSubAccounts>True</UseFundsSubAccounts> 
		///			<NoSubDepCheck>-1</NoSubDepCheck> 
		///			<AssignCheckStockToSubAcc>False</AssignCheckStockToSubAcc> 
		///			<CheckSetChanged>
		///				<NumberOfChecks>20</NumberOfChecks> 
		///				<TotalAmount>$28,628.19</TotalAmount> 
		///				<PrintPre>True</PrintPre> 
		///				<SelectedChecksIds>Comma Seprated List of TransIds</SelectedChecksIds>
		///				<SelectedAutoChecksIds>Comma Seprated List of Auto TransIds</SelectedAutoChecksIds>
		///			</CheckSetChanged>
		///			<CheckBatchChangedPre>
		///				<NumberOfChecks>0</NumberOfChecks> 
		///				<OrderPreCheck>ADJUSTER_EID</OrderPreCheck> 
		///				<DatePreCheck>3/4/2005</DatePreCheck> 
		///				<PrintBatch>False</PrintBatch> 
		///			</CheckBatchChangedPre>
		///			<CheckBatchChangedPost>
		///				<NumberOfPayments>0</NumberOfPayments> 
		///				<NumberOfChecks>0</NumberOfChecks> 
		///				<PrintPost>False</PrintPost> 
		///				<OrderPostCheck /> 
		///				<DatePostCheck>Not Run</DatePostCheck> 
		///			</CheckBatchChangedPost>
		///		</AccountChanged>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool AccountChanged( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut )
		{

			CheckSetup objCheckSetup = null ;
			
			try
			{
				// Create the instance of CheckSetup.
                objCheckSetup = new CheckSetup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);
				
				// Invoke the AccountChanged function.
				p_objXmlOut = objCheckSetup.AccountChanged( p_objXmlIn );

                // npadhy 11632 - If the Distribution Type is associated with Printer only then we need to show error
                if (p_objXmlIn.SelectSingleNode("//IsReCreate") != null && p_objXmlOut.SelectSingleNode("//RecreateError") != null)
                {
                    p_objErrOut.Add("Error", p_objXmlOut.SelectSingleNode("//RecreateError").InnerText, BusinessAdaptorErrorType.Error);
                }
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add( p_objException , Globalization.GetString("PrintChecksAdaptor.AccountChanged.Error", base.ClientId), BusinessAdaptorErrorType.Error );
				return false;
			}
			finally
			{
				if( objCheckSetup != null )
				{
					objCheckSetup.Dispose();
					objCheckSetup = null ;
				}
			}
		}

		/// <summary>
		///		This method is a wrapper to CheckSetup.OnLoad method.
		///		This method will return the XML for OnLoad event. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.</param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		///		
		///		The structure of output XML document would be:
		///		<OnLoad>
		///			<Accounts>
		///				<Account AccountName="Set-Up Account" AccountId="1" /> 
		///				<Account AccountName="Manmohan" AccountId="8" /> 					 
		///			</Accounts>
		///			<AccountChanged>
		///				<Stocks Count="1">
		///					<Stock Name="aaa" Id="172" /> 
		///				</Stocks>
		///				<FirstCheckNum>25</FirstCheckNum> 
		///				<BatchNumber>57</BatchNumber> 
		///				<PayerLevel /> 
		///				<BRSInstalled>True</BRSInstalled> 
		///				<UseFundsSubAccounts>True</UseFundsSubAccounts> 
		///				<NoSubDepCheck>-1</NoSubDepCheck> 
		///				<AssignCheckStockToSubAcc>False</AssignCheckStockToSubAcc> 
		///				<CheckSetChanged>
		///					<NumberOfChecks>20</NumberOfChecks> 
		///					<TotalAmount>$28,628.19</TotalAmount> 
		///					<PrintPre>True</PrintPre> 
		///					<SelectedChecksIds>Comma Seprated List of TransIds</SelectedChecksIds>
		///					<SelectedAutoChecksIds>Comma Seprated List of Auto TransIds</SelectedAutoChecksIds>
		///				</CheckSetChanged>
		///				<CheckBatchChangedPre>
		///					<NumberOfChecks>0</NumberOfChecks> 
		///					<OrderPreCheck>ADJUSTER_EID</OrderPreCheck> 
		///					<DatePreCheck>3/4/2005</DatePreCheck> 
		///					<PrintBatch>False</PrintBatch> 
		///				</CheckBatchChangedPre>
		///				<CheckBatchChangedPost>
		///					<NumberOfPayments>0</NumberOfPayments> 
		///					<NumberOfChecks>0</NumberOfChecks> 
		///					<PrintPost>False</PrintPost> 
		///					<OrderPostCheck /> 
		///					<DatePostCheck>Not Run</DatePostCheck> 
		///				</CheckBatchChangedPost>
		///			</AccountChanged>
		///		</OnLoad>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool OnLoad( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut )
		{

			CheckSetup objCheckSetup = null ;
			try
			{
				if (!userLogin.IsAllowedEx(RMB_FUNDS,RMO_FUNDS_PRINTCHK))
					throw new PermissionViolationException(RMPermissions.RMO_FUNDS_PRINTCHK,RMB_FUNDS);
				// Create the instance of CheckSetup.
                objCheckSetup = new CheckSetup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);

                // npadhy RMA-11632 - During Load we need to distinguish if the Recreate Check is calling this function or not.
                bool p_bRecreateCheck = p_objXmlIn.SelectSingleNode("//IsReCreate") != null ? true : false; 
				// Invoke the OnLoad function.
                p_objXmlOut = objCheckSetup.OnLoad(p_bRecreateCheck);

                // npadhy 11632 - If the Distribution Type is associated with Printer only then we need to show error
                if (p_objXmlIn.SelectSingleNode("//IsReCreate") != null && p_objXmlOut.SelectSingleNode("//RecreateError") != null)
                {
                    p_objErrOut.Add("Error", p_objXmlOut.SelectSingleNode("//RecreateError").InnerText, BusinessAdaptorErrorType.Error);
                }
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add( p_objException , Globalization.GetString("PrintChecksAdaptor.OnLoad.Error", base.ClientId), BusinessAdaptorErrorType.Error );
				return false;
			}
			finally
			{
				if( objCheckSetup != null )
				{
					objCheckSetup.Dispose();
					objCheckSetup = null ;
				}
			}
		}

		/// <summary>
		///		This method is a wrapper to CheckSetup.GetListOfChecks method.
		///		This method will return the XML contains list of Checks.
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<GetListOfChecks>
		///				<AccountId>Account Id</AccountId> 
		///				<IncludeAutoPayment>Include Auto Payment Flag</IncludeAutoPayment> 
		///				<AttachedClaimOnly>Attached Claim Only Flag</AttachedClaimOnly> 
		///				<FromDateFlag>From Date Flag</FromDateFlag> 
		///				<FromDate>From Date</FromDate>  
		///				<ToDateFlag>To Date Flag</ToDateFlag> 
		///				<ToDate>To Date</ToDate> 
		///			</GetListOfChecks>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// 
		///		The structure of output XML document would be:
		///		<GetListOfChecks>
		///			<Checks>
		///				<Check CtlNumber="0001071" PayeeName="Chesser Carrie" ClaimNumber="WCSATYA123" Amount="$150.00" CheckDate="1/10/2005" TransId="1071" AutoCheck="true" /> 
		///				<Check CtlNumber="0001072" PayeeName="Chesser Carrie" ClaimNumber="WCSATYA123" Amount="$99.00" CheckDate="1/10/2005" TransId="1072" AutoCheck="true" /> 
		///			</Checks>
		///		</GetListOfChecks>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool GetListOfChecks( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			CheckSetup objCheckSetup = null ;
			try
			{
				// Create the instance of CheckSetup.
                objCheckSetup = new CheckSetup(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);
				
				// Invoke the GetListOfChecks function.
				p_objXmlOut = objCheckSetup.GetListOfChecks( p_objXmlIn );
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add( p_objException, Globalization.GetString("PrintChecksAdaptor.GetListOfChecks.Error", base.ClientId), BusinessAdaptorErrorType.Error );
				return false;
			}
			finally
			{
				if( objCheckSetup != null )
				{
					objCheckSetup.Dispose();
					objCheckSetup = null ;
				}				
			}
		}

		#endregion 

		#region Public Methods For Check Register	
		/// <summary>
		///		This method is a wrapper to CheckRegister.PostCheckSubAccount method.
		///		This method will return XML document which contains the PDF file( PostCheckSubAccount report ) 
		///		in binary format for the given criteria. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PrintChecks>				
		///				<PostCheckSubAccount>
		///					<AccountId>Account Id</AccountId> 
		///					<BatchNumber>Batch Number</BatchNumber> 
		///					<PostcheckDate>Post Check Date</PostcheckDate> 					 
		///				</PostCheckSubAccount>
		///			</PrintChecks>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// 
		///		The structure of output XML document would be:
		///		<PrintChecks>
		///			<File Name="xyz"></File> 
		///		</PrintChecks>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool PostCheckSubAccount( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut )
		{
			CheckRegister objCheckRegister = null ;
			XmlElement objTargetElement = null ;
			XmlElement objRootElement = null ;
			
			bool bReturnValue = false ;
			int iAccountId = 0 ;
			int iBatchNumebr = 0 ;
			// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            int iDistributionType = 0;
			// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
			string sDate = string.Empty ;
			string sPdfDocPath = string.Empty ;

			try
			{
				// Get the Batch Number
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//BatchNumber" );
				if( objTargetElement == null )
				{
					p_objErrOut.Add( "BatchNumberMissing" , Globalization.GetString("PrintChecksAdaptor.BatchNumberNotFound", base.ClientId) , BusinessAdaptorErrorType.Error );
					return false;
				}
				iBatchNumebr = Conversion.ConvertStrToInteger( objTargetElement.InnerText );
				
				// Get the Account Id 
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//AccountId" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("AccountIdMissing", Globalization.GetString("PrintChecksAdaptor.AccountIdNotFound", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iAccountId = Conversion.ConvertStrToInteger( objTargetElement.InnerText );
				
				// Get the Post Check Date
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//PostcheckDate" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("DateMissing", Globalization.GetString("PrintChecksAdaptor.DateNotFound", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sDate = objTargetElement.InnerText ;
				// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                // Get the Distribution Type
                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//DistributionType");
                if (objTargetElement == null)
                {
                    p_objErrOut.Add("DistributionTypeMissing", Globalization.GetString("PrintChecksAdaptor.DistributionTypeNotFound", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                iDistributionType = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
				
				// Initialize the Check Register object.
				objCheckRegister = new CheckRegister( userLogin.objRiskmasterDatabase.DataSourceName , userLogin.LoginName ,userLogin.Password , userLogin.UserId , userLogin.GroupId, base.ClientId );

				// Call PostCheckSubAccount Function.
                bReturnValue = objCheckRegister.PostCheckSubAccount(iAccountId, iBatchNumebr, sDate, iDistributionType, ref sPdfDocPath);
				// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks

				// If return value true then copy the binary content of file in the p_objXmlOut doc.
				if( bReturnValue )
				{					
					p_objXmlOut = new XmlDocument();
					objRootElement = p_objXmlOut.CreateElement( "PrintChecks" );
					p_objXmlOut.AppendChild( objRootElement );
					this.CreateNodeWithFileContent( "PostCheckSubAccount", objRootElement , sPdfDocPath );
					return( true );
				}
				return( false );
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch( Exception p_objException )
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintChecksAdaptor.PostCheckSubAccount.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				if( objCheckRegister != null )
				{
					objCheckRegister.Dispose();
					objCheckRegister = null ;
				}
				objTargetElement = null ;
				objRootElement = null ;
			}
		}

		/// <summary>
		///		This method is a wrapper to CheckRegister.PostCheckSummary method.
		///		This method will return XML document which contains the PDF file( PostCheckSummary report ) 
		///		in binary format for the given criteria. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PrintChecks>				
		///				<PostCheckSummary>
		///					<AccountId>Account Id</AccountId> 
		///					<BatchNumber>Batch Number</BatchNumber> 
		///					<PostcheckDate>Post Check Date</PostcheckDate> 					 
		///				</PostCheckSummary>
		///			</PrintChecks>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// 
		///		The structure of output XML document would be:
		///		<PrintChecks>
		///			<File Name="xyz"></File> 
		///		</PrintChecks>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool PostCheckSummary( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut )
		{
			CheckRegister objCheckRegister = null ;
			XmlElement objTargetElement = null ;
			XmlElement objRootElement = null ;
			
			bool bReturnValue = false ;
			int iAccountId = 0 ;
			int iBatchNumebr = 0 ; 
			string sDate = string.Empty ;
			string sPdfDocPath = string.Empty ;

			try
			{
				// Get the Batch Number
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//BatchNumber" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("BatchNumberMissing", Globalization.GetString("PrintChecksAdaptor.BatchNumberNotFound", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iBatchNumebr = Conversion.ConvertStrToInteger( objTargetElement.InnerText );
				
				// Get the Account Id 
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//AccountId" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("AccountIdMissing", Globalization.GetString("PrintChecksAdaptor.AccountIdNotFound", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iAccountId = Conversion.ConvertStrToInteger( objTargetElement.InnerText );
				
				// Get the Post Check Date
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//PostcheckDate" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("DateMissing", Globalization.GetString("PrintChecksAdaptor.DateNotFound", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sDate = objTargetElement.InnerText ;
				
				// Initialize the Check Register object.
                objCheckRegister = new CheckRegister(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);

				// Call PostCheckSummary Function.
				bReturnValue = objCheckRegister.PostCheckSummary( iAccountId , iBatchNumebr , sDate , ref sPdfDocPath );

				// If return value true then copy the binary content of file in the p_objXmlOut doc.
				if( bReturnValue )
				{		
					p_objXmlOut = new XmlDocument();
					objRootElement = p_objXmlOut.CreateElement( "PrintChecks" );
					p_objXmlOut.AppendChild( objRootElement );
					this.CreateNodeWithFileContent( "PostCheckSummary", objRootElement , sPdfDocPath );
					return( true );
				}
				return( false );
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch( Exception p_objException )
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintChecksAdaptor.PostCheckSummary.DateNotFound", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				if( objCheckRegister != null )
				{
					objCheckRegister.Dispose();
					objCheckRegister = null ;
				}
				objTargetElement = null ;
				objRootElement = null ;
			}
		}

		/// <summary>
		///		This method is a wrapper to CheckRegister.PostCheckDetail method.
		///		This method will return XML document which contains the PDF file( PostCheckDetail report ) 
		///		in binary format for the given criteria. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PrintChecks>				
		///				<PostCheckDetail>
		///					<AccountId>Account Id</AccountId> 
		///					<BatchNumber>Batch Number</BatchNumber> 
		///					<OrderBy>Order By Field</OrderBy>
		///					<PostcheckDate>Post Check Date</PostcheckDate> 					 
		///				</PostCheckDetail>
		///			</PrintChecks>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// 
		///		The structure of output XML document would be:
		///		<PrintChecks>
		///			<File Name="xyz"></File> 
		///		</PrintChecks>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool PostCheckDetail( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut )
		{
			CheckRegister objCheckRegister = null ;
			XmlElement objTargetElement = null ;
			XmlElement objRootElement = null ;
			
			bool bReturnValue = false ;
			int iAccountId = 0 ;
			int iBatchNumebr = 0 ;
			// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            int iDistributionType = 0;
			// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
			string sOrderByField = string.Empty ;
			string sDate = string.Empty ;
			string sPdfDocPath = string.Empty ;

			try
			{
				// Get the Batch Number
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//BatchNumber" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("BatchNumberMissing", Globalization.GetString("PrintChecksAdaptor.BatchNumberNotFound", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iBatchNumebr = Conversion.ConvertStrToInteger( objTargetElement.InnerText );
				
				// Get the Account Id 
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//AccountId" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("AccountIdMissing", Globalization.GetString("PrintChecksAdaptor.AccountIdNotFound", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				iAccountId = Conversion.ConvertStrToInteger( objTargetElement.InnerText );
				
				// Get the Report Order By Field
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//OrderBy" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("OrderByFieldMissing", Globalization.GetString("PrintChecksAdaptor.PostCheckDetail.OrderByNotFound", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sOrderByField = objTargetElement.InnerText ;

				// Get the Post Check Date
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//PostcheckDate" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("DateMissing", Globalization.GetString("PrintChecksAdaptor.DateNotFound", base.ClientId), BusinessAdaptorErrorType.Error);
					return false;
				}
				sDate = objTargetElement.InnerText ;
				// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                // Get the Distribution Type
                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//DistributionType");
                if (objTargetElement == null)
                {
                    p_objErrOut.Add("DistributionTypeMissing", Globalization.GetString("PrintChecksAdaptor.DistributionTypeNotFound", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                iDistributionType = Conversion.ConvertStrToInteger(objTargetElement.InnerText);

				// Initialize the Check Register object.
                objCheckRegister = new CheckRegister(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);

				// Call PostCheckDetail Function.
				bReturnValue = objCheckRegister.PostCheckDetail( iAccountId , sOrderByField , iBatchNumebr , sDate , iDistributionType, ref sPdfDocPath );
				// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks

				// If return value true then copy the binary content of file in the p_objXmlOut doc.
				if( bReturnValue )
				{				
					p_objXmlOut = new XmlDocument();
					objRootElement = p_objXmlOut.CreateElement( "PrintChecks" );
					p_objXmlOut.AppendChild( objRootElement );
					this.CreateNodeWithFileContent( "PostCheckDetail", objRootElement , sPdfDocPath );
					return( true );
				}
				return( false );
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch( Exception p_objException )
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintChecksAdaptor.PostCheckDetail.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				if( objCheckRegister != null )
				{
					objCheckRegister.Dispose();
					objCheckRegister = null ;
				}
				objTargetElement = null ;
				objRootElement = null ;
			}
		}
		
		/// <summary>
		///		This method is a wrapper to CheckRegister.SavePreCheckDetail method.
		///		This method will return XML after saving the Pre Check Detail.		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PrintChecks>				
		///				<SavePreCheckDetail>
		///					<AccountId>Account Id</AccountId> 
		///					<OrderBy>Order By Field</OrderBy> 
		///					<IncludeAutoPayment>Include Auto Payment Flag</IncludeAutoPayment> 
		///					<UsingSelection>Using Selection</UsingSelection> 
		///					<NumAutoChecksToPrint>Number of Auto Checks To Print, if Selection is used </NumAutoChecksToPrint> 
		///					<AttachedClaimOnly>Attached Claim Only Flag</AttachedClaimOnly> 
		///					<FromDateFlag>From Date Flag</FromDateFlag> 
		///					<FromDate>From Date</FromDate> 
		///					<ToDateFlag>To Date Flag</ToDateFlag> 
		///					<ToDate>To Date</ToDate> 
		///					<SelectedChecksIds>123,124</SelectedChecksIds>
		///					<SelectedAutoChecksIds>234,235</SelectedAutoChecksIds>
		///				</SavePreCheckDetail>
		///			</PrintChecks>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// 
		///		The structure of output XML document would be:
		///		<PrintChecks>
		///			 <CheckSetChanged>
		///				<NumberOfChecks>20</NumberOfChecks> 
		///				<TotalAmount>$28,628.19</TotalAmount> 
		///				<PrintPre>True</PrintPre> 					
		///			</CheckSetChanged>
		///			<CheckBatchChangedPre>
		///				<NumberOfChecks>0</NumberOfChecks> 
		///				<OrderPreCheck>ADJUSTER_EID</OrderPreCheck> 
		///				<DatePreCheck>3/4/2005</DatePreCheck> 
		///				<PrintBatch>False</PrintBatch> 
		///			</CheckBatchChangedPre>				
		///		</PrintChecks>
		///		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool SavePreCheckDetail( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut )
		{
			CheckRegister objCheckRegister = null ;
			
			try
			{								
				// Initialize the Check Register object.
                objCheckRegister = new CheckRegister(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);

				// Call SavePreCheckDetail Function.
				p_objXmlOut = objCheckRegister.SavePreCheckDetail( p_objXmlIn );
				
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch( Exception p_objException )
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintChecksAdaptor.SavePreCheckDetail.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				if( objCheckRegister != null )
				{
					objCheckRegister.Dispose();
					objCheckRegister = null ;
				}				
			}
		}

		/// <summary>
		///		This method is a wrapper to CheckRegister.PreCheckDetail method.
		///		This method will return XML document which contains the PDF file( PreCheckDetail report ) 
		///		in binary format for the given criteria. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PrintChecks>				
		///				<PreCheckDetail>
		///					<AccountId>Account Id</AccountId> 
		///					<OrderBy>Order By Field</OrderBy> 
		///					<IncludeAutoPayment>Include Auto Payment Flag</IncludeAutoPayment> 
		///					<UsingSelection>Using Selection</UsingSelection> 
		///					<NumAutoChecksToPrint>Number of Auto Checks To Print, if Selection is used </NumAutoChecksToPrint> 
		///					<AttachedClaimOnly>Attached Claim Only Flag</AttachedClaimOnly> 
		///					<FromDateFlag>From Date Flag</FromDateFlag> 
		///					<FromDate>From Date</FromDate> 
		///					<ToDateFlag>To Date Flag</ToDateFlag> 
		///					<ToDate>To Date</ToDate> 
		///					<SelectedChecksIds>123,124</SelectedChecksIds>
		///					<SelectedAutoChecksIds>234,235</SelectedAutoChecksIds>
		///				</PreCheckDetail>
		///			</PrintChecks>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// 
		///		The structure of output XML document would be:
		///		<PrintChecks>
		///			<File Name="xyz"></File> 					
		///		</PrintChecks>
		///		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool PreCheckDetail( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut )
		{
			CheckRegister objCheckRegister = null ;
			XmlElement objRootElement = null ;
			
			bool bReturnValue = false ;		
			string sPdfDocPath = string.Empty ;
			
			try
			{								
				// Initialize the Check Register object.
                objCheckRegister = new CheckRegister(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);

				// Call PreCheckDetail Function.
				bReturnValue = objCheckRegister.PreCheckDetail( p_objXmlIn , ref sPdfDocPath );

				// If return value true then copy the binary content of file in the p_objXmlOut doc.
                if (bReturnValue && sPdfDocPath != "")
				{			
					p_objXmlOut = new XmlDocument();
					objRootElement = p_objXmlOut.CreateElement( "PrintChecks" );
					p_objXmlOut.AppendChild( objRootElement );
					
					this.CreateNodeWithFileContent( "PreCheckDetail" , objRootElement , sPdfDocPath );
										
					return( true );
				}
				return( false );
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch( Exception p_objException )
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintChecksAdaptor.PreCheckDetail.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				if( objCheckRegister != null )
				{
					objCheckRegister.Dispose();
					objCheckRegister = null ;
				}
				objRootElement = null ;
			}
		}

		/// <summary>
		///		This method is a wrapper to CheckManager.GetInsufficientFundsPreEditFlag method.
		///		This method will return XML document which contains the insufficient fund flag. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PrintChecks>								
		///				<PreCheckInsuffFund>
		///					<AccountId>1</AccountId> 
		///					<FromDateFlag>0</FromDateFlag> 
		///					<FromDate>0</FromDate> 
		///					<ToDateFlag>1</ToDateFlag> 
		///					<ToDate>20041230</ToDate> 
		///					<IncludeAutoPayment>1</IncludeAutoPayment> 
		///				</PreCheckInsuffFund> 		
		///			</PrintChecks>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// 
		///		The structure of output XML document would be:
		///		<PrintChecks>
		///			<InsufficientFund>True</InsufficientFund>
		///		</PrintChecks>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool GetInsufficientFundsPreEditFlag( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut )
		{
			CheckManager objCheckManager = null ;
            CheckRegister objCheckRegister = null;
			
			try
			{								
				// Initialize the Check Manager object.
				objCheckManager = new CheckManager( userLogin.objRiskmasterDatabase.DataSourceName , userLogin.LoginName ,userLogin.Password, base.ClientId );

				// Call GetInsufficientFundsPreEditFlag Function.
				p_objXmlOut = objCheckManager.GetInsufficientFundsPreEditFlag( p_objXmlIn );
                //rsushilaggar MITS 37250 11/18/2014
                objCheckRegister = new CheckRegister(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, userLogin.UserId, userLogin.GroupId, base.ClientId);
                objCheckRegister.CheckTransDateOnPrintedChecks(p_objXmlIn);
				
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch( Exception p_objException )
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintChecksAdaptor.GetInsufficientFundsPreEditFlag.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				if( objCheckManager != null )
				{
					objCheckManager.Dispose();
					objCheckManager = null ;
				}				
			}
		}

		#endregion 

		#region Public Methods For Check Manager
		/// <summary>
		///		This method is a wrapper to CheckManager.PrintSampleCheck method.
		///		This method will return XML document which contains the PDF file( Sample Check PDF ) 
		///		in binary format for the given check stock id. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PrintChecks>				
		///				<PrintSampleCheck>
		///					<CheckStockId>Check Stock Id</CheckStockId> 	
		///				</PrintSampleCheck>
		///			</PrintChecks>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// 
		///		The structure of output XML document would be:
		///		<PrintChecks>
		///			<File Name="xyz"></File> 
		///		</PrintChecks>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool PrintSampleCheck( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut )
		{
			CheckManager objCheckManager = null ;
			XmlElement objTargetElement = null ;
			XmlElement objRootElement = null ;
			
			int iCheckStockId = 0 ;
            string sSampleText = string.Empty; 
			string sPdfDocPath = string.Empty ;

			try
			{
				// Get the Check Stock Id
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//CheckStockId" );
				if( objTargetElement == null )
				{
                    p_objErrOut.Add("CheckStockIdMissing", Globalization.GetString("PrintChecksAdaptor.PrintSampleCheck.CheckStockNotFound", base.ClientId), BusinessAdaptorErrorType.Error);
					return false ;
				}
				iCheckStockId = Conversion.ConvertStrToInteger( objTargetElement.InnerText );
                //Ashish Ahuja - Pay To The Order Config starts
                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//SampleText");
                if (objTargetElement == null)
                {
                    p_objErrOut.Add("CheckStockSetupAdaptorParameterMissing", Globalization.GetString("CheckStockSetupAdaptorParameterMissing",base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                sSampleText = objTargetElement.InnerText;

                //Ashish Ahuja - Pay To The Order Config ends		
				// Initialize the Check Manager object.
                objCheckManager = new CheckManager(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);

				// Call PrintSampleCheck Function.
				objCheckManager.PrintSampleCheck( iCheckStockId ,sSampleText, ref sPdfDocPath );

				// Copy the binary content of PDF file in the p_objXmlOut doc.
				p_objXmlOut = new XmlDocument();
				objRootElement = p_objXmlOut.CreateElement( "PrintChecks" );
				p_objXmlOut.AppendChild( objRootElement );
				this.CreateNodeWithFileContent( "PrintSampleCheck", objRootElement , sPdfDocPath );

				return( true );				
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch( Exception p_objException )
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintChecksAdaptor.PrintSampleCheck.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				if( objCheckManager != null )
				{
					objCheckManager.Dispose();
					objCheckManager = null ;
				}
				objTargetElement = null ;
				objRootElement = null ;
			}
		}

		/// <summary>
		///		This method is a wrapper to CheckManager.InsufficientFundsPreEdit method.
		///		This method will return XML document which contains the PDF file( Insufficient Funds Report PDF ) 
		///		for the given account. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PrintChecks>								
		///				<PreCheckInsuffFund>
		///					<AccountId>1</AccountId> 
		///					<FromDateFlag>0</FromDateFlag> 
		///					<FromDate>0</FromDate> 
		///					<ToDateFlag>1</ToDateFlag> 
		///					<ToDate>20041230</ToDate> 
		///					<IncludeAutoPayment>1</IncludeAutoPayment> 
		///				</PreCheckInsuffFund> 		
		///			</PrintChecks>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// 
		///		The structure of output XML document would be:
		///		<PrintChecks>
		///			<File Name="xyz"></File> 
		///		</PrintChecks>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool InsufficientFundsPreEdit( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut )
		{
			CheckManager objCheckManager = null ;
			XmlElement objRootElement = null ;
			string sPdfDocPath = string.Empty ;
			bool bReturnValue = false ;
			
			try
			{
				// Initialize the Check Manager object.
                objCheckManager = new CheckManager(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);

				// Call InsufficientFundsPreEdit Function.
				bReturnValue = objCheckManager.InsufficientFundsPreEdit( p_objXmlIn , ref sPdfDocPath );

				// Copy the binary content of PDF file in the p_objXmlOut doc.
				if( bReturnValue )
				{		
					p_objXmlOut = new XmlDocument();
					objRootElement = p_objXmlOut.CreateElement( "PrintChecks" );
					p_objXmlOut.AppendChild( objRootElement );

					this.CreateNodeWithFileContent( "InsufficientFunds", objRootElement , sPdfDocPath );
				}
				return( true );				
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch( Exception p_objException )
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintChecksAdaptor.InsufficientFundsPreEdit.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				if( objCheckManager != null )
				{
					objCheckManager.Dispose();
					objCheckManager = null ;
				}
				objRootElement = null ;
			}
		}
        public bool GetPrintCheckFile(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sFileName = p_objXmlIn.SelectSingleNode("//FileName").InnerText;
            string sFileType = p_objXmlIn.SelectSingleNode("//FileType").InnerText;
            string sFilePath = "";
            MemoryStream objReturnValue = null; //Add by kuladeep for move file into database Jira 527
            XmlElement objOutElement = null;

            try
            {
                if ((sFileType == "check") ||(sFileType=="holdPayments"))
                {
                    //Add by kuladeep for move file into database Start Jira 527
                    //sFilePath = String.Format("{0}\\{1}", RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "PrintChecks"), sFileName);
                    objReturnValue = CommonFunctions.RetreiveTempFileFromDB(sFileName, sFilePath, userLogin.objRiskmasterDatabase.ConnectionString, base.ClientId);
                    if (objReturnValue == null)
                    {
                        sFilePath = String.Format("{0}\\{1}", RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "PrintChecks"), sFileName);
                    }
                    //Add by kuladeep for move file into database End
                }
                else
                {
                    //Code add by kuladeep for Cloud Jira-163
                    //sFilePath = String.Format("{0}\\{1}", RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "EOB"), sFileName);
                    objReturnValue = CommonFunctions.RetreiveTempFileFromDB(sFileName, sFilePath, userLogin.objRiskmasterDatabase.ConnectionString, base.ClientId);
                    if (objReturnValue == null)
                    {
                        sFilePath = String.Format("{0}\\{1}", RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "EOB"), sFileName);
                    }
                }
                //if ((sFileType == "check" || sFileType == "eob") & objReturnValue != null)
                if (objReturnValue != null)//Add case by kuladeep for move file into database Jira 527
                {
                    p_objXmlOut = new XmlDocument();
                    objOutElement = p_objXmlOut.CreateElement("File");
                    p_objXmlOut.AppendChild(objOutElement);
                    objOutElement = p_objXmlOut.CreateElement("File");
                    objOutElement.SetAttribute("name", sFileName);
                    objOutElement.InnerText = Convert.ToBase64String(objReturnValue.ToArray());
                    p_objXmlOut.FirstChild.AppendChild(objOutElement);
                }
                else
                {
                    p_objXmlOut = new XmlDocument();
                    XmlElement objRootElement = p_objXmlOut.CreateElement("PrintChecks");
                    p_objXmlOut.AppendChild(objRootElement);
                    if (File.Exists(sFilePath))
                    {
                        this.CreateNodeWithFileContent("PrintCheckFile", objRootElement, sFilePath);
                    }
                }
                return (true);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintChecksAdaptor.GetPrintCheckFile.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
               
            }
        }
        public bool GetPrintJob(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            CheckManager objCheckManager = null;
            string sOutJobXml = string.Empty;
            try
            {
                // Initialize the Check Manager object.
                objCheckManager = new CheckManager(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);
                if(p_objXmlIn.SelectSingleNode("//JobId")!=null)
                {
                    sOutJobXml = objCheckManager.GetPrintJob(p_objXmlIn.SelectSingleNode("//JobId").InnerText);
                }
                p_objXmlOut = new XmlDocument();
                p_objXmlOut.LoadXml(sOutJobXml);
                return (true);
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("PrintChecksAdaptor.PrintSampleCheck.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                if (objCheckManager != null)
                {
                    objCheckManager.Dispose();
                    objCheckManager = null;
                }
            }
        }
		/// <summary>
		///		This method is a wrapper to CheckManager.PrintChecksBatch method.
		///		This method will return XML document which contains the PDF file( PDF contains complete Check ). 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<PrintChecks>								
		///				<PrintChecksBatch>
		///					<CheckId>0</CheckId>
		///					<BatchNumber>1</BatchNumber>
		///					<AccountId>17</AccountId> 
		///					<CheckStockId>64</CheckStockId>   
		///					<FirstCheckNum>7</FirstCheckNum>
		///					<OrderBy></OrderBy>
		///					<CheckDate>20041229</CheckDate>
		///					<IncludeAutoPayment>1</IncludeAutoPayment>
		///					<UsingSelection>0</UsingSelection>
		///					<NumAutoChecksToPrint>0</NumAutoChecksToPrint> 
		///				</PrintChecksBatch>
		///		</Document> 
		/// 
		/// </param>
		/// <param name="p_objXmlOut">XML contains the output document</param>
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>
		public bool PrintChecksBatch( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut )
		{
			CheckManager objCheckManager = null ;
			ArrayList arrlstEobPdfPaths = null ;
			string[] arrPDFFileNameAndId = null ;
			XmlElement objRootElement = null ;
			XmlElement objFilesNode = null ;
			string sPdfCheckPath = string.Empty ;
			string sErrorXml = string.Empty ;
			bool bReturnValue = true ;
			XmlDocument objOutputDoc = null ;
			XmlElement objPrintCheckDetails = null ;
            string sFileNameAndType = "";
            bool bPrintCheckPdfOrDirect = false;
            bool bReCreate = false;//Ashutosh To Set the Reprint of check true or false
			try
			{
                if (!Functions.IsPrinterSelected(userLogin.objRiskmasterDatabase.ConnectionString,base.ClientId))
                {
                    throw new RMAppException("No Printer Selected");
                }
                bPrintCheckPdfOrDirect = Functions.IsDirectToPrinter(userLogin.objRiskmasterDatabase.ConnectionString);
				arrlstEobPdfPaths = new ArrayList();

				// Initialize the Check Manager object.
				objCheckManager = new CheckManager( userLogin.objRiskmasterDatabase.DataSourceName , userLogin.LoginName ,userLogin.Password , userLogin.objRiskmasterDatabase.DataSourceId.ToString(), base.ClientId );

                //Ashutosh To set the isRecreate option

                objCheckManager.PrintChecksBatch(p_objXmlIn, ref sPdfCheckPath, arrlstEobPdfPaths, ref objOutputDoc);

				// Copy the binary content of PDF file in the p_objXmlOut doc.
                if (bReturnValue)
                {
                    p_objXmlOut = new XmlDocument();
                    objRootElement = p_objXmlOut.CreateElement("PrintChecks");
                    // Adding attribute which tell us on UI that are we Printing directly to Printer or not
                    if (bPrintCheckPdfOrDirect == true)
                    {
                        objRootElement.SetAttribute("PrintDirectlyToPrinter", "true");
                    }
                    else
                    {
                        objRootElement.SetAttribute("PrintDirectlyToPrinter", "false");
                    }
                    p_objXmlOut.AppendChild(objRootElement);

                    objPrintCheckDetails = objRootElement.OwnerDocument.CreateElement("PrintCheckDetails");
                    objRootElement.AppendChild(objPrintCheckDetails);

					objPrintCheckDetails.InnerXml = objOutputDoc.SelectSingleNode( "//PrintCheckDetails" ).InnerXml;

					objFilesNode = p_objXmlOut.CreateElement( "Files" );
					objRootElement.AppendChild( objFilesNode );

                        // Copy the check PDF content.
                        // If "Print to Printer" Option is selected then return the Check and EOB PDFs.
                        if (File.Exists(sPdfCheckPath))
                        {//Amitosh

                            if (sPdfCheckPath.LastIndexOf(".csv") > 0)
                                sFileNameAndType += this.CreateNodeWithFileContentForPrintBatch("PrintCheck", objFilesNode, sPdfCheckPath, "eft", bPrintCheckPdfOrDirect) + ",";
                            else
                            {
                                sFileNameAndType += this.CreateNodeWithFileContentForPrintBatch("PrintCheck", objFilesNode, sPdfCheckPath, "check", bPrintCheckPdfOrDirect) + ",";
                                //nsachdeva2 - MITS:26428 - 12/28/2011
                                if (p_objXmlIn.SelectSingleNode("//FromTaskManager") != null) //MITS 26949 - Anshul   Added check when request is coming from scheduler
                                {
                                    if (objCheckManager.arrPdfPath != null && objCheckManager.arrPdfPath.Length > 0)   //MITS 26949 - Anshul     Added check for  objCheckManager.arrPdfPath  null
                                    {
                                        foreach (string Path in objCheckManager.arrPdfPath)
                                        {
                                            if (Path != sPdfCheckPath && File.Exists(Path))
                                            {
                                                sFileNameAndType += this.CreateNodeWithFileContentForPrintBatch("PrintCheck", objFilesNode, Path, "check", bPrintCheckPdfOrDirect) + ",";
                                            }
                                        }
                                    }
                                }
                                //End - MITS:26428 - 12/28/2011
                            }
                            
                            if (!objCheckManager.PdfAdditionalPath.Equals(string.Empty))
                            {
                                if (File.Exists(objCheckManager.PdfAdditionalPath))
                                {
                                    if (sPdfCheckPath.LastIndexOf(".csv") > 0)
                                        sFileNameAndType += this.CreateNodeWithFileContentForPrintBatch("AddlDetail", objFilesNode, objCheckManager.PdfAdditionalPath, "eft", bPrintCheckPdfOrDirect) + ",";
                                    else
                                                                            sFileNameAndType += this.CreateNodeWithFileContentForPrintBatch("AddlDetail", objFilesNode, objCheckManager.PdfAdditionalPath, "check", bPrintCheckPdfOrDirect) + ",";
                                }
                            }

					
						// For each Eob PDF, copy the contents.
						foreach( string sPdfEobPath in arrlstEobPdfPaths )
						{
							arrPDFFileNameAndId = sPdfEobPath.Split( "|".ToCharArray() );

                                if (arrPDFFileNameAndId.Length >= 2)
                                    if (arrPDFFileNameAndId[0] != "")
                                        sFileNameAndType += this.CreateNodeWithFileContentForPrintBatch(arrPDFFileNameAndId[1], objFilesNode, arrPDFFileNameAndId[0], "eob", bPrintCheckPdfOrDirect) + ",";

                            }
                        }
                        //nsachdeva2 - MITS:26428 - 12/28/2011
                        if (p_objXmlIn.SelectSingleNode("//FromTaskManager") != null)   //MITS 26949 - Anshul   Added check when request is coming from scheduler
                        {
                            // npadhy JIRA 12516 - In Case of Printer and Custom File only PF file is created
                            if (File.Exists(objCheckManager.CsvPDPath))
                            {
                                sFileNameAndType += this.CreateNodeWithFileContentForPrintBatch("PrintCheck", objFilesNode, objCheckManager.CsvPDPath, "check", bPrintCheckPdfOrDirect) + ",";
                            }
                            if(File.Exists(objCheckManager.CsvPFPath))
                            {
                                sFileNameAndType += this.CreateNodeWithFileContentForPrintBatch("PrintCheck", objFilesNode, objCheckManager.CsvPFPath, "check", bPrintCheckPdfOrDirect) + ",";
                            }
                        }
                        //End - MITS:26428 - 12/28/2011
                        if (!string.IsNullOrEmpty(objCheckManager.HoldAutoChecks))
                        {
                            if (File.Exists(objCheckManager.HoldAutoChecksFilePath))
                            {
                                sFileNameAndType += this.CreateNodeWithFileContentForPrintBatch("holdPayments", objFilesNode, objCheckManager.HoldAutoChecksFilePath, "holdPayments", bPrintCheckPdfOrDirect) + ",";
                            }
                        }
                        
                    if (sFileNameAndType.Length >1)
                    {
                        sFileNameAndType = sFileNameAndType.Substring(0, sFileNameAndType.Length - 1);
                    } // if

                    objRootElement.SetAttribute("FileNameAndType", sFileNameAndType);
                }
				return( true );				
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch( Exception p_objException )
			{
				p_objErrOut.Add( p_objException , Globalization.GetString("PrintChecksAdaptor.PrintChecksBatch.Error", base.ClientId), BusinessAdaptorErrorType.Error );//rkaur27
				return false;
			}
			finally
			{
				if( objCheckManager != null )
				{
					objCheckManager.Dispose();
					objCheckManager = null ;
				}							
				arrlstEobPdfPaths = null ;
				objRootElement = null ;
				objFilesNode = null ;				
			}
		}

		#endregion 

		#region Public Methods For SingleCheck
		
		public bool LoadSingleCheck( XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
		{
			SingleCheck objSingleCheck = null ;
			XmlElement objTargetElement = null;
 
			int iTransId = 0 ;
            // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            //bool bPrintEFTPayment = false; 		//JIRA:438 START: ajohari2
            int iDistributionType = 0;
            // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
			double dblTotalColletion = 0 ;
            string sLangCode = string.Empty;
			try
			{
				if (!userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_PRINT_CHK))
					throw new PermissionViolationException(RMPermissions.RMO_PRINT_CHK,RMB_FUNDS_TRANSACT);
				
				// Get the Trans Id.
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//TransId" );
				if( objTargetElement == null )
				{
					p_objErrOut.Add( "BatchNumberMissing" , Globalization.GetString("PrintChecksAdaptor.TransIdNotFound", base.ClientId) , BusinessAdaptorErrorType.Error );
					return false;
				}
				iTransId = Conversion.ConvertStrToInteger( objTargetElement.InnerText );

                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //JIRA:438 START: ajohari2
                //objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//PrintEFTPayment");
                //if (objTargetElement != null)
                //{
                //    bPrintEFTPayment = Conversion.ConvertStrToBool(objTargetElement.InnerText);
                //}
                //JIRA:438 End: 
				objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//DistributionType");
                if (objTargetElement == null)
                {
                    p_objErrOut.Add("DistributionTypeMissing", Globalization.GetString("PrintChecksAdaptor.DistributionTypeNotFound", base.ClientId), BusinessAdaptorErrorType.Error);
                    return false;
                }
                iDistributionType = Conversion.ConvertStrToInteger(objTargetElement.InnerText);
                // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment

				// Get the Total Payment Amount.
				objTargetElement = ( XmlElement ) p_objXmlIn.SelectSingleNode( "//TotalCollection" );
				if( objTargetElement == null )
				{
					p_objErrOut.Add( "AccountIdMissing" , Globalization.GetString("PrintChecksAdaptor.TotalCollNotFound", base.ClientId) , BusinessAdaptorErrorType.Error );
					return false;
				}
				dblTotalColletion = Conversion.ConvertStrToDouble( objTargetElement.InnerText );

                objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//LangCode");
                if (objTargetElement != null)
                {
                    sLangCode = objTargetElement.InnerText;
                }
                

				// Create instance of the SingleCheck.
				objSingleCheck = new SingleCheck( userLogin.objRiskmasterDatabase.DataSourceName , userLogin.LoginName ,userLogin.Password , userLogin.UserId , userLogin.GroupId, base.ClientId );

				// Invoke the LoadSingleCheck function.
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                p_objXmlOut = objSingleCheck.LoadSingleCheck(iTransId, dblTotalColletion, sLangCode, iDistributionType);//JIRA:438 : ajohari2
                // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
				return true;
			}
			catch( RMAppException p_objException )
			{
				p_objErrOut.Add( p_objException , BusinessAdaptorErrorType.Error );
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add( p_objException, Globalization.GetString("PrintChecksAdaptor.LoadSingleCheck.Error", base.ClientId), BusinessAdaptorErrorType.Error );
				return false;
			}
			finally
			{
				if( objSingleCheck != null )
				{
					objSingleCheck.Dispose();
					objSingleCheck = null ;
				}
				objTargetElement = null;
			}
		}
		#endregion 

		#region Private Methods
		/// <summary>
		/// This function appends the output file to the output xml document.
		/// </summary>
		/// <param name="p_sNodeName">Name of the Node</param>
		/// <param name="p_objParentNode">Parent Node</param>
		/// <param name="p_sOutFilepath">Complete Path of the file.</param>
		private void CreateNodeWithFileContent( string p_sNodeName, XmlElement p_objParentNode, string p_sOutFilepath )
		{
			XmlElement objElement = null;
			MemoryStream objMemoryStream = null;
			FileStream objFileStream = null ;
			BinaryReader objBReader = null ;
			byte[] arrRet = null ;
			try
			{
				// Check the file existence.
				if( ! File.Exists( p_sOutFilepath ) )
					throw new RMAppException( Globalization.GetString( "PrintChecksAdaptor.CreateNodeWithFileContent.FileNotFound" , base.ClientId));
				
				// Read the file in memory stream.
				objFileStream = new FileStream( p_sOutFilepath ,FileMode.Open);
				objBReader = new BinaryReader( objFileStream );
				arrRet = objBReader.ReadBytes( (int)objFileStream.Length );				
				objMemoryStream = new MemoryStream( arrRet );
				
				objFileStream.Close();

				// Delete the temp file.
				//File.Delete( p_sOutFilepath );
				
				// Insert the memory stream as base64 string into the Xml document.
				objElement = p_objParentNode.OwnerDocument.CreateElement( "File" );											
				objElement.SetAttribute( "Name", p_sNodeName );
                //Changed to get FileName in xml 
                objElement.SetAttribute( "FileName", System.IO.Path.GetFileName(p_sOutFilepath) );             																
				objElement.InnerText = Convert.ToBase64String( objMemoryStream.ToArray() );
				p_objParentNode.AppendChild( objElement );				
			}
			catch( Exception p_objException )
			{
				throw new RMAppException( Globalization.GetString("PrintChecksAdaptor.CreateNodeWithFileContent.Error", base.ClientId ), p_objException ) ;
			}
			finally
			{
				objElement = null;
				if( objMemoryStream != null ) 
				{
					objMemoryStream.Close();
                    objMemoryStream.Dispose();					
				}
				if( objFileStream != null )
				{
					//objFileStream.Close();
                    objFileStream.Dispose();					
				}
				if( objBReader != null )
				{
					objBReader.Close();                    
					objBReader = null ;
				}
			}
		}
        private string CreateNodeWithFileContentForPrintBatch(string p_sNodeName, XmlElement p_objParentNode, string p_sOutFilepath, string p_sFileType, bool p_bPrintCheckPdfOrDirect)
        {
            XmlElement objElement = null;
            MemoryStream objMemoryStream = null;
            FileStream objFileStream = null;
            BinaryReader objBReader = null;
            byte[] arrRet = null;
            string sFileNameAndType="";
            try
            {
                // Check the file existence.
                if (!File.Exists(p_sOutFilepath))
                    throw new RMAppException(Globalization.GetString("PrintChecksAdaptor.CreateNodeWithFileContent.FileNotFound", base.ClientId));
                if (p_bPrintCheckPdfOrDirect != true)
                {
                    // Read the file in memory stream.
                    objFileStream = new FileStream(p_sOutFilepath, FileMode.Open);
                    objBReader = new BinaryReader(objFileStream);
                    arrRet = objBReader.ReadBytes((int)objFileStream.Length);
                    objMemoryStream = new MemoryStream(arrRet);

                    objFileStream.Close();

                    // Delete the temp file.
                    //File.Delete( p_sOutFilepath );


                    // Insert the memory stream as base64 string into the Xml document.
                    objElement = p_objParentNode.OwnerDocument.CreateElement("File");
                    objElement.SetAttribute("Name", p_sNodeName);
                    //Changed to get FileName in xml 
                    objElement.SetAttribute("FileName", System.IO.Path.GetFileName(p_sOutFilepath));


                    objElement.SetAttribute("FileType", p_sFileType);
                    sFileNameAndType = System.IO.Path.GetFileName(p_sOutFilepath) + "~" + p_sFileType;
                    //objElement.InnerText = Convert.ToBase64String(objMemoryStream.ToArray());
                    p_objParentNode.AppendChild(objElement);
                }
                else
                {
                    sFileNameAndType = System.IO.Path.GetFileName(p_sOutFilepath) + "~" + p_sFileType;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PrintChecksAdaptor.CreateNodeWithFileContent.Error", base.ClientId), p_objException);
            }
            finally
            {
                objElement = null;
                if (objMemoryStream != null)
                {
                    objMemoryStream.Close();
                    objMemoryStream.Dispose();
                }
                if (objFileStream != null)
                {
                    //objFileStream.Close();
                    objFileStream.Dispose();
                }
                if (objBReader != null)
                {
                    objBReader.Close();
                    objBReader = null;
                }
            }
            return sFileNameAndType;
        }

		#endregion

	}
}
