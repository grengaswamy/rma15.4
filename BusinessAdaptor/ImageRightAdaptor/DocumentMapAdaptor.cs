﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.ImagRightWrapper;
using Riskmaster.Common;

namespace Riskmaster.BusinessAdaptor
{
    public class DocumentMapAdaptor : BusinessAdaptorBase
    {
        public bool Get(XmlDocument p_xDocIn, ref XmlDocument p_xDocOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sIrDocMapId = string.Empty;
            try
            {
                DocumentMap oDocMap = new DocumentMap(base.userLogin,base.ClientId);

                if (p_xDocIn.SelectSingleNode("//IrDocMapId") != null)
                {
                    sIrDocMapId = p_xDocIn.SelectSingleNode("//IrDocMapId").InnerText;
                }
                if (!string.IsNullOrEmpty(sIrDocMapId) && string.Compare(sIrDocMapId,"0")!=0)
                {
                    p_xDocOut = oDocMap.Get(sIrDocMapId);
                }
                else
                {
                    p_xDocOut = oDocMap.Get();
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DocumentMapAdaptor.GetMethodError", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }

        public bool Save(XmlDocument p_xDocIn, ref XmlDocument p_xDocOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            try
            {
                DocumentMap oDocMap = new DocumentMap(base.userLogin, base.ClientId);
                oDocMap.Save(p_xDocIn);
                p_xDocOut = p_xDocIn;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DocumentMapAdaptor.SaveMethodError", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }

        public bool Delete(XmlDocument p_xDocIn, ref XmlDocument p_xDocOut, ref BusinessAdaptorErrors p_objErrOut)
        {
			bool bSuccess = false;
            string sIrDocMapId = string.Empty;
            try
            {
                DocumentMap oDocMap = new DocumentMap(base.userLogin, base.ClientId);

                if (p_xDocIn.SelectSingleNode("//IrDocMapId") != null)
                {
                    sIrDocMapId = p_xDocIn.SelectSingleNode("//IrDocMapId").InnerText;
                }
                if (!string.IsNullOrEmpty(sIrDocMapId) && string.Compare(sIrDocMapId, "0") != 0)
                {
                    bSuccess = oDocMap.Delete(sIrDocMapId);
                }
                //else
                //{
                //    p_xDocOut = oDocMap.Get();
                //}
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("DocumentMapAdaptor.DeleteMethodError", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return bSuccess;
        }
    }
}
