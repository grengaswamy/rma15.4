using System;
using System.Xml;
using System.IO;

using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Application.DCC;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;

namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File				: DCCAdaptor.cs 
	///* $Revision			: 1.0.0.0 
	///* $Creation Date		: 18-Feb-2005
	///* $Author			: Neelima Dabral
	///***************************************************************	
	
	/// <summary>	
	///	This class is used to call the application layer component for DCC Reports.
	/// </summary>
	public class DCCAdaptor:BusinessAdaptorBase
	{
		#region security codes
		private const int RMB_DCC_NET = 359000;
		private const int RMB_DCC_REPORT = 359100;
		private const int RMB_DCC_TEMPLATE = 359200;
		private const int RMO_ADD = 10;
		private const int RMO_DELETE = 20;
		private const int RMO_EDIT = 30;
		private const int RMO_RESTORE = 40;
		private const int RMO_DCCDISPLAY = 50;
		#endregion

		#region Constructor
		/// <summary>
		/// Class Constructor.
		/// </summary>
		public DCCAdaptor()
		{			
		}
		#endregion

		#region Public Methods
		
		/// <summary>
		/// This method is a wrapper to the LoadAllReports method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<DCC>
		///				<EditPermission>true</EditPermission> 
		///				<AddPermission>true</AddPermission> 
		///				<DeletePermission>true</DeletePermission> 
		///				<RestorePermission>true</RestorePermission> 
		///				<DisplayPermission>true</DisplayPermission> 
		///				<TemplatePermission>true</TemplatePermission> 
		///			</DCC>
		///		</Document>
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///			<reports count="4">
		///			<report name="last10claimsentered" id="115" templateid="133" editallow="1" /> 
		///			<report name="last10eventsentered" id="117" templateid="134" editallow="1" /> 				
		///			<button name="Add" /> 
		///			<button name="Delete" /> 
		///			<button name="Restore" /> 
		///			<button name="DCCDisplay" /> 
		///			<button name="Templates" /> 
		///		</reports>
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool LoadAllReports(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			
			Desktop					objDCC = null;				
			XmlElement				objElement=null;				
			bool					bEditPermission=false;
			bool					bAddPermission=false;
			bool					bDeletePermission=false;
			bool					bRestorePermission=false;
			bool					bDisplayPermission=false;
			bool					bTemplatePermission=false;		
			try
			{
                //Start by Shivendu for MITS 17643
                if (!m_userLogin.IsAllowedEx(RMB_DCC_REPORT, 0))
                    throw new PermissionViolationException(0,RMB_DCC_REPORT);
                //End by Shivendu for MITS 17643
		
				//Get the Edit Permission Flag 
				bEditPermission = m_userLogin.IsAllowedEx(RMB_DCC_REPORT, RMO_EDIT);
				
				//Get the Add Permission Flag 
				bAddPermission = m_userLogin.IsAllowedEx(RMB_DCC_REPORT, RMO_ADD);

				//Get the Delete Permission Flag
				bDeletePermission = m_userLogin.IsAllowedEx(RMB_DCC_REPORT, RMO_DELETE);

				//Get the Restore Permission Flag
				bRestorePermission = m_userLogin.IsAllowedEx(RMB_DCC_REPORT, RMO_RESTORE);

				//Get the Display Permission Flag 
				bDisplayPermission = m_userLogin.IsAllowedEx(RMB_DCC_REPORT, RMO_DCCDISPLAY);

				//Get the Template Permission Flag 
				bTemplatePermission = m_userLogin.IsAllowedEx(RMB_DCC_TEMPLATE, 0);

				objDCC = new Desktop(connectionString,base.ClientId);

				p_objOutXML = objDCC.LoadAllReports(bEditPermission,bAddPermission,
					bDeletePermission,bRestorePermission,bDisplayPermission,bTemplatePermission);
				

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException ,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.LoadAllReports.Error", base.ClientId),//sonali
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}


		/// <summary>
		/// This method is a wrapper to the LoadAllTemplates method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<DCC>
		///				<EditPermission>true</EditPermission> 
		///				<AddPermission>true</AddPermission> 
		///				<DeletePermission>true</DeletePermission> 
		///				<RestorePermission>true</RestorePermission> 
		///				<ReportPermission>true</ReportPermission> 
		///			</DCC>
		///		<Document>
		/// </param>
		/// <param name="p_objOutXML">Output XML
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages
		///		<templates count="4">
		///			<template name="claimcount" id="1" editallow="0" /> 
		///			<template name="eventcount" id="3" editallow="0" /> 
		///			<template name="topxpayee" id="5" editallow="0" /> 
		///			<template name="lastxclaimsentered" id="6" editallow="0" /> 
		///			<button name="Add" /> 
		///			<button name="Delete" /> 
		///			<button name="Restore" /> 
		///			<button name="Reports" /> 
		///		</templates>
		/// </param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool LoadAllTemplates(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			
			Desktop					objDCC = null;				
			XmlElement				objElement=null;				
			bool					bEditPermission=false;
			bool					bAddPermission=false;
			bool					bDeletePermission=false;
			bool					bRestorePermission=false;			
			bool					bReportPermission=false;		
			try
			{
		
				//Get the Edit Permission Flag 
				bEditPermission = m_userLogin.IsAllowedEx(RMB_DCC_TEMPLATE, RMO_EDIT);

				//Get the Add Permission Flag 
				bAddPermission = m_userLogin.IsAllowedEx(RMB_DCC_TEMPLATE, RMO_ADD);

				//Get the Delete Permission Flag 
				bDeletePermission = m_userLogin.IsAllowedEx(RMB_DCC_TEMPLATE, RMO_DELETE);

				//Get the Restore Permission Flag 
				bRestorePermission = m_userLogin.IsAllowedEx(RMB_DCC_TEMPLATE, RMO_RESTORE);		

				//Get the Report Permission Flag 
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportPermission");
				if (objElement==null)
				{
                    p_objErrOut.Add("ReportPerMissing", Globalization.GetString("ReportPerMissing", base.ClientId),
						BusinessAdaptorErrorType.Error);										
					return false;
				}
				bReportPermission = Convert.ToBoolean(objElement.InnerText);

				objDCC = new Desktop(connectionString,base.ClientId);

				p_objOutXML = objDCC.LoadAllTemplates(bEditPermission,bAddPermission,
					bDeletePermission,bRestorePermission,bReportPermission);
				

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,	BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.LoadAllTemplates.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}


		/// <summary>
		/// This method is a wrapper to the AddUpdateTemplate method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<DCC>
		///				<TemplateId>0</TemplateId>
		///				<TemplateName>topxpayee</TemplateName>
		///				<ShowPercentage>0</ShowPercentage>
		///				<ShowGraph>0</ShowGraph>
		///			</DCC>
		///		</Document>
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///		<template id="133" name="NewTemplateUpdate" showpercentage="1" showgraph="0">
		///			<filters>
		///				<filter name="rank" /> 
		///				<filter name="forcurrentuser" /> 
		///				<filter name="adjuster" /> 
		///			</filters>
		///			<outputs>
		///				<output name="dateadded" /> 
		///				<output name="claimnumber" /> 
		///				<output name="claimtype" /> 
		///				<output name="lob" /> 
		///				<output name="claimstatus" /> 
		///				<output name="claimtime" /> 
		///				<output name="claimdate" /> 
		///			</outputs>
		///		</template>
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool AddUpdateTemplate(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;				
			XmlElement	objElement=null;		
			long		lTemplateId=0;		
			string		sTemplateName="";
			int			iShowPercentage=0;
			int			iShowGraph=0;
			try
			{
		
				//Get the template id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TemplateId");
				if (objElement != null)									
					lTemplateId = Conversion.ConvertStrToLong(objElement.InnerText);

				//Get the template name
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TemplateName");
				if ((objElement==null) || (objElement.InnerText.Trim() == ""))
				{
                    p_objErrOut.Add("TemplateNameMissing", Globalization.GetString("TemplateNameMissing", base.ClientId),//sonali
						BusinessAdaptorErrorType.Error);					
					return false;
				}								
				sTemplateName = objElement.InnerText;

				//Get the value of Show Percentage
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ShowPercentage");
				if (objElement != null)													
					iShowPercentage = Conversion.ConvertStrToInteger(objElement.InnerText);

				//Get the value of Show Graph
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ShowGraph");
				if (objElement != null)													
					iShowGraph = Conversion.ConvertStrToInteger(objElement.InnerText);				

				objDCC = new Desktop(connectionString,base.ClientId);

				p_objOutXML = objDCC.AddUpdateTemplate(lTemplateId,sTemplateName,iShowPercentage,iShowGraph);
				
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,	BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.AddUpdateTemplate.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}


		/// <summary>
		/// This method is a wrapper to the CreateFilter method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<DCC>
		///				<TemplateId>135</TemplateId>
		///				<OldFilterName></OldFilterName>
		///				<NewFilterName>paymenttype</NewFilterName>
		///				<FilterType>2</FilterType>
		///				<SQL>SELECT CODES_TEXT.CODE_DESC,CODES.CODE_ID FROM CODES,CODES_TEXT WHERE CODES.TABLE_ID = 1075 AND CODES_TEXT.CODE_ID = CODES.CODE_ID</SQL>
		///				<From>FUNDS_TRANS_SPLIT</From>
		///				<Where>FUNDS.TRANS_ID=FUNDS_TRANS_SPLIT.TRANS_ID</Where>
		///				<Parameter>FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE IN</Parameter>
		///				<OrderBy></OrderBy>
		///			</DCC>
		///		</Document>
		/// </param>
		/// <param name="p_objOutXML">Null value.Function only returns a true/false</param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool CreateFilter(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;				
			XmlElement	objElement=null;		
			long		lTemplateId=0;					
			string		sOriginalFilterName="";
			string		sNewFilterName="";
			int			iFilterType=0;
			string		sSQL="";
			string		sSQLFrom="";
			string		sSQLWhere="";
			string      sParameter="";
			string      sOrderBy =""; 
			string      sMinValue="";
			string      sMaxValue="";
			string      sDefaultValue="";

			try
			{
				//Get the Template id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TemplateId");
				if (objElement != null)													
					lTemplateId = Conversion.ConvertStrToLong(objElement.InnerText);

				//Get the Old Filter name
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//OldFilterName");
				if (objElement != null)													
					sOriginalFilterName = objElement.InnerText;

				//Get the New Filter name
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//NewFilterName");
				if (objElement != null)													
					sNewFilterName = objElement.InnerText;
				
				//Get the Filter type
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FilterType");
				if (objElement != null)													
					iFilterType = Conversion.ConvertStrToInteger(objElement.InnerText);			

				//Get the Select part of SQL query for the Filter
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//SQL");
				if (objElement != null)													
					sSQL = objElement.InnerText;

				//Get the From part of SQL query for the Filter
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//From");
				if (objElement != null)													
					sSQLFrom = objElement.InnerText;

				//Get the Where part of SQL query for the Filter
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Where");
				if (objElement != null)													
					sSQLWhere = objElement.InnerText;

				//Get the Parameter part of SQL query for the Filter
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//Parameter");
				if (objElement != null)													
					sParameter = objElement.InnerText;

				//Get the Order By part of SQL query for the Filter
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//OrderBy");
				if (objElement != null)													
					sOrderBy = objElement.InnerText;

				//Get the Min value for the filter of type SPIN / TOPX
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//MinValue");
				if (objElement != null)													
					sMinValue = objElement.InnerText;

				//Get the Max value for the filter of type SPIN / TOPX
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//MaxValue");
				if (objElement != null)													
					sMaxValue = objElement.InnerText;

				//Get the Default value for the filter of type SPIN / TOPX
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//DefaultValue");
				if (objElement != null)													
					sDefaultValue = objElement.InnerText;
				
				objDCC = new Desktop(connectionString,base.ClientId);
				objDCC.CreateFilter(lTemplateId,sOriginalFilterName,sNewFilterName,
					iFilterType,sSQL,sSQLFrom,sSQLWhere,sParameter,sOrderBy,sMinValue,sMaxValue,sDefaultValue);

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,	BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.CreateFilter.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}


		/// <summary>
		/// This method is a wrapper to the CreateOutput method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		/// <Document>
		///		<DCC>
		///			<TemplateId>131</TemplateId>
		///			<OldOutputName></OldOutputName>
		///			<NewOutputName>payee</NewOutputName>
		///			<OutputLabel>Payee</OutputLabel>
		///			<GraphType>X</GraphType>
		///			<DataField>ENTITY.FIRST_NAME,ENTITY.LAST_NAME</DataField>
		///			<JoinSelect>ENTITY.FIRST_NAME&amp;&amp;JOINSELECT&amp;&amp;ENTITY.LAST_NAME</JoinSelect>
		///			<JoinFrom>FUNDS, ENTITY</JoinFrom>
		///			<JoinWhere>FUNDS.PAYEE_EID=ENTITY.ENTITY_ID</JoinWhere>
		///			<FilterDependencies></FilterDependencies>
		///			<OutputDependencies>payeecount</OutputDependencies>
		///		</DCC>
		///	</Document>
		/// </param>
		/// <param name="p_objOutXML">Null value.Function only returns a true/false</param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool CreateOutput(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;				
			XmlElement	objElement=null;
			long		lTemplateId=0;
			string		sOriginalOutputName="";
			string		sNewOutputName="";
			string		sLabel="";
			string      sGraphType ="";
			string      sDataField ="";
			string      sJoinSelect="";
			string      sJoinFrom="";
			string      sJoinWhere="";
			string      sFDependencies="";
			string      sODependencies="";
			
			try
			{
				//Get the value of Template Id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TemplateId");
				if (objElement != null)													
					lTemplateId = Conversion.ConvertStrToLong(objElement.InnerText);

				//Get the value of Old Output name
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//OldOutputName");
				if (objElement != null)													
					sOriginalOutputName = objElement.InnerText;

				//Get the value of New Output name
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//NewOutputName");
				if (objElement != null)													
					sNewOutputName = objElement.InnerText;

				//Get the value of Output Label
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//OutputLabel");
				if (objElement != null)													
					sLabel = objElement.InnerText;

				//Get the value of Graph Type
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//GraphType");
				if (objElement != null)													
					sGraphType = objElement.InnerText;

				//Get the value of Data to be shown
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//DataField");
				if (objElement != null)													
					sDataField = objElement.InnerText;

				//Get the value of Select part of the SQL for the Output Join
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//JoinSelect");
				if (objElement != null)													
					sJoinSelect = objElement.InnerText;

				//Get the value of From part of the SQL for the Output Join
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//JoinFrom");
				if (objElement != null)													
					sJoinFrom = objElement.InnerText;

				//Get the value of Where part of the SQL for the Output Join
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//JoinWhere");
				if (objElement != null)													
					sJoinWhere = objElement.InnerText;

				//Get the value of Filter Dependencies
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FilterDependencies");
				if (objElement != null)													
					sFDependencies = objElement.InnerText;

				//Get the value of Output Dependencies
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//OutputDependencies");
				if (objElement != null)													
					sODependencies = objElement.InnerText;
				
				objDCC = new Desktop(connectionString,base.ClientId);
				objDCC.CreateOutput(lTemplateId,sOriginalOutputName,sNewOutputName,
					sLabel,sGraphType,sDataField,sJoinSelect,sJoinFrom,sJoinWhere,sFDependencies,sODependencies);

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,	BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
				p_objErrOut.Add(p_objException,Globalization.GetString("DCCAdaptor.CreateOutput.Error",base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}


		/// <summary>
		/// This method is a wrapper to the DeleteItem method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		/// <Document>
		///		<DCC>
		///			<ItemList>|F|payeetype|O|payeecount</ItemList>
		///			<ItemType>filteroutput</ItemType>
		///			<TemplateId>127</TemplateId>
		///		</DCC>
		///	</Document>
		/// </param>
		/// <param name="p_objOutXML">Null value.Function only returns a true/false</param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool DeleteItem(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;				
			XmlElement	objElement=null;		
			long		lTemplateId=0;		
			string		sItemList ="";
			string		sItemType ="";
			try
			{
				//Get the Item List
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ItemList");
				if ((objElement==null) || (objElement.InnerText.Trim() == ""))
				{
                    p_objErrOut.Add("NoItemSelected", Globalization.GetString("NoDeleteItemSelected", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}
				else
					sItemList = objElement.InnerText;

				//Get the Item Type
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ItemType");
				if (objElement != null)				
					sItemType = objElement.InnerText;

				//Get the Template id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TemplateId");
				if (objElement != null)													
					lTemplateId = Conversion.ConvertStrToLong(objElement.InnerText);
				
				objDCC = new Desktop(connectionString,base.ClientId);
				objDCC.DeleteItem(sItemList,sItemType,lTemplateId);				
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.DeleteItem.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}


		/// <summary>
		/// This method is a wrapper to the LoadDeletedItem method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<DCC>
		///				<ItemType>template</ItemType>
		///			</DCC>
		///		</Document>
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///		<restorelist itemtype="template">
		///			<item id="38" name="claimcount" /> 
		///			<item id="13" name="eventcount" /> 
		///			<item id="58" name="lastxclaimsentered" /> 		
		///		</restorelist>
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool LoadDeletedItem(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;				
			XmlElement	objElement=null;			
			string		sItemType="";
			try
			{
				//Get the value of Item Type
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ItemType");
				if (objElement != null)													
					sItemType = objElement.InnerText;
				
				objDCC = new Desktop(connectionString,base.ClientId);
				p_objOutXML = objDCC.LoadDeletedItem(sItemType);

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,	BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.LoadDeletedItem.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}


		/// <summary>
		/// This method is a wrapper to the RestoreItem method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<DCC>
		///				<ItemList>126,128</ItemList>
		///				<ItemType>templates</ItemType>
		///			</DCC>
		///		</Document>		
		/// </param>
		/// <param name="p_objOutXML">Null value.Function only returns a true/false</param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool RestoreItem(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;				
			XmlElement	objElement=null;			
			string		sItemType="";
			string		sItemList="";
			try
			{
				//Get the value of Item List
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ItemList");
				if ((objElement==null) || (objElement.InnerText.Trim() == ""))			
				{
                    p_objErrOut.Add("NoItemSelected", Globalization.GetString("NoRestoreItemSelected", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}
				else
					sItemList = objElement.InnerText;

				//Get the value of Item Type
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ItemType");
				if (objElement != null)													
					sItemType = objElement.InnerText;
				
				objDCC = new Desktop(connectionString,base.ClientId);
				objDCC.RestoreItem(sItemList,sItemType);

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,	BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.RestoreItem.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}


		/// <summary>
		/// This method is a wrapper to the NewReport method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<DCC>
		///				<TemplateId>128</TemplateId>
		///				<ReportName>Top10claimsentered</ReportName>
		///				<ReportTitle>Top10claimsentered</ReportTitle>
		///			</DCC>
		///		</Document>
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///		<report name="Top10claimsentered" templatename="lastxclaimsentered">
		///			<filters>
		///				<filter name="rank" selected="0" disabled="1" />
		///				<filter name="forcurrentuser" selected="0" disabled="1" />
		///				<filter name="adjuster" selected="0" disabled="1" />
		///			</filters>
		///			<outputs>
		///				<output name="dateadded" />
		///				<output name="claimnumber" />
		///				<output name="claimtype" />
		///				<output name="lob" />
		///				<output name="claimstatus" />
		///				<output name="claimdate" />
		///				<output name="claimtime" />
		///			</outputs>
		///			<selectedoutputs />
		///			<graphgrouplist />
		///			<graphdatalist />
		///			<graphtype value="1" />
		///			<showgraph value="0" />
		///		</report>
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool NewReport(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;				
			XmlElement	objElement=null;		
			long		lTemplateId=0;		
			string		sReportName="";			
			string		sReportTitle="";			
			try
			{
		
				//Get the template id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TemplateId");
				if (objElement != null)									
					lTemplateId = Conversion.ConvertStrToLong(objElement.InnerText);

				//Get the report name
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportName");
				if ((objElement==null) || (objElement.InnerText.Trim() == ""))
				{
                    p_objErrOut.Add("ReportNameMissing", Globalization.GetString("ReportNameMissing", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}								
				sReportName = objElement.InnerText;		

				//Get the report title
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportTitle");
				if ((objElement==null) || (objElement.InnerText.Trim() == ""))
				{
                    p_objErrOut.Add("ReportTitleMissing", Globalization.GetString("ReportTitleMissing", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}								
				sReportTitle = objElement.InnerText;	

				objDCC = new Desktop(connectionString,base.ClientId);

				p_objOutXML = objDCC.NewReport(lTemplateId,sReportName,sReportTitle);
				
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,	BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.NewReport.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}


		/// <summary>
		/// This method is a wrapper to the EditReport method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<DCC>
		///				<TemplateId>134</TemplateId>
		///				<ReportId>117</ReportId>
		///				<ReportName>last10claimsentered</ReportName>
		///				<FilterList>rank,forcurrentuser,adjuster</FilterList>
		///				<OutputList>claimdate,claimtime,claimstatus,lob,claimtype,claimnumber,dateadded</OutputList>
		///				<GraphGroup>claimdate</GraphGroup>
		///				<GraphData></GraphData>
		///				<GraphType>1</GraphType>
		///				<ShowGraph>on</ShowGraph>
		///			</DCC>
		///		</Document>
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///		<report name="last10claimsentered" templatename="lastxclaimsenteredNet">
		///			<filters>
		///				<filter name="rank" selected="1" disabled="0" />
		///				<filter name="forcurrentuser" selected="1" disabled="0" />
		///				<filter name="adjuster" selected="1" disabled="0" />
		///			</filters>
		///			<outputs>
		///				<output name="dateadded" />		
		///				<output name="claimtype" />
		///				<output name="lob" />
		///				<output name="claimstatus" />		
		///			</outputs>
		///			<selectedoutputs>				
		///				<selectedoutput name="claimstatus" />
		///				<selectedoutput name="lob" />
		///				<selectedoutput name="claimtype" />		
		///				<selectedoutput name="dateadded" />
		///			</selectedoutputs>
		///			<graphgrouplist>		
		///				<graphgroup name="claimstatus" selected="0" />
		///				<graphgroup name="lob" selected="0" />
		///				<graphgroup name="claimtype" selected="0" />		
		///				<graphgroup name="dateadded" selected="0" />
		///			</graphgrouplist>
		///			<graphdatalist />
		///			<graphtype value="1" />
		///			<showgraph value="1" />
		///		</report>
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool EditReport(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;				
			XmlElement	objElement=null;		
			long		lTemplateId=0;		
			long		lReportId =0 ;
			string		sReportName="";
			string		sFilterList="";
			string      sOutputList="";
			string      sGraphGroup="";
			string      sGraphData="";
			string		sGraphType="";
			string		sShowGraph="";
			try
			{
		
				//Get the template id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TemplateId");
				if (objElement != null)									
					lTemplateId = Conversion.ConvertStrToLong(objElement.InnerText);

				if (lTemplateId == 0)
				{
                    p_objErrOut.Add("InvalidTemplateId", Globalization.GetString("InvalidTemplateId", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}

				//Get the report id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportId");
				if (objElement != null)									
					lReportId = Conversion.ConvertStrToLong(objElement.InnerText);

				if (lReportId == 0)
				{
                    p_objErrOut.Add("InvalidReportId", Globalization.GetString("InvalidReportId", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}

				//Get the report name
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportName");
				if ((objElement==null) || (objElement.InnerText.Trim() == ""))
				{
                    p_objErrOut.Add("ReportNameMissing", Globalization.GetString("ReportNameMissing", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}								
				sReportName = objElement.InnerText;		

				//Get the filter list
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FilterList");
				if (objElement != null)									
					sFilterList = objElement.InnerText;		

				//Get the output list
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//OutputList");
				if (objElement != null)									
					sOutputList = objElement.InnerText;		

				//Get the graph group
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//GraphGroup");
				if (objElement != null)									
					sGraphGroup = objElement.InnerText;		

				//Get the graph data
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//GraphData");
				if (objElement != null)									
					sGraphData = objElement.InnerText;		

				//Get the graph type
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//GraphType");
				if (objElement != null)									
					sGraphType = objElement.InnerText;		

				//Get the show graph value
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ShowGraph");
				if (objElement != null)									
					sShowGraph = objElement.InnerText;		

				objDCC = new Desktop(connectionString,base.ClientId);
				p_objOutXML = objDCC.EditReport(lTemplateId,lReportId,sReportName,sFilterList,
					sOutputList,sGraphGroup,sGraphData,sGraphType,sShowGraph);
				
				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,	BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.EditReport.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}


		/// <summary>
		/// This method is a wrapper to the GetFilterNode method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<dcc>
		///				<TemplateId>128</TemplateId>
		///				<FilterName>claimtype</FilterName>
		///			</dcc>
		///		</Document>
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///		<filter name="claimtype" type="2" selected="0" data="" all="0" rankon="">
		///			<sql>SELECT CODES_TEXT.CODE_DESC,CODES.CODE_ID FROM CODES,CODES_TEXT WHERE CODES.TABLE_ID = 1023 AND CODES_TEXT.CODE_ID = CODES.CODE_ID</sql> 
		///			<sqlfrom /> 
		///			<sqlwhere /> 
		///			<parameter>CLAIM_TYPE_CODE IN</parameter> 
		///			<orderby>ORDER BY CODES_TEXT.CODE_DESC</orderby> 
		///		</filter>
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool GetFilterNode(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;				
			XmlElement	objElement=null;
			long		lTemplateId=0;
			string		sFilterName="";
			try
			{
				//Get the value of Template Id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TemplateId");
				if (objElement != null)													
					lTemplateId = Conversion.ConvertStrToLong(objElement.InnerText);

				//Get the value of Filter Name
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FilterName");
				if ((objElement==null) || (objElement.InnerText.Trim() == ""))	
				{
                    p_objErrOut.Add("FilterNameMissing", Globalization.GetString("FilterNameMissing", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}
				else
					sFilterName = objElement.InnerText;
								
				objDCC = new Desktop(connectionString,base.ClientId);
				p_objOutXML = objDCC.GetFilterNode(lTemplateId,sFilterName);

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.GetFilterNode.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}

	
		/// <summary>
		/// This method is a wrapper to the GetOutputNode method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<dcc>
		///				<TemplateId>128</TemplateId>
		///				<OutputName>claimcount</OutputName>
		///			</dcc>
		///		</Document>
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///		<output name="claimcount" label="claimcount" graph="Y" selected="0" dependentselected="0" groupby="0" order="0">
		///			<datafield>COUNT(*)</datafield> 
		///			<joinselect>COUNT(*) ENTRY_COUNT</joinselect> 
		///			<joinfrom>CLAIM</joinfrom> 
		///			<joinwhere>CLAIM.CLAIM_ID <> 0</joinwhere> 
		///			<fdependencies /> 
		///			<odependencies /> 
		///		</output>
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool GetOutputNode(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;				
			XmlElement	objElement=null;
			long		lTemplateId=0;
			string		sOutputName="";
			try
			{
				//Get the value of Template Id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TemplateId");
				if (objElement != null)													
					lTemplateId = Conversion.ConvertStrToLong(objElement.InnerText);

				//Get the value of Output Name
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//OutputName");
				if ((objElement==null) || (objElement.InnerText.Trim() == ""))												
				{
                    p_objErrOut.Add("OutputNameMissing", Globalization.GetString("OutputNameMissing", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}
				else
					sOutputName = objElement.InnerText;
								
				objDCC = new Desktop(connectionString,base.ClientId);
				p_objOutXML = objDCC.GetOutputNode(lTemplateId,sOutputName);

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.GetOutputNode.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}

	
		/// <summary>
		/// This method is a wrapper to the TemplateOptions method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<dcc>
		///				<TemplateId>133</TemplateId>
		///			</dcc>
		///		</Document>
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///		<templates count="4">
		///			<template name="lastxclaimsentered" id="1" selected="0" />
		///			<template name="lastxeventsentered" id="133" selected="1" />
		///		</templates>		
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool TemplateOptions(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;				
			XmlElement	objElement=null;			
			long        lTemplateId=0;
			try
			{
				//Get the value of Template id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TemplateId");
				if (objElement != null)													
					lTemplateId = Conversion.ConvertStrToLong(objElement.InnerText);
				
				objDCC = new Desktop(connectionString,base.ClientId);
				p_objOutXML = objDCC.TemplateOptions(lTemplateId);

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.TemplateOptions.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}

		
		/// <summary>
		/// This method is a wrapper to the GetSelectedReport method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<dcc>
		///				<TemplateId>134</TemplateId>
		///				<ReportId>117</ReportId>
		///			</dcc>
		///		</Document>		
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///			<report name="last10claimsentered" templatename="lastxclaimsentered">
		///				<filters>
		///					<filter name="rank" selected="1" disabled="0" />
		///					<filter name="forcurrentuser" selected="1" disabled="0" />
		///					<filter name="adjuster" selected="1" disabled="0" />
		///				</filters>
		///				<outputs>		
		///					<output name="claimstatus" />
		///					<output name="claimdate" />
		///					<output name="claimtime" />
		///				</outputs>
		///				<selectedoutputs>
		///					<selectedoutput name="claimdate" />
		///					<selectedoutput name="claimtime" />	
		///				</selectedoutputs>
		///				<graphgrouplist>
		///					<graphgroup name="claimdate" selected="1" />
		///					<graphgroup name="claimtime" selected="0" />		
		///				</graphgrouplist>
		///				<graphdatalist />
		///				<graphtype value="1" />
		///				<showgraph value="1" />
		///			</report>		
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool GetSelectedReport(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;				
			XmlElement	objElement=null;			
			long        lTemplateId=0;
			long        lReportId=0;
			try
			{
				//Get the value of Template id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TemplateId");
				if (objElement != null)													
					lTemplateId = Conversion.ConvertStrToLong(objElement.InnerText);

				if (lTemplateId == 0)
				{
                    p_objErrOut.Add("InvalidTemplateId", Globalization.GetString("InvalidTemplateId", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}

				//Get the value of report id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportId");
				if (objElement != null)													
					lReportId = Conversion.ConvertStrToLong(objElement.InnerText);

				if (lReportId == 0)
				{
                    p_objErrOut.Add("InvalidReportId", Globalization.GetString("InvalidReportId", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}
				
				objDCC = new Desktop(connectionString,base.ClientId);
				p_objOutXML = objDCC.GetSelectedReport(lReportId, lTemplateId);

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.GetSelectedReport.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}


		/// <summary>
		/// This method is a wrapper to the GetSelectedTemplate method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<dcc>
		///				<TemplateId>134</TemplateId>
		///			</dcc>
		///		</Document>
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///		<template id="134" name="lastxclaimsenteredNet" showpercentage="0" showgraph="0">
		///			<filters>
		///				<filter name="rank" />
		///				<filter name="forcurrentuser" />
		///				<filter name="adjuster" />
		///			</filters>
		///			<outputs>
		///				<output name="dateadded" />
		///				<output name="claimnumber" />
		///				<output name="claimtype" />
		///				<output name="lob" />
		///				<output name="claimstatus" />
		///				<output name="claimdate" />
		///				<output name="claimtime" />
		///			</outputs>
		///		</template>
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool GetSelectedTemplate(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;				
			XmlElement	objElement=null;			
			long        lTemplateId=0;			
			try
			{
				//Get the value of Template id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TemplateId");
				if (objElement != null)													
					lTemplateId = Conversion.ConvertStrToLong(objElement.InnerText);
				
				objDCC = new Desktop(connectionString,base.ClientId);
				p_objOutXML = objDCC.GetSelectedTemplate(lTemplateId);

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.GetSelectedTemplate.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}


		/// <summary>
		/// This method is a wrapper to the GetSelectedFilterData method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<dcc>
		///				<TemplateId>128</TemplateId>
		///				<ReportId>104</ReportId>
		///				<FilterName>claimstatus</FilterName>
		///			</dcc>
		///		</Document>
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///		<Selectedfilter>
		///			<graphdatalist>
		///				<graphdata name="claimcount" selected="0" /> 
		///			</graphdatalist>
		///			<filter name="claimstatus" type="2" checkall="1">
		///				<filterdata data="Closed" value="11" selected="1" /> 
		///				<filterdata data="Closed Pending Final Chgs" value="2587" selected="1" /> 
		///				<filterdata data="Open" value="10" selected="1" /> 
		///				<filterdata data="Open Attorney Lien" value="2434" selected="1" /> 
		///				<filterdata data="Open Litigated" value="2433" selected="1" /> 
		///				<filterdata data="Open Potential" value="2545" selected="1" /> 
		///				<filterdata data="Reopened" value="1732" selected="1" /> 
		///				<filterdata data="Reopened, Closed" value="1733" selected="1" /> 		
		///			</filter>
		///		</Selectedfilter>
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool GetSelectedFilterData(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;				
			XmlElement	objElement=null;			
			long        lTemplateId=0;			
			long		lReportId=0;
			string		sFilterName="";
			try
			{
				//Get the value of Template id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TemplateId");
				if (objElement != null)													
					lTemplateId = Conversion.ConvertStrToLong(objElement.InnerText);

				if (lTemplateId == 0)
				{
                    p_objErrOut.Add("InvalidTemplateId", Globalization.GetString("InvalidTemplateId", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}

				//Get the value of Report id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportId");
				if (objElement != null)													
					lReportId = Conversion.ConvertStrToLong(objElement.InnerText);

				if (lReportId == 0)
				{
                    p_objErrOut.Add("InvalidReportId", Globalization.GetString("InvalidReportId", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}

				//Get the value of filter name
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FilterName");
				if ((objElement==null) || (objElement.InnerText.Trim() == ""))													
				{
                    p_objErrOut.Add("FilterNameMissing", Globalization.GetString("FilterNameMissing", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}
				else
					sFilterName = objElement.InnerText;				
				
				objDCC = new Desktop(connectionString,base.ClientId);
				p_objOutXML = objDCC.GetSelectedFilterData(lTemplateId,lReportId,sFilterName);

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.GetSelectedFilterData.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}


		/// <summary>
		/// This method is a wrapper to the UpdateFilterData method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<dcc>
		///				<TemplateId>128</TemplateId>
		///				<ReportId>104</ReportId>
		///				<FilterName>paymenttype</FilterName>
		///				<FilterAll>0</FilterAll>
		///				<FilterData>294,2353</FilterData>
		///				<FilterType>2</FilterType>
		///			</dcc>
		///		</Document>	
		/// </param>
		/// <param name="p_objOutXML">Null value.Function only returns a true/false</param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool UpdateFilterData(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;				
			XmlElement	objElement=null;			
			long        lTemplateId=0;			
			long		lReportId=0;
			string		sFilterName="";
			string		sFilterAll="";
			string		sFilterData="";
			string		sFilterType="";
			string		sSelectedRank="";

			try
			{
				//Get the value of Template id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//TemplateId");
				if (objElement != null)													
					lTemplateId = Conversion.ConvertStrToLong(objElement.InnerText);
				
				if (lTemplateId == 0)
				{
                    p_objErrOut.Add("InvalidTemplateId", Globalization.GetString("InvalidTemplateId", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}

				//Get the value of Report id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportId");
				if (objElement != null)													
					lReportId = Conversion.ConvertStrToLong(objElement.InnerText);
				
				if (lReportId == 0)
				{
                    p_objErrOut.Add("InvalidReportId", Globalization.GetString("InvalidReportId", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}

				//Get the value of filter name
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FilterName");
				if((objElement == null) || (objElement.InnerText.Trim() == ""))
				{
                    p_objErrOut.Add("FilterNameMissing", Globalization.GetString("FilterNameMissing", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}
				sFilterName = objElement.InnerText;

				//Get the value of Filter All flag
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FilterAll");
				if (objElement != null)													
					sFilterAll = objElement.InnerText;

				//Get the value of Filter Data
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FilterData");
				if (objElement != null)													
					sFilterData = objElement.InnerText;

				//Get the value of Filter Type
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//FilterType");
				if (objElement != null)													
					sFilterType = objElement.InnerText;

				//Get the value of Selected Rank
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//SelectedRank");
				if (objElement != null)													
					sSelectedRank = objElement.InnerText;
				
				objDCC = new Desktop(connectionString,base.ClientId);
				objDCC.UpdateFilterData(lTemplateId,lReportId,sFilterName,sFilterAll,
					sFilterData,sFilterType,sSelectedRank);

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.UpdateFilterData.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}


		/// <summary>
		/// This method is a wrapper to the GetReportData method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<dcc>
		///				<ReportId>104</ReportId>
		///			</dcc>
		///		</Document>	
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///		<report name="last10claimsentered" showgraph="1">
		///			<columns>
		///				<columnheader name="Claim Date" />
		///				<columnheader name="Claim Time" />
		///				<columnheader name="Claim Status" />
		///				<columnheader name="Line of Business" />
		///				<columnheader name="Claim Type" />
		///				<columnheader name="Claim Number" />
		///				<columnheader name="Date Added" />
		///			</columns>
		///			<row>
		///				<column value="3/16/2004" />
		///				<column value="4:55:00 PM" />
		///				<column value="Closed" />
		///				<column value="General Claims" />
		///				<column value="PL-GC" />
		///				<column value="CSC-101" />
		///				<column value="3/16/2004 4:38 PM" />
		///			</row>
		///			<row>
		///				<column value="7/5/2004" />
		///				<column value="12:00:00 AM" />
		///				<column value="Open" />
		///				<column value="General Claims" />
		///				<column value="General Liability" />
		///				<column value="GC2004001342" />
		///				<column value="7/6/2004 4:54 PM" />
		///			</row>
		///		</report>
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool GetReportData(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;				
			XmlElement	objElement=null;						
			long        lReportId=0;
			try
			{
				//Get the value of report id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportId");
				if (objElement != null)													
					lReportId = Conversion.ConvertStrToLong(objElement.InnerText);

				if (lReportId == 0)
				{
                    p_objErrOut.Add("InvalidReportId", Globalization.GetString("InvalidReportId", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}
				
				objDCC = new Desktop(connectionString,base.ClientId);
				p_objOutXML = objDCC.GetReportData(lReportId);

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.GetReportData.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}


		/// <summary>
		/// This method is a wrapper to the LoadReportsForPortal method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document></Document>
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///		<reportsforportal>
		///			<report id="117" name="last10claimsentered" />
		///			<report id="118" name="last10eventsentered" />
		///		</reportsforportal>
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool LoadReportsForPortal(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;							
			try
			{
				objDCC = new Desktop(connectionString,base.ClientId);
				p_objOutXML = objDCC.LoadReportsForPortal(userID);

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.LoadReportsForPortal.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{				
				objDCC = null;	
			}
		}


		/// <summary>
		/// This method is a wrapper to the GetShowGraphValue method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<dcc>
		///				<ReportId>117</ReportId>
		///			</dcc>
		///		</Document>
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///		<Report id="117" showgraph="1" />
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool GetShowGraphValue(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop		objDCC = null;				
			XmlElement	objElement=null;						
			long        lReportId=0;
			try
			{
				//Get the value of report id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportId");
				if (objElement != null)													
					lReportId = Conversion.ConvertStrToLong(objElement.InnerText);

				if (lReportId == 0)
				{
                    p_objErrOut.Add("InvalidReportId", Globalization.GetString("InvalidReportId", base.ClientId),
						BusinessAdaptorErrorType.Error);					
					return false;
				}
				
				objDCC = new Desktop(connectionString,base.ClientId);
				p_objOutXML = objDCC.GetShowGraphValue(lReportId);

				return true;
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.GetShowGraphValue.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objElement=null;	
				objDCC = null;	
			}
		}


		/// <summary>
		/// This method is a wrapper to the GetChart method in Riskmaster.Application.DCC.Desktop.
		/// </summary>
		/// <param name="p_objXmlIn">Input XML
		///		<Document>
		///			<dcc>
		///				<ReportId>115</ReportId>
		///			</dcc>
		///		</Document>	
		/// </param>
		/// <param name="p_objOutXML">Output XML
		///		<dcc>
		///			<file>
		///				data for the file in Base64 encoding
		///			</file>
		///		</dcc>
		/// </param>		
		/// <param name="p_objErrOut">Collection of Errors/Messages</param>
		/// <returns>True/False for Success/Failure of the method</returns>
		public bool GetChart(XmlDocument p_objXmlIn, ref XmlDocument p_objOutXML, ref BusinessAdaptorErrors p_objErrOut)
		{
			Desktop			objDCC = null;				
			XmlElement		objElement=null;
			XmlElement		objElementReport=null;
			long			lReportId=0;
			bool			bReturnValue = false;

			string sReportIds = string.Empty;

			string [] arrsReportIds = null;
			XmlDocument XmlOut = null;

			try
			{
				//Get the value of report id
				objElement=(XmlElement)p_objXmlIn.SelectSingleNode("//ReportId");

				if (objElement==null)
				{
					bReturnValue = false;
                    p_objErrOut.Add("InvalidReportId", Globalization.GetString("InvalidReportId", base.ClientId),
						BusinessAdaptorErrorType.Error);  
					return bReturnValue;
				}
				sReportIds = objElement.InnerText;
				
				objDCC = new Desktop(connectionString,base.ClientId);	
			
				arrsReportIds = sReportIds.Split(",".ToCharArray());

				//create the output xml document containing the data in Base64 format
				XmlOut = new XmlDocument();
				p_objOutXML=new XmlDocument();
				objElement=p_objOutXML.CreateElement("DCC");
				objElement.SetAttribute("count", arrsReportIds.Length.ToString());
				p_objOutXML.AppendChild(objElement);

				foreach (string sReportId in arrsReportIds)
				{
					lReportId = Convert.ToInt64(sReportId.ToString());

					XmlOut = objDCC.GetChart(lReportId);
				
					objElementReport=p_objOutXML.CreateElement("DCCReport");
					objElementReport.InnerXml += XmlOut.OuterXml;
					p_objOutXML.FirstChild.AppendChild(objElementReport);
				}				
				bReturnValue = true;
			}
			catch(RMAppException p_objException)
			{
				bReturnValue = false;
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return bReturnValue;
			}
			catch(Exception p_objException)
			{
				bReturnValue = false;
                p_objErrOut.Add(p_objException, Globalization.GetString("DCCAdaptor.GetChart.Error", base.ClientId),
					BusinessAdaptorErrorType.Error);
				return bReturnValue;
			}
			finally
			{
				objElement=null;	
				objDCC = null;
				objElementReport = null;
			}
		return bReturnValue;
		}

		#endregion
	}
}

