using System;
using Riskmaster.BusinessAdaptor;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.VSSInterface;

namespace Riskmaster.BusinessAdaptor
{
	/// <summary>
	/// Summary description for VSSInterfaceAdaptor.
	/// </summary>
	public class VSSInterfaceAdaptor: BusinessAdaptorBase
	{

		/// Name		: VSSInterfaceAdaptor
		/// Author		: Anshul Verma
		/// Date Created: 01/03/2013
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		///	<summary>
		/// Constructor for the Class
		/// </summary>
		public VSSInterfaceAdaptor()
		{
		}

		/// Name		: Execute
		/// Author		: Anshul Verma
        /// Date Created: 01/03/2013
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		///	<summary>
		/// This method is called from Web Service
		/// </summary>
		public bool Execute(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			ImportProcessFactory objImportProcessFactory = null;
			IImportProcess objImportProcess = null;

			try
			{
				objImportProcessFactory = new ImportProcessFactory(userLogin.objRiskmasterDatabase.ConnectionString, userLogin.LoginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, base.ClientId);
				objImportProcess = objImportProcessFactory.CreateImportProcess();

				p_objXmlOut = objImportProcess.Execute(p_objXmlIn);
			}
			catch(RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException,BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("VSSInterfaceAdaptor.Execute.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objImportProcessFactory = null;
				objImportProcess = null;
			}
			return true;
		}

        //Start - averma62 MITS 32848
        /// Name		: VSSExport
        /// Author		: Anshul Verma
        /// Date Created: 05/31/2013
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        ///	<summary>
        /// This method is called from Web Service
        /// </summary>
        public bool VSSExport(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            int iClaimId = 0;
            int iClaimantEId = 0;
            int iRcRowId = 0;

            try
            {
                iClaimId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText);
                iClaimantEId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ClaimantEID").InnerText);
                iRcRowId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//ReserveID").InnerText);
                if (iRcRowId.Equals(0))
                {
                    string sql = "SELECT RC_ROW_ID FROM RESERVE_CURRENT WHERE CLAIM_ID = " + iClaimId + " AND CLAIMANT_EID = " + iClaimantEId;
                    using (DbReader objRdr = DbFactory.GetDbReader(this.connectionString, sql))
                    {
                        while (objRdr.Read())
                        {
                            iRcRowId = objRdr.GetInt32("RC_ROW_ID");
                            if (iRcRowId > 0)
                            {
                                VssExportAsynCall objVss = new VssExportAsynCall(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);
                                objVss.AsynchVssReserveExport(iClaimId, iClaimantEId, iRcRowId, 0, 0, "", "", "", "", "Reserve");
                            }
                        }
                    }
                }
                else
                {
                    VssExportAsynCall objVss = new VssExportAsynCall(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);
                    objVss.AsynchVssReserveExport(iClaimId, iClaimantEId, iRcRowId, 0, 0, "", "", "", "", "Reserve");
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("VSSInterfaceAdaptor.VSSExport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }
        //End - averma62 MITS 32848

        //Asharma326 MITS 32848 Starts
        /// <summary>
        /// Fucntion which is called by ReserveListing.aspx in case of carrier claim off.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool VSSExportForCCOff(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            int iClaimId = 0;
            int iClaimantEID = 0;
            int iReserveTypeCode = 0;
            int iRcRowId = 0;

            try
            {
                iClaimId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "ClaimID").InnerText);
                iClaimantEID = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "ClaimantEID").InnerText);
                iReserveTypeCode = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//" + "Reserve").InnerText);
                if (iReserveTypeCode.Equals(0))
                {
                    string sql = "SELECT RC_ROW_ID FROM RESERVE_CURRENT WHERE CLAIM_ID = " + iClaimId + " AND CLAIMANT_EID=" + iClaimantEID ;
                    using (DbReader objRdr = DbFactory.GetDbReader(this.connectionString, sql))
                    {
                        while (objRdr.Read())
                        {
                            iRcRowId = objRdr.GetInt32("RC_ROW_ID");
                            if (iRcRowId > 0)
                            {
                                VssExportAsynCall objVss = new VssExportAsynCall(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);
                                objVss.AsynchVssReserveExport(iClaimId, iClaimantEID, iRcRowId, 0, 0, "", "", "", "", "Reserve");
                            }
                        }
                    }
                }
                else
                {
                    string sql = "SELECT RC_ROW_ID FROM RESERVE_CURRENT WHERE CLAIM_ID = " + iClaimId + " AND CLAIMANT_EID=" + iClaimantEID+ " AND RESERVE_TYPE_CODE=" + iReserveTypeCode;
                    using (DbReader objRdr = DbFactory.GetDbReader(this.connectionString, sql))
                    {
                        while (objRdr.Read())
                        {
                            iRcRowId = objRdr.GetInt32("RC_ROW_ID");
                            if (iRcRowId > 0)
                            {
                                VssExportAsynCall objVss = new VssExportAsynCall(userLogin.objRiskmasterDatabase.DataSourceName, userLogin.LoginName, userLogin.Password, base.ClientId);
                                objVss.AsynchVssReserveExport(iClaimId, iClaimantEID, iRcRowId, 0, 0, "", "", "", "", "Reserve");
                            }
                        }
                    }
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("VSSInterfaceAdaptor.VSSExport.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }
        //Asharma326 MITS 32848 Ends
	}
}
