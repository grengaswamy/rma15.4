﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.BusinessAdaptor.Common;
using System.Configuration;
using Riskmaster.Application.VSSInterface;

namespace Riskmaster.BusinessAdaptor
{
    /// <summary>
    ///Author  :   Anshul Verma
    ///Dated   :   01/09/2012
    /// Class containing the adaptor methods to access Application layer methods for 
    /// RMX-VSS Single Signon.
    /// </summary>
    class SingleSignonAdaptor : BusinessAdaptorBase
    {
        //#region Constructor
        public SingleSignonAdaptor()
        { }
        //#endregion
       
        #region Public Methods

        public bool SingleSignonVSSXML(XmlDocument p_objXMLIn, ref XmlDocument p_objXMLOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            SingleSignon objSingleSignon = null;
            string sVSS_ws_username = string.Empty;
            string sVSS_ws_password = string.Empty;

            XmlDocument XmlVSSConfigDoc = new XmlDocument();
            try
            {
                objSingleSignon = new SingleSignon(userLogin.LoginName, userLogin.Password, base.securityConnectionString, base.ClientId);
                p_objXMLOut = objSingleSignon.SingleSignonXML(p_objXMLIn);

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("SingleSignonAdaptor.SingleSignonVSSXML.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objSingleSignon = null;
            }
            return true;
        }

        #endregion
    }
}
