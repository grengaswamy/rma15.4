﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using System.IO;
using Riskmaster.Security;
using Riskmaster.Application.MDINavigationTreeNodeProfile;
using Riskmaster.Models;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Xml.Linq;

using Riskmaster.Settings;


namespace Riskmaster.BusinessAdaptor.MDINavigationTreeNodeProfile
{
    public class MDINavigationTreeNodeProfileAdaptor : BusinessAdaptorBase
    {
        #region Constructor
        /// <summary>
        /// Constructor for the class.
        /// </summary>
        public MDINavigationTreeNodeProfileAdaptor()
        {
        }

        #endregion

        #region Public Methods
        public MDIClaimantFinancialRes GetClaimantDetails(MDIClaimantFinancialReq objRequest)
        {

            
            int iClaimantEId = 0;
            int iClaimId = 0;
            bool isGCLOB = false;
            MDINavigationTreeNodeProfileManager objNavigationManager = null;
            MDIClaimantFinancialRes objReturn = null;
            try
            {
                objNavigationManager= new MDINavigationTreeNodeProfileManager(connectionString, userLogin, base.ClientId);
              objReturn  =new MDIClaimantFinancialRes();

              objNavigationManager.GetClaimantDetails(objRequest.ClaimantRowId, ref iClaimantEId, ref iClaimId, ref isGCLOB);
              objReturn.ClaimantEID = iClaimantEId;
              objReturn.ClaimID=iClaimId;


              objReturn.IsGCLOB = isGCLOB;

                     return objReturn;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }            
           
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputRequest"></param>
        /// <param name="objReturn"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public MDINavigationTreeNodeProfileResponse QueryProfile(MDINavigationTreeNodeProfileRequest inputRequest)
        {
            int nodeId = inputRequest.NodeId;
            int viewId = inputRequest.ViewId;

            string objectName = inputRequest.ObjectName;
            string parentObjectName = string.Empty;
            int parentId = 0;
            //Mits id:32160 -Performance issue - Start 
            LocalCache objCache = new LocalCache(connectionString,base.ClientId);
            SysSettings objSys = new SysSettings(connectionString,base.ClientId);


            try
            {
                MDINavigationTreeNodeProfileResponse objReturn = new MDINavigationTreeNodeProfileResponse();
                MDINavigationTreeNodeProfileManager objNavigationManager = new MDINavigationTreeNodeProfileManager(connectionString, userLogin,base.ClientId);

                //objReturn.NodeDetail = GetNodeDetail(nodeId, objectName, viewId, DSNID, objNavigationManager);
                // akaushik5 Changed for MITS 38011 Starts
                //objReturn.NodeDetail = GetNodeDetail(nodeId, objectName, viewId, DSNID, objNavigationManager, objSys.UseClaimantName, objCache);
                if(inputRequest.ObjectName=="reservelistingclaimant")
                {
                    objectName = "reservelisting";
                }
                objReturn.NodeDetail = GetNodeDetail(nodeId, objectName, viewId, DSNID, objNavigationManager, objSys.UseClaimantName, objCache, objSys);
                // akaushik5 Changed for MITS 38011 Ends
                if (objReturn.NodeDetail != null)
                {
                    objectName = objReturn.NodeDetail[0][1];
                }

                if (objectName != "event" && ((objectName != "reservelisting") || (inputRequest.ObjectName =="reservelistingclaimant" )) && objectName.ToLower() != "reserveworksheet" && nodeId != 0)
                {
                    objReturn.NodeParent = GetNodeParent(nodeId, objectName, viewId, DSNID, objNavigationManager,inputRequest.ObjectName);
                    if (objReturn.NodeParent != null)
                    {
                        parentObjectName = objReturn.NodeParent[0][1];
                        parentId = Convert.ToInt32(objReturn.NodeParent[0][0]);
                    }

                    objReturn.NodeSiblings = GetNodeSiblings(nodeId, objectName, parentObjectName, parentId.ToString(), viewId, DSNID, objNavigationManager, objSys.UseClaimantName);
                }
                objReturn.NodeChildern = GetNodeChildern(nodeId, objectName, parentId, parentObjectName, viewId, DSNID, objNavigationManager, objSys, objCache);
                
                
                
                return objReturn;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }            
            finally
            {
                if(objCache!=null)
                    objCache.Dispose();

                if (objSys != null)
                {
                    objSys = null;
                }
            }
            //Mits id:32160 -Performance issue - End 
            
        }

        public MDINavigationTreeNodeProfileResponse QueryProfile(MDINavigationTreeNodeProfileRequestByParent inputRequest)
        {
            int nodeId = inputRequest.NodeId;
            int viewId = inputRequest.ViewId;

            string objectName = inputRequest.ObjectName;
            string parentObjectName = inputRequest.ParentObjectName;
            int parentId = inputRequest.ParentId;
            //Mits id:32160 -Performance issue - Start 
            LocalCache objCache = new LocalCache(connectionString,ClientId);
            SysSettings objSys = new SysSettings(connectionString, base.ClientId); //Ash - cloud

            try
            {
                MDINavigationTreeNodeProfileResponse objReturn = new MDINavigationTreeNodeProfileResponse();
                //MDINavigationTreeNodeProfileManager objNavigationManager = new MDINavigationTreeNodeProfileManager(connectionString,userLogin);
                MDINavigationTreeNodeProfileManager objNavigationManager = new MDINavigationTreeNodeProfileManager(connectionString, userLogin, base.ClientId);


                // akaushik5 Changed for MITS 38011 Starts
                //objReturn.NodeDetail = GetNodeDetail(nodeId, objectName, viewId, DSNID, objNavigationManager, objSys.UseClaimantName, objCache);
                objReturn.NodeDetail = GetNodeDetail(nodeId, objectName, viewId, DSNID, objNavigationManager, objSys.UseClaimantName, objCache, objSys);
                // akaushik5 Changed for MITS 38011 Ends
                if (objReturn.NodeDetail != null)
                {
                    objectName = objReturn.NodeDetail[0][1];
                }

                /*if (objectName != "event" && objectName != "reservelisting" && objectName.ToLower() != "reserveworksheet" && nodeId != 0)
                {
                    objReturn.NodeParent = GetNodeParent(nodeId, objectName, viewId, DSNID, objNavigationManager);
                    if (objReturn.NodeParent != null)
                    {
                        parentObjectName = objReturn.NodeParent[0][1];
                        parentId = objReturn.NodeParent[0][0];
                    }

                    objReturn.NodeSiblings = GetNodeSiblings(nodeId, objectName, parentObjectName, parentId, viewId, DSNID, objNavigationManager);
                }*/
                if (nodeId == 0 || objectName == "demandoffer")
                {
                    objReturn.NodeSiblings = GetNodeSiblings(0, objectName, parentObjectName, parentId.ToString(), viewId, DSNID, objNavigationManager, objSys.UseClaimantName);
                }

                objReturn.NodeChildern = GetNodeChildern(nodeId, objectName, parentId, parentObjectName, viewId, DSNID, objNavigationManager,objSys, objCache);
                
                return objReturn;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (objCache != null)
                    objCache.Dispose();

                if (objSys != null)
                {
                    objSys = null;
                }
            }

            //Mits id:32160 -Performance issue - End 
        }

        public InitMDIResponse InitMDI(InitMDIRequest request)
        {
            try
            {
                InitMDIResponse response = new InitMDIResponse();
                MDINavigationTreeNodeProfileManager objNavigationManager = new MDINavigationTreeNodeProfileManager(connectionString, userLogin, base.ClientId);
                Riskmaster.Application.SecurityManagement.UserPermissions objUserPermissions = new Riskmaster.Application.SecurityManagement.UserPermissions(base.ClientId);
                string landingArg = string.Empty;
                List<string> landingItems = null;
                bool noLandingItemAdded = true;
                XElement el = null;
                XElement MDIMenu = XElement.Parse(request.SerializedMDIMenu);

                objUserPermissions.MDISecureMenu(ref MDIMenu, userLogin, request.ViewId, DSNID);

                landingItems = objNavigationManager.GetLandingItems(request.ViewId, DSNID, userID);

                if (landingItems != null)
                {
                    for (int i = 0; i < landingItems.Count; i++)
                    {
                        IEnumerable<XElement> query = from c in MDIMenu.Descendants()
                                                      where c.Attribute("sysName") != null && c.Attribute("sysName").Value.ToLower() == landingItems[i].ToLower()
                                                      select c;
                        el = query.FirstOrDefault();
                        if (el != null)
                        {
                            if (!noLandingItemAdded && el.Attribute("sysName").Value.ToLower() == "whatsnew")
                            {
                                continue;
                            }
                            if (i + 1 == landingItems.Count)
                            {
                                el.SetAttributeValue("landingItem", "PRIMARY");
                                noLandingItemAdded = false;
                            }
                            else
                            {
                                el.SetAttributeValue("landingItem", "YES");
                                noLandingItemAdded = false;
                            }
                        }
                    }
                }
                //response.SerializedMDIMenu = MDIMenu.ToString();
                response.SerializedMDIMenu = MDIMenu;

                return response;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }


        #endregion
        //nkaranam2 - 34408
        public GetSysSettingsResponse GetSysSettings(GetSysSettingsRequest settings)
        {
            try
            {
                List<String> oSysSettingsReqList = settings.oSysSettingsReqList;
                GetSysSettingsResponse oSysSettingsResponse = new GetSysSettingsResponse();
                SysSettings oSysSettings = new SysSettings(userLogin.objRiskmasterDatabase.ConnectionString, base.ClientId); //Ash - cloud
                oSysSettingsResponse.oSysSettingsList = new Dictionary<String, Object>();
                foreach (String oSysSettingKey in oSysSettingsReqList)
                {
                    //List<Object> responseList = new List<Object>();
                    //responseList.Add(oSysSettings.MultiCovgPerClm);
                    if (String.Compare(oSysSettingKey, "MultiCovgPerClm") == 0)
                    {
                        int oSysSettingValue = oSysSettings.MultiCovgPerClm;
                        oSysSettingsResponse.oSysSettingsList.Add(oSysSettingKey, oSysSettingValue);
                    }
                    //Start rsushilaggar JIRA 11730
                    if (String.Compare(oSysSettingKey, "UseEntityRole") == 0)
                    {
                        int oSysSettingValue =Convert.ToInt32( oSysSettings.UseEntityRole);
                        oSysSettingsResponse.oSysSettingsList.Add(oSysSettingKey, oSysSettingValue);
                    }
                }
                return oSysSettingsResponse;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
        //nkaranam2 - 34408

        #region PrivateMethods
        // akaushik5 Changed for MITS 38011 Starts
        //private List<string[]> GetNodeDetail(int nodeId, string objectName, int viewId, int dsnId, MDINavigationTreeNodeProfileManager objNavigationManager, int objSysUseClaimantName, LocalCache objCache)
        private List<string[]> GetNodeDetail(int nodeId, string objectName, int viewId, int dsnId, MDINavigationTreeNodeProfileManager objNavigationManager, int objSysUseClaimantName, LocalCache objCache, SysSettings objSysSettings)
        // akaushik5 Changed for MITS 38011 Ends
        {
            List<string[]> nodeDetail = null;

            try
            {
                // akaushik5 Changed for MITS 38011 Starts
                //nodeDetail = objNavigationManager.GetNode(nodeId, objectName, viewId, dsnId, objSysUseClaimantName, objCache);
                nodeDetail = objNavigationManager.GetNode(nodeId, objectName, viewId, dsnId, objSysUseClaimantName, objCache, objSysSettings);
                // akaushik5 Changed for MITS 38011 Ends
                return nodeDetail;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private List<string[]> GetNodeParent(int nodeId, string objectName, int viewId, int dsnId, MDINavigationTreeNodeProfileManager objNavigationManager,string sInputObjectName="")
        {
            List<string[]> arrListParent = null;
            try
            {
                arrListParent = objNavigationManager.GetNodeParent(nodeId, objectName, viewId, dsnId, sInputObjectName);

                return arrListParent;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private List<string[]> GetNodeChildern(int nodeId, string objectName, int parentId, string parentObjectName, int viewId, int dsnId, MDINavigationTreeNodeProfileManager objNavigationManager, SysSettings objSys, LocalCache objCache)
        {
            List<string[]> arrListChildern = null;

            try
            {

                
                arrListChildern = objNavigationManager.GetNodeChildren(nodeId, objectName, parentId, parentObjectName, viewId, dsnId, objSys, objCache);

                return arrListChildern;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private List<string[]> GetNodeSiblings(int nodeId, string objectName, string parentObjectName, string parentId, int viewId, int dsnId, MDINavigationTreeNodeProfileManager objNavigationManager, int objSysUseClaimantName)
        {
            List<string[]> arrListSiblings = null;
            try
            {
                arrListSiblings = objNavigationManager.GetNodeSiblings(nodeId, objectName, parentObjectName, parentId, viewId, dsnId, objSysUseClaimantName);

                return arrListSiblings;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        #endregion
    }
}
