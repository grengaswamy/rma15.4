﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.239
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Riskmaster.BusinessAdaptor.AutoFroiAcord {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class FroiAcord_en_us {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal FroiAcord_en_us() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Riskmaster.BusinessAdaptor.AutoFroiAcord.FroiAcord-en-us", typeof(FroiAcord_en_us).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CSC Financial Services Group.
        /// </summary>
        internal static string AmyuniTechLicenseCompany {
            get {
                return ResourceManager.GetString("AmyuniTechLicenseCompany", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 07EFCDAB01000100C9D1AB2346F36D68B637892D59E2B2D416C18F0397811F5604016685DBC54D7928B23578A87E09BFEB3D9FC4FE9F.
        /// </summary>
        internal static string AmyuniTechLicenseKey {
            get {
                return ResourceManager.GetString("AmyuniTechLicenseKey", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An error has occurred while generating First report of injury PDF File..
        /// </summary>
        internal static string AutoFroiAcordAdaptor_generateFROIACORD_Error {
            get {
                return ResourceManager.GetString("AutoFroiAcordAdaptor.generateFROIACORD.Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fail to load the FROI Options for this Claim..
        /// </summary>
        internal static string AutoFroiAcordAdaptor_generateFROIACORD_LoadFROIOptionsFailed {
            get {
                return ResourceManager.GetString("AutoFroiAcordAdaptor.generateFROIACORD.LoadFROIOptionsFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Body Part or Parts are not linked to the Industry Standards..
        /// </summary>
        internal static string AutoFroiAcordAdaptor_generateFROIACORD_NCCIBodyParts {
            get {
                return ResourceManager.GetString("AutoFroiAcordAdaptor.generateFROIACORD.NCCIBodyParts", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Body Part or Parts,Cause Code or codes,Injury/Illness are not linked to the Industry Standards.The NCCI Class Code(Short Code) contains a non-numeric character..
        /// </summary>
        internal static string AutoFroiAcordAdaptor_generateFROIACORD_NCCIBodyParts_NCCICause_NCCIInjury_Illness_NonNumericClassCode {
            get {
                return ResourceManager.GetString("AutoFroiAcordAdaptor.generateFROIACORD.NCCIBodyParts_NCCICause_NCCIInjury/Illness" +
                        "_NonNumericClassCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cause Code or Codes are not linked to the Industry Standards..
        /// </summary>
        internal static string AutoFroiAcordAdaptor_generateFROIACORD_NCCICause {
            get {
                return ResourceManager.GetString("AutoFroiAcordAdaptor.generateFROIACORD.NCCICause", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Claim is missing NCCI Class Code..
        /// </summary>
        internal static string AutoFroiAcordAdaptor_generateFROIACORD_NCCIClass {
            get {
                return ResourceManager.GetString("AutoFroiAcordAdaptor.generateFROIACORD.NCCIClass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Injury/Illness are not linked to the Industry Standards..
        /// </summary>
        internal static string AutoFroiAcordAdaptor_generateFROIACORD_NCCIInjury_Illness {
            get {
                return ResourceManager.GetString("AutoFroiAcordAdaptor.generateFROIACORD.NCCIInjury/Illness", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Claimant is missing or the data is not complete..
        /// </summary>
        internal static string AutoFroiAcordAdaptor_generateFROIACORD_NoClaimant {
            get {
                return ResourceManager.GetString("AutoFroiAcordAdaptor.generateFROIACORD.NoClaimant", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Claimant is missing an Assigned Department, this breaks the link to the employer..
        /// </summary>
        internal static string AutoFroiAcordAdaptor_generateFROIACORD_NoDepartmentAssigned {
            get {
                return ResourceManager.GetString("AutoFroiAcordAdaptor.generateFROIACORD.NoDepartmentAssigned", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The NCCI Class Code(Short Code) contains a non-numeric character..
        /// </summary>
        internal static string AutoFroiAcordAdaptor_generateFROIACORD_NonNumericClassCode {
            get {
                return ResourceManager.GetString("AutoFroiAcordAdaptor.generateFROIACORD.NonNumericClassCode", resourceCulture);
            }
        }
    }
}
