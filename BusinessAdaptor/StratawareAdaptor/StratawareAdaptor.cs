﻿using System;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Settings;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Db;

namespace Riskmaster.BusinessAdaptor.StratawareAdaptor
{
    public class StratawareAdaptor : BusinessAdaptorBase
    {


       public bool GetInfo(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
       {
           UserLogin objUserLogin = null;
           SysSettings objSettings = null;
           XmlNode objRoot = null;
           XmlNode objChild = null;
           bool bReturn = false;
           try
           {
               objSettings = new SysSettings(m_connectionString, base.ClientId); //Ash - cloud
             
               if (Conversion.CastToType<bool>(objSettings.EnableSingleuser.ToString(),out bReturn))
               {
                    objUserLogin = new UserLogin(objSettings.StratawareUserID, base.ClientId);

                    p_objXmlIn.SelectSingleNode("//UserId").InnerText = objSettings.StratawareUserID.ToString();

                    p_objXmlIn.SelectSingleNode("//UserName").InnerText = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_securityConnectionString, string.Format("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.USER_ID={0}", objSettings.StratawareUserID)));
                    p_objXmlIn.SelectSingleNode("//UserEmail").InnerText = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_securityConnectionString, string.Format("SELECT EMAIL_ADDR FROM USER_TABLE WHERE USER_TABLE.USER_ID={0}", objSettings.StratawareUserID)));

                }
                else
                {
                    p_objXmlIn.SelectSingleNode("//UserId").InnerText = m_userLogin.UserId.ToString();
                    p_objXmlIn.SelectSingleNode("//UserName").InnerText = m_userLogin.LoginName;
                    p_objXmlIn.SelectSingleNode("//UserEmail").InnerText = m_userLogin.objUser.Email;
                }
               //mbahl3 mits  34646
               if (p_objXmlIn.SelectSingleNode("//UserEmail").InnerText == "")
               {

                   p_objErrOut.Add(Globalization.GetString("ValidationError", base.ClientId), Globalization.GetString("StrataWareEmail.RecordNotFound", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                   return false;
                }
               
               //mbahl3 mits 34646
                p_objXmlIn.SelectSingleNode("//ServiceURL").InnerText = objSettings.StratwareServiceUrl;
                p_objXmlIn.SelectSingleNode("//ConsumerURL").InnerText = objSettings.StratawareConsumerUrl;

                p_objXmlOut = new XmlDocument();
                p_objXmlOut.LoadXml(p_objXmlIn.OuterXml);
                return true;
               

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, Globalization.GetString("StratawareApaptor.GetInfo.Error", base.ClientId), BusinessAdaptorErrorType.Error);//rkaur27
                return false;
            }
            finally
            {
                objSettings = null;
                objUserLogin = null;
            }
        }


    }
}
