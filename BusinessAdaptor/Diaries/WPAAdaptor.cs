using Riskmaster.Application.Diaries;
using Riskmaster.Application.RMUtilities;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Models;
using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Xml;


namespace Riskmaster.BusinessAdaptor
{
    ///************************************************************** 
    ///* $File		: WPAAdaptor.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 21-Feb-2005
    ///* $Author	: Sumeet Rathod
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    public class WPAAdaptor : BusinessAdaptorBase
    {

        // Mihika 12/19/2005
        // Constants added to identify the security permisions
        private const int RMB_DIARY = 19000;
        //agupta298 : Since Create/Attach/DiaryDetails screens are in FDM now, need standard data entry permission breakout for it in SMS.
        private const int RMO_DIARY_CREATE = 3;//30;
        private const int RMO_DIARY_EDIT = 2;//32;
        //agupta298 : Since Create/Attach/DiaryDetails screens are in FDM now, need standard data entry permission breakout for it in SMS.
        private const int RMO_DIARY_COMPL = 31;
        private const int RMO_DIARY_VOID = 33;
        private const int RMO_DIARY_ROUTE = 34;
        private const int RMO_DIARY_ROLL = 35;
        private const int RMO_DIARY_GO = 36;
        private const int RMO_DIARY_PEEK = 37;
        private const int RMO_DIARY_HISTORY = 500;
        private const int RMO_ALL_DIARY_CLAIM_DELETE = 600;
        //Mits 19096
        private const int RMO_DIARY_CALENDER = 39;
        //igupta3 jira 439
        private const int RMO_DIARY_NOTROLL = 41;
        private const int RMO_DIARY_NOTROUTE = 40;
        /// <summary>
        /// Default Constructor
        /// </summary>
        public WPAAdaptor()
        { }

        /// <param name="p_objXmlIn">XML containing the input parameters needed to delete a given diary
        ///		The structure of input XML document would be:
        ///		<Document>
        ///			<DeleteDiary diaryid="121">
        ///			</DeleteDiary>
        ///		</Document>
        /// </param>

        //As such this function does not return any output XML. But we may plan to return some success/failure message

        /// <summary>
        /// Deletes a diary for the given diary id.
        /// </summary>
        /// <param name="p_objXmlIn">XML document containing diary related information</param>
        /// <param name="p_objXmlOut">XML document containing diary information</param>
        /// <param name="p_objErr">Error object</param>
        /// <returns>True if sucessful else returns false</returns>
        public bool DeleteDiary(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            XmlElement objXmlUser = null;
            WPA objWPA = null;
            int iDiaryId = 0;
            try
            {
                objXmlUser = (XmlElement)p_objXmlIn.SelectSingleNode("//DeleteDiary");
                iDiaryId = Conversion.ConvertStrToInteger(objXmlUser.GetAttribute("diaryid"));
                objWPA = new WPA(connectionString, base.ClientId);//rkaur27
                objWPA.DeleteDiary(iDiaryId);
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.DeleteDiary.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objWPA = null;
                objXmlUser = null;
            }
            return (true);
        }

        ///<param name="p_objXmlIn">Input XML not required while fetching available peek list information

        /// <param name="p_objXmlOut">Output XML format is as follows:
        ///		<diaries>
        ///			<Users>
        ///				<User username="rathod,sumeet" loginname="srathod2" id="2" />
        ///				<subject />
        ///				<date>12/12/2004</date>
        ///				<User username="bali,jas" loginname="jasbali" id="3" />
        ///				<subject />
        ///				<date>12/12/2004</date>
        ///			</Users>
        ///		</diaries>

		/// <summary>
		/// Gets the list of available users.
		/// </summary>
		/// <param name="p_objXmlIn">XML input parameter</param>
		/// <param name="p_objXmlOut">XML document containing user user list</param>
		/// <param name="p_objErr">Error object</param>
		/// <returns>True if successful else returns false</returns>
		public bool GetAvailablePeekList (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
		{
            string peekList = string.Empty;
            return getReportees(ref peekList, p_objXmlIn, ref p_objXmlOut, ref p_objErr);
        }
        public bool GetAvailablePeekList(ref string peekList, ref BusinessAdaptorErrors p_objErr)
        {
            XmlDocument p_objXmlIn = null;
            XmlDocument p_objXmlOut = null;
            return getReportees(ref peekList,p_objXmlIn, ref p_objXmlOut,ref p_objErr);
        }
        private bool getReportees(ref string peekList,XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
			if (!userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_PEEK))
				throw new PermissionViolationException(RMPermissions.RMO_DIARY_PEEK,RMB_DIARY);

			WPA objWPA = null;
			//XmlElement objXmlUser = null;
			try 
			{
				//objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode ("//PeekDiary");
                objWPA = new WPA(connectionString, base.ClientId);//rkaur27
				objWPA.SecureDSN = securityConnectionString;

				//Fetching UserId should be handled by the adaptor and not by UI..changed by raman bhatia
				//if (objXmlUser != null)
				//	objWPA.UserId = Conversion.ConvertStrToLong (objXmlUser.GetAttribute ("UserId"));
				objWPA.UserId =  userID;
				objWPA.DSNId = DSNID;
                if (p_objXmlIn==null) {
                    peekList = objWPA.GetReportees();
                }
                else {
				p_objXmlOut = objWPA.GetAvailablePeekList ();
                }
			}
			catch(RMAppException p_objException)
			{
				p_objErr.Add (p_objException, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.GetAvailablePeekList.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally 
			{
				objWPA = null;
				//objXmlUser = null;
			}
			return (true);
		}

		/// <param name="p_objXmlIn">XML containing the input parameters needed to delete a given diary
		///		The structure of input XML document would be:
		///		<Document>
		///			<GetDiaryHistoryDom sortorder="121" recordcount="10" pagecount="10" pagenumber="10" attachtable="claim" attachrecordid="">
		///			</GetDiaryHistoryDom>
		///		</Document>
		/// </param>

		/// <param name="p_objXmlOut">Output XML format is as follows:
		///		<diaries user="srathod2" opendiaries="0" attach_table="" attach_recordid="" orderby="5" nextpage="2" 
		///				lastpage="4" pagenumber="1" pagecount="4">
		///			<diary entry_id="1170" img="img/user.gif" alt="My Diary" complete_date="3/18/2005" 
		///					assigning_user="srathod2" is_attached="0" attach_table="" attach_recordid="0" attsecrecid="0" 
		///					attachprompt="" pixmlformname="" priority="1" tasksubject="dfsdf" regarding="">
		///					-- Routed To srathod2 on 3/16/2005
		///			</diary >
		///		<diaries>
		
		/// <summary>
		/// Gets the diary history
		/// </summary>
		/// <param name="p_objXmlIn">XML document containing input parameters</param>
		/// <param name="p_objXmlOut">XML document containing diary history</param>
		/// <param name="p_objErr">Error object</param>
		/// <returns>true if successful else false</returns>
		public bool GetDiaryHistoryDom (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
		{
			WPA objWPA = null;
			XmlElement objXmlUser = null;
			int iSortOrder = 0;
			long lRecordCount = 0;
			long lPageCount = 0;
			long lPageNumber = 0;
			long lPageSize = 0;
			string sAttachTable = "";
			string sAttachRecordId = "";
			string sUserName = "";

			try 
			{
				objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode ("//GetDiaryHistoryDom");
				iSortOrder = Conversion.ConvertStrToInteger (objXmlUser.GetAttribute ("sortorder"));
				lRecordCount = Conversion.ConvertStrToLong (objXmlUser.GetAttribute ("recordcount"));
				lPageCount = Conversion.ConvertStrToLong (objXmlUser.GetAttribute ("pagecount"));
				lPageNumber = Conversion.ConvertStrToLong (objXmlUser.GetAttribute ("pagenumber"));
				lPageSize = Conversion.ConvertStrToLong (objXmlUser.GetAttribute ("pagesize"));
				sAttachTable = objXmlUser.GetAttribute ("attachtable");
				sAttachRecordId = objXmlUser.GetAttribute ("attachrecordid");
				//username can be different in case of peeking.. fetching it from UI..modified by raman bhatia
				sUserName = objXmlUser.GetAttribute("username");
                if (string.IsNullOrEmpty(sUserName))
                    sUserName= loginName;
                objWPA = new WPA(connectionString, base.ClientId);//rkaur27
				objWPA.PageSize = lPageSize;
				if (!userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_HISTORY))
				{
					throw new PermissionViolationException(RMPermissions.RMO_DIARY_HISTORY,RMB_DIARY);
				}
                //pkandhari Jira 6412 starts 
                objWPA.UserId = userLogin.UserId;
                //pkandhari Jira 6412 Ends
                //Aman ML Change
                objWPA.LanguageCode = base.userLogin.objUser.NlsCode;
				//p_objXmlOut = objWPA.GetDiaryHistoryDom (iSortOrder,loginName, lRecordCount, lPageCount, lPageNumber, sAttachTable, sAttachRecordId);
				p_objXmlOut = objWPA.GetDiaryHistoryDom (iSortOrder, sUserName, lRecordCount, lPageCount, lPageNumber, sAttachTable, sAttachRecordId);

				// Check for security permisions for 'Go' - Mihika
				if (!userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_GO))
				{
					XmlElement objTempElm = p_objXmlOut.CreateElement("GoNotAllowed");
					((XmlElement)p_objXmlOut.SelectSingleNode("//diaries")).AppendChild(objTempElm);
					objTempElm = null;
				}
			}
			catch(RMAppException p_objException)
			{
				p_objErr.Add (p_objException, BusinessAdaptorErrorType.Error);			
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.GetDiaryHistoryDom.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally 
			{
				objXmlUser = null;
				objWPA = null;
			}
			return (true);
		}

		/// <param name="p_objXmlIn">XML containing the input parameters needed to delete a given diary
		///		The structure of input XML document would be:
		///		<Document>
		///			<GetDiaryDom sortorder="121" duedate="12/12/2004"  recordcount="10" pagecount="10" pagenumber="10" open="true" attachtable="claim" attachrecordid="">
		///			</GetDiaryDom>
		///		</Document>
		/// </param>

		/// <param name="p_objXmlOut">Output XML format is as follows:
		///		<diaries user="srathod2" opendiaries="0" datedue="3/3/2005" attach_table="" attach_recordid="" orderby="5" nextpage="2" lastpage="4" pagenumber="1" pagecount="4">
		///			<diary entry_id="1231" img="img/user_overdue.gif" alt="My Diary" 
		///				complete_date="9/29/2004" assigning_user="srathod2" is_attached="1" 
		///					attach_table="CLAIM" attach_recordid="14" attsecrecid="241" 
		///					claimstatus="Reopened, Closed" attachprompt="Claim: GC1998000677u65r014" 
		///					pixmlformname="" priority="1" tasksubject="QD for CLAIM 14" 
		///					regarding="General Claims [ GC1998000677u65r014 * City of Oak Hills COMPANY * Che">
		///			Quick Diary</diary>
		///		<diaries>
		
		/// <summary>
		/// Gets the latest diaries DOM
		/// </summary>
		/// <param name="p_objXmlIn">XML document containing input parameters</param>
		/// <param name="p_objXmlOut">XML document containing latest diary information</param>
		/// <param name="p_objErr">Error object</param>
		/// <returns>true if successful else returns false</returns>
		public bool GetDiaryDom (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
		{
			WPA objWPA = null;
			XmlElement objXmlUser = null;
			XmlElement objXmlCaller = null;
			int iSortOrder = 0;
			long lRecordCount = 0;
			long lPageCount = 0;
			long lPageNumber = 0;
			string sAttachTable = "";
			string sAttachRecordId = "";
            //Indu - Mits 33843
            string sParentRecord = "";
			string sDueDate = "";
			string sFunction= "";
            string sActiveDiaryChecked = ""; //Umesh
			string sAllClaimDetails = string.Empty;
			bool bOpen = false;
            bool bReqActDiary = false;//Umesh
			string sAssignedUser = "";
			//MITS 11998, Abhishek
            string sShowAllDiariesForActiveRec = "no";
            string sUserSortOrder = ""; //Mits 23477 Neha
            string sTaskType = "";
            string sTakeName = "";
            string sShowRegarding = string.Empty;//Amitosh            
            string sFilterExpression = string.Empty;
			string sCaller = string.Empty;
            string sClaimActiveDiaryChecked = string.Empty;//aanandpraka2:MITS 27074
            string sUIDiarySource = "";//aanandpraka2:MITS 27074
			try 
			{
				objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode ("//GetDiaryDom");
				iSortOrder = Conversion.ConvertStrToInteger (objXmlUser.GetAttribute ("sortorder"));
				lRecordCount = Conversion.ConvertStrToLong (objXmlUser.GetAttribute ("recordcount"));
				lPageCount = Conversion.ConvertStrToLong (objXmlUser.GetAttribute ("pagecount"));
				lPageNumber = Conversion.ConvertStrToLong (objXmlUser.GetAttribute ("pagenumber"));
                bReqActDiary = Conversion.ConvertStrToBool(objXmlUser.GetAttribute("reqactdiary"));//REM Umesh
                sActiveDiaryChecked = objXmlUser.GetAttribute("activediarychecked");// Rem Umesh                
                sClaimActiveDiaryChecked = objXmlUser.GetAttribute("claimactivediarychecked"); //aanandpraka2:MITS 27074
                sUIDiarySource = objXmlUser.GetAttribute("diarysource");//aanandpraka2:MITS 27074
                string showNotes = objXmlUser.GetAttribute("shownotes");  //tkr
				sAttachTable = objXmlUser.GetAttribute ("attachtable");
				sAttachRecordId = objXmlUser.GetAttribute ("attachrecordid");
				sFunction = objXmlUser.GetAttribute ("diaryfunction"); 
				sDueDate = objXmlUser.GetAttribute ("duedate");
				bOpen = Conversion.ConvertStrToBool (objXmlUser.GetAttribute ("open"));
				sAssignedUser = objXmlUser.GetAttribute ("user");
                //Ankit Start : Worked on MITS - 29939
                //if (string.IsNullOrEmpty(sClaimActiveDiaryChecked))vkumar258 RMA-10872
                    sAllClaimDetails = objXmlUser.GetAttribute("showallclaimdiaries");
                //Ankit End
                sUserSortOrder = objXmlUser.GetAttribute("usersortorder");//Mits 23477 Neha saving diary sort order to user pref
                sTaskType = objXmlUser.GetAttribute("tasknamefilter");//05/16/2011 Mits 23538 Neha Filter criteria
                sTakeName = objXmlUser.GetAttribute("tasknametext");//05/16/2011 Mits 23538 Neha Filter criteria
                //MITS 11998 Abhishek start
                if (sAttachRecordId.Trim() != "")
                {
                    sShowAllDiariesForActiveRec = objXmlUser.GetAttribute("showalldiariesforactiverecord");
                }
                //MITS 11998 Abhishek end
				if (sAssignedUser == "")
					sAssignedUser = loginName;
				objWPA = new WPA (connectionString,base.userLogin.objRiskmasterDatabase.DataSourceName,base.userLogin.Password,base.m_loginName, base.ClientId);
				objWPA.PageSize = Conversion.ConvertStrToLong (objXmlUser.GetAttribute ("pagesize"));
                objWPA.UserId = userID;
                objWPA.SecureDSN = m_securityConnectionString; //pmittal5 Confidential Record
                objWPA.LanguageCode = base.userLogin.objUser.NlsCode;
                objWPA.DSNId = base.userLogin.DatabaseId;
                objWPA.SetOtherDiariesEditPermission = ((base.userLogin.IsAllowedEx(19042) ? "0" : "1"));//19042 is permission for Allow Edit due diaries for other users. 
                //objWPA.iSortOrder = iSortOrder; //Mits 23477 Neha saving diary sort order to user pref
                //Added by Amitosh for Terelik Grid Ui
                sShowRegarding = objXmlUser.GetAttribute("showregarding");
                sFilterExpression = objXmlUser.GetAttribute("filterexpression"); 
                //End Amitosh
                //Ankit:Start changes for MITS 27074
                //p_objXmlOut = objWPA.GetDiaryDom(iSortOrder, sAssignedUser, sDueDate, lRecordCount, lPageCount, lPageNumber, bOpen, bReqActDiary, sAttachTable, sAttachRecordId, sFunction, sActiveDiaryChecked, showNotes, sAllClaimDetails, sShowAllDiariesForActiveRec, sUserSortOrder, sTaskType, sTakeName);//Neha Mits 23568
                //Aman ML Change 
                objWPA.LanguageCode = base.userLogin.objUser.NlsCode;
                //p_objXmlOut = objWPA.GetDiaryDom(iSortOrder, sAssignedUser, sDueDate, lRecordCount, lPageCount, lPageNumber, bOpen, bReqActDiary, sAttachTable, sAttachRecordId, sFunction, sActiveDiaryChecked, showNotes, sAllClaimDetails, sShowAllDiariesForActiveRec, sUserSortOrder, sTaskType, sTakeName, sShowRegarding, sFilterExpression);//Added by Amitosh for Terelik Grid UI
                p_objXmlOut =   objWPA.GetDiaryDom(iSortOrder, sAssignedUser, sDueDate, lRecordCount, lPageCount, lPageNumber, bOpen, bReqActDiary, sAttachTable, sAttachRecordId,sParentRecord, sFunction, sActiveDiaryChecked, showNotes, sAllClaimDetails, sShowAllDiariesForActiveRec, sUserSortOrder, sTaskType, sTakeName, sShowRegarding, sFilterExpression,sClaimActiveDiaryChecked,sUIDiarySource);
                //Ankit-End changes
                // Check for security permisions for 'Go' - Mihika
				if (!userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_GO))
				{
					XmlElement objTempElm = p_objXmlOut.CreateElement("GoNotAllowed");
					((XmlElement)p_objXmlOut.SelectSingleNode("//diaries")).AppendChild(objTempElm);
					objTempElm = null;
				}
			}
			catch(RMAppException p_objException)
			{
				p_objErr.Add (p_objException, BusinessAdaptorErrorType.Error);			
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.GetDiaryDom.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally 
			{
				objWPA = null;
				objXmlUser = null;
			}
			return (true);
		}

		/// <param name="p_objXmlIn">XML containing the input parameters needed to delete a given diary
		///		The structure of input XML document would be:
		///		<Document>
		///			<VoidDiary fromdate="121" todate="12/12/2004"  assignuser="10" diaryid="1212">
		///			</VoidDiary>
		///		</Document>
		/// </param>

		/// <param name="p_objXmlOut"> Output XML not required

		/// <summary>
		/// Voids the given diary
		/// </summary>
		/// <param name="p_objXmlIn">XML document containing input parameters</param>
		/// <param name="p_objXmlOut">XML document containing void diary information</param>
		/// <param name="p_objErr">Error object</param>
		/// <returns>true if successful else false</returns>
		public bool VoidDiary (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
		{
			// Mihika 12/19/2005 Check security and fail if sufficient privilege doesn't exist
			if (!userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_VOID))
				throw new PermissionViolationException(RMPermissions.RMO_DIARY_VOID,RMB_DIARY);

			WPA objWPA = null;
			XmlElement objXmlUser = null;
			string sFromDate = "";
			string sToDate = "";
			string sAssignUser = "";
			long lDiaryId = 0;
            //Start: BRD#:5.1.18; MITS:29791; nsureshjain; 09/28/2012;
            string sMultipleEntryIds = string.Empty;
            string[] arrEntryIds;
            int iIterator = 0;
            bool bIsSuccess = false;
            //End: BRD#:5.1.18; MITS:29791; nsureshjain; 09/28/2012;
			try 
			{
				objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode ("//VoidDiary");
				sFromDate = objXmlUser.GetAttribute ("fromdate");
				sToDate = objXmlUser.GetAttribute ("todate");
				sAssignUser = objXmlUser.GetAttribute ("assignuser");
                //Start: BRD#:5.1.18; MITS:29791; nsureshjain; 09/28/2012; void multiple diaries
                //lDiaryId = Conversion.ConvertStrToLong(objXmlUser.GetAttribute("diaryid"));

                //objWPA = new WPA(connectionString);
                //objWPA.VoidDiary(sFromDate, sToDate, sAssignUser, lDiaryId);

                sMultipleEntryIds = objXmlUser.GetAttribute ("diaryid");
                if (sMultipleEntryIds.IndexOf(',') == -1)
                {
                    lDiaryId = Conversion.CastToType<long>(sMultipleEntryIds,out bIsSuccess);

                    objWPA = new WPA(connectionString, base.ClientId);//rkaur27
                    objWPA.VoidDiary(sFromDate, sToDate, sAssignUser, lDiaryId);
                }
                else
                {
                    arrEntryIds = sMultipleEntryIds.Split(new char[] { ',' });
                    for (iIterator = 0; iIterator < arrEntryIds.Length; iIterator++)
                    {
                        lDiaryId = Conversion.CastToType<long>(arrEntryIds[iIterator], out bIsSuccess);

                        //objWPA = new WPA(connectionString);
                        objWPA = new WPA(connectionString, base.ClientId);
                        objWPA.VoidDiary(sFromDate, sToDate, sAssignUser, lDiaryId);
                    }
                }
                //End: BRD#:5.1.18; MITS:29791; nsureshjain; 09/28/2012; void multiple diaries
			}
			catch(RMAppException p_objException)
			{
				p_objErr.Add (p_objException, BusinessAdaptorErrorType.Error);			
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.VoidDiary.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally 
			{
				objWPA = null;
				objXmlUser = null;
			}
			return (true);
		}

		/// <summary>
		/// Deletes all Diaries with the given claim.
		/// </summary>
		/// <param name="p_objXmlIn">XML document containing input parameters</param>
		/// <param name="p_objXmlOut">XML document containing attached diary information</param>
		/// <param name="p_objErr">Error object</param>
		/// <returns>true if successful else returns false</returns>
		public bool DeleteAllDiaries (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
		{
			XmlElement objXmlUser = null;
			WPA objWPA = null;
			int iClaimId = 0;
			bool bReturn = false;
			try 
			{
				objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode ("//ClaimId");
				iClaimId = Conversion.ConvertStrToInteger (objXmlUser.InnerText);
				objWPA = new WPA (connectionString,base.userLogin.objRiskmasterDatabase.DataSourceName,base.userLogin.Password,base.m_loginName, base.ClientId);
				
			//	objWPA.SecureDSN = securityConnectionString;
				if (!userLogin.IsAllowedEx(RMB_DIARY, RMO_ALL_DIARY_CLAIM_DELETE))
					throw new PermissionViolationException(RMPermissions.RMO_DELETE,RMB_DIARY);
				else
					bReturn = objWPA.DeleteAllDiaries (iClaimId);
				
			}
			catch(RMAppException p_objException)
			{
				p_objErr.Add (p_objException, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.AttachDiary.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally 
			{
				objWPA = null;
			}
			return (true);
		}

		/// <param name="p_objXmlIn">Input XML not required

		/// <param name="p_objXmlOut">Output XML containing user information.
		///		<users>
		///			<user userid="3" firstname="bali" lastname="jas" loginname="jasbali" />
		///			<user userid="2" firstname="rathod" lastname="sumeet" loginname="srathod2" />
		///		</users>

		/// <summary>
		/// Attaches a diary with the given claim or event etc.
		/// </summary>
		/// <param name="p_objXmlIn">XML document containing input parameters</param>
		/// <param name="p_objXmlOut">XML document containing attached diary information</param>
		/// <param name="p_objErr">Error object</param>
		/// <returns>true if successful else returns false</returns>
		public bool AttachDiary (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
		{
			WPA objWPA = null;
			try 
			{
                objWPA = new WPA(connectionString, base.ClientId);//rkaur27
				objWPA.SecureDSN = securityConnectionString;
				if (!userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_CREATE))
					throw new PermissionViolationException(RMPermissions.RMO_CREATE,RMB_DIARY);
				else
					p_objXmlOut = objWPA.AttachDiary (DSNID.ToString ());
			}
			catch(RMAppException p_objException)
			{
				p_objErr.Add (p_objException, BusinessAdaptorErrorType.Error);
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.AttachDiary.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally 
			{
				objWPA = null;
			}
			return (true);
		}

		/// <param name="p_objXmlIn">XML containing the input parameters needed to delete a given diary
		///		The structure of input XML document would be:
		///		<Document>
		///			<LoadDiary entryid="121">
		///			</LoadDiary>
		///		</Document>
		/// </param>

		/// <param name="p_objXmlOut">Output XML format is as follows:
		///		<LoadDiary>
		///			<EntryId>1241</EntryId>
		///			<TaskSubject>QD for FUNDS 1</TaskSubject>
		///			<TaskNotes>Quick Diary</TaskNotes>
		///			<CompleteDate>10/12/2004</CompleteDate>
		///			<EstimateTime></EstimateTime>
		///			<Priority>1</Priority>
		///			<Regarding>Funds Management - Payments</Regarding>
		///			<Activities></Activities>
		///			<StatusOpen>True</StatusOpen>
		///			<ResponseDate></ResponseDate>
		///			<Response></Response>
		///			<TeTracked>False</TeTracked>
		///			<TEStartTime></TEStartTime>
		///			<TEEndTime></TEEndTime>
		///			<TeTotalHours>0</TeTotalHours>
		///			<TeExpenses>0</TeExpenses>
		///		</LoadDiary>

		/// <summary>
		/// Load the diary information
		/// </summary>
		/// <param name="p_objXmlIn">XML document containing input parameter</param>
		/// <param name="p_objXmlOut">XML document containing diary information</param>
		/// <param name="p_objErr">Error object</param>
		/// <returns>true if successful else false</returns>
		public bool LoadDiary (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
		{
			WPA objWPA = null;
            XmlElement objXmlUser, xmlele = null; //igupta3 jira 439
			int iEntryId = 0;
			try 
			{
				objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode ("//LoadDiary");
				iEntryId = Conversion.ConvertStrToInteger (objXmlUser.GetAttribute ("entryid"));
                objWPA = new WPA(connectionString, base.ClientId);//rkaur27
				//p_objXmlOut = objWPA.LoadDiary (iEntryId, loginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName);
                p_objXmlOut = objWPA.LoadDiary(iEntryId, loginName, userLogin.Password, base.userLogin); //Aman ML Change -- parameter being passed is changed
                //igupta3 jira 439
                xmlele = p_objXmlOut.CreateElement("OVERRIDE_NOTROLL_PERM");               
                if (!userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_NOTROLL))
                {
                    xmlele.InnerText = Boolean.FalseString;
                }
                else
                {
                    xmlele.InnerText = Boolean.TrueString;
                }
                p_objXmlOut.DocumentElement.AppendChild(xmlele);

                xmlele = p_objXmlOut.CreateElement("OVERRIDE_NOTROUTE_PERM");
                if (!userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_NOTROUTE))
                {
                    xmlele.InnerText = Boolean.FalseString;
                }
                else
                {
                    xmlele.InnerText = Boolean.TrueString;
                }
                p_objXmlOut.DocumentElement.AppendChild(xmlele);

			}
			catch(RMAppException p_objException)
			{
				p_objErr.Add (p_objException, BusinessAdaptorErrorType.Error);			
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.LoadDiary.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally 
			{
				objWPA = null;
				objXmlUser = null;
                xmlele = null;
			}
			return (true);
		}

		/// <param name="p_objXmlIn">XML containing the input parameters needed to delete a given diary
		///		The structure of input XML document would be:
		///		<Document>
		///			<SaveDiary base="" notify="false" root="">
		///				<EntryId>1243</EntryId> 
		///				<StatusOpen>False</StatusOpen> 
		///				<TeTracked>True</TeTracked> 
		///				<TeTotalHours>22</TeTotalHours> 
		///				<TeExpenses>450</TeExpenses> 
		///				<TeStartTime>12:22</TeStartTime> 
		///				<TeEndTime>23:23</TeEndTime> 
		///				<ResponseDate>09/22/2004</ResponseDate> 
		///				<Response>EWRwerwerwe</Response> 
		///			</SaveDiary>
		///		</Document>
		/// </param>
		
		/// <param name="p_objXmlIn">
		/// The structure of output XML document would be:
		/// <Diary>true</Diary>

		/// <summary>
		/// Save the diary information
		/// </summary>
		/// <param name="p_objXmlIn">XML Document containing diary information to be saved.</param>
		/// <param name="p_objXmlOut">Output XML document</param>
		/// <param name="p_objErr">Error object</param>
		/// <returns>true if successful else returns false</returns>
		public bool SaveDiary (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
		{
			// Mihika 12/19/2005 Check security and fail if sufficient privilege doesn't exist

			string sAction = string.Empty;
			if (p_objXmlIn.SelectSingleNode("//Action") != null)
				sAction	= p_objXmlIn.SelectSingleNode("//Action").InnerText;

			// Check for 'Create' permission
			if(sAction == "" && !userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_CREATE))
				throw new PermissionViolationException(RMPermissions.RMO_CREATE,RMB_DIARY);
			if (sAction == "Create" && !userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_CREATE))
				throw new PermissionViolationException(RMPermissions.RMO_CREATE,RMB_DIARY);
			// Check for 'Edit' permission
			if (sAction == "Edit" && !userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_EDIT))
				throw new PermissionViolationException(RMPermissions.RMO_UPDATE,RMB_DIARY);
			// Check for 'Route' permission
			if (sAction == "Route" && !userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_ROUTE))
				throw new PermissionViolationException(RMPermissions.RMO_DIARY_ROUTE,RMB_DIARY);
			// Check for 'Roll' permission
			if (sAction == "Roll" && !userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_ROLL))
				throw new PermissionViolationException(RMPermissions.RMO_DIARY_ROLL,RMB_DIARY);
			// Check for 'Complete' permission
			if (sAction == "" && p_objXmlIn.SelectSingleNode("//StatusOpen").InnerText == "False"
				&& !userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_COMPL))
				throw new PermissionViolationException(RMPermissions.RMO_DIARY_COMPLETE,RMB_DIARY);

			WPA objWPA = null;
			XmlElement objXmlUser = null;
			XmlElement objResult = null;
			bool bNotify = false;
			string sBase = "";
			string sRoot = "";
			bool bRetVal = false;
            Hashtable objErrors = null; //srajindersin MITS 26319 04/20/2012
			XmlNode objEntry = null;//Mobile apps
			XmlElement objCaller = null;//Mobile apps
            XmlNode objcreatetime = null;   //mbahl3 cloud changes for mobile
			
			try 
			{
				objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode ("//SaveDiary");
				bNotify = Conversion.ConvertStrToBool (objXmlUser.GetAttribute ("notify"));
				sBase = objXmlUser.GetAttribute ("base");
				sRoot = objXmlUser.GetAttribute ("root");
                objWPA = new WPA(connectionString, base.ClientId);//rkaur27
				//Mukul Added 3/12/2007 MITS 9020

                //Raman R7 Prf Imp
                if (p_objXmlIn.GetElementsByTagName("AttachTable")[0] != null)
                {
                    objWPA.SecureDSN = securityConnectionString;
                }
                //r7 prf imp
                //bRetVal = objWPA.SaveDiary(ref p_objXmlIn, loginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, bNotify, sBase, sRoot, userLogin);
                //bRetVal = objWPA.SaveDiary(ref p_objXmlIn, loginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, bNotify, sBase, sRoot, userID); //srajindersin MITS 26319 04/20/2012
                bRetVal = objWPA.SaveDiary(ref p_objXmlIn, loginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, bNotify, sBase, sRoot, userID, ref objErrors); //srajindersin MITS 26319 04/20/2012
				//  Ishan :Mobile Adjuster- Add EntryId to the response
				//akaur9 R8 Mobile Adjuster
                objResult = p_objXmlOut.CreateElement("Diary");
				objResult.InnerXml = bRetVal.ToString ();
				objCaller = (XmlElement)p_objXmlIn.SelectSingleNode("//caller");
				if (objCaller != null)
				{
                    if (objCaller.InnerText.Equals("MobileAdjuster") || objCaller.InnerText.Equals("MobilityAdjuster"))
					{
						objEntry =p_objXmlIn.SelectSingleNode("//EntryId");
						if (objEntry != null)
							objResult.SetAttribute("EntryId", objEntry.InnerText);
					}
                    //mbahl3 cloud changes for mobile
                    if (objCaller.InnerText.Equals("MobilityAdjuster"))
                    {
                        objcreatetime = p_objXmlIn.SelectSingleNode("//TimeCreated");
                        if (objcreatetime != null)
                            objResult.SetAttribute("TimeCreated", objcreatetime.InnerText);
                    }
                    //mbahl3 cloud changes for mobile
				}
				p_objXmlOut.AppendChild (objResult);
				// Ishan End
			}
			catch(RMAppException p_objException)
			{
                if (objErrors == null)
                    objErrors = new Hashtable();// intialised the hashtable in case its not so that UI gets specific error. //rkaur27
                //srajindersin MITS 26319 04/20/2012
                 foreach (System.Collections.DictionaryEntry objError in objErrors)
                 p_objErr.Add("ValidationScriptError", objError.Value.ToString(), Common.BusinessAdaptorErrorType.Error);

                 if (objErrors.Count == 0) //Mits id : 27990 - Govind
                    p_objErr.Add(p_objException.Source, p_objException.Message, Common.BusinessAdaptorErrorType.Error); // MITS 31546 averma62
				
                //srajindersin MITS 26319 04/20/2012				
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.SaveDiary.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally 
			{
				objWPA = null;
				objXmlUser = null;
				objResult = null;
                objEntry = null;
                objCaller = null;
                objcreatetime = null;


			}
			return (bRetVal);
		}
		/// Added By Rahul Sharma on 01/18/2006.
		/// <param name="p_objXmlIn">XML containing the input parameters needed to route a given diary
		///		The structure of input XML document would be:
		///		<Document>
		///			<SaveDiary base="" notify="false" root="">
		///				<EntryId>1243</EntryId> 
		///				<StatusOpen>False</StatusOpen> 
		///				<TeTracked>True</TeTracked> 
		///				<TeTotalHours>22</TeTotalHours> 
		///				<TeExpenses>450</TeExpenses> 
		///				<TeStartTime>12:22</TeStartTime> 
		///				<TeEndTime>23:23</TeEndTime> 
		///				<ResponseDate>09/22/2004</ResponseDate> 
		///				<Response>EWRwerwerwe</Response> 
		///			</SaveDiary>
		///		</Document>
		/// </param>
		
		/// <param name="p_objXmlIn">
		/// The structure of output XML document would be:
		/// <Diary>true</Diary>

		/// <summary>
		/// Route the diary information
		/// </summary>
		/// <param name="p_objXmlIn">XML Document containing diary information to be saved.</param>
		/// <param name="p_objXmlOut">Output XML document</param>
		/// <param name="p_objErr">Error object</param>
		/// <returns>true if successful else returns false</returns>
		public bool RouteDiary (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
		{
			// Mihika 12/19/2005 Check security and fail if sufficient privilege doesn't exist

			string sAction = string.Empty;
			if (p_objXmlIn.SelectSingleNode("//Action") != null)
				sAction	= p_objXmlIn.SelectSingleNode("//Action").InnerText;

			if (sAction == "Route" && !userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_ROUTE))
				throw new PermissionViolationException(RMPermissions.RMO_DIARY_ROUTE,RMB_DIARY);
			
			if (sAction == "" && p_objXmlIn.SelectSingleNode("//StatusOpen").InnerText == "False"
				&& !userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_COMPL))
				throw new PermissionViolationException(RMPermissions.RMO_DIARY_COMPLETE,RMB_DIARY);

			WPA objWPA = null;
			XmlElement objXmlUser = null;
			XmlElement objResult = null;
			bool bNotify = false;
			string sBase = "";
			string sRoot = "";
			bool bRetVal = false;
			
			try 
			{
				objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode ("//RouteDiary");
				bNotify = Conversion.ConvertStrToBool (objXmlUser.GetAttribute ("notify"));
				sBase = objXmlUser.GetAttribute ("base");
				sRoot = objXmlUser.GetAttribute ("root");
				//Raman: need login name too for setting completedbyuser
                //objWPA = new WPA(connectionString);
				objWPA = new WPA(connectionString, base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.Password, base.m_loginName, base.ClientId);
                
                objWPA.SecureDSN = m_securityConnectionString; //pmittal5 Confidential Record
                bRetVal = objWPA.RouteDiary(ref p_objXmlIn, loginName, userLogin.Password, userLogin.objRiskmasterDatabase.DataSourceName, bNotify, sBase, sRoot, base.m_loginName, userID);
				objResult = p_objXmlOut.CreateElement ("Diary");
				objResult.InnerXml = bRetVal.ToString ();
				p_objXmlOut.AppendChild (objResult);
			}
			catch(RMAppException p_objException)
			{
				p_objErr.Add (p_objException, BusinessAdaptorErrorType.Error);				
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.RouteDiary.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally 
			{
				objWPA = null;
				objXmlUser = null;
				objResult = null;
			}
			return (bRetVal);
		}

		/// <param name="p_objXmlIn">XML containing the input parameters needed to delete a given diary
		///		The structure of input XML document would be:
		///		<Document>
		///			<DiaryCalendar assigneduser="srathod2" fromdate="12/12/2004" todate="" sortby="" sortorder="">
		///			</DiaryCalendar>
		///		</Document>
		/// </param>

		///<param name="p_objXmlOut">
		///The structure of output XML document would be:
		//		<Diaries>
		//			<DiaryCalendar Date="February 04" />
		//			<DiaryCalendar Date="February 05" />
		//			<DiaryCalendar Date="February 06" />
		//		</Diaries>

		/// <summary>
		/// Diary calendar
		/// </summary>
		/// <param name="p_objXmlIn">XML document containing input parameters</param>
		/// <param name="p_objXmlOut">XML document containing diary calendar information</param>
		/// <param name="p_objErr">Error object</param>
		/// <returns>true if successful else returns false</returns>
		public bool DiaryCalender (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
		{
            //Mits 19096
            if (!userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_CALENDER))
                throw new PermissionViolationException(RMPermissions.RMO_ACCESS, RMB_DIARY);
			WPA objWPA = null;
			XmlElement objXmlUser = null;
			string sAssignedUser = "";
			string sFromDate = "";
			string sToDate = "";
			string sSortBy = "";
			string sSortOrder = "";
			int iDateRange = 0;
			int iDiaryStyle = 0;
			int iSelectedMonth = 0;
			int iSelectedYear = 0;
            string sLangCode = string.Empty;
            string sPageId = string.Empty;
			try 
			{
				//objXmlUser = p_objXmlIn.DocumentElement;
				objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode ("//DiaryCalendar");
				sAssignedUser = objXmlUser.GetAttribute ("assigneduser");
				sFromDate = objXmlUser.GetAttribute ("fromdate");
				sToDate = objXmlUser.GetAttribute ("todate");
				sSortBy = objXmlUser.GetAttribute ("sortby");
				sSortOrder = objXmlUser.GetAttribute ("sortorder");
				iDateRange = Conversion.ConvertStrToInteger(objXmlUser.GetAttribute("daterange"));
				iDiaryStyle = Conversion.ConvertStrToInteger(objXmlUser.GetAttribute("diarystyle"));
				iSelectedMonth = Conversion.ConvertStrToInteger(objXmlUser.GetAttribute("selectedmonth"));
				iSelectedYear = Conversion.ConvertStrToInteger(objXmlUser.GetAttribute("selectedyear"));
                sLangCode = objXmlUser.GetAttribute("LangCode");
                sPageId = objXmlUser.GetAttribute("PageID");

                objWPA = new WPA(connectionString, base.ClientId);//rkaur27
                p_objXmlOut = objWPA.DiaryCalender(sAssignedUser, sFromDate, sToDate, sSortBy, sSortOrder, iDateRange, iDiaryStyle, iSelectedMonth, iSelectedYear, sLangCode, sPageId);
			}
			catch(RMAppException p_objException)
			{
				p_objErr.Add (p_objException, BusinessAdaptorErrorType.Error);			
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.DiaryCalendar.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally 
			{
				objWPA = null;
				objXmlUser = null;
			}
			return (true);
		}

		/// <param name="p_objXmlIn">XML containing the input parameters needed to delete a given diary
		///		The structure of input XML document would be:
		///		<Document>
		///			<DiaryList assigneduser="srathod2" fromdate="12/12/2004" todate="1/1/2005" sortby="" diaryselected="">
		///			</DiaryList>
		///		</Document>
		/// </param>

		/// <param name="p_objXmlOut">Output XML format is as follows
		///		<Diaries>
		///			<DiaryList>
		///				<Diary Assigned_By="srathod2" Due_Date="9/9/2004" Priority="Optional" AttachedRecord="CLAIM WCLT00000461" claimantname=", " departmentname="City of Oak Hills COMPANY" Create_Date="9/9/2004" Task_Description="QD for CLAIM 4">
		///					<Activities>
		///						<Activity>No Activity Specified</Activity>
		///					</Activities>
		///				</Diary>
		///				<Diary Assigned_By="srathod2" Due_Date="9/16/2004" Priority="Optional" AttachedRecord="No " departmentname="City of Oak Hills COMPANY" Create_Date="9/16/2004" Task_Description="LOLO">
		///					<Activities>
		///						<Activity>DDD</Activity>
		///							<Activity>DDDDDDDD</Activity>
		///					</Activities>
		///				</Diary>
		///			</DiaryList>
		///		</Diaries>

		/// <summary>
		/// Fetches the diary list
		/// </summary>
		/// <param name="p_objXmlIn">XML document containing input parameters</param>
		/// <param name="p_objXmlOut">XML document containing diary list</param>
		/// <param name="p_objErr">Error object</param>
		/// <returns>true if successful else false</returns>
		public bool DiaryList (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
		{
			// Mihika 12/19/2005 Check security and fail if sufficient privilege doesn't exist
			if (!userLogin.IsAllowedEx(RMB_DIARY))
				throw new PermissionViolationException(RMPermissions.RMO_ACCESS,RMB_DIARY);

			WPA objWPA = null;
			XmlElement objXmlUser = null;
			string sAssignedUser = "";
			string sFromDate = "";
			string sToDate = "";
			string sSortBy = "";
            string sTaskDesc = "";
            string sSortOrder = string.Empty;
            bool bUseTaskDesc = false;
			int iDiarySel = 0;
            //asingh263 MITS 34874 starts
            string sScreen = string.Empty;
            string sTaskName = string.Empty;
            string sCreatedOn = string.Empty;
            string sDueDate = string.Empty;
            string sWorkActivity = string.Empty;
            string sEstimatedTime = string.Empty;
            string sPriority=string.Empty;
            string sNotes = string.Empty;
            string sRegarding = string.Empty;

            //asingh263 MITS 34874 ends

            //Added by Nitin on 28 Feb 2009 for Mits 10641 merging with R5
            //Asif MITS:10641 START
            MemoryStream objMemory = null;
            XmlElement objTempElement = null;
            WPA objResults = null;
            XmlDocument objGetDiaryList = null;
            //ASIF MITS:10641 END
            //Ended by Nitin for Mits 10641 merging with R5
			XmlElement objCaller = null;
			string sClaimNumber = string.Empty;
            string sLangCode = string.Empty; //MITS 34392 hlv begin
            string sPageId = string.Empty;
            string sCurrentPageId = string.Empty;
            string createDate = string.Empty; //nsharma202

			try 
			{
				objXmlUser = (XmlElement) p_objXmlIn.SelectSingleNode ("//DiaryList");
				objCaller = (XmlElement)  p_objXmlIn.SelectSingleNode("//caller");
                createDate = objXmlUser.GetAttribute("createDate"); //nsharma202
				sAssignedUser = objXmlUser.GetAttribute ("assigneduser");
				sFromDate = objXmlUser.GetAttribute ("fromdate");
				sToDate = objXmlUser.GetAttribute ("todate");
				sSortBy = objXmlUser.GetAttribute ("sortby");
                sSortOrder = objXmlUser.GetAttribute("sortorder"); //MITS 36691: aaggarwal29
                //asingh263 MITS 34874 starts
                sScreen = objXmlUser.GetAttribute("screen");
                sTaskName = objXmlUser.GetAttribute("taskname");
                sCreatedOn = objXmlUser.GetAttribute("createdon");
                sDueDate = objXmlUser.GetAttribute("duedate");
                sWorkActivity = objXmlUser.GetAttribute("workactivity");
                sEstimatedTime = objXmlUser.GetAttribute("estimatedtime");
                sPriority = objXmlUser.GetAttribute("priority");
                sNotes = objXmlUser.GetAttribute("notes");
                sRegarding = objXmlUser.GetAttribute("regarding");
                //asingh263 MITS 34874 ends
                //mbahl3 code change for cloud
                if (objCaller!=null && (objCaller.InnerText == "MobileAdjuster" || objCaller.InnerText == "MobilityAdjuster"))//Add null check by kuladeep for Jira-1531
                {
                    sLangCode = m_userLogin.objUser.NlsCode.ToString();
                }
                //mbahl3 code change for cloud
                else
                {
                    sLangCode = objXmlUser.GetAttribute("langcode"); //MITS 34392 hlv
                }

                sPageId = objXmlUser.GetAttribute("CreateDiaryPageId");
                sCurrentPageId = objXmlUser.GetAttribute("CurrentPageId"); 
                //sgoel6 04/06/2009 | MITS 14636 Replace single quote with two single quotes 
                sTaskDesc = objXmlUser.GetAttribute("taskdesc").Replace("'", "''");
                bUseTaskDesc = Conversion.ConvertStrToBool(objXmlUser.GetAttribute("usetaskdesc"));

                if (bUseTaskDesc == false)
                    sTaskDesc = "";

				iDiarySel = Conversion.ConvertStrToInteger (objXmlUser.GetAttribute ("diarysel"));
				objWPA = new WPA (connectionString, base.ClientId); //rkaur27
                objWPA.LanguageCode = Convert.ToInt32(sLangCode); //MITS 34392 hlv
                //Amandeep MITS 28801
                if (objCaller == null)   //for RMX objCaller node will be null
                {
                    //if (string.IsNullOrEmpty(objCaller.InnerText) || objCaller.InnerText != "MobileAdjuster")
                    //{
                        //Added by Nitin on 28 Feb 2009 for Mits 10641 merging with R5
                        //03/08/2008 Asif Start MITS:10641
                        // p_objXmlOut = objWPA.DiaryList(sAssignedUser, iDiarySel, sFromDate, sToDate, sSortBy, sTaskDesc);
                    //asingh263 MITS 34874 starts

                    if (sScreen == "selected")
                    {
                        objGetDiaryList = objWPA.DiaryList(sAssignedUser, iDiarySel, sFromDate, sToDate, sSortBy, sTaskDesc, sTaskName, sCreatedOn, sDueDate, sWorkActivity, sEstimatedTime, sPriority, sNotes, sRegarding, sScreen, sPageId, sCurrentPageId, sSortOrder); //MITS36691:aaggarwal29, method def changed to add sort direction
                        objWPA.CreateActiveDiariesReportDetails(p_objXmlIn.InnerXml, out objMemory,"");
                        objTempElement = p_objXmlOut.CreateElement("Diaries");
                        p_objXmlOut.AppendChild(objTempElement);
                        objTempElement = p_objXmlOut.CreateElement("File");
                        objTempElement.SetAttribute("Filename", Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".pdf"));
                        objTempElement.InnerText = Convert.ToBase64String(objMemory.ToArray());
                        p_objXmlOut.FirstChild.AppendChild(objTempElement);
                    }
                    else
                    //asingh263 MITS 34874 ends
                    {
                        objGetDiaryList = objWPA.DiaryList(sAssignedUser, iDiarySel, sFromDate, sToDate, sSortBy, sTaskDesc, sTaskName, sCreatedOn, sDueDate, sWorkActivity, sEstimatedTime, sPriority, sNotes, sRegarding, sScreen, sPageId, sCurrentPageId, sSortOrder); //MITS36691:aaggarwal29, method def changed to add sort direction
                        objMemory = objWPA.PrintActiveDiaries(objGetDiaryList, sAssignedUser, sCurrentPageId);
                        objTempElement = p_objXmlOut.CreateElement("Diaries");
                        p_objXmlOut.AppendChild(objTempElement);
                        objTempElement = p_objXmlOut.CreateElement("File");
                        objTempElement.SetAttribute("Filename", Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".pdf"));
                        objTempElement.InnerText = Convert.ToBase64String(objMemory.ToArray());
                        p_objXmlOut.FirstChild.AppendChild(objTempElement);
                    }
                        //END Asif MITS:10641
                        //End by Nitin on 28 Feb 2009 for Mits 10641 merging with R5
                   // }
                }
                else if (objCaller != null && (objCaller.InnerText == "MobileAdjuster" || objCaller.InnerText == "MobilityAdjuster"))  //akaur9 R8 Mobile Adjuster   //Amandeep MITS 28801
                {
                    sClaimNumber = objXmlUser.GetAttribute("claimnumber");
                    //objGetDiaryList = objWPA.DiaryList(sAssignedUser, iDiarySel, sFromDate, sToDate, sSortBy, sTaskDesc, sClaimNumber);
                    p_objXmlOut = objWPA.DiaryList(sAssignedUser, sFromDate, sToDate, sSortBy, sTaskDesc, sClaimNumber, objCaller.InnerText, sPageId, sCurrentPageId, createDate); //nsharma202 //Mobility changes Mits 35351
                    p_objXmlOut.DocumentElement.SetAttribute("assigneduser", sAssignedUser);
                    p_objXmlOut.DocumentElement.SetAttribute("fromdate", sFromDate);
                    p_objXmlOut.DocumentElement.SetAttribute("sortby", sSortBy);
                    p_objXmlOut.DocumentElement.SetAttribute("taskdesc", sTaskDesc);
                    p_objXmlOut.DocumentElement.SetAttribute("todate", sToDate);
                    p_objXmlOut.DocumentElement.SetAttribute("usetaskdesc", true.ToString());
                    p_objXmlOut.DocumentElement.SetAttribute("claimnumber", sClaimNumber);
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.DiaryList.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objWPA = null;
                objXmlUser = null;
            }
            return (true);
        }

        //nsharma202
        /// <summary>
        /// Gets Diaries added after lastSyncDateTime stamp
        /// </summary>
        /// <param name="user"></param>
        /// <param name="lastSyncDateTime"></param>
        /// <param name="output"></param>
        /// <param name="p_objErr"></param>
        /// <returns>DiaryList object containing diaries</returns>
        public bool DiaryList(String user, String lastSyncDateTime, ref DiaryList output, ref BusinessAdaptorErrors p_objErr)
        {
            // Mihika 12/19/2005 Check security and fail if sufficient privilege doesn't exist
            if (!userLogin.IsAllowedEx(RMB_DIARY))
                throw new PermissionViolationException(RMPermissions.RMO_ACCESS, RMB_DIARY);
            WPA objWPA = null;
            try
            {
                objWPA = new WPA(connectionString, base.ClientId);
                string assignedUser = string.Empty;
                string fromdate = string.Empty;
                string toDate = string.Empty;
                string sortBy = string.Empty;
                string taskDescription = string.Empty;
                string claimNumber = string.Empty;
                string pageId = string.Empty;
                string currentPageId = string.Empty;
                string createDate = string.Empty;

                assignedUser = user;
                createDate = lastSyncDateTime;

                output = objWPA.DiaryListObject(assignedUser, fromdate, toDate, sortBy, taskDescription, claimNumber, pageId, currentPageId, createDate);
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), 1);
                return false;
                //1 should be clientid;
            }
            finally
            {
                objWPA = null;
            }
            return (true);
        }

      

        /// <param name="p_objXmlIn">XML containing the input parameters needed to get the Activity Codes
        ///		The structure of input XML document would be:
        ///		<Document>
        ///		</Document>
        /// </param>

		/// <param name="p_objXmlOut">Output XML format is as follows
		///		<Diaries>
		///			<Activities Count>
		///				<Activity codeid>ShortCode + CodeDesc</Activity>
		///			</Activities>
		///		</Diaries>

		/// <summary>
		/// Fetches the Activity Codes
		/// </summary>
		/// <param name="p_objXmlIn">XML document containing input parameters</param>
		/// <param name="p_objXmlOut">XML document containing diary list</param>
		/// <param name="p_objErr">Error object</param>
		/// <returns>true if successful else false</returns>
		public bool GetActivityCodes (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
		{
			WPA objWPA = null;
						
			try 
			{
				objWPA = new WPA (connectionString, base.ClientId); //rkaur27
                p_objXmlOut = objWPA.GetActivityCodes(base.userLogin);  //Aman ML Change
			}
			catch(RMAppException p_objException)
			{
				p_objErr.Add (p_objException, BusinessAdaptorErrorType.Error);				
				return false;
			}
			catch(Exception p_objException)
			{
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.GetActivityCodes.Error", base.ClientId), BusinessAdaptorErrorType.Error);
				return false;
			}
			finally
			{
				objWPA = null;
			}
			return (true);
		}
		#region public bool GetUsers(XmlDocument p_objXmlDocIn, ref XmlDocument p_objXmlDocOut, ref BusinessAdaptorErrors p_objErrors)
		/// Name			: GetUsers
		/// Author			: Rahul Sharma
		/// Date Created	: 12-June-2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment                                   *    Author
		///	7/12/07			*	Mits 9388, add node of wpa tasks			* Tom Regan
		/// ************************************************************
		/// <summary>
		/// Wrapper to WPA.GetUsers of Application
		/// layer that returns users in form of XML
		/// </summary>
		/// <param name="p_objXmlDocIn"> 
		///	Input paramters in xml format.
		///	Structure:
		///		<GetUsers/>
		/// </param>
		/// <param name="p_objXmlDocOut"> 
		///	Method output in xml format.
		///	Structure:
		///		<GetUsersResult>
		///			<data>
		///				<user firstname="" lastname="" userid="">
		///			</data>
		///		</GetUsersResult>
		///	</param>
		/// <param name="p_objErrors">
		///	Collection of errors/messages.	
		///	</param>
		/// <returns>
		///		true - success
		///		false - failure
		///	</returns>
		
		public bool GetUsers(XmlDocument p_objXmlDocIn, ref XmlDocument p_objXmlDocOut, 
			ref BusinessAdaptorErrors p_objErrors)
		{
			bool bReturnValue = false;
			string sCurrentUserName = "";
			int iPSID = 0;
			XmlNode objXmlNode = null;
            XmlDocument p_objXmlDocTemp = null;
			XmlElement objRootElement = null;
			
			WPA objWPA = null;

			String objXmlDocOut;		

			try	
			{
                string sCurrentDataSourceName = this.userLogin.objRiskmasterDatabase.DataSourceName;
                sCurrentUserName = this.userLogin.LoginName;
                string sSQL = "";
                bool scriptEditor = false;
                object keyValue = 0;
                int firingScriptFlag = 0;

				//get psid value
				objXmlNode = p_objXmlDocIn.SelectSingleNode("//psid");
				if( objXmlNode != null)
				{
					if(objXmlNode.InnerText.Trim() != "" )
					{
						iPSID = int.Parse(objXmlNode.InnerText);
					}
				}
				objXmlNode = null;

                objWPA = new WPA(base.m_connectionString, base.ClientId);//rkaur27
                objWPA.GetUsers(sCurrentDataSourceName, iPSID, out objXmlDocOut, base.userLogin);

                objRootElement = p_objXmlDocOut.CreateElement("GetUsersResult");
                objRootElement.InnerXml = objXmlDocOut;

                //adding attribute LoginUser to retrieve the current user..raman bhatia
                objRootElement.SetAttribute("LoginUser", sCurrentUserName);
                objRootElement.SetAttribute("LoginID", this.userLogin.UserId.ToString());
                objRootElement.SetAttribute("DefaultAssignedTo", objWPA.GetDefaultAssignedTo());
                //adding attribute to retrieve the current date and cuurent time..raman bhatia
                objRootElement.SetAttribute("CurrentDate", String.Format("{0:MM/dd/yyyy}", DateTime.Now));
                objRootElement.SetAttribute("CurrentTime", String.Format("{0:hh:mm tt}", DateTime.Now));

                // rrachev JIRA 4607
                //string disabledMail = "false";
                //if (objWPA.GetOption(WPA.DISABLE_MAIL))
                //{
                //    disabledMail = "true";
                //}
                //objRootElement.SetAttribute("MAIL_DISABLED", disabledMail);

                //dvatsa-JIRA 11627(start)
                string enabledMail = "false";
                if (objWPA.GetOption(WPA.ENABLE_MAIL))
                {
                    enabledMail = "true";
                }
                else
                {
                    enabledMail = "false";
                }
                objRootElement.SetAttribute("MAIL_ENABLED", enabledMail);
                //dvatsa-JIRA-11627(end)


                //zmohammad MITs 33655 Diary Initialisation script : Start
                sSQL = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE UPPER(PARM_NAME) = 'SCRPT_EDI_FLAG'";

                scriptEditor = Conversion.ConvertObjToBool(DbFactory.ExecuteScalar(connectionString, sSQL), base.ClientId);

                if (scriptEditor)
                {
                    sSQL = "SELECT TYPE FROM VBS_SCRIPTS WHERE UPPER(OBJECT_NAME) = 'WPADIARYENTRY'";
                    keyValue = DbFactory.ExecuteScalar(connectionString, sSQL);

                    if (keyValue != null && keyValue.ToString() == "3")
                    {
                        firingScriptFlag = 1;
                    }

                }
                if (firingScriptFlag == 1)
                {
                    p_objXmlDocTemp = objWPA.LoadDiary(0, base.userLogin.LoginName, base.userLogin.Password, base.userLogin);
                    objRootElement.SetAttribute("ScriptFired", "true");
                    objRootElement.SetAttribute("TaskSubject", p_objXmlDocTemp.SelectSingleNode("//LoadDiary/TaskSubject").InnerText);
                    objRootElement.SetAttribute("TaskNotes", p_objXmlDocTemp.SelectSingleNode("//LoadDiary/TaskNotes").InnerText);
                    objRootElement.SetAttribute("AttachRecord", p_objXmlDocTemp.SelectSingleNode("//LoadDiary/AttachRecord").InnerText);
                    objRootElement.SetAttribute("CompleteDate", p_objXmlDocTemp.SelectSingleNode("//LoadDiary/CompleteDate").InnerText);
                    objRootElement.SetAttribute("CompleteTime", p_objXmlDocTemp.SelectSingleNode("//LoadDiary/CompleteTime").InnerText);
                    objRootElement.SetAttribute("EstimateTime", p_objXmlDocTemp.SelectSingleNode("//LoadDiary/EstimateTime").InnerText);
                    objRootElement.SetAttribute("Priority", p_objXmlDocTemp.SelectSingleNode("//LoadDiary/Priority").InnerText);
                    objRootElement.SetAttribute("AutoConfirm", p_objXmlDocTemp.SelectSingleNode("//LoadDiary/AutoConfirm").InnerText);
                    objRootElement.SetAttribute("OverDueDiaryNotify", p_objXmlDocTemp.SelectSingleNode("//LoadDiary/OverDueDiaryNotify").InnerText);
                    objRootElement.SetAttribute("NotifyFlag", p_objXmlDocTemp.SelectSingleNode("//LoadDiary/NotifyFlag").InnerText);
                    objRootElement.SetAttribute("AssignedUser", p_objXmlDocTemp.SelectSingleNode("//LoadDiary/AssignedUser").InnerText);
                    objRootElement.SetAttribute("AssignedUserID", p_objXmlDocTemp.SelectSingleNode("//LoadDiary/AssignedUserID").InnerText);
                    //zmohammad MITs 33655 Diary Initialisation script : End
                    //caggarwal4 for RMA-11624
                    objRootElement.SetAttribute("NonRoutableFlag", p_objXmlDocTemp.SelectSingleNode("//LoadDiary/NonRoutableFlag").InnerText);
                    objRootElement.SetAttribute("NonRollableFlag", p_objXmlDocTemp.SelectSingleNode("//LoadDiary/NonRollableFlag").InnerText);
                }
                else
                {
                    objRootElement.SetAttribute("ScriptFired", "false");
                }
                p_objXmlDocOut.AppendChild(objRootElement);
                objWPA.GetWpaTaskList(base.userLogin, p_objXmlDocOut);  //mits 9388  //Aman ML Change
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                objWPA = new WPA(connectionString, base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.Password, base.m_loginName, base.ClientId);
                objWPA.GetSuppInformation(base.userLogin, p_objXmlDocOut);
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691

                bReturnValue = true;

			}
			catch(InitializationException p_objInitializationException)
			{
				bReturnValue = false;
				p_objErrors.Add(p_objInitializationException,BusinessAdaptorErrorType.Error);   
				return bReturnValue;
			}
			catch(RMAppException p_objRMAppException)
			{
				bReturnValue = false;
				p_objErrors.Add(p_objRMAppException,BusinessAdaptorErrorType.Error);   
				return bReturnValue;
			}
			catch(Exception p_objException)
			{
				bReturnValue = false;
                p_objErrors.Add(p_objException, Globalization.GetString("WPA.GetUsers.Error", base.ClientId), BusinessAdaptorErrorType.Error);   
				return bReturnValue;
			}
			finally
			{
				objRootElement =  null;				
			}

			return bReturnValue;
		}
		#endregion

        /// <summary>
        /// Fetches the Task Descriptions
        /// </summary>
        /// <param name="p_objXmlIn">XML document containing input parameters</param>
        /// <param name="p_objXmlOut">XML document containing diary list</param>
        /// <param name="p_objErr">Error object</param>
        /// <returns>true if successful else false</returns>
        public bool GetDiaryListing (XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            WPA objWPA = null;

            try
            {
                objWPA = new WPA(connectionString, base.ClientId);//rkaur27
                p_objXmlOut = objWPA.GetDiaryListing();
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.GetDiaryListing.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objWPA = null;
            }
            return (true);
        
        }

        /// <summary>
        /// Fetches the Task Descriptions Setting MITS 36764 srajindersin 5/23/2014
        /// </summary>
        /// <param name="p_objXmlIn">XML document containing input parameters</param>
        /// <param name="p_objXmlOut">XML document containing diary list</param>
        /// <param name="p_objErr">Error object</param>
        /// <returns>true if successful else false</returns>
        public bool GetTaskDescSetting(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            WPA objWPA = null;

            try
            {
                objWPA = new WPA(connectionString, base.ClientId);//rkaur27
                p_objXmlOut = objWPA.GetTaskDescSetting();
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.GetDiaryListing.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objWPA = null;
            }
            return (true);

        }

        #region Changes For R5 DiaryCalendar By Nitin
        public bool GetOnLoadInformation(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            WPA objWPA = null;
            bool isBESEnabled = false;
            try
            {
                objWPA = new WPA(connectionString, base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.Password, base.m_loginName, base.ClientId);
                objWPA.UserId = userID;
                isBESEnabled = CheckBESEnability();
                p_objXmlOut = objWPA.GetOnLoadInformation(isBESEnabled);
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.GetDiaryHistoryDom.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objWPA = null;
            }
            return true;
        }

        public bool GetCalendarView(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            WPA objWPA = null;
            try
            {
                objWPA = new WPA(connectionString, base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.Password, base.m_loginName, base.ClientId);
                objWPA.UserId = userID;
                p_objXmlOut = objWPA.GetCalendarView();
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.GetDiaryHistoryDom.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {

                objWPA = null;
            }
            return true;
        }

        public bool SetCalendarView(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            WPA objWPA = null;
            string code = string.Empty;
            XmlNode xmlCode = null;

            try
            {
                objWPA = new WPA(connectionString, base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.Password, base.m_loginName, base.ClientId);
                objWPA.UserId = userID;

                xmlCode = p_objXmlIn.SelectSingleNode("GetDiaryDom/@Code");
                if (xmlCode != null && xmlCode.Value != null)
                {
                    code = xmlCode.Value;
                }
                objWPA.SetCalendarView(code);
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.GetDiaryHistoryDom.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objWPA = null;
            }
            return true;
        }

        /// <summary>
        /// Created by Nitin to check BES Enability
        /// </summary>
        /// <returns></returns>
        public bool CheckBESEnability()
        {
            ORGSEC objOrgSec = null;
            XmlElement besNode = null;

            try
            {
                //objOrgSec = new ORGSEC(userLogin);
                objOrgSec = new ORGSEC(userLogin, base.ClientId);
                if (objOrgSec.IsOrgSetFirstTime()) //if it is true , then BES is false
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        /// <summary>
        /// Created by Nitin to fetch DiaryList from DataBase as per given action peek/processingoffice/default
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErr"></param>
        /// <returns></returns>
        public bool GetDairiesForDairyCalender(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            XmlElement objXmlUser = null;
            string calenderAction = string.Empty;
            bool bFromStyle = false;
            try
            {
                objXmlUser = (XmlElement)p_objXmlIn.SelectSingleNode("//GetDiaryDom");
                calenderAction = objXmlUser.GetAttribute("action");
                //SAFEWAY: paggarwal2: 04/11/2008: Added for Diary Screens-start
                bFromStyle = Convert.ToBoolean(objXmlUser.GetAttribute("FromStyle"));
                if (bFromStyle)
                {
                    SetCalendarView(p_objXmlIn, ref p_objXmlOut, ref p_objErr);
                }
                //SAFEWAY: paggarwal2: 04/11/2008: Added for Diary Screens-end
                switch (calenderAction.ToLower())
                {
                    case "peek": //get dairylist of subordinates of current loggedin user
                        GetPeekDairyForDairyCalender(p_objXmlIn, ref  p_objXmlOut, ref p_objErr);
                        break;
                    case "processingoffice": //fetch dairylist of users in same processing office
                        GetDairiesForProcessingOffice(p_objXmlIn, ref  p_objXmlOut, ref  p_objErr);
                        break;
                    case "checkbesenability":
                        //CheckBESEnability(ref p_objXmlOut);
                        break;
                    default:  //fetch dairylist of current user
                        GetDiaryDomForDiaryCalender(p_objXmlIn, ref p_objXmlOut, ref p_objErr);
                        break;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }

        }

        /// <summary>
        /// Created by Nitin for DiaryCalender inorder to fetch DiaryList for Peek Functionality
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErr"></param>
        /// <returns></returns>
        private bool GetPeekDairyForDairyCalender(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            CustomizeUserList objCustList = null;
            DataSet dset = new DataSet();
            XmlNode dairyDomNode = null;

            XmlElement peekNode = null;
            XmlElement peekUser = null;
            XmlAttribute userLoginName = null;
            XmlAttribute userLastName = null;
            XmlAttribute userFirstName = null;

            XmlDocument outPutDoc = null;

            try
            {
                objCustList = new CustomizeUserList(connectionString, userLogin.objRiskmasterDatabase.DataSourceId.ToString(),
                    securityConnectionString, userLogin.UserId,base.ClientId);

                dset = objCustList.GetUserListForDiaryCalendar("peek");

                peekNode = p_objXmlOut.CreateElement("PeekList");
                if (dset != null && dset.Tables[0] != null)
                {
                    foreach (DataRow drow in dset.Tables[0].Rows)
                    {
                        peekUser = p_objXmlOut.CreateElement("User");

                        userLoginName = p_objXmlOut.CreateAttribute("LoginName");
                        userLoginName.Value = drow["LOGIN_NAME"].ToString();

                        userLastName = p_objXmlOut.CreateAttribute("LastName");
                        userLastName.Value = drow["LAST_NAME"].ToString();

                        userFirstName = p_objXmlOut.CreateAttribute("FirstName");
                        userFirstName.Value = drow["FIRST_NAME"].ToString();


                        peekUser.Attributes.Append(userLoginName);

                        peekUser.Attributes.Append(userLastName);
                        peekUser.Attributes.Append(userFirstName);

                        dairyDomNode = p_objXmlIn.SelectSingleNode("GetDiaryDom");

                        //Incase of Peek , value of user attribute will we blank ,
                        //so adding user name = current subordinate name,to fetch dairies of the same user
                        if (dairyDomNode != null)
                        {
                            dairyDomNode.Attributes["user"].Value = drow["LOGIN_NAME"].ToString();
                        }

                        outPutDoc = new XmlDocument();

                        GetDiaryDomForDiaryCalender(p_objXmlIn, ref  outPutDoc, ref  p_objErr);

                        //outPutDairies  = p_objXmlOut.CreateElement("DairyList");

                        peekUser.InnerXml = outPutDoc.InnerXml;

                        //peekUser.AppendChild(outPutDairies);

                        peekNode.AppendChild(peekUser);
                    }
                }
                p_objXmlOut.AppendChild(peekNode);

            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
               //rsharma220 MITS 31099 -start
                throw new ApplicationException(p_objException.Message);
                //return false;    
                //rsharma220 MITS 31099 -End
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.GetDiaryDom.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objCustList = null;
                dset = null;
            }
            return (true);

        }

        /// <summary>
        /// Created by Nitin
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErr"></param>
        /// <returns></returns>
        private bool GetDairiesForProcessingOffice(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            CustomizeUserList objCustList = null;
            DataSet dset = null;
            XmlNode dairyDomNode = null;
            XmlElement peekNode = null;
            XmlElement peekUser = null;
            XmlAttribute userLoginName = null;
            XmlAttribute userLastName = null;
            XmlAttribute userFirstName = null;
            XmlDocument outPutDoc = null;

            try
            {
                objCustList = new CustomizeUserList(connectionString, userLogin.objRiskmasterDatabase.DataSourceId.ToString(),
                    securityConnectionString, userLogin.UserId,base.ClientId);

                dset = objCustList.GetUserListForDiaryCalendar("processingoffice");

                peekNode = p_objXmlOut.CreateElement("PeekProcessingOffice");

                if (dset == null)
                {
                    peekNode.InnerText = "0";
                    p_objXmlOut.AppendChild(peekNode);

                    return false;
                }
                foreach (DataRow drow in dset.Tables[0].Rows)
                {
                    peekUser = p_objXmlOut.CreateElement("User");

                    userLoginName = p_objXmlOut.CreateAttribute("LoginName");
                    userLoginName.Value = drow["LOGIN_NAME"].ToString();

                    userLastName = p_objXmlOut.CreateAttribute("LastName");
                    userLastName.Value = drow["LAST_NAME"].ToString();

                    userFirstName = p_objXmlOut.CreateAttribute("FirstName");
                    userFirstName.Value = drow["FIRST_NAME"].ToString();


                    peekUser.Attributes.Append(userLoginName);

                    peekUser.Attributes.Append(userLastName);
                    peekUser.Attributes.Append(userFirstName);

                    dairyDomNode = p_objXmlIn.SelectSingleNode("GetDiaryDom");

                    //Incase of Peek , value of user attribute will we blank ,
                    //so adding user name = current subordinate name,to fetch dairies of the same user
                    if (dairyDomNode != null)
                    {
                        dairyDomNode.Attributes["user"].Value = drow["LOGIN_NAME"].ToString();
                    }

                    outPutDoc = new XmlDocument();

                    GetDiaryDomForDiaryCalender(p_objXmlIn, ref  outPutDoc, ref  p_objErr);

                    //outPutDairies  = p_objXmlOut.CreateElement("DairyList");

                    peekUser.InnerXml = outPutDoc.InnerXml;

                    //peekUser.AppendChild(outPutDairies);

                    peekNode.AppendChild(peekUser);
                }

                p_objXmlOut.AppendChild(peekNode);

            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.GetDiaryDom.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objCustList = null;
                dset = null;
            }
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErr"></param>
        /// <returns></returns>
        public bool GetDiaryDomForDiaryCalender(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            WPA objWPA = null;
            XmlElement objXmlUser = null;
            int iSortOrder = 0;
            long lRecordCount = 0;
            long lPageCount = 0;
            long lPageNumber = 0;
            string sAttachTable = "";
            string sAttachRecordId = "";
            string sDueDate = "";
            string sFunction = "";
            string sActiveDiaryChecked = ""; //Umesh
            string sAllClaimDetails = string.Empty;
            bool bOpen = false;
            bool bReqActDiary = false;//Umesh
            string sAssignedUser = "";
            //MITS 11998, Abhishek
            string sShowAllDiariesForActiveRec = "no";
            string navigationMonth = string.Empty;
            string navigationYear = string.Empty;
            string currentScheduleControl = string.Empty;
            //added by Nitin
            string weekViewFromDate = string.Empty;
            string weekViewToDate = string.Empty;
            string currentActiveDate = string.Empty;
            int iGroupid=0;
            bool isGroupId=false;


            try
            {
                objXmlUser = (XmlElement)p_objXmlIn.SelectSingleNode("//GetDiaryDom");
                iSortOrder = Conversion.ConvertStrToInteger(objXmlUser.GetAttribute("sortorder"));
                lRecordCount = Conversion.ConvertStrToLong(objXmlUser.GetAttribute("recordcount"));
                lPageCount = Conversion.ConvertStrToLong(objXmlUser.GetAttribute("pagecount"));
                lPageNumber = Conversion.ConvertStrToLong(objXmlUser.GetAttribute("pagenumber"));
                bReqActDiary = Conversion.ConvertStrToBool(objXmlUser.GetAttribute("reqactdiary"));//REM Umesh
                sActiveDiaryChecked = objXmlUser.GetAttribute("activediarychecked");// Rem Umesh
                string showNotes = objXmlUser.GetAttribute("shownotes");  //tkr
                sAttachTable = objXmlUser.GetAttribute("attachtable");
                sAttachRecordId = objXmlUser.GetAttribute("attachrecordid");
                sFunction = objXmlUser.GetAttribute("diaryfunction");
                sDueDate = objXmlUser.GetAttribute("duedate");
                bOpen = Conversion.ConvertStrToBool(objXmlUser.GetAttribute("open"));
                sAssignedUser = objXmlUser.GetAttribute("user");
                sAllClaimDetails = objXmlUser.GetAttribute("showallclaimdiaries");



                currentScheduleControl = objXmlUser.GetAttribute("currentschedulecontrol");

                if (currentScheduleControl == "webmonthview")
                {
                    navigationMonth = objXmlUser.GetAttribute("navigationmonth");
                    navigationYear = objXmlUser.GetAttribute("navigationyear");
                }
                else if (currentScheduleControl == "webweekview")
                {
                    weekViewFromDate = objXmlUser.GetAttribute("fromdate");
                    weekViewToDate = objXmlUser.GetAttribute("todate");
                }
                else
                {
                    currentActiveDate = objXmlUser.GetAttribute("currentactivedate");
                }

                //MITS 11998 Abhishek start
                if (sAttachRecordId.Trim() != "")
                {
                    sShowAllDiariesForActiveRec = objXmlUser.GetAttribute("showalldiariesforactiverecord");
                }
                if(!string.IsNullOrEmpty(sAssignedUser))
                    isGroupId = int.TryParse(sAssignedUser, out iGroupid);
                //MITS 11998 Abhishek end
                if (sAssignedUser == "")
                    sAssignedUser = loginName;
                objWPA = new WPA(connectionString, base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.Password, base.m_loginName, base.ClientId);
                objWPA.PageSize = Conversion.ConvertStrToLong(objXmlUser.GetAttribute("pagesize"));
                objWPA.UserId = userID;
                if (isGroupId)
                {
                    objWPA.GroupId = iGroupid;
                    sAssignedUser = "";
                }
                //Aman ML Change               
                objWPA.LanguageCode = base.userLogin.objUser.NlsCode; //Language code of the user logged in
                p_objXmlOut = objWPA.GetDiaryDomForDiaryCalender(iSortOrder, sAssignedUser, sDueDate, lRecordCount, lPageCount, lPageNumber, bOpen, bReqActDiary, sAttachTable, sAttachRecordId, sFunction, sActiveDiaryChecked, showNotes, sAllClaimDetails, sShowAllDiariesForActiveRec, navigationMonth, navigationYear, currentScheduleControl, currentActiveDate
                        , weekViewFromDate, weekViewToDate);

                // Check for security permisions for 'Go' - Mihika
                if (!userLogin.IsAllowedEx(RMB_DIARY, RMO_DIARY_GO))
                {
                    XmlElement objTempElm = p_objXmlOut.CreateElement("GoNotAllowed");
                    ((XmlElement)p_objXmlOut.SelectSingleNode("//diaries")).AppendChild(objTempElm);
                    objTempElm = null;
                }
            }
            catch (RMAppException p_objException)
            {
                p_objErr.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("WPAAdaptor.GetDiaryDom.Error", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                objWPA = null;
                objXmlUser = null;
            }
            return (true);
        }

        //Code merged by Nitin from Safeway to RMX R6 release on 22-July-2009 starts

        //SAFEWAY: paggarwal2: 04/11/2008: Added for Diary calendar end
        /// <summary>
        /// Author		: paggarwal2
        /// Date Created: 11/08/2008
        /// Sets the configuration for the diaries
        /// SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Header Config
        /// </summary>
        /// <param name="p_objXmlIn">XmlDocument</param>
        /// <param name="p_objXmlOut">XmlDocument</param>
        /// <param name="p_objErr">BusinessAdaptorErrors</param>
        /// <returns>bool</returns>
        public bool SetDiaryConfig(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            WPA objWPA = null;

            //objWPA = new WPA(connectionString, m_userLogin.objRiskmasterDatabase.DataSourceName,m_userLogin.Password, m_userLogin.LoginName);
            objWPA = new WPA(connectionString, m_userLogin.objRiskmasterDatabase.DataSourceName, m_userLogin.Password, m_userLogin.LoginName, base.ClientId);
            objWPA.UserId = userID;

            objWPA.SetDiaryConfig(p_objXmlIn);
            return true;
        }
        /// <summary>
        /// Author		: paggarwal2
        /// Date Created: 11/08/2008
        /// Get the configuration settings for the diaries
        /// SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Header Config
        /// </summary>
        /// <param name="p_objXmlIn">XmlDocument</param>
        /// <param name="p_objXmlOut">XmlDocument</param>
        /// <param name="p_objErr">BusinessAdaptorErrors</param>
        /// <returns>bool</returns>
        public bool GetDiaryConfig(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            WPA objWPA = null;

            //objWPA = new WPA(connectionString, m_userLogin.objRiskmasterDatabase.DataSourceName,m_userLogin.Password, m_userLogin.LoginName);
            objWPA = new WPA(connectionString, m_userLogin.objRiskmasterDatabase.DataSourceName, m_userLogin.Password, m_userLogin.LoginName, base.ClientId);
            objWPA.UserId = userID;
            p_objXmlOut = objWPA.GetDiaryConfig(userID);
            return true;
        }

        //Code merged by Nitin from Safeway to RMX R6 release on 22-July-2009 ends

        #endregion

	}
}
