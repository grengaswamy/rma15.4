using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.PatriotProtectorWrapper;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Security;


namespace Riskmaster.BusinessAdaptor
{
	///************************************************************** 
	///* $File		: PatriotProtectorWrapperAdaptor.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 01-July-2005
	///* $Author	: Raman Bhatia
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************

	/// <summary>
	/// This class is used to call the application layer component for Patriot Protector Wrapper that 
	///	is is a Wrapper Class for .NET Patriot Protector Class.	
	/// </summary>
	public class PatriotProtectorWrapperAdaptor : BusinessAdaptorBase 
	{
		#region Constructor 
		/// Name		: PatriotProtectorWrapperAdaptor
		/// Author		: Raman Bhatia
		/// Date Created: 21-Feb-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		///	<summary>
		/// Constructor for the Class
		/// </summary>
		public PatriotProtectorWrapperAdaptor()
		{

		}

		#endregion

		#region Public Methods
		
		/// Name		: AdvertiseOnly
		/// Author		: Raman Bhatia
		/// Date Created: 01-July-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to PatriotProtectorWrapper.AdvertiseOnly method.
		///		This method will call the methods which verify whether License File Exists. 		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document />
		///	</param>
		/// <param name="p_objXmlOut">XML contains the output document.
		///		The structure of output XML document would be:
		///		<Document>
		///			<AdvertiseOnly>true/false</AdvertiseOnly>
		///		</Document>
		/// </param>
		/// 
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool AdvertiseOnly(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			PatriotProtectorWrapper objPatriotProtectorWrapper = null;		//object of PatriotProtectorWrapper Class
			XmlElement objParentElement = null;					            //used for creating the Output Xml
			bool bAdvertiseOnly = true;
			
			try
			{
				objPatriotProtectorWrapper = new PatriotProtectorWrapper(base.userLogin.objRiskmasterDatabase.DataSourceName , base.userLogin.LoginName, base.userLogin.Password,base.ClientId);//dvatsa-cloud

				//Calling the AdvertiseOnly method to get the output xml document
				bAdvertiseOnly = objPatriotProtectorWrapper.AdvertiseOnly();

				//Creating the output XML containing the User information
				objParentElement = p_objXmlOut.CreateElement("AdvertiseOnly");
				objParentElement.InnerText = bAdvertiseOnly.ToString();
				p_objXmlOut.AppendChild(objParentElement);
				return true;
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.Error);
				return false;
			}
			
			catch (Exception p_objException)
			{
                p_objErrOut.Add(p_objException, Globalization.GetString("PatriotProtectorWrapperAdaptor.AdvertiseOnly.error", base.ClientId), BusinessAdaptorErrorType.Error); //dvatsa-cloud
				return false;				
			}
			finally
			{
				if(objPatriotProtectorWrapper != null)
				{
					objPatriotProtectorWrapper.Dispose();
					objPatriotProtectorWrapper = null;
				}
				objParentElement = null;
			}
		}

		/// Name		: Lookup
		/// Author		: Raman Bhatia
		/// Date Created: 01-July-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///		This method is a wrapper to PatriotProtectorWrapper.Lookup method.
		///		This method will call the method which calls the .Net Patriotprotector Class		
		/// </summary>
		/// <param name="p_objXmlIn">XML contains the input parameters needed.
		///		
		///		The structure of input XML document would be:
		///		<Document>
		///			<LastName>Last Name</LastName>
		///			<FirstName>First Name</FirstName>
		///		</Document>
		///			
		///	</param>
		/// <param name="p_objXmlOut">XML contains the output document.
		///		The structure of output XML document would be:
		///		<Document>
		///			<PATRIOTPROTECTOR LASTNAME FIRSTNAME>Results</PATRIOTPROTECTOR>
		///		</Document>
		///		If the License is not present then the output XML document would be: 
		///		<Document>
		///			<AdvertiseOnly>true/false</AdvertiseOnly>
		///		</Document>
		/// </param>
		/// 
		/// <param name="p_objErrOut">Collection of Error Messages</param>		
		/// <returns>True/False for success or failure of the function</returns>	
		public bool Lookup(XmlDocument p_objXmlIn,ref XmlDocument p_objXmlOut,ref BusinessAdaptorErrors p_objErrOut)
		{
			PatriotProtectorWrapper objPatriotProtectorWrapper = null;		//object of PatriotProtectorWrapper Class
			XmlElement objTargetElement = null;					            //used for creating the Output Xml
			XmlElement objParentElement = null;					            //used for creating the Output Xml
			string sLastName = "";
			string sFirstName = "";
			string sResults = "";
			bool bAdvertiseOnly = true;
			
			try
			{
				objPatriotProtectorWrapper = new PatriotProtectorWrapper(base.userLogin.objRiskmasterDatabase.DataSourceName , base.userLogin.LoginName, base.userLogin.Password,base.ClientId);//dvatsa-cloud
				
				//Verifying the License Information
				bAdvertiseOnly = objPatriotProtectorWrapper.AdvertiseOnly();
				if(bAdvertiseOnly)
				{
					//Creating the output XML containing the User information
					objParentElement = p_objXmlOut.CreateElement("AdvertiseOnly");
					objParentElement.InnerText = bAdvertiseOnly.ToString();
					p_objXmlOut.AppendChild(objParentElement);
									
					return true;
				}
				else
				{
					objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "LastName");
					sLastName = objTargetElement.InnerText;

					objTargetElement = (XmlElement)p_objXmlIn.SelectSingleNode("//" + "FirstName");
					sFirstName = objTargetElement.InnerText;


					//Calling the Lookup method to get the output xml document
					sResults = objPatriotProtectorWrapper.Lookup(sLastName , sFirstName);
				
					//Creating the output XML containing the User information
					objParentElement = p_objXmlOut.CreateElement("PATRIOTPROTECTOR");
					objParentElement.SetAttribute("LASTNAME" , sLastName);
					objParentElement.SetAttribute("FIRSTNAME" , sFirstName);
					objParentElement.InnerXml = sResults;
					p_objXmlOut.AppendChild(objParentElement);
					return true;
				}
			}
			catch (RMAppException p_objException)
			{
				p_objErrOut.Add(p_objException , BusinessAdaptorErrorType.Error);
				return false;
			}
			
			catch (Exception p_objException)
			{
				p_objErrOut.Add(p_objException ,Globalization.GetString("PatriotProtectorWrapperAdaptor.Lookup.error",base.ClientId) , BusinessAdaptorErrorType.Error); //dvatsa-cloud
				return false;				
			}
			finally
			{
				if(objPatriotProtectorWrapper != null)
				{
					objPatriotProtectorWrapper.Dispose();
					objPatriotProtectorWrapper = null;
				}
				objParentElement = null;
				objTargetElement = null;
			}
		}
	

		#endregion



	}
}
