﻿using System;
using System.Xml;
using System.IO;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.MMSEA;



namespace Riskmaster.BusinessAdaptor
{
    public class ClaimantMMSEAAdapter: BusinessAdaptorBase
    {
        #region Variable Declarations

        /// <summary>
        /// Represents the Module variable for ClaimantMMSEA Class.
        /// </summary>
        private ClaimantMMSEA m_objClaimantMMSEA = null;
        private string m_strDBConnectionString = String.Empty;
        private string m_strPassword = String.Empty;
        private string m_strLoginName = String.Empty;
        private string m_strDSN = String.Empty;
        # endregion


        private void Initialize()
        {
            m_strDBConnectionString = base.connectionString;
            m_strPassword = base.userLogin.Password;
            m_strLoginName = base.loginName;
            m_strDSN = base.userLogin.objRiskmasterDatabase.DataSourceName;
        }//method: Initialize()
        
		#region Default Constructor
		
        public ClaimantMMSEAAdapter()
		{}

        public ClaimantMMSEAAdapter(string strLoginName, string strPassword, string strDSN)
        {
            this.m_strLoginName = strLoginName;
            this.m_strPassword = strPassword;
            this.m_strDSN = strDSN;
        }
	
		#endregion

        #region Public Methods

        public bool SaveClaimantMMSEAData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Initialize();
            //Element used for parsing the input xml
            m_objClaimantMMSEA = new ClaimantMMSEA(m_strDSN, m_strLoginName, m_strPassword, base.ClientId);
            
            try
            {//skhare7 MITS 18069 
                return m_objClaimantMMSEA.SaveClaimantMMSEAData(p_objXmlIn,base.userLogin);
                //skhare7 MITS 18069 end
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, "ClaimantMMSEAAdapter.SaveClaimantMMSEAData.Error",
                    BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
            }

        }

        public bool GetClaimantRowIdIfExist(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlNode objNode = null;

            try
            {
                objNode = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimantEid");
                int ClaimantEid = Conversion.ConvertStrToInteger(objNode.InnerXml);

                objNode = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimId");
                int ClaimId = Conversion.ConvertStrToInteger(objNode.InnerXml);

                Initialize();
                //Element used for parsing the input xml
                m_objClaimantMMSEA = new ClaimantMMSEA(m_strDSN, m_strLoginName, m_strPassword, base.ClientId);

                p_objXmlOut = m_objClaimantMMSEA.GetClaimantRowIdIfExist(ClaimantEid, ClaimId);

                return true;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, "ClaimantMMSEAAdapter.GetClaimantMMSEAData.Error",
                    BusinessAdaptorErrorType.Error);
                return false;
            }
            
        }

        public bool GetClaimantMMSEADummyData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
          
            XmlNode objNode = null;
            XmlNode objXnode = null;
            XmlNode objNodeToDelete = null;
            string sIdToDelete = "";
            objNode = (XmlElement)p_objXmlIn.SelectSingleNode("//SelectedId");
            sIdToDelete = objNode.InnerXml;

            string[] strSplit = sIdToDelete.Split(new Char[] { '|' });
            string sGridToDelete = strSplit[1];
            sIdToDelete = strSplit[0];

            XmlNodeList objNodeCollection = null;
            try
            {
                //---------------------TPOC
                if (sGridToDelete == "TPOC")
                {
                    objNodeCollection = p_objXmlIn.SelectNodes("//TPOC/option");
                    foreach (XmlNode objXmlNode in objNodeCollection)
                    {
                        objXnode = objXmlNode.SelectSingleNode("TpocRowID");

                        if (objXnode.InnerXml == sIdToDelete)
                        {
                            objNodeToDelete = objXmlNode;
                            break;
                        }

                    }
                    if (objNodeToDelete != null)
                    {
                        objXnode = p_objXmlIn.SelectSingleNode("//TPOC");
                        objXnode.RemoveChild(objNodeToDelete);
                        objNodeToDelete = null;
                    }
                }

                XmlNode objListOption = p_objXmlIn.CreateElement("option");
                XmlAttribute objXmlAttribute = p_objXmlIn.CreateAttribute("type");
                objXmlAttribute.InnerText = "new";
                objListOption.Attributes.Append(objXmlAttribute);

                string sNode = "<DelayedBeyongTPOCStartDate></DelayedBeyongTPOCStartDate>";
                sNode = sNode + "<PaymentObligationAmount></PaymentObligationAmount>";
                sNode = sNode + "<DateofWrittenAgreement></DateofWrittenAgreement>";
                sNode = sNode + "<DateofCourtApproval></DateofCourtApproval>";
                sNode = sNode + "<DateofPaymentIssue></DateofPaymentIssue>";
                sNode = sNode + "<DeletedFlag></DeletedFlag>";
                sNode = sNode + "<FilMdcreRcdDATE></FilMdcreRcdDATE>";
                sNode = sNode + "<TpocEditFlag></TpocEditFlag>";
                sNode = sNode + "<ClaimantRowID></ClaimantRowID>";
                sNode = sNode + "<TpocRowID></TpocRowID>";
                sNode = sNode + "<DTTM_RCD_LAST_UPD></DTTM_RCD_LAST_UPD>";
                sNode = sNode + "<AddedByUser></AddedByUser>";
                sNode = sNode + "<UpdatedByUser></UpdatedByUser>";
                sNode = sNode + "<DttmRcdAdded></DttmRcdAdded>";

                objListOption.InnerXml = sNode;

                objXnode = p_objXmlIn.SelectSingleNode("//TPOC");
                objXnode.AppendChild(objListOption);
                //--------------------------End TPOC

                //-------------------------------Parties to Claim

                if (sGridToDelete == "Party")
                {
                    objNodeCollection = p_objXmlIn.SelectNodes("//MMSEAParty/option");
                    foreach (XmlNode objXmlNode in objNodeCollection)
                    {
                        objXnode = objXmlNode.SelectSingleNode("MMSEAClaimantRowID");

                        if (objXnode.InnerXml == sIdToDelete)
                        {
                            objNodeToDelete = objXmlNode;
                            break;
                        }

                    }
                    foreach (XmlNode objXmlNode in objNodeCollection)
                    {
                        objXnode = objXmlNode.SelectSingleNode("att1lastfirstname");
                        objXnode.InnerXml = objXnode.InnerXml.Replace("&amp;","&"); //ampersand in PartyName
                        objXnode.InnerXml = objXnode.InnerXml.Replace("&gt;", ">");
                        objXnode.InnerXml = objXnode.InnerXml.Replace("&lt;", "<");
                    }
                    foreach (XmlNode objXmlNode in objNodeCollection)
                    {
                        objXnode = objXmlNode.SelectSingleNode("attlastfirstname");
                        objXnode.InnerXml = objXnode.InnerXml.Replace("&amp;", "&"); //ampersand in PartyToClaimRepresentative
                        objXnode.InnerXml = objXnode.InnerXml.Replace("&gt;", ">");
                        objXnode.InnerXml = objXnode.InnerXml.Replace("&lt;", "<");
                    }
                    if (objNodeToDelete != null)
                    {
                        objXnode = p_objXmlIn.SelectSingleNode("//MMSEAParty");
                        objXnode.RemoveChild(objNodeToDelete);
                        objNodeToDelete = null;
                    }
                }

                objListOption = p_objXmlIn.CreateElement("option");
                objXmlAttribute = p_objXmlIn.CreateAttribute("type");
                objXmlAttribute.InnerText = "new";
                objListOption.Attributes.Append(objXmlAttribute);

                sNode = sNode + "<att1lastfirstname codeid='-1'></att1lastfirstname>";
                sNode = sNode + "<RelationToBenificiary codeid='-1'></RelationToBenificiary>";
                sNode = sNode + "<TypeOfRepresentative codeid='-1'></TypeOfRepresentative>";
                sNode = sNode + "<attlastfirstname codeid='-1'></attlastfirstname>";
                sNode = sNode + "<ClaimantRowID></ClaimantRowID>";
                sNode = sNode + "<MMSEAClaimantRowID></MMSEAClaimantRowID>";
                sNode = sNode + "<DttmRcdAdded></DttmRcdAdded>";
                sNode = sNode + "<DttmRcdLastUpd></DttmRcdLastUpd>";
                sNode = sNode + "<AddedByUser></AddedByUser>";
                sNode = sNode + "<UpdatedByUser></UpdatedByUser>";
                objListOption.InnerXml = sNode;

                objXnode = p_objXmlIn.SelectSingleNode("//MMSEAParty");
                objXnode.AppendChild(objListOption);

                //-------------------------------End Parties

                //-------------------------------Add Diagnosis
                XmlNode objXNode = p_objXmlIn.SelectSingleNode("//CSCDiagnosisList");
                objNodeCollection = p_objXmlIn.SelectNodes("//CSCDiagnosisList/DiagnosisList/Item");
                string sShortCode = string.Empty;
                string sCodeDes = string.Empty;
                sNode = string.Empty;

                Initialize();

                m_objClaimantMMSEA = new ClaimantMMSEA(m_strDSN, m_strLoginName, m_strPassword,base.ClientId);//sonali jira 880

                foreach (XmlNode objXmlNode in objNodeCollection)
                {
                    //objXnode = objXmlNode.SelectSingleNode("Item");
                    //if (objXnode != null)
                    //{
                    if (objXmlNode.Attributes["codeid"].Value != "")
                        {
                            m_objClaimantMMSEA.GetCode(Int32.Parse(objXmlNode.Attributes["codeid"].Value), ref sShortCode, ref sCodeDes);
                            sNode = sNode + "<Item value='" + objXmlNode.Attributes["codeid"].Value + "' codeid='" + objXmlNode.Attributes["codeid"].Value + "'><![CDATA[" + sShortCode + " " + sCodeDes + "]]></Item>";
                        }
                    //}
                }
                sNode = "<DiagnosisList  type='codelist'>" + sNode + "</DiagnosisList>";
                objXNode.InnerXml = sNode;
                //-------------------------------End Add Diagnosis

                //prashbiharis ICD10 Changes MITS: 32423

                //-------------------------------Add Diagnosis ICD10
                XmlNode objXNode1 = p_objXmlIn.SelectSingleNode("//CSCICD10DiagnosisList");
                objNodeCollection = p_objXmlIn.SelectNodes("//CSCICD10DiagnosisList/DiagnosisList/Item");
                sShortCode = string.Empty;
                sCodeDes = string.Empty;
                sNode = string.Empty;

                Initialize();

                m_objClaimantMMSEA = new ClaimantMMSEA(m_strDSN, m_strLoginName, m_strPassword, base.ClientId);

                foreach (XmlNode objXmlNode in objNodeCollection)
                {
                    //objXnode = objXmlNode.SelectSingleNode("Item");
                    //if (objXnode != null)
                    //{
                    if (objXmlNode.Attributes["codeid"].Value != "")
                    {
                        m_objClaimantMMSEA.GetCode(Int32.Parse(objXmlNode.Attributes["codeid"].Value), ref sShortCode, ref sCodeDes);
                        sNode = sNode + "<Item value='" + objXmlNode.Attributes["codeid"].Value + "' codeid='" + objXmlNode.Attributes["codeid"].Value + "'><![CDATA[" + sShortCode + " " + sCodeDes + "]]></Item>";
                    }
                    //}
                }
                sNode = "<DiagnosisList  type='codelist'>" + sNode + "</DiagnosisList>";
                objXNode1.InnerXml = sNode;
                //-------------------------------End Add Diagnosis ICD 10
               //prashbiharis ICD10 Changes MITS: 32423

            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, "ClaimantMMSEAAdapter.GetClaimantMMSEADummyData.Error",
                    BusinessAdaptorErrorType.Error);
                return false;
            }
           
            p_objXmlOut = p_objXmlIn;
            return true;
        }



        public bool GetClaimantMMSEAData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            Initialize();
            //Element used for parsing the input xml
            m_objClaimantMMSEA = new ClaimantMMSEA(m_strDSN, m_strLoginName, m_strPassword, base.ClientId);
            int intClaimantRowId = 0;

            XmlNode objNode = null;
             
            objNode = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimantRowID");
            intClaimantRowId = Conversion.ConvertStrToInteger(objNode.InnerXml);
			//rkulavil - ML Changes - MITS 34107 -start
            string sLangCode = string.Empty;
            string sPageId = string.Empty;
            objNode = (XmlElement)p_objXmlIn.SelectSingleNode("//LangCodeID");
            sLangCode = objNode.InnerXml;

            objNode = (XmlElement)p_objXmlIn.SelectSingleNode("//PageID");
            sPageId = objNode.InnerXml;
			
			//rkulavil - ML Changes - MITS 34107 -end
            try
            {
                //Asharma326 25824 Check permissions Start Add User Object
                p_objXmlOut = m_objClaimantMMSEA.GetClaimantMMSEAData(intClaimantRowId, base.userLogin,sPageId, sLangCode);  //rkulavil - ML Changes - MITS 34107
                //Asharma326 25824 Check permissions Ends
                
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, "ClaimantMMSEAAdapter.GetClaimantMMSEAData.Error",
                    BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
            }
        }//end GetBillingCodes


        public bool GetData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            try
            {
                Initialize();
                m_objClaimantMMSEA = new ClaimantMMSEA(m_strDSN, m_strLoginName, m_strPassword, base.ClientId);
                p_objXmlOut = m_objClaimantMMSEA.GetData(p_objXmlIn);
                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, "ClaimantMMSEAAdapter.GetData.Error",
                    BusinessAdaptorErrorType.Error);
                return false;
            }


        }





        #endregion
    }
}
