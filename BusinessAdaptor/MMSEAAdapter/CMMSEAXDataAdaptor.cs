﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.MMSEA;
using System.Xml;
using Riskmaster.Common;

namespace Riskmaster.BusinessAdaptor
{
    class CMMSEAXDataAdaptor:BusinessAdaptorBase
    {
        #region Variable Declarations

        /// <summary>
        /// Represents the Module variable for CMMSEAXDATA Class.
        /// </summary>
        private CMMSEAXDATA m_objCMMSEAXDATA = null;
        private string m_strDBConnectionString = String.Empty;
        private string m_strPassword = String.Empty;
        private string m_strLoginName = String.Empty;
        private string m_strDSN = String.Empty;
        # endregion


        private void Initialize()
        {
            m_strDBConnectionString = base.connectionString;
            m_strPassword = base.userLogin.Password;
            m_strLoginName = base.loginName;
            m_strDSN = base.userLogin.objRiskmasterDatabase.DataSourceName;
        }//method: Initialize()
        
		#region Default Constructor
		
        public CMMSEAXDataAdaptor()
		{}

        public CMMSEAXDataAdaptor(string strLoginName, string strPassword, string strDSN)
        {
            this.m_strLoginName = strLoginName;
            this.m_strPassword = strPassword;
            this.m_strDSN = strDSN;
        }
	
		#endregion
        #region Public Methods


        public bool GetMMSEAXData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            int iRowId = 0;
            XmlElement objElement = null;

            try
            {
                Initialize();

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//EntMMSEARowID");
                if (objElement == null)
                {
                    p_objErrOut.Add("MMSEAParameterMissing","MMSEAParameterMissing", BusinessAdaptorErrorType.Error);
                    return false;
                }
                iRowId = Conversion.ConvertStrToInteger(objElement.InnerText);
                
                m_objCMMSEAXDATA = new CMMSEAXDATA(m_strDSN, m_strLoginName, m_strPassword, base.ClientId);

                p_objXmlOut = m_objCMMSEAXDATA.GetMMSEAXData(iRowId);

                return true;
            }
           catch (XmlOperationException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (DataModelException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, "MMSEA.GetMMSEA.Error", BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                m_objCMMSEAXDATA = null;
                objElement = null;
            }
            
        }
        public bool GetMMSEAXDataList(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            int iEntityId = 0;
            XmlElement objElement = null;

            try
            {
                Initialize();

                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//EntityEID");
                if (objElement == null)
                {
                    p_objErrOut.Add("MMSEAParameterMissing", "MMSEAParameterMissing", BusinessAdaptorErrorType.Error);
                    return false;
                }
                iEntityId = Conversion.ConvertStrToInteger(objElement.InnerText);

                m_objCMMSEAXDATA = new CMMSEAXDATA(m_strDSN, m_strLoginName, m_strPassword, base.ClientId);

                p_objXmlOut = m_objCMMSEAXDATA.GetMMSEAXDataList(iEntityId, m_strDBConnectionString);

                return true;
            }
            catch (XmlOperationException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (DataModelException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, "MMSEA.GetMMSEA.Error", BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                m_objCMMSEAXDATA = null;
                objElement = null;
            }

        }
        public bool DeleteMMSEAXData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objElement = null;
            int iRowId = 0;
            try
            {
                Initialize();
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//EntMMSEARowID");
                if (objElement == null)
                {
                    p_objErrOut.Add("MMSEAParameterMissing","MMSEAParameterMissing", BusinessAdaptorErrorType.Error);
                    return false;
                }
                iRowId = Conversion.ConvertStrToInteger(objElement.InnerText);

                m_objCMMSEAXDATA = new CMMSEAXDATA(m_strDSN, m_strLoginName, m_strPassword, base.ClientId);
                m_objCMMSEAXDATA.DeleteMMSEAXData(iRowId);
                return true;

            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, "MMSEA.DeleteMMSEA.Error", BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                m_objCMMSEAXDATA = null;
            }
        }
        public bool SaveMMSEAXData(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlElement objElement = null;
            string sInputXml = string.Empty;
            try
            {

                Initialize();
                objElement = (XmlElement)p_objXmlIn.SelectSingleNode("//MMSEAXDataDetails");
                if (objElement == null)
                {

                    p_objErrOut.Add("MMSEAParameterMissing","MMSEAParameterMissing", BusinessAdaptorErrorType.Error);
                    return false;


                }
                sInputXml = objElement.InnerXml;

                m_objCMMSEAXDATA = new CMMSEAXDATA(m_strDSN, m_strLoginName, m_strPassword, base.ClientId);


                m_objCMMSEAXDATA.SaveMMSEAXData(sInputXml);

                return true;
            }
            catch (RMAppException p_objException)
            {
                p_objErrOut.Add(p_objException, BusinessAdaptorErrorType.Error);
                return false;
            }
            catch (Exception p_objException)
            {
                p_objErrOut.Add(p_objException, "MMSEA.SaveMMSEA.Error", BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
                m_objCMMSEAXDATA = null;
            }
        }

        #endregion
    }
}
