﻿
/***************************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            
 ***************************************************************************************************
 * 05/05/2014 | 33574   | pgupta93   | Changes for Continuous Trigger and Policy Search functionality.
 ***************************************************************************************************/
using System;
using System.Xml;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.IO;
using Riskmaster.Security;
using System.Xml.Linq;
using Riskmaster.Models;
using Riskmaster.Application.PolicySystemInterface;
using System.Xml.XPath;
using Riskmaster.Settings;
using System.Collections.Generic;
using System.Text;
using Riskmaster.Db;

namespace Riskmaster.BusinessAdaptor
{
    public class ValidatePolicyAdaptor : BusinessAdaptorBase
    {
        public ValidatePolicyAdaptor() { }


        public bool PolicyValidation(PolicyValidateInput objRequest, out PolicyValidateResponse objResponse, ref BusinessAdaptorErrors p_objErrOut)
        {

            ValidatePolicy oValidatePolicy = null;
            // bool bSuccess = false;
            bool bresult = false;
            objResponse = new PolicyValidateResponse();
            string sPolicyNumber = string.Empty;
            DbReader objReader = null;
            bool bPolicyTriggerClaimFlag = false;
            string sData = string.Empty;//skhare7 Point Policy Interface
            string sPolicyLobShortCode = string.Empty;//MITS:33574
            string sClaimStausShortCode = string.Empty;//MITS:33574
            string sParentLobCode = String.Empty;//MITS:33574
            int iLoBCodeId = 0;//MITS:33574
            int iClaimStausID = 0;//MITS:33574
            LocalCache objCache = new LocalCache(connectionString, base.ClientId);//MITS:33574
            try
            {
                oValidatePolicy = new ValidatePolicy(connectionString, base.ClientId);
                //nsachdeva2 - 7/26/2012
                //objReader = oValidatePolicy.PolicyValidation(objRequest.PolicyId.ToString(), Conversion.ToDbDate(Convert.ToDateTime(objRequest.DateOfEvent)), Conversion.ToDbDate(Convert.ToDateTime(objRequest.DateOfClaim)));
                //MITS:33574 START

                if (objRequest.PolicyLobShortcode != null)
                {
                    sPolicyLobShortCode = objRequest.PolicyLobShortcode;
                    sClaimStausShortCode = "O";

                    iClaimStausID = objCache.GetCodeId(sClaimStausShortCode, "CLAIM_STATUS");
                    iLoBCodeId = objCache.GetCodeId(sPolicyLobShortCode, "POLICY_CLAIM_LOB");
                    sParentLobCode = objCache.GetRelatedShortCode(iLoBCodeId);
                    objRequest.PolicyLobCode = iLoBCodeId;
                }
                //MITS:33574 END
                objReader = oValidatePolicy.PolicyValidation(objRequest.PolicyId.ToString(), Conversion.ToDbDate(Convert.ToDateTime(objRequest.DateOfEvent)), Conversion.ToDbDate(Convert.ToDateTime(objRequest.DateOfClaim)), objRequest.CurrencyCode, objRequest.PolicyLobCode.ToString());
                if (objReader.Read())
                {
                    bresult = true;
                    sPolicyNumber = Conversion.ConvertObjToStr(objReader.GetValue("POLICY_NAME"));
                    objResponse.PolicySystemId = Conversion.ConvertObjToInt(objReader.GetValue("POLICY_SYSTEM_ID"), base.ClientId);
                    objResponse.LoBId = iLoBCodeId;//MITS:33574
                    objResponse.ClaimStatusID = iClaimStausID;//MITS:33574
                    objResponse.ParentLobCode = sParentLobCode;//MITS:33574
                }
                else
                {
                    bPolicyTriggerClaimFlag = oValidatePolicy.GetPolicyTriggerClaimFlag(objRequest.PolicyId);
                }
                objResponse.IsPolicyValid = bresult;


                ////----
                if (bresult)
                {
                    //Fixed review comments for MITS- 34260/RMA-4362 :pgupta93: START
                    //if(objResponse.PolicySystemId>0)
                    //sData = Riskmaster.BusinessAdaptor.CommonForm.PointPolicyDataToDisplay(connectionString, objRequest.PolicyId);
                    //else
                    //Fixed review comments for MITS- 34260/RMA-4362 :pgupta93: END
                    sData = sPolicyNumber;
                }
                //  objResponse.PolicyNumber = sPolicyNumber;
                objResponse.PolicyNumber = sData;
                objResponse.IsMultiCurrencyEnable = Conversion.ConvertStrToBool(new SysSettings(connectionString, base.ClientId).UseMultiCurrency.ToString());
                objResponse.TriggerClaimFlag = bPolicyTriggerClaimFlag;

                bresult = true;
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                bresult = false;
            }
            finally
            {
                if (oValidatePolicy != null)
                    oValidatePolicy = null;
                if (objReader != null)
                    objReader.Dispose();
                //MITS:33574
                if (objCache != null)
                    objCache = null;

            }
            return bresult;
        }

    } 
}
