﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA  | Programmer | Description                                       *
 **********************************************************************************************
 * 05/26/2014 | 34278       | abhadouria | Changes req for adding Reinsurance indicator
 * 05/27/2014 | 33574       | pgupta93   | Changes for Continuous Trigger and Policy Search functionality.
 * 06/04/2014 | 33371       | ajohari2   | Changes for TPA Access Policy search
 * 7/24/2014  | RMA-718     | ajohari2   | Changes for TPA Access ON/OFF
 * 11/20/2014 | JIRA-4362   | ngupta73   | Changes for Policy Expansion
 **********************************************************************************************/
using System;
using System.Xml;
using System.Data;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.IO;
using Riskmaster.Security;
using System.Xml.Linq;
using Riskmaster.Models;
using Riskmaster.Application.PolicySystemInterface;
using System.Xml.XPath;
using Riskmaster.Settings;
using Riskmaster.DataModel;
using System.Collections.Generic;
using System.Text;
using System.Xml.XPath;
using System.Linq;
using Riskmaster.Db;
using Riskmaster.Application.RMUtilities;//MITS:33371 ajohari2
using Riskmaster.Settings;//MITS:33371 ajohari2
using System.Collections; //RMA-12047

namespace Riskmaster.BusinessAdaptor
{
    public class PolicySystemInterfaceAdaptor : BusinessAdaptorBase
    {
        enum PolicyFieldsData
        { symbol, policynumber, mastercompany, locationcompany, module, insuredname, issuecode } // ngupta73 : JIRA 4362
        public PolicySystemInterfaceAdaptor() { }

        //Ashish Ahuja: Claims Made Jira 1342
        public bool GetClaimReportedDateType(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bRet = false;
            SysSettings objSettings;
            XmlNode oClaimDateRptType = null;
            try
            {
                objSettings = new SysSettings(connectionString, base.ClientId);

                // ((XmlElement)oPolSearchSettings).SetAttribute("NoRequiredField", objSettings.NoReqFieldsForPolicySearch.ToString());
                oClaimDateRptType = p_objXmlOut.AppendChild(p_objXmlOut.CreateElement("ClaimDateRptType"));
                oClaimDateRptType.InnerText = objSettings.ClaimDateRptType.ToString();

                bRet = true;
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                objSettings = null;
            }
            return bRet;
        }
        public bool GetPolicySystemList(out PolicySystemList oPolicySystemList, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicySystemInterface oPolicySystemInterface = null;
            XmlDocument oXMLPolicySystemList = null;
            bool bresult = false;
            int iPolicyId = 0;
            oPolicySystemList = new PolicySystemList();
            try
            {
                oPolicySystemInterface = new PolicySystemInterface(connectionString, base.ClientId);
                oXMLPolicySystemList = oPolicySystemInterface.GetPolicySystemList(ref iPolicyId);
                oPolicySystemList.ResponseXML = oXMLPolicySystemList.InnerXml;
                bresult = true;
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oXMLPolicySystemList = null;
                bresult = false;
            }
            finally
            {
                if (oPolicySystemInterface != null)
                    oPolicySystemInterface = null;
                if (oXMLPolicySystemList != null)
                    oXMLPolicySystemList = null;
            }
            return bresult;
        }

        public bool GetXml(out DisplayFileData oDisplayFileData, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicySystemInterface oPolicySystemInterface = null;
            XmlDocument oDataList = null;
            bool bresult = false;
            string sMode = string.Empty;
            string sWCUnitId = string.Empty;
            oDisplayFileData = new DisplayFileData();
            string oAcordAutoRequest = string.Empty;
            string oAcordAutoResponse = string.Empty;

            try
            {
                oPolicySystemInterface = new PolicySystemInterface(m_userLogin, base.ClientId);

                oDataList = oPolicySystemInterface.GetXmlData(ref sMode, base.GetSessionObject().SessionId);
                if (oDataList != null)
                    oDisplayFileData.ResponseXML = oDataList.OuterXml;
                oDisplayFileData.Mode = sMode;
                if (string.Equals(sMode, "entity", StringComparison.InvariantCultureIgnoreCase) || (string.Equals(sMode, "unitinterest", StringComparison.InvariantCultureIgnoreCase)))
                {
                    oDisplayFileData.EntityTypelist = oPolicySystemInterface.GetEntityList().OuterXml;
                }
                bresult = true;
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oDataList = null;
                bresult = false;
            }
            finally
            {
                oPolicySystemInterface = null;
            }
            return bresult;

        }
        public bool GetUnitXml(out DisplayFileData oDisplayFileData, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicySystemInterface oPolicySystemInterface = null;
            XmlDocument oDataList = null;
            bool bresult = false;
            oDisplayFileData = new DisplayFileData();
            string oAcordAutoRequest = string.Empty;
            string oAcordAutoResponse = string.Empty;
            XmlNodeList objNodeList = null;
            LocalCache objCache = null;
            try
            {
                oPolicySystemInterface = new PolicySystemInterface(m_userLogin, base.ClientId);

                oDataList = oPolicySystemInterface.GetUnitXmlData(base.GetSessionObject().SessionId);
                if (oDataList != null)
                {
                    objNodeList = oDataList.SelectNodes("//SequenceNumber");
                    if (objNodeList != null)
                    {
                        objCache = new LocalCache(m_connectionString, base.ClientId);
                        if (objNodeList.Count > Conversion.ConvertStrToInteger(objCache.GetPolicyInterfaceConfigSettings("PolicyInterface", "FetchUnitNumber")))
                        { oDisplayFileData.ResponseXML = string.Empty; }
                        else
                            oDisplayFileData.ResponseXML = oDataList.OuterXml;

                        oDisplayFileData.ShowUnitSearch = true;
                    }
                }
                else
                {
                    oDisplayFileData.ResponseXML = string.Empty;
                    oDisplayFileData.ShowUnitSearch = false;
                }
                oDisplayFileData.Mode = "Unit";

                bresult = true;
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oDataList = null;
                bresult = false;
            }
            finally
            {
                oPolicySystemInterface = null;
            }
            return bresult;

        }

        public bool SearchPolicyUnits(UnitSearch oUnitSearch, out UnitListing oUnitListing, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicySystemInterface oPolicySystemInterface = null;
            XmlDocument oDataList = null;
            bool bresult = false;
            oUnitListing = new UnitListing();
            string oAcordAutoRequest = string.Empty;
            string oAcordAutoResponse = string.Empty;
            DataTable dtFilteredData = null;
            DataSet dsFilteredData = null;
            DataSet dSet = null;
            XElement oElement = null;
            // IEnumerable<XElement> oNodeElements = null;
            //string sWhrClause = string.Empty;

           
            try
            {
                oPolicySystemInterface = new PolicySystemInterface(m_userLogin, base.ClientId);

               oDataList = oPolicySystemInterface.GetUnitXmlData(base.GetSessionObject().SessionId);
               

                if (oDataList != null)
                {
                    oElement = XElement.Parse(oDataList.OuterXml);

                    string sFilterCriteria = string.Empty;
                    try
                    {
                        dSet = new DataSet();
                        dSet.ReadXml(new XmlNodeReader(oDataList));
                        //return dSet;
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException(ex.Message);
                    }

                    //var query = oElement.XPathSelectElements("./" + "Instance/Unit");
                    if (!string.IsNullOrEmpty(oUnitSearch.UnitType))
                    {

                        //query = query.Where(a => a.XPathSelectElement("./UnitType").Value.ToUpper() == oUnitSearch.UnitType.ToUpper());
                        sFilterCriteria = (sFilterCriteria == string.Empty ? "" : sFilterCriteria + " and ") + "UnitType = '" + oUnitSearch.UnitType.ToUpper() + "'";
                    }
                    if (!string.IsNullOrEmpty(oUnitSearch.City))
                    {
                        //query = query.Where(a1 => a1.XPathSelectElement("./City").Value.ToUpper() == oUnitSearch.City.ToUpper());
                        sFilterCriteria = (sFilterCriteria == string.Empty ? "" : sFilterCriteria + " and ") + "City = '" + oUnitSearch.City.ToUpper() + "'";
                    }
                    if (!string.IsNullOrEmpty(oUnitSearch.Status))
                    {
                        //query = query.Where(a2 => a2.XPathSelectElement("./Status").Value.ToUpper() == oUnitSearch.Status.ToUpper());
                        sFilterCriteria = (sFilterCriteria == string.Empty ? "" : sFilterCriteria + " and ") + "Status = '" + oUnitSearch.Status.ToUpper() + "'";
                    }
                    if (!string.IsNullOrEmpty(oUnitSearch.UnitDescription))
                    {
                        //query = query.Where(a3 => a3.XPathSelectElement("./Desc").Value.ToUpper() == oUnitSearch.UnitDescription.ToUpper());
                        sFilterCriteria = (sFilterCriteria == string.Empty ? "" : sFilterCriteria + " and ") + "Desc like '" + oUnitSearch.UnitDescription.ToUpper() + "'";
                    }
                    if (!string.IsNullOrEmpty(oUnitSearch.UnitNumber))
                    {
                        //query = query.Where(a4 => a4.XPathSelectElement("./UnitNo").Value.ToUpper() == oUnitSearch.UnitNumber.ToUpper());
                        sFilterCriteria = (sFilterCriteria == string.Empty ? "" : sFilterCriteria + " and ") + "StatUnitNo = '" + oUnitSearch.UnitNumber.ToUpper() + "'";
                    }
                    if (!string.IsNullOrEmpty(oUnitSearch.State))
                    {
                        //query = query.Where(a5 => a5.XPathSelectElement("./State").Value.ToUpper() == oUnitSearch.State.ToUpper());
                        sFilterCriteria = (sFilterCriteria == string.Empty ? "" : sFilterCriteria + " and ") + "State = '" + oUnitSearch.State.ToUpper() + "'";
                    }
                    if (!string.IsNullOrEmpty(oUnitSearch.VehicleID))
                    {
                        //query = query.Where(a6 => a6.XPathSelectElement("./Vin").Value.ToUpper() == oUnitSearch.VehicleID.ToUpper());
                        sFilterCriteria = (sFilterCriteria == string.Empty ? "" : sFilterCriteria + " and ") + "Vin = '" + oUnitSearch.VehicleID.ToUpper() + "'";
                    }
                    string sFilterCriteria1 = string.Empty;
                    if (!string.IsNullOrEmpty(oUnitSearch.SelectedValue))
                    {

                        foreach (string sSequenceNumber in oUnitSearch.SelectedValue.Split(','))
                        {
                            //query = query.Where(a7 => a7.XPathSelectElement("./SequenceNumber").Value == sSequenceNumber);
                            sFilterCriteria1 = (sFilterCriteria1 == string.Empty ? "" : sFilterCriteria1 + " or ") + "SequenceNumber = '" + sSequenceNumber + "'";
                        }
                    }

                    //oNodeElements = query.ToList();
                    string sFilterCriteria2 = string.Empty;
                    if (string.IsNullOrEmpty(sFilterCriteria) == false)
                    {
                        sFilterCriteria2 = sFilterCriteria;
                        //dSet.Tables[1].DefaultView.RowFilter = (sFilterCriteria == string.Empty ? "" : sFilterCriteria + " and ") + "(" + (sFilterCriteria1==string.Empty? "": " and (" +sFilterCriteria1 + ")");
                    }
                    if (string.IsNullOrEmpty(sFilterCriteria1) == false)
                    {
                        sFilterCriteria2 = (sFilterCriteria == string.Empty ? sFilterCriteria1 : sFilterCriteria + " and (" + sFilterCriteria1 + ")");
                    }
                    //tanwar2 - Policy Download from staging - start
                    if (dSet.Tables.Count < 2)
                    {
                        oUnitListing.ResponseXML = string.Empty;
                        oUnitListing.ShowUnitSearch = false;
                    }
                    else
                    {
                        //tanwar2 - Policy Download from staging - end
                        dSet.Tables[1].DefaultView.RowFilter = sFilterCriteria2;
                        dtFilteredData = dSet.Tables[1].DefaultView.ToTable();
                        dtFilteredData.TableName = "UnitFiltered";
                        dsFilteredData = new DataSet("InstanceFiltered");
                        dsFilteredData.Tables.Add(dtFilteredData);
                        oElement = XElement.Parse(dsFilteredData.GetXml());
                        var query = oElement.XPathSelectElements("./" + "UnitFiltered");
                        oUnitListing.UnitList = query.ToList();
                    }
                }
                else
                {
                    oUnitListing.ResponseXML = string.Empty;
                    oUnitListing.ShowUnitSearch = false;
                }
                oUnitListing.Mode = "Unit";

                bresult = true;
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oDataList = null;
                bresult = false;
            }
            finally
            {
                oPolicySystemInterface = null;
                oElement = null;
                oDataList = null;
                dtFilteredData = null;
                dsFilteredData = null;
                dSet = null;
            }
            return bresult;

        }
        public bool DeleteOldFiles(string sMode, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicySystemInterface oPolicySystemInterface = null;
            bool bResult = false;
            try
            {
                oPolicySystemInterface = new PolicySystemInterface(m_userLogin, base.ClientId);
                oPolicySystemInterface.DeleteOldFiles(sMode, base.GetSessionObject().SessionId);
                bResult = true;
            }
            catch (Exception e)
            {
                bResult = false;
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                oPolicySystemInterface = null;
            }
            return bResult;
        }

        public bool GetDuplicateOptions(SaveDownloadOptions oSaveDownloadOptions, out DisplayFileData oDisplayFileData, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicySystemInterface oPolicySystemInterface = null;
            XmlDocument oDataList = null;
            bool bresult = false;
            string sMode = string.Empty;
            oDisplayFileData = new DisplayFileData();
            try
            {
                oPolicySystemInterface = new PolicySystemInterface(m_userLogin, base.ClientId);
                oDataList = oPolicySystemInterface.GetDuplicateOptions(oSaveDownloadOptions.Mode, oSaveDownloadOptions.SelectedValue);
                oDisplayFileData.ResponseXML = oDataList.OuterXml;
                oDisplayFileData.Mode = oSaveDownloadOptions.Mode;
                bresult = true;
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oDataList = null;
                bresult = false;
            }
            finally
            {
                oPolicySystemInterface = null;
            }
            return bresult;
        }

        //public void SaveOptions(ref SaveDownloadOptions oSaveDownloadOptions, ref BusinessAdaptorErrors p_objErrOut)
        //{
        //    PolicySystemInterface oPolicySystemInterface = null;
        //    string sRecordIds = string.Empty;
        //    PolicyEnquiry objEnReq = null;
        //    PolicyEnquiry objEnRes = null;
        //    string[] objRecordIds = null;
        //    string sUnitNumbers = string.Empty;
        //    string sPolicyExternalId = string.Empty;
        //    string sPolicySymbol = string.Empty;
        //    string oAcordAutoRequest = string.Empty;
        //    string oAcordAutoResponse = string.Empty;
        //    string sUpdatedIds = string.Empty;
        //    bool bSuccess = false;

        //    try
        //    {
        //        oPolicySystemInterface = new PolicySystemInterface(m_userLogin);
        //        sRecordIds = oPolicySystemInterface.SaveOptions(oSaveDownloadOptions.Mode, oSaveDownloadOptions.SelectedValue,oSaveDownloadOptions.PolicyId,oSaveDownloadOptions.AddEntityAs,ref sUnitNumbers,ref sPolicyExternalId,ref sPolicySymbol, ref sUpdatedIds);

        //        oSaveDownloadOptions.UpdatedIds = sUpdatedIds;

        //        if (string.Equals(oSaveDownloadOptions.Mode, "vehicle", StringComparison.InvariantCultureIgnoreCase) || string.Equals(oSaveDownloadOptions.Mode, "property", StringComparison.InvariantCultureIgnoreCase))
        //            {
        //                objRecordIds = sRecordIds.Split(',');

        //                objEnReq = new PolicyEnquiry();
        //                objEnRes = new PolicyEnquiry();

        //                foreach (string sRecordId in objRecordIds)
        //                {
        //                    if (string.Equals(oSaveDownloadOptions.Mode, "property", StringComparison.InvariantCultureIgnoreCase))
        //                    {
        //                        //objEnReq.PropertyPIN = sRecordId;
        //                       // oAcordAutoRequest = GetCoverageListAcordRequest(objEnReq, ref p_objErrOut);

        //                        if (!string.IsNullOrEmpty(oAcordAutoRequest))
        //                        {
        //                         //   oAcordAutoResponse = GetPropertyUnitCoverageListAcordResponse(oAcordAutoRequest, ref p_objErrOut);
        //                            if (!string.IsNullOrEmpty(oAcordAutoResponse))
        //                                GetPropertyCoverageListFormattedResult(oAcordAutoResponse, out objEnRes, ref p_objErrOut);
        //                            //SaveCoverages(objEnRes.PropertyListAcordXML, Conversion.CastToType<int>(sRecordId, out bSuccess), ref p_objErrOut);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        objEnReq.AutoVIN = sRecordId;
        //                      //  oAcordAutoRequest = GetCoverageListAcordRequest(objEnReq, ref p_objErrOut);

        //                        if (!string.IsNullOrEmpty(oAcordAutoRequest))
        //                        {
        //                           // oAcordAutoResponse = GetAutoUnitCoverageListAcordResponse(oAcordAutoRequest, ref p_objErrOut);
        //                            if (!string.IsNullOrEmpty(oAcordAutoResponse))
        //                                GetAutoCoverageListFormattedResult(oAcordAutoResponse, out objEnRes, ref p_objErrOut);
        //                            //SaveCoverages(objEnRes.AutoListAcordXML, Conversion.CastToType<int>(sRecordId, out bSuccess), ref p_objErrOut);
        //                        }
        //                    }
        //                }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);

        //    }
        //    finally
        //    {
        //        oPolicySystemInterface = null;
        //    }
        //}

        //public void SaveCoverages(string sAccordResponse, int p_iPolicyUnitId, ref BusinessAdaptorErrors p_objErrOut)
        //{
        //    PolicySystemInterface oPolicySystemInterface = null;
        //    string sRecordIds = string.Empty;
        //    string sMode = string.Empty;

        //    try
        //    {
        //        oPolicySystemInterface = new PolicySystemInterface(m_userLogin);
        //        oPolicySystemInterface.SaveCoverage(sAccordResponse, p_iPolicyUnitId);
        //    }
        //    catch (Exception e)
        //    {
        //        p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
        //    }
        //    finally
        //    {
        //        oPolicySystemInterface = null;
        //    }
        //}

        public void SavePSEndorsementData(string sAccordRequest, int iPolicyId, string sTableName, int iRowId, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicySystemInterface oPolicySystemInterface = null;
            string sRecordIds = string.Empty;
            string sMode = string.Empty;

            try
            {
                oPolicySystemInterface = new PolicySystemInterface(m_userLogin, base.ClientId);
                oPolicySystemInterface.SavePSEndorsementData(sAccordRequest, iPolicyId, sTableName, iRowId);

            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                oPolicySystemInterface = null;
            }
        }

        public bool GetPolicySearchFormattedResult(string oSearchAcordResponse, out PolicySearch oPolicySearchResponse, ref BusinessAdaptorErrors p_objErrOut)
        {
            XmlDocument oXMLPolicySearchResult = null;
            XElement oSearchAcordXML = null;
            XmlElement objRowXmlElement = null;
            XmlElement objXmlElement = null;
            XmlElement objRootElement = null;
            IEnumerable<XElement> oElements = null;
            IEnumerable<XElement> oTempElements = null;
            XElement oElementNode = null;
            XElement oSubElementNode = null;
            SysSettings objSettings = null;//MITS:33574
            int iCodeId = 0;
            bool bresult = false;
            XmlElement objXmlAdditionalElement = null;
            XmlElement objXmlAdditionalTempElement = null;

            XmlElement objRowAdditionalElement = null;


            oPolicySearchResponse = new PolicySearch();
            oPolicySearchResponse.ResponseXML = string.Empty;
            try
            {
                oSearchAcordXML = XElement.Parse(oSearchAcordResponse);
                if (oSearchAcordXML != null)
                {
                    oXMLPolicySearchResult = new XmlDocument();
                    objRootElement = oXMLPolicySearchResult.CreateElement("SearchResults");
                    oXMLPolicySearchResult.AppendChild(objRootElement);

                    oElements = oSearchAcordXML.XPathSelectElements("./InsuranceSvcRs/com.csc_PolicyListInquiryRs/com.csc_SelectList/com.csc_ListEntry");
                    if (oElements != null)
                    {
                        objRowAdditionalElement = oXMLPolicySearchResult.CreateElement("AdditonalFields");
                        foreach (XElement oElement in oElements)
                        {
                            objRowXmlElement = oXMLPolicySearchResult.CreateElement("Row");

                            objXmlElement = oXMLPolicySearchResult.CreateElement("status");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/PolicySummaryInfo/PolicyStatusCd");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                //iCodeId = (new PolicySystemInterface(connectionString)).GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "POLICY_STATUS");
                                //if (int.Equals(iCodeId, 0))
                                //{
                                //    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicySearchFormattedResult.NoMappedCodeFoundError"));
                                //}
                                //else
                                //{
                                //    objXmlElement.InnerText = (new LocalCache(connectionString)).GetCodeDesc(iCodeId);
                                objXmlElement.InnerText = oElementNode.Value;
                                objRowXmlElement.AppendChild(objXmlElement);
                                //}
                            }

                            oTempElements = oElement.XPathSelectElements("./com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");
                            if (oTempElements != null)
                            {
                                foreach (XElement oEle in oTempElements)
                                {
                                    oElementNode = oEle.XPathSelectElement("./OtherIdTypeCd");
                                    oSubElementNode = oEle.XPathSelectElement("./OtherId");
                                    switch (oElementNode.Value)
                                    {
                                        case "com.csc_LocCompany":
                                            objXmlElement = oXMLPolicySearchResult.CreateElement("location");
                                            if (oSubElementNode != null)
                                            {
                                                objXmlElement.InnerText = oSubElementNode.Value;
                                            }
                                            objRowXmlElement.AppendChild(objXmlElement);
                                            break;
                                        case "com.csc_MasterCompany":
                                            objXmlElement = oXMLPolicySearchResult.CreateElement("mastercompany");
                                            if (oSubElementNode != null)
                                            {
                                                objXmlElement.InnerText = oSubElementNode.Value;
                                            }
                                            objRowXmlElement.AppendChild(objXmlElement);
                                            break;
                                        case "com.csc_Module":
                                            objXmlElement = oXMLPolicySearchResult.CreateElement("module");
                                            if (oSubElementNode != null)
                                            {
                                                objXmlElement.InnerText = oSubElementNode.Value;
                                            }
                                            objRowXmlElement.AppendChild(objXmlElement);
                                            break;
                                    }
                                }
                                oElementNode = null;
                                oSubElementNode = null;
                            }

                            objXmlElement = oXMLPolicySearchResult.CreateElement("policynumber");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("policyname");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/CompanyProductCd");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("commercialname");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("lastname");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/Surname");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("firstname");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/GivenName");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("middlename");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/OtherGivenName");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("sortname");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/NickName");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("agency");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/ContractNumber");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("city");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/City");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("state");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/StateProvCd");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("lob");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/PolicySummaryInfo/LOBCd");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("effectivedate");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/com.csc_ContractTerm/EffectiveDt");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("expirationdate");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/com.csc_ContractTerm/ExpirationDt");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("inceptiondate");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/OriginalInceptionDt");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("InsurerName");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/com.csc_Insurer/GeneralPartyInfo/NameInfo/CommlName/CommercialName");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);


                            objXmlElement = oXMLPolicySearchResult.CreateElement("InsurerAddr1");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/com.csc_Insurer/GeneralPartyInfo/Addr/Addr1");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("InsurerCity");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/com.csc_Insurer/GeneralPartyInfo/Addr/City");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("InsurerPostalCode");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/com.csc_Insurer/GeneralPartyInfo/Addr/PostalCode");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("TaxId");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);
                            objXmlElement = oXMLPolicySearchResult.CreateElement("ClientSeqNo");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/InsuredOrPrincipal/ItemIdInfo/com.csc_ClientSeqNum");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);
                            objXmlElement = oXMLPolicySearchResult.CreateElement("AddressSeqNo");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/InsuredOrPrincipal/ItemIdInfo/com.csc_AddressSeqNum");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);
                            objXmlElement = oXMLPolicySearchResult.CreateElement("BirthDt");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/BirthDt");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);
                            objRootElement.AppendChild(objRowXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("NameType");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/NameType");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);
                            objRootElement.AppendChild(objRowXmlElement);

                            //JIRA 1342 ajohari2: Start
                            objXmlElement = oXMLPolicySearchResult.CreateElement("MinimumRetroDate");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/MinimumRetroDate");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);
                            objRootElement.AppendChild(objRowXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("MaximumExtendDate");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/MaximumExtendDate");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);
                            objRootElement.AppendChild(objRowXmlElement);

                            objXmlElement = oXMLPolicySearchResult.CreateElement("IsClaimsMade");
                            oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/IsClaimsMade");
                            if (oElementNode != null)
                            {
                                objXmlElement.InnerText = oElementNode.Value;
                            }
                            objRowXmlElement.AppendChild(objXmlElement);
                            if (oElement.XPathSelectElements("./com.csc_PolicyLevel/com.csc_AdditonalFields").Count() > 0)
                            {
                                foreach (XElement objAdditionalNodes in oElement.XPathSelectElements("./com.csc_PolicyLevel/com.csc_AdditonalFields/OtherIdentifier"))
                                {
                                    XElement objEle = objAdditionalNodes.XPathSelectElement("./OtherIdTypeCd");
                                    XElement objSubEle = objAdditionalNodes.XPathSelectElement("./OtherId");
                                    XElement objColumnName = objAdditionalNodes.XPathSelectElement("./OtherIdDisplayName");


                                    if (objEle != null)
                                    {
                                        if (!string.IsNullOrEmpty(objEle.Value))
                                        {

                                            if (objRowAdditionalElement.SelectSingleNode(objEle.Value) == null)
                                            {
                                                objXmlAdditionalElement = oXMLPolicySearchResult.CreateElement(objEle.Value);
                                                if (objColumnName != null)
                                                {
                                                    objXmlAdditionalElement.InnerText = objColumnName.Value;
                                                }
                                                else
                                                {

                                                    objXmlAdditionalElement.InnerText = objEle.Value;
                                                }
                                                objRowAdditionalElement.AppendChild(objXmlAdditionalElement);
                                            }
                                            objXmlAdditionalElement = null;
                                            objXmlAdditionalElement = oXMLPolicySearchResult.CreateElement(objEle.Value);
                                            if (objSubEle != null)
                                            {
                                                objXmlAdditionalElement.InnerText = objSubEle.Value;
                                            }

                                            objRowXmlElement.AppendChild(objXmlAdditionalElement);
                                        }
                                    }
                                }
                            }
                            objRootElement.AppendChild(objRowAdditionalElement);

                            objRootElement.AppendChild(objRowXmlElement);
                            //JIRA 1342 ajohari2: End
                        }
                        //MITS:33574: START
                        if (objRowXmlElement != null)
                        {
                            objXmlElement = oXMLPolicySearchResult.CreateElement("PointClaimEventSetting");

                            objSettings = new SysSettings(m_connectionString, base.ClientId);
                            if (objSettings.PolicyCvgType.ToString() == "0")
                                objXmlElement.InnerText = "true";
                            else
                                objXmlElement.InnerText = "false";
                            objRowXmlElement.AppendChild(objXmlElement);
                            objRootElement.AppendChild(objRowXmlElement);
                        }
                        //MITS:33574 END
                        oPolicySearchResponse.ResponseXML = oXMLPolicySearchResult.InnerXml;
                        bresult = true;
                    }
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oXMLPolicySearchResult = null;
                bresult = false;
            }
            finally
            {
                objRowAdditionalElement = null;
                objXmlAdditionalElement = null;
                objXmlAdditionalTempElement = null;


                if (oXMLPolicySearchResult != null)
                    oXMLPolicySearchResult = null;
                if (oSearchAcordXML != null)
                    oSearchAcordXML = null;
                if (objRowXmlElement != null)
                    objRowXmlElement = null;
                if (objXmlElement != null)
                    objXmlElement = null;
                if (objRootElement != null)
                    objRootElement = null;
                if (oElements != null)
                    oElements = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oTempElements != null)
                    oTempElements = null;
                if (oSubElementNode != null)
                    oSubElementNode = null;
                //MITS:33574
                if (objSettings != null)
                    objSettings = null;
            }

            return bresult;
        }

        public bool GetAutoUnitDetailFormattedResult(string oAcordResponse, out PolicyEnquiry oAutoUnitDetailResponse, ref BusinessAdaptorErrors p_objErrOut, string sPolicySystemID) // aaggarwal29: Code mapping change
        {
            XElement oAcordXML = null;
            IEnumerable<XElement> oElements = null;
            int iCodeId = 0;
            bool bResult = false;

            oAutoUnitDetailResponse = new PolicyEnquiry();
            //oAutoUnitDetailResponse.AutoListAcordXML = string.Empty;
            try
            {
                oAcordXML = XElement.Parse(oAcordResponse);
                if (oAcordXML != null)
                {
                    oElements = oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_ClaimsUnitFetchRs/AutoLossInfo/com.csc_VehicleTypeCd");
                    if (oElements != null)
                    {
                        foreach (XElement oElement in oElements)
                        {
                            if (oElement != null && !string.IsNullOrEmpty(oElement.Value))
                            {
                                iCodeId = (new PolicySystemInterface(connectionString, base.ClientId)).GetRMXCodeIdFromPSMappedCode(oElement.Value, "UNIT_TYPE_CODE", "VehicleType Code on GetAutoUnitDetailFormattedResult", sPolicySystemID); // aaggarwal29: Code mapping change
                                //if (int.Equals(iCodeId, 0))
                                //{
                                //    throw new RMAppException(string.Format(Globalization.GetString("PolicySystemInterfaceAdaptor.GetAutoUnitDetailFormattedResult.NoMappedCodeFoundError"),"VehicleType Code"));
                                //}
                                //else
                                //{
                                oElement.Value = (new LocalCache(connectionString, base.ClientId)).GetCodeDesc(iCodeId);
                                //}
                            }
                        }
                        //oAutoUnitDetailResponse.AutoListAcordXML = oAcordXML.ToString();
                        bResult = true;
                    }
                }
            }
            catch (Exception e)
            {
                bResult = false;
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oAcordXML != null)
                    oAcordXML = null;
                if (oElements != null)
                    oElements = null;
            }
            return bResult;
        }

        public bool GetPropertyUnitDetailFormattedResult(string oAcordResponse, out PolicyEnquiry oPropertyUnitDetailResponse, ref BusinessAdaptorErrors p_objErrOut, string sPolicySystemID) // aaggarwal29: Code mapping change
        {
            XElement oAcordXML = null;
            IEnumerable<XElement> oElements = null;
            int iCodeId = 0;
            bool bResult = false;

            oPropertyUnitDetailResponse = new PolicyEnquiry();
            //oPropertyUnitDetailResponse.PropertyListAcordXML = string.Empty;
            try
            {
                oAcordXML = XElement.Parse(oAcordResponse);
                if (oAcordXML != null)
                {
                    //oElements = oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_ClaimsUnitFetchRs/PropertyLossInfo/com.csc_ConstructionCd");
                    //if (oElements != null)
                    //{
                    //    foreach (XElement oElement in oElements)
                    //    {
                    //        if (oElement != null && !string.IsNullOrEmpty(oElement.Value))
                    //        {
                    //            iCodeId = (new PolicySystemInterface(connectionString)).GetRMXCodeIdFromPSMappedCode(oElement.Value, "CLASS_OF_CONSTRUCTION", "ConstructionCd on GetPropertyUnitDetailFormattedResult", sPolicySystemID);//aaggarwal29: Code mapping change
                    //            //if (int.Equals(iCodeId, 0))
                    //            //{
                    //            //    throw new RMAppException(string.Format(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPropertyUnitDetailFormattedResult.NoMappedCodeFoundError"),"ConstructionCd"));
                    //            //}
                    //            //else
                    //            //{
                    //                oElement.Value = (new LocalCache(connectionString)).GetCodeDesc(iCodeId);
                    //            //}
                    //        }
                    //    }
                    //    //oPropertyUnitDetailResponse.PropertyListAcordXML = oAcordXML.ToString();
                    //    bResult = true;
                    //}
                }
            }
            catch (Exception e)
            {
                bResult = false;
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oAcordXML != null)
                    oAcordXML = null;
                if (oElements != null)
                    oElements = null;
            }
            return bResult;
        }

        public bool GetPropertyCoverageListFormattedResult(string oAcordResponse, out PolicyEnquiry oPropertyCoverageListResponse, ref BusinessAdaptorErrors p_objErrOut, string sPolicySystemID) // aaggarwal29: Code mapping change
        {
            XElement oAcordXML = null;
            IEnumerable<XElement> oElements = null;
            XElement oTmpElement = null;
            int iCodeId = 0;
            bool bResult = false;

            oPropertyCoverageListResponse = new PolicyEnquiry();
            //oPropertyCoverageListResponse.PropertyListAcordXML = string.Empty;
            try
            {
                oAcordXML = XElement.Parse(oAcordResponse);
                if (oAcordXML != null)
                {
                    oElements = oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_ClmFinancialAddFetchRs/PropertyLossInfo/com.csc_CoverageLossInfo/Coverage");
                    if (oElements != null)
                    {
                        foreach (XElement oElement in oElements)
                        {
                            oTmpElement = oElement.XPathSelectElement("./CoverageCd");
                            if (oTmpElement != null && !string.IsNullOrEmpty(oTmpElement.Value))
                            {
                                iCodeId = (new PolicySystemInterface(connectionString, base.ClientId)).GetRMXCodeIdFromPSMappedCode(oTmpElement.Value, "COVERAGE_TYPE", "Coverage on GetPropertyCoverageListFormattedResult", sPolicySystemID); // aaggarwal29: Code mapping change
                                //if (int.Equals(iCodeId, 0))
                                //{
                                //    throw new RMAppException(string.Format(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPropertyCoverageListFormattedResult.NoMappedCodeFoundError"),"Coverage"));
                                //}
                                //else
                                //{
                                oTmpElement.Value = (new LocalCache(connectionString, base.ClientId)).GetShortCode(iCodeId);
                                oTmpElement = oElement.XPathSelectElement("./CoverageDesc");
                                if (oTmpElement != null)
                                    oTmpElement.Value = (new LocalCache(connectionString, base.ClientId)).GetCodeDesc(iCodeId);
                                //}
                            }
                        }
                        //oPropertyCoverageListResponse.PropertyListAcordXML = oAcordXML.ToString();
                        bResult = true;
                    }
                }
            }
            catch (Exception e)
            {
                bResult = false;
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oAcordXML != null)
                    oAcordXML = null;
                if (oElements != null)
                    oElements = null;
                if (oTmpElement != null)
                    oTmpElement = null;
            }
            return bResult;
        }

        public bool GetWorkLossCoverageListFormattedResult(string oAcordResponse, out PolicyEnquiry oPropertyCoverageListResponse, ref BusinessAdaptorErrors p_objErrOut, string sPolicySystemID) // aaggarwal29: Code mapping changes start
        {
            XElement oAcordXML = null;
            IEnumerable<XElement> oElements = null;
            XElement oTmpElement = null;
            int iCodeId = 0;
            bool bResult = false;

            oPropertyCoverageListResponse = new PolicyEnquiry();
            //oPropertyCoverageListResponse.PropertyListAcordXML = string.Empty;
            try
            {
                oAcordXML = XElement.Parse(oAcordResponse);
                if (oAcordXML != null)
                {
                    oElements = oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_ClmFinancialAddFetchRs/WorkCompLossInfo/com.csc_CoverageLossInfo/Coverage");
                    if (oElements != null)
                    {
                        foreach (XElement oElement in oElements)
                        {
                            oTmpElement = oElement.XPathSelectElement("./CoverageCd");
                            if (oTmpElement != null && !string.IsNullOrEmpty(oTmpElement.Value))
                            {
                                iCodeId = (new PolicySystemInterface(connectionString, base.ClientId)).GetRMXCodeIdFromPSMappedCode(oTmpElement.Value, "COVERAGE_TYPE", "Coverage on GetWorkLossCoverageListFormattedResult", sPolicySystemID);// aaggarwal29: Code mapping change
                                //if (int.Equals(iCodeId, 0))
                                //{
                                //    throw new RMAppException(string.Format(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPropertyCoverageListFormattedResult.NoMappedCodeFoundError"),"Coverage"));
                                //}
                                //else
                                //{
                                //    oTmpElement.Value = (new LocalCache(connectionString)).GetShortCode(iCodeId);
                                oTmpElement = oElement.XPathSelectElement("./CoverageDesc");
                                if (oTmpElement != null)
                                    oTmpElement.Value = (new LocalCache(connectionString, base.ClientId)).GetCodeDesc(iCodeId);
                                //}
                            }
                        }
                        //oPropertyCoverageListResponse.WorkLossAcordXML = oAcordXML.ToString();
                        bResult = true;
                    }
                }
            }
            catch (Exception e)
            {
                bResult = false;
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oAcordXML != null)
                    oAcordXML = null;
                if (oElements != null)
                    oElements = null;
                if (oTmpElement != null)
                    oTmpElement = null;
            }
            return bResult;
        }

        public bool GetAutoCoverageListFormattedResult(string oAcordResponse, out PolicyEnquiry oAutoCoverageListResponse, ref BusinessAdaptorErrors p_objErrOut, string sPolicySystemId)// aaggarwal29: Code mapping changes start
        {
            XElement oAcordXML = null;
            XElement oTmpElement = null;
            IEnumerable<XElement> oElements = null;
            int iCodeId = 0;
            bool bResult = false;

            oAutoCoverageListResponse = new PolicyEnquiry();
            //oAutoCoverageListResponse.AutoListAcordXML = string.Empty;
            try
            {
                oAcordXML = XElement.Parse(oAcordResponse);
                if (oAcordXML != null)
                {
                    oElements = oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_ClmFinancialAddFetchRs/AutoLossInfo/com.csc_CoverageLossInfo/Coverage");
                    if (oElements != null)
                    {
                        foreach (XElement oElement in oElements)
                        {
                            oTmpElement = oElement.XPathSelectElement("./CoverageCd");
                            if (oTmpElement != null && !string.IsNullOrEmpty(oTmpElement.Value))
                            {
                                iCodeId = (new PolicySystemInterface(connectionString, base.ClientId)).GetRMXCodeIdFromPSMappedCode(oTmpElement.Value, "COVERAGE_TYPE", "Coverage on GetAutoCoverageListFormattedResult", sPolicySystemId);

                                //if (int.Equals(iCodeId, 0))
                                //{
                                //    throw new RMAppException(string.Format(Globalization.GetString("PolicySystemInterfaceAdaptor.GetAutoCoverageListFormattedResult.NoMappedCodeFoundError"),"Coverage"));
                                //}
                                //else
                                //{
                                oTmpElement.Value = (new LocalCache(connectionString, base.ClientId)).GetShortCode(iCodeId);
                                oTmpElement = oElement.XPathSelectElement("./CoverageDesc");
                                if (oTmpElement != null)
                                    oTmpElement.Value = (new LocalCache(connectionString, base.ClientId)).GetCodeDesc(iCodeId);
                                //}
                            }
                        }
                        //oAutoCoverageListResponse.AutoListAcordXML = oAcordXML.ToString();
                        bResult = true;
                    }
                }
            }
            catch (Exception e)
            {
                bResult = false;
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oAcordXML != null)
                    oAcordXML = null;
                if (oElements != null)
                    oElements = null;
                if (oTmpElement != null)
                    oTmpElement = null;
            }
            return bResult;
        }

        public string GetPolicyIssueCode(int iPolicyId)
        {
            XElement objPolicySaveAccord = null;
            XElement oElement = null;
            PolicySystemInterface objManager = null;
            string sIssueCode = string.Empty;
            try
            {
                objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                objPolicySaveAccord = XElement.Parse(objManager.GetDownLoadXMLData("POLICY", iPolicyId, iPolicyId));


                oElement = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_IssueCd");
                if (oElement != null)
                {
                    sIssueCode = oElement.Value;
                    oElement = null;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objPolicySaveAccord = null;
                oElement = null;
                objManager = null;

            }
            return sIssueCode;

        }
		//RMA-12047
        /// <summary>
        /// CheckDuplicate Policy Download
        /// </summary>
        /// <param name="objCheckReq"></param>
        /// <param name="objCheckRes"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public bool CheckDuplicate(CheckDuplicateReq objCheckReq, ref CheckDuplicateRes objCheckRes, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sDupCriteria = string.Empty;
            string sDupXDays = string.Empty;
            DbReader dbr = null;
            string sPolicyName = string.Empty;
            string sSQL = string.Empty;
            string sClaimIds = string.Empty;
            string sPolLOBCodeId=string.Empty;
            try
            {
                if (objCheckReq.OverRideFlag == true)
                {
                    objCheckRes.DuplicateClaimIds = string.Empty;
                    return false;
                }
                sPolicyName = GetPolicyName(objCheckReq);
                if (string.IsNullOrEmpty(objCheckReq.LOBCodeId) || string.Equals(objCheckReq.LOBCodeId, "0"))
                {
                    int iCodeId = (new PolicySystemInterface(connectionString, base.ClientId)).GetRMXCodeIdFromPSMappedCode(objCheckReq.Lob, "POLICY_CLAIM_LOB", "Policy Lob on CheckDuplicate", objCheckReq.PolicySystemId);
                    sPolLOBCodeId = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(base.connectionString, "SELECT LINE_OF_BUS_CODE FROM CODES WHERE CODE_ID=" + iCodeId));
                }
                else
                {
                    sPolLOBCodeId = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(base.connectionString, "SELECT LINE_OF_BUS_CODE FROM CODES WHERE CODE_ID=" + objCheckReq.LOBCodeId));
                }
                using (dbr = DbFactory.ExecuteReader(base.connectionString, "SELECT POL_DUP_CLM_CRITERIA,POL_DUP_NUM_DAYS FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE=" + sPolLOBCodeId))
                {
                    while (dbr.Read())
                    {
                        int iPolDupCriteria = Conversion.ConvertObjToInt(dbr["POL_DUP_CLM_CRITERIA"],base.ClientId);
                        int iDupNumDays = Conversion.ConvertObjToInt(dbr["POL_DUP_NUM_DAYS"], base.ClientId);
                        if (iPolDupCriteria != -1)
                        {
                            switch (iPolDupCriteria)
                            {
                                case 0: //Policy Name & Date of Loss within X Days
                                    sSQL = @"SELECT DISTINCT CLAIM.CLAIM_ID AS CLAIM_ID,POLICY.POLICY_NUMBER AS POLICY_NUMBER,POLICY.POLICY_SYMBOL AS SYMBOL,POLICY.MASTER_COMPANY AS MASTER_COMPANY,POLICY.LOCATION_COMPANY AS LOCATION_COMPANY,POLICY.MODULE AS MODULE FROM CLAIM, EVENT, CLAIM_X_POLICY, POLICY WHERE CLAIM.LINE_OF_BUS_CODE =" + sPolLOBCodeId
                                          + " AND CLAIM.EVENT_ID = EVENT.EVENT_ID AND CLAIM.CLAIM_ID = CLAIM_X_POLICY.CLAIM_ID AND CLAIM_X_POLICY.POLICY_ID = POLICY.POLICY_ID "
                                          + " AND POLICY.POLICY_NAME = '" + sPolicyName + "' ";
                                    if (objCheckReq.SearchByDateType != -1)
                                    {
                                        if (objCheckReq.SearchByDateType == 0)
                                        {
                                            sSQL = sSQL + " AND EVENT.DATE_OF_EVENT >= '" + Conversion.ToDbDate(Conversion.ToDate(objCheckReq.LossDate).AddDays(-iDupNumDays)) + "'"
                                        + " AND EVENT.DATE_OF_EVENT <= '" + Conversion.ToDbDate(Conversion.ToDate(objCheckReq.LossDate).AddDays(iDupNumDays)) + "'";
                                        }
                                        else
                                        {
                                            sSQL = sSQL + " AND CLAIM.DATE_OF_CLAIM >= '" + Conversion.ToDbDate(Conversion.ToDate(objCheckReq.LossDate).AddDays(-iDupNumDays)) + "'"
                                        + " AND CLAIM.DATE_OF_CLAIM <= '" + Conversion.ToDbDate(Conversion.ToDate(objCheckReq.LossDate).AddDays(iDupNumDays)) + "'";
                                        }
                                    }
                                    else
                                    {
                                      string sSearchType = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(base.connectionString, "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME ='POLICY_CVG_TYPE'"));
                                      if (sSearchType == "0")
                                      {
                                          sSQL = sSQL + " AND EVENT.DATE_OF_EVENT >= '" + Conversion.ToDbDate(Conversion.ToDate(objCheckReq.LossDate).AddDays(-iDupNumDays)) + "'"
                                      + " AND EVENT.DATE_OF_EVENT <= '" + Conversion.ToDbDate(Conversion.ToDate(objCheckReq.LossDate).AddDays(iDupNumDays)) + "'";
                                      }
                                      else
                                      {
                                          sSQL = sSQL + " AND CLAIM.DATE_OF_CLAIM >= '" + Conversion.ToDbDate(Conversion.ToDate(objCheckReq.LossDate).AddDays(-iDupNumDays)) + "'"
                                      + " AND CLAIM.DATE_OF_CLAIM <= '" + Conversion.ToDbDate(Conversion.ToDate(objCheckReq.LossDate).AddDays(iDupNumDays)) + "'";
                                      }
                                    }
                                    break;
                            }
                            using (DbReader rdr = DbFactory.ExecuteReader(base.connectionString,sSQL))
                            {
                                if (!rdr.Read())
                                    return false;
                                objCheckRes.DuplicateClaimIds = string.Empty;
                                string sExtraParameters = string.Empty;
                                do
                                {
                                    sClaimIds = sClaimIds + rdr.GetInt("CLAIM_ID").ToString() + ",";
                                    if (string.IsNullOrEmpty(sExtraParameters))
                                        sExtraParameters = Convert.ToString(rdr["POLICY_NUMBER"]) + ";" + Convert.ToString(rdr["SYMBOL"]) + ";" + Convert.ToString(rdr["MASTER_COMPANY"])
                                                            + ";" + Convert.ToString(rdr["LOCATION_COMPANY"]) + ";" + Convert.ToString(rdr["MODULE"]);
                                } while (rdr.Read());
                                sClaimIds = sClaimIds.Substring(0, sClaimIds.Length - 1);
                                sClaimIds = sClaimIds + ";" + sPolicyName + ";" + Conversion.ToDate(objCheckReq.LossDate).ToShortDateString() + ";"
                                             + sExtraParameters;
                            }
                            objCheckRes.DuplicateClaimIds = sClaimIds;
                            return true;
                        }
                        else
                        {
                            objCheckRes.DuplicateClaimIds = string.Empty;
                            return true;
                        }
                    }
                }
                return true; ;
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                return false;
            }
            finally
            {
            }
        }
		//RMA-12047
        /// <summary>
        /// GetPolicyName
        /// </summary>
        /// <param name="objCheckReq"></param>
        /// <returns></returns>
        private string GetPolicyName(CheckDuplicateReq objCheckReq)
        {
            string sPolicyName = string.Empty;
            if (objCheckReq.PolicySystemType == "INTEGRAL")
            {
                string Policysymbol = objCheckReq.PolicySymbol;
                if (sPolicyName != string.Empty)
                {
                    sPolicyName = sPolicyName + " " + Policysymbol;
                }
                else
                {
                    sPolicyName = Policysymbol;
                }
                string PolicyNumber = objCheckReq.PolicyNumber;
                if (sPolicyName != string.Empty)
                {
                    sPolicyName = sPolicyName + " " + PolicyNumber;
                }
                else
                {
                    sPolicyName = PolicyNumber;
                }
                string Module = objCheckReq.Module;
                if (sPolicyName != string.Empty)
                {
                    sPolicyName = sPolicyName + " " + Module;
                }
                else
                {
                    sPolicyName = Module;
                }
            }
            else if (objCheckReq.PolicySystemType == "POINT")
            {
                LocalCache objCache = new LocalCache(base.connectionString, base.ClientId);
                string sPolicyFields = objCache.GetPolicyInterfaceConfigSettings("PolicyInterface", "PolicyDownloadDataToDisplay");
                string[] values = Enum.GetNames(typeof(PolicyFieldsData));
                if (!(string.IsNullOrEmpty(sPolicyFields)))
                {
                    string[] arrValues = sPolicyFields.Split(',');
                    for (int i = 0; i <= arrValues.Length - 1; i++)
                    {
                        if (arrValues[i] == values[0])//symbol
                        {
                            string Policysymbol = objCheckReq.PolicySymbol;
                            if (sPolicyName != string.Empty)
                            {
                                sPolicyName = sPolicyName + " " + Policysymbol;
                            }
                            else
                            {
                                sPolicyName = Policysymbol;
                            }
                        }
                        else if (arrValues[i] == values[1])//policynumber
                        {
                            string PolicyNumber = objCheckReq.PolicyNumber;
                            if (sPolicyName != string.Empty)
                            {
                                sPolicyName = sPolicyName + " " + PolicyNumber;
                            }
                            else
                            {
                                sPolicyName = PolicyNumber;
                            }
                        }

                        else if (arrValues[i] == values[2])//mastercompany
                        {
                            string sMastercompany = objCheckReq.MasterCompany;
                            if (sPolicyName != string.Empty)
                            {
                                sPolicyName = sPolicyName + " " + sMastercompany;
                            }
                            else
                            {
                                sPolicyName = sMastercompany;
                            }
                        }

                        else if (arrValues[i] == values[3])//locationcompany
                        {
                            string sLocationcompany = objCheckReq.LocationCompany;
                            if (sPolicyName != string.Empty)
                            {
                                sPolicyName = sPolicyName + " " + sLocationcompany;
                            }
                            else
                            {
                                sPolicyName = sLocationcompany;
                            }
                        }

                        else if (arrValues[i] == values[4])//module
                        {
                            string Module = objCheckReq.Module;
                            if (sPolicyName != string.Empty)
                            {
                                sPolicyName = sPolicyName + " " + Module;
                            }
                            else
                            {
                                sPolicyName = Module;
                            }
                        }

                        else if (arrValues[i] == values[5])//insuredname
                        {
                            string sInsrdName = objCheckReq.InsuredName;
                            if (sPolicyName != string.Empty)
                            {
                                sPolicyName = sPolicyName + " " + sInsrdName;
                            }
                            else
                            {
                                sPolicyName = sInsrdName;
                            }
                        }
                    }
                }
                else
                {

                    string Policysymbol = objCheckReq.PolicySymbol;
                    if (sPolicyName != string.Empty)
                    {
                        sPolicyName = sPolicyName + " " + Policysymbol;
                    }
                    else
                    {
                        sPolicyName = Policysymbol;
                    }
                    string PolicyNumber = objCheckReq.PolicyNumber;
                    if (sPolicyName != string.Empty)
                    {
                        sPolicyName = sPolicyName + " " + PolicyNumber;
                    }
                    else
                    {
                        sPolicyName = PolicyNumber;
                    }
                    string Module = objCheckReq.Module;
                    if (sPolicyName != string.Empty)
                    {
                        sPolicyName = sPolicyName + " " + Module;
                    }
                    else
                    {
                        sPolicyName = Module;
                    }

                }
                if (objCache != null)
                    objCache.Dispose();
            }
            else
            {

                string Policysymbol = objCheckReq.PolicySymbol;
                if (sPolicyName != string.Empty)
                {
                    sPolicyName = sPolicyName + " " + Policysymbol;
                }
                else
                {
                    sPolicyName = Policysymbol;
                }
                string PolicyNumber = objCheckReq.PolicyNumber;
                if (sPolicyName != string.Empty)
                {
                    sPolicyName = sPolicyName + " " + PolicyNumber;
                }
                else
                {
                    sPolicyName = PolicyNumber;
                }
                string Module = objCheckReq.Module;
                if (sPolicyName != string.Empty)
                {
                    sPolicyName = sPolicyName + " " + Module;
                }
                else
                {
                    sPolicyName = Module;
                }

            }
            return sPolicyName;
        }
        public bool GetPolicySaveFormattedResult(int iClaimId, string oAcordResponse, ref PolicySaveRequest oSaveResponse, ref BusinessAdaptorErrors p_objErrOut, string sPolicySystemId)
        {
            XElement oDataModelTemplate = null;
            XElement oSaveAcordXML = null;
            XElement oElement = null;
            XElement oElementNode = null;
            XElement oDataModelElement = null;
            int iCodeId = 0;
            //oSaveResponse = new PolicySaveRequest();
            oSaveResponse.Result = false;
            string sFileName = "policy_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml";
            PolicySystemInterface objManager = null;
            SysSettings objSettings = null;
            int iInsuredEid = 0;
            int iInsurerEid = 0;
            int iPolicyId = 0;
            bool bIsNewPolicy = true, bIsOrphan = true;
            string sAgentNumber = string.Empty;
            string sAgentName = string.Empty;
            string sPolicyLOB = string.Empty;
            string sPolCompny = string.Empty;
            string sPolicyName = string.Empty;
            DataTable dtPolicyData;
            string sShortCd = string.Empty;
            string sStatesTableNm;
            int iAddressSeqNum = -1;
            //Start -  Changed by Nikhil on 11/03/14            
            //bool bClientFile;
            bool bClientFile = false;
            //end -  Changed by Nikhil on 11/03/14
            //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim.
            LocalCache objCache = new LocalCache(connectionString, base.ClientId);
            string sPolicyFields = objCache.GetPolicyInterfaceConfigSettings("PolicyInterface", "PolicyDownloadDataToDisplay");
            string[] values = Enum.GetNames(typeof(PolicyFieldsData));
            string sNode = string.Empty;
            //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim.
            ArrayList objLimitList = null;
            IEnumerable<XElement> oElements = null;
            try
            {
                oSaveAcordXML = XElement.Parse(oAcordResponse);
                DeleteFile(sFileName);
                if (oSaveAcordXML != null)
                {
                    objManager = new PolicySystemInterface(m_connectionString, base.ClientId);
                    // oElement = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo");
                    //if (oElement != null)
                    //{
                    oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity");
                    if (oElementNode != null)
                    {
                        oElementNode.Value = oSaveResponse.TaxId;
                    }
                    oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Policy", ref p_objErrOut));
                    objLimitList = new ArrayList();
                    if (oDataModelTemplate != null)
                    {
                        //oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier/OtherId");
                        //if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                        //{
                        //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/ExternalPolicyId");
                        //    if (oDataModelElement != null)
                        //        oDataModelElement.Value = oElementNode.Value;
                        //}
                        //else
                        //{
                        //    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicySaveFormattedResult.NoExtPolicyIdFound"));
                        //}

                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/PolicyStatusCd");
                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                        {
                            iCodeId = (new PolicySystemInterface(connectionString, base.ClientId)).GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "POLICY_STATUS", "Policy Status on GetPolicySaveFormattedResult", sPolicySystemId);
                            //if (int.Equals(iCodeId, 0))
                            //{
                            //        throw new RMAppException(string.Format(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicySaveFormattedResult.NoMappedCodeFoundError"),"Policy Status"));
                            //}
                            //else
                            //{
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicyStatusCode");
                            if (oDataModelElement != null)
                                oDataModelElement.Attribute("codeid").Value = iCodeId.ToString();
                            //}
                        }

                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_PolicyCurCd");
                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                        {
                            //commented code for currency type code mapping check during policy download 
                            //iCodeId = (new PolicySystemInterface(connectionString)).GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "CURRENCY_TYPE", "Currency on GetPolicySaveFormattedResult", sPolicySystemId);
                            ////if (int.Equals(iCodeId, 0))
                            ////{
                            ////        throw new RMAppException(string.Format(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicySaveFormattedResult.NoMappedCodeFoundError"),"Currency"));
                            ////}
                            ////else
                            ////{
                            //oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/CurrencyCode");
                            //if (oDataModelElement != null)
                            //    oDataModelElement.Value = iCodeId.ToString();
                            //}
                        }

                        //MITS 31746 start
                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/StateProvCd");
                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                        {

                            int iTemp = Conversion.ConvertStrToInteger(sPolicySystemId);
                            dtPolicyData = objManager.GetPolicySystemInfoById(iTemp);
                            sStatesTableNm = dtPolicyData.Rows[0]["TABLE_PREFIX"].ToString() + "_" + "STATES";
                            sShortCd = objManager.GetShortCode(sStatesTableNm, oElementNode.Value);
                            if (sShortCd.Trim() == string.Empty)
                            {
                                throw new RMAppException(string.Format(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicySaveFormattedResult.NoMappedCodeDesc", base.ClientId), oElementNode.Value, sStatesTableNm));
                            }

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicyXState");
                            if (oDataModelElement != null)
                                oDataModelElement.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(sShortCd, "STATES", "Policy state on GetPolicySaveFormattedResult", sPolicySystemId).ToString();
                        }

                        // MITS 31746 end
                        //Ashish Ahuja: Claims Made Jira 1342
                        objCache = new LocalCache(connectionString, base.ClientId);
                        if (!string.IsNullOrEmpty(oSaveResponse.ClaimBasedFlag))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/ClaimBasedFlag");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oSaveResponse.ClaimBasedFlag;
                        }
                        //Ashish Ahuja

                        //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim.
                        if (!(string.IsNullOrEmpty(sPolicyFields)))
                        {
                            string[] arrValues = sPolicyFields.Split(',');
                            for (int i = 0; i <= arrValues.Length - 1; i++)
                            {
                                if (arrValues[i] == values[0])//symbol
                                {
                                    //Fixed review comments for MITS- 34260/RMA-4362 :pgupta93: START
                                    //sNode = "CompanyProductCd";
                                    //oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/CompanyProductCd");


                                    //if (oElementNode.Name.LocalName == sNode)
                                    //{
                                    //    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                    //    {
                                    //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicySymbol");
                                    //        if (oDataModelElement != null)
                                    //        {
                                    //            oDataModelElement.Value = oElementNode.Value;
                                    //            if (sPolicyName != string.Empty)
                                    //            {
                                    //                sPolicyName = sPolicyName + " " + oElementNode.Value;
                                    //            }
                                    //            else
                                    //            {
                                    //                sPolicyName = oElementNode.Value;
                                    //            }
                                    //        }
                                    //    }

                                    string Policysymbol = GetPolicySymbol(oAcordResponse, oDataModelTemplate);
                                    if (sPolicyName != string.Empty)
                                    {
                                        sPolicyName = sPolicyName + " " + Policysymbol;
                                    }
                                    else
                                    {
                                        sPolicyName = Policysymbol;
                                    }
                                }

                                        //Fixed review comments for MITS- 34260/RMA-4362 :pgupta93: END


                                else if (arrValues[i] == values[1])//policynumber
                                {
                                    //Fixed review comments for MITS- 34260/RMA-4362 :pgupta93: START
                                    //sNode = "PolicyNumber";
                                    //oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber");
                                    //if (oElementNode.Name.LocalName == sNode)
                                    //{
                                    //    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                    //    {
                                    //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicyNumber");
                                    //        if (oDataModelElement != null)
                                    //        {
                                    //            oDataModelElement.Value = oElementNode.Value;
                                    //            if (sPolicyName != string.Empty)
                                    //            {
                                    //                sPolicyName = sPolicyName + " " + oElementNode.Value;
                                    //            }
                                    //            else
                                    //            {
                                    //                sPolicyName = oElementNode.Value;
                                    //            }
                                    //        }
                                    //    }
                                    //}



                                    string PolicyNumber = GetPolicyNumber(oAcordResponse, oDataModelTemplate);
                                    if (sPolicyName != string.Empty)
                                    {
                                        sPolicyName = sPolicyName + " " + PolicyNumber;
                                    }
                                    else
                                    {
                                        sPolicyName = PolicyNumber;
                                    }
                                    //Fixed review comments for MITS- 34260/RMA-4362 :pgupta93: END
                                }

                                else if (arrValues[i] == values[2])//mastercompany
                                {
                                    sNode = "com.csc_MasterCompany";
                                    oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherIdTypeCd");
                                    if (oElementNode.Value == sNode)
                                    {
                                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId");
                                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                        {
                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/MasterCompany");
                                            if (oDataModelElement != null)
                                            {
                                                oDataModelElement.Value = oElementNode.Value;
                                                if (sPolicyName != string.Empty)
                                                {
                                                    sPolicyName = sPolicyName + " " + oElementNode.Value;
                                                }
                                                else
                                                {
                                                    sPolicyName = oElementNode.Value;
                                                }
                                            }
                                        }
                                    }
                                }

                                else if (arrValues[i] == values[3])//locationcompany
                                {
                                    sNode = "com.csc_LocCompany";
                                    oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherIdTypeCd");
                                    if (oElementNode.Value == sNode)
                                    {
                                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                        {
                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/LocationCompany");
                                            if (oDataModelElement != null)
                                            {
                                                oDataModelElement.Value = oElementNode.Value;
                                                if (sPolicyName != string.Empty)
                                                {
                                                    sPolicyName = sPolicyName + " " + oElementNode.Value;
                                                }
                                                else
                                                {
                                                    sPolicyName = oElementNode.Value;
                                                }
                                            }
                                        }
                                    }
                                }

                                else if (arrValues[i] == values[4])//module
                                {
                                    //Fixed review comments for MITS- 34260/RMA-4362 :pgupta93: START
                                    //sNode = "com.csc_Module";
                                    //oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherIdTypeCd");
                                    //if (oElementNode.Value == sNode)
                                    //{
                                    //    oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId");
                                    //    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                    //    {
                                    //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/Module");
                                    //        if (oDataModelElement != null)
                                    //        {
                                    //            oDataModelElement.Value = oElementNode.Value;
                                    //            if (sPolicyName != string.Empty)
                                    //            {
                                    //                sPolicyName = sPolicyName + " " + oElementNode.Value;
                                    //            }
                                    //            else
                                    //            {
                                    //                sPolicyName = oElementNode.Value;
                                    //            }
                                    //        }
                                    //    }
                                    //}
                                    string Module = GetModule(oAcordResponse, oDataModelTemplate);
                                    if (sPolicyName != string.Empty)
                                    {
                                        sPolicyName = sPolicyName + " " + Module;
                                    }
                                    else
                                    {
                                        sPolicyName = Module;
                                    }
                                    //Fixed review comments for MITS- 34260/RMA-4362 :pgupta93: END
                                }

                                else if (arrValues[i] == values[5])//insuredname
                                {
                                    sNode = "CommercialName";
                                    oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName");

                                    if (oElementNode.Name.LocalName == sNode)
                                    {
                                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                        {
                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicyXInsured");
                                            if (oDataModelElement != null)
                                            {
                                                oDataModelElement.Value = oElementNode.Value;
                                                if (sPolicyName != string.Empty)
                                                {
                                                    sPolicyName = sPolicyName + " " + oElementNode.Value;
                                                }
                                                else
                                                {
                                                    sPolicyName = oElementNode.Value;
                                                }
                                                //sPolicyName = sPolicyName + " " + oElementNode.Value;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {

                            string Policysymbol = GetPolicySymbol(oAcordResponse, oDataModelTemplate);
                            if (sPolicyName != string.Empty)
                            {
                                sPolicyName = sPolicyName + " " + Policysymbol;
                            }
                            else
                            {
                                sPolicyName = Policysymbol;
                            }
                            string PolicyNumber = GetPolicyNumber(oAcordResponse, oDataModelTemplate);
                            if (sPolicyName != string.Empty)
                            {
                                sPolicyName = sPolicyName + " " + PolicyNumber;
                            }
                            else
                            {
                                sPolicyName = PolicyNumber;
                            }
                            string Module = GetModule(oAcordResponse, oDataModelTemplate);
                            if (sPolicyName != string.Empty)
                            {
                                sPolicyName = sPolicyName + " " + Module;
                            }
                            else
                            {
                                sPolicyName = Module;
                            }

                        }


                        //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim.

                        if (!sPolicyName.Equals(""))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicyName");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sPolicyName;
                        }

                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ContractTerm/EffectiveDt");
                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/EffectiveDate");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oElementNode.Value;
                        }

                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ContractTerm/ExpirationDt");
                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/ExpirationDate");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oElementNode.Value;
                        }

                        //oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/CompanyProductCd");
                        //if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                        //{
                        //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicySymbol");
                        //    if (oDataModelElement != null)
                        //        oDataModelElement.Value = oElementNode.Value;
                        //}

                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/LOBCd");
                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                        {
                            sPolicyLOB = oElementNode.Value;
                            iCodeId = (new PolicySystemInterface(connectionString, base.ClientId)).GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "POLICY_CLAIM_LOB", "Policy Lob on GetPolicySaveFormattedResult", sPolicySystemId);
                            //if (int.Equals(iCodeId, 0))
                            //{
                            //        throw new RMAppException(string.Format(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicySaveFormattedResult.NoMappedCodeFoundError"),"Policy Lob"));
                            //}
                            //else
                            //{
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicyLOB");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = iCodeId.ToString();
                            //}
                        }
                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_AgentPremium");
                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/Premium");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oElementNode.Value;
                        }

                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId");
                        if (oElementNode != null)
                        {
                            oSaveResponse.MasterCompany = oElementNode.Value;
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/MasterCompany");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oElementNode.Value;
                        }

                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                        if (oElementNode != null)
                        {
                            oSaveResponse.Location = oElementNode.Value;
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/LocationCompany");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oElementNode.Value;
                        }

                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_BranchCd");
                        if (oElementNode != null)
                        {
                            oSaveResponse.Location = oElementNode.Value;
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/BranchCode");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oElementNode.Value;
                        }

                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolCompany']/OtherId");
                        if (oElementNode != null)
                        {
                            sPolCompny = oElementNode.Value;
                        }

                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_AgentName");
                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                        {
                            sAgentName = oElementNode.Value;
                        }

                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/ContractNumber");
                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                        {
                            sAgentNumber = oElementNode.Value;
                        }

                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PrimaryPolicyFlg");
                        if (oDataModelElement != null)
                            oDataModelElement.Value = "True";

                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicySystemId");
                        if (oDataModelElement != null)
                            oDataModelElement.Value = sPolicySystemId;

                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/TriggerClaimFlag");
                        if (oDataModelElement != null)
                        {
                            objSettings = new SysSettings(m_connectionString, base.ClientId);
                            if (objSettings.PolicyCvgType.ToString() == "0")
                                oDataModelElement.Value = "True";
                            else
                                oDataModelElement.Value = "False";
                        }

                    }
                    oSaveResponse.Result = true;
                    //WriteFormattedXmlToFile(sFileName, oDataModelTemplate.ToString(), ref p_objErrOut);
                    //   }
                    objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                    //  objManager.s
                    //rsharma220 MITS 35964 (removed orphan policy concept)
                    // aaggarwal29: MITS 34580 start ( added bIsOrphan check)	
                    iPolicyId = objManager.CheckPolicyDuplication(iClaimId, oAcordResponse, sPolicySystemId);
                    if (iPolicyId > 0)
                    {
                        bIsNewPolicy = false;
                        //JIRA 1342 ajohari2: Start
                        int iClaimBase = 0;
                        objSettings = new SysSettings(m_connectionString, base.ClientId);
                        StringBuilder sbSql = new StringBuilder();
                        DbConnection objConn = DbFactory.GetDbConnection(m_connectionString);

                        sbSql.AppendFormat("SELECT CLAIM_BASED_FLAG FROM POLICY WHERE POLICY_ID = {0}", iPolicyId);

                        if (sbSql != null)
                        {
                            objConn.Open();
                            iClaimBase = Conversion.ConvertObjToInt(objConn.ExecuteScalar(sbSql.ToString()), base.ClientId);
                        }
                        // PolicySearchByDate == 1 Claim Date
                        // PolicySearchByDate == 0 Event Date
                        if (iClaimBase != oSaveResponse.PolicySearchByDate)
                        {
                            if (iClaimBase == 1)
                            {
                                throw new RMAppException(string.Format(Globalization.GetString("ClaimMade.UseAutoClaimReported.ClaimDateError", base.ClientId)));
                            }
                            else
                            {
                                throw new RMAppException(string.Format(Globalization.GetString("ClaimMade.UseAutoClaimReported.SystemDateError", base.ClientId)));
                            }

                        }
                        //JIRA 1342 ajohari2: End
                        //if (bIsOrphan)
                        //{
                        //    objManager.OrphanPolicyCleanUp(iPolicyId);
                        //}
                        // aaggarwal29: MITS 34580 end
                    }
                    else
                        bIsNewPolicy = true;
                    GetInsuredSaveFormattedResult(oSaveAcordXML.ToString(), ref p_objErrOut, sPolicyLOB, sPolicySystemId); // aaggarwal29: Code mapping change

                    //GetInsuredSaveFormattedResult(oAcordResponse, ref p_objErrOut, sPolicyLOB, sPolicySystemId); // aaggarwal29: Code mapping change
                    //start - Changed by Nikhil on 11/03/14.Null check included
                    // bClientFile = Convert.ToBoolean(oSaveAcordXML.XPathSelectElement("./SignonRs/ClientApp/com.csc_ClientFile").Value);
                    if (oSaveAcordXML.XPathSelectElement("./SignonRs/ClientApp/com.csc_ClientFile") != null)
                    {
                        bClientFile = Convert.ToBoolean(oSaveAcordXML.XPathSelectElement("./SignonRs/ClientApp/com.csc_ClientFile").Value);
                    }
                    //end - Changed by Nikhil on 11/03/14.Null check included

                    iInsuredEid = objManager.SaveInsuredEntity(bIsNewPolicy, iPolicyId, base.GetSessionObject().SessionId, bClientFile);
                    //if (sPolicyLOB.Equals("WC") || sPolicyLOB.Equals("WCV") || sPolicyLOB.Equals("WCA"))
                    //    objManager.CreateSubTypeEntity(iInsuredEid);



                    GetInsurerSaveFormattedResult(oSaveResponse, ref p_objErrOut, sPolicySystemId, sPolCompny);//, ref oInsurerTempData);
                    iInsurerEid = objManager.SaveInsurerEntity(base.GetSessionObject().SessionId);

                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicyXInsured");
                    if (oDataModelElement != null)
                        oDataModelElement.Attribute("codeid").Value = iInsuredEid.ToString();

                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/InsurerEid");
                    if (oDataModelElement != null)
                        oDataModelElement.Attribute("codeid").Value = iInsurerEid.ToString();

                    WriteFormattedXmlToFile(sFileName, oDataModelTemplate.ToString(), ref p_objErrOut);
                    // = oElementNode.Value;
                    
                    oElements = oSaveAcordXML.XPathSelectElements("../ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/Limit");
                    if (oElements != null)
                    {
                        foreach (XElement oEle in oElements)
                        {
                            XElement oElement1 = oEle.XPathSelectElement("./LimitAppliesToCd");
                            XElement oSubElement1 = oEle.XPathSelectElement("./FormatCurrencyAmt/Amt");
                            if (!string.IsNullOrEmpty(oElement1.Value))
                            {

                                objLimitList.Add(oElement1.Value + "|" + oSubElement1.Value);
                            }

                            oElement1 = null;
                            oSubElement1 = null;
                        }
                    }   

                    oSaveResponse.AddedPolicyId = objManager.SavePolicy(iPolicyId, base.GetSessionObject().SessionId,oAcordResponse,objLimitList);
                    // mits 35925 start
                    if (oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/ItemIdInfo/com.csc_AddressSeqNum") != null && !string.IsNullOrEmpty(oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/ItemIdInfo/com.csc_AddressSeqNum").Value.ToString()))       //Worked for JIRA(RMA-813)
                        iAddressSeqNum = Convert.ToInt32(oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/ItemIdInfo/com.csc_AddressSeqNum").Value.ToString());
                    AddAddressIDToPolicyInsured(oSaveResponse.AddedPolicyId, iInsuredEid, iAddressSeqNum);
                    //mits 35925 end
                    objManager.SaveAgents(sAgentNumber, sAgentName, oSaveResponse.AddedPolicyId, oAcordResponse);
                    //objManager.SavePolicyDownloadXMLData(oSaveResponse.AddedPolicyId, oAcordResponse, objManager.GetPolicySystemId(oSaveResponse.AddedPolicyId), "POLICY");
                    // objManager.SaveAccordResponse(oAcordResponse,(new LocalCache(connectionString)).GetTableId("POLICY"),oSaveResponse.AddedPolicyId);
                    // objManager.SaveEntity();

                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oSaveResponse.Result = false;
            }
            finally
            {
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElement != null)
                    oElement = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
            }

            return oSaveResponse.Result;
        }

        /// <summary>
        /// pgupta93 : MITS- 34260/RMA-4362
        /// </summary>
        /// <param name="oAcordResponse"></param>
        /// <returns></returns>
        public string GetPolicySymbol(string oAcordResponse, XElement oDataModelTemplate)
        {

            XElement oElementNode = null;
            XElement oDataModelElement = null;
            XElement oSaveAcordXML = null;
            string sPolicyName = string.Empty;
            string sNode = string.Empty;

            oSaveAcordXML = XElement.Parse(oAcordResponse);
            try
            {
                sNode = "CompanyProductCd";
                oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/CompanyProductCd");

                if (oElementNode != null)
                {
                    if (oElementNode.Name.LocalName == sNode)
                    {
                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicySymbol");
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = oElementNode.Value;
                                sPolicyName = oElementNode.Value;
                            }
                        }
                    }
                }
            }
            catch (Exception e) { throw e; }
            finally
            {
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
            }
            return sPolicyName;
        }
        /// <summary>
        /// pgupta93 : MITS- 34260/RMA-4362
        /// </summary>
        /// <param name="oAcordResponse"></param>
        /// <returns></returns>
        public string GetPolicyNumber(string oAcordResponse, XElement oDataModelTemplate)
        {

            XElement oElementNode = null;
            XElement oDataModelElement = null;
            XElement oSaveAcordXML = null;
            string sPolicyName = string.Empty;
            string sNode = string.Empty;

            oSaveAcordXML = XElement.Parse(oAcordResponse);
            try
            {
                sNode = "PolicyNumber";
                oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber");
                if (oElementNode.Name.LocalName == sNode)
                {
                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                    {
                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/PolicyNumber");
                        if (oDataModelElement != null)
                        {
                            oDataModelElement.Value = oElementNode.Value;
                            sPolicyName = oElementNode.Value;
                        }
                    }
                }
            }

            catch (Exception e) { throw e; }
            finally
            {
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
            }
            return sPolicyName;
        }
        /// <summary>
        /// pgupta93 : MITS- 34260/RMA-4362
        /// </summary>
        /// <param name="oAcordResponse"></param>
        /// <returns></returns>
        public string GetModule(string oAcordResponse, XElement oDataModelTemplate)
        {
            XElement oElementNode = null;
            XElement oDataModelElement = null;
            XElement oSaveAcordXML = null;
            string sPolicyName = string.Empty;
            string sNode = string.Empty;

            oSaveAcordXML = XElement.Parse(oAcordResponse);
            try
            {
                sNode = "com.csc_Module";
                oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherIdTypeCd");
                if (oElementNode.Value == sNode)
                {
                    oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId");
                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                    {
                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Policy/Module");
                        if (oDataModelElement != null)
                        {
                            oDataModelElement.Value = oElementNode.Value;
                            sPolicyName = oElementNode.Value;
                        }
                    }
                }
            }

            catch (Exception e) { throw e; }
            finally
            {
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
            }
            return sPolicyName;
        }

        // mits 35925 start

        public void AddAddressIDToPolicyInsured(int iAddedPolicyId, int iInsuredEid, int iAddressSeqNum)
        {

            StringBuilder ssql = new StringBuilder();
            Dictionary<string, dynamic> dictobj = new Dictionary<string, dynamic>();
			//RMA-8753 nshah28(Added by ashish)
            //ssql.Append(" UPDATE POLICY_X_INSURED SET ADDRESS_ID =(SELECT MIN(ADDRESS_ID) FROM ENTITY_X_ADDRESSES WHERE ENTITY_ID = ~INSUREDID~ AND ADDRESS_SEQ_NUM=~ADDRESSSEQNUM~ )");
            ssql.Append(" UPDATE POLICY_X_INSURED SET ADDRESS_ID =(SELECT MIN(EA.ADDRESS_ID) FROM Address A INNER JOIN ENTITY_X_ADDRESSES EA ON EA.ENTITY_ID = ~INSUREDID~ AND A.ADDRESS_SEQ_NUM=~ADDRESSSEQNUM~ )");
            ssql.Append(" WHERE POLICY_ID = ~POLICYID~ AND INSURED_EID = ~INSUREDID~ ");
            dictobj.Add("ADDRESSSEQNUM", iAddressSeqNum);
            dictobj.Add("POLICYID", iAddedPolicyId);
            dictobj.Add("INSUREDID", iInsuredEid);
            try
            {
                DbFactory.ExecuteNonQuery(connectionString, ssql.ToString(), dictobj);

            }
            catch (Exception e) { throw e; }
            finally
            {
                ssql = null;
                dictobj = null;
            }
        }
        //mits 35925 end

        //Start: MITS 31601: Neha Suresh Jain,02/27/2013
        /// <summary>
        /// Read response from POINT and save policy change history data
        /// </summary>
        /// <param name="p_iPolicyId"></param>
        /// <param name="oAcordResponse"></param>
        /// <param name="p_objErrOut"></param>
        public void GetPolicyChangeDateListSaveFormattedResult(int p_iPolicyId, string oAcordResponse, ref BusinessAdaptorErrors p_objErrOut)
        {
            XElement oSaveAcordXML = null;
            XElement oElementNode = null;
            PolicySystemInterface objManager = null;
            string sReasonAmended = string.Empty;
            string sEnterDate = string.Empty;
            string sChangeEffDate = string.Empty;
            try
            {
                oSaveAcordXML = XElement.Parse(oAcordResponse);
                if (oSaveAcordXML != null)
                {
                    objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                    foreach (XElement objElemList in oSaveAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyChangeDateListRs/com.csc_PolicyAmendmentList/com.csc_PolicyAmendment"))
                    {
                        //Reason Amended
                        oElementNode = objElemList.XPathSelectElement("com.csc_ReasonAmended");
                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                        {
                            sReasonAmended = oElementNode.Value;
                        }

                        //Enter Date
                        oElementNode = objElemList.XPathSelectElement("com.csc_EnterDate");
                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                        {
                            sEnterDate = Riskmaster.Common.Conversion.GetDate(oElementNode.Value);
                        }

                        //Change Effective Date
                        oElementNode = objElemList.XPathSelectElement("com.csc_ChangeEffDate");
                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                        {
                            sChangeEffDate = Riskmaster.Common.Conversion.GetDate(oElementNode.Value);
                        }

                        //check if particular history row is already for that policyi, if not present than save
                        objManager.CheckAndSavePolicyAmendHist(p_iPolicyId, sReasonAmended, sEnterDate, sChangeEffDate);
                    }
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oSaveAcordXML != null)
                {
                    oSaveAcordXML = null;
                }
                if (oElementNode != null)
                {
                    oElementNode = null;
                }
                if (objManager != null)
                {
                    objManager = null;
                }
            }
        }
        //End:MITS 31601
        //public bool SaveWorkLossUnit(int p_iPolicyId, ref BusinessAdaptorErrors p_objErrOut)
        //{
        //    bool bResult = false;
        //    PolicySystemInterface objManager = null;
        //    int iPolicyUnitRowId = 0;
        //    PolicyEnquiry objEnReq = null;
        //    PolicyEnquiry objEnRes = null;
        //    string oAcordWorkLossReq = string.Empty;
        //    string oAcordWorkLossRes = string.Empty;
        //    try
        //    {
        //        objManager = new PolicySystemInterface(m_userLogin);
        //        iPolicyUnitRowId = objManager.SaveWorkLossUnit(p_iPolicyId);

        //        if (iPolicyUnitRowId > 0)
        //        {
        //            objEnReq = new PolicyEnquiry();
        //            objEnRes = new PolicyEnquiry();

        //            oAcordWorkLossReq = GetWorkLossCoverageListAcordRequest(objEnReq, ref p_objErrOut);

        //            if (!string.IsNullOrEmpty(oAcordWorkLossReq))
        //            {
        //                oAcordWorkLossRes = GetWorkLossCoverageListAcordResponse(oAcordWorkLossReq, ref p_objErrOut);
        //                if (!string.IsNullOrEmpty(oAcordWorkLossRes))
        //                    bResult = GetWorkLossCoverageListFormattedResult(oAcordWorkLossRes, out objEnRes, ref p_objErrOut);
        //                if (bResult)
        //                {
        //                    //SaveCoverages(objEnRes.WorkLossAcordXML, iPolicyUnitRowId, ref p_objErrOut);
        //                }
        //            }
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
        //    }
        //    finally
        //    {
        //        objManager = null;
        //        if (objEnReq != null)
        //            objEnReq = null;
        //        if (objEnRes != null)
        //            objEnRes = null;
        //    }

        //    return bResult;
        //}

        public int GetAutoUnitDetailSaveFormattedResult(string oAcordResponse, int iPolicyId, ref BusinessAdaptorErrors p_objErrOut, string p_sUnitDesc, string p_sBaseLOB)
        {
            XElement oAcordXML = null;
            XElement oDataModelTemplate = null;
            XElement oSaveAcordXML = null;
            XElement oElement = null;
            XElement oElementNode = null;
            XElement oDataModelElement = null;
            //oSaveResponse = new PolicySaveRequest();
            //oSaveResponse.Result = false;
            string sFileName = "unit_" + userLogin.LoginName + "_tmp.xml";
            PolicySystemInterface objManager = null;
            StringBuilder objBuilder = null;
            int iRecordId = 0;
            string sUnitNumber = string.Empty;
            string sStatUnitNumber = string.Empty;
            string sRiskLoc = string.Empty;
            string sRiskSubLoc = string.Empty;
            string sProductCode = string.Empty;
            string sInsLine = string.Empty;
            int iPolicySystemID = Int32.MinValue; // aaggarwal29: Code mapping change
            string sVehDesc = string.Empty;
            try
            {
                objManager = new PolicySystemInterface(connectionString, base.ClientId);
                oAcordXML = XElement.Parse(oAcordResponse);
                if (oAcordXML != null)
                {
                    objBuilder = new StringBuilder();
                    foreach (XElement objEle in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/AutoLossInfo"))
                    {
                        oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Vehicle", ref p_objErrOut));
                        if (oDataModelTemplate != null)
                        {
                            iPolicySystemID = objManager.GetPolicySystemId(iPolicyId);

                            //oElementNode = objEle.XPathSelectElement("./com.csc_VehicleTypeCd");
                            //if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            //{
                            //    // aaggarwal29: Code mapping change start
                            //   

                            //    iCodeId = (new PolicySystemInterface(connectionString)).GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "UNIT_TYPE_CODE", "VehicleTypeCd on GetAutoUnitDetailSaveFormattedResult", iPolicySystemID.ToString());
                            //    // aaggarwal29: Code mapping change end
                            //    //if (int.Equals(iCodeId, 0))
                            //    //{
                            //    //    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetAutoUnitDetailFormattedResult.NoMappedCodeFoundError"));
                            //    //}

                            //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Vehicle/UnitTypeCode");
                            //    if (oDataModelElement != null)
                            //        oDataModelElement.Attribute("codeid").Value = iCodeId.ToString();

                            //}


                            oElementNode = objEle.XPathSelectElement("./VehInfo/Model");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Vehicle/VehicleModel");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                            }





                            oElement = objEle.XPathSelectElement("./VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId");
                            if (oElement != null)
                            {
                                sUnitNumber = oElement.Value;
                                oElement = null;
                            }
                            oElement = objEle.XPathSelectElement("./VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_StatUnitNumber']/OtherId");
                            if (oElement != null)
                            {
                                sStatUnitNumber = oElement.Value;
                                oElement = null;
                            }
                            oElement = objEle.XPathSelectElement("./VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId");
                            if (oElement != null)
                            {
                                sRiskLoc = oElement.Value;
                                oElement = null;
                            }
                            oElement = objEle.XPathSelectElement("./VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId");
                            if (oElement != null)
                            {

                                sRiskSubLoc = oElement.Value;
                                oElement = null;
                            }
                            //skhare7 Point Policy interface
                            oElement = objEle.XPathSelectElement("./VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId");
                            if (oElement != null)
                            {

                                sProductCode = oElement.Value;
                                oElement = null;
                            }
                            oElement = objEle.XPathSelectElement("./VehInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                            if (oElement != null)
                            {

                                sInsLine = oElement.Value;
                                oElement = null;
                            }
                            sVehDesc = sStatUnitNumber + " - ";
                            //Set VIN value as ="Stat Unit number - Vehicle identification number/Unit List Desc" 
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Vehicle/Vin");
                            if (oDataModelElement != null)
                            {
                                oElementNode = objEle.XPathSelectElement("./com.csc_VehicleIdentificationNumber");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                    oDataModelElement.Value = oElementNode.Value;
                                sVehDesc = sVehDesc + oElementNode.Value;

                            }


                            oElementNode = objEle.XPathSelectElement("./VehInfo/Manufacturer");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Vehicle/VehicleMake");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;
                                sVehDesc = sVehDesc + " " + oElementNode.Value;
                            }

                            oElementNode = objEle.XPathSelectElement("./VehInfo/ModelYear");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Vehicle/VehicleYear");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;
                                sVehDesc = sVehDesc + " " + oElementNode.Value;

                            }


                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Vehicle/VehDesc");
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = sVehDesc;
                            }
                            objManager = new PolicySystemInterface(m_userLogin, base.ClientId);

                            iRecordId = objManager.SaveVehicle(oDataModelTemplate.ToString(), iPolicyId, oAcordResponse);
                            if (iRecordId > 0)
                            {
                                objManager.SavePolicyunitData(iRecordId, "V", sUnitNumber, sRiskLoc, sRiskSubLoc, sProductCode, sInsLine, string.Empty, sStatUnitNumber);
                                iRecordId = objManager.SavePolicyXUnit(iPolicyId, iRecordId, "V", oAcordResponse, iPolicySystemID);
                             //   bool bResult = objManager.SavePolicyDownloadXMLData(iRecordId, oAcordResponse, objManager.GetPolicySystemId(iPolicyId), "POLICY_X_UNIT");
                            }
                        }
                    }



                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElement != null)
                    oElement = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
                if (objBuilder != null)
                    objBuilder = null;

                objManager = null;
            }
            return iRecordId;

        }

        public int GetMRUnitDetailSaveFormattedResult(string oAcordResponse, int iPolicyId, ref BusinessAdaptorErrors p_objErrOut)
        {
            XElement oDataModelTemplate = null;
            XElement oSaveAcordXML = null;
            XElement oElement = null;
            XElement oElementNode = null;
            XElement oDataModelElement = null;
            //oSaveResponse = new PolicySaveRequest();
            //oSaveResponse.Result = false;
            //string sFileName = "unit_" + userLogin.LoginName + "_tmp.xml";
            StringBuilder objBuilder = null;
            IEnumerable<XElement> oElements = null;
            int iRecordId = 0;
            PolicySystemInterface objManager = null;
            string sUnitNo = string.Empty;
            string sInsuranceLine = string.Empty; // bsharma33 MITS # 34196, Insurance line save for manual rated policy
            int iPolicySystemID = Int32.MinValue; // aaggarwal29: Code mapping change
            LocalCache objCache = new LocalCache(m_connectionString, base.ClientId);
            SysSettings objSettings = null;
            try
            {
                oSaveAcordXML = XElement.Parse(oAcordResponse);
                if (oSaveAcordXML != null)
                {
                    objSettings = new SysSettings(m_connectionString, base.ClientId);
                    objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                    oElement = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/InsuredOrPrincipal");
                    if (oElement != null)
                    {
                        oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Entity", ref p_objErrOut));
                        if (oDataModelTemplate != null)
                        {
                            oElements = oElement.XPathSelectElements("./com.csc_InsuredNameIdInfo/ItemIdInfo/OtherIdentifier");
                            foreach (XElement objElement in oElements)
                            {
                                XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                XElement objSubEle = objElement.XPathSelectElement("./OtherId");

                                if ((objEle != null) && (objEle.Value.ToUpper().StartsWith("NAME") || oElements.ElementAt(1) == objElement))
                                {
                                    string[] arrName = objSubEle.Value.Split(' ');

                                    if (arrName.Length > 3)
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = objSubEle.Value.Substring(0, objSubEle.Value.IndexOf(" "));

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = arrName[1];

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = objSubEle.Value.Substring(objSubEle.Value.IndexOf(arrName[1]) + arrName[1].Length);
                                    }
                                    else if (arrName.Length > 1)
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = objSubEle.Value.Substring(0, objSubEle.Value.IndexOf(" "));

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = objSubEle.Value.Substring(objSubEle.Value.IndexOf(" ") + 1);

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = "";
                                    }
                                    else
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = "";

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = "";

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = objSubEle.Value;
                                    }
                                    continue;

                                }

                                //if ((objEle != null) && objEle.Value.ToUpper().StartsWith("Address/Class Code"))
                                //{
                                //    string[] arrName = objSubEle.Value.Split(' ');
                                //    if (arrName.Length > 0)
                                //    {
                                //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                //        if (oDataModelElement != null)
                                //            oDataModelElement.Value = objSubEle.Value.Substring(0, objSubEle.Value.IndexOf(arrName[arrName.Length - 1]));

                                //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                //        if (oDataModelElement != null)
                                //            oDataModelElement.Value = arrName[arrName.Length - 1];

                                //    }
                                //    else
                                //    {
                                //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                //        if (oDataModelElement != null)
                                //            oDataModelElement.Value = objSubEle.Value;
                                //    }
                                //}
                                if ((objEle != null) && objEle.Value.ToUpper().StartsWith("ADDRESS"))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/Addr1");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = objSubEle.Value;
                                    continue;
                                }

                                if ((objEle != null) && objEle.Value.ToUpper().StartsWith("CITY"))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/City");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = objSubEle.Value.Substring(0, objSubEle.Value.LastIndexOf(','));

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/StateId");
                                    if (oDataModelElement != null)
                                    {
                                        string oStateCode = objSubEle.Value.Substring(objSubEle.Value.LastIndexOf(',') + 1).Trim();
                                        if (!string.IsNullOrEmpty(oStateCode))
                                        {
                                            // aaggarwal29: Code mapping changes start
                                            iPolicySystemID = objManager.GetPolicySystemId(iPolicyId);
                                            oDataModelElement.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(oStateCode, "STATES", "StateId on GetMRUnitDetailSaveFormattedResult", iPolicySystemID.ToString()).ToString();
                                            // aaggarwal29: Code mapping changes end
                                        }
                                    }
                                    continue;
                                }

                                if ((objEle != null) && objEle.Value.ToUpper().StartsWith("POSTAL CODE"))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/ZipCode");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = objSubEle.Value;
                                    continue;
                                }

                                //dbisht start mits 35655
                                if ((objEle != null) && objEle.Value.ToUpper().StartsWith("DESCRIPTION / USE 1:"))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/ReferenceNumber");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = objSubEle.Value;
                                    continue;
                                }
                                // dbisht6 end


                                objEle = null;
                                objSubEle = null;

                            }
                            oElements = null;

                            oElement = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/InsuredOrPrincipal/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId");
                            if (oElement != null)
                            {
                                sUnitNo = oElement.Value;
                                oElement = null;
                            }
                            //Start Changes : bsharma33 MITS # 34196 Insurance line save for manual rated policy
                            oElement = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/InsuredOrPrincipal/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                            if (oElement != null)
                            {
                                sInsuranceLine = oElement.Value;
                                oElement = null;
                            }
                            //End Changes : bsharma33 MITS # 34196
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/EntityTableId");
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = objCache.GetTableId("POLICY_INSURED").ToString();
                            }

                            //objManager = new PolicySystemInterface(m_userLogin);
                            iRecordId = objManager.SaveStatUnit(oDataModelTemplate.ToString(), iPolicyId, iPolicySystemID, sUnitNo, objCache.GetTableId("POLICY_INSURED"));
                            if (iRecordId > 0)
                            {
                                objManager.SavePolicyunitData(iRecordId, "SU", sUnitNo, string.Empty, string.Empty, string.Empty, sInsuranceLine, string.Empty, sUnitNo); //bsharma33 MITS # 34196 Insurance line save for manual rated policy
                                iRecordId = objManager.SavePolicyXUnit(iPolicyId, iRecordId, "SU", oAcordResponse, iPolicySystemID);
                               // bool bResult = objManager.SavePolicyDownloadXMLData(iRecordId, oAcordResponse, objManager.GetPolicySystemId(iPolicyId), "POLICY_X_UNIT");
                            }
                            //WriteFormattedXmlToFile(sFileName, "<Entities>" + objBuilder.ToString() + "</Entities>", ref p_objErrOut);
                        }
                    }
                }
            }

            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElement != null)
                    oElement = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
                if (objBuilder != null)
                    objBuilder = null;
                if (objCache != null)
                    objCache = null;
                objSettings = null;
            }

            return iRecordId;
        }

        public int GetPropertyDetailSaveFormattedResult(string oAcordResponse, int iPolicyId, ref BusinessAdaptorErrors p_objErrOut, string p_sUnitDesc)
        {
            XElement oAcordXML = null;
            XElement oDataModelTemplate = null;
            XElement oSaveAcordXML = null;
            XElement oElement = null;
            XElement oElementNode = null;
            XElement oDataModelElement = null;
            //oSaveResponse = new PolicySaveRequest();
            //oSaveResponse.Result = false;
            //string sFileName = "property_" + userLogin.LoginName + "_tmp.xml";
            StringBuilder objBuilder = null;
            PolicySystemInterface objManager = null;
            int iRecordId = 0;
            string sUnitNumber = string.Empty;
            string sStatUnitNumber = string.Empty;
            string sRiskLoc = string.Empty;
            string sRiskSubLoc = string.Empty;
            string sProductCode = string.Empty;
            string sInsLine = string.Empty;
            int iPolicySystemId = Int32.MinValue; // aaggarwal29: Code mapping change
            LocalCache objCache = null;
            string sPIN = string.Empty;
            try
            {
                oAcordXML = XElement.Parse(oAcordResponse);
                if (oAcordXML != null)
                {
                    objBuilder = new StringBuilder();
                    objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                    foreach (XElement objEle in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo"))
                    {

                        oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Property", ref p_objErrOut));
                        if (oDataModelTemplate != null)
                        {



                            oElementNode = oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo/com.csc_UnitLossData/com.csc_UnitDescription/com.csc_UnitDesc");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/Addr1");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                                sPIN = oElementNode.Value;
                            }
                            oElementNode = oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo/com.csc_UnitLossData/com.csc_UnitDescription/com.csc_Zipcode");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/ZipCode");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;
                            }

                            oElementNode = oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo/com.csc_UnitLossData/com.csc_UnitDescription/com.csc_UnitCity");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/City");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                                if (string.IsNullOrEmpty(sPIN))
                                    sPIN = oElementNode.Value;
                                else
                                    sPIN = sPIN + " " + oElementNode.Value;

                            }

                            oElementNode = oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo//ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/StateId");
                                iPolicySystemId = objManager.GetPolicySystemId(iPolicyId);
                                string sAbbr = string.Empty;
                                string sStateDesc = string.Empty;

                                int iStateId = objManager.GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "STATES", "RateState on GetPropertyDetailSaveFormattedResult", iPolicySystemId.ToString());
                                if (oDataModelElement != null)
                                {
                                    // aaggarwal29: Code mapping changes start

                                    oDataModelElement.Attribute("codeid").Value = iStateId.ToString();
                                    //aaggarwal29: Code mapping changes end
                                }

                                objCache = new LocalCache(m_connectionString, base.ClientId);

                                objCache.GetStateInfo(iStateId, ref sAbbr, ref sStateDesc);
                                if (string.IsNullOrEmpty(sPIN))
                                    sPIN = sStateDesc;
                                else
                                    sPIN = sPIN + " " + sStateDesc;
                            }

                            oElement = oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId");
                            if (oElement != null)
                            {
                                sUnitNumber = oElement.Value;
                                oElement = null;
                            }
                            oElement = oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_StatUnitNumber']/OtherId");
                            if (oElement != null)
                            {
                                sStatUnitNumber = oElement.Value;
                                oElement = null;
                            }
                            oElement = oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId");
                            if (oElement != null)
                            {
                                sRiskLoc = oElement.Value;
                                oElement = null;
                            }
                            oElement = oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId");
                            if (oElement != null)
                            {
                                sRiskSubLoc = oElement.Value;
                                oElement = null;
                            }
                            oElement = oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId");
                            if (oElement != null)
                            {
                                sProductCode = oElement.Value;
                                oElement = null;
                            }
                            oElement = oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                            if (oElement != null)
                            {
                                sInsLine = oElement.Value;
                                oElement = null;
                            }
                            if (string.IsNullOrEmpty(sPIN))
                            {
                                sPIN = sStatUnitNumber;
                            }

                            // Set PIN value as = "Stat unit number - Unit List desc" instead of getting from unit detail response 
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PropertyUnit/Pin");
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = sStatUnitNumber + " - " + p_sUnitDesc;

                            }



                            // objManager = new PolicySystemInterface(m_userLogin);
                            iRecordId = objManager.SaveProperty(oDataModelTemplate.ToString(), iPolicyId, oAcordResponse);
                            if (iRecordId > 0)
                            {
                                objManager.SavePolicyunitData(iRecordId, "P", sUnitNumber, sRiskLoc, sRiskSubLoc, sProductCode, sInsLine, string.Empty, sStatUnitNumber);
                                iRecordId = objManager.SavePolicyXUnit(iPolicyId, iRecordId, "P", oAcordResponse, iPolicySystemId);
                               // bool bResult = objManager.SavePolicyDownloadXMLData(iRecordId, oAcordResponse, objManager.GetPolicySystemId(iPolicyId), "POLICY_X_UNIT");
                            }
                        }


                    }
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElement != null)
                    oElement = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
                if (objBuilder != null)
                    objBuilder = null;
                objManager = null;

                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }

            return iRecordId;
        }

        public int GetEntitySaveFormattedResult(string oAcordResponse, SaveDownloadOptions oSaveDownload, string sSequenceNumber, string UnitRowId, ref BusinessAdaptorErrors p_objErrOut)
        {
            XElement oDataModelTemplate = null;
            XElement oSaveAcordXML = null;
            XElement oElement = null;
            XElement oElementNode = null;
            XElement oDataModelElement = null;
            //oSaveResponse = new PolicySaveRequest();
            //oSaveResponse.Result = false;
            //string sFileName = "entity_" + userLogin.LoginName + "_tmp.xml";
            int iRecordId = 0;
            PolicySystemInterface objManager = null;
            IEnumerable<XElement> oElements = null;
            string sRoleCode = string.Empty;
            string sEntityBusinessRole = string.Empty;
            string sExculdeEntities = string.Empty;
            // mits 35925 start
            int iAddressSeqNum = 0;
            int iClientSeqNum = 0;
            XElement oEntityAddressDatamodelTemplate = null;
            int iAddressId = 0;
            //Start -  Changed by Nikhil on 11/03/14            
            //bool bClientFile;
            bool bClientFile = false;
            //end -  Changed by Nikhil on 11/03/14
            //mits 35925 end
            //LocalCache objCache = null;
            SysSettings objSettings = null;
            try
            {
                oSaveAcordXML = XElement.Parse(oAcordResponse);
                if (oSaveAcordXML != null)
                {
                    objSettings = new SysSettings(m_connectionString,base.ClientId);
                    oElement = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInterestDetailRs/InsuredOrPrincipal");
                    if (oElement != null)
                    {
                        oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Entity", ref p_objErrOut));
                        if (oDataModelTemplate != null)
                        {
                            //  objCache = new LocalCache(m_connectionString);
                            objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                            sExculdeEntities = objManager.GetPolicySystemParamteres("EXCLUDE_INTEREST", oSaveDownload.PolicySystemId).Replace(" ", "");
                            sEntityBusinessRole = objManager.GetPolicySystemParamteres("BUSINESS_ROLES", oSaveDownload.PolicySystemId).Replace(" ", "");


                            oElements = oElement.XPathSelectElements("./GeneralPartyInfo/NameInfo/TaxIdentity");
                            foreach (XElement objElement in oElements)
                            {
                                XElement objEle = objElement.XPathSelectElement("./TaxIdTypeCd");
                                XElement objSubEle = objElement.XPathSelectElement("./TaxId");

                                if ((objEle != null) && (string.Equals(objEle.Value, "SSN", StringComparison.InvariantCultureIgnoreCase)))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/TaxId");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = objSubEle.Value;
                                }
                                objEle = null;
                                objSubEle = null;

                            }
                            oElements = null;
                            //if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            //{

                            //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/TaxId");
                            //    if (oDataModelElement != null)
                            //        oDataModelElement.Value = oElementNode.Value;
                            //}
                            //else
                            //{
                            //    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetEntitySaveFormattedResult.NoTaxIdFound"));
                            //}

                            oElementNode = oElement.XPathSelectElement("./InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd");
                            if (oElementNode != null)
                            {
                                sRoleCode = oElementNode.Value;
                            }

                            if (!string.IsNullOrEmpty(sEntityBusinessRole) && !string.IsNullOrEmpty(sRoleCode) && sExculdeEntities.Split(',').Contains(sRoleCode))
                            {
                                return 0;
                            }

                            if (!string.IsNullOrEmpty(sEntityBusinessRole) && !string.IsNullOrEmpty(sRoleCode) && sEntityBusinessRole.Split(',').Contains(sRoleCode))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;
                            }
                            else
                            {
                                oElementNode = oElement.XPathSelectElement("./GeneralPartyInfo/NameInfo/CommlName/CommercialName");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    //string[] arrName = oElementNode.Value.Split(' ');
                                    //if (arrName.Length > 0)
                                    //{
                                    //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                    //    if (oDataModelElement != null)
                                    //        oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(arrName[arrName.Length - 1]));

                                    //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                    //    if (oDataModelElement != null)
                                    //        oDataModelElement.Value = arrName[arrName.Length - 1];

                                    //}
                                    //else
                                    //{
                                    //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                    //    if (oDataModelElement != null)
                                    //        oDataModelElement.Value = oElementNode.Value;
                                    //}

                                    //objCache = new LocalCache(m_connectionString);
                                    //sEntityBusinessRole = objCache.GetPolicyInterfaceConfigSettings("PolicyInterface", "EntityBusinessRole");

                                    string[] arrName = oElementNode.Value.Split(' ');

                                    if (arrName.Length > 3)
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(" "));

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = arrName[1];

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value.Substring(oElementNode.Value.IndexOf(arrName[1]) + arrName[1].Length);
                                    }
                                    else if (arrName.Length > 1)
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(" "));

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value.Substring(oElementNode.Value.IndexOf(" ") + 1);

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = "";
                                    }
                                    else
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = "";

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = "";

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value;
                                    }
                                }
                            }

                            oElementNode = oElement.XPathSelectElement("./GeneralPartyInfo/Addr/Addr1");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/Addr1");
								//RMA-8753 nshah28(Added by ashish)
                                //oEntityAddressDatamodelTemplate = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Addr1");// mits 35925
                                oEntityAddressDatamodelTemplate = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Address/Addr1");
                                if (oDataModelElement != null)
                                {
                                    oDataModelElement.Value = oElementNode.Value;
                                }
                                if (oEntityAddressDatamodelTemplate != null)
                                    oEntityAddressDatamodelTemplate.Value = oElementNode.Value; //mits 35925
                            }

                            oElementNode = oElement.XPathSelectElement("./GeneralPartyInfo/Addr/Addr2");
                            if (oElementNode != null)
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/Addr2");
								//RMA-8753 nshah28(Added by ashish)
                                //oEntityAddressDatamodelTemplate = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Addr2");//mits 35925
                                oEntityAddressDatamodelTemplate = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Address/Addr2");
                                if (oDataModelElement != null)
                                {
                                    oDataModelElement.Value = oElementNode.Value;
                                }
                                if (oEntityAddressDatamodelTemplate != null)
                                    oEntityAddressDatamodelTemplate.Value = oElementNode.Value; // mits 35925
                            }

                            oElementNode = oElement.XPathSelectElement("./GeneralPartyInfo/Addr/PostalCode");
                            if (oElementNode != null)
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/ZipCode");
								//RMA-8753 nshah28(Added by ashish)
                                //oEntityAddressDatamodelTemplate = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/ZipCode");// mits 35925
                                oEntityAddressDatamodelTemplate = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Address/ZipCode");
                                if (oDataModelElement != null)
                                {
                                    oDataModelElement.Value = oElementNode.Value;

                                }
                                if (oEntityAddressDatamodelTemplate != null)
                                    oEntityAddressDatamodelTemplate.Value = oElementNode.Value; // mits 35925
                            }
                            oElementNode = oElement.XPathSelectElement("./GeneralPartyInfo/Addr/City");
                            if (oElementNode != null)
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/City");
								//RMA-8753 nshah28(Added by ashish)
                                //oEntityAddressDatamodelTemplate = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/City");// mits 35925
                                oEntityAddressDatamodelTemplate = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Address/City");
                                if (oDataModelElement != null)
                                {
                                    oDataModelElement.Value = oElementNode.Value;

                                }
                                if (oEntityAddressDatamodelTemplate != null)
                                    oEntityAddressDatamodelTemplate.Value = oElementNode.Value; // mits 35925
                            }



                            oElementNode = oElement.XPathSelectElement("./GeneralPartyInfo/Addr/StateProvCd");
                            if (oElementNode != null)
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/StateId");
                                oEntityAddressDatamodelTemplate = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Address/State");//RMA-8753 nshah28(Added by ashish)
                                if (oDataModelElement != null && (!string.IsNullOrEmpty(oElementNode.Value)))
                                    //     oDataModelElement.Value =
                                    oDataModelElement.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "STATES", "StateProvCd on GetEntitySaveFormattedResult", oSaveDownload.PolicySystemId.ToString()).ToString(); // aaggarwal29: Code mapping change
									//RMA-8753 nshah28(Added by ashish)
                                if (oEntityAddressDatamodelTemplate != null && (!string.IsNullOrEmpty(oElementNode.Value)))
                                    oEntityAddressDatamodelTemplate.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "STATES", "StateProvCd on GetEntitySaveFormattedResult", oSaveDownload.PolicySystemId.ToString()).ToString();
                            }
                            oElementNode = oElement.XPathSelectElement("./GeneralPartyInfo/Addr/County");
                            if (oElementNode != null)
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/County");
								//RMA-8753 nshah28(Added by ashish)
                                //oEntityAddressDatamodelTemplate = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/County");// mits 35925
                                oEntityAddressDatamodelTemplate = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Address/County");
                                if (oDataModelElement != null)
                                {
                                    oDataModelElement.Value = oElementNode.Value;

                                }
                                if (oEntityAddressDatamodelTemplate != null)
                                    oEntityAddressDatamodelTemplate.Value = oElementNode.Value; // mits 35925
                            }


                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/EntityTableId");
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = oSaveDownload.EntityRole;
                            }
                            // mits 35925 start
                            oElementNode = oElement.XPathSelectElement("./ItemIdInfo//OtherIdentifier[OtherIdTypeCd='com.csc_AddressSeqNum']/OtherId");
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/AddressSequenceNumber");
							//RMA-8753 nshah28(Added by ashish)
                            //oEntityAddressDatamodelTemplate = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/AddressSeqNum");
                            oEntityAddressDatamodelTemplate = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Address/AddressSeqNum");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                iAddressSeqNum = Convert.ToInt32(oElementNode.Value.ToString());
                                if (oDataModelElement != null)
                                {
                                    oDataModelElement.Value = oElementNode.Value;

                                }
                                if (oEntityAddressDatamodelTemplate != null)
                                    oEntityAddressDatamodelTemplate.Value = oElementNode.Value; // mits 35925
                            }
                            oElementNode = oElement.XPathSelectElement("./ItemIdInfo//OtherIdentifier[OtherIdTypeCd='com.csc_ClientSeqNum']/OtherId");
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/ClientSequenceNumber");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                iClientSeqNum = Convert.ToInt32(oElementNode.Value.ToString());
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;
                            }
                            oElementNode = null;

                            //mits 35925 end
                            //start - Changed by Nikhil on 11/03/14.Null check included
                            // bClientFile = Convert.ToBoolean(oSaveAcordXML.XPathSelectElement("./SignonRs/ClientApp/com.csc_ClientFile").Value);
                            if (oSaveAcordXML.XPathSelectElement("./SignonRs/ClientApp/com.csc_ClientFile") != null)
                            {
                                bClientFile = Convert.ToBoolean(oSaveAcordXML.XPathSelectElement("./SignonRs/ClientApp/com.csc_ClientFile").Value);
                            }
                            //end - Changed by Nikhil on 11/03/14.Null check included

                            
                            iRecordId = objManager.SaveEntity(oDataModelTemplate.ToString(), oSaveDownload.PolicyId, oSaveDownload.Mode, ref iAddressId, bClientFile,Conversion.ConvertStrToInteger(oSaveDownload.EntityRole));
                            
                            iRecordId = objManager.SavePolicyXEntity(oSaveDownload.PolicyId, iRecordId, Conversion.ConvertStrToInteger(oSaveDownload.EntityRole), Conversion.ConvertStrToInteger(UnitRowId), sRoleCode, iAddressId, oAcordResponse);
                            //WriteFormattedXmlToFile(sFileName, "<Entities>" + objBuilder.ToString() + "</Entities>", ref p_objErrOut);
                          //  objManager.SavePolicyDownloadXMLData(iRecordId, oAcordResponse, objManager.GetPolicySystemId(oSaveDownload.PolicyId), "POLICY_X_ENTITY");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                //   oSaveResponse.Result = false;

            }
            finally
            {
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElement != null)
                    oElement = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;

                objSettings = null;
                objManager = null;

                //if (objCache != null)
                //{
                //    objCache.Dispose();
                //    objCache = null;
                //}
            }

            return iRecordId;
        }

        public int GetDriverDetailSaveFormattedResult(string oAcordResponse, SaveDownloadOptions oSaveDownload, ref BusinessAdaptorErrors p_objErrOut)
        {
            XElement oDataModelTemplate = null;
            XElement oSaveAcordXML = null;
            XElement oElement = null;
            XElement oElementNode = null;
            XElement oDataModelElement = null;
            //oSaveResponse = new PolicySaveRequest();
            //oSaveResponse.Result = false;
            //string sFileName = "entity_" + userLogin.LoginName + "_tmp.xml";
            int iRecordId = 0;
            PolicySystemInterface objManager = null;
            IEnumerable<XElement> oElements = null;

            string sLicenceDate = string.Empty;
            string sDriversLicNo = string.Empty;
            LocalCache objCache = null;
            int iMaritalStatus = 0;
            // mits 35925
            int iAddressSeqNum = -1;
            int iAddressId = 0;
            //Start -  Changed by Nikhil on 11/03/14            
            //bool bClientFile;
            bool bClientFile = false;
            //end -  Changed by Nikhil on 11/03/14
            string sLocationCompany = null;
            string sMasterCompany = null;
            string sPolicySymbol = null;
            string sPolicyNumber = null;
            string sPolicyModule = null;
            string sInsuranceLine = null;
            string sRiskLocationNumber = null;
            string sRiskSubLocationNumber = null;
            string sProduct = null;
            string sDriverID = null;
            string sRecordStatus = null;
            XElement oEle = null;
            SysSettings objSettings = null;
            //mits 35925 end
            try
            {
                oSaveAcordXML = XElement.Parse(oAcordResponse);
                if (oSaveAcordXML != null)
                {
                    objSettings = new SysSettings(m_connectionString, base.ClientId);
                    oElement = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_DriverDetailRs/DriverInfo");
                    if (oElement != null)
                    {
                        oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Entity", ref p_objErrOut));
                        if (oDataModelTemplate != null)
                        {
                            oElementNode = oElement.XPathSelectElement("./PersonInfo/NameInfo/PersonName/GivenName");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {

                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;


                            }
                            oElementNode = null;
                            oElementNode = oElement.XPathSelectElement("./PersonInfo/NameInfo/PersonName/Surname");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {

                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                            }
                            oElementNode = null;
                            oElementNode = oElement.XPathSelectElement("./PersonInfo/NameInfo/TaxIdentity/TaxId");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {

                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/TaxId");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                            }
                            oElementNode = null;
                            objCache = new LocalCache(m_connectionString, base.ClientId);
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/EntityTableId");
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = objCache.GetTableId("DRIVERS").ToString();
                            }
                            oElementNode = null;

                            oElementNode = oElement.XPathSelectElement("./PersonInfo/BirthDt");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {

                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/BirthDate");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                            }
                            oElementNode = null;
                            oElementNode = oElement.XPathSelectElement("./PersonInfo/GenderCd");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {

                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/SexCode");
                                if (oDataModelElement != null)
                                    oDataModelElement.Attribute("codeid").Value = objCache.GetCodeId(oElementNode.Value, "SEX_CODE").ToString();

                            }
                            oElementNode = null;
                            // mits 35925

                            oEle = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs");
                            sDriverID = oEle.XPathSelectElement("./com.csc_DriverDetailRs/DriverInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_DriverId']/OtherId").Value.ToString();
                            sInsuranceLine = oEle.XPathSelectElement("./com.csc_DriverDetailRs/DriverInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLine']/OtherId").Value.ToString();
                            sProduct = oEle.XPathSelectElement("./com.csc_DriverDetailRs/DriverInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId").Value.ToString();
                            sRiskLocationNumber = oEle.XPathSelectElement("./com.csc_DriverDetailRs/DriverInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLocationNumber']/OtherId").Value.ToString();
                            sRiskSubLocationNumber = oEle.XPathSelectElement("./com.csc_DriverDetailRs/DriverInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLocationNumber']/OtherId").Value.ToString();
                            sLocationCompany = oEle.XPathSelectElement("./com.csc_DriverDetailRs/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId").Value.ToString();
                            sMasterCompany = oEle.XPathSelectElement("./com.csc_DriverDetailRs/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId").Value.ToString();
                            sPolicyModule = oEle.XPathSelectElement("./com.csc_DriverDetailRs/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId").Value.ToString();
                            sRecordStatus = oEle.XPathSelectElement("./com.csc_DriverDetailRs/DriverInfo/com.csc_DriverStatus").Value.ToString();
                            sPolicySymbol = oEle.XPathSelectElement("./com.csc_DriverDetailRs/com.csc_PolicyLevel/CompanyProductCd").Value.ToString();
                            sPolicyNumber = oEle.XPathSelectElement("./com.csc_DriverDetailRs/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber").Value.ToString();
                            oDataModelTemplate.Add(new XElement("com.csc_DriverDupCheckFields",
                                                  new XElement("LocationCompany", sLocationCompany),
                                                  new XElement("MasterCompany", sMasterCompany),
                                                  new XElement("PolicySymbol", sPolicySymbol),
                                                  new XElement("PolicyNumber", sPolicyNumber),
                                                  new XElement("PolicyModule", sPolicyModule),
                                                  new XElement("InsuranceLine", sInsuranceLine),
                                                  new XElement("RiskLocationNumber", sRiskLocationNumber),
                                                  new XElement("RiskSubLocationNumber", sRiskSubLocationNumber),
                                                  new XElement("Product", sProduct),
                                                  new XElement("DriverID", sDriverID),
                                                  new XElement("RecordStatus", sRecordStatus)
                                                              )
                                                           );

                            //mits 35925 end

                            objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                            //start - Changed by Nikhil on 11/03/14.Null check included
                            // bClientFile = Convert.ToBoolean(oSaveAcordXML.XPathSelectElement("./SignonRs/ClientApp/com.csc_ClientFile").Value);
                            if (oSaveAcordXML.XPathSelectElement("./SignonRs/ClientApp/com.csc_ClientFile") != null)
                            {
                                bClientFile = Convert.ToBoolean(oSaveAcordXML.XPathSelectElement("./SignonRs/ClientApp/com.csc_ClientFile").Value);
                            }
                            //end - Changed by Nikhil on 11/03/14.Null check included
                            
                            iRecordId = objManager.SaveEntity(oDataModelTemplate.ToString(), oSaveDownload.PolicyId, "driver", ref iAddressId, bClientFile, objCache.GetTableId("DRIVERS"));

                            oElementNode = oElement.XPathSelectElement("./License/LicensePermitNumber");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                sDriversLicNo = oElementNode.Value;
                            }
                            oElementNode = null;
                            oElementNode = oElement.XPathSelectElement("./License/LicensedDt");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                sLicenceDate = oElementNode.Value;
                            }
                            oElementNode = null;


                            oElementNode = oElement.XPathSelectElement("./PersonInfo/MaritalStatusCd");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                iMaritalStatus = objCache.GetCodeId(oElementNode.Value, "MARITAL_STATUS");


                            }
                            oElementNode = null;

                            //oElements = oElement.XPathSelectElements("./ItemIdInfo/OtherIdentifier");

                            //if (oElements != null)
                            //{
                            //    foreach (XElement objElement in oElements)
                            //    {
                            //        XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                            //        XElement oSubElement = objElement.XPathSelectElement("./OtherId");
                            //        if (objEle != null && string.Equals(objEle.Value, "com.csc_DriverType", StringComparison.InvariantCultureIgnoreCase))
                            //        {

                            //            sDriverType = oSubElement.Value;

                            //        }
                            //    }
                            //}
                            objManager.SaveDriver(sLicenceDate, sDriversLicNo, iRecordId, oSaveDownload.PolicySystemId, iMaritalStatus, oDataModelTemplate.XPathSelectElement("./com.csc_DriverDupCheckFields"));// aaggarwal29: Code mapping change

                            iRecordId = objManager.SavePolicyXEntity(oSaveDownload.PolicyId, iRecordId, objCache.GetTableId("DRIVERS"), 0, "driver", iAddressId,oAcordResponse);
                            //objManager.SavePolicyDownloadXMLData(iRecordId, oAcordResponse, objManager.GetPolicySystemId(oSaveDownload.PolicyId), "POLICY_X_ENTITY");
                            //WriteFormattedXmlToFile(sFileName, "<Entities>" + objBuilder.ToString() + "</Entities>", ref p_objErrOut);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                //   oSaveResponse.Result = false;

            }
            finally
            {
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElement != null)
                    oElement = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
                if (objCache != null)
                    objCache.Dispose();
                objManager = null;
                objSettings = null;
            }

            return iRecordId;
        }

        public void GetInterestListFormattedResult(string oAcordResponse, ref BusinessAdaptorErrors p_objErrOut, int iPolicySystemId)
        {
            XElement oDataModelTemplate = null;
            XElement oSaveAcordXML = null;
            XElement oElement = null;
            XElement oElementNode = null;
            XElement oDataModelElement = null;

            string sFileName = "entity_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml";
            StringBuilder objBuilder = null;
            IEnumerable<XElement> oElements = null;
            string sRoleCode = string.Empty;
            int iSequenceNumber = 1;
            //  LocalCache objCache = null;
            string sEntityBusinessRole = string.Empty;
            string sExculdeEntities = string.Empty;
            PolicySystemInterface objManager = null;
            try
            {
                oSaveAcordXML = XElement.Parse(oAcordResponse);
                DeleteFile(sFileName);
                if (oSaveAcordXML != null)
                {
                    objBuilder = new StringBuilder();
                    oElements = oSaveAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInterestListRs/InsuredOrPrincipal");
                    if ((oElements != null) && (oElements.Count() > 0))
                    {
                        //   objCache = new LocalCache(m_connectionString);
                        objManager = new PolicySystemInterface(m_connectionString, base.ClientId);
                        sEntityBusinessRole = objManager.GetPolicySystemParamteres("BUSINESS_ROLES", iPolicySystemId).Replace(" ", "");
                        sExculdeEntities = objManager.GetPolicySystemParamteres("EXCLUDE_INTEREST", iPolicySystemId).Replace(" ", "");

                        foreach (XElement objELe in oElements)
                        {
                            oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("EntityList", ref p_objErrOut));
                            if (oDataModelTemplate != null)
                            {
                                oElementNode = objELe.XPathSelectElement("./GeneralPartyInfo/TaxIdentity");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/TaxId");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }


                                oElementNode = objELe.XPathSelectElement("./InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/RoleCd");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;

                                    sRoleCode = oElementNode.Value;
                                }

                                if (!string.IsNullOrEmpty(sExculdeEntities) && !string.IsNullOrEmpty(sRoleCode) && sExculdeEntities.Split(',').Contains(sRoleCode))
                                {
                                    continue;
                                }

                                oElementNode = objELe.XPathSelectElement("./GeneralPartyInfo/NameInfo/CommlName/CommercialName");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    //string[] arrName = oElementNode.Value.Split(' ');
                                    //if (arrName.Length > 0)
                                    //{
                                    //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                    //    if (oDataModelElement != null)
                                    //        oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(arrName[arrName.Length - 1]));

                                    //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                    //    if (oDataModelElement != null)
                                    //        oDataModelElement.Value = arrName[arrName.Length - 1];

                                    //}
                                    //else
                                    //{
                                    //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                    //    if (oDataModelElement != null)
                                    //        oDataModelElement.Value = oElementNode.Value;
                                    //}

                                    if (!string.IsNullOrEmpty(sEntityBusinessRole) && !string.IsNullOrEmpty(sRoleCode) && sEntityBusinessRole.Split(',').Contains(sRoleCode))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value;
                                    }
                                    else
                                    {
                                        string[] arrName = oElementNode.Value.Split(' ');

                                        if (arrName.Length > 3)
                                        {
                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(" "));

                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = arrName[1];

                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = oElementNode.Value.Substring(oElementNode.Value.IndexOf(arrName[1]) + arrName[1].Length);
                                        }
                                        else if (arrName.Length > 1)
                                        {
                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(" "));

                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = oElementNode.Value.Substring(oElementNode.Value.IndexOf(" ") + 1);

                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = "";
                                        }
                                        else
                                        {
                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = "";

                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = "";

                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = oElementNode.Value;
                                        }
                                    }
                                }


                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/SequenceNumber");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = iSequenceNumber.ToString();

                                // oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");

                                oElementNode = objELe.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Location']/OtherId");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/Location");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }


                                oElementNode = objELe.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_SeqNum']/OtherId");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/PointSeqNumber");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                // mits 35925
                                oElementNode = objELe.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_AddressSeqNum']/OtherId");

                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/AddressSequenceNumber");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objELe.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_ClientSeqNum']/OtherId");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/ClientSequenceNumber");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }
                                oElementNode = null;

                                //mits 35925 end

                                iSequenceNumber++;
                                //else
                                //{
                                //    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetEntitySaveFormattedResult.NoTaxIdFound"));
                                //}

                                //oElementNode = oElementNode.Parent.XPathSelectElement("OtherId");
                                //if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                //{
                                //    string[] arrName = oElementNode.Value.Split(' ');
                                //    if (arrName.Length > 0)
                                //    {
                                //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                //        if (oDataModelElement != null)
                                //            oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(arrName[arrName.Length - 1]));

                                //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                //        if (oDataModelElement != null)
                                //            oDataModelElement.Value = arrName[arrName.Length - 1];

                                //    }
                                //    else
                                //    {
                                //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                //        if (oDataModelElement != null)
                                //            oDataModelElement.Value = oElementNode.Value;
                                //    }
                                //}

                                objBuilder.Append(oDataModelTemplate.ToString());

                                //oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/com.csc_TaxId");

                                //if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                //{

                                //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/TaxId");
                                //    if (oDataModelElement != null)
                                //        oDataModelElement.Value = oElementNode.Value;
                                //}
                                //else
                                //{
                                //    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetEntitySaveFormattedResult.NoTaxIdFound"));
                                //}

                                //oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/com.csc_LongName");
                                //if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                //{
                                //    string[] arrName = oElementNode.Value.Split(' ');
                                //    if (arrName.Length > 0)
                                //    {
                                //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                //        if (oDataModelElement != null)
                                //            oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(arrName[arrName.Length - 1]));

                                //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                //        if (oDataModelElement != null)
                                //            oDataModelElement.Value = arrName[arrName.Length - 1];

                                //    }
                                //    else
                                //    {
                                //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                //        if (oDataModelElement != null)
                                //            oDataModelElement.Value = oElementNode.Value;
                                //    }
                                //}
                                //else
                                //{
                                //    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetEntitySaveFormattedResult.NoLastNameFound"));
                                //}
                                //   objBuilder.Append(oDataModelTemplate.ToString());


                            }
                        }
                        if (!string.IsNullOrEmpty(objBuilder.ToString()))
                            WriteFormattedXmlToFile(sFileName, "<Entities>" + objBuilder.ToString() + "</Entities>", ref p_objErrOut);
                    }
                }

            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);


            }
            finally
            {
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElement != null)
                    oElement = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
                if (objBuilder != null)
                    objBuilder = null;
                //if (objCache != null)
                //{
                //    objCache.Dispose();
                //    objCache = null;
                //}

                objManager = null;
            }


        }

        public void GetDriverListFormattedResult(string oAcordResponse, ref BusinessAdaptorErrors p_objErrOut)
        {
            XElement oDataModelTemplate = null;
            XElement oSaveAcordXML = null;
            XElement oElement = null;
            XElement oElementNode = null;
            XElement oDataModelElement = null;
            //oSaveResponse = new PolicySaveRequest();
            //oSaveResponse.Result = false;
            string sFileName = "driver_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml";
            StringBuilder objBuilder = null;
            IEnumerable<XElement> oElements = null;
            int iSequenceNumber = 1;
            try
            {
                oSaveAcordXML = XElement.Parse(oAcordResponse);
                DeleteFile(sFileName);
                if (oSaveAcordXML != null)
                {
                    objBuilder = new StringBuilder();
                    oElements = oSaveAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_DriverListRs/DriverInfo");
                    if ((oElements != null) && (oElements.Count() > 0))
                    {
                        foreach (XElement objELe in oElements)
                        {
                            oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("EntityList", ref p_objErrOut));
                            if (oDataModelTemplate != null)
                            {
                                //oElementNode = objELe.XPathSelectElement("./PersonInfo/TaxIdentity");
                                //if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                //{

                                //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/TaxId");
                                //    if (oDataModelElement != null)
                                //        oDataModelElement.Value = oElementNode.Value;
                                //}

                                oElementNode = objELe.XPathSelectElement("./PersonInfo/NameInfo/CommlName/CommercialName");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    //string[] arrName = oElementNode.Value.Split(' ');
                                    //if (arrName.Length > 0)
                                    //{
                                    //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                    //    if (oDataModelElement != null)
                                    //        oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(arrName[arrName.Length - 1]));

                                    //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                    //    if (oDataModelElement != null)
                                    //        oDataModelElement.Value = arrName[arrName.Length - 1];

                                    //}
                                    //else
                                    //{
                                    //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                    //    if (oDataModelElement != null)
                                    //        oDataModelElement.Value = oElementNode.Value;
                                    //}

                                    string[] arrName = oElementNode.Value.Split(' ');

                                    if (arrName.Length > 3)
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(" "));

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = arrName[1];

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value.Substring(oElementNode.Value.IndexOf(arrName[1]) + arrName[1].Length);
                                    }
                                    else if (arrName.Length > 1)
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(" "));

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value.Substring(oElementNode.Value.IndexOf(" ") + 1);

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = "";
                                    }
                                    else
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = "";

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = "";

                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value;
                                    }
                                }




                                oElementNode = objELe.XPathSelectElement("./com.csc_DriverStatus");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/DriverStatus");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objELe.XPathSelectElement("./License/StateProvCd");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/DriverState");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }
                                oElements = objELe.XPathSelectElements("./ItemIdInfo/OtherIdentifier");
                                if (oElements != null)
                                {
                                    foreach (XElement objElement in oElements)
                                    {
                                        XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                        XElement oSubElement = objElement.XPathSelectElement("./OtherId");
                                        if (objEle != null && string.Equals(objEle.Value, "com.csc_DriverType", StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/DriverType");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = oSubElement.Value;

                                        }

                                        if (objEle != null && string.Equals(objEle.Value, "com.csc_DriverId", StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/DriverId");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = oSubElement.Value;
                                        }
                                        if (objEle != null && string.Equals(objEle.Value, "com.csc_DriverProduct", StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/DriverProduct");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = oSubElement.Value;
                                        }
                                        if (objEle != null && string.Equals(objEle.Value, "com.csc_RiskSubLoc", StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/RiskSubLoc");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = oSubElement.Value;
                                        }
                                        if (objEle != null && string.Equals(objEle.Value, "com.csc_RiskLoc", StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/RiskLoc");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = oSubElement.Value;
                                        }

                                        objEle = null;
                                        oSubElement = null;
                                    }
                                }


                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/SequenceNumber");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = iSequenceNumber.ToString();

                                iSequenceNumber++;


                                objBuilder.Append(oDataModelTemplate.ToString());


                            }
                        }
                        WriteFormattedXmlToFile(sFileName, "<Entities>" + objBuilder.ToString() + "</Entities>", ref p_objErrOut);
                    }

                }

            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                //  oSaveResponse.Result = false;

            }
            finally
            {
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElement != null)
                    oElement = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
                if (objBuilder != null)
                    objBuilder = null;
            }

            //  return oSaveResponse.Result;
        }

        public void GetInsuredSaveFormattedResult(string oAcordResponse, ref BusinessAdaptorErrors p_objErrOut, string sPolicyLOB, string sPolicySystemID) // aaggarwal29: Code mapping change
        {
            XElement oDataModelTemplate = null;
            XElement oSaveAcordXML = null;
            XElement oElement = null;
            XElement oElementNode = null;
            XElement oDataModelElement = null;
            XElement oEntityDataModelElement = null;
            
            System.Collections.Specialized.NameValueCollection oCollection  = RMConfigurationManager.GetNameValueSectionSettings("PolicyInterface", this.connectionString, base.ClientId);
            string sSaveInsured = string.Empty;
            if (oCollection != null && oCollection["DownloadInsuredTaxId"] != null)
                sSaveInsured = oCollection["DownloadInsuredTaxId"];
            string sFileName = "insured_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml";
            PolicySystemInterface objManager = null;
            StringBuilder objBuilder = null;
            SysSettings objSettings = null;
            try
            {
                oSaveAcordXML = XElement.Parse(oAcordResponse);
                DeleteFile(sFileName);


                objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                if (oSaveAcordXML != null)
                {
                    objSettings = new SysSettings(m_connectionString, base.ClientId);
                    objBuilder = new StringBuilder();

                    oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Entity", ref p_objErrOut));
                    if (oDataModelTemplate != null)
                    {
                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity");
                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value) && sSaveInsured.Trim().ToUpper() == "Y")
                        {

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/TaxId");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oElementNode.Value;
                        }

                        if ((oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/NameType") != null) && (string.Equals(oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/NameType").Value, "I", StringComparison.InvariantCultureIgnoreCase)))
                        {
                            oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {

                                string[] arrName = oElementNode.Value.Split(' ');

                                if (arrName.Length > 3)
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(" "));

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = arrName[1];

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value.Substring(oElementNode.Value.IndexOf(arrName[1]) + arrName[1].Length);
                                }
                                else if (arrName.Length > 1)
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(" "));

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value.Substring(oElementNode.Value.IndexOf(" ") + 1);

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = "";
                                }
                                else
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = "";

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = "";

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }
                            }
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/NameType");
                            if (oDataModelElement != null)
                                oDataModelElement.Attribute("codeid").Value = (new LocalCache(connectionString, base.ClientId)).GetCodeId("IND", "ENTITY_NAME_TYPE").ToString();
                        }

                        else
                        {


                            oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;
                            }
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/NameType");
                            if (oDataModelElement != null)
                                oDataModelElement.Attribute("codeid").Value = (new LocalCache(connectionString, base.ClientId)).GetCodeId("BUS", "ENTITY_NAME_TYPE").ToString();
                        }
                        oElementNode = null;
                        oDataModelElement = null;
                        //oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName");
                        //if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                        //{
                        //    //string[] arrName = oElementNode.Value.Split(' ');
                        //    //if (arrName.Length > 0)
                        //    //{
                        //    //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                        //    //    if (oDataModelElement != null)
                        //    //        oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(arrName[arrName.Length - 1]));

                        //    //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                        //    //    if (oDataModelElement != null)
                        //    //        oDataModelElement.Value = arrName[arrName.Length - 1];

                        //    //}
                        //    //else
                        //    //{
                        //    //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                        //    //    if (oDataModelElement != null)
                        //    //        oDataModelElement.Value = oElementNode.Value;
                        //    //}
                        //    string[] arrName = oElementNode.Value.Split(' ');

                        //    if (arrName.Length > 3)
                        //    {
                        //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                        //        if (oDataModelElement != null)
                        //            oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(" "));

                        //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                        //        if (oDataModelElement != null)
                        //            oDataModelElement.Value = arrName[1];

                        //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                        //        if (oDataModelElement != null)
                        //            oDataModelElement.Value = oElementNode.Value.Substring(oElementNode.Value.IndexOf(arrName[1]) + arrName[1].Length);
                        //    }
                        //    else if (arrName.Length > 1)
                        //    {
                        //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                        //        if (oDataModelElement != null)
                        //            oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(" "));

                        //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                        //        if (oDataModelElement != null)
                        //            oDataModelElement.Value = oElementNode.Value.Substring(oElementNode.Value.IndexOf(" ") + 1);

                        //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                        //        if (oDataModelElement != null)
                        //            oDataModelElement.Value = "";
                        //    }
                        //    else
                        //    {
                        //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                        //        if (oDataModelElement != null)
                        //            oDataModelElement.Value = "";

                        //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                        //        if (oDataModelElement != null)
                        //            oDataModelElement.Value = "";

                        //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                        //        if (oDataModelElement != null)
                        //            oDataModelElement.Value = oElementNode.Value;
                        //    }
                        //}






                        //oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/Surname");

                        //oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                        //if (oDataModelElement != null)
                        //    oDataModelElement.Value = oElementNode.Value;

                        //oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/GivenName");

                        //oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                        //if (oDataModelElement != null)
                        //    oDataModelElement.Value = oElementNode.Value;

                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr1");
                        if (oElementNode != null)
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/Addr1");
							//RMA-8753 nshah28(Added by ashish)
                            //oEntityDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Addr1");
                            oEntityDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Address/Addr1");
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = oElementNode.Value;
                                oEntityDataModelElement.Value = oElementNode.Value;
                            }
                        }

                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/Addr2");
                        if (oElementNode != null)
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/Addr2");
							//RMA-8753 nshah28(Added by ashish)
                            //oEntityDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Addr2");
                            oEntityDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Address/Addr2");
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = oElementNode.Value;
                                oEntityDataModelElement.Value = oElementNode.Value;
                            }
                        }

                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/PostalCode");
                        if (oElementNode != null)
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/ZipCode");
							//RMA-8753 nshah28(Added by ashish)
                            //oEntityDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/ZipCode");
                            oEntityDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Address/ZipCode");
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = oElementNode.Value;
                                oEntityDataModelElement.Value = oElementNode.Value;
                            }
                        }
                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/City");
                        if (oElementNode != null)
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/City");
							//RMA-8753 nshah28(Added by ashish)
                            //oEntityDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/City");
                            oEntityDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Address/City");
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = oElementNode.Value;
                                oEntityDataModelElement.Value = oElementNode.Value;
                            }
                        }

                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProvCd");
                        if (oElementNode != null)
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/StateId");
                            oEntityDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Address/State");//RMA-8753 nshah28(Added by ashish)
                            if (oDataModelElement != null && (!string.IsNullOrEmpty(oElementNode.Value)))
                                oDataModelElement.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "STATES", "StateProvCd on GetInsuredSaveFormattedResult", sPolicySystemID).ToString(); // aaggarwal29: Code mapping change
								//RMA-8753 nshah28(Added by ashish)
                            if (oEntityDataModelElement != null && (!string.IsNullOrEmpty(oElementNode.Value)))
                                oEntityDataModelElement.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "STATES", "StateProvCd on GetInsuredSaveFormattedResult", sPolicySystemID).ToString();
                        }
                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/County");
                        if (oElementNode != null)
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/County");
                            oEntityDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Address/County");//RMA-8753 nshah28(Added by ashish)
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = oElementNode.Value;
                                oEntityDataModelElement.Value = oElementNode.Value;
                            }
                        }
                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/BirthDt");
                        if (oElementNode != null)
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/BirthDate");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = oElementNode.Value;
                        }
                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/ItemIdInfo/com.csc_ClientSeqNum");
                        if (oElementNode != null)
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/ClientSequenceNumber");
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = oElementNode.Value;

                            }
                        }
                        oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/ItemIdInfo/com.csc_AddressSeqNum");
                        if (oElementNode != null)
                        {
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/AddressSequenceNumber");
							//RMA-8753 nshah28(Added by ashish)
                            //oEntityDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/AddressSeqNum");
                            oEntityDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/EntityXAddressesList/EntityXAddresses/Address/AddressSeqNum");
                            if (oDataModelElement != null)
                            {
                                oDataModelElement.Value = oElementNode.Value;
                                oEntityDataModelElement.Value = oElementNode.Value;
                            }
                        }
                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/EntityTableId");
                        if (oDataModelElement != null)
                        {
                            //    if (sPolicyLOB.Equals("WC") || sPolicyLOB.Equals("WCV") || sPolicyLOB.Equals("WCA"))
                            //        oDataModelElement.Value = (new LocalCache(connectionString)).GetTableId("EMPLOYEES").ToString();
                            //    else
                            oDataModelElement.Value = (new LocalCache(connectionString, base.ClientId)).GetTableId("POLICY_INSURED").ToString();
                        }
                        objBuilder.Append(oDataModelTemplate.ToString());

                        WriteFormattedXmlToFile(sFileName, objBuilder.ToString(), ref p_objErrOut);
                    }

                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);

            }
            finally
            {
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElement != null)
                    oElement = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
                if (objBuilder != null)
                    objBuilder = null;
                objSettings = null;
            }

            //   return oSaveResponse.Result;
        }

        public void GetInsurerSaveFormattedResult(PolicySaveRequest oRequest, ref BusinessAdaptorErrors p_objErrOut, string sPolicySystemID, string p_PolCmpny)
        {
            XElement oDataModelTemplate = null;
            XElement oDataModelElement = null;

            string sFileName = "insurer_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml";
            PolicySystemInterface objManager = null;
            StringBuilder objBuilder = null;
            string sCity = string.Empty, sStateCd = string.Empty;
            string sMasterCmpny = string.Empty, sLocatnCompny = string.Empty, sPolCompny = string.Empty;
            SysSettings objSettings = null;
            try
            {
                DeleteFile(sFileName);
                objManager = new PolicySystemInterface(m_userLogin, base.ClientId);

                objBuilder = new StringBuilder();
                objSettings = new SysSettings(m_connectionString, base.ClientId);
                oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Entity", ref p_objErrOut));
                if (oDataModelTemplate != null)
                {
                    {
                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/TaxId");
                        if (oDataModelElement != null)
                            oDataModelElement.Value = "";
                    }

                    if (oRequest.InsurerNm != null && !string.IsNullOrEmpty(oRequest.InsurerNm))
                    {
                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                        if (oDataModelElement != null)
                            oDataModelElement.Value = oRequest.InsurerNm;
                    }

                    if (oRequest.InsurerAddr1 != null && !string.IsNullOrEmpty(oRequest.InsurerAddr1))
                    {
                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/Addr1");
                        if (oDataModelElement != null)
                            oDataModelElement.Value = oRequest.InsurerAddr1;
                    }

                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/Addr2");
                    if (oDataModelElement != null)
                        oDataModelElement.Value = "";
                    if (oRequest.InsurerPostalCd != null && !string.IsNullOrEmpty(oRequest.InsurerPostalCd))
                    {
                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/ZipCode");
                        if (oDataModelElement != null)
                            oDataModelElement.Value = oRequest.InsurerPostalCd;
                    }

                    string[] sAddress;
                    sAddress = oRequest.InsurerCity.Split(','); // since insurercity has data like "Columbia, S.C.  or Coulmbia, SC" 

                    sCity = sAddress[0].Trim();
                    if (sAddress.Length == 2)
                        sStateCd = sAddress[1].Trim();
                    sStateCd = sStateCd.Replace(".", ""); // to change S.C. to SC

                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/City");
                    if (oDataModelElement != null)
                        oDataModelElement.Value = sCity;

                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/StateId");
                    if (oDataModelElement != null && sStateCd.Trim() != string.Empty)
                        oDataModelElement.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(sStateCd, "STATES", "StateProvCd on GetInsurerSaveFormattedResult", sPolicySystemID).ToString(); // aaggarwal29: Code mapping change

                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/County");
                    if (oDataModelElement != null)
                        oDataModelElement.Value = "";

                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/EntityTableId");
                    if (oDataModelElement != null)
                    {
                        oDataModelElement.Value = (new LocalCache(connectionString, base.ClientId)).GetTableId("INSURERS").ToString();
                    }

                    sMasterCmpny = oRequest.MasterCompany;
                    sLocatnCompny = oRequest.Location;
                    sPolCompny = p_PolCmpny;

                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/ReferenceNumber");
                    if (oDataModelElement != null)
                    {
                        oDataModelElement.Value = sLocatnCompny + "|" + sMasterCmpny + "|" + sPolCompny;
                    }

                    objBuilder.Append(oDataModelTemplate.ToString());

                    WriteFormattedXmlToFile(sFileName, objBuilder.ToString(), ref p_objErrOut);
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);

            }
            finally
            {
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
                if (objBuilder != null)
                    objBuilder = null;

                objSettings = null;
            }
        }



        public string GetCoveragDetailAcordRequest(Object oRequest, ref BusinessAdaptorErrors p_objErrOut, string sAccordXML, string sMode)
        {
            string oAcordRequest = string.Empty;
            XElement oElement = null;
            XElement objEle = null;
            XElement oTemplate = GetCoverageDetailAcordTemplate();
            PolicyEnquiry oPolicyEnquiry = null;
            SaveDownloadOptions oSaveOptions = null;
            PolicySystemInterface objManager = null;
            XElement objPolicySaveAccord = null;
            XElement oPolicySaveNode = null;
            IEnumerable<XElement> objElements = null;
            XElement objListResponse = null;
            string sLOB = string.Empty;
            string sIssueCd = string.Empty;
            string sParentShortCode = string.Empty;
            try
            {
                if (sMode == "POLICYENQUIRY")
                {
                    oPolicyEnquiry = (PolicyEnquiry)oRequest;
                    PopulateCommonAcordRequestParameters(ref oTemplate, oPolicyEnquiry.PolicySystemId, ref p_objErrOut);
                    if (!string.IsNullOrEmpty(oPolicyEnquiry.PolicyIdentfier) && (oPolicyEnquiry.PolicySystemId > 0))
                    {

                        oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.Token;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/PolicyNumber");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.PolicyNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/CompanyProductCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolicySymbol))
                        {
                            oElement.Value = oPolicyEnquiry.PolicySymbol;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/LOBCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.LOB))
                        {
                            oElement.Value = oPolicyEnquiry.LOB;
                            oElement = null;
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_BaseLOBLine");
                            if (oElement != null)
                            {

                                sParentShortCode = GetPolicyLOBRelatedShortCode(oPolicyEnquiry.LOB, "POLICY_CLAIM_LOB", ref p_objErrOut);
                                if (sParentShortCode == string.Empty)
                                {
                                    throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetCoverageListAcordRequest.NoBaseLobLine", base.ClientId));

                                }
                                else
                                {
                                    oElement.Value = sParentShortCode;
                                    oElement = null;

                                }

                            }
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_IssueCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.IssueCode))
                        {
                            oElement.Value = oPolicyEnquiry.IssueCode;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.Location;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.MasterCompany;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.Module;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.InsLine;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.UnitNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.Product;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.UnitRiskLocation;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.UnitSubRiskLocation;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.UnitState;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageCd']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.CovCode;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageSeq']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.CovSeqNo;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_TransSeq']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.TransSeq;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CovStatus']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.CovStatus;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolCompany']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.PolCompany;
                            oElement = null;
                        }
                    }
                }
                else if (sMode == "SAVEUNIT")
                {
                    oSaveOptions = (SaveDownloadOptions)oRequest;
                    PopulateCommonAcordRequestParameters(ref oTemplate, oSaveOptions.PolicySystemId, ref p_objErrOut);
                    oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                    if (oElement != null)
                    {
                        oElement.Value = oSaveOptions.Token;
                        oElement = null;
                    }
                    objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                    objPolicySaveAccord = XElement.Parse(objManager.GetDownLoadXMLData("POLICY", oSaveOptions.PolicyId, oSaveOptions.PolicyId));
                    objListResponse = XElement.Parse(sAccordXML);
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/PolicyNumber");
                    if (oElement != null)
                    {
                        oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber");
                        if (oPolicySaveNode != null)
                        {
                            oElement.Value = oPolicySaveNode.Value;
                            oElement = null;
                            oPolicySaveNode = null;
                        }
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/CompanyProductCd");
                    if (oElement != null)
                    {
                        oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/CompanyProductCd");
                        if (oPolicySaveNode != null)
                        {
                            oElement.Value = oPolicySaveNode.Value;
                            oElement = null;
                            oPolicySaveNode = null;
                        }
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/LOBCd");
                    if (oElement != null)
                    {
                        oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/LOBCd");
                        if (oPolicySaveNode != null)
                        {
                            oElement.Value = oPolicySaveNode.Value;
                            sLOB = oPolicySaveNode.Value;
                            oElement = null;
                            oPolicySaveNode = null;
                            //skhare7 Policy interfcae
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_BaseLOBLine");

                            if (oElement != null)
                            {


                                sParentShortCode = GetPolicyLOBRelatedShortCode(sLOB, "POLICY_CLAIM_LOB", ref p_objErrOut);
                                if (sParentShortCode == string.Empty)
                                {
                                    throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetCoverageListAcordRequest.NoBaseLobLine", base.ClientId));

                                }

                                if (oElement != null && !string.IsNullOrEmpty(sParentShortCode))
                                {
                                    oElement.Value = sParentShortCode;
                                    oElement = null;
                                }



                            }
                        }
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_IssueCd");
                    if (oElement != null)
                    {
                        oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_IssueCd");
                        if (oPolicySaveNode != null)
                        {

                            oElement.Value = oPolicySaveNode.Value;
                            sIssueCd = oPolicySaveNode.Value;
                            oElement = null;
                            oPolicySaveNode = null;
                        }
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                    if (oElement != null)
                    {
                        objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                        foreach (XElement objElement in objElements)
                        {
                            objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                            XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                            if (objEle != null && string.Equals(objEle.Value, "com.csc_LocCompany", StringComparison.InvariantCultureIgnoreCase))
                            {
                                oElement.Value = oSubElement.Value;
                            }

                            objEle = null;
                            oSubElement = null;
                        }
                        objElements = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId");
                    if (oElement != null)
                    {
                        objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                        foreach (XElement objElement in objElements)
                        {
                            objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                            XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                            if (objEle != null && string.Equals(objEle.Value, "com.csc_MasterCompany", StringComparison.InvariantCultureIgnoreCase))
                            {
                                oElement.Value = oSubElement.Value;
                            }

                            objEle = null;
                            oSubElement = null;
                        }
                        objElements = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId");
                    if (oElement != null)
                    {
                        objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                        foreach (XElement objElement in objElements)
                        {
                            objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                            XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                            if (objEle != null && string.Equals(objEle.Value, "com.csc_Module", StringComparison.InvariantCultureIgnoreCase))
                            {
                                oElement.Value = oSubElement.Value;
                            }

                            objEle = null;
                            oSubElement = null;
                        }
                        objElements = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                    if (oElement != null)
                    {

                        oElement.Value = oSaveOptions.InsuranceLine;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oSaveOptions.UnitNumber;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oSaveOptions.Product;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oSaveOptions.RiskLocation;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oSaveOptions.RiskSubLocation;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oSaveOptions.UnitState;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageCd']/OtherId");
                    if (oElement != null)
                    {
                        oPolicySaveNode = objListResponse.XPathSelectElement("./Coverage/CoverageCd");
                        if (oPolicySaveNode != null)
                        {
                            oElement.Value = oPolicySaveNode.Value;
                            oElement = null;
                            oPolicySaveNode = null;
                        }
                    }
                    //skhare7 Policy interfcae
                    if (sParentShortCode == "WL")
                    {
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageSeq']/OtherId");
                        if (oElement != null)
                        {

                            objElements = objListResponse.XPathSelectElements("./ItemIdInfo/OtherIdentifier");
                            if (objElements != null)
                            {
                                foreach (XElement objElement in objElements)
                                {
                                    objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./OtherId");
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_ClassSeq", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oElement.Value = oSubElement.Value;
                                        break;
                                    }

                                    objEle = null;
                                    oSubElement = null;
                                }
                            }

                        }
                    }
                    else
                    {
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageSeq']/OtherId");
                        if (oElement != null)
                        {

                            objElements = objListResponse.XPathSelectElements("./ItemIdInfo/OtherIdentifier");
                            if (objElements != null)
                            {
                                foreach (XElement objElement in objElements)
                                {
                                    objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./OtherId");
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_CoverageSeq", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oElement.Value = oSubElement.Value;
                                        break;
                                    }

                                    objEle = null;
                                    oSubElement = null;
                                }
                            }

                        }
                    }
                    if (sParentShortCode == "WL")
                    {
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_TransSeq']/OtherId");
                        if (oElement != null)
                        {
                            oPolicySaveNode = objListResponse.XPathSelectElement("./Coverage/com.csc_ExpCovCd");
                            if (oPolicySaveNode != null)
                            {
                                oElement.Value = oPolicySaveNode.Value;
                                oElement = null;
                                oPolicySaveNode = null;
                            }
                        }

                    }
                    else
                    {
                        if (sIssueCd == "M")
                        {
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_TransSeq']/OtherId");
                            if (oElement != null)
                            {
                                objElements = objListResponse.XPathSelectElements("./ItemIdInfo/OtherIdentifier");
                                if (objElements != null)
                                {
                                    foreach (XElement objElement in objElements)
                                    {
                                        objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                        XElement oSubElement = objElement.XPathSelectElement("./OtherId");
                                        if (objEle != null && string.Equals(objEle.Value, "com.csc_TrxnSeq", StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            oElement.Value = oSubElement.Value;
                                        }


                                        objEle = null;
                                        oSubElement = null;
                                    }
                                }
                            }
                        }
                        else
                        {
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_TransSeq']/OtherId");
                            if (oElement != null)
                            {
                                objElements = objListResponse.XPathSelectElements("./ItemIdInfo/OtherIdentifier");
                                if (objElements != null)
                                {
                                    foreach (XElement objElement in objElements)
                                    {
                                        objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                        XElement oSubElement = objElement.XPathSelectElement("./OtherId");
                                        if (objEle != null && string.Equals(objEle.Value, "com.csc_TransSeq", StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            oElement.Value = oSubElement.Value;
                                        }


                                        objEle = null;
                                        oSubElement = null;
                                    }
                                }
                            }
                        }
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CovStatus']/OtherId");
                    if (oElement != null)
                    {
                        oPolicySaveNode = objListResponse.XPathSelectElement("./com.csc_RecordStatus");
                        if (oPolicySaveNode != null)
                        {
                            oElement.Value = oPolicySaveNode.Value;
                            oElement = null;
                            oPolicySaveNode = null;
                        }
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolCompany']/OtherId");
                    if (oElement != null)
                    {
                        objEle = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolCompany']/OtherId");
                        if (objEle != null)
                        {
                            oElement.Value = objEle.Value;
                            oElement = null;
                        }
                    }
                }

                if (string.IsNullOrEmpty(oTemplate.ToString()))
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetCoverageDetailAcordRequest.RequestOperationError", base.ClientId));
                }
                else
                    oAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
            }

            return oAcordRequest;
        }

        public string GetPolicySearchAcordRequest(PolicySearch oSearchRequest, ref BusinessAdaptorErrors p_objErrOut)
        {
            string oSearchAcordRequest = string.Empty;
            int iPSMappedCode = 0;
            SearchFilters oSearchFilters = oSearchRequest.objSearchFilters;
            XElement oElement = null;
            XElement oSubElement = null;
            IEnumerable<XElement> oElements = null;
            PolicySystemInterface oPolicySystemInterface = null;
            XElement oTemplate = GetPolicySearchAcordTemplate();
            PopulateCommonAcordRequestParameters(ref oTemplate, oSearchRequest.objSearchFilters.PolicySystemId, ref p_objErrOut);
            int iFetchRecordCount;
            try
            {
                if (oSearchFilters != null && oSearchRequest.objSearchFilters.PolicySystemId > 0)
                {
                    oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                    if (oElement != null)
                    {
                        oElement.Value = oSearchRequest.Token;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.PolicyNumber))
                    {
                        oElement.Value = oSearchFilters.PolicyNumber;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/CompanyProductCd");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.PolicySymbol))
                    {
                        oElement.Value = oSearchFilters.PolicySymbol;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/StateProvCd");
                    if (oElement != null && oSearchFilters.State > 0)
                    {
                        oElement.Value = (new LocalCache(connectionString, base.ClientId)).GetStateCode(oSearchFilters.State);
                        oElement = null;
                    }
                    oElements = oTemplate.XPathSelectElements("./ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");
                    if (oElements != null)
                    {
                        foreach (XElement oEle in oElements)
                        {
                            oElement = oEle.XPathSelectElement("./OtherIdTypeCd");
                            oSubElement = oEle.XPathSelectElement("./OtherId");
                            switch (oElement.Value)
                            {
                                case "com.csc_LocCompany":
                                    if (!string.IsNullOrEmpty(oSearchFilters.LocationCompany))
                                        oSubElement.Value = oSearchFilters.LocationCompany;
                                    break;
                                case "com.csc_MasterCompany":
                                    if (!string.IsNullOrEmpty(oSearchFilters.MasterCompany))
                                        oSubElement.Value = oSearchFilters.MasterCompany;
                                    break;
                                case "com.csc_Module":
                                    if (!string.IsNullOrEmpty(oSearchFilters.Module))
                                        oSubElement.Value = oSearchFilters.Module;
                                    break;
                                //MITS:33371 ajohari2:Start
                                case "com.csc_CombinedPolicyFilter":
                                    SysSettings oSettings = new SysSettings(m_userLogin.objRiskmasterDatabase.ConnectionString, base.ClientId);
                                    if (m_userLogin.objRiskmasterDatabase.OrgSecFlag && (oSettings.MultiCovgPerClm == -1) && oSettings.EnableTPAAccess) //JIRA RMA-718 ajohari2
                                    {
                                        PolicySearchDataSetup objPolicySearchDataSetup = new PolicySearchDataSetup(connectionString, base.ClientId);
                                        int sGroupId = m_userLogin.BESGroupId;
                                        int sPolicySystemId = oSearchFilters.PolicySystemId;
                                        string sCombinedPolicyFilter = string.Empty;
                                        sCombinedPolicyFilter = objPolicySearchDataSetup.GetByPolicySystemAndGroupName(sGroupId, sPolicySystemId);

                                        if (!string.IsNullOrEmpty(sCombinedPolicyFilter))
                                        {
                                            oSubElement.Value = sCombinedPolicyFilter;
                                        }
                                        objPolicySearchDataSetup = null;
                                    }
                                    oSettings = null;
                                    break;
                                //MITS:33371 ajohari2:End
                            }
                        }
                        oElements = null;
                        oElement = null;
                        oSubElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/PolicySummaryInfo/LOBCd");
                    if (oElement != null && oSearchFilters.LOB > 0)
                    {
                        oPolicySystemInterface = new PolicySystemInterface(connectionString, base.ClientId);
                        iPSMappedCode = oPolicySystemInterface.GetPSMappedCodeIDFromRMXCodeId(oSearchFilters.LOB, "POLICY_CLAIM_LOB", "Claim LOB on GetPolicySearchAcordRequest", oSearchRequest.objSearchFilters.PolicySystemId.ToString()); // aaggarwal29: Code mapping change
                        //if (string.IsNullOrEmpty(sPSMappedCode))
                        //{
                        //    throw new RMAppException(string.Format( Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicySearchAcordRequest.NoMappedCodeFoundError"),"Claim LOB "));
                        //}
                        oElement.Value = (new LocalCache(connectionString, base.ClientId)).GetShortCode(iPSMappedCode);
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/PolicySummaryInfo/GroupId");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.GroupNo))
                    {
                        oElement.Value = oSearchFilters.GroupNo;
                        oElement = null;
                    }

                    // oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/PolicySummaryInfo/PolicyStatusCd");
                    //if (oElement != null && oSearchFilters.PolicyStatus > 0)
                    //{
                    //    oPolicySystemInterface = new PolicySystemInterface(connectionString);
                    //    if (string.IsNullOrEmpty(oPolicySystemInterface.GetPSMappedCodeIDFromRMXCodeId(oSearchFilters.PolicyStatus, "POLICY_STATUS")))
                    //    {
                    //        throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicySearchAcordRequest.NoMappedCodeFoundError"));
                    //    }
                    //    oElement.Value = oSearchFilters.PolicyNumber;
                    //    oElement = null;
                    //}
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/ContractNumber");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.Agent))
                    {
                        oElement.Value = oSearchFilters.Agent;
                        oElement = null;
                    }

                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/CommlName/CommercialName");
                    if (oElement != null && (oSearchRequest.ClientFile == "true") && (!string.IsNullOrEmpty(oSearchFilters.InsuredFirstName) || !string.IsNullOrEmpty(oSearchFilters.InsuredLastName)))
                    {
                        oElement.Value = oSearchFilters.InsuredFirstName + "|" + oSearchFilters.InsuredLastName;
                        oElement = null;
                    }
                    else if (oElement != null && (oSearchRequest.ClientFile == "false") && (!string.IsNullOrEmpty(oSearchFilters.Name)))
                    {
                        oElement.Value = oSearchFilters.Name + "|" + string.Empty;
                        oElement = null;
                    }

                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.InsuredSSN))
                    {
                        oElement.Value = oSearchFilters.InsuredSSN;
                        oElement = null;
                    }

                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/City");
                    if (oElement != null && (oSearchRequest.ClientFile == "true") && !string.IsNullOrEmpty(oSearchFilters.City))
                    {
                        oElement.Value = oSearchFilters.City;
                        oElement = null;
                    }
                    else if (oElement != null && (oSearchRequest.ClientFile == "false") && !string.IsNullOrEmpty(oSearchFilters.Address))
                    {
                        oElement.Value = oSearchFilters.Address;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/PostalCode");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.Zip))
                    {
                        oElement.Value = oSearchFilters.Zip;
                        oElement = null;
                    }

                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/InsuredOrPrincipal/ItemIdInfo/CustPermId");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.CustomerNo))
                    {
                        oElement.Value = oSearchFilters.CustomerNo;
                        oElement = null;
                    }

                    //oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/ReportedDt");
                    //if (oElement != null && oSearchFilters.DateReported != DateTime.MinValue)
                    //{
                    //    oElement.Value = oSearchFilters.DateReported.ToString();
                    //    oElement = null;
                    //}
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/LossDt");
                    if (oElement != null && !string.IsNullOrEmpty(oSearchFilters.LossDate))
                    {
                        oElement.Value = oSearchFilters.LossDate.ToString();
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyListInquiryRq/com.csc_SearchInfo/com.csc_RoutingInfo");
                    iFetchRecordCount = (new SysSettings(connectionString, base.ClientId)).PolicySearchCount;
                    if (oElement != null)
                    {
                        oElement.Value = iFetchRecordCount.ToString();
                        oElement = null;
                    }
                }
                if (string.IsNullOrEmpty(oTemplate.ToString()))
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicySearchAcordRequest.RequestOperationError", base.ClientId));
                }
                else
                    oSearchAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oPolicySystemInterface != null)
                    oPolicySystemInterface = null;
                if (oSearchFilters != null)
                    oSearchFilters = null;
                if (oTemplate != null)
                    oTemplate = null;
                if (oElements != null)
                    oElements = null;
                if (oElement != null)
                    oElement = null;
                if (oSubElement != null)
                    oSubElement = null;
            }

            return oSearchAcordRequest;
        }

        public void GetSiteUnitListFormattedResult(string oAcordResponse, ref BusinessAdaptorErrors p_objErrOut)
        {
            XElement oAcordXML = null;
            XElement oDataModelTemplate = null;
            XElement oSaveAcordXML = null;
            XElement oElement = null;
            XElement oElementNode = null;
            XElement oDataModelElement = null;
            string sFileName = "Unit_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml";
            StringBuilder objBuilder = null;
            string sAddr = string.Empty;
            IEnumerable<XElement> oElements = null;
            string sUnitNo = string.Empty;
            string sStatUnitNo = string.Empty;
            int iSequenceNo = 1;
            try
            {
                oAcordXML = XElement.Parse(oAcordResponse);
                if (oAcordXML != null)
                {
                    objBuilder = new StringBuilder();
                    foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyUnitListRs/InsuredOrPrincipal"))
                    {
                        oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("UnitList", ref p_objErrOut));
                        if (oDataModelTemplate != null)
                        {
                            //dbisht6 start for merging mits 35655
                            oElementNode = objEles.XPathSelectElement("./com.csc_InsuredNameIdInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='DESCUSE1']/OtherId");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Vin");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;
                            }
                            //dbisht6 end
                            oElementNode = objEles.XPathSelectElement("./GeneralPartyInfo/NameInfo/CommlName/CommercialName");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                sAddr = oElementNode.Value;
                            }

                            oElementNode = objEles.XPathSelectElement("./GeneralPartyInfo/Addr/Addr1");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                if (string.IsNullOrEmpty(sAddr))
                                    sAddr = oElementNode.Value;
                                else
                                    sAddr = sAddr + " " + oElementNode.Value;

                            }

                            oElementNode = objEles.XPathSelectElement("./GeneralPartyInfo/Addr/Addr2");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                if (string.IsNullOrEmpty(sAddr))
                                    sAddr = oElementNode.Value;
                                else
                                    sAddr = sAddr + " " + oElementNode.Value;


                            }

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Desc");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sAddr;

                            sAddr = string.Empty;

                            oElementNode = objEles.XPathSelectElement("./GeneralPartyInfo/Addr/City");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/City");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                            }

                            oElementNode = objEles.XPathSelectElement("./GeneralPartyInfo/Addr/StateProvCd");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/State");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                            }

                            oElements = objEles.XPathSelectElements("./ItemIdInfo/OtherIdentifier");
                            if (oElements != null)
                            {
                                foreach (XElement objElement in oElements)
                                {
                                    XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./OtherId");
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_UnitNumber", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        sUnitNo = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_StatUnitNumber", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        sStatUnitNo = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_InsLineCd", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/InsLineCd");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_Product", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Product");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_RiskSubLoc", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/RiskSubLoc");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_RiskLoc", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/RiskLoc");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    objEle = null;
                                    oSubElement = null;
                                }
                            }

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/SequenceNumber");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = iSequenceNo.ToString();

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/UnitNo");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sUnitNo;
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/StatUnitNo");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sStatUnitNo;

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/UnitType");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = "Stat";

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Status");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = "";

                            iSequenceNo++;
                        }
                        objBuilder.Append(oDataModelTemplate.ToString());
                    }

                    foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyUnitListRs/WorkCompLocInfo"))
                    {
                        oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("UnitList", ref p_objErrOut));
                        if (oDataModelTemplate != null)
                        {
                            oElementNode = objEles.XPathSelectElement("./Location/LocationName");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                sAddr = oElementNode.Value;

                            }

                            oElementNode = objEles.XPathSelectElement("./Location/Addr/Addr1");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                if (string.IsNullOrEmpty(sAddr))
                                    sAddr = oElementNode.Value;
                                else
                                    sAddr = sAddr + " " + oElementNode.Value;

                            }

                            oElementNode = objEles.XPathSelectElement("./Location/Addr/Addr2");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                if (string.IsNullOrEmpty(sAddr))
                                    sAddr = oElementNode.Value;
                                else
                                    sAddr = sAddr + " " + oElementNode.Value;
                            }

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Desc");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sAddr;

                            sAddr = string.Empty;

                            oElementNode = objEles.XPathSelectElement("./Location/Addr/City");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/City");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                            }

                            oElementNode = objEles.XPathSelectElement("./Location/Addr/StateProvCd");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/State");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                            }

                            oElements = objEles.XPathSelectElements("./Location/ItemIdInfo/OtherIdentifier");
                            if (oElements != null)
                            {
                                foreach (XElement objElement in oElements)
                                {
                                    XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./OtherId");
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_UnitNumber", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        sUnitNo = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_StatUnitNumber", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        sStatUnitNo = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_InsLineCd", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/InsLineCd");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_Product", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Product");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_RiskSubLoc", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/RiskSubLoc");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_RiskLoc", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/RiskLoc");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }

                                    objEle = null;
                                    oSubElement = null;
                                }
                            }



                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/SequenceNumber");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = iSequenceNo.ToString();

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/UnitNo");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sUnitNo;
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/StatUnitNo");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sStatUnitNo;

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/UnitType");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = "Site";
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Status");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = "";
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Vin");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = "";



                            iSequenceNo++;

                        }
                        objBuilder.Append(oDataModelTemplate.ToString());
                    }

                    if (oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyUnitListRs/WorkCompLocInfo").Count() > 0)
                        WriteFormattedXmlToFile(sFileName, "<Units>" + objBuilder.ToString() + "</Units>", ref p_objErrOut);

                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                oElements = null;
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElement != null)
                    oElement = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
                if (objBuilder != null)
                    objBuilder = null;
            }
        }

        public void SaveCoverages(string oAcordResponse, ref BusinessAdaptorErrors p_objErrOut, int iPolicyUnitId, int iPolicyId, string sLossDate)
        {
            XElement oAcordXML = null;
            XElement oDataModelTemplate = null;
            XElement oSaveAcordXML = null;
            XElement oElement = null;
            XElement oElementNode = null;
            XElement oDataModelElement = null;
            string sUnitNo = string.Empty;
            int iRecordId = 0;
            PolicySystemInterface objManager = null;
            LocalCache objCache = null;
            string sCvgSequenNo = string.Empty;
            string sTempCvgSequenNo = string.Empty;
            string sTempCvgCode = string.Empty;
            string sCvgCode = string.Empty;
            string sBaseLOBLine = string.Empty;
            int iPolicySystemId = Int32.MinValue; // aaggarwal29: Code mapping change
            XElement objTemp = null;
            string sExcludeCVGs = string.Empty;
            string sCvgText = string.Empty;
            string sClassCode = string.Empty; //MITS 36753 changes
            string sClassDesc = string.Empty;//MITS 36753 changes
            ArrayList objLimitList = null;
            IEnumerable<XElement> objElements = null;
            try
            {
                oAcordXML = XElement.Parse(oAcordResponse);
                if (oAcordXML != null)
                {
                    objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                    //rupal:start, omig
                    string sCoverageScriptFilePath = Path.Combine(RMConfigurator.UserDataPath, @"PolicyInterface\Scripts\PolicyDownload.vb");
                    if (File.Exists(sCoverageScriptFilePath))
                        objManager.ScriptFileExists = true;
                    else
                        objManager.ScriptFileExists = false;
                    //rupal:end,omig
                    objLimitList = new ArrayList();
                    iPolicySystemId = objManager.GetPolicySystemId(iPolicyId); // aaggarwal29: Code mapping change
                    sExcludeCVGs = objManager.GetPolicySystemParamteres("EXCLUDE_MAJOR_PERIL", iPolicySystemId).Replace(" ", "");
                    oElement = oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_UnitCoverageListRs/Policy/com.csc_BaseLOBLine");
                    if (oElement != null)
                        sBaseLOBLine = oElement.Value;
                    if (!string.IsNullOrEmpty(sBaseLOBLine))
                    {
                        foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitCoverageListRs/com.csc_CoverageLossInfo"))
                        {
                            oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Coverage", ref p_objErrOut));
                            if (oDataModelTemplate != null)
                            {
                                if (objLimitList.Count > 0)
                                {
                                    objLimitList.Clear();
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/CoverageCd");

                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageTypeCode");
                                    if (oDataModelElement != null)
                                    {
                                        // aaggarwal29: Code mapping changes start
                                        iPolicySystemId = objManager.GetPolicySystemId(iPolicyId);
                                        if (!string.IsNullOrEmpty(oElementNode.Value) && !string.IsNullOrEmpty(sExcludeCVGs) && sExcludeCVGs.Split(',').Contains(oElementNode.Value))
                                            continue;

                                        oDataModelElement.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "COVERAGE_TYPE", "CoverageType on SaveCoverages", iPolicySystemId.ToString()).ToString();
                                        sCvgCode = oDataModelElement.Attribute("codeid").Value;
                                        //aaggarwal29: Code mapping changes end
                                    }

                                }
                                //Ashish Ahuja: Claims Made Jira 1342
                                objCache = new LocalCache(connectionString, base.ClientId);
                                oElementNode = objEles.XPathSelectElement("./Coverage/ClaimsMadeInd");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    int iYesCodeId = objCache.GetCodeId("Y", "YES_NO");
                                    int iNoCodeId = objCache.GetCodeId("N", "YES_NO");

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ClaimsMadeInd");
                                    if (oElementNode != null)
                                    {
                                        if (string.Equals(oElementNode.Value, "Y", StringComparison.OrdinalIgnoreCase))
                                        {
                                            oDataModelElement.Value = iYesCodeId.ToString();
                                        }
                                        else
                                        {
                                            oDataModelElement.Value = iNoCodeId.ToString();
                                        }
                                    }
                                    else
                                    {
                                        oDataModelElement.Value = iNoCodeId.ToString();
                                    }
                                }

                                //Ashish Ahuja: Claims Made Jira 1342 End
                                //MITS 34278 START
                                objCache = new LocalCache(connectionString, base.ClientId);
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Reinsurance");

                                if (oDataModelElement != null)
                                {
                                    int iYesCodeId = objCache.GetCodeId("Y", "YES_NO");
                                    int iNoCodeId = objCache.GetCodeId("N", "YES_NO");

                                    oElementNode = objEles.XPathSelectElement("./com.csc_ReInsuranceInd");
                                    if (oElementNode != null)
                                    {
                                        if (string.Equals(oElementNode.Value, "Y", StringComparison.OrdinalIgnoreCase))
                                        {
                                            oDataModelElement.Value = iYesCodeId.ToString();
                                        }
                                        else
                                        {
                                            oDataModelElement.Value = iNoCodeId.ToString();
                                        }
                                    }
                                    else
                                    {
                                        oDataModelElement.Value = iNoCodeId.ToString();
                                    }
                                }
                                //MITS 34278 END
                                oElementNode = objEles.XPathSelectElement("./Coverage/com.csc_Exposure");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Exposure");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/EffectiveDt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/EffectiveDate");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/ExpirationDt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ExpirationDate");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/Deductible/FormatCurrencyAmt/Amt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/SelfInsureDeduct");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/CurrentTermAmt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/FullTermPremium");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/com.csc_TotalPremium");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/TotalWrittenPremium");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/com.csc_OriginalPremium");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/OrginialPremium");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/com.csc_WrittenPremium");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/WrittenPremium");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageSeq']/OtherId");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CvgSequenceNo");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_TrxnSeq']/OtherId");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/TransSequenceNo");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/Limit[LimitAppliesToCd='OccurenceLimit']/FormatCurrencyAmt/Amt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/OccurrenceLimit");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/Limit[LimitAppliesToCd='AggregateLimit']/FormatCurrencyAmt/Amt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyLimit");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                objElements = objEles.XPathSelectElements("./Coverage/Limit");
                                if (objElements != null)
                                {
                                    foreach (XElement oEle in objElements)
                                    {
                                        XElement oElement1 = oEle.XPathSelectElement("./LimitAppliesToCd");
                                        XElement oSubElement1 = oEle.XPathSelectElement("./FormatCurrencyAmt/Amt");
                                        if (!string.IsNullOrEmpty(oElement1.Value))
                                        {

                                            objLimitList.Add(oElement1.Value + "|" + oSubElement1.Value);
                                        }

                                        oElement1 = null;
                                        oSubElement1 = null;
                                    }
                                }

                                if (sBaseLOBLine == "PL")
                                {
                                    oElementNode = objEles.XPathSelectElement("./Coverage/Limit[LimitAppliesToCd='LIMIT-COVA']/FormatCurrencyAmt/Amt");
                                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/LimitCovA");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value;
                                    }
                                    oElementNode = objEles.XPathSelectElement("./Coverage/Limit[LimitAppliesToCd='LIMIT-COVB']/FormatCurrencyAmt/Amt");
                                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/LimitCovB");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value;
                                    }
                                    oElementNode = objEles.XPathSelectElement("./Coverage/Limit[LimitAppliesToCd='LIMIT-COVC']/FormatCurrencyAmt/Amt");
                                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/LimitCovC");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value;
                                    }
                                    oElementNode = objEles.XPathSelectElement("./Coverage/Limit[LimitAppliesToCd='LIMIT-COVD']/FormatCurrencyAmt/Amt");
                                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/LimitCovD");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value;
                                    }
                                    oElementNode = objEles.XPathSelectElement("./Coverage/Limit[LimitAppliesToCd='LIMIT-COVE']/FormatCurrencyAmt/Amt");
                                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/LimitCovE");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value;
                                    }
                                    oElementNode = objEles.XPathSelectElement("./Coverage/Limit[LimitAppliesToCd='LIMIT-COVF']/FormatCurrencyAmt/Amt");
                                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/LimitCovF");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value;
                                    }

                                }
                                if (!sBaseLOBLine.Equals("WL"))
                                {
                                    //Ankit Start : Worked on MITS - 34333
                                    oElementNode = objEles.XPathSelectElement("./com.csc_Subline");
                                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/SubLine");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value;
                                    }
                                    //Ankit End

                                    objCache = new LocalCache(m_connectionString, base.ClientId);
                                    //Start: added by Nitin goel, MITS 36753,06/14/2014
                                    //oElementNode = objEles.XPathSelectElement("./com.csc_SublineDesc");
                                    //if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                    //{
                                    //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageText");
                                    //    if (oDataModelElement != null)
                                    //    {
                                    //        oDataModelElement.Value = objCache.GetShortCode(Conversion.ConvertStrToInteger(sCvgCode)) + " " + objCache.GetCodeDesc(Conversion.ConvertStrToInteger(sCvgCode)) + " " + oElementNode.Value;
                                    //        sCvgText = oDataModelElement.Value;
                                    //    }

                                    //}
                                    //else
                                    //{
                                    //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageText");
                                    //    if (oDataModelElement != null)
                                    //    {
                                    //        oDataModelElement.Value = objCache.GetShortCode(Conversion.ConvertStrToInteger(sCvgCode)) + " " + objCache.GetCodeDesc(Conversion.ConvertStrToInteger(sCvgCode));
                                    //        sCvgText = oDataModelElement.Value;
                                    //    }
                                    //}


                                    oElementNode = objEles.XPathSelectElement("./com.csc_ClassCd");
                                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                    {
                                        sClassCode = oElementNode.Value;
                                        oElementNode = objEles.XPathSelectElement("./com.csc_ClassDesc");
                                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                        {
                                            sClassDesc = oElementNode.Value;
                                        }
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageText");
                                        if (oDataModelElement != null)
                                        {
                                            oDataModelElement.Value = objCache.GetShortCode(Conversion.ConvertStrToInteger(sCvgCode)) + " " + objCache.GetCodeDesc(Conversion.ConvertStrToInteger(sCvgCode)) + " " + sClassCode + " " + sClassDesc;
                                            sCvgText = oDataModelElement.Value;
                                        }
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CvgClassCode");
                                        if (oDataModelElement != null)
                                        {
                                            oDataModelElement.Value = sClassCode;
                                        }
                                    }
                                    else
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageText");
                                        if (oDataModelElement != null)
                                        {
                                            oDataModelElement.Value = objCache.GetShortCode(Conversion.ConvertStrToInteger(sCvgCode)) + " " + objCache.GetCodeDesc(Conversion.ConvertStrToInteger(sCvgCode));
                                            sCvgText = oDataModelElement.Value;
                                        }
                                    }
                                    //end: added by Nitin goel
                                }
                                else
                                {
                                    objCache = new LocalCache(m_connectionString, base.ClientId);

                                    //string sClassCode = string.Empty;

                                    oElementNode = objEles.XPathSelectElement("./com.csc_ClassCd");
                                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                    {

                                        sClassCode = oElementNode.Value;
                                    }
                                    //string sClassDesc = string.Empty;
                                    oElementNode = null;
                                    oElementNode = objEles.XPathSelectElement("./com.csc_ClassDesc");
                                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                    {

                                        sClassDesc = oElementNode.Value;
                                    }

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageText");
                                    if (oDataModelElement != null)
                                    {
                                        oDataModelElement.Value = objCache.GetShortCode(Conversion.ConvertStrToInteger(sCvgCode)) + " " + sClassCode + " " + sClassDesc;
                                        sCvgText = oDataModelElement.Value;
                                    }
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CvgClassCode");
                                    if (oDataModelElement != null)
                                    {
                                        oDataModelElement.Value = sClassCode;
                                    }
                                }

                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyId");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = "0";

                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyUnitRowId");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = iPolicyUnitId.ToString();

                                oElementNode = objEles.XPathSelectElement("./com.csc_AnnualStatement");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/AnnualStmt");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./com.csc_ProductLine");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ProductLine");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                //MITS:33573 starts
                                oElementNode = objEles.XPathSelectElement("./com.csc_RetroDt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/RetroDate");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./com.csc_ExtendDt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/TailDate");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }
                                //MITS:33573 ends
                                int iPolicyCVGRowId = objManager.CheckCoverageDuplication(iPolicyUnitId, objEles.ToString(), sBaseLOBLine, iPolicySystemId, sCvgText); // aaggarwal29: Code mapping change

                                objTemp = XElement.Parse(oAcordResponse);
                                IEnumerable<XElement> objTempEls = objTemp.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitCoverageListRs/com.csc_CoverageLossInfo");
                                for (int i = objTempEls.Count() - 1; i >= 0; i--)
                                {
                                    objTempEls.ElementAt(i).Remove();
                                }
                                iRecordId = objManager.SaveCoverage(oDataModelTemplate.ToString(), iPolicyCVGRowId, sLossDate, objTemp.ToString(), objManager.GetPolicySystemId(iPolicyId),objLimitList);
                                objTemp.XPathSelectElement("./ClaimsSvcRs/com.csc_UnitCoverageListRs").Add(objEles);
                                //bool bResult = objManager.SavePolicyDownloadXMLData(iRecordId, objTemp.ToString(), objManager.GetPolicySystemId(iPolicyId), "POLICY_X_CVG_TYPE");
                            }
                        }
                    }
                    else
                    {
                        throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetPolicySystemList.NoBaseLobLine", base.ClientId));
                    }
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElement != null)
                    oElement = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (oDataModelElement != null)
                    oDataModelElement = null;
            }
        }
        //public void GetMRCvgDetailSaveFormattedResult(string oAcordResponse, ref BusinessAdaptorErrors p_objErrOut, int iPolicyUnitId, int iPolicyId)
        //{
        //    XElement oAcordXML = null;
        //    XElement oDataModelTemplate = null;
        //    XElement oSaveAcordXML = null;
        //    XElement oElement = null;
        //    XElement oElementNode = null;
        //    XElement oDataModelElement = null;
        //    IEnumerable<XElement> oElements = null;
        //    string sUnitNo = string.Empty;
        //    LocalCache objCache = null;
        //    int iRecordId = 0;
        //    PolicySystemInterface objManager = null;
        //    try
        //    {
        //        oAcordXML = XElement.Parse(oAcordResponse);
        //        if (oAcordXML != null)
        //        {
        //            objManager = new PolicySystemInterface(m_userLogin);
        //            foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo"))
        //            {
        //                oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Coverage", ref p_objErrOut));
        //                if (oDataModelTemplate != null)
        //                {
        //                    oElementNode = objEles.XPathSelectElement("./com.csc_ChangeDt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {

        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ChangeDate");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/CoverageCd");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageTypeCode");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "COVERAGE_TYPE", "CoverageType on GetMRCvgDetailSaveFormattedResult").ToString();

        //                    }



        //                    oElementNode = objEles.XPathSelectElement("./Coverage/com.csc_Exposure");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Exposure");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    objCache = new LocalCache(m_connectionString);

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/EffectiveDt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/EffectiveDate");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/ExpirationDt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ExpirationDate");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/CurrentTermAmt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/FullTermPremium");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }
        //                    oElementNode = objEles.XPathSelectElement("./Coverage/com.csc_OriginalPremium");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/OrginialPremium");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }
        //                    oElementNode = objEles.XPathSelectElement("./Coverage/com.csc_WrittenPremium");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/WrittenPremium");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }
        //                    oElementNode = objEles.XPathSelectElement("./Coverage/com.csc_TotalPremium");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/TotalWrittenPremium");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/Deductible/FormatCurrencyAmt/Amt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/SelfInsureDeduct");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }
        //                    oElementNode = objEles.XPathSelectElement("./Coverage/Deductible/FormatCurrencyAmt/Amt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/SelfInsureDeduct");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }
        //                    oElements = objEles.XPathSelectElements("./Coverage/Limit");
        //                    if (oElements != null)
        //                    {
        //                        foreach (XElement objElement in oElements)
        //                        {
        //                            XElement objEle = objElement.XPathSelectElement("./LimitAppliesToCd");
        //                            XElement oSubElement = objElement.XPathSelectElement("./TaxId");
        //                            if (objEle != null && string.Equals(objEle.Value, "OccurenceLimit", StringComparison.InvariantCultureIgnoreCase))
        //                            {
        //                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/OccurrenceLimit ");
        //                                if (oDataModelElement != null)
        //                                    oDataModelElement.Value = oElementNode.Value;
        //                            }
        //                            if (objEle != null && string.Equals(objEle.Value, "AggregateLimit", StringComparison.InvariantCultureIgnoreCase))
        //                            {
        //                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyLimit ");
        //                                if (oDataModelElement != null)
        //                                    oDataModelElement.Value = oElementNode.Value;
        //                            }


        //                            objEle = null;
        //                            oSubElement = null;
        //                        }
        //                    }

        //                    oElements = null;


        //                    oElements = objEles.XPathSelectElements("./ItemIdInfo/OtherIdentifier");
        //                    if (oElements != null)
        //                    {
        //                        foreach (XElement objElement in oElements)
        //                        {
        //                            XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
        //                            XElement oSubElement = objElement.XPathSelectElement("./OtherId");
        //                            if (objEle != null && string.Equals(objEle.Value, "com.csc_CoverageSeq", StringComparison.InvariantCultureIgnoreCase))
        //                            {
        //                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CvgSequenceNo");
        //                                if (oDataModelElement != null)
        //                                    oDataModelElement.Value = oSubElement.Value;
        //                            }


        //                            objEle = null;
        //                            oSubElement = null;
        //                        }
        //                    }
        //                    oElements = null;
        //                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyId");
        //                    if (oDataModelElement != null)
        //                        oDataModelElement.Value = "0";

        //                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyUnitRowId");
        //                    if (oDataModelElement != null)
        //                        oDataModelElement.Value = iPolicyUnitId.ToString();
        //                    int iPolicyCVGRowId = objManager.CheckCoverageDuplication(iPolicyUnitId, oAcordResponse);
        //                    iRecordId = objManager.SaveCoverage(oDataModelTemplate.ToString(),iPolicyCVGRowId);

        //                    bool bResult = objManager.SavePolicyDownloadXMLData(iRecordId, oAcordResponse, objManager.GetPolicySystemId(iPolicyId), "POLICY_X_CVG_TYPE");

        //                }
        //            }


        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
        //    }
        //    finally
        //    {
        //        oElements = null;
        //        if (oSaveAcordXML != null)
        //            oSaveAcordXML = null;
        //        if (oElement != null)
        //            oElement = null;
        //        if (oElementNode != null)
        //            oElementNode = null;
        //        if (oDataModelTemplate != null)
        //            oDataModelTemplate = null;
        //        if (oDataModelElement != null)
        //            oDataModelElement = null;
        //        if (objCache != null)
        //            objCache.Dispose();
        //    }
        //}

        //public void GetWLCvgSaveFormattedResult(string oAcordResponse, ref BusinessAdaptorErrors p_objErrOut, int iPolicyUnitId, int iPolicyId)
        //{
        //    XElement oAcordXML = null;
        //    XElement oDataModelTemplate = null;
        //    XElement oSaveAcordXML = null;
        //    XElement oElement = null;
        //    XElement oElementNode = null;
        //    XElement oDataModelElement = null;
        //    IEnumerable<XElement> oElements = null;
        //    string sUnitNo = string.Empty;
        //    LocalCache objCache = null;
        //    int iRecordId = 0;
        //    PolicySystemInterface objManager = null;
        //    int iCodeId = 0;
        //    try
        //    {
        //        oAcordXML = XElement.Parse(oAcordResponse);
        //        if (oAcordXML != null)
        //        {
        //            objManager = new PolicySystemInterface(m_userLogin);
        //            foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo"))
        //            {
        //                oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Coverage", ref p_objErrOut));
        //                if (oDataModelTemplate != null)
        //                {
        //                    oElementNode = objEles.XPathSelectElement("./com.csc_ChangeDt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {

        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ChangeDate");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/CoverageCd");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageTypeCode");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "COVERAGE_TYPE", "CoverageType on GetWLCvgSaveFormattedResult").ToString();
        //                    }
        //                    oElementNode = objEles.XPathSelectElement("./Coverage/com.csc_Exposure");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Exposure");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    objCache = new LocalCache(m_connectionString);

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/EffectiveDt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/EffectiveDate");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/ExpirationDt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ExpirationDate");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/CurrentTermAmt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/FullTermPremium");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }


        //                    oElementNode = objEles.XPathSelectElement("./Coverage/Limit/FormatCurrencyAmt/Amt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Limit");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }
        //                    oElementNode = objEles.XPathSelectElement("./Coverage/Deductible/FormatCurrencyAmt/Amt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/SelfInsureDeduct");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElements = objEles.XPathSelectElements("./ItemIdInfo/OtherIdentifier");
        //                    if (oElements != null)
        //                    {
        //                        foreach (XElement objElement in oElements)
        //                        {
        //                            XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
        //                            XElement oSubElement = objElement.XPathSelectElement("./OtherId");
        //                            if (objEle != null && string.Equals(objEle.Value, "com.csc_ClassSeq", StringComparison.InvariantCultureIgnoreCase))
        //                            {
        //                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CvgSequenceNo");
        //                                if (oDataModelElement != null)
        //                                    oDataModelElement.Value = oSubElement.Value;
        //                            }


        //                            objEle = null;
        //                            oSubElement = null;
        //                        }
        //                    }

        //                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyId");
        //                    if (oDataModelElement != null)
        //                        oDataModelElement.Value = "0";

        //                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyUnitRowId");
        //                    if (oDataModelElement != null)
        //                        oDataModelElement.Value = iPolicyUnitId.ToString();
        //                    int iPolicyCVGRowId = objManager.CheckCoverageDuplication(iPolicyUnitId, oAcordResponse);
        //                    iRecordId = objManager.SaveCoverage(oDataModelTemplate.ToString(), iPolicyCVGRowId);


        //                    bool bResult = objManager.SavePolicyDownloadXMLData(iRecordId, oAcordResponse, objManager.GetPolicySystemId(iPolicyId), "POLICY_X_CVG_TYPE");

        //                }
        //            }


        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
        //    }
        //    finally
        //    {
        //        oElements = null;
        //        if (oSaveAcordXML != null)
        //            oSaveAcordXML = null;
        //        if (oElement != null)
        //            oElement = null;
        //        if (oElementNode != null)
        //            oElementNode = null;
        //        if (oDataModelTemplate != null)
        //            oDataModelTemplate = null;
        //        if (oDataModelElement != null)
        //            oDataModelElement = null;
        //        if (objCache != null)
        //            objCache.Dispose();
        //    }
        //}

        //public void GetALCvgSaveFormattedResult(string oAcordResponse,  ref BusinessAdaptorErrors p_objErrOut,int iPolicyUnitId,int iPolicyId)
        //{
        //    XElement oAcordXML = null;
        //    XElement oDataModelTemplate = null;
        //    XElement oSaveAcordXML = null;
        //    XElement oElement = null;
        //    XElement oElementNode = null;
        //    XElement oDataModelElement = null;
        //    IEnumerable<XElement> oElements = null;
        //    string sUnitNo = string.Empty;
        //    LocalCache objCache = null;
        //    int iRecordId = 0;
        //    PolicySystemInterface objManager = null;
        //    string sCvgSequenNo = string.Empty;
        //    string sTempCvgSequenNo = string.Empty;
        //    string sTempCvgCode = string.Empty;
        //    string sCvgCode = string.Empty;
        //    XElement objTemp = null;
        //    try
        //    {
        //        oAcordXML = XElement.Parse(oAcordResponse);
        //        if (oAcordXML != null)
        //        {
        //            objManager = new PolicySystemInterface(m_userLogin);
        //            foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitCoverageListRs/com.csc_CoverageLossInfo"))
        //            {
        //                oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Coverage", ref p_objErrOut));
        //                if (oDataModelTemplate != null)
        //                {
        //                    oElementNode = objEles.XPathSelectElement("./com.csc_ChangeDt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {

        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ChangeDate");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/CoverageCd");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageTypeCode");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "COVERAGE_TYPE", "CoverageType on GetALCvgSaveFormattedResult").ToString();
        //                        sCvgCode = oElementNode.Value;

        //                    }



        //                    oElementNode = objEles.XPathSelectElement("./Coverage/com.csc_Exposure");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Exposure");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    objCache = new LocalCache(m_connectionString);

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/EffectiveDt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/EffectiveDate");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/ExpirationDt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ExpirationDate");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }


        //                    oElementNode = objEles.XPathSelectElement("./Coverage/Limit/FormatCurrencyAmt/Amt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Limit");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }
        //                    oElementNode = objEles.XPathSelectElement("./Coverage/Deductible/FormatCurrencyAmt/Amt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/SelfInsureDeduct");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/CurrentTermAmt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/FullTermPremium");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElements = objEles.XPathSelectElements("./ItemIdInfo/OtherIdentifier");
        //                    if (oElements != null)
        //                    {
        //                        foreach (XElement objElement in oElements)
        //                        {
        //                            XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
        //                            XElement oSubElement = objElement.XPathSelectElement("./OtherId");
        //                            if (objEle != null && string.Equals(objEle.Value, "com.csc_CoverageSeq", StringComparison.InvariantCultureIgnoreCase))
        //                            {
        //                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CvgSequenceNo");
        //                                if (oDataModelElement != null)
        //                                    oDataModelElement.Value = oSubElement.Value;
        //                                sCvgSequenNo = oSubElement.Value;
        //                            }


        //                            objEle = null;
        //                            oSubElement = null;
        //                        }
        //                    }

        //                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyId");
        //                    if (oDataModelElement != null)
        //                        oDataModelElement.Value ="0";

        //                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyUnitRowId");
        //                    if (oDataModelElement != null)
        //                        oDataModelElement.Value = iPolicyUnitId.ToString();

        //                    XElement objTempElement = new XElement("LOBCd");


        //                    if (oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_UnitCoverageListRs/LOBCd") != null)
        //                        objTempElement.Value = oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_UnitCoverageListRs/LOBCd").Value;



        //                    objEles.AddFirst(objTempElement);

        //                    int iPolicyCVGRowId = objManager.CheckCoverageDuplication(iPolicyUnitId, objEles.ToString());
        //                    iRecordId = objManager.SaveCoverage(oDataModelTemplate.ToString(), iPolicyCVGRowId);
        //                     objTemp = XElement.Parse(oAcordResponse);
        //                    IEnumerable<XElement> objTempEls=objTemp.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitCoverageListRs/com.csc_CoverageLossInfo");
        //                    for (int i = objTempEls.Count() - 1; i >=0; i--)
        //                    {
        //                        objTempEls.ElementAt(i).Remove();
        //                    }
        //                    objTemp.XPathSelectElement("./ClaimsSvcRs/com.csc_UnitCoverageListRs").Add(objEles);

        //                    bool bResult = objManager.SavePolicyDownloadXMLData(iRecordId, objTemp.ToString(), objManager.GetPolicySystemId(iPolicyId), "POLICY_X_CVG_TYPE");

        //                }
        //            }
        //            //foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo"))
        //            //{
        //            //    oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Coverage", ref p_objErrOut));
        //            //    if (oDataModelTemplate != null)
        //            //    {
        //            //        oElementNode = objEles.XPathSelectElement("./com.csc_ChangeDt");
        //            //        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //            //        {

        //            //            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ChangeDate");
        //            //            if (oDataModelElement != null)
        //            //                oDataModelElement.Value = oElementNode.Value;

        //            //        }

        //            //        oElementNode = objEles.XPathSelectElement("./Coverage/CoverageCd");
        //            //        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //            //        {
        //            //            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageTypeCode");
        //            //            if (oDataModelElement != null)
        //            //                oDataModelElement.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "COVERAGE_TYPE", "CoverageType on GetALCvgSaveFormattedResult").ToString();

        //            //        }



        //            //        oElementNode = objEles.XPathSelectElement("./Coverage/com.csc_Exposure");
        //            //        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //            //        {
        //            //            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Exposure");
        //            //            if (oDataModelElement != null)
        //            //                oDataModelElement.Value = oElementNode.Value;

        //            //        }

        //            //        objCache = new LocalCache(m_connectionString);

        //            //        oElementNode = objEles.XPathSelectElement("./Coverage/EffectiveDt");
        //            //        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //            //        {
        //            //            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/EffectiveDt");
        //            //            if (oDataModelElement != null)
        //            //                oDataModelElement.Value = oElementNode.Value;

        //            //        }

        //            //        oElementNode = objEles.XPathSelectElement("./Coverage/ExpirationDt");
        //            //        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //            //        {
        //            //            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ExpirationDate");
        //            //            if (oDataModelElement != null)
        //            //                oDataModelElement.Value = oElementNode.Value;

        //            //        }


        //            //        oElementNode = objEles.XPathSelectElement("./Coverage/Limit/FormatCurrencyAmt/Amt");
        //            //        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //            //        {
        //            //            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Limit");
        //            //            if (oDataModelElement != null)
        //            //                oDataModelElement.Value = oElementNode.Value;

        //            //        }
        //            //        oElementNode = objEles.XPathSelectElement("./Coverage/Deductible/FormatCurrencyAmt/Amt");
        //            //        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //            //        {
        //            //            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/SelfInsureDeduct");
        //            //            if (oDataModelElement != null)
        //            //                oDataModelElement.Value = oElementNode.Value;

        //            //        }

        //            //        oElements = objEles.XPathSelectElements("./ItemIdInfo/OtherIdentifier");
        //            //        if (oElements != null)
        //            //        {
        //            //            foreach (XElement objElement in oElements)
        //            //            {
        //            //                XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
        //            //                XElement oSubElement = objElement.XPathSelectElement("./TaxId");
        //            //                if (objEle != null && string.Equals(objEle.Value, "com.csc_CoverageSeq", StringComparison.InvariantCultureIgnoreCase))
        //            //                {
        //            //                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CvgSequenceNo");
        //            //                    if (oDataModelElement != null)
        //            //                        oDataModelElement.Value = oElementNode.Value;
        //            //                }


        //            //                objEle = null;
        //            //                oSubElement = null;
        //            //            }
        //            //        }

        //            //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyId");
        //            //        if (oDataModelElement != null)
        //            //            oDataModelElement.Value = "0";

        //            //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyUnitRowId");
        //            //        if (oDataModelElement != null)
        //            //            oDataModelElement.Value = iPolicyUnitId.ToString();

        //            //        int iPolicyCVGRowId = objManager.CheckCoverageDuplication(iPolicyUnitId, oAcordResponse);
        //            //        iRecordId = objManager.SaveCoverage(oDataModelTemplate.ToString(), iPolicyCVGRowId);

        //            //        bool bResult = objManager.SavePolicyDownloadXMLData(iRecordId, oAcordResponse, objManager.GetPolicySystemId(iPolicyId), "POLICY_X_CVG_TYPE");

        //            //    }
        //        //    }




        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
        //    }
        //    finally
        //    {
        //        oElements = null;
        //        if (oSaveAcordXML != null)
        //            oSaveAcordXML = null;
        //        if (oElement != null)
        //            oElement = null;
        //        if (oElementNode != null)
        //            oElementNode = null;
        //        if (oDataModelTemplate != null)
        //            oDataModelTemplate = null;
        //        if (oDataModelElement != null)
        //            oDataModelElement = null;
        //        if (objCache != null)
        //            objCache.Dispose();
        //    }
        //}

        //public void GetPLCvgSaveFormattedResult(string oAcordResponse, ref BusinessAdaptorErrors p_objErrOut, int iPolicyUnitId, int iPolicyId)
        //{
        //    XElement oAcordXML = null;
        //    XElement oDataModelTemplate = null;
        //    XElement oSaveAcordXML = null;
        //    XElement oElement = null;
        //    XElement oElementNode = null;
        //    XElement oDataModelElement = null;
        //    IEnumerable<XElement> oElements = null;
        //    string sUnitNo = string.Empty;
        //    LocalCache objCache = null;
        //    int iRecordId = 0;
        //    PolicySystemInterface objManager = null;
        //    try
        //    {
        //        oAcordXML = XElement.Parse(oAcordResponse);
        //        if (oAcordXML != null)
        //        {
        //            objManager = new PolicySystemInterface(m_userLogin);
        //            foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo/com.csc_CoverageLossInfo/Coverage"))
        //            {
        //                oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Coverage", ref p_objErrOut));
        //                if (oDataModelTemplate != null)
        //                {


        //                    oElementNode = objEles.XPathSelectElement("./CoverageCd");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageTypeCode");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "COVERAGE_TYPE", "CoverageType on GetPLCvgSaveFormattedResult").ToString();





        //                        oElementNode = objEles.XPathSelectElement("./Limit/LimitAmt");
        //                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                        {
        //                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Limit");
        //                            if (oDataModelElement != null)
        //                                oDataModelElement.Value = oElementNode.Value;

        //                        }




        //                    oElementNode = objEles.XPathSelectElement("./CurrentTermAmt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/FullTermPremium");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }
        //                  oElementNode=  oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/PropertyLossInfo/com.csc_CoverageLossInfo/Deductible/DeductibleAmt");
        //                        if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                        {
        //                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/SelfInsureDeduct");
        //                            if (oDataModelElement != null)
        //                                oDataModelElement.Value = oElementNode.Value;

        //                        }



        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyId");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = "0";

        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyUnitRowId");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = iPolicyUnitId.ToString();
        //                        XElement objTempElement = new XElement("LOBCd");


        //                        if (oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/Policy/LOBCd") != null)
        //                            objTempElement.Value = oAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/Policy/LOBCd").Value;



        //                        objEles.AddFirst(objTempElement);



        //                        int iPolicyCVGRowId = objManager.CheckCoverageDuplication(iPolicyUnitId, objEles.ToString());
        //                        iRecordId = objManager.SaveCoverage(oDataModelTemplate.ToString(), iPolicyCVGRowId);

        //                        bool bResult = objManager.SavePolicyDownloadXMLData(iRecordId, objEles.ToString(), objManager.GetPolicySystemId(iPolicyId), "POLICY_X_CVG_TYPE");

        //                    }
        //                }
        //            }

        //        }
        //        }
        //    //}
        //    catch (Exception e)
        //    {
        //        p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
        //    }
        //    finally
        //    {
        //        oElements = null;
        //        if (oSaveAcordXML != null)
        //            oSaveAcordXML = null;
        //        if (oElement != null)
        //            oElement = null;
        //        if (oElementNode != null)
        //            oElementNode = null;
        //        if (oDataModelTemplate != null)
        //            oDataModelTemplate = null;
        //        if (oDataModelElement != null)
        //            oDataModelElement = null;
        //        if (objCache != null)
        //            objCache.Dispose();
        //    }
        //}

        //public void GetCLCvgSaveFormattedResult(string oAcordResponse, ref BusinessAdaptorErrors p_objErrOut, int iPolicyUnitId, int iPolicyId)
        //{
        //    XElement oAcordXML = null;
        //    XElement oDataModelTemplate = null;
        //    XElement oSaveAcordXML = null;
        //    XElement oElement = null;
        //    XElement oElementNode = null;
        //    XElement oDataModelElement = null;
        //    IEnumerable<XElement> oElements = null;
        //    string sUnitNo = string.Empty;
        //    LocalCache objCache = null;
        //    int iRecordId = 0;
        //    PolicySystemInterface objManager = null;
        //    int iCodeId = 0;
        //    try
        //    {
        //        oAcordXML = XElement.Parse(oAcordResponse);
        //        if (oAcordXML != null)
        //        {
        //            objManager = new PolicySystemInterface(m_userLogin);
        //            foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo"))
        //            {
        //                oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Coverage", ref p_objErrOut));
        //                if (oDataModelTemplate != null)
        //                {
        //                    oElementNode = objEles.XPathSelectElement("./com.csc_ChangeDt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {

        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ChangeDate");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/CoverageCd");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageTypeCode");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "COVERAGE_TYPE", "CoverageType on GetCLCvgSaveFormattedResult").ToString();

        //                    }



        //                    oElementNode = objEles.XPathSelectElement("./Coverage/com.csc_Exposure");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Exposure");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    objCache = new LocalCache(m_connectionString);

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/EffectiveDt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/EffectiveDate");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/ExpirationDt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ExpirationDate");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/CurrentTermAmt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/FullTermPremium");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }


        //                    oElementNode = objEles.XPathSelectElement("./Coverage/Limit/FormatCurrencyAmt/Amt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Limit");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }
        //                    oElementNode = objEles.XPathSelectElement("./Coverage/Deductible/FormatCurrencyAmt/Amt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/SelfInsureDeduct");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElements = objEles.XPathSelectElements("./ItemIdInfo/OtherIdentifier");
        //                    if (oElements != null)
        //                    {
        //                        foreach (XElement objElement in oElements)
        //                        {
        //                            XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
        //                            XElement oSubElement = objElement.XPathSelectElement("./TaxId");
        //                            if (objEle != null && string.Equals(objEle.Value, "com.csc_CoverageSeq", StringComparison.InvariantCultureIgnoreCase))
        //                            {
        //                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CvgSequenceNo");
        //                                if (oDataModelElement != null)
        //                                    oDataModelElement.Value = oElementNode.Value;
        //                            }


        //                            objEle = null;
        //                            oSubElement = null;
        //                        }
        //                    }

        //                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyId");
        //                    if (oDataModelElement != null)
        //                        oDataModelElement.Value = "0";

        //                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyUnitRowId");
        //                    if (oDataModelElement != null)
        //                        oDataModelElement.Value = iPolicyUnitId.ToString();

        //                    int iPolicyCVGRowId = objManager.CheckCoverageDuplication(iPolicyUnitId, oAcordResponse);
        //                    iRecordId = objManager.SaveCoverage(oDataModelTemplate.ToString(), iPolicyCVGRowId);

        //                    bool bResult = objManager.SavePolicyDownloadXMLData(iRecordId, oAcordResponse, objManager.GetPolicySystemId(iPolicyId), "POLICY_X_CVG_TYPE");

        //                }
        //            }


        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
        //    }
        //    finally
        //    {
        //        oElements = null;
        //        if (oSaveAcordXML != null)
        //            oSaveAcordXML = null;
        //        if (oElement != null)
        //            oElement = null;
        //        if (oElementNode != null)
        //            oElementNode = null;
        //        if (oDataModelTemplate != null)
        //            oDataModelTemplate = null;
        //        if (oDataModelElement != null)
        //            oDataModelElement = null;
        //        if (objCache != null)
        //            objCache.Dispose();
        //    }
        //}

        //public void GetPLOptionalCvgSaveFormattedResult(string oAcordResponse, ref BusinessAdaptorErrors p_objErrOut, int iPolicyUnitId, int iPolicyId)
        //{
        //    XElement oAcordXML = null;
        //    XElement oDataModelTemplate = null;
        //    XElement oSaveAcordXML = null;
        //    XElement oElement = null;
        //    XElement oElementNode = null;
        //    XElement oDataModelElement = null;
        //    IEnumerable<XElement> oElements = null;
        //    string sUnitNo = string.Empty;
        //    LocalCache objCache = null;
        //    int iRecordId = 0;
        //    PolicySystemInterface objManager = null;
        //    int iCodeId = 0;
        //    try
        //    {
        //        oAcordXML = XElement.Parse(oAcordResponse);
        //        if (oAcordXML != null)
        //        {
        //            objManager = new PolicySystemInterface(m_userLogin);
        //            foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitCoverageDetailRs/com.csc_CoverageLossInfo"))
        //            {
        //                oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Coverage", ref p_objErrOut));
        //                if (oDataModelTemplate != null)
        //                {
        //                    oElementNode = objEles.XPathSelectElement("./com.csc_ChangeDt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {

        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ChangeDate");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/CoverageCd");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageTypeCode");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "COVERAGE_TYPE", "CoverageType on GetPLOptionalCvgSaveFormattedResult").ToString();

        //                    }



        //                    oElementNode = objEles.XPathSelectElement("./Coverage/com.csc_Exposure");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Exposure");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    objCache = new LocalCache(m_connectionString);

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/CurrentTermAmt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/FullTermPremium");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }
        //                    oElementNode = objEles.XPathSelectElement("./Coverage/EffectiveDt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/EffectiveDate");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElementNode = objEles.XPathSelectElement("./Coverage/ExpirationDt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ExpirationDate");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }


        //                    oElementNode = objEles.XPathSelectElement("./Coverage/Limit/FormatCurrencyAmt/Amt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Limit");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }
        //                    oElementNode = objEles.XPathSelectElement("./Coverage/Deductible/FormatCurrencyAmt/Amt");
        //                    if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
        //                    {
        //                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/SelfInsureDeduct");
        //                        if (oDataModelElement != null)
        //                            oDataModelElement.Value = oElementNode.Value;

        //                    }

        //                    oElements = objEles.XPathSelectElements("./ItemIdInfo/OtherIdentifier");
        //                    if (oElements != null)
        //                    {
        //                        foreach (XElement objElement in oElements)
        //                        {
        //                            XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
        //                            XElement oSubElement = objElement.XPathSelectElement("./OtherId");
        //                            if (objEle != null && string.Equals(objEle.Value, "com.csc_CoverageSeq", StringComparison.InvariantCultureIgnoreCase))
        //                            {
        //                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CvgSequenceNo");
        //                                if (oDataModelElement != null)
        //                                    oDataModelElement.Value = oElementNode.Value;
        //                            }


        //                            objEle = null;
        //                            oSubElement = null;
        //                        }
        //                    }

        //                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyId");
        //                    if (oDataModelElement != null)
        //                        oDataModelElement.Value = "0";
        //                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Remarks");
        //                    if (oDataModelElement != null)
        //                        oDataModelElement.Value = "Optional Coverages";

        //                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyUnitRowId");
        //                    if (oDataModelElement != null)
        //                        oDataModelElement.Value = iPolicyUnitId.ToString();

        //                    int iPolicyCVGRowId = objManager.CheckCoverageDuplication(iPolicyUnitId, oAcordResponse);
        //                    iRecordId = objManager.SaveCoverage(oDataModelTemplate.ToString(), iPolicyCVGRowId);

        //                    bool bResult = objManager.SavePolicyDownloadXMLData(iRecordId, oAcordResponse, objManager.GetPolicySystemId(iPolicyId), "POLICY_X_CVG_TYPE");

        //                }
        //            }


        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
        //    }
        //    finally
        //    {
        //        oElements = null;
        //        if (oSaveAcordXML != null)
        //            oSaveAcordXML = null;
        //        if (oElement != null)
        //            oElement = null;
        //        if (oElementNode != null)
        //            oElementNode = null;
        //        if (oDataModelTemplate != null)
        //            oDataModelTemplate = null;
        //        if (oDataModelElement != null)
        //            oDataModelElement = null;
        //        if (objCache != null)
        //            objCache.Dispose();
        //    }
        //}

        public int GetSiteUnitDetailSaveFormattedResult(string oAcordResponse, SaveDownloadOptions oSaveDownload, ref BusinessAdaptorErrors p_objErrOut)
        {
            XElement oAcordXML = null;
            XElement oDataModelTemplate = null;
            XElement oSaveAcordXML = null;
            XElement oElement = null;
            XElement oElementNode = null;
            XElement oDataModelElement = null;
            //string sFileName = "Unit_" + userLogin.LoginName + "_tmp.xml";
            //StringBuilder objBuilder = null;
            string sAddr = string.Empty;
            IEnumerable<XElement> oElements = null;
            string sUnitNo = string.Empty;
            LocalCache objCache = null;
            int iRecordId = 0;
            PolicySystemInterface objManager = null;
            //string sUnitNumber = string.Empty;
            string sSiteSeqNumber = string.Empty;
            string sInsLine = string.Empty;
            string sPolicySystemID = string.Empty; // aaggarwal29: Code mapping change
            try
            {
                objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                //DeleteFile(sFileName);
                oAcordXML = XElement.Parse(oAcordResponse);
                sPolicySystemID = oSaveDownload.PolicySystemId.ToString(); // aaggarwal29: Code mapping change
                if (oAcordXML != null)
                {
                    foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyUnitFetchRs/WorkCompLocInfo"))
                    {
                        oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("SiteUnit", ref p_objErrOut));
                        if (oDataModelTemplate != null)
                        {
                            oElementNode = objEles.XPathSelectElement("./Location/Addr/Addr1");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {

                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./SiteUnit/Adsress1");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;
                            }

                            oElementNode = objEles.XPathSelectElement("./Location/Addr/Addr2");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./SiteUnit/Adsress2");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;
                            }



                            oElementNode = objEles.XPathSelectElement("./Location/Addr/City");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./SiteUnit/City");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                            }

                            objCache = new LocalCache(m_connectionString, base.ClientId);

                            oElementNode = objEles.XPathSelectElement("./Location/Addr/StateProvCd");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./SiteUnit/StateId");
                                if (oDataModelElement != null)
                                    //oDataModelElement.Attribute("codeid").Value = objCache.GetStateRowID(oElementNode.Value).ToString();
                                    objManager = new PolicySystemInterface(m_connectionString, base.ClientId);
                                oDataModelElement.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(oElementNode.Value, "STATES", "StateId on GetSiteUnitDetailSaveFormattedResult", sPolicySystemID).ToString(); // aaggarwal29: Code mapping change
                            }

                            oElementNode = objEles.XPathSelectElement("./Location/LocationName");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./SiteUnit/Name");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./SiteUnit/SiteNumber");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;
                            }

                            oElementNode = objEles.XPathSelectElement("./NumEmployees");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./SiteUnit/NoOfEmp");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;
                            }


                            oElementNode = objEles.XPathSelectElement("./SICCd");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./SiteUnit/SIC");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;
                            }

                            oElementNode = objEles.XPathSelectElement("./Location/Addr/PostalCode");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./SiteUnit/ZipCode");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;
                            }

                            oElements = objEles.XPathSelectElements("./Location/TaxCodeInfo/TaxIdentity");
                            if (oElements != null)
                            {
                                foreach (XElement objElement in oElements)
                                {
                                    XElement objEle = objElement.XPathSelectElement("./TaxIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./TaxId");
                                    if (objEle != null && string.Equals(objEle.Value, "FEIN", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./SiteUnit/FEIN");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    else if (objEle != null && string.Equals(objEle.Value, "Unemployment", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./SiteUnit/UnemployementNumber");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }

                                    objEle = null;
                                    oSubElement = null;
                                }
                            }


                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/UnitNo");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sUnitNo;

                            oElements = null;
                            oElements = objEles.XPathSelectElements("./Location/ItemIdInfo/OtherIdentifier");
                            if (oElements != null)
                            {
                                foreach (XElement objElement in oElements)
                                {
                                    XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./OtherId");
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_UnitNumber", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        sUnitNo = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_SiteSequenceNum", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        sSiteSeqNumber = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_InsLineCd", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        sInsLine = oSubElement.Value;
                                    }


                                    objEle = null;
                                    oSubElement = null;
                                }
                            }


                            //oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/UnitNo");
                            //if (oDataModelElement != null)
                            //    oDataModelElement.Value = sUnitNo;

                            objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                            iRecordId = objManager.SaveSiteUnit(oDataModelTemplate.ToString(), oSaveDownload.PolicyId, oAcordResponse);
                            if (iRecordId > 0)
                            {
                                objManager.SavePolicyunitData(iRecordId, "S", sUnitNo, string.Empty, string.Empty, string.Empty, sInsLine, sSiteSeqNumber, sUnitNo);
                                iRecordId = objManager.SavePolicyXUnit(oSaveDownload.PolicyId, iRecordId, "S", oAcordResponse, Conversion.ConvertStrToInteger(sPolicySystemID));
                              //  bool bResult = objManager.SavePolicyDownloadXMLData(iRecordId, oAcordResponse, objManager.GetPolicySystemId(oSaveDownload.PolicyId), "POLICY_X_UNIT");
                            }
                        }
                    }


                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                oElements = null;
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElement != null)
                    oElement = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
                if (objCache != null)
                    objCache.Dispose();
            }
            return iRecordId;
        }

        public void GetMRUnitListFormattedResult(string oAcordResponse, ref BusinessAdaptorErrors p_objErrOut)
        {
            XElement oAcordXML = null;
            XElement oDataModelTemplate = null;
            XElement oSaveAcordXML = null;
            XElement oElement = null;
            XElement oElementNode = null;
            XElement oDataModelElement = null;
            string sFileName = "Unit_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml";
            StringBuilder objBuilder = null;
            string sAddr = string.Empty;
            IEnumerable<XElement> oElements = null;
            string sUnitNo = string.Empty;
            string sStatUnitNo = string.Empty;
            try
            {
                oAcordXML = XElement.Parse(oAcordResponse);
                DeleteFile(sFileName);
                if (oAcordXML != null)
                {
                    objBuilder = new StringBuilder();
                    foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyUnitListRs/InsuredOrPrincipal"))
                    {
                        oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("UnitList", ref p_objErrOut));
                        if (oDataModelTemplate != null)
                        {
                            //dbisht6 start mergin for mits 35655
                            oElementNode = objEles.XPathSelectElement("./com.csc_InsuredNameIdInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='DESCUSE1']/OtherId");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Vin");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;
                            }
                            // dbisht6 end
                            oElementNode = objEles.XPathSelectElement("./GeneralPartyInfo/NameInfo/CommlName/CommercialName");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                sAddr = oElementNode.Value;
                            }




                            oElementNode = objEles.XPathSelectElement("./GeneralPartyInfo/Addr/Addr1");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                if (string.IsNullOrEmpty(sAddr))
                                    sAddr = oElementNode.Value;
                                else
                                    sAddr = sAddr + " " + oElementNode.Value;

                            }

                            oElementNode = objEles.XPathSelectElement("./GeneralPartyInfo/Addr/Addr2");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                if (string.IsNullOrEmpty(sAddr))
                                    sAddr = oElementNode.Value;
                                else
                                    sAddr = sAddr + " " + oElementNode.Value;


                            }

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Desc");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sAddr;

                            sAddr = string.Empty;

                            oElementNode = objEles.XPathSelectElement("./GeneralPartyInfo/Addr/City");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/City");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                            }



                            oElementNode = objEles.XPathSelectElement("./GeneralPartyInfo/Addr/StateProvCd");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/State");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                            }

                            oElements = objEles.XPathSelectElements("./ItemIdInfo/OtherIdentifier");
                            if (oElements != null)
                            {
                                foreach (XElement objElement in oElements)
                                {
                                    XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./OtherId");
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_UnitNumber", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        sUnitNo = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_StatUnitNumber", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        sStatUnitNo = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_InsLineCd", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/InsLineCd");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_Product", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Product");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_RiskSubLoc", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/RiskSubLoc");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_RiskLoc", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/RiskLoc");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    objEle = null;
                                    oSubElement = null;
                                }
                            }



                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/SequenceNumber");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sUnitNo;

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/UnitNo");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sUnitNo;
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/StatUnitNo");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sStatUnitNo;

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/UnitType");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = "Stat";

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Status");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = "";


                        }
                        objBuilder.Append(oDataModelTemplate.ToString());
                    }
                    if (oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyUnitListRs/InsuredOrPrincipal").Count() > 0)
                        WriteFormattedXmlToFile(sFileName, "<Units>" + objBuilder.ToString() + "</Units>", ref p_objErrOut);

                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                oElements = null;
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElement != null)
                    oElement = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
                if (objBuilder != null)
                    objBuilder = null;
            }
        }

        public void GetUnitListFormattedResult(string oAcordResponse, string sBaseLOB, ref BusinessAdaptorErrors p_objErrOut)
        {
            XElement oAcordXML = null;
            XElement oDataModelTemplate = null;
            XElement oSaveAcordXML = null;
            XElement oElement = null;
            XElement oElementNode = null;
            XElement oDataModelElement = null;
            string sFileName = "Unit_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml";
            StringBuilder objBuilder = null;
            string sAddr = string.Empty;
            string sProduct = string.Empty;
            IEnumerable<XElement> oElements = null;
            string sUnitNo = string.Empty;
            string sStatUnitNo = string.Empty;
            int iSequenceNo = 1;
            try
            {
                oAcordXML = XElement.Parse(oAcordResponse);
                DeleteFile(sFileName);
                if (oAcordXML != null)
                {
                    objBuilder = new StringBuilder();

                    foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyUnitListRs/InsuredOrPrincipal"))
                    {
                        oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("UnitList", ref p_objErrOut));
                        if (oDataModelTemplate != null)
                        {
                            //dbisht6 start for merging mits 35655
                            oElementNode = objEles.XPathSelectElement("./com.csc_InsuredNameIdInfo/ItemIdInfo/OtherIdentifier[OtherIdTypeCd='DESCUSE1']/OtherId");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Vin");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;
                            }

                            //dbisht6 end
                            oElementNode = objEles.XPathSelectElement("./GeneralPartyInfo/NameInfo/CommlName/CommercialName");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                sAddr = oElementNode.Value;
                            }

                            oElementNode = objEles.XPathSelectElement("./GeneralPartyInfo/Addr/Addr1");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                if (string.IsNullOrEmpty(sAddr))
                                    sAddr = oElementNode.Value;
                                else
                                    sAddr = sAddr + " " + oElementNode.Value;

                            }

                            oElementNode = objEles.XPathSelectElement("./GeneralPartyInfo/Addr/Addr2");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                if (string.IsNullOrEmpty(sAddr))
                                    sAddr = oElementNode.Value;
                                else
                                    sAddr = sAddr + " " + oElementNode.Value;


                            }

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Desc");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sAddr;

                            sAddr = string.Empty;

                            oElementNode = objEles.XPathSelectElement("./GeneralPartyInfo/Addr/City");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/City");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                            }

                            oElementNode = objEles.XPathSelectElement("./GeneralPartyInfo/Addr/StateProvCd");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/State");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                            }

                            oElements = objEles.XPathSelectElements("./ItemIdInfo/OtherIdentifier");
                            if (oElements != null)
                            {
                                foreach (XElement objElement in oElements)
                                {
                                    XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./OtherId");
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_UnitNumber", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        sUnitNo = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_StatUnitNumber", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        sStatUnitNo = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_InsLineCd", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/InsLineCd");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_Product", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Product");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_RiskSubLoc", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/RiskSubLoc");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_RiskLoc", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/RiskLoc");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    objEle = null;
                                    oSubElement = null;
                                }
                            }

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/SequenceNumber");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = iSequenceNo.ToString();

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/UnitNo");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sUnitNo;
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/StatUnitNo");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sStatUnitNo;

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/UnitType");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = "Stat";

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Status");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = "";

                            iSequenceNo++;
                        }
                        objBuilder.Append(oDataModelTemplate.ToString());
                    }

                    foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyUnitListRs/AutoLossInfo"))
                    {
                        sProduct = string.Empty;
                        oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("UnitList", ref p_objErrOut));
                        if (oDataModelTemplate != null)
                        {
                            oElementNode = objEles.XPathSelectElement("com.csc_VehicleIdentificationNumber");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Vin");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;
                            }




                            oElements = objEles.XPathSelectElements("./VehInfo/ItemIdInfo/OtherIdentifier");
                            if (oElements != null)
                            {
                                foreach (XElement objElement in oElements)
                                {
                                    XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./OtherId");
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_UnitNumber", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        sUnitNo = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_StatUnitNumber", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        sStatUnitNo = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_RateState", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/State");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_InsLineCd", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/InsLineCd");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_Product", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Product");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                        sProduct = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_RiskSubLoc", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/RiskSubLoc");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_RiskLoc", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/RiskLoc");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }

                                    objEle = null;
                                    oSubElement = null;
                                }
                            }


                            oElementNode = objEles.XPathSelectElement("./VehInfo/Manufacturer");
                            if (oElementNode != null)// && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Desc");
                                if (oDataModelElement != null)
                                    if (sBaseLOB == "CL")
                                        oDataModelElement.Value = sProduct + " : " + oElementNode.Value;
                                    else
                                        oDataModelElement.Value = oElementNode.Value;
                            }


                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/SequenceNumber");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = iSequenceNo.ToString();

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/UnitNo");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sUnitNo;

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/UnitType");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = "Vehicle";
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/City");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = "";

                            oElementNode = objEles.XPathSelectElement("./com.csc_UnitLossData/com.csc_UnitStatus");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Status");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                            }

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/StatUnitNo");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sStatUnitNo;



                            iSequenceNo++;
                        }
                        objBuilder.Append(oDataModelTemplate.ToString());
                    }

                    foreach (XElement objEles in oAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyUnitListRs/PropertyLossInfo"))
                    {
                        sProduct = string.Empty;
                        oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("UnitList", ref p_objErrOut));
                        if (oDataModelTemplate != null)
                        {

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/City");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = "";

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Vin");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = "";



                            oElements = objEles.XPathSelectElements("./ItemIdInfo/OtherIdentifier");
                            if (oElements != null)
                            {
                                foreach (XElement objElement in oElements)
                                {
                                    XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./OtherId");
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_UnitNumber", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        sUnitNo = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_StatUnitNumber", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        sStatUnitNo = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_RateState", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/State");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_InsLineCd", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/InsLineCd");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_Product", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Product");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                        sProduct = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_RiskSubLoc", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/RiskSubLoc");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_RiskLoc", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/RiskLoc");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oSubElement.Value;
                                    }
                                    objEle = null;
                                    oSubElement = null;
                                }
                            }

                            oElementNode = objEles.XPathSelectElement("./com.csc_UnitLossData/com.csc_UnitDescription/com.csc_UnitDesc");
                            if (oElementNode != null)// && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Desc");
                                if (oDataModelElement != null)
                                {
                                    if (sBaseLOB == "CL")
                                        oDataModelElement.Value = sProduct + " : " + oElementNode.Value;
                                    else
                                        oDataModelElement.Value = oElementNode.Value;
                                }
                            }

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/SequenceNumber");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = iSequenceNo.ToString();

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/UnitNo");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sUnitNo;

                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/UnitType");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = "Property";


                            oElementNode = objEles.XPathSelectElement("./com.csc_UnitStatus");
                            if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                            {
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/Status");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = oElementNode.Value;

                            }
                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Unit/StatUnitNo");
                            if (oDataModelElement != null)
                                oDataModelElement.Value = sStatUnitNo;


                            iSequenceNo++;


                        }
                        objBuilder.Append(oDataModelTemplate.ToString());
                    }
                    if (!string.IsNullOrEmpty(objBuilder.ToString()))
                        WriteFormattedXmlToFile(sFileName, "<Units>" + objBuilder.ToString() + "</Units>", ref p_objErrOut);

                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                oElements = null;
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElement != null)
                    oElement = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
                if (objBuilder != null)
                    objBuilder = null;
            }
        }

        public string GetPolicyInquireAcordRequest(PolicyEnquiry oPolicyEnquiry, ref BusinessAdaptorErrors p_objErrOut)
        {
            string oAcordRequest = string.Empty;
            XElement oElement = null;
            XElement oTemplate = GetPolicyInquireAcordTemplate();
            PopulateCommonAcordRequestParameters(ref oTemplate, oPolicyEnquiry.PolicySystemId, ref p_objErrOut);
            IEnumerable<XElement> oElements = null;
            try
            {
                if (!string.IsNullOrEmpty(oPolicyEnquiry.PolicyIdentfier) && (oPolicyEnquiry.PolicySystemId > 0))
                {
                    oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                    if (oElement != null)
                    {
                        oElement.Value = oPolicyEnquiry.Token;
                        oElement = null;
                    }

                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber");
                    if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolicyNumber))
                    {
                        oElement.Value = oPolicyEnquiry.PolicyNumber;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/CompanyProductCd");
                    if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolicySymbol))
                    {
                        oElement.Value = oPolicyEnquiry.PolicySymbol;
                        oElement = null;
                    }
                    oElements = oTemplate.XPathSelectElements("./ClaimsSvcRq/com.csc_PolicyInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");
                    if (oElements != null)
                    {
                        foreach (XElement objElement in oElements)
                        {
                            XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                            XElement oSubElement = objElement.XPathSelectElement("./OtherId");
                            if (objEle != null && string.Equals(objEle.Value, "com.csc_MasterCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicyEnquiry.MasterCompany))
                            {
                                oSubElement.Value = oPolicyEnquiry.MasterCompany;
                            }
                            else if (objEle != null && string.Equals(objEle.Value, "com.csc_LocCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicyEnquiry.Location))
                            {
                                oSubElement.Value = oPolicyEnquiry.Location;
                            }
                            else if (objEle != null && string.Equals(objEle.Value, "com.csc_Module", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicyEnquiry.Module))
                            {
                                oSubElement.Value = oPolicyEnquiry.Module;
                            }
                            objEle = null;
                            oSubElement = null;

                        }
                    }
                    oElements = null;
                }

                if (string.IsNullOrEmpty(oTemplate.ToString()))
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicyInquireAcordRequest.RequestOperationError", base.ClientId));
                }
                else
                    oAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
            }

            return oAcordRequest;
        }

        public string GetPolicySaveAcordRequest(PolicySaveRequest oPolicySaveRequest, ref BusinessAdaptorErrors p_objErrOut)
        {

            string oAcordRequest = string.Empty;
            XElement oElement = null;
            XElement oTemplate = GetPolicyInquireAcordTemplate();
            PopulateCommonAcordRequestParameters(ref oTemplate, oPolicySaveRequest.PolicySystemId, ref p_objErrOut);
            IEnumerable<XElement> oElements = null;
            try
            {
                if (!string.IsNullOrEmpty(oPolicySaveRequest.PolicyIdentfier) && (oPolicySaveRequest.PolicySystemId > 0))
                {
                    oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                    if (oElement != null)
                    {
                        oElement.Value = oPolicySaveRequest.Token;
                        oElement = null;
                    }

                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber");
                    if (oElement != null && !string.IsNullOrEmpty(oPolicySaveRequest.PolicyNumber))
                    {
                        oElement.Value = oPolicySaveRequest.PolicyNumber;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/CompanyProductCd");
                    if (oElement != null && !string.IsNullOrEmpty(oPolicySaveRequest.PolicySymbol))
                    {
                        oElement.Value = oPolicySaveRequest.PolicySymbol;
                        oElement = null;
                    }
                    oElements = oTemplate.XPathSelectElements("./ClaimsSvcRq/com.csc_PolicyInquiryRq/com.csc_SearchInfo/com.csc_SearchCriteria/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");
                    if (oElements != null)
                    {
                        foreach (XElement objElement in oElements)
                        {
                            XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                            XElement oSubElement = objElement.XPathSelectElement("./OtherId");
                            if (objEle != null && string.Equals(objEle.Value, "com.csc_MasterCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicySaveRequest.MasterCompany))
                            {
                                oSubElement.Value = oPolicySaveRequest.MasterCompany;
                            }
                            else if (objEle != null && string.Equals(objEle.Value, "com.csc_LocCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicySaveRequest.Location))
                            {
                                oSubElement.Value = oPolicySaveRequest.Location;
                            }
                            else if (objEle != null && string.Equals(objEle.Value, "com.csc_Module", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicySaveRequest.Module))
                            {
                                oSubElement.Value = oPolicySaveRequest.Module;
                            }
                            objEle = null;
                            oSubElement = null;
                        }
                    }
                    oElements = null;
                }

                if (string.IsNullOrEmpty(oTemplate.ToString()))
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicySaveAcordRequest.RequestOperationError", base.ClientId));
                }
                else
                    oAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
            }

            return oAcordRequest;

        }

        public string GetUnitListAcordRequest(Object oRequest, ref BusinessAdaptorErrors p_objErrOut, string sMode)
        {
            string oAcordRequest = string.Empty;
            string sPSMappedCode = string.Empty;
            XElement oElement = null;
            XElement oSubElement = null;
            IEnumerable<XElement> oElements = null;
            //PolicySystemInterface oPolicySystemInterface = null;
            XElement oTemplate = GetUnitListAcordTemplate();
            PolicyEnquiry oPolicyEnquiry = null;
            PolicySaveRequest oPolicySave = null;
            //bool bResult = false;
            try
            {
                if (string.Equals(sMode, "ENQUIRY", StringComparison.InvariantCultureIgnoreCase))
                {
                    oPolicyEnquiry = (PolicyEnquiry)oRequest;
                    PopulateCommonAcordRequestParameters(ref oTemplate, oPolicyEnquiry.PolicySystemId, ref p_objErrOut);
                    if (!string.IsNullOrEmpty(oPolicyEnquiry.PolicyIdentfier) && (oPolicyEnquiry.PolicySystemId > 0))
                    {
                        oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.Token;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/PolicyNumber");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.PolicyNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/CompanyProductCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolicySymbol))
                        {
                            oElement.Value = oPolicyEnquiry.PolicySymbol;
                            oElement = null;
                        }

                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/LOBCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.LOB))
                        {
                            //oPolicySystemInterface = new PolicySystemInterface(connectionString);
                            //sPSMappedCode = oPolicySystemInterface.GetPSMappedCodesFromRMXCodeId(oEnquireRequest.LOB, "POLICY_CLAIM_LOB");
                            //if (string.IsNullOrEmpty(sPSMappedCode))
                            //{
                            //    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetUnitListAcordRequest.NoMappedCodeFoundError"));
                            //}
                            //oElement.Value = (new LocalCache(connectionString)).GetShortCode(Conversion.CastToType<int>(sPSMappedCode, out bResult));
                            oElement.Value = oPolicyEnquiry.LOB;
                            oElement = null;
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/com.csc_BaseLOBLine");
                            if (oElement != null)
                            {

                                string sParentLobCode = GetPolicyLOBRelatedShortCode(oPolicyEnquiry.LOB, "POLICY_CLAIM_LOB", ref p_objErrOut);

                                if (oElement != null && !string.IsNullOrEmpty(sParentLobCode))
                                {
                                    oElement.Value = sParentLobCode;
                                    oElement = null;
                                }
                            }
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/com.csc_IssueCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.IssueCode))
                        {
                            oElement.Value = oPolicyEnquiry.IssueCode;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitListRq/Location/Addr/StateProvCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.State))
                        {
                            oElement.Value = oPolicyEnquiry.State;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/LossDt");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolLossDt))
                        {
                            oElement.Value = oPolicyEnquiry.PolLossDt;
                            oElement = null;
                        }
                        oElements = oTemplate.XPathSelectElements("./ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier");
                        if (oElements != null)
                        {
                            foreach (XElement oEle in oElements)
                            {
                                oElement = oEle.XPathSelectElement("./OtherIdTypeCd");
                                oSubElement = oEle.XPathSelectElement("./OtherId");
                                switch (oElement.Value)
                                {
                                    case "com.csc_LocCompany":
                                        if (!string.IsNullOrEmpty(oPolicyEnquiry.Location))
                                            oSubElement.Value = oPolicyEnquiry.Location;
                                        break;
                                    case "com.csc_MasterCompany":
                                        if (!string.IsNullOrEmpty(oPolicyEnquiry.MasterCompany))
                                            oSubElement.Value = oPolicyEnquiry.MasterCompany;
                                        break;
                                    case "com.csc_Module":
                                        if (!string.IsNullOrEmpty(oPolicyEnquiry.Module))
                                            oSubElement.Value = oPolicyEnquiry.Module;
                                        break;
                                }
                            }
                            oElements = null;
                            oElement = null;
                            oSubElement = null;

                        }
                    }
                }
                else if (string.Equals(sMode, "SAVEPOLICY", StringComparison.InvariantCultureIgnoreCase))
                {
                    oPolicySave = (PolicySaveRequest)oRequest;
                    PopulateCommonAcordRequestParameters(ref oTemplate, oPolicySave.PolicySystemId, ref p_objErrOut);
                    if (!string.IsNullOrEmpty(oPolicySave.PolicyIdentfier) && (oPolicySave.PolicySystemId > 0))
                    {
                        oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicySave.Token;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/PolicyNumber");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicySave.PolicyNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/CompanyProductCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicySave.PolicySymbol))
                        {
                            oElement.Value = oPolicySave.PolicySymbol;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/LOBCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicySave.LOB))
                        {
                            //oPolicySystemInterface = new PolicySystemInterface(connectionString);
                            //sPSMappedCode = oPolicySystemInterface.GetPSMappedCodesFromRMXCodeId(oPolicySave.LOB, "POLICY_CLAIM_LOB");
                            //if (string.IsNullOrEmpty(sPSMappedCode))
                            //{
                            //    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetUnitListAcordRequest.NoMappedCodeFoundError"));
                            //}
                            //oElement.Value = (new LocalCache(connectionString)).GetShortCode(Conversion.CastToType<int>(sPSMappedCode, out bResult));
                            oElement.Value = oPolicySave.LOB;
                            oElement = null;
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/com.csc_BaseLOBLine");

                            string sParentLobCode = GetPolicyLOBRelatedShortCode(oPolicySave.LOB, "POLICY_CLAIM_LOB", ref p_objErrOut);

                            if (oElement != null && !string.IsNullOrEmpty(sParentLobCode))
                            {
                                oElement.Value = sParentLobCode;
                                oElement = null;
                            }
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/com.csc_IssueCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicySave.IssueCode))
                        {
                            oElement.Value = oPolicySave.IssueCode;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/LossDt");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicySave.PolLossDt))
                        {
                            oElement.Value = oPolicySave.PolLossDt;
                            oElement = null;
                        }

                        //oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitListRq/Location/Addr/StateProvCd");
                        //if (oElement != null && !string.IsNullOrEmpty(oPolicySave.State))
                        //{
                        //    oElement.Value = oPolicySave.State;
                        //    oElement = null;
                        //}
                        oElements = oTemplate.XPathSelectElements("./ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier");
                        if (oElements != null)
                        {
                            foreach (XElement oEle in oElements)
                            {
                                oElement = oEle.XPathSelectElement("./OtherIdTypeCd");
                                oSubElement = oEle.XPathSelectElement("./OtherId");
                                switch (oElement.Value)
                                {
                                    case "com.csc_LocCompany":
                                        if (!string.IsNullOrEmpty(oPolicySave.Location))
                                            oSubElement.Value = oPolicySave.Location;
                                        break;
                                    case "com.csc_MasterCompany":
                                        if (!string.IsNullOrEmpty(oPolicySave.MasterCompany))
                                            oSubElement.Value = oPolicySave.MasterCompany;
                                        break;
                                    case "com.csc_Module":
                                        if (!string.IsNullOrEmpty(oPolicySave.Module))
                                            oSubElement.Value = oPolicySave.Module;
                                        break;
                                }
                            }
                            oElements = null;
                            oElement = null;
                            oSubElement = null;
                        }
                    }
                }

                if (string.IsNullOrEmpty(oTemplate.ToString()))
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetUnitListAcordRequest.RequestOperationError", base.ClientId));
                }
                else
                    oAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
                if (oElements != null)
                    oElements = null;
                if (oElement != null)
                    oElement = null;
                if (oSubElement != null)
                    oSubElement = null;
            }

            return oAcordRequest;
        }

        public string GetCPPUnitInsuranceLineAcordRequest(Object oRequest, ref BusinessAdaptorErrors p_objErrOut, string sMode)
        {
            string oAcordRequest = string.Empty;
            XElement oElement = null;
            XElement oTemplate = GetCPPUnitInsuranceLineAcordTemplate();
            PolicyEnquiry oPolicyEnquiry = null;
            XElement objPolicySaveAccord = null;
            XElement oPolicySaveNode = null;
            PolicySystemInterface objManager = null;
            SaveDownloadOptions oSaveOptions = null;
            string sTemp = string.Empty;
            try
            {
                if (sMode.ToUpper() == "POLICYENQUIRY")
                {
                    oPolicyEnquiry = (PolicyEnquiry)oRequest;
                    PopulateCommonAcordRequestParameters(ref oTemplate, oPolicyEnquiry.PolicySystemId, ref p_objErrOut);
                    if (!string.IsNullOrEmpty(oPolicyEnquiry.PolicyIdentfier) && (oPolicyEnquiry.PolicySystemId > 0))
                    {
                        oElement = oTemplate.XPathSelectElement("./SignonRq/ClientApp/Version");

                        if (oElement != null)
                        {
                            if (oElement.Value.Contains("PIJ"))
                            {
                                oElement = null;
                                {
                                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/RqUID");

                                    if (oElement != null)
                                    {
                                        sTemp = ReplaceTemplateforPIJ(oElement.Value);
                                        oElement.Value = sTemp;
                                        oElement = null;
                                    }

                                }



                            }
                        }
                        oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.Token;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_CPPUnitInsLineRq/Policy/PolicyNumber");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.PolicyNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_CPPUnitInsLineRq/Policy/CompanyProductCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolicySymbol))
                        {
                            oElement.Value = oPolicyEnquiry.PolicySymbol;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_CPPUnitInsLineRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.UnitNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_CPPUnitInsLineRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.UnitRiskLocation;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_CPPUnitInsLineRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.UnitSubRiskLocation;
                            oElement = null;
                        }
                    }
                }
                else if (sMode.ToUpper() == "SAVEUNIT")
                {
                    oSaveOptions = (SaveDownloadOptions)oRequest;
                    PopulateCommonAcordRequestParameters(ref oTemplate, oSaveOptions.PolicySystemId, ref p_objErrOut);
                    oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                    if (oElement != null)
                    {
                        oElement.Value = oSaveOptions.Token;
                        oElement = null;
                    }
                    objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                    objPolicySaveAccord = XElement.Parse(objManager.GetDownLoadXMLData("POLICY", oSaveOptions.PolicyId, oSaveOptions.PolicyId));

                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_CPPUnitInsLineRq/Policy/PolicyNumber");
                    if (oElement != null)
                    {
                        oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber");
                        if (oPolicySaveNode != null)
                        {
                            oElement.Value = oPolicySaveNode.Value;
                            oElement = null;
                            oPolicySaveNode = null;
                        }
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_CPPUnitInsLineRq/Policy/CompanyProductCd");
                    if (oElement != null)
                    {
                        oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/CompanyProductCd");
                        if (oPolicySaveNode != null)
                        {
                            oElement.Value = oPolicySaveNode.Value;
                            oElement = null;
                            oPolicySaveNode = null;
                        }
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_CPPUnitInsLineRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oSaveOptions.UnitNumber;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_CPPUnitInsLineRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oSaveOptions.RiskLocation;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_CPPUnitInsLineRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oSaveOptions.RiskSubLocation;
                        oElement = null;
                    }

                }
                if (string.IsNullOrEmpty(oTemplate.ToString()))
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetCPPUnitInsuranceLineAcordRequest.RequestOperationError", base.ClientId));
                }
                else
                    oAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
            }

            return oAcordRequest;
        }

        public string GetUnitDetailAcordRequest(PolicyEnquiry oRequest, ref BusinessAdaptorErrors p_objErrOut)
        {
            string oAcordRequest = string.Empty;
            XElement oElement = null;
            XElement oTemplate = GetUnitDetailAcordTemplate();
            PopulateCommonAcordRequestParameters(ref oTemplate, oRequest.PolicySystemId, ref p_objErrOut);
            string sParentShortCode = string.Empty;
            LocalCache objCache = null;
            string sStatesTableNm = string.Empty;
            PolicySystemInterface objManager;
            DataTable dtPolData = new DataTable();
            try
            {
                if (!string.IsNullOrEmpty(oRequest.PolicyIdentfier) && (oRequest.PolicySystemId > 0))
                {
                    objManager = new PolicySystemInterface(m_connectionString, base.ClientId);
                    oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                    if (oElement != null)
                    {
                        oElement.Value = oRequest.Token;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/PolicyNumber");
                    if (oElement != null)
                    {
                        oElement.Value = oRequest.PolicyNumber;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/CompanyProductCd");
                    if (oElement != null && !string.IsNullOrEmpty(oRequest.PolicySymbol))
                    {
                        oElement.Value = oRequest.PolicySymbol;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/StateProvCd");
                    if (oElement != null && !string.IsNullOrEmpty(oRequest.State))
                    {
                        oElement.Value = oRequest.State;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/LOBCd");
                    if (oElement != null && !string.IsNullOrEmpty(oRequest.LOB))
                    {
                        oElement.Value = oRequest.LOB;
                        oElement = null;
                        //skhare7 Policy interface
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_BaseLOBLine");
                        if (oElement != null)
                        {
                            sParentShortCode = GetPolicyLOBRelatedShortCode(oRequest.LOB, "POLICY_CLAIM_LOB", ref p_objErrOut);

                            if (oElement != null && !string.IsNullOrEmpty(sParentShortCode))
                            {
                                oElement.Value = sParentShortCode;
                                oElement = null;
                            }



                        }
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_IssueCd");
                    if (oElement != null && !string.IsNullOrEmpty(oRequest.IssueCode))
                    {
                        oElement.Value = oRequest.IssueCode;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oRequest.Location;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oRequest.MasterCompany;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oRequest.Module;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oRequest.InsLine;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oRequest.UnitNumber;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oRequest.Product;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oRequest.UnitRiskLocation;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oRequest.UnitSubRiskLocation;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId");
                    if (oElement != null)
                    {
                        // 
                        //skhare7 Poilicy Interface
                        //  sParentShortCode = GetPolicyLOBRelatedShortCode(oRequest.LOB.ToUpper(), "POLICY_CLAIM_LOB" ,ref p_objErrOut);
                        //if (sParentShortCode == string.Empty)
                        //{
                        //    throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetPolicySystemList.NoBaseLobLine"));

                        //}
                        if (sParentShortCode == "WL")
                        {
                            objCache = new LocalCache(m_connectionString, base.ClientId);
                            dtPolData = objManager.GetPolicySystemInfoById(oRequest.PolicySystemId);
                            sStatesTableNm = dtPolData.Rows[0]["TABLE_PREFIX"] + "_" + "STATES";

                            //int iCodeID = objCache.GetCodeId(oRequest.UnitState, "POINT_STATES");
                            int iCodeID = objCache.GetCodeId(oRequest.UnitState, sStatesTableNm);
                            string sCodeDesc = objCache.GetCodeDesc(iCodeID);
                            oElement.Value = sCodeDesc;
                            //oElement.Value = "";
                        }
                        else
                        {
                            oElement.Value = oRequest.UnitState;
                        }
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolCompany']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oRequest.PolCompany;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_UnitStatus");
                    if (oElement != null)
                    {
                        oElement.Value = oRequest.UnitStatus;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ContractTerm/EffectiveDt");
                    if (oElement != null)
                    {
                        oElement.Value = oRequest.EffDate;
                        oElement = null;
                    }
                    //aaggarwal29 : changes for Mits 33345 start 
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitTypeInd']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oRequest.UnitTypeInd;
                        oElement = null;
                    }
                    //aaggarwal29 end: 
                }

                if (string.IsNullOrEmpty(oTemplate.ToString()))
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetUnitDetailAcordRequest.RequestOperationError", base.ClientId));
                }
                else
                    oAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
            }

            return oAcordRequest;
        }

        public string ReadFileContent(string sMode)
        {
            string sFilePath = string.Empty;
            string sFileContent = string.Empty;
            string sFileName = string.Empty;
            switch (sMode.ToUpper())
            {
                case "ENTITYLIST": sFileName = "Entity_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml"; break;
                case "UNITLIST": sFileName = "Unit_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml"; break;
                case "DRIVERLIST": sFileName = "Driver_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml"; break;
                case "UNITINTERESTLIST": sFileName = "UnitInterestList_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml"; break;//skhare7 Point Policy interface Unitinterest
                case "COVERAGELIST": sFileName = "CoverageList_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml"; break;

            }
            if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + sFileName))
            {
                sFilePath = RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + sFileName;
                sFileContent = File.ReadAllText(sFilePath);

            }

            return sFileContent;
        }

        public string GetUnitDetailSaveAcordRequest(string sSelectedValue, string sFileContent, ref SaveDownloadOptions oSaveDownloadOptions, ref BusinessAdaptorErrors p_objErrOut, ref string sUnitType, ref string sDesc)
        {
            string oAcordRequest = string.Empty;
            XElement oElement = null;
            XElement oTemplate = GetUnitDetailAcordTemplate();
            PopulateCommonAcordRequestParameters(ref oTemplate, oSaveDownloadOptions.PolicySystemId, ref p_objErrOut);
            XmlDocument objDocument = null;
            XmlNodeList objNodeList = null;
            XmlDocument objDoc = null;
            PolicySystemInterface objManager = null;
            XElement objPolicySaveAccord = null;
            XElement oPolicySaveNode = null;
            IEnumerable<XElement> objElements = null;
            //string sParentShortCode = string.Empty;
            string sParentLobCode = string.Empty;
            string sStatesTableNm = string.Empty;
            DataTable dtPolicyData = new DataTable();

            try
            {
                objDocument = new XmlDocument();
                objDocument.LoadXml(sFileContent);

                if (objDocument != null)
                {
                    objNodeList = objDocument.SelectNodes("//SequenceNumber");
                    foreach (XmlNode objNode in objNodeList)
                    {
                        if (string.Equals(objNode.InnerText, sSelectedValue.Split('|')[0], StringComparison.InvariantCultureIgnoreCase))
                        {
                            objDoc = new XmlDocument();
                            objDoc.LoadXml(objNode.ParentNode.ParentNode.OuterXml);

                            objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                            objPolicySaveAccord = XElement.Parse(objManager.GetDownLoadXMLData("POLICY", oSaveDownloadOptions.PolicyId, oSaveDownloadOptions.PolicyId));
                            //if (!string.IsNullOrEmpty(oSaveDownloadOptions.PolicyIdentfier) && (oRequest.PolicySystemId > 0))
                            //{
                            if (objDoc.SelectSingleNode("//UnitType") != null)
                            {
                                sUnitType = objDoc.SelectSingleNode("//UnitType").InnerText;
                            }
                            if (objDoc.SelectSingleNode("//Desc") != null)
                                sDesc = objDoc.SelectSingleNode("//Desc").InnerText;

                            oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                            if (oElement != null)
                            {
                                oElement.Value = oSaveDownloadOptions.Token;
                                oElement = null;
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/PolicyNumber");
                            if (oElement != null)
                            {
                                oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber");
                                if (oPolicySaveNode != null)
                                {
                                    oElement.Value = oPolicySaveNode.Value;
                                    oElement = null;
                                    oPolicySaveNode = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/CompanyProductCd");
                            if (oElement != null)
                            {

                                oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/CompanyProductCd");
                                if (oPolicySaveNode != null)
                                {
                                    oElement.Value = oPolicySaveNode.Value;
                                    oElement = null;
                                    oPolicySaveNode = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/StateProvCd");
                            if (oElement != null)
                            {
                                oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/StateProvCd");
                                //oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProvCd");
                                if (oPolicySaveNode != null)
                                {
                                    oElement.Value = oPolicySaveNode.Value;
                                    oElement = null;
                                    oPolicySaveNode = null;
                                }

                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/LOBCd");
                            if (oElement != null)
                            {
                                oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/LOBCd");
                                if (oPolicySaveNode != null)
                                {
                                    oElement.Value = oPolicySaveNode.Value;
                                    oElement = null;
                                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_BaseLOBLine");

                                    if (oElement != null)
                                    {
                                        sParentLobCode = GetPolicyLOBRelatedShortCode(oPolicySaveNode.Value, "POLICY_CLAIM_LOB", ref p_objErrOut);
                                        oElement.Value = sParentLobCode;
                                        oElement = null;

                                    }
                                    oPolicySaveNode = null;

                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_IssueCd");
                            if (oElement != null)
                            {
                                oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_IssueCd");
                                if (oPolicySaveNode != null)
                                {
                                    oElement.Value = oPolicySaveNode.Value;
                                    oElement = null;
                                    oPolicySaveNode = null;
                                }
                            }

                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                            if (oElement != null)
                            {
                                objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                                foreach (XElement objElement in objElements)
                                {
                                    XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_LocCompany", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oElement.Value = oSubElement.Value;
                                    }

                                    objEle = null;
                                    oSubElement = null;
                                }
                                objElements = null;
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId");
                            if (oElement != null)
                            {

                                objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                                foreach (XElement objElement in objElements)
                                {
                                    XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_MasterCompany", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oElement.Value = oSubElement.Value;
                                    }

                                    objEle = null;
                                    oSubElement = null;
                                }
                                objElements = null;
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId");
                            if (oElement != null)
                            {
                                objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                                foreach (XElement objElement in objElements)
                                {
                                    XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_Module", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oElement.Value = oSubElement.Value;
                                    }

                                    objEle = null;
                                    oSubElement = null;
                                }
                                objElements = null;
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                            if (oElement != null)
                            {
                                if ((objDoc.SelectSingleNode("//InsLineCd") != null) && (!string.IsNullOrEmpty(objDoc.SelectSingleNode("//InsLineCd").InnerText)))
                                {
                                    oElement.Value = objDoc.SelectSingleNode("//InsLineCd").InnerText;
                                    oElement = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId");
                            if (oElement != null)
                            {
                                if (objDoc.SelectSingleNode("//UnitNo") != null)
                                {
                                    oElement.Value = objDoc.SelectSingleNode("//UnitNo").InnerText;
                                    oSaveDownloadOptions.UnitNumber = objDoc.SelectSingleNode("//UnitNo").InnerText;
                                    oElement = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_StatUnitNumber']/OtherId");
                            if (oElement != null)
                            {
                                if (objDoc.SelectSingleNode("//StatUnitNo") != null)
                                {
                                    oElement.Value = objDoc.SelectSingleNode("//StatUnitNo").InnerText;
                                    oElement = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_UnitStatus");
                            if (oElement != null)
                            {
                                if (objDoc.SelectSingleNode("//Status") != null)
                                {
                                    oElement.Value = objDoc.SelectSingleNode("//Status").InnerText;
                                    oElement = null;
                                }
                            }

                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId");
                            if (oElement != null)
                            {
                                if (objDoc.SelectSingleNode("//Product") != null)
                                {
                                    oElement.Value = objDoc.SelectSingleNode("//Product").InnerText;
                                    oSaveDownloadOptions.Product = objDoc.SelectSingleNode("//Product").InnerText;
                                    oElement = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId");
                            if (oElement != null)
                            {
                                if (objDoc.SelectSingleNode("//RiskLoc") != null)
                                {
                                    oElement.Value = objDoc.SelectSingleNode("//RiskLoc").InnerText;
                                    oSaveDownloadOptions.RiskLocation = objDoc.SelectSingleNode("//RiskLoc").InnerText;
                                    oElement = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId");
                            if (oElement != null)
                            {
                                if (objDoc.SelectSingleNode("//RiskSubLoc") != null)
                                {
                                    oElement.Value = objDoc.SelectSingleNode("//RiskSubLoc").InnerText;
                                    oSaveDownloadOptions.RiskSubLocation = objDoc.SelectSingleNode("//RiskSubLoc").InnerText;
                                    oElement = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId");
                            if (oElement != null)
                            {
                                if (objDoc.SelectSingleNode("//State") != null)
                                {
                                    if (sParentLobCode == "WL") //if (sParentShortCode == "WL")
                                    {
                                        LocalCache objCache = new LocalCache(m_connectionString, base.ClientId);

                                        dtPolicyData = objManager.GetPolicySystemInfoById(oSaveDownloadOptions.PolicySystemId);
                                        sStatesTableNm = dtPolicyData.Rows[0]["TABLE_PREFIX"].ToString() + "_" + "STATES";

                                        int iCodeID = objCache.GetCodeId(objDoc.SelectSingleNode("//State").InnerText, sStatesTableNm);
                                        string sCodeDesc = objCache.GetCodeDesc(iCodeID);
                                        oElement.Value = sCodeDesc;
                                    }
                                    else
                                    {
                                        oElement.Value = objDoc.SelectSingleNode("//State").InnerText;
                                    }

                                    //oSaveDownloadOptions.UnitState = objDoc.SelectSingleNode("//State").InnerText;
                                    oSaveDownloadOptions.UnitState = oElement.Value;
                                    oElement = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolCompany']/OtherId");
                            if (oElement != null)
                            {
                                objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                                foreach (XElement objElement in objElements)
                                {
                                    XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_PolCompany", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oElement.Value = oSubElement.Value;
                                    }

                                    objEle = null;
                                    oSubElement = null;
                                }
                                objElements = null;
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ContractTerm/EffectiveDt");
                            if (oElement != null)
                            {
                                oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ContractTerm/EffectiveDt");
                                if (oPolicySaveNode != null)
                                {
                                    oElement.Value = oPolicySaveNode.Value;
                                    oElement = null;
                                    oPolicySaveNode = null;
                                }
                            }
                            //aaggarwal29 : changes for Mits 33345 start 
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitTypeInd']/OtherId");
                            if (oElement != null)
                            {
                                switch (sUnitType.ToLower())
                                {
                                    case "stat": oElement.Value = "SU";
                                        break;
                                    case "property": oElement.Value = "P";
                                        break;
                                    case "vehicle": oElement.Value = "V";
                                        break;
                                    case "site": oElement.Value = "S";
                                        break;
                                }

                                oElement = null;
                            }
                            //aaggarwal29 end
                        }
                    }

                }
                oAcordRequest = oTemplate.ToString();

            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
            }

            return oAcordRequest;
        }

        public string GetPolicyInterestDetailSaveAcordRequest(string sSelectedValue, string sFileContent, SaveDownloadOptions oSaveDownloadOptions, ref string sUnitRowId, ref BusinessAdaptorErrors p_objErrOut)
        {
            string oAcordRequest = string.Empty;
            XElement oElement = null;
            XElement oTemplate = GetPolicyInterestDetailAcordTemplate();
            PopulateCommonAcordRequestParameters(ref oTemplate, oSaveDownloadOptions.PolicySystemId, ref p_objErrOut);
            IEnumerable<XElement> oElements = null;
            XmlDocument objDocument = null;
            XmlNodeList objNodeList = null;
            XmlDocument objDoc = null;
            PolicySystemInterface objManager = null;
            XElement objPolicySaveAccord = null;
            XElement oPolicySaveNode = null;
            IEnumerable<XElement> objElements = null;
            string sParentShortCode = string.Empty;
            try
            {
                objDocument = new XmlDocument();
                objDocument.LoadXml(sFileContent);

                if (objDocument != null)
                {
                    objNodeList = objDocument.SelectNodes("//SequenceNumber");
                    foreach (XmlNode objNode in objNodeList)
                    {
                        if (string.Equals(objNode.InnerText, sSelectedValue.Split('|')[0], StringComparison.InvariantCultureIgnoreCase))
                        {
                            objDoc = new XmlDocument();
                            objDoc.LoadXml(objNode.ParentNode.ParentNode.OuterXml);

                            objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                            objPolicySaveAccord = XElement.Parse(objManager.GetDownLoadXMLData("POLICY", oSaveDownloadOptions.PolicyId, oSaveDownloadOptions.PolicyId));
                            //if (!string.IsNullOrEmpty(oEnquiry.PolicyIdentfier) && (oEnquiry.PolicySystemId > 0))
                            //{
                            oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                            if (oElement != null)
                            {
                                oElement.Value = oSaveDownloadOptions.Token;
                                oElement = null;
                            }

                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/PolicyNumber");
                            if (oElement != null)
                            {
                                oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber");
                                if (oPolicySaveNode != null)
                                {
                                    oElement.Value = oPolicySaveNode.Value;
                                    oElement = null;
                                    oPolicySaveNode = null;
                                }
                            }
                            oElement = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/LOBCd");
                            if (oElement != null)
                            {

                                sParentShortCode = GetPolicyLOBRelatedShortCode(oElement.Value, "POLICY_CLAIM_LOB", ref p_objErrOut);
                                oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_BaseLOBLine");
                                if (oElement != null)
                                {

                                    if (sParentShortCode == string.Empty)
                                    {
                                        throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetCoverageListAcordRequest.NoBaseLobLine", base.ClientId));

                                    }
                                    else
                                    {
                                        oElement.Value = sParentShortCode;
                                        oElement = null;

                                    }
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/CompanyProductCd");
                            if (oElement != null)
                            {

                                oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/CompanyProductCd");
                                if (oPolicySaveNode != null)
                                {
                                    oElement.Value = oPolicySaveNode.Value;
                                    oElement = null;
                                    oPolicySaveNode = null;
                                }

                            }
                            oElements = oTemplate.XPathSelectElements("./ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier");
                            if (oElements != null)
                            {
                                foreach (XElement objElement in oElements)
                                {
                                    XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElementNode = objElement.XPathSelectElement("./OtherId");
                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_MasterCompany", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                                        foreach (XElement objElem in objElements)
                                        {
                                            XElement objEle1 = objElem.XPathSelectElement("./OtherIdTypeCd");
                                            XElement oSubElement1 = objElem.XPathSelectElement("./OtherId");

                                            if (objEle1 != null && string.Equals(objEle1.Value, "com.csc_MasterCompany", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                oSubElementNode.Value = oSubElement1.Value;
                                            }

                                            objEle1 = null;
                                            oSubElement1 = null;
                                        }


                                        // oSubElementNode.Value = oEnquiry.MasterCompany;
                                    }
                                    else if (objEle != null && string.Equals(objEle.Value, "com.csc_LocCompany", StringComparison.InvariantCultureIgnoreCase))
                                    {

                                        oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo//OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                                        if (oPolicySaveNode != null)
                                        {
                                            oSubElementNode.Value = oPolicySaveNode.Value;

                                        }


                                        //       oSubElementNode.Value = oEnquiry.Location;
                                    }

                                    else if (objEle != null && string.Equals(objEle.Value, "com.csc_Location", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        if (objDoc.SelectSingleNode("//Location") != null)
                                        {
                                            oSubElementNode.Value = objDoc.SelectSingleNode("//Location").InnerText;
                                        }



                                        //       oSubElementNode.Value = oEnquiry.Location;
                                    }
                                    else if (objEle != null && string.Equals(objEle.Value, "com.csc_SeqNum", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        if (objDoc.SelectSingleNode("//PointSeqNumber") != null)
                                        {
                                            oSubElementNode.Value = objDoc.SelectSingleNode("//PointSeqNumber").InnerText;
                                        }



                                        //       oSubElementNode.Value = oEnquiry.Location;
                                    }
                                    else if (objEle != null && string.Equals(objEle.Value, "com.csc_Module", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                                        foreach (XElement objElem in objElements)
                                        {
                                            XElement objEle1 = objElem.XPathSelectElement("./OtherIdTypeCd");
                                            XElement oSubElement1 = objElem.XPathSelectElement("./OtherId");

                                            if (objEle1 != null && string.Equals(objEle1.Value, "com.csc_Module", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                oSubElementNode.Value = oSubElement1.Value;
                                            }

                                            objEle1 = null;
                                            oSubElement1 = null;
                                        }
                                    }
                                    else if (objEle != null && string.Equals(objEle.Value, "com.csc_RoleCd", StringComparison.InvariantCultureIgnoreCase))
                                    {

                                        if (objDoc.SelectSingleNode("//RoleCd") != null)
                                        {
                                            oSubElementNode.Value = objDoc.SelectSingleNode("//RoleCd").InnerText;
                                        }

                                    }
                                    else if (objEle != null && string.Equals(objEle.Value, "com.csc_UnitNumber", StringComparison.InvariantCultureIgnoreCase))
                                    {

                                        if (objDoc.SelectSingleNode("//UnitNumber") != null)
                                        {
                                            oSubElementNode.Value = objDoc.SelectSingleNode("//UnitNumber").InnerText;
                                        }

                                    }
                                    //dbisht6 mits 35925
                                    else if (objEle != null && string.Equals(objEle.Value, "com.csc_ClientSeqNum", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        if (objDoc.SelectSingleNode("//ClientSequenceNumber") != null)
                                        {
                                            oSubElementNode.Value = objDoc.SelectSingleNode("//ClientSequenceNumber").InnerText;
                                        }
                                    }

                                    else if (objEle != null && string.Equals(objEle.Value, "com.csc_AddressSeqNum", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        if (objDoc.SelectSingleNode("//AddressSequenceNumber") != null)
                                        {
                                            oSubElementNode.Value = objDoc.SelectSingleNode("//AddressSequenceNumber").InnerText;
                                        }
                                    }


                                    //dbisht6 end
                                    if (objDoc.SelectSingleNode("//PolicyUnitRowid") != null)
                                    {
                                        sUnitRowId = objDoc.SelectSingleNode("//PolicyUnitRowid").InnerText;
                                    }
                                    objEle = null;
                                    oSubElementNode = null;
                                }
                            }
                            oElements = null;
                        }

                    }
                }
                oAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
            }

            return oAcordRequest;
        }

        public string GetCoverageListAcordRequest(Object oRequest, ref BusinessAdaptorErrors p_objErrOut, string sMode, string sRequestXML)
        {
            string oAcordRequest = string.Empty;
            XElement oElement = null;
            XElement oTemplate = GetCoverageListAcordTemplate();
            PolicyEnquiry objPolicyEnquiry = null;
            SaveDownloadOptions oSaveOptions = null;
            XElement objPolicySaveAccord = null;
            PolicySystemInterface objManager = null;
            XElement oPolicySaveNode = null;
            IEnumerable<XElement> objElements = null;
            string sParentShortCode = string.Empty;
            try
            {
                if (sMode.ToUpper() == "POLICYENQUIRY")
                {
                    objPolicyEnquiry = (PolicyEnquiry)oRequest;
                    PopulateCommonAcordRequestParameters(ref oTemplate, objPolicyEnquiry.PolicySystemId, ref p_objErrOut);
                    if (!string.IsNullOrEmpty(objPolicyEnquiry.PolicyIdentfier) && (objPolicyEnquiry.PolicySystemId > 0))
                    {
                        oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicyEnquiry.Token;
                            oElement = null;
                        }

                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/LossDt");
                        if (oElement != null && !string.IsNullOrEmpty(objPolicyEnquiry.PolLossDt))
                        {
                            oElement.Value = objPolicyEnquiry.PolLossDt;
                            oElement = null;
                        }
                        //Ashish Ahuja Claims Made Jira 1342
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/ReportedDt");
                        if (oElement != null && !string.IsNullOrEmpty(objPolicyEnquiry.ClaimDateReported))
                        {
                            oElement.Value = objPolicyEnquiry.ClaimDateReported;
                            oElement = null;
                        }

                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/PolicyNumber");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicyEnquiry.PolicyNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/CompanyProductCd");
                        if (oElement != null && !string.IsNullOrEmpty(objPolicyEnquiry.PolicySymbol))
                        {
                            oElement.Value = objPolicyEnquiry.PolicySymbol;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/LOBCd");
                        if (oElement != null && !string.IsNullOrEmpty(objPolicyEnquiry.LOB))
                        {
                            oElement.Value = objPolicyEnquiry.LOB;
                            oElement = null;
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_BaseLOBLine");
                            if (oElement != null)
                            {
                                sParentShortCode = GetPolicyLOBRelatedShortCode(objPolicyEnquiry.LOB, "POLICY_CLAIM_LOB", ref p_objErrOut);
                                if (sParentShortCode == string.Empty)
                                {
                                    throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetCoverageListAcordRequest.NoBaseLobLine", base.ClientId));

                                }
                                else
                                {

                                    oElement.Value = sParentShortCode;
                                }
                            }
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_IssueCd");
                        if (oElement != null && !string.IsNullOrEmpty(objPolicyEnquiry.IssueCode))
                        {
                            oElement.Value = objPolicyEnquiry.IssueCode;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicyEnquiry.Location;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicyEnquiry.MasterCompany;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicyEnquiry.Module;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicyEnquiry.InsLine;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicyEnquiry.UnitNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicyEnquiry.Product;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicyEnquiry.UnitRiskLocation;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicyEnquiry.UnitSubRiskLocation;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = objPolicyEnquiry.UnitState;
                            oElement = null;
                        }
                    }
                }
                else if (sMode.ToUpper() == "SAVEUNIT")
                {
                    oSaveOptions = (SaveDownloadOptions)oRequest;
                    PopulateCommonAcordRequestParameters(ref oTemplate, oSaveOptions.PolicySystemId, ref p_objErrOut);
                    XElement objDoc = XElement.Parse(sRequestXML);
                    string sLOB = string.Empty;
                    oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                    if (oElement != null)
                    {
                        oElement.Value = oSaveOptions.Token;
                        oElement = null;
                    }
                    objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                    objPolicySaveAccord = XElement.Parse(objManager.GetDownLoadXMLData("POLICY", oSaveOptions.PolicyId, oSaveOptions.PolicyId));


                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/PolicyNumber");
                    if (oElement != null)
                    {
                        oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber");
                        if (oPolicySaveNode != null)
                        {
                            oElement.Value = oPolicySaveNode.Value;
                            oElement = null;
                            oPolicySaveNode = null;
                        }
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/CompanyProductCd");
                    if (oElement != null)
                    {
                        oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/CompanyProductCd");
                        if (oPolicySaveNode != null)
                        {
                            oElement.Value = oPolicySaveNode.Value;
                            oElement = null;
                            oPolicySaveNode = null;
                        }
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/LOBCd");
                    if (oElement != null)
                    {
                        oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/LOBCd");
                        if (oPolicySaveNode != null)
                        {
                            oElement.Value = oPolicySaveNode.Value;
                            sLOB = oPolicySaveNode.Value;
                            oElement = null;
                            sParentShortCode = GetPolicyLOBRelatedShortCode(sLOB, "POLICY_CLAIM_LOB", ref p_objErrOut);
                            if (sParentShortCode == string.Empty)
                            {
                                throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetCoverageListAcordRequest.NoBaseLobLine", base.ClientId));

                            }
                            else
                            {
                                oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_BaseLOBLine");
                                if (oElement != null)
                                    oElement.Value = sParentShortCode;
                            }
                            oPolicySaveNode = null;
                            oElement = null;
                        }
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_IssueCd");
                    if (oElement != null)
                    {
                        oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_IssueCd");
                        if (oPolicySaveNode != null)
                        {
                            oElement.Value = oPolicySaveNode.Value;
                            oElement = null;
                            oPolicySaveNode = null;
                        }
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                    if (oElement != null)
                    {
                        objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                        foreach (XElement objElement in objElements)
                        {
                            XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                            XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                            if (objEle != null && string.Equals(objEle.Value, "com.csc_LocCompany", StringComparison.InvariantCultureIgnoreCase))
                            {
                                oElement.Value = oSubElement.Value;
                            }

                            objEle = null;
                            oSubElement = null;
                        }
                        objElements = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId");
                    if (oElement != null)
                    {
                        objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                        foreach (XElement objElement in objElements)
                        {
                            XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                            XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                            if (objEle != null && string.Equals(objEle.Value, "com.csc_MasterCompany", StringComparison.InvariantCultureIgnoreCase))
                            {
                                oElement.Value = oSubElement.Value;
                            }

                            objEle = null;
                            oSubElement = null;
                        }
                        objElements = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId");
                    if (oElement != null)
                    {
                        objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                        foreach (XElement objElement in objElements)
                        {
                            XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                            XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                            if (objEle != null && string.Equals(objEle.Value, "com.csc_Module", StringComparison.InvariantCultureIgnoreCase))
                            {
                                oElement.Value = oSubElement.Value;
                            }

                            objEle = null;
                            oSubElement = null;
                        }
                        objElements = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oSaveOptions.InsuranceLine;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId");
                    if (oElement != null)
                    {
                        //if (objDoc.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId") != null)
                        if (objDoc.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_StatUnitNumber']/OtherId") != null)
                        {
                            //oElement.Value = objDoc.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId").Value;
                            oElement.Value = objDoc.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_StatUnitNumber']/OtherId").Value;
                            oElement = null;
                        }
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId");
                    if (oElement != null)
                    {
                        if (objDoc.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId") != null)
                        {
                            oElement.Value = objDoc.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId").Value;
                            oElement = null;
                        }
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId");
                    if (oElement != null)
                    {
                        if (objDoc.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId") != null)
                        {
                            oElement.Value = objDoc.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId").Value;
                            oElement = null;
                        }
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId");
                    if (oElement != null)
                    {
                        if (objDoc.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId") != null)
                        {
                            oElement.Value = objDoc.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId").Value;
                            oElement = null;
                        }
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/LossDt");
                    if (oElement != null && !string.IsNullOrEmpty(oSaveOptions.LossDate))
                    {
                        oElement.Value = oSaveOptions.LossDate.ToString();
                        oElement = null;
                    }
                    ////Ashish Ahuja: Claims Made Jira 1342
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/ReportedDt");
                    if (oElement != null && !string.IsNullOrEmpty(oSaveOptions.ClaimDateReported))
                    {
                        oElement.Value = oSaveOptions.ClaimDateReported.ToString();
                        oElement = null;
                    }
                    ////Ashish Ahuja: Claims Made Jira 1342
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageListRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId");
                    if (oElement != null)
                    {
                        if (objDoc.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId") != null)
                        {
                            sLOB = sLOB.Trim().ToUpper();
                            if (sParentShortCode == "WL")
                            {
                                oElement.Value = oSaveOptions.UnitState;
                            }
                            else
                            {
                                oElement.Value = objDoc.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RateState']/OtherId").Value;
                            }

                            oElement = null;
                        }
                    }
                }
                if (string.IsNullOrEmpty(oTemplate.ToString()))
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetCoverageListAcordRequest.RequestOperationError", base.ClientId));
                }
                else
                    oAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
            }

            return oAcordRequest;
        }

        //Start: MITS 31601, Neha Suresh Jain, 02/27/2013
        /// <summary>
        /// format request object to post data to policy system
        /// </summary>
        /// <param name="oRequest"></param>
        /// <param name="p_objErrOut"></param>
        /// <param name="sMode"></param>
        /// <returns></returns>
        public string GetPolicyChangeDateListAcordRequest(Object oRequest, ref BusinessAdaptorErrors p_objErrOut, string sMode)
        {
            string oAcordRequest = string.Empty;
            XElement oElement = null;
            XElement oTemplate = GetPolicyChangeDateListAcordTemplate();
            IEnumerable<XElement> oElements = null;
            PolicySaveRequest oPolicySave = null;
            //Anu Tennyson Starts - For PIJ
            string sTemp = string.Empty;
            //Anu Tennyson Ends
            try
            {
                if (string.Equals(sMode, "SAVEPOLICY", StringComparison.InvariantCultureIgnoreCase))
                {
                    oPolicySave = (PolicySaveRequest)oRequest;
                    PopulateCommonAcordRequestParameters(ref oTemplate, oPolicySave.PolicySystemId, ref p_objErrOut);
                    if (!string.IsNullOrEmpty(oPolicySave.PolicyIdentfier) && (oPolicySave.PolicySystemId > 0))
                    {
                        oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicySave.Token))
                        {
                            oElement.Value = oPolicySave.Token;
                            oElement = null;
                        }
                        //Anu Tennyson Starts
                        oElement = oTemplate.XPathSelectElement("./SignonRq/ClientApp/Version");

                        if (oElement != null)
                        {
                            if (oElement.Value.Contains("PIJ"))
                            {
                                //oElement.Value = string.Empty;
                                oElement = null;
                                {
                                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/RqUID");

                                    if (oElement != null)
                                    {
                                        sTemp = ReplaceTemplateforPIJ(oElement.Value);
                                        oElement.Value = sTemp;
                                        oElement = null;
                                    }

                                }

                            }

                        }
                        //Anu Tennyson Ends
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyChangeDateListRq/com.csc_PolicyChangeDateSearch/Policy/PolicyNumber");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicySave.PolicyNumber))
                        {
                            oElement.Value = oPolicySave.PolicyNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyChangeDateListRq/com.csc_PolicyChangeDateSearch/Policy/CompanyProductCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicySave.PolicySymbol))
                        {
                            oElement.Value = oPolicySave.PolicySymbol;
                            oElement = null;
                        }

                        oElements = oTemplate.XPathSelectElements("./ClaimsSvcRq/com.csc_PolicyChangeDateListRq/com.csc_PolicyChangeDateSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier");
                        if (oElements != null)
                        {
                            foreach (XElement objElement in oElements)
                            {
                                XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                XElement oSubElementNode = objElement.XPathSelectElement("./OtherId");
                                if (objEle != null && oSubElementNode != null && string.Equals(objEle.Value, "com.csc_MasterCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicySave.MasterCompany))
                                {
                                    oSubElementNode.Value = oPolicySave.MasterCompany;
                                }
                                else if (objEle != null && oSubElementNode != null && string.Equals(objEle.Value, "com.csc_LocCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicySave.Location))
                                {
                                    oSubElementNode.Value = oPolicySave.Location;
                                }
                                else if (objEle != null && oSubElementNode != null && string.Equals(objEle.Value, "com.csc_Module", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicySave.Module))
                                {
                                    oSubElementNode.Value = oPolicySave.Module;
                                }
                                objEle = null;
                                oSubElementNode = null;
                            }
                        }
                        oElements = null;
                    }
                }


                if (string.IsNullOrEmpty(oTemplate.ToString()))
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicyChangeDateListAcordRequest.RequestOperationError", base.ClientId));
                }
                else
                    oAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
            }

            return oAcordRequest;
        }
        //End: MITS 31601
        public string GetPolicyInterestListAcordRequest(Object oRequest, ref BusinessAdaptorErrors p_objErrOut, string sMode)
        {

            string oAcordRequest = string.Empty;
            XElement oElement = null;
            XElement oTemplate = GetPolicyInterestListAcordTemplate();
            IEnumerable<XElement> oElements = null;
            PolicyEnquiry oPolicyEnquiry = null;
            PolicySaveRequest oPolicySave = null;
            try
            {
                if (string.Equals(sMode, "ENQUIRY", StringComparison.InvariantCultureIgnoreCase))
                {
                    oPolicyEnquiry = (PolicyEnquiry)oRequest;
                    PopulateCommonAcordRequestParameters(ref oTemplate, oPolicyEnquiry.PolicySystemId, ref p_objErrOut);
                    if (!string.IsNullOrEmpty(oPolicyEnquiry.PolicyIdentfier) && (oPolicyEnquiry.PolicySystemId > 0))
                    {
                        oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.Token;
                            oElement = null;
                        }

                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInterestListRq/com.csc_PolicyInterestSearch/Policy/PolicyNumber");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolicyNumber))
                        {
                            oElement.Value = oPolicyEnquiry.PolicyNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInterestListRq/com.csc_PolicyInterestSearch/Policy/CompanyProductCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolicySymbol))
                        {
                            oElement.Value = oPolicyEnquiry.PolicySymbol;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInterestListRq/com.csc_PolicyInterestSearch/Policy/LossDt");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolLossDt))
                        {
                            oElement.Value = oPolicyEnquiry.PolLossDt;
                            oElement = null;
                        }

                        oElements = oTemplate.XPathSelectElements("./ClaimsSvcRq/com.csc_PolicyInterestListRq/com.csc_PolicyInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier");
                        if (oElements != null)
                        {
                            foreach (XElement objElement in oElements)
                            {
                                XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                XElement oSubElementNode = objElement.XPathSelectElement("./OtherId");
                                if (objEle != null && string.Equals(objEle.Value, "com.csc_MasterCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicyEnquiry.MasterCompany))
                                {
                                    oSubElementNode.Value = oPolicyEnquiry.MasterCompany;
                                }
                                else if (objEle != null && string.Equals(objEle.Value, "com.csc_LocCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicyEnquiry.Location))
                                {
                                    oSubElementNode.Value = oPolicyEnquiry.Location;
                                }
                                else if (objEle != null && string.Equals(objEle.Value, "com.csc_Module", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicyEnquiry.Module))
                                {
                                    oSubElementNode.Value = oPolicyEnquiry.Module;
                                }
                                objEle = null;
                                oSubElementNode = null;
                            }
                        }
                        oElements = null;
                    }

                }
                else if (string.Equals(sMode, "SAVEPOLICY", StringComparison.InvariantCultureIgnoreCase))
                {
                    oPolicySave = (PolicySaveRequest)oRequest;
                    PopulateCommonAcordRequestParameters(ref oTemplate, oPolicySave.PolicySystemId, ref p_objErrOut);
                    if (!string.IsNullOrEmpty(oPolicySave.PolicyIdentfier) && (oPolicySave.PolicySystemId > 0))
                    {
                        oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicySave.Token;
                            oElement = null;
                        }

                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInterestListRq/com.csc_PolicyInterestSearch/Policy/PolicyNumber");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicySave.PolicyNumber))
                        {
                            oElement.Value = oPolicySave.PolicyNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInterestListRq/com.csc_PolicyInterestSearch/Policy/CompanyProductCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicySave.PolicySymbol))
                        {
                            oElement.Value = oPolicySave.PolicySymbol;
                            oElement = null;
                        }

                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInterestListRq/com.csc_PolicyInterestSearch/Policy/LossDt");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicySave.PolLossDt))
                        {
                            oElement.Value = oPolicySave.PolLossDt;
                            oElement = null;
                        }

                        oElements = oTemplate.XPathSelectElements("./ClaimsSvcRq/com.csc_PolicyInterestListRq/com.csc_PolicyInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier");
                        if (oElements != null)
                        {
                            foreach (XElement objElement in oElements)
                            {
                                XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                XElement oSubElementNode = objElement.XPathSelectElement("./OtherId");
                                if (objEle != null && string.Equals(objEle.Value, "com.csc_MasterCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicySave.MasterCompany))
                                {
                                    oSubElementNode.Value = oPolicySave.MasterCompany;
                                }
                                else if (objEle != null && string.Equals(objEle.Value, "com.csc_LocCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicySave.Location))
                                {
                                    oSubElementNode.Value = oPolicySave.Location;
                                }
                                else if (objEle != null && string.Equals(objEle.Value, "com.csc_Module", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicySave.Module))
                                {
                                    oSubElementNode.Value = oPolicySave.Module;
                                }
                                objEle = null;
                                oSubElementNode = null;
                            }
                        }
                        oElements = null;
                    }
                }


                if (string.IsNullOrEmpty(oTemplate.ToString()))
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicyInterestListRequest.RequestOperationError", base.ClientId));
                }
                else
                    oAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
            }

            return oAcordRequest;
        }

        public string GetPolicyInterestDetailAcordRequest(PolicyEnquiry oEnquiry, ref BusinessAdaptorErrors p_objErrOut)
        {
            string oAcordRequest = string.Empty;
            XElement oElement = null;
            XElement oTemplate = GetPolicyInterestDetailAcordTemplate();
            PopulateCommonAcordRequestParameters(ref oTemplate, oEnquiry.PolicySystemId, ref p_objErrOut);
            IEnumerable<XElement> oElements = null;
            string sParentShortCode = string.Empty;
            LocalCache objCache = null;
            string sStatesTableNm = string.Empty;
            DataTable dtPolicyData = new DataTable();
            PolicySystemInterface objManager = null;
            try
            {
                if (!string.IsNullOrEmpty(oEnquiry.PolicyIdentfier) && (oEnquiry.PolicySystemId > 0))
                {
                    oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                    if (oElement != null)
                    {
                        oElement.Value = oEnquiry.Token;
                        oElement = null;
                    }

                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/PolicyNumber");
                    if (oElement != null && !string.IsNullOrEmpty(oEnquiry.PolicyNumber))
                    {
                        oElement.Value = oEnquiry.PolicyNumber;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/CompanyProductCd");
                    if (oElement != null && !string.IsNullOrEmpty(oEnquiry.PolicySymbol))
                    {
                        oElement.Value = oEnquiry.PolicySymbol;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/LOBCd");
                    if (oElement != null && !string.IsNullOrEmpty(oEnquiry.LOB))
                    {
                        oElement.Value = oEnquiry.LOB;
                        oElement = null;
                        //skhare7 Policy interface
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_BaseLOBLine");
                        if (oElement != null)
                        {
                            sParentShortCode = GetPolicyLOBRelatedShortCode(oEnquiry.LOB, "POLICY_CLAIM_LOB", ref p_objErrOut);

                            if (oElement != null && !string.IsNullOrEmpty(sParentShortCode))
                            {
                                oElement.Value = sParentShortCode;
                                oElement = null;
                            }



                        }
                    }
                    oElements = oTemplate.XPathSelectElements("./ClaimsSvcRq/com.csc_PolicyInterestDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier");
                    if (oElements != null)
                    {
                        foreach (XElement objElement in oElements)
                        {
                            XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                            XElement oSubElementNode = objElement.XPathSelectElement("./OtherId");
                            if (objEle != null && string.Equals(objEle.Value, "com.csc_MasterCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oEnquiry.MasterCompany))
                            {
                                oSubElementNode.Value = oEnquiry.MasterCompany;
                            }
                            else if (objEle != null && string.Equals(objEle.Value, "com.csc_LocCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oEnquiry.Location))
                            {
                                oSubElementNode.Value = oEnquiry.Location;
                            }
                            else if (objEle != null && string.Equals(objEle.Value, "com.csc_Module", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oEnquiry.Module))
                            {
                                oSubElementNode.Value = oEnquiry.Module;
                            }
                            else if (objEle != null && string.Equals(objEle.Value, "com.csc_RoleCd", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oEnquiry.InterestTypeCode))
                            {
                                oSubElementNode.Value = oEnquiry.InterestTypeCode;
                            }
                            else if (objEle != null && string.Equals(objEle.Value, "com.csc_Location", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oEnquiry.UnitRiskLocation))
                            {
                                oSubElementNode.Value = oEnquiry.UnitRiskLocation;
                            }
                            else if (objEle != null && string.Equals(objEle.Value, "com.csc_SeqNum", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oEnquiry.UnitSubRiskLocation))
                            {
                                oSubElementNode.Value = oEnquiry.UnitSubRiskLocation;
                            }
                            else if (objEle != null && string.Equals(objEle.Value, "com.csc_UnitNumber", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oEnquiry.UnitNumber))
                            {
                                oSubElementNode.Value = oEnquiry.UnitNumber;
                            }
                            else if (objEle != null && sParentShortCode.CompareTo("WL") == 0 && string.Equals(objEle.Value, "com.csc_State", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oEnquiry.UnitState))
                            {
                                oSubElementNode.Value = oEnquiry.UnitState;
                            }
                            else if (objEle != null && sParentShortCode.CompareTo("WL") == 0 && string.Equals(objEle.Value, "com.csc_PolicyCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oEnquiry.PolCompany))
                            {
                                oSubElementNode.Value = oEnquiry.PolCompany;
                            }
                            else if (objEle != null && sParentShortCode.CompareTo("WL") == 0 && string.Equals(objEle.Value, "com.csc_RateState", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oEnquiry.UnitState))
                            //skhare7 Poilicy Interface
                            //  sParentShortCode = GetPolicyLOBRelatedShortCode(oRequest.LOB.ToUpper(), "POLICY_CLAIM_LOB" ,ref p_objErrOut);
                            //if (sParentShortCode == string.Empty)
                            //{
                            //    throw new RMAppException(Globalization.GetString("PolicySystemInterface.GetPolicySystemList.NoBaseLobLine"));

                            //}
                            {
                                objManager = new PolicySystemInterface(m_connectionString, base.ClientId);
                                dtPolicyData = objManager.GetPolicySystemInfoById(oEnquiry.PolicySystemId);
                                sStatesTableNm = dtPolicyData.Rows[0]["TABLE_PREFIX"].ToString() + "_" + "STATES";
                                objCache = new LocalCache(m_connectionString, base.ClientId);
                                int iCodeID = objCache.GetCodeId(oEnquiry.UnitState, sStatesTableNm);//"POINT_STATES");
                                string sCodeDesc = objCache.GetCodeDesc(iCodeID);
                                oSubElementNode.Value = sCodeDesc;
                                //oElement.Value = "";
                                oSubElementNode = null;
                            }
                            objEle = null;
                            oSubElementNode = null;
                        }
                    }
                    oElements = null;
                }

                if (string.IsNullOrEmpty(oTemplate.ToString()))
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicyInterestDetailAcordRequest.RequestOperationError", base.ClientId));
                }
                else
                    oAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
                if (objCache != null)
                    objCache = null;
                if (objManager != null)
                    objManager = null;
                if (dtPolicyData != null)
                    dtPolicyData = null;
            }

            return oAcordRequest;
        }

        public string GetPolicyDriverListAcordRequest(Object oRequest, ref BusinessAdaptorErrors p_objErrOut, string sMode)
        {
            string oAcordRequest = string.Empty;
            XElement oElement = null;
            XElement oTemplate = GetDriverListAcordTemplate();
            IEnumerable<XElement> oElements = null;
            PolicyEnquiry oPolicyEnquiry = null;
            PolicySaveRequest oPolicySave = null;

            try
            {
                if (sMode.ToUpper().Equals("ENQUIRY"))
                {
                    oPolicyEnquiry = (PolicyEnquiry)oRequest;
                    PopulateCommonAcordRequestParameters(ref oTemplate, oPolicyEnquiry.PolicySystemId, ref p_objErrOut);
                    if (!string.IsNullOrEmpty(oPolicyEnquiry.PolicyIdentfier) && (oPolicyEnquiry.PolicySystemId > 0))
                    {
                        oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.Token;
                            oElement = null;
                        }

                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverListRq/com.csc_DriverSearch/Policy/PolicyNumber");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolicyNumber))
                        {
                            oElement.Value = oPolicyEnquiry.PolicyNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverListRq/com.csc_DriverSearch/Policy/CompanyProductCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolicySymbol))
                        {
                            oElement.Value = oPolicyEnquiry.PolicySymbol;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverListRq/com.csc_DriverSearch/Policy/LOBCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.LOB))
                        {
                            oElement.Value = oPolicyEnquiry.LOB;
                            oElement = null;
                        }
                        oElements = oTemplate.XPathSelectElements("./ClaimsSvcRq/com.csc_DriverListRq/com.csc_DriverSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier");
                        if (oElements != null)
                        {
                            foreach (XElement objElement in oElements)
                            {
                                XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                XElement oSubElementNode = objElement.XPathSelectElement("./OtherId");
                                if (objEle != null && string.Equals(objEle.Value, "com.csc_MasterCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicyEnquiry.MasterCompany))
                                {
                                    oSubElementNode.Value = oPolicyEnquiry.MasterCompany;
                                }
                                else if (objEle != null && string.Equals(objEle.Value, "com.csc_LocCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicyEnquiry.Location))
                                {
                                    oSubElementNode.Value = oPolicyEnquiry.Location;
                                }
                                else if (objEle != null && string.Equals(objEle.Value, "com.csc_Module", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicyEnquiry.Module))
                                {
                                    oSubElementNode.Value = oPolicyEnquiry.Module;
                                }
                                objEle = null;
                                oSubElementNode = null;
                            }
                        }
                        oElements = null;
                    }
                }
                else if (sMode.ToUpper().Equals("SAVEPOLICY"))
                {
                    oPolicySave = (PolicySaveRequest)oRequest;
                    PopulateCommonAcordRequestParameters(ref oTemplate, oPolicySave.PolicySystemId, ref p_objErrOut);
                    if (!string.IsNullOrEmpty(oPolicySave.PolicyIdentfier) && (oPolicySave.PolicySystemId > 0))
                    {
                        oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicySave.Token;
                            oElement = null;
                        }

                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverListRq/com.csc_DriverSearch/Policy/PolicyNumber");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicySave.PolicyNumber))
                        {
                            oElement.Value = oPolicySave.PolicyNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverListRq/com.csc_DriverSearch/Policy/CompanyProductCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicySave.PolicySymbol))
                        {
                            oElement.Value = oPolicySave.PolicySymbol;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverListRq/com.csc_DriverSearch/Policy/LOBCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicySave.LOB))
                        {
                            oElement.Value = oPolicySave.LOB;
                            oElement = null;
                        }
                        oElements = oTemplate.XPathSelectElements("./ClaimsSvcRq/com.csc_DriverListRq/com.csc_DriverSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier");
                        if (oElements != null)
                        {
                            foreach (XElement objElement in oElements)
                            {
                                XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                XElement oSubElementNode = objElement.XPathSelectElement("./OtherId");
                                if (objEle != null && string.Equals(objEle.Value, "com.csc_MasterCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicySave.MasterCompany))
                                {
                                    oSubElementNode.Value = oPolicySave.MasterCompany;
                                }
                                else if (objEle != null && string.Equals(objEle.Value, "com.csc_LocCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicySave.Location))
                                {
                                    oSubElementNode.Value = oPolicySave.Location;
                                }
                                else if (objEle != null && string.Equals(objEle.Value, "com.csc_Module", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicySave.Module))
                                {
                                    oSubElementNode.Value = oPolicySave.Module;
                                }
                                objEle = null;
                                oSubElementNode = null;
                            }
                        }
                        oElements = null;
                    }
                }

                if (string.IsNullOrEmpty(oTemplate.ToString()))
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicyDriverListAcordRequest.RequestOperationError", base.ClientId));
                }
                else
                    oAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
                oPolicySave = null;
                oPolicyEnquiry = null;
            }

            return oAcordRequest;
        }

        public string GetPolicyDriverDetailAcordRequest(PolicyEnquiry oEnquiry, ref BusinessAdaptorErrors p_objErrOut)
        {
            string oAcordRequest = string.Empty;
            XElement oElement = null;
            XElement oTemplate = GetDriverDetailAcordTemplate();
            PopulateCommonAcordRequestParameters(ref oTemplate, oEnquiry.PolicySystemId, ref p_objErrOut);
            string sParentLobCode = string.Empty;
            try
            {

                if (!string.IsNullOrEmpty(oEnquiry.PolicyIdentfier) && (oEnquiry.PolicySystemId > 0))
                {
                    oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                    if (oElement != null)
                    {
                        oElement.Value = oEnquiry.Token;
                        oElement = null;
                    }

                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/PolicyNumber");
                    if (oElement != null && !string.IsNullOrEmpty(oEnquiry.PolicyNumber))
                    {
                        oElement.Value = oEnquiry.PolicyNumber;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/CompanyProductCd");
                    if (oElement != null && !string.IsNullOrEmpty(oEnquiry.PolicySymbol))
                    {
                        oElement.Value = oEnquiry.PolicySymbol;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/LOBCd");
                    if (oElement != null && !string.IsNullOrEmpty(oEnquiry.LOB))
                    {
                        oElement.Value = oEnquiry.LOB;
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_BaseLOBLine");
                        sParentLobCode = GetPolicyLOBRelatedShortCode(oEnquiry.LOB, "POLICY_CLAIM_LOB", ref p_objErrOut);

                        if (oElement != null && !string.IsNullOrEmpty(sParentLobCode))
                        {
                            oElement.Value = sParentLobCode;
                            oElement = null;
                        }
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_DriverStatus");
                    if (oElement != null && !string.IsNullOrEmpty(oEnquiry.UnitStatus))
                    {
                        oElement.Value = oEnquiry.UnitStatus;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oEnquiry.Location;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oEnquiry.MasterCompany;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oEnquiry.Module;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_DriverId']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oEnquiry.UnitNumber;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oEnquiry.Product;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_DriverState']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oEnquiry.UnitState;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolCompany']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oEnquiry.PolCompany;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_DriverType']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oEnquiry.InterestTypeCode;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oEnquiry.UnitRiskLocation;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId");
                    if (oElement != null)
                    {
                        oElement.Value = oEnquiry.UnitSubRiskLocation;
                        oElement = null;
                    }
                }


                if (string.IsNullOrEmpty(oTemplate.ToString()))
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicyDriverDetailAcordRequest.RequestOperationError", base.ClientId));
                }
                else
                    oAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
            }

            return oAcordRequest;
        }

        public string GetPolicyDriverDetailSaveAcordRequest(string sSelectedValue, string sFileContent, SaveDownloadOptions oSaveDownloadOptions, ref BusinessAdaptorErrors p_objErrOut)
        {
            string oAcordRequest = string.Empty;
            XElement oElement = null;
            XElement oTemplate = GetDriverDetailAcordTemplate();
            PopulateCommonAcordRequestParameters(ref oTemplate, oSaveDownloadOptions.PolicySystemId, ref p_objErrOut);
            XmlDocument objDocument = null;
            XmlNodeList objNodeList = null;
            XmlDocument objDoc = null;
            PolicySystemInterface objManager = null;
            XElement objPolicySaveAccord = null;
            XElement oPolicySaveNode = null;
            IEnumerable<XElement> objElements = null;

            try
            {

                objDocument = new XmlDocument();
                objDocument.LoadXml(sFileContent);

                if (objDocument != null)
                {
                    objNodeList = objDocument.SelectNodes("//SequenceNumber");
                    foreach (XmlNode objNode in objNodeList)
                    {
                        if (string.Equals(objNode.InnerText, sSelectedValue.Split('|')[0], StringComparison.InvariantCultureIgnoreCase))
                        {

                            objDoc = new XmlDocument();
                            objDoc.LoadXml(objNode.ParentNode.ParentNode.OuterXml);

                            objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                            objPolicySaveAccord = XElement.Parse(objManager.GetDownLoadXMLData("POLICY", oSaveDownloadOptions.PolicyId, oSaveDownloadOptions.PolicyId));

                            oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                            if (oElement != null)
                            {
                                oElement.Value = oSaveDownloadOptions.Token;
                                oElement = null;
                            }

                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/PolicyNumber");
                            if (oElement != null)
                            {
                                oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber");
                                if (oPolicySaveNode != null)
                                {
                                    oElement.Value = oPolicySaveNode.Value;
                                    oElement = null;
                                    oPolicySaveNode = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/CompanyProductCd");
                            if (oElement != null)
                            {
                                oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/CompanyProductCd");
                                if (oPolicySaveNode != null)
                                {
                                    oElement.Value = oPolicySaveNode.Value;
                                    oElement = null;
                                    oPolicySaveNode = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/LOBCd");
                            if (oElement != null)
                            {
                                oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/LOBCd");
                                if (oPolicySaveNode != null)
                                {
                                    oElement.Value = oPolicySaveNode.Value;
                                    oElement = null;
                                    //oPolicySaveNode = null;
                                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_BaseLOBLine");

                                    string sParentLobCode = GetPolicyLOBRelatedShortCode(oPolicySaveNode.Value, "POLICY_CLAIM_LOB", ref p_objErrOut);

                                    if (oElement != null)
                                    {
                                        oElement.Value = sParentLobCode;
                                        oElement = null;
                                    }
                                    oElement = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_DriverStatus");
                            if (oElement != null)
                            {
                                if (objDoc.SelectSingleNode("//DriverStatus") != null)
                                {
                                    oElement.Value = objDoc.SelectSingleNode("//DriverStatus").InnerText;
                                    oElement = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                            if (oElement != null)
                            {
                                objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                                foreach (XElement objElement in objElements)
                                {
                                    XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_LocCompany", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oElement.Value = oSubElement.Value;
                                    }

                                    objEle = null;
                                    oSubElement = null;
                                }
                                objElements = null;

                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId");
                            if (oElement != null)
                            {
                                objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                                foreach (XElement objElement in objElements)
                                {
                                    XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_MasterCompany", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oElement.Value = oSubElement.Value;
                                    }

                                    objEle = null;
                                    oSubElement = null;
                                }
                                objElements = null;

                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId");
                            if (oElement != null)
                            {
                                objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                                foreach (XElement objElement in objElements)
                                {
                                    XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_Module", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oElement.Value = oSubElement.Value;
                                    }

                                    objEle = null;
                                    oSubElement = null;
                                }
                                objElements = null;

                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_DriverId']/OtherId");
                            if (oElement != null)
                            {
                                if (objDoc.SelectSingleNode("//DriverId") != null)
                                {
                                    oElement.Value = objDoc.SelectSingleNode("//DriverId").InnerText;
                                    oElement = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Product']/OtherId");
                            if (oElement != null)
                            {
                                if (objDoc.SelectSingleNode("//DriverProduct") != null)
                                {
                                    oElement.Value = objDoc.SelectSingleNode("//DriverProduct").InnerText;
                                    oElement = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_DriverState']/OtherId");
                            if (oElement != null)
                            {
                                if (objDoc.SelectSingleNode("//DriverState") != null)
                                {
                                    oElement.Value = objDoc.SelectSingleNode("//DriverState").InnerText;
                                    oElement = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PolCompany']/OtherId");
                            if (oElement != null)
                            {
                                objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                                foreach (XElement objElement in objElements)
                                {
                                    XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                    XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                                    if (objEle != null && string.Equals(objEle.Value, "com.csc_LocCompany", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oElement.Value = oSubElement.Value;
                                    }

                                    objEle = null;
                                    oSubElement = null;
                                }
                                objElements = null;

                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_DriverType']/OtherId");
                            if (oElement != null)
                            {
                                if (objDoc.SelectSingleNode("//DriverType") != null)
                                {
                                    oElement.Value = objDoc.SelectSingleNode("//DriverType").InnerText;
                                    oElement = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId");
                            if (oElement != null)
                            {
                                if (objDoc.SelectSingleNode("//RiskLoc") != null)
                                {
                                    oElement.Value = objDoc.SelectSingleNode("//RiskLoc").InnerText;
                                    oElement = null;
                                }
                            }
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_DriverDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId");
                            if (oElement != null)
                            {
                                if (objDoc.SelectSingleNode("//RiskSubLoc") != null)
                                {
                                    oElement.Value = objDoc.SelectSingleNode("//RiskSubLoc").InnerText;
                                    oElement = null;
                                }
                            }



                        }
                    }
                }
                oAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
            }

            return oAcordRequest;
        }

        public string GetPolicyEndorsementDataAcordRequest(Object oRequest, ref BusinessAdaptorErrors p_objErrOut, string sMode)
        {
            string oAcordRequest = string.Empty;
            XElement oElement = null;
            XElement oTemplate = GetEndorsementDataAcordTemplate();
            PolicyEnquiry oEnquiryRequest = null;
            PolicySaveRequest oSavePolicyRequest = null;
            SaveDownloadOptions oSaveDownloadOptions = null;
            XElement objPolicySaveAccord = null;
            PolicySystemInterface objManager = null;
            XElement oPolicySaveNode = null;
            IEnumerable<XElement> objElements = null;
            string sBaseLobLine = string.Empty;
            LocalCache objCache = null;
            DataTable dtPolData = null;
            string sStatesTableNm = string.Empty;
            int iCodeID = 0;
            string sCodeDesc = string.Empty;
            try
            {
                if (sMode == "ENQUIRY")
                {
                    oEnquiryRequest = (PolicyEnquiry)oRequest;
                    PopulateCommonAcordRequestParameters(ref oTemplate, oEnquiryRequest.PolicySystemId, ref p_objErrOut);
                    objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                    if (!string.IsNullOrEmpty(oEnquiryRequest.PolicyIdentfier) && (oEnquiryRequest.PolicySystemId > 0))
                    {
                        oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                        if (oElement != null)
                        {
                            oElement.Value = oEnquiryRequest.Token;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/PolicyNumber");
                        if (oElement != null)
                        {
                            oElement.Value = oEnquiryRequest.PolicyNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/CompanyProductCd");
                        if (oElement != null && !string.IsNullOrEmpty(oEnquiryRequest.PolicySymbol))
                        {
                            oElement.Value = oEnquiryRequest.PolicySymbol;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oEnquiryRequest.Location;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oEnquiryRequest.MasterCompany;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oEnquiryRequest.Module;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oEnquiryRequest.InsLine;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oEnquiryRequest.UnitNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oEnquiryRequest.UnitRiskLocation;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oEnquiryRequest.UnitSubRiskLocation;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitStateID']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = "0";
                            if (!string.IsNullOrEmpty(oEnquiryRequest.UnitState))// Contains unit state's state id in corresponding point system.
                            {
                                if (oEnquiryRequest.BaseLOBLine == "WL")
                                {
                                    objCache = new LocalCache(m_connectionString, base.ClientId);
                                    dtPolData = objManager.GetPolicySystemInfoById(oEnquiryRequest.PolicySystemId);
                                    sStatesTableNm = dtPolData.Rows[0]["TABLE_PREFIX"] + "_" + "STATES";

                                    //int iCodeID = objCache.GetCodeId(oRequest.UnitState, "POINT_STATES");
                                    iCodeID = objCache.GetCodeId(oEnquiryRequest.UnitState, sStatesTableNm);
                                    sCodeDesc = objCache.GetCodeDesc(iCodeID);
                                    oElement.Value = sCodeDesc;
                                    //oElement.Value = "";
                                    oElement.Value = sCodeDesc;
                                }

                            }
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_BaseLOBLine");
                        if (oElement != null)
                        {
                            oElement.Value = oEnquiryRequest.BaseLOBLine;
                            oElement = null;
                        }
                    }
                }
                else if (sMode.ToUpper().Equals("SAVEPOLICY"))
                {
                    oSavePolicyRequest = (PolicySaveRequest)oRequest;
                    PopulateCommonAcordRequestParameters(ref oTemplate, oSavePolicyRequest.PolicySystemId, ref p_objErrOut);
                    if (!string.IsNullOrEmpty(oSavePolicyRequest.PolicyIdentfier) && (oSavePolicyRequest.PolicySystemId > 0))
                    {
                        oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                        if (oElement != null)
                        {
                            oElement.Value = oSavePolicyRequest.Token;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/PolicyNumber");
                        if (oElement != null)
                        {
                            oElement.Value = oSavePolicyRequest.PolicyNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/CompanyProductCd");
                        if (oElement != null && !string.IsNullOrEmpty(oSavePolicyRequest.PolicySymbol))
                        {
                            oElement.Value = oSavePolicyRequest.PolicySymbol;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oSavePolicyRequest.Location;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oSavePolicyRequest.MasterCompany;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oSavePolicyRequest.Module;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = "*AL";
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitStateID']/OtherId");
                        if (oElement != null)
                        {
                            if (oSavePolicyRequest.BaseLOBLine == "WL")
                            {
                                oElement.Value = "0"; // In case of WL claim, Unit state will be 0
                            }
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_BaseLOBLine");
                        if (oElement != null)
                        {
                            oElement.Value = oSavePolicyRequest.BaseLOBLine;
                            oElement = null;
                        }
                    }
                }
                else if (sMode.ToUpper().Equals("SAVEUNIT"))
                {
                    oSaveDownloadOptions = (SaveDownloadOptions)oRequest;
                    sBaseLobLine = GetPolicyLOBRelatedShortCode(oSaveDownloadOptions.LOBCd, "POLICY_CLAIM_LOB", ref p_objErrOut);
                        //if (!string.IsNullOrEmpty(oSavePolicyRequest.PolicyIdentfier) && (oSavePolicyRequest.PolicySystemId > 0))
                        //{
                        objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                        objPolicySaveAccord = XElement.Parse(objManager.GetDownLoadXMLData("POLICY", oSaveDownloadOptions.PolicyId, oSaveDownloadOptions.PolicyId));
                        PopulateCommonAcordRequestParameters(ref oTemplate, oSaveDownloadOptions.PolicySystemId, ref p_objErrOut);
                        oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                        if (oElement != null)
                        {
                            oElement.Value = oSaveDownloadOptions.Token;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/PolicyNumber");
                        if (oElement != null)
                        {
                            //oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/PolicyNumber");
                            //if (oElement != null)
                            //{
                            oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber");
                            if (oPolicySaveNode != null)
                            {
                                oElement.Value = oPolicySaveNode.Value;
                                oElement = null;
                                oPolicySaveNode = null;
                            }
                            //}
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/CompanyProductCd");
                        if (oElement != null)
                        {
                            oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/CompanyProductCd");
                            if (oPolicySaveNode != null)
                            {
                                oElement.Value = oPolicySaveNode.Value;
                                oElement = null;
                                oPolicySaveNode = null;
                            }
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                        if (oElement != null)
                        {
                            objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                            foreach (XElement objElement in objElements)
                            {
                                XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                                if (objEle != null && string.Equals(objEle.Value, "com.csc_LocCompany", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    oElement.Value = oSubElement.Value;
                                }

                                objEle = null;
                                oSubElement = null;
                            }
                            objElements = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId");
                        if (oElement != null)
                        {
                            objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                            foreach (XElement objElement in objElements)
                            {
                                XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                                if (objEle != null && string.Equals(objEle.Value, "com.csc_MasterCompany", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    oElement.Value = oSubElement.Value;
                                }

                                objEle = null;
                                oSubElement = null;
                            }
                            objElements = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId");
                        if (oElement != null)
                        {
                            objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                            foreach (XElement objElement in objElements)
                            {
                                XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                                XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                                if (objEle != null && string.Equals(objEle.Value, "com.csc_Module", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    oElement.Value = oSubElement.Value;
                                }

                                objEle = null;
                                oSubElement = null;
                            }
                            objElements = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_InsLineCd']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oSaveDownloadOptions.InsuranceLine;
                            oElement = null;
                        }


                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oSaveDownloadOptions.UnitNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskLoc']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oSaveDownloadOptions.RiskLocation;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_RiskSubLoc']/OtherId");
                        if (oElement != null)
                        {
                            oElement.Value = oSaveDownloadOptions.RiskSubLocation;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_BaseLOBLine");
                        if (oElement != null)
                        {
                            oElement.Value = sBaseLobLine;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyFormDataRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitStateID']/OtherId");
                        if (oElement != null)
                        {
                            if (sBaseLobLine == "WL")
                            {
                                objCache = new LocalCache(m_connectionString, base.ClientId);
                                dtPolData = objManager.GetPolicySystemInfoById(oSaveDownloadOptions.PolicySystemId);
                                sStatesTableNm = dtPolData.Rows[0]["TABLE_PREFIX"] + "_" + "STATES";

                                //int iCodeID = objCache.GetCodeId(oRequest.UnitState, "POINT_STATES");
                                iCodeID = objCache.GetCodeId(oSaveDownloadOptions.UnitState, sStatesTableNm);
                                sCodeDesc = objCache.GetCodeDesc(iCodeID);
                                oElement.Value = sCodeDesc;
                                //oElement.Value = "";
                            }
                            oElement = null;
                        }
                        //}
                    
                }
                if (string.IsNullOrEmpty(oTemplate.ToString()))
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicyFormDataAcordRequest.RequestOperationError", base.ClientId));
                }
                else
                    oAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
                oEnquiryRequest = null;
                oSavePolicyRequest = null;
                objCache = null;
                dtPolData = null;
            }

            return oAcordRequest;
        }

        private void PopulateCommonAcordRequestParameters(ref XElement oTemplate, int iPolicySystemId, ref BusinessAdaptorErrors p_objErrOut)
        {
            XElement oElement = null;
            SysSettings oSysSettings = new SysSettings(connectionString, base.ClientId);
            LocalCache objCache = new LocalCache(connectionString, base.ClientId);
            if (oTemplate != null)
            {
                oElement = oTemplate.XPathSelectElement("./SignonRq/ClientApp/Org");
                if (oElement != null)
                {
                    oElement.Value = oSysSettings.ClientName;
                    oElement = null;
                }
                oElement = oTemplate.XPathSelectElement("./SignonRq/ClientApp/Name");
                if (oElement != null)
                {
                    oElement.Value = oSysSettings.DTGProduct;
                    oElement = null;
                }
                oElement = oTemplate.XPathSelectElement("./SignonRq/ClientApp/Version");
                if (oElement != null)
                {
                    oElement.Value = GetPSVersionKeyForAcordRequest(ref oTemplate, iPolicySystemId, ref p_objErrOut);
                    oElement = null;
                }
            }
            oSysSettings = null;
        }

        private string GetPSVersionKeyForAcordRequest(ref XElement oTemplate, int iPolicySystemId, ref BusinessAdaptorErrors p_objErrOut)
        {
            DataTable oDT = null;
            XElement oElement = null;
            XElement oNodeElement = null;
            string sVersionKey = string.Empty;
            string sSystemTypeName = string.Empty;
            string sSystemVersion = string.Empty;
            string sFileName = string.Empty;
            string sFilePath = string.Empty;
            string sFileContent = string.Empty;
            string sNodeValues = string.Empty;
            string sUseRMAUser = string.Empty;
            string sCFWUserName = string.Empty;
            try
            {
                if (iPolicySystemId > 0)
                {
                    oDT = GetPolicySystemInfoById(iPolicySystemId, ref p_objErrOut);
                    if (oDT != null && oDT.Rows.Count > 0)
                    {
                        sSystemTypeName = oDT.Rows[0]["POLICY_SYSTEM_TYPE"].ToString();
                        sSystemVersion = oDT.Rows[0]["VERSION"].ToString();
                        sUseRMAUser = oDT.Rows[0]["USE_RMA_USER"].ToString();
                        sCFWUserName = oDT.Rows[0]["CFW_USERNAME"].ToString();


                        if (string.Equals(sSystemTypeName, "POINT"))
                        {
                            string sPointClientFileEnable = oDT.Rows[0]["CLIENTFILE_FLAG"].ToString();
                            oElement = oTemplate.XPathSelectElement("./SignonRq/ClientApp/com.csc_ClientFile");
                            if (oElement != null)
                                oElement.Value = sPointClientFileEnable == "1" ? "true" : "false";
                            oElement = null;


                            oElement = oTemplate.XPathSelectElement("./SignonRq/SignonPswd/CustId/CustLoginId");
                            if (oElement != null)
                            {
                                if (Conversion.ConvertStrToBool(sUseRMAUser))
                                    oElement.Value = m_loginName;
                                else
                                    oElement.Value = sCFWUserName;
                                oElement = null;
                            }
                        }

                        //if (string.Equals(oTemplate.XPathSelectElement("./ClaimsSvcRq/*[2]").Name.ToString(), "com.csc_PolicyUnitListRq", StringComparison.InvariantCultureIgnoreCase))
                        //{
                        //    PopulateCLUnitMappingValues(ref oTemplate, oDT);
                        //}

                        //tanwar2 - staging - added if condition
                        if (string.Compare(sSystemTypeName,"STAGING",true)!=0)
                        {
                            sFileName = sSystemTypeName + "_VersionVerifier.xml";
                            if (File.Exists(RMConfigurator.UserDataPath + "\\PolicyInterface\\" + sFileName))
                            {
                                sFilePath = RMConfigurator.UserDataPath + "\\PolicyInterface\\" + sFileName;
                                sFileContent = File.ReadAllText(sFilePath);
                                if (!string.IsNullOrEmpty(sFileContent))
                                {
                                    oElement = XElement.Parse(sFileContent);
                                    if (oElement != null)
                                    {
                                        oNodeElement = oElement.XPathSelectElements("./" + sSystemTypeName).Where(a => a.Attribute("version").Value == sSystemVersion).SingleOrDefault();
                                        sNodeValues = oNodeElement.Value;
                                        if (sNodeValues.Contains(oTemplate.XPathSelectElement("./ClaimsSvcRq/*[2]").Name.ToString()))
                                        {
                                            sVersionKey = oNodeElement.Attribute("versionkey").Value.Trim();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                throw new RMAppException(string.Format(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPSVersionKeyForAcordRequest.FileNotFoundError", base.ClientId), sSystemTypeName));
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oDT != null)
                    oDT = null;
                if (oElement != null)
                    oElement = null;
                if (oNodeElement != null)
                    oNodeElement = null;
            }

            return sVersionKey;
        }

        private void WriteFormattedXmlToFile(string p_sFileName, string sData, ref BusinessAdaptorErrors p_objErrOut)
        {
            StreamWriter objWriter = null;
            string sFilePath = string.Empty;
            try
            {
                sFilePath = RMConfigurator.BasePath + "//temp//PolicyInterface//" + p_sFileName;
                if (File.Exists(sFilePath))
                {
                    File.Delete(sFilePath);
                }

                objWriter = new StreamWriter(sFilePath);
                objWriter.WriteLine(sData);
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (objWriter != null)
                {
                    objWriter.Close();
                }
            }
        }

        private void DeleteFile(string p_sFileName)
        {
            string sFilePath = string.Empty;
            try
            {
                sFilePath = RMConfigurator.BasePath + "//temp//PolicyInterface//" + p_sFileName;
                if (File.Exists(sFilePath))
                {
                    File.Delete(sFilePath);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public DataTable GetPolicySystemInfoById(int iPolicySystemId, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicySystemInterface oPolicySystemInterface = null;
            DataTable oDT = null;
            try
            {
                oPolicySystemInterface = new PolicySystemInterface(connectionString, base.ClientId);
                oDT = oPolicySystemInterface.GetPolicySystemInfoById(iPolicySystemId);
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oDT = null;
            }
            finally
            {
                if (oPolicySystemInterface != null)
                    oPolicySystemInterface = null;
            }
            return oDT;
        }

        public int GetPolicySystemId(int iPolicyId, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicySystemInterface oPolicySystemInterface = null;
            int iPolicySystemId = 0;
            try
            {
                oPolicySystemInterface = new PolicySystemInterface(connectionString, base.ClientId);
                iPolicySystemId = oPolicySystemInterface.GetPolicySystemId(iPolicyId);
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oPolicySystemInterface != null)
                    oPolicySystemInterface = null;
            }
            return iPolicySystemId;
        }

        public void GetDownLoadXMLData(PolicyDownload oRequest, out PolicyDownload oResponse, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicySystemInterface oPolicySystemInterface = null;
            string sResult = string.Empty, sXSLTPath = string.Empty, sTypeCd = string.Empty;
            string sSystemName = string.Empty;
            DataTable dtPolicySystemInfo = null;
            sTypeCd = oRequest.UnitTypeCode;
            oResponse = new PolicyDownload();
            try
            {

                oPolicySystemInterface = new PolicySystemInterface(connectionString, base.ClientId);
                sResult = oPolicySystemInterface.GetDownLoadXMLData(oRequest.TableName, oRequest.TableRowID, oRequest.PolicyID);
                sXSLTPath = RMConfigurator.UserDataPath;
                sXSLTPath = RMConfigurator.CombineFilePath(sXSLTPath, "PolicyInterface/PolicyDetailXSL");
                if (oRequest.PolicySystemId != 0)
                {
                    dtPolicySystemInfo = oPolicySystemInterface.GetPolicySystemInfoById(oRequest.PolicySystemId);//vkumar258
                }
                else
                {
                    dtPolicySystemInfo = oPolicySystemInterface.GetPolicySystemInfoById(oPolicySystemInterface.GetPolicySystemId(oRequest.PolicyID));
                }
                if (dtPolicySystemInfo != null && dtPolicySystemInfo.Rows.Count > 0)
                    sSystemName = dtPolicySystemInfo.Rows[0]["POLICY_SYSTEM_TYPE"].ToString();
                if (CommonFunctions.GetPolicySystemTypeIndicator(sSystemName.Trim()) == Constants.POLICY_SYSTEM_TYPE.POINT)
                {
                    switch (oRequest.TableName)
                    {
                        case "POLICY":
                            sXSLTPath = RMConfigurator.CombineFilePath(sXSLTPath, "PolicyEnquireRs.xslt");
                            break;
                        case "POLICY_X_UNIT":
                            sXSLTPath = RMConfigurator.CombineFilePath(sXSLTPath, "PolicyUnitDetailRs.xslt");
                            break;
                        case "POLICY_X_ENTITY":
                            if (sTypeCd == "D")           // load XSLT for driver based on type code set in UI
                                sXSLTPath = RMConfigurator.CombineFilePath(sXSLTPath, "PolicyDriverDetailRs.xslt");
                            else if (sTypeCd == "PI")   // load XSLT for policy interest based on type code set in UI
                                sXSLTPath = RMConfigurator.CombineFilePath(sXSLTPath, "PolicyInterestDetailRs.xslt");
                            break;
                        case "POLICY_X_CVG_TYPE":
                            sXSLTPath = RMConfigurator.CombineFilePath(sXSLTPath, "PointCoveragesdtl.xslt");
                            break;
                    }
                }
                else
                    sXSLTPath = RMConfigurator.CombineFilePath(sXSLTPath, "PolicyAdditionalDetails.xslt");

                oResponse.AcordResponse = sResult;
                oResponse.XSLTPath = sXSLTPath;
                oResponse.PolicySystemType = sSystemName;
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);

            }
            finally
            {
                if (oPolicySystemInterface != null)
                    oPolicySystemInterface = null;
            }
        }

        public void GetEndorsementData(PolicyDownload oRequest, out PolicyDownload oResponse, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicySystemInterface oPolicySystemInterface = null;
            DataTable dtEndorsementData;
            oResponse = new PolicyDownload();
            try
            {
                oPolicySystemInterface = new PolicySystemInterface(connectionString, base.ClientId);
                dtEndorsementData = new DataTable();

                dtEndorsementData = oPolicySystemInterface.GetEndorsementDataInfo(oRequest.TableName, oRequest.TableRowID, oRequest.PolicyID, oRequest.UnitTypeCode);
                oResponse.EndorsementData = dtEndorsementData;
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);

            }
            finally
            {
                if (oPolicySystemInterface != null)
                    oPolicySystemInterface = null;
            }
        }

        public void SaveCoveragesXMLToFile(string sData, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sFileName = "Coverages_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml";
            WriteFormattedXmlToFile(sFileName, sData, ref p_objErrOut);
        }

        public string GetCoverageXMLFromFile(string sCovCd, string sCovSeqNo, string sCovTransSeqNo)
        {
            string sFilePath = string.Empty;
            string sFileContent = string.Empty;
            string sFileName = string.Empty;
            XElement oCoverageXML = null;
            XElement oCoveragesXML = null;

            sFileName = "Coverages_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml";
            if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + sFileName))
            {
                sFilePath = RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + sFileName;
                sFileContent = File.ReadAllText(sFilePath);

                if (!string.IsNullOrEmpty(sFileContent))
                {
                    oCoveragesXML = XElement.Parse(sFileContent);
                    foreach (XElement objEle in oCoveragesXML.XPathSelectElements("//com.csc_CoverageLossInfo"))
                    {
                        if (objEle.XPathSelectElement("Coverage/CoverageCd") != null &&
                        objEle.XPathSelectElement("ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_CoverageSeq']/OtherId") != null &&
                        objEle.XPathSelectElement("ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_TrxnSeq']/OtherId") != null
                            &&
                            objEle.XPathSelectElement("Coverage/CoverageCd").Value == sCovCd &&
                        objEle.XPathSelectElement("ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_CoverageSeq']/OtherId").Value == sCovSeqNo &&
                        objEle.XPathSelectElement("ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_TrxnSeq']/OtherId").Value == sCovTransSeqNo
                        )
                        {
                            sFileContent = objEle.ToString();
                            break;
                        }
                    }
                }
            }

            return sFileContent;
        }

        private string GetXMLTemplateByType(string Type, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sFilePath = string.Empty;
            string sFileName = string.Empty;
            XmlDocument objXmlDocument = null;
            string sFileContent = string.Empty;

            try
            {
                switch (Type)
                {
                    case "Policy": sFileName = "Policy_Template.xml"; break;
                    case "Entity": sFileName = "Entity_Template.xml"; break;
                    case "Vehicle": sFileName = "Vehicle_Template.xml"; break;
                    case "Property": sFileName = "Property_Template.xml"; break;
                    case "EntityList": sFileName = "EntityList_Template.xml"; break;
                    case "UnitList": sFileName = "UnitList_Template.xml"; break;
                    case "SiteUnit": sFileName = "Site_Template.xml"; break;
                    case "Coverage": sFileName = "Coverage_Template.xml"; break;
                    //skhare7 Unit interest list
                    case "UnitInterestList": sFileName = "UnitInterestlist_Template.xml"; break;
                    case "CoverageList": sFileName = "CoverageList_Template.xml"; break;
                }
                if (File.Exists(RMConfigurator.BasePath + "\\userdata\\PolicyInterface\\" + sFileName))
                {
                    sFilePath = RMConfigurator.BasePath + "\\userdata\\PolicyInterface\\" + sFileName;
                    sFileContent = File.ReadAllText(sFilePath);
                    if (!string.IsNullOrEmpty(sFileContent))
                    {
                        objXmlDocument = new XmlDocument();
                        objXmlDocument.LoadXml(sFileContent);
                        sFileContent = objXmlDocument.OuterXml;
                    }
                }
                else
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetDataModelTemplateByType.DataModelTemplateError", base.ClientId));
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                sFileContent = string.Empty;
            }
            finally
            {
                if (objXmlDocument != null)
                    objXmlDocument = null;
            }

            return sFileContent;
        }
        //skhare7 Policy Interface
        public string GetPolicyLOBRelatedShortCode(string p_sShortCode, string p_sTableName, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sRelatedShortCode = string.Empty;
            LocalCache objcache = new LocalCache(connectionString, base.ClientId);
            sRelatedShortCode = objcache.GetRelatedShortCode(objcache.GetCodeId(p_sShortCode.Trim(), p_sTableName));
            if (string.IsNullOrEmpty(sRelatedShortCode))
            {
                throw new RMAppException("Policy System Base LOB Line Mapping Error: Missing Base LOB Line.");
            }

            return sRelatedShortCode;
        }

        public string GetUnitInterestListAcordRequest(Object oRequest, ref BusinessAdaptorErrors p_objErrOut, string sMode, string sRequestXML)
        {

            string oAcordRequest = string.Empty;
            XElement oElement = null;
            XElement oTemplate = GetUnitInterestListAcordTemplate();
            IEnumerable<XElement> oElements = null;
            PolicyEnquiry oPolicyEnquiry = null;
            PolicySaveRequest oPolicySave = null;
            SaveDownloadOptions oSaveOptions = null;
            XElement objPolicySaveAccord = null;
            PolicySystemInterface objManager = null;
            XElement oPolicySaveNode = null;
            IEnumerable<XElement> objElements = null;
            string sParentShortCode = string.Empty;
            string sTemp = string.Empty;
            string sParentLobCode = string.Empty;
            try
            {
                if (string.Equals(sMode, "ENQUIRY", StringComparison.InvariantCultureIgnoreCase))
                {
                    oPolicyEnquiry = (PolicyEnquiry)oRequest;
                    PopulateCommonAcordRequestParameters(ref oTemplate, oPolicyEnquiry.PolicySystemId, ref p_objErrOut);
                    if (!string.IsNullOrEmpty(oPolicyEnquiry.PolicyIdentfier) && (oPolicyEnquiry.PolicySystemId > 0))
                    {
                        oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                        if (oElement != null)
                        {
                            oElement.Value = oPolicyEnquiry.Token;
                            oElement = null;
                        }

                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/PolicyNumber");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolicyNumber))
                        {
                            oElement.Value = oPolicyEnquiry.PolicyNumber;
                            oElement = null;
                        }

                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/CompanyProductCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolicySymbol))
                        {
                            oElement.Value = oPolicyEnquiry.PolicySymbol;
                            oElement = null;
                        }

                        //  dbisht6  Jira RMA-369 
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/LossDt");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolLossDt))
                        {
                            oElement.Value = oPolicyEnquiry.PolLossDt;
                            oElement = null;
                        }
                        //dbisht6 end

                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.MasterCompany))
                        {

                            oElement.Value = oPolicyEnquiry.MasterCompany;
                            oElement = null;
                        }

                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.Location))
                        {

                            oElement.Value = oPolicyEnquiry.Location;
                            oElement = null;
                        }

                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.Module))
                        {

                            oElement.Value = oPolicyEnquiry.Module;
                            oElement = null;
                        }

                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.UnitNumber))
                        {

                            oElement.Value = oPolicyEnquiry.UnitNumber;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/LOBCd");
                        if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.LOB))
                        {
                            //oPolicySystemInterface = new PolicySystemInterface(connectionString);
                            //sPSMappedCode = oPolicySystemInterface.GetPSMappedCodesFromRMXCodeId(oEnquireRequest.LOB, "POLICY_CLAIM_LOB");
                            //if (string.IsNullOrEmpty(sPSMappedCode))
                            //{
                            //    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetUnitListAcordRequest.NoMappedCodeFoundError"));
                            //}
                            //oElement.Value = (new LocalCache(connectionString)).GetShortCode(Conversion.CastToType<int>(sPSMappedCode, out bResult));
                            oElement.Value = oPolicyEnquiry.LOB;
                            oElement = null;
                            oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_BaseLOBLine");
                            if (oElement != null)
                            {

                                sParentLobCode = GetPolicyLOBRelatedShortCode(oPolicyEnquiry.LOB, "POLICY_CLAIM_LOB", ref p_objErrOut);

                                if (oElement != null && !string.IsNullOrEmpty(sParentLobCode))
                                {
                                    oElement.Value = sParentLobCode;
                                    oElement = null;
                                }
                            }
                        }
                        oElement = oTemplate.XPathSelectElement("./SignonRq/ClientApp/Version");

                        if (oElement != null)
                        {
                            if (oElement.Value.Contains("PIJ") && sParentLobCode.CompareTo("WL") == 0)
                            {
                                oElement = null;
                                {
                                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/RqUID");

                                    if (oElement != null)
                                    {
                                        sTemp = ReplaceTemplateforPIJ(oElement.Value);
                                        oElement.Value = sTemp;
                                        oElement = null;
                                    }

                                }

                            }

                        }
                    }

                    //skhare7 for PIJ



                }
                else if (string.Equals(sMode, "SAVEUNIT", StringComparison.InvariantCultureIgnoreCase))
                {
                    oSaveOptions = (SaveDownloadOptions)oRequest;
                    PopulateCommonAcordRequestParameters(ref oTemplate, oSaveOptions.PolicySystemId, ref p_objErrOut);
                    XElement objDoc = XElement.Parse(sRequestXML);
                    // XElement objDoc = XElement.Parse(sRequestXML);
                    //  string sLOB = string.Empty;
                    // PopulateCommonAcordRequestParameters(ref oTemplate, oPolicySave.PolicySystemId, ref p_objErrOut);
                    // if (!string.IsNullOrEmpty(oPolicySave.PolicyIdentfier) && (oPolicySave.PolicySystemId > 0))
                    //  {
                    oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                    if (oElement != null)
                    {
                        oElement.Value = oSaveOptions.Token;
                        oElement = null;
                    }
                    objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                    objPolicySaveAccord = XElement.Parse(objManager.GetDownLoadXMLData("POLICY", oSaveOptions.PolicyId, oSaveOptions.PolicyId));

                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/LOBCd");

                    oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/LOBCd");
                    if (oPolicySaveNode != null && !string.IsNullOrEmpty(oPolicySaveNode.Value))
                    {
                        //oPolicySystemInterface = new PolicySystemInterface(connectionString);
                        //sPSMappedCode = oPolicySystemInterface.GetPSMappedCodesFromRMXCodeId(oPolicySave.LOB, "POLICY_CLAIM_LOB");
                        //if (string.IsNullOrEmpty(sPSMappedCode))
                        //{
                        //    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetUnitListAcordRequest.NoMappedCodeFoundError"));
                        //}
                        //oElement.Value = (new LocalCache(connectionString)).GetShortCode(Conversion.CastToType<int>(sPSMappedCode, out bResult));
                        if (oElement != null)
                        {
                            oElement.Value = oPolicySaveNode.Value;
                            oElement = null;
                        }
                        oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_BaseLOBLine");

                        sParentLobCode = GetPolicyLOBRelatedShortCode(oPolicySaveNode.Value, "POLICY_CLAIM_LOB", ref p_objErrOut);

                        if (oElement != null && !string.IsNullOrEmpty(sParentLobCode))
                        {
                            oElement.Value = sParentLobCode;
                            oElement = null;
                        }
                    }



                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/PolicyNumber");
                    if (oElement != null)
                    {
                        oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/PolicySummaryInfo/PolicyNumber");
                        if (oPolicySaveNode != null)
                        {
                            oElement.Value = oPolicySaveNode.Value;
                            oElement = null;
                            oPolicySaveNode = null;
                        }
                    }

                    //dbisht6 jira rma-369
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/LossDt");
                    if (oElement != null)
                    {
                        oElement.Value = oSaveOptions.LossDate;
                        oElement = null;

                    }
                    //jira rma-369 end

                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/CompanyProductCd");
                    if (oElement != null)
                    {
                        oPolicySaveNode = objPolicySaveAccord.XPathSelectElement("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/CompanyProductCd");
                        if (oPolicySaveNode != null)
                        {
                            oElement.Value = oPolicySaveNode.Value;
                            oElement = null;
                            oPolicySaveNode = null;
                        }
                    }

                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId");


                    if (oElement != null)
                    {

                        objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                        foreach (XElement objElement in objElements)
                        {
                            XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                            XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                            if (objEle != null && string.Equals(objEle.Value, "com.csc_MasterCompany", StringComparison.InvariantCultureIgnoreCase))
                            {
                                oElement.Value = oSubElement.Value;
                            }

                            objEle = null;
                            oSubElement = null;
                        }
                        objElements = null;

                    }


                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                    if (oElement != null)
                    {
                        objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                        foreach (XElement objElement in objElements)
                        {
                            XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                            XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                            if (objEle != null && string.Equals(objEle.Value, "com.csc_LocCompany", StringComparison.InvariantCultureIgnoreCase))
                            {
                                oElement.Value = oSubElement.Value;
                            }

                            objEle = null;
                            oSubElement = null;
                        }
                        objElements = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId");
                    if (oElement != null)
                    {
                        objElements = objPolicySaveAccord.XPathSelectElements("./ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_ItemIdInfo/OtherIdentifier");

                        foreach (XElement objElement in objElements)
                        {
                            XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                            XElement oSubElement = objElement.XPathSelectElement("./OtherId");

                            if (objEle != null && string.Equals(objEle.Value, "com.csc_Module", StringComparison.InvariantCultureIgnoreCase))
                            {
                                oElement.Value = oSubElement.Value;
                            }

                            objEle = null;
                            oSubElement = null;
                        }
                        objElements = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitInterestListRq/com.csc_UnitInterestSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId");
                    if (oElement != null)
                    {
                        if (objDoc.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId") != null)
                        {
                            oElement.Value = objDoc.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitFetchRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_UnitNumber']/OtherId").Value;
                            oElement = null;
                        }
                    }


                }

                if (string.IsNullOrEmpty(oTemplate.ToString()))
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicyInterestListRequest.RequestOperationError", base.ClientId));
                }
                else
                    oAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
            }

            return oAcordRequest;
        }

        public void WriteUnitInterestListToFile(string sData, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sFileName = "UnitInterestList_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml";
            try
            {
                DeleteFile(sFileName);
                if (!string.IsNullOrEmpty(sData))
                    WriteFormattedXmlToFile(sFileName, "<Entities>" + sData + "</Entities>", ref p_objErrOut);
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);


            }
        }

        public void WriteCovListToFile(string sData, ref BusinessAdaptorErrors p_objErrOut)
        {
            string sFileName = "CoverageList_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml";
            string sFileContent = string.Empty;
            try
            {
                DeleteFile(sFileName);
                WriteFormattedXmlToFile(sFileName, "<Coverage>" + sData + "</Coverage>", ref p_objErrOut);
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);


            }
        }

        public string GetUnitInterestListFormattedResult(string oAcordResponse, string sStatUnitNo, string sUnitNo, int iUnitRowId, ref BusinessAdaptorErrors p_objErrOut, int iPolicySystemId, ref int iSequenceNumber)
        {
            XElement oDataModelTemplate = null;
            XElement oSaveAcordXML = null;
            XElement oElement = null;
            XElement oElementNode = null;
            XElement oDataModelElement = null;
            string sEntityBusinessRole = string.Empty;
            string sExculdeEntities = string.Empty;
            StringBuilder objBuilder = null;
            IEnumerable<XElement> oElements = null;
            //int iSequenceNumber = 1;
            string sRoleCode = string.Empty;
            // LocalCache objCache = null;
            string sReturn = string.Empty;
            PolicySystemInterface objManager = null;
            try
            {
                oSaveAcordXML = XElement.Parse(oAcordResponse);
                //DeleteFile(sFileName);
                if (oSaveAcordXML != null)
                {
                    objBuilder = new StringBuilder();
                    oElements = oSaveAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitInterestListRs/InsuredOrPrincipal");
                    if ((oElements != null) && (oElements.Count() > 0))
                    {
                        // objCache = new LocalCache(m_connectionString);
                        objManager = new PolicySystemInterface(m_connectionString, base.ClientId);
                        sEntityBusinessRole = objManager.GetPolicySystemParamteres("BUSINESS_ROLES", iPolicySystemId).Replace(" ", "");
                        sExculdeEntities = objManager.GetPolicySystemParamteres("EXCLUDE_INTEREST", iPolicySystemId).Replace(" ", "");

                        foreach (XElement objELe in oElements)
                        {
                            oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("UnitInterestList", ref p_objErrOut));
                            if (oDataModelTemplate != null)
                            {
                                oElementNode = objELe.XPathSelectElement("./GeneralPartyInfo/TaxIdentity");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/TaxId");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }


                                oElementNode = objELe.XPathSelectElement("./InsuredOrPrincipalInfo/InsuredOrPrincipalRoleCd");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/RoleCd");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;

                                    sRoleCode = oElementNode.Value;
                                }

                                if (!string.IsNullOrEmpty(sExculdeEntities) && !string.IsNullOrEmpty(sRoleCode) && sExculdeEntities.Split(',').Contains(sRoleCode))
                                {
                                    continue;
                                }

                                oElementNode = objELe.XPathSelectElement("./GeneralPartyInfo/NameInfo/CommlName/CommercialName");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    //string[] arrName = oElementNode.Value.Split(' ');
                                    //if (arrName.Length > 0)
                                    //{
                                    //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                    //    if (oDataModelElement != null)
                                    //        oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(arrName[arrName.Length - 1]));

                                    //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                    //    if (oDataModelElement != null)
                                    //        oDataModelElement.Value = arrName[arrName.Length - 1];

                                    //}
                                    //else
                                    //{
                                    //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                    //    if (oDataModelElement != null)
                                    //        oDataModelElement.Value = oElementNode.Value;
                                    //}
                                    if (!string.IsNullOrEmpty(sEntityBusinessRole) && !string.IsNullOrEmpty(sRoleCode) && sEntityBusinessRole.Split(',').Contains(sRoleCode))
                                    {
                                        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                        if (oDataModelElement != null)
                                            oDataModelElement.Value = oElementNode.Value;
                                    }
                                    else
                                    {
                                        string[] arrName = oElementNode.Value.Split(' ');

                                        if (arrName.Length > 3)
                                        {
                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(" "));

                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = arrName[1];

                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = oElementNode.Value.Substring(oElementNode.Value.IndexOf(arrName[1]) + arrName[1].Length);
                                        }
                                        else if (arrName.Length > 1)
                                        {
                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(" "));

                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = oElementNode.Value.Substring(oElementNode.Value.IndexOf(" ") + 1);

                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = "";
                                        }
                                        else
                                        {
                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = "";

                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/MiddleName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = "";

                                            oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                            if (oDataModelElement != null)
                                                oDataModelElement.Value = oElementNode.Value;
                                        }
                                    }
                                }

                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/SequenceNumber");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = iSequenceNumber.ToString();

                                // oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_UnitCoverageDetailRq/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");

                                oElementNode = objELe.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Location']/OtherId");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/Location");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }


                                oElementNode = objELe.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_SeqNum']/OtherId");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/PointSeqNumber");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/StatUnitNumber");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = sStatUnitNo;
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/UnitNumber");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = sUnitNo;
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/PolicyUnitRowid");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = iUnitRowId.ToString();

                                // mits 35925 start
                                oElementNode = objELe.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_AddressSeqNum']/OtherId");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/AddressSequenceNumber");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;

                                }
                                oElementNode = objELe.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_ClientSeqNum']/OtherId");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/ClientSequenceNumber");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;



                                }




                                //mits 35925 end


                                iSequenceNumber++;
                                //else
                                //{
                                //    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetEntitySaveFormattedResult.NoTaxIdFound"));
                                //}

                                //oElementNode = oElementNode.Parent.XPathSelectElement("OtherId");
                                //if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                //{
                                //    string[] arrName = oElementNode.Value.Split(' ');
                                //    if (arrName.Length > 0)
                                //    {
                                //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                //        if (oDataModelElement != null)
                                //            oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(arrName[arrName.Length - 1]));

                                //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                //        if (oDataModelElement != null)
                                //            oDataModelElement.Value = arrName[arrName.Length - 1];

                                //    }
                                //    else
                                //    {
                                //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                //        if (oDataModelElement != null)
                                //            oDataModelElement.Value = oElementNode.Value;
                                //    }
                                //}

                                objBuilder.Append(oDataModelTemplate.ToString());

                                //oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/com.csc_TaxId");

                                //if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                //{

                                //    oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/TaxId");
                                //    if (oDataModelElement != null)
                                //        oDataModelElement.Value = oElementNode.Value;
                                //}
                                //else
                                //{
                                //    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetEntitySaveFormattedResult.NoTaxIdFound"));
                                //}

                                //oElementNode = oElement.XPathSelectElement("./com.csc_PolicyLevel/com.csc_LongName");
                                //if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                //{
                                //    string[] arrName = oElementNode.Value.Split(' ');
                                //    if (arrName.Length > 0)
                                //    {
                                //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/FirstName");
                                //        if (oDataModelElement != null)
                                //            oDataModelElement.Value = oElementNode.Value.Substring(0, oElementNode.Value.IndexOf(arrName[arrName.Length - 1]));

                                //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                //        if (oDataModelElement != null)
                                //            oDataModelElement.Value = arrName[arrName.Length - 1];

                                //    }
                                //    else
                                //    {
                                //        oDataModelElement = oDataModelTemplate.XPathSelectElement("./Entity/LastName");
                                //        if (oDataModelElement != null)
                                //            oDataModelElement.Value = oElementNode.Value;
                                //    }
                                //}
                                //else
                                //{
                                //    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetEntitySaveFormattedResult.NoLastNameFound"));
                                //}
                                //   objBuilder.Append(oDataModelTemplate.ToString());


                            }
                        }
                        sReturn = objBuilder.ToString();
                        //WriteFormattedXmlToFile(sFileName, "<Entities>" + objBuilder.ToString() + "</Entities>", ref p_objErrOut);
                    }
                }

            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);


            }
            finally
            {
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElement != null)
                    oElement = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
                if (objBuilder != null)
                    objBuilder = null;
                //if (objCache != null)
                //{
                //    objCache.Dispose();
                //    objCache = null;
                //}
                objManager = null;
            }

            return sReturn;
        }

        public void GetPolicyUnitInterestList(PolicyEnquiry oRequest, out PolicyEnquiry oResponse, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicySystemInterface oPolicySystemInterface = null;
            string sResult = string.Empty, sXSLTPath = string.Empty;
            int iUnitRowId = 0;
            iUnitRowId = oRequest.UnitRowId;
            oResponse = new PolicyEnquiry();
            try
            {

                oPolicySystemInterface = new PolicySystemInterface(connectionString, base.ClientId);
                sResult = oPolicySystemInterface.GetPolicyUnitInterestList(oRequest.UnitRowId);

                oResponse.ResponseAcordXML = sResult;

            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);

            }
            finally
            {
                if (oPolicySystemInterface != null)
                    oPolicySystemInterface = null;
            }
        }

        public string GetWLCoverageList(string oAcordResponse, string sStatUnitNo, string sUnitNo, int iUnitRowId, ref BusinessAdaptorErrors p_objErrOut, int iPolicySystemId, ref int iSequenceNumber, string sAcordFileNm, string sUnitDesc)
        {
            XElement oDataModelTemplate = null;
            XElement oSaveAcordXML = null;

            XElement oElementNode = null;
            XElement oDataModelElement = null;
            string sEntityBusinessRole = string.Empty;

            StringBuilder objBuilder = null;
            IEnumerable<XElement> oElements = null;

            string sRoleCode = string.Empty;
            string sExcludeCVGs = string.Empty;
            string sReturn = string.Empty, sBaseLOBLine = string.Empty;
            string sCvgCode = string.Empty, sCvgText = string.Empty;
            PolicySystemInterface objManager = null;

            LocalCache objCache = null;
            try
            {
                oSaveAcordXML = XElement.Parse(oAcordResponse);
                objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                sExcludeCVGs = objManager.GetPolicySystemParamteres("EXCLUDE_MAJOR_PERIL", iPolicySystemId);
                if (oSaveAcordXML != null)
                {
                    oElementNode = oSaveAcordXML.XPathSelectElement("./ClaimsSvcRs/com.csc_UnitCoverageListRs/Policy/com.csc_BaseLOBLine");
                    if (oElementNode != null)
                        sBaseLOBLine = oElementNode.Value;
                    objBuilder = new StringBuilder();
                    oElements = oSaveAcordXML.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitCoverageListRs/com.csc_CoverageLossInfo");
                    if ((oElements != null) && (oElements.Count() > 0))
                    {
                        foreach (XElement objEles in oElements)
                        {
                            oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("CoverageList", ref p_objErrOut));
                            if (oDataModelTemplate != null)
                            {
                                oElementNode = objEles.XPathSelectElement("./Coverage/CoverageCd");

                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CovCode");
                                    if (oDataModelElement != null)
                                    {
                                        oDataModelElement.Value = oElementNode.Value;
                                    }
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/com.csc_Exposure");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Exposure");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/EffectiveDt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/EffectiveDate");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/ExpirationDt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ExpirationDate");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/Deductible/FormatCurrencyAmt/Amt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/SelfInsureDeduct");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/CurrentTermAmt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/FullTermPremium");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/com.csc_TotalPremium");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/TotalWrittenPremium");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/com.csc_OriginalPremium");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/OrginialPremium");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/com.csc_WrittenPremium");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/WrittenPremium");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CoverageSeq']/OtherId");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CvgSequenceNo");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_TrxnSeq']/OtherId");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/TransSequenceNo");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/Limit[LimitAppliesToCd='OccurenceLimit']/FormatCurrencyAmt/Amt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/OccurrenceLimit");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./Coverage/Limit[LimitAppliesToCd='AggregateLimit']/FormatCurrencyAmt/Amt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyLimit");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                objCache = new LocalCache(m_connectionString, base.ClientId);

                                string sClassCode = string.Empty;

                                oElementNode = objEles.XPathSelectElement("./com.csc_ClassCd");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    sClassCode = oElementNode.Value;
                                }
                                string sClassDesc = string.Empty;
                                oElementNode = null;
                                oElementNode = objEles.XPathSelectElement("./com.csc_ClassDesc");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    sClassDesc = oElementNode.Value;
                                }
                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ClassData");
                                if (oDataModelElement != null)
                                {
                                    oDataModelElement.Value = sClassCode + " " + sClassDesc;
                                }

                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyId");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = "0";

                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyUnitRowId");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = iUnitRowId.ToString();

                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/BaseLob");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = sBaseLOBLine.Trim();

                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/SequenceNumber");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = iSequenceNumber.ToString();

                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/FileNm");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = sAcordFileNm;

                                oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/UnitDesc");
                                if (oDataModelElement != null)
                                    oDataModelElement.Value = sStatUnitNo + " " + sUnitDesc;

                                oElementNode = objEles.XPathSelectElement("./Coverage/CoverageDesc");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CovDesc");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }
                                oElementNode = objEles.XPathSelectElement("./Coverage/ExcessWorkCompDeductible/FormatCurrencyAmt/Amt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/WcDedAmt");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }
                                oElementNode = objEles.XPathSelectElement("./Coverage/ExcessWorkCompDeductible/com.csc_AggrAmt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/WcDedAggr");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./com.csc_ProductLine");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ProductLine");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./com.csc_AnnualStatement");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/AnnualStmt");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                //MITS:33573 starts
                                oElementNode = objEles.XPathSelectElement("./com.csc_RetroDt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/RetroDate");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }

                                oElementNode = objEles.XPathSelectElement("./com.csc_ExtendDt");
                                if (oElementNode != null && !string.IsNullOrEmpty(oElementNode.Value))
                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/TailDate");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElementNode.Value;
                                }
                                //MITS:33573 ends

                                iSequenceNumber++;
                                objBuilder.Append(oDataModelTemplate.ToString());
                            }
                        }
                        sReturn = objBuilder.ToString();
                    }
                }
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (oSaveAcordXML != null)
                    oSaveAcordXML = null;
                if (oElementNode != null)
                    oElementNode = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (oDataModelElement != null)
                    oDataModelElement = null;
                if (objBuilder != null)
                    objBuilder = null;
                objManager = null;
            }
            return sReturn;
        }

        public string GetAcordWLCoverageList(string oAcordResponse, ref BusinessAdaptorErrors p_objErrOut, string p_StatUnitNo)
        {
            string sFileNm = string.Empty;

            try
            {
                sFileNm = "CoverageListAcord_" + p_StatUnitNo + "_" + userLogin.LoginName + "_" + base.GetSessionObject().SessionId + "_tmp.xml";

                WriteFormattedXmlToFile(sFileNm, oAcordResponse, ref p_objErrOut);
            }
            catch (Exception ex)
            {
                p_objErrOut.Add(ex, BusinessAdaptorErrorType.Error);
            }
            return sFileNm;
        }

        public void SaveCoveragesForWC(int p_iPolicyId, string sSelectedValue, string sFileContent, ref BusinessAdaptorErrors p_objErrOut, string sLossDate)
        {
            PolicySystemInterface objManager = null;

            int iRecordId, iPolicyUnitId, p_iPolicySystemID;

            string sBaseLOBLine;
            XElement objTemp, objDoc, oTempValueEle, objEles = null, oDataModelTemplate = null;
            XmlNodeList oElements;
            XmlDocument objDocument = new XmlDocument();
            XmlNode oTempNode, oFileNmNode, oCovNode;
            IEnumerable<XElement> oListElements;

            string sAcordFileNm = string.Empty;
            string sAcordFileContent = string.Empty;

            string sFilePath = string.Empty;
            string sCvgText = string.Empty;
            string sCovSeqAcord = string.Empty, sCovSeqFile;
            XElement oDataModelElement = null;
            string sCvgCode = string.Empty, sExcludeCVGs = string.Empty;
            LocalCache objCache = null;
            ArrayList objLimitList = null;
            try
            {
                objManager = new PolicySystemInterface(m_userLogin, base.ClientId);
                //rupal:start, omig
                string sCoverageScriptFilePath = Path.Combine(RMConfigurator.UserDataPath, @"PolicyInterface\Scripts\PolicyDownload.vb");
                if (File.Exists(sCoverageScriptFilePath))
                    objManager.ScriptFileExists = true;
                else
                    objManager.ScriptFileExists = false;
                //rupal:end,omig
                objLimitList = new ArrayList();
                p_iPolicySystemID = objManager.GetPolicySystemId(p_iPolicyId);
                objCache = new LocalCache(m_connectionString, base.ClientId);
                sExcludeCVGs = objManager.GetPolicySystemParamteres("EXCLUDE_MAJOR_PERIL", p_iPolicySystemID);
                objDocument.LoadXml(sFileContent);
                if (objDocument != null)
                {
                    oElements = objDocument.SelectNodes("//Instance/PolicyXCvgType");
                    foreach (XmlNode oElement in oElements)
                    {
                        if (objLimitList.Count > 0)
                        {
                            objLimitList.Clear();
                        }
                        
                        oTempNode = oElement.SelectSingleNode("./SequenceNumber");
                        if (oTempNode != null && string.Equals(oTempNode.InnerText, sSelectedValue, StringComparison.InvariantCultureIgnoreCase))
                        {
                            oFileNmNode = oElement.SelectSingleNode("./FileNm");
                            sAcordFileNm = oFileNmNode.InnerText;
                            if (File.Exists(RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + sAcordFileNm))
                            {
                                sFilePath = RMConfigurator.BasePath + "\\temp\\PolicyInterface\\" + sAcordFileNm;
                                sAcordFileContent = File.ReadAllText(sFilePath);
                            }
                            oCovNode = oElement.SelectSingleNode("./CvgSequenceNo");
                            sCovSeqFile = oCovNode.InnerText;

                            XmlNode oPolicyUnitID = oElement.SelectSingleNode("./PolicyUnitRowId");// RMA-8764, MITS 37908: aaggarwal29
                            iPolicyUnitId = Conversion.ConvertStrToInteger(oPolicyUnitID.InnerText);

                            //XmlNode oCoverageText = oElement.SelectSingleNode("./CoverageText");
                            //sCvgText = oCoverageText.InnerText;


                            XmlNode oBaseLOB = oElement.SelectSingleNode("./BaseLob");
                            sBaseLOBLine = oBaseLOB.InnerText;

                            // get the required coveragelossinfo node from acord reponse
                            objDoc = XElement.Parse(sAcordFileContent);
                            oListElements = objDoc.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitCoverageListRs/com.csc_CoverageLossInfo");
                            foreach (XElement oListElement in oListElements)
                            {
                                oTempValueEle = oListElement.XPathSelectElement("./ItemIdInfo/OtherIdentifier[OtherIdTypeCd = 'com.csc_CoverageSeq']/OtherId");
                                if (oTempValueEle != null)
                                    sCovSeqAcord = oTempValueEle.Value.Trim();
                                if (sCovSeqFile == sCovSeqAcord)
                                {
                                    objEles = oListElement;
                                    break;
                                }
                            }


                            oDataModelTemplate = XElement.Parse(GetXMLTemplateByType("Coverage", ref p_objErrOut));
                            if (oDataModelTemplate != null)
                            {

                                {
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageTypeCode");
                                    if (oDataModelElement != null)
                                    {
                                        if (!string.IsNullOrEmpty(oElement.SelectSingleNode("./CovCode").InnerText) && !string.IsNullOrEmpty(sExcludeCVGs) && sExcludeCVGs.Split(',').Contains(oElement.SelectSingleNode("//CovCode").InnerText))
                                            continue;

                                        oDataModelElement.Attribute("codeid").Value = objManager.GetRMXCodeIdFromPSMappedCode(oElement.SelectSingleNode("./CovCode").InnerText, "COVERAGE_TYPE", "CoverageType on SaveCoverages", p_iPolicySystemID.ToString()).ToString();
                                        sCvgCode = oDataModelElement.Attribute("codeid").Value;

                                    }

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/Exposure");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElement.SelectSingleNode("./Exposure").InnerText;

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/EffectiveDate");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElement.SelectSingleNode("./EffectiveDate").InnerText;

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ExpirationDate");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElement.SelectSingleNode("./ExpirationDate").InnerText;

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/SelfInsureDeduct");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElement.SelectSingleNode("./SelfInsureDeduct").InnerText;

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/FullTermPremium");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElement.SelectSingleNode("./FullTermPremium").InnerText;

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/TotalWrittenPremium");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElement.SelectSingleNode("./TotalWrittenPremium").InnerText;

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/OrginialPremium");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElement.SelectSingleNode("./OrginialPremium").InnerText;

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/WrittenPremium");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElement.SelectSingleNode("./WrittenPremium").InnerText;

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CvgSequenceNo");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElement.SelectSingleNode("./CvgSequenceNo").InnerText;

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/TransSequenceNo");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElement.SelectSingleNode("./TransSequenceNo").InnerText;

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/OccurrenceLimit");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElement.SelectSingleNode("./OccurrenceLimit").InnerText;

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyLimit");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElement.SelectSingleNode("./PolicyLimit").InnerText;

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CoverageText");
                                    if (oDataModelElement != null)
                                    {
                                        oDataModelElement.Value = objCache.GetShortCode(Conversion.ConvertStrToInteger(sCvgCode)) + " " + oElement.SelectSingleNode("./ClassData").InnerText;
                                        sCvgText = oDataModelElement.Value;
                                    }

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/CvgClassCode");
                                    if (oDataModelElement != null)
                                    {
                                        oDataModelElement.Value = oElement.SelectSingleNode("./ClassData").InnerText.Substring(0, oElement.SelectSingleNode("./ClassData").InnerText.IndexOf(' '));
                                    }

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyId");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = "0";

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/PolicyUnitRowId");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElement.SelectSingleNode("./PolicyUnitRowId").InnerText;

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/WcDedAmt");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElement.SelectSingleNode("./WcDedAmt").InnerText;

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/WcDedAggr");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElement.SelectSingleNode("./WcDedAggr").InnerText;

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/AnnualStmt");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElement.SelectSingleNode("./AnnualStmt").InnerText;

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/ProductLine");
                                    if (oDataModelElement != null)
                                        oDataModelElement.Value = oElement.SelectSingleNode("./ProductLine").InnerText;

                                    //MITS:33573 starts
                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/RetroDate");
                                    if (oDataModelElement != null)
                                        if (oElement.SelectSingleNode("./RetroDate") != null)
                                            oDataModelElement.Value = oElement.SelectSingleNode("./RetroDate").InnerText;

                                    oDataModelElement = oDataModelTemplate.XPathSelectElement("./PolicyXCvgType/TailDate");
                                    if (oDataModelElement != null)
                                        if (oElement.SelectSingleNode("./TailDate") != null)
                                            oDataModelElement.Value = oElement.SelectSingleNode("./TailDate").InnerText;
                                    //MITS:33573 ends
                                }

                            }

                            int iPolicyCVGRowId = objManager.CheckCoverageDuplication(iPolicyUnitId, objEles.ToString(), sBaseLOBLine, p_iPolicySystemID, sCvgText);
                            objTemp = XElement.Parse(sAcordFileContent);
                            IEnumerable<XElement> objTempEls = objTemp.XPathSelectElements("./ClaimsSvcRs/com.csc_UnitCoverageListRs/com.csc_CoverageLossInfo");
                            for (int i = objTempEls.Count() - 1; i >= 0; i--)
                            {
                                objTempEls.ElementAt(i).Remove();
                            }
                            objTemp.XPathSelectElement("./ClaimsSvcRs/com.csc_UnitCoverageListRs").Add(objEles);
                            iRecordId = objManager.SaveCoverage(oDataModelTemplate.ToString(), iPolicyCVGRowId, sLossDate, objTemp.ToString(),p_iPolicySystemID, objLimitList);
                           // bool bResult = objManager.SavePolicyDownloadXMLData(iRecordId, objTemp.ToString(), p_iPolicySystemID, "POLICY_X_CVG_TYPE");
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                p_objErrOut.Add(ex, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                if (objManager != null)
                    objManager = null;
                if (objDocument != null)
                    objDocument = null;
                if (oDataModelTemplate != null)
                    oDataModelTemplate = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (oDataModelElement != null)
                    oDataModelElement = null;
            }


        }

        public bool DeleteTempCoverageFiles(ref BusinessAdaptorErrors p_objErrOut, string sFileContent)
        {
            PolicySystemInterface oPolicySystemInterface = null;
            XElement oDoc = null;
            bool bResult = false;
            string sFileList = string.Empty;
            try
            {
                oPolicySystemInterface = new PolicySystemInterface(m_userLogin, base.ClientId);
                if (!(sFileContent.Trim() == string.Empty))
                {
                    oDoc = XElement.Parse(sFileContent);
                    IEnumerable<XElement> oFiles = oDoc.XPathSelectElements("//FileNm");
                    foreach (XElement oEle in oFiles)
                    {
                        sFileList = sFileList + oEle.Value + ",";
                    }
                    oPolicySystemInterface.DeleteTempCoverageFiles(sFileList);
                }
                bResult = true;
            }
            catch (Exception e)
            {
                bResult = false;
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                oPolicySystemInterface = null;
            }
            return bResult;
        }

        public void AddAditionalData(PolicySaveRequest oRequest, PolicySaveRequest oResponse, ref string oAcordSaveResponse)
        {
            XmlDocument objDocument = null;
            XmlNode objNode = null;
            try
            {
                objDocument = new XmlDocument();
                oResponse.InsurerAddr1 = oRequest.InsurerAddr1;
                oResponse.InsurerNm = oRequest.InsurerNm;
                oResponse.InsurerCity = oRequest.InsurerCity;
                oResponse.InsurerPostalCd = oRequest.InsurerPostalCd;
                oResponse.TaxId = oRequest.TaxId;
                oResponse.ClaimBasedFlag = oRequest.ClaimBasedFlag;//Ashish Ahuja
                oResponse.PolicySearchByDate = oRequest.PolicySearchByDate;//JIRA 1342 ajohari2
                objDocument.LoadXml(oAcordSaveResponse);
                objNode = objDocument.SelectSingleNode("/ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/ItemIdInfo/com.csc_ClientSeqNum");
                if (objNode != null)
                {
                    objNode.InnerText = oRequest.ClientSeqNo;
                }
                objNode = objDocument.SelectSingleNode("/ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/ItemIdInfo/com.csc_AddressSeqNum");
                if (objNode != null)
                {
                    objNode.InnerText = oRequest.AddressSeqNo;
                }
                objNode = objDocument.SelectSingleNode("/ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/TaxIdentity");
                if (objNode != null)
                {
                    objNode.InnerText = oRequest.TaxId;
                }
                objNode = objDocument.SelectSingleNode("/ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/BirthDt");
                if (objNode != null)
                {
                    objNode.InnerText = oRequest.BirthDt;
                }
                objNode = objDocument.SelectSingleNode("/ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_Insurer/GeneralPartyInfo/Addr/Addr1");
                if (objNode != null)
                {
                    objNode.InnerText = oRequest.InsurerAddr1;
                }
                objNode = objDocument.SelectSingleNode("/ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_Insurer/GeneralPartyInfo/Addr/City");
                if (objNode != null)
                {
                    objNode.InnerText = oRequest.InsurerCity;
                }
                objNode = objDocument.SelectSingleNode("/ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_Insurer/GeneralPartyInfo/Addr/PostalCode");
                if (objNode != null)
                {
                    objNode.InnerText = oRequest.InsurerPostalCd;
                }
                objNode = objDocument.SelectSingleNode("/ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/com.csc_Insurer/GeneralPartyInfo/NameInfo/CommlName/CommercialName");
                if (objNode != null)
                {
                    objNode.InnerText = oRequest.InsurerNm;
                }

                objNode = objDocument.SelectSingleNode("/ACORD/ClaimsSvcRs/com.csc_PolicyInquiryRs/com.csc_DetailInfo/com.csc_PolicyLevel/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/NameType");
                if (objNode != null)
                {
                    objNode.InnerText = oRequest.NameType;
                }

                oAcordSaveResponse = objDocument.InnerXml;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDocument = null;
                objNode = null;
            }
        }


        #region method not required now
        /*
        private void PopulateCLUnitMappingValues(ref XElement oTemplate, DataTable oDataTable)
        {
            XElement oElementNode = null;
            string sVehicleMappingValues = oDataTable.Rows[0]["CL_VEHICLE_UNIT_MAPPING"].ToString();
            string sPropertyMappingValues = oDataTable.Rows[0]["CL_PROPERTY_UNIT_MAPPING"].ToString();

            oElementNode = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_VehicleUnitMapping']/OtherId");
            if (oElementNode != null)
            {
                oElementNode.Value = sVehicleMappingValues;
            }

            oElementNode = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyUnitListRq/com.csc_PolicyUnitSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_PropertyUnitMapping']/OtherId");
            if (oElementNode != null)
            {
                oElementNode.Value = sPropertyMappingValues;
            }
            oElementNode = null;
        } */
        #endregion

        public void AddAditionalData(PolicySaveRequest oRequest, PolicySaveRequest oResponse)
        {
            try
            {
                oResponse.InsurerAddr1 = oRequest.InsurerAddr1;
                oResponse.InsurerNm = oRequest.InsurerNm;
                oResponse.InsurerCity = oRequest.InsurerCity;
                oResponse.InsurerPostalCd = oRequest.InsurerPostalCd;
                oResponse.TaxId = oRequest.TaxId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// tanwar2 - mits 30910
        /// </summary>
        /// <returns></returns>
        public string GetDimEvalDate(PolicyEnquiry oPolicyEnquiry, ref BusinessAdaptorErrors p_objErrOut)
        {
            string oAcordRequest = string.Empty;
            XElement oElement = null;
            XElement oTemplate = GetDimEvalTemplate();

            //IEnumerable<XElement> oElements = null;
            try
            {
                PopulateCommonAcordRequestParameters(ref oTemplate, oPolicyEnquiry.PolicySystemId, ref p_objErrOut);

                if (oPolicyEnquiry.PolicySystemId > 0)
                {
                    oElement = oTemplate.XPathSelectElement("./SignonRq/com.csc_SessKey");
                    if (oElement != null)
                    {
                        oElement.Value = oPolicyEnquiry.Token;
                        oElement = null;
                    }

                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyDimEvalDateRq/com.csc_PolicyDimEvalDateSearch/Policy/PolicyNumber");
                    if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolicyNumber))
                    {
                        oElement.Value = oPolicyEnquiry.PolicyNumber;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyDimEvalDateRq/com.csc_PolicyDimEvalDateSearch/Policy/CompanyProductCd");
                    if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.PolicySymbol))
                    {
                        oElement.Value = oPolicyEnquiry.PolicySymbol;
                        oElement = null;
                    }

                    //tanwar2 - JIRA RMA 4824 - Code Review - start
                    //oElements = oTemplate.XPathSelectElements("./ClaimsSvcRq/com.csc_PolicyDimEvalDateRq/com.csc_PolicyDimEvalDateSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier");
                    //if (oElements != null)
                    //{
                    //    foreach (XElement objElement in oElements)
                    //    {
                    //        XElement objEle = objElement.XPathSelectElement("./OtherIdTypeCd");
                    //        XElement oSubElement = objElement.XPathSelectElement("./OtherId");
                    //        //Start -  Changed by Nikhil.Code review changes
                    //        //if (objEle != null && string.Equals(objEle.Value, "com.csc_MasterCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicyEnquiry.MasterCompany))
                    //        if (objEle != null && string.Compare(objEle.Value.ToString(), "com.csc_MasterCompany", true) == 0 && !string.IsNullOrEmpty(oPolicyEnquiry.MasterCompany))
                    //        //End -  Changed by Nikhil.Code review changes
                    //        {
                    //            oSubElement.Value = oPolicyEnquiry.MasterCompany;
                    //        }
                    //        //Start -  Changed by Nikhil.Code review changes.
                    //        //else if (objEle != null && string.Equals(objEle.Value, "com.csc_LocCompany", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicyEnquiry.Location))
                    //        else if (objEle != null && string.Compare(objEle.Value.ToString(), "com.csc_LocCompany", true) == 0 && !string.IsNullOrEmpty(oPolicyEnquiry.Location))
                    //        //END -  Changed by Nikhil.Code review changes.
                    //        {
                    //            oSubElement.Value = oPolicyEnquiry.Location;
                    //        }
                    //        //Start -  Changed by Nikhil.Code review changes.
                    //        //else if (objEle != null && string.Equals(objEle.Value, "com.csc_Module", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicyEnquiry.Module))
                    //        else if (objEle != null && string.Compare(objEle.Value.ToString(), "com.csc_Module", true) == 0 && !string.IsNullOrEmpty(oPolicyEnquiry.Module))
                    //        //End -  Changed by Nikhil.Code review changes.
                    //        {
                    //            oSubElement.Value = oPolicyEnquiry.Module;
                    //        }
                    //        //Start -  Changed by Nikhil.Code review changes.
                    //        // else if (objEle != null && string.Equals(objEle.Value, "com.csc_StatUnitNumber", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicyEnquiry.Module))
                    //        else if (objEle != null && string.Compare(objEle.Value.ToString(), "com.csc_StatUnitNumber", true) == 0 && !string.IsNullOrEmpty(oPolicyEnquiry.Module))
                    //        //End -  Changed by Nikhil.Code review changes.
                    //        {
                    //            oSubElement.Value = oPolicyEnquiry.UnitNumber;
                    //        }
                    //        //Start -  Changed by Nikhil.Code review changes.
                    //        //else if (objEle != null && string.Equals(objEle.Value, "com.csc_CvgSequenceNo", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicyEnquiry.Module))
                    //        else if (objEle != null && string.Compare(objEle.Value.ToString(), "com.csc_CvgSequenceNo", true) == 0 && !string.IsNullOrEmpty(oPolicyEnquiry.Module))
                    //        //End -  Changed by Nikhil.Code review changes.
                    //        {
                    //            oSubElement.Value = oPolicyEnquiry.CovSeqNo;
                    //        }
                    //        //Start -  Changed by Nikhil.Code review changes.        
                    //        //else if (objEle != null && string.Equals(objEle.Value, "com.csc_TransSequenceNo", StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(oPolicyEnquiry.Module))
                    //        else if (objEle != null && string.Compare(objEle.Value.ToString(), "com.csc_TransSequenceNo", true) == 0 && !string.IsNullOrEmpty(oPolicyEnquiry.Module))
                    //        //END -  Changed by Nikhil.Code review changes.        
                    //        {
                    //            oSubElement.Value = oPolicyEnquiry.TransSeq;
                    //        }
                    //        objEle = null;
                    //        oSubElement = null;
                    //    }
                    //}

                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyDimEvalDateRq/com.csc_PolicyDimEvalDateSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_MasterCompany']/OtherId");
                    if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.MasterCompany))
                    {
                        oElement.Value = oPolicyEnquiry.MasterCompany;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyDimEvalDateRq/com.csc_PolicyDimEvalDateSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_LocCompany']/OtherId");
                    if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.Location))
                    {
                        oElement.Value = oPolicyEnquiry.Location;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyDimEvalDateRq/com.csc_PolicyDimEvalDateSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_Module']/OtherId");
                    if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.Module))
                    {
                        oElement.Value = oPolicyEnquiry.Module;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyDimEvalDateRq/com.csc_PolicyDimEvalDateSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_StatUnitNumber']/OtherId");
                    if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.UnitNumber))
                    {
                        oElement.Value = oPolicyEnquiry.UnitNumber;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyDimEvalDateRq/com.csc_PolicyDimEvalDateSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_CvgSequenceNo']/OtherId");
                    if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.CovSeqNo))
                    {
                        oElement.Value = oPolicyEnquiry.CovSeqNo;
                        oElement = null;
                    }
                    oElement = oTemplate.XPathSelectElement("./ClaimsSvcRq/com.csc_PolicyDimEvalDateRq/com.csc_PolicyDimEvalDateSearch/Policy/com.csc_ItemIdInfo/OtherIdentifier[OtherIdTypeCd='com.csc_TransSequenceNo']/OtherId");
                    if (oElement != null && !string.IsNullOrEmpty(oPolicyEnquiry.TransSeq))
                    {
                        oElement.Value = oPolicyEnquiry.TransSeq;
                        oElement = null;
                    }
                    //tanwar2 - JIRA RMA 4824 - Code Review - start
                }

                if (string.IsNullOrEmpty(oTemplate.ToString()))
                {
                    throw new RMAppException(Globalization.GetString("PolicySystemInterfaceAdaptor.GetPolicyInquireAcordRequest.RequestOperationError", base.ClientId));
                }
                else
                    oAcordRequest = oTemplate.ToString();
            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);
                oTemplate = null;
            }
            finally
            {
                if (oElement != null)
                    oElement = null;
                if (oTemplate != null)
                    oTemplate = null;
            }
            return oAcordRequest;
        }
        # region AcordTemplates
        private XElement GetPolicySearchAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <ACORD>
	                <SignonRq>
		                <SignonPswd>
			                <CustId>
				                <SPName/>
				                <CustLoginId></CustLoginId>
			                </CustId>
			                <CustPswd>
				                <EncryptionTypeCd/>
				                <Pswd/>
			                </CustPswd>
		                </SignonPswd>
		                <ClientDt/>
		                <CustLangPref/>
		                <ClientApp>
			                <Org/>
			                <Name></Name>
			                <Version/>
                            <com.csc_ClientFile/>
		                </ClientApp>
		                <com.csc_SessKey></com.csc_SessKey>
	                </SignonRq>
	                <ClaimsSvcRq>
		                <RqUID>RMJDBC_PolicyListInquiryRq</RqUID>
		                <com.csc_PolicyListInquiryRq>
			                <RqUID/>
			                <TransactionRequestDt/>
			                <CurCd/>
			                <com.csc_SearchInfo>
				                <com.csc_RoutingInfo></com.csc_RoutingInfo>
				                <com.csc_SearchCriteria>
					                <com.csc_PolicyLevel>
						                <PolicySummaryInfo>
							                <PolicyNumber></PolicyNumber>
                                            <PolicyStatusCd></PolicyStatusCd>
                                            <com.csc_BaseLOBLine></com.csc_BaseLOBLine>
                                            <LOBCd></LOBCd>
                                            <GroupId></GroupId>
						                </PolicySummaryInfo>
						                <StateProvCd/>
                                        <CompanyProductCd></CompanyProductCd>
						                <NAICCd/>
						                <LossDt></LossDt>
                                        <ReportedDt></ReportedDt>
						                <ActionCd></ActionCd>
                                        <ContractNumber></ContractNumber>
						                <com.csc_ItemIdInfo>
							                <OtherIdentifier>
								                <OtherIdTypeCd>com.csc_PolHolderRoles</OtherIdTypeCd>
								                <OtherId>AIN,ANI,NIN</OtherId>
							                </OtherIdentifier>
							                <OtherIdentifier>
								                <OtherIdTypeCd>com.csc_ClaimTypeCd</OtherIdTypeCd>
								                <OtherId/>
							                </OtherIdentifier>
							                <OtherIdentifier>
								                <OtherIdTypeCd>com.csc_ClaimSubType</OtherIdTypeCd>
								                <OtherId/>
							                </OtherIdentifier>
                                            <OtherIdentifier>
								                <OtherIdTypeCd>com.csc_LocCompany</OtherIdTypeCd>
								                <OtherId/>
							                </OtherIdentifier>
                                            <OtherIdentifier>
								                <OtherIdTypeCd>com.csc_MasterCompany</OtherIdTypeCd>
								                <OtherId/>
							                </OtherIdentifier>
                                            <OtherIdentifier>
								                <OtherIdTypeCd>com.csc_Module</OtherIdTypeCd>
								                <OtherId/>
							                </OtherIdentifier>
                                            <!--MITS:33371 ajohari2:Start-->
                                            <OtherIdentifier>
								                <OtherIdTypeCd>com.csc_CombinedPolicyFilter</OtherIdTypeCd>
								                <OtherId/>
							                </OtherIdentifier>
                                            <!--MITS:33371 ajohari2:End-->
						                </com.csc_ItemIdInfo>
                                        <InsuredOrPrincipal>
                                            <ItemIdInfo>
                                                <AgencyId></AgencyId>
                                                <CustPermId></CustPermId>
                                            </ItemIdInfo>
                                            <GeneralPartyInfo>
                                                <NameInfo>
                                                    <CommlName>
                                                        <CommercialName></CommercialName>
                                                    </CommlName>
                                                    <LegalEntityCd></LegalEntityCd>
                                                    <TaxIdentity></TaxIdentity>
                                                </NameInfo>
                                                <Addr>
                                                    <AddrTypeCd>MailingAddress</AddrTypeCd>
                                                    <Addr1></Addr1>
                                                    <City></City>
                                                    <StateProvCd></StateProvCd>
                                                    <PostalCode></PostalCode>
                                                    <County></County>
                                                </Addr>
                                            </GeneralPartyInfo>
                                            <InsuredOrPrincipalInfo>
                                            <InsuredOrPrincipalRoleCd>Insured</InsuredOrPrincipalRoleCd>
                                                <BusinessInfo>
                                                    <SICCd></SICCd>
                                                </BusinessInfo>
                                            </InsuredOrPrincipalInfo>
                                        </InsuredOrPrincipal>
					                </com.csc_PolicyLevel>
				                </com.csc_SearchCriteria>
			                </com.csc_SearchInfo>
		                </com.csc_PolicyListInquiryRq>
	                </ClaimsSvcRq>
                </ACORD>");

            return oTemplate;
        }

        private XElement GetPolicyInquireAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <ACORD>
	                <SignonRq>
		                <SignonPswd>
			                <CustId>
				                <SPName/>
				                <CustLoginId></CustLoginId>
			                </CustId>
			                <CustPswd>
				                <EncryptionTypeCd/>
				                <Pswd/>
			                </CustPswd>
		                </SignonPswd>
		                <ClientDt/>
		                <CustLangPref/>
		                <ClientApp>
			                <Org/>
			                <Name></Name>
			                <Version/>
                            <com.csc_ClientFile/>
		                </ClientApp>
		                <com.csc_SessKey></com.csc_SessKey>
	                </SignonRq>
	                <ClaimsSvcRq>
		                <RqUID>RMBM_PolicyInquireRq</RqUID>
		                <com.csc_PolicyInquiryRq>
			                <RqUID/>
			                <TransactionRequestDt/>
			                <CurCd/>
			                <com.csc_SearchInfo>
				                <com.csc_RoutingInfo/>
				                <com.csc_SearchCriteria>
					                <com.csc_PolicyLevel>
						                <PolicySummaryInfo>
							                <PolicyNumber></PolicyNumber>
                                            <PolicyStatusCd></PolicyStatusCd>
                                               <com.csc_BaseLOBLine></com.csc_BaseLOBLine>
                                            <LOBCd></LOBCd>
                                            <GroupId></GroupId>
                                            <ContractTerm>
                                                <EffectiveDt></EffectiveDt>
                                                <ExpirationDt></ExpirationDt>
                                            </ContractTerm>
						                </PolicySummaryInfo>
						                <StateProvCd/>
                                        <CompanyProductCd></CompanyProductCd>
						                <ActionCd></ActionCd>
                                        <ContractNumber></ContractNumber>
						                <com.csc_ItemIdInfo>
							                <OtherIdentifier>
								                <OtherIdTypeCd>com.csc_PolHolderRoles</OtherIdTypeCd>
								                <OtherId>AIN,ANI,NIN</OtherId>
							                </OtherIdentifier>
							                <OtherIdentifier>
								                <OtherIdTypeCd>com.csc_ClaimTypeCd</OtherIdTypeCd>
								                <OtherId/>
							                </OtherIdentifier>
							                <OtherIdentifier>
								                <OtherIdTypeCd>com.csc_ClaimSubType</OtherIdTypeCd>
								                <OtherId/>
							                </OtherIdentifier>
                                            <OtherIdentifier>
								                <OtherIdTypeCd>com.csc_LocCompany</OtherIdTypeCd>
								                <OtherId/>
							                </OtherIdentifier>
                                            <OtherIdentifier>
								                <OtherIdTypeCd>com.csc_MasterCompany</OtherIdTypeCd>
								                <OtherId/>
							                </OtherIdentifier>
                                            <OtherIdentifier>
								                <OtherIdTypeCd>com.csc_Module</OtherIdTypeCd>
								                <OtherId/>
							                </OtherIdentifier>
						                </com.csc_ItemIdInfo>
                                        <com.csc_Insurer>
                                          <GeneralPartyInfo>
                                            <NameInfo>
                                              <CommlName>
                                                <CommercialName></CommercialName>
                                              </CommlName>
                                              <TaxIdentity/>
                                            </NameInfo>
                                            <Addr>
                                              <AddrTypeCd>MailingAddress</AddrTypeCd>
                                              <Addr1></Addr1>
                                              <City></City>
                                              <StateProvCd/>
                                              <PostalCode></PostalCode>
                                              <County/>
                                            </Addr>
                                          </GeneralPartyInfo>
                                        </com.csc_Insurer>
					                </com.csc_PolicyLevel>
				                </com.csc_SearchCriteria>
			                </com.csc_SearchInfo>
		                </com.csc_PolicyInquiryRq>
	                </ClaimsSvcRq>
                </ACORD>");

            return oTemplate;
        }

        private XElement GetUnitListAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <ACORD>
	                <SignonRq>
		                <SignonPswd>
			                <CustId>
				                <SPName/>
				                <CustLoginId></CustLoginId>
			                </CustId>
			                <CustPswd>
				                <EncryptionTypeCd/>
				                <Pswd/>
			                </CustPswd>
		                </SignonPswd>
		                <ClientDt/>
		                <CustLangPref/>
		                <ClientApp>
			                <Org/>
			                <Name></Name>
			                <Version/>
                            <com.csc_ClientFile/>
		                </ClientApp>
		                <com.csc_SessKey></com.csc_SessKey>
	                </SignonRq>
	                <ClaimsSvcRq>
		                <RqUID>RMJDBC_PolicyUnitListRq</RqUID>
		                <com.csc_PolicyUnitListRq>
			                <RqUID/>
			                <TransactionRequestDt/>
			                <CurCd/>
			                <com.csc_PagingInfo>
				                <Status>
					                <StatusCd/>
				                </Status>
				                <com.csc_RoutingInfo/>
			                </com.csc_PagingInfo>
			                <com.csc_PolicyUnitSearch>
				                <Policy id=''>
                                    <PolicyNumber></PolicyNumber>
					                <CompanyProductCd></CompanyProductCd>
                                    <com.csc_BaseLOBLine></com.csc_BaseLOBLine>
					                <LOBCd></LOBCd>
					                <NAICCd/>
                                    <LossDt></LossDt>
					                <com.csc_ActionCd></com.csc_ActionCd>
                                    <com.csc_IssueCd></com.csc_IssueCd>
					                <com.csc_ItemIdInfo>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_LocCompany</OtherIdTypeCd>
								            <OtherId/>
							            </OtherIdentifier>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_MasterCompany</OtherIdTypeCd>
								            <OtherId/>
							            </OtherIdentifier>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_Module</OtherIdTypeCd>
								            <OtherId/>
							            </OtherIdentifier>
						            </com.csc_ItemIdInfo>
				                </Policy>
			                </com.csc_PolicyUnitSearch>
			                <Location>
				                <ItemIdInfo/>
				                <Addr>
					                <StateProvCd/>
					                <PostalCode/>
				                </Addr>
			                </Location>
		                </com.csc_PolicyUnitListRq>
	                </ClaimsSvcRq>
                </ACORD>");
            return oTemplate;
        }

        private XElement GetUnitDetailAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <ACORD>
	                <SignonRq>
		                <SignonPswd>
			                <CustId>
				                <SPName/>
				                <CustLoginId></CustLoginId>
			                </CustId>
			                <CustPswd>
				                <EncryptionTypeCd/>
				                <Pswd/>
			                </CustPswd>
		                </SignonPswd>
		                <ClientDt/>
		                <CustLangPref/>
		                <ClientApp>
			                <Org/>
			                <Name></Name>
			                <Version/>
                            <com.csc_ClientFile/>
		                </ClientApp>
		                <com.csc_SessKey></com.csc_SessKey>
	                </SignonRq>
	                <ClaimsSvcRq>
		                <RqUID>RMBM_PolicyUnitFetchRq</RqUID>
		                <com.csc_PolicyUnitFetchRq>
			                <RqUID/>
			                <TransactionRequestDt/>
			                <CurCd/>
			                <Policy id=''>
				                <PolicyNumber></PolicyNumber>
					            <CompanyProductCd></CompanyProductCd>
                                <com.csc_BaseLOBLine></com.csc_BaseLOBLine>
					            <LOBCd></LOBCd>
                                <StateProvCd></StateProvCd>
					            <NAICCd/>
					            <com.csc_ActionCd></com.csc_ActionCd>
                                <com.csc_IssueCd></com.csc_IssueCd>
				                <com.csc_ItemIdInfo>
                                    <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_LocCompany</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
							        <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_MasterCompany</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
							        <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_Module</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
					                <OtherIdentifier>
						                <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
						                <OtherId></OtherId>
					                </OtherIdentifier>
                                    <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_UnitNumber</OtherIdTypeCd>
                                        <OtherId/>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_StatUnitNumber</OtherIdTypeCd>
                                     <OtherId/>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                      <OtherIdTypeCd>com.csc_Product</OtherIdTypeCd>
                                      <OtherId/>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_RiskLoc</OtherIdTypeCd>
                                        <OtherId></OtherId>
                                    </OtherIdentifier>
                                     <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_RiskSubLoc</OtherIdTypeCd>
                                        <OtherId></OtherId>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                      <OtherIdTypeCd>com.csc_RateState</OtherIdTypeCd>
                                      <OtherId></OtherId>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                      <OtherIdTypeCd>com.csc_PolCompany</OtherIdTypeCd>
                                      <OtherId></OtherId>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                      <OtherIdTypeCd>com.csc_UnitTypeInd</OtherIdTypeCd>
                                      <OtherId></OtherId>
                                    </OtherIdentifier>
				                </com.csc_ItemIdInfo>
                                <com.csc_ContractTerm>
                                    <EffectiveDt></EffectiveDt>
                                    <ExpirationDt></ExpirationDt>
                                </com.csc_ContractTerm>
                                <com.csc_UnitStatus></com.csc_UnitStatus>
			                </Policy>
		                </com.csc_PolicyUnitFetchRq>
	                </ClaimsSvcRq>
                </ACORD>");
            return oTemplate;
        }
        //Ashish Ahuja - added node of reported date
        private XElement GetCoverageListAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <ACORD>
	                <SignonRq>
		                <SignonPswd>
			                <CustId>
				                <SPName/>
				                <CustLoginId></CustLoginId>
			                </CustId>
			                <CustPswd>
				                <EncryptionTypeCd/>
				                <Pswd/>
			                </CustPswd>
		                </SignonPswd>
		                <ClientDt/>
		                <CustLangPref/>
		                <ClientApp>
			                <Org/>
			                <Name></Name>
			                <Version/>
                            <com.csc_ClientFile/>
		                </ClientApp>
		                <com.csc_SessKey></com.csc_SessKey>
	                </SignonRq>
	                <ClaimsSvcRq>
		                <RqUID>RMJDBC_UnitCoverageListRq</RqUID>
		                <com.csc_UnitCoverageListRq>
			                <RqUID/>
			                <TransactionRequestDt/>
			                <CurCd/>
			                <Policy id=''>
				                <PolicyNumber></PolicyNumber>
					            <CompanyProductCd></CompanyProductCd>
                                <com.csc_BaseLOBLine></com.csc_BaseLOBLine>
					            <LOBCd></LOBCd>
                                <LossDt></LossDt>
                                <ReportedDt></ReportedDt>
					            <NAICCd/>
					            <com.csc_ActionCd></com.csc_ActionCd>
                                <com.csc_IssueCd></com.csc_IssueCd>
				                <com.csc_ItemIdInfo>
                                    <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_LocCompany</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
							        <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_MasterCompany</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
							        <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_Module</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
					                <OtherIdentifier>
						                <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
						                <OtherId></OtherId>
					                </OtherIdentifier>
                                    <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_UnitNumber</OtherIdTypeCd>
                                        <OtherId/>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                      <OtherIdTypeCd>com.csc_Product</OtherIdTypeCd>
                                      <OtherId/>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_RiskLoc</OtherIdTypeCd>
                                        <OtherId></OtherId>
                                    </OtherIdentifier>
                                     <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_RiskSubLoc</OtherIdTypeCd>
                                        <OtherId></OtherId>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                      <OtherIdTypeCd>com.csc_RateState</OtherIdTypeCd>
                                      <OtherId></OtherId>
                                    </OtherIdentifier>
				                </com.csc_ItemIdInfo>
                            </Policy>
		                </com.csc_UnitCoverageListRq>
	                </ClaimsSvcRq>
                </ACORD>");
            return oTemplate;
        }

        private XElement GetCoverageDetailAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <ACORD>
	                <SignonRq>
		                <SignonPswd>
			                <CustId>
				                <SPName/>
				                <CustLoginId></CustLoginId>
			                </CustId>
			                <CustPswd>
				                <EncryptionTypeCd/>
				                <Pswd/>
			                </CustPswd>
		                </SignonPswd>
		                <ClientDt/>
		                <CustLangPref/>
		                <ClientApp>
			                <Org/>
			                <Name></Name>
			                <Version/>
                            <com.csc_ClientFile/>
		                </ClientApp>
		                <com.csc_SessKey></com.csc_SessKey>
	                </SignonRq>
	                <ClaimsSvcRq>
		                <RqUID>RMBM_UnitCoverageDetailRq</RqUID>
		                <com.csc_UnitCoverageDetailRq>
			                <RqUID/>
			                <TransactionRequestDt/>
			                <CurCd/>
			                <Policy id=''>
				                <PolicyNumber></PolicyNumber>
					            <CompanyProductCd></CompanyProductCd>
                                   <com.csc_BaseLOBLine></com.csc_BaseLOBLine>
					            <LOBCd></LOBCd>
					            <NAICCd/>
					            <com.csc_ActionCd></com.csc_ActionCd>
                                <com.csc_IssueCd></com.csc_IssueCd>
				                <com.csc_ItemIdInfo>
                                    <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_LocCompany</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
							        <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_MasterCompany</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
							        <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_Module</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
					                <OtherIdentifier>
						                <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
						                <OtherId></OtherId>
					                </OtherIdentifier>
                                    <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_UnitNumber</OtherIdTypeCd>
                                        <OtherId/>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                      <OtherIdTypeCd>com.csc_CoverageCd</OtherIdTypeCd>
                                      <OtherId/>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                      <OtherIdTypeCd>com.csc_CoverageSeq</OtherIdTypeCd>
                                      <OtherId/>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_TransSeq</OtherIdTypeCd>
                                        <OtherId></OtherId>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                      <OtherIdTypeCd>com.csc_RateState</OtherIdTypeCd>
                                      <OtherId></OtherId>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                      <OtherIdTypeCd>com.csc_Product</OtherIdTypeCd>
                                      <OtherId/>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_RiskLoc</OtherIdTypeCd>
                                        <OtherId></OtherId>
                                    </OtherIdentifier>
                                     <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_RiskSubLoc</OtherIdTypeCd>
                                        <OtherId></OtherId>
                                    </OtherIdentifier>
                                     <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_CovStatus</OtherIdTypeCd>
                                        <OtherId></OtherId>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                      <OtherIdTypeCd>com.csc_PolCompany</OtherIdTypeCd>
                                      <OtherId></OtherId>
                                    </OtherIdentifier>
				                </com.csc_ItemIdInfo>
                            </Policy>
		                </com.csc_UnitCoverageDetailRq>
	                </ClaimsSvcRq>
                </ACORD>");
            return oTemplate;
        }

        private XElement GetPolicySaveAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <ACORD>
                  <SignonRq>
                    <SignonPswd>
                      <CustId>
                        <SPName/>
                        <CustLoginId></CustLoginId>
                      </CustId>
                      <CustPswd>
                        <EncryptionTypeCd/>
                        <Pswd/>
                      </CustPswd>
                    </SignonPswd>
                    <ClientDt/>
                    <CustLangPref/>
                    <ClientApp>
                      <Org/>
                      <Name></Name>
                      <Version/>
                        <com.csc_ClientFile/>
                    </ClientApp>
                    <com.csc_SessKey></com.csc_SessKey>
                  </SignonRq>
                  <ClaimsSvcRq>
                    <RqUID/>
                    <com.csc_PolicyListInquiryRq>
                      <RqUID/>
                      <TransactionRequestDt/>
                      <CurCd/>
                      <com.csc_SearchInfo>
                        <com.csc_RoutingInfo/>
                        <com.csc_SearchCriteria>
                          <com.csc_PolicyLevel>
                            <PolicySummaryInfo>
                              <PolicyNumber></PolicyNumber>
                              <PolicyStatusCd></PolicyStatusCd>
                            </PolicySummaryInfo>
                            <StateProvCd/>
                            <CompanyProductCd/>
                            <NAICCd/>
                            <com.csc_ItemIdInfo>
                              <OtherIdentifier>
                                <OtherIdTypeCd>com.csc_PolicyId</OtherIdTypeCd>
                                <OtherId></OtherId>
                              </OtherIdentifier>
                              <OtherIdentifier>
                                <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
                                <OtherId></OtherId>
                              </OtherIdentifier>
                            </com.csc_ItemIdInfo>
                          </com.csc_PolicyLevel>
                        </com.csc_SearchCriteria>
                      </com.csc_SearchInfo>
                    </com.csc_PolicyListInquiryRq>
                  </ClaimsSvcRq>
                </ACORD>");

            return oTemplate;
        }

        private XElement GetPolicyInterestListAcordTemplate() // XML Chnaged By Nitika Added Loss Dt for Co op
        {
            XElement oTemplate = XElement.Parse(@"
                <ACORD>
	                <SignonRq>
		                <SignonPswd>
			                <CustId>
				                <SPName/>
				                <CustLoginId></CustLoginId>
			                </CustId>
			                <CustPswd>
				                <EncryptionTypeCd/>
				                <Pswd/>
			                </CustPswd>
		                </SignonPswd>
		                <ClientDt/>
		                <CustLangPref/>
		                <ClientApp>
			                <Org/>
			                <Name></Name>
			                <Version/>
                            <com.csc_ClientFile/>
		                </ClientApp>
		                <com.csc_SessKey></com.csc_SessKey>
	                </SignonRq>
	                <ClaimsSvcRq>
		                <RqUID>RMJDBC_PolicyInterestListRq</RqUID>
		                <com.csc_PolicyInterestListRq>
			                <RqUID/>
			                <TransactionRequestDt/>
			                <CurCd/>
			                <com.csc_PagingInfo>
				                <Status>
					                <StatusCd/>
				                </Status>
				                <com.csc_RoutingInfo/>
			                </com.csc_PagingInfo>
			                <com.csc_PolicyInterestSearch>
				                <Policy id=''>
                                    <PolicyNumber></PolicyNumber>
					                <CompanyProductCd></CompanyProductCd>
                                    <com.csc_BaseLOBLine></com.csc_BaseLOBLine>
					                <LOBCd></LOBCd>
                                    <LossDt></LossDt>
					                <NAICCd/>
					                <com.csc_ActionCd></com.csc_ActionCd>
                                    <com.csc_IssueCd></com.csc_IssueCd>
					                <com.csc_ItemIdInfo>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_LocCompany</OtherIdTypeCd>
								            <OtherId/>
							            </OtherIdentifier>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_MasterCompany</OtherIdTypeCd>
								            <OtherId/>
							            </OtherIdentifier>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_Module</OtherIdTypeCd>
								            <OtherId/>
							            </OtherIdentifier>
						            </com.csc_ItemIdInfo>
				                </Policy>
			                </com.csc_PolicyInterestSearch>
			                <Location>
				                <ItemIdInfo/>
				                <Addr>
					                <StateProvCd/>
					                <PostalCode/>
				                </Addr>
			                </Location>
		                </com.csc_PolicyInterestListRq>
	                </ClaimsSvcRq>
                </ACORD>");

            return oTemplate;
        }

        private XElement GetDriverListAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <ACORD>
	                <SignonRq>
		                <SignonPswd>
			                <CustId>
				                <SPName/>
				                <CustLoginId></CustLoginId>
			                </CustId>
			                <CustPswd>
				                <EncryptionTypeCd/>
				                <Pswd/>
			                </CustPswd>
		                </SignonPswd>
		                <ClientDt/>
		                <CustLangPref/>
		                <ClientApp>
			                <Org/>
			                <Name></Name>
			                <Version/>
                            <com.csc_ClientFile/>
		                </ClientApp>
		                <com.csc_SessKey></com.csc_SessKey>
	                </SignonRq>
	                <ClaimsSvcRq>
		                <RqUID>RMJDBC_DriverListRq</RqUID>
		                <com.csc_DriverListRq>
			                <RqUID/>
			                <TransactionRequestDt/>
			                <CurCd/>
			                <com.csc_DriverSearch>
				                <Policy id=''>
                                    <PolicyNumber></PolicyNumber>
					                <CompanyProductCd></CompanyProductCd>
                                    <com.csc_BaseLOBLine></com.csc_BaseLOBLine>
					                <LOBCd></LOBCd>
					                <NAICCd/>
					                <com.csc_ActionCd></com.csc_ActionCd>
                                    <com.csc_IssueCd></com.csc_IssueCd>
					                <com.csc_ItemIdInfo>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_LocCompany</OtherIdTypeCd>
								            <OtherId/>
							            </OtherIdentifier>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_MasterCompany</OtherIdTypeCd>
								            <OtherId/>
							            </OtherIdentifier>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_Module</OtherIdTypeCd>
								            <OtherId/>
							            </OtherIdentifier>
						            </com.csc_ItemIdInfo>
				                </Policy>
			                </com.csc_DriverSearch>
			                <Location>
				                <ItemIdInfo/>
				                <Addr>
					                <StateProvCd/>
					                <PostalCode/>
				                </Addr>
			                </Location>
		                </com.csc_DriverListRq>
	                </ClaimsSvcRq>
                </ACORD>");

            return oTemplate;
        }

        private XElement GetDriverDetailAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <ACORD>
	                <SignonRq>
		                <SignonPswd>
			                <CustId>
				                <SPName/>
				                <CustLoginId></CustLoginId>
			                </CustId>
			                <CustPswd>
				                <EncryptionTypeCd/>
				                <Pswd/>
			                </CustPswd>
		                </SignonPswd>
		                <ClientDt/>
		                <CustLangPref/>
		                <ClientApp>
			                <Org/>
			                <Name></Name>
			                <Version/>
                            <com.csc_ClientFile/>
		                </ClientApp>
		                <com.csc_SessKey></com.csc_SessKey>
	                </SignonRq>
	                <ClaimsSvcRq>
		                <RqUID>RMBM_DriverDetailRq</RqUID>
		                <com.csc_DriverDetailRq>
			                <RqUID/>
			                <TransactionRequestDt/>
			                <CurCd/>
			                <com.csc_PagingInfo/>
				            <Policy id=''>
                                <PolicyNumber></PolicyNumber>
					            <CompanyProductCd></CompanyProductCd>
                                <com.csc_BaseLOBLine></com.csc_BaseLOBLine>
                                <LOBCd></LOBCd>
					            <NAICCd/>
					            <com.csc_ActionCd></com.csc_ActionCd>
                                <com.csc_IssueCd></com.csc_IssueCd>
					            <com.csc_ItemIdInfo>
							        <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_LocCompany</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
							        <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_MasterCompany</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
							        <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_Module</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
                                    <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_DriverId</OtherIdTypeCd>
                                        <OtherId/>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                      <OtherIdTypeCd>com.csc_Product</OtherIdTypeCd>
                                      <OtherId/>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                      <OtherIdTypeCd>com.csc_DriverState</OtherIdTypeCd>
                                      <OtherId></OtherId>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                      <OtherIdTypeCd>com.csc_PolCompany</OtherIdTypeCd>
                                      <OtherId></OtherId>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                      <OtherIdTypeCd>com.csc_DriverType</OtherIdTypeCd>
                                      <OtherId></OtherId>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                      <OtherIdTypeCd>com.csc_RiskLoc</OtherIdTypeCd>
                                      <OtherId></OtherId>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                      <OtherIdTypeCd>com.csc_RiskSubLoc</OtherIdTypeCd>
                                      <OtherId></OtherId>
                                    </OtherIdentifier>
						        </com.csc_ItemIdInfo>
                                <com.csc_DriverStatus></com.csc_DriverStatus>
				            </Policy>			                
		                </com.csc_DriverDetailRq>
	                </ClaimsSvcRq>
                </ACORD>");

            return oTemplate;
        }

        private XElement GetEndorsementDataAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <ACORD>
	                <SignonRq>
		                <SignonPswd>
			                <CustId>
				                <SPName/>
				                <CustLoginId></CustLoginId>
			                </CustId>
			                <CustPswd>
				                <EncryptionTypeCd/>
				                <Pswd/>
			                </CustPswd>
		                </SignonPswd>
		                <ClientDt/>
		                <CustLangPref/>
		                <ClientApp>
			                <Org/>
			                <Name></Name>
			                <Version/>
                            <com.csc_ClientFile/>
		                </ClientApp>
		                <com.csc_SessKey></com.csc_SessKey>
	                </SignonRq>
	                <ClaimsSvcRq>
		                <RqUID>RMJDBC_PolicyFormDataRq</RqUID>
		                <com.csc_PolicyFormDataRq>
			                <RqUID/>
			                <TransactionRequestDt/>
			                <CurCd/>
			                <com.csc_PagingInfo>
				                <Status>
					                <StatusCd/>
				                </Status>
				                <com.csc_RoutingInfo/>
			                </com.csc_PagingInfo>
				            <Policy id=''>
                                <PolicyNumber></PolicyNumber>
					            <CompanyProductCd></CompanyProductCd>
                                <com.csc_BaseLOBLine></com.csc_BaseLOBLine>
					            <LOBCd></LOBCd>
					            <NAICCd/>
					            <com.csc_ActionCd></com.csc_ActionCd>
                                <com.csc_IssueCd></com.csc_IssueCd>
					            <com.csc_ItemIdInfo>
							        <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_LocCompany</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
							        <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_MasterCompany</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
							        <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_Module</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
					                <OtherIdentifier>
						                <OtherIdTypeCd>com.csc_InsLineCd</OtherIdTypeCd>
						                <OtherId></OtherId>
					                </OtherIdentifier>
                                    <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_UnitNumber</OtherIdTypeCd>
                                        <OtherId/>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_RiskLoc</OtherIdTypeCd>
                                        <OtherId></OtherId>
                                    </OtherIdentifier>
                                        <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_RiskSubLoc</OtherIdTypeCd>
                                        <OtherId></OtherId>
                                    </OtherIdentifier>
                                     <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_UnitStateID</OtherIdTypeCd>
                                        <OtherId></OtherId>
                                    </OtherIdentifier>
						        </com.csc_ItemIdInfo>
				            </Policy>
		                </com.csc_PolicyFormDataRq>
	                </ClaimsSvcRq>
                </ACORD>");

            return oTemplate;
        }

        private XElement GetCPPUnitInsuranceLineAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <ACORD>
	                <SignonRq>
		                <SignonPswd>
			                <CustId>
				                <SPName/>
				                <CustLoginId></CustLoginId>
			                </CustId>
			                <CustPswd>
				                <EncryptionTypeCd/>
				                <Pswd/>
			                </CustPswd>
		                </SignonPswd>
		                <ClientDt/>
		                <CustLangPref/>
		                <ClientApp>
			                <Org/>
			                <Name></Name>
			                <Version/>
                            <com.csc_ClientFile/>
		                </ClientApp>
		                <com.csc_SessKey></com.csc_SessKey>
	                </SignonRq>
	                <ClaimsSvcRq>
		                <RqUID>RMJDBC_CPPUnitInsLineRq</RqUID>
		                <com.csc_CPPUnitInsLineRq>
			                <RqUID/>
			                <TransactionRequestDt/>
			                <CurCd/>
			                <Policy id=''>
				                <PolicyNumber></PolicyNumber>
					            <CompanyProductCd></CompanyProductCd>
				                <com.csc_ItemIdInfo>
                                    <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_UnitNumber</OtherIdTypeCd>
                                        <OtherId/>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_RiskLoc</OtherIdTypeCd>
                                        <OtherId></OtherId>
                                    </OtherIdentifier>
                                     <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_RiskSubLoc</OtherIdTypeCd>
                                        <OtherId></OtherId>
                                    </OtherIdentifier>
				                </com.csc_ItemIdInfo>
			                </Policy>
		                </com.csc_CPPUnitInsLineRq>
	                </ClaimsSvcRq>
                </ACORD>");
            return oTemplate;
        }

        private XElement GetUnitInterestListAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <ACORD>
	                <SignonRq>
		                <SignonPswd>
			                <CustId>
				                <SPName/>
				                <CustLoginId></CustLoginId>
			                </CustId>
			                <CustPswd>
				                <EncryptionTypeCd/>
				                <Pswd/>
			                </CustPswd>
		                </SignonPswd>
		                <ClientDt/>
		                <CustLangPref/>
		                <ClientApp>
			                <Org/>
			                <Name></Name>
			                <Version/>
                            <com.csc_ClientFile/>
		                </ClientApp>
		                <com.csc_SessKey></com.csc_SessKey>
	                </SignonRq>
	                <ClaimsSvcRq>
		                <RqUID>RMJDBC_UnitInterestListRq</RqUID>
		                <com.csc_UnitInterestListRq>
			                <RqUID/>
			                <TransactionRequestDt/>
			                <CurCd/>
			                <com.csc_PagingInfo>
				                <Status>
					                <StatusCd/>
				                </Status>
				                <com.csc_RoutingInfo/>
			                </com.csc_PagingInfo>
			                <com.csc_UnitInterestSearch>
				                <Policy id=''>
                                    <PolicyNumber></PolicyNumber>
					                <CompanyProductCd></CompanyProductCd>
                                    <com.csc_BaseLOBLine></com.csc_BaseLOBLine>
					                <LOBCd></LOBCd>
                                    <LossDt></LossDt>
					                <NAICCd/>
					                <com.csc_ActionCd></com.csc_ActionCd>
                                    <com.csc_IssueCd></com.csc_IssueCd>
					                <com.csc_ItemIdInfo>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_LocCompany</OtherIdTypeCd>
								            <OtherId/>
							            </OtherIdentifier>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_MasterCompany</OtherIdTypeCd>
								            <OtherId/>
							            </OtherIdentifier>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_Module</OtherIdTypeCd>
								            <OtherId/>
							            </OtherIdentifier>
                                        <OtherIdentifier>
                                            <OtherIdTypeCd>com.csc_UnitNumber</OtherIdTypeCd>
                                            <OtherId/>
                                        </OtherIdentifier>
						            </com.csc_ItemIdInfo>
				                </Policy>
			                </com.csc_UnitInterestSearch>
			                <Location>
				                <ItemIdInfo/>
				                <Addr>
					                <StateProvCd/>
					                <PostalCode/>
				                </Addr>
			                </Location>
		                </com.csc_UnitInterestListRq>
	                </ClaimsSvcRq>
                </ACORD>");

            return oTemplate;
        }

        private XElement GetPolicyInterestDetailAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <ACORD>
	                <SignonRq>
		                <SignonPswd>
			                <CustId>
				                <SPName/>
				                <CustLoginId></CustLoginId>
			                </CustId>
			                <CustPswd>
				                <EncryptionTypeCd/>
				                <Pswd/>
			                </CustPswd>
		                </SignonPswd>
		                <ClientDt/>
		                <CustLangPref/>
		                <ClientApp>
			                <Org/>
			                <Name></Name>
			                <Version/>
                            <com.csc_ClientFile></com.csc_ClientFile>
		                </ClientApp>
		                <com.csc_SessKey></com.csc_SessKey>
	                </SignonRq>
	                <ClaimsSvcRq>
		                <RqUID>RMBM_PolicyInterestDetailRq</RqUID>
		                <com.csc_PolicyInterestDetailRq>
			                <RqUID/>
			                <TransactionRequestDt/>
			                <CurCd/>
			                <com.csc_PagingInfo>
				                <Status>
					                <StatusCd/>
				                </Status>
				            <com.csc_RoutingInfo/>
			                </com.csc_PagingInfo>
				            <Policy id=''>
                                <PolicyNumber></PolicyNumber>
					            <CompanyProductCd></CompanyProductCd>
                                 <com.csc_BaseLOBLine></com.csc_BaseLOBLine>
					             <LOBCd></LOBCd>
					            <NAICCd/>
					            <com.csc_ActionCd></com.csc_ActionCd>
                                <com.csc_IssueCd></com.csc_IssueCd>
					            <com.csc_ItemIdInfo>
							        <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_LocCompany</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
							        <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_MasterCompany</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
							        <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_Module</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
                                    <OtherIdentifier>
								        <OtherIdTypeCd>com.csc_RoleCd</OtherIdTypeCd>
								        <OtherId/>
							        </OtherIdentifier>
                                    <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_Location</OtherIdTypeCd>
                                        <OtherId/>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
		                                <OtherIdTypeCd>com.csc_SeqNum</OtherIdTypeCd>
                                        <OtherId/>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_UnitNumber</OtherIdTypeCd>
                                        <OtherId/>
                                    </OtherIdentifier>

                                     <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_State</OtherIdTypeCd>
                                        <OtherId/>
                                    </OtherIdentifier>
                                    <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_RateState</OtherIdTypeCd>
                                        <OtherId/>
                                    </OtherIdentifier>
                                     <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_PolicyCompany</OtherIdTypeCd>
                                        <OtherId/>
                                    </OtherIdentifier>
                                     <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_ClientSeqNum</OtherIdTypeCd>
                                        <OtherId/>
                                    </OtherIdentifier>
                                       <OtherIdentifier>
                                        <OtherIdTypeCd>com.csc_AddressSeqNum</OtherIdTypeCd>
                                        <OtherId/>
                                    </OtherIdentifier>
                                    </com.csc_ItemIdInfo>
				            </Policy>			                
		                </com.csc_PolicyInterestDetailRq>
	                </ClaimsSvcRq>
                </ACORD>");

            return oTemplate;
        }
        //skhare7 Policy Interface
        //tanwar2 - mits 30910 - start
        private XElement GetDimEvalTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <ACORD>
	                <SignonRq>
		                <SignonPswd>
			                <CustId>
				                <SPName/>
				                <CustLoginId></CustLoginId>
			                </CustId>
			                <CustPswd>
				                <EncryptionTypeCd/>
				                <Pswd/>
			                </CustPswd>
		                </SignonPswd>
		                <ClientDt/>
		                <CustLangPref/>
		                <ClientApp>
			                <Org></Org>
			                <Name></Name>
			                <Version/>
                            <com.csc_ClientFile/>
		                </ClientApp>
		                <com.csc_SessKey></com.csc_SessKey>
	                </SignonRq>
	                <ClaimsSvcRq>
		                <RqUID>RMJDBC_PolicyDimEvalDateRq</RqUID>
		                <com.csc_PolicyDimEvalDateRq>
			                <RqUID/>
			                <TransactionRequestDt/>
			                <CurCd/>
			                <com.csc_PagingInfo>
				                <Status>
					                <StatusCd/>
				                </Status>
				                <com.csc_RoutingInfo/>
			                </com.csc_PagingInfo>
			                <com.csc_PolicyDimEvalDateSearch>
				                <Policy id=''>
                                    <PolicyNumber></PolicyNumber>
					                <CompanyProductCd></CompanyProductCd>   
                                    <LOBCd></LOBCd>                                
					                <com.csc_ItemIdInfo>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_LocCompany</OtherIdTypeCd>
								            <OtherId></OtherId>
							            </OtherIdentifier>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_MasterCompany</OtherIdTypeCd>
								             <OtherId></OtherId>
							            </OtherIdentifier>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_Module</OtherIdTypeCd>
								            <OtherId></OtherId>
							            </OtherIdentifier>
										<OtherIdentifier>
											<OtherIdTypeCd>com.csc_StatUnitNumber</OtherIdTypeCd>
											<OtherId></OtherId>
										</OtherIdentifier>
										<OtherIdentifier>
											<OtherIdTypeCd>com.csc_CvgSequenceNo</OtherIdTypeCd>
											<OtherId></OtherId>
										</OtherIdentifier>
										<OtherIdentifier>
											<OtherIdTypeCd>com.csc_TransSequenceNo</OtherIdTypeCd>
											<OtherId></OtherId>
										</OtherIdentifier>
						            </com.csc_ItemIdInfo>
				                </Policy>
			                </com.csc_PolicyDimEvalDateSearch>
			                <Location>
				                <ItemIdInfo/>
				                <Addr>
					                <StateProvCd/>
					                <PostalCode/>
				                </Addr>
			                </Location>
		                </com.csc_PolicyDimEvalDateRq>
	                </ClaimsSvcRq>
                </ACORD>");

            return oTemplate;
        }
        //tanwar2 - mits 30910 - end
        # endregion

        private string ReplaceTemplateforPIJ(string p_sTemp)
        {
            return p_sTemp = p_sTemp.Replace("RMJDBC", "RMJPIJ");


        }

        public void GetPolicyDetails(FetchPolicyDetails oRequest, out FetchPolicyDetails oResponse, ref BusinessAdaptorErrors p_objErrOut)
        {
            PolicySystemInterface oPolicySystemInterface = null;
            string sResult = string.Empty, sXSLTPath = string.Empty;
            XmlDocument objXmlDocument = null;
            XmlNode objNode = null;
            oResponse = new FetchPolicyDetails();
            try
            {

                oPolicySystemInterface = new PolicySystemInterface(connectionString, base.ClientId);
                sResult = oPolicySystemInterface.GetPolicyDetails(oRequest.PolicyID);
                objXmlDocument = new XmlDocument();
                objXmlDocument.LoadXml(sResult);

                objNode = objXmlDocument.SelectSingleNode("//FetchPolicyDetails/PolicyID");
                if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
                    oResponse.PolicyID = Conversion.ConvertStrToInteger(objNode.InnerText);

                objNode = objXmlDocument.SelectSingleNode("//FetchPolicyDetails/PolicyNumber");
                if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
                    oResponse.PolicyNumber = objNode.InnerText;

                objNode = objXmlDocument.SelectSingleNode("//FetchPolicyDetails/Module");
                if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
                    oResponse.Module = objNode.InnerText;


                objNode = objXmlDocument.SelectSingleNode("//FetchPolicyDetails/PolicySymbol");
                if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
                    oResponse.Symbol = objNode.InnerText;


                objNode = objXmlDocument.SelectSingleNode("//FetchPolicyDetails/MasterCompany");
                if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
                    oResponse.MasterCompany = objNode.InnerText;


                objNode = objXmlDocument.SelectSingleNode("//FetchPolicyDetails/LocationCompany");
                if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
                    oResponse.LocationCompany = objNode.InnerText;


                objNode = objXmlDocument.SelectSingleNode("//FetchPolicyDetails/PolSysUserName");
                if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
                    oResponse.PolSysUserName = objNode.InnerText;


                objNode = objXmlDocument.SelectSingleNode("//FetchPolicyDetails/PolSysPassword");
                if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
                    oResponse.PolSysPassword = objNode.InnerText;

                objNode = objXmlDocument.SelectSingleNode("//FetchPolicyDetails/PointURL");
                if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
                    oResponse.PointURL = objNode.InnerText;

            }
            catch (Exception e)
            {
                p_objErrOut.Add(e, BusinessAdaptorErrorType.Error);

            }
            finally
            {
                if (oPolicySystemInterface != null)
                    oPolicySystemInterface = null;
            }
        }
        //Start: MITS 31601: Neha Suresh Jain,02/27/2013, accord template to save policy change data
        /// <summary>
        /// Accord template for request
        /// </summary>
        /// <returns></returns>
        private XElement GetPolicyChangeDateListAcordTemplate()
        {
            XElement oTemplate = XElement.Parse(@"
                <ACORD>
	                <SignonRq>
		                <SignonPswd>
			                <CustId>
				                <SPName/>
				                <CustLoginId></CustLoginId>
			                </CustId>
			                <CustPswd>
				                <EncryptionTypeCd/>
				                <Pswd/>
			                </CustPswd>
		                </SignonPswd>
		                <ClientDt/>
		                <CustLangPref/>
		                <ClientApp>
			                <Org/>
			                <Name></Name>
			                <Version/>
                            <com.csc_ClientFile/>
		                </ClientApp>
		                <com.csc_SessKey></com.csc_SessKey>
	                </SignonRq>
	                <ClaimsSvcRq>
		                <RqUID>RMJDBC_PolicyChangeDateListRq</RqUID>
		                <com.csc_PolicyChangeDateListRq>
			                <RqUID/>
			                <TransactionRequestDt/>
			                <CurCd/>
			                <com.csc_PagingInfo>
				                <Status>
					                <StatusCd/>
				                </Status>
				                <com.csc_RoutingInfo/>
			                </com.csc_PagingInfo>
			                <com.csc_PolicyChangeDateSearch>
				                <Policy id=''>
                                    <PolicyNumber></PolicyNumber>
					                <CompanyProductCd></CompanyProductCd>                                                     
					                <com.csc_ItemIdInfo>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_LocCompany</OtherIdTypeCd>
								            <OtherId/>
							            </OtherIdentifier>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_MasterCompany</OtherIdTypeCd>
								            <OtherId/>
							            </OtherIdentifier>
							            <OtherIdentifier>
								            <OtherIdTypeCd>com.csc_Module</OtherIdTypeCd>
								            <OtherId/>
							            </OtherIdentifier>
						            </com.csc_ItemIdInfo>
				                </Policy>
			                </com.csc_PolicyChangeDateSearch>			                
		                </com.csc_PolicyChangeDateListRq>
	                </ClaimsSvcRq>
                </ACORD>");
            return oTemplate;
        }
        //End: MITS 31601


        /// <summary>
        /// Fetches Policy search system settings of utilities
        /// </summary>
        /// <param name="objXmlIn"></param>
        /// <param name="objXmlOut"></param>
        /// <param name="objErrOut"></param>
        /// <returns></returns>
        /// <Author>Added by PSHEKHAWAT on 12/12/12</Author>
        public bool GetPolicySearchSystem(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bRet = false;
            SysSettings objSettings;
            XmlNode oPolSearchSettings = null;
            try
            {
                objSettings = new SysSettings(connectionString,base.ClientId);

                oPolSearchSettings = p_objXmlOut.AppendChild(p_objXmlOut.CreateElement("PolicySearchSystem"));
                //oPolSearchSettings.InnerText = objSettings.PolicySearchSystemList.ToString();
                ((XmlElement)oPolSearchSettings).SetAttribute("NoRequiredField", objSettings.NoReqFieldsForPolicySearch.ToString());

                bRet = true;
            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
            }
            finally
            {
                objSettings = null;
            }
            return bRet;
        }

        public bool GetPolicyLimits(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bRet = false;
            GetPolicyLimit objManager = null;

           
            try
            {
                //p_objXmlOut = p_objXmlIn;
                objManager = new GetPolicyLimit(base.userLogin, base.ClientId);
                p_objXmlOut = objManager.GetPolicyLimits(p_objXmlIn);

            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
            }
            finally
            {
            }
            return bRet;
        }

        /// <summary>
        /// This functio is used to get default user pref for the grid
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErr"></param>
        /// <returns></returns>
        public bool RestoreDefaults(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErr)
        {
            string sPageName = "";
            GetPolicyLimit objGetPolicyLimit = null;
            string sGridId = "";
            string sUserPrefString = "";
            XmlElement oUserPrefElement;
            try
            {
                objGetPolicyLimit = new GetPolicyLimit(this.m_userLogin, base.ClientId);//rkaur27
                if (p_objXmlIn.SelectSingleNode("//PageName") != null)
                    sPageName = p_objXmlIn.SelectSingleNode("//PageName").InnerText.Trim();
                if (p_objXmlIn.SelectSingleNode("//GridId") != null)
                    sGridId = p_objXmlIn.SelectSingleNode("//GridId").InnerText.Trim();
                sUserPrefString = objGetPolicyLimit.RestoreDefaultUserPref(m_userLogin.UserId, sPageName, m_userLogin.DatabaseId, sGridId);
                oUserPrefElement = p_objXmlOut.CreateElement("UserPref");
                oUserPrefElement.InnerText = sUserPrefString;
                p_objXmlOut.AppendChild(oUserPrefElement);
            }
            catch (Exception p_objException)
            {
                p_objErr.Add(p_objException, Globalization.GetString("GridPreference.RestoreDefaults.Err", base.ClientId), BusinessAdaptorErrorType.Error);
                return false;
            }
            return true;
        }




        public bool GetErosionDetails(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut, ref BusinessAdaptorErrors p_objErrOut)
        {
            bool bRet = true;
            GetPolicyLimit objManager = null;

            
            try
            {
               // p_objXmlOut = p_objXmlIn;
                objManager = new GetPolicyLimit(base.userLogin, base.ClientId);
                p_objXmlOut = objManager.GetErorsionDetails(p_objXmlIn);                

            }
            catch (RMAppException objException)
            {
                p_objErrOut.Add(objException, BusinessAdaptorErrorType.Error);
            }
            finally
            {
            }
            return bRet;
        }
    }
}


