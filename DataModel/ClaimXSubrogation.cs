﻿
using System;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Summary of ClaimXSubrogation Class.
    /// </summary>
    [Riskmaster.DataModel.Summary("CLAIM_X_SUBRO", "SUBROGATION_ROW_ID", "StatusDate")]
    public class ClaimXSubrogation : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
														{"SubrogationRowId", "SUBROGATION_ROW_ID"},
														{"ClaimId", "CLAIM_ID"},
														{"SubTypeCode", "SUB_TYPE_CODE"},
                                                        {"SubSpecialistEid", "SUB_SPECIALIST_EID"},
														{"SubStatusCode", "SUB_STATUS_CODE"},
														{"SubAdversePartyEid", "SUB_ADVERSE_PARTY_EID"},
														{"SubAdverseInsCoEid", "SUB_ADVERSE_INS_CO_EID"},
														{"SubAdversePolicyNum", "SUB_ADVERSE_POLICY_NUM"},
														{"SubAdverseClaimNum", "SUB_ADVERSE_CLAIM_NUM"},
														{"SubAdvAdjusterEid", "SUB_ADV_ADJUSTER_EID"},
														{"Comments", "COMMENTS"},
														{"HTMLComments", "HTMLCOMMENTS"},
                                                        {"StatusDate", "STATUS_DATE"},
                                                        {"SubStatusDescCode", "SUB_STATUS_DESC_CODE"},
                                                        {"NbOfYears", "NB_OF_YEARS"},
                                                        {"StatuteLimitationDate", "STATUTE_LIMITATION_DATE"},
                                                        {"AdvPartiesCovLimit", "ADV_PARTIES_COV_LIMIT"},
                                                        {"AmountPaid", "AMOUNT_PAID"},
                                                        {"CovDedAmount", "COV_DED_AMOUNT"},
                                                        {"SettlementPercent", "SETTLEMENT_PERCENT"},
                                                        {"TotalToBeRecov", "TOTAL_TO_BE_RECOV"},
                                                        {"PotPayoutAmt", "POT_PAYOUT_AMT"},
                                                        {"PotDedAmt", "POT_DED_AMT"},
                                                        {"TotalPotRecov", "TOTAL_POT_RECOV"}, 
														{"AddedByUser","ADDED_BY_USER"},			
														{"UpdatedByUser","UPDATED_BY_USER"},
														{"DttmRcdAdded","DTTM_RCD_ADDED"},
														{"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
                                                        {"LegacyUniqueIdentifier","LEGACY_UNIQUE_IDENTIFIER"}
                                                  
		};
        public string LegacyUniqueIdentifier { get { return GetFieldString("LEGACY_UNIQUE_IDENTIFIER"); } set { SetField("LEGACY_UNIQUE_IDENTIFIER", value); } }
        public int SubrogationRowId { get { return GetFieldInt("SUBROGATION_ROW_ID"); } set { SetField("SUBROGATION_ROW_ID", value); } }
        public int ClaimId { get { return GetFieldInt("CLAIM_ID"); } set { SetField("CLAIM_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "SUBRO_TYPE")]
        public int SubTypeCode { get { return GetFieldInt("SUB_TYPE_CODE"); } set { SetField("SUB_TYPE_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Entity, "OTHER PEOPLE")]
        public int SubSpecialistEid { get { return GetFieldInt("SUB_SPECIALIST_EID"); } set { SetField("SUB_SPECIALIST_EID", value); } }        
        [ExtendedTypeAttribute(RMExtType.Code, "SUBRO_STATUS")]
        public int SubStatusCode { get { return GetFieldInt("SUB_STATUS_CODE"); } set { SetField("SUB_STATUS_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Entity, "OTHER PEOPLE")]
        public int SubAdversePartyEid { get { return GetFieldInt("SUB_ADVERSE_PARTY_EID"); } set { SetField("SUB_ADVERSE_PARTY_EID", value); } }
        [ExtendedTypeAttribute(RMExtType.Entity, "INSURERS")]
        public int SubAdverseInsCoEid { get { return GetFieldInt("SUB_ADVERSE_INS_CO_EID"); } set { SetField("SUB_ADVERSE_INS_CO_EID", value); } }
        public string SubAdversePolicyNum { get { return GetFieldString("SUB_ADVERSE_POLICY_NUM"); } set { SetField("SUB_ADVERSE_POLICY_NUM", value); } }
        public string SubAdverseClaimNum { get { return GetFieldString("SUB_ADVERSE_CLAIM_NUM"); } set { SetField("SUB_ADVERSE_CLAIM_NUM", value); } }
        [ExtendedTypeAttribute(RMExtType.Entity, "OTHER PEOPLE")]
        public int SubAdvAdjusterEid { get { return GetFieldInt("SUB_ADV_ADJUSTER_EID"); } set { SetField("SUB_ADV_ADJUSTER_EID", value); } }
        //public int SubAdvAdjusterEid { get { return GetFieldInt("SUB_ADV_ADJUSTER_EID"); } set { SetField("SUB_ADV_ADJUSTER_EID", value); } }
        public string Comments { get { return GetFieldString("COMMENTS"); } set { SetField("COMMENTS", value); } }
        public string HTMLComments { get { return GetFieldStringFallback("HTMLCOMMENTS", "COMMENTS"); } set { SetField("HTMLCOMMENTS", value); } }
        public string StatusDate { get { return GetFieldString("STATUS_DATE"); } set { SetField("STATUS_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        [ExtendedTypeAttribute(RMExtType.Code, "SUBRO_STATUS_DESC")]
        public int SubStatusDescCode { get { return GetFieldInt("SUB_STATUS_DESC_CODE"); } set { SetField("SUB_STATUS_DESC_CODE", value); } }
        public int NbOfYears { get { return GetFieldInt("NB_OF_YEARS"); } set { SetField("NB_OF_YEARS", value); } }
        public string StatuteLimitationDate { get { return GetFieldString("STATUTE_LIMITATION_DATE"); } set { SetField("STATUTE_LIMITATION_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        //public string AdvPartiesCovLimit { get { return GetFieldString("ADV_PARTIES_COV_LIMIT"); } set { SetField("ADV_PARTIES_COV_LIMIT", value); } }
        //Deb:Changed to below format because type for this field is float in database
        public double AdvPartiesCovLimit { get { return GetFieldDouble("ADV_PARTIES_COV_LIMIT"); } set { SetField("ADV_PARTIES_COV_LIMIT", value); } }
        public double AmountPaid { get { return GetFieldDouble("AMOUNT_PAID"); } set { SetField("AMOUNT_PAID", value); } }
        public double CovDedAmount { get { return GetFieldDouble("COV_DED_AMOUNT"); } set { SetField("COV_DED_AMOUNT", value); } }
        public double SettlementPercent { get { return GetFieldDouble("SETTLEMENT_PERCENT"); } set { SetField("SETTLEMENT_PERCENT", value); } }
        public double TotalToBeRecov { get { return GetFieldDouble("TOTAL_TO_BE_RECOV"); } set { SetField("TOTAL_TO_BE_RECOV", value); } }
        public double PotPayoutAmt { get { return GetFieldDouble("POT_PAYOUT_AMT"); } set { SetField("POT_PAYOUT_AMT", value); } }
        public double PotDedAmt { get { return GetFieldDouble("POT_DED_AMT"); } set { SetField("POT_DED_AMT", value); } }
        public double TotalPotRecov { get { return GetFieldDouble("TOTAL_POT_RECOV"); } set { SetField("TOTAL_POT_RECOV", value); } }

        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        #endregion

        #region Child Implementation
        private string[,] sChildren = { { "DemandOfferList", "DemandOfferList" } };
                                          //Rijul 340
                                      //Rijul 340 end

        // Lock Entity Tables
        // Also Initialize SimpleList with desired table, field details.
        override internal void OnChildInit(string childName, string childType)
        {
            //Do default per-child processing.
            base.OnChildInit(childName, childType);

            //Do any custom per-child processing.
            //Rijul 340
            //Rijul 340 end
        }
        //Handle adding the our key to any additions to the record collection properties.
        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            switch (childName)
            {
                case "DemandOfferList":
                    (itemValue as DemandOffer).ParentId = this.SubrogationRowId;
                    (itemValue as DemandOffer).ParentName = typeof(ClaimXSubrogation).Name;
                    break;
            }
            base.OnChildItemAdded(childName, itemValue);
        }

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
            //Rijul 340
            //Rijul 340 end
        }

        //Handle any sub-object(s) for which we need the key in our own table.
            // If Entity is involved we save this first (in case we need the EntityId.);
            //Rijul 340
            //Rijul 340 end

        //Handle child saves.  Set the current key into the children since this could be a new record.
        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            switch (childName)
            {
                case "DemandOfferList":
                    if (isNew)
                        foreach (DemandOffer item in (childValue as DemandOfferList))
                        {
                            item.ParentId = this.SubrogationRowId;
                            item.ParentName = typeof(ClaimXSubrogation).Name;
                        }
                    (childValue as IPersistence).Save();
                    break;
            }
            base.OnChildPostSave(childName, childValue, isNew);
        }

        //Protect Entities that should not be deleted.
        internal override void OnChildDelete(string childName, IDataModel childValue)
        {
            base.OnChildDelete(childName, childValue);
        }

        //Child Property Accessors
        public DemandOfferList DemandOfferList
        {
            get
            {
                DemandOfferList objList = base.m_Children["DemandOfferList"] as DemandOfferList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = DemandOffer.ParentKeyFieldName + "=" + this.KeyFieldValue + " AND " + DemandOffer.ParentNameFieldName + "='" + typeof(ClaimXSubrogation).Name + "'";
                return objList;
            }
        }

        //Rijul 340
        //Rijul 340 end
        #endregion

        internal ClaimXSubrogation(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }
        new private void Initialize()
        {

            // base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

            this.m_sTableName = "CLAIM_X_SUBRO";
            this.m_sKeyField = "SUBROGATION_ROW_ID";
            this.m_sFilterClause = "";

            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Add all object Children into the Children collection from our "sChildren" list.
            base.InitChildren(sChildren);
            this.m_sParentClassName = "Claim";
            base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

        }
        //Override Parent by Shivendu for MITS 9467
        public override IDataModel Parent
        {
            get
            {


                if (base.Parent == null)
                {
                    base.Parent = Context.Factory.GetDataModelObject("Claim", true);
                    m_isParentStale = false;
                    if (this.ClaimId != 0)
                        (base.Parent as DataObject).MoveTo(this.ClaimId);
                }

                return base.Parent;
            }
            set
            {
                base.Parent = value;
            }
        }
        #region Supplemental Fields Exposed
        //Supplementals are implemented in the base class but not exposed externally.
        //This allows the derived class to control whether the object appears to have
        //supplementals or not. 
        //Entity exposes Supplementals.
        internal override string OnBuildSuppTableName()
        {
            return "SUBROGATION_SUPP";
        }

        public new Supplementals Supplementals
        {
            get
            {
                return base.Supplementals;
            }
        }
        #endregion
        protected override void OnBuildNewUniqueId()
        {
            base.OnBuildNewUniqueId();
        }
    }
}