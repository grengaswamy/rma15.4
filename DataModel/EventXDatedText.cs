using System;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for EventXDatedText.
	/// </summary>
	[Riskmaster.DataModel.Summary("EVENT_X_DATED_TEXT","EV_DT_ROW_ID", "DateEntered")]
	public class EventXDatedText : DataObject
	{
		#region Database Field List
		private string[,] sFields = {
										{"EvDtRowId","EV_DT_ROW_ID"},
									   {"EventId","EVENT_ID"},
									   {"EnteredByUser","ENTERED_BY_USER"},
									   {"DateEntered","DATE_ENTERED"},
									   {"TimeEntered","TIME_ENTERED"},
									   {"DatedText","DATED_TEXT"},
									   {"DttmRcdAdded","DTTM_RCD_ADDED"},
									   {"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
									   {"UpdatedByUser","UPDATED_BY_USER"},
									   {"AddedByUser","ADDED_BY_USER"},
									   {"DatedText_HTMLComments", "DATED_TEXT_HTMLCOMMENTS"}
									};
		public int  EvDtRowId{	get{  return GetFieldInt("EV_DT_ROW_ID");}	set{ SetField("EV_DT_ROW_ID",value);}}
		public int  EventId{get{  return   GetFieldInt("EVENT_ID");}set{ SetField("EVENT_ID",value);}}
		public string EnteredByUser{get{ return GetFieldString("ENTERED_BY_USER");}set{SetField("ENTERED_BY_USER",value);}}
		public string DateEntered{get{ return GetFieldString("DATE_ENTERED");}set{SetField("DATE_ENTERED",Riskmaster.Common.Conversion.GetDate(value));}}
		public string TimeEntered{get{ return GetFieldString("TIME_ENTERED");}set{SetField("TIME_ENTERED",Riskmaster.Common.Conversion.GetTime(value));}}
		public string DatedText{get{ return GetFieldString("DATED_TEXT");}set{SetField("DATED_TEXT",value);}}
		public string DttmRcdAdded{get{ return GetFieldString("DTTM_RCD_ADDED");}set{SetField("DTTM_RCD_ADDED",value);}}
		public string DttmRcdLastUpd{get{ return GetFieldString("DTTM_RCD_LAST_UPD");}set{SetField("DTTM_RCD_LAST_UPD",value);}}
		public string UpdatedByUser{get{ return GetFieldString("UPDATED_BY_USER");}set{SetField("UPDATED_BY_USER",value);}}
		public string AddedByUser{get{ return GetFieldString("ADDED_BY_USER");}set{SetField("ADDED_BY_USER",value);}}
		//public string DatedText_HTMLComments{get{return GetFieldString("DATED_TEXT_HTMLCOMMENTS");}set{SetField("DATED_TEXT_HTMLCOMMENTS",value);}}
		public string DatedText_HTMLComments{get{return GetFieldStringFallback("DATED_TEXT_HTMLCOMMENTS","DATED_TEXT");}set{SetField("DATED_TEXT_HTMLCOMMENTS",value);}}
		#endregion
		internal EventXDatedText(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "EVENT_X_DATED_TEXT";
			this.m_sKeyField = "EV_DT_ROW_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
		
			//Add all object Children into the Children collection from our "sChildren" list.
//			base.InitChildren(sChildren);
			this.m_sParentClassName = "Event";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
		//TODO Remove this after debugging is done.  Could be a security issue.
		new string ToString()
		{
			return (this as DataObject).Dump();
		}
	}
}
