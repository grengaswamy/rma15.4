﻿using System;
using Riskmaster.Settings;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Collections;
using System.Text;
using System.Xml;


namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("ENTITY_X_BANKING ", "BANKING_ROW_ID")]
   public  class BankingInfo : DataObject
    {
        private string[,] sFields = {
                                        		
														{"BankingRowId", "BANKING_ROW_ID"},
														{"ParentId", "ENTITY_ID"},
														{"AcctTypeCode", "ACCOUNT_TYPE_CODE"},
														{"Transit", "ROUTING_NUMBER"},
														{"AccountNumber", "ACCOUNT_NUMBER"},
														{"BankName", "BANK_NAME"},
														{"BankingStatus", "BANK_STATUS_CODE"},
														{"EffectiveDate", "EFFECTIVE_DATE"},
														{"ExpirationDate","EXPIRATION_DATE"},
													    {"DttmRcdAdded", "DTTM_RCD_ADDED"},
														{"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
													
		};




        public int BankingRowId { get { return GetFieldInt("BANKING_ROW_ID"); } set { SetField("BANKING_ROW_ID", value); } }
        public int ParentId { get { return GetFieldInt("ENTITY_ID"); } set { SetField("ENTITY_ID", value); } }
        public string AccountNumber { get { return GetFieldString("ACCOUNT_NUMBER"); } set { SetField("ACCOUNT_NUMBER", value); } }
        public string Transit { get { return GetFieldString("ROUTING_NUMBER"); } set { SetField("ROUTING_NUMBER", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "ACCOUNT_TYPE")]
        public int AcctTypeCode { get { return GetFieldInt("ACCOUNT_TYPE_CODE"); } set { SetField("ACCOUNT_TYPE_CODE", value); } }
        //[ExtendedTypeAttribute(RMExtType.Entity, "BANKS")]
        public string BankName { get { return GetFieldString("BANK_NAME"); } set { SetField("BANK_NAME", value); } }

       [ExtendedTypeAttribute(RMExtType.Code, "EFT_BANKING_STATUS")]
        public int BankingStatus { get { return GetFieldInt("BANK_STATUS_CODE"); } set { SetField("BANK_STATUS_CODE", value); } }

        public string EffectiveDate { get { return GetFieldString("EFFECTIVE_DATE"); } set { SetField("EFFECTIVE_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string ExpirationDate { get { return GetFieldString("EXPIRATION_DATE"); } set { SetField("EXPIRATION_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }

        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
     



        private string[,] sChildren = { //{"BankEntity","Entity"},
                                          	{"BankingStatusHistlList","BankingStatusHistlList"},
																};


        override internal void OnChildInit(string childName, string childType)
        {
            Entity objEnt = null;
            //Do default per-child processing.
            base.OnChildInit(childName, childType);            
        }

        private string m_StatusApprovedBy = "";
        private string m_StatusReason = "";
        private string m_StatusDateChg = "";

        	protected override void Clear()
		{
			m_StatusApprovedBy = "";
			m_StatusReason="";
			m_StatusDateChg="";
			base.Clear ();
		}
        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
        
           // this.SetFieldAndNavTo("BANK_EID", this.BankEid, "BankEntity", true);

            BankingStatusHist objHist = this.BankingStatusHistList.LastStatusChange();
            if (objHist != null)
            {
                m_StatusApprovedBy = objHist.ApprovedBy;
          m_StatusReason = objHist.Reason;
            }
            else
            {
                m_StatusApprovedBy = "";
               m_StatusReason = "";
            }
          
        }

        internal override void OnChildPreSave(string childName, IDataModel childValue)
        {
            // If Entity is involved we save this first (in case we need the EntityId.)
            //if (childName == "BankEntity")
            //{
            //    (childValue as IPersistence).Save();
            //    this.m_Fields["BANK_EID"] = (childValue as Entity).EntityId;
            //}
        }


        internal override void OnChildDelete(string childName, IDataModel childValue)
        {
            //if (childName == "BankEntity")
            //    return;
            //base.OnChildDelete(childName, childValue);
        }

        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            switch (childName)
            {

                case "BankingStatusHistlList":
                    (itemValue as BankingStatusHist).BankingRowId = this.BankingRowId;
                    break;
         
            }
            base.OnChildItemAdded(childName, itemValue);
        }

        //Handle child saves.  Set the current key into the children since this could be a new record.
        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            //Some of these children are 1 to {0|1} so we'll check and see if the 
            // child actually was touched before forcing it to save.
            bool bIsNewAndModified = isNew && childValue.DataChanged;

            switch (childName)
            {


                case "BankingStatusHistlList":
                    if (isNew)
                    {
                        //BSB 09.21.2004 Fix for query looking for "last change id" run against the list during save.
                        //Most child list items do not need their filter set before calling save.
                        (childValue as BankingStatusHistlList).SQLFilter = String.Format("{0}={1}", "BANKING_ROW_ID", this.BankingRowId);
                        foreach (BankingStatusHist item in (childValue as BankingStatusHistlList))
                            item.BankingRowId = this.BankingRowId;
                        //abisht MITS 11041 Calling UpdateClaimStatusHistory method in case the claim is created for the first time.
                        UpdateBankingStatusHistory();
                    }
                    (childValue as IPersistence).Save();
                    break;
                
               
            }
            base.OnChildPostSave(childName, childValue, isNew);
        }		


        //public Entity BankEntity
        //{
        //    get
        //    {
        //        Entity objItem = base.m_Children["BankEntity"] as Entity;

        //        if ((objItem as IDataModel).IsStale)
        //            objItem = this.CreatableMoveChildTo("BankEntity", (objItem as DataObject), this.BankEid) as Entity;
        //        //				if (!base.IsNew && (objItem as IDataModel).IsStale)
        //        //					objItem.MoveTo(this.BankEid);
        //        //				if(base.IsNew && (objItem as IDataModel).IsStale)
        //        //				{	
        //        //					objItem.Refresh();
        //        //				}
        //        return objItem;
        //    }
        //}

        public BankingStatusHistlList BankingStatusHistList
        {
            get
            {
                BankingStatusHistlList objList = base.m_Children["BankingStatusHistlList"] as BankingStatusHistlList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = String.Format("{0}={1}", "BANKING_ROW_ID", this.BankingRowId);
                return objList;
            }
        }

      
        public string StatusApprovedBy
        {
            get { return m_StatusApprovedBy; }
            set
            {
                if (m_StatusApprovedBy != value)
                {
                    m_StatusApprovedBy = value;
                    this.DataChanged = true;
                }
            }
        }
        public string StatusReason
        {
            get { return m_StatusReason; }
            set
            {
                if (m_StatusReason != value)
                {
                    m_StatusReason = value;
                    this.DataChanged = true;
                }
            }
        }
        public string StatusDateChg
        {
            get { return m_StatusDateChg; }
            set
            {
                if (m_StatusDateChg != Conversion.GetDate(value))
                {
                    m_StatusDateChg = Conversion.GetDate(value);
                    this.DataChanged = true;
                }
            }
        }

        public override void Save()
        {
           
            UpdateBankingStatusHistory();
           
            base.Save();

           

        }

        private void UpdateBankingStatusHistory()
        {

            BankingStatusHist objHist = null;
            objHist = this.BankingStatusHistList.LastStatusChange();

            if (objHist == null || objHist.StatusCode != this.BankingStatus)
            {
                if (objHist == null || !objHist.IsNew)
                    objHist = this.BankingStatusHistList.AddNew();
                objHist.StatusCode = this.BankingStatus;
             objHist.Reason = this.StatusReason;
                objHist.StatusChgdBy = Context.RMUser.LoginName;
                objHist.ApprovedBy = this.StatusApprovedBy;
                if (this.StatusDateChg == "")
                    objHist.DateStatusChgd = Conversion.ToDbDate(DateTime.Today);
                else
                    objHist.DateStatusChgd = this.StatusDateChg;
            }
        }
        internal BankingInfo(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "ENTITY_X_BANKING";
			this.m_sKeyField = "BANKING_ROW_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
			base.InitChildren(sChildren);
			this.m_sParentClassName = "";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
    }

}
