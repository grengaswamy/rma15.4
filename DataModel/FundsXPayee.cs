﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("FUNDS_X_PAYEE", "PAYEE_ROW_ID")]
  public  class FundsXPayee : DataObject	
    {

        private string[,] sFields = {
                                         {"PayeeEid", "PAYEE_EID"},
                                         {"TransId", "FUNDS_TRANS_ID"},
                                         {"Payee1099Flag", "PAYEE_1099_FLAG"},
										 {"AddedByUser", "ADDED_BY_USER"},
										 {"DttmRcdAdded", "DTTM_RCD_ADDED"},
										 {"UpdatedByUser", "UPDATED_BY_USER"},
    			                         {"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
    				                     {"PayeeRowId", "PAYEE_ROW_ID"},
                                         {"PayeeTypeCode", "PAYEE_TYPE_CODE"},
                                         {"PhraseTypeCode", "PHRASE_TYPE_CODE"},
                                         {"BeforePayee", "BEFORE_PAYEE"}
                                         //{"PayeeErRowId", "PAYEE_ER_ROW_ID"}     //avipinsrivas Start : Worked for Jira-340

                    		};

        [ExtendedTypeAttribute(RMExtType.Code, "PAYEE_TYPE")]
        public int PayeeTypeCode { get { return GetFieldInt("PAYEE_TYPE_CODE"); } set { SetField("PAYEE_TYPE_CODE", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
          [ExtendedTypeAttribute(RMExtType.Entity, "ANY")]
        public int  PayeeEid { get { return GetFieldInt("PAYEE_EID"); } set { SetField("PAYEE_EID", value); } }
        public int TransId { get { return GetFieldInt("FUNDS_TRANS_ID"); } set { SetField("FUNDS_TRANS_ID", value); } }
        public bool Payee1099Flag { get { return GetFieldBool("PAYEE_1099_FLAG"); } set { SetField("PAYEE_1099_FLAG", value); } }
        public int PayeeRowId { get { return GetFieldInt("PAYEE_ROW_ID"); } set { SetField("PAYEE_ROW_ID", value); } }
        //Ankit Start : Financial Enhancements - Payee Phrase Change
        public int PhraseTypeCode { get { return GetFieldInt("PHRASE_TYPE_CODE"); } set { SetField("PHRASE_TYPE_CODE", value); } }
        public int BeforePayee { get { return GetFieldInt("BEFORE_PAYEE"); } set { SetField("BEFORE_PAYEE", value); } }
        //End Ankit
        //public int PayeeErRowId { get { return GetFieldInt("PAYEE_ER_ROW_ID"); } set { SetField("PAYEE_ER_ROW_ID", value); } }      //avipinsrivas Start : Worked for Jira-340

        internal FundsXPayee(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }
        new private void Initialize()
        {
            this.m_sTableName = "FUNDS_X_PAYEE";
            this.m_sKeyField = "PAYEE_ROW_ID";
            this.m_sFilterClause = "";

        
            base.InitFields(sFields);
         
            this.m_sParentClassName = "Funds";
            base.Initialize(); 
        }

        public FundsXPayee CreateFundsXPayeeClone()
        {
            FundsXPayee objFundsXPayee = Context.Factory.GetDataModelObject("FundsXPayee", true) as FundsXPayee;
            foreach (string sFieldName in this.m_Fields)
                if (objFundsXPayee.m_Fields.ContainsField(sFieldName))
                    objFundsXPayee.m_Fields[sFieldName] = this.m_Fields[sFieldName];

            objFundsXPayee.DataChanged = true;
            return objFundsXPayee;
        }
    }
}
