using System;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for EventXDatedTextList.
	/// </summary>
	public class EventXOshaList : DataCollection
	{
		internal EventXOshaList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"EVENT_ID";
			this.SQLFromTable =	"EVENT_X_OSHA";
			//	this.SQLFilter =				"EVENT_ID=???" //Must be set by parent at runtime for current record.
			this.TypeName = "EventXOsha";
		}
		public new EventXOsha this[int keyValue]{get{return base[keyValue] as EventXOsha;}}
		public new EventXOsha AddNew(){return base.AddNew() as EventXOsha;}

	}
}
