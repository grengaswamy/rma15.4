
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forCmXVocrehabList.
	/// </summary>
	public class CmXVocrehabList : DataCollection
	{
		internal CmXVocrehabList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"CMVR_ROW_ID";
			this.SQLFromTable =	"CM_X_VOCREHAB";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "CmXVocrehab";
		}
		public new CmXVocrehab this[int keyValue]{get{return base[keyValue] as CmXVocrehab;}}
		public new CmXVocrehab AddNew(){return base.AddNew() as CmXVocrehab;}
		public  CmXVocrehab Add(CmXVocrehab obj){return base.Add(obj) as CmXVocrehab;}
		public new CmXVocrehab Add(int keyValue){return base.Add(keyValue) as CmXVocrehab;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}