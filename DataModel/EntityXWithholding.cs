﻿namespace Riskmaster.DataModel
{
    /// <summary>
    /// Summary description for EntityXWithholding.cs.
    /// </summary>
    //Created by SMISHRA54:  R8 Withholding Enhancement, MITS 26019
    [Riskmaster.DataModel.Summary("ENTITY_X_WITHHOLDING", "WITHHOLDING_ROW_ID")]
    public class EntityXWithholding : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
										{"WithholdingRowId","WITHHOLDING_ROW_ID"},
										{"WithholdingFlag","WITHHOLDING_FLAG"},
										{"EntityId","ENTITY_ID"},
										{"WithholdingTypeCode","WITHHOLDING_TYPE_CODE"},
										{"PercentageNum","PERCENTAGE_NUM"},
										{"RecipientEid","RECIPIENT_EID"},
										{"VoucherFlag","VOUCHER_FLAG"},
										{"DttmRcdAdded","DTTM_RCD_ADDED"},
										{"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
										{"AddedByUser","ADDED_BY_USER"},
                                        {"UpdatedByUser","UPDATED_BY_USER"},
									};
        public int WithholdingRowId { get { return GetFieldInt("WITHHOLDING_ROW_ID"); } set { SetField("WITHHOLDING_ROW_ID", value); } }
        public bool WithholdingFlag { get { return GetFieldBool("WITHHOLDING_FLAG"); } set { SetField("WITHHOLDING_FLAG", value); } }
        public int EntityId { get { return GetFieldInt("ENTITY_ID"); } set { SetField("ENTITY_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "WITHHOLDING_TYPE")]
        public int WithholdingTypeCode { get { return GetFieldInt("WITHHOLDING_TYPE_CODE"); } set { SetField("WITHHOLDING_TYPE_CODE", value); } }
        public double PercentageNum { get { return GetFieldDouble("PERCENTAGE_NUM"); } set { SetField("PERCENTAGE_NUM", value); } }
        [ExtendedTypeAttribute(RMExtType.Entity, "ANY")]
        public int RecipientEid { get { return GetFieldInt("RECIPIENT_EID"); } set { SetField("RECIPIENT_EID", value); } }
        public bool VoucherFlag { get { return GetFieldBool("VOUCHER_FLAG"); } set { SetField("VOUCHER_FLAG", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }

        #endregion

        internal EntityXWithholding(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        new private void Initialize()
        {
            this.m_sTableName = "ENTITY_X_WITHHOLDING";
            this.m_sKeyField = "WITHHOLDING_ROW_ID";
            this.m_sFilterClause = "";
            base.InitFields(sFields);
            this.m_sParentClassName = "Entity";
            base.Initialize();
        }

    }
}
