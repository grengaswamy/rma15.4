﻿
namespace Riskmaster.DataModel
{
    /// <summary>
    /// Table Name: CLAIM_X_POL_DED_HIST 
    /// Maintains History of the changes done to CLAIM_X_POL_DED (ClaimXPolDed)
    /// mits 30910
    /// </summary>
    [Riskmaster.DataModel.Summary("CLAIM_X_POL_DED_HIST", "CLM_X_POL_DED_HIST_ID")]
    public class ClaimXPolDedHist:DataObject
    {
        #region Database Field List
        private string[,] sFields = {
                                        {"ClmXPolDedHistId", "CLM_X_POL_DED_HIST_ID"},
                                        {"ClmXPolDedId", "CLM_X_POL_DED_ID"},
                                        {"DedTypeCode", "DED_TYPE_CODE"},
                                        {"ExcludeExpenseFlag", "EXCLUDE_EXPENSE_FLAG"},
                                        {"SirDedAmt", "SIR_DED_AMT"},
                                        {"DateDedctblChgd", "DATE_DEDCTBL_CHGD"},
                                        {"DedctblChgdBy", "DEDCTBL_CHGD_BY"},
                                        {"DiminishingTypeCode", "DIMNSHNG_TYPE_CODE"},
                                        {"DimPercentNum", "DIM_PERCENT_NUM"},
                                        {"ClaimantEid", "CLAIMANT_EID"}
                                    };

        public int ClmXPolDedHistId { get { return GetFieldInt("CLM_X_POL_DED_HIST_ID"); } set { SetField("CLM_X_POL_DED_HIST_ID", value); } }
        public int ClmXPolDedId { get { return GetFieldInt("CLM_X_POL_DED_ID"); } set { SetField("CLM_X_POL_DED_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "DEDUCTIBLE_TYPE")]
        public int DedTypeCode { get { return GetFieldInt("DED_TYPE_CODE"); } set { SetField("DED_TYPE_CODE", value); } }
        public bool ExcludeExpenseFlag { get { return GetFieldBool("EXCLUDE_EXPENSE_FLAG"); } set { SetField("EXCLUDE_EXPENSE_FLAG", value); } }
        public double SirDedAmt { get { return GetFieldDouble("SIR_DED_AMT"); } set { SetField("SIR_DED_AMT", value); } }
        public string DateDedctblChgd { get { return GetFieldString("DATE_DEDCTBL_CHGD"); } set { SetField("DATE_DEDCTBL_CHGD", value); } }
        public string DedctblChgdBy { get { return GetFieldString("DEDCTBL_CHGD_BY"); } set { SetField("DEDCTBL_CHGD_BY", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "DIMINISHING_TYPE")]
        public int DiminishingTypeCode { get { return GetFieldInt("DIMNSHNG_TYPE_CODE"); } set { SetField("DIMNSHNG_TYPE_CODE", value); } }
        public double DimPercentNum { get { return GetFieldDouble("DIM_PERCENT_NUM"); } set { SetField("DIM_PERCENT_NUM", value); } }
        public int ClaimantEid { get { return GetFieldInt("CLAIMANT_EID"); } set { SetField("CLAIMANT_EID", value); } }

        #endregion

        internal ClaimXPolDedHist(bool isLocked, Context context)
            : base(isLocked, context)
		{
			this.Initialize();
		}

        new private void Initialize()
        {
            this.m_sTableName = "CLAIM_X_POL_DED_HIST";
            this.m_sKeyField = "CLM_X_POL_DED_HIST_ID";
            this.m_sFilterClause = "";

            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            
            this.m_sParentClassName = "ClaimXPolDed";
            base.Initialize(); 

        }
    }
}
