using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forCmXTreatmentPlnList.
	/// </summary>
	public class CmXTreatmentPlnList : DataCollection
	{
		internal CmXTreatmentPlnList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"CMTP_ROW_ID";
			this.SQLFromTable =	"CM_X_TREATMENT_PLN";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "CmXTreatmentPln";
		}
		public new CmXTreatmentPln this[int keyValue]{get{return base[keyValue] as CmXTreatmentPln;}}
		public new CmXTreatmentPln AddNew(){return base.AddNew() as CmXTreatmentPln;}
		public  CmXTreatmentPln Add(CmXTreatmentPln obj){return base.Add(obj) as CmXTreatmentPln;}
		public new CmXTreatmentPln Add(int keyValue){return base.Add(keyValue) as CmXTreatmentPln;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}