using System;

namespace Riskmaster.DataModel
{
	//TODO EventXOsha will have a problem saving new records.  The base class will 
	// try to call GetNextUID() for a new key value and foul up referential integrity.
	// DBUpgrade Required to place both a row_id and the timestamp fields into this table.
	/// <summary>
	/// Summary description for EventXOsha.
	/// </summary>
	//TODO: Complete Summary
	[Riskmaster.DataModel.Summary("EVENT_QM","EVENT_ID", "MedCaseNumber")]
	public class EventQM : DataObject	
	{
		#region Database Field List
		private string[,] sFields = {
														{"EventId", "EVENT_ID"},
														{"MedCaseNumber", "MED_CASE_NUMBER"},
														{"MedFileCode", "MED_FILE_CODE"},
														{"MedIndCode", "MED_IND_CODE"},
														{"IrStatusCode", "IR_STATUS_CODE"},
														{"IrReviewerUid", "IR_REVIEWER_UID"},
														{"IrReviewerName", "IR_REVIEWER_NAME"},
														{"IrDetermCode", "IR_DETERM_CODE"},
														{"IrReviewDate", "IR_REVIEW_DATE"},
														{"IrFollowUpDate", "IR_FOLLOWUP_DATE"},
														{"IrComments", "IR_COMMENTS"},
														{"IrRecActions", "IR_REC_ACTIONS"},
														{"PaStatusCode", "PA_STATUS_CODE"},
														{"PaReviewerEid", "PA_REVIEWER_EID"},
														{"PaDetermCode", "PA_DETERM_CODE"},
														{"PaReviewDate", "PA_REVIEW_DATE"},
														{"PaFollowUpDate", "PA_FOLLOWUP_DATE"},
														{"PaComments", "PA_COMMENTS"},
														{"PaRecActions", "PA_REC_ACTIONS"},
														{"CdStatusCode", "CD_STATUS_CODE"},
														{"CdCommitteeCode", "CD_COMMITTEE_CODE"},
														{"CdDeptEid", "CD_DEPT_EID"},
														{"CdDetermCode", "CD_DETERM_CODE"},
														{"CdReviewDate", "CD_REVIEW_DATE"},
														{"CdFollowUpDate", "CD_FOLLOWUP_DATE"},
														{"CdComments", "CD_COMMENTS"},
														{"CdRecActions", "CD_REC_ACTIONS"},
														{"QmStatusCode", "QM_STATUS_CODE"},
														{"QmReviewerUid", "QM_REVIEWER_UID"},
														{"QmReviewerName", "QM_REVIEWER_NAME"},
														{"QmDetermCode", "QM_DETERM_CODE"},
														{"QmReviewDate", "QM_REVIEW_DATE"},
														{"QmCloseDate", "QM_CLOSE_DATE"},
														{"QmComments", "QM_COMMENTS"},
														{"QmRecActions", "QM_REC_ACTIONS"}};

		public int  EventId{get{  return   GetFieldInt("EVENT_ID");}set{ SetField("EVENT_ID",value);}}
		public string MedCaseNumber{get{ return GetFieldString("MED_CASE_NUMBER");}set{SetField("MED_CASE_NUMBER",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"QM_MED_FILES")]
        public int MedFileCode{get{ return GetFieldInt("MED_FILE_CODE");}set{SetField("MED_FILE_CODE",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"QM_MED_INDICATORS")]
        public int MedIndCode{get{ return GetFieldInt("MED_IND_CODE");}set{SetField("MED_IND_CODE",value);}}
//IR
        [ExtendedTypeAttribute(RMExtType.Code,"QM_STATUS")]
        public int IrStatusCode{get{ return GetFieldInt("IR_STATUS_CODE");}set{SetField("IR_STATUS_CODE",value);}}
		[ExtendedTypeAttribute(RMExtType.Code,"RM_SYS_USERS")]
		public int IrReviewerUid{get{ return GetFieldInt("IR_REVIEWER_UID");}set{SetField("IR_REVIEWER_UID",value);}}
		public string IrReviewerName{get{ return GetFieldString("IR_REVIEWER_NAME");}set{SetField("IR_REVIEWER_NAME",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"QM_DETERMINATION")]
        public int IrDetermCode{get{ return GetFieldInt("IR_DETERM_CODE");}set{SetField("IR_DETERM_CODE",value);}}
		public string IrReviewDate{get{ return GetFieldString("IR_REVIEW_DATE");}set{SetField("IR_REVIEW_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string IrFollowUpDate{get{ return GetFieldString("IR_FOLLOWUP_DATE");}set{SetField("IR_FOLLOWUP_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string IrComments{get{ return GetFieldString("IR_COMMENTS");}set{SetField("IR_COMMENTS",value);}}
		public string IrRecActions{get{ return GetFieldString("IR_REC_ACTIONS");}set{SetField("IR_REC_ACTIONS",value);}}
//PA
        [ExtendedTypeAttribute(RMExtType.Code,"QM_STATUS")]
        public int PaStatusCode{get{ return GetFieldInt("PA_STATUS_CODE");}set{SetField("PA_STATUS_CODE",value);}}
		[ExtendedTypeAttribute(RMExtType.Entity,"PHYSICIANS")]
        public int PaReviewerEid{get{ return GetFieldInt("PA_REVIEWER_EID");}set{SetField("PA_REVIEWER_EID",value);}}
//		public string PaReviewerName{get{ return GetFieldString("PA_REVIEWER_NAME");}set{SetField("PA_REVIEWER_NAME",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"QM_DETERMINATION")]
        public int PaDetermCode{get{ return GetFieldInt("PA_DETERM_CODE");}set{SetField("PA_DETERM_CODE",value);}}
		public string PaReviewDate{get{ return GetFieldString("PA_REVIEW_DATE");}set{SetField("PA_REVIEW_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string PaFollowUpDate{get{ return GetFieldString("PA_FOLLOWUP_DATE");}set{SetField("PA_FOLLOWUP_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string PaComments{get{ return GetFieldString("PA_COMMENTS");}set{SetField("PA_COMMENTS",value);}}
		public string PaRecActions{get{ return GetFieldString("PA_REC_ACTIONS");}set{SetField("PA_REC_ACTIONS",value);}}
//CD
        [ExtendedTypeAttribute(RMExtType.Code,"QM_STATUS")]
        public int CdStatusCode{get{ return GetFieldInt("CD_STATUS_CODE");}set{SetField("CD_STATUS_CODE",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"QM_COMMITTEES")]
        public int CdCommitteeCode{get{ return GetFieldInt("CD_COMMITTEE_CODE");}set{SetField("CD_COMMITTEE_CODE",value);}}
		[ExtendedTypeAttribute(RMExtType.OrgH,"DEPARTMENT")]
        public int CdDeptEid{get{ return GetFieldInt("CD_DEPT_EID");}set{SetField("CD_DEPT_EID",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"QM_DETERMINATION")]
        public int CdDetermCode{get{ return GetFieldInt("CD_DETERM_CODE");}set{SetField("CD_DETERM_CODE",value);}}
		public string CdReviewDate{get{ return GetFieldString("CD_REVIEW_DATE");}set{SetField("CD_REVIEW_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string CdFollowUpDate{get{ return GetFieldString("CD_FOLLOWUP_DATE");}set{SetField("CD_FOLLOWUP_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string CdComments{get{ return GetFieldString("CD_COMMENTS");}set{SetField("CD_COMMENTS",value);}}
		public string CdRecActions{get{ return GetFieldString("CD_REC_ACTIONS");}set{SetField("CD_REC_ACTIONS",value);}}
//QM
        [ExtendedTypeAttribute(RMExtType.Code,"QM_STATUS")]
        public int QmStatusCode{get{ return GetFieldInt("QM_STATUS_CODE");}set{SetField("QM_STATUS_CODE",value);}}
		[ExtendedTypeAttribute(RMExtType.Code,"RM_SYS_USERS")]
		public int QmReviewerUid{get{ return GetFieldInt("QM_REVIEWER_UID");}set{SetField("QM_REVIEWER_UID",value);}}
		public string QmReviewerName{get{ return GetFieldString("QM_REVIEWER_NAME");}set{SetField("QM_REVIEWER_NAME",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"QM_DETERMINATION")]
        public int QmDetermCode{get{ return GetFieldInt("QM_DETERM_CODE");}set{SetField("QM_DETERM_CODE",value);}}
		public string QmReviewDate{get{ return GetFieldString("QM_REVIEW_DATE");}set{SetField("QM_REVIEW_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string QmCloseDate{get{ return GetFieldString("QM_CLOSE_DATE");}set{SetField("QM_CLOSE_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string QmComments{get{ return GetFieldString("QM_COMMENTS");}set{SetField("QM_COMMENTS",value);}}
		public string QmRecActions{get{ return GetFieldString("QM_REC_ACTIONS");}set{SetField("QM_REC_ACTIONS",value);}}
		#endregion

		internal EventQM(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "EVENT_QM";
			this.m_sKeyField = "EVENT_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
			//			base.InitChildren(sChildren);
			this.m_sParentClassName = "Event";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
		protected override void OnBuildNewUniqueId()
		{
			//Don't do anything since we're "grafted" onto the event and use the already assigned event id for this.
		}

		//TODO Remove this after debugging is done.  Could be a security issue.
		public override string ToString()
		{
			return (this as DataObject).Dump();
		}
	}
}

