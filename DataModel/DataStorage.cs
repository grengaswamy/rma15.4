using System;
using System.Data;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using System.Reflection;

namespace Riskmaster.DataModel
{
	///<summary>
	///This class supports scripting by giving scripting a place to store variables into the riskmaster database.
	///</summary>
	public class DataStorage: DataRoot
	{

		internal DataStorage(bool isLocked,Context context):base(isLocked, context){;}
		
		private const string m_TableName = "OBJSTORAGE";

		public void Delete(string itemName)
		{
			if(itemName ==null)
				return;
			
			itemName = itemName.Trim();
			
			if (itemName =="")
				return;
			
			try{this.Context.DbConnLookup.ExecuteNonQuery("DELETE FROM " + m_TableName + " WHERE ITEMNAME='" + itemName + "'");}
            catch (Exception e) { throw new DataModelException(Globalization.GetString("DataStorage.DeleteValue.Exception", this.Context.ClientId), e); }
		}
	
		public object this[string itemName]
		{
			get
			{
				if(itemName ==null)
					return null;
							
				itemName = itemName.Trim();
							
				if (itemName =="")
					return null;
				try{return this.Context.DbConnLookup.ExecuteString("SELECT ITEMVALUE FROM " + m_TableName + " WHERE ITEMNAME='" + itemName + "'");}
                catch (Exception e) { throw new DataModelException(Globalization.GetString("DataStorage.Value.get.Exception", this.Context.ClientId), e); }
			}
			set
			{
				if(itemName ==null)
				return;
								
				itemName = itemName.Trim();
								
				if (itemName =="")
				return;
				
				try
				{
					if(	0 == (int) this.Context.DbConnLookup.ExecuteInt("SELECT COUNT(*) FROM " + m_TableName + " WHERE ITEMNAME='" + itemName + "'"))
						this.Context.DbConnLookup.ExecuteNonQuery(String.Format("INSERT INTO {0} ( ITEMNAME, ITEMVALUE) VALUES('{1}', '{2}')",m_TableName,  itemName, value.ToString()));
					else
						this.Context.DbConnLookup.ExecuteNonQuery("UPDATE " + m_TableName + " SET ITEMVALUE='" + value.ToString() + "'  WHERE ITEMNAME='" + itemName + "'");
				}
                catch (Exception e) { throw new DataModelException(Globalization.GetString("DataStorage.Value.set.Exception", this.Context.ClientId), e); }
			}
		}
	}
}
