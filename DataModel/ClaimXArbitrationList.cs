﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Summary description for ClaimXArbitrationList.
    /// </summary>
    public class ClaimXArbitrationList : DataCollection
    {
        internal ClaimXArbitrationList(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "ARBITRATION_ROW_ID";
            this.SQLFromTable = "CLAIM_X_ARB";
            this.TypeName = "ClaimXArbitration";
        }

        public new ClaimXArbitration this[int keyValue] { get { return base[keyValue] as ClaimXArbitration; } }
        public new ClaimXArbitration AddNew() { return base.AddNew() as ClaimXArbitration; }
        public ClaimXArbitration Add(ClaimXArbitration obj) { return base.Add(obj) as ClaimXArbitration; }
        public new ClaimXArbitration Add(int keyValue) { return base.Add(keyValue) as ClaimXArbitration; }

        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }

    }
}
