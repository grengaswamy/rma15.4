using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPiXDiagHistList.
	/// TODO Check with Venkat since he said this should be 
	/// read only but he implemented full read/write...
	/// </summary>
	public class PiXDiagHistList : DataCollection
	{
		internal PiXDiagHistList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"PI_X_DIAGROW_ID";
			this.SQLFromTable =	"PI_X_DIAG_HIST";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PiXDiagHist";
		}
		public new PiXDiagHist this[int keyValue]{get{return base[keyValue] as PiXDiagHist;}}
		public new PiXDiagHist AddNew(){return base.AddNew() as PiXDiagHist;}
		public  PiXDiagHist Add(PiXDiagHist obj){return base.Add(obj) as PiXDiagHist;}
		public new PiXDiagHist Add(int keyValue){return base.Add(keyValue) as PiXDiagHist;}
		
        //BSB To support "Journal" style make a routine that gets the "most recent" collection entry.
        public PiXDiagHist GetMostRecentlyCommittedPiXDiagHist()
        {
            this.SQLFilter = "((IS_ICD10 is null) or (IS_ICD10 <> -1))"; // MITS 32423 : prashbiharis ICD10 Changes
            if (this.Count == 0)
                return null;
            int[] keys = new int[m_keySet.Keys.Count];
			m_keySet.Keys.CopyTo(keys,0);
            Array.Sort(keys); // arrange from smallest to largest
	        return this[keys[keys.Length-1]]; //pick object with largest key value to return;
        }

		//prashbiharis ICD10 changes (MITS 32423) Start

        public PiXDiagHist GetMostRecentlyCommittedPiXDiagHistICD10()
        {
            this.SQLFilter = "(IS_ICD10 = -1)";
            if (this.Count == 0)
                return null;
            int[] keys = new int[m_keySet.Keys.Count];
            m_keySet.Keys.CopyTo(keys, 0);
            Array.Sort(keys); // arrange from smallest to largest
            return this[keys[keys.Length - 1]]; //pick object with largest key value to return;
        }
        public PiXDiagHist GetMostRecentlyCommittedPiXDiagHistbyPiRowId(string pirowid)
        {
            this.SQLFilter = "(((IS_ICD10 is null) or (IS_ICD10 <> -1)) and (PI_ROW_ID = " + pirowid +"))";
            if (this.Count == 0)
                return null;
            int[] keys = new int[m_keySet.Keys.Count];
            m_keySet.Keys.CopyTo(keys, 0);
            Array.Sort(keys); // arrange from smallest to largest
            return this[keys[keys.Length - 1]]; //pick object with largest key value to return;
        }
        public PiXDiagHist GetMostRecentlyCommittedPiXDiagHistICD10byPiRowId(string pirowid)
        {
            this.SQLFilter = "((IS_ICD10 = -1) and (PI_ROW_ID = " + pirowid + "))";
            if (this.Count == 0)
                return null;
            int[] keys = new int[m_keySet.Keys.Count];
            m_keySet.Keys.CopyTo(keys, 0);
            Array.Sort(keys); // arrange from smallest to largest
            return this[keys[keys.Length - 1]]; //pick object with largest key value to return;
        }
		//prashbiharis ICD10 changes (MITS 32423) end

        	//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}