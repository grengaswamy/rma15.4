﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    public class DeviceInfoList : DataCollection
    {
        internal DeviceInfoList(bool isLocked, Context context)
            : base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"DEVICE_ROW_ID";
            this.SQLFromTable = "EVENT_X_MED_DEVICE_PSO";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "DeviceInfo";
		}
        public new DeviceInfo this[int keyValue] { get { return base[keyValue] as DeviceInfo; } }
        public new DeviceInfo AddNew() { return base.AddNew() as DeviceInfo; }
        public DeviceInfo Add(DeviceInfo obj) { return base.Add(obj) as DeviceInfo; }
        public new DeviceInfo Add(int keyValue) { return base.Add(keyValue) as DeviceInfo; }
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
    }
}
