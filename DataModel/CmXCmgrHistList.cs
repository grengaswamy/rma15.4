using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forCmXCmgrHistList.
	/// </summary>
	public class CmXCmgrHistList : DataCollection
	{
		internal CmXCmgrHistList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"CMCMH_ROW_ID";
			this.SQLFromTable =	"CM_X_CMGR_HIST";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "CmXCmgrHist";
		}
		public new CmXCmgrHist this[int keyValue]{get{return base[keyValue] as CmXCmgrHist;}}
		public new CmXCmgrHist AddNew(){return base.AddNew() as CmXCmgrHist;}
		public  CmXCmgrHist Add(CmXCmgrHist obj){return base.Add(obj) as CmXCmgrHist;}
		public new CmXCmgrHist Add(int keyValue){return base.Add(keyValue) as CmXCmgrHist;}
		
		//Find the "most current" Case Manager Record - but don't create if none found.
		public CmXCmgrHist GetCurrentCmXCmgrHist()
		{

			if(this.m_keySet.Count==0)
				return null;

			if(this.SQLFilter=="")
				return null;

			int key = Context.DbConnLookup.ExecuteInt("SELECT CMCMH_ROW_ID FROM CM_X_CMGR_HIST WHERE NOT PRIMARY_CMGR_FLAG IS NULL AND PRIMARY_CMGR_FLAG !=0 AND  " + this.SQLFilter);
			
			if(key !=0)
				return this[key];
			else
			{ //Attempt to pick the last adjuster added.
				int maxRowId=99999; 
				foreach(int curKey in this.m_keySet.Keys)
					if(maxRowId==99999 || curKey>maxRowId)
						maxRowId=curKey;
				return this[maxRowId];
			}
		}
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}