
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forClientLimitsList.
	/// </summary>
	public class ClientLimitsList : DataCollection
	{
		internal ClientLimitsList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"CLIENT_LIMITS_ROWID";
			this.SQLFromTable =	"CLIENT_LIMITS";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "ClientLimits";
		}
		public new ClientLimits this[int keyValue]{get{return base[keyValue] as ClientLimits;}}
		public new ClientLimits AddNew(){return base.AddNew() as ClientLimits;}
		public  ClientLimits Add(ClientLimits obj){return base.Add(obj) as ClientLimits;}
		public new ClientLimits Add(int keyValue){return base.Add(keyValue) as ClientLimits;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}