using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for PolicyXInsurerList.
	/// </summary>
	public class PolicyXInsurerList : DataCollection
	{
		internal PolicyXInsurerList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"IN_ROW_ID";
			this.SQLFromTable =	"POLICY_X_INSURER";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PolicyXInsurer";
		}
		public new PolicyXInsurer this[int keyValue]{get{return base[keyValue] as PolicyXInsurer;}}
		public new PolicyXInsurer AddNew(){return base.AddNew() as PolicyXInsurer;}
		public  PolicyXInsurer Add(PolicyXInsurer obj){return base.Add(obj) as PolicyXInsurer;}
		public new PolicyXInsurer Add(int keyValue){return base.Add(keyValue) as PolicyXInsurer;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}