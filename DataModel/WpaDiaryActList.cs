
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forWpaDiaryActList.
	/// </summary>
	public class WpaDiaryActList : DataCollection
	{
		internal WpaDiaryActList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);

			//			this.SQLKeyColumn =	"PARENT_ENTRY_ID";
			this.SQLKeyColumn =	"ACTIVITY_ID";
			this.SQLFromTable =	"WPA_DIARY_ACT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "WpaDiaryAct";
		}
		public new WpaDiaryAct this[int keyValue]{get{return base[keyValue] as WpaDiaryAct;}}
		public new WpaDiaryAct AddNew(){return base.AddNew() as WpaDiaryAct;}
		public  WpaDiaryAct Add(WpaDiaryAct obj){return base.Add(obj) as WpaDiaryAct;}
		public new WpaDiaryAct Add(int keyValue){return base.Add(keyValue) as WpaDiaryAct;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}