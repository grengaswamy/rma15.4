﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 02/19/2014 | 34276  | anavinkumars   | Add new EntityXEntityIDTypeList class to fetch data from db 
 **********************************************************************************************/
using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Summary description for EntityXEntityIDTypeList.
    /// </summary>
    public class EntityXEntityIDTypeList : DataCollection
    {
        internal EntityXEntityIDTypeList(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "ID_NUM_ROW_ID";
            this.SQLFromTable = "ENT_ID_TYPE";
            this.TypeName = "EntityXEntityIDType";
        }
        public new EntityXEntityIDType this[int keyValue] { get { return base[keyValue] as EntityXEntityIDType; } }
        public new EntityXEntityIDType AddNew() { return base.AddNew() as EntityXEntityIDType; }
        //public EntityXEntityIDType Add(EntityXEntityIDType obj) { return base.Add(obj) as EntityXEntityIDType; }
        //public new EntityXEntityIDType Add(int keyValue) { return base.Add(keyValue) as EntityXEntityIDType; }

        //TODO Remove after debugging this could be a security hole.
        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }
    }
}
