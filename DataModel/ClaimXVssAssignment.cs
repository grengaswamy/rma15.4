﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Common;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Summary of ClaimXVssAssignment Class.
    /// </summary>
    [Riskmaster.DataModel.Summary("CLAIM_X_VSSASSIGNMENT", "ROW_ID", "VENDOR_NUMBER")]
    public class ClaimXVssAssignment : DataObject
    {
        #region Database Field List

        private string[,] sFields = {
														{"VssAssignmentRowId", "ROW_ID"},
														{"ClaimId", "CLAIM_ID"},
                                                        {"VendorEid", "VENDOR_EID"},
														{"AssignmentName", "ASSIGNMENT_NAME"},
                                                        {"AssignmentDueDate", "ASSIGNMENT_DUE_DATE"},
                                                        {"Adjuster", "ADJUSTER"},
                                                        {"Manager", "MANAGER"},
                                                        {"AccountNumber", "ACCOUNT_NUMBER"},
                                                        {"AccountId", "ACCOUNT_ID"},
                                                        {"SubAccountId", "SUB_ACCOUNT_ID"},
                                                        {"VendorNumber", "VENDOR_NUMBER"},
                                                        {"Unit", "UNIT"},
                                                        {"AddedByUser","ADDED_BY_USER"},			
														{"UpdatedByUser","UPDATED_BY_USER"},
														{"DttmRcdAdded","DTTM_RCD_ADDED"},
														{"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"}
		};
        //private string[,] sChildren = {	{"VssAssignmentEntity","Entity"}
        //   };

        public int VssAssignmentRowId { get { return GetFieldInt("ROW_ID"); } set { SetField("ROW_ID", value); } }
        public int ClaimId { get { return GetFieldInt("CLAIM_ID"); } set { SetField("CLAIM_ID", value); } }
     //   public int VendorEid { get { return GetFieldInt("VENDOR_EID"); } set { SetField("VENDOR_EID", value); } }
        public int AccountId { get { return GetFieldInt("ACCOUNT_ID"); } set { SetField("ACCOUNT_ID", value); } }
        public int SubAccountId { get { return GetFieldInt("SUB_ACCOUNT_ID"); } set { SetField("SUB_ACCOUNT_ID", value); } }
        public string AssignmentName { get { return GetFieldString("ASSIGNMENT_NAME"); } set { SetField("ASSIGNMENT_NAME", value); } }
        public string AssignmentDueDate { get { return GetFieldString("ASSIGNMENT_DUE_DATE"); } set { SetField("ASSIGNMENT_DUE_DATE", value); } }
        public string Adjuster { get { return GetFieldString("ADJUSTER"); } set { SetField("ADJUSTER", value); } }
        public string Manager { get { return GetFieldString("MANAGER"); } set { SetField("MANAGER", value); } }
        public string AccountNumber { get { return GetFieldString("ACCOUNT_NUMBER"); } set { SetField("ACCOUNT_NUMBER", value); } }
        public string VendorNumber { get { return GetFieldString("VENDOR_NUMBER"); } set { SetField("VENDOR_NUMBER", value); } }
        public string Unit { get { return GetFieldString("UNIT"); } set { SetField("UNIT", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        #endregion

        public int VendorEid
        {
            get
            {
                return GetFieldInt("VENDOR_EID");
            }
            set
            {
                SetField("VENDOR_EID", value);
                SetFieldAndNavTo("VENDOR_EID", value, "VssAssignmentEntity");
            }
        }

        #region Child Implementation
        private string[,] sChildren = {	{"VssAssignmentEntity","Entity"}};
        // Lock Entity Tables
        // Also Initialize SimpleList with desired table, field details.
        override internal void OnChildInit(string childName, string childType)
        {
            //Do default per-child processing.
            base.OnChildInit(childName, childType);

            //Do any custom per-child processing.
            object obj = base.m_Children[childName];
            switch (childType)
            {
                case "Entity": //Is Entity
                    Entity objEnt = (obj as Entity);
                    objEnt.EntityTableId = Context.LocalCache.GetTableId(Globalization.BusinessGlossaryTableNames.VENDOR.ToString());
                    objEnt.LockEntityTableChange = true;
                    //no real way to lock entity class navigation from internal access.
                    break;

                default:
                    //No default process.
                    break;
            }
        }
        //Handle adding the our key to any additions to the record collection properties.
   
        //internal override void LoadData(Riskmaster.Db.DbReader objReader)
        //{
        //    base.LoadData(objReader);
        //}

        //Handle any sub-object(s) for which we need the key in our own table.
        internal override void OnChildPreSave(string childName, IDataModel childValue)
        {

            if (childName == "VssAssignmentEntity")
            {

                if ((childValue as DataObject).WillSave())
                    (childValue as Entity).EntityTableId = Context.LocalCache.GetTableId(Globalization.BusinessGlossaryTableNames.VENDOR.ToString());
                (childValue as IPersistence).Save();
                this.m_Fields["VENDOR_EID"] = (childValue as Entity).EntityId;
            }
            base.OnChildPreSave(childName, childValue);
        }

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
            this.SetFieldAndNavTo("VENDOR_EID", this.VendorEid, "VssAssignmentEntity", true);
        }
        //Protect Entities that should not be deleted.
        internal override void OnChildDelete(string childName, IDataModel childValue)
        {
            base.OnChildDelete(childName, childValue);
        }

        #endregion

        protected override void OnPreCommitDelete()
        {
            // BSB Go Ahead and Flag our Entity since by virtue of being in this particular
            // override we KNOW that the user initiated a deletion against us directly.
            this.VssAssignmentEntity.Delete(); // This call simply flags the entity in the RMX database.
            // True record deletion is not supported for entities within the 
            // base product by ANY MEANS.
        }

        internal ClaimXVssAssignment(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }
        new private void Initialize()
        {

            // base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

            this.m_sTableName = "CLAIM_X_VSSASSIGNMENT";
            this.m_sKeyField = "ROW_ID";
            this.m_sFilterClause = "";

            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Add all object Children into the Children collection from our "sChildren" list.
            base.InitChildren(sChildren);
            this.m_sParentClassName = "Claim";
            base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

        }
        //Override Parent by Shivendu for MITS 9467
        public override IDataModel Parent
        {
            get
            {


                if (base.Parent == null)
                {
                    base.Parent = Context.Factory.GetDataModelObject("Claim", true);
                    m_isParentStale = false;
                    if (this.ClaimId != 0)
                        (base.Parent as DataObject).MoveTo(this.ClaimId);
                }

                return base.Parent;
            }
            set
            {
                base.Parent = value;
            }
        }
    
        protected override void OnBuildNewUniqueId()
        {
            base.OnBuildNewUniqueId();
        }

        public Entity VssAssignmentEntity
        {
            get
            {
                Entity objItem = base.m_Children["VssAssignmentEntity"] as Entity;
                if ((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("VssAssignmentEntity", (objItem as DataObject), this.VendorEid) as Entity;
                return objItem;
            }
        }
    }
}
