using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPolicyXCvgTypeList.
	/// </summary>
	public class PolicyXCvgTypeList : DataCollection
	{
		internal PolicyXCvgTypeList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"POLCVG_ROW_ID";
			this.SQLFromTable =	"POLICY_X_CVG_TYPE";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PolicyXCvgType";
		}
		public new PolicyXCvgType this[int keyValue]{get{return base[keyValue] as PolicyXCvgType;}}
		public new PolicyXCvgType AddNew(){return base.AddNew() as PolicyXCvgType;}
		public  PolicyXCvgType Add(PolicyXCvgType obj){return base.Add(obj) as PolicyXCvgType;}
		public new PolicyXCvgType Add(int keyValue){return base.Add(keyValue) as PolicyXCvgType;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}