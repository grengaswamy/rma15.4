﻿using System;
using Riskmaster.Common;


namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("CLAIM_X_PROPERTYLOSS", "ROW_ID", "DATEREPORTED")]
   public class ClaimXPropertyLoss : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
														{"RowId", "ROW_ID"},
                                                        {"PropertyID","PROPERTY_ID"},  //Aman 
														{"ClaimId", "CLAIM_ID"},
														{"Insured", "INSURED"},
                                                        {"Damage", "DAMAGE"},
														{"PropertyType", "PROPERTYTYPE"},
														{"EstDamage", "ESTDAMAGE"},
														{"SeenAt", "SEENAT"},
														{"Date", "DATEREPORTED"},
														{"Time", "TIMEREPORTED"},
														{"Description", "DESCRIPTION"},
														{"Owner", "OWNER"},
														{"LocOfTheft", "LOCOFTHEFT"},
                                                        {"IncendiaryFire", "INCENDIARYFIRE"},
                                                        {"Vacant", "VACANT"},
                                                        {"UnderConstruction", "UNDERCONSTRUCTION"},
                                                        {"ExpenseFrom", "EXPENSEFROM"},
                                                        {"ExpenseTo", "EXPENSETO"},
                                                        {"OtherInsurance", "OTHERINSURANCE"},
                                                        {"PolholderIsPropertyOwner", "POLHOLDERISPROPERTYOWNER"},
                                                        {"PolicyHolder", "POLICYHOLDER"},
                                                        {"Company", "COMPANY"},
                                                        {"PolicyNumber", "POLICYNUMBER"},
                                                        {"Coverages", "COVERAGES"},
                                                        {"Limits", "LIMITS"}, 
														{"AddedByUser","ADDED_BY_USER"},			
														{"UpdatedByUser","UPDATED_BY_USER"},
														{"DttmRcdAdded","DTTM_RCD_ADDED"},
														{"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
                                                        {"LegacyUniqueIdentifier","LEGACY_UNIQUE_IDENTIFIER"},
                                                        //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                                                        {"AdjusterEId", "ASSIGNADJ_EID"},
														{"Contact", "CONTACT_EID"} //spahariya MITS 30426
                                                        //Ankit End
		};
        public string LegacyUniqueIdentifier { get { return GetFieldString("LEGACY_UNIQUE_IDENTIFIER"); } set { SetField("LEGACY_UNIQUE_IDENTIFIER", value); } }

         public int RowId { get { return GetFieldInt("ROW_ID"); } set { SetField("ROW_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.ChildLink, "PropertyUnit")]       
         public int PropertyID { get { return GetFieldInt("PROPERTY_ID"); } set { SetField("PROPERTY_ID", value); } }  //Aman        
        public int ClaimId { get { return GetFieldInt("CLAIM_ID"); } set { SetField("CLAIM_ID", value); } }

        public bool Insured { get { return GetFieldBool("INSURED"); } set { SetField("INSURED", value); } }
        

       
        public string Damage { get { return GetFieldString("DAMAGE"); } set { SetField("DAMAGE", value); } }
       [ExtendedTypeAttribute(RMExtType.Code, "PROPERTY_TYPE")]
        public int PropertyType { get { return GetFieldInt("PROPERTYTYPE"); } set { SetField("PROPERTYTYPE", value); } }
        
        public double EstDamage { get { return GetFieldDouble("ESTDAMAGE"); } set { SetField("ESTDAMAGE", value); } }
        
        public string SeenAt { get { return GetFieldString("SEENAT"); } set { SetField("SEENAT", value); } }
        public string Date { get { return GetFieldString("DATEREPORTED"); } set { SetField("DATEREPORTED", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string Time { get { return GetFieldString("TIMEREPORTED"); } set { SetField("TIMEREPORTED", Riskmaster.Common.Conversion.GetTime(value)); } }
       
        public string Description { get { return GetFieldString("DESCRIPTION"); } set { SetField("DESCRIPTION", value); } }
         [ExtendedTypeAttribute(RMExtType.Entity, "OTHER PEOPLE")] //RUPAL, MITS 27481
        public int Owner { get { return GetFieldInt("OWNER"); } set { SetField("OWNER", value); } }
        //public int Owner { get { return GetFieldInt("OWNER"); } set { SetField("OWNER", value); } }
          [ExtendedTypeAttribute(RMExtType.Code, "Claim_X_PropertyLocOfTheft")]
        public int LocOfTheft { get { return GetFieldInt("LOCOFTHEFT"); } set { SetField("LOCOFTHEFT", value); } }

          public bool IncendiaryFire { get { return GetFieldBool("INCENDIARYFIRE"); } set { SetField("INCENDIARYFIRE", value); } }
          public bool Vacant { get { return GetFieldBool("VACANT"); } set { SetField("VACANT", value); } }
          public bool UnderConstruction { get { return GetFieldBool("UnderConstruction"); } set { SetField("UnderConstruction", value); } }



     
        public string ExpenseFrom { get { return GetFieldString("EXPENSEFROM"); } set { SetField("EXPENSEFROM", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string ExpenseTo { get { return GetFieldString("EXPENSETO"); } set { SetField("EXPENSETO", Riskmaster.Common.Conversion.GetDate(value)); } }

        public bool OtherInsurance { get { return GetFieldBool("OTHERINSURANCE"); } set { SetField("OTHERINSURANCE", value); } }
        public bool PolholderIsPropertyOwner { get { return GetFieldBool("POLHOLDERISPROPERTYOWNER"); } set { SetField("POLHOLDERISPROPERTYOWNER", value); } }


        [ExtendedTypeAttribute(RMExtType.Entity, "OTHER PEOPLE")] //RUPAL, MITS 27481
        public int PolicyHolder { get { return GetFieldInt("POLICYHOLDER"); } set { SetField("POLICYHOLDER", value); } }
        //spahariya MITS 30426 - add contact 12/07/2012
        [ExtendedTypeAttribute(RMExtType.Entity, "OTHER PEOPLE")]
        public int Contact { get { return GetFieldInt("CONTACT_EID"); } set { SetField("CONTACT_EID", value); } }
        //public int Contact { get { return GetFieldInt("CONTACT_EID"); } set { SetField("CONTACT_EID", value); } }
        //spahariya - End
        [ExtendedTypeAttribute(RMExtType.Entity, "OTHER PEOPLE")]
        public int Company { get { return GetFieldInt("COMPANY"); } set { SetField("COMPANY", value); } }
        //public int Company { get { return GetFieldInt("COMPANY"); } set { SetField("COMPANY", value); } }
        
        public string PolicyNumber { get { return GetFieldString("POLICYNUMBER"); } set { SetField("POLICYNUMBER", value); } }
  
        public string Coverages { get { return GetFieldString("COVERAGES"); } set { SetField("COVERAGES", value); } }
      
        public string Limits { get { return GetFieldString("LIMITS"); } set { SetField("LIMITS", value); } }

        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
        [ExtendedTypeAttribute(RMExtType.Entity, "ADJUSTERS")]
        public int AdjusterEId { get { return GetFieldInt("ASSIGNADJ_EID"); } set { SetField("ASSIGNADJ_EID", value); } }
        //Ankit End
        #endregion

        #region Child Implementation
        private string[,] sChildren = { {"PropertyUnit","PropertyUnit"},
                                      { "PlProperty", "DataSimpleList" },
                                      { "DemandOfferList", "DemandOfferList" },
                                      //Rijul 340
                                      //Rijul 340 end
                                      { "SalvageList", "SalvageList" }};

        [ExtendedTypeAttribute(RMExtType.CodeList, "CATEGORY")]
        public DataSimpleList PlProperty
        {
            get
            {
                DataSimpleList objList = base.m_Children["PlProperty"] as DataSimpleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.LoadSimpleList((int)this.KeyFieldValue);
                return objList;
            }
        }
        public DemandOfferList DemandOfferList
        {
            get
            {
                DemandOfferList objList = base.m_Children["DemandOfferList"] as DemandOfferList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = DemandOffer.ParentKeyFieldName + "=" + this.KeyFieldValue + " AND " + DemandOffer.ParentNameFieldName + "='" + typeof(ClaimXPropertyLoss).Name + "'";
                return objList;
            }
        }
        //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
        public PropertyUnit PropertyUnit
        {
            get
            {
                PropertyUnit objItem = base.m_Children["PropertyUnit"] as PropertyUnit;
                if ((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("PropertyUnit", (objItem as DataObject), this.PropertyID) as PropertyUnit;
                return objItem;
            }
        }
        //Ankit End
        
        //Rijul 340

        //Rijul 340 end

        public SalvageList SalvageList
        {
            get
            {
                SalvageList objList = base.m_Children["SalvageList"] as SalvageList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = Salvage.ParentKeyFieldName + "=" + this.KeyFieldValue + " AND " + Salvage.ParentNameFieldName + "='" + typeof(ClaimXPropertyLoss).Name + "'";
                return objList;
            }
        }
        // Lock Entity Tables
        // Also Initialize SimpleList with desired table, field details.
        override internal void OnChildInit(string childName, string childType)
        {
         
            //Do default per-child processing.
            base.OnChildInit(childName, childType);

            //Do any custom per-child processing.
            object obj = base.m_Children[childName];
            switch (childType)
            {
                case "DataSimpleList": 
                    DataSimpleList objList = base.m_Children[childName] as DataSimpleList;
                    objList.Initialize("PROPERTYINVOLVED_ID", "CLAIM_X_PROPERTYINVOLVED", "ROW_ID");
                    break;
                //rijul 340
                    //Rijul 340 end
            }
            
        }

        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            switch (childName)
            {
                case "DemandOfferList":
                    (itemValue as DemandOffer).ParentId = this.RowId;
                    (itemValue as DemandOffer).ParentName = typeof(ClaimXPropertyLoss).Name;
                    break;
                case "SalvageList":
                    (itemValue as Salvage).ParentId = this.RowId;
                    (itemValue as Salvage).ParentName = typeof(ClaimXPropertyLoss).Name;
                    break;
            }
            base.OnChildItemAdded(childName, itemValue);
        }

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
            //Rijul 340
            //Rijul 340 end
        }

        //Handle any sub-object(s) for which we need the key in our own table.
            // If Entity is involved we save this first (in case we need the EntityId.);
            
            //Rijul 340
            //Rijul End

        //Handle child saves.  Set the current key into the children since this could be a new record.
        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            switch (childName)
            {
                case "PlProperty":
                    if (isNew)
                        (childValue as DataSimpleList).SetKeyValue(this.RowId);
                    (childValue as DataSimpleList).SaveSimpleList();
                    break;
                case "DemandOfferList":
                    if (isNew)
                        foreach (DemandOffer item in (childValue as DemandOfferList))
                        {
                            item.ParentId = this.RowId;
                            item.ParentName = typeof(ClaimXPropertyLoss).Name;
                        }
                    (childValue as IPersistence).Save();
                    break;
                case "SalvageList":
                    if (isNew)
                        foreach (Salvage item in (childValue as SalvageList))
                        {
                            item.ParentId = this.RowId;
                            item.ParentName = typeof(ClaimXPropertyLoss).Name;
                        }
                    (childValue as IPersistence).Save();
                    break;
                
            }
            base.OnChildPostSave(childName, childValue, isNew);
        }

        //Protect Entities that should not be deleted.
        internal override void OnChildDelete(string childName, IDataModel childValue)
        {
            base.OnChildDelete(childName, childValue);
        }


      
        #endregion

        internal ClaimXPropertyLoss(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        public override IDataModel Parent
        {
            get
            {


                if (base.Parent == null)
                {
                    base.Parent = Context.Factory.GetDataModelObject("Claim", true);
                    m_isParentStale = false;
                    if (this.ClaimId != 0)
                        (base.Parent as DataObject).MoveTo(this.ClaimId);
                }

                return base.Parent;
            }
            set
            {
                base.Parent = value;
            }
        }

        new private void Initialize()
        {

            // base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

            this.m_sTableName = "CLAIM_X_PROPERTYLOSS";
            this.m_sKeyField = "ROW_ID";
            this.m_sFilterClause = "";

            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Add all object Children into the Children collection from our "sChildren" list.
          //  base.InitChildren(sChildren);
            base.InitChildren(sChildren);
            this.m_sParentClassName = "Claim";
            base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

        }

        public override void Save()
        {
            base.Save();

            if (!this.IsMigrationImport)
            {
                //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                int iAssignmentLvlCodeID = Context.LocalCache.GetCodeId("UN", "ASSIGNMENT_LEVEL");
                string sAttachTable = Constants.WPA_DIARY_ENTRY_ATTACH_TABLE.PROPERTYLOSS.ToString();
                int iAttachRecordID = this.RowId;

                CommonFunctions.UpdateAdjAssignment(Context.LocalCache.DbConnectionString, this.AdjusterEId, this.ClaimId, iAssignmentLvlCodeID, sAttachTable, iAttachRecordID, this.Context.ClientId);

                if (this.Context.InternalSettings.SysSettings.AdjAssignmentAutoDiary.Equals(-1))
                {
                    string sEntryName = "Diary for Claim ";
                    string sClaimNumber = string.Empty;
                    int iAdjUserID = CommonFunctions.GetAdjusterUserID(Context.LocalCache.DbConnectionString, this.AdjusterEId, this.Context.ClientId);
                    string sAssignedUser = string.Empty;

                if (!int.Equals(iAdjUserID, 0))
                {
                    //Getting Login Name for Adjuster******************************************
                    sAssignedUser = Context.LocalCache.GetSystemLoginName(iAdjUserID, Context.RMUser.DatabaseId, Riskmaster.Security.SecurityDatabase.GetSecurityDsn(this.Context.ClientId),Context.ClientId);
                    //End for Login Name*******************************************************

                        if (!string.IsNullOrEmpty(sAssignedUser))
                        {
                            Claim objClaim = this.Parent as Claim;
                            if (objClaim == null && this.ClaimId != 0)
                            {
                                objClaim = (Claim)Context.Factory.GetDataModelObject("Claim", false);
                                objClaim.MoveTo(this.ClaimId);
                            }
                            if ((objClaim != null) && (this.ClaimId != 0))
                            {
                                sClaimNumber = objClaim.ClaimNumber;
                            }

                            sEntryName = string.Concat(sEntryName, sClaimNumber, ", New Property Assignment, ", PropertyUnit.Pin);

                            //Creation of Diary for Assigned Adjuster**********************************
                            CommonFunctions.CreateWPADiary(Context.LocalCache.DbConnectionString, sEntryName, null, 2, -1, sAssignedUser, "SYSTEM", string.Empty, -1, sAttachTable, iAttachRecordID, null, Conversion.GetTime(DateTime.Now.ToString()), Conversion.ToDbDate(System.DateTime.Today), "0|0|0|0|0|0|0|0|0", this.Context.ClientId);
                            //End Diary Creation*******************************************************
                        }
                    }
                }
                //Ankit End
            }
        }

        public override void Delete()
        {
            //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            string sSql = string.Empty;
            sSql = String.Format("DELETE FROM CLAIM_ADJ_ASSIGNMENT WHERE ATTACH_RECORDID={0} AND ATTACH_TABLE='{1}'", this.RowId, Constants.WPA_DIARY_ENTRY_ATTACH_TABLE.PROPERTYLOSS.ToString());
            Context.DbConnLookup.ExecuteNonQuery(sSql);
            //Ankit End

            base.Delete();
        }

        #region Supplemental Fields Exposed
        //Supplementals are implemented in the base class but not exposed externally.
        //This allows the derived class to control whether the object appears to have
        //supplementals or not. 
        //Entity exposes Supplementals.
        internal override string OnBuildSuppTableName()
        {
            return "CLAIM_X_PROPERTYLOSS_SUPP";
        }

        public new Supplementals Supplementals
        {
            get
            {
                return base.Supplementals;
            }
        }
        #endregion
    }
}
