﻿using System;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// This class will be used to retrieve and update Comments related to database
    /// table COMMENTS_TEXT
    /// </summary>
    [Riskmaster.DataModel.Summary("COMMENTS_TEXT", "COMMENT_ID")]
    public class Comment : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
										{"AttachTable", "ATTACH_TABLE"},
										{"AttachRecordId", "ATTACH_RECORDID"},
										{"CommentsRecordId", "COMMENT_ID"},
                                        {"Comments", "COMMENTS"},
										{"HTMLComments", "HTMLCOMMENTS"},
                                        //Raman MITS 19155 : Comments added by one user overwrite other users changes if both users had open the same comments window
                                        {"DttmRcdAdded","DTTM_RCD_ADDED"},
									    {"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
                                        {"AddedByUser", "ADDED_BY_USER"},
                                        {"UpdatedByUser", "UPDATED_BY_USER"},
		};

        public string AttachTable { get { return GetFieldString("ATTACH_TABLE"); } set { SetField("ATTACH_TABLE", value); } }
        public int AttachRecordId { get { return GetFieldInt("ATTACH_RECORDID"); } set { SetField("ATTACH_RECORDID", value); } }
        public int CommentsRecordId { get { return GetFieldInt("COMMENT_ID"); } set { SetField("COMMENT_ID", value); } }
        //Raman MITS 19155 : Comments added by one user overwrite other users changes if both users had open the same comments window
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }

        public string Comments 
        { 
            get { return GetFieldString("COMMENTS"); }
            set { SetField("COMMENTS", value); if (m_DataChanged) m_isStale = false; } 
        }
        public string HTMLComments 
        { 
            get { return GetFieldString("HTMLCOMMENTS"); }
            set { SetField("HTMLCOMMENTS", value); if (m_DataChanged) m_isStale = false; } 
        }
        #endregion

        private bool m_Loaded = false;

        internal override void LoadData(DbReader objReader)
        {
            base.LoadData(objReader);

            this.DataChanged = false;
            m_Loaded = true;            
        }

        internal void LoadData(string sAttachTable, int iAttachRecordId)
        {
            if (m_Loaded)
                return;

            AttachTable = sAttachTable;
            AttachRecordId = iAttachRecordId;

            this.m_sSQL = String.Format("SELECT {0} FROM {1} WHERE ATTACH_TABLE='{2}' AND ATTACH_RECORDID={3}", this.m_Fields.ToString(), this.m_sTableName, AttachTable, AttachRecordId);

            using (DbReader oReader = DbFactory.ExecuteReader(base.Context.RMDatabase.ConnectionString, this.m_sSQL))
            {
                if (oReader.Read())
                {
                    base.LoadData(oReader);
                }
            }
            this.DataChanged = false;
            m_Loaded = true;

            //Try to avoid save parent again
            //this.Parent = null;
        }

        //public override void Delete()
        //{
        //    ; //We never ever ever want to delete reserve history records
        //}

        internal Comment(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        new private void Initialize()
        {
            this.m_sTableName = "COMMENTS_TEXT";
            this.m_sKeyField = "COMMENT_ID";
            this.m_sFilterClause = "";

            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Add all object Children into the Children collection from our "sChildren" list.
            //			base.InitChildren(sChildren);
            this.m_sParentClassName = string.Empty;
            base.Initialize();  //Moved after most init logic so that scripting can be called successfully.
        }
    }
}
