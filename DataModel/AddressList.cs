﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riskmaster.DataModel
{
    public class AddressList:DataCollection
    {
        //JIRA RMA-8753 nshah28 start
        internal AddressList(bool isLocked, Context context): base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "ADDRESS_ID";
            this.SQLFromTable = "ADDRESS";
            this.TypeName = "Address";
		}
        public new Address this[int keyValue] { get { return base[keyValue] as Address; } }
        public new Address AddNew() { return base.AddNew() as Address; }
        public Address Add(Address obj) { return base.Add(obj) as Address; }
        public new Address Add(int keyValue) { return base.Add(keyValue) as Address; }
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}

    }
}
