
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forCmXAccommodationList.
	/// </summary>
	public class CmXAccommodationList : DataCollection
	{
		internal CmXAccommodationList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"CM_ACCOMM_ROW_ID";
			this.SQLFromTable =	"CM_X_ACCOMMODATION";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "CmXAccommodation";
		}
		public new CmXAccommodation this[int keyValue]{get{return base[keyValue] as CmXAccommodation;}}
		public new CmXAccommodation AddNew(){return base.AddNew() as CmXAccommodation;}
		public  CmXAccommodation Add(CmXAccommodation obj){return base.Add(obj) as CmXAccommodation;}
		public new CmXAccommodation Add(int keyValue){return base.Add(keyValue) as CmXAccommodation;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}