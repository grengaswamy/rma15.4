﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riskmaster.DataModel
{
   
    public class PolCovLimitList : DataCollection
    {
        internal PolCovLimitList(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "LIMIT_ROW_ID";
            this.SQLFromTable = "POL_COV_LIMIT";
            this.TypeName = "PolCovLimit";
        }
        public new PolCovLimit this[int keyValue] { get { return base[keyValue] as PolCovLimit; } }
        public new PolCovLimit AddNew() { return base.AddNew() as PolCovLimit; }
        public PolCovLimit Add(DemandOffer obj) { return base.Add(obj) as PolCovLimit; }
        public new PolCovLimit Add(int keyValue) { return base.Add(keyValue) as PolCovLimit; }

        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }
    }
}
