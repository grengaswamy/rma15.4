﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("EVENT_X_MED_DEVICE_PSO", "DEVICE_ROW_ID", "DeviceType")]
    public class DeviceInfo: DataObject
    {
        #region Database Field List
        private string[,] sFields = {
														{"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
														{"UpdatedByUser", "UPDATED_BY_USER"},
														{"DttmRcdAdded", "DTTM_RCD_ADDED"},
														{"AddedByUser", "ADDED_BY_USER"},
														{"EvtPsoRowId", "EVT_PSO_ROW_ID"},
                                                        {"DeviceRowId", "DEVICE_ROW_ID"},
                                                        {"DeviceType", "PSO_EQUIP_TYPE_CODE"},
                                                        {"DeviceBrandName", "PSO_DEVICE_BRAND_NAME"},
                                                        {"DevManfName", "PSO_DEVICE_MANF_NAME"},                                                        
                                                        {"ImpDevPlacement", "PSO_DEVICE_PLACED_CODE"},
                                                        {"ImpDevRemoval", "PSO_IMP_DEVICE_CODE"},
                                                        {"DevModelNumber", "MODEL_NUMBER"},
                                                        {"DevLotNumber", "LOT_NUMBER"},
                                                        {"DevOtherNumber", "OTHER_NUMBER"},
                                                        {"DevExpDate", "EXPIRATION_DATE"},
		};

        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public int EvtPsoRowId { get { return GetFieldInt("EVT_PSO_ROW_ID"); } set { SetField("EVT_PSO_ROW_ID", value); } }
        public int DeviceRowId { get { return GetFieldInt("DEVICE_ROW_ID"); } set { SetField("DEVICE_ROW_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_DEVICE_TYPE")]
        public int DeviceType { get { return GetFieldInt("PSO_EQUIP_TYPE_CODE"); } set { SetField("PSO_EQUIP_TYPE_CODE", value); } }
        public string DeviceBrandName { get { return GetFieldString("PSO_DEVICE_BRAND_NAME"); } set { SetField("PSO_DEVICE_BRAND_NAME", value); } }
        public string DevManfName { get { return GetFieldString("PSO_DEVICE_MANF_NAME"); } set { SetField("PSO_DEVICE_MANF_NAME", value); } }       
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int ImpDevPlacement { get { return GetFieldInt("PSO_DEVICE_PLACED_CODE"); } set { SetField("PSO_DEVICE_PLACED_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int ImpDevRemoval { get { return GetFieldInt("PSO_IMP_DEVICE_CODE"); } set { SetField("PSO_IMP_DEVICE_CODE", value); } }
        public string DevModelNumber { get { return GetFieldString("MODEL_NUMBER"); } set { SetField("MODEL_NUMBER", value); } }
        public string DevLotNumber { get { return GetFieldString("LOT_NUMBER"); } set { SetField("LOT_NUMBER", value); } }
        public string DevOtherNumber { get { return GetFieldString("OTHER_NUMBER"); } set { SetField("OTHER_NUMBER", value); } }
        public string DevExpDate { get { return GetFieldString("EXPIRATION_DATE"); } set { SetField("EXPIRATION_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        
        #endregion
        internal DeviceInfo(bool isLocked, Context context)
            : base(isLocked, context)
		{
			this.Initialize();
            
		}
        new private void Initialize()
        {
            //Moved after most init logic so that scripting can be called successfully.
            // base.Initialize();  
            this.m_sTableName = "EVENT_X_MED_DEVICE_PSO";
            this.m_sKeyField = "DEVICE_ROW_ID";
            this.m_sFilterClause = string.Empty;
            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            base.InitChildren(sChildren);
            //Add all object Children into the Children collection from our "sChildren" list.
            this.m_sParentClassName = "EventPSO";
            //Add all object Children into the Children collection from our "sChildren" list.
            base.Initialize();

        }

        #region Child Implementation
        private string[,] sChildren = {{"DevIdentifiers","DataSimpleList"},
                                       
									  };

        // Lock Entity Tables
        // Also Initialize SimpleList with desired table, field details.
        override internal void OnChildInit(string childName, string childType)
        {
            //Do default per-child processing.
            base.OnChildInit(childName, childType);
            //Do any custom per-child processing.
            object obj = base.m_Children[childName];
            switch (childType)
            {
                case "DataSimpleList":
                    DataSimpleList objList = obj as DataSimpleList;
                    switch (childName)
                    {
                        case "DevIdentifiers":
                            objList.Initialize("PSO_DEVISE_IDENTI_CODE", "EVENT_X_MED_DEV_IDENT_PSO", "DEVICE_ROW_ID");
                            break;
                    }
                    break;
            }

        }
        //Handle adding the our key to any additions to the record collection properties.
        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
           base.OnChildItemAdded(childName, itemValue);
        }

        //Handle child saves.  Set the current key into the children since this could be a new record.
        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            switch (childName)
            {
                case "DevIdentifiers":
                    if (isNew)
                    {
                        (childValue as DataSimpleList).SetKeyValue(this.DeviceRowId);
                    }
					(childValue as DataSimpleList).SaveSimpleList();
                    break;

            }
            base.OnChildPostSave(childName, childValue, isNew);
        }

        [ExtendedTypeAttribute(RMExtType.CodeList, "PSO_DEVICE_INDENTI")]
        public DataSimpleList DevIdentifiers
        {
            get
            {
                DataSimpleList objList = base.m_Children["DevIdentifiers"] as DataSimpleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.LoadSimpleList((int)this.KeyFieldValue);
                return objList;
            }
        }

        #endregion

        
    }
}
