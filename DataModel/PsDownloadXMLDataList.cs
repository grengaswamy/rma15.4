﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riskmaster.DataModel
{
   public  class PsDownloadXMLDataList : DataCollection
    {
       internal PsDownloadXMLDataList(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "PS_ROW_ID";
            this.SQLFromTable = "PS_DOWNLOAD_DATA_XML";
            this.TypeName = "PsDownloadXMLData";
        }
       public new PsDownloadXMLData this[int keyValue] { get { return base[keyValue] as PsDownloadXMLData; } }
       public new PsDownloadXMLData AddNew() { return base.AddNew() as PsDownloadXMLData; }
       public PsDownloadXMLData Add(PsDownloadXMLData obj) { return base.Add(obj) as PsDownloadXMLData; }
       public new PsDownloadXMLData Add(int keyValue) { return base.Add(keyValue) as PsDownloadXMLData; }

        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }
    }
}
