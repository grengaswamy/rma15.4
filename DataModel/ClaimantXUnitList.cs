﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for ClaimantXUnitList.
	/// </summary>

    public class ClaimantXUnitList : DataCollection
    {
        internal ClaimantXUnitList(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "CLMNT_UNIT_ROW_ID";
            this.SQLFromTable = "CLAIMANT_X_UNIT";
            this.TypeName = "ClaimantXUnit";

        }
        public new ClaimantXUnit this[int keyValue] { get { return base[keyValue] as ClaimantXUnit; } }
        public new ClaimantXUnit AddNew() { return base.AddNew() as ClaimantXUnit; }
        public ClaimantXUnit Add(ClaimantXUnit obj) { return base.Add(obj) as ClaimantXUnit; }
        public new ClaimantXUnit Add(int keyValue) { return base.Add(keyValue) as ClaimantXUnit; }


        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }
    }
}

