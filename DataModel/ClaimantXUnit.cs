﻿using System;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("CLAIMANT_X_UNIT", "CLMNT_UNIT_ROW_ID")]
    public class ClaimantXUnit : DataObject
    {
         #region Database Field List
        private string[,] sFields = {
                                        {"ClaimantUnitRowId","CLMNT_UNIT_ROW_ID"},
                                        {"UnitId", "UNIT_ID"},
                                        {"UnitType", "UNIT_TYPE"},
                                        {"ClaimantRowId","CLAIMANT_ROW_ID" }

                                    };

        public int ClaimantUnitRowId { get { return GetFieldInt("CLMNT_UNIT_ROW_ID"); } set { SetField("CLMNT_UNIT_ROW_ID", value); } }
        public int UnitId { get { return GetFieldInt("UNIT_ID"); } set { SetField("UNIT_ID", value); } }

        public int ClaimantRowId { get { return GetFieldInt("CLAIMANT_ROW_ID"); } set { SetField("CLAIMANT_ROW_ID", value); } }
        public string UnitType { get { return GetFieldString("UNIT_TYPE"); } set { SetField("UNIT_TYPE", value); } }
         #endregion

        internal ClaimantXUnit(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        new private void Initialize()
        {
            this.m_sTableName = "CLAIMANT_X_UNIT";
            this.m_sKeyField = "CLMNT_UNIT_ROW_ID";
            this.m_sFilterClause = "";

            base.InitFields(sFields);
            this.m_sParentClassName = "Claimant";
            base.Initialize();
        }
    }

}

