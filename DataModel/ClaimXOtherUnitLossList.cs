﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    public class ClaimXOtherUnitLossList : DataCollection
    {
        internal ClaimXOtherUnitLossList(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "ROW_ID";
            this.SQLFromTable = "CLAIM_X_OTHERUNIT";
            this.TypeName = "ClaimXOtherUnit";
        }

        public new ClaimXOtherUnit this[int keyValue] { get { return base[keyValue] as ClaimXOtherUnit; } }
        public new ClaimXOtherUnit AddNew() { return base.AddNew() as ClaimXOtherUnit; }
        public ClaimXOtherUnit Add(ClaimXSiteLoss obj) { return base.Add(obj) as ClaimXOtherUnit; }
        public new ClaimXOtherUnit Add(int keyValue) { return base.Add(keyValue) as ClaimXOtherUnit; }

        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }
    }
}
