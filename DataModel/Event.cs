using System;
using System.Xml;
using Riskmaster.Settings;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// First Draft (by hand) Derived RMObject implementation.
	/// </summary>
	[Riskmaster.DataModel.Summary("EVENT","EVENT_ID", "EventNumber")]
	public class Event : DataObject
	{
		#region Database Field List
		private string[,] sFields = {
										{"CountyOfInjury","COUNTY_OF_INJURY"},
									   {"EventId","EVENT_ID"},
									   {"EventNumber","EVENT_NUMBER"},
									   {"EventTypeCode","EVENT_TYPE_CODE"},
									   {"EventStatusCode","EVENT_STATUS_CODE"},
									   {"EventIndCode","EVENT_IND_CODE"},
									   {"EventDescription","EVENT_DESCRIPTION"},
									   {"BriefDesc","BRIEF_DESC"},
									   {"DeptEid","DEPT_EID"},
									   {"DeptInvolvedEid","DEPT_INVOLVED_EID"},
									   {"Addr1","ADDR1"},
									   {"Addr2","ADDR2"},
                                       {"Addr3","ADDR3"},
									   {"Addr4","ADDR4"},
									   {"City","CITY"},
									   {"StateId","STATE_ID"},
									   {"ZipCode","ZIP_CODE"},
									   {"CountryCode","COUNTRY_CODE"},
									   {"LocationAreaDesc","LOCATION_AREA_DESC"},
									   {"PrimaryLocCode","PRIMARY_LOC_CODE"},
									   {"LocationTypeCode","LOCATION_TYPE_CODE"},
									   {"OnPremiseFlag","ON_PREMISE_FLAG"},
									   {"NoOfInjuries","NO_OF_INJURIES"},
									   {"NoOfFatalities","NO_OF_FATALITIES"},
									   {"CauseCode","CAUSE_CODE"},
									   {"DateOfEvent","DATE_OF_EVENT"},
									   {"TimeOfEvent","TIME_OF_EVENT"},
									   {"DateReported","DATE_REPORTED"},
									   {"TimeReported","TIME_REPORTED"},
									   {"RptdByEid","RPTD_BY_EID"},
									   {"DateRptdToRm","DATE_RPTD_TO_RM"},
									   {"DateToFollowUp","DATE_TO_FOLLOW_UP"},
									   //{"Comments","COMMENTS"},
									   {"AccountId","ACCOUNT_ID"},
									   {"DatePhysAdvised","DATE_PHYS_ADVISED"},
									   {"TimePhysAdvised","TIME_PHYS_ADVISED"},
									   {"TreatmentGiven","TREATMENT_GIVEN"}, 
									   {"ReleaseSigned","RELEASE_SIGNED"}, 
									   {"DeptHeadAdvised","DEPT_HEAD_ADVISED"},
									   {"PhysNotes","PHYS_NOTES"},
									   {"DateCarrierNotif","DATE_CARRIER_NOTIF"},
									   {"DttmRcdAdded","DTTM_RCD_ADDED"},
									   {"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
									   {"UpdatedByUser","UPDATED_BY_USER"},
									   {"AddedByUser","ADDED_BY_USER"},
//									   {"SecDeptEid","SEC_DEPT_EID"}, // BES Internal Only.
									   {"InjuryFromDate","INJURY_FROM_DATE"},
									   {"InjuryToDate","INJURY_TO_DATE"},
									   //{"HTMLComments","HTMLCOMMENTS"},
									   {"EventDescription_HTMLComments","EVENT_DESCRIPTION_HTMLCOMMENTS"},
                                       {"LocationAreaDesc_HTMLComments","LOCATION_AREA_DESC_HTMCOMMENTS"},
									   {"RequireIntervention","INT_REQ_FLAG"}, 
                                       {"ConfRecFlag","CONF_FLAG"},
                                       {"CatastropheNumber","CATASTROPHE_CODE"}    //Added by Amitosh for R8 enhancment
									   };
		#endregion

		//String array of the form {{ChildName1,ChildType1}{ChildName2,ChildType2}...}
        private string[,] sChildren = {{"EventXAction","DataSimpleList"},
															{"EventXOutcome","DataSimpleList"},
															{"EventXDatedTextList","EventXDatedTextList"},
															{"EventOsha","EventXOsha"},
															{"ReporterEntity","Entity"},
															{"EventFallIndicator","FallIndicator"},
															{"EventQM","EventQM"},
															{"EventMedwatch","EventMedwatch"},
															{"EventSentinel","EventSentinel"},
															{"PiList","PersonInvolvedList"},
															{"ProgressNoteList","ClaimProgressNoteList"},
															{"ClaimList","ClaimList"},
															{"EventXInterventionList","EventXInterventionList"},
                                                            {"Comment","Comment"}
															};
        //R7 Perf Imp
        private bool bScriptEnabled = false;

		internal Event(bool isLocked, Context context):base(isLocked, context){Initialize();}

		new private void Initialize()
		{
            SysSettings oSettings = new SysSettings(base.Context.RMDatabase.ConnectionString, this.Context.ClientId);
            if (oSettings.UseLegacyComments)
            {
                m_bUseLegacyComments = true;

                string[,] sFields2 = new string[sFields.GetLength(0) + 2, sFields.GetLength(1)];
                int iRow = sFields.GetLength(0);
                int iColumn = sFields.GetLength(1);
                for (int i = 0; i < iRow; i++)
                    for (int j = 0; j < iColumn; j++)
                        sFields2[i, j] = sFields[i, j];

                sFields2[iRow, 0] = "Comments";
                sFields2[iRow, 1] = "COMMENTS";
                sFields2[iRow + 1, 0] = "HTMLComments";
                sFields2[iRow + 1, 1] = "HTMLCOMMENTS";

                sFields = sFields2;
            }

			this.m_sTableName = "EVENT";
			this.m_sKeyField = "EVENT_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
            
			base.InitFields(sFields);
		
			//Add all object Children into the Children collection from our "sChildren" list.
			base.InitChildren(sChildren);
			this.m_sParentClassName = "";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.
		}
		public override void Save()
		{
			if((this as IDataModel).DataObjectState == DataObjectState.IsSaving)
			{	
				base.Save();
				return;
			}

			// JP 10.24.2005    if(this.EventNumber=="")
			// JP 10.24.2005    	CreateEventNumber();
			base.Save();
		}

        // pyadav25 6/27/2013 MITS 31799 - To delete associated diaries when an event is deleted.

        protected override void OnPreCommitDelete()
        {
            DeleteAssociatedDiaries();
            base.OnPreCommitDelete();
        }

        private void DeleteAssociatedDiaries()
        {
            try
            {
                if (this.EventId == 0)
                    return;

                //Delete all Activity Records (if any)
                string SQL = "DELETE FROM WPA_DIARY_ACT WHERE PARENT_ENTRY_ID IN ";
                SQL += " (SELECT ENTRY_ID FROM WPA_DIARY_ENTRY WHERE ATTACH_RECORDID=" + this.EventId;
                SQL += " AND ATTACH_TABLE='EVENT')";

                if (Context.DbTrans == null)
                    Context.DbConn.ExecuteNonQuery(SQL);
                else
                    Context.DbConn.ExecuteNonQuery(SQL, Context.DbTrans);

                //Delete Main Diary Records (if any)
                SQL = "DELETE FROM WPA_DIARY_ENTRY WHERE ATTACH_RECORDID=" + this.EventId;
                SQL += " AND ATTACH_TABLE='EVENT'";

                if (Context.DbTrans == null)
                    Context.DbConn.ExecuteNonQuery(SQL);
                else
                    Context.DbConn.ExecuteNonQuery(SQL, Context.DbTrans);
            }
            catch (Exception e) {
                throw new Riskmaster.ExceptionTypes.DataModelException(Common.Globalization.GetString("Event.DeleteAssociatedDiaries.Exception", this.Context.ClientId), e);
            }
        }

        // pyadav25 6/27/2013 MITS 31799 - End
        
		// *BEGIN*  JP 10.24.2005     Moved from Save because EventId has to be allocated before event number can be generated.
		protected override void OnBuildNewUniqueId()
		{
			base.OnBuildNewUniqueId ();

			if(this.EventNumber=="")
				CreateEventNumber();
            //Umesh BOB Enhancement
            if (this.DeptEid == 0)
            {
                AssignDepartmentEid();
            }
		}
		// *END*   JP 10.24.2005

	private void CreateEventNumber()
	{

		string sTmp = "";
		string sId = "";
		int lCount = 0;
		int lId = 0;
		bool boolExit = false;
		Random objRand = new Random((int)DateTime.Now.Ticks);

#if NO_SCRIPT
#else
        bScriptEnabled = Context.LocalCache.IsScriptEditorEnabled();
        //R7 Prf smishra25: Run custom script only if setting is on 
        if (this.EventNumber.Trim() == "" && bScriptEnabled) //Run Custom Script for this if present.
			Context.ScriptEngine.RunScriptMethod("GenerateEventNumber", this);
#endif		
		if(this.EventNumber.Trim() !="") //Done - we got our new event number via script.
			return;

	//Generate the standard event number
	sTmp = Context.InternalSettings.SysSettings.EvPrefix;
	if( Context.InternalSettings.SysSettings.EvIncYearFlag)
		
		if(this.DateOfEvent=="")
			sTmp +=DateTime.Now.Year;
		else
	        sTmp +=this.DateOfEvent.Substring(0,4);

	do
	{	sId = sTmp;
		if(lId>0)
			sId += lId.ToString() + DateTime.Now.TimeOfDay.Seconds.ToString();
		sId += this.EventId.ToString("000000");

		//Check Already In Use
		if(null==Context.DbConnLookup.ExecuteScalar("SELECT EVENT_ID FROM EVENT WHERE EVENT_NUMBER='" + sId + "'"))
			boolExit = true;
		else
		{
			lCount++;
			lId += objRand.Next(1,9);
		}
	}while(!boolExit && lCount < 1000);

	this.EventNumber = sId;
 }
    private void AssignDepartmentEid()
    { //BOB Enhancement  :  Ukusvaha
        if (this.DeptEid == 0)
        {
            if (this.Context.InternalSettings.SysSettings.AutoPopulateDpt == -1)
            {

                this.DeptEid = this.Context.InternalSettings.SysSettings.AutoFillDpteid;
            }
        }
        //End BOB Enhancement 
        
    }
		#region Child Object Properties
		internal override void LoadData(Riskmaster.Db.DbReader objReader)
		{
			base.LoadData (objReader);

            //If Use Legacy Comments flag not st


			//BSB Fix for AutoNav Child(ren) Not Loaded before pre-save.
			this.SetFieldAndNavTo("RPTD_BY_EID",this.RptdByEid,"ReporterEntity",true);
		}
		//Objects that must be in the database before the main class fields. 
		// For example, an event  reporter entity must be in place FIRST because the 
		// reporter's EIDkey will be included as a field value of the event.
		override internal void OnChildPreSave(string childName, IDataModel childValue)
		{
			switch(childName)
			{
				case "ReporterEntity":
					Entity obj = (childValue as Entity);
					if(obj.DataChanged)
					{
						if(obj.EntityTableId == 0)
                            obj.EntityTableId = Context.LocalCache.GetTableId(Globalization.IndividualGlossaryTableNames.OTHER_PEOPLE.ToString());
						(obj as IPersistence).Save();
                        if (this.RptdByEid != (childValue as Entity).EntityId)
                        {
                            this.m_Fields["RPTD_BY_EID"] = (childValue as Entity).EntityId;
                            //bkuzhanthaim - RMA-9056 : Event -> Reported Info -> Save is not working : ENTITY_ID  always being 0 in EVENT Table. 
                            this.DataChanged = true;
                        }
					}
					break;
			}
		}
		override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
		{
			//Some of these children are 1 to {0|1} so we'll check and see if the 
			// child actually was touched before forcing it to save.
			bool bIsNewAndModified = isNew && childValue.DataChanged;
			switch(childName)
			{
				case "EventXAction": case "EventXOutcome":
					if(isNew)
						(childValue as DataSimpleList).SetKeyValue(this.EventId);
					(childValue as DataSimpleList).SaveSimpleList();
					break;
				case "EventXDatedTextList":
					if(isNew)
						foreach(EventXDatedText item in (childValue as EventXDatedTextList))
							item.EventId = this.EventId;
					(childValue as IPersistence).Save();
					break;
				case "PiList":
				    if(isNew)
                        foreach (PersonInvolved item in (childValue as PersonInvolvedList))
                        {
                            item.ParentTableName = "EVENT";
                            item.ParentRowId = this.EventId;
                            item.EventId = this.EventId;
                        }
					(childValue as IPersistence).Save();
					break;
				case "ClaimList":
					// BSB\Mihika 11.30.2005
					// The order in which children are saved is generally arbitrary.
					// However, in the sitation where an employee is being created on the fly
					// during a claimwc save it is important that the PiList be saved first.
					// This is necessary becuase the pi eid (also stored as the claimant_eid) 
					// and the employee record are created at that time.
					// We would like for them to exist and be valid before any claims of this event are saved.
					
					// Do this first... 
					// Innocuous since the dirty flags etc will prevent duplication.
					this.OnChildPostSave("PiList",base.m_Children["PiList"] as IDataModel,isNew); 					
					
					//Proceed with ClaimList Save...
					if(isNew)
						foreach(Claim item in (childValue as ClaimList))
							item.EventId = this.EventId;
					(childValue as IPersistence).Save();
					break;
				case "ProgressNoteList":
					if(isNew)
						foreach(ClaimProgressNote item in (childValue as ClaimProgressNoteList))
							item.EventId = this.EventId;
					(childValue as IPersistence).Save();
					break;
				case "EventOsha":
					if(bIsNewAndModified)
						 (childValue as EventXOsha).EventId = this.EventId;
					(childValue as IPersistence).Save();
					break;
				case "EventFallIndicator":
					if(bIsNewAndModified)
						 (childValue as FallIndicator).EventId = this.EventId;
					(childValue as IPersistence).Save();
					break;
				case "EventQM":
					if(bIsNewAndModified)
						 (childValue as EventQM).EventId = this.EventId;
					(childValue as IPersistence).Save();
					break;
				case "EventMedwatch":
					if(bIsNewAndModified)
						(childValue as EventMedwatch).EventId = this.EventId;
					(childValue as IPersistence).Save();
					break;
				//Added for Froi Migration- Mits 33585,33586,33587
                case "EventXMedWatch":
                    if (bIsNewAndModified)
                        (childValue as EventMedwatch).EventId = this.EventId;
                    (childValue as IPersistence).Save();
                    break;
				case "EventSentinel":
					if(bIsNewAndModified)
						(childValue as EventSentinel).EventId = this.EventId;
					(childValue as IPersistence).Save();
					break;
                case "Comment":
                    ((Comment)this.Children["Comment"]).AttachRecordId = this.EventId;
                    ((Comment)this.Children["Comment"]).Save();
                    break;
				default: 
					if (childValue is IPersistence)
						(childValue as IPersistence).Save();
					break;
			}
		}
		override internal  void OnChildInit(string childName, string childType)
		{
			//Do default per-child processing.
			base.OnChildInit(childName, childType);
			
			//Do any custom per-child processing.
			object obj = base.m_Children[childName];
			switch(childType)
				{
					case "DataSimpleList":
						DataSimpleList objList =  obj as DataSimpleList;
						switch(childName)
						{
							case "EventXAction":
								objList.Initialize("ACTION_CODE", "EVENT_X_ACTION","EVENT_ID");
								break;
							case "EventXOutcome":
								objList.Initialize("OUTCOME_CODE", "EVENT_X_OUTCOME","EVENT_ID");
                                break;
						}
						break;
					default: 
						//No default process.
						break;
				}
		}

        /// <summary>
        /// Get back the Comment object
        /// </summary>
        public Comment Comment
        {
            get
            {
                return LoadComments();
            }
        }

        /// <summary>
        /// property for Comments
        /// </summary>
        public string Comments
        {
            get
            {
                if (!m_bUseLegacyComments)
                {
                    Comment oComment = LoadComments();
                    return oComment.Comments;
                }
                else
                {
                    return GetFieldString("COMMENTS");
                }
            }

            set
            {
                if (!m_bUseLegacyComments)
                {
                    Comment oComment = LoadComments();
                    oComment.Comments = value;
                }
                else
                {
                    SetField("COMMENTS",value);
                }
            }
        }

        /// <summary>
        /// property for Html Comments
        /// </summary>
        public string HTMLComments
        {
            get
            {
                if (!m_bUseLegacyComments)
                {
                    Comment oComment = LoadComments();
                    return oComment.HTMLComments;
                }
                else
                {
                    return GetFieldString("HTMLCOMMENTS");
                }
            }

            set
            {
                if (!m_bUseLegacyComments)
                {
                    Comment oComment = LoadComments();
                    oComment.HTMLComments = value;
                }
                else
                {
                    SetField("HTMLCOMMENTS", value);
                }
            }
        }

        private Comment LoadComments()
        {
            Comment oComment = (Comment)m_Children["Comment"];
            oComment.LoadData(m_sTableName, this.EventId);

            return oComment;
        }

		// This is where for collection items, the foreign key values on new items 
		// can be set by the parent at the time of collection membership.
		internal override void OnChildItemAdded(string childName, IDataModel itemValue)
		{
			switch(childName)
			{
				case "EventXDatedTextList":
					itemValue.Parent = this;
					(itemValue as EventXDatedText).EventId = this.EventId;
					break;
				case "PiList":
					itemValue.Parent = this;
					(itemValue as PersonInvolved).EventId= this.EventId;
                    (itemValue as PersonInvolved).ParentTableName = "EVENT";
                    (itemValue as PersonInvolved).ParentRowId = this.EventId;
					break;
				case "ClaimList":
					itemValue.Parent = this;
					(itemValue as Claim).EventId= this.EventId;
					break;
				case "ProgressNoteList":
					itemValue.Parent = this;
					(itemValue as ClaimProgressNote).EventId= this.EventId;
					break;
				case "EventXInterventionList":
					itemValue.Parent = this;
					(itemValue as EventXIntervention).EventId= this.EventId;
					break;

					
			}
		}
		public EventXInterventionList EventXInterventionList
		{
			get
			{
				EventXInterventionList objList = base.m_Children["EventXInterventionList"] as EventXInterventionList;
				if (!base.IsNew && (objList as IDataModel).IsStale)
					objList.SQLFilter = this.KeyFieldName+ "=" + this.KeyFieldValue;
				return objList;
			}
		}		
		[ExtendedTypeAttribute(RMExtType.CodeList,"ACTION_CODE")]
        public DataSimpleList EventXAction
		{
			get
			{
				DataSimpleList objList = base.m_Children["EventXAction"] as DataSimpleList;
				if (!base.IsNew && (objList as IDataModel).IsStale)
					objList.LoadSimpleList((int)this.KeyFieldValue);
				return objList;
			}
		}
        [ExtendedTypeAttribute(RMExtType.CodeList,"OUTCOME_CODE")]
        public DataSimpleList EventXOutcome
		{
			get
			{
				DataSimpleList objList = base.m_Children["EventXOutcome"] as DataSimpleList;
				if (!base.IsNew && (objList as IDataModel).IsStale)
					objList.LoadSimpleList((int)this.KeyFieldValue);
				return objList;
			}
		}

		public EventXDatedTextList EventXDatedTextList
		{
			get
			{
				EventXDatedTextList objList = base.m_Children["EventXDatedTextList"] as EventXDatedTextList;
				if (!base.IsNew && (objList as IDataModel).IsStale)
					objList.SQLFilter = this.KeyFieldName+ "=" + this.KeyFieldValue;
				return objList;
			}
		}		
		public PersonInvolvedList PiList
		{
			get
			{
				PersonInvolvedList objList = base.m_Children["PiList"] as PersonInvolvedList;
				if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = String.Format("{0}={1} AND {2}={3}", "PARENT_ROW_ID", this.EventId, "PARENT_TABLE_NAME", "'EVENT'");
				return objList;
			}
		}
		public ClaimList ClaimList
		{
			get
			{
				ClaimList objList = base.m_Children["ClaimList"] as ClaimList;
				if (!base.IsNew && (objList as IDataModel).IsStale)
					objList.SQLFilter = this.KeyFieldName+ "=" + this.KeyFieldValue;
				return objList;
			}
		}	
		public  ClaimProgressNoteList ProgressNoteList
		{
			get
			{
				ClaimProgressNoteList objList = base.m_Children["ProgressNoteList"] as ClaimProgressNoteList;
				if (!base.IsNew && (objList as IDataModel).IsStale)
					objList.SQLFilter = String.Format("{0}={1} AND {2}={3}" ,this.KeyFieldName,this.KeyFieldValue, "CLAIM_ID", 0);
				//	objList.SQLFilter = this.KeyFieldName+ "=" + this.KeyFieldValue;
				return objList;
			}
		}	

		public EventXOsha EventOsha
		{
			get
			{
				EventXOsha objItem = base.m_Children["EventOsha"] as EventXOsha;
				if((objItem as IDataModel).IsStale)
				{
					objItem = this.CreatableMoveChildTo("EventOsha",(objItem as DataObject),this.EventId) as EventXOsha;
					objItem.EventId=EventId;
				}//				if (!base.IsNew && (objItem as IDataModel).IsStale)
//					try{objItem.MoveTo(this.EventId);}	
//					catch(RecordNotFoundException)
//					{// 12.27.2005 BSB Allow for creation of new record on the fly.
//						CallOnChildInit("EventOsha","EventXOsha");
//						objItem = base.m_Children["EventOsha"] as EventXOsha;
//					}
//				if(base.IsNew && (objItem as IDataModel).IsStale)
//				{	
//					objItem.Refresh();
//					objItem.EventId = this.EventId;
//				}
				return objItem;
			}
		}
		//Added for Froi Migration- Mits 33585,33586,33587
        public EventXOsha EventXOSHA
        {
            get
            {
                return this.EventOsha;
            }
        }
		
		public EventQM EventQM
		{
			get
			{
				EventQM objItem = base.m_Children["EventQM"] as EventQM;
				if((objItem as IDataModel).IsStale)
				{
					objItem = this.CreatableMoveChildTo("EventQM",(objItem as DataObject),this.EventId) as EventQM;
					objItem.EventId= EventId;
				}

//				if (!base.IsNew && (objItem as IDataModel).IsStale)
//					try{objItem.MoveTo(this.EventId);}	
//					catch(RecordNotFoundException)
//					{// 12.27.2005 BSB Allow for creation of new record on the fly.
//						CallOnChildInit("EventQM","EventQM");
//						objItem = base.m_Children["EventQM"] as EventQM;
//					}
//				if(base.IsNew && (objItem as IDataModel).IsStale)
//				{	
//					objItem.Refresh();
//					objItem.EventId = this.EventId;
//				}
				return objItem;
			}
		}	
		public EventSentinel EventSentinel
		{
			get
			{
				EventSentinel objItem = base.m_Children["EventSentinel"] as EventSentinel;
				if((objItem as IDataModel).IsStale)
				{
					objItem = this.CreatableMoveChildTo("EventSentinel",(objItem as DataObject),this.EventId) as EventSentinel;
					objItem.EventId= EventId;
				}

//				if (!base.IsNew && (objItem as IDataModel).IsStale)
//					try{objItem.MoveTo(this.EventId);}	
//					catch(RecordNotFoundException)
//					{// 12.27.2005 BSB Allow for creation of new record on the fly.
//						CallOnChildInit("EventSentinel","EventSentinel");
//						objItem = base.m_Children["EventSentinel"] as EventSentinel;
//					}
//				if(base.IsNew && (objItem as IDataModel).IsStale)
//				{	
//					objItem.Refresh();
//					objItem.EventId = this.EventId;
//				}
				return objItem;
			}
		}	

		public FallIndicator EventFallIndicator
		{
			get
			{
				FallIndicator objItem = base.m_Children["EventFallIndicator"] as FallIndicator;
				if((objItem as IDataModel).IsStale)
				{
					objItem = this.CreatableMoveChildTo("EventFallIndicator",(objItem as DataObject),this.EventId) as FallIndicator;
					objItem.EventId= EventId;
				}

//				if (!base.IsNew && (objItem as IDataModel).IsStale)
//					try{objItem.MoveTo(this.EventId);}	
//					catch(RecordNotFoundException)
//					{// 12.27.2005 BSB Allow for creation of new record on the fly.
//						CallOnChildInit("EventFallIndicator","FallIndicator");
//						objItem = base.m_Children["EventFallIndicator"] as FallIndicator;
//					}
//				if(base.IsNew && (objItem as IDataModel).IsStale)
//				{	
//					objItem.Refresh();
//					objItem.EventId = this.EventId;
//				}
				return objItem;
			}
		}	
		
		public EventMedwatch EventMedwatch
		{
			get
			{
				EventMedwatch objItem = base.m_Children["EventMedwatch"] as EventMedwatch;
				if((objItem as IDataModel).IsStale)
				{
					objItem = this.CreatableMoveChildTo("EventMedwatch",(objItem as DataObject),this.EventId) as EventMedwatch;
					objItem.EventId= EventId;
				}

//				if (!base.IsNew && (objItem as IDataModel).IsStale)
//					try{objItem.MoveTo(this.EventId);}	
//					catch(RecordNotFoundException)
//					{// 12.27.2005 BSB Allow for creation of new record on the fly.
//						CallOnChildInit("EventMedwatch","EventMedwatch");
//						objItem = base.m_Children["EventMedwatch"] as EventMedwatch;
//					}
//				if(base.IsNew && (objItem as IDataModel).IsStale)
//				{	
//					objItem.Refresh();
//					objItem.EventId = this.EventId;
//				}
				return objItem;
			}
		}
		//Added for Froi Migration- Mits 33585,33586,33587
        public EventMedwatch EventXMedWatch
        {
            get
            {
                EventMedwatch objItem = base.m_Children["EventMedwatch"] as EventMedwatch;
                if ((objItem as IDataModel).IsStale)
                {
                    objItem = this.CreatableMoveChildTo("EventMedwatch", (objItem as DataObject), this.EventId) as EventMedwatch;
                    objItem.EventId = EventId;
                }
                return objItem;
            }
        }		

		public Entity ReporterEntity
		{
			get
			{
				Entity objItem = base.m_Children["ReporterEntity"] as Entity;
				if((objItem as IDataModel).IsStale)
					objItem = this.CreatableMoveChildTo("ReporterEntity",(objItem as DataObject),this.RptdByEid) as Entity;

//				if (!base.IsNew && (objItem as IDataModel).IsStale)
//				{	
//					if(this.RptdByEid > 0)
//						objItem.MoveTo(this.RptdByEid);
//					else //BSB Fix for leaving "create on the fly rptd by entity" marked stale.
//						objItem.Refresh();
//				}
//				if(base.IsNew && (objItem as IDataModel).IsStale)
//				{	
//					objItem.Refresh();
//				}
				return objItem;
			}
		}
		#endregion

		//Supplementals are implemented in the base class but not exposed externally.
		//This allows the derived class to control whether the object appears to have
		//supplementals or not.
		public new Supplementals Supplementals
		{	
			get
			{
				return base.Supplementals;
			}
		}
		#region  Database Field Properties
		//TODO: Use Macro Code to Generate this Region
		public int  EventId{get{  return   GetFieldInt("EVENT_ID");}set{ SetField("EVENT_ID",value);}}
		public string  CountyOfInjury{	get{  return GetFieldString("COUNTY_OF_INJURY");}	set{ SetField("COUNTY_OF_INJURY",value);}}
		public string EventNumber{get{ return GetFieldString("EVENT_NUMBER");}set{SetField("EVENT_NUMBER",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"EVENT_TYPE")]
        public int EventTypeCode{get{ return GetFieldInt("EVENT_TYPE_CODE");}set{SetField("EVENT_TYPE_CODE",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"EVENT_STATUS")]
        public int EventStatusCode{get{ return GetFieldInt("EVENT_STATUS_CODE");}set{SetField("EVENT_STATUS_CODE",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"EVENT_INDICATOR")]
        public int EventIndCode{get{ return GetFieldInt("EVENT_IND_CODE");}set{SetField("EVENT_IND_CODE",value);}}
		public string EventDescription{get{ return GetFieldString("EVENT_DESCRIPTION");}set{SetField("EVENT_DESCRIPTION",value);}}
		public string BriefDesc{get{ return GetFieldString("BRIEF_DESC");}set{SetField("BRIEF_DESC",value);}}
		[ExtendedTypeAttribute(RMExtType.OrgH,"DEPARTMENT")]
        public int DeptEid{get{ return GetFieldInt("DEPT_EID");}set{SetField("DEPT_EID",value);}}
		[ExtendedTypeAttribute(RMExtType.OrgH,"DEPARTMENT")]
        public int DeptInvolvedEid{get{ return GetFieldInt("DEPT_INVOLVED_EID");}set{SetField("DEPT_INVOLVED_EID",value);}}
		public string Addr1{get{ return GetFieldString("ADDR1");}set{SetField("ADDR1",value);}}
		public string Addr2{get{ return GetFieldString("ADDR2");}set{SetField("ADDR2",value);}}
        public string Addr3 { get { return GetFieldString("ADDR3"); } set { SetField("ADDR3", value); } }
        public string Addr4 { get { return GetFieldString("ADDR4"); } set { SetField("ADDR4", value); } }
		public string City{get{ return GetFieldString("CITY");}set{SetField("CITY",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"STATES")]
        public int StateId{get{ return GetFieldInt("STATE_ID");}set{SetField("STATE_ID",value);}}
		public string ZipCode{get{ return GetFieldString("ZIP_CODE");}set{SetField("ZIP_CODE",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"COUNTRY")]
        public int CountryCode{get{ return GetFieldInt("COUNTRY_CODE");}set{SetField("COUNTRY_CODE",value);}}
		public string LocationAreaDesc{get{ return GetFieldString("LOCATION_AREA_DESC");}set{SetField("LOCATION_AREA_DESC",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"PRIMARY_LOCATION")]
        public int PrimaryLocCode{get{ return GetFieldInt("PRIMARY_LOC_CODE");}set{SetField("PRIMARY_LOC_CODE",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"LOCATION_TYPE")]
        public int LocationTypeCode{get{ return GetFieldInt("LOCATION_TYPE_CODE");}set{SetField("LOCATION_TYPE_CODE",value);}}
		public bool OnPremiseFlag {get{ return GetFieldBool("ON_PREMISE_FLAG");}set{SetField("ON_PREMISE_FLAG",value);}}
		public int NoOfInjuries{get{ return GetFieldInt("NO_OF_INJURIES");}set{SetField("NO_OF_INJURIES",value);}}
		public int NoOfFatalities{get{ return GetFieldInt("NO_OF_FATALITIES");}set{SetField("NO_OF_FATALITIES",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"CAUSE_CODE")]
        public int CauseCode{get{ return GetFieldInt("CAUSE_CODE");}set{SetField("CAUSE_CODE",value);}}
		public string DateOfEvent{get{ return GetFieldString("DATE_OF_EVENT");}set{SetField("DATE_OF_EVENT",Riskmaster.Common.Conversion.GetDate(value));}}
		public string TimeOfEvent{get{ return GetFieldString("TIME_OF_EVENT");}set{SetField("TIME_OF_EVENT",Riskmaster.Common.Conversion.GetTime(value));}}
		public string DateReported{get{ return GetFieldString("DATE_REPORTED");}set{SetField("DATE_REPORTED",Riskmaster.Common.Conversion.GetDate(value));}}
		public string	 TimeReported{get{ return GetFieldString("TIME_REPORTED");}set{SetField("TIME_REPORTED",Riskmaster.Common.Conversion.GetTime(value));}}
		
		[ExtendedTypeAttribute(RMExtType.Entity,"ANY")]
        public int RptdByEid{get{ return GetFieldInt("RPTD_BY_EID");}set{SetFieldAndNavTo("RPTD_BY_EID",value,"ReporterEntity");}}
		
		public string DateRptdToRm{get{ return GetFieldString("DATE_RPTD_TO_RM");}set{SetField("DATE_RPTD_TO_RM",Riskmaster.Common.Conversion.GetDate(value));}}
		public string DateToFollowUp{get{ return GetFieldString("DATE_TO_FOLLOW_UP");}set{SetField("DATE_TO_FOLLOW_UP",Riskmaster.Common.Conversion.GetDate(value));}}
		//public string Comments{get{ return GetFieldString("COMMENTS");}set{SetField("COMMENTS",value);}}
		public int AccountId{get{ return GetFieldInt("ACCOUNT_ID");}set{SetField("ACCOUNT_ID",value);}}
		public string DatePhysAdvised{get{ return GetFieldString("DATE_PHYS_ADVISED");}set{SetField("DATE_PHYS_ADVISED",Riskmaster.Common.Conversion.GetDate(value));}}
		public string TimePhysAdvised{get{ return GetFieldString("TIME_PHYS_ADVISED");}set{SetField("TIME_PHYS_ADVISED",Riskmaster.Common.Conversion.GetTime(value));}}
		public bool TreatmentGiven {get{ return GetFieldBool("TREATMENT_GIVEN");}set{SetField("TREATMENT_GIVEN",value);}}
		public bool ReleaseSigned {get{ return GetFieldBool("RELEASE_SIGNED");}set{SetField("RELEASE_SIGNED",value);}}
		public bool DeptHeadAdvised {get{ return GetFieldBool("DEPT_HEAD_ADVISED");}set{SetField("DEPT_HEAD_ADVISED",value);}}
		public string PhysNotes{get{ return GetFieldString("PHYS_NOTES");}set{SetField("PHYS_NOTES",value);}}
		public string DateCarrierNotif{get{ return GetFieldString("DATE_CARRIER_NOTIF");}set{SetField("DATE_CARRIER_NOTIF",Riskmaster.Common.Conversion.GetDate(value));}}
		public string DttmRcdAdded{get{ return GetFieldString("DTTM_RCD_ADDED");}set{SetField("DTTM_RCD_ADDED",value);}}
		public string DttmRcdLastUpd{get{ return GetFieldString("DTTM_RCD_LAST_UPD");}set{SetField("DTTM_RCD_LAST_UPD",value);}}
		public string UpdatedByUser{get{ return GetFieldString("UPDATED_BY_USER");}set{SetField("UPDATED_BY_USER",value);}}
		public string AddedByUser{get{ return GetFieldString("ADDED_BY_USER");}set{SetField("ADDED_BY_USER",value);}}
		public string InjuryFromDate{get{ return GetFieldString("INJURY_FROM_DATE");}set{SetField("INJURY_FROM_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string InjuryToDate{get{ return GetFieldString("INJURY_TO_DATE");}set{SetField("INJURY_TO_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		
		//BSB 04.18.2006 
		// Enhanced to include a "fall-back" to the "comments" field if HTMLCOMMENTS is empty at the DB.
		// This is done to address migration issues where comment data is not copied into new HTMLCOMMENTS field.
		// public string HTMLComments{get{return GetFieldString("HTMLCOMMENTS");}set{SetField("HTMLCOMMENTS",value);}}
		//public string HTMLComments{get{return GetFieldStringFallback("HTMLCOMMENTS","COMMENTS");}set{SetField("HTMLCOMMENTS",value);}}
		public string EventDescription_HTMLComments{get{return GetFieldStringFallback("EVENT_DESCRIPTION_HTMLCOMMENTS","EVENT_DESCRIPTION");}set{SetField("EVENT_DESCRIPTION_HTMLCOMMENTS",value);}}
        public string LocationAreaDesc_HTMLComments { get { return GetFieldStringFallback("LOCATION_AREA_DESC_HTMCOMMENTS", "LOCATION_AREA_DESC"); } set { SetField("LOCATION_AREA_DESC_HTMCOMMENTS", value); } }
		public bool RequireIntervention {get{ return GetFieldBool("INT_REQ_FLAG");}set{SetField("INT_REQ_FLAG",value);}}
		//pmittal5 Confidential Record
        public bool ConfRecFlag { get { return GetFieldBool("CONF_FLAG"); } set { SetField("CONF_FLAG", value); } }

        //Added by Amitosh for R8 enhancement
                [ExtendedTypeAttribute(RMExtType.Code,"CATASTROPHE_NUMBER")]
        public int CatastropheNumber{get{ return GetFieldInt("CATASTROPHE_CODE");}set{SetField("CATASTROPHE_CODE",value);}}
 //End Amitosh
		#endregion
	}

}
