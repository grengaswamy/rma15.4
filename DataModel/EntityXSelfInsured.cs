
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for EntityXSelfInsured.cs.
	/// </summary>
	//TODO: Verify that CertificateNumber is an acceptable default.
	[Riskmaster.DataModel.Summary("ENTITY_X_SELFINSUR","SI_ROW_ID", "CertificateNumber")]
	public class EntityXSelfInsured : DataObject
	{
		#region Database Field List
		private string[,] sFields = {
												{"SIRowId","SI_ROW_ID"},
												{"EntityId","ENTITY_ID"},
												{"Jurisdiction","JURIS_ROW_ID"},
												{"DeletedFlag","DELETED_FLAG"},
												{"LegalEntityId","LEGAL_ENTITY_EID"},
												{"CertNameEid","CERT_NAME_EID"}, //Not exposed previously???
												{"CertificateNumber","CERT_NUMBER"},
												{"EffectiveDate","EFFECTIVE_DATE"},
												{"ExpirationDate","EXPIRATION_DATE"},
												{"Authorization","SI_AUTHORIZATION"},
												{"Organization","SI_ORGANIZATION"},
												{"DttmRcdAdded","DTTM_RCD_ADDED"},
												{"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
												{"UpdatedByUser","UPDATED_BY_USER"},
												{"AddedByUser","ADDED_BY_USER"},
                                                //sgoel6 Medicare 05/12/2009
                                                {"LineOfBusCode","LINE_OF_BUS_CODE"},
                                                {"IsCertDiffFlag","IS_CERT_DIFF_FLAG"},
												};
        public int SIRowId { get { return GetFieldInt("SI_ROW_ID"); } set { SetField("SI_ROW_ID", value); } }
		public int EntityId{get{ return GetFieldInt("ENTITY_ID");}set{SetField("ENTITY_ID",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"STATES")]
        public int  Jurisdiction{get{  return GetFieldInt("JURIS_ROW_ID");}set{ SetField("JURIS_ROW_ID",value);}}
		public bool DeletedFlag{get{ return GetFieldBool("DELETED_FLAG");}set{SetField("DELETED_FLAG",value);}}
		public int LegalEntityId{get{ return GetFieldInt("LEGAL_ENTITY_EID");}set{SetField("LEGAL_ENTITY_EID",value);}}
		public int CertNameEid{get{ return GetFieldInt("CERT_NAME_EID");}set{SetField("CERT_NAME_EID",value);}} //Not exposed previously???
		public string CertificateNumber{get{ return GetFieldString("CERT_NUMBER");}set{SetField("CERT_NUMBER",value);}}
		public string EffectiveDate{get{ return GetFieldString("EFFECTIVE_DATE");}set{SetField("EFFECTIVE_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string ExpirationDate{get{ return GetFieldString("EXPIRATION_DATE");}set{SetField("EXPIRATION_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
        [ExtendedTypeAttribute(RMExtType.Code,"SI_AUTH_TYPE")]
        public int Authorization{get{ return GetFieldInt("SI_AUTHORIZATION");}set{SetField("SI_AUTHORIZATION",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"SI_ORG_TYPE")]
        public int Organization{get{ return GetFieldInt("SI_ORGANIZATION");}set{SetField("SI_ORGANIZATION",value);}}
		public string DttmRcdAdded{get{ return GetFieldString("DTTM_RCD_ADDED");}set{SetField("DTTM_RCD_ADDED",value);}}
		public string DttmRcdLastUpd{get{ return GetFieldString("DTTM_RCD_LAST_UPD");}set{SetField("DTTM_RCD_LAST_UPD",value);}}
		public string UpdatedByUser{get{ return GetFieldString("UPDATED_BY_USER");}set{SetField("UPDATED_BY_USER",value);}}
		public string AddedByUser{get{ return GetFieldString("ADDED_BY_USER");}set{SetField("ADDED_BY_USER",value);}}
        //sgoel6 Medicare 05/12/2009
        [ExtendedTypeAttribute(RMExtType.Code, "LINE_OF_BUSINESS")]
        public int LineOfBusCode { get { return GetFieldInt("LINE_OF_BUS_CODE"); } set { SetField("LINE_OF_BUS_CODE", value); } }
        public int LOB { get { return GetFieldInt("LINE_OF_BUS_CODE"); } set { SetField("LINE_OF_BUS_CODE", value); } }//Added for Froi Migration- Mits 33585,33586,33587

        public bool IsCertDiffFlag { get { return GetFieldBool("IS_CERT_DIFF_FLAG"); } set { SetField("IS_CERT_DIFF_FLAG", value); } }
		#endregion

		internal EntityXSelfInsured(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "ENTITY_X_SELFINSUR";
			this.m_sKeyField = "SI_ROW_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
		
			//Add all object Children into the Children collection from our "sChildren" list.
			//			base.InitChildren(sChildren);
			this.m_sParentClassName = "Entity";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
		//TODO Remove this after debugging is done.  Could be a security issue.
		new string ToString()
		{
			return (this as DataObject).Dump();
		}
		//Added for Froi Migration- Mits 33585,33586,33587
        public void LoadData()
        {
            base.LoadData();
        }
	}
}
