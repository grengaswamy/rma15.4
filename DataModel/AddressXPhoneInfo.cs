﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("ADDRESS_X_PHONEINFO", "PHONE_ID")]
    public class AddressXPhoneInfo:DataObject
    {
        #region Database Field List
        private string[,] sFields ={
                                         { "EntityId","ENTITY_ID"},
                                         { "PhoneId","PHONE_ID"},
                                         { "PhoneCode", "PHONE_CODE"}, 
                                         { "PhoneNo", "PHONE_NO"}
                                 };
        public int EntityId { get { return GetFieldInt("ENTITY_ID"); } set { SetField("ENTITY_ID", value); } }
        public int PhoneId { get { return GetFieldInt("PHONE_ID"); } set { SetField("PHONE_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PHONES_CODES")]
        public int PhoneCode { get { return GetFieldInt("PHONE_CODE"); } set { SetField("PHONE_CODE", value); } }
        public string PhoneNo { get { return GetFieldString("PHONE_NO"); } set { SetField("PHONE_NO", value); } }

        #endregion

        internal AddressXPhoneInfo(bool isLocked, Context context): base(isLocked, context)
        {
            this.Initialize();
        }
        new private void Initialize()
        {

            this.m_sTableName = "ADDRESS_X_PHONEINFO";
            this.m_sKeyField = "PHONE_ID";
            this.m_sFilterClause = "";

            //Add all object Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Add all object Children into the Children	collection from	our	"sChildren"	list.
            this.m_sParentClassName = "Entity";
            base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

        }
    }
}
