﻿using System;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Summary description for ClaimXSubrogationList.
    /// </summary>
    public class ClaimXSubrogationList : DataCollection
    {
        internal ClaimXSubrogationList(bool isLocked, Context context)
            : base(isLocked, context)
        {
            (this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "SUBROGATION_ROW_ID";
            this.SQLFromTable = "CLAIM_X_SUBRO";
            this.TypeName = "ClaimXSubrogation";
        }
        public new ClaimXSubrogation this[int keyValue] { get { return base[keyValue] as ClaimXSubrogation; } }
        public new ClaimXSubrogation AddNew() { return base.AddNew() as ClaimXSubrogation; }
        public ClaimXSubrogation Add(ClaimXSubrogation obj) { return base.Add(obj) as ClaimXSubrogation; }
        public new ClaimXSubrogation Add(int keyValue) { return base.Add(keyValue) as ClaimXSubrogation; }

        public override string ToString()
        {
            string s = "";
            foreach (DataObject obj in this)
                s += obj.Dump();
            return s;
        }
    }
}