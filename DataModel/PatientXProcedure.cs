using System;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary of PatientXProcedure AutoGenerated Class.
	/// Don't forget to add children and custom override behaviors if necessary.
	/// </summary>
	//TODO: Verify that ProcedureCode is an acceptable default string.
	[Riskmaster.DataModel.Summary("PATIENT_PROCEDURE","PROC_ROW_ID", "ProcedureCode")]
	public class PatientXProcedure  : DataObject	
	{
		#region Database Field List
		private string[,] sFields = {
														{"ProcRowId", "PROC_ROW_ID"},
														{"PatientId", "PATIENT_ID"},
														{"ProcedureCode", "PROCEDURE_CODE"},
														{"ProcTypeCode", "PROC_TYPE_CODE"},
														{"DateOfProcedure", "DATE_OF_PROCEDURE"},
														{"SurgeonEid", "SURGEON_EID"},
														{"AnesthAdminFlag", "ANESTH_ADMIN_FLAG"},
														{"AnesthTypeCode", "ANESTH_TYPE_CODE"},
														{"AnesthEid", "ANESTH_EID"},
														{"AsaPsClassCode", "ASA_PS_CLASS_CODE"},
														{"ComplicationDate", "COMPLICATION_DATE"},
														{"PriorToAnesFlag", "PRIOR_TO_ANES_FLAG"},
														{"IccLevelCode", "ICC_LEVEL_CODE"},
														{"LenOfTime", "LEN_OF_TIME"},
														{"Complications", "COMPLICATIONS"},
		};

		public int ProcRowId{get{return GetFieldInt("PROC_ROW_ID");}set{SetField("PROC_ROW_ID",value);}}
		public int PatientId{get{return GetFieldInt("PATIENT_ID");}set{SetField("PATIENT_ID",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"PROCEDURE_CODE")]
        public int ProcedureCode{get{return GetFieldInt("PROCEDURE_CODE");}set{SetField("PROCEDURE_CODE",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"PROCEDURE_TYPES")]
        public int ProcTypeCode{get{return GetFieldInt("PROC_TYPE_CODE");}set{SetField("PROC_TYPE_CODE",value);}}
		public string DateOfProcedure{get{return GetFieldString("DATE_OF_PROCEDURE");}set{SetField("DATE_OF_PROCEDURE",Riskmaster.Common.Conversion.GetDate(value));}}
		[ExtendedTypeAttribute(RMExtType.Entity,"PHYSICIANS")]
        public int SurgeonEid{get{return GetFieldInt("SURGEON_EID");}set{SetField("SURGEON_EID",value);}}
		public bool AnesthAdminFlag{get{return GetFieldBool("ANESTH_ADMIN_FLAG");}set{SetField("ANESTH_ADMIN_FLAG",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"ANESTHESIA_TYPE")]
        public int AnesthTypeCode{get{return GetFieldInt("ANESTH_TYPE_CODE");}set{SetField("ANESTH_TYPE_CODE",value);}}
		[ExtendedTypeAttribute(RMExtType.Entity,"ANESTHETISTS")]
        public int AnesthEid{get{return GetFieldInt("ANESTH_EID");}set{SetField("ANESTH_EID",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"ASA_PS_CLASS_CODE")]
        public int AsaPsClassCode{get{return GetFieldInt("ASA_PS_CLASS_CODE");}set{SetField("ASA_PS_CLASS_CODE",value);}}
		public string ComplicationDate{get{return GetFieldString("COMPLICATION_DATE");}set{SetField("COMPLICATION_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public bool PriorToAnesFlag{get{return GetFieldBool("PRIOR_TO_ANES_FLAG");}set{SetField("PRIOR_TO_ANES_FLAG",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"ICC_LEVEL_CODE")]
        public int IccLevelCode{get{return GetFieldInt("ICC_LEVEL_CODE");}set{SetField("ICC_LEVEL_CODE",value);}}
		public double LenOfTime{get{return GetFieldDouble("LEN_OF_TIME");}set{SetField("LEN_OF_TIME",value);}}
		public string Complications{get{return GetFieldString("COMPLICATIONS");}set{SetField("COMPLICATIONS",value);}}
		#endregion
		

		internal PatientXProcedure(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "PATIENT_PROCEDURE";
			this.m_sKeyField = "PROC_ROW_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
			//			base.InitChildren(sChildren);
			this.m_sParentClassName = "Patient";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
		#region Supplemental Fields Exposed
		internal override string OnBuildSuppTableName()
		{
			return "PATIENT_PROC_SUPP";
		}

		//Supplementals are implemented in the base class but not exposed externally.
		//This allows the derived class to control whether the object appears to have
		//supplementals or not. 
		//Entity exposes Supplementals.
		public new Supplementals Supplementals
		{	
			get
			{
				return base.Supplementals;
			}
		}
		#endregion

	}
}