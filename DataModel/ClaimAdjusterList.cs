using System;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forClaimAdjusterList.
	/// </summary>
	public class ClaimAdjusterList : DataCollection
	{
		internal ClaimAdjusterList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"ADJ_ROW_ID";
			this.SQLFromTable =	"CLAIM_ADJUSTER";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "ClaimAdjuster";
		}
		public new ClaimAdjuster this[int keyValue]{get{return base[keyValue] as ClaimAdjuster;}}
		public new ClaimAdjuster AddNew(){return base.AddNew() as ClaimAdjuster;}
		public  ClaimAdjuster Add(ClaimAdjuster obj){return base.Add(obj) as ClaimAdjuster;}
		public new ClaimAdjuster Add(int keyValue){return base.Add(keyValue) as ClaimAdjuster;}
		
		
		//Find the current adjuster
		public ClaimAdjuster GetCurrentAdjuster()
		{
			if(this.SQLFilter=="")
				return null;
				//	throw new DataModelException(Globalization.GetString("ClaimAdjusterList.GetCurrentAdjuster.Exception.CollectionNotReady"));

			if(this.m_keySet.Count==0)
				return this.AddNew();

			int key = Context.DbConnLookup.ExecuteInt("SELECT ADJ_ROW_ID FROM CLAIM_ADJUSTER WHERE NOT CURRENT_ADJ_FLAG IS NULL AND CURRENT_ADJ_FLAG !=0 AND  " + this.SQLFilter);
			
			if(key !=0)
				return this[key];
			else
			{ //Attempt to pick the last adjuster added.
				int maxRowId=99999; 
				foreach(int curKey in this.m_keySet.Keys)
					if(maxRowId==99999 || curKey>maxRowId)
						maxRowId=curKey;
				return this[maxRowId];
			}
		}

		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}