using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for EntityXOperatingAsList.
	/// </summary>
	public class EntityXOperatingAsList : DataCollection
	{
		internal EntityXOperatingAsList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"OPERATING_ID";
			this.SQLFromTable =	"ENT_X_OPERATINGAS";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "EntityXOperatingAs";
		}
		public new EntityXOperatingAs this[int keyValue]{get{return base[keyValue] as EntityXOperatingAs;}}
		public new EntityXOperatingAs AddNew(){return base.AddNew() as EntityXOperatingAs;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}