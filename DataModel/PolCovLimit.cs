﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("POL_COV_LIMIT", "LIMIT_ROW_ID")]
    public class PolCovLimit : DataObject
    {
        //public static string ParentKeyFieldName = "PARENT_ID";
        //public static string ParentNameFieldName = "PARENT_NAME";
        #region Database Field List
        private string[,] sFields = {
														{"LimitRowId", "LIMIT_ROW_ID"},
														{"PolicyKey", "POLICY_KEY"},
														{"CoverageKey", "COVERAGE_KEY"},
														{"LimitTypeCode", "LIMIT_TYPE_CODE"},
														{"LimitAmount", "LIMIT_AMOUNT"},
                                                        {"AddedByUser","ADDED_BY_USER"},
														{"UpdatedByUser","UPDATED_BY_USER"},
														{"DttmRcdAdded","DTTM_RCD_ADDED"},
														{"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"}
													
		};
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }

        [ExtendedTypeAttribute(RMExtType.Code, "LIMIT_TYPE")]
        public int LimitTypeCode { get { return GetFieldInt("LIMIT_TYPE_CODE"); } set { SetField("LIMIT_TYPE_CODE", value); } }
        public int LimitRowId { get { return GetFieldInt("LIMIT_ROW_ID"); } set { SetField("LIMIT_ROW_ID", value); } }


        public string PolicyKey { get { return GetFieldString("POLICY_KEY"); } set { SetField("POLICY_KEY", value); } }
        public string CoverageKey { get { return GetFieldString("COVERAGE_KEY"); } set { SetField("COVERAGE_KEY", value); } }
        public double LimitAmount { get { return GetFieldDouble("LIMIT_AMOUNT"); } set { SetField("LIMIT_AMOUNT", value); } }
       
        #endregion

        internal PolCovLimit(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }
        new private void Initialize()
        {


            this.m_sTableName = "POL_COV_LIMIT";
            this.m_sKeyField = "LIMIT_ROW_ID";
   

            base.InitFields(sFields);
            
           

            base.Initialize(); 

        }

    }
}
