﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Riskmaster.Db;
using Riskmaster.Common;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("ADDRESS", "ADDRESS_ID")]
    public class Address:DataObject
    {
        //JIRA RMA-8753 nshah28 start
        #region Database Field List
        private string[,] sFields ={
                                         { "AddressId","ADDRESS_ID"},
                                         { "Addr1", "ADDR1"}, 
                                         { "Addr2", "ADDR2"},
                                         { "Addr3", "ADDR3"}, 
                                         { "Addr4", "ADDR4"},
                                         { "City", "CITY"}, 
                                         { "County", "COUNTY"},
                                         { "Country", "COUNTRY_CODE"}, 
                                         { "State", "STATE_ID"}, 
                                         { "ZipCode", "ZIP_CODE"},
                                         { "AddedByUser", "ADDED_BY_USER"},
                                         { "UpdatedByUser", "UPDATED_BY_USER"},
                                         { "DttmRcdAdded", "DTTM_RCD_ADDED"},
                                         { "DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
                                         { "SearchString", "SEARCH_STRING"},
                                         {"AddressSeqNum","ADDRESS_SEQ_NUM"},
                                         {"LegacyUniqueIdentifier","LEGACY_UNIQUE_IDENTIFIER"},
                                         {"DeletedFlag","DELETED_FLAG"} //RMA-14248(Part of RMA-8753 addr master)
                                         
                                 };
        public string LegacyUniqueIdentifier { get { return GetFieldString("LEGACY_UNIQUE_IDENTIFIER"); } set { SetField("LEGACY_UNIQUE_IDENTIFIER", value); } }
        public int AddressId { get { return GetFieldInt("ADDRESS_ID"); } set { SetField("ADDRESS_ID", value); } }
        public int EntityId { get { return GetFieldInt("ENTITY_ID"); } set { SetField("ENTITY_ID", value); } }
        public string Addr1 { get { return GetFieldString("ADDR1"); } set { SetField("ADDR1", value); } }
        public string Addr2 { get { return GetFieldString("ADDR2"); } set { SetField("ADDR2", value); } }
        public string Addr3 { get { return GetFieldString("ADDR3"); } set { SetField("ADDR3", value); } }
        public string Addr4 { get { return GetFieldString("ADDR4"); } set { SetField("ADDR4", value); } }
        public string City { get { return GetFieldString("CITY"); } set { SetField("CITY", value); } }
        public string County { get { return GetFieldString("COUNTY"); } set { SetField("COUNTY", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "COUNTRY")]
        public int Country { get { return GetFieldInt("COUNTRY_CODE"); } set { SetField("COUNTRY_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "STATES")]
        public int State { get { return GetFieldInt("STATE_ID"); } set { SetField("STATE_ID", value); } }
        public string ZipCode { get { return GetFieldString("ZIP_CODE"); } set { SetField("ZIP_CODE", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string SearchString { get { return GetFieldString("SEARCH_STRING"); } set { SetField("SEARCH_STRING", value); } }
        public int AddressSeqNum { get { return GetFieldInt("ADDRESS_SEQ_NUM"); } set { SetField("ADDRESS_SEQ_NUM",value); } }

        public bool DeletedFlag { get { return GetFieldBool("DELETED_FLAG"); } set { SetField("DELETED_FLAG", value); } }  //RMA-14248(Part of RMA-8753 addr master)
        #endregion

        public string Isdupeoverride { get; set; } // RMA-8753 for Dup popup(RMA-14262) nshah28

        internal Address(bool isLocked, Context context): base(isLocked, context)
        {
            this.Initialize();
        }
        public override void Save()
        {
            Context.OnPostTransCommit += new Riskmaster.DataModel.Context.PostTransCommitHandler(UpdateEntityAddresses);
			base.Save();
        }
        public override void Refresh()
        {
            int l = this.AddressId;
            //				int l1 = m_PiRowId;
            int lEID = this.AddressId;

            base.Refresh();

            this.AddressId = l;
            //				m_PiRowId = l1;
            this.AddressId = lEID;
        }

        private void UpdateEntityAddresses()
        { 
            string sSQL = string.Empty;
            //string sEntityID = string.Empty;
            //DbReader objReader = null;
            //bool bSuccess = false;
            //List<int> lstEntityID = new List<int>();
            //Entity objEntity = null;

            sSQL = String.Format("UPDATE ENTITY SET ADDR1 = '{0}',ADDR2='{1}',ADDR3 = '{2}',ADDR4 = '{3}',CITY='{4}',STATE_ID='{5}',COUNTRY_CODE='{6}',COUNTY='{7}',ZIP_CODE='{8}' WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM ENTITY_X_ADDRESSES WHERE ADDRESS_ID =" + this.AddressId + " AND PRIMARY_ADD_FLAG = -1)",this.Addr1,this.Addr2,this.Addr3,this.Addr4,this.City,this.State,this.Country,this.County,this.ZipCode);
            Context.DbConn.ExecuteNonQuery(sSQL, Context.DbTrans);

            //objReader = DbFactory.GetDbReader(this.Context.DbConn.ConnectionString, sSQL);

            //if (objReader != null)
            //{
            //    while (objReader.Read())
            //    {
            //        sEntityID = Convert.ToString(objReader.GetValue("ENTITY_ID"));
            //        if (objReader.GetValue("ENTITY_ID") != null)
            //        {
            //            lstEntityID.Add(Conversion.CastToType<int>(sEntityID, out bSuccess));
            //        }
            //    }
            //    objReader.Close();
            //}

            //foreach (int iEntityId in lstEntityID)
            //{ 
            //    objEntity = (Entity)this.Context.Factory.GetDataModelObject("Entity", false);

            //    objEntity.MoveTo(iEntityId);
               
            //    objEntity.Addr1 = this.Addr1;
            //    objEntity.Addr2 = this.Addr2;
            //    objEntity.Addr3 = this.Addr3;
            //    objEntity.Addr4 = this.Addr4;
            //    objEntity.City = this.City;
            //    objEntity.StateId = this.State;
            //    objEntity.CountryCode = this.Country;
            //    objEntity.County = this.County;
            //    objEntity.ZipCode = this.ZipCode;
            //    objEntity.FormName = "EntityMaintForm";
            //    objEntity.Save();
            //    objEntity.Dispose();

            //}

        }
       new private void Initialize()
		{

            this.m_sTableName = "ADDRESS";
            this.m_sKeyField = "ADDRESS_ID";
			this.m_sFilterClause = "";

            base.InitFields(sFields);
            this.m_sParentClassName = "";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}

       //RMA-14248(Part of RMA-8753 addr master,Delete functionality)
       internal override string OnBuildDeleteSQL()
       {
           if (this.Parent == null)
               return string.Format(" UPDATE ADDRESS SET DELETED_FLAG = -1, DTTM_RCD_LAST_UPD= '{0}', UPDATED_BY_USER='{1}' WHERE ADDRESS_ID={2}", Common.Conversion.ToDbDateTime(DateTime.Now), Context.RMUser.LoginName, AddressId); 
          else
              return string.Format(" UPDATE ADDRESS SET DELETED_FLAG = 0 WHERE ADDRESS_ID={0}", AddressId); 
       }
    }
}
