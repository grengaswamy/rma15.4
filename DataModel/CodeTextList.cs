using System;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;


namespace Riskmaster.DataModel
{
	/// <summary>
	/// The base class for an iteratable collection used to represent 1 to many record 
	/// relationships from the parent side.
	/// </summary>
	public class CodeTextList : DataRoot, IEnumerable
	{
		internal Hashtable m_values = new Hashtable();
		private Hashtable m_isDeleted = new Hashtable();
		//		private Hashtable m_isNew = new Hashtable();

		private string m_itemTypeName="CodeTextItem";
/*		private string m_SQLFilter = "";
		private string m_SQLFromTable = "CODES_TEXT";
		private string m_SQLKeyColumn = "";

		private int m_newItemCounter = 0;
		
		private bool m_BulkLoadFlag = false;
		private void SwapKey(int origKeyValue, int newKeyValue)
		{
			object objTemp = null;
			objTemp  = m_isDeleted[origKeyValue];
			m_isDeleted.Remove(origKeyValue);
			m_isDeleted.Add(newKeyValue,objTemp);
			
			objTemp  = m_keySet[origKeyValue];
			m_keySet.Remove(origKeyValue);
			m_keySet.Add(newKeyValue, objTemp);
		}
		protected virtual string  DerivedTypeNameFromKey(int keyValue)
		{
			return m_itemTypeName;
		}

		//		internal event OnChildItemAddedEventHandler OnChildItemAdded;

		/// <summary>
		/// Checks if the following information has been provided:
		///   1.) What datatype to use.
		///   2.) How to select the collection member rows.
		///   3.) What table the rows are in.
		///   4.) What field is the table key.
		/// </summary>
		/// <returns></returns>
		internal bool IsReady()
		{
			if(m_SQLKeyColumn =="")
				return false;
			if(m_SQLFromTable =="")
				return false;
			if(this.Parent != null)
				if((this.Parent as DataObject).IsNew)
					m_SQLFilter = "1 = 0"; //Fake filter to force an empty keyset but allow us to be considered "Ready"
			if(m_SQLFilter =="")
				return false;
			if(m_itemTypeName =="")
				return false;
			if(m_itemTypeName =="DataObject")
				return false;

			return true;
		}
*/
		internal CodeTextList(bool isLocked,Context context):base(isLocked, context){(this as IDataModel).Initialize(isLocked, context);}
	/*	public CodeTextItem this[int keyValue]
		{
			get
			{
				//Check for is the collection Prepared? (knows what datatype to use and how to select the collection member rows.)
				if(! IsReady())
					if(!this.IsNew)
						throw new DataModelException(Globalization.GetString("DataCollection.this[].get.Exception.CollectionNotReady"));

				//(Allow the first one to be created "on demand.")
				if(m_keySet.Count == 0)
					this.Add(keyValue);

				//Check for keyValue in this collection. (Allow the first one to be created "on demand.")
				if(!m_keySet.ContainsKey(keyValue) )
					return null;
				
				//Check for keyValue has been deleted from this collection.
				if(IsDeleted(keyValue))
					return null;
				
				//Fetch Cached Instance if any.
				DataObject obj = (m_keySet[keyValue] as DataObject);
				
				// Child not yet loaded\cached
				if (obj == null)
				{
					obj = (this.Context.Factory.GetDataModelObject(DerivedTypeNameFromKey(keyValue),true) as DataObject);
					obj.Parent = this.Parent;
					try
					{
						(obj as INavigation).MoveTo(keyValue);
					}
					catch(RecordNotFoundException e) //Catch if someone deleted our member object, just add it to the "deleted" list???
					{
						m_isDeleted[keyValue] = true;
						obj = null;
					}
					m_keySet[keyValue]=obj;
				}
				
				return obj;
			}
		}*/
		public virtual  CodeTextItem AddNew()
		{
			CodeTextItem obj= (this.Context.Factory.GetDataModelObject(m_itemTypeName,true) as CodeTextItem);
			obj.Parent = this.Parent;
			return Add(obj);
		}

		public CodeTextItem Add(CodeTextItem obj)
		{

			m_values.Add(obj.GetHashCode(),obj);
			base.m_DataChanged= true;
			if(this.Parent is Code)
				obj.CodeId = (this.Parent as Code).CodeId;
			return obj;
		}


	/*	internal string TypeName
		{
			get
			{
				return m_itemTypeName;
			}
			set
			{
				if(m_itemTypeName != value)
				{
					m_itemTypeName = value;
					Reset();
				}
			}
		}
		internal string SQLFilter
		{
			get
			{
				return m_SQLFilter;
			}
			set
			{
				if(m_SQLFilter != value)
				{
					m_SQLFilter = value;
					Reset();
				}
			}
		}		
		internal string SQLKeyColumn
		{
			get
			{
				return m_SQLKeyColumn;
			}
			set
			{
				if(m_SQLKeyColumn != value)
				{
					m_SQLKeyColumn = value;
					Reset();
				}
			}
		}
		internal string SQLFromTable
		{
			get
			{
				return m_SQLFromTable;
			}
			set
			{
				if(m_SQLFromTable != value)
				{
					m_SQLFromTable = value;
					Reset();
				}
			}
		}		
		internal bool BulkLoadFlag
		{
			get
			{
				return m_BulkLoadFlag;
			}
			set
			{
				m_BulkLoadFlag = value;
			}
		}
		private void Reset()
		{
			m_keySet = new Hashtable();
			m_isDeleted = new Hashtable();
			this.DataChanged = false;
			(this as IDataModel).IsStale = false;
			if(IsReady())
				LoadKeySet();
		}
		private bool IsDeleted(int keyValue){return (bool) m_isDeleted[keyValue];}
*/
		
		//BSB Note All add/remove f(x)'s are "in memory" only and persisted to the Database via a .Save() call.
		public bool Remove(int codeId, int langId)
		{
			bool bRet = false;
			foreach(CodeTextItem obj in this)
				if(obj.CodeId == codeId && obj.LanguageCode== langId)
				{
					Remove(obj);
					bRet = true;
				}
			return bRet;
		}
		public bool Remove(CodeTextItem obj)
		{
			if(m_values.Contains(obj.GetHashCode()))
				m_values.Remove(obj.GetHashCode());
			return true;
		}

//		private void LoadKeySet()
//		{
//			DbReader objReader = null;
//			DataObject objItem = null;
//			try
//			{
//
//
//				if(this.BulkLoadFlag) //Uses this single query to populate multiple objects.
//					objReader = this.Context.DbConnLookup.ExecuteReader(String.Format("SELECT * FROM {0} WHERE {1} ORDER BY {2}",SQLFromTable, SQLFilter, SQLKeyColumn));
//				else
//					objReader= this.Context.DbConnLookup.ExecuteReader(String.Format("SELECT {0} FROM {1} WHERE {2} ORDER BY {3}",SQLKeyColumn,SQLFromTable, SQLFilter, SQLKeyColumn));
//
//				while(objReader.Read())
//				{	
//					objItem = null;
//					if(this.BulkLoadFlag) //Uses this single query to populate multiple objects.
//					{
//						objItem = this.Context.Factory.GetDataModelObject(this.TypeName,true) as DataObject;
//						objItem.LoadData(objReader);
//					}
//					m_keySet.Add(objReader.GetInt(SQLKeyColumn.ToUpper()),objItem);
//					m_isDeleted.Add( objReader.GetInt(SQLKeyColumn.ToUpper()), false);
//				}
//			}
//			catch(Exception e){throw new DataModelException(Globalization.GetString("DataCollection.LoadKeySet.Exception"),e);}
//			finally
//			{
//				if (objReader != null)
//					objReader.Close();
//				objReader = null;
//			}
//		}
		private CodeTextItem  this[int hashValue]
		{
			get
			{
				if(m_values.Contains(hashValue))
					return (CodeTextItem) m_values[hashValue];
				else
					return (CodeTextItem) null;
			}
		}
		public int Count{	get{return this.m_values.Count;}
		}
		#region Enumeration Implementation
		IEnumerator IEnumerable.GetEnumerator(){return new CodeTextListEnumerator(this);}
		class CodeTextListEnumerator : IEnumerator
		{
			int m_iPos = -1;
			CodeTextList m_parent = null;
			object[] keys = null;

			internal CodeTextListEnumerator(CodeTextList parent)
			{
				m_parent = parent;
				keys = new object[m_parent.m_values.Keys.Count];
				m_parent.m_values.Keys.CopyTo(keys,0);
			}
			//TODO IMPLEMENT DataCollection Enum Class.
			public void Reset()
			{
				m_iPos =-1;
			}

			public object Current
			{
				get
				{
					if(m_iPos <0 || m_iPos >=keys.Length)
						return null;
					return m_parent[(int)keys[m_iPos]];
				}
			}

			public bool MoveNext()
			{
				m_iPos++;
				if(!(m_iPos <0 || m_iPos >=keys.Length))
                    return true;
				else
					return false;
//				while(!(m_iPos <0 || m_iPos >=keys.Length))
//					if(m_parent.IsDeleted((int)keys[m_iPos]))
//						m_iPos++;
//					else
//						return true;
				
//				return false;
			}
		}
		#endregion

		override public  bool DataChanged
		{
			get
			{
				if (base.m_DataChanged)
					return true;

				foreach(CodeTextItem obj in this)
					if(obj.DataChanged)
						return true;
				return false;
			}
			set
			{
				base.m_DataChanged= value;
				foreach(CodeTextItem obj in this)
					obj.DataChanged = value;
			}
		}

	
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(CodeTextItem obj in this)
				s += obj.ToString();
			return s;
		}
	}
}
