using System;
using System.Diagnostics;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPolicyXRtngEnhList.
	/// </summary>
	public class PolicyXRtngEnhList : DataCollection
	{
		internal PolicyXRtngEnhList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"RATING_ID";
			this.SQLFromTable =	"POLICY_X_RTNG_ENH";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PolicyXRtngEnh";
		}

        //public new PolicyXRtngEnh this[int keyValue] { get { return base[keyValue] as PolicyXRtngEnh; } }
        public override DataObject this[int keyValue]
        {
            get
            {
                DataObject objPolicyXRtngEnh = null;
                StackTrace stackTrace = new StackTrace();
                string sStackTrace = Convert.ToString(stackTrace);
                if (!base.m_keySet.ContainsKey(keyValue) && sStackTrace.Contains("Riskmaster.ActiveScript.ScriptHost.ExecuteMethod") && sStackTrace.Contains("Riskmaster.Application.FormProcessor.FormProcessor.ProcessForm"))
                {
                    int[] arrKeys = new int[base.m_keySet.Count];
                    base.m_keySet.Keys.CopyTo(arrKeys, 0);
                    objPolicyXRtngEnh = base[arrKeys[keyValue - 1]] as DataObject;
                }
                else
                {
                    objPolicyXRtngEnh = base[keyValue] as DataObject;
                }
                return objPolicyXRtngEnh as PolicyXRtngEnh;
                }
        }
		public new PolicyXRtngEnh AddNew(){return base.AddNew() as PolicyXRtngEnh;}
		public  PolicyXRtngEnh Add(PolicyXRtngEnh obj){return base.Add(obj) as PolicyXRtngEnh;}
		public new PolicyXRtngEnh Add(int keyValue){return base.Add(keyValue) as PolicyXRtngEnh;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}