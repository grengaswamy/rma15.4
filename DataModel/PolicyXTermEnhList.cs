
using System;
using System.Diagnostics;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPolicyXTermEnhList.
	/// </summary>
	public class PolicyXTermEnhList : DataCollection
	{
		internal PolicyXTermEnhList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"TERM_ID";
			this.SQLFromTable =	"POLICY_X_TERM_ENH";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PolicyXTermEnh";
		}

        //public new PolicyXTermEnh this[int keyValue] { get { return base[keyValue] as PolicyXTermEnh; } }

        public override DataObject this[int keyValue]
        {
            get
            {
                DataObject objPolicyXTermEnh = null;
                StackTrace stackTrace = new StackTrace();
                string sStackTrace = Convert.ToString(stackTrace);
                if (!base.m_keySet.ContainsKey(keyValue) &&  sStackTrace.Contains("Riskmaster.ActiveScript.ScriptHost.ExecuteMethod") && sStackTrace.Contains("Riskmaster.Application.FormProcessor.FormProcessor.ProcessForm"))
                {
                    int[] arrKeys = new int[base.m_keySet.Count];
                    base.m_keySet.Keys.CopyTo(arrKeys, 0);
                    objPolicyXTermEnh = base[arrKeys[keyValue - 1]] as DataObject;
                }
                else
                {
                    objPolicyXTermEnh = base[keyValue] as DataObject;
                }
                return objPolicyXTermEnh as PolicyXTermEnh;
            }
        }

		public new PolicyXTermEnh AddNew(){return base.AddNew() as PolicyXTermEnh;}
		public  PolicyXTermEnh Add(PolicyXTermEnh obj){return base.Add(obj) as PolicyXTermEnh;}
		public new PolicyXTermEnh Add(int keyValue){return base.Add(keyValue) as PolicyXTermEnh;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}