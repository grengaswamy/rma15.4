using System;
using System.Diagnostics;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPolicyXCvgEnhList.
	/// </summary>
	public class PolicyXCvgEnhList : DataCollection
	{
		internal PolicyXCvgEnhList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"POLCVG_ROW_ID";
			this.SQLFromTable =	"POLICY_X_CVG_ENH";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PolicyXCvgEnh";
		}
		//public new PolicyXCvgEnh this[int keyValue]{get{return base[keyValue] as PolicyXCvgEnh;}}

        public override DataObject this[int keyValue]
        {
            get
            {
                DataObject objPolicyXCvgEnh = null;
                StackTrace stackTrace = new StackTrace();
                string sStackTrace = Convert.ToString(stackTrace);
                if (!base.m_keySet.ContainsKey(keyValue) && sStackTrace.Contains("Riskmaster.ActiveScript.ScriptHost.ExecuteMethod") && sStackTrace.Contains("Riskmaster.Application.FormProcessor.FormProcessor.ProcessForm"))
                {
                    int[] arrKeys = new int[base.m_keySet.Count];
                    base.m_keySet.Keys.CopyTo(arrKeys, 0);
                    objPolicyXCvgEnh = base[arrKeys[keyValue - 1]] as DataObject;
                }
                else
                {
                    objPolicyXCvgEnh = base[keyValue] as DataObject;
                }
                return objPolicyXCvgEnh as PolicyXCvgEnh;
            }
        }
		public new PolicyXCvgEnh AddNew(){return base.AddNew() as PolicyXCvgEnh;}
		public  PolicyXCvgEnh Add(PolicyXCvgEnh obj){return base.Add(obj) as PolicyXCvgEnh;}
		public new PolicyXCvgEnh Add(int keyValue){return base.Add(keyValue) as PolicyXCvgEnh;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}