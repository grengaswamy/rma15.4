﻿using System;
using System.Collections.Generic;

using System.Text;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("POLICY_X_INSLINE_GROUP", "POLICY_X_INSLINE_GROUP_ID")]
    public class PolicyXInslineGroup : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
                                        {"POlicyXInslineGroupId", "POLICY_X_INSLINE_GROUP_ID"},
                                        {"PolicySystemId", "POLICY_SYSTEM_ID"},
                                        {"PolicyNumber", "POLICY_NUMBER"},
                                        {"PolicySymbol", "POLICY_SYMBOL"},
                                        {"Module", "MODULE"},
                                        {"MasterCompany", "MASTER_COMPANY"},
                                        {"LocationCompany", "LOCATION_COMPANY"},
                                        {"InsLine", "INS_LINE"},
                                        {"ProdLine", "PRODLINE"},
                                        {"AslLine", "ASLINE"},
                                        {"Coverage", "COVERAGE_CODE"},
                                        {"CovGroupId", "COV_GROUP_CODE"},
                                        {"DttmRcdAdded", "DTTM_RCD_ADDED"},
                                        {"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
                                        {"AddedByUser", "ADDED_BY_USER"},
                                        {"UpdatedByUser", "UPDATED_BY_USER"},
                                        {"UnitStateRowId","UNIT_STATE_ROW_ID"},
                                        {"ClaimLineOfBusiness", "CLAIM_LINE_BUS_CODE"},
                                        {"StatUnitNumber", "STAT_UNIT_NUMBER"},
                                        {"CoverageClassCode", "COVERAGE_CLASS_CODE"},
                                        {"CoverageSubLineCode", "COVERAGE_SUBLINE_CODE"}
                                    };

        public int PolicyXInslineGroupId { get { return GetFieldInt("POLICY_X_INSLINE_GROUP_ID"); } set { SetField("POLICY_X_INSLINE_GROUP_ID", value); } }
        public int PolicySystemId { get { return GetFieldInt("POLICY_SYSTEM_ID"); } set { SetField("POLICY_SYSTEM_ID", value); } }
        public string PolicyNumber { get { return GetFieldString("POLICY_NUMBER"); } set { SetField("POLICY_NUMBER", value); } }
        public string PolicySymbol { get { return GetFieldString("POLICY_SYMBOL"); } set { SetField("POLICY_SYMBOL", value); } }
        public string Module { get { return GetFieldString("MODULE"); } set { SetField("MODULE", value); } }
        public string MasterCompany { get { return GetFieldString("MASTER_COMPANY"); } set { SetField("MASTER_COMPANY", value); } }
        public string LocationCompany { get { return GetFieldString("LOCATION_COMPANY"); } set { SetField("LOCATION_COMPANY", value); } }
        public string InsLine { get { return GetFieldString("INS_LINE"); } set { SetField("INS_LINE", value); } }
        public string ProdLine { get { return GetFieldString("PRODLINE"); } set { SetField("PRODLINE", value); } }
        public string AslLine { get { return GetFieldString("ASLINE"); } set { SetField("ASLINE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "COVERAGE_TYPE")]
        public int Coverage { get { return GetFieldInt("COVERAGE_CODE"); } set { SetField("COVERAGE_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "COV_GROUP_CODE")]
        public int CovGroupId { get { return GetFieldInt("COV_GROUP_CODE"); } set { SetField("COV_GROUP_CODE", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public int ClaimLineOfBusiness { get { return GetFieldInt("CLAIM_LINE_BUS_CODE"); } set { SetField("CLAIM_LINE_BUS_CODE", value); } }
        public string StatUnitNumber { get { return GetFieldString("STAT_UNIT_NUMBER"); } set { SetField("STAT_UNIT_NUMBER", value); } }
        public string CoverageClassCode { get { return GetFieldString("COVERAGE_CLASS_CODE"); } set { SetField("COVERAGE_CLASS_CODE", value); } }
        public string CoverageSubLineCode { get { return GetFieldString("COVERAGE_SUBLINE_CODE"); } set { SetField("COVERAGE_SUBLINE_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "STATES")]
        public int UnitStateRowId { get { return GetFieldInt("UNIT_STATE_ROW_ID"); } set { SetField("UNIT_STATE_ROW_ID", value); } }
       
        #endregion

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
        }

        public override void Save()
        {
            base.Save();
        }

        internal PolicyXInslineGroup(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }

        new private void Initialize()
        {
            this.m_sTableName = "POLICY_X_INSLINE_GROUP";
            this.m_sKeyField = "POLICY_X_INSLINE_GROUP_ID";
            this.m_sFilterClause = "";

            base.InitFields(sFields);
            base.Initialize();

        }
    }
}
