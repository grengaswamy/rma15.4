
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forEmpXDependentList.
	/// </summary>
	public class EmpXDependentList : DataCollection
	{
		internal EmpXDependentList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"EMP_DEP_ROW_ID";
			this.SQLFromTable =	"EMP_X_DEPENDENT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "EmpXDependent";
		}
		public new EmpXDependent this[int keyValue]{get{return base[keyValue] as EmpXDependent;}}
		public new EmpXDependent AddNew(){return base.AddNew() as EmpXDependent;}
		public  EmpXDependent Add(EmpXDependent obj){return base.Add(obj) as EmpXDependent;}
		public new EmpXDependent Add(int keyValue){return base.Add(keyValue) as EmpXDependent;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}