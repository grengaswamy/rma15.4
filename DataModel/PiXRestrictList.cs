
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPiXRestrictList.
	/// </summary>
	public class PiXRestrictList : DataCollection
	{
		internal PiXRestrictList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"PI_RESTRICT_ROW_ID";
			this.SQLFromTable =	"PI_X_RESTRICT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PiXRestrict";
		}
		public new PiXRestrict this[int keyValue]{get{return base[keyValue] as PiXRestrict;}}
		public new PiXRestrict AddNew(){return base.AddNew() as PiXRestrict;}
		public  PiXRestrict Add(PiXRestrict obj){return base.Add(obj) as PiXRestrict;}
		public new PiXRestrict Add(int keyValue){return base.Add(keyValue) as PiXRestrict;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}