﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    public class EntityXAddressesList:DataCollection
    {
        internal EntityXAddressesList(bool isLocked, Context context): base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
            //this.SQLKeyColumn = "ADDRESS_ID";
            this.SQLKeyColumn = "ROW_ID"; //RMA-8753 nshah28(commenting "ADDRESS_ID" and introduce this)
            this.SQLFromTable = "ENTITY_X_ADDRESSES";
            this.TypeName = "EntityXAddresses";
		}
        public new EntityXAddresses this[int keyValue] { get { return base[keyValue] as EntityXAddresses; } }
        public new EntityXAddresses AddNew() { return base.AddNew() as EntityXAddresses; }
        public EntityXAddresses Add(EntityXAddresses obj) { return base.Add(obj) as EntityXAddresses; }
        public new EntityXAddresses Add(int keyValue) { return base.Add(keyValue) as EntityXAddresses; }
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
    }
}
