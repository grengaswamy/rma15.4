﻿using System;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Add by skhare7 for policyInterface
    
    /// </summary>

    [Riskmaster.DataModel.Summary("POLICY_X_ENTITY", "POLICYENTITY_ROWID", "PolicyEid")]
    public class PolicyXEntity : DataObject
    {
        #region Database Field List
        private string[,] sFields = {   
                                        {"EntityId","ENTITY_ID"},
                                       {"PolicyId","POLICY_ID"},
                                        {"PolicyEid","POLICYENTITY_ROWId"},
                                        {"TypeCode","TYPE_CODE"},
                                          {"PolicyUnitRowid","POLICY_UNIT_ROW_ID"},
                                          {"ExternalRole","EXTERNAL_ROLE"},
                                          // psharma Jira 12759
										  {"AddAsClaimant","ADD_AS_CLAIMANT"},    
                                          // mits 35925 start
                                          {"AddressId","ADDRESS_ID"}
                                          //mits 35925 end
                                    };
        public int EntityId { get { return GetFieldInt("ENTITY_ID"); } set { SetField("ENTITY_ID", value); } }
        public int PolicyId { get { return GetFieldInt("POLICY_ID"); } set { SetField("POLICY_ID", value); } }

        public int PolicyEid { get { return GetFieldInt("POLICYENTITY_ROWId"); } set { SetField("POLICYENTITY_ROWId", value); } }
        public int TypeCode { get { return GetFieldInt("TYPE_CODE"); } set { SetField("TYPE_CODE", value); } }
        public int PolicyUnitRowid { get { return GetFieldInt("POLICY_UNIT_ROW_ID"); } set { SetField("POLICY_UNIT_ROW_ID", value); } }
        public string ExternalRole { get { return GetFieldString("EXTERNAL_ROLE"); } set { SetField("EXTERNAL_ROLE", value); } }
       // mits 35925 start
         public int AddressId { get { return GetFieldInt("ADDRESS_ID"); } set { SetField("ADDRESS_ID",value); } }
        //mits 35925 end
		public bool AddAsClaimant { get { return GetFieldBool("ADD_AS_CLAIMANT"); } set { SetField("ADD_AS_CLAIMANT", value); } }
        #endregion
        private string[,] sChildren = { 
                                      { "PsDownloadXMLDataList", "PsDownloadXMLDataList" }
                                     };

        public PsDownloadXMLDataList PsDownloadXMLDataList
        {
            get
            {
                PsDownloadXMLDataList objList = base.m_Children["PsDownloadXMLDataList"] as PsDownloadXMLDataList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = PsDownloadXMLData.ParentKeyFieldName + "=" + this.KeyFieldValue + " AND " + PsDownloadXMLData.ParentNameFieldName + "=" + this.Context.LocalCache.GetTableId(this.Table);
                return objList;
            }
        }

        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            switch (childName)
            {
                case "PsDownloadXMLDataList":
                    (itemValue as PsDownloadXMLData).TableRowID = this.PolicyEid;
                    (itemValue as PsDownloadXMLData).TableID = this.Context.LocalCache.GetTableId(typeof(PolicyXEntity).Name);
                    break;
               
            }
            base.OnChildItemAdded(childName, itemValue);
        }

        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            switch (childName)
            {

                case "PsDownloadXMLDataList":
                    if (isNew)
                        foreach (PsDownloadXMLData item in (childValue as PsDownloadXMLDataList))
                        {
                            item.TableRowID = this.PolicyEid;
                            item.TableID = this.Context.LocalCache.GetTableId(this.Table);
                        }
                    (childValue as IPersistence).Save();
                    break;
               

            }
            base.OnChildPostSave(childName, childValue, isNew);
        }


        internal PolicyXEntity(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }
        new private void Initialize()
        {
            //Moved after most init logic so that scripting can be called successfully.
            // base.Initialize();  
            this.m_sTableName = "POLICY_X_ENTITY";
            this.m_sKeyField = "POLICYENTITY_ROWID";
            this.m_sParentClassName = "Policy";
            this.m_sFilterClause = string.Empty;
                     base.InitFields(sFields);
                     base.InitChildren(sChildren);
            base.Initialize();
        }
    }
}
