
using System;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary of PolicyXBillEnh Class.
	/// Don't forget to add children and custom override behaviors if necessary.
	/// </summary>
    [Riskmaster.DataModel.Summary("POLICY_X_BILL_ENH", "POLICY_BILLING_ID", "PolicyBillingId")]
	public class PolicyXBillEnh  : DataObject	
	{
		#region Database Field List
		private string[,] sFields = {
														{"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
														{"UpdatedByUser", "UPDATED_BY_USER"},
														{"DttmRcdAdded", "DTTM_RCD_ADDED"},
														{"AddedByUser", "ADDED_BY_USER"},
														{"PolicyBillingId", "POLICY_BILLING_ID"},
														{"PolicyId", "POLICY_ID"},
														{"PayPlanRowId", "PAY_PLAN_ROWID"},
														{"BillToEid", "BILL_TO_EID"},
														{"BillToType", "BILL_TO_TYPE"},
														{"BillToOverride", "BILL_TO_OVERRIDE"},
														{"NewBillToEid", "NEW_BILL_TO_EID"},
														{"TermNumber", "TERM_NUMBER"},
														{"TransactionId", "TRANSACTION_ID"},
														{"BillingRuleRowId", "BILLING_RULE_ROWID"},
                                                        {"DoNotBill", "DONOTBILL"},
};

		public string DttmRcdLastUpd{get{return GetFieldString("DTTM_RCD_LAST_UPD");}set{SetField("DTTM_RCD_LAST_UPD",value);}}
		public string UpdatedByUser{get{return GetFieldString("UPDATED_BY_USER");}set{SetField("UPDATED_BY_USER",value);}}
		public string DttmRcdAdded{get{return GetFieldString("DTTM_RCD_ADDED");}set{SetField("DTTM_RCD_ADDED",value);}}
		public string AddedByUser{get{return GetFieldString("ADDED_BY_USER");}set{SetField("ADDED_BY_USER",value);}}
		public int PolicyBillingId{get{return GetFieldInt("POLICY_BILLING_ID");}set{SetField("POLICY_BILLING_ID",value);}}
		public int PolicyId{get{return GetFieldInt("POLICY_ID");}set{SetField("POLICY_ID",value);}}
		public int PayPlanRowId{get{return GetFieldInt("PAY_PLAN_ROWID");}set{SetField("PAY_PLAN_ROWID",value);}}
        public int BillToEid { get { return GetFieldInt("BILL_TO_EID"); } set {SetFieldAndNavTo("BILL_TO_EID", value,"InsuredEntity"); } }
        // Start Naresh Fetch the Code Description for Bill To Type
        [ExtendedTypeAttribute(RMExtType.Code, "BILL_TO_TYPE")]
		public int BillToType{get{return GetFieldInt("BILL_TO_TYPE");}set{SetField("BILL_TO_TYPE",value);}}
        // End Naresh Fetch the Code Description for Bill To Type
		public bool BillToOverride{get{return GetFieldBool("BILL_TO_OVERRIDE");}set{SetField("BILL_TO_OVERRIDE",value);}}
        public int NewBillToEid { get { return GetFieldInt("NEW_BILL_TO_EID"); } set { SetFieldAndNavTo("NEW_BILL_TO_EID", value, "NewInsuredEntity"); } }
		public int TermNumber{get{return GetFieldInt("TERM_NUMBER");}set{SetField("TERM_NUMBER",value);}}
		public int TransactionId{get{return GetFieldInt("TRANSACTION_ID");}set{SetField("TRANSACTION_ID",value);}}
		public int BillingRuleRowId{get{return GetFieldInt("BILLING_RULE_ROWID");}set{SetField("BILLING_RULE_ROWID",value);}}
        public bool DoNotBill { get { return GetFieldBool("DONOTBILL"); } set { SetField("DONOTBILL", value); } }
#endregion
		

		#region Child Implementation
		private string[,] sChildren = { 	{"InsuredEntity","Entity"},
											{"NewInsuredEntity","Entity"} };
		
		internal override void LoadData(Riskmaster.Db.DbReader objReader)
		{
			base.LoadData (objReader);

			// TODO - Discuss the following code 

			//BSB Fix for AutoNav Child(ren) Not Loaded before pre-save.
			//this.CustomerEid= this.CustomerEid;
			//this.WorkPerfByEid = this.WorkPerfByEid;

            // BSB04.17.2006 The above code was out of date.
            // The current implementation is shown here.
            // this.SetFieldAndNavTo("CUSTOMER_EID", this.CustomerEid, "CustomerEntity", true);
            // this.SetFieldAndNavTo("WORK_PERF_BY_EID", this.WorkPerfByEid, "WorkPerfByEntity", true);

            this.SetFieldAndNavTo("BILL_TO_EID", this.BillToEid, "InsuredEntity", true);
            this.SetFieldAndNavTo("NEW_BILL_TO_EID", this.NewBillToEid, "NewInsuredEntity", true);

		}
		//Handle any sub-object(s) for which we need the key in our own table.
		internal override void OnChildPreSave(string childName, IDataModel childValue)
		{
			// If Entity is involved we save this first (in case we need the EntityId.)
			if(childName=="InsuredEntity")
			{
				(childValue as IPersistence).Save();
				this.m_Fields["BILL_TO_EID"]= (childValue as Entity).EntityId;
			}
			if(childName=="NewInsuredEntity")
			{
				(childValue as IPersistence).Save();
				this.m_Fields["NEW_BILL_TO_EID"]= (childValue as Entity).EntityId;
			}
		}
				
		//Protect Entities that should not be deleted.
		internal override void OnChildDelete(string childName, IDataModel childValue)
		{
			if(childName == "InsuredEntity")
				return;
			if(childName == "NewInsuredEntity")
				return;
			base.OnChildDelete (childName, childValue);
		}


		//Child Property Accessors
		public Entity InsuredEntity
		{
			get
			{
				Entity objItem = base.m_Children["InsuredEntity"] as Entity;
                if ((objItem as IDataModel).IsStale)
                    objItem = this.CreatableMoveChildTo("InsuredEntity", (objItem as DataObject), this.BillToEid) as Entity;

				return objItem;
			}
		}	
		public Entity NewInsuredEntity
		{
			get
			{
				Entity objItem = base.m_Children["NewInsuredEntity"] as Entity;
                objItem = this.CreatableMoveChildTo("NewInsuredEntity", (objItem as DataObject), this.NewBillToEid) as Entity;
                return objItem;
			}
		}	
		
		#endregion

		internal PolicyXBillEnh(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{
			this.m_sTableName = "POLICY_X_BILL_ENH";
			this.m_sKeyField = "POLICY_BILLING_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
			base.InitChildren(sChildren);

			// Initialize base after init logic
			// so that scripting can be called successfully.

            this.m_sParentClassName = "PolicyEnh";
			base.Initialize();  
		}
	}
}