
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forFundsTransSplitList.
	/// </summary>
	public class FundsTransSplitList : DataCollection
	{
		internal FundsTransSplitList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"SPLIT_ROW_ID";
			this.SQLFromTable =	"FUNDS_TRANS_SPLIT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "FundsTransSplit";
		}
		public new FundsTransSplit this[int keyValue]{get{return base[keyValue] as FundsTransSplit;}}
		public new FundsTransSplit AddNew(){return base.AddNew() as FundsTransSplit;}
		public  FundsTransSplit Add(FundsTransSplit obj){return base.Add(obj) as FundsTransSplit;}
		public new FundsTransSplit Add(int keyValue){return base.Add(keyValue) as FundsTransSplit;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}