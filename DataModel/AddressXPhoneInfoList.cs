﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    public class AddressXPhoneInfoList:DataCollection
    {
        internal AddressXPhoneInfoList(bool isLocked, Context context): base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "PHONE_ID";
            this.SQLFromTable = "ADDRESS_X_PHONEINFO";
            this.TypeName = "AddressXPhoneInfo";
		}
        public new AddressXPhoneInfo this[int keyValue] { get { return base[keyValue] as AddressXPhoneInfo; } }
        public new AddressXPhoneInfo AddNew() { return base.AddNew() as AddressXPhoneInfo; }
        public AddressXPhoneInfo Add(AddressXPhoneInfo obj) { return base.Add(obj) as AddressXPhoneInfo; }
        public new AddressXPhoneInfo Add(int keyValue) { return base.Add(keyValue) as AddressXPhoneInfo; }
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
    }
}
