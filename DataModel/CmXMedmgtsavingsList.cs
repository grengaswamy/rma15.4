using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forCmXMedmgtsavingsList.
	/// </summary>
	public class CmXMedmgtsavingsList : DataCollection
	{
		internal CmXMedmgtsavingsList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"CMMS_ROW_ID";
			this.SQLFromTable =	"CM_X_MEDMGTSAVINGS";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "CmXMedmgtsavings";
		}
		public new CmXMedmgtsavings this[int keyValue]{get{return base[keyValue] as CmXMedmgtsavings;}}
		public new CmXMedmgtsavings AddNew(){return base.AddNew() as CmXMedmgtsavings;}
		public  CmXMedmgtsavings Add(CmXMedmgtsavings obj){return base.Add(obj) as CmXMedmgtsavings;}
		public new CmXMedmgtsavings Add(int keyValue){return base.Add(keyValue) as CmXMedmgtsavings;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}