
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forFundsAutoList.
	/// </summary>
	public class FundsAutoList : DataCollection
	{
		internal FundsAutoList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"AUTO_TRANS_ID";
			this.SQLFromTable =	"FUNDS_AUTO";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "FundsAuto";
		}
		public new FundsAuto this[int keyValue]{get{return base[keyValue] as FundsAuto;}}
		public new FundsAuto AddNew(){return base.AddNew() as FundsAuto;}
		public  FundsAuto Add(FundsAuto obj){return base.Add(obj) as FundsAuto;}
		public new FundsAuto Add(int keyValue){return base.Add(keyValue) as FundsAuto;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}