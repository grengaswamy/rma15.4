using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPolicyXMcoList.
	/// </summary>
	public class PolicyXMcoList : DataCollection
	{
		internal PolicyXMcoList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"POL_X_MCO_ROW_ID";
			this.SQLFromTable =	"POLICY_X_MCO";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PolicyXMco";
		}
		public new PolicyXMco this[int keyValue]{get{return base[keyValue] as PolicyXMco;}}
		public new PolicyXMco AddNew(){return base.AddNew() as PolicyXMco;}
		public  PolicyXMco Add(PolicyXMco obj){return base.Add(obj) as PolicyXMco;}
		public new PolicyXMco Add(int keyValue){return base.Add(keyValue) as PolicyXMco;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}