
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPiXDependentList.
	/// </summary>
	public class PiXDependentList : DataCollection
	{
		internal PiXDependentList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"PI_DEP_ROW_ID";
			this.SQLFromTable =	"PI_X_DEPENDENT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PiXDependent";
		}
		public new PiXDependent this[int keyValue]{get{return base[keyValue] as PiXDependent;}}
		public new PiXDependent AddNew(){return base.AddNew() as PiXDependent;}
		public  PiXDependent Add(PiXDependent obj){return base.Add(obj) as PiXDependent;}
		public new PiXDependent Add(int keyValue){return base.Add(keyValue) as PiXDependent;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}