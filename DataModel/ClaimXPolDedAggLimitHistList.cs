﻿using System;

namespace Riskmaster.DataModel
{
    public class ClaimXPolDedAggLimitHistList : DataCollection
    {
        internal ClaimXPolDedAggLimitHistList(bool isLocked, Context context)
            : base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
            this.SQLKeyColumn = "DED_AGG_LIMIT_HIST_ID";
            this.SQLFromTable = "CLAIM_X_POL_DED_AGG_LIMIT_HIST";

            this.TypeName = "ClaimXPolDedAggLimitHist";
		}

        public new ClaimXPolDedAggLimitHist this[int keyValue] { get { return base[keyValue] as ClaimXPolDedAggLimitHist; } }
        public new ClaimXPolDedAggLimitHist AddNew() { return base.AddNew() as ClaimXPolDedAggLimitHist; }
        public ClaimXPolDedAggLimitHist Add(ClaimXPolDedHist obj) { return base.Add(obj) as ClaimXPolDedAggLimitHist; }
        public new ClaimXPolDedAggLimitHist Add(int keyValue) { return base.Add(keyValue) as ClaimXPolDedAggLimitHist; }

        public ClaimXPolDedAggLimitHist LastStatusChange()
        {
            //BSB "fake SQL filter  1=0" may not have been set yet.
            // Note that SQL Filter is initialized to in order to indicate that the datacollection is not ready.
            string SQL = "SELECT MAX(DED_AGG_LIMIT_HIST_ID) FROM CLAIM_X_POL_DED_AGG_LIMIT_HIST ";
            if (this.SQLFilter == "")
                this.SQLFilter = " 1=0 ";
            SQL += "WHERE " + this.SQLFilter;
            int lastChangeId = (int)Context.DbConnLookup.ExecuteInt(SQL);
            if (lastChangeId == 0 && this.Count != 0)//Try to get "In Memory Only" item if available.
                foreach (object iKey in this.m_keySet.Keys)
                    lastChangeId = Math.Min((int)iKey, lastChangeId);
            return this[lastChangeId];
        }
    }
}
