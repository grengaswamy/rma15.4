using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Reflection;
using System.Reflection.Emit;

namespace Riskmaster.Tools
{
	/// <summary>
	/// Summary description for ConstructDataObjects.
	/// </summary>
	public class ConstructDataObjects : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ConstructDataObjects()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(72, 136);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(152, 32);
			this.button1.TabIndex = 0;
			this.button1.Text = "button1";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// ConstructDataObjects
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.button1});
			this.Name = "ConstructDataObjects";
			this.Text = "ConstructDataObjects";
			this.ResumeLayout(false);

		}
		#endregion

		private void button1_Click(object sender, System.EventArgs e)
		{
		
		}
		/*/////////////////////////////////////////////	
		/// Code Adapted from Microsoft SDK Example.
		/// Used to generate a DLLImport function for testing
		/// whether a file is a COM Dll.
		/// This must be generated at run-time using reflection
		/// since the file-name must be known at "compile time"
		/// otherwise.
		*/////////////////////////////////////////////
		
		private static bool CreateAndCall(string sTargetToCheck) 
		{
			AppDomain appDomain = Thread.GetDomain();
			AssemblyBuilderAccess access = AssemblyBuilderAccess.RunAndSave;

			// Create a simple name for the callee assembly.
			AssemblyName assemblyName = new AssemblyName();
			assemblyName.Name = "ProtoDataObjAssembly";
			
			// Create the callee dynamic assembly.
			AssemblyBuilder assembly = appDomain.DefineDynamicAssembly(assemblyName, access);
			
			// Create a dynamic module named "TransientModule" in the callee assembly.
			ModuleBuilder module = assembly.DefineDynamicModule("TransientModule");
			
			// Define a public class named "HelloWorld" in the assembly.
			//Note: Class is ALWAYS required even for our static method.
			TypeBuilder myClass = module.DefineType("TransientClass", TypeAttributes.Public);

			// Create the static PInvoke method.
			Type[] Params= {};
			MethodBuilder method = myClass.DefinePInvokeMethod(
				"IsComDll",
				sTargetToCheck,
				"DllCanUnloadNow",
				MethodAttributes.Public|MethodAttributes.Static,
				CallingConventions.Standard,
				typeof(int),
				Params,
				CallingConvention.StdCall,
				CharSet.Ansi);

			
			// Bake the class
			Type t = myClass.CreateType();
  
			try
			{
				if ((int)(t.InvokeMember("IsComDll",BindingFlags.Public | BindingFlags.InvokeMethod | BindingFlags.Static,null,null,Params))
					==0)
					return true;
			}
			catch{return false;}
			return false;
		}
	}
}
