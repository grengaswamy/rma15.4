using System;
using System.Xml;
using System.Collections;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.Scripting;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for Supplementals.
	/// </summary>
	public class Acord :SupplementalObject , IDataModel, IPersistence, IEnumerable
	{

		internal  Acord(bool isLocked, Context context) : base(isLocked, context){Initialize();}
		private void Initialize(){;}
		override protected string OnBuildSuppSectionName(){return "acorddata";}
		override protected string OnBuildSuppSectionTitle(){return "ACORD";}
		public override void Save()
		{
			if((this as IDataModel).DataObjectState == DataObjectState.IsSaving)
			{	
				base.Save();
				return;
			}

							 if(this.KeyValue==0)
				throw new Riskmaster.ExceptionTypes.DataModelException(Globalization.GetString("Supplementals.PopulateObject.Exception.NoCreateNewSupp",this.Context.ClientId));

			base.Save ();
		}
	}
}

