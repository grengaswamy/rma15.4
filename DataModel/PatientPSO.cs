﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("PATIENT_PSO", "PAT_PSO_ROW_ID", "psocdInfectionPresent")]
    public class PatientPSO : DataObject
    {
        #region Database Field List
        private string[,] sFields = {
                                      {"EventId","EVENT_ID"},
                                      {"PatPsoRowId", "PAT_PSO_ROW_ID"},
                                      {"DateTimeRecAdded","DTTM_RCD_ADDED"},
                                      {"AddedByUser","ADDED_BY_USER"},
                                      {"DateTimeRecLastUpdated","DTTM_RCD_LAST_UPD"},
                                      {"UpdatedByUser","UPDATED_BY_USER"},
                                      {"DateTimeRecLastExp","DTTM_RCD_LAST_EXP"},
                                      {"psocdPatientAge","PSO_PATIENT_AGERANGE_CODE"},
                                      {"psocdPatientAgeNeo","PSO_PATIENT_AGERANGE_NEO_CODE"},
                                      {"cdPatientEthnicity","PSO_ETHNIC_CODE"}, 
                                      {"cdPatientEthnicityNeo","PSO_ETHNIC_NEO_CODE"}, 
                                      {"cdPatientRace","PSO_RACE_CODE"}, 
                                      {"cdPatientRaceNeo","PSO_RACE_NEO_CODE"}, 
                                      {"cdDiagnosisCode","PSO_DIAGNO_CODE"}, 
                                      {"cdDiagnosisCodeNeo","PSO_DIAGNO_NEO_CODE"}, 
                                      {"cdExtentHarm","PSO_HARM_EXT_CODE"}, 
                                      {"cdExtentHarmNeo","PSO_HARM_EXT_NEO_CODE"}, 
                                      {"cdAssessedHarm","PSO_HARM_TIM_CODE"}, 
                                      {"cdAssessedHarmNeo","PSO_HARM_TIM_NEO_CODE"}, 
                                      {"psocdRescuePatient","PSO_RESCUE_INTV_CODE"},                                                         
                                      {"psocdRescuePatientNeo","PSO_RESCUE_INTV_NEO_CODE"},                                                         
                                      {"txtOtherRescuePerform","PSO_RESCUE_INTV_TYP_MEMO"}, 
                                      {"txtOtherRescuePerformNeo","PSO_RESCUE_INTV_TYP_NEO_MEMO"}, 
                                      {"cdResultLength","PSO_INCR_STAY_CODE"}, 
                                      {"cdResultLengthNeo","PSO_INCR_STAY_NEO_CODE"}, 
                                      {"cdIncidentNotified","PSO_FMLY_NOTIFY_CODE"},
                                      {"cdIncidentNotifiedNeo","PSO_FMLY_NOTIFY_NEO_CODE"},
                                      {"psocdInfectionPresent","PSO_INF_PRES_ADM_CODE"},  
                                      {"psocdBestDescription","PSO_INFECTION_DESC_CODE"}, 
                                      {"psocdHAIPerson","PSO_INFECTION_CTRL_PERSON_CODE"}, 
                                      {"psocdHAITypeReported","PSO_HAI_TYPE_CODE"}, 
                                      {"psocdCLABSI","PSO_CENTRAL_LINE_ASSOC_CODE"}, 
                                      {"psocdAssocPneumonia","PSO_VENTI_ASSOC_PNEU_CODE"}, 
                                      {"psocdCAUTI","PSO_CATH_ASSOC_CODE"}, 
                                      {"psocdSSIClasified","PSO_SSI_CLASS_CODE"}, 
                                      {"psocdInfectionType","PSO_INF_CODE"}, 
                                      {"txtOtherInfectionType","PSO_INF_MEMO"}, 
                                      {"psocdCentralLine","PSO_CL_CODE"}, 
                                      {"psocdVAPClassified","PSO_VAP_CODE"}, 
                                      {"psocdDiagnosisCAUTI","PSO_CAUTI_STAT_CODE"}, 
                                      {"psocdClasifiedCAULI","PSO_CAUTI_CODE"}, 
                                      {"psocdInpatientLoc","PSO_INP_LOC_CODE"},
                                      {"psocdEventInvolved","PSO_PN_EVNT_CODE"},  
                                      {"psocdAffectedByEventBirthing","PSO_PAT_AFFEC_CODE"},  
                                      {"psocdAffectedByEventIntrauterine","PSO_PAT_AFFEC_INT_CODE"},  
                                      {"dtDeliveryDate","PSO_DATE_OF_DELV"}, 
                                      {"cdPrimipara","PSO_MAT_PARIT_CODE"},  
                                      {"txtFetusesNum","PSO_FT_CNT_NUM"}, 
                                      {"psocdLaborinduced", "PSO_LABR_IND"}, //rkaur27 : RMA-19199
                                      {"psocdLaborYes","PSO_LABR_MOD_CODE"},  
                                      {"cdDeliveryMode","PSO_MODE_DEL_CODE"},  
                                      {"cdGestation","PSO_PATIENT_LEN_OF_GEST_CODE"}, 
                                      {"psocdVaginalDeliveryMother","PSO_INSTR_USED_CODE"},  
                                      {"psocdVaginalDeliveryMotherNeonate","PSO_INSTR_USED_NEO_CODE"},  
                                      {"psocdInstrumentationMother","PSO_VAGDEL_INSTR_CODE"},  
                                      {"psocdInstrumentationMotherNeonate","PSO_VAGDEL_INSTR_NEO_CODE"},  
                                      {"txtOtherAdverseOutcome","PSO_MAT_OUTCM_MEMO"}, 
                                      {"txtOtherAdverseOutcomeMother","PSO_MAT_OUTCM_PER_MEMO"}, 
                                      {"txtOtherAdverseOutcomeMotherNeonate","PSO_MAT_OUTCM_NEO_MEMO"}, 
                                      {"psocdMeternalInfection","PSO_MAT_INF_CODE"},  
                                      {"psocdMeternalInfectionMother","PSO_MAT_INF_PER_CODE"},  
                                      {"psocdMeternalInfectionMotherNeonate","PSO_MAT_INF_NEO_CODE"},  
                                      {"txtOtherMeternalInfection","PSO_MAT_INF_MEMO"}, 
                                      {"txtOtherMeternalInfectionMother","PSO_MAT_INF_PER_MEMO"}, 
                                      {"txtOtherMeternalInfectionMotherNeonate","PSO_MAT_INF_NEO_MEMO"}, 
                                      {"txtOtherBodyPart","PSO_MAT_INJ_MEMO"}, 
                                      {"txtOtherBodyPartMother","PSO_MAT_INJ_PER_MEMO"}, 
                                      {"txtOtherBodyPartMotherNeonate","PSO_MAT_INJ_NEO_MEMO"}, 
                                      {"psocdFetusSustain","PSO_FT_OUTCM_CODE"},  
                                      {"txtOtherNeonateSustain","PSO_NN_ADV_OUTCM_MEMO"}, 
                                      {"psocdBirthTrauma","PSO_BRTH_TRMA_CODE"},  
                                      {"txtOtherBirthTrauma","PSO_BRTH_TRMA_MEMO"}, 
                                      {"psocdUlcerStage","PSO_ADV_STAG_REP_CODE"},  
                                      {"psocdTissueInjury","PSO_sDTI_STAT_CODE"}, 
                                      {"psocdUnstageablePressure","PSO_ADMSN_STAGE_CODE"},  
                                      {"cdSkinInspection","PSO_SKN_INSPCTN_CODE"}, 
                                      {"psocdUlserAssessment","PSO_TIME_OF_RSKASSESS_CODE"}, 
                                      {"cdRiskAssessmentType","PSO_RISK_ASESS_CODE"}, 
                                      {"cdUlcerRisk","PSO_DOCU_RISK_CODE"}, 
                                      {"psocdIntervention","PSO_PREVN_INTVN_CODE"},                                                        
                                      {"txtOtehrInterventionType","PSO_INTRVN_TYP_MEMO"}, 
                                      {"psocdUlcerdeviceUsed","PSO_DEV_INVOLV_CODE"}, 
                                      {"psocdApplianceType","PSO_PU_DEV_TYP_CODE"}, 
                                      {"txtOtherApplianceType","PSO_PU_DEV_TYP_MEMO"}, 
                                      {"psocdTubeType","PSO_TYP_TUB_CODE"}, 
                                      {"txtOtherTubeType","PSO_TYP_TUB_MEMO"}, 
                                      {"psocdSecondaryMorbidity","PSO_SEC_MORBID_STAY_CODE"}, 
                                      {"cdMorbiditySuspected","PSO_SEC_MORBID_CODE"},  
                                      {"PatInvEntId", "PAT_INV_ENT_ID"},
                                      {"PatInvNeoEntId", "PAT_INV_NEO_ENT_ID"},
                 
		};

        public int PatPsoRowId { get { return GetFieldInt("PAT_PSO_ROW_ID"); } set { SetField("PAT_PSO_ROW_ID", value); } }
        public int PatInvEntId { get { return GetFieldInt("PAT_INV_ENT_ID"); } set { SetField("PAT_INV_ENT_ID", value); } }
        public int PatInvNeoEntId { get { return GetFieldInt("PAT_INV_NEO_ENT_ID"); } set { SetField("PAT_INV_NEO_ENT_ID", value); } }
        public int EventId { get { return GetFieldInt("EVENT_ID"); } set { SetField("EVENT_ID", value); } }
        public string DateTimeRecAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string DateTimeRecLastUpdated { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DateTimeRecLastExp { get { return GetFieldString("DTTM_RCD_LAST_EXP"); } set { SetField("DTTM_RCD_LAST_EXP", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_PAT_AGE")]
        public int psocdPatientAge { get { return GetFieldInt("PSO_PATIENT_AGERANGE_CODE"); } set { SetField("PSO_PATIENT_AGERANGE_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_PAT_AGE")]
        public int psocdPatientAgeNeo { get { return GetFieldInt("PSO_PATIENT_AGERANGE_NEO_CODE"); } set { SetField("PSO_PATIENT_AGERANGE_NEO_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_ETHNIC")]
        public int cdPatientEthnicity { get { return GetFieldInt("PSO_ETHNIC_CODE"); } set { SetField("PSO_ETHNIC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_ETHNIC")]
        public int cdPatientEthnicityNeo { get { return GetFieldInt("PSO_ETHNIC_NEO_CODE"); } set { SetField("PSO_ETHNIC_NEO_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_RACE")]
        public int cdPatientRace { get { return GetFieldInt("PSO_RACE_CODE"); } set { SetField("PSO_RACE_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_RACE")]
        public int cdPatientRaceNeo { get { return GetFieldInt("PSO_RACE_NEO_CODE"); } set { SetField("PSO_RACE_NEO_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "CDC_DIAGNOSIS_CODE")]
        public int cdDiagnosisCode { get { return GetFieldInt("PSO_DIAGNO_CODE"); } set { SetField("PSO_DIAGNO_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "CDC_DIAGNOSIS_CODE")]
        public int cdDiagnosisCodeNeo { get { return GetFieldInt("PSO_DIAGNO_NEO_CODE"); } set { SetField("PSO_DIAGNO_NEO_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_HARM_EXT")]
        public int cdExtentHarm { get { return GetFieldInt("PSO_HARM_EXT_CODE"); } set { SetField("PSO_HARM_EXT_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_HARM_EXT")]
        public int cdExtentHarmNeo { get { return GetFieldInt("PSO_HARM_EXT_NEO_CODE"); } set { SetField("PSO_HARM_EXT_NEO_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_HARM_TIME")]
        public int cdAssessedHarm { get { return GetFieldInt("PSO_HARM_TIM_CODE"); } set { SetField("PSO_HARM_TIM_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_HARM_TIME")]
        public int cdAssessedHarmNeo { get { return GetFieldInt("PSO_HARM_TIM_NEO_CODE"); } set { SetField("PSO_HARM_TIM_NEO_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdRescuePatient { get { return GetFieldInt("PSO_RESCUE_INTV_CODE"); } set { SetField("PSO_RESCUE_INTV_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdRescuePatientNeo { get { return GetFieldInt("PSO_RESCUE_INTV_NEO_CODE"); } set { SetField("PSO_RESCUE_INTV_NEO_CODE", value); } }
        public string txtOtherRescuePerform { get { return GetFieldString("PSO_RESCUE_INTV_TYP_MEMO"); } set { SetField("PSO_RESCUE_INTV_TYP_MEMO", value); } }
        public string txtOtherRescuePerformNeo { get { return GetFieldString("PSO_RESCUE_INTV_TYP_NEO_MEMO"); } set { SetField("PSO_RESCUE_INTV_TYP_NEO_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_INC_LEN_STAY")]
        public int cdResultLength { get { return GetFieldInt("PSO_INCR_STAY_CODE"); } set { SetField("PSO_INCR_STAY_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_INC_LEN_STAY")]
        public int cdResultLengthNeo { get { return GetFieldInt("PSO_INCR_STAY_NEO_CODE"); } set { SetField("PSO_INCR_STAY_NEO_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int cdIncidentNotified { get { return GetFieldInt("PSO_FMLY_NOTIFY_CODE"); } set { SetField("PSO_FMLY_NOTIFY_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int cdIncidentNotifiedNeo { get { return GetFieldInt("PSO_FMLY_NOTIFY_NEO_CODE"); } set { SetField("PSO_FMLY_NOTIFY_NEO_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int FamilyGuardianNotifiedCodeNeo { get { return GetFieldInt("PSO_FMLY_NOTIFY_NEO_CODE"); } set { SetField("PSO_FMLY_NOTIFY_NEO_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_INF_PRESENT")]
        public int psocdInfectionPresent { get { return GetFieldInt("PSO_INF_PRES_ADM_CODE"); } set { SetField("PSO_INF_PRES_ADM_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_INFECTION_DESC")]
        public int psocdBestDescription { get { return GetFieldInt("PSO_INFECTION_DESC_CODE"); } set { SetField("PSO_INFECTION_DESC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdHAIPerson { get { return GetFieldInt("PSO_INFECTION_CTRL_PERSON_CODE"); } set { SetField("PSO_INFECTION_CTRL_PERSON_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_HAI_TYPE")]
        public int psocdHAITypeReported { get { return GetFieldInt("PSO_HAI_TYPE_CODE"); } set { SetField("PSO_HAI_TYPE_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO")]
        public int psocdCLABSI { get { return GetFieldInt("PSO_CENTRAL_LINE_ASSOC_CODE"); } set { SetField("PSO_CENTRAL_LINE_ASSOC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO")]
        public int psocdAssocPneumonia { get { return GetFieldInt("PSO_VENTI_ASSOC_PNEU_CODE"); } set { SetField("PSO_VENTI_ASSOC_PNEU_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO")]
        public int psocdCAUTI { get { return GetFieldInt("PSO_CATH_ASSOC_CODE"); } set { SetField("PSO_CATH_ASSOC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_SSI_CLASS")]
        public int psocdSSIClasified { get { return GetFieldInt("PSO_SSI_CLASS_CODE"); } set { SetField("PSO_SSI_CLASS_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_INF_TYP")]
        public int psocdInfectionType { get { return GetFieldInt("PSO_INF_CODE"); } set { SetField("PSO_INF_CODE", value); } }
        public string txtOtherInfectionType { get { return GetFieldString("PSO_INF_MEMO"); } set { SetField("PSO_INF_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_CL_TYP")]
        public int psocdCentralLine { get { return GetFieldInt("PSO_CL_CODE"); } set { SetField("PSO_CL_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_VAP_CLASS")]
        public int psocdVAPClassified { get { return GetFieldInt("PSO_VAP_CODE"); } set { SetField("PSO_VAP_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_CAUTI_STAT")]
        public int psocdDiagnosisCAUTI { get { return GetFieldInt("PSO_CAUTI_STAT_CODE"); } set { SetField("PSO_CAUTI_STAT_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_CAUTI_CLASS")]
        public int psocdClasifiedCAULI { get { return GetFieldInt("PSO_CAUTI_CODE"); } set { SetField("PSO_CAUTI_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_INP_LOC")]
        public int psocdInpatientLoc { get { return GetFieldInt("PSO_INP_LOC_CODE"); } set { SetField("PSO_INP_LOC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_PN_EVNT_TYP")]
        public int psocdEventInvolved { get { return GetFieldInt("PSO_PN_EVNT_CODE"); } set { SetField("PSO_PN_EVNT_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_AFFEC_PAT")]
        public int psocdAffectedByEventBirthing { get { return GetFieldInt("PSO_PAT_AFFEC_CODE"); } set { SetField("PSO_PAT_AFFEC_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_AFFEC_PAT")]
        public int psocdAffectedByEventIntrauterine { get { return GetFieldInt("PSO_PAT_AFFEC_INT_CODE"); } set { SetField("PSO_PAT_AFFEC_INT_CODE", value); } }
        //public string dtDeliveryDate { get { return GetFieldString("PSO_DATE_OF_DELV"); } set { SetField("PSO_DATE_OF_DELV", value); } }
        public string dtDeliveryDate { get { return GetFieldString("PSO_DATE_OF_DELV"); } set { SetField("PSO_DATE_OF_DELV", Riskmaster.Common.Conversion.GetDate(value)); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int cdPrimipara { get { return GetFieldInt("PSO_MAT_PARIT_CODE"); } set { SetField("PSO_MAT_PARIT_CODE", value); } }
        public int txtFetusesNum { get { return GetFieldInt("PSO_FT_CNT_NUM"); } set { SetField("PSO_FT_CNT_NUM", value); } }
		 //rkaur27 : RMA-19199 - Start
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdLaborinduced { get { return GetFieldInt("PSO_LABR_IND"); } set { SetField("PSO_LABR_IND", value); } }
		 //rkaur27 : RMA-19199 - End
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_LABR_TYP")]
        public int psocdLaborYes { get { return GetFieldInt("PSO_LABR_MOD_CODE"); } set { SetField("PSO_LABR_MOD_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_DELV_MODE")]
        public int cdDeliveryMode { get { return GetFieldInt("PSO_MODE_DEL_CODE"); } set { SetField("PSO_MODE_DEL_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_LEN_OF_GEST")]
        public int cdGestation { get { return GetFieldInt("PSO_PATIENT_LEN_OF_GEST_CODE"); } set { SetField("PSO_PATIENT_LEN_OF_GEST_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdVaginalDeliveryMother { get { return GetFieldInt("PSO_INSTR_USED_CODE"); } set { SetField("PSO_INSTR_USED_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdVaginalDeliveryMotherNeonate { get { return GetFieldInt("PSO_INSTR_USED_NEO_CODE"); } set { SetField("PSO_INSTR_USED_NEO_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_INSTRMNT_TYP")]
        public int psocdInstrumentationMother { get { return GetFieldInt("PSO_VAGDEL_INSTR_CODE"); } set { SetField("PSO_VAGDEL_INSTR_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_INSTRMNT_TYP")]
        public int psocdInstrumentationMotherNeonate { get { return GetFieldInt("PSO_VAGDEL_INSTR_NEO_CODE"); } set { SetField("PSO_VAGDEL_INSTR_NEO_CODE", value); } }
        public string txtOtherAdverseOutcome { get { return GetFieldString("PSO_MAT_OUTCM_MEMO"); } set { SetField("PSO_MAT_OUTCM_MEMO", value); } }
        public string txtOtherAdverseOutcomeMother { get { return GetFieldString("PSO_MAT_OUTCM_PER_MEMO"); } set { SetField("PSO_MAT_OUTCM_PER_MEMO", value); } }
        public string txtOtherAdverseOutcomeMotherNeonate { get { return GetFieldString("PSO_MAT_OUTCM_NEO_MEMO"); } set { SetField("PSO_MAT_OUTCM_NEO_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_MAT_INF_TYP")]
        public int psocdMeternalInfection { get { return GetFieldInt("PSO_MAT_INF_CODE"); } set { SetField("PSO_MAT_INF_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_MAT_INF_TYP")]
        public int psocdMeternalInfectionMother { get { return GetFieldInt("PSO_MAT_INF_PER_CODE"); } set { SetField("PSO_MAT_INF_PER_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_MAT_INF_TYP")]
        public int psocdMeternalInfectionMotherNeonate { get { return GetFieldInt("PSO_MAT_INF_NEO_CODE"); } set { SetField("PSO_MAT_INF_NEO_CODE", value); } }
        public string txtOtherMeternalInfection { get { return GetFieldString("PSO_MAT_INF_MEMO"); } set { SetField("PSO_MAT_INF_MEMO", value); } }
        public string txtOtherMeternalInfectionMother { get { return GetFieldString("PSO_MAT_INF_PER_MEMO"); } set { SetField("PSO_MAT_INF_PER_MEMO", value); } }
        public string txtOtherMeternalInfectionMotherNeonate { get { return GetFieldString("PSO_MAT_INF_NEO_MEMO"); } set { SetField("PSO_MAT_INF_NEO_MEMO", value); } }
        public string txtOtherBodyPart { get { return GetFieldString("PSO_MAT_INJ_MEMO"); } set { SetField("PSO_MAT_INJ_MEMO", value); } }
        public string txtOtherBodyPartMother { get { return GetFieldString("PSO_MAT_INJ_PER_MEMO"); } set { SetField("PSO_MAT_INJ_PER_MEMO", value); } }
        public string txtOtherBodyPartMotherNeonate { get { return GetFieldString("PSO_MAT_INJ_NEO_MEMO"); } set { SetField("PSO_MAT_INJ_NEO_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_FT_OUTCM_TYP")]
        public int psocdFetusSustain { get { return GetFieldInt("PSO_FT_OUTCM_CODE"); } set { SetField("PSO_FT_OUTCM_CODE", value); } }
        public string txtOtherNeonateSustain { get { return GetFieldString("PSO_NN_ADV_OUTCM_MEMO"); } set { SetField("PSO_NN_ADV_OUTCM_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_TRUMA_TYP")]
        public int psocdBirthTrauma { get { return GetFieldInt("PSO_BRTH_TRMA_CODE"); } set { SetField("PSO_BRTH_TRMA_CODE", value); } }
        public string txtOtherBirthTrauma { get { return GetFieldString("PSO_BRTH_TRMA_MEMO"); } set { SetField("PSO_BRTH_TRMA_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_SDTI_ADV_STAG")]
        public int psocdUlcerStage { get { return GetFieldInt("PSO_ADV_STAG_REP_CODE"); } set { SetField("PSO_ADV_STAG_REP_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_SDTI_STAT")]
        public int psocdTissueInjury { get { return GetFieldInt("PSO_sDTI_STAT_CODE"); } set { SetField("PSO_sDTI_STAT_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_STAGE_STAT")]
        public int psocdUnstageablePressure { get { return GetFieldInt("PSO_ADMSN_STAGE_CODE"); } set { SetField("PSO_ADMSN_STAGE_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int cdSkinInspection { get { return GetFieldInt("PSO_SKN_INSPCTN_CODE"); } set { SetField("PSO_SKN_INSPCTN_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_FRST_RISK_TIME")]
        public int psocdUlserAssessment { get { return GetFieldInt("PSO_TIME_OF_RSKASSESS_CODE"); } set { SetField("PSO_TIME_OF_RSKASSESS_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_RISK_TYPE")]
        public int cdRiskAssessmentType { get { return GetFieldInt("PSO_RISK_ASESS_CODE"); } set { SetField("PSO_RISK_ASESS_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int cdUlcerRisk { get { return GetFieldInt("PSO_DOCU_RISK_CODE"); } set { SetField("PSO_DOCU_RISK_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdIntervention { get { return GetFieldInt("PSO_PREVN_INTVN_CODE"); } set { SetField("PSO_PREVN_INTVN_CODE", value); } }
        public string txtOtehrInterventionType { get { return GetFieldString("PSO_INTRVN_TYP_MEMO"); } set { SetField("PSO_INTRVN_TYP_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdUlcerdeviceUsed { get { return GetFieldInt("PSO_DEV_INVOLV_CODE"); } set { SetField("PSO_DEV_INVOLV_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_DEV_INV_TYP")]
        public int psocdApplianceType { get { return GetFieldInt("PSO_PU_DEV_TYP_CODE"); } set { SetField("PSO_PU_DEV_TYP_CODE", value); } }
        public string txtOtherApplianceType { get { return GetFieldString("PSO_PU_DEV_TYP_MEMO"); } set { SetField("PSO_PU_DEV_TYP_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_TUBE_TYP")]
        public int psocdTubeType { get { return GetFieldInt("PSO_TYP_TUB_CODE"); } set { SetField("PSO_TYP_TUB_CODE", value); } }
        public string txtOtherTubeType { get { return GetFieldString("PSO_TYP_TUB_MEMO"); } set { SetField("PSO_TYP_TUB_MEMO", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int psocdSecondaryMorbidity { get { return GetFieldInt("PSO_SEC_MORBID_STAY_CODE"); } set { SetField("PSO_SEC_MORBID_STAY_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "PSO_YES_NO_JCAHO")]
        public int cdMorbiditySuspected { get { return GetFieldInt("PSO_SEC_MORBID_CODE"); } set { SetField("PSO_SEC_MORBID_CODE", value); } }

        #endregion

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
		{
			base.LoadData (objReader);
		}

        internal PatientPSO(bool isLocked, Context context)
            : base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{
            //Moved after most init logic so that scripting can be called successfully.
            // base.Initialize();  
            this.m_sTableName = "PATIENT_PSO";
            this.m_sKeyField = "PAT_PSO_ROW_ID";
            this.m_sFilterClause = string.Empty;
            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            base.InitChildren(sChildren);
            //Add all object Children into the Children collection from our "sChildren" list.
            this.m_sParentClassName = string.Empty;
            //Add all object Children into the Children collection from our "sChildren" list.
            base.Initialize();
		}

        #region Child Implementation
        private string[,] sChildren = {{"psolstRescuePerform","DataSimpleList"}, 
                                       {"psolstRescuePerformNeo","DataSimpleList"}, 
                                       {"psolstAdverseOutcome","DataSimpleList"}, 
                                       {"psolstAdverseOutcomeMother","DataSimpleList"}, 
                                       {"psolstAdverseOutcomeMotherNeonate","DataSimpleList"}, 
                                       {"psolstBodyPart","DataSimpleList"}, 
                                       {"psolstBodyPartMother","DataSimpleList"}, 
                                       {"psolstBodyPartMotherNeonate","DataSimpleList"}, 
                                       {"psolstNeonateSustain","DataSimpleList"},
                                       {"psolstInterventionType","DataSimpleList"}, 
									  };

        // Lock Entity Tables
        // Also Initialize SimpleList with desired table, field details.
        override internal void OnChildInit(string childName, string childType)
        {
            //Do default per-child processing.
            base.OnChildInit(childName, childType);
            //Do any custom per-child processing.
            object obj = base.m_Children[childName];
            switch (childType)
            {
                case "DataSimpleList":
                    DataSimpleList objList = obj as DataSimpleList;
                    switch (childName)
                    {
                        //case "DiagnosisCodes":
                        //    objList.Initialize("DIAGNOSIS_CODE", "PSO_DIAGNOSIS", "PSO_ROW_ID");
                        //    break;
                        case "psolstRescuePerform":
                            objList.Initialize("PSO_RESCUE_INTV_TYP_CODE", "PAT_X_RESC_INERVEN_PSO", "PAT_PSO_ROW_ID");
                            break;
                        case "psolstRescuePerformNeo":
                            objList.Initialize("PSO_RESCUE_INTV_TYP_NEO_CODE", "PAT_X_RESC_INERVEN_NEO_PSO", "PAT_PSO_ROW_ID");
                            break;
                        case "psolstAdverseOutcome":
                            objList.Initialize("PSO_MAT_OUTCM_CODE", "PAT_X_MAT_OUTCM_PSO", "PAT_PSO_ROW_ID");
                            break;
                        case "psolstAdverseOutcomeMother":
                            objList.Initialize("PSO_MAT_OUTCM_PER_CODE", "PAT_X_MAT_OUTCM_PER_PSO", "PAT_PSO_ROW_ID");
                            break;
                        case "psolstAdverseOutcomeMotherNeonate":
                            objList.Initialize("PSO_MAT_OUTCM_NEO_CODE", "PAT_X_MAT_OUTCM_NEO_PSO", "PAT_PSO_ROW_ID");
                            break;
                        case "psolstBodyPart":
                            objList.Initialize("PSO_MAT_INJ_CODE", "PAT_X_MAT_INJ_PSO", "PAT_PSO_ROW_ID");
                            break;
                        case "psolstBodyPartMother":
                            objList.Initialize("PSO_MAT_INJ_PER_CODE", "PAT_X_MAT_INJ_PER_PSO", "PAT_PSO_ROW_ID");
                            break;
                        case "psolstBodyPartMotherNeonate":
                            objList.Initialize("PSO_MAT_INJ_NEO_CODE", "PAT_X_MAT_INJ_NEO_PSO", "PAT_PSO_ROW_ID");
                            break;
                        case "psolstNeonateSustain":
                            objList.Initialize("PSO_NN_ADV_OUTCM_CODE", "PAT_X_NN_OUTCM_TYP_PSO", "PAT_PSO_ROW_ID");
                            break;
                        case "psolstInterventionType":
                            objList.Initialize("PSO_INTRVN_TYP_CODE", "PAT_X_PRESS_ULC_PSO", "PAT_PSO_ROW_ID");
                            break;

                    }
                    break;
            }

        }
        //Handle adding the our key to any additions to the record collection properties.
        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            base.OnChildItemAdded(childName, itemValue);
        }

        //Handle child saves.  Set the current key into the children since this could be a new record.
        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            switch (childName)
            {
                case "psolstRescuePerform":
                    if (isNew)
                    {
                        (childValue as DataSimpleList).SetKeyValue(this.PatPsoRowId);
                    }
                    (childValue as DataSimpleList).SaveSimpleList();
                    break;
                case "psolstRescuePerformNeo":
                    if (isNew)
                    {
                        (childValue as DataSimpleList).SetKeyValue(this.PatPsoRowId);
                    }
                    (childValue as DataSimpleList).SaveSimpleList();
                    break;
                case "psolstAdverseOutcome":
                    if (isNew)
                    {
                        (childValue as DataSimpleList).SetKeyValue(this.PatPsoRowId);
                    }
                    (childValue as DataSimpleList).SaveSimpleList();
                    break;
                case "psolstAdverseOutcomeMother":
                    if (isNew)
                    {
                        (childValue as DataSimpleList).SetKeyValue(this.PatPsoRowId);
                    }
                    (childValue as DataSimpleList).SaveSimpleList();
                    break;
                case "psolstAdverseOutcomeMotherNeonate":
                    if (isNew)
                    {
                        (childValue as DataSimpleList).SetKeyValue(this.PatPsoRowId);
                    }
                    (childValue as DataSimpleList).SaveSimpleList();
                    break;
                case "psolstBodyPart":
                    if (isNew)
                    {
                        (childValue as DataSimpleList).SetKeyValue(this.PatPsoRowId);
                    }
                    (childValue as DataSimpleList).SaveSimpleList();
                    break;
                case "psolstBodyPartMother":
                    if (isNew)
                    {
                        (childValue as DataSimpleList).SetKeyValue(this.PatPsoRowId);
                    }
                    (childValue as DataSimpleList).SaveSimpleList();
                    break;
                case "psolstBodyPartMotherNeonate":
                    if (isNew)
                    {
                        (childValue as DataSimpleList).SetKeyValue(this.PatPsoRowId);
                    }
                    (childValue as DataSimpleList).SaveSimpleList();
                    break;
                case "psolstNeonateSustain":
                    if (isNew)
                    {
                        (childValue as DataSimpleList).SetKeyValue(this.PatPsoRowId);
                    }
                    (childValue as DataSimpleList).SaveSimpleList();
                    break;
                case "psolstInterventionType":
                    if (isNew)
                    {
                        (childValue as DataSimpleList).SetKeyValue(this.PatPsoRowId);
                    }
                    (childValue as DataSimpleList).SaveSimpleList();
                    break;
            }
            base.OnChildPostSave(childName, childValue, isNew);
        }

        [ExtendedTypeAttribute(RMExtType.CodeList, "PSO_RESCUE_INTV")]
        public DataSimpleList psolstRescuePerform
        {
            get
            {
                DataSimpleList objList = base.m_Children["psolstRescuePerform"] as DataSimpleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.LoadSimpleList((int)this.KeyFieldValue);
                return objList;
            }
        }
        [ExtendedTypeAttribute(RMExtType.CodeList, "PSO_RESCUE_INTV")]
        public DataSimpleList psolstRescuePerformNeo
        {
            get
            {
                DataSimpleList objList = base.m_Children["psolstRescuePerformNeo"] as DataSimpleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.LoadSimpleList((int)this.KeyFieldValue);
                return objList;
            }
        }
        [ExtendedTypeAttribute(RMExtType.CodeList, "PSO_MAT_OUTCM")]
        public DataSimpleList psolstAdverseOutcome
        {
            get
            {
                DataSimpleList objList = base.m_Children["psolstAdverseOutcome"] as DataSimpleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.LoadSimpleList((int)this.KeyFieldValue);
                return objList;
            }
        }
        [ExtendedTypeAttribute(RMExtType.CodeList, "PSO_MAT_OUTCM")]
        public DataSimpleList psolstAdverseOutcomeMother
        {
            get
            {
                DataSimpleList objList = base.m_Children["psolstAdverseOutcomeMother"] as DataSimpleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.LoadSimpleList((int)this.KeyFieldValue);
                return objList;
            }
        }
        [ExtendedTypeAttribute(RMExtType.CodeList, "PSO_MAT_OUTCM")]
        public DataSimpleList psolstAdverseOutcomeMotherNeonate
        {
            get
            {
                DataSimpleList objList = base.m_Children["psolstAdverseOutcomeMotherNeonate"] as DataSimpleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.LoadSimpleList((int)this.KeyFieldValue);
                return objList;
            }
        }
        [ExtendedTypeAttribute(RMExtType.CodeList, "PSO_MAT_INJ_TYP")]
        public DataSimpleList psolstBodyPart
        {
            get
            {
                DataSimpleList objList = base.m_Children["psolstBodyPart"] as DataSimpleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.LoadSimpleList((int)this.KeyFieldValue);
                return objList;
            }
        }
        [ExtendedTypeAttribute(RMExtType.CodeList, "PSO_MAT_INJ_TYP")]
        public DataSimpleList psolstBodyPartMother
        {
            get
            {
                DataSimpleList objList = base.m_Children["psolstBodyPartMother"] as DataSimpleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.LoadSimpleList((int)this.KeyFieldValue);
                return objList;
            }
        }
        [ExtendedTypeAttribute(RMExtType.CodeList, "PSO_MAT_INJ_TYP")]
        public DataSimpleList psolstBodyPartMotherNeonate
        {
            get
            {
                DataSimpleList objList = base.m_Children["psolstBodyPartMotherNeonate"] as DataSimpleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.LoadSimpleList((int)this.KeyFieldValue);
                return objList;
            }
        }
        [ExtendedTypeAttribute(RMExtType.CodeList, "PSO_NN_OUTCM_TYP")]
        public DataSimpleList psolstNeonateSustain
        {
            get
            {
                DataSimpleList objList = base.m_Children["psolstNeonateSustain"] as DataSimpleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.LoadSimpleList((int)this.KeyFieldValue);
                return objList;
            }
        }
        [ExtendedTypeAttribute(RMExtType.CodeList, "PSO_INTERVN_TYP")]
        public DataSimpleList psolstInterventionType
        {
            get
            {
                DataSimpleList objList = base.m_Children["psolstInterventionType"] as DataSimpleList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.LoadSimpleList((int)this.KeyFieldValue);
                return objList;
            }
        }
        #endregion
    }
}
