﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.DataModel
{
    /// <summary>
    /// Summary of ClaimXArbitration Class.
    /// </summary>
    [Riskmaster.DataModel.Summary("CLAIM_X_ARB", "ARBITRATION_ROW_ID", "FiledDate")]
    public class ClaimXArbitration : DataObject
    {
        #region Database Field List

        private string[,] sFields = {
														{"ArbitrationRowId", "ARBITRATION_ROW_ID"},
														{"ClaimId", "CLAIM_ID"},
														{"ArbTypeCode", "ARB_TYPE_CODE"},
                                                        {"ArbStatusCode", "ARB_STATUS_CODE"},
                                                        {"ArbAdversePartyEid", "ADVERSE_NAME_EID"},
                                                        {"ArbPartyCode", "ADDITIONAL_ADVERSE"},
                                                        {"ArbAdverseCoEid", "ADVERSE_CO_EID"},
														{"DateFiled", "FILED_DATE"},
														{"ArbAdvAdjusterEid", "ADV_ADJUSTER_EID"},
														{"DateHearing", "HEARING_DATE"},
														{"ArbAdversClaimNum", "ARB_CLAIM_NUM"},
														{"ArbAwardAmount", "AWARD_AMOUNT"},
														{"Comments", "COMMENTS"},
														{"HTMLComments", "HTMLCOMMENTS"},
														{"AddedByUser","ADDED_BY_USER"},			
														{"UpdatedByUser","UPDATED_BY_USER"},
														{"DttmRcdAdded","DTTM_RCD_ADDED"},
														{"DttmRcdLastUpd","DTTM_RCD_LAST_UPD"},
                                                        {"LegacyUniqueIdentifier","LEGACY_UNIQUE_IDENTIFIER"}
		};
        public string LegacyUniqueIdentifier { get { return GetFieldString("LEGACY_UNIQUE_IDENTIFIER"); } set { SetField("LEGACY_UNIQUE_IDENTIFIER", value); } }
        public int ArbitrationRowId { get { return GetFieldInt("ARBITRATION_ROW_ID"); } set { SetField("ARBITRATION_ROW_ID", value); } }
        public int ClaimId { get { return GetFieldInt("CLAIM_ID"); } set { SetField("CLAIM_ID", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "ARBITRATION_TYPE")]
        public int ArbTypeCode { get { return GetFieldInt("ARB_TYPE_CODE"); } set { SetField("ARB_TYPE_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Entity, "OTHER PEOPLE")]
        public int ArbAdversePartyEid { get { return GetFieldInt("ADVERSE_NAME_EID"); } set { SetField("ADVERSE_NAME_EID", value); } }
        //public int ArbAdversePartyEid { get { return GetFieldInt("ADVERSE_NAME_EID"); } set { SetField("ADVERSE_NAME_EID", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "ARBITRATION_STATUS")]
        public int ArbStatusCode { get { return GetFieldInt("ARB_STATUS_CODE"); } set { SetField("ARB_STATUS_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "ARBITRATION_PARTY")]
        public int ArbPartyCode { get { return GetFieldInt("ADDITIONAL_ADVERSE"); } set { SetField("ADDITIONAL_ADVERSE", value); } }
        [ExtendedTypeAttribute(RMExtType.Entity, "OTHER PEOPLE")]
        public int ArbAdverseCoEid { get { return GetFieldInt("ADVERSE_CO_EID"); } set { SetField("ADVERSE_CO_EID", value); } }
        public string DateFiled { get { return GetFieldString("FILED_DATE"); } set { SetField("FILED_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        [ExtendedTypeAttribute(RMExtType.Entity, "OTHER PEOPLE")]
        public int ArbAdvAdjusterEid { get { return GetFieldInt("ADV_ADJUSTER_EID"); } set { SetField("ADV_ADJUSTER_EID", value); } }
        public string DateHearing { get { return GetFieldString("HEARING_DATE"); } set { SetField("HEARING_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public string ArbAdversClaimNum { get { return GetFieldString("ARB_CLAIM_NUM"); } set { SetField("ARB_CLAIM_NUM", value); } }
        public double ArbAwardAmount { get { return GetFieldDouble("AWARD_AMOUNT"); } set { SetField("AWARD_AMOUNT", value); } }
        public string Comments { get { return GetFieldString("COMMENTS"); } set { SetField("COMMENTS", value); } }
        public string HTMLComments { get { return GetFieldStringFallback("HTMLCOMMENTS", "COMMENTS"); } set { SetField("HTMLCOMMENTS", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }

        #endregion

        #region Child Implementation
        private string[,] sChildren = { { "DemandOfferList", "DemandOfferList" } };
                                        //Rijul 340
                                        //Rijul 340 end

        // Lock Entity Tables
        // Also Initialize SimpleList with desired table, field details.
        override internal void OnChildInit(string childName, string childType)
        {
            //Do default per-child processing.
            base.OnChildInit(childName, childType);
                    //Rijul 340 
                //Rijul 340 end

            //Do any custom per-child processing.
        }
        //Handle adding the our key to any additions to the record collection properties.
        internal override void OnChildItemAdded(string childName, IDataModel itemValue)
        {
            switch (childName)
            {
                case "DemandOfferList":
                    (itemValue as DemandOffer).ParentId = this.ArbitrationRowId;
                    (itemValue as DemandOffer).ParentName = typeof(ClaimXArbitration).Name;
                    break;
            }
            base.OnChildItemAdded(childName, itemValue);
        }

        internal override void LoadData(Riskmaster.Db.DbReader objReader)
        {
            base.LoadData(objReader);
            //Rijul 340
            //Rijul 340 end

        //Handle any sub-object(s) for which we need the key in our own table.
            // If Entity is involved we save this first (in case we need the EntityId.);
            //Rijul 340
            //Rijul 340 end
        }

        //Handle child saves.  Set the current key into the children since this could be a new record.
        override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
        {
            switch (childName)
            {
                case "DemandOfferList":
                    if (isNew)
                        foreach (DemandOffer item in (childValue as DemandOfferList))
                        {
                            item.ParentId = this.ArbitrationRowId;
                            item.ParentName = typeof(ClaimXArbitration).Name;
                        }
                    (childValue as IPersistence).Save();
                    break;
            }
            base.OnChildPostSave(childName, childValue, isNew);
        }

        //Protect Entities that should not be deleted.
        internal override void OnChildDelete(string childName, IDataModel childValue)
        {
            base.OnChildDelete(childName, childValue);
        }

        //Child Property Accessors
        public DemandOfferList DemandOfferList
        {
            get
            {
                DemandOfferList objList = base.m_Children["DemandOfferList"] as DemandOfferList;
                if (!base.IsNew && (objList as IDataModel).IsStale)
                    objList.SQLFilter = DemandOffer.ParentKeyFieldName + "=" + this.KeyFieldValue + " AND " + DemandOffer.ParentNameFieldName + "='" + typeof(ClaimXSubrogation).Name + "'";
                return objList;
            }
        }

        //Rijul 340
        //Rijul 340 end
        #endregion

        internal ClaimXArbitration(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }
        new private void Initialize()
        {

            // base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

            this.m_sTableName = "CLAIM_X_ARB";
            this.m_sKeyField = "ARBITRATION_ROW_ID";
            this.m_sFilterClause = "";

            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Add all object Children into the Children collection from our "sChildren" list.
            base.InitChildren(sChildren);
            this.m_sParentClassName = "Claim";
            base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

        }
        //Override Parent by Shivendu for MITS 9467
        public override IDataModel Parent
        {
            get
            {


                if (base.Parent == null)
                {
                    base.Parent = Context.Factory.GetDataModelObject("Claim", true);
                    m_isParentStale = false;
                    if (this.ClaimId != 0)
                        (base.Parent as DataObject).MoveTo(this.ClaimId);
                }

                return base.Parent;
            }
            set
            {
                base.Parent = value;
            }
        }
        #region Supplemental Fields Exposed
        //Supplementals are implemented in the base class but not exposed externally.
        //This allows the derived class to control whether the object appears to have
        //supplementals or not. 
        //Entity exposes Supplementals.
        internal override string OnBuildSuppTableName()
        {
            return "ARBITRATION_SUPP";
        }

        public new Supplementals Supplementals
        {
            get
            {
                return base.Supplementals;
            }
        }
        #endregion
        protected override void OnBuildNewUniqueId()
        {
            base.OnBuildNewUniqueId();
        }
    }
}
