using System;

namespace Riskmaster.DataModel
{
	//TODO EventXOsha will have a problem saving new records.  The base class will 
	// try to call GetNextUID() for a new key value and foul up referential integrity.
	// DBUpgrade Required to place both a row_id and the timestamp fields into this table.
	/// <summary>
	/// Summary description for EventXOsha.
	/// </summary>
	//TODO: Complete Summary
	[Riskmaster.DataModel.Summary("EVENT_X_OSHA","EVENT_ID", "HowAccOccurred")]
	public class EventXOsha : DataObject
	{
		#region Database Field List
		private string[,] sFields = {
										{"ActivityWhenInj","ACTIVITY_WHEN_INJ"},
									   {"EventId","EVENT_ID"},
									   {"HowAccOccurred","HOW_ACC_OCCURRED"},
									   //{"NoRulesFlag","NO_RULES_FLAG"},
									   {"ObjSubstThatInj","OBJ_SUBST_THAT_INJ"},
									   {"RecordableFlag","RECORDABLE_FLAG"},
									   //{"SafegNotUsedFlag","SAFEG_NOTUSED_FLAG"},
									   //{"SafegaurdFlag","SAFEGUARD_FLAG"},
									   {"PrivacyCaseFlag","PRIVACY_CASE_FLAG"},
									   {"SharpsObject","SHARPS_OBJECT"},
									   {"SharpsBrandMake","SHARPS_BRAND_MAKE"},
									   {"SafegNotUsed","SAFEG_NOTUSED"},
									   {"SafegProvided","SAFEG_PROVIDED"},
									   {"RulesNotFollowed","RULES_NOT_FOLLOWED"},
									   {"OshaEstabEID","OSHA_ESTAB_EID"}
    	};
		public string  ActivityWhenInj{	get{  return GetFieldString("ACTIVITY_WHEN_INJ");}	set{ SetField("ACTIVITY_WHEN_INJ",value);}}
		public int  EventId{get{  return   GetFieldInt("EVENT_ID");}set{ SetField("EVENT_ID",value);}}
		public string HowAccOccurred{get{ return GetFieldString("HOW_ACC_OCCURRED");}set{SetField("HOW_ACC_OCCURRED",value);}}
		public string ObjSubstThatInj{get{ return GetFieldString("OBJ_SUBST_THAT_INJ");}set{SetField("OBJ_SUBST_THAT_INJ",value);}}
		public bool RecordableFlag{get{ return GetFieldBool("RECORDABLE_FLAG");}set{SetField("RECORDABLE_FLAG",value);}}
		//BSB 06.06.2005  These fields deprecated in favor of code driven values below.
//		public bool NoRulesFlag{get{ return GetFieldBool("NO_RULES_FLAG");}set{SetField("NO_RULES_FLAG",value);}}
//		public bool SafegNotUsedFlag{get{ return GetFieldBool("SAFEG_NOTUSED_FLAG");}set{SetField("SAFEG_NOTUSED_FLAG",value);}}
//		public bool SafegaurdFlag{get{ return GetFieldBool("SAFEGUARD_FLAG");}set{SetField("SAFEGUARD_FLAG",value);}}
		
		//BSB 06.07.2005 This field switched from numeric to boolean to support use in the gui as a check-box.
		//The code will convert back to int for DB storage as needed.
		public bool PrivacyCaseFlag{get{ return GetFieldBool("PRIVACY_CASE_FLAG");}set{SetField("PRIVACY_CASE_FLAG",value);}}
		[ExtendedTypeAttribute(RMExtType.Code,"SHAPRS_LOG_OBJECT")]
		public int SharpsObject{get{ return GetFieldInt("SHARPS_OBJECT");}set{SetField("SHARPS_OBJECT",value);}}
		[ExtendedTypeAttribute(RMExtType.Code,"SHAPRS_LOG_BRAND")]
		public int SharpsBrandMake{get{ return GetFieldInt("SHARPS_BRAND_MAKE");}set{SetField("SHARPS_BRAND_MAKE",value);}}
		[ExtendedTypeAttribute(RMExtType.Code,"YES_NO")]
		public int SafegNotUsed{get{ return GetFieldInt("SAFEG_NOTUSED");}set{SetField("SAFEG_NOTUSED",value);}}
		[ExtendedTypeAttribute(RMExtType.Code,"YES_NO")]
		public int SafegProvided{get{ return GetFieldInt("SAFEG_PROVIDED");}set{SetField("SAFEG_PROVIDED",value);}}
		[ExtendedTypeAttribute(RMExtType.Code,"YES_NO")]
		public int RulesNotFollowed{get{ return GetFieldInt("RULES_NOT_FOLLOWED");}set{SetField("RULES_NOT_FOLLOWED",value);}}
		[ExtendedTypeAttribute(RMExtType.Entity,"OSHA_ESTABLISHMENT")]
        public int OshaEstabEID{get{ return GetFieldInt("OSHA_ESTAB_EID");}set{SetField("OSHA_ESTAB_EID",value);}}
       
		#endregion
        
		#region Supplemental Fields Exposed
		
		//Supplementals are implemented in the base class but not exposed externally.
		//This allows the derived class to control whether the object appears to have
		//supplementals or not. 
		//Entity exposes Supplementals.
		public new Supplementals Supplementals
		{	
			get
			{
				return base.Supplementals;
			}
		}
		#endregion

		internal EventXOsha(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "EVENT_X_OSHA";
			this.m_sKeyField = "EVENT_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
			//			base.InitChildren(sChildren);
			this.m_sParentClassName = "Event";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
		
		protected override void OnBuildNewUniqueId()
		{
			//Don't do anything since we're "grafted" onto the event and use the already assigned event id for this.
		}

			
		//TODO Remove this after debugging is done.  Could be a security issue.
		public override string ToString()
		{
			return (this as DataObject).Dump();
		}
	}
}
