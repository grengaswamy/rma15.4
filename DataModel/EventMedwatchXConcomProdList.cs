using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forEvXConcomProdList.
	/// </summary>
	public class EvXConcomProdList : DataCollection
	{
		internal EvXConcomProdList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"EV_CONCOM_ROW_ID";
			this.SQLFromTable =	"EV_X_CONCOM_PROD";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "EvXConcomProd";
		}
		public new EvXConcomProd this[int keyValue]{get{return base[keyValue] as EvXConcomProd;}}
		public new EvXConcomProd AddNew(){return base.AddNew() as EvXConcomProd;}
		public  EvXConcomProd Add(EvXConcomProd obj){return base.Add(obj) as EvXConcomProd;}
		public new EvXConcomProd Add(int keyValue){return base.Add(keyValue) as EvXConcomProd;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}