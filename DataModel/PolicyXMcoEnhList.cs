
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPolicyXMcoEnhList.
	/// </summary>
	public class PolicyXMcoEnhList : DataCollection
	{
		internal PolicyXMcoEnhList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"POL_X_MCO_ENH_ROW_ID";
			this.SQLFromTable =	"POLICY_X_MCO_ENH";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PolicyXMcoEnh";
		}
		public new PolicyXMcoEnh this[int keyValue]{get{return base[keyValue] as PolicyXMcoEnh;}}
		public new PolicyXMcoEnh AddNew(){return base.AddNew() as PolicyXMcoEnh;}
		public  PolicyXMcoEnh Add(PolicyXMcoEnh obj){return base.Add(obj) as PolicyXMcoEnh;}
		public new PolicyXMcoEnh Add(int keyValue){return base.Add(keyValue) as PolicyXMcoEnh;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}