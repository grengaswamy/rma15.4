
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPiXMMIHistList.
	/// </summary>
	public class PiXMMIHistList : DataCollection
	{
		internal PiXMMIHistList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"PI_X_MMIROW_ID";
			this.SQLFromTable =	"PI_X_MMI_HIST";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PiXMMIHist";
		}
		public new PiXMMIHist this[int keyValue]{get{return base[keyValue] as PiXMMIHist;}}
		public new PiXMMIHist AddNew(){return base.AddNew() as PiXMMIHist;}
		public  PiXMMIHist Add(PiXMMIHist obj){return base.Add(obj) as PiXMMIHist;}
		public new PiXMMIHist Add(int keyValue){return base.Add(keyValue) as PiXMMIHist;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}