using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPatientProcedureList.
	/// </summary>
	public class PatientXProcedureList : DataCollection
	{
		internal PatientXProcedureList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"PROC_ROW_ID";
			this.SQLFromTable =	"PATIENT_PROCEDURE";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PatientXProcedure";
		}
		public new PatientXProcedure this[int keyValue]{get{return base[keyValue] as PatientXProcedure;}}
		public new PatientXProcedure AddNew(){return base.AddNew() as PatientXProcedure;}
		public  PatientXProcedure Add(PatientXProcedure obj){return base.Add(obj) as PatientXProcedure;}
		public new PatientXProcedure Add(int keyValue){return base.Add(keyValue) as PatientXProcedure;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}