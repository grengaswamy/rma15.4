﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riskmaster.DataModel
{
    [Riskmaster.DataModel.Summary("PS_DOWNLOAD_DATA_XML", "PS_ROW_ID")]
    public class PsDownloadXMLData : DataObject
    {

        public static string ParentKeyFieldName = "TABLE_ROW_ID";
        public static string ParentNameFieldName = "TABLE_ID";
        private string[,] sFields = {
                                             {"PsRowID",   "PS_ROW_ID"},
                                        {"TableID"  ,   "TABLE_ID"},
                                        {"TableRowID"    ,  "TABLE_ROW_ID"},
                                        {"DttmRcdLastUpd",  "DTTM_RCD_LAST_UPD"},
                                        {"DttmRcdAdded"   ,     "DTTM_RCD_ADDED"},
                                        {"UpdatedByUser"  ,     "UPDATED_BY_USER"},
                                        {"AddedByUser"   ,  "ADDED_BY_USER"},
                                        {"AcordXML"     ,  "ACORD_XML"},
                                        {"PolicySystemID" , "POLICY_SYSTEM_ID"}
                                    };


        public int PsRowID { get { return GetFieldInt("PS_ROW_ID"); } set { SetField("PS_ROW_ID", value); } }
        public int TableID { get { return GetFieldInt("TABLE_ID"); } set { SetField("TABLE_ID", value); } }
        public int TableRowID { get { return GetFieldInt("TABLE_ROW_ID"); } set { SetField("TABLE_ROW_ID", value); } }

        public string AcordXML { get { return GetFieldString("ACORD_XML"); } set { SetField("ACORD_XML", value); } }

        public int PolicySystemID { get { return GetFieldInt("POLICY_SYSTEM_ID"); } set { SetField("POLICY_SYSTEM_ID", value); } }

        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }




        internal PsDownloadXMLData(bool isLocked, Context context)
            : base(isLocked, context)
        {
            this.Initialize();
        }
        new private void Initialize()
        {

            // base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

            this.m_sTableName = "PS_DOWNLOAD_DATA_XML";
            this.m_sKeyField = "PS_ROW_ID";
            this.m_sFilterClause = "";

            //Add all obect Fields into the Field Collection from our Field List.
            base.InitFields(sFields);
            //Add all object Children into the Children collection from our "sChildren" list.
            //base.InitChildren(sChildren);

            //     this.m_sParentClassName = this.ParentName;

            base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

        }


    }
}
