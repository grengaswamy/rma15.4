using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for EventXDatedTextList.
	/// </summary>
	public class EventXDatedTextList : DataCollection
	{
		internal EventXDatedTextList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"EV_DT_ROW_ID";
			this.SQLFromTable =	"EVENT_X_DATED_TEXT";
//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "EventXDatedText";
		}
		public new EventXDatedText this[int keyValue]{get{return base[keyValue] as EventXDatedText;}}
		public new EventXDatedText AddNew(){return base.AddNew() as EventXDatedText;}
		public  EventXDatedText Add(EventXDatedText obj){return base.Add(obj) as EventXDatedText;}
		public new EventXDatedText Add(int keyValue){return base.Add(keyValue) as EventXDatedText;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}
