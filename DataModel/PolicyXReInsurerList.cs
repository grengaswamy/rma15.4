using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for PolicyXReInsurerList.
	/// </summary>
    public class PolicyXReInsurerList : DataCollection
	{
		internal PolicyXReInsurerList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"POL_X_INS_REINS_ROW_ID";
			this.SQLFromTable =	"POLICY_X_INS_REINS";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
            this.TypeName = "PolicyXReInsurer";
		}
		public new PolicyXReInsurer this[int keyValue]{get{return base[keyValue] as PolicyXReInsurer;}}
        public new PolicyXReInsurer AddNew() { return base.AddNew() as PolicyXReInsurer; }
		public  PolicyXReInsurer Add(PolicyXReInsurer obj){return base.Add(obj) as PolicyXReInsurer;}
		public new PolicyXReInsurer Add(int keyValue){return base.Add(keyValue) as PolicyXReInsurer;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}