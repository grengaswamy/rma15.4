using System;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary of PropertyUnit Class.
    /// Author : Mridul Bansal.
    /// Date   : 10/19/09.
    /// MITS 18230
	/// Don't forget to add children and custom override behaviours if necessary.
	/// </summary>
	[Riskmaster.DataModel.Summary("PROPERTY_UNIT","PROPERTY_ID", "PIN")]
	public class PropertyUnit  : DataObject	
	{
		#region Database Field List
		private string[,] sFields = {
                                                        {"PropertyId", "PROPERTY_ID"},
                                                        {"Pin", "PIN"},
                                                        {"Addr1", "ADDR1"},
                                                        {"Addr2", "ADDR2"},
                                                        {"Addr3", "ADDR3"},
                                                        {"Addr4", "ADDR4"},
                                                        {"City", "CITY"},
                                                        {"StateId", "STATE_ID"},
                                                        {"ZipCode", "ZIP_CODE"},
                                                        {"ClassOfConstruction", "CLASS_OF_CONS_CODE"},
                                                        {"YearOfConstruction", "YEAR_OF_CONS"},
                                                        {"WallConstructionCode", "WALL_CONS_CODE"},
                                                        {"RoofConstructionCode", "ROOF_CONS_CODE"},
                                                        {"SquareFootage", "SQUARE_FOOTAGE"},
                                                        {"NoOfStories", "NO_OF_STORIES"},
                                                        {"AvgStoryHeight", "AVG_STORY_HEIGHT"},
                                                        {"HeatingSysCode", "HEATING_SYS_CODE"},
                                                        {"CoolingSysCode", "COOLING_SYS_CODE"},
                                                        {"FireAlarmCode", "FIRE_ALARM_CODE"},
                                                        {"SprinklersCode", "SPRINKLERS_CODE"},
                                                        {"EntryAlarmCode", "ENTRY_ALARM_CODE"},
                                                        {"PlotPlansCode", "PLOT_PLANS_CODE"},
                                                        {"FloodZoneCertCode", "FLOOD_ZONE_CERT_CODE"},
                                                        {"EarthquakeZoneCode", "EARTHQUAKE_ZONE_CODE"},
                                                        {"GPSLatitude", "GPS_LATITUDE"},
                                                        {"GPSLongitude", "GPS_LONGITUDE"},
                                                        {"GPSAltitude", "GPS_ALTITUDE"},
                                                        {"RoofAnchoringCode", "ROOF_ANCHORING_CODE"},
                                                        {"GlassStrengthCode", "GLASS_STRENGTH_CODE"},
                                                        {"AppraisedValue", "APPRAISED_VALUE"},
                                                        {"ReplacementValue", "REPLACEMENT_VALUE"},
                                                        {"DttmRcdAdded", "DTTM_RCD_ADDED"},
                                                        {"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
                                                        {"AddedByUser", "ADDED_BY_USER"},
                                                        {"UpdatedByUser", "UPDATED_BY_USER"},
                                                        {"DeletedFlag", "DELETED_FLAG"},
                                                        {"Description", "DESCRIPTION"},
                                                        {"AppraisedDate", "APPRAISED_DATE"},
                                                        {"LandValue", "LAND_VALUE"},
                                                        {"TerritoryCode","TERRITORY_CODE"},
                                                        {"CategoryCode","CATEGORY_CODE"},
                                                        {"AppraisalSourceCode","APPRAISAL_SOURCE_CODE"},
                                                         //Start:Added new field Country code, Neha Suresh Jain, 06/08/2010
                                                        { "CountryCode", "COUNTRY_CODE"},
                                                        {"LegacyUniqueIdentifier","LEGACY_UNIQUE_IDENTIFIER"},
                                                        //End: Neha Suresh Jain
                                                        {"RiskClause", "RISK_CLAUSE"} //Payal RMA:7893
		};
        public string LegacyUniqueIdentifier { get { return GetFieldString("LEGACY_UNIQUE_IDENTIFIER"); } set { SetField("LEGACY_UNIQUE_IDENTIFIER", value); } }
		public int PropertyId{get{return GetFieldInt("PROPERTY_ID");}set{SetField("PROPERTY_ID",value);}}
        public string Pin{get{return GetFieldString("PIN");}set{SetField("PIN",value);}}
        public string Addr1{get{ return	GetFieldString("ADDR1");}set{SetField("ADDR1",value);}}
		public string Addr2{get{ return	GetFieldString("ADDR2");}set{SetField("ADDR2",value);}}
        public string Addr3 { get { return GetFieldString("ADDR3"); } set { SetField("ADDR3", value); } }
        public string Addr4 { get { return GetFieldString("ADDR4"); } set { SetField("ADDR4", value); } }
		public string City{get{	return GetFieldString("CITY");}set{SetField("CITY",value);}}
        [ExtendedTypeAttribute(RMExtType.Code, "STATES")]
    	public int StateId{get{	return GetFieldInt("STATE_ID");}set{SetField("STATE_ID",value);}}
		public string ZipCode{get{ return GetFieldString("ZIP_CODE");}set{SetField("ZIP_CODE",value);}}
        [ExtendedTypeAttribute(RMExtType.Code, "CLASS_OF_CONSTRUCTION")]
        public int ClassOfConstruction{get{	return GetFieldInt("CLASS_OF_CONS_CODE");}set{SetField("CLASS_OF_CONS_CODE",value);}}
        public int YearOfConstruction{get{ return GetFieldInt("YEAR_OF_CONS");}set{SetField("YEAR_OF_CONS", value);}}
        [ExtendedTypeAttribute(RMExtType.Code, "WALL_CONSTRUCTION")]
        public int WallConstructionCode { get { return GetFieldInt("WALL_CONS_CODE"); } set { SetField("WALL_CONS_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "ROOF_CONSTRUCTION")]
        public int RoofConstructionCode { get { return GetFieldInt("ROOF_CONS_CODE"); } set { SetField("ROOF_CONS_CODE", value); } }
        public double SquareFootage{get{return GetFieldDouble("SQUARE_FOOTAGE");}set{SetField("SQUARE_FOOTAGE",value);}}
        public int NoOfStories{get{return GetFieldInt("NO_OF_STORIES");}set{SetField("NO_OF_STORIES", value);}}
        public double AvgStoryHeight{get{return GetFieldDouble("AVG_STORY_HEIGHT");}set{SetField("AVG_STORY_HEIGHT", value);}}
        [ExtendedTypeAttribute(RMExtType.Code, "HEATING_SYSTEM")]
        public int HeatingSysCode{get{ return GetFieldInt("HEATING_SYS_CODE");}set{SetField("HEATING_SYS_CODE", value);}}
        [ExtendedTypeAttribute(RMExtType.Code, "COOLING_SYSTEM")]
        public int CoolingSysCode { get { return GetFieldInt("COOLING_SYS_CODE"); } set { SetField("COOLING_SYS_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "FIRE_ALARM")]
        public int FireAlarmCode{get{ return GetFieldInt("FIRE_ALARM_CODE");}set{SetField("FIRE_ALARM_CODE", value);}}
        [ExtendedTypeAttribute(RMExtType.Code, "SPRINKLERS")]
        public int SprinklersCode{get{ return GetFieldInt("SPRINKLERS_CODE");}set{SetField("SPRINKLERS_CODE", value);}}
        [ExtendedTypeAttribute(RMExtType.Code, "ENTRY_ALARM")]
        public int EntryAlarmCode{get{ return GetFieldInt("ENTRY_ALARM_CODE");}set{SetField("ENTRY_ALARM_CODE", value);}}
        [ExtendedTypeAttribute(RMExtType.Code, "PLOT_PLANS")]
        public int PlotPlansCode{get{ return GetFieldInt("PLOT_PLANS_CODE");}set{SetField("PLOT_PLANS_CODE", value);}}
        [ExtendedTypeAttribute(RMExtType.Code, "FLOOD_ZONE_CERT")]
        public int FloodZoneCertCode{get{ return GetFieldInt("FLOOD_ZONE_CERT_CODE");}set{SetField("FLOOD_ZONE_CERT_CODE", value);}}
        [ExtendedTypeAttribute(RMExtType.Code, "EARTHQUAKE_ZONE")]
        public int EarthquakeZoneCode{get{ return GetFieldInt("EARTHQUAKE_ZONE_CODE");}set{SetField("EARTHQUAKE_ZONE_CODE", value);}}
        public double GPSLatitude{get{return GetFieldDouble("GPS_LATITUDE");}set{SetField("GPS_LATITUDE", value);}}
        public double GPSLongitude{get{return GetFieldDouble("GPS_LONGITUDE");}set{SetField("GPS_LONGITUDE", value);}}
        public double GPSAltitude{get{return GetFieldDouble("GPS_ALTITUDE");}set{SetField("GPS_ALTITUDE", value);}}
        [ExtendedTypeAttribute(RMExtType.Code, "ROOF_ANCHORING")]
        public int RoofAnchoringCode{get{ return GetFieldInt("ROOF_ANCHORING_CODE");}set{SetField("ROOF_ANCHORING_CODE", value);}}
        [ExtendedTypeAttribute(RMExtType.Code, "GLASS_STRENGTH")]
        public int GlassStrengthCode{get{ return GetFieldInt("GLASS_STRENGTH_CODE");}set{SetField("GLASS_STRENGTH_CODE", value);}}
        public double AppraisedValue{get{return GetFieldDouble("APPRAISED_VALUE");}set{SetField("APPRAISED_VALUE",value);}}
        public double ReplacementValue{get{return GetFieldDouble("REPLACEMENT_VALUE");}set{SetField("REPLACEMENT_VALUE",value);}}
        public string DttmRcdAdded { get { return GetFieldString("DTTM_RCD_ADDED"); } set { SetField("DTTM_RCD_ADDED", value); } }
        public string DttmRcdLastUpd { get { return GetFieldString("DTTM_RCD_LAST_UPD"); } set { SetField("DTTM_RCD_LAST_UPD", value); } }
        public string UpdatedByUser { get { return GetFieldString("UPDATED_BY_USER"); } set { SetField("UPDATED_BY_USER", value); } }
        public string AddedByUser { get { return GetFieldString("ADDED_BY_USER"); } set { SetField("ADDED_BY_USER", value); } }
        public bool DeletedFlag { get { return GetFieldBool("DELETED_FLAG"); } set { SetField("DELETED_FLAG", value); } }
        public string Description { get { return GetFieldString("DESCRIPTION"); } set { SetField("DESCRIPTION", value); } }
        public string AppraisedDate { get { return GetFieldString("APPRAISED_DATE"); } set { SetField("APPRAISED_DATE", Riskmaster.Common.Conversion.GetDate(value)); } }
        public double LandValue { get { return GetFieldDouble("LAND_VALUE"); } set { SetField("LAND_VALUE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "TERRITORY_CODE")]
        public int TerritoryCode { get { return GetFieldInt("TERRITORY_CODE"); } set { SetField("TERRITORY_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "CATEGORY_CODE")]
        public int CategoryCode { get { return GetFieldInt("CATEGORY_CODE"); } set { SetField("CATEGORY_CODE", value); } }
        [ExtendedTypeAttribute(RMExtType.Code, "APPRAISAL_SOURCE")]
        public int AppraisalSourceCode { get { return GetFieldInt("APPRAISAL_SOURCE_CODE"); } set { SetField("APPRAISAL_SOURCE_CODE", value); } }
        //Start:Neha Suresh Jain, 06/08/2010, to save country information
        [ExtendedTypeAttribute(RMExtType.Code, "COUNTRY")]
        public int CountryCode { get { return GetFieldInt("COUNTRY_CODE"); } set { SetField("COUNTRY_CODE", value); } }
        //End: Neha Suresh Jain
        public string RiskClause { get { return GetFieldString("RISK_CLAUSE"); } set { SetField("RISK_CLAUSE", value); } } //Payal RMA:7893
        #endregion
		
		#region Child Implementation

		//Handle adding the our key to any additions to the record collection properties.
		internal override void OnChildItemAdded(string childName, IDataModel itemValue)
		{
			base.OnChildItemAdded (childName, itemValue);
		}					

		//Handle child saves.  Set the current key into the children since this could be a new record.
		override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
		{
			base.OnChildPostSave (childName, childValue, isNew);
		}		
		#endregion
		
        internal PropertyUnit(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}

		new private void Initialize()
		{
			this.m_sTableName = "PROPERTY_UNIT";
			this.m_sKeyField = "PROPERTY_ID";
			this.m_sFilterClause = string.Empty;
			//Add all object Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
            this.m_sParentClassName = string.Empty;
            //Moved after most init logic so that scripting can be called successfully.
			base.Initialize();  
		}
		
        internal override string OnBuildDeleteSQL()
		{
			return "UPDATE PROPERTY_UNIT SET DELETED_FLAG = -1 WHERE PROPERTY_ID=" + this.PropertyId;
		}

		// Cancel Deletion of Supplementals during "delete" of this object.
		internal override bool OnSuppDelete()
		{
			return false;
		}

		// Cancel Deletion of all PropertyUnit children.
		// (Obviously will these children would be needed if the entity is ever 
		// "undeleted" by changing the entity deleted flag.
		internal override void OnChildDelete(string childName, IDataModel childValue)
		{
			return;
		}

		internal override string OnApplyFilterClauseSQL(string CurrentSQL)
		{
			string sExtendedFilterClause = this.m_sFilterClause;
			string sSQL = CurrentSQL;
            if (!string.IsNullOrEmpty(sExtendedFilterClause))
            {
                sExtendedFilterClause += " AND ";
            }
			sExtendedFilterClause += "DELETED_FLAG=0";

            if (!string.IsNullOrEmpty(sExtendedFilterClause))
            {
                if (sSQL.IndexOf(" WHERE ") > 0)
                {
                    sSQL += " AND (" + sExtendedFilterClause + ")";
                }
                else
                {
                    sSQL += " WHERE " + sExtendedFilterClause;
                }
            }
			return sSQL;
		}

		protected override void OnBuildNewUniqueId()
		{
			base.OnBuildNewUniqueId();
            if (this.Pin == "" && this.Context.InternalSettings.SysSettings.AutoNumPin)
            {
                this.Pin = GetPin();
            }
		}

		private string GetPin()
		{
			int iPin= this.Context.GetNextUID("PIN_NUMBERS");
			return iPin.ToString("0000000");
		}

		#region Supplemental Fields Exposed
		
        /// <summary>
        /// Supplementals are implemented in the base class but not exposed externally.
		///This allows the derived class to control whether the object appears to have
		///supplementals or not. Entity exposes Supplementals.
        /// </summary>
		public new Supplementals Supplementals
		{	
			get
			{
				return base.Supplementals;
			}
		}
		#endregion
	}
}