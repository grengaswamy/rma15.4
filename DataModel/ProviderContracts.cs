using System;
using System.Collections;
using Riskmaster.Db;

namespace Riskmaster.DataModel
{
	///************************************************************** 
	///* $File		: ProviderContracts.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 28-Jan-2005
	///* $Author	: Nikhil Kr. Garg
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	/// Provides access to the ProviderContracts Database Table
	/// </summary>
	[Riskmaster.DataModel.Summary("PROVIDER_CONTRACTS","PRVD_CONT_ROW_ID", "ProviderEid")] 
	public class ProviderContracts: DataObject	
	{
		#region Database Field List
		//list of Database fields
		private string[,] m_sFields = {
										{"PrvdContRowId", "PRVD_CONT_ROW_ID"},
										{"ProviderEid", "PROVIDER_EID"},
										{"StartDate", "START_DATE"},
										{"EndDate", "END_DATE"},
										{"Amount", "AMOUNT"},
										{"Discount", "DISCOUNT"},
										{"Comments", "COMMENTS"},
										{"FeeTableId", "FEE_TABLE_ID"},
										{"FirstRate", "FIRST_RATE"},
										{"LastRate", "LAST_RATE"},
										{"RateChg", "RATE_CHG"},
										{"DiscOnSchdFlag", "DISC_ON_SCHD_FLAG"},
										{"DiscOnBillFlag", "DISC_ON_BILL_FLAG"},
										{"SecondRate", "SECOND_RATE"},
										{"ThirdRate", "THIRD_RATE"},
										{"RateChg2nd", "RATE_CHG_2ND"},
										{"Disc2ndSchdFlag", "DISC_2ND_SCHD_FLAG"},
										{"Discount2nd", "DISCOUNT_2ND"},
										{"DiscountBill", "DISCOUNT_BILL"},
										{"FeeTableId2nd", "FEE_TABLE_ID_2ND"}
									};
		/*Following consts added by Tanuj for fee schedules*/
		private const int APPLY_UNKNOWN = -1;
		private const int idbUCR94 = 0 ; // ingenix/Medicode Table Type (CALC_TYPE)
		private const int idbUCR95 = 1 ; 
		private const int idbWC99 = 2  ;   
		private const int idbOPt = 3   ;
		private const int idbHCPCS = 4 ;

		private const int idbAnes = 7  ;
		private const int idbDent = 8   ;
		private const int idbOhio96 = 9 ; // Caredata/Medirisk Table Type (CALC_TYPE)
		private const int idbOhio97 = 10 ;// 98,99 Caredata/Medirisk
		private const int idbStAnth = 11; // 5/1998 Mirage custom rjn (now owned by ingenix)
		private const int idbBasic = 12 ;//"Basic User Entered Schedule" 9/1999 Millennium custom dcm
		private const int idbWC00 = 13  ;// "Workers' Compensation 2000" ingenix/Medicode Table Type dcm
		private const int idbExtended = 14 ; // "Extended BRS Schedule" 4/2002 dcm
		private const int idbFreePharm = 15 ;// "Free Entry Pharmacy" To allow Pharmacy Fee Entry without a Table. 6/2003 dcm
		private const int idbFreeHosp = 16 ;// "Free Entry Hospital" To allow Hospital Fee Entry without a Table. 6/2003 dcm
		private const int idbFreeGeneric = 17; // "Generic Free Entry" To allow Fee Entry without a Table. 6/2003 dcm
		private const int idbMax = idbFreeGeneric  ;//  6/2003 dcm  was idbExtended  ' 4/2002 dcm

		/// <summary>
		/// Gets & sets the Prvd Cont Row Id
		/// </summary>
		public int PrvdContRowId
		{
			get
			{
				return GetFieldInt("PRVD_CONT_ROW_ID");
			}
			set
			{
				SetField("PRVD_CONT_ROW_ID",value);
			}
		}

		/// <summary>
		/// Gets & sets the Provider EId
		/// </summary>
		public int ProviderEid
		{
			get
			{
				return GetFieldInt("PROVIDER_EID");
			}
			set
			{
				SetField("PROVIDER_EID",value);
			}
		}

		/// <summary>
		/// Gets & sets the Start Date
		/// </summary>
		public string StartDate
		{
			get
			{
				return GetFieldString("START_DATE");
			}
			set
			{
				SetField("START_DATE",Riskmaster.Common.Conversion.GetDate(value));
			}
		}
		/// <summary>
		/// Gets & sets the End Date
		/// </summary>
		public string EndDate
		{
			get
			{
				return GetFieldString("END_DATE");
			}
			set
			{
				SetField("END_DATE",Riskmaster.Common.Conversion.GetDate(value));
			}
		}
       
		/// <summary>
		/// Gets & sets the Amount
		/// </summary>
		public double Amount
		{
			get
			{
				return GetFieldDouble("AMOUNT");
			}
			set
			{
				SetField("AMOUNT",value);
			}
		}
		/// <summary>
		/// Gets & sets the Discount
		/// </summary>
		public double Discount
		{
			get
			{
				return GetFieldDouble("DISCOUNT");
			}
			set
			{
				SetField("DISCOUNT",value);
			}
		}

		/// <summary>
		/// Gets & sets the Comments
		/// </summary>
		public string Comments
		{
			get
			{
				return GetFieldString("COMMENTS");
			}
			set
			{
				SetField("COMMENTS",value);
			}
		}
		/// <summary>
		/// Gets & sets the Fee Table Id
		/// </summary>
		public int FeeTableId
		{
			get
			{
				return GetFieldInt("FEE_TABLE_ID");
			}
			set
			{
				SetField("FEE_TABLE_ID",value);
			}
		}

		/// <summary>
		/// Gets & sets the First Rate
		/// </summary>
		public double FirstRate
		{
			get
			{
				return GetFieldDouble("FIRST_RATE");
			}
			set
			{
				SetField("FIRST_RATE",value);
			}
		}
		/// <summary>
		/// Gets & sets the Last Rate
		/// </summary>
		public double LastRate
		{
			get
			{
				return GetFieldDouble("LAST_RATE");
			}
			set
			{
				SetField("LAST_RATE",value);
			}
		}

		/// <summary>
		/// Gets & sets the Rate Chg
		/// </summary>
		public int RateChg
		{
			get
			{
				return GetFieldInt("RATE_CHG");
			}
			set
			{
				SetField("RATE_CHG",value);
			}
		}

		/// <summary>
		/// Gets & sets the Disc On Schd Flag
		/// </summary>
		public bool DiscOnSchdFlag
		{
			get
			{
				return GetFieldBool("DISC_ON_SCHD_FLAG");
			}
			set
			{
				SetField("DISC_ON_SCHD_FLAG",value);
			}
		}
		/// <summary>
		/// Gets & sets the Disc On Bill Flag
		/// </summary>
		public bool DiscOnBillFlag
		{
			get
			{
				return GetFieldBool("DISC_ON_BILL_FLAG");
			}
			set
			{
				SetField("DISC_ON_BILL_FLAG",value);
			}
		}
		/// <summary>
		/// Gets & sets the Second Rate
		/// </summary>
		public double SecondRate
		{
			get
			{
				return GetFieldDouble("SECOND_RATE");
			}
			set
			{
				SetField("SECOND_RATE",value);
			}
		}
		/// <summary>
		/// Gets & sets the Disc On Third Rate
		/// </summary>
		public double ThirdRate
		{
			get
			{
				return GetFieldDouble("THIRD_RATE");
			}
			set
			{
				SetField("THIRD_RATE",value);
			}
		}

		/// <summary>
		/// Gets & sets the Rate Chg 2nd
		/// </summary>
		public int RateChg2nd
		{
			get
			{
				return GetFieldInt("RATE_CHG_2ND");
			}
			set
			{
				SetField("RATE_CHG_2ND",value);
			}
		}

		/// <summary>
		/// Gets & sets the Disc 2nd Schd Flag
		/// </summary>
		public bool Disc2ndSchdFlag
		{
			get
			{
				return GetFieldBool("DISC_2ND_SCHD_FLAG");
			}
			set
			{
				SetField("DISC_2ND_SCHD_FLAG",value);
			}
		}
		/// <summary>
		/// Gets & sets the Discount 2nd
		/// </summary>
		public double Discount2nd
		{
			get
			{
				return GetFieldDouble("DISCOUNT_2ND");
			}
			set
			{
				SetField("DISCOUNT_2ND",value);
			}
		}
		/// <summary>
		/// Gets & sets the Discount Bill
		/// </summary>
		public double DiscountBill
		{
			get
			{
				return GetFieldDouble("DISCOUNT_BILL");
			}
			set
			{
				SetField("DISCOUNT_BILL",value);
			}
		}

		/// <summary>
		/// Gets & sets the Fee Table Id 2nd
		/// </summary>
		public int FeeTableId2nd
		{
			get
			{
				return GetFieldInt("FEE_TABLE_ID_2ND");
			}
			set
			{
				SetField("FEE_TABLE_ID_2ND",value);
			}
		}

		#endregion

		#region Constructor
		/// <summary>
		///	Initializes the Provider Contracts class
		/// </summary>
		/// <param name="p_bIsLocked">Whether the current Provider Contract object is locked</param>
		/// <param name="p_objContext">Global context information for the current Provider Contract object</param>
		internal ProviderContracts(bool p_bIsLocked,Context p_objContext):base(p_bIsLocked, p_objContext)
		{
			this.Initialize();
		}
		#endregion

		#region Private Functions
		/// Name			: Initialize
		/// Author			: Nikhil Kr. Garg
		/// Date Created	: 28-Jan-2005	
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Initialize the Provider contract class object.
		/// </summary>
		new private void Initialize()
		{
			this.m_sTableName = "PROVIDER_CONTRACTS";
			this.m_sKeyField = "PRVD_CONT_ROW_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(m_sFields);
			this.m_sParentClassName = "";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.
			
			  
		}
		
		public Hashtable FeeScheds
		{
			/*Added by Tanuj for fetching fee schedules*/
			get
			{
			   string sSql= "SELECT TABLE_ID,USER_TABLE_NAME" +
					        " FROM FEE_TABLES" +
					        " WHERE CALC_TYPE IN (" + idbWC99 + "," + idbWC00 + "," + idbOhio96 + "," + idbOhio97 + "," + idbStAnth + "," + idbExtended + ")" +
					        " ORDER BY USER_TABLE_NAME";
				return this.KeyVal(sSql);
			}
			
		}
		
		public Hashtable FeeScheds2
		{
			/*Added by Tanuj for fetching fee schedules*/
			get
			{
				 string sSql= "SELECT TABLE_ID,USER_TABLE_NAME" +
                              " FROM FEE_TABLES" +
                              " WHERE CALC_TYPE IN (" + idbUCR94 + "," + idbUCR95 + "," + idbOPt + "," + idbHCPCS + "," + idbAnes + "," + idbDent + "," + idbBasic + "," + idbFreePharm + "," + idbFreeHosp + "," + idbFreeGeneric + ")" +
                              " ORDER BY USER_TABLE_NAME";
				return this.KeyVal(sSql);
			}
		}
        
	   private Hashtable KeyVal(string p_sSql)
		{
			Hashtable htFeeScheds=new Hashtable();
			DbReader objReader;
			objReader=base.Context.DbConn.ExecuteReader(p_sSql);
			if(objReader!=null)
			{
				while(objReader.Read())
				{
					htFeeScheds.Add(objReader.GetInt32(0),objReader.GetString(1));
				}
				objReader.Close();
			}
			objReader=null;
			return htFeeScheds;
		}
		
		#endregion
	}
}
