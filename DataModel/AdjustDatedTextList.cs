
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forAdjustDatedTextList.
	/// </summary>
	public class AdjustDatedTextList : DataCollection
	{
		internal AdjustDatedTextList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"ADJ_DTTEXT_ROW_ID";
			this.SQLFromTable =	"ADJUST_DATED_TEXT";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "AdjustDatedText";
		}
		public new AdjustDatedText this[int keyValue]{get{return base[keyValue] as AdjustDatedText;}}
		public new AdjustDatedText AddNew(){return base.AddNew() as AdjustDatedText;}
		public  AdjustDatedText Add(AdjustDatedText obj){return base.Add(obj) as AdjustDatedText;}
		public new AdjustDatedText Add(int keyValue){return base.Add(keyValue) as AdjustDatedText;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}