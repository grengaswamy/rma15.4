
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forBillXAdjstmntList.
	/// </summary>
	public class BillXAdjstmntList : DataCollection
	{
		internal BillXAdjstmntList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"";
			this.SQLFromTable =	"BILL_X_ADJSTMNT";
//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "BillXAdjstmnt";
		}
		public new BillXAdjstmnt this[int keyValue]{get{return base[keyValue] as BillXAdjstmnt;}}
		public new BillXAdjstmnt AddNew(){return base.AddNew() as BillXAdjstmnt;}
		public  BillXAdjstmnt Add(BillXAdjstmnt obj){return base.Add(obj) as BillXAdjstmnt;}
		public new BillXAdjstmnt Add(int keyValue){return base.Add(keyValue) as BillXAdjstmnt;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}