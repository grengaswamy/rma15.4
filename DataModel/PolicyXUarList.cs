using System;
using System.Text;

namespace Riskmaster.DataModel
{
	/// <summary>
    /// Summary description for PolicyXUarList.
    /// Author: Sumit Kumar
    /// Date: 03/16/2010
    /// MITS: 18229 
	/// </summary>
    public class PolicyXUarList : DataCollection
	{
		internal PolicyXUarList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"UAR_ROW_ID";
			this.SQLFromTable =	"POLICY_X_UAR";
            this.TypeName = "PolicyXUar";
		}
        public new PolicyXUar this[int keyValue] { get { return base[keyValue] as PolicyXUar; } }
        public new PolicyXUar AddNew() { return base.AddNew() as PolicyXUar; }
        public PolicyXUar Add(PolicyXUar obj) { return base.Add(obj) as PolicyXUar; }//Sumit
        public new PolicyXUar Add(int keyValue) { return base.Add(keyValue) as PolicyXUar; }
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
            StringBuilder s = new StringBuilder();
            foreach (DataObject obj in this)
            {
                s.Append(obj.Dump());
            }
            return s.ToString();
		}
	}
}