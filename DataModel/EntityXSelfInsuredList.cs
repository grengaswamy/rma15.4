using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description for EntityXSelfInsuredList.
	/// </summary>
	public class EntityXSelfInsuredList : DataCollection
	{
		internal EntityXSelfInsuredList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"SI_ROW_ID";
			this.SQLFromTable =	"ENTITY_X_SELFINSUR";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "EntityXSelfInsured";
		}
		public new EntityXSelfInsured this[int keyValue]{get{return base[keyValue] as EntityXSelfInsured;}}
		public new EntityXSelfInsured AddNew(){return base.AddNew() as EntityXSelfInsured;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}