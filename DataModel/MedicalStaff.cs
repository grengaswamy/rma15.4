
using System;
using Riskmaster.Common;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary of MedicalStaff AutoGenerated Class.
	/// Don't forget to add children and custom override behaviors if necessary.
	/// </summary>
	[Riskmaster.DataModel.Summary("MED_STAFF","STAFF_EID")]
	public class MedicalStaff  : DataObject	
	{
		public override string Default
		{
			get
			{
				if(this.StaffEid> 0)
					return Context.LocalCache.GetEntityLastFirstName(this.StaffEid);
				return "";
			}
		}
		#region Database Field List
		private string[,] sFields = {
														{"StaffEid", "STAFF_EID"},
														{"MedicalStaffNumber", "MED_STAFF_NUMBER"},
														{"PrimaryPolicyId", "PRIMARY_POLICY_ID"},
														{"HomeAddr1", "HOME_ADDR1"},
														{"HomeAddr2", "HOME_ADDR2"},
                                                        {"HomeAddr3", "HOME_ADDR3"},
														{"HomeAddr4", "HOME_ADDR4"},
														{"HomeCity", "HOME_CITY"},
														{"HomeStateId", "HOME_STATE_ID"},
														{"HomeZipCode", "HOME_ZIP_CODE"},
														{"MaritalStatCode", "MARITAL_STAT_CODE"},
														{"BeeperNumber", "BEEPER_NUMBER"},
														{"CellularNumber", "CELLULAR_NUMBER"},
														{"EmergencyContact", "EMERGENCY_CONTACT"},
														{"StaffStatusCode", "STAFF_STATUS_CODE"},
														{"StaffPosCode", "STAFF_POS_CODE"},
														{"StaffCatCode", "STAFF_CAT_CODE"},
														{"DeptAssignedEid", "DEPT_ASSIGNED_EID"},
														{"HireDate", "HIRE_DATE"},
														{"LicNum", "LIC_NUM"},
														{"LicState", "LIC_STATE"},
														{"LicIssueDate", "LIC_ISSUE_DATE"},
														{"LicExpiryDate", "LIC_EXPIRY_DATE"},
														{"LicDeaNum", "LIC_DEA_NUM"},
														{"LicDeaExpDate", "LIC_DEA_EXP_DATE"}
		};

//		[ExtendedTypeAttribute(RMExtType.Entity,"MEDICAL_STAFF")]
        public int StaffEid{get{return GetFieldInt("STAFF_EID");}set{SetFieldAndNavTo("STAFF_EID",value,"StaffEntity");}}
		public string MedicalStaffNumber{get{return GetFieldString("MED_STAFF_NUMBER");}set{SetField("MED_STAFF_NUMBER",value);}}
		[ExtendedTypeAttribute(RMExtType.ChildLink,"Policy")]
		public int PrimaryPolicyId{get{return GetFieldInt("PRIMARY_POLICY_ID");}set{SetField("PRIMARY_POLICY_ID",value);}}
		public string HomeAddr1{get{return GetFieldString("HOME_ADDR1");}set{SetField("HOME_ADDR1",value);}}
		public string HomeAddr2{get{return GetFieldString("HOME_ADDR2");}set{SetField("HOME_ADDR2",value);}}
        public string HomeAddr3 { get { return GetFieldString("HOME_ADDR3"); } set { SetField("HOME_ADDR3", value); } }
        public string HomeAddr4 { get { return GetFieldString("HOME_ADDR4"); } set { SetField("HOME_ADDR4", value); } }
		public string HomeCity{get{return GetFieldString("HOME_CITY");}set{SetField("HOME_CITY",value);}}
		[ExtendedTypeAttribute(RMExtType.Code,"STATES")]
		public int HomeStateId{get{return GetFieldInt("HOME_STATE_ID");}set{SetField("HOME_STATE_ID",value);}}
		public string HomeZipCode{get{return GetFieldString("HOME_ZIP_CODE");}set{SetField("HOME_ZIP_CODE",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"MARITAL_STATUS")]
        public int MaritalStatCode{get{return GetFieldInt("MARITAL_STAT_CODE");}set{SetField("MARITAL_STAT_CODE",value);}}
		public string BeeperNumber{get{return GetFieldString("BEEPER_NUMBER");}set{SetField("BEEPER_NUMBER",value);}}
		public string CellularNumber{get{return GetFieldString("CELLULAR_NUMBER");}set{SetField("CELLULAR_NUMBER",value);}}
		public string EmergencyContact{get{return GetFieldString("EMERGENCY_CONTACT");}set{SetField("EMERGENCY_CONTACT",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"STAFF_STATUS")]
        public int StaffStatusCode{get{return GetFieldInt("STAFF_STATUS_CODE");}set{SetField("STAFF_STATUS_CODE",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"STAFF_POSITIONS")]
        public int StaffPosCode{get{return GetFieldInt("STAFF_POS_CODE");}set{SetField("STAFF_POS_CODE",value);}}
        [ExtendedTypeAttribute(RMExtType.Code,"STAFF_CATEGORY")]
        public int StaffCatCode{get{return GetFieldInt("STAFF_CAT_CODE");}set{SetField("STAFF_CAT_CODE",value);}}
		[ExtendedTypeAttribute(RMExtType.OrgH,"DEPARTMENT")]
        public int DeptAssignedEid{get{return GetFieldInt("DEPT_ASSIGNED_EID");}set{SetField("DEPT_ASSIGNED_EID",value);}}
		public string HireDate{get{return GetFieldString("HIRE_DATE");}set{SetField("HIRE_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string LicNum{get{return GetFieldString("LIC_NUM");}set{SetField("LIC_NUM",value);}}
		[ExtendedTypeAttribute(RMExtType.Code,"STATES")]
		public int LicState{get{return GetFieldInt("LIC_STATE");}set{SetField("LIC_STATE",value);}}
		public string LicIssueDate{get{return GetFieldString("LIC_ISSUE_DATE");}set{SetField("LIC_ISSUE_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string LicExpiryDate{get{return GetFieldString("LIC_EXPIRY_DATE");}set{SetField("LIC_EXPIRY_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string LicDeaNum{get{return GetFieldString("LIC_DEA_NUM");}set{SetField("LIC_DEA_NUM",value);}}
		public string LicDeaExpDate{get{return GetFieldString("LIC_DEA_EXP_DATE");}set{SetField("LIC_DEA_EXP_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		#endregion

		private string[,] sChildren = {{"CertificationList","MedicalStaffCertList"},
																{"PrivilegeList","MedicalStaffPrivilegeList"},
																{"StaffEntity","Entity"}
									  };
		//Lock Entity Table into MedStaff for StaffEntity Entity object..
		override internal  void OnChildInit(string childName, string childType)
		{
			//Do default per-child processing.
			base.OnChildInit(childName, childType);
			
			//Do any custom per-child processing.
			object obj = base.m_Children[childName];
			switch(childType)
			{
				case "Entity": //Is StaffEntity
					Entity objEnt = (obj as Entity);
                    objEnt.EntityTableId = Context.LocalCache.GetTableId(Globalization.IndividualGlossaryTableNames.MEDICAL_STAFF.ToString());
                    objEnt.LockEntityTableChange = true;
					//no real way to lock entity class navigation from internal access.
					break;
			}
		}
		//Handle adding the our key to any additions to the record collection properties.
		internal override void OnChildItemAdded(string childName, IDataModel itemValue)
		{
			switch(childName)
			{
				case "CertificationList" :
					(itemValue as MedicalStaffCert).StaffEid= this.StaffEid;
					break;
				case "PrivilegeList" :
					(itemValue as MedicalStaffPrivilege).StaffEid= this.StaffEid;
					break;
			}
			base.OnChildItemAdded (childName, itemValue);
		}					
		internal override void LoadData(Riskmaster.Db.DbReader objReader)
		{
			base.LoadData (objReader);
			//BSB Fix for AutoNav Child(ren) Not Loaded before pre-save.
			this.SetFieldAndNavTo("STAFF_EID",this.StaffEid,"StaffEntity",true);
		}
		//Handle any sub-object(s) for which we need the key in our own table.
		internal override void OnChildPreSave(string childName, IDataModel childValue)
		{
			// If Entity is involved we save this first (in case we need the EntityId.)
			if(childName=="StaffEntity")
			{
				(childValue as IPersistence).Save();
				this.m_Fields["STAFF_EID"]= (childValue as Entity).EntityId;
			}
		}
		//Handle child saves.  Set the current key into the children since this could be a new record.
		override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
		{
			switch(childName)
			{
				case "CertificationList":
					if(isNew)
						foreach(MedicalStaffCert item in (childValue as MedicalStaffCertList))
							item.StaffEid= this.StaffEid;
					(childValue as IPersistence).Save();
					break;
				case "PrivilegeList":
					if(isNew)
						foreach(MedicalStaffPrivilege item in (childValue as MedicalStaffPrivilegeList))
							item.StaffEid= this.StaffEid;
					(childValue as IPersistence).Save();
					break;
			}
			base.OnChildPostSave (childName, childValue, isNew);
		}		

		// BSB 04.18.2007 Cascading Delete Fix.
		// Delete will NOT cascade down onto Employee Entity.
		// We wish to "Flag" our entity as deleted if and only if
		// the delete is initiated on this object.
		// That is the invocation strategy for this "OnPreCommitDelete" routine.
		// It ONLY runs on the object that client code initiated the deletion.
		// It is NEVER invoked on a "cascading" deleted object. 
		protected override void OnPreCommitDelete()
		{
			// BSB Go Ahead and Flag our Entity since by virtue of being in this particular
			// override we KNOW that the user initiated a deletion against us directly.
            //rkatyal start : worked on JIRA - 18986
            if (this.Context.InternalSettings.SysSettings.UseEntityRole)
                base.Delete();
            else
                this.StaffEntity.Delete(); // This call simply flags the entity in the RMX database.
			// True record deletion is not supported for entities within the 
			// base product by ANY MEANS.
            //rkatyal end
		}

		//Child Property Accessors
		public Entity StaffEntity
		{
			get
			{
				Entity objItem = base.m_Children["StaffEntity"] as Entity;
				if((objItem as IDataModel).IsStale)
					objItem = this.CreatableMoveChildTo("StaffEntity",(objItem as DataObject),this.StaffEid) as Entity;
				return objItem;
			}
		}	

		public MedicalStaffCertList CertificationList
		{
			get
			{
				MedicalStaffCertList objList = base.m_Children["CertificationList"] as MedicalStaffCertList;
				if (!base.IsNew && (objList as IDataModel).IsStale)
					objList.SQLFilter = "STAFF_EID=" + this.StaffEid;
				return objList;
			}
		}	
		public MedicalStaffPrivilegeList PrivilegeList
		{
			get
			{
				MedicalStaffPrivilegeList objList = base.m_Children["PrivilegeList"] as MedicalStaffPrivilegeList;
				if (!base.IsNew && (objList as IDataModel).IsStale)
					objList.SQLFilter = "STAFF_EID=" + this.StaffEid;
				return objList;
			}
		}	
	
		internal MedicalStaff(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		//Unique Id's are set during "Pre-Child Save" for new records.
		// Don't allow this function (or base implementation) to mess with them here.
		protected override void OnBuildNewUniqueId()
		{
			if(this.KeyFieldValue ==0)
                throw new Riskmaster.ExceptionTypes.DataModelException(Riskmaster.Common.Globalization.GetString("MedicalStaff.SaveWithMissingEntity", this.Context.ClientId));
		}
		//Otherwise will cause endless recursive loop when save initiated from PiPhysician.
		protected override bool	OnForceParentSave()
		{
			return false;
		}

		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "MED_STAFF";
			this.m_sKeyField = "STAFF_EID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
			base.InitChildren(sChildren);
			this.m_sParentClassName = "";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
		#region Supplemental Fields Exposed
		internal override string OnBuildSuppTableName()
		{
			return "MED_STAFF_SUPP";
		}

		//Supplementals are implemented in the base class but not exposed externally.
		//This allows the derived class to control whether the object appears to have
		//supplementals or not. 
		//Entity exposes Supplementals.
		public new Supplementals Supplementals
		{	
			get
			{
				return base.Supplementals;
			}
		}
		#endregion
	}
}