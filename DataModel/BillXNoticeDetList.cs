
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forBillXNoticeDetList.
	/// </summary>
	public class BillXNoticeDetList : DataCollection
	{
		internal BillXNoticeDetList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"NOTICE_DET_ROWID";
			this.SQLFromTable =	"BILL_X_NOTICE_DET";
//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "BillXNoticeDet";
		}
		public new BillXNoticeDet this[int keyValue]{get{return base[keyValue] as BillXNoticeDet;}}
		public new BillXNoticeDet AddNew(){return base.AddNew() as BillXNoticeDet;}
		public  BillXNoticeDet Add(BillXNoticeDet obj){return base.Add(obj) as BillXNoticeDet;}
		public new BillXNoticeDet Add(int keyValue){return base.Add(keyValue) as BillXNoticeDet;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}