
using System;

namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary of FundsAutoBatch AutoGenerated Class.
	/// Don't forget to add children and custom override behaviors if necessary.
	/// </summary>
	[Riskmaster.DataModel.Summary("FUNDS_AUTO_BATCH","AUTO_BATCH_ID")]
	public class FundsAutoBatch  : DataObject	
	{
		public override string Default
		{
			get
			{
				return String.Format("{0} - {1} {2}", Common.Conversion.ToDate(this.StartDate).ToShortDateString(),Common.Conversion.ToDate(this.EndDate).ToShortDateString(),this.CurrentPayment.ToString());
			}
		}

		#region Database Field List
		private string[,] sFields = {
														{"AutoBatchId", "AUTO_BATCH_ID"},
														{"TotalPayments", "TOTAL_PAYMENTS"},
														{"CurrentPayment", "CURRENT_PAYMENT"},
														{"PaymentInterval", "PAYMENT_INTERVAL"},
														{"StartDate", "START_DATE"},
														{"EndDate", "END_DATE"},
														{"BenStartDate", "BEN_START_DATE"},
														{"BenEndDate", "BEN_END_DATE"},
														{"PayPeriodStart", "PAY_PERIOD_START"},
														{"GrossToDate", "GROSS_TO_DATE"},
														{"PaymentsToDate", "PAYMENTS_TO_DATE"},
														{"DisabilityFlag", "DISABILITY_FLAG"},
														{"DailyAmount", "DAILY_AMOUNT"},
														{"FreezeBatchFlag", "FREEZE_BATCH_FLAG"},
														{"DailySuppAmount", "DAILY_SUPP_AMOUNT"},
                                                        {"PaymentIntervalDays", "PAYMENT_INTERVAL_DAYS"},
                                                        {"AdjustPrintDatesFlag", "ADJUST_PRINT_DATE"}};//Neha 12/08/2011 R8 Changes



		public int AutoBatchId{get{return GetFieldInt("AUTO_BATCH_ID");}set{SetField("AUTO_BATCH_ID",value);}}
		public int TotalPayments{get{return GetFieldInt("TOTAL_PAYMENTS");}set{SetField("TOTAL_PAYMENTS",value);}}
		public int CurrentPayment{get{return GetFieldInt("CURRENT_PAYMENT");}set{SetField("CURRENT_PAYMENT",value);}}
		[ExtendedTypeAttribute(RMExtType.Code,"PAYMENT_INTERVAL")]
		public int PaymentInterval{get{return GetFieldInt("PAYMENT_INTERVAL");}set{SetField("PAYMENT_INTERVAL",value);}}
		public string StartDate{get{return GetFieldString("START_DATE");}set{SetField("START_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string EndDate{get{return GetFieldString("END_DATE");}set{SetField("END_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string BenStartDate{get{return GetFieldString("BEN_START_DATE");}set{SetField("BEN_START_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string BenEndDate{get{return GetFieldString("BEN_END_DATE");}set{SetField("BEN_END_DATE",Riskmaster.Common.Conversion.GetDate(value));}}
		public string PayPeriodStart{get{return GetFieldString("PAY_PERIOD_START");}set{SetField("PAY_PERIOD_START",value);}}
		public double GrossToDate{get{return GetFieldDouble("GROSS_TO_DATE");}set{SetField("GROSS_TO_DATE",value);}}
		public int PaymentsToDate{get{return GetFieldInt("PAYMENTS_TO_DATE");}set{SetField("PAYMENTS_TO_DATE",value);}}
		public bool DisabilityFlag{get{return GetFieldBool("DISABILITY_FLAG");}set{SetField("DISABILITY_FLAG",value);}}
		public double DailyAmount{get{return GetFieldDouble("DAILY_AMOUNT");}set{SetField("DAILY_AMOUNT",value);}}
		public bool FreezeBatchFlag{get{return GetFieldBool("FREEZE_BATCH_FLAG");}set{SetField("FREEZE_BATCH_FLAG",value);}}
		public double DailySuppAmount{get{return GetFieldDouble("DAILY_SUPP_AMOUNT");}set{SetField("DAILY_SUPP_AMOUNT",value);}}
        public bool AdjustPrintDatesFlag { get { return GetFieldBool("ADJUST_PRINT_DATE"); } set { SetField("ADJUST_PRINT_DATE", value); } }
        //Add by kuladeep for Day interval for auto checks mits:19360 Start
        public int PaymentIntervalDays { get { return GetFieldInt("PAYMENT_INTERVAL_DAYS"); } set { SetField("PAYMENT_INTERVAL_DAYS", value); } }
        //Add by kuladeep for Day interval for auto checks mits:19360 End

		#endregion
		
		#region Child Implementation
		private string[,] sChildren = { {"FundsAutoList","FundsAutoList"}};

		//Handle adding our key to any additions to the record collection properties.
		internal override void OnChildItemAdded(string childName, IDataModel itemValue)
		{
			switch(childName)
			{
				case "FundsAutoList" :
					(itemValue as FundsAuto).AutoBatchId= this.AutoBatchId;
					break;
			}
			base.OnChildItemAdded (childName, itemValue);
		}					
	
		//Handle child saves.  Set the current key into the children since this could be a new record.
		override internal void OnChildPostSave(string childName, IDataModel childValue, bool isNew)
		{
			switch(childName)
			{
				case "FundsAutoList":
					// Aditya - 7-Nov-2005 - Ensure AutoBatchId is set for all FundsAuto objects each time
					//if(isNew)
					foreach(FundsAuto item in (childValue as FundsAutoList))
						item.AutoBatchId= this.AutoBatchId;
					// Aditya - 7-Nov-2005 - The following Save() call is made through the base class method.
					//(childValue as IPersistence).Save();
					break;
			}
			base.OnChildPostSave (childName, childValue, isNew);
		}		

		//Child Property Accessors
		public FundsAutoList FundsAutoList
		{
			get
			{
				FundsAutoList objList = base.m_Children["FundsAutoList"] as FundsAutoList;
				if (!base.IsNew && (objList as IDataModel).IsStale)
					objList.SQLFilter = this.KeyFieldName+ "=" + this.KeyFieldValue;
				return objList;
			}
		}
		#endregion

		internal FundsAutoBatch(bool isLocked,Context context):base(isLocked, context)
		{
			this.Initialize();
		}
		new private void Initialize()
		{

			// base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

			this.m_sTableName = "FUNDS_AUTO_BATCH";
			this.m_sKeyField = "AUTO_BATCH_ID";
			this.m_sFilterClause = "";

			//Add all obect Fields into the Field Collection from our Field List.
			base.InitFields(sFields);
			//Add all object Children into the Children collection from our "sChildren" list.
			base.InitChildren(sChildren);
			this.m_sParentClassName = "";
			base.Initialize();  //Moved after most init logic so that scripting can be called successfully.

		}
	}
}