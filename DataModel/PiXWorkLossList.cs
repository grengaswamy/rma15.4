
using System;
namespace Riskmaster.DataModel
{
	/// <summary>
	/// Summary description forPiXWorkLossList.
	/// </summary>
	public class PiXWorkLossList : DataCollection
	{
		internal PiXWorkLossList(bool isLocked,Context context):base(isLocked, context)
		{
			(this as IDataModel).Initialize(isLocked, context);
			this.SQLKeyColumn =	"PI_WL_ROW_ID";
			this.SQLFromTable =	"PI_X_WORK_LOSS";
			//			this.SQLFilter =				"???" //Must be set by parent at runtime for current record.
			this.TypeName = "PiXWorkLoss";
		}
		public new PiXWorkLoss this[int keyValue]{get{return base[keyValue] as PiXWorkLoss;}}
		public new PiXWorkLoss AddNew(){return base.AddNew() as PiXWorkLoss;}
		public  PiXWorkLoss Add(PiXWorkLoss obj){return base.Add(obj) as PiXWorkLoss;}
		public new PiXWorkLoss Add(int keyValue){return base.Add(keyValue) as PiXWorkLoss;}
		
		//TODO Remove after debugging this could be a security hole.
		public override string ToString()
		{
			string s = "";
			foreach(DataObject obj in this)
				s += obj.Dump();
			return s;
		}
	}
}