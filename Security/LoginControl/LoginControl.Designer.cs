﻿namespace Riskmaster.Security
{
    partial class LoginControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.cmdLoadDSNs = new System.Windows.Forms.Button();
            this.errValidation = new System.Windows.Forms.ErrorProvider(this.components);
            this.cmbDSNs = new System.Windows.Forms.ComboBox();
            this.cmdLogin = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.errValidation)).BeginInit();
            this.SuspendLayout();
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(23, 68);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(63, 13);
            this.lblUserName.TabIndex = 0;
            this.lblUserName.Text = "User Name:";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(23, 103);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(56, 13);
            this.lblPassword.TabIndex = 1;
            this.lblPassword.Text = "Password:";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(105, 61);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(232, 20);
            this.txtUserName.TabIndex = 4;
            this.txtUserName.Validated += new System.EventHandler(this.txtUserName_Validated);
            this.txtUserName.Validating += new System.ComponentModel.CancelEventHandler(this.txtUserName_Validating);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(105, 96);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(232, 20);
            this.txtPassword.TabIndex = 5;
            this.txtPassword.Validated += new System.EventHandler(this.txtPassword_Validated);
            this.txtPassword.Validating += new System.ComponentModel.CancelEventHandler(this.txtPassword_Validating);
            // 
            // cmdLoadDSNs
            // 
            this.cmdLoadDSNs.Location = new System.Drawing.Point(121, 179);
            this.cmdLoadDSNs.Name = "cmdLoadDSNs";
            this.cmdLoadDSNs.Size = new System.Drawing.Size(75, 23);
            this.cmdLoadDSNs.TabIndex = 6;
            this.cmdLoadDSNs.Text = "Load DSNs";
            this.cmdLoadDSNs.UseVisualStyleBackColor = true;
            this.cmdLoadDSNs.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // errValidation
            // 
            this.errValidation.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errValidation.ContainerControl = this;
            // 
            // cmbDSNs
            // 
            this.cmbDSNs.FormattingEnabled = true;
            this.cmbDSNs.Location = new System.Drawing.Point(105, 134);
            this.cmbDSNs.Name = "cmbDSNs";
            this.cmbDSNs.Size = new System.Drawing.Size(232, 21);
            this.cmbDSNs.TabIndex = 7;
            // 
            // cmdLogin
            // 
            this.cmdLogin.Location = new System.Drawing.Point(202, 179);
            this.cmdLogin.Name = "cmdLogin";
            this.cmdLogin.Size = new System.Drawing.Size(75, 23);
            this.cmdLogin.TabIndex = 8;
            this.cmdLogin.Text = "Login";
            this.cmdLogin.UseVisualStyleBackColor = true;
            this.cmdLogin.Click += new System.EventHandler(this.cmdLogin_Click);
            // 
            // LoginControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cmdLogin);
            this.Controls.Add(this.cmbDSNs);
            this.Controls.Add(this.cmdLoadDSNs);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblUserName);
            this.Name = "LoginControl";
            this.Size = new System.Drawing.Size(358, 220);
            ((System.ComponentModel.ISupportInitialize)(this.errValidation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button cmdLoadDSNs;
        private System.Windows.Forms.ErrorProvider errValidation;
        private System.Windows.Forms.ComboBox cmbDSNs;
        private System.Windows.Forms.Button cmdLogin;
    }
}
