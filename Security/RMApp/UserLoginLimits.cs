using System;
using Riskmaster.Db;
using Riskmaster.Common;
using System.Data;
using System.Collections;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;

namespace Riskmaster.Security.RMApp
{

	/// UserLoginLimits contains populated "write once" configuration information about
	/// the business logic limitations to be imposed on a specific UserLogin.
	/// Also contains limited "lookup" features for ease of use.
	/// </summary>
	[Serializable]
	public class UserLoginLimits
	{
        private int m_iClientId = 0;

		#region Limit Data Structures
		[Serializable]
		class PrintCheckLimit
		{
			public readonly int LOB;
			public readonly double MaxAmount;
			public PrintCheckLimit(int LOB, double maxAmount){this.LOB = LOB;this.MaxAmount = maxAmount;}
		}
		private Hashtable m_colPrintLimits = new Hashtable(); 
		[Serializable]
		class ReserveLimit
		{
			public readonly int LOB;
			public readonly int ReserveType;
			public readonly double MaxAmount;
			public ReserveLimit(int LOB, int reserveType, double maxAmount){this.LOB = LOB;this.ReserveType = reserveType;this.MaxAmount = maxAmount;}

		}
		private Hashtable m_colReserveLimits = new Hashtable();

		[Serializable]
		class LOBPerClaimReserveLimit
		{
			public readonly int LOB;
			public readonly int ClaimType;
			public readonly int ReserveType;
			public readonly double MaxAmount;
			public LOBPerClaimReserveLimit(int LOB, int claimType, int reserveType, double maxAmount){this.LOB = LOB;this.ClaimType = claimType;this.ReserveType = reserveType;this.MaxAmount = maxAmount;}
		}
		private Hashtable m_colLOBPerClaimReserveLimits = new Hashtable();

		[Serializable]
		class FundsLimit
		{
			public readonly int LOB;
			public readonly double MaxAmount;
			public FundsLimit(int LOB, double maxAmount){this.LOB = LOB;this.MaxAmount = maxAmount;}

		}
		private Hashtable m_colFundsLimits = new Hashtable();

		[Serializable]
		class FundsDetailLimit
		{
			public readonly int LOB;
			public readonly int ReserveType;
			public readonly double MaxAmount;
			public FundsDetailLimit(int LOB, int reserveType, double maxAmount){this.LOB = LOB;this.ReserveType = reserveType;this.MaxAmount = maxAmount;}

		}
		private Hashtable m_colFundsDetailLimits = new Hashtable();

		[Serializable]
		class ClaimLimit
		{
			public readonly int LOB;
			public readonly int ClaimType;
			public readonly int ClaimStatus;
			public readonly bool CreateAllowed;
			public readonly bool AccessAllowed;
			public ClaimLimit(int LOB, int claimType, int claimStatus, bool createAllowed, bool accessAllowed){this.LOB = LOB;this.ClaimType = claimType;this.ClaimStatus = claimStatus;this.CreateAllowed = createAllowed;this.AccessAllowed= accessAllowed;}
			//internal ClaimLimit(){;}

		}
		private Hashtable m_colClaimLimits = new Hashtable();
		
		[Serializable]
		class EventTypeLimit
		{
			public readonly int EventType;
			public readonly bool CreateAllowed;
			public readonly bool AccessAllowed;
			public EventTypeLimit(int eventType, bool createAllowed, bool accessAllowed){this.EventType = eventType;this.CreateAllowed = createAllowed;this.AccessAllowed= accessAllowed;}
			//internal EventTypeLimit(){;}

		}
		private Hashtable m_colEventLimits = new Hashtable();
		#endregion
		#region Limit Loading Logic
		public UserLoginLimits(UserLogin objUserLogin, int p_iClientId)
		{
			DbConnection dbConn=null;
			try
			{
                m_iClientId = p_iClientId;
				dbConn = DbFactory.GetDbConnection(objUserLogin.objRiskmasterDatabase.ConnectionString);
				dbConn.Open();
				LoadLimits(objUserLogin.UserId, objUserLogin.GroupId, dbConn,p_iClientId);
			}
			catch(Exception e)
			{
                throw new RMAppException(String.Format(Globalization.GetString("UserLoginLimits.Consructor.LoadFailedException", m_iClientId), objUserLogin.UserId), e);
			}
			finally
			{
				if(dbConn!=null)
					dbConn.Close();
				dbConn = null;
			}
		}
		public UserLoginLimits(int userId, int groupId, DbConnection dbConn, int p_iClientId)
		{
			LoadLimits(userId,groupId,dbConn,p_iClientId);
		}
		
		
		//Expected to be run ONLY once - during a Ctor call.
		//All data are Read-Only from this point.
		private void LoadLimits(int userId, int groupId, DbConnection dbConn, int p_iClientId)
		{
			DbReader rdr;
			string sKey="";
			string sNewKey ="";
			string sFilterSQL = " USER_ID=" + userId; 
			if(groupId >0)
				sFilterSQL += " OR GROUP_ID=" + groupId; 

			//CHECK PRINTING LIMITS
			PrintCheckLimit xPrintCheckLimit;
			rdr = dbConn.ExecuteReader(
				"SELECT LINE_OF_BUS_CODE, MAX(MAX_AMOUNT) FROM PRT_CHECK_LIMITS WHERE " + sFilterSQL + " GROUP BY LINE_OF_BUS_CODE");
			while(rdr.Read())
			{
				xPrintCheckLimit = new PrintCheckLimit(
					rdr.GetInt(0),
					rdr.GetDouble(1));
				
				//Note: Hashtable is keyed on LOB for easy retrieval.
				sKey = xPrintCheckLimit.LOB.ToString();
				this.m_colPrintLimits.Add(sKey,xPrintCheckLimit);
			}
			rdr.Close();

			//RESERVE LIMITS
			ReserveLimit xReserveLimit;
			rdr = dbConn.ExecuteReader(
				"SELECT LINE_OF_BUS_CODE, RESERVE_TYPE_CODE, MAX(MAX_AMOUNT) FROM RESERVE_LIMITS WHERE " + sFilterSQL + " GROUP BY LINE_OF_BUS_CODE,RESERVE_TYPE_CODE");
			while(rdr.Read())
			{
				xReserveLimit = new ReserveLimit(
					rdr.GetInt(0),
					rdr.GetInt(1),
					rdr.GetDouble(2));
				
				//Note: Hashtable is keyed on LOB & ReserveType for quick retrieval.
				sKey = String.Format("{0}-{1}", xReserveLimit.LOB, xReserveLimit.ReserveType);
				this.m_colReserveLimits.Add(sKey,xReserveLimit);
			}
			rdr.Close();

			//LOB & CLAIM SPECIFIC RESERVE LIMITS
			LOBPerClaimReserveLimit xLOBPerClaimReserveLimit;
			rdr = dbConn.ExecuteReader(
				"SELECT LINE_OF_BUS_CODE, CLAIM_TYPE_CODE, RES_TYPE_CODE, MAX(MAX_AMOUNT) FROM CL_RES_PAY_LMTS WHERE " + sFilterSQL + " GROUP BY LINE_OF_BUS_CODE,RES_TYPE_CODE,CLAIM_TYPE_CODE");
			while(rdr.Read())
			{
				xLOBPerClaimReserveLimit = new LOBPerClaimReserveLimit(
					rdr.GetInt(0),
					rdr.GetInt(1),
					rdr.GetInt(2),
					rdr.GetDouble(3));
				
				//Note: Hashtable is keyed on LOB-ClaimType-ReserveType for quick retrieval.
				sKey = String.Format("{0}-{1}-{2}", xLOBPerClaimReserveLimit.LOB, xLOBPerClaimReserveLimit.ClaimType,xLOBPerClaimReserveLimit.ReserveType);
				this.m_colLOBPerClaimReserveLimits.Add(sKey,xLOBPerClaimReserveLimit);
			}
			rdr.Close();

			//FUNDS PAYMENT LIMITS
			FundsLimit xFundsLimit;
			rdr = dbConn.ExecuteReader(
				"SELECT LINE_OF_BUS_CODE,MAX(MAX_AMOUNT) FROM FUNDS_LIMITS WHERE " + sFilterSQL + " GROUP BY LINE_OF_BUS_CODE");
			while(rdr.Read())
			{
				xFundsLimit = new FundsLimit(
					rdr.GetInt(0),
					rdr.GetDouble(1));
				
				//Note: Hashtable is keyed on LOB for quick retrieval.
				sKey = String.Format("{0}", xFundsLimit.LOB);
				this.m_colFundsLimits.Add(sKey,xFundsLimit);
			}
			rdr.Close();

			//FUNDS DETAIL PAYMENT LIMITS
			FundsDetailLimit xFundsDetailLimit;
			rdr = dbConn.ExecuteReader(
				"SELECT LINE_OF_BUS_CODE, RESERVE_TYPE_CODE, MAX(MAX_AMOUNT) FROM FUNDS_DET_LIMITS WHERE " + sFilterSQL + " GROUP BY LINE_OF_BUS_CODE,RESERVE_TYPE_CODE");
			while(rdr.Read())
			{
				xFundsDetailLimit = new FundsDetailLimit(
					rdr.GetInt(0),
					rdr.GetInt(1),
					rdr.GetDouble(2));
				
				//Note: Hashtable is keyed on LOB & ReserveType for quick retrieval.
				sKey = String.Format("{0}-{1}", xFundsDetailLimit.LOB, xFundsDetailLimit.ReserveType);
				this.m_colFundsDetailLimits.Add(sKey,xFundsDetailLimit);
			}
			rdr.Close();

			//EVENT ACCESS LIMITS
			EventTypeLimit xEventTypeLimit = new EventTypeLimit(0,false,false);
			EventTypeLimit xPrevLimit = new EventTypeLimit(0,false,false);
			rdr = dbConn.ExecuteReader(
				"SELECT EVENT_TYPE_CODE, CREATE_FLAG, ACCESS_FLAG FROM EVENT_ACC_LIMITS WHERE " + sFilterSQL + " ORDER BY EVENT_TYPE_CODE");
			while(rdr.Read())
			{
				xPrevLimit = xEventTypeLimit;
				if(xEventTypeLimit.EventType != rdr.GetInt(0)) //New Event Type
					xEventTypeLimit = new EventTypeLimit(
						rdr.GetInt(0),
						rdr.GetBoolean(1),
						rdr.GetBoolean(2));
				else	//always allow the most permissive combination
				{		// of permissions available.
					xEventTypeLimit = new EventTypeLimit(
						rdr.GetInt(0),
						xPrevLimit.CreateAllowed | rdr.GetBoolean(1),
						xPrevLimit.AccessAllowed | rdr.GetBoolean(2));
					this.m_colEventLimits.Remove(xEventTypeLimit.EventType.ToString());
				}

				//Note: Hashtable is keyed on EventType for quick retrieval.
				sKey = String.Format("{0}", xEventTypeLimit.EventType);
				this.m_colEventLimits.Add(sKey,xEventTypeLimit);
			}
			rdr.Close();

			//CLAIM ACCESS LIMITS
			ClaimLimit xClaimLimit = new ClaimLimit(0,0,0,false,false);
			ClaimLimit xPrevClaimLimit = new ClaimLimit(0,0,0,false,false);
            //pmittal5 - Added LINE_OF_BUS_CODE in OrderBy clause
			rdr = dbConn.ExecuteReader(
                "SELECT LINE_OF_BUS_CODE, CLAIM_TYPE_CODE, CLAIM_STATUS_CODE,CREATE_FLAG, ACCESS_FLAG FROM CLAIM_ACC_LIMITS WHERE " + sFilterSQL + " ORDER BY LINE_OF_BUS_CODE,CLAIM_TYPE_CODE,CLAIM_STATUS_CODE");
			while(rdr.Read())
			{
				xPrevClaimLimit = xClaimLimit;
				sNewKey = String.Format("{0}-{1}-{2}", rdr.GetInt(0),rdr.GetInt(1),rdr.GetInt(2));
				if(sNewKey != sKey) //New Permission Set
					xClaimLimit = new ClaimLimit(
						rdr.GetInt(0),
						rdr.GetInt(1),
						rdr.GetInt(2),
						rdr.GetBoolean(3),
						rdr.GetBoolean(4));
				else	//always allow the most permissive combination
				{		// of the (multiple settings) available for these rights.
					xClaimLimit = new ClaimLimit(
						rdr.GetInt(0),
						rdr.GetInt(1),
						rdr.GetInt(2),
						xPrevClaimLimit.CreateAllowed | rdr.GetBoolean(3),
						xPrevClaimLimit.AccessAllowed | rdr.GetBoolean(4));
					this.m_colClaimLimits.Remove(sNewKey);
				}

				//Note: Hashtable is keyed on ClaimType-ClaimStatus for quick retrieval.
				sKey = String.Format("{0}-{1}-{2}", xClaimLimit.LOB,xClaimLimit.ClaimType,xClaimLimit.ClaimStatus);
				this.m_colClaimLimits.Add(sKey,xClaimLimit);
			}
			rdr.Close();
            //asharma326 Events MITS 30874
            rdr = dbConn.ExecuteReader(
                "SELECT EV_TYPE_ACC_USR_FLAG FROM SYS_PARMS");
            while (rdr.Read())
            {
                isUserRestrictedEvent = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(rdr.GetValue("EV_TYPE_ACC_USR_FLAG").ToString()),p_iClientId);
            }
            rdr.Close();
            //asharma326 MITS 30874 Ends
		}
		#endregion
        #region "Asharma326 Properties for MITS 30874 Claim And Events restriction"
        //private bool isUserRestrictedClaim = false;
        private bool isUserRestrictedEvent = false;

        #endregion
		#region Limit Usage Functions

		//Split vb GetMaxXXX functions into two chunks for clarity.
		//One asks if Maximum Amount is Set, other one gets the set amount if any.
		public double MaxReserveAmount(int LOB, int ReserveTypeCode)
		{
			string sKey = String.Format("{0}-{1}",LOB,ReserveTypeCode);
			try{return ((ReserveLimit)this.m_colReserveLimits[sKey]).MaxAmount;}
			catch{return 0;}
		}
		public bool MaxReserveAmountIsLimited(int LOB, int ReserveTypeCode)
		{
			string sKey = String.Format("{0}-{1}",LOB,ReserveTypeCode);
			return this.m_colReserveLimits.ContainsKey(sKey);
		}

		public double MaxPrintAmount(int LOB)
		{
			string sKey = LOB.ToString();
			try{return ((PrintCheckLimit)this.m_colPrintLimits[sKey]).MaxAmount;}
			catch{return 0;}
		}
		public bool MaxPrintAmountIsLimited(int LOB)
		{
			string sKey = LOB.ToString();
			return this.m_colPrintLimits.ContainsKey(sKey);
		}
		public bool MaxPerClaimReserveAmountIsLimited(int LOB, int reserveType, int claimType, out int limitedByReserveType)
		{
			string sKey = String.Format("{0}-{1}-{2}", LOB, claimType,reserveType);

			//First Try finding the Most Specific (Full) Level Setting
			limitedByReserveType=reserveType;
			if(this.m_colLOBPerClaimReserveLimits.ContainsKey(sKey))
				return true;

			//Back off to LOB & ReserveType Level Setting
			sKey = String.Format("{0}-{1}-{2}", LOB, "0",reserveType);
			if (this.m_colLOBPerClaimReserveLimits.ContainsKey(sKey))
				return true;


			//Back off to LOB & ClaimType Level Setting
			limitedByReserveType=0;
			sKey = String.Format("{0}-{1}-{2}", LOB, claimType,"0");
			if(this.m_colLOBPerClaimReserveLimits.ContainsKey(sKey))
				return true;

			//Back off to plain LOB Level Setting
			sKey = String.Format("{0}-{1}-{2}", LOB, "0","0");
			if(this.m_colLOBPerClaimReserveLimits.ContainsKey(sKey))
				return true;
		return false;
		}

		public double MaxPerClaimReserveAmount(int LOB, int reserveType, int claimType)
		{
			string sKey = String.Format("{0}-{1}-{2}", LOB, claimType,reserveType);

			//First Try finding the Most Specific (Full) Level Setting
			if(this.m_colLOBPerClaimReserveLimits.ContainsKey(sKey))
				return ((LOBPerClaimReserveLimit)this.m_colLOBPerClaimReserveLimits[sKey]).MaxAmount;

			//Back off to LOB & ReserveType Level Setting
			sKey = String.Format("{0}-{1}-{2}", LOB, "0",reserveType);
			if (this.m_colLOBPerClaimReserveLimits.ContainsKey(sKey))
				return ((LOBPerClaimReserveLimit)this.m_colLOBPerClaimReserveLimits[sKey]).MaxAmount;

			//Back off to LOB & ClaimType Level Setting
			sKey = String.Format("{0}-{1}-{2}", LOB, claimType,"0");
			if(this.m_colLOBPerClaimReserveLimits.ContainsKey(sKey))
				return ((LOBPerClaimReserveLimit)this.m_colLOBPerClaimReserveLimits[sKey]).MaxAmount;

			//Back off to plain LOB Level Setting
			sKey = String.Format("{0}-{1}-{2}", LOB, "0","0");
			if(this.m_colLOBPerClaimReserveLimits.ContainsKey(sKey))
				return ((LOBPerClaimReserveLimit)this.m_colLOBPerClaimReserveLimits[sKey]).MaxAmount;
		
			return 0;
		}

		public double MaxPayDetailAmount(int LOB)
		{
			string sKey = LOB.ToString();
			try{return ((FundsDetailLimit)this.m_colFundsDetailLimits[sKey]).MaxAmount;}
			catch{return 0;}
		}
		public bool MaxPayDetailAmountIsLimited(int LOB)
		{
			string sKey = LOB.ToString();
			return this.m_colFundsDetailLimits.ContainsKey(sKey);
		}

		public double MaxPayAmount(int LOB)
		{
			string sKey = LOB.ToString();
			try{return ((FundsLimit)this.m_colFundsLimits[sKey]).MaxAmount;}
			catch{return 0;}
		}
		public bool MaxPayAmountIsLimited(int LOB)
		{
			string sKey = LOB.ToString();
			return this.m_colFundsLimits.ContainsKey(sKey);
		}

        public bool ClaimAccessAllowed(int iLOB, int claimType, int claimStatus, bool userFlag)
		{
            //asharma326 MITS 30874 Starts 
            if ((this.m_colClaimLimits.Count == 0) && (userFlag))
                return false;
            //asharma326 MITS 30874 Ends
			foreach(ClaimLimit limit in this.m_colClaimLimits.Values)
			{
				if(limit.LOB == iLOB)
				{
					if(((limit.ClaimType == claimType && limit.ClaimStatus==0)
						| ((limit.ClaimStatus == claimStatus) && limit.ClaimType==0)
						| ((limit.ClaimStatus == claimStatus) && limit.ClaimType == claimType))
						&& !limit.AccessAllowed)
						return false;
				}
			}
			return true;
		}

//		public string[] ClaimAccessTypeList(int iLOB)
//		{
//			
//				string[] ret;
//				int idx = 0;
//			try
//			{	foreach(ClaimLimit limit in this.m_colClaimLimits.Values)
//					if(limit.LOB == iLOB && limit.ClaimType !=0 && !limit.AccessAllowed)
//						idx++;
//			}catch(Exception e)
//			{
//				System.Diagnostics.Trace.WriteLine(e.Message);
//			}
//			
//			ret = new string[idx];
//			idx =0;
//			foreach(ClaimLimit limit in this.m_colClaimLimits.Values)
//				if(limit.LOB == iLOB && limit.ClaimType !=0 && !limit.AccessAllowed)
//				{
//					ret[idx]=limit.ClaimType.ToString();
//					idx++;
//				}
//			return ret;
//		}
//		public string[] ClaimStatusAccessList(int iLOB)
//		{
//			
//			string[] ret;
//			int idx = 0;
//			try
//			{
//					foreach(ClaimLimit limit in this.m_colClaimLimits.Values)
//					if(limit.LOB == iLOB && limit.ClaimStatus !=0 && !limit.AccessAllowed)
//						idx++;
//			}
//			catch(Exception e)
//			{
//				System.Diagnostics.Trace.WriteLine(e.Message);
//			}
//			
//			ret = new string[idx];
//			idx =0;
//			foreach(ClaimLimit limit in this.m_colClaimLimits.Values)
//				if(limit.LOB == iLOB && limit.ClaimStatus !=0 && !limit.AccessAllowed)
//				{
//					ret[idx]=limit.ClaimStatus.ToString();
//					idx++;
//				}
//			return ret;
//		}

		public string[,] ClaimStatusTypeAccessList(int iLOB)
		{
			
			string[,] ret;
			int idx = 0;
			try
			{
					foreach(ClaimLimit limit in this.m_colClaimLimits.Values)
					if(limit.LOB == iLOB && !limit.AccessAllowed && (limit.ClaimType !=0 || limit.ClaimStatus !=0))
						idx++;
			}
			catch(Exception e)
			{
				System.Diagnostics.Trace.WriteLine(e.Message);
			}
			
			ret = new string[idx,2];
			idx =0;
			foreach(ClaimLimit limit in this.m_colClaimLimits.Values)
				if(limit.LOB == iLOB && !limit.AccessAllowed && (limit.ClaimType !=0 || limit.ClaimStatus !=0))
				{
					ret[idx, 0]=limit.ClaimType.ToString();
					ret[idx, 1]=limit.ClaimStatus.ToString();
					idx++;
				}
			return ret;
		}
        public bool ClaimCreateAllowed(int iLOB, int claimType, int claimStatus, bool userFlag)
		{
            //asharma326 MITS 30874 Starts 
            if ((this.m_colClaimLimits.Count == 0) && (userFlag))
                return false;
            //asharma326 MITS 30874 Ends
			foreach(ClaimLimit limit in this.m_colClaimLimits.Values)
			{
				if(limit.LOB == iLOB)
				{
					if(((limit.ClaimType == claimType && limit.ClaimStatus==0)
						| ((limit.ClaimStatus == claimStatus) && limit.ClaimType==0)
						| ((limit.ClaimStatus == claimStatus) && limit.ClaimType == claimType))
						&& !limit.CreateAllowed)
						return false;
				}

			}
			return true;
		}
		public string[] EventAccessTypeList()
		{
			string[] ret;
			int idx = 0;
			
			foreach(EventTypeLimit limit in this.m_colEventLimits.Values)
			{
				if((!limit.AccessAllowed)  && (limit.EventType!=0))
				{
					idx++;
				}
			}

			ret = new string[idx];
			idx =0;
			foreach(EventTypeLimit limit in this.m_colEventLimits.Values)
			{
				if((!limit.AccessAllowed)  && (limit.EventType!=0))
				{
					ret[idx]=limit.EventType.ToString();
					idx++;
				}
			}
			
			return ret;
		}
		public bool EventTypeCreateAllowed(int eventType)
		{
			try
			{
				EventTypeLimit limit = (EventTypeLimit)this.m_colEventLimits[eventType.ToString()];
                //asharma326 MITS 30874 Starts 
                if ((this.m_colEventLimits.Count == 0) && (this.isUserRestrictedEvent))
                    return false;
                //asharma326 MITS 30874 Ends
				if(limit.EventType!=0)
					return limit.CreateAllowed;
				return true;
			}
			catch{return true;}
		}
		public bool EventTypeAccessAllowed(int eventType)
		{
			try
			{
				EventTypeLimit limit = (EventTypeLimit)this.m_colEventLimits[eventType.ToString()];
                //asharma326 MITS 30874 Starts 
                if ((this.m_colEventLimits.Count == 0) && (this.isUserRestrictedEvent))
                    return false;
                //asharma326 MITS 30874 Ends
                //srajindersin MITS 32606 dt:05/14/2013
				if(limit != null && limit.EventType!=0)
					return limit.AccessAllowed;
				return true;
			}
			catch{return true;}
		}
		#endregion
	}
}
