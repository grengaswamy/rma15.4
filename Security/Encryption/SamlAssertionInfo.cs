﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using ComponentSoft.Saml2;
using System.Security.Cryptography.X509Certificates;



namespace Riskmaster.Security.Authentication
{
    public class SamlAssertionInfo
    {

        public string AudienceUri
        {
            get;
            set;
        }

        public Issuer SamlIssuer
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string SubjectRecipient
        {
            get;
            set;
        }

        public Dictionary<string, string> ClaimSetInfo
        {
            get;
            set;
        }//property: ClaimSetInfo

        public X509Certificate2 SigningCertificate
        {
            get;
            set;
        }//property: SigningCertificate


        /// <summary>
        /// Sets the AudienceUri property
        /// </summary>
        /// <param name="strSubjectRecipient">string containing the subject recipient of the Saml Token</param>
        public void SetAudienceUri(string strSubjectRecipient)
        {
            UriBuilder destinationUri = new UriBuilder(strSubjectRecipient);
            string[] paths = destinationUri.Path.Split('/');
            UriBuilder audienceUriBldr = new UriBuilder(destinationUri.Scheme, destinationUri.Host) { Path = string.Format("/{0}", paths[1]) };

            //Set the property for the AudienceUri
            AudienceUri = audienceUriBldr.ToString();
            destinationUri = null;
            audienceUriBldr = null;
        }//method: SetAudienceUri();

    }
}
