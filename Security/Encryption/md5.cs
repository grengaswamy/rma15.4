using System;

namespace Riskmaster.Security.Encryption
{
	/// <summary>
	///  Implementation of the RSA Data Security, Inc.
	///  MD5 Message-Digest Algorithm
	///
	///  Author: Ray Comas@DTG
	///

	///  Copyright (C) 1994, DORN Technology Group, Inc.
	///  All Rights Reserved
	///

	///  Algorithm developed by Ron Rivest, RSA Data Security, Inc.,
	///  and placed in the public domain.
	///
	///  Summary:
	///      MD5 processes message blocks 512 bits (64 bytes) at a time,
	///      computing a 128-bit (16 byte) hash value for each block. The
	///      hash value for the entire message is the sum of the hash values
	///      for the individual 512-bit blocks.
	///
	///      MD5 is secure in that it is not feasible to find two messages
	///      that generate the same hash, or to find a message that generates
	///      a specified hash.
	///
	///      MD5 is resistant to attack in that a 1-bit change in the input
	///      will, on average, change half of the output bits.
	/// </summary>
	/// 
	public class CSCMD5
	{
		const uint InitA = 0x01234567;
		const uint InitB = 0x89ABCDEF;
		const uint InitC = 0xFEDCBA98;
		const uint InitD = 0x76543210;

		const int MaxLastBlock = 56;

		public CSCMD5()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		//////////////////////////////////////////////////////////////////////////
		//
		//  Non-linear functions, one for each of four rounds
		//

		//  Function for round 1
		uint F (uint X, uint Y, uint Z)
		{  return (X & Y) | (~X & Z);  }
		//------------------------------------------------------------------------
		  
		//  Function for round 2
		uint G (uint X, uint Y, uint Z)
		{  return  (X & Z) | (Y & ~Z);  }
		//------------------------------------------------------------------------  

		//  Function for round 3
		uint H (uint X, uint Y, uint Z)
		{  return  X ^ Y ^ Z;  }
		//------------------------------------------------------------------------
		  
		//  Function for round 4
		uint I (uint X, uint Y, uint Z)
		{  return Y ^ (X | ~Z);  }
		//------------------------------------------------------------------------


		//////////////////////////////////////////////////////////////////////////
		//
		//  Compression function:   a = b + ((f(b,c,d) + M[j] + t[j]) <<< s)
		//

		//  RotateLeft
		//      Rotate-left 32-bit number by given bits
		//      (IN) DWORD n    The number to rotate
		//      (IN) int s      Rotation length
		//      RETURNS:    number n rotated left by s bits
		uint RotateLeft (uint n, int s)
		{  return (uint)((n << s) | (n >> (32 - s)));  }
		//------------------------------------------------------------------------

		//  Round 1 Transformation
		void FF (out uint a, uint b, uint c, uint d,
							uint M, int s, uint t)
		{  a = b + RotateLeft(F(b,c,d) + M + t, s);  }
		//------------------------------------------------------------------------  

		//  Round 2 Transformation
		void GG (out uint a, uint b, uint c, uint d,
							uint M, int s, uint t)
		{  a = b + RotateLeft(G(b,c,d) + M + t, s);  }
		//------------------------------------------------------------------------  

		//  Round 3 Transformation
		void HH (out uint a, uint b, uint c, uint d,
							uint M, int s, uint t)
		{  a = b + RotateLeft(H(b,c,d) + M + t, s);  }
		//------------------------------------------------------------------------
		  
		//  Round 4 Transformation
		void II (out uint a, uint b, uint c, uint d,
							uint M, int s, uint t)
		{  a = b + RotateLeft(I(b,c,d) + M + t, s);  }
		//------------------------------------------------------------------------  

		//////////////////////////////////////////////////////////////////////////
		//
		//  MD5 Algorithm
		//

		//  ProcessBlock
		//      Process one block of data, computing intermediate hash A,B,C,D
		//      (IN/OUT) DWORD &H1   Hash, part 1
		//      (IN/OUT) DWORD &H2   Hash, part 2
		//      (IN/OUT) DWORD &H3   Hash, part 3
		//      (IN/OUT) DWORD &H4   Hash, part 4
		//      (IN) DWORD * M       512-bit block (pre-conditioned)
		void ProcessBlock (ref uint H1, ref uint H2, ref uint H3, ref uint H4, uint[] M)
		{

			uint a = H1, b = H2, c = H3, d = H4;

			// Hex constants below were chosen so that for step N,
			//      constant = integral part of 2**32 * |sin(N)|, n in radians
			//

			//  Round 1
			FF(out a,b,c,d, M[0],   7, 0xd76aa478);    // Step 1
			FF(out d,a,b,c, M[1],  12, 0xe8c7b756);    // Step 2
			FF(out c,d,a,b, M[2],  17, 0x242070db);    // Step 3
			FF(out b,c,d,a, M[3],  22, 0xc1bdceee);    // Step 4
			FF(out a,b,c,d, M[4],   7, 0xf57c0faf);    // Step 5
			FF(out d,a,b,c, M[5],  12, 0x4787c62a);    // Step 6
			FF(out c,d,a,b, M[6],  17, 0xa8304613);    // Step 7
			FF(out b,c,d,a, M[7],  22, 0xfd469501);    // Step 8
			FF(out a,b,c,d, M[8],   7, 0x698098d8);    // Step 9
			FF(out d,a,b,c, M[9],  12, 0x8b44f7af);    // Step 10
			FF(out c,d,a,b, M[10], 17, 0xffff5bb1);    // Step 11
			FF(out b,c,d,a, M[11], 22, 0x895cd7be);    // Step 12
			FF(out a,b,c,d, M[12],  7, 0x6b901122);    // Step 13
			FF(out d,a,b,c, M[13], 12, 0xfd987193);    // Step 14
			FF(out c,d,a,b, M[14], 17, 0xa679438e);    // Step 15
			FF(out b,c,d,a, M[15], 22, 0x49b40821);    // Step 16
		    
			//  Round 2
			GG(out a,b,c,d, M[1],   5, 0xf61e2562);    // Step 17
			GG(out d,a,b,c, M[6],   9, 0xc040b340);    // Step 18
			GG(out c,d,a,b, M[11], 14, 0x265e5a51);    // Step 19
			GG(out b,c,d,a, M[0],  20, 0xe9b6c7aa);    // Step 20
			GG(out a,b,c,d, M[5],   5, 0xd62f105d);    // Step 21
			GG(out d,a,b,c, M[10],  9, 0x02441453);    // Step 22
			GG(out c,d,a,b, M[15], 14, 0xd8a1e681);    // Step 23
			GG(out b,c,d,a, M[4],  20, 0xe7d3fbc8);    // Step 24
			GG(out a,b,c,d, M[9],   5, 0x21e1cde6);    // Step 25
			GG(out d,a,b,c, M[14],  9, 0xc33707d6);    // Step 26
			GG(out c,d,a,b, M[3],  14, 0xf4d50d87);    // Step 27
			GG(out b,c,d,a, M[8],  20, 0x455a14ed);    // Step 28
			GG(out a,b,c,d, M[13],  5, 0xa9e3e905);    // Step 29
			GG(out d,a,b,c, M[2],   9, 0xfcefa3f8);    // Step 30
			GG(out c,d,a,b, M[7],  14, 0x676f02d9);    // Step 31
			GG(out b,c,d,a, M[12], 20, 0x8d2a4c8a);    // Step 32
		    
			//  Round 3
			HH(out a,b,c,d, M[5],   4, 0xfffa3942);    // Step 33
			HH(out d,a,b,c, M[8],  11, 0x8771f681);    // Step 34
			HH(out c,d,a,b, M[11], 16, 0x6d9d6122);    // Step 35
			HH(out b,c,d,a, M[14], 23, 0xfde5380c);    // Step 36
			HH(out a,b,c,d, M[1],   4, 0xa4beea44);    // Step 37
			HH(out d,a,b,c, M[4],  11, 0x4bdecfa9);    // Step 38
			HH(out c,d,a,b, M[7],  16, 0xf6bb4b60);    // Step 39
			HH(out b,c,d,a, M[10], 23, 0xbebfbc70);    // Step 40
			HH(out a,b,c,d, M[13],  4, 0x289b7ec6);    // Step 41
			HH(out d,a,b,c, M[0],  11, 0xeaa127fa);    // Step 42
			HH(out c,d,a,b, M[3],  16, 0xd4ef3085);    // Step 43
			HH(out b,c,d,a, M[6],  23, 0x04881d05);    // Step 44
			HH(out a,b,c,d, M[9],   4, 0xd9d4d039);    // Step 45
			HH(out d,a,b,c, M[12], 11, 0xe6db99e5);    // Step 46
			HH(out c,d,a,b, M[15], 16, 0x1fa27cf8);    // Step 47
			HH(out b,c,d,a, M[2],  23, 0xc4ac5665);    // Step 48
		    
			//  Round 4
			II(out a,b,c,d, M[0],   6, 0xf4292244);    // Step 49
			II(out d,a,b,c, M[7],  10, 0x411aff97);    // Step 50
			II(out c,d,a,b, M[14], 15, 0xab9423a7);    // Step 51
			II(out b,c,d,a, M[5],  21, 0xfc93a039);    // Step 52
			II(out a,b,c,d, M[12],  6, 0x655b59c3);    // Step 53
			II(out d,a,b,c, M[3],  10, 0x8f0ccc92);    // Step 54
			II(out c,d,a,b, M[10], 15, 0xffeff47d);    // Step 55
			II(out b,c,d,a, M[1],  21, 0x85845dd1);    // Step 56
			II(out a,b,c,d, M[8],   6, 0x6fa87e4f);    // Step 57
			II(out d,a,b,c, M[15], 10, 0xfe2ce6e0);    // Step 58
			II(out c,d,a,b, M[6],  15, 0xa3014314);    // Step 59
			II(out b,c,d,a, M[13], 21, 0x4e0811a1);    // Step 60
			II(out a,b,c,d, M[4],   6, 0xf7537e82);    // Step 61
			II(out d,a,b,c, M[11], 10, 0xbd3af235);    // Step 62
			II(out c,d,a,b, M[2],  15, 0x2ad7d2bb);    // Step 63
			II(out b,c,d,a, M[9],  21, 0xeb86d391);    // Step 64

			H1 += a;  H2 += b;  H3 += c;  H4 += d;
		}
		//------------------------------------------------------------------------

		//  MD5Hash
		//      Compute MD5 hash value
		//      (IN) void * buffer          Data buffer to hash
		//      (IN) WORD length            Length of buffer
		//      (OUT) unsigned long * H     MD5 Hash value
		public uint[] MD5Hash (byte[] buffer, uint length)
		{
			int pos = 0;
			uint dataLen = length;
			uint[] HashBuf = new uint[16];   // Used for hashing a block
			uint[] H = new uint[4];
			int bytePos;
			byte byte1, byte2, byte3, byte4;

			// Load Magic Numbers
			H[0] = InitA;  H[1] = InitB;
			H[2] = InitC;  H[3] = InitD;

			// Process initial blocks, 64 bytes at a time
			while (length >= 64)
			{
				for (int ix=0; ix < 16; ix++)
					HashBuf[ix] = (uint) buffer[pos + ix * 4 + 0] +
								  (uint) (buffer[pos + ix * 4 + 1] << 8) +
								  (uint) (buffer[pos + ix * 4 + 2] << 16) +
								  (uint) (buffer[pos + ix * 4 + 3] << 24);

				ProcessBlock(ref H[0], ref H[1], ref H[2], ref H[3], HashBuf);
				pos += 64;
				length -= 64;
			}

			for (int ix=0; ix < 16; ix++)
				HashBuf[ix] = 0;

			// Last block: Add padding byte
			// Old C++   ((unsigned char *)HashBuf)[length] = 0x80;
			HashBuf[length / 4] = 0x80;
			HashBuf[length / 4] = (HashBuf[length / 4] << (byte) ((length % 4) * 8));

			// If last block > 448 bits, we'll need an additional
			// block for message length
			if (length > MaxLastBlock)
			{
				for (int ix = 0; ix <= ((int) ((length-1) / 4)); ix++)
				{
					bytePos = pos + ix * 4;

					byte1 = buffer[bytePos + 0];
					byte2 = byte3 = byte4 = 0;
					if ((bytePos + 1) < buffer.Length) byte2 = buffer[bytePos + 1];
					if ((bytePos + 2) < buffer.Length) byte3 = buffer[bytePos + 2];
					if ((bytePos + 3) < buffer.Length) byte4 = buffer[bytePos + 3];

					HashBuf[ix] |= (uint) byte1 + (uint) (byte2 << 8) + 
								   (uint) (byte3 << 16) + (uint) (byte4 << 24);
				}

				ProcessBlock(ref H[0], ref H[1], ref H[2], ref H[3], HashBuf);
				length = 0;
				for (int ix=0; ix < 16; ix++)
					HashBuf[ix] = 0;
			}

			// If last block <= 448 bits, we put in the message length
			// at the end of the block
			if (length > 0)
			{
				for (int ix = 0; ix <= ((int) ((length-1) / 4)); ix++)
				{
					bytePos = pos + ix * 4;

					byte1 = buffer[bytePos + 0];
					byte2 = byte3 = byte4 = 0;
					//if ((bytePos) < buffer.Length) byte1 = buffer[bytePos + 0];
					if ((bytePos + 1) < buffer.Length) byte2 = buffer[bytePos + 1];
					if ((bytePos + 2) < buffer.Length) byte3 = buffer[bytePos + 2];
					if ((bytePos + 3) < buffer.Length) byte4 = buffer[bytePos + 3];

					HashBuf[ix] |= (uint) byte1 + (uint) (byte2 << 8) + 
						(uint) (byte3 << 16) + (uint) (byte4 << 24);
				}
			}

			// Message Length: Last 64 bits == number of bits in message
			HashBuf[14] = dataLen << 3;
			HashBuf[15] = dataLen >> 29;
			ProcessBlock(ref H[0], ref H[1], ref H[2], ref H[3], HashBuf);

		    return H;
		}
		//------------------------------------------------------------------------


	}
}
