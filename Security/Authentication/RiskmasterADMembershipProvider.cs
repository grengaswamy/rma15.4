﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Runtime.Serialization;
using System.Web.Configuration;
using System.Web.Security;
using Riskmaster.Common;
using Riskmaster.Db;

namespace Riskmaster.Security.Authentication
{
    /// <summary>
    /// Membership Provider for Active Directory/Domain Authorization repositories
    /// </summary>
    [DataContract]
    public class RiskmasterADMembershipProvider : CustomMembershipProvider
    {
        private const int ERROR_LOGON_FAILURE = -2147023570;
        private string m_strMembershipProviderName = "RiskmasterADMembershipProvider";
        private string m_strADUserName = string.Empty, m_strADPassword = string.Empty;
        

        /// <summary>
        /// OVERLOADED: class constructor which initializes Active Directory 
        /// connection string
        /// </summary>
        /// <param name="strADConnString">string containing the connection string to the Active Director repository</param>
        public RiskmasterADMembershipProvider(string strADConnString)
        {
            this.ConnectionString = strADConnString;
        } // constructor


        /// <summary>
        /// Gets and sets the Application Name for the Membership Provider
        /// </summary>
        public override string ApplicationName
        {
            get
            {
                return m_strMembershipProviderName;
            }
            set
            {
                m_strMembershipProviderName = value;
            }
        }

        /// <summary>
        /// Provides the ability to change a user's Active Directory password
        /// </summary>
        /// <param name="username"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns>boolean indicating whether or not the user's password was successfully changed</returns>
        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            bool blnPwdChanged = false;

            try
            {
                using (DirectoryEntry root = new DirectoryEntry(this.ConnectionString, username, oldPassword, AuthenticationTypes.Secure | AuthenticationTypes.FastBind))
                {
                    root.Invoke("ChangePassword", oldPassword, newPassword);
                }//using
            }
            catch (Exception ex)
            {

                throw new MembershipPasswordException("Unable to change user password.", ex.InnerException);
            }//catch


            return blnPwdChanged;
        }//method: ChangePassword

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Authenticates the user against the Active Directory repository
        /// </summary>
        /// <param name="username">string containing the user name of the Active Directory user</param>
        /// <param name="password">string containing the password of the Active Directory user</param>
        /// <returns>boolean indicating whether or not the user was successfully authenticated</returns>
        public override bool ValidateUser(string username, string password)
        {
            bool blnIsValidUser = false;

            //srajindersin 05/25/2012 MITS 26800
            int intUserID = 0;
            bool isExist = false;
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            string connectionString = WebConfigurationManager.ConnectionStrings["RMXSecurity"].ConnectionString;
            //Add the parameters to the query
            dictParams.Add("USER_NAME", username);
            //Execute the query to authenticate the user
            using (DbReader dbReader = DbFactory.ExecuteReader(connectionString, AuthenticationRepository.GetUserID(), dictParams))
            {
                //Verify that the DbReader has rows/records
                if (dbReader.Read())
                {
                    intUserID = Common.Conversion.CastToType<Int32>(dbReader[AuthenticationCredentials.VALID_USER].ToString(), out isExist);
                }
            }//using
            //END srajindersin 05/25/2012 MITS 26800

            //rsuhsilaggar MITS 22777 Date 07/06/2012
            //Abhinav Sharma changes for SSL AD  starts
            // akaushik5 Changed for RMA-17510 Starts
            //string sAdusername = this.ConnectionString.Substring(this.ConnectionString.IndexOf("://") + 3) + @"\" + username;
            string domain = this.ConnectionString.Substring(this.ConnectionString.IndexOf("://") + 3);
            // akaushik5 Changed for RMA-17510 Ends
            // akaushik5 Added for RMA-13310 Starts 
            string strEncPassword = UtilityFunctions.EncryptString(password);
            // akaushik5 Added for RMA-13310 Ends
            // akaushik5 Changed for RMA-17510 Starts
            //if (this.ConnectionString.IndexOf("LDAPS") > -1)
            //{
            ////this.ConnectionString = this.ConnectionString.Replace("LDAPS", "LDAP");
            //using (DirectoryEntry root = new DirectoryEntry(this.ConnectionString, sAdusername, password, AuthenticationTypes.Secure))
            //{
            //    using (root)
            //    {
            //        try
            //        {
            //            //force the bind
            //            object tmp = root.NativeObject;

            //            blnIsValidUser = true;
            //        } // try
            //        catch (COMException ex)
            //        {
            //            //some other error happened, so rethrow it
            //            if (ex.ErrorCode != ERROR_LOGON_FAILURE)
            //            {
            //                //srajindersin 05/25/2012 MITS 26800
            //                //Update the Login tables for auditing/tracking purposes
            //                // akaushik5 Changed for RMA-13310 Starts
            //                //UtilityFunctions.AuditUserLogins(connectionString, username, password, intUserID, blnIsValidUser);
            //                UtilityFunctions.AuditUserLogins(connectionString, username, strEncPassword, intUserID, blnIsValidUser);
            //                // akaushik5 Changed for RMA-13310 Ends
            //                throw;
            //            } // if
            //        } // catch


            //    } // using
            //} // using

            //srajindersin 05/25/2012 MITS 26800
            //Update the Login tables for auditing/tracking purposes
            // akaushik5 Changed for RMA-13310 Starts
            //UtilityFunctions.AuditUserLogins(connectionString, username, password, intUserID, blnIsValidUser);
            //UtilityFunctions.AuditUserLogins(connectionString, username, strEncPassword, intUserID, blnIsValidUser);
            // akaushik5 Changed for RMA-13310 Ends
            //}
            //else
            //{
            //using (DirectoryEntry root = new DirectoryEntry(this.ConnectionString, sAdusername, password, AuthenticationTypes.FastBind))
            //{
            //    using (root)
            //    {
            //        try
            //        {
            //            //force the bind
            //            object tmp = root.NativeObject;

            //            blnIsValidUser = true;
            //        } // try
            //        catch (COMException ex)
            //        {
            //            //some other error happened, so rethrow it
            //            if (ex.ErrorCode != ERROR_LOGON_FAILURE)
            //            {
            //                //srajindersin 05/25/2012 MITS 26800
            //                //Update the Login tables for auditing/tracking purposes
            //                // akaushik5 Changed for RMA-13310 Starts
            //                //UtilityFunctions.AuditUserLogins(connectionString, username, password, intUserID, blnIsValidUser);
            //                UtilityFunctions.AuditUserLogins(connectionString, username, strEncPassword, intUserID, blnIsValidUser);
            //                // akaushik5 Changed for RMA-13310 Ends
            //                throw;
            //            } // if
            //        } // catch


            //    } // using
            //} // using

            //srajindersin 05/25/2012 MITS 26800
            //Update the Login tables for auditing/tracking purposes
            // akaushik5 Changed for RMA-13310 Starts
            //UtilityFunctions.AuditUserLogins(connectionString, username, password, intUserID, blnIsValidUser);
            //UtilityFunctions.AuditUserLogins(connectionString, username, strEncPassword, intUserID, blnIsValidUser);
            // akaushik5 Changed for RMA-13310 Ends
            //}
            //Abhinav Sharma changes for SSL AD ends
            blnIsValidUser = this.AuthenticateUsingPrincipalcontext(domain, username, password, this.ConnectionString.IndexOf("LDAPS") > -1);

            UtilityFunctions.AuditUserLogins(connectionString, username, strEncPassword, intUserID, blnIsValidUser);
            // akaushik5 Changed for RMA-17510 Ends
            return blnIsValidUser;
        }

        public override bool ValidateSMSUser(string username, string password)
        {
            throw new NotImplementedException();
        }

        #region IMembershipProviderExtensions Members


        public override string connectionStringName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string connectionUsername
        {
            get
            {
                return m_strADUserName;
            }
            set
            {
                m_strADUserName = value;
            }
        }

        public override string connectionPassword
        {
            get
            {
                return m_strADPassword;
            }
            set
            {
                m_strADPassword = value;
            }
        }

        #endregion

        // akaushik5 Added for RMA-17510 Starts
        /// <summary>
        /// Authenticates the using principalcontext.
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <param name="useSSL">if set to <c>true</c> [use SSL].</param>
        /// <returns></returns>
        private bool AuthenticateUsingPrincipalcontext(string domain, string userName, string password, bool useSSL)
        {
            bool bValid = default(bool);
            try
            {
                if (useSSL)
                {
                    domain += ":636";
                    ContextOptions options = ContextOptions.SimpleBind | ContextOptions.SecureSocketLayer;
                    using (PrincipalContext ctx = new PrincipalContext(ContextType.Domain, domain, null, options))
                    {
                        bValid = ctx.ValidateCredentials(userName, password, options);
                    }
                }
                else
                {
                    using (PrincipalContext ctx = new PrincipalContext(ContextType.Domain, domain))
                    {
                        bValid = ctx.ValidateCredentials(userName, password);
                    }
                }
            }
            catch (Exception exception)
            {
                if (!string.IsNullOrEmpty(exception.Message))
                {
                    Log.Write(exception.Message);
                }

                if (!string.IsNullOrEmpty(exception.StackTrace))
                {
                    Log.Write(exception.StackTrace);
                }
            }
            return bValid;
        }
        // akaushik5 Added for RMA-17510 Ends
    }
}
