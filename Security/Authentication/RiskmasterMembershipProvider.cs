using System;
using System.Collections.Generic;
using System.Web.Security;
using Riskmaster.Db;
using Riskmaster.Common;


namespace Riskmaster.Security.Authentication
{
    /// <summary>
    /// Summary description for RiskmasterMembershipProvider
    /// </summary>
    public class RiskmasterMembershipProvider : CustomMembershipProvider
    {
        private string m_strMembershipProviderName = "RiskmasterMembershipProvider";
        private string m_strDBUserName = string.Empty, m_strDBPassword = string.Empty;
        private const string RESOURCE_FILE = "AuthenticationCredentials.resx";


        /// <summary>
        /// Overloaded Class constructor
        /// </summary>
        /// <param name="strDbConnString">string containing the
        /// database connection string to the RISKMASTER Security database</param>
        public RiskmasterMembershipProvider(string strDbConnString)
        {
            this.ConnectionString = strDbConnString;
        }


        /// <summary>
        /// Gets and sets the Application Name for the Membership Provider
        /// </summary>
        public override string ApplicationName
        {
            get
            {
                return m_strMembershipProviderName;
            }
            set
            {
                m_strMembershipProviderName = value;
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool EnablePasswordReset
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch,
                           int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection users = new MembershipUserCollection();
            try
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                string strSQL = string.Format("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE LOGIN_NAME LIKE {0}%", "~EMAIL~");

                dictParams.Add("EMAIL", emailToMatch);

                using (DbReader dbReader = DbFactory.ExecuteReader(this.ConnectionString, strSQL, dictParams))
                {
                    while (dbReader.Read())
                    {
                        MembershipUser user =
                                     new MembershipUser(this.ApplicationName,
                                        dbReader["LOGIN_NAME"].ToString(), null, dbReader["LOGIN_NAME"].ToString(),
                                null, string.Empty, true, false,
                                        DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now,
                                        DateTime.Now);
                        users.Add(user);
                    }
                    //Raman: Closing dbReader
                    if (dbReader != null)
                    {
                        dbReader.Close();
                    }
                }//using
            }
            catch (Exception ex)
            {
                //-- throw exception
            }
            finally
            {
                //-- clean up and close connection
            }
            totalRecords = users.Count;
            return users;
        }


        public override MembershipUserCollection FindUsersByName
                   (string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection users = new MembershipUserCollection();
            try
            {
                Dictionary<string, string> dictParams = new Dictionary<string, string>();
                string strSQL = string.Format("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE LOGIN_NAME LIKE {0}%", "~USERNAME~");

                dictParams.Add("USERNAME", usernameToMatch);

                using (DbReader dbReader = DbFactory.ExecuteReader(this.ConnectionString, strSQL, dictParams))
                {
                    while (dbReader.Read())
                    {
                        MembershipUser user =
                                     new MembershipUser(this.ApplicationName,
                                        dbReader["LOGIN_NAME"].ToString(), null, dbReader["LOGIN_NAME"].ToString(),
                                null, string.Empty, true, false,
                                        DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now,
                                        DateTime.Now);
                        users.Add(user);
                    }
                    //Raman: Closing dbReader
                    if (dbReader != null)
                    {
                        dbReader.Close();
                    }
                }//using
            }
            catch (Exception ex)
            {
                //-- throw exception
            }
            finally
            {
                //-- clean up and close connection
            }
            totalRecords = users.Count;
            return users;
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override string GetPassword(string username, string answer)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            return new RiskmasterMembershipUser(username, this.ApplicationName, this.ConnectionString);
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            return new RiskmasterMembershipUser(providerUserKey.ToString(), this.ApplicationName, this.ConnectionString);
        }

        public override string GetUserNameByEmail(string email)
        {
            string username = string.Empty;
            
            Dictionary<string, string> dictParams = new Dictionary<string, string>();

            dictParams.Add("EMAIL_ADDRESS", email);

            using (DbReader dbReader = DbFactory.ExecuteReader(this.ConnectionString, AuthenticationRepository.GetUserNameByEmail(), dictParams))
            {
                while (dbReader.Read())
                {
                    username = dbReader["LOGIN_NAME"].ToString();
                }//while
                //Raman: Closing dbReader
                if (dbReader != null)
                {
                    dbReader.Close();
                }
            }//using
            
            return username;
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool UnlockUser(string userName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// Updates the RiskmasterUser with the specified properties
        /// </summary>
        /// <param name="user"></param>
        public override void UpdateUser(MembershipUser user)
        {
            throw new Exception("The method or operation is not implemented.");
            //TODO: Call the UpdateUserData method to save and persist these values to the db
            //TODO: Call the UpdateUser method to save and persist these values to the db
        }

        /// <summary>
        /// Validates that a user is a member of the Security Database
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public override bool ValidateUser(string username, string password)
        {
            bool blnIsValidUser = false;
            string strResult = string.Empty;
            string strCurrDate = DateTime.Now.ToString("yyyyMMdd");
            int intUserID = 0;
           
            Dictionary<string, string> dictParams = new Dictionary<string, string>();

            //Encrypt the password
            string strEncPassword = UtilityFunctions.EncryptString(password);

            //Add the parameters to the query
            dictParams.Add("UID1", username);
            dictParams.Add("PWD1", strEncPassword);
            
            //Raman Bhatia 04/22/2009: Current Date and expiry date validations should be a part of AUTHORIZATION and not AUTHENTICATION
            //These settings work at a DataSource level and NOT during Authentication
            //Commenting all such code from here

            //dictParams.Add("CURR_DATE", strCurrDate);

            //Verify if Enhanced Security is turned on
            //if (UtilityFunctions.IsEnhancedSecurityEnabled())
            //{
            //    dictParams.Add("CURR_DATE", strCurrDate);

            //    //Execute the query to authenticate the user
            //    using (DbReader dbReader = DbFactory.ExecuteReader(this.ConnectionString, AuthenticationRepository.VerifyEnhancedSecurityUser(), dictParams))
            //    {
            //        //Verify that the DbReader has rows/records
            //        if (dbReader.Read())
            //        {
            //            strResult = dbReader[AuthenticationCredentials.VALID_USER].ToString();
            //        } // if 
            //    }//using
            //}//if
            //else
            //{
                
                //Execute the query to authenticate the user
                using (DbReader dbReader = DbFactory.ExecuteReader(this.ConnectionString, AuthenticationRepository.VerifyUser(), dictParams))
                {
                    //Verify that the DbReader has rows/records
                    if (dbReader.Read())
                    {
                        strResult = dbReader[AuthenticationCredentials.VALID_USER].ToString();
                    } // if 
                    //Raman: Closing dbReader
                    if (dbReader != null)
                    {
                        dbReader.Close();
                    }
                }//using
            //}//else
            
            bool blnSuccess = Int32.TryParse(strResult, out intUserID);
            
            //Verify that a result was returned
            if (blnSuccess)
            {
                blnIsValidUser = true;
            } // if

            //Update the Login tables for auditing/tracking purposes
            UtilityFunctions.AuditUserLogins(this.ConnectionString, username, strEncPassword, intUserID, blnIsValidUser);

            //return the boolean indicating whether or not
            //the user has been authenticated successfully
            return blnIsValidUser;
        }

        /// <summary>
        /// Validates that a user is an admin of the Security Database
        /// Created by : Raman Bhatia
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public override bool ValidateSMSUser(string username, string password)
        {
            bool blnIsValidSMSUser = false;
            string strResult = string.Empty;
            int intUserID = 0;

            Dictionary<string, string> dictParams = new Dictionary<string, string>();

            //Encrypt the password
            string strEncPassword = UtilityFunctions.EncryptString(password);

            //Add the parameters to the query
            dictParams.Add("UID1", username);
            dictParams.Add("PWD1", strEncPassword);

            //Execute the query to authenticate the user
            using (DbReader dbReader = DbFactory.ExecuteReader(this.ConnectionString, AuthenticationRepository.VerifySMSUser(), dictParams))
            {
                //Verify that the DbReader has rows/records
                if (dbReader.Read())
                {
                    strResult = dbReader[AuthenticationCredentials.VALID_USER].ToString();
                } // if 
                //Raman: Closing dbReader
                if (dbReader != null)
                {
                    dbReader.Close();
                }
            }//using
            //}//else

            bool blnSuccess = Int32.TryParse(strResult, out intUserID);

            //Verify that a result was returned
            if (blnSuccess)
            {
                blnIsValidSMSUser = true;
            } // if

            //Update the Login tables for auditing/tracking purposes
            UtilityFunctions.AuditUserLogins(this.ConnectionString, username, strEncPassword, intUserID, blnIsValidSMSUser);

            //return the boolean indicating whether or not
            //the user has been authenticated successfully
            return blnIsValidSMSUser;
        }

        #region IMembershipProviderExtensions Members

        

        public override string connectionStringName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string connectionUsername
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string connectionPassword
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

    }//class RiskmasterMembershipProvider
}//namsepace