﻿using System;
using System.Security.Principal;

namespace Riskmaster.Security.Authentication
{
    public interface IRiskmasterMembershipUser
    {
        bool ChangePassword(string oldPassword, string newPassword);
        string Comment { get; set; }
        DateTime CreationDate { get; }
        string Email { get; set; }
        int GetHashCode();
        string GetPassword();
        bool IsApproved { get; set; }
        bool IsLockedOut { get; }
        DateTime LastActivityDate { get; set; }
        DateTime LastLockoutDate { get; }
        DateTime LastLoginDate { get; set; }
        DateTime LastPasswordChangedDate { get; }
        string ProviderName { get; }
        object ProviderUserKey { get; }
        string ToString();
        bool UnlockUser();
        string UserName { get; }
    }//interface
}//namespace
