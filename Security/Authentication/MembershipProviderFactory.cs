﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using Riskmaster.Db;
using Riskmaster.Security.Encryption;
namespace Riskmaster.Security.Authentication
{
    /// <summary>
    /// Provides a Factory Method implementation for obtaining a handle to a particular
    /// Membership Provider
    /// </summary>
    public class MembershipProviderFactory
    {
        private const string strRMADProvider = "Active Directory", strRMLDAPProvider = "LDAP", strRMDBProvider = "Riskmaster";

        #region Internal methods
        /// <summary>
        /// Factory method implementation to obtain a handle
        /// to the specified Membership Provider required for authentication
        /// </summary>
        /// <param name="strMembershipProviderName">string containing the name of the MembershipProvider to be instantiated</param>
        /// <param name="strProviderConnString">string containing the connection string for the specified Membership Provider</param>
        /// <param name="dictMembershipProviderAttributes">generic string dictionary collection containing all of the relevant 
        /// user attribute mappings required for a particular membership authentication provider</param>
        /// <returns>CustomMembershipProvider</returns>
        internal static CustomMembershipProvider CreateMembershipProvider(string strMembershipProviderName, string strProviderConnString,
            Dictionary<string, string> dictMembershipProviderAttributes, int p_iClientId)
        {
            CustomMembershipProvider objMembProvider = null;

            switch (strMembershipProviderName)
            {
                case strRMADProvider:
                    objMembProvider = new RiskmasterADMembershipProvider(strProviderConnString);
                    break;
                case strRMLDAPProvider:
                    objMembProvider = new RiskmasterLDAPMembershipProvider(strProviderConnString, dictMembershipProviderAttributes["USER_ATTRIBUTE_MAPPING"], dictMembershipProviderAttributes["ADMIN_UID"], dictMembershipProviderAttributes["ADMIN_PWD"], p_iClientId);
                    break;
                case strRMDBProvider:
                    objMembProvider = new RiskmasterMembershipProvider(strProviderConnString);
                    break;
                default:
                    objMembProvider = new RiskmasterMembershipProvider(strProviderConnString);
                    break;
            } // switch

            return objMembProvider;
        } // method: CreateMembershipProvider

        /// <summary>
        /// Gets the configured membership provider from the Riskmaster Security database
        /// </summary>
        /// <param name="strSecurityConnString">string containing the connection
        /// string to the RISKMASTER Security database</param>
        /// <param name="strMembershipProviderName">out string parameter containing the name 
        /// of the configured Membership Provider from the database</param>
        /// <returns>string containing the connection string for any configured Membership Providers</returns>
        internal static string GetMembershipProvider(string strSecurityConnString, out string strMembershipProviderName)
        {
            string strProviderConnString = string.Empty;

            //Specify the default connection string and membership provider as RISKMASTEr
            strMembershipProviderName = strRMDBProvider;
            strProviderConnString = strSecurityConnString;

            //Get any enabled SSO authentication provider from the Security database
            using (DbReader dbReader = DbFactory.ExecuteReader(strSecurityConnString, AuthenticationRepository.GetEnabledAuthenticationProvider()))
            {
                while (dbReader.Read())
                {
                    object objProviderConnectionString = dbReader["CONN_STRING"];

                    //Verify that the return value is not null for alternative SSO authentication providers
                    if (objProviderConnectionString != null)
                    {
                        strProviderConnString = objProviderConnectionString.ToString();
                        strMembershipProviderName = dbReader["AUTH_PROVIDER_TYPE_DESC"].ToString();
                    } // if
                }//while
            }//using

            //return the connection string for the specified SSO authentication provider
            return strProviderConnString;
        } // method: GetMembershipProvider 

        /// <summary>
        /// Populates the relevant additional attributes required
        /// for instantiating a specific MembershipProvider
        /// </summary>
        /// <param name="strSecurityConnString">string containing the database connection string</param>
        /// <param name="strMembProviderName">string containing the name of the specified Membership Provider</param>
        /// <returns>Generic String Dictionary collection containing all additional membership provider properties</returns>
        internal static Dictionary<string, string> PopulateMembershipProviderAttributes(string strSecurityConnString, string strMembProviderName)
        {
            Dictionary<string, string> dictMembProviderAttributes = new Dictionary<string, string>();

         
            //Populate the required attributes for the MembershipProvider
            using (DbReader dbReader = DbFactory.ExecuteReader(strSecurityConnString, AuthenticationRepository.GetSSOProviderAttributes(strMembProviderName)))
            {
                while (dbReader.Read())
                {
                    dictMembProviderAttributes.Add("USER_ATTRIBUTE_MAPPING", dbReader["USER_ATTRIBUTE_MAPPING"].ToString());
                    dictMembProviderAttributes.Add("ADMIN_UID", dbReader["ADMIN_UID"].ToString());
                    dictMembProviderAttributes.Add("ADMIN_PWD",RMCryptography.DecryptString( dbReader["ADMIN_PWD"].ToString()));//rsushilaggar MITS 23841
                }//while
            }//using

            //return the populated attributes
            return dictMembProviderAttributes;
        } // method: PopulateMembershipProviderAttributes

        #endregion

        #region Public methods
        /// <summary>
        /// Factory method implementation to obtain a handle
        /// to the specified Membership Provider required for authentication
        /// </summary>
        /// <param name="strSecurityConnString">string containing the database connection string to the RISKMASTER
        /// Security database</param>
        /// <returns>CustomMembershipProvider</returns>
        public static CustomMembershipProvider CreateMembershipProvider(string strSecurityConnString, int p_iClientId)
        {
            string strMembershipProviderName = string.Empty;
            Dictionary<string, string> dictMembershipProviderAttributes = new Dictionary<string,string>();

            string strProviderConnString = GetMembershipProvider(strSecurityConnString, out strMembershipProviderName);

            dictMembershipProviderAttributes = PopulateMembershipProviderAttributes(strSecurityConnString, strMembershipProviderName);

            CustomMembershipProvider membProvider = CreateMembershipProvider(strMembershipProviderName, strProviderConnString, dictMembershipProviderAttributes, p_iClientId);

            return membProvider;
        } // method: CreateMembershipProvider



        /// <summary>
        /// Authenticates a user against a specified Membership Provider
        /// </summary>
        /// <param name="strSecurityConnString">string specifying the RISKMASTER 
        /// Security database connection string</param>
        /// <param name="strUserName">string specifying the user name to authenticate
        /// against the security database</param>
        /// <param name="strPassword">string specifying the user's password to authenticate</param>
        /// <param name="strMessage">string specifying the exception message if any</param>
        /// <returns>boolean indicating whether or not the user was successfully authenticated</returns>
        public static bool AuthenticateUser(string strSecurityConnString,
            string strUserName, string strPassword, out string strMessage, int p_iClientId)
        {
            strMessage = string.Empty;

            bool blnIsAuthenticated = false;
            const string RISKMASTER_MEMBERSHIP_PROVIDER = "RiskmasterMembershipProvider";

            CustomMembershipProvider membProvider = CreateMembershipProvider(strSecurityConnString, p_iClientId);

            if (string.IsNullOrEmpty(strUserName) || string.IsNullOrEmpty(strPassword))
            {
                throw new ArgumentNullException("Username or password credentials were not provided.");
            } // if
            else
            {
                blnIsAuthenticated = membProvider.ValidateUser(strUserName, strPassword);
            } // else

            //Raman Bhatia: 10/30/2009
            //Enhanced Security Changes
            
            /*
            
            //If the Membership Provider selected is other than the RISKMASTER Membership Provider
            //and the user has already been successfully authenticated against the other provider
            if (blnIsAuthenticated && string.Compare(membProvider.ApplicationName, RISKMASTER_MEMBERSHIP_PROVIDER, true) != 0) //0 means equality
            {
                //Verify that the user also exists in the Riskmaster Security database
                blnIsAuthenticated = RiskmasterMembershipUser.IsValidRiskmasterUser(strSecurityConnString, strUserName, out strMessage);
            } // if
             
            */

            if (blnIsAuthenticated)
            {
                blnIsAuthenticated = RiskmasterMembershipUser.IsValidRiskmasterUser(strSecurityConnString, strUserName, out strMessage, p_iClientId);
            }
            else if (string.Compare(membProvider.ApplicationName, RISKMASTER_MEMBERSHIP_PROVIDER, true) == 0)
            {
                RiskmasterMembershipUser.CheckPasswordPolicy(strSecurityConnString, strUserName, ref blnIsAuthenticated, out strMessage, p_iClientId);
            }

            return blnIsAuthenticated;
        } // method: AuthenticateUser 


        /// <summary>
        /// Allows a user to be authenticated for non-SAML/non-Claims based Single Sign On Authentication schemes
        /// </summary>
        /// <param name="strSecurityConnString">string specifying the RISKMASTER 
        /// Security database connection string</param>
        /// <param name="strUserName">string specifying the user name to authenticate
        /// against the security database</param>
        /// <returns>boolean indicating whether or not the user was successfully authenticated</returns>
        public static bool SingleSignOnUser(string strSecurityConnString, string strUserName, int p_iClientId)
        {
            string strMessage = string.Empty;

            return RiskmasterMembershipUser.IsValidRiskmasterUser(strSecurityConnString, strUserName, out strMessage, p_iClientId);
        }//method: SingleSignOnUser()
        /// <summary>
        /// Authenticates an SMS user against a specified Membership Provider
        /// </summary>
        /// <param name="strSecurityConnString">string specifying the RISKMASTER 
        /// Security database connection string</param>
        /// <param name="strUserName">string specifying the user name to authenticate
        /// against the security database</param>
        /// <param name="strPassword">string specifying the user's password to authenticate</param>
        /// <param name="strMessage">string specifying the exception message if any</param>
        /// <returns>boolean indicating whether or not the user was successfully authenticated</returns>
        public static bool AuthenticateSMSUser(string strSecurityConnString,
            string strUserName, string strPassword, out string strMessage, int p_iClientId)
        {
            strMessage = string.Empty;

            bool blnIsAuthenticated = false;

            CustomMembershipProvider membProvider = CreateMembershipProvider(strSecurityConnString, p_iClientId);

            if (string.IsNullOrEmpty(strUserName) || string.IsNullOrEmpty(strPassword))
            {
                throw new ArgumentNullException("Username or password credentials were not provided.");
            } // if
            else
            {
                blnIsAuthenticated = membProvider.ValidateSMSUser(strUserName, strPassword);
            } // else

            return blnIsAuthenticated;
        } // method: AuthenticateUser 

        #endregion

       

    }
}
