using System;
using System.ComponentModel;
using Riskmaster.Db;
using Riskmaster.Security.Encryption;
using Riskmaster.Common.Win32;
using Riskmaster.Common;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Win32;
using Riskmaster.ExceptionTypes;
using System.Collections.Specialized;
using System.Runtime.InteropServices;
using Riskmaster.Security.Authentication;

namespace Riskmaster.Security
{
	/// <summary>
	/// Riskmaster.Security.Login wraps a native provider Component object.
	/// This class is designed to do the primary authentication.
	/// </summary>
	[Serializable]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [ComVisible(true)]
	public class Login:Component
	{
        
        private string m_Dsn = string.Empty;
		// private bool m_bSuppressLicense=false;
		private int m_hMapObject=0;
		private unsafe LoginDlgInfo* m_lpGDlgInfo;
        private int m_iClientId = 0;
		/// <summary>
		/// Default class constructor
		/// </summary>
        /// <remarks>Should no longer be used since this requires a reference handle
        /// to the Windows 32-bit registry and therefore limits usage of the Login
        /// class to only 32-bit OSes</remarks>
        /// <see cref="Login">Login overloaded constructors</see>
        //[Obsolete]
        //public Login()
        //{
        //    m_Dsn=SecurityDatabase.GetSecurityDsn(0);
        //}
        /// <summary>
        /// Overloaded class constructor
        /// </summary>
        /// Add by kuladeep for Cloud.
        /// <param name="ClientId"></param>
        public Login(int p_iClientId)
        {
            m_iClientId = p_iClientId;
            m_Dsn = SecurityDatabase.GetSecurityDsn(m_iClientId);
            
        }
        /// <summary>
        /// Overloaded class constructor
        /// </summary>
        /// <param name="objStrDictValues">StringDictionary value contains key-value pairs
        /// which determine how the ultimate security database connection string
        /// will be constructed</param>
        public Login(StringDictionary objStrDictValues, int p_iClientId)
        {
            SecurityDatabaseManager.CreateSecurityDatabase();
            m_iClientId = p_iClientId;
            m_Dsn = SecurityDatabase.GetSecurityDsn(m_iClientId);            
        } // constructor


		/// <summary>
		/// Overloaded class constructor
		/// </summary>
		/// <param name="securityDsn">string containing the database connection string
        /// to the Riskmaster Security database</param>
        /// <remarks>The database connection string to the Riskmaster Security database
        /// could be either a System DSN or a fully qualified (OLEDB/ODBC etc.) database connection string</remarks>
		public Login(string securityDsn, int p_iClientId)
		{
			m_Dsn=securityDsn;
            m_iClientId = p_iClientId;
		}

		/// <summary>
		/// Gets the Riskmaster Security DSN
		/// </summary>
		/// <returns>string containing the Riskmaster Security DSN</returns>
		public string SecurityDsn
		{
			get
			{
				return m_Dsn;
			}
		}


		/// <summary>
		/// Riskmaster.Security.LoginsActive is the read only property assign new object to the struct LoginDlgInfo. 
		/// </summary>
		/// <returns>It returns the sared memory map name with refference to the object of memory mapped data structure</returns>
		public bool LoginsActive
		{
			get
			{
				LoginDlgInfo DialogInfo = new LoginDlgInfo();
				return CheckMemMap(ref DialogInfo); //Memory Map is cleaned up by Dispose method
			}
		}

		/// <summary>
		/// Gets a list of all RISKMASTER Databases
        /// configured in the Security database
		/// </summary>
		/// <returns>string array containing all RISKMASTER Database Names</returns>
		public string[] GetDatabases()
		{
			string[] Databases;
			
			ArrayList dbList=new ArrayList(10);
            using(DbReader reader=DbFactory.ExecuteReader(m_Dsn, "SELECT DATA_SOURCE_TABLE.DSN FROM DATA_SOURCE_TABLE ORDER BY DATA_SOURCE_TABLE.DSN"))
            {
        		while(reader.Read())
				{
					dbList.Add(reader.GetString(0));
				} 
            }//using
			
			Databases=new string[dbList.Count];
			dbList.CopyTo(Databases);
	
			return Databases;
		}//method: GetDatabases()


		/// <summary>
		/// Riskmaster.Security.GetUserDatabases returns the list of DSN's that this UserLogin name should have access to.
		/// Note: It does not do a full authorization request for each DSN, only tests that this user is configured as a login for each DSN returned.
		/// </summary>
		/// <param name="username">User Name</param>
		/// <returns>string array of DSN Names.</returns>
		/// <remarks>Note: This override provides the same lookup as the version that takes a password but simply assumes that the 
		/// password will be the same accross all available datasources for the given UserName.  This is a change going forward to support
		/// the idea of "hot-swapping" your database with-out fully logging off of the system.  
		/// The primary reason for this is to deal with Single Sign-On (SSO).  
		/// In that case, password is logically an attribute of the User, not the UserLogin anymore.
		/// </remarks>
		public string[] GetUserDatabases(string username)
		{
			
			List<string> dbList = new List<string>();

            using (DbReader reader = DbFactory.ExecuteReader(m_Dsn, String.Format("SELECT DATA_SOURCE_TABLE.DSN FROM USER_DETAILS_TABLE, DATA_SOURCE_TABLE WHERE USER_DETAILS_TABLE.DSNID=DATA_SOURCE_TABLE.DSNID AND USER_DETAILS_TABLE.LOGIN_NAME='{0}'  ORDER BY DATA_SOURCE_TABLE.DSN", username)))
            {
                while (reader.Read())
                {
                	dbList.Add(reader.GetString(0));
                }//while	
            }//using

            return dbList.ToArray();
		}//method: GetUserDatabases()

		/// <summary>
		/// Riskmaster.Security.GetUserDatabases returns the list of DSN's that this UserLogin name should have access to.
		/// Note: It does not do a full authorization request for each DSN, only tests that this user is configured as a login for each DSN returned.
		/// </summary>
		/// <param name="username">User Name</param>
		/// <param name="password">Databse Users password </param>
		/// <returns>string array of DSN names.</returns>
		public string[] GetUserDatabases(string username, string password)
		{
         
			List<string> dbList = new List<string>();
            string encPassword = RMCryptography.EncryptString(password);

            using (DbReader reader = DbFactory.ExecuteReader(m_Dsn, String.Format("SELECT DATA_SOURCE_TABLE.DSN FROM USER_DETAILS_TABLE, DATA_SOURCE_TABLE WHERE USER_DETAILS_TABLE.DSNID=DATA_SOURCE_TABLE.DSNID AND USER_DETAILS_TABLE.LOGIN_NAME='{0}' AND USER_DETAILS_TABLE.PASSWORD='{1}' ORDER BY DATA_SOURCE_TABLE.DSN", username, encPassword)))
	        {
        		while(reader.Read())
				{
					dbList.Add(reader.GetString(0));
				}//while
	        }//using

            return dbList.ToArray();
        }//method: GetUserDatabases()
	
		/// <summary>
		/// Riskmaster.Security.bNoAutoLogin is the internal function which return based on Registry Setting
		/// </summary>
		/// <returns>It returns login status true or false</returns>
		
		internal bool bNoAutoLogin()
		{	
            bool blnAutoLogin = false;

			blnAutoLogin = Convert.ToBoolean(RegistryManager.ReadRegistryKey(@"SOFTWARE\DTG\RISKMASTER\Security", "NoAutoLogin"));

            return blnAutoLogin;
		}
		
		/// <summary>
		/// Private utility function Riskmaster.Security.CheckMemMap used to get the current session id for use building a properly scoped Shared Memory Map Name in a
		/// Terminal Services situation.
		/// </summary>
		/// <param name="dlgInfo">Objects of the struct LoginDlgInfo</param>
		/// <returns>It returns boolean</returns>
	
		private unsafe  bool CheckMemMap(ref LoginDlgInfo dlgInfo)
		{		 
			string szShmName="";

			if (m_hMapObject!=0)
			{
				Common.Win32.Functions.CloseHandle(m_hMapObject);
				m_hMapObject = 0;
			}
			szShmName = GetSharedMemName(); // get name of shared memory segment (differs if running on Terminal Server)

			// Connect to shared memory segment
			m_hMapObject = Functions.CreateFileMapping(unchecked((int) 0xFFFFFFFF),0, (int)eMemMapRights.PAGE_READWRITE, 0, 4096, szShmName);
			if (m_hMapObject!=0)
			{
				if (Functions.GetLastError() == Const.ERROR_ALREADY_EXISTS)
				{   // silent login
					
					m_lpGDlgInfo= (LoginDlgInfo*)  Functions.MapViewOfFile(m_hMapObject, (int)eMemViewRights.FILE_MAP_WRITE, 0, 0, 0); 
					//unchecked{(m_lpGDlgInfo) }
					if (!m_lpGDlgInfo->szUserName.Equals(""))
					{
						dlgInfo.szUserName  = m_lpGDlgInfo->szUserName;
						dlgInfo.szPassword =  m_lpGDlgInfo->szPassword;
						dlgInfo.szDSN =  m_lpGDlgInfo->szDSN;
                        dlgInfo.ClientId = m_iClientId;
//						dlgInfo.pObj = (void*)0;
						return true;
					}
				}
				else
					m_lpGDlgInfo=(LoginDlgInfo*)Functions.MapViewOfFile(m_hMapObject, (int)eMemViewRights.FILE_MAP_WRITE, 0, 0, 0);
			}
			return false;
		}

		/// <summary>
		/// Riskmaster.Security.GetSharedMemName gets shared memory name by passing the session id.
		/// </summary>
		/// <returns>It returns string -"RMLOGINSILENTLOGIN" with sessionid if is terminal server</returns>
		private string GetSharedMemName()
		{
			
            return "RMLOGINSILENTLOGIN";
		}


		/// <summary>
		/// Riskmaster.Security.RegisterApplication takes the refernce of the natvive providers. 
		/// It is called from the Client desktop layer by passing the Application Window with reference 
		/// to Userlogin. 
		/// </summary>
		/// <param name="hAppWindow">To get the handle of desktop window </param>
		/// <param name="dwFlags">Integer Flag</param>
		/// <param name="pUser">Object reference to the class UsrLogin</param>

        public bool RegisterApplication(int hAppWindow, int dwFlags, ref UserLogin pUser, int p_iClientId)
		{
            m_iClientId = p_iClientId; // Only this function, other than constructors, is allowed to set clientID for login from tools // Ash

			if(! System.Environment.UserInteractive) 
				//// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
				///Replaced general exception with ProcDeniedInteractiveLogonException.Added by Tanuj Narula on 27/5/2004
                throw new ProcDeniedInteractiveLogonException(Globalization.GetString("Login.RegisterApplication.ProcDeniedInteractiveLogon", m_iClientId));
			
			DbConnection db = DbFactory.GetDbConnection(m_Dsn); //Factory.GetDbConnection(m_Dsn);
			DbReader rdr = null;
			LoginDlgInfo DialogInfo = new LoginDlgInfo();
			frmLogin frm = null;
			Riskmaster.Common.Win32.hWnd hwnd = new Riskmaster.Common.Win32.hWnd((IntPtr)hAppWindow);
			UserLogin objUserLogin = null;

			//Login Configuration Settings
			bool bSecureLogin = false;
			string sSMTPServer = "";
			string sAdminEmailAddr = "";
			bool bSuccess = false;
			bool bQuit = false;
		    bool bSilent = false;
			int i = 1;
			
			//Hold the defaults.
			string sPrevLogin="";
			string sPrevDSN="";
			string[] DSNs = null;
			bool ret=false;
			//m_bSuppressLicense = false; 


			if (hAppWindow ==0) 
					hAppWindow = Functions.GetDesktopWindow();
			hwnd = new Riskmaster.Common.Win32.hWnd( (IntPtr) hAppWindow);

			if (System.Globalization.DateTimeFormatInfo.CurrentInfo.ShortDatePattern.ToUpper().IndexOf("YYYY") <0 )
				//// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
				///Replaced general exception with DateNotY2KCompliantException custom exception.Added by Tanuj Narula on 27/5/2004 
                throw new DateNotY2KCompliantException(Globalization.GetString("Login.RegisterApplication.DateNotY2KCompliant", m_iClientId));
			
			//Get Secure Login Option from SecDB
			// Get administrator Email info from SecDB (we send email if 3 consecutive failed login attempts.)
			try
			{
				db.Open();
				rdr = db.ExecuteReader("SELECT SECURE_DSN_LOGIN, SMTP_SERVER, ADMIN_EMAIL_ADDR  FROM SETTINGS");
				if (rdr.Read())
				{
					bSecureLogin = (rdr.GetInt16("SECURE_DSN_LOGIN")!=0);
					sSMTPServer = rdr.GetString("SMTP_SERVER");
					sAdminEmailAddr = rdr.GetString("ADMIN_EMAIL_ADDR");
				}
			}
			finally
			{ 
				if (rdr !=null)
					rdr.Close();
				if (db !=null) 
					db.Close();
			}
	
			// Set-Up & Check the global Memory Map
            
			bSilent = CheckMemMap(ref DialogInfo);

			//Prepare the login Dialog Info
			if (!bSilent) 
			{
				DialogInfo.szUserName = sPrevLogin;
				DialogInfo.szDSN = sPrevDSN;
				DialogInfo.bSecureLogin = bSecureLogin;
                DialogInfo.ClientId = m_iClientId;
			}
			
			//Show the login dialog
			unsafe{frm = new frmLogin(&DialogInfo);}
			for(i = 1; i<=3 && !bSuccess && !bQuit; i++)
			{
				if(bSilent || frm.ShowDialog(hwnd)==System.Windows.Forms.DialogResult.OK  ) 
				{ 
					//Do we need to Guess or Prompt for the desired DSN now?
					if (DialogInfo.szDSN=="" && bSecureLogin)
					{
						DSNs = this.GetUserDatabases(DialogInfo.szUserName,DialogInfo.szPassword);

                        if (DSNs != null && DSNs.Length != 0) //Got "Valid" Credentials
						{
                            if (DSNs.Length == 1) //User can only access a single DSN, just default it.
                            {
                                DialogInfo.szDSN = DSNs[0];
                            } // if
                            else //More than one, better ask...
                            {
                                frm.Dispose();
                                unsafe { frm = new frmLogin(&DialogInfo); }
                                bQuit = (frm.ShowDialog(hwnd) != System.Windows.Forms.DialogResult.OK);
                                if (bQuit)
                                    continue;
                            }
						}	
                        //BSB 06.01.2007 Was getting multiple "invalid login" msg boxes.
                        // Let eventual failed login attempt exception generate this message box...
                        //else  //Couldn't get DSN list, must be bad credentials...
                        //    System.Windows.Forms.MessageBox.Show(hwnd, Globalization.GetString("Login.RegisterApplication.InvalidUNamePwd"), Globalization.GetString("Login.RegisterApplication.LoginError"), System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Stop);	
						
					}
					if(!bSecureLogin)
					{
						sPrevLogin =DialogInfo.szUserName;
						sPrevDSN = DialogInfo.szDSN;
					}		

					try
					{
						//If we have a full set of credentials, then try to authenticate - else, run the loop again (will populate dsn-list)

                        bSuccess = AuthenticateUser(DialogInfo.szDSN, DialogInfo.szUserName, DialogInfo.szPassword, out objUserLogin, m_iClientId);	
						if(bSuccess)
						{
							ret = true;
						}
						try //Store User Entries
						{							
							unsafe
							{
								if (!bSilent && m_lpGDlgInfo!=null)
								{		//Shared Memory
									m_lpGDlgInfo->szUserName = DialogInfo.szUserName;
									m_lpGDlgInfo->szPassword = DialogInfo.szPassword;
									m_lpGDlgInfo->szDSN = DialogInfo.szDSN;
								}
							}
						}
						catch
						{
							//Eat any errors.
						}


					}
					catch(Exception e)
					{
						//// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
						if(e.InnerException!=null)
                            System.Windows.Forms.MessageBox.Show(hwnd, e.InnerException.Message, Globalization.GetString("Login.RegisterApplication.LoginError", m_iClientId), System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Stop);	
						else
                            System.Windows.Forms.MessageBox.Show(hwnd, e.Message, Globalization.GetString("Login.RegisterApplication.LoginError", m_iClientId), System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Stop);	
					}
				}
				else //User cancelled.
					break;
				if (bSilent) //Only silently do a single check.
					break;
			}//End For
			
			try
			{			//Failed all 3 times, send email message to administrator
				if((ret ==false && i == 4) && (sSMTPServer != "" && sAdminEmailAddr != ""))
				{
					System.Net.Mail.SmtpClient objClient = new System.Net.Mail.SmtpClient(sSMTPServer);
                    //// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                    objClient.Send(sAdminEmailAddr, sAdminEmailAddr, Globalization.GetString("Login.RegisterApplication.LoginFailure", m_iClientId), Globalization.GetString("Login.RegisterApplication.FailedLogin", m_iClientId) + System.Environment.MachineName + Globalization.GetString("Login.RegisterApplication.UsingUID", m_iClientId) + sPrevLogin + " .");			
				}
			}
			catch{} //Eat Errors (like prev version)

			pUser = objUserLogin;
			return ret;
		}

        
		/// <summary>
		/// Riskmaster.Security.AuthUser wraps the native provider. It check the following
		///  1. Permissions and Password Expiration, 
		///  2. Permissions to login today. Raise the PermissionExpired exception if user cannot login
		///  3. Checksum's and CRC for the License Info (Handled at object level.)
		///  4. Check number of seat licences against number of currently logged in machines.
		///  It do the following,
		///  1. Write a row into the MACHINE_LOGIN table (if necessary).
		///  2. Load User Security settings.
		///  3. Apply any Organizational Security (aka "BES") Login Settings (Handled at object level.)
		/// </summary>
		///<param name="databaseName">string containing the database name</param>
        ///<param name="username">string containing the user name</param>
        ///<param name="password">string containing the user's password</param>
        ///<param name="objUserLogin">object instance of the current UserLogin object</param>
        [ComVisible(false)]
        public bool AuthenticateUser(string databaseName, string username, string password,
            out UserLogin objUserLogin, int p_iClientId)
		{
            string sMessage = string.Empty;
            bool blnAuthenticated = MembershipProviderFactory.AuthenticateUser(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(p_iClientId), username, password, out sMessage, p_iClientId);

            if (blnAuthenticated)
            {
                objUserLogin = new UserLogin(username, databaseName, p_iClientId);
            } // if
            else
            {
                objUserLogin = null;
            } // else

            return blnAuthenticated;
		}//method: AuthenticateUser
        

        /// <summary>
        /// Riskmaster.Security.AuthUser wraps the native provider. It check the following
        ///  1. Permissions and Password Expiration, 
        ///  2. Permissions to login today. Raise the PermissionExpired exception if user cannot login
        ///  3. Checksum's and CRC for the License Info (Handled at object level.)
        ///  4. Check number of seat licences against number of currently logged in machines.
        ///  It do the following,
        ///  1. Write a row into the MACHINE_LOGIN table (if necessary).
        ///  2. Load User Security settings.
        ///  3. Apply any Organizational Security (aka "BES") Login Settings (Handled at object level.)
        /// </summary>
        ///<param name="username">string containing the user name</param>
        ///<param name="password">string containing the user's password</param>
        ///<param name="databaseName">string containing the database name</param>
        [ComVisible(true)]
        public object AuthenticateUser(string username, string password,
            string databaseName, int p_iClientId)
        {
            string sMessage = string.Empty;
            UserLogin objUserLogin = null;

            bool blnAuthenticated = MembershipProviderFactory.AuthenticateUser(SecurityDatabase.GetSecurityDsn(p_iClientId), username, password, out sMessage, p_iClientId);

            if (blnAuthenticated)
            {
                objUserLogin = new UserLogin(username, databaseName, p_iClientId);
            } // if
            else
            {
                objUserLogin = null;
            } // else

            return objUserLogin as object;
        }//method: AuthenticateUser
		
		

		/// <summary>
		/// Riskmaster.Security.Dispose is used to clean up File Mapping Object.
		/// </summary>
		/// <returns>None</returns>
		 new void  Dispose()
		{
			//Q.) How do we clean up our File Mapping Object?
			//A.) A File Mapping Object is held open by the OS until all views are unmapped and
			//      all map & map view handles are free'd.  This means that if every consuming process closes their 
			// handles then we are all set.  Hence we unmap our view and free our handle here in each 
			// login object to clean up after ourselves.
			if(m_hMapObject!=0)
			{
				unsafe{Functions.UnMapViewOfFile(unchecked((int)m_lpGDlgInfo));}
				Functions.CloseHandle(m_hMapObject);
				m_hMapObject =0;
				unsafe{m_lpGDlgInfo = (LoginDlgInfo*)  0;}
			}
			base.Dispose();
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Riskmaster.Security.Login is class destructor.
		/// </summary>
		~Login()
		{
			this.Dispose();
		}

	}

	/// <summary>
	/// Memory Mapped Data Structure 
	/// </summary>
	///<remarks>Data is maintained as ascii by previous login component.  Hence, we must 
	/// store as ascii instead of the default UTF16 of the CLR.  
	/// IFF this is maintained as is - it will interoperate with older compnonents using the same 
	/// memory maps.
	/// </remarks>
	
	[System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Explicit)]
	unsafe internal  struct LoginDlgInfo
	{
		//[System.Runtime.InteropServices.FieldOffset(0)] public Login* m_pObj;     // in -- Not used anymore BSB 04.22.2003
		[System.Runtime.InteropServices.FieldOffset(4)]  public  char m_szDSN;     // out
		// long lDSNID;      // Note, even though sizes are in 50's must align on 4 byte boundaries.
		[System.Runtime.InteropServices.FieldOffset(124)] public char m_szUserName; // in/out 
		[System.Runtime.InteropServices.FieldOffset(174)] public char m_szPassword; // out
		[System.Runtime.InteropServices.FieldOffset(228)] public bool m_bSecureLogin;// in  -- Not used anymore? BSB
        [System.Runtime.InteropServices.FieldOffset(10)]  public int m_iClientId;
		
		/// <summary>
		/// This internal property contain string value for DSN.
		/// </summary>
		/// <returns>It return the pointer to the buffer</returns>
		internal  string szDSN
		{
			get
			{
				fixed (void* vpBuff = &m_szDSN) return ReadBuffer((sbyte*)vpBuff,120);
			}
			set //Copy memory contents from string into my buffer.
			{
				fixed (void* pBuff = &m_szDSN)	FillBuffer(pBuff,value,120);
			}
		}		

		/// <summary>
		/// This internal property contain string value for user name.
		/// </summary>
		internal  string  szUserName
		{
			get
			{
				fixed (void* vpBuff = &m_szUserName) return ReadBuffer((sbyte*)vpBuff,50);
			}
			set
			{
				fixed (void* pBuff = &m_szUserName)	FillBuffer(pBuff,value,50);
			}
		}

		/// <summary>
		/// This internal property contain string value for password.
		/// </summary>
		internal  string  szPassword
		{
			get
			{
				fixed (void* vpBuff = &m_szPassword) return ReadBuffer((sbyte*)vpBuff,50);
			}
			set
			{
				fixed (void* pBuff = &m_szPassword)	FillBuffer(pBuff,value,50);
			}
		}

        /// <summary>
        /// This internal property which return the ClientId.
        /// </summary>
        internal int ClientId
        {
            get { return m_iClientId; }
            set { m_iClientId = value; }
        }

		/// <summary>
		/// This internal property which return the secure login and sets the secure login.
		/// </summary>
		internal bool bSecureLogin
		{
			get{return m_bSecureLogin;	}
			set{m_bSecureLogin = value;}
		}		

		/// <summary>
		/// This method sets the pointer reference to the byte data of the content. 
		/// </summary>
		/// <param name="vpBuff">Pointer to the  buffer content </param>
		/// <param name="Content">Content</param>
		/// <param name="MaxLength">Length of the content</param>
		private bool FillBuffer(void* vpBuff, string Content, int MaxLength)
		{
			if(Content ==null || Content.Length ==0) return false;

			fixed( void* vpContent = System.Text.ASCIIEncoding.ASCII.GetBytes(Content))	
			{
				IntPtr pContent = new IntPtr(vpContent);
				IntPtr pBuff = new IntPtr(vpBuff);
				Riskmaster.Common.Win32.Functions.CopyMemory( vpBuff,vpContent,Math.Min((Content.Length+1),MaxLength));
				return (pBuff.ToInt32() != 0);
			}

			}
        

		/// <summary>
		/// Returns string value at the Buffer pointer.
		/// </summary>
		/// <param name="Buff">Pointer to the buffer </param>
		/// <param name="MaxLength">Lenth of the content of buffer.</param>
		/// <returns>It returns pointer to the string</returns>
		private string ReadBuffer(sbyte* Buff,int MaxLength)
		{
			return new String(Buff);
            //return new String(Buff,0,MaxLength,System.Text.ASCIIEncoding.ASCII);
		}
	}	
	
}
