using System;
using System.Text;
using Riskmaster.Db;
using Riskmaster.Common;
using System.Collections;
using Riskmaster.ExceptionTypes;
using System.Runtime.InteropServices;
using Riskmaster.Security.Encryption;
using System.Collections.Generic;
using System.Data;

namespace Riskmaster.Security
{

    /// <summary>
    /// Riskmaster.Security.UserLogin is used to retrive the user information , it also check for the user 
    /// previlage to use the application. Pick the permissible access time span for today.
    /// </summary>
    [Serializable]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [ComVisible(true)]
    public class UserLogin
    {
        private string m_LoginName = string.Empty, m_sUserName = string.Empty, m_Password = string.Empty;
        private System.DateTime m_PrivilegesExpire = System.DateTime.MinValue;
        private System.DateTime m_PasswordExpire = System.DateTime.MinValue;
        private int m_ForceChangePwd = 0, m_UserId = 0;
        private string m_DocumentPath = string.Empty, m_CheckSum = string.Empty;
        private string m_SunStart = string.Empty, m_SunEnd = string.Empty;
        private string m_MonStart = string.Empty, m_MonEnd = string.Empty;
        private string m_TueStart = string.Empty, m_TueEnd = string.Empty;
        private string m_WedStart = string.Empty, m_WedEnd = string.Empty;
        private string m_ThuStart = string.Empty, m_ThuEnd = string.Empty;
        private string m_FriStart = string.Empty, m_FriEnd = string.Empty;
        private string m_SatStart = string.Empty, m_SatEnd = string.Empty;
        private System.DateTime m_LoginDateTime = new System.DateTime();
        private RiskmasterDatabase m_objRMDB = null;


        //private User m_objUser = new User();
        User m_objUser = null;
        //gagnihotri MITS 11995 Changes made for Audit table End

        // JP 6/15/2005    Moved out of UserLogin because this is RM specific and not needed by AC.
        //		private UserLoginLimits m_objLimits = null;
        private Hashtable m_SecHashTable = null;
        private Hashtable m_SecSuppHashTable = null;
        private int m_GroupId = 0, m_DatabaseId = 0; int m_BESGroupId = 0;//Deb: Performance changes


        private bool m_DataChanged = false, m_IsTerminalServer = false, m_IsOrgSecAdmin = false;
        /*
        Added by Tanuj on 02-Sep-2005
        Refer 'RiskmasterDatabase.cs' file for the explanation of this(m_bFindDBType) variable.
        */
        private bool m_bFindDBType = true;
        private bool m_bEnforceCheckSum = true;

        /// <summary>
        /// Private Variable for Client Id
        /// </summary>
        private int m_iClientId = 0;

      

        /// <summary>
        /// Default class constructor
        /// </summary>
        public UserLogin(int p_iClientId)
        {
            m_iClientId = p_iClientId;
            m_objUser = new User(m_iClientId);        //gagnihotri MITS 11995 Changes made for Audit table
            m_objRMDB = new RiskmasterDatabase(m_iClientId);
        }

        /// <summary>
        /// OVERLOADED: Class constructor
        /// which supports creation of Users through User ID
        /// </summary>
        /// We can't make second int parameter optional if first one is also int. This can create ambiguity.
        ///public UserLogin(int intUserId, int p_iClientId)
        public UserLogin(int intUserId, int p_iClientId)
        {
            m_iClientId = p_iClientId;
            m_objUser = new User(intUserId, m_iClientId);
            m_objRMDB = new RiskmasterDatabase(m_iClientId);
        } // constructor


        /// <summary>
        /// OVERLOADED: class constructor
        /// </summary>
        /// <param name="p_bFindDBType"></param>
        public UserLogin(bool p_bFindDBType, int p_iClientId)
        {
            m_iClientId = p_iClientId;
            m_objUser = new User(m_iClientId);        //gagnihotri MITS 11995 Changes made for Audit table
            m_bFindDBType = p_bFindDBType;
            m_objRMDB = new RiskmasterDatabase(m_iClientId);
        }

        /// <summary>
        /// OVERLOADED: class constructor
        /// </summary>
        /// <param name="p_bFindDBType"></param>
        /// <param name="p_sUserName"></param>
        public UserLogin(bool p_bFindDBType, string p_sUserName, int p_iClientId)
        {
            m_iClientId = p_iClientId;
            m_objUser = new User(p_sUserName, m_iClientId);        //gagnihotri MITS 11995 Changes made for Audit table
            m_bFindDBType = p_bFindDBType;
            m_sUserName = p_sUserName;
            m_objRMDB = new RiskmasterDatabase(m_iClientId);
        }

        /// <summary>
        /// OVERLOADED: Class constructor
        /// </summary>
        /// <param name="strUserName">string containing the user's Login Name</param>
        /// <param name="strDatabaseName">string containing the user's selected DSN</param>
        /// <remarks>Allows instantiation of a User Login object without 
        /// password information.  This is particularly required when needed to retrieve 
        /// Views etc.</remarks>
        public UserLogin(string strUserName, string strDatabaseName, int p_iClientId)
        {
            StringBuilder strSQL = new StringBuilder();
            strSQL.Append("SELECT DATA_SOURCE_TABLE.DSN, USER_DETAILS_TABLE.*, USER_TABLE.* ");
            strSQL.Append("FROM USER_DETAILS_TABLE, DATA_SOURCE_TABLE, USER_TABLE ");
            strSQL.Append("WHERE DATA_SOURCE_TABLE.DSN=");
            strSQL.Append(String.Format("'{0}' ", strDatabaseName));
            strSQL.Append("AND USER_DETAILS_TABLE.DSNID=DATA_SOURCE_TABLE.DSNID ");
            strSQL.Append("AND USER_DETAILS_TABLE.LOGIN_NAME=");
            strSQL.Append(String.Format("'{0}' ", strUserName));
            strSQL.Append("AND USER_TABLE.USER_ID=USER_DETAILS_TABLE.USER_ID");
            m_iClientId = p_iClientId;
            m_objUser = new User(m_iClientId);            
            m_objRMDB = new RiskmasterDatabase(m_iClientId);
            //Call the method to load and populate the UserLogin object
            PopulateUserLoginData(strSQL.ToString());

        } // constructor

        /// <summary>
        /// OVERLOADED: Class constructor
        /// It uses username, Password and databaseName
        /// to retrieve information from USER_DETAILS_TABLE, USER_TABLE and DATA_SOURCE_TABLE through DbReader 
        /// object, that is further passed to LoadData(). 
        /// If it is unable to retrieve the data, InvalidUserNameOrPasswordException is thrown.
        /// </summary>
        /// <param name="username">User name</param>
        /// <param name="Password">User's Password</param>
        /// <param name="databaseName">database name</param>
        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
        public UserLogin(string username, string Password, string databaseName, int p_iClientId)
        {
            StringBuilder strSQL = new StringBuilder();
            if (!string.IsNullOrEmpty(Password))
            {
                strSQL.Append("SELECT DATA_SOURCE_TABLE.DSN, USER_DETAILS_TABLE.*, USER_TABLE.* ");
                strSQL.Append("FROM USER_DETAILS_TABLE, DATA_SOURCE_TABLE, USER_TABLE ");
                strSQL.Append(String.Format("WHERE DATA_SOURCE_TABLE.DSN='{0}' ", databaseName));
                strSQL.Append("AND USER_DETAILS_TABLE.DSNID=DATA_SOURCE_TABLE.DSNID ");
                strSQL.Append(String.Format("AND USER_DETAILS_TABLE.LOGIN_NAME='{0}' ", username));
                strSQL.Append("AND USER_TABLE.USER_ID=USER_DETAILS_TABLE.USER_ID");
            } // if
            else
            {
                strSQL.Append("SELECT DATA_SOURCE_TABLE.DSN, USER_DETAILS_TABLE.*, USER_TABLE.* ");
                strSQL.Append("FROM USER_DETAILS_TABLE, DATA_SOURCE_TABLE, USER_TABLE ");
                strSQL.Append(String.Format("WHERE DATA_SOURCE_TABLE.DSN='{0}'", databaseName));
                strSQL.Append("AND USER_DETAILS_TABLE.DSNID=DATA_SOURCE_TABLE.DSNID ");
                strSQL.Append("AND USER_DETAILS_TABLE.LOGIN_NAME='" + username + "' ");
                strSQL.Append(string.Format("AND USER_DETAILS_TABLE.PASSWORD='{0}'", RMCryptography.EncryptString(Password)));
                strSQL.Append("AND USER_TABLE.USER_ID=USER_DETAILS_TABLE.USER_ID");
            }//else				

            // npadhy PopulateUserLoginData fills Data in m_objUser. Since this object is not instantiated, It was causing error inside the function.            
            m_iClientId = p_iClientId;
            m_objUser = new User(m_iClientId);
            m_objRMDB = new RiskmasterDatabase(m_iClientId);
            PopulateUserLoginData(strSQL.ToString());
        }


        /// <summary>
        /// Riskmaster.Security.UpdatePassword uses frmChgPassword object to update PASSWORD and CHECKSUM in USER_DETAILS_TABLE, for a fat client.
        /// </summary>
        /// <param name="hwnd">Window handle</param>
        /// <returns>It returns the boolean value true or false</returns>
        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
        public bool UpdatePassword(int hwnd)
        {
            bool ret = false;
            string sHash = string.Empty, sPwd = string.Empty;

            frmChgPassword frmChgPwd = new frmChgPassword(this.Password, this.objRiskmasterDatabase.DataSourceName, this.LoginName, m_iClientId);

            //Get and validate a new Password from the user. (Fat client only!)
            if (hwnd == 0)
                hwnd = Common.Win32.Functions.GetDesktopWindow();

            if (frmChgPwd.ShowDialog(new Common.Win32.hWnd(hwnd)) == System.Windows.Forms.DialogResult.OK)
            {
                sPwd = frmChgPwd.NewPassword;
                this.Password = sPwd;
            }
            else
                return false;

            //Store the New Password if we get this far.
            DbConnection db = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
            Db.DbWriter writer = null;

            //Re-Hash the new security record info.
            sHash = this.GetCheckSum();
            try
            {

                db.Open();
                DbReader reader = db.ExecuteReader("SELECT PASSWORD, CHECKSUM FROM USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.LOGIN_NAME='" + this.LoginName + "' AND USER_DETAILS_TABLE.DSNID=" + this.DatabaseId);
                writer = DbFactory.GetDbWriter(reader, true);
                reader.Close();

                writer.Fields["PASSWORD"].Value = RMCryptography.EncryptString(sPwd);
                writer.Fields["CHECKSUM"].Value = sHash;
                writer.Execute();
                ret = true;
            }
            finally { db.Close(); }
            return ret;
        }

        /// <summary>
        /// Populates required User Data for authentication calls
        /// </summary>
        /// <param name="strSQL">string containing the SQL statement to be executed</param>
        internal void PopulateUserLoginData(string strSQL)
        {
            //START srajindersin MITS 30547 dt 12/04/2012
            DataSet dsUser = new DataSet();
            dsUser = DbFactory.GetDataSet(SecurityDatabase.GetSecurityDsn(m_iClientId), strSQL, m_iClientId);
            if (dsUser.Tables.Count > 0)
            {
                for (int i = 0; i < dsUser.Tables[0].Rows.Count; i++)
                {
                    bool blnSuccess = false;
                    m_UserId = Conversion.CastToType<int>(dsUser.Tables[0].Rows[0]["USER_ID"].ToString(), out blnSuccess);
                    m_LoginName = dsUser.Tables[0].Rows[0]["LOGIN_NAME"].ToString();
                    m_Password = RMCryptography.DecryptString(dsUser.Tables[0].Rows[0]["PASSWORD"].ToString());
                    m_DocumentPath = dsUser.Tables[0].Rows[0]["DOC_PATH"].ToString();

                    m_DatabaseId = Conversion.CastToType<int>(dsUser.Tables[0].Rows[0]["DSNID"].ToString(), out blnSuccess);
                    m_CheckSum = dsUser.Tables[0].Rows[0]["CHECKSUM"].ToString();

                    m_ForceChangePwd = Conversion.CastToType<int>(dsUser.Tables[0].Rows[0]["FORCE_CHANGE_PWD"].ToString(), out blnSuccess);
                    m_PrivilegesExpire = Conversion.ToDate(dsUser.Tables[0].Rows[0]["PRIVS_EXPIRE"].ToString());
                    m_PasswordExpire = Conversion.ToDate(dsUser.Tables[0].Rows[0]["PASSWD_EXPIRE"].ToString());
                    m_SunStart = dsUser.Tables[0].Rows[0]["SUN_START"].ToString();
                    m_SunEnd = dsUser.Tables[0].Rows[0]["SUN_END"].ToString();
                    m_MonStart = dsUser.Tables[0].Rows[0]["MON_START"].ToString();
                    m_MonEnd = dsUser.Tables[0].Rows[0]["MON_END"].ToString();
                    m_TueStart = dsUser.Tables[0].Rows[0]["TUES_START"].ToString();
                    m_TueEnd = dsUser.Tables[0].Rows[0]["TUES_END"].ToString();
                    m_WedStart = dsUser.Tables[0].Rows[0]["WEDS_START"].ToString();
                    m_WedEnd = dsUser.Tables[0].Rows[0]["WEDS_END"].ToString();
                    m_ThuStart = dsUser.Tables[0].Rows[0]["THURS_START"].ToString();
                    m_ThuEnd = dsUser.Tables[0].Rows[0]["THURS_END"].ToString();
                    m_FriStart = dsUser.Tables[0].Rows[0]["FRI_START"].ToString();
                    m_FriEnd = dsUser.Tables[0].Rows[0]["FRI_END"].ToString();
                    m_SatStart = dsUser.Tables[0].Rows[0]["SAT_START"].ToString();
                    m_SatEnd = dsUser.Tables[0].Rows[0]["SAT_END"].ToString();


                    //Get Database Specific Properties via sub-object.
                    // m_objRMDB.Load(m_DatabaseId);
                    m_objRMDB = new RiskmasterDatabase(m_DatabaseId, m_iClientId);
                    //Get User info populated
                    // m_objUser.Load(m_UserId);
                    m_objUser = new User(m_UserId, m_iClientId);
                    //Cache Security Settings for "bIsAllowed() function."
                    m_SecHashTable = PopulateSecurityHashTable();
                    //Populate supplemental hashtable
                    m_SecSuppHashTable = PopulateSuppHashTable();
                }
            }

            ////Execute and create a standard DataReader
            //using (DbReader reader = DbFactory.ExecuteReader(SecurityDatabase.GetSecurityDsn(0), strSQL))
            //{
            //    //Loop through the DataReader
            //    while (reader.Read())
            //    {
            //        bool blnSuccess = false;
            //        m_UserId = Conversion.CastToType<int>(reader["USER_ID"].ToString(), out blnSuccess);
            //        m_LoginName = reader["LOGIN_NAME"].ToString();
            //        m_Password = RMCryptography.DecryptString(reader["PASSWORD"].ToString());
            //        m_DocumentPath = reader["DOC_PATH"].ToString();

            //        m_DatabaseId = Conversion.CastToType<int>(reader["DSNID"].ToString(), out blnSuccess);
            //        m_CheckSum = reader["CHECKSUM"].ToString();

            //        m_ForceChangePwd = Conversion.CastToType<int>(reader["FORCE_CHANGE_PWD"].ToString(), out blnSuccess);
            //        m_PrivilegesExpire = Conversion.ToDate(reader["PRIVS_EXPIRE"].ToString());
            //        m_PasswordExpire = Conversion.ToDate(reader["PASSWD_EXPIRE"].ToString());
            //        m_SunStart = reader["SUN_START"].ToString();
            //        m_SunEnd = reader["SUN_END"].ToString();
            //        m_MonStart = reader["MON_START"].ToString();
            //        m_MonEnd = reader["MON_END"].ToString();
            //        m_TueStart = reader["TUES_START"].ToString();
            //        m_TueEnd = reader["TUES_END"].ToString();
            //        m_WedStart = reader["WEDS_START"].ToString();
            //        m_WedEnd = reader["WEDS_END"].ToString();
            //        m_ThuStart = reader["THURS_START"].ToString();
            //        m_ThuEnd = reader["THURS_END"].ToString();
            //        m_FriStart = reader["FRI_START"].ToString();
            //        m_FriEnd = reader["FRI_END"].ToString();
            //        m_SatStart = reader["SAT_START"].ToString();
            //        m_SatEnd = reader["SAT_END"].ToString();


            //        //Get Database Specific Properties via sub-object.
            //        m_objRMDB.Load(m_DatabaseId);
            //        //Get User info populated
            //        m_objUser.Load(m_UserId);
            //        //Cache Security Settings for "bIsAllowed() function."
            //        m_SecHashTable = PopulateSecurityHashTable();
            //        //Populate supplemental hashtable
            //        m_SecSuppHashTable = PopulateSuppHashTable();
            //    } // while

            //} // using
            //END srajindersin MITS 30547 dt 12/04/2012

            #region BES--Parijat
            //Get BES Properties
            if (m_objRMDB.OrgSecFlag)
            {
                DbConnection db = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
                DbReader rdrBES = null;
                try
                {
                    string sDataSrcRMUserId;
                    db.Open();
                    rdrBES = db.ExecuteReader(String.Format("SELECT * FROM USER_ORGSEC WHERE USER_ID={0} AND DSN_ID={1}", m_UserId, m_DatabaseId));
                    if (!rdrBES.Read())
                        //// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                        throw new Riskmaster.ExceptionTypes.OrgSecAccessViolationException(Globalization.GetString("UserLogin.LoadData.BusEntitySecurityEnabled", m_iClientId));


                    if (string.Compare(RMCryptography.DecryptString(rdrBES.GetString("DB_UID")), "DBO", true) == 0)
                        sDataSrcRMUserId = this.objRiskmasterDatabase.RMUserId;// Is admin DBO (No UID swap, user can see all data.)
                    else //replace userid\pwd with BES userid\password.
                    {
                        sDataSrcRMUserId = this.objRiskmasterDatabase.RMUserId;

                        //BSB\Xilong Fix for BES login's (sg0xx) never applied to connection string.
                        string sBESUid = RMCryptography.DecryptString(rdrBES.GetString("DB_UID"));
                        string sBESPwd = RMCryptography.DecryptString(rdrBES.GetString("DB_PWD"));
                        string sConn = this.objRiskmasterDatabase.ConnectionString;

                        sConn = sConn.Replace("UID=" + objRiskmasterDatabase.RMUserId + ";", "UID=" + sBESUid + ";");
                        //In case the password is blank. MITS 7480 Xilong Hu
                        if (string.IsNullOrEmpty(objRiskmasterDatabase.RMPassword))
                        {
                            sConn = sConn.Replace("PWD=;", String.Format("PWD={0};", sBESPwd));
                        } // if
                        else
                        {
                            sConn = sConn.Replace(String.Format("PWD={0};", objRiskmasterDatabase.RMPassword), String.Format("PWD={0};", sBESPwd));
                        } // else

                        this.objRiskmasterDatabase.ConnectionString = sConn;
                        this.BESGroupId = rdrBES.GetInt("GROUP_ID");//Deb: Performance changes
                        this.objRiskmasterDatabase.RMUserId = sBESUid;
                        this.objRiskmasterDatabase.RMPassword = sBESPwd;
                    }
                    //If data source and user ids match, we are a orgsec admin.
                    m_IsOrgSecAdmin = (sDataSrcRMUserId == m_objRMDB.RMUserId);//rdrBES.GetString("RM_USERID"));
                }
                finally
                {
                    rdrBES.Close();
                    db.Close();
                }
            }


            #endregion
        }//method: PopulateUserLoginData()

        /// <summary>
        /// Riskmaster.Security.LoadData populates the attributes for the UserLogin class, from the input 
        /// DbReader object.If OrgSec flag for the Riskmaster database instance is true, the function fetches details
        /// from USER_ORGSEC table for the particular user to determine if (s)he is an orgsec admin.
        /// Sets the flag m_DataChanged to false.ChecksumMatches() function is called to validate user information
        /// </summary>
        /// <param name="reader">The native reader to wrap</param>
        protected virtual void LoadData(DbReader reader)
        {
            DbConnection db = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
            DbReader rdrBES = null;
            m_UserId = reader.GetInt("USER_ID");
            m_LoginName = reader.GetString("LOGIN_NAME");
            m_Password = RMCryptography.DecryptString(reader.GetString("PASSWORD"));
            m_DocumentPath = reader.GetString("DOC_PATH");
            m_DatabaseId = reader.GetInt("DSNID");
            m_CheckSum = reader.GetString("CHECKSUM");

            m_ForceChangePwd = reader.GetInt("FORCE_CHANGE_PWD");
            m_PrivilegesExpire = Conversion.ToDate(reader.GetString("PRIVS_EXPIRE"));
            m_PasswordExpire = Conversion.ToDate(reader.GetString("PASSWD_EXPIRE"));
            m_SunStart = reader.GetString("SUN_START");
            m_SunEnd = reader.GetString("SUN_END");
            m_MonStart = reader.GetString("MON_START");
            m_MonEnd = reader.GetString("MON_END");
            m_TueStart = reader.GetString("TUES_START");
            m_TueEnd = reader.GetString("TUES_END");
            m_WedStart = reader.GetString("WEDS_START");
            m_WedEnd = reader.GetString("WEDS_END");
            m_ThuStart = reader.GetString("THURS_START");
            m_ThuEnd = reader.GetString("THURS_END");
            m_FriStart = reader.GetString("FRI_START");
            m_FriEnd = reader.GetString("FRI_END");
            m_SatStart = reader.GetString("SAT_START");
            m_SatEnd = reader.GetString("SAT_END");

            //Get Database Specific Properties via sub-object.
            m_objRMDB.Load(m_DatabaseId);

            //Get User Specific Properties via sub-object.
            m_objUser.Load(m_UserId);

            //Get any Non-Database Properties
            m_LoginDateTime = System.DateTime.Now;
            //			m_DBMake= 0;
            //Cache Security Settings for "bIsAllowed() function."
            m_SecHashTable = PopulateSecurityHashTable();

            //Populate supplemental hashtable
            m_SecSuppHashTable = PopulateSuppHashTable();

            //Get BES Properties
            if (m_objRMDB.OrgSecFlag)
            {
                try
                {
                    string sDataSrcRMUserId;
                    db.Open();
                    rdrBES = db.ExecuteReader(String.Format("SELECT * FROM USER_ORGSEC WHERE USER_ID={0} AND DSN_ID={1}", m_UserId, m_DatabaseId));
                    if (!rdrBES.Read())
                        //// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                        throw new Riskmaster.ExceptionTypes.OrgSecAccessViolationException(Globalization.GetString("UserLogin.LoadData.BusEntitySecurityEnabled", m_iClientId));


                    if (string.Compare(RMCryptography.DecryptString(rdrBES.GetString("DB_UID")), "DBO", true) == 0)
                        sDataSrcRMUserId = this.objRiskmasterDatabase.RMUserId;// Is admin DBO (No UID swap, user can see all data.)
                    else //replace userid\pwd with BES userid\password.
                    {
                        sDataSrcRMUserId = this.objRiskmasterDatabase.RMUserId;

                        //BSB\Xilong Fix for BES login's (sg0xx) never applied to connection string.
                        string sBESUid = RMCryptography.DecryptString(rdrBES.GetString("DB_UID"));
                        string sBESPwd = RMCryptography.DecryptString(rdrBES.GetString("DB_PWD"));
                        string sConn = this.objRiskmasterDatabase.ConnectionString;

                        sConn = sConn.Replace("UID=" + objRiskmasterDatabase.RMUserId + ";", "UID=" + sBESUid + ";");
                        //In case the password is blank. MITS 7480 Xilong Hu
                        if (objRiskmasterDatabase.RMPassword == string.Empty)
                            sConn = sConn.Replace("PWD=;", String.Format("PWD={0};", sBESPwd));
                        else
                            sConn = sConn.Replace(String.Format("PWD={0};", objRiskmasterDatabase.RMPassword), String.Format("PWD={0};", sBESPwd));

                        this.objRiskmasterDatabase.ConnectionString = sConn;

                        this.objRiskmasterDatabase.RMUserId = sBESUid;
                        this.objRiskmasterDatabase.RMPassword = sBESPwd;
                    }
                    //If data source and user ids match, we are a orgsec admin.
                    m_IsOrgSecAdmin = (sDataSrcRMUserId == m_objRMDB.RMUserId);//rdrBES.GetString("RM_USERID"));
                }
                finally
                {
                    rdrBES.Close();
                    db.Close();
                }
            }
            //BSB 04.24.2005 Moved UserLimits loading\checking logic here for access from 
            // FDM equivalent.
            // JP 6/15/2005    Moved out of UserLogin because this is RM specific and not needed by AC. UserLoginLimits now created in LoginAdaptor or standalone if needed.
            //			LoadUserLimits();

            m_DataChanged = false;
            if (m_bEnforceCheckSum)
            {
                if (!ChecksumMatches())
                    //// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                    throw new Riskmaster.ExceptionTypes.InvalidChecksumException(Globalization.GetString("UserLogin.LoadData.InvalidSecurityLoginInfo", m_iClientId));
            }
        }//method: LoadData()

        /// <summary>
        /// Retrieves the list of all currently available RISKMASTER Databases
        /// </summary>
        /// <returns>string array containing names of all current databases</returns>
        private string[] GetRiskmasterUserDatabases()
        {
            List<string> dbList = new List<string>();

            using (DbReader reader = DbFactory.ExecuteReader(SecurityDatabase.GetSecurityDsn(m_iClientId), String.Format("SELECT DATA_SOURCE_TABLE.DSN FROM USER_DETAILS_TABLE, DATA_SOURCE_TABLE WHERE USER_DETAILS_TABLE.DSNID=DATA_SOURCE_TABLE.DSNID AND USER_DETAILS_TABLE.LOGIN_NAME='{0}'  ORDER BY DATA_SOURCE_TABLE.DSN", m_objUser.Name)))
            {
                while (reader.Read())
                {
                    dbList.Add(reader.GetString(0));
                }
            }//using

            return dbList.ToArray();
        }//method: GetRiskmasterDatabases()

        /*
        // BSB 10.24.2003
        // We don't want every client script kitty to be able
        // to use this interface and change security table rows...
        */
        /// <summary>
        /// Riskmaster.Security.Save gets a DbWriter object using DbFactory instance and invokes SaveData().
        /// </summary>
        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
        public void Save()
        {
            DbWriter writer = DbFactory.GetDbWriter(SecurityDatabase.GetSecurityDsn(m_iClientId));
            writer.Connection = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
            writer.Connection.Open();
            SaveData(writer);
            writer.Connection.Close();
        }

        /// <summary>
        /// Riskmaster.Security.SaveData check for the property DataChanged is true, this function updates the 
        /// attributes of the class to USER_DETAILS_TABLE using writer object. 
        /// It sets the DbReader object to determine whether the record exists in the table or not. 
        /// It also invokes sub-object m_objUserís function SaveData()
        /// </summary>
        /// <param name="writer">The native writer to wrap</param>
        [System.Security.Permissions.StrongNameIdentityPermission(System.Security.Permissions.SecurityAction.LinkDemand, PublicKey = UtilityFunctions.CODE_ACCESS_PUBLICKEY)]
        public virtual void SaveData(DbWriter writer)
        {

            if (m_UserId == 0) //Must Create subobjects first....
                //// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                throw new Riskmaster.ExceptionTypes.ReferentialInegrityException(Globalization.GetString("UserLogin.SaveData.ReqUserExit", m_iClientId));
            if (m_DatabaseId == 0) //Must Create subobjects first....
                //// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                throw new Riskmaster.ExceptionTypes.ReferentialInegrityException(Globalization.GetString("UserLogin.SaveData.ReqDbExit", m_iClientId));


            if (!DataChanged)
                return;

            //Save Sub-Objects
            //			m_objRMDB.SaveData(writer); TODO
            m_objUser.SaveData(writer);

            //Make Sure Writer is Clean for our Use - Empty the field, table collections etc.
            writer.Reset(true);

            //Proceed with Top Level Save

            // Pick your Storage Table
            writer.Tables.Add("USER_DETAILS_TABLE");
            DbReader reader = null;
            try
            {
                //Get the next unique ID
                reader = writer.Connection.ExecuteReader(String.Format("SELECT USER_DETAILS_TABLE.USER_ID FROM USER_DETAILS_TABLE WHERE USER_ID={0} AND DSNID={1}", m_UserId, m_DatabaseId));
                if (reader.Read())  // Existing Record, add filter for the update query.
                    writer.Where.Add(String.Format(" USER_ID={0} AND DSNID={1}", m_UserId, m_DatabaseId));
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }

            writer.Fields.Add("DSNID", m_DatabaseId);
            writer.Fields.Add("USER_ID", m_UserId);
            writer.Fields.Add("LOGIN_NAME", m_LoginName);
            writer.Fields.Add("PASSWORD", RMCryptography.EncryptString(m_Password));
            writer.Fields.Add("DOC_PATH", m_DocumentPath);
            writer.Fields.Add("FORCE_CHANGE_PWD", m_ForceChangePwd);
            writer.Fields.Add("PRIVS_EXPIRE", Conversion.ToDbDate(m_PrivilegesExpire));
            /*Changed by Tanuj on 25-Aug-2005
             * Following line of code will insert null if Conversion.ToDbDateTime(m_PasswordExpire) returns "00000000000000"
             * */
            writer.Fields.Add("PASSWD_EXPIRE", Conversion.ToDbDateTime(m_PasswordExpire) == "00000000000000" ? null : Conversion.ToDbDateTime(m_PasswordExpire));
            writer.Fields.Add("SUN_START", m_SunStart);
            writer.Fields.Add("SUN_END", m_SunEnd);
            writer.Fields.Add("MON_START", m_MonStart);
            writer.Fields.Add("MON_END", m_MonEnd);
            writer.Fields.Add("TUES_START", m_TueStart);
            writer.Fields.Add("TUES_END", m_TueEnd);
            writer.Fields.Add("WEDS_START", m_WedStart);
            writer.Fields.Add("WEDS_END", m_WedEnd);
            writer.Fields.Add("THURS_START", m_ThuStart);
            writer.Fields.Add("THURS_END", m_ThuEnd);
            writer.Fields.Add("FRI_START", m_FriStart);
            writer.Fields.Add("FRI_END", m_FriEnd);
            writer.Fields.Add("SAT_START", m_SatStart);
            writer.Fields.Add("SAT_END", m_SatEnd);
            writer.Fields.Add("CHECKSUM", GetCheckSum());
            //gagnihotri MITS 11995 Changes made for Audit table
            writer.Fields.Add("UPDATED_BY_USER", m_sUserName);
            writer.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
            writer.Execute();
            m_DataChanged = false;
        }

        /// <summary>
        /// Riskmaster.Security.PopulateSecurityHashTable returns Hashtable object for group permissions associated with the user
        /// </summary>
        /// <returns>It returns the hash table object for group permission</returns>
        private Hashtable PopulateSecurityHashTable()
        {
            DbConnection db = DbFactory.GetDbConnection(m_objRMDB.ConnectionString);
            DbReader rdr = null;
            Hashtable ret = new Hashtable();
            string sSQL = String.Format("SELECT GROUP_PERMISSIONS.FUNC_ID,USER_MEMBERSHIP.GROUP_ID FROM USER_MEMBERSHIP, GROUP_PERMISSIONS  WHERE USER_MEMBERSHIP.USER_ID ={0} AND USER_MEMBERSHIP.GROUP_ID = GROUP_PERMISSIONS.GROUP_ID", m_UserId);
            try
            {
                db.Open();
                rdr = db.ExecuteReader(sSQL);
                if (rdr.Read())
                {
                    m_GroupId = rdr.GetInt("GROUP_ID");
                    do
                    {
                        try { ret.Add(rdr.GetInt("FUNC_ID"), true); }
                        catch (Exception) { ;}
                    }
                    while (rdr.Read());
                }
            }
            catch (Exception e)
            {
                //// To retrieve message from language specific resource file,Modified by Abhimanyu Gupta on 20/5/2004 
                //Replaced general exception with SecuritySettingException.Added by Tanuj Narula on 27/5/2004
                throw new SecuritySettingException(Globalization.GetString("UserLogin.PopulateSecurityHashTable.CannotGetSecSettings", m_iClientId), e);
            }
            finally
            {
                if (rdr != null)
                    rdr.Close();
                db.Close();
            }
            return ret;
        }
        ///Geeta Sharma change done for mits 10729
        /// <summary>
        /// Riskmaster.Security.PopulateSuppHashTable returns Hashtable object for Supplemental Fields
        /// </summary>
        /// <returns>It returns the hash table object for Supplementals</returns>
        private Hashtable PopulateSuppHashTable()
        {
            DbReader objReader = null;
            DbConnection objSecurityConn = null;
            Hashtable htSuppFields = new Hashtable();

            objSecurityConn = DbFactory.GetDbConnection(SecurityDatabase.GetSecurityDsn(m_iClientId));
            try
            {
                objSecurityConn.Open();
                objReader = objSecurityConn.ExecuteReader("SELECT FUNC_ID FROM FUNCTION_LIST WHERE FUNCTION_NAME = 'Supplemental Fields'");

                while (objReader.Read())
                {
                    htSuppFields.Add(objReader.GetInt("FUNC_ID"), true);
                }

                return htSuppFields;
            }
            catch (Exception e)
            {
                throw new SecuritySettingException(Globalization.GetString("UserLogin.PopulateSecurityHashTable.CannotGetSecSettings", m_iClientId), e);
            }
            finally
            {
                if (objReader != null)
                    objReader.Close();
                objSecurityConn.Close();
            }

        }


        /// <summary>
        /// Riskmaster.Security.DataChanged is the read only property . It returns the value form_DataChanged || m_objRMDB.m_DataChanged || m_objUser.m_DataChanged
        /// </summary>
        public bool DataChanged
        {
            get { return (m_DataChanged || m_objRMDB.m_DataChanged || m_objUser.m_DataChanged); }
        }

        /// <summary>
        /// Riskmaster.Security.IsAllowed is an absolutely basic check
        /// whether or not the security function list for this login contains
        /// a particular function id.
        /// </summary>
        /// <param name="FunctionId">Function ID</param>
        /// <returns>It returns value returned by ContainsKey()</returns>
        public bool IsAllowed(int FunctionId)
        {
            return m_SecHashTable.ContainsKey(FunctionId);
        }

        public bool IsAllowedEx(int secId) { return IsAllowedEx(secId, 0); }
        /// <summary>
        /// Riskmaster.Security.IsAllowedEx is a more sophisticated check
        /// whether or not the user "should" according to application configuration settings
        /// AND the current user's function list have access to the requested function.
        /// </summary>
        /// <param name="FunctionId">Function ID</param>
        /// <returns>It returns true if the user should be allowed to perform the requested function.</returns>

        public bool IsAllowedEx(int secBase, int secOffset)
        {
            int RMO_ACCESS = 0;

            //If security is turned off for this DSN, always return TRUE
            if (!this.objRiskmasterDatabase.Status || secBase == 0)
                return true;

            //MGaba2: MITS 22185: Commenting below code as Admin Tracking settings were not working:Start
            //Below code seems to be copy pasted from rmworld(Rmsec.bas). 
            //Probably long time back,rmworld clients were using single administrative tracking security setting which has func_id=13000
            //But now, func_id=13000 has been used for "Vendor"

            //		  JP 2/20/2003    admin tracking tables broken out in this range
            //        First, check and see if the old AT permissions (13,000+) are still in use. If so, apply those as an override to the individual permissions.
            //        This is done because this feature is being rolled out midstream and not all clients will be using individual AT security.
            //        If they're using the new individual security permissions, the old ones (13,000+) won't be there and the first bIsAllowed will always fail, deferring to the new permissions.
            //        At some point in the post-6.0 future, if the old permissions are officially gone, this code can be removed.

            //if (secBase >= 5000000 && secBase <= 6000000) //Admin Tracking Tables
            //{
            //    if (IsAllowed(13000 + secOffset))
            //        return true;
            //    else  //Now, check new individual permissions                 
            //    {
            //        if (!IsAllowed(secBase + RMO_ACCESS))
            //            return false;
            //        else // check form security here                           
            //            return (IsAllowed(secBase + secOffset));
            //    }
            //}
            //else 
            //MGaba2: MITS 22185:End
            if (secBase == 62500)
            {//TR :-001565: for Non Occ payments
                return (IsAllowed(secBase + RMO_ACCESS));
            }
            else  // All other tables
            {
                if (!IsAllowed(secBase + RMO_ACCESS))
                    return false;
                else // check form security here                           
                    return (IsAllowed(secBase + secOffset));
            }
        }

        /// function added by Geeta Sharma for mits 10729
        public bool IsAllowedSuppEx(int secBase, int secOffset)
        {
            int RMO_ACCESS = 0;

            //If security is turned off for this DSN, always return TRUE
            if (!this.objRiskmasterDatabase.Status || secBase == 0)
                return true;

            bool bSuppFlag = CheckForSuppField(secBase + RMO_ACCESS);

            if (!bSuppFlag)
            {
                return true;
            }
            else
            {
                if (!IsAllowed(secBase + RMO_ACCESS))
                    return false;
                else // check form security here                           
                    return (IsAllowed(secBase + secOffset));
            }
        }

        private bool CheckForSuppField(int FunctionId)
        {
            return m_SecSuppHashTable.ContainsKey(FunctionId);
        }


        /// <summary>
        /// Riskmaster.Security.IsTimePermittedForLogin returns whether the current time lies in the permissible 
        /// time span for the day, for login.
        /// </summary>
        /// <returns>It returns the boolean value true or false</returns>
        public bool IsTimePermittedForLogin()
        {
            System.DateTime CurDateTime = System.DateTime.Now;
            string sStart = "000000";
            string sEnd = "240000";
            string sCur = Conversion.ToDbDateTime(CurDateTime).Substring(8);
            //Pick the permissible access time span for today.
            switch ((int)CurDateTime.DayOfWeek)
            {
                case 0:
                    sStart = m_SunStart;
                    sEnd = m_SunEnd;
                    break;
                case 1:
                    sStart = m_MonStart;
                    sEnd = m_MonEnd;
                    break;
                case 2:
                    sStart = m_TueStart;
                    sEnd = m_TueEnd;
                    break;
                case 3:
                    sStart = m_WedStart;
                    sEnd = m_WedEnd;
                    break;
                case 4:
                    sStart = m_ThuStart;
                    sEnd = m_ThuEnd;
                    break;
                case 5:
                    sStart = m_FriStart;
                    sEnd = m_FriEnd;
                    break;
                case 6:
                    sStart = m_SatStart;
                    sEnd = m_SatEnd;
                    break;
            }
            /*SMS database stores 'null' if user is not allowed for a particular day and 
             * corresponding module level variables(m_ThuStart etc) initializes to blank("") in case of 'null' in SMS DB, 
             * so removing that condition from following 'if' loops. -Tanuj on 5-Jan-2006  
             * */
            if (sStart == "0" || sStart == null)
                sStart = "000000";
            if (sEnd == "0" || sEnd == null || sEnd == "000000")
                sEnd = "240000";

            //Run the test...
            if (sCur.CompareTo(sStart) > 0 && sCur.CompareTo(sEnd) < 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Gets the CheckSum for the object
        /// </summary>
        /// <returns>string containing the CheckSum for the object</returns>
        internal string GetCheckSum()
        {
            byte[] byteArr = null;
            byte[] tmpArr = null;
            int cBytes;
            string sStringBuff = "";
            ushort iHI, iLO;
            DTGCrypt32 objCrypt = new DTGCrypt32();

            // Fill er up
            sStringBuff = m_LoginName + Conversion.ToDbDate(m_PrivilegesExpire);

            if (Conversion.ToDbDate(m_PrivilegesExpire).Length == 0)
                sStringBuff += "";
            else
                sStringBuff += "000000";

            sStringBuff = sStringBuff +
                m_Password +
                Conversion.ToDbDate(m_PrivilegesExpire) +
                m_SunStart +
                m_SunEnd +
                m_MonStart +
                m_MonEnd +
                m_TueStart +
                m_TueEnd +
                m_WedStart +
                m_WedEnd +
                m_ThuStart +
                m_ThuEnd +
                m_FriStart +
                m_FriEnd +
                m_SatStart +
                m_SatEnd;
            tmpArr = Conversion.GetByteArr(sStringBuff);
            cBytes = tmpArr.Length;
            byteArr = new byte[tmpArr.Length + 8];
            tmpArr.CopyTo(byteArr, 0);

            Conversion.GetHILOWord(m_DatabaseId, out iLO, out iHI);
            Conversion.GetHILOByte(iLO, out byteArr[cBytes], out byteArr[cBytes + 1]);
            Conversion.GetHILOByte(iHI, out byteArr[cBytes + 2], out byteArr[cBytes + 3]);

            Conversion.GetHILOWord(m_UserId, out iLO, out iHI);
            Conversion.GetHILOByte(iLO, out byteArr[cBytes + 4], out byteArr[cBytes + 5]);
            Conversion.GetHILOByte(iHI, out byteArr[cBytes + 6], out byteArr[cBytes + 7]);

            return RMCryptography.ComputeHashAsString(byteArr);
        }

        /// <summary>
        /// Riskmaster.Security.ChecksumMatches internal method compares the stored checksum attribute that with a newly computed hash value for the user attributes
        /// </summary>
        /// <returns>It returns the computed hash value.</returns>
        internal bool ChecksumMatches()
        {
            string sHash = "";
            sHash = GetCheckSum();
            return (sHash == m_CheckSum);
        }
        /// <summary>
        /// This function returns the User Id and group Id of currently logged in User
        /// </summary>
        /// <param name="strUserName">login name</param>
        /// <param name="strDatabaseName">Database name</param>
        /// <param name="iGroupId">groupid</param>
        /// <returns></returns>
        public int GetUserIdAndGroupId(string strUserName, string strDatabaseName, out int iGroupId)
        {
            StringBuilder strSQL = new StringBuilder();
            int iUserId = 0;
            bool blnSuccess = false;
            string sConnectionString = string.Empty;
            string sDataSourceName = string.Empty;
            string sRMUserId = string.Empty;
            string sRMPassword = string.Empty;

            iGroupId = 0;
            strSQL.Append("SELECT USER_DETAILS_TABLE.USER_ID,DATA_SOURCE_TABLE.CONNECTION_STRING,DATA_SOURCE_TABLE.DSN,DATA_SOURCE_TABLE.RM_USERID,DATA_SOURCE_TABLE.RM_PASSWORD  ");
            strSQL.Append("FROM USER_DETAILS_TABLE, DATA_SOURCE_TABLE, USER_TABLE ");
            strSQL.Append("WHERE DATA_SOURCE_TABLE.DSN=");
            strSQL.Append(String.Format("'{0}' ", strDatabaseName));
            strSQL.Append("AND USER_DETAILS_TABLE.DSNID=DATA_SOURCE_TABLE.DSNID ");
            strSQL.Append("AND USER_DETAILS_TABLE.LOGIN_NAME=");
            strSQL.Append(String.Format("'{0}' ", strUserName));
            strSQL.Append("AND USER_TABLE.USER_ID=USER_DETAILS_TABLE.USER_ID");

            using (DbReader reader = DbFactory.ExecuteReader(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId), strSQL.ToString()))
            {
                while (reader.Read())
                {

                    iUserId = Conversion.CastToType<int>(reader["USER_ID"].ToString(), out blnSuccess);
                    sConnectionString = reader.GetString("CONNECTION_STRING");
                    sDataSourceName = reader.GetString("DSN");
                    sRMUserId = RMCryptography.DecryptString(reader.GetString("RM_USERID"));
                    sRMPassword = RMCryptography.DecryptString(reader.GetString("RM_PASSWORD"));

                }
            }
            //Set up a DSN-based connection string if the string is empty
            if (string.IsNullOrEmpty(sConnectionString))
            {
                sConnectionString = UtilityFunctions.BuildDSNConnectionString(sDataSourceName, sRMUserId, sRMPassword);
            }//if
            //create a DSN-less based connection string if the string only currently contains server & database information
            else
            {
                List<string> arrConnStrParams = new List<string>();

                arrConnStrParams.Add(sConnectionString);
                arrConnStrParams.Add(String.Format("{0}{1};", DSNRegistryKeys.UID, sRMUserId));
                arrConnStrParams.Add(String.Format("{0}{1};", DSNRegistryKeys.PWD, sRMPassword));

                //Use a standardized function to build the connection string
                //TODO: Create a standard helper function for these common connection string builder operations using the .Net ConnectionStringBuilder library
                sConnectionString = UtilityFunctions.BuildConnectionString(arrConnStrParams);
            }//else

            strSQL.Remove(0, strSQL.Length);
            strSQL.Append("SELECT USER_MEMBERSHIP.GROUP_ID ");
            strSQL.Append("FROM USER_MEMBERSHIP ");
            strSQL.AppendFormat("WHERE USER_MEMBERSHIP.USER_ID=");
            strSQL.Append(String.Format("{0} ", iUserId));

            using (DbReader reader = DbFactory.ExecuteReader(sConnectionString, strSQL.ToString()))
            {
                while (reader.Read())
                {

                    iGroupId = reader.GetInt("GROUP_ID");

                }
            }

            return iUserId;
            //smishra25:End by Shivendu for performance updates
        }
        // ******************************************************************************
        // UserLogin object properties (Not wrapped from anywhere.)
        // ******************************************************************************		

        public int UserId
        {
            get { return m_UserId; }
            set
            {
                m_objUser = new User(value, m_bEnforceCheckSum, m_iClientId); // Throws error if user doesn't exist.
                m_UserId = value;
                m_DataChanged = true;
            }
        }

        public int DatabaseId
        {
            get { return m_DatabaseId; }
            set
            {
                m_objRMDB = new RiskmasterDatabase(value, m_bFindDBType, m_bEnforceCheckSum, m_iClientId); //Throws error if RMDB doesn't exist.
                m_DatabaseId = value;
                m_DataChanged = true;
            }
        }
        //Deb: Performance changes
        public int BESGroupId
        {
            get { return m_BESGroupId; }
            set { m_BESGroupId = value; }
        }
        //Deb: Performance changes
        public int GroupId
        {
            get { return m_GroupId; }
        }

        public bool IsTerminalServer
        {
            get { return m_IsTerminalServer; }
        }

        public bool IsOrgSecAdmin
        {
            get { return m_IsOrgSecAdmin; }
        }

        public string LoginName
        {
            get { return m_LoginName; }
            set
            {
                m_LoginName = value;
                m_DataChanged = true;
            }
        }

        public string DocumentPath
        {
            get { return m_DocumentPath; }
            set
            {
                m_DocumentPath = value;
                m_DataChanged = true;
            }
        }

        public string Password
        {
            get { return m_Password; }
            set
            {
                m_Password = value;
                m_DataChanged = true;
            }
        }

        public int ForceChangePwd
        {
            get { return m_ForceChangePwd; }
            set
            {
                m_ForceChangePwd = value;
                m_DataChanged = true;
            }
        }

        public System.DateTime PrivilegesExpire
        {
            get { return m_PrivilegesExpire; }
            set
            {
                if (!DateTime.Equals(m_PrivilegesExpire, value))
                {
                    m_PrivilegesExpire = value;
                    m_DataChanged = true;
                }
            }
        }

        public System.DateTime PasswordExpire
        {
            get { return m_PasswordExpire; }
            set
            {
                if (!DateTime.Equals(m_PasswordExpire, value))
                {
                    m_PasswordExpire = value;
                    m_DataChanged = true;
                }
            }
        }

        public string SunStart
        {
            get { return m_SunStart; }
            set
            {
                m_SunStart = value;
                m_DataChanged = true;
            }
        }

        public string SunEnd
        {
            get { return m_SunEnd; }
            set
            {
                m_SunEnd = value;
                m_DataChanged = true;
            }
        }

        public string MonStart
        {
            get { return m_MonStart; }
            set
            {
                m_MonStart = value;
                m_DataChanged = true;
            }
        }

        public string MonEnd
        {
            get { return m_MonEnd; }
            set
            {
                m_MonEnd = value;
                m_DataChanged = true;
            }
        }

        public string TueStart
        {
            get { return m_TueStart; }
            set
            {
                m_TueStart = value;
                m_DataChanged = true;
            }
        }

        public string TueEnd
        {
            get { return m_TueEnd; }
            set
            {
                m_TueEnd = value;
                m_DataChanged = true;
            }
        }

        public string WedStart
        {
            get { return m_WedStart; }
            set
            {
                m_WedStart = value;
                m_DataChanged = true;
            }
        }

        public string WedEnd
        {
            get { return m_WedEnd; }
            set
            {
                m_WedEnd = value;
                m_DataChanged = true;
            }
        }

        public string ThuStart
        {
            get { return m_ThuStart; }
            set
            {
                m_ThuStart = value;
                m_DataChanged = true;
            }
        }

        public string ThuEnd
        {
            get { return m_ThuEnd; }
            set
            {
                m_ThuEnd = value;
                m_DataChanged = true;
            }
        }

        public string FriStart
        {
            get { return m_FriStart; }
            set
            {
                m_FriStart = value;
                m_DataChanged = true;
            }
        }

        public string FriEnd
        {
            get { return m_FriEnd; }
            set
            {
                m_FriEnd = value;
                m_DataChanged = true;
            }
        }

        public string SatStart
        {
            get { return m_SatStart; }
            set
            {
                m_SatStart = value;
                m_DataChanged = true;
            }
        }

        public string SatEnd
        {
            get { return m_SatEnd; }
            set
            {
                m_SatEnd = value;
                m_DataChanged = true;
            }
        }
        public bool EnforceCheckSum
        {
            get
            {
                return m_bEnforceCheckSum;
            }
            set
            {
                m_bEnforceCheckSum = value;
            }
        }

        // ******************************************************************************
        //  Database specific properties (wrapped from RiskmasterDatabase object.)
        // ******************************************************************************		

        /// <summary>
        /// Riskmaster.Security.objRiskmasterDatabase is the read only property it takes accessor for Riskmaster.Security.RiskmasterDatabase
        /// </summary>
        /// <returns>It returns object of riskmaster securuty database</returns>
        public RiskmasterDatabase objRiskmasterDatabase
        {
            get { return m_objRMDB; }
        }

        /// <summary>
        /// Gets the list of Riskmaster Databases 
        /// for a specified user
        /// </summary>
        public string[] RiskmasterUserDatabases
        {
            get
            {
                return GetRiskmasterUserDatabases();
            }//get
        }//property: RiskmasterUserDatabases()

        // ******************************************************************************
        // User specific properties (wrapped from User object).
        // ******************************************************************************		
        /// <summary>
        /// Riskmaster.Security.objUser is the read only property takes the accessor for Riskmaster.Security.User object
        /// </summary>
        /// <returns>It returns the object of riskmaster security user.</returns>
        public User objUser
        {
            get { return m_objUser; }
        }

        // ******************************************************************************
        // User specific Limit properties (wrapped from UserLoginLimits object).
        // ******************************************************************************		
        // JP 6/15/2005    Moved out of UserLogin because this is RM specific and not needed by AC.
        //		/// <summary>
        //		/// Riskmaster.Security.objLimits is a read only property set up to 
        //		/// give access to business logic imposed limits specific to this user and/or his/her group.
        //		/// </summary>
        //		/// <returns>It returns the object of riskmaster user limits.</returns>
        //		public Riskmaster.Security.UserLoginLimits objLimits
        //		{
        //			get{return m_objLimits;}
        //		}

    }//Class
} //Namespace
