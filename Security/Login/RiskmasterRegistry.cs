﻿using Riskmaster.Security.Encryption;
using System;
using System.Runtime.InteropServices;
using Riskmaster.Common;


namespace Riskmaster.Security
{   
    /// <summary>
    /// Manages RISKMASTER Registry operations 
    /// </summary>
    public class RiskmasterRegistry
    {
        /// <summary>
        /// Retrieves the Security DSN from the 32-bit Registry
        /// </summary>
        /// <returns>string containing the Security DSN connection string</returns>
        public static string GetSecurityDSN()
        {

            string dsn = DSNRegistryKeys.SecurityDSN;

            //Get the appropriate values from the registry
            string password = RegistryManager.ReadRegistryKey(DSNRegistryKeys.EncDBPassword, string.Empty);

            string username = RegistryManager.ReadRegistryKey(DSNRegistryKeys.EncDBUserID, string.Empty);
            
            
            // Decrypt the values
            if (!UtilityFunctions.IsEmptyString(username))
            {
                username = RMCryptography.DecryptString(username);
            }//if
            else //Try clear Text
            {
                username = RegistryManager.ReadRegistryKey(DSNRegistryKeys.DBUserID, string.Empty);
            }
            if (!UtilityFunctions.IsEmptyString(username))
            {
                password = RMCryptography.DecryptString(password);
            }//if
            else //Try clear Text
            {
                password = RegistryManager.ReadRegistryKey(DSNRegistryKeys.DBPassword, string.Empty);
            }//else

            
            //return the final connection string
            return UtilityFunctions.BuildDSNConnectionString(dsn, username, password);       
        }//method: GetSecurityDSN()
    }//class
}//namespace
