﻿using System;
using Riskmaster.Security.Encryption;

namespace Riskmaster.Security.Licensing
{
    /// <summary>
    /// Manages Licenses for RISKMASTER Applications
    /// </summary>
    public class RMLicenseManager
    {
        /// <summary>
        /// Function to generate new license codes for clients
        /// </summary>
        /// /// <param name="strWorkstationNumber">string containing the number of workstations to be licensed</param>
        /// <param name="strDSN">string containing the name of the Riskmaster DSN</param>
        /// <returns>string containing the generated license</returns>
        public static string GenerateLicense(string strWorkstationNumber, string strDSN)
        {
            string strDate = string.Empty;
            string strLicenseCode = string.Empty;
            Int16 intNumber = 0;


            strDate = DateTime.Now.ToString("yyyyMMdd");
            intNumber = Convert.ToInt16(strWorkstationNumber);

            strLicenseCode = String.Format("{0}:{1}:{2}", strDate, strDSN, intNumber);

            //Get the encrypted string
            strLicenseCode = RMCryptography.EncryptString(strLicenseCode);

            //return the License code
            return strLicenseCode;
        }//method: GenerateLicense()

        /// <summary>
        /// Function to generate new license codes for clients
        /// </summary>
        /// /// <param name="strWorkstationNumber">string containing the number of workstations to be licensed</param>
        /// <param name="strDSN">string containing the name of the Riskmaster DSN</param>
        /// <param name="strDate">string containing the date to use for license generation</param>
        /// <returns>string containing the generated license</returns>
        /// <remarks>
        /// Date string needs to be in the following format: yyyyMMdd
        /// </remarks>
        public static string GenerateLicense(string strWorkstationNumber, string strDSN, string strDate)
        {
            string strLicenseCode = string.Empty;
            Int16 intNumber = 0;

            intNumber = Convert.ToInt16(strWorkstationNumber);

            strLicenseCode = String.Format("{0}:{1}:{2}", strDate, strDSN, intNumber);

            //Get the encrypted string
            strLicenseCode = RMCryptography.EncryptString(strLicenseCode);


            //return the License code
            return strLicenseCode;
        }//method: GenerateLicense()

        /// <summary>
        /// Generates a Clear License Code for clients
        /// </summary>
        /// <returns>a string containing the Clear License Code</returns>
        public static string GenerateClearLicenseCode()
        {
            string strTemp = string.Empty;
            string strDate = string.Empty;

            strDate = DateTime.Now.ToString("yyyyMMdd");
            strTemp = String.Format("{0}:Mynd", strDate);

            strTemp = RMCryptography.EncryptString(strTemp);

            return strTemp;
        }//method:GenerateClearLicenseCode()

    }
}
