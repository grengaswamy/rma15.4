using System;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using System.Data;
using System.Text;
using System.Xml;
using System.IO;
using Riskmaster.Security.Encryption;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;

namespace Riskmaster.Common
{
	/// <summary>
	/// Utilities is a static class library of commonly used functions
	/// </summary>
	public class Utilities
	{
		/// <summary>
		/// This is code access key control.
		/// </summary>
		private const string CODE_ACCESS_PUBLIC_KEY = "0x00240000048000009400000006020000002400005253413100040000010001007D4803001C5CD7C2B19D9EE894D4BB28ED97F0AC4D3D5C101DA3EF7E12747BF213C1A0DA0F8EA293FA9643671D2B73746D5378F57C5F93E1BE68C1880FB6F1825D6EC9BDA12FB0C45960CD08AE493F0C7E6EB394F57461AD6496FA039C916293A395DC820D1F24B603EBBD69917EEE6A51FA2526E0FF376752DCE16083F587B6";

		/// <summary>
		/// Performs a Binary serialization of an object graph
		/// using the BinaryFormatter
		/// </summary>
		/// <param name="objForSerialization">object graph to be serialized</param>
		/// <returns>byte array containing the serialized object graph</returns>
		public static byte[] BinarySerialize(object objForSerialization)
		{
			BinaryFormatter formatter = new BinaryFormatter();
			byte[] arrSerializedObject = null;

			using (MemoryStream memStream = new MemoryStream())
			{
				formatter.Serialize(memStream, objForSerialization);
				arrSerializedObject = memStream.ToArray();
			}//using

			return arrSerializedObject;
		} // method: BinarySerialize

		/// <summary>
		/// Performs a Binary deserialization of a byte array
		/// using the BinaryFormatter
		/// </summary>
		/// <param name="arrForDeserialization">byte array to be deserialized</param>
		/// <returns>object containing the deserialized object graph</returns>
		public static object BinaryDeserialize(byte[] arrForDeserialization)
		{
			BinaryFormatter formatter = new BinaryFormatter();
			object objDeserialized = null;

			using (MemoryStream memStream = new MemoryStream(arrForDeserialization, false))
			{
				objDeserialized = formatter.Deserialize(memStream);
			}//using

			return objDeserialized;
		} // method: BinaryDeserialize

		/// <summary>
		/// Performs XmlSerialization of a particular object/class
		/// using the .Net Xml Serializer
		/// </summary>
		/// <param name="objForSerialization">object or class instance to be serialized</param>
		/// <returns>string containing the serialized object in Xml Document format</returns>
		public static string XmlSerialize(object objForSerialization)
		{
			//Deb : MITS 34351
			//string strSerialized = string.Empty;
            return XmlSerialize(objForSerialization, SerializationType.XmlSerializer);
            
		}//method: XmlSerialize


        // npadhy Introduced DataContract Serializationas XMLserialization does not support serialization of Dictionary
        /// <summary>
        /// Performs XmlSerialization of a particular object/class
        /// using the .Net Xml Serializer
        /// </summary>
        /// <param name="objForSerialization">object or class instance to be serialized</param>
        /// <returns>string containing the serialized object in Xml Document format</returns>
        public static string XmlSerialize(object objForSerialization, SerializationType currentSerialization)
        {
            if (currentSerialization == SerializationType.XmlSerializer)
            {
                StringWriter stringWriter = new StringWriter();
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    XmlSerializer serializer = new XmlSerializer(objForSerialization.GetType());
                    XmlSerializerNamespaces xmlnsEmpty = new XmlSerializerNamespaces(new[] { new XmlQualifiedName(string.Empty, string.Empty), });
                    //XmlTextWriter writer = new XmlTextWriter(memoryStream, UnicodeEncoding.UTF8);
                    //serializer.Serialize(writer, objForSerialization, xmlnsEmpty);
                    //strSerialized = new string(UnicodeEncoding.UTF8.GetChars(memoryStream.GetBuffer())).Trim();
                    serializer.Serialize(stringWriter, objForSerialization, xmlnsEmpty);
                }//using
                return stringWriter.ToString();
            }
            else if (currentSerialization == SerializationType.DataContractSerializer)
            {
                StringWriter stringWriter = new StringWriter();
                var serializer = new DataContractSerializer(objForSerialization.GetType());
                string xmlString = string.Empty;
                using (var sw = new StringWriter())
                {
                    using (var writer = new XmlTextWriter(sw))
                    {
                        writer.Formatting = Formatting.Indented; // indent the Xml so it's human readable
                        serializer.WriteObject(writer, objForSerialization);
                        writer.Flush();
                        xmlString = sw.ToString();
                    }
                }
                return xmlString;
            }
            else
                throw new Exception("Invalid Serialization mechanism");
        }

        public enum SerializationType
        {
            XmlSerializer,
            DataContractSerializer
        }

		/// <summary>
		/// SoundEx is a simple scheme for assigning searchable 
		/// keys to Names based on Sound instead of spelling.
		/// Initially used by the U.S. Census.  See:
		/// www.archives.gov/research_room/geneology/censuse/soundex.html
		/// for implementation specifics.
		/// </summary>
		/// <param name="lastName"></param>
		/// <returns></returns>
		public static string FindSoundexValue(string lastName)
		{
			string sLastLetter,sLetter;
			string sNewLastName;
			string sTotal;

			if(lastName==null || lastName=="")
				return "0000";

			sNewLastName = lastName.Trim().ToUpper();
			//MITS 7052:Raman Bhatia : Soundex Values screen throws error
			if(sNewLastName.Length > 0)
				sTotal =sNewLastName.Substring(0, 1);
			else
				return "0000";
			sLastLetter = sTotal;
			for (int i = 1; i < sNewLastName.Length; i++)
			{
				sLetter = sNewLastName.Substring(i, 1);
				if (sLetter !=  sLastLetter)
					switch(sLetter)
					{
						case "B": case "F": case "P": case "V":
							sTotal += "1";
							break;
						case "C": case "G": case "J": case  "K": case "S": case "X": case  "Z": case "Q":
							sTotal +="2";
							break;
						case "D": case "T":
							sTotal += "3";
							break;
						case "L":
							sTotal += "4";
							break;
						case "M": case "N":
							sTotal += "5";
							break;
						case "R":
							sTotal += "6";
							break;
					}
				sLastLetter = sLetter;
			} //end foreach character
			if(sTotal.Length >4)
				return sTotal.Substring(0,4);
			else
				return sTotal.PadRight(4,'0');
		}
		/// <summary>		
		/// This method checks whether the string passed is numeric or not		
		/// </summary>		
		/// <param name="p_sCheckString">The string that has to be checked for valid number</param>												
		/// <returns>True/False</returns>
		public  static  bool IsNumeric(string p_sCheckString)
		{
            //try 
            //{
            //    p_sCheckString = p_sCheckString.Trim();
            //    Int32.Parse(p_sCheckString);
            //}
            //catch 
            //{
            //    return false;
            //}
            //Deb:Performance Changes, Commnented Above line because TryParse is better than Try/Catch here
            int iOut;
            p_sCheckString = p_sCheckString.Trim();
            bool bRet = Int32.TryParse(p_sCheckString, out iOut);
            return bRet;
            //Deb:Performance Changes
		}
		/// <summary>
		/// Appends "," in the input string parameter
		/// </summary>
		/// <param name="p_sText"></param>
		/// <returns>string appended with ',' character</returns>
		public static string AppendComma(string p_sText)
		{
			string sComma = "";
			try
			{
				if (p_sText != "") 
					sComma = "," + p_sText;
				else 
					sComma = "";
			}
			catch 
			{
				return ("");
			}
			return (sComma);
		}
		
		/// <summary>
		/// Finds the index corresponding to the input string in the Arraylist
		/// </summary>
		/// <param name="p_arrlstArray">Arraylist containing display table names</param>
		/// <param name="p_sItem">string for which index is to be find</param>
		/// <returns>Arraylist index corresponding to the input string</returns>
		public static int IndexInArray(ArrayList p_arrlstArray, string p_sItem)
		{
			int iRet = -1;// Mihika 15-Feb-2006: The method should return '-1' in case no matching record if found.
			try
			{
				for (int iCnt = 0; iCnt< p_arrlstArray.Count ;iCnt++)
				{
					if (p_arrlstArray[iCnt].ToString() == p_sItem)
					{
						iRet = iCnt;
						break;
					}
					
				}
			}
			catch
			{
				return (-1);
			}
			return (iRet);
		}

		/// <summary>
		/// Finds whether a string is present in an Arraylist or not
		/// </summary>
		/// <param name="p_arrlstArray">Arraylist containing name of the Query tables</param>
		/// <param name="p_sItem">string containing name of a single Query table</param>
		/// <returns>True if string is present in an ArrayList else false</returns>
		public static bool ExistsInArray(ArrayList p_arrlstArray, string p_sItem)
		{				
			int iCnt = 0;
			bool bRet = false;
			try
			{
				for (iCnt = 0; iCnt<p_arrlstArray.Count; iCnt++)
				{
					if (Convert.ToString(p_arrlstArray[iCnt]) == p_sItem)
					{
						bRet = true;
						break;
					}
				}
			}
			catch
			{
				return (false);
			}
			return (bRet);
		}

		/// <summary>
		/// This function formats a string into a valid date.
		/// </summary>
		/// <returns>True if string is successfully formatted into date.</returns>
		public static bool IsDate(string p_sDate)
		{ 
			bool bRetVal=false;
			try
			{
				DateTime.Parse(p_sDate);
				bRetVal=true;
			}
			catch
			{
				return (false);
			}
			return bRetVal;
		}

		/// Name		: FormatSqlFieldValue
		/// Author		: Parag Sarin
		/// Date Created: 11/29/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns a string value to be used for insertion/updation into database
		/// Quotes the string if not empty. Double quotes any single quotes so they will work.
		/// </summary>
		/// <param name="p_sArg">SQL query to be formatted</param>
		/// <returns>Formatted SQL query</returns>
		public static string FormatSqlFieldValue(string p_sArg)
		{
			string sTmp = "";
			try
			{
				if (p_sArg == "")
				{
					sTmp = "null";
				}
				else
				{
					sTmp = "N'" + p_sArg.Replace("'", "''") + "'";
				}
			}
			catch {} //Empty catch as simple string manipulation is taking place
			return (sTmp);
		}

		#region Add Delimiter
		/// <summary>
		/// creates string by concatanating a given string i.e. A and another string i.e B seperated by given seperator string
		/// </summary>
		/// <param name="p_sOriginal">string value passed i.e. A</param>
		/// <param name="p_sToAppend">string value to append i.e. B</param>
		/// <param name="p_sSep">seperator string value</param>
		public static void AddDelimited(ref string p_sOriginal, string p_sToAppend, string p_sSep, int p_iClientId)
		{
			try
			{
				if(p_sSep==null)
				{
					p_sSep=",";
				}
				p_sOriginal=AddSep(p_sOriginal,p_sSep, p_iClientId)+p_sToAppend;

			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Utilities.AddDelimited.GeneralError", p_iClientId), p_objException);
			}
		}

		/// <summary>
		/// Creates a string concatanating a given string value A and a seperator string B
		/// </summary>
		/// <param name="p_sStr">string value passed i.e A</param>
		/// <param name="p_sSep">seperator i.e. B</param>
		/// <returns></returns>
		public static string AddSep(string p_sStr,string p_sSep, int p_iClientId)
		{
			string sRetVal="";
			try
			{
				if(p_sSep==null)
				{
					p_sSep=",";
				}
				sRetVal=p_sStr!="" ? p_sStr+p_sSep : p_sStr;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Utilities.AddSep.GeneralError", p_iClientId), p_objException);
			}
			return sRetVal;
		}
		#endregion

		#region Get Next Unique ID for a database table

		/// <summary>
		/// Generates a new GUID to be used 
		/// for any code which requires a unique identifier
		/// </summary>
		/// <returns>string containing the generated Guid</returns>
		public static string GenerateGuid()
		{
			return Guid.NewGuid().ToString();
		} // method: GenerateGuid()


		/// Name			: GetNextUID
		/// Author			: Aditya Babbar
		/// Date Created	: 10-Nov-2004
		/// ***********************************************************************
		/// Amendment History
		/// ***********************************************************************
		/// Date Amended	*   Amendment						*    Author
		///					*									*
		///	08-Feb-2005		*	Added connection in parameter	*    Rajeev Chauhan	
		///	10-Feb-2005		*	Accepts the open connection and *    Rajeev Chauhan
		///					*   leaves the connection opened	*    	
		///	08-Sep-2005		*	Modified to call overloaded		*	 Aditya Babbar
		///						GetNextUID() with null			*
		///						DbTransaction object			*    
		/// ***********************************************************************
		/// <summary>
		/// Retrieves and increments the current unique id counter for 
		/// "p_sTableName" from the Riskmaster Glossary.
		/// </summary>
		/// <param name="p_oDBConnection">
		///		Connection object assumed to be open
		///	</param>
		/// <param name="p_sTableName">
		///		The name of the table who's next unique row id should be
		///		retrieved and incremented.
		///	</param>
		/// <returns>Integer containing the requested unique id value.</returns>
		/// <remarks>Let the connection be open</remarks> 
        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetNextUID
        /// </summary>     
        public static int GetNextUID(DbConnection p_oDBConnection, string p_sTableName)
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetNextUID( p_oDBConnection, p_sTableName, 0);
            }
            else
                throw new Exception("GetNextUID" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
        
        
        
        public static int GetNextUID(DbConnection p_oDBConnection, string p_sTableName, int p_iClientId)
		{
            return GetNextUID(p_oDBConnection, p_sTableName, null, p_iClientId);  
		}


		/// Name			: GetNextUID
		/// Author			: Aditya Babbar
		/// Date Created	: 08-Sep-2004
		/// ***********************************************************************
		/// Amendment History
		/// ***********************************************************************
		/// Date Amended	*   Amendment						*    Author
		///					*									*
		/// ***********************************************************************
		/// <summary>
		/// Retrieves and increments the current unique id counter for 
		/// "p_sTableName" from the Riskmaster Glossary.
		/// </summary>
		/// <param name="p_oDBConnection">
		///		Connection object assumed to be open
		///	</param>
		/// <param name="p_sTableName">
		///		The name of the table who's next unique row id should be
		///		retrieved and incremented.
		///	</param>
		///	<param name="p_objTrans">
		///		Existing transaction context.
		///	</param>
		/// <returns>Integer containing the requested unique id value.</returns>
		/// <remarks>Let the connection be open</remarks> 
        /// 
        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetNextUID
        /// </summary>     
        public static int GetNextUID(DbConnection p_oDBConnection, string p_sTableName, DbTransaction p_objTrans)
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetNextUID(p_oDBConnection, p_sTableName, p_objTrans, 0);
            }
            else
                throw new Exception("GetNextUID" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

		public static int GetNextUID(DbConnection p_oDBConnection, string p_sTableName, DbTransaction p_objTrans, int p_iClientId)
		{
			string sSQL=string.Empty;
			int iTableID=0;
			int iNextUID=0;
			int iOrigUID=0;
			int iRows=0;
			int iCollisionRetryCount=0;
			int iErrRetryCount=0;
			
			DbReader objDbReader = null;

			try
			{
				do
				{
					if(p_oDBConnection.State != System.Data.ConnectionState.Open)  
						p_oDBConnection.Open();
					
					//Get Current Id.
					objDbReader = p_oDBConnection.ExecuteReader("SELECT TABLE_ID, NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '" + p_sTableName + "'",p_objTrans);
					if(!objDbReader.Read())
					{	
						objDbReader.Close();
						objDbReader = null;
                        throw new RMAppException(Globalization.GetString("Utilities.GetNextUID.Exception.NoSuchTable", p_iClientId));
					}
					iTableID = objDbReader.GetInt("TABLE_ID");
					iNextUID = objDbReader.GetInt("NEXT_UNIQUE_ID");
					objDbReader.Close();
					objDbReader = null;
			
					// Compute next id
					iOrigUID = iNextUID;
					if(iOrigUID !=0)
						iNextUID++;
					else 
						iNextUID = 2;
		
					// try to reserve id (searched update)
					sSQL = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " + iNextUID 
						+ " WHERE TABLE_ID = " + iTableID;
	
					// only add searched clause if no chance of a null originally
					// in row (only if no records ever saved against table)   JP 12/7/96
					if(iOrigUID !=0)
						sSQL +=" AND NEXT_UNIQUE_ID = " + iOrigUID;
				
					// Try update
					try
					{
						iRows = p_oDBConnection.ExecuteNonQuery(sSQL,p_objTrans);
					}
					catch(Exception e)
					{
						iErrRetryCount++;
						if (iErrRetryCount >= 5)
						{
                            throw new RMAppException(Globalization.GetString("Context.GetNextUID.Exception.ErrorModifyingDB", p_iClientId), e);
						}
					}
					// if success, return
					if( iRows == 1)
					{
						return iNextUID - 1;
					}
					else
					{
						// collided with another user - try again (up to 1000 times)
						iCollisionRetryCount++;   
						// Close the connection to be reopened
						p_oDBConnection.Close();
					}
				}
				while ((iErrRetryCount < 5) && (iCollisionRetryCount < 1000));
	 
				if(iCollisionRetryCount >= 1000)
				{
                    throw new RMAppException(Globalization.GetString("Utilities.GetNextUID.Exception.CollisionTimeout", p_iClientId));
				}
			}
			catch(Exception p_oException)
			{
                throw new RMAppException(Globalization.GetString("Utilities.GetNextUID.Exception", p_iClientId), p_oException);
			}
			//shouldn't get here under normal conditions.
			return 0;  
		}

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetNextUID
        /// </summary>     
        public static int GetNextUID(string strConnString, string tableName)
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetNextUID(strConnString, tableName, 0);
            }
            else
                throw new Exception("GetNextUID" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
		/// <summary>
		/// Retrieves and increments the current unique id counter for "tableName" from the Riskmaster Glossary.</summary>
		/// <param name="strConnString">string containing the database connection string</param>
		/// <param name="tableName">The name of the table who's next unique row id should be retrieved and incremented.</param>
		/// <returns>Integer containing the requested unique id value.</returns>
		public static int GetNextUID(string strConnString, string tableName, int p_iClientId)
		{

			int nextUID = 0, origUID = 0, rows = 0, collisionRetryCount = 0, errRetryCount = 0;
            int tableID = -1; //Initialize the value to a non-existent table ID in the Glossary table
            string SQL = string.Empty;
            const int COLLISION_RETRY_COUNT = 10;
			const int ERROR_RETRY_COUNT = 5;

			do
			{

				//Always ensure that the DbReader is closed and the underlying database connection is closed
				using (DbReader objReader = DbFactory.ExecuteReader(strConnString, String.Format("SELECT TABLE_ID,NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '{0}'", tableName)))
				{
					//Loop through the records				   
					while (objReader.Read())
					{
						tableID = objReader.GetInt(0);
						nextUID = objReader.GetInt(1);
					}//while   
				}//using

                //Throw an exception if the specified Table does not exist in the Glossary
                if (tableID  == -1)
                {
                    throw new RMAppException(Globalization.GetString("Utilities.GetNextUID.Exception.NoSuchTable", p_iClientId));
                }//if


				//Compute next id
				origUID = nextUID;
				if (origUID != 0)
				{
					nextUID++;
				}//if
				else
				{
					nextUID = 2;
				}//else


				//try to reserve id (searched update)
				SQL = String.Format("UPDATE GLOSSARY SET NEXT_UNIQUE_ID = {0} WHERE TABLE_ID = {1}", nextUID, tableID);

				// only add searched clause if no chance of a null originally in row (only if no records ever saved against table)   JP 12/7/96
				if (origUID != 0)
				{
					SQL += String.Format(" AND NEXT_UNIQUE_ID = {0}", origUID);
				}//if


				// Try update
				try
				{
					rows = DbFactory.ExecuteNonQuery(strConnString, SQL);
				}
				catch (Exception e)
				{
					errRetryCount++;
					if (errRetryCount >= ERROR_RETRY_COUNT)
					{
                        throw new RMAppException(Globalization.GetString("Context.GetNextUID.Exception.ErrorModifyingDB", p_iClientId), e);
					}
				}
				// if success, return
				if (rows == 1)
				{
					return nextUID - 1;
				}
				else
				{
					collisionRetryCount++;   // collided with another user - try again (up to 1000 times)
				}//else


			} while ((errRetryCount < ERROR_RETRY_COUNT) && (collisionRetryCount < COLLISION_RETRY_COUNT));

			if (collisionRetryCount >= COLLISION_RETRY_COUNT)
			{
                throw new RMAppException(Globalization.GetString("Context.GetNextUID.Exception.CollisionTimeout", p_iClientId));
			}
			return 0;  //shouldn't get here under normal conditions.
		}


		#endregion

		#region BSB Xml functions
		/// <summary>
		/// Picks an element from srcDom using sXPath to be returned as it's own new document.
		/// </summary>
		/// <param name="sXPath"></param>
		/// <param name="srcDom"></param>
		/// <returns></returns>
		public static  XmlDocument XPointerDoc(string sXPath, XmlDocument srcDom)
		{
			XmlDocument ret = null;
			XmlNode nd = srcDom.SelectSingleNode(sXPath);
			if(nd!=null)
			{
				ret =  new XmlDocument();
				ret.LoadXml(nd.OuterXml);
			}
			return ret;
		}
		/// <summary>
		/// Copies an element into it's own new XmlDocument..
		/// </summary>
		/// <param name="elt"></param>
		/// <returns></returns>
	
		public static XmlDocument XmlElement2XmlDoc(XmlElement elt)
		{
			XmlDocument ret = null;
			if(elt!=null)
			{
				ret =  new XmlDocument();
				ret.LoadXml(elt.OuterXml);
			}
			return ret;
		}
		
		#endregion

		#region Get difference between two dates in years



        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetDateDiffInYears
        /// </summary>     
        public static int GetDateDiffInYears(string p_sDateFrom, string p_sDateTo)
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return  GetDateDiffInYears( p_sDateFrom, p_sDateTo,0);
            }
            else
                throw new Exception("GetDateDiffInYears" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

		/// Name		: GetDateDiff
		/// Author		: Parag Sarin
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the difference between two dates in years
		/// </summary>
		/// <param name="p_sDateFrom">From date</param>
		/// <param name="p_sDateTo">To date</param>
		/// <returns>Difference between two dates in years</returns>
		public static int GetDateDiffInYears(string p_sDateFrom,string p_sDateTo, int p_iClientId)
		{
            DateTime datFrom = Riskmaster.Common.Conversion.ToDate(p_sDateFrom);
            DateTime datTo = Riskmaster.Common.Conversion.ToDate(p_sDateTo);
			return datTo.Year-datFrom.Year;
		}
		#endregion

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for ConvertFilestreamToMemorystream
        /// </summary>     
        public static MemoryStream ConvertFilestreamToMemorystream(string p_sFilePath)
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return ConvertFilestreamToMemorystream(p_sFilePath, 0);
            }
            else
                throw new Exception("ConvertFilestreamToMemorystream" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
		/// Name			: ConvertFilestreamToMemorystream
		/// Author			: Tanuj Narula
		/// Date Created	: 30-Mar-2005
		/// **************************************************************************
		/// Amendment History
		/// **************************************************************************
		/// Date Amended	*   Amendment							*   Author
		/// **************************************************************************
		/// <summary>
		/// This method converts file stream to memory stream.
		/// </summary>
		/// <param name="p_sFilePath">Name of the file to be converted to memory stream.</param>
		/// <returns>Memory stream</returns>
		public static MemoryStream ConvertFilestreamToMemorystream(string p_sFilePath, int p_iClientId)
		{
			MemoryStream objMs=new MemoryStream();
			FileStream objFilestream=null;
			byte[] arrBytes=new byte[1024];
			try
			{
				if(p_sFilePath==null || p_sFilePath.Trim()=="")
				{
                    throw new RMAppException(Globalization.GetString("Utilities.ConvertFilestreamToMemorystream.FilePathError", p_iClientId));
				}
				objFilestream=new FileStream(p_sFilePath,FileMode.Open,FileAccess.Read);
				while(true)
				{
					int iNoOfBytes=objFilestream.Read(arrBytes,0,1024);
					if(iNoOfBytes==0)
					{
						break;
					}
					objMs.Write(arrBytes,0,iNoOfBytes);
				}
				objFilestream.Close();
				
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				if(objFilestream!=null)
				{
					objFilestream.Close();
					objFilestream=null;
				}
				arrBytes=null;
			}
			return objMs;
		}

		/// <summary>
		///GetOrgTableName returns the Table Name.
		/// </summary>
		/// <param name="p_iTableId">Table ID for which the Table Name is required</param>
		public static string GetOrgTableName(int p_iTableId)
		{	
			switch (p_iTableId)
			{
				case 1005:
					return "CLIENT";
				case 1006:
					return "COMPANY";
				case 1007:
					return "OPERATION";
				case 1008:
					return "REGION";
				case 1009:
					return "DIVISION";
				case 1010:
					return "LOCATION";
				case 1011:
					return "FACILITY";
				case 1012:
					return "DEPARTMENT";
				default:
					return "";
			}
		}

		/// Name			: ReadFileStream
		/// Author			: Jasbinder Singh Bali
		/// Date Created	: 05-May-2005
		/// **************************************************************************
		/// Amendment History
		/// **************************************************************************
		/// Date Amended	*   Amendment							*   Author
		/// **************************************************************************
		/// <summary>
		/// This method read a file stream 
		/// </summary>
		/// <param name="p_sFilePath">Name of the file to be be read.</param>
		/// <returns>String (File Content)</returns>
        /// 
        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for ReadFileStream
        /// </summary>     
        public static string ReadFileStream(string p_sFilePath)
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                return  ReadFileStream( p_sFilePath, 0);
            }
            else
                throw new Exception("ReadFileStream" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486


		public static string ReadFileStream(string p_sFilePath, int p_iClientId)
		{
			FileStream objFileStream = null;	
			StreamReader objStreamReader = null;
			string FileContent;
			
			try
			{
				if(p_sFilePath==null || p_sFilePath.Trim()=="")
				{
                    throw new RMAppException(Globalization.GetString("Utilities.ReadFileStream.FilePathError", p_iClientId));
				}
				objFileStream=new FileStream(p_sFilePath,FileMode.Open,FileAccess.Read);

				objStreamReader = new StreamReader(objFileStream);
				
				FileContent = objStreamReader.ReadToEnd();
				
			}
			catch(Exception p_objException)
			{
				throw p_objException;
			}
			finally
			{
				if(objFileStream!=null)
				{
					objFileStream.Close();	
					objStreamReader.Close();
				}
				
			}
			return FileContent;
		}

		/// Name		: FileTransfer
		/// Author		: Ratheen Chaturvedi
		/// Date Created:13-September-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// writes the file on the hard disk
		/// </summary>
		/// <param name="p_sFileContent"></param>
		/// <param name="p_sFileName"></param>
		/// <returns>Success 1 or Failure 0 in execution of the function</returns>
		public static bool FileTranfer(string p_sFileContent,string p_sFileName, int p_iClientId)
		{			
			try
			{	
				if((p_sFileContent!="")&&(p_sFileName!=""))
				{
					char[] b64Data=p_sFileContent.ToCharArray();
					byte[] decodeData=Convert.FromBase64CharArray(b64Data,0,b64Data.Length);
					FileStream fs = new FileStream(p_sFileName, FileMode.Create, FileAccess.Write, FileShare.None);
					fs.Write(decodeData, 0, decodeData.Length);
					fs.Close();
					return true;
				}
				return false;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("UploadFile.FileTransfer.Error", p_iClientId), p_objException);
			}
		}
		//Changed for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.  
		public static string CalculateAgeInYears(string birthDate)
		{
			return CalculateAgeInYears(birthDate,"");
		}
		//Changed for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.  

		public static string CalculateAgeInYears(string birthDate,string sEventDate)//Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.  
		{
			DateTime objNow=DateTime.Now;
			DateTime objBirthDate=Conversion.ToDate(birthDate);

			//Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.  
			if (!String.IsNullOrEmpty(sEventDate))
			{
				objNow = Conversion.ToDate(sEventDate);
			}
			//Added for Mits 19315:The person involved age is being calculated off the current date, and not the date of event.  

			if (objNow.Month < objBirthDate.Month || (objNow.Month==objBirthDate.Month && objNow.Day< objBirthDate.Day))
				return Convert.ToString(objNow.Year- objBirthDate.Year-1);
			else
				return Convert.ToString(objNow.Year- objBirthDate.Year);
		}
		
		public static long CopyStream(Stream stmIn, Stream stmOut)
		{
			byte[] arrBytes=new byte[1024];
			int iBytes = 0;
			int iTotal = 0;

			while((iBytes=stmIn.Read(arrBytes,0,1024)) > 0)
			{
				stmOut.Write(arrBytes,0,iBytes);
				iTotal += iBytes;
			}
			return iTotal;
		}
		public static MemoryStream ReadFileMemoryStream(string sFilePath)
		{
			MemoryStream ms = new MemoryStream();
			FileStream fs = new FileStream(sFilePath,FileMode.Open);
			CopyStream(fs,ms);
			return ms;
		}

		public static bool NTLMLogon(string sLogin,string sPassword,string sDomain)
		{
			//Test User Allowed by NTLM Authentication 
			IntPtr pHandle;
			bool ret = Common.Win32.Functions.LogonUser(sLogin,sDomain,sPassword, (int)Common.Win32.LogonType.LOGON32_LOGON_NETWORK,(int)Common.Win32.LogonProvider.LOGON32_PROVIDER_DEFAULT, out pHandle);
			Common.Win32.Functions.CloseHandle(pHandle.ToInt32());
			return ret;
		}
		/// <summary>
		/// Encypt document path connection string's UID/Password
		/// if database is used as document storage.
		/// </summary>
		/// <param name="p_sDocPath">Document storage page</param>
		/// <param name="p_iDocPathType">Document storage type: 0 - File; 1 - Database</param>
		/// <returns></returns>
		public static string EncryptDocPathCredentials(string p_sDocPath, int p_iDocPathType)
		{
			//If it's not a Document Database Connection String - Just Leave.
			if(p_iDocPathType != 1)
				return p_sDocPath;

			string[] sPairs;
			try
			{
				sPairs= p_sDocPath.Split(';');
				string [] sPair = null;
				for(int i=0;i< sPairs.Length;i++)
				{
					sPair = sPairs[i].Split('=');
					if(sPair[0].Trim().ToUpper() == "UID" || sPair[0].Trim().ToUpper() == "PWD")
						sPairs[i] = String.Format("{0}={1}",sPair[0], RMCryptography.EncryptString(sPair[1]));
				}
			}	
			finally 
			{
 
			} // finally

			return String.Join(";",sPairs);
		}
	
		/// <summary>Riskmaster.Db.DbConnection.ParseDocPathCredentials supports the Document Database
		/// by decrypting the uid and pwd sections of the connection string and 
		/// returning the fully modified connection string.</summary>
		/// <param name="p_sDocPath">The connection string in which to effect the change.</param>
		/// <param name="p_iDocPathType">Document storage type 0 - File; 1 - Database</param>
		/// <returns>An updated version of the connection string from docPath.</returns>
		/// <remarks>There may be ODBC niceties like {} or escape sequences that are not handled properly here yet.</remarks>
		public static string ParseDocPathCredentials(string p_sDocPath, int p_iDocPathType)
		{
			//If it's not a Document Database Connection String - Just Leave.
			if(p_iDocPathType != 1)
				return p_sDocPath;

			string[] sPairs= p_sDocPath.Split(';');
			string [] sPair = null;
			for(int i=0;i< sPairs.Length;i++)
			{
				sPair = sPairs[i].Split('=');
				if(sPair[0].Trim().ToUpper() == "UID" || sPair[0].Trim().ToUpper() == "PWD")
					sPairs[i] = String.Format("{0}={1}",sPair[0], RMCryptography.DecryptString(sPair[1]));
			}
			return String.Join(";",sPairs);
		}

		/// Name		: GetMultiCodeFieldWhereClauseSearchForAnyItem
		/// Author		: Nima Norouzi
		/// Date Created: May 2010		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Returns a where clause to see if any of items can be found in a multi code field. It can also determine if a field(e.g. "CLAIM_ID"; as searchItems) can be found in another field(e.g. "CLAIM_NUMBER"; as fieldToSearch).
		/// </summary>
		/// <param name="fieldToSearch">The table field to search</param>
		/// <param name="searchItems">Item(s) to search for</param>
		/// <param name="isOracle">To see if the where clause is for Oracle or not</param>
		/// <returns>Returns a where clause to see if any of items can be found in a multi code field</returns>
		public static string GetMultiCodeFieldWhereClauseSearchForAnyItem(string fieldToSearch, string searchItems, bool isOracle)
		{
			StringBuilder strClause = new StringBuilder();
			string[] splitItems = null;
			if (!string.IsNullOrEmpty(fieldToSearch) && searchItems != null)
			{
				if (searchItems == string.Empty)
				{
					splitItems = new string[] { "''" };
				}
				else
				{
					splitItems = searchItems.Split((char)32);
				}
				for (int i = 0; i < splitItems.Length; i++)
				{
					if (i > 0)
					{
						strClause.Append(" OR ");
					}
					if (isOracle)
					{
                        //Changes by bsharma33 for MITS 28245
						//strClause.Append(string.Format("INSTR({0}, {1}) > 0", fieldToSearch, splitItems[i]));
                        strClause.Append(string.Format("INSTR({0}, {1}) > 0", fieldToSearch, "' " + splitItems[i] + " '"));
                        //End changes bsharma33 for MITS 28245
					}
					else
					{
                        //Changes by bsharma33 for MITS 28245
						//strClause.Append(string.Format("CHARINDEX(CONVERT(varchar,{0}), {1}) > 0", splitItems[i], fieldToSearch));
                        strClause.Append(string.Format("CHARINDEX(CONVERT(varchar,{0}), {1}) > 0", "' " + splitItems[i] + " '", fieldToSearch));
                        //End changes bsharma33 for MITS 28245
					}

				}
                //skhare7 MITS 28499
                if (isOracle)
                {
                    strClause.Append(" OR Trim(" + fieldToSearch + ") is NULL");
                  
                }
                else
                {
                    strClause.Append(" OR " + fieldToSearch + " = ''  OR " + fieldToSearch + " is null ");
                   
                }

                //skhare7 MITS 28499 End

				if (strClause.Length > 0)
				{
					strClause.Insert(0, "(");
					strClause.Append(")");
				}
			}
			return strClause.ToString();
		}
        
        //Yatharth: Added common function for stripping the XML/HTML tags from string. MITS 22248
        //This function will remove characters like &amp; from your string and convert to its original form.
        public static string StripHTMLTags(string InString) //Parijat :19880 --Strip HTML tags and creating Plain text
        {
            string cleanedbuffer = "";
            string PatternString = "\\<[^<>]+\\>";
            Regex re = new Regex(PatternString, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.Multiline);
            cleanedbuffer = re.Replace(InString, "");

            //replace special characters
            PatternString = "(&nbsp;)|(&quot;)|(&ldquo;)|(&rdquo;)|(&lsquo;)|(&rsquo;)|(&bull;)";
            re = new Regex(PatternString, RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.Multiline);
            cleanedbuffer = re.Replace(cleanedbuffer, " ");
            cleanedbuffer = cleanedbuffer.Replace("&amp;", "&").Replace(@"&lt;", "<").Replace(@"&gt;", ">");//Parijat: replacing &amp; with &--19297
           
            return cleanedbuffer;
        }

        /// <summary>
        /// To replace & in the xml to &amp
        /// </summary>
        /// <param name="InString"></param>
        /// <returns></returns>
        public static string AddTags(string InString)
        {
            InString = InString.Replace("&", "&amp;");
            return InString;
        }

        /// <summary>
        /// To replace & in the xml to &amp
        /// </summary>
        /// <param name="InString"></param>
        /// <returns></returns>
        public static string AddHTMLTags(string InString)
        {
            InString = InString.Replace("&", "&amp;").Replace(@"<", "&lt;").Replace(@">", "&gt;");
            return InString;
        }
        public static string StringToXMLTags(string InString) //akaur9 replacing & with &amp; --Mobile Apps
        {
            string cleanedbuffer = "";
            cleanedbuffer = InString.Replace("&", "&amp;").Replace("<", "&lt;").Replace("<=", "&lt;=");  //akaur9 replacing & with &amp; --Mobile Apps
            return cleanedbuffer;
        }
	}
}
