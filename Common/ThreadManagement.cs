﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Riskmaster.Common
{
    public class ThreadManagement
    {
        /// <summary>
        /// Gets the current ThreadId for thread identification
        /// and management
        /// </summary>
        /// <returns>integer containing a unique identifier for each Thread</returns>
        public int GetThreadID()
        {
            return Thread.CurrentThread.ManagedThreadId;
        }//method: GetThread()
    }
}
