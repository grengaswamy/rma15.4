﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Riskmaster.Common
{
    public class NameValueSection : ConfigurationSection
    {
        [ConfigurationProperty("", IsDefaultCollection = true)]
        public NameValueConfigurationCollection Settings
        {
            get{return (NameValueConfigurationCollection)base[""];}
        } // property Settings

    }
}
