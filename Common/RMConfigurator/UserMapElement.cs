using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

///************************************************************** 
///* $File		: UserMapElement.cs
///* $Date		: Jan-25-2009 
///* $Author	: rahul solanki
///* $Comment	: 
///* $Source	: 
///**************************************************************
/// <summary>	
/// Provides classes for reading custom config file 
/// mapping RMX login id's to MCM login id's
/// </summary>
namespace Riskmaster.Common
{
   
	public class UserMapElement: ConfigurationElement
	{
		#region Constructors
		static UserMapElement()
		{
            
			s_RmxUser = new ConfigurationProperty( "RmxUser", typeof(string),null,ConfigurationPropertyOptions.IsRequired);

			s_AcrosoftUserId = new ConfigurationProperty("AcrosoftUserId",typeof(string),null,ConfigurationPropertyOptions.None);

			s_AcrosoftPassword = new ConfigurationProperty("AcrosoftPassword",typeof(string),null,ConfigurationPropertyOptions.IsRequired);

			s_properties = new ConfigurationPropertyCollection();

			s_properties.Add(s_RmxUser);
			s_properties.Add(s_AcrosoftUserId);            
			s_properties.Add(s_AcrosoftPassword);
		}
		#endregion

		#region Fields
		private static ConfigurationPropertyCollection s_properties;
		private static ConfigurationProperty s_RmxUser;
		private static ConfigurationProperty s_AcrosoftUserId;        
		private static ConfigurationProperty s_AcrosoftPassword;
		#endregion

		#region Properties
		public string RmxUser
		{
			get { return (string)base[s_RmxUser]; }
			set { base[s_RmxUser] = value; }
		}

		public string AcrosoftUserId
		{
			get { return (string)base[s_AcrosoftUserId]; }
			set { base[s_AcrosoftUserId] = value; }
		}

		public string AcrosoftPassword
		{
			get { return (string)base[s_AcrosoftPassword]; }
			set { base[s_AcrosoftPassword] = value; }
		}

		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				return s_properties;
			}
		}
		#endregion
	}
}
