﻿using System;
using System.Runtime.InteropServices;
using System.Configuration;
using System.Web.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Data;
using System.Collections;
using Riskmaster.Cache;
using Riskmaster.Db; //ash-cloud

namespace Riskmaster.Common
{
    /// <summary>
    /// Wraps functionality provided by both .Net Configuration APIs
    /// 1. WebConfigurationManager
    /// 2. ConfigurationManager
    /// </summary>
    [ClassInterface(ClassInterfaceType.AutoDual)]
    public class RMConfigurationManager
    {
        private static Configuration m_objConfig = null;

        internal static Configuration GetConfigurationFile
        {
            get { return m_objConfig; }
            set { m_objConfig = value; }
        } // property GetConfigurationFile


        /// <summary>
        /// Gets and sets whether or not the configuration 
        /// file is an Executable configuration file
        /// </summary>
        public static bool IsExeConfiguration { get; set; }

        /// <summary>
        /// Gets and sets whether or not the Configuration
        /// file is a Web configuration file
        /// </summary>
        public static bool IsWebConfiguration { get; set; }

        /// <summary>
        /// Opens the specific Web configuration file
        /// </summary>
        /// <param name="strWebConfigPath">string containing the virtual path
        /// to the specified Web.config file</param>
        public static void OpenWebConfigurationFile(string strWebConfigPath)
        {
            m_objConfig = WebConfigurationManager.OpenWebConfiguration("/" + strWebConfigPath);

        } // method: OpenWebConfigurationFile

        /// <summary>
        /// Opens the specified Exe configuration file
        /// </summary>
        /// <param name="strExePath">string containing the executable path</param>
        public static void OpenExeConfigurationFile(string strExePath)
        {
            m_objConfig = ConfigurationManager.OpenExeConfiguration(strExePath);
        } // method: OpenExeConfigurationFile


        #region Individual ConfigurationSettings retrieval methods


        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetLocalResourceString
        /// </summary>     
        public static string GetSessionDSN()
        {
            if (DbFactory.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetSessionDSN(0);
            }
            else
                throw new ConfigurationErrorsException(string.Format("GetLocalResourceString-The current function overload is available to Scripting assembly (or RMXCustomScripts) only"));
                 // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// <summary>
        /// Gets the Session DSN connection string
        /// </summary>
        /// <returns>ConnectionString for the Session database</returns>
        public static string GetSessionDSN(int p_iClientId)
        {
            // return ConfigurationManager.ConnectionStrings["SessionDSN"].ConnectionString;
            return ConfigurationInfo.GetSessionConnectionString(p_iClientId);
        } // method: GetSessionDSN



        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetConnectionString
        /// </summary>     
        public static string GetConnectionString(string strConnStringKey)
        {
            if (DbFactory.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetConnectionString( strConnStringKey,0);
            }
            else
                throw new ConfigurationErrorsException(string.Format("GetConnectionString-The current function overload is available to Scripting assembly (or RMXCustomScripts) only"));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// <summary>
        /// Gets the connection string from the currently accessible configuration file
        /// </summary>
        /// <param name="strConnStringKey">string containing the connection string key</param>
        /// <returns>database connection string</returns>
        public static string GetConnectionString(string strConnStringKey, int p_iClientId)
        {
            //return ConfigurationManager.ConnectionStrings[strConnStringKey].ConnectionString;
            return ConfigurationInfo.GetConnectionString(strConnStringKey, p_iClientId);
        } // method: GetConnectionString

        /// <summary>
        /// Gets the ADO.Net Provider name based on the specified connection string in the 
        /// connectionStrings section in the configuration file
        /// </summary>
        /// <param name="strConnStringKey">string containing the connection string key</param>
        /// <returns>database provider name for the specified connection string</returns>
        public static string GetConnectionStringProviderName(string strConnStringKey)
        {
            ConnectionStringSettings connSettings = ConfigurationManager.ConnectionStrings[strConnStringKey];
            return connSettings.ProviderName;
        }//method: GetConnectionStringProviderName

        /// <summary>
        /// Gets the specified Application Settings from the currently accessible configuration file
        /// </summary>
        /// <param name="strAppSettingKey">string containing the application setting key</param>
        /// <returns>application setting value</returns>
        /// ClientID will remain optional as not all keys will be moved to customer specific db 
        public static string GetAppSetting(string strAppSettingKey, string sConnString = "", int ClientId = 0)
        {
            string sAppSettingValue = "";
            if (ClientId == 0)
                sAppSettingValue = ConfigurationManager.AppSettings[strAppSettingKey];
            else
            {
                // Pending - We have not moved any app setting yet to DB.
                // This will be handled later when we decide to move app settings to DB
                if (!CacheCommonFunctions.CheckIfKeyExists(strAppSettingKey, ClientId))
                {
                    string sSQL = String.Format("select VALUE from config_keys where NAME = '{0}'", strAppSettingKey);
                    using (DbReader objRdr = DbFactory.GetDbReader(sConnString, sSQL))
                    {
                        while (objRdr.Read())
                        {
                            sAppSettingValue = objRdr["VALUE"].ToString();
                        }
                    }
                    CacheCommonFunctions.AddValue2Cache<string>(strAppSettingKey, ClientId, sAppSettingValue);
                }
                else
                {
                    sAppSettingValue = CacheCommonFunctions.RetreiveValueFromCache<string>(strAppSettingKey, ClientId);
                }               
            }
            return sAppSettingValue;

        }

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetConfigConnectionString
        /// </summary>     
        public static string GetConfigConnectionString(string strConnStringKey)
        {
            if (DbFactory.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetConfigConnectionString(strConnStringKey, 0);
            }
            else
                throw new ConfigurationErrorsException(string.Format("GetConfigConnectionString-The current function overload is available to Scripting assembly (or RMXCustomScripts) only"));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
        /// <summary>
        /// Gets a specific connection string
        /// </summary>
        /// <param name="strConnStringKey">string containing the NameValueCollection key
        /// for a specific ConnectionString</param>
        /// <returns>ConnectionString for a specified key</returns>
        public static string GetConfigConnectionString(string strConnStringKey, int ClientId)
        {
            // Ash-cloud
            //ConnectionStringsSection connStrSection = ConfigurationManager.GetSection(Global.ConnectionStrings) as ConnectionStringsSection;
            //return connStrSection.ConnectionStrings[strConnStringKey].ConnectionString;
            return GetConnectionString(strConnStringKey, ClientId);
        } // method: GetConnectionString

        /// <summary>
        /// Gets a specific application setting
        /// </summary>
        /// <param name="strAppSettingKey">string containing the NameValueCollection key</param>
        /// <returns>AppSetting for a specified key</returns>
        public static string GetConfigAppSetting(string strAppSettingKey)
        {

            AppSettingsSection appSection = m_objConfig.GetSection(Global.AppSettings) as AppSettingsSection;
            string strAppSetting = appSection.Settings[strAppSettingKey].Value;

            return strAppSetting;
        } // method: GetAppSetting 
        #endregion

        #region ConfigurationSection retrieval methods
        /// <summary>
        /// Retrieves and populates all of the Acrosoft/MCM settings
        /// required throughout various areas of the application
        /// </summary>
        public static void GetAcrosoftSettings()
        {
            //Invoke the method to populate all of the Acrosoft settings
            //from the configuration file
            AcrosoftSection.GetAcrosoftSection();
        } // method: GetAcrosoftSettings


        /// <summary>
        /// ************************************
        ///Mona:PaperVisionMerge :  Author: Animesh Sahai
        /// ************************************
        /// Retrieves and populates all of the PaperVisionsettings
        /// required throughout various areas of the application
        /// </summary>
        public static void GetPaperVisionSettings()
        {
            //Invoke the method to populate all of the Acrosoft settings
            //from the configuration file
            PaperVisionSection.GetPaperVisionSection();
        } // method: GetPaperVisionSection

        public static void GetMediaViewSettings()
        {
            //Invoke the method to populate all of the Media View settings
            //from the configuration file
            MediaViewSection.GetMediaViewSection();
        }

        //Start : averma62 VSS config settings
        public static void GetVSSExportSettings()
        {
            //Invoke the method to populate all of the VSS export settings
            //from the configuration file
            VSSExportSection.GetVSSExportSection();
        }
        //End : averma62 VSS config settings
        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetSectionSettings
        /// </summary>     
        public static NameValueCollection GetSectionSettings(string strSectionName, string sConnString)
        {
            if (DbFactory.CheckCallingStack("Riskmaster.Scripting"))
            {
                return  GetSectionSettings( strSectionName,  sConnString,0);
            }
            else
                throw new ConfigurationErrorsException(string.Format("GetSectionSettings-The current function overload is available to Scripting assembly (or RMXCustomScripts) only"));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// <summary>
        /// Gets a NameValueCollection based on a specified custom configuration
        /// section
        /// </summary>
        /// <param name="strSectionName">string containing the custom configuration section</param>
        /// <returns>NameValueCollection containing all of the settings from the 
        /// custom configuration section</returns>
        public static NameValueCollection GetSectionSettings(string strSectionName, string sConnString, int ClientId)
        {
            NameValueCollection nvCollSettings = new NameValueCollection();

            if (ClientId == 0)
            {
                NameValueSection nvSection = ConfigurationManager.GetSection(strSectionName) as NameValueSection;
                foreach (NameValueConfigurationElement nvConfigElement in nvSection.Settings)
                {
                    nvCollSettings.Add(nvConfigElement.Name, nvConfigElement.Value);
                } // foreach

            }
            else
            {
                if(!CacheCommonFunctions.CheckIfKeyExists(strSectionName, ClientId))
                {
                    string sSQL = String.Format("select ck.name as NAME, ck.value as VALUE from config_sections cs"
                        + " inner join config_keys ck on cs.row_id=ck.section_id where cs.name = '{0}'", strSectionName);
                    using (DbReader objRdr = DbFactory.GetDbReader(sConnString, sSQL))
                    {
                        while (objRdr.Read())
                        {
                            nvCollSettings.Add(objRdr["NAME"].ToString(), objRdr["VALUE"].ToString());
                        }
                    }
                    CacheCommonFunctions.AddValue2Cache<NameValueCollection>(strSectionName,ClientId,nvCollSettings);
                }
                else
                {
                    nvCollSettings = CacheCommonFunctions.RetreiveValueFromCache<NameValueCollection>(strSectionName, ClientId);
                }

            }

            return nvCollSettings;
        } // method: GetSectionSettings

        /// <summary>
        /// Gets all of the available settings from a configuration file
        /// when the section is defined as a single element/single tag
        /// </summary>
        /// <param name="strSectionName">string containing the name of the defined section in the 
        /// Configuration File</param>
        /// <returns>Hashtable containing all of the tag attributes</returns>
        /// <example>
        /// <section name="MySingleTag" type="System.Configuration.SingleTagSectionHandler"/>
        /// <MySingleTag myattribute1="value" myattribute2="value2" />
        /// </example>
        /// <summary>
        /// Function overload with main db connection string to fetch config key/value from database
        /// </summary>
        public static Hashtable GetSingleTagSectionSettings(string strSectionName, string sConnString, int p_iClientId)
        {
            Hashtable objSingleTagSection = new Hashtable();

            if (p_iClientId == 0)
                objSingleTagSection = ConfigurationManager.GetSection(strSectionName) as Hashtable;
            else
            {
                if (!CacheCommonFunctions.CheckIfKeyExists(strSectionName, p_iClientId))
                {
                    string sSQL = String.Format("select ck.name as NAME, ck.value as VALUE from config_sections cs"
                    + " inner join config_keys ck on cs.row_id=ck.section_id where cs.name = '{0}'", strSectionName);
                    using (DbReader objRdr = DbFactory.GetDbReader(sConnString, sSQL))
                    {
                        while (objRdr.Read())
                        {
                            objSingleTagSection.Add(objRdr["NAME"].ToString(), objRdr["VALUE"].ToString());
                        }
                    }
                    CacheCommonFunctions.AddValue2Cache<Hashtable>(strSectionName, p_iClientId, objSingleTagSection);
                }
                else
                {
                    objSingleTagSection = CacheCommonFunctions.RetreiveValueFromCache<Hashtable>(strSectionName, p_iClientId);
                }
            }

            CheckConfigurationSection(objSingleTagSection, strSectionName);

            return objSingleTagSection;
        } // method: GetSingleTagSection

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetDictionarySectionSettings
        /// </summary>     
        public static Hashtable GetDictionarySectionSettings(string strSectionName, string sConnString)
        {
            if (DbFactory.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetDictionarySectionSettings(strSectionName, sConnString, 0);
            }
            else
                throw new ConfigurationErrorsException(string.Format("GetDictionarySectionSettings -The current function overload is available to Scripting assembly (or RMXCustomScripts) only"));
             // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// <summary>
        /// Gets all of the available settings from a configuration file
        /// when the section is defined as a Dictionary collection (i.e. Key/Value pair)
        /// </summary>
        /// <param name="strSectionName">string containing the name of the defined section in the 
        /// Configuration File</param>
        /// <returns>Hashtable containing all of the tag elements expressed as key/value pairs</returns>
        /// <example>
        /// <section name="MyDictionarySection" type="System.Configuration.DictionarySectionHandler"/>
        /// <MyDictionarySection>
        /// <add key="mykey" value="myvalue" />
        /// <add key="mykey2" value="myvalue2" />
        /// </MyDictionarySection>
        /// </example>
        public static Hashtable GetDictionarySectionSettings(string strSectionName, string sConnString , int p_iClientId)
        {
            Hashtable objDictSection = new Hashtable();

            if (p_iClientId == 0)
                objDictSection = ConfigurationManager.GetSection(strSectionName) as Hashtable;
            else
            {
                if (!CacheCommonFunctions.CheckIfKeyExists(strSectionName, p_iClientId))
                {
                    string sSQL = String.Format("select ck.name as NAME, ck.value as VALUE from config_sections cs"
                    + " inner join config_keys ck on cs.row_id=ck.section_id where cs.name ='{0}'", strSectionName);
                    using (DbReader objRdr = DbFactory.GetDbReader(sConnString, sSQL))
                    {
                        while (objRdr.Read())
                        {
                            objDictSection.Add(objRdr["NAME"].ToString(), objRdr["VALUE"].ToString());
                        }
                    }
                    CacheCommonFunctions.AddValue2Cache<Hashtable>(strSectionName, p_iClientId, objDictSection);
                }
                else
                {
                    objDictSection = CacheCommonFunctions.RetreiveValueFromCache<Hashtable>(strSectionName, p_iClientId);
                }
            }

            CheckConfigurationSection(objDictSection, strSectionName);

            return objDictSection;
        } // method: GetDictionarySectionSettings

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetNameValueSectionSettings
        /// </summary>     
        public static NameValueCollection GetNameValueSectionSettings(string strSectionName, string sConnString)
        {
            if (DbFactory.CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetNameValueSectionSettings(strSectionName, sConnString, 0);
            }
            else
                throw new ConfigurationErrorsException(string.Format("GetNameValueSectionSettings -The current function overload is available to Scripting assembly (or RMXCustomScripts) only"));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486

        /// <summary>
        /// Gets all of the available settings from a configuration file
        /// when the section is defined as a NameValueCollection (i.e. Key/Value pair)
        /// </summary>
        /// <param name="strSectionName">string containing the name of the defined section in the 
        /// Configuration File</param>
        /// <returns>NameValueCollection containing all of the tag elements expressed as key/value pairs</returns>
        /// <example>
        /// <section name="MyNameValueSection" type="System.Configuration.NameValueSectionHandler"/>
        /// <MyNameValueSection>
        /// <add key="mykey" value="myvalue" />
        /// <add key="mykey2" value="myvalue2" />
        /// </MyNameValueSection>
        /// </example>
        /// <remarks>NameValueCollections are particularly useful since they are natively expressed 
        /// as strings and can therefore be easily used to store string collections.  However, given that they are stored 
        /// as strings, an additional cast would be required to cast it to its appropriate object and therefore would be equivalent
        /// to using the SingleTagSectionHandler or DictionarySectionHandler which uses Hashtables instead.  In addition, NameValueCollections
        /// do not have to be unique and therefore can be problematic if configuration sections are accidentally duplicated.
        /// </remarks>
        /// <seealso cref="SingleTagSectionHandler"/>
        /// <seealso cref="DictionarySectionHandler"/>
        public static NameValueCollection GetNameValueSectionSettings(string strSectionName, string sConnString, int p_iClientId)
        {
            NameValueCollection nvCollSettings = null;

            if (p_iClientId == 0)
                nvCollSettings = ConfigurationManager.GetSection(strSectionName) as NameValueCollection;
            else
            {
                if (!CacheCommonFunctions.CheckIfKeyExists(strSectionName, p_iClientId))
                {
                    nvCollSettings = new NameValueCollection();
                    string sSQL = String.Format("select ck.name as NAME, ck.value as VALUE from config_sections cs"
                   + " inner join config_keys ck on cs.row_id=ck.section_id where cs.name = '{0}'", strSectionName);

                    using (DbReader objRdr = DbFactory.GetDbReader(sConnString, sSQL))
                    {
                        while (objRdr.Read())
                        {
                            nvCollSettings.Add(objRdr["NAME"].ToString(), objRdr["VALUE"].ToString());
                        }
                    }
                    CacheCommonFunctions.AddValue2Cache<NameValueCollection>(strSectionName, p_iClientId, nvCollSettings);
                }
                else
                {
                    nvCollSettings = CacheCommonFunctions.RetreiveValueFromCache<NameValueCollection>(strSectionName, p_iClientId);
                }

            }

            CheckConfigurationSection(nvCollSettings, strSectionName);
            return nvCollSettings;

        } // method: GetNameValueSectionSettings



        /// <summary>
        /// Gets the list of Riskmaster Business Adaptors
        /// </summary>
        /// <exception cref="NullReferenceException" />
        /// <returns>RMAdaptorsCollection containing all 
        /// Riskmaster Business Adaptors required for dependency 
        /// injection by the Web Service</returns>
        public static RMAdaptersCollection GetRMAdaptors()
        {
            RMAdaptersSection rmAdaptorsSection = ConfigurationManager.GetSection(Global.RiskmasterAdaptors) as RMAdaptersSection;

            CheckConfigurationSection(rmAdaptorsSection, Global.RiskmasterAdaptors);

            return rmAdaptorsSection.RMAdapters;
        } // method: GetRMAdaptors

        /// <summary>
        /// Gets the list of Riskmaster Business Adaptors used for Extensibility
        /// </summary>
        /// <exception cref="NullReferenceException" />
        /// <returns>RMAdaptorsCollection containing all 
        /// Riskmaster Business Adaptors required for dependency 
        /// injection by the Web Service</returns>
        public static RMAdaptersCollection GetRMExtensibilityAdaptors()
        {
            RMExtensibilitySection rmExtensibilityAdaptorsSection = ConfigurationManager.GetSection(Global.Extensibility) as RMExtensibilitySection;

            CheckConfigurationSection(rmExtensibilityAdaptorsSection, Global.Extensibility);

            return rmExtensibilityAdaptorsSection.RMExtensibilityAdapters;
        } // method: GetRMExtensibilityAdaptors

        /// <summary>
        /// Gets the list of Riskmaster Forms used for Extensibility
        /// </summary>
        /// <exception cref="NullReferenceException" />
        /// <returns>RMExtensibilityCollection containing all 
        /// Riskmaster Extensibility Forms required for dependency 
        /// injection by the Web Service</returns>
        public static RMExtensibilityCollection GetRMExtensibilityForms()
        {
            RMExtensibilitySection rmExtensibilityFormsSection = ConfigurationManager.GetSection(Global.Extensibility) as RMExtensibilitySection;

            CheckConfigurationSection(rmExtensibilityFormsSection, Global.Extensibility);

            return rmExtensibilityFormsSection.RMFormMapping;
        } // method: GetRMExtensibilityForms


        /// <summary>
        /// Verifies whether or not a specified configuration section exists
        /// </summary>
        /// <param name="objConfigSection">object instance of the configuration section</param>
        /// <param name="strSectionName">string containing the name of the configuration section</param>
        internal static void CheckConfigurationSection(object objConfigSection, string strSectionName)
        {
            if (objConfigSection == null)
            {
                throw new ConfigurationErrorsException(string.Format("Specified configuration section does not exist or has been removed: {0}", strSectionName));
            }//if
        }//method: CheckConfigurationSection();



        /// <summary>
        /// Gets a DataTable of RMAdapter elements
        /// </summary>
        /// <returns>DataTable containing all available Adapter attributes</returns>
        /// <remarks>This method is created exclusively for reducing coupling
        /// to the RMConfigurator class and specifically the type casting
        /// required for RMAdapterCollections and Elements. 
        /// The DataTable was chosen due to problems with merging 
        /// multiple collections as is the case
        /// with the NameValueCollection, StringDictionary or Generic String Dictionary
        /// collections.
        /// </remarks>
        public static DataTable GetRMAdaptorsCollection()
        {
            DataTable dtAdapters = new DataTable();

            dtAdapters.Columns.Add("name");
            dtAdapters.Columns.Add("assembly");
            dtAdapters.Columns.Add("class");
            dtAdapters.Columns.Add("method");
            dtAdapters.Columns.Add("bypasssecurity");
            dtAdapters.Columns.Add("trustedonly");

            //Dictionary<string, string> objDictAdapterColl = new Dictionary<string, string>();
            NameValueCollection objDictAdapterColl = new NameValueCollection();

            //Build each of the individual collections
            RMAdaptersCollection rmAdapterColl = GetRMAdaptors();

            //Build the DataTable containing all of the base Business Adapters
            BuildRMAdapterCollection(rmAdapterColl, ref dtAdapters);

            RMAdaptersCollection rmExtensibleAdapterColl = GetRMExtensibilityAdaptors();

            //Build the DataTable containing all of the extensible Business Adapters
            BuildRMAdapterCollection(rmExtensibleAdapterColl, ref dtAdapters);

            return dtAdapters;
        }//method: GetRMAdaptorsCollection

        /// <summary>
        /// Builds a DataTable with the number of rows provided
        /// in the RMAdapters Collection
        /// </summary>
        /// <param name="rmAdapterColl">Collection of RiskmasterAdapters</param>
        /// <param name="dtAdapters">DataTable containing the current set of Business Adapters</param>
        internal static void BuildRMAdapterCollection(RMAdaptersCollection rmAdapterColl, ref DataTable dtAdapters)
        {

            foreach (RMAdapterElement rmAdapter in rmAdapterColl)
            {
                DataRow dtRow = dtAdapters.NewRow();

                dtRow["name"] = rmAdapter.Name;
                dtRow["assembly"] = rmAdapter.Assembly;
                dtRow["class"] = rmAdapter.Class;
                dtRow["method"] = rmAdapter.Method;
                dtRow["bypasssecurity"] = rmAdapter.BypassSecurity.ToString();
                dtRow["trustedonly"] = rmAdapter.TrustedOnly.ToString();

                dtAdapters.Rows.Add(dtRow);
            } // foreach
        }//method: BuildRMAdapterCollection 
        #endregion




    }
}
