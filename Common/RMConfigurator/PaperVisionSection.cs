﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Reflection;

namespace Riskmaster.Common
{
    /// <summary>
    /// /// ************************************
    /// Author : Animesh Sahai
    /// ************************************
    /// ConfigurationSection class for PaperVision settings
    /// </summary>
    /// <remarks>This class is not inherited from ConfigurationSection
    /// since it naturally derives from the NameValueSection class</remarks>
    public class PaperVisionSection
    {
        #region Class Constructor
        /// <summary>
        /// Default class constructor
        /// </summary>
        public PaperVisionSection()
        {

        } // constructor 
        #endregion

        #region Static Public Properties
        public static string PVServerAddress { get; set; }
        public static string LoginUrl { get; set; }
        public static string DocumentLink { get; set; }
        public static string EntityID { get; set; }
        public static string ProjectID { get; set; }
        //Animesh Inserted for MITS 18345
        public static string SharedNetworkLocationPath { get; set; }
        public static string DomainName { get; set; }
        public static string UserID { get; set; }
        public static string Password { get; set; }
        //Animesh Insertion ends
        private static string m_sConnString = string.Empty;
        private static int m_iClientId = 0;
        #endregion
        /// <summary>
        /// ************************************
        /// Author : Animesh Sahai
        /// ************************************
        /// Populates all of the PaperVision Section 
        /// static properties through Reflection
        /// by reading the values from the configuration file
        /// </summary>
        public static void GetPaperVisionSection()
        {
            NameValueCollection nvCollAcrosoft = RMConfigurationManager.GetSectionSettings("PaperVision",m_sConnString, m_iClientId);

            PaperVisionSection objPaperVisionSection = new PaperVisionSection();

            Type typPaperVisionSection = Type.GetType("Riskmaster.Common.PaperVisionSection");

            PropertyInfo[] objProps = typPaperVisionSection.GetProperties();

            foreach (PropertyInfo objProp in objProps)
            {
                objProp.SetValue(objPaperVisionSection, nvCollAcrosoft[objProp.Name], null);
            }//foreach
        } // method: GetPaperVisionSection

    } // class
}//namespace
