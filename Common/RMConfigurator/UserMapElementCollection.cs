using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

///************************************************************** 
///* $File		: UserMapElementCollection.cs
///* $Date		: Jan-25-2009 
///* $Author	: rahul solanki
///* $Comment	: 
///* $Source	: 
///**************************************************************
/// <summary>	
/// Provides classes for reading custom config file 
/// mapping RMX login id's to MCM login id's
/// </summary>
namespace Riskmaster.Common
{
	public class UserMapElementCollection: ConfigurationElementCollection
	{
		#region Constructor
		public UserMapElementCollection()
		{
		}
		#endregion

		#region Properties
		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				return ConfigurationElementCollectionType.BasicMap;
			}
		}
		protected override string ElementName
		{
			get
			{
                return "UserMap";
			}
		}

		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				return new ConfigurationPropertyCollection();
			}
		}
		#endregion

		#region Indexers
        public UserMapElement this[int index]
		{
			get
			{
                return (UserMapElement)base.BaseGet(index);
			}
			set
			{
				if (base.BaseGet(index) != null)
				{
					base.BaseRemoveAt(index);
				}
				base.BaseAdd(index, value);
			}
		}

        public UserMapElement this[string name]
		{
			get
			{
                return (UserMapElement)base.BaseGet(name);
			}
		}
		#endregion

		#region Methods
        public void Add(UserMapElement item)
		{
			base.BaseAdd(item);
		}

        public void Remove(UserMapElement item)
		{
			base.BaseRemove(item);
		}

		public void RemoveAt(int index)
		{
			base.BaseRemoveAt(index);
		}
		#endregion

		#region Overrides
		protected override ConfigurationElement CreateNewElement()
		{
            return new UserMapElement();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
            return (element as UserMapElement).RmxUser;
		}
		#endregion
	}
}
