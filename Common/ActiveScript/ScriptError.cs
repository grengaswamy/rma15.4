﻿// Copyright (c) 2008 Eric Eicke
//
// Permission is hereby granted, free of charge, to any person obtaining a 
// copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the 
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included 
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
// THE SOFTWARE.
//
using System;
//using System.Scripting.ActiveScript;

namespace Riskmaster.ActiveScript
{
    public class ScriptError
    {
        private System.Runtime.InteropServices.ComTypes.EXCEPINFO _exception;
        private uint _sourcecontext = 0;
        private uint _linenumber = 0;
        private int _characterposition = 0;
        private string _sourceline = "";

        public ScriptError(System.Runtime.InteropServices.ComTypes.EXCEPINFO exception, uint sourcecontext, uint linenumber, int characterposition, string sourceline)
        {            
            _exception = exception;
            _sourcecontext = sourcecontext;
            _linenumber = linenumber;
            _characterposition = characterposition;
            _sourceline = sourceline;
        }

        public System.Runtime.InteropServices.ComTypes.EXCEPINFO Exception
        {
            get
            {
                return _exception;
            }
        }

        public uint SourceContext
        {
            get
            {
                return _sourcecontext;
            }
        }

        public uint LineNumber
        {
            get
            {
                return _linenumber;
            }
        }

        public int CharacterPosition
        {
            get
            {
                return _characterposition;
            }
        }

        public string SourceLine
        {
            get
            {
                return _sourceline;
            }
        }

        public string Description
        {
            get
            {
                return _exception.bstrDescription;
            }
        }
    }
}
