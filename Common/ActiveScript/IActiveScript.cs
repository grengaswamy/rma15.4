﻿// Copyright (c) 2008 Eric Eicke
//
// Permission is hereby granted, free of charge, to any person obtaining a 
// copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the 
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included 
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
// THE SOFTWARE.
//
using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
//using System.Scripting.ActiveScript;

namespace Riskmaster.ActiveScript
{
    [ComVisible(true)]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid("BB1A2AE1-A4F9-11cf-8F20-00805F2CD064")]
    public interface IActiveScript
    {        
        void SetScriptSite([In, MarshalAs(UnmanagedType.Interface)]IActiveScriptSite site);
        void GetScriptSite(ref Guid riid, out IntPtr ppvObject);
        void SetScriptState(ScriptState ss);
        void GetScriptState(out ScriptState ss);
        void Close();
        void AddNamedItem([In, MarshalAs(UnmanagedType.BStr)] string name, [In, MarshalAs(UnmanagedType.U4)] uint flags);
        void AddTypeLib(ref Guid rguidTypeLib, uint major, uint minor, uint flags);
        void GetScriptDispatch(string itemName, [Out, MarshalAs(UnmanagedType.IDispatch)]out object ppdisp);
        void GetCurrentScriptThreadiD(out uint id);
        void GetScriptThreadID(uint threadID, out uint id);
        void GetScriptThreadState(uint id, out ScriptThreadState state);
        void InterruptScriptThread(uint id, ref System.Runtime.InteropServices.ComTypes.EXCEPINFO info, uint flags);
        void Clone(out IActiveScript item);
    }
}