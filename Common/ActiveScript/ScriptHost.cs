﻿// Copyright (c) 2008 Eric Eicke
//
// Permission is hereby granted, free of charge, to any person obtaining a 
// copy of this software and associated documentation files (the "Software"), 
// to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the 
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included 
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
// THE SOFTWARE.
//
using System;
using System.Collections.Generic;
//using System.Scripting.ActiveScript;

namespace Riskmaster.ActiveScript
{                 
    public class ScriptHost : IActiveScriptSite, IActiveScriptSiteWindow
    {
        #region Private Class
        private class NamedObjectEntry
        {
            private string _name = "";
            private object _object = null;

            public NamedObjectEntry(string name, object o)
            {
                _name = name;
                _object = o;
            }

            public string Name
            {
                get
                {
                    return _name;
                }
            }

            public object Object
            {
                get
                {
                    return _object;
                }
            }
        }        
        #endregion

        private object _engine = null;
        private IntPtr _handle = IntPtr.Zero;
        IActiveScript _activescript = null;
        IActiveScriptParse _activeparser = null;

        private Dictionary<string, NamedObjectEntry> _namedobjects = new Dictionary<string, NamedObjectEntry>();
        private List<string> _scripts = new List<string>();
        private bool _opened = false;
        private List<ScriptError> _errors = new List<ScriptError>();


        public ScriptHost(object engine) 
            : this(engine,IntPtr.Zero)
        {
        }
        
        public ScriptHost(object engine, IntPtr handle)
        {
            _handle = handle;
            _engine = engine;            
        }

        public List<ScriptError> Errors
        {
            get
            {
                return _errors;
            }
        }

        public void AddObject(string name, object o)
        {            
            if (!_namedobjects.ContainsKey(name.ToLower()))
            {
                ScriptState state = ScriptState.Closed;

                if (_activescript != null)
                {
                    _activescript.GetScriptState(out state);
                }

                if (state == ScriptState.Connected)
                {
                    AddNamedObject(name);
                }                
                
                _namedobjects.Add(name.ToLower(), new NamedObjectEntry(name,o));
            }            
        }

        public void AddScript(string script)
        {
            ScriptState state = ScriptState.Closed;

            if (_activescript != null)
            {
                _activescript.GetScriptState(out state);
            }

            if (state == ScriptState.Connected)
            {
                ParseScript(script);                
            }
            else
            {
                _scripts.Add(script);
            }            
        }

        public void Open()
        {
            
                if (!_opened)
                {
                    _activescript = _engine as IActiveScript;
                    try
                    {
                    _activescript.SetScriptSite(this);
                     }
                    catch (Exception Ex)
                    {

                    }
                    _activeparser = _engine as IActiveScriptParse;

                    _activeparser.InitNew();

                    foreach (string name in _namedobjects.Keys)
                    {
                        AddNamedObject(_namedobjects[name].Name);
                    }

                    foreach (string script in _scripts)
                    {
                        ParseScript(script);
                    }

                    _activescript.SetScriptState(ScriptState.Connected);
              

                _opened = true;
            }
            
        }                      

        public object ExecuteMethod(string methodname, object[] parameters)
        {
            object retval = null;
            object disp = new object();

            _activescript.GetScriptDispatch(null, out disp);

            if (disp != null)
            {
                Type disptype = disp.GetType();

                retval = disp.GetType().InvokeMember(methodname, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.InvokeMethod, null, disp, parameters);

                System.Runtime.InteropServices.Marshal.ReleaseComObject(disp);
            }

            return retval;
        }

        public void Close()
        {
            if ((_opened) && (_activescript != null))
            {
                _activescript.SetScriptState(ScriptState.Disconnected);
                _activescript.Close();                        
            }

            _activescript = null;
            _activeparser = null;
            _opened = false;
        }

        #region IActiveScriptSite Members

        
        public void GetLCID(out uint id)
        {
            throw new NotImplementedException();
        }

        public void GetItemInfo(string name, uint returnMask, [System.Runtime.InteropServices.Out, System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.IUnknown)] out object item, IntPtr ppti)
        {
            item = null;

            if (_namedobjects.ContainsKey(name.ToLower()))
            {
                item = _namedobjects[name.ToLower()].Object;

                if (ppti != IntPtr.Zero)
                {
                    System.Runtime.InteropServices.Marshal.WriteIntPtr(ppti, System.Runtime.InteropServices.Marshal.GetITypeInfoForType(item.GetType()));
                }
            }
        }

        public void GetDocVersionString(out string v)
        {
            throw new NotImplementedException();
        }

        public void OnScriptTerminate(ref object result, ref System.Runtime.InteropServices.ComTypes.EXCEPINFO info)
        {
            if (info.scode > 0)
            {
                _errors.Add(new ScriptError(info, 0, 0, -1, ""));   
            }
        }

        public void OnStateChange(ScriptState state)
        {
            
        }

        public void OnScriptError(object err)
        {
            IActiveScriptError error = err as IActiveScriptError;

            System.Runtime.InteropServices.ComTypes.EXCEPINFO exception;
            uint sourcecontext = 0;
            uint linenumber = 0;
            int characterposition = 0;
            string sourceline = "";

            error.GetExceptionInfo(out exception);
            error.GetSourcePosition(out sourcecontext,out linenumber, out characterposition);
            error.GetSourceLineText(out sourceline);

            _errors.Add(new ScriptError(exception,sourcecontext,linenumber,characterposition,sourceline));

            System.Runtime.InteropServices.Marshal.ReleaseComObject(error);
        }

        public void OnEnterScript()
        {

        }

        public void OnLeaveScript()
        {
            
        }

        #endregion

        #region IActiveScriptSiteWindow Members

        public void GetWindow(out IntPtr phwnd)
        {
            phwnd = _handle;
        }

        public void EnableModeless([System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.Bool)]bool fEnable)
        {
            
        }

        #endregion

        private void AddNamedObject(string name)
        {
            _activescript.AddNamedItem(name, (uint)(ScriptItemFalgs.IsVisible | ScriptItemFalgs.GlobalMembers));
        }

        private void ParseScript(string script)
        {
            System.Runtime.InteropServices.ComTypes.EXCEPINFO exception;

            try
            {
                _activeparser.ParseScriptText(script, null, IntPtr.Zero, null, 0, 0, (uint)(ScriptItemFalgs.IsPersistent | ScriptItemFalgs.IsVisible), IntPtr.Zero, out exception);

                if (exception.scode > 0)
                {
                    _errors.Add(new ScriptError(exception, 0, 0, -1, ""));   
                }
            }
            catch (System.Runtime.InteropServices.COMException cex)
            {
                //If the HRESULT is --2147352319(0x80020101) it means it was already reported in
                //OnScriptError.
                if (cex.ErrorCode != -2147352319)
                {
                    throw;
                }
            }
        } 
    }
}
