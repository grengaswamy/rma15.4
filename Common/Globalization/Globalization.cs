using System;
using System.Resources;
using System.Collections;
using System.Reflection;
using Riskmaster.Db;
using System.Configuration;
using System.Diagnostics;
using System.Linq;

namespace Riskmaster.Common
{
	/// <summary>
	/// Riskmaster.Common.Globalization provides access to methods to ease internationalization efforts.</summary>
	/// <remarks>none</remarks>
	public class Globalization
	{
        //Start rsushilaggar Jira 11730
        public enum BusinessGlossaryTableNames
        {
            ATTORNEY_FIRMS,
            BANKS,
            BILL_REV_CM_EID,
            BROKER_FIRM,
            CLAIM_ADMIN_TPA,
            CONTRACTOR,
            DEFAULT_PARENT,
            FINANCE_COMPANY,
            HOSPITAL,
            INSURERS,
            LEASE_COMPANY,
            MANUFACTURERS,
            MCO,
            MMSEA_PRTYENT_TYPE,
            OSHA_ESTABLISHMENT,
            PHYS_EDUC_INSTITUT,
            POLICE_AGENCY,
            PROVIDER,
            REHAB_JOB_FIRMS,
            REHAB_PROV_FIRMS,
            SALVAGE_YARD,
            SI_CERTIFICATE,
            ST_AGENCY_LOCATION,
            STATE_AGENCY,
            TAX_ENTITY,
            VENDOR,
            WC_DEF_CLAIM_ADMIN
        }

        public enum IndividualGlossaryTableNames
        {
            ADDITIONAL_INSURED,
            ADJUSTERS,
            AGENTS,
            ANESTHETISTS,
            ATTORNEYS,
            BROKER,
            CASE_MANAGER,
            DEPENDENTS,
            DRIVER_INSURED,
            DRIVER_OTHER,
            DRIVERS,
            EMPLOYEES,
            EXPERT_WITNESS,
            GUARDIAN_TYPE,
            INCLUDED_OFFICERS,
            INJURED,
            INSURED,
            JUDGES,
            LEAVE_RB_ENTITY,
            LEGAL_ADDRESS,
            LEGAL_ENTITY,
            LOSS_PAYEE,
            MAN_NOT_INSURED,
            MEDIATOR,
            MEDICAL_STAFF,
            MMSEA_CLMPRTY_TYPE,
            MORTGAGEE,
            NAMED_INSURED,
            OTHER_EMPLOYEES,
            OTHER_MEDICAL_STAFF,
            OTHER_PATIENTS,
            OTHER_PEOPLE,
            OTHER_PHYSICIANS,
            OTHER_WITNESS,
            OUT_OF_BUSINESS,
            OWNER_NOT_INSURED,
            PASSENGER,
            PATIENTS,
            PHYSICIANS,
            POLICY_INSURED,
            POWOFATTORNEY_TYPE,
            REHAB_JOB_PROVIDER,
            REHAB_PROVIDER,
            REPORTED_BY,
            SYSTEM_USERS,
            WITNESS	

        }

        public enum PersonInvolvedGlossaryTableNames
        {
            EMPLOYEE,
            MEDSTAFF,
            DRIVER,
            PATIENT,
            PHYSICIAN,
            WITNESS,
            OTHERPERSON
        }

        // End RMA-11730
		/// <summary>
		/// m_resourceManagers stores a pool of resource managers to ease internationalization efforts.
		/// Using this method to get a ResourceManager in Riskmaster code 
		/// minimizes memory requirements.</summary>
		static protected Hashtable m_resourceManagers=new Hashtable();
        
		/// <summary>
		/// Riskmaster.Common.Globalization.ResourceManager provides simplified access to a single
		/// ResourceManager object to ease internationalization efforts.</summary>
		/// <remarks>none</remarks>
		public static ResourceManager ResourceManager
		{
			get
			{
			
				Assembly assembly = Assembly.GetCallingAssembly();
				string s = assembly.FullName;
				s = s.Split(',')[0];
				s +=".Global";
				ResourceManager mgr = (ResourceManager)(m_resourceManagers[assembly]);
			
				if(mgr == null)
				{
					mgr = new ResourceManager(s, assembly);
					m_resourceManagers[assembly] = mgr;
				}
				return mgr;
			}
		}	
		/// <summary>
		/// Riskmaster.Common.Globalization.GetResourceManager provides internal access to a single
		/// ResourceManager object for internal use by the GetString and GetObject static methods.</summary>
		private static ResourceManager GetResourceManager(Assembly assembly) 
		{
			string s = assembly.FullName;
			s = s.Split(',')[0];
			s +=".Global";
			ResourceManager mgr = (ResourceManager)(m_resourceManagers[assembly]);
			if(mgr == null)
			{
				mgr = new ResourceManager(s, assembly);
				m_resourceManagers[assembly] = mgr;
			}
			return mgr;
		}

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetString
        /// </summary>     
        public static string GetString(string resourceName, string sLangCode = "")
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                return GetString(resourceName, 0, sLangCode);
            }
            else
                throw new Exception("GetString" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
		/// <summary>
		/// Riskmaster.Common.Globalization.GetString provides simplified resource string retrieval 
		/// for all Riskmaster assemblies.</summary>
		public static string GetString(string resourceName,int iClientId,string sLangCode="")
		{
            if (sLangCode == string.Empty)
                sLangCode = ConfigurationSettings.AppSettings["RMABaseLangCodeWithCulture"].Split('|')[0].ToString();
            //Deb ML Changes
            string resourceValue = GlobalResource.GetResource(resourceName,iClientId);
            resourceValue = FilterBusinessMessage(resourceValue, sLangCode);
            //if (string.IsNullOrEmpty(resourceValue))
            //{
            //    resourceValue = GetResourceManager(Assembly.GetCallingAssembly()).GetString(resourceName);
            //}
            return resourceValue;
            //string resvalue = GetResourceManager(Assembly.GetCallingAssembly()).GetString(resourceName);
            //return resvalue;
			//return GetResourceManager(Assembly.GetCallingAssembly()).GetString(resourceName);
            //Deb ML Changes
		}

        /// <summary>
        /// Update the error message according to the language code
        /// </summary>
        /// <param name="p_sKeyValue"></param>
        /// <param name="p_sLangCode"></param>
        /// <returns></returns>
        public static string FilterBusinessMessage(string p_sKeyValue, string p_sLangCode)
        {
            string[] sDataSeparator = { "~*~" };
            string[] ArrDataSeparator = { "|^|" };
            string[] ArrErrMsg = p_sKeyValue.Split(sDataSeparator, StringSplitOptions.None);
            string sReturnMsg = string.Empty;
            string sKeyId = string.Empty;
            try
            {
                if (!p_sKeyValue.Contains("|^|"))
                {
                    return p_sKeyValue;
                }
                string sLangCode = p_sLangCode;
                foreach (string s in ArrErrMsg)
                {
                    if (s.Contains(sLangCode))
                    {
                        sReturnMsg = s;
                        break;
                    }
                }
                if (string.IsNullOrEmpty(sReturnMsg))
                {
                    //used ConfigurationSettings.AppSettings instead of RMConfigurationManager.GetAppSetting because referencing RMConfigurator.dll was causing cyclic reference issue.
                    sLangCode=ConfigurationSettings.AppSettings["RMABaseLangCodeWithCulture"].Split('|')[0].ToString();
                   // sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
                    foreach (string s in ArrErrMsg)
                    {
                        if (s.Contains(sLangCode))
                        {
                            sReturnMsg = s;
                            break;
                        }
                    }
                }
                string[] sLangSeparator = { sLangCode };
                string[] sArr = sReturnMsg.Split(sLangSeparator, StringSplitOptions.None);
                if (sArr.Length > 1)
                {
                    sKeyId = sArr[0].Replace("|^|", "");
                    sReturnMsg = sArr[sArr.Length - 1].Replace("|^|", "");
                }

            }
            catch (Exception ee)
            {

            }
            return (!string.IsNullOrEmpty(sKeyId) ? sReturnMsg : p_sKeyValue);
        }

        //public static string GetString(string resourceName)
        //{
        //    //Deb ML Changes
        //    string resourceValue = GlobalResource.GetResource(resourceName, 0);
        //    //if (string.IsNullOrEmpty(resourceValue))
        //    //{
        //    //    resourceValue = GetResourceManager(Assembly.GetCallingAssembly()).GetString(resourceName);
        //    //}
        //    return resourceValue;
        //    //string resvalue = GetResourceManager(Assembly.GetCallingAssembly()).GetString(resourceName);
        //    //return resvalue;
        //    //return GetResourceManager(Assembly.GetCallingAssembly()).GetString(resourceName);
        //    //Deb ML Changes
        //}		
		/// <summary>
		/// Riskmaster.Common.Globalization.GetObject provides simplified resource object retrieval 
		/// for all Riskmaster assemblies.</summary>
		public static object GetObject(string resourceName)
		{
			return GetResourceManager(Assembly.GetCallingAssembly()).GetObject(resourceName);
		}

        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for GetLocalResourceString
        /// </summary>     
        public static string GetLocalResourceString(string resourceName, int resourceType, string sPageId )
        {
            if (CheckCallingStack("Riskmaster.Scripting"))
            {
                return  GetLocalResourceString( resourceName, resourceType, sPageId, 0);
            }
            else
                throw new Exception("GetLocalResourceString" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486
        /// <summary>
        /// Riskmaster.Common.Globalization.GetLocalResourceString provides simplified resource string retrieval from LOCAL_RESOURCE Table
        /// for all Riskmaster assemblies.</summary>
        public static string GetLocalResourceString(string resourceName,int resourceType,string sPageId,int iClientId)
        {
            //rkulavil -  ML Changes
            string resourceValue = LocalResource.GetResource(resourceName,resourceType,sPageId,iClientId);
            return resourceValue;
            //rkulavil -  ML Changes
        }

        /// <summary>
        // This function verifies whether assembly name (passed as argument) was called or not, after ProcessRequest() funtion of RiskmasterService assembly
        // For example, When argument passed is "Scripting", then function verifies whether Custom Script is calling assembly or not.
        /// <summary>
        //mbahl3
        public static bool CheckCallingStack(string Key)
        {
            StackTrace stackTrace = new StackTrace();
            StackFrame[] frames = stackTrace.GetFrames();
            string sServiceAssembly = "RiskmasterService";

            var script = frames.FirstOrDefault(s => s.GetMethod().DeclaringType.Assembly.ToString().Contains(Key) || s.GetMethod().DeclaringType.Assembly.ToString().Contains(sServiceAssembly));
            return script.GetMethod().DeclaringType.Assembly.ToString().Contains(Key);
            //mbahl3 used linq instead of foreach

            //foreach (var stackFrame in frames)
            //{
            //    string ownerAssembly = stackFrame.GetMethod().DeclaringType.Assembly.ToString();
            //    if (ownerAssembly.Contains(Key))
            //        return true;
            //    else if (ownerAssembly.Contains(sServiceAssembly))
            //        return false;
            //}
            //return false;
        }

        //mbahl3 scripts work jira RMA-8486

        //rkotak:9681, 4608 starts
        /// <summary>
        /// Get Page Id from Page Name
        /// </summary>
        /// <param name="sPageName">For example, FinacialHistoryDetail.aspx</param>
        /// <param name="iClientId">Client ID</param>
        /// <returns>Page ID</returns>
        public static string GetPageId(string sPageName, int iClientId)
        {
            string resourceValue = LocalResource.GetPageId(sPageName, iClientId);
            return resourceValue;
        }
        //rkotak:9681,4608 ends
	}

}
