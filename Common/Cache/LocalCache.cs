using System;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using System.Collections;
using System.Text.RegularExpressions;
using System.Text;
using System.Xml;
using System.IO;
using System.Collections.Specialized;
using Riskmaster.Cache;



namespace Riskmaster.Common
{
    #region Class LocalCache Implementation
    /// <summary>
    /// This component of Common was broken out of Riskmaster.Common to avoid a circular
    /// dependency with the Database layer code which this class consumes.
    /// This class provides a simple local in memory cache of previously requested database
    /// info (codes, etc.)
    /// </summary>
    public class LocalCache : IDisposable
    {

        private bool isDisposed = false;
        protected CacheTable m_Cache = null;
        protected DynCacheTable m_DynCache = null;
        private int m_iClientId = 0;
        public string LanguageCode
        {
            get;
            set;
        }
        /// <summary>
        /// Gets and sets the database connection string
        /// </summary>
        public string DbConnectionString
        {
            get;
            set;
        } // property DbConnectionString

        /// <summary>
        /// OVERLOADED class constructor
        /// </summary>
        /// <param name="connectionString">string containing the database connection string</param>
        public LocalCache(string connectionString, int p_iClientId)
        {
            this.DbConnectionString = connectionString;
            m_iClientId = p_iClientId;
            m_Cache = new CacheTable(connectionString,p_iClientId);
            m_DynCache = new DynCacheTable(connectionString,p_iClientId);
        }//constructor

        
        /// <summary>
        /// Dispose method to destroy all open
        /// managed and unmanaged resources in the class
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Protected method following IDisposable pattern
        /// for destroying managed and unmanaged resources
        /// </summary>
        /// <param name="disposing">boolean indicating whether or not the Dispose method was explicitly
        /// called on the class/object</param>
        protected virtual void Dispose(bool disposing)
        {
            
            if (! isDisposed)    
            {
                if (disposing)
                {
                    //Cleanup managed objects
                    if (m_Cache != null)
                    {
                        m_Cache = null;
                    } // if
                    if (m_DynCache != null)
                    {
                        m_DynCache = null;
                    } // if
                } // if

            } // if
            isDisposed = true;
        } // method: Dispose


        /// <summary>
        /// Class destructor in the event that the Dispose method
        /// is not called on the class
        /// </summary>
        ~LocalCache()
        {
            Dispose(false);
        }//destructor

        public void Clear(int p_iClientId) 
        { 
            m_Cache = new CacheTable(this.DbConnectionString,p_iClientId); 
        }

        /// <summary>
        /// Gets Code IDs
        /// </summary>
        /// <param name="shortCode">string containing the short code</param>
        /// <param name="tableName">string containing the Table Name</param>
        /// <returns>integer containing the Code ID</returns>
        /// <remarks>returns a 0 for records which have NULL for Code IDs
        /// Ideally, all records, should have 0 for these additional records
        /// </remarks>
        public int GetCodeId(string shortCode, string tableName)
        {
            shortCode = shortCode.ToUpper();
            tableName = tableName.ToUpper();

            string sKey = String.Format("CID_{0}_{1}", shortCode, tableName);
            try
            {
                if (m_Cache.ContainsKey(sKey))
                    return (int)m_Cache[sKey];
                else
                {
                    int codeId = 0;
                    string SQL = String.Format(@"SELECT CODE_ID FROM CODES,GLOSSARY 
																				WHERE GLOSSARY.SYSTEM_TABLE_NAME='{0}' AND 
																				GLOSSARY.TABLE_ID=CODES.TABLE_ID AND 
																				UPPER(CODES.SHORT_CODE)='{1}'", tableName, shortCode);//Activity Log : Nshah8 upper for "Re" case. JIRA 6310
                    codeId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(this.DbConnectionString, SQL), m_iClientId);
                    m_Cache.Add(sKey, codeId);
                    return codeId;
                }
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("LocalCache.GetCodeId.Exception", m_iClientId), e); }
        }

        /// <summary> MITS MANISH
        /// Gets a Code ID based on a CheckStatus
        /// </summary>
        /// <param name="CheckStatus">string containing the CheckStatus</param>
        /// <returns></returns>
        /// <remarks>returns  Code IDs
        /// 
        /// </remarks>
        public int GetCheckStatus(string CheckStatus)
        {

            string sKey = String.Format("SID_{0}", CheckStatus);
            try
            {
                if (m_Cache.ContainsKey(sKey))
                    return (int)m_Cache[sKey];
                else
                {
                    int CodeId = 0;
                    string SQL = String.Format(@"SELECT CODE_ID  FROM CODES_TEXT WHERE upper(CODE_DESC) = '{0}'", CheckStatus);
                    CodeId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(this.DbConnectionString, SQL), m_iClientId);
                    m_Cache.Add(sKey, CodeId);
                    return CodeId;
                }
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("LocalCache.GetTableId.Exception", m_iClientId), e); }
        }

        /// <summary>
        /// Gets a Table ID based on a Table Name
        /// </summary>
        /// <param name="tableName">string containing the Table Name</param>
        /// <returns></returns>
        /// <remarks>returns a 0 for records which have NULL for Code IDs
        /// Ideally, all records, should have 0 for these additional records
        /// </remarks>
        public int GetTableId(string tableName)
        {
            tableName = tableName.ToUpper();
            string sKey = String.Format("TID_{0}", tableName);
            try
            {
                if (m_Cache.ContainsKey(sKey))
                    return (int)m_Cache[sKey];
                else
                {
                    int tableId = 0;
                    // rrachev JIRA RMA 4032 BEGIN
                    //string SQL = String.Format(@"SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME ='{0}'", tableName);
                    string SQL = String.Format(@"SELECT TABLE_ID FROM GLOSSARY WHERE Upper(SYSTEM_TABLE_NAME) ='{0}'", tableName);
                    // rrachev JIRA RMA 4032 END
                    tableId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(this.DbConnectionString, SQL), m_iClientId);
                    m_Cache.Add(sKey, tableId);
                    return tableId;
                }
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("LocalCache.GetTableId.Exception", m_iClientId), e); }
        }
        //R7 Perf Imp
        public bool IsScriptEditorEnabled()
        {
            string sKey = String.Format("SCRIPTEDITOR");
            bool bRetVal = false;
            DbConnection objConn = null;
            DbReader objReader = null;
            string sQuery = string.Empty;
            string sValue = string.Empty;
            try
            {
               if (m_Cache.ContainsKey(sKey))
                    return Conversion.ConvertStrToBool(m_Cache[sKey].ToString());
                else
                {
                    sQuery = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='SCRPT_EDI_FLAG'";
                    objConn = DbFactory.GetDbConnection(DbConnectionString);
                    objConn.Open();
                    objReader = objConn.ExecuteReader(sQuery);
                    if (objReader.Read())
                    {
                        sValue = objReader.GetString(0);
                        bRetVal = Conversion.ConvertStrToBool(sValue);                  
                    }

                }

            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.UpdateCacheforSystemSettings", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    //if(objConn.State==System.Data.ConnectionState.Open)
                    //{
                    //	objConn.Close();
                    objConn.Dispose();
                    //}
                }
            }
            m_Cache.Add(sKey, sValue);
            return bRetVal;
        }
        //R7 Perf Imp
        public bool IsCaseManagementEnabled()
        {
            string sKey = String.Format("CASEMGMT");
            bool bRetVal = false;
            DbConnection objConn = null;
            DbReader objReader = null;
            string sQuery = string.Empty;
            string sValue = string.Empty;
            try
            {
                if (m_Cache.ContainsKey(sKey))
                    return Conversion.ConvertStrToBool(m_Cache[sKey].ToString());
                else
                {
                    sQuery = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='CASE_MGT_FLAG'";
                    objConn = DbFactory.GetDbConnection(DbConnectionString);
                    objConn.Open();
                    objReader = objConn.ExecuteReader(sQuery);
                    if (objReader.Read())
                    {
                        sValue = Conversion.ConvertObjToStr(objReader[0]);
                        if (objReader[0].ToString() == "-1")
                        {
                            bRetVal= true;
                        }
                        else
                        {
                            bRetVal= false;
                        }                       
                    }

                }

            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.UpdateCacheforSystemSettings", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    //if(objConn.State==System.Data.ConnectionState.Open)
                    //{
                    //	objConn.Close();
                    objConn.Dispose();
                    //}
                }
            }
            m_Cache.Add(sKey, sValue);
            return bRetVal;
        }
        /// <summary>
        /// Gets the Short Code based on the specified Code ID
        /// </summary>
        /// <param name="codeId">integer containing the Code ID from the database</param>
        /// <returns>the short code from the database</returns>
        public string GetShortCode(int codeId)
        {
            string sKey = String.Format("CID_{0}", codeId);
            string strShortCode = string.Empty;
            const string EMPTY_DOUBLE_QUOTES = "\" \"";

            if (m_Cache.ContainsKey(sKey))
                return (string)m_Cache[sKey];
            else
            {
                string shortCode = string.Empty; //Initialize the short code to an empty string
                string SQL = String.Format(@"SELECT SHORT_CODE FROM CODES WHERE CODE_ID={0}", codeId);
                strShortCode = DbFactory.ExecuteScalar(this.DbConnectionString, SQL) as string;

                //igupta3 Mits:25259 Checking the condition for space from DB
                strShortCode = strShortCode == "\"\"" ? "\" \"" : strShortCode;
                //igupta3

                if (!string.IsNullOrEmpty(strShortCode))
                {
                    //Verify that the string is not equal to a pair of double quotes
                    if (!strShortCode.Equals(EMPTY_DOUBLE_QUOTES))
                    {
                        shortCode = strShortCode.Trim();
                    }//if
                }//if
                m_Cache.Add(sKey, shortCode);
                return shortCode;
            }//else
        }//method: GetShortCode()


        //Changed by Gagan for MITS 10376 : Start

        /// <summary>
        /// Returns width for the control
        /// </summary>
        /// <param name="sControl">Control for which width is needed</param>
        /// <returns></returns>
        public int GetWidth(string sControl)
        {
            sControl = sControl.ToUpper();

            string sKey = String.Format("WIDTH_{0}", sControl);

            try
            {
                if (m_Cache.ContainsKey(sKey))
                    return (int)m_Cache[sKey];
                else
                {
                    int iTextMLCols = 0;
                    int iTextMLRows = 0;
                    int iFreeCodeCols = 0;
                    int iFreeCodeRows = 0;
                    int iReadOnlyMemoCols = 0;
                    int iReadOnlyMemoRows = 0;
                    int iMemoCols = 0;
                    int iHtmlCols = 0;
                    int iMemoRows = 0;
                    int iHtmlRows = 0;
                    XmlDocument objDocument = null;
                    string sSQL = String.Empty;
                    string sXmlFile = string.Empty;



                    sXmlFile = RMSessionManager.GetCustomSettings(m_iClientId);

                    objDocument = new XmlDocument();
                    objDocument.LoadXml(sXmlFile);

                    SetRowCol(objDocument, ref iTextMLCols, ref iTextMLRows, "//TextML/");
                    SetRowCol(objDocument, ref iFreeCodeCols, ref iFreeCodeRows, "//FreeCode/");
                    SetRowCol(objDocument, ref iReadOnlyMemoCols, ref iReadOnlyMemoRows, "//ReadOnlyMemo/");
                    SetRowCol(objDocument, ref iMemoCols, ref iMemoRows, "//Memo/");
                    SetRowCol(objDocument, ref iHtmlCols, ref iHtmlRows, "//HtmlText/");//asharma326 JIRA 6422

                    m_Cache.Add("WIDTH_TEXTML", iTextMLCols);
                    m_Cache.Add("WIDTH_FREECODE", iFreeCodeCols);
                    m_Cache.Add("WIDTH_READONLYMEMO", iReadOnlyMemoCols);
                    m_Cache.Add("WIDTH_MEMO", iMemoCols);
                    m_Cache.Add("WIDTH_HTMLTEXT", iHtmlCols);//asharma326 JIRA 6422

                    m_Cache.Add("HEIGHT_TEXTML", iTextMLRows);
                    m_Cache.Add("HEIGHT_FREECODE", iFreeCodeRows);
                    m_Cache.Add("HEIGHT_READONLYMEMO", iReadOnlyMemoRows);
                    m_Cache.Add("HEIGHT_MEMO", iMemoRows);
                    m_Cache.Add("HEIGHT_HTMLTEXT", iHtmlRows);//asharma326 JIRA 6422

                    return (int)m_Cache[sKey];
                }
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("LocalCache.GetWidth.Exception", m_iClientId), e); }
        }


        /// <summary>
        /// Returns height for the control
        /// </summary>
        /// <param name="sControl">Control for which height is needed</param>
        /// <returns></returns>
        public int GetHeight(string sControl)
        {
            sControl = sControl.ToUpper();

            string sKey = String.Format("HEIGHT_{0}", sControl);
            try
            {
                if (m_Cache.ContainsKey(sKey))
                    return (int)m_Cache[sKey];
                else
                {
                    int iTextMLCols = 0;
                    int iTextMLRows = 0;
                    int iFreeCodeCols = 0;
                    int iFreeCodeRows = 0;
                    int iReadOnlyMemoCols = 0;
                    int iReadOnlyMemoRows = 0;
                    int iMemoCols = 0;
                    int iMemoRows = 0;
                    int iHtmlCols = 0;//asharma326 JIRA 6422
                    int iHtmlRows = 0;//asharma326 JIRA 6422
                    XmlDocument objDocument = null;
                    string sSQL = String.Empty;
                    string sXmlFile = string.Empty;


                    sXmlFile = RMSessionManager.GetCustomSettings(m_iClientId);

                    objDocument = new XmlDocument();
                    objDocument.LoadXml(sXmlFile);

                    SetRowCol(objDocument, ref iTextMLCols, ref iTextMLRows, "//TextML/");
                    SetRowCol(objDocument, ref iFreeCodeCols, ref iFreeCodeRows, "//FreeCode/");
                    SetRowCol(objDocument, ref iReadOnlyMemoCols, ref iReadOnlyMemoRows, "//ReadOnlyMemo/");
                    SetRowCol(objDocument, ref iMemoCols, ref iMemoRows, "//Memo/");
                    SetRowCol(objDocument, ref iMemoCols, ref iMemoRows, "//HtmlText/");//asharma326 JIRA 6422

                    m_Cache.Add("WIDTH_TEXTML", iTextMLCols);
                    m_Cache.Add("WIDTH_FREECODE", iFreeCodeCols);
                    m_Cache.Add("WIDTH_READONLYMEMO", iReadOnlyMemoCols);
                    m_Cache.Add("WIDTH_MEMO", iMemoCols);
                    m_Cache.Add("WIDTH_HTMLTEXT", iHtmlCols);//asharma326 JIRA 6422

                    m_Cache.Add("HEIGHT_TEXTML", iTextMLRows);
                    m_Cache.Add("HEIGHT_FREECODE", iFreeCodeRows);
                    m_Cache.Add("HEIGHT_READONLYMEMO", iReadOnlyMemoRows);
                    m_Cache.Add("HEIGHT_MEMO", iMemoRows);
                    m_Cache.Add("HEIGHT_HTMLTEXT", iHtmlRows);//asharma326 JIRA 6422

                    return (int)m_Cache[sKey];
                }
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("LocalCache.GetHeight.Exception", m_iClientId), e); }
        }

        /// <summary>
        /// An XML is maintained in appfiles which contain field names which are 
        /// required for reports and should be selected automatically for maintaining history
        /// This function will store that xml in cache
        /// </summary>
        /// <returns></returns>
        public XmlDocument GetXmlForHistTrack()
        {
            try
            {
                if (m_Cache.ContainsKey("HistoryTrackXml"))
                    return (XmlDocument)m_Cache["HistoryTrackXml"];
                else
                {
                    XmlDocument objDocument = null;
                    string sSQL = String.Empty;
                    string sXmlFile = string.Empty;
                    
                    sXmlFile = RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, @"HistoryTracking\MandatoryFieldsForReport.xml");

                    objDocument = new XmlDocument();
                    if (File.Exists(sXmlFile))
                    {
                        objDocument.Load(sXmlFile);
                        m_Cache.Add("HistoryTrackXml", objDocument);
                    }
                    return (XmlDocument)m_Cache["HistoryTrackXml"];
                }
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("LocalCache.GetXmlForHistTrack.Exception", m_iClientId), e); }
        }


        /// <summary>
        /// Updates width and height of controls in Local Cache
        /// </summary>
        /// <param name="iTextMLCols"></param>
        /// <param name="iTextMLRows"></param>
        /// <param name="iFreeCodeCols"></param>
        /// <param name="iFreeCodeRows"></param>
        /// <param name="iReadOnlyMemoCols"></param>
        /// <param name="iReadOnlyMemoRows"></param>
        /// <param name="iMemoCols"></param>
        /// <param name="iMemoRows"></param>
        public void UpdateWidthHeight(int iTextMLCols, int iTextMLRows, int iFreeCodeCols, int iFreeCodeRows, int iReadOnlyMemoCols, int iReadOnlyMemoRows, int iMemoCols, int iMemoRows)
        {
            try
            {
                if (m_Cache.ContainsKey("WIDTH_TEXTML"))
                    m_Cache["WIDTH_TEXTML"] = iTextMLCols;
                else
                    m_Cache.Add("WIDTH_TEXTML", iTextMLCols);

                if (m_Cache.ContainsKey("WIDTH_FREECODE"))
                    m_Cache["WIDTH_FREECODE"] = iFreeCodeCols;
                else
                    m_Cache.Add("WIDTH_FREECODE", iFreeCodeCols);

                if (m_Cache.ContainsKey("WIDTH_READONLYMEMO"))
                    m_Cache["WIDTH_READONLYMEMO"] = iReadOnlyMemoCols;
                else
                    m_Cache.Add("WIDTH_READONLYMEMO", iReadOnlyMemoCols);

                if (m_Cache.ContainsKey("WIDTH_MEMO"))
                    m_Cache["WIDTH_MEMO"] = iMemoCols;
                else
                    m_Cache.Add("WIDTH_MEMO", iMemoCols);


                if (m_Cache.ContainsKey("HEIGHT_TEXTML"))
                    m_Cache["HEIGHT_TEXTML"] = iTextMLRows;
                else
                    m_Cache.Add("HEIGHT_TEXTML", iTextMLRows);

                if (m_Cache.ContainsKey("HEIGHT_FREECODE"))
                    m_Cache["HEIGHT_FREECODE"] = iFreeCodeRows;
                else
                    m_Cache.Add("HEIGHT_FREECODE", iFreeCodeRows);

                if (m_Cache.ContainsKey("HEIGHT_READONLYMEMO"))
                    m_Cache["HEIGHT_READONLYMEMO"] = iReadOnlyMemoRows;
                else
                    m_Cache.Add("HEIGHT_READONLYMEMO", iReadOnlyMemoRows);

                if (m_Cache.ContainsKey("HEIGHT_MEMO"))
                    m_Cache["HEIGHT_MEMO"] = iMemoRows;
                else
                    m_Cache.Add("HEIGHT_MEMO", iMemoRows);
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("LocalCache.UpdateWidthHeight.Exception", m_iClientId), e); }
        }




        /// <summary>
        /// Sets control variables for columns and Rows
        /// </summary>
        /// <param name="p_objXmlDoc">xml containing data</param>
        /// <param name="iCol">variable that will contain column</param>
        /// <param name="iRow"></param>
        /// <param name="strToken"></param>
        public void SetRowCol(XmlDocument p_objXmlDoc, ref int iCol, ref int iRow, string strToken)
        {
            try
            {
                if (p_objXmlDoc.SelectSingleNode(strToken + "Width").InnerText != "" ||
                    p_objXmlDoc.SelectSingleNode(strToken + "Width") != null)
                {
                    iCol = int.Parse(p_objXmlDoc.SelectSingleNode(strToken + "Width").InnerText);
                }
                else
                {
                    iCol = int.Parse(p_objXmlDoc.SelectSingleNode(strToken + "Width").Attributes["default"].Value);
                }

                if (p_objXmlDoc.SelectSingleNode(strToken + "Height").InnerText != "" ||
                    p_objXmlDoc.SelectSingleNode(strToken + "Height") != null)
                {
                    iRow = int.Parse(p_objXmlDoc.SelectSingleNode(strToken + "Height").InnerText);
                }
                else
                {
                    iRow = int.Parse(p_objXmlDoc.SelectSingleNode(strToken + "Height").Attributes["default"].Value);
                }
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("LocalCache.SetRowCol.Exception", m_iClientId), e); }
            finally
            {
            }

        }
        

        //Changed by Gagan for MITS 10376 : End   

        /// <summary>
        /// The function updates the cache when any setting is updated in the cache
        /// </summary>
        /// <param name="p_sParam_key">The key corresponding to which the value needs to be updated</param>
        /// <param name="p_bParam_Value">The Changed Value</param>
        public void UpdateCacheforSystemSettings(string p_sParam_key, string p_sParam_Value)
        {
            string sKey = String.Format("SYSINF_{0}", p_sParam_key);

            try
            {
                if (m_Cache.ContainsKey(sKey))
                    m_Cache[sKey] = p_sParam_Value;
                else
                {
                    m_Cache.Add(sKey, p_sParam_Value);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(Globalization.GetString("LocalCache.UpdateCacheforSystemSettings", m_iClientId));
            }
        }



        public string GetStateCode(int stateId)
        {
            string sKey = String.Format("SID_{0}", stateId);
            object objShortCode = null;//Added by Shivendu
            try
            {
                if (m_Cache.ContainsKey(sKey))
                    return (string)m_Cache[sKey];
                else
                {
                    string shortCode = "";
                    string SQL = String.Format(@"SELECT STATE_ID FROM STATES
																				WHERE STATE_ROW_ID={0}", stateId);
                    //shortCode = DbFactory.ExecuteScalar(this.DbConnectionString, SQL).ToString();
                    //Start by Shivendu for MITS 18453
                    objShortCode = DbFactory.ExecuteScalar(this.DbConnectionString, SQL);
                    if (objShortCode != null && objShortCode != System.DBNull.Value)
                        shortCode = objShortCode.ToString();
                    //End by Shivendu for MITS 18453
                    m_Cache.Add(sKey, shortCode);
                    return shortCode;
                }
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("LocalCache.GetStateCode.Exception", m_iClientId), e); }
        }

        /// <summary>
        /// Gets the Related Code ID
        /// </summary>
        /// <param name="codeId">code ID for any related codes</param>
        /// <returns>the related Code ID (if any) exists</returns>
        /// <remarks>returns a 0 for records which have NULL for related Code IDs
        /// Ideally, all records, should have 0 for these additional records
        /// </remarks>
        public int GetRelatedCodeId(int codeId)
        {
            string sKey = String.Format("RID_{0}", codeId);
            try
            {
                if (m_Cache.ContainsKey(sKey))
                    return (int)m_Cache[sKey];
                else
                {
                    int relCode = 0;
                    string SQL = String.Format(@"SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID={0}", codeId);
                    relCode = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(this.DbConnectionString, SQL), m_iClientId);
                    m_Cache.Add(sKey, relCode);
                    return relCode;
                }
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("LocalCache.GetRelatedCodeId.Exception", m_iClientId), e); }
        }
        // Mits 10300 - Rahul (17th sept 2007) - the cache doesnt gets updated when 
        // the related code id is chanegd int hr frontend by the user
        public void RemoveRelatedCodeId(int codeId)
        {
            string sKey = String.Format("RID_{0}", codeId);
            try
            {
                if (m_Cache.ContainsKey(sKey))
                    m_Cache.Remove(sKey);
            }
            catch (Exception e)
            { throw new DataModelException(Globalization.GetString("LocalCache.RemoveRelatedCodeId.Exception", m_iClientId), e); }
        }
        public string GetRelatedShortCode(int codeId)
        {
            return this.GetShortCode(this.GetRelatedCodeId(codeId));
        }

        #region Get Table Name
        /// <summary>
        ///GetTableName returns the GetCodeTableId(codeId)
        ///for a particular codeId in the CODES table.
        /// <param name="codeId">CODE_ID for which the CODE_TABLE_ID is required</param>
        /// <returns>the System Table ID (if any) exists</returns>
        /// <remarks>returns a 0 for records which have NULL for related Code IDs
        /// Ideally, all records, should have 0 for these additional records
        /// </remarks>
        public int GetCodeTableId(int codeId)
        {
            int tableId = 0;
            string sKey = "";

            try
            {
                if (codeId == 0)
                    return 0;

                sKey = String.Format("GCTID_{0}", codeId);

                if (m_Cache.ContainsKey(sKey))
                    return (int)m_Cache[sKey];

                tableId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(this.DbConnectionString, "SELECT TABLE_ID FROM CODES WHERE CODE_ID = " + codeId), m_iClientId);
                m_Cache.Add(sKey, tableId);
            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetCodeTableId.DataError", m_iClientId), p_objException);
            }
            finally
            {
            }
            return tableId;
        }
        #endregion

        #region Get Table Name
        /// <summary>
        ///GetTableName returns the SYSTEM_TABLE_NAME
        ///for a particular TABLE_ID in the GLOSSARY table.
        /// </summary>
        /// <param name="p_iTableID">TABLE_ID for which the SYSTEM_TABLE_NAME is required</param>
        /// <returns>System table name</returns>
        public string GetTableName(int p_iTableID)
        {
            string sTableName = "";
            string sKey = "";

            try
            {
                if (p_iTableID == 0)
                    return "";

                sKey = String.Format("GTN_{0}", p_iTableID);

                if (m_Cache.ContainsKey(sKey))
                    return (string)m_Cache[sKey];

                sTableName = Convert.ToString(DbFactory.ExecuteScalar(this.DbConnectionString, "SELECT SYSTEM_TABLE_NAME FROM GLOSSARY WHERE TABLE_ID = " + p_iTableID));
                m_Cache.Add(sKey, sTableName);
            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetTableName.DataError", m_iClientId), p_objException);
            }
            finally
            {
            }
            return sTableName;
        }
        #endregion

        #region Fetch user table names
        /// <summary>
        /// This function fetches the table name for the give table id.
        /// </summary>
        /// <param name="p_lTableID">Table id</param>
        /// <returns>Table name corresponding to the table id.</returns>
        public string GetUserTableName(long p_lTableID)
        {
            string sRetTable = "";
            string sKey = "";

            try
            {
                if (p_lTableID == 0)
                    return sRetTable;

                sKey = String.Format("UTN_{0}", p_lTableID);

                if (m_Cache.ContainsKey(sKey))
                    return (string)m_Cache[sKey];

                sRetTable = Convert.ToString(DbFactory.ExecuteScalar(this.DbConnectionString,"SELECT TABLE_NAME FROM GLOSSARY_TEXT WHERE TABLE_ID = " + p_lTableID + " AND LANGUAGE_CODE = 1033"));
                m_Cache.Add(sKey, sRetTable);
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("LocalCache.GetUserTable.UserTable", m_iClientId), p_objException);
            }

            finally { }
            return sRetTable;
        }
        #endregion

        public string GetAllParentCodesForClosedClaims(int p_iLOB, int p_idsnId)
        {
            string sKey = String.Format("CODLOB_{0}{1}", p_iLOB, p_idsnId);
            string sToMatch = "";
            if (m_Cache.ContainsKey(sKey))
                return (string)m_Cache[sKey];
            //string sSQL = "SELECT CODES.CODE_ID, C2.SHORT_CODE FROM CODES_TEXT,GLOSSARY,CODES FULL OUTER JOIN CODES C2 ON CODES.RELATED_CODE_ID = C2.CODE_ID WHERE GLOSSARY.SYSTEM_TABLE_NAME = 'CLAIM_STATUS' " +
            string sSQL = "SELECT CODES.CODE_ID FROM CODES_TEXT,GLOSSARY,CODES FULL OUTER JOIN CODES C2 ON CODES.RELATED_CODE_ID = C2.CODE_ID WHERE GLOSSARY.SYSTEM_TABLE_NAME = 'CLAIM_STATUS' " +
                " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID  " +
                " AND CODES.CODE_ID = CODES_TEXT.CODE_ID  " +
                " AND CODES_TEXT.LANGUAGE_CODE = 1033 " +
                " AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) " +
                //tkatsarski: 01/19/15 - RMA-1688: Removed ORG_GROUP_EID check, so all codes with parent of type "Closed" can be returned.
                // " AND (CODES.LINE_OF_BUS_CODE = " + p_iLOB + " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) AND (CODES.ORG_GROUP_EID IS NULL OR CODES.ORG_GROUP_EID=0)" +
                " AND (CODES.LINE_OF_BUS_CODE = " + p_iLOB + " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0)" +
                " AND C2.SHORT_CODE='C'";
            using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, sSQL))
            {
                while (objReader.Read())
                {
                    sToMatch = String.Format("{0}{1},", sToMatch, Conversion.ConvertObjToStr(objReader.GetValue(0)));
                }//while
            }//using
            
            m_Cache.Add(sKey, sToMatch);
            return sToMatch;
        }

        //MITS 11874 Abhishek Start
        /// <summary>
        /// Returns parent codes for closed events
        /// </summary>
        /// <param name="p_iLOB"></param>
        /// <param name="p_idsnId">R7 Perf Imp:Added this parameter to be able to cache this</param>
        /// <returns>comma separated code ids</returns>
        public string GetAllParentCodesForClosedEvents(int p_iLOB, int p_idsnId)
        {
            string sKey = String.Format("CODPARCLOSEDEVE_{0}{1}", p_iLOB, p_idsnId);
            string sToMatch = "";
            if (m_Cache.ContainsKey(sKey))
                return (string)m_Cache[sKey];

            string sSQL = "SELECT CODES.CODE_ID, C2.SHORT_CODE FROM CODES_TEXT,GLOSSARY,CODES FULL OUTER JOIN CODES C2 ON CODES.RELATED_CODE_ID = C2.CODE_ID WHERE GLOSSARY.SYSTEM_TABLE_NAME = 'EVENT_STATUS' " +
                " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID  " +
                " AND CODES.CODE_ID = CODES_TEXT.CODE_ID  " +
                " AND CODES_TEXT.LANGUAGE_CODE = 1033 " +
                " AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) " +
                " AND (CODES.LINE_OF_BUS_CODE = " + p_iLOB + " OR CODES.LINE_OF_BUS_CODE IS NULL OR CODES.LINE_OF_BUS_CODE = 0) AND (CODES.ORG_GROUP_EID IS NULL OR CODES.ORG_GROUP_EID=0)" +
                " AND C2.SHORT_CODE='C'";
            
            using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, sSQL))
            {
                while (objReader.Read())
                {
                    sToMatch = String.Format("{0}{1},", sToMatch, Conversion.ConvertObjToStr(objReader.GetValue(0)));
                }//while
            }//using

            m_Cache.Add(sKey, sToMatch);
            return sToMatch;
        }
        //MITS 11874 End

        #region Get Glossary Type Code
        //spahariya MITS# 30426 - start 12/07/2012
        /// <summary>
        /// Gets Glossary type code from Glossary table
        /// </summary>
        /// <param name="p_iTableID">Table ID</param>
        /// <returns>Glossary Type code</returns>
        public int GetGlossaryType(int p_iTableID)
        {
            string sKey = String.Format("GLSTYP_{0}", p_iTableID);
            try
            {
                if (m_Cache.ContainsKey(sKey))
                    return (int)m_Cache[sKey];
                else
                {
                    int iGlossarytype = 0;
                    string sSQL = String.Format(@"SELECT GLOSSARY_TYPE_CODE FROM GLOSSARY WHERE TABLE_ID ={0}", p_iTableID);
                    iGlossarytype = Convert.ToInt16(DbFactory.ExecuteScalar(this.DbConnectionString, sSQL));
                    m_Cache.Add(sKey, iGlossarytype);
                    return iGlossarytype;
                }
            }
            catch (Exception p_objEx)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetCodeDesc.Exception", m_iClientId), p_objEx);
            }
            finally { }
        }
        //spahariya MITS# 30426 - End
        #endregion
        #region Get Code Description
        /// <summary>
        /// Gets Code Description from Code Text table
        /// </summary>
        /// <param name="p_iCodeID">Code ID</param>
        /// <returns>Code Description</returns>
        public string GetCodeDesc(int p_iCodeID)
        {
            string sKey = String.Format("COD_{0}", p_iCodeID);
            try
            {
                if (m_Cache.ContainsKey(sKey))
                    return (string)m_Cache[sKey];
                else
                {
                    string sCodeDesc = "";
                    string sSQL = String.Format(@"SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID={0}", p_iCodeID);
                    sCodeDesc = Convert.ToString(DbFactory.ExecuteScalar(this.DbConnectionString,sSQL));
                    m_Cache.Add(sKey, sCodeDesc);
                    return sCodeDesc;
                }
            }
            catch (Exception p_objEx)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetCodeDesc.Exception", m_iClientId), p_objEx);
            }
            finally { }
        }
        #endregion
        #region Get Code Description based on Language
        /// Name		: GetCodeDesc
        /// Author		: Amandeep Kaur
        /// Date Created: 11/08/2012		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// <summary>
        /// Gets Code Description from Code Text table using the language code and code id         
        /// </summary>
        /// <param name="p_iCodeID">Code ID</param>
        /// <returns>Code Description</returns>
        public string GetCodeDesc(int p_iCodeID, int p_iLangCode)
        {
            string sKey = string.Empty;
            string sSQL = string.Empty;
            int iLangCode = 0;
            if(p_iLangCode != 0)
                sKey = String.Format("COD_{0}_{1}", p_iCodeID,p_iLangCode);
            else
                sKey = string.Format("COD_{0}_{1}", p_iCodeID, RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
            try
            {
                iLangCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
                if (m_Cache.ContainsKey(sKey))
                    return (string)m_Cache[sKey];
                else
                {
                    string sCodeDesc = "";
                    if (p_iLangCode == 0)
                        p_iLangCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
                    sSQL = String.Format(@"SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID={0} AND LANGUAGE_CODE = {1}", p_iCodeID,p_iLangCode);
                    sCodeDesc = Convert.ToString(DbFactory.ExecuteScalar(this.DbConnectionString, sSQL));
                    if (string.IsNullOrEmpty(sCodeDesc))
                    {
                        sSQL = String.Format(@"SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID={0} AND LANGUAGE_CODE = {1}", p_iCodeID, iLangCode);
                        sCodeDesc = Convert.ToString(DbFactory.ExecuteScalar(this.DbConnectionString, sSQL));
                        sKey = string.Format("COD_{0}_{1}", p_iCodeID, iLangCode);
                    }
                    m_Cache.Add(sKey, sCodeDesc);
                    return sCodeDesc;
                }
            }
            catch (Exception p_objEx)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetCodeDesc.Exception", m_iClientId), p_objEx);
            }
            finally { }
        }
        #endregion
        #region Get Code information
        /// <summary>
        ///GetCodeInfo returns the SHORT_CODE, CODE_DESC
        ///for a particular CODE_ID in the CODES,CODES_TEXT table.
        /// </summary>
        /// <param name="p_iCode">CODE_ID for which the SHORT_CODE, CODE_DESC is required</param>
        /// <param name="p_ShortCode">SHORT_CODE, reference parameter to store the value</param>
        /// <param name="p_sDesc">CODE_DESC, reference parameter to store the value</param>
        /// <remarks>Short code and the description must not contain 'Comma' as it is delimiter</remarks> 
        public void GetCodeInfo(int p_iCode, ref string p_ShortCode, ref string p_sDesc)
        {
            if (!string.IsNullOrEmpty(LanguageCode))
            {
                if (LanguageCode.Length != 4) { LanguageCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]; }
                GetCodeInfo(p_iCode, ref p_ShortCode, ref p_sDesc, Conversion.ConvertStrToInteger(LanguageCode));
            }
            else
            {
                p_ShortCode = "";
                p_sDesc = "";

                if (p_iCode == 0)
                    return;


                string sKey = String.Format("CODINF_{0}", p_iCode);

                try
                {
                    if (m_Cache.ContainsKey(sKey))
                    {
                        String[] sSplit = Regex.Split((string)m_Cache[sKey], ",");

                        p_ShortCode = sSplit[0];
                        //Changed Rakhi for Mits 12946:START-Admin tracking search not displaying data correctly
                        //p_sDesc = sSplit[1];
                        //The value for short code YL is Yes,Late.
                        //Spliting on the basis of "," and then taking the description p_sDesc=sSplit[1] gives YL-Yes which is wrong.
                        //So this comma separated description needs to be joined again after separating it from its short code.
                        for (int i = 1; i < sSplit.Length; i++)
                        {
                            if (p_sDesc.Trim().Equals(""))
                            {
                                p_sDesc = sSplit[i];
                            }
                            else
                            {
                                p_sDesc += "," + sSplit[i];
                            }
                            //Changed Rakhi for Mits 12946:END-Admin tracking search not displaying data correctly
                        }
                    }
                    else
                    {
                        string sSQL = String.Format(@"SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT WHERE CODES.CODE_ID = {0}" +
                                    " AND CODES.CODE_ID = CODES_TEXT.CODE_ID", p_iCode);

                        using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, sSQL))
                        {
                            while (objReader.Read())
                            {
                                p_ShortCode = objReader.GetString("SHORT_CODE");
                                p_sDesc = objReader.GetString("CODE_DESC");
                                m_Cache.Add(sKey, p_ShortCode + "," + p_sDesc);
                            }//while
                        }//using
                    }
                }
                catch (Exception p_objEx)
                {
                    throw new DataModelException(Globalization.GetString("LocalCache.GetCodeInfo.Exception", m_iClientId), p_objEx);
                }
            }
        }

        /// <summary>
        /// GetCodeInfo returns the SHORT_CODE, CODE_DESC
        /// for a particular CODE_ID in the CODES,CODES_TEXT table depending upon the language code.
        /// </summary>
        /// Created By Amandeep Kaur Date: 12/12/2012
        /// <param name="p_iCode">CODE_ID for which the SHORT_CODE, CODE_DESC is required</param>
        /// <param name="p_ShortCode">SHORT_CODE, reference parameter to store the value</param>
        /// <param name="p_sDesc">CODE_DESC, reference parameter to store the value</param>
        /// <param name="p_iLangCode">LANGUAGE_CODE, language code to get the corresponding codes description</param>
        /// <remarks>Short code and the description must not contain 'Comma' as it is delimiter</remarks> 
        public void GetCodeInfo(int p_iCode, ref string p_ShortCode, ref string p_sDesc, int p_iLangCode)
        {
            p_ShortCode = "";
            p_sDesc = "";

            if (p_iCode == 0)
                return;

            string sKey = string.Empty;
            if(p_iLangCode != 0)
                sKey = String.Format("CODINF_{0}_{1}", p_iCode,p_iLangCode);
            else
                sKey = string.Format("CODINF_{0}_{1}", p_iCode, RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);

            int iLangCode = 0;
            
            try
            {
                iLangCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
                if (m_Cache.ContainsKey(sKey))
                {
                    String[] sSplit = Regex.Split((string)m_Cache[sKey], ",");

                    p_ShortCode = sSplit[0];
                   
                    for (int i = 1; i < sSplit.Length; i++)
                    {
                        if (p_sDesc.Trim().Equals(""))
                        {
                            p_sDesc = sSplit[i];
                        }
                        else
                        {
                            p_sDesc += "," + sSplit[i];
                        }              
                    }
                }
                else
                {
                    if (p_iLangCode == 0)
                    {
                        p_iLangCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]); 
                    }
                    string sSQL = String.Format(@"SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT WHERE CODES.CODE_ID = {0}" +
                                " AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = {1}", p_iCode,p_iLangCode);

                    using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, sSQL))
                    {
                        while (objReader.Read())
                        {
                            p_ShortCode = objReader.GetString("SHORT_CODE");
                            p_sDesc = objReader.GetString("CODE_DESC");                            
                        }//while
                    }//using
                    if (string.IsNullOrEmpty(p_sDesc) || string.IsNullOrEmpty(p_ShortCode))
                    {
                        sSQL = String.Format(@"SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT WHERE CODES.CODE_ID = {0}" +
                                " AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = {1}", p_iCode, iLangCode);
                        using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, sSQL))
                        {
                            while (objReader.Read())
                            {
                                p_ShortCode = objReader.GetString("SHORT_CODE");
                                p_sDesc = objReader.GetString("CODE_DESC");
                                sKey = string.Format("CODINF_{0}_{1}", p_iCode, iLangCode);                               
                            }//while
                        }//using                                               
                    }
                    m_Cache.Add(sKey, p_ShortCode + "," + p_sDesc); 
                }
            }
            catch (Exception p_objEx)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetCodeInfo.Exception", m_iClientId), p_objEx);
            }
        }
        //25/12/2007 Abhishek, MITS 11097 - START
        //Code information for entity should be retrieved from glossary table
        /// <summary>
        ///GetCodeInfoForEntity returns the SHORT_CODE, CODE_DESC
        ///for a particular CODE_ID in the GLOSSARY table.
        /// </summary>
        /// <param name="p_iCode">CODE_ID for which the SHORT_CODE, CODE_DESC is required</param>
        /// <param name="p_ShortCode">SHORT_CODE, reference parameter to store the value</param>
        /// <param name="p_sDesc">CODE_DESC, reference parameter to store the value</param>
        /// <remarks>Short code and the description must not contain 'Comma' as it is delimiter</remarks>
        public void GetCodeInfoForEntity(int p_iCode, ref string p_ShortCode, ref string p_sDesc)
        {
            p_ShortCode = "";
            p_sDesc = "";

            if (p_iCode == 0)
                return;

            string sKey = String.Format("CODINF_{0}", p_iCode);

            try
            {

                string sSQL = String.Format(@"SELECT SYSTEM_TABLE_NAME FROM GLOSSARY WHERE TABLE_ID = {0}", p_iCode);

                //JIRA RMA-4690:neha goel:05202014 added to handle null with convert.tostring MITS # 35595--START
                //p_sDesc = DbFactory.ExecuteScalar(this.DbConnectionString, sSQL).ToString();
                p_sDesc = Convert.ToString(DbFactory.ExecuteScalar(this.DbConnectionString, sSQL));
                //JIRA RMA-4690:neha goel:05202014 added to handle null with convert.tostring MITS # 35595--End
               

            }
            catch (Exception p_objEx)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetCodeInfo.Exception", m_iClientId), p_objEx);
            }
            
        }
        //25/12/2007 Abhishek, MITS 11097 - END
        #endregion

        #region Get system parameter information
        /// <summary>
        /// This function checks for the given system parameter in the Sys_Parms table
        /// </summary>
        /// <param name="p_sFieldName">System parameter</param>
        /// <param name="p_lCaseSen">Ref parameter containing the value corresponding to the given system parameter</param>
        public void GetSysInfo(string p_sFieldName, ref string p_sFieldValue)
        {
            string sKey = String.Format("SYSINF_{0}", p_sFieldName);

            try
            {
                if (m_Cache.ContainsKey(sKey))
                    p_sFieldValue = (string)m_Cache[sKey];
                else
                {
                    using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString,@"SELECT * FROM SYS_PARMS"))
                    {   
                        int iCntRecord = 0;
                        
                        while (objReader.Read() && objReader.FieldCount > iCntRecord)
                        {
                            if (objReader.GetName(iCntRecord).ToUpper() == p_sFieldName.ToUpper())
                            {
                                p_sFieldValue = objReader.GetValue(iCntRecord).ToString();
                                m_Cache.Add(sKey, p_sFieldValue);
                                return;
                            }
                            iCntRecord++;
                        } // while

                    } // using
                }
            }
            catch (Exception p_objEx)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetSysInfo.Exception", m_iClientId), p_objEx);
            }
        }
        #endregion

        #region Get system parameter information from SYS_SETTINGS
        /// <summary>
        /// This function checks for the given system parameter in the Sys_Settings table
        /// </summary>
        /// <param name="p_sFieldName">System parameter</param>
        /// <param name="p_sFieldValue">Ref parameter containing the value corresponding to the given system parameter</param>
        public void GetSystemInfo(string p_sFieldName, ref string p_sFieldValue)
        {
            string sKey = String.Format("SYSINF_{0}", p_sFieldName);

            try
            {
                if (m_Cache.ContainsKey(sKey))
                    p_sFieldValue = (string)m_Cache[sKey];
                else
                {
                    using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, @"SELECT PARM_NAME, STR_PARM_VALUE FROM PARMS_NAME_VALUE"))
                    {
                        while (objReader.Read())
                        {
                            if (objReader.GetValue("PARM_NAME").ToString() == p_sFieldName.ToUpper())
                            {
                                p_sFieldValue = objReader.GetValue("STR_PARM_VALUE").ToString();
                                m_Cache.Add(sKey, p_sFieldValue);
                                return;
                            }
                        } // while

                    } // using
                }
            }
            catch (Exception p_objEx)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetSystemInfo.Exception", m_iClientId), p_objEx);
            }
        }

        #endregion
   

        #region Remove system parameter information
        /// <summary>
        /// This function remove the given key from the cache
        /// </summary>
        /// <param name="p_sFieldName">Key With full name should be passed to this function</param>
        //abisht MITS 10526
        public void RemoveAnyKeyFromCache(string p_sFieldName)
        {
            string sKey = p_sFieldName;
            try
            {
                if (m_Cache.ContainsKey(sKey))
                {
                    m_Cache.Remove(sKey);
                }
            }
            catch (Exception p_objEx)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.RemoveSysInfo.Exception", m_iClientId), p_objEx);
            }
        }
        #endregion
        #region Get State Information
        /// <summary>
        ///GetStateInfo returns the STATE_ID,STATE_NAME
        ///for a particular STATE_ROW_ID in the STATES table.
        /// </summary>
        /// <param name="p_iState">STATE_ROW_ID for which the SHORT_CODE, CODE_DESC is required</param>
        /// <param name="p_sAbbreviation">STATE_ID, reference parameter to store the value</param>
        /// <param name="p_sName">STATE_NAME, reference parameter to store the value</param>
        public void GetStateInfo(int p_iState, ref string p_sAbbreviation, ref string p_sName)
        {
            p_sAbbreviation = "";
            p_sName = "";

            if (p_iState == 0)
                return;

            string sKey = String.Format("STATINF_{0}", p_iState);

            try
            {
                if (m_Cache.ContainsKey(sKey))
                {
                    String[] sSplit = Regex.Split((string)m_Cache[sKey], ",");

                    p_sAbbreviation = sSplit[0];
                    p_sName = sSplit[1];

                }
                else
                {
                    string sSQL = String.Format(@"SELECT STATE_ID,STATE_NAME FROM STATES WHERE STATE_ROW_ID = {0}", p_iState);

                    using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString,sSQL))
                    {

                        while (objReader.Read())
                        {
                            p_sAbbreviation = objReader.GetString("STATE_ID");
                            p_sName = objReader.GetString("STATE_NAME");
                            m_Cache.Add(sKey, p_sAbbreviation + "," + p_sName);
                        }
                    } // using


                    
                }
            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetStateInfo.DataError", m_iClientId), p_objException);
            }
        }
        #endregion
        #region Get Coverage Group Information

               
        /// <summary>
        /// Added by Nitin Goel, MITS 30910 for National Interstate,08/22/2013
        ///GetCoverageGroupInfo returns the Coverage Group Code,Coverage Group Description
        /// </summary>
        /// <param name="p_iCovGroupId"></param>
        /// <param name="p_sCovGroupCode"></param>
        /// <param name="p_CovGroupDesc"></param>
        public void GetCoverageGroupInfo(int p_iCovGroupId, ref string p_sCovGroupCode, ref string p_CovGroupDesc)
        {
            p_sCovGroupCode = string.Empty;
            p_CovGroupDesc = string.Empty;
            //start -  Multilingual changes
            int iLangCode = 0;
            iLangCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);
            //end -  Multilingual changes   
            if (p_iCovGroupId == 0)
                return;

            string sKey = String.Format("COVGRPINF_{0}", p_iCovGroupId);

            try
            {
                if (m_Cache.ContainsKey(sKey))
                {
                    String[] sSplit = Regex.Split((string)m_Cache[sKey], ",");

                    p_sCovGroupCode = sSplit[0];
                    p_CovGroupDesc = sSplit[1];

                }
                else
                {
                    //Added by Nitin Goel, To fetch short code from Coverage Group table always.
                    ////start -  Multilingual changes   
                    ////string sSQL = String.Format(@"SELECT COV_GROUP,COV_GROUP_DESC FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID = {0}", p_iCovGroupId);
                    //string sSQL = String.Format(@"SELECT COV_GROUP,GROUP_DESCRIPTION FROM COVERAGE_GROUP_TEXT WHERE COVERAGE_GROUP_ID = {0} AND LANGUAGE_CODE = {1}", p_iCovGroupId, iLangCode);
                    ////end -  Multilingual changes   
                    string sSQL = String.Format(@"SELECT CG.COV_GROUP,CGT.GROUP_DESCRIPTION FROM COVERAGE_GROUP CG INNER JOIN COVERAGE_GROUP_TEXT CGT ON CG.COVERAGE_GROUP_ID = CGT.COVERAGE_GROUP_ID WHERE CG.COVERAGE_GROUP_ID = {0} AND CGT.LANGUAGE_CODE = {1}", p_iCovGroupId, iLangCode);
                    //End:Added by Nitin Goel
                    using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, sSQL))
                    {

                        while (objReader.Read())
                        {
                            p_sCovGroupCode = objReader.GetString("COV_GROUP");
                            //start -  Multilingual changes   
                        //    p_CovGroupDesc = objReader.GetString("COV_GROUP_DESC");
                            p_CovGroupDesc = objReader.GetString("GROUP_DESCRIPTION");
                            //end -  Multilingual changes   
                            m_Cache.Add(sKey, p_sCovGroupCode + "," + p_CovGroupDesc);
                        }
                    } // using



                }
            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetCoverageGroupInfo.DataError", m_iClientId), p_objException);
            }
        }
        #endregion
        #region Fetch Entity information
        /// <summary>
        /// This function fetches the first name & last name and assigns it to the variables being passed as reference.
        /// </summary>
        /// <param name="p_lEntityID">Entity id</param>
        /// <param name="p_sFirstName">First name of the entity corresponding to the entity id</param>
        /// <param name="p_sLastName">Last name of the entity corresponding to the entity id</param>
        public void GetEntityInfo(long p_lEntityID, ref string p_sFirstName, ref string p_sLastName)
        {
            p_sFirstName = "";
            p_sLastName = "";

            if (p_lEntityID == 0)
                return;

            string sKey = String.Format("ENTINF_{0}", p_lEntityID);

            try
            {
                if (m_DynCache.ContainsKey(sKey))
                {
                    String[] sSplit = Regex.Split((string)m_DynCache[sKey], ",");

                    p_sFirstName = sSplit[0];
                    p_sLastName = sSplit[1];

                }
                else
                {
                    string sSQL = String.Format(@"SELECT FIRST_NAME,LAST_NAME FROM ENTITY WHERE ENTITY_ID = {0}", p_lEntityID);

                    using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString,sSQL))
                    {
                        while (objReader.Read())
                        {
                            p_sFirstName = objReader.GetValue(0).ToString();
                            p_sLastName = objReader.GetValue(1).ToString();
                            m_DynCache.Add(sKey, p_sFirstName + "," + p_sLastName);
                        }//while 
                    }//using
                        
                }
            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetEntityInfo.GetName", m_iClientId), p_objException);
            }
        }
        #endregion
        #region Fetch Claimant information
        /// <summary>
        /// This function fetches the first name & last name and assigns it to the variables being passed as reference.
        /// </summary>
        /// <param name="p_lEntityID">Entity id</param>
        /// <param name="p_sFirstName">First name of the entity corresponding to the entity id</param>
        /// <param name="p_sLastName">Last name of the entity corresponding to the entity id</param>
        public void GetClaimantInfo(long p_lEntityID, ref string p_sFirstName, ref string p_sLastName)
        {
            p_sFirstName = "";
            p_sLastName = "";

            if (p_lEntityID == 0)
                return;

            try
            {

                string sSQL = String.Format(@"SELECT FIRST_NAME,LAST_NAME FROM ENTITY WHERE ENTITY_ID = (SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIMANT_ROW_ID = {0})", p_lEntityID);

                using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, sSQL))
                {
                    while (objReader.Read())
                    {
                        p_sFirstName = objReader.GetValue(0).ToString();
                        p_sLastName = objReader.GetValue(1).ToString();
                    }//while 
                }//using
            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetEntityInfo.GetName", m_iClientId), p_objException);
            }
        }
        #endregion

        #region Get Organization information
        /// <summary>
        ///GetOrgInfo returns the ABBREVIATION, LAST_NAME
        ///for a particular ENTITY_ID in the ENTITY table.
        /// </summary>
        /// <param name="p_iEntityID">ENTITY_ID for which the ABBREVIATION, LAST_NAME is required</param>
        /// <param name="p_sAbbreviation">ABBREVIATION, reference parameter to store the value</param>
        /// <param name="p_sName">LAST_NAME, reference parameter to store the value</param>
        public void GetOrgInfo(int p_iEntityID, ref string p_sAbbreviation, ref string p_sName)
        {
            p_sAbbreviation = "";
            p_sName = "";

            if (p_iEntityID == 0)
                return;

            string sKey = String.Format("ORGINF_{0}", p_iEntityID);

            try
            {
                if (m_DynCache.ContainsKey(sKey))
                {
                    String[] sSplit = Regex.Split((string)m_DynCache[sKey], ",");

                    p_sAbbreviation = sSplit[0];
                    p_sName = sSplit[1];

                }
                else
                {
                    string sSQL = String.Format(@"SELECT ABBREVIATION,LAST_NAME FROM ENTITY WHERE ENTITY_ID = {0}", p_iEntityID);

                    using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, sSQL))
                    {
                        while (objReader.Read())
                        {
                            p_sAbbreviation = objReader.GetString("ABBREVIATION");
                            p_sName = objReader.GetString("LAST_NAME");
                            m_DynCache.Add(sKey, p_sAbbreviation + "," + p_sName);
                        }//while 
                    }//using

                }
            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetOrgInfo.DataError", m_iClientId), p_objException);
            }
        }

        /// <summary>
        ///GetParentOrgInfo returns the ABBREVIATION, LAST_NAME
        ///for a particular parent level ENTITY of the supplied Orgh EntityId.
        /// </summary>
        /// <param name="p_iEntityID">ENTITY_ID for which the ABBREVIATION, LAST_NAME is required</param>
        /// <param name="p_iParentLevel">ABBREVIATION, reference parameter to store the value</param>
        /// <param name="p_sAbbreviation">LAST_NAME, reference parameter to store the value</param>
        /// <remarks>returns a 0 for records which have NULL for Parent IDs
        /// Ideally, all records, should have 0 for these additional records
        /// </remarks>
        public void GetParentOrgInfo(int p_iEntityId, int p_iParentLevel, ref string p_sAbbreviation, ref string p_sName, ref int p_iParentId)
        {
            try
            {
                //Determine Applicable Parent EID.
                p_iParentId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(this.DbConnectionString, String.Format("SELECT {0}_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID={1}", Conversion.EntityTableIdToOrgTableName(p_iParentLevel), p_iEntityId)), m_iClientId);

            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetOrgInfo.DataError", m_iClientId), p_objException);
            }
            
            //Get Parent Org Info
            GetOrgInfo(p_iParentId, ref p_sAbbreviation, ref p_sName);
            return;
        }

        /// <summary>
        ///GetOrgTableId returns the Entity Table Id.
        /// </summary>
        /// <param name="p_iEntityID">ENTITY_ID for which the Table Id is required</param>
        public int GetOrgTableId(int p_iEntityID)
        {
            int iTableId = 0;

            string sKey = String.Format("ORGTABLEID_{0}", p_iEntityID);

            try
            {
                if (m_DynCache.ContainsKey(sKey))
                    return (int)m_DynCache[sKey];
                else
                {
                    string sSQL = String.Format(@"SELECT ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY_ID = {0}", p_iEntityID);
                    using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, sSQL))
                    {
                        while (objReader.Read())
                        {
                            iTableId = Conversion.ConvertStrToInteger(objReader["ENTITY_TABLE_ID"].ToString());
                            m_DynCache.Add(sKey, iTableId);
                        }//while 
                    }//using
                    return iTableId;
                }
            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetOrgTableId.DataError", m_iClientId), p_objException);
            }
        }

        #endregion

        #region Make a list of Codes
        /// <summary>Returns Code list for a query.
        /// e.g. return what goes in parenthesis in sql statement like "...WHERE CODE_ID IN(15,78,84,87)..."</summary>
        /// <param name="p_sSQL">Query string</param>
        /// <returns>String List</returns>
        public string GetCodeList(string p_sSQL)
        {
            StringBuilder sList = null;

            string sKey = String.Format("CDLST_{0}", p_sSQL);

            try
            {
                if (m_Cache.ContainsKey(sKey))
                    return (string)m_Cache[sKey];
                else
                {
                    sList = new StringBuilder();

                    using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, p_sSQL))
                    {
                        while (objReader.Read())
                        {
                            if (sList.ToString() == "")
                                sList.Append(objReader.GetString(0));
                            else
                            {
                                sList.Append(",");
                                sList.Append(objReader.GetString(0));
                            }
                        }//while 
                    }//using

                    m_Cache.Add(sKey, sList.ToString());
                    return sList.ToString();
                }
            }
            catch (Exception p_objExp)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetCodeList.DataError", m_iClientId), p_objExp);
            }
        }
        #endregion


        #region Get State Row ID
        /// <summary>
        /// Gets State Row ID from States
        /// </summary>
        /// <param name="stateId">State ID</param>
        /// <returns>State Row ID</returns>
        /// <remarks>returns a 0 for records which have NULL for State Row IDs
        /// Ideally, all records, should have 0 for these additional records
        /// </remarks>
        public int GetStateRowID(string p_sStateId)
        {
            p_sStateId = p_sStateId.ToUpper();

            string sKey = String.Format("SRID_{0}", p_sStateId);
            try
            {
                if (m_Cache.ContainsKey(sKey))
                    return (int)m_Cache[sKey];
                else
                {
                    int iStateRowId = 0;
                    string SQL = String.Format(@"SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID='{0}'", p_sStateId);
                    iStateRowId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(this.DbConnectionString, SQL), m_iClientId);
                    m_Cache.Add(sKey, iStateRowId);
                    return iStateRowId;
                }
            }
            catch (Exception p_objEx)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetStateRowID.Exception", m_iClientId), p_objEx);
            }
            finally { }
        }
        #endregion
        #region Get Coverage Group ID
       /// <summary>
       /// Added by nitin goel, MITS 30910 for National Interstate deductible tracking enhancement, 08/22/2013
       /// </summary>
       /// <param name="p_sCovGroupCode"></param>
       /// <returns></returns>
        public int GetCoverageGroupID(string p_sCovGroupCode)
        {
            p_sCovGroupCode = p_sCovGroupCode.ToUpper();            
            int iCovGroupId = 0;
            string SQL = string.Empty;
            string sKey = String.Format("COVGROUPID_{0}", p_sCovGroupCode);
            try
            {
                if (m_Cache.ContainsKey(sKey))
                    return (int)m_Cache[sKey];
                else
                {                    
                    SQL = String.Format(@"SELECT COVERAGE_GROUP_ID FROM COVERAGE_GROUP WHERE COV_GROUP='{0}'", p_sCovGroupCode);
                    iCovGroupId =DbFactory.ExecuteAsType<int>(this.DbConnectionString, SQL);
                    m_Cache.Add(sKey, iCovGroupId);
                    return iCovGroupId;
                }
            }
            catch (Exception p_objEx)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetCoverageGroupID.Exception", m_iClientId), p_objEx);
            }
            finally { }
        }
        #endregion
        //Added by Amitosh for R8 enhancment of Salvage
        public void GetAddressInFo(int p_iAddrId, ref string p_sAddr1, ref string p_sAddr2, ref string p_sAddr3, ref string p_sAddr4, ref string p_sCITY, ref string p_sCounty, ref string p_sState, ref string p_sCountry, ref string p_sZipCode)
        {
          
            string p_sStateAbr = null;
            string p_sStateName = null;
     
            try
            {
                //Ashish Ahuja - Address Master RMA 8753
                   // string SQL = "SELECT ADDR1,ADDR2,ADDR3,ADDR4,CITY,COUNTY,STATE_ID,COUNTRY_CODE,ZIP_CODE FROM ENTITY_X_ADDRESSES WHERE  ADDRESS_ID=" + p_iAddrId;
                string SQL = "SELECT ADDR1,ADDR2,ADDR3,ADDR4,CITY,COUNTY,STATE_ID,COUNTRY_CODE,ZIP_CODE FROM ADDRESS WHERE  ADDRESS_ID=" + p_iAddrId;    
                    using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, SQL))
                    {
                        if (objReader.Read())
                        {
                            p_sAddr1 = objReader.GetString("ADDR1");
                            p_sAddr2 = objReader.GetString("ADDR2");
                            p_sAddr3 = objReader.GetString("ADDR3");
                            p_sAddr4 = objReader.GetString("ADDR4");
                            p_sCITY = objReader.GetString("CITY");
                            p_sCounty = objReader.GetString("COUNTY");
                            GetStateInfo(objReader.GetInt("STATE_ID"), ref p_sStateAbr, ref p_sStateName);
                            p_sCountry = GetCodeDesc(objReader.GetInt("COUNTRY_CODE"), m_iClientId);
                            p_sZipCode = objReader.GetString("ZIP_CODE");
                        }
                    }
            }
            catch (Exception p_objEx)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetAddrInfo.Exception", m_iClientId), p_objEx);
            }
            finally { }
        }
        //end Amitosh
        #region Get Entity Details
        public string GetEntityLastName(int entityId)
        {
            string sKey = String.Format("ELN_{0}", entityId);
            try
            {
                if (m_DynCache.ContainsKey(sKey))
                    return (string)m_DynCache[sKey];
                else
                {
                    string entityLastName = "";
                    string SQL = String.Format(@"SELECT LAST_NAME FROM ENTITY
																				WHERE ENTITY_ID={0}", entityId);
                    entityLastName = (string)Convert.ToString(DbFactory.ExecuteScalar(this.DbConnectionString,SQL));
                    m_DynCache.Add(sKey, entityLastName);
                    return entityLastName;
                }
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("LocalCache.GetEntityLastName.Exception", m_iClientId), e); }
        }
        public string GetEntityLastFirstName(int entityId)
        {
            //string sKey = String.Format("ELFN_{0}", entityId);
            try
            {
                //if (m_DynCache.ContainsKey(sKey))
                //    return (string)m_DynCache[sKey];
                //else
                //{
                    //DbReader rdr = null;
                    //LastName and Firstname should not be cached, PI list doesnot show updated value becoz of this
                    string entityLastName = "";
                    string SQL = String.Format(@"SELECT FIRST_NAME, LAST_NAME FROM ENTITY
																				WHERE ENTITY_ID={0}", entityId);
                    using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, SQL))
                    {
                        while (objReader.Read())
                        {
                            entityLastName = objReader.GetString("LAST_NAME");
                            if (objReader.GetString("FIRST_NAME") != "")
                                entityLastName = String.Format("{0}, {1}", entityLastName, objReader.GetString("FIRST_NAME"));
                        }//while 
                    }//using
                    //m_DynCache.Add(sKey, entityLastName);
                    return entityLastName;
                //}
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("LocalCache.GetEntityLastFirstName.Exception", m_iClientId), e); }
        }
        //Anu Tennyson for MITS 18229 Starts UAR List
        /// <summary>
        /// The Function will give the Vehicle VIN.
        /// </summary>
        /// <param name="iUnitID">The Unique ID of the Vehicle Table</param>
        /// <returns></returns>
        public string GetUARDetailVIN(int iUnitID, bool bIsExternalUnit)
        {
            //bIsExternalUnit = false;
            string sKey = String.Format("UAR_{0}", iUnitID);
            string sVehDesc = string.Empty, sVIN = string.Empty;
            try
            {
                if (m_DynCache.ContainsKey(sKey))
                    return (string)m_DynCache[sKey];
                else
                {
                    string sUARName = string.Empty;
                    string SQL = String.Format(@"SELECT VIN, VEH_DESC FROM VEHICLE WHERE UNIT_ID={0}", iUnitID);
																				
                    using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, SQL))
                    {
                        if (objReader.Read())
                        {
                            sVehDesc = objReader.GetString("VEH_DESC");
                            sVIN = objReader.GetString("VIN");
                            if (bIsExternalUnit)
                            {
                                sUARName = sVehDesc==string.Empty ? sVIN : sVehDesc; //objReader.GetString("VEH_DESC");
                            }
                            else
                            {
                                sUARName = sVIN;// objReader.GetString("VIN");
                            }
                            m_DynCache.Add(sKey, sUARName);
                        }//while 
                    }//using
                    
                    return sUARName;
                }
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("LocalCache.GetUARDetailVIN.Exception", m_iClientId), e); }
        }
        //Anu Tennyson for MITS 18229 Ends UAR List

        //Anu Tennyson for MITS 18229 Starts UAR List
        /// <summary>
        /// The Function will give the Property PIN.
        /// </summary>
        /// <param name="iPropertyID">The Unique ID of the Property Table</param>
        /// <returns></returns>
        public string GetUARDetailPIN(int iPropertyID)
        {
            string sKey = String.Format("PID_{0}", iPropertyID);
            try
            {
                if (m_DynCache.ContainsKey(sKey))
                    return (string)m_DynCache[sKey];
                else
                {
                    string sPropertyName = string.Empty;
                    string SQL = String.Format(@"SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID={0}", iPropertyID);

                    using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, SQL))
                    {
                        if (objReader.Read())
                        {
                            sPropertyName = objReader.GetString("PIN");
                            m_DynCache.Add(sKey, sPropertyName);
                        }//while 
                    }//using
                    
                    return sPropertyName;
                }
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("LocalCache.GetUARDetailPIN.Exception", m_iClientId), e); }
        }
        //Anu Tennyson for MITS 18229 Ends UAR List
        public string GetEntityAbbreviation(int entityId)
        {
            string sKey = String.Format("ELA_{0}", entityId);
            try
            {
                if (m_DynCache.ContainsKey(sKey))
                    return (string)m_DynCache[sKey];
                else
                {
                    string entityAbbreviation = "";
                    string SQL = String.Format(@"SELECT ABBREVIATION FROM ENTITY WHERE ENTITY_ID={0}", entityId);
                    entityAbbreviation = DbFactory.ExecuteScalar(this.DbConnectionString, SQL).ToString();
                    m_DynCache.Add(sKey, entityAbbreviation);
                    return entityAbbreviation;
                }
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("LocalCache.GetEntityLastFirstName.Exception", m_iClientId), e); }
        }
        #endregion

        #region JIRA - 4691
        //WWIG GAP20A - agupta298 - MITS 36804 - Start - JIRA - 4691
        public string[] GetHyperlinkByWebLinkId(int iWebLinkId)
        {
            string sKey = String.Format("GHBW{0}", iWebLinkId);
            try
            {
                if (m_DynCache.ContainsKey(sKey))
                    return (string[])m_DynCache[sKey];
                else
                {
                    string[] arrWebLinkData = new string[2];
                    string sPropertyName = string.Empty;
                    string SQL = String.Format(@"SELECT ITEM,VALUE FROM SYS_SETTINGS WHERE ROW_ID={0}", iWebLinkId);

                    using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, SQL))
                    {
                        if (objReader.Read())
                        {
                            arrWebLinkData[0] = objReader.GetString("ITEM");
                            arrWebLinkData[1] = objReader.GetString("VALUE");
                            m_DynCache.Add(sKey, arrWebLinkData);
                        }//while 
                    }//using

                    return arrWebLinkData;
                }
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("LocalCache.GetHyperlinkByWebLinkId.Exception"), e); }

        }

        public string[] GetWebLinkDataBySuppFieldId(int iSuppFieldId)
        {
            string sKey = String.Format("GWSF{0}", iSuppFieldId);
            try
            {
                if (m_DynCache.ContainsKey(sKey))
                    return (string[])m_DynCache[sKey];
                else
                {
                    string[] arrWebLinkData = new string[2];
                    string sPropertyName = string.Empty;
                    string SQL = String.Format(@" SELECT ss.ITEM , ss.VALUE FROM SUPP_DICTIONARY sd INNER JOIN SYS_SETTINGS ss ON sd.WEB_LINK_ID = ss.ROW_ID WHERE sd.FIELD_ID={0}", iSuppFieldId);

                    using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, SQL))
                    {
                        if (objReader.Read())
                        {
                            arrWebLinkData[0] = objReader.GetString("ITEM");
                            arrWebLinkData[1] = objReader.GetString("VALUE");
                            m_DynCache.Add(sKey, arrWebLinkData);
                        }//while 
                    }//using

                    return arrWebLinkData;
                }
            }
            catch (Exception e) { throw new DataModelException(Globalization.GetString("LocalCache.GetWebLinkDataBySuppFieldId.Exception"), e); }

        }
        //WWIG GAP20A - agupta298 - MITS 36804 - End - JIRA - 4691 
        #endregion

        #region Get System User Login Name
        /// <summary>
        ///GetTableName returns the SYSTEM_TABLE_NAME
        ///for a particular TABLE_ID in the GLOSSARY table.
        /// </summary>
        /// <param name="p_iTableID">TABLE_ID for which the SYSTEM_TABLE_NAME is required</param>
        /// <returns>System table name</returns>
        public string GetSystemLoginName(int userId, int dsnId, string secConnStr,int p_iClientId)
        {
            string sTableName = string.Empty;
            string sKey = string.Empty;
            object objTableName = null;   //pmittal5
            try
            {
                sKey = String.Format("GSLN_{0}{1}", userId, dsnId);
                if (m_Cache == null)
                {
                    m_Cache = new CacheTable(this.DbConnectionString,p_iClientId);
                }
                if (m_Cache.ContainsKey(sKey))
                    return (string)m_Cache[sKey];

                //pmittal5 Mits 18022 10/09/09 - If NULL is returned by the query, calling ToString() throws an error.
                //sTableName = DbFactory.ExecuteScalar(secConnStr, String.Format("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID={0} AND DSNID={1}", userId, dsnId)).ToString();
                objTableName = DbFactory.ExecuteScalar(secConnStr, String.Format("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID={0} AND DSNID={1}", userId, dsnId));
                if (objTableName != null && objTableName != System.DBNull.Value)
                    sTableName = objTableName.ToString();
                //End - pmittal5

                m_Cache.Add(sKey, sTableName);
            }
            catch (Exception p_objException)
            {
                throw new Exception(Globalization.GetString("LocalCache.GetSystemLoginName.DataError", m_iClientId), p_objException);
            }
            return sTableName;
        }
        #endregion

        #region Get System User Login Name for list of User Ids
        
        /// <summary>
        /// Return the Login Names for Comma separated User Ids
        /// </summary>
        /// <param name="p_sUserIdList">Comma separated User Ids </param>
        /// <param name="p_iDsnId">DSN id for the current DB</param>
        /// <param name="p_sSecConnStr">Connection String for Security DB</param>
        /// <param name="p_iClientId">Client Id</param>
        /// <returns></returns>
        public string GetSystemLoginNames(string p_sUserIdList, int p_iDsnId, string p_sSecConnStr, int p_iClientId)
        {
            string sLoginNames = string.Empty;
            string sKey = string.Empty;
            DbReader objReader = null;
            try
            {
                sKey = String.Format("GSLN_{0}{1}", p_sUserIdList, p_iDsnId);
                if (m_Cache == null)
                {
                    m_Cache = new CacheTable(this.DbConnectionString, p_iClientId);
                }
                if (m_Cache.ContainsKey(sKey))
                    return (string)m_Cache[sKey];

                objReader = DbFactory.ExecuteReader(p_sSecConnStr, String.Format("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID IN ({0}) AND DSNID={1}", p_sUserIdList, p_iDsnId));
                while (objReader.Read())
                {
                    sLoginNames = sLoginNames + objReader.GetValue("LOGIN_NAME") + ", ";
                }

                if (sLoginNames != string.Empty)
                    sLoginNames = sLoginNames.Substring(0, sLoginNames.Length - 2);
                m_Cache.Add(sKey, sLoginNames);
            }
            catch (Exception p_objException)
            {
                throw new Exception(Globalization.GetString("LocalCache.GetSystemLoginNames.DataError", m_iClientId), p_objException);
            }
            return sLoginNames;
        }
        #endregion

        #region MITS 12137 Clear Cache of Parent code for closed claims

        // Mits 12137- shivendu - function to clear the cache
        /// <summary>
        /// Clears the cache
        /// </summary>
        /// <param name="p_iLOB">line of business</param>
        /// <param name="p_idsnId">DSN ID </param>
        public void ClearCacheForParentCodeForClosedClaims(int p_iLOB, int p_idsnId)
        {
            string sKey = String.Format("CODLOB_{0}{1}", p_iLOB, p_idsnId);
            try
            {
                if (m_Cache.ContainsKey(sKey))
                    m_Cache.Remove(sKey);
            }
            catch (Exception e)
            { throw new DataModelException(Globalization.GetString("LocalCache.RemoveRelatedCodeId.Exception", m_iClientId), e); }
        }
        #endregion

        /// <summary>
        /// Gets the base connection string by removing the UID and PWD attributes
        /// from the connection string
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <returns>returns the base connection string without the user name and password attributes</returns>
        /// <remarks>Since user names and passwords can differ from connetion to connection especially under
        /// the case of BES, this information has to be removed to ensure that configuration changes 
        /// are applied uniformly across all users regardless of BES users (specifically in the areas of UI customization)</remarks>
        private static string GetBaseConnectionString(string strConnectionString)
        {
            string strRegExpr = "^(?<CONN_STR_VALUE>[^;].+)UID=(?<UID_VALUE>[^;]+);PWD=(?<PWD_VALUE>[^;]+)[;]*$";
            const string GROUP_NAME = "CONN_STR_VALUE";

            string strMatchValue = string.Empty;

            //Get the Match for the string
            Match regExMatch = Regex.Match(strConnectionString, strRegExpr, RegexOptions.IgnoreCase);

            //Get the matching value for the specified group name
            strMatchValue = regExMatch.Groups[GROUP_NAME].Value;

            return strMatchValue;
        }//method: GetBaseConnectionString()

        #region Get Module Security Group Name
        /// <summary>
        /// Returns the Group Name corresponding to the entererd Group Id
        /// </summary>
        /// <param name="groupId"> The Group Id</param>
        /// <param name="dsnId"> DSN ID is passed to make the key unique across multiple DSNs</param>
        /// <returns>Module Security Group Name</returns>
        public string GetGroupName(int groupId, int dsnId)
        {
            string sTableName = string.Empty;
            string sKey = string.Empty;
            object objTableName = null;   
            try
            {
                sKey = String.Format("GSGN_{0}{1}", groupId, dsnId);
                if (m_Cache == null)
                {
                    m_Cache = new CacheTable(this.DbConnectionString, m_iClientId);
                }
                if (m_Cache.ContainsKey(sKey))
                    return (string)m_Cache[sKey];

                //pmittal5 Mits 18022 10/09/09 - If NULL is returned by the query, calling ToString() throws an error.
                //sTableName = DbFactory.ExecuteScalar(secConnStr, String.Format("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID={0} AND DSNID={1}", userId, dsnId)).ToString();
                objTableName = DbFactory.ExecuteScalar(this.DbConnectionString, String.Format("SELECT GROUP_NAME from USER_GROUPS where GROUP_ID = {0}", groupId));
                if (objTableName != null && objTableName != System.DBNull.Value)
                    sTableName = objTableName.ToString();
                //End - pmittal5

                m_Cache.Add(sKey, sTableName);
            }
            catch (Exception p_objException)
            {
                throw new Exception(Globalization.GetString("LocalCache.GetGroupName.DataError", m_iClientId), p_objException);
            }
            return sTableName;
        }
        #endregion

        #region Get Module Security Group Name
        /// <summary>
        /// Returns the Comma Separated Group Name corresponding to the entererd Comma separated Group Ids
        /// </summary>
        /// <param name="p_sGroupIdList"> The Group Id</param>
        /// <param name="p_iDsnId"> DSN ID is passed to make the key unique across multiple DSNs</param>
        /// <returns>Comma separated Module Security Group Names</returns>
        public string GetGroupNames(string p_sGroupIdList, int p_iDsnId)
        {
            string sGroupNames = string.Empty;
            string sKey = string.Empty;
            DbReader objReader = null;
            try
            {
                sKey = String.Format("GSGN_{0}{1}", p_sGroupIdList, p_iDsnId);
                if (m_Cache == null)
                {
                    m_Cache = new CacheTable(this.DbConnectionString, m_iClientId);
                }
                if (m_Cache.ContainsKey(sKey))
                    return (string)m_Cache[sKey];

                objReader = DbFactory.ExecuteReader(this.DbConnectionString, String.Format("SELECT GROUP_NAME from USER_GROUPS where GROUP_ID IN ({0})", p_sGroupIdList));
                while (objReader.Read())
                {
                    sGroupNames = sGroupNames + objReader.GetValue("GROUP_NAME") + ", ";
                }

                if (sGroupNames != string.Empty)
                    sGroupNames = sGroupNames.Substring(0, sGroupNames.Length - 2);
                m_Cache.Add(sKey, sGroupNames);
            }
            catch (Exception p_objException)
            {
                throw new Exception(Globalization.GetString("LocalCache.GetGroupNames.DataError", m_iClientId), p_objException);
            }
            return sGroupNames;
        }
        #endregion
		// npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Check
		// Get the Print Option based on the Distribution Type
        #region Get Printing Options based on Distribution Type
        
        public int GetPrintOptions(int p_iDistributionType)
        {
            string sKey = string.Empty;
            // npadhy JIRA RMA- 11632Initialize Default Value to -1 as if a mapping does not exist in DB, we were returning 0 which is "To Printer"
            int iPrintOption = -1;
            DbReader objReader = null;
            try
            {
                sKey = String.Format("GPOD_{0}", p_iDistributionType);
                if (m_Cache == null)
                {
                    m_Cache = new CacheTable(this.DbConnectionString, m_iClientId);
                }
                if (m_Cache.ContainsKey(sKey))
                    return (Int32)m_Cache[sKey];
                // npadhy JIRA RMA-10227 - As we do not have any distribution type for EFT, and we hard code a distribution type for EFT and use it in Recreate 
                // For EFT we do not have Distribution Type, but it will always be printed using file system. But we need to keep track of a print option which can be used while Reprint
                if (p_iDistributionType != this.GetCodeId("EFT", "DISTRIBUTION_TYPE"))
                {
                    objReader = DbFactory.ExecuteReader(this.DbConnectionString, String.Format("SELECT PRINT_OPTION FROM CHECK_PRINT_OPTIONS WHERE DSTRBN_TYPE_CODE = ({0})", p_iDistributionType));
                    if (objReader.Read())
                    {
                        iPrintOption = Conversion.ConvertObjToInt(objReader.GetValue("PRINT_OPTION"), m_iClientId);
                    }
                }
                else
                {
                    iPrintOption = -999;
                }

                m_Cache.Add(sKey, iPrintOption);
            }
            catch (Exception p_objException)
            {
                throw new Exception(Globalization.GetString("LocalCache.GetPrintOptions.DataError", m_iClientId), p_objException);
            }
            return iPrintOption;
        }
        #endregion
		// npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Check
        /// <summary>
        /// Provides Caching for codes
        /// </summary>
        /// <remarks>
        /// Cache "new" codes for performance to prevent unnecessary DataModel requests.
        /// Therefore, cache them statically (across page requests) and include the connection string in 
        // the key to ensure that you only get the ones back from the correct
        // database even though many different databases could be in use 
        // simultaneously on a single server.
        /// </remarks>
        protected class CacheTable
        {
            //private static Hashtable m_StaticCache = new Hashtable(50);
            private string m_sConnectionString = string.Empty;
            private int m_iClientId = 0;
            //private int m_iDSNId = 0;

            //string sSql = string.Empty;
            internal CacheTable(string sConnectionString,int p_iClientId)
            {
                // npadhy RMA-6617 In case of oracle the Connection string across the DSNs could be same as Server and Database name store the Service name only.
                // 
                //MITS 11896 : Umesh
                //removing UID and PWD from connection string because key should be unique for database.
                //Key should not get changed if database user id and password changed e.g. in case of BES.
                if (DbFactory.GetDatabaseType(sConnectionString) == eDatabaseType.DBMS_IS_SQLSRVR)
                    m_sConnectionString = GetBaseConnectionString(sConnectionString);
                else
                    m_sConnectionString = sConnectionString;
                // Get DSN Id from Security Database
                //sSql = string.Format("SELECT DSNID FROM DATA_SOURCE_TABLE WHERE CONNECTION_STRING LIKE '{0}'", m_sConnectionString);

                //m_iDSNId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(RMConfigurationManager.GetConfigConnectionString("RMXSecurity", p_iClientId), sSql), p_iClientId);


                m_iClientId = p_iClientId;
                //CacheCommonFunctions.ClearCache(String.Format("_m_LStaticCache_{0}", m_sConnectionString), m_iClientId);
            }
            public object this[string sKey]
            {
                get
                {
                    //return m_StaticCache[String.Format("{0}_{1}", sKey, m_sConnectionString)];
                    return CacheCommonFunctions.RetreiveValueFromCache<object>(String.Format("{0}_m_LStaticCache_{1}", sKey, m_sConnectionString), m_iClientId);
                }
                set
                {
                    //m_StaticCache[String.Format("{0}_{1}", sKey, m_sConnectionString)] = value;
                    CacheCommonFunctions.UpdateValue2Cache<object>(String.Format("{0}_m_LStaticCache_{1}", sKey, m_sConnectionString), m_iClientId, value);
                }
            }
            public bool ContainsKey(object sKey)
            {
                //return m_StaticCache.ContainsKey(String.Format("{0}_{1}", sKey, m_sConnectionString));
                return CacheCommonFunctions.CheckIfKeyExists(String.Format("{0}_m_LStaticCache_{1}", sKey, m_sConnectionString), m_iClientId);
            }
            public void Add(object sKey, object objValue)
            {
                //Not critical if collection already contains this memeber -
                //just means we lost a race condition but the content data will be the same so who cares.
                try
                {
                    //m_StaticCache.Add(String.Format("{0}_{1}", sKey, m_sConnectionString), objValue);
                    CacheCommonFunctions.AddValue2Cache<object>(String.Format("{0}_m_LStaticCache_{1}", sKey, m_sConnectionString), m_iClientId, objValue);
                }
                catch { };
            }
            //Rahul - mits 10300 (17 sept 2007) the related codes if changed from table maintanance 
            // were not getting updated into the  cache, so added a provision to remove the entry from cache.
            public void Remove(object sKey)
            {
                try
                {
                    //m_StaticCache.Remove(String.Format("{0}_{1}", sKey, m_sConnectionString));
                    CacheCommonFunctions.RemoveValueFromCache(String.Format("{0}_m_LStaticCache_{1}", sKey, m_sConnectionString), m_iClientId);
                }
                catch { };
            }
        }//class CacheTable

        /// <summary>
        /// DynamicCacheTable 
        /// to contain ENTITY data and tables that change frequently
        /// </summary>
        protected class DynCacheTable
        {
            //private Hashtable m_DynamicCache = new Hashtable(50);
            private string m_sConnectionString = string.Empty;
            private int m_iClientId = 0;
            //private int m_iDSNId = 0;

            //string sSql = string.Empty;
            internal DynCacheTable(string sConnectionString,int p_iClientId)
            {
                // npadhy RMA-6617 In case of oracle the Connection string across the DSNs could be same as Server and Database name store the Service name only.
                //MITS 11896 : Umesh
                //removing UID and PWD from connection string because key should be unique for database.
                //Key should not get changed if database user id and password changed e.g. in case of BES.
                if (DbFactory.GetDatabaseType(sConnectionString) == eDatabaseType.DBMS_IS_SQLSRVR)
                    m_sConnectionString = GetBaseConnectionString(sConnectionString);
                else
                    m_sConnectionString = sConnectionString;

                // Get DSN Id from Security Database
                //sSql = string.Format("SELECT DSNID FROM DATA_SOURCE_TABLE WHERE CONNECTION_STRING LIKE '{0}'", m_sConnectionString);

                //m_iDSNId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(RMConfigurationManager.GetConfigConnectionString("RMXSecurity", p_iClientId), sSql), p_iClientId);

                m_iClientId = p_iClientId;

                //CacheCommonFunctions.ClearCache(String.Format("_m_LDynamicCache_{0}", m_sConnectionString),m_iClientId);
            }
            public object this[string sKey]
            {
                get 
                { 
                    //return m_DynamicCache[String.Format("{0}_{1}", sKey, m_sConnectionString)];
                    return CacheCommonFunctions.RetreiveValueFromCache<object>(String.Format("{0}_m_LDynamicCache_{1}", sKey, m_sConnectionString), m_iClientId);
                }
                set 
                { 
                    //m_DynamicCache[String.Format("{0}_{1}", sKey, m_sConnectionString)] = value; 
                    CacheCommonFunctions.UpdateValue2Cache<object>(String.Format("{0}_m_LDynamicCache_{1}", sKey, m_sConnectionString), m_iClientId, value);
                }
            }
            public bool ContainsKey(object sKey)
            {
                //return m_DynamicCache.ContainsKey(String.Format("{0}_{1}", sKey, m_sConnectionString));
                return CacheCommonFunctions.CheckIfKeyExists(String.Format("{0}_m_LDynamicCache_{1}", sKey, m_sConnectionString), m_iClientId);
            }
            public void Add(object sKey, object objValue)
            {
                //Not critical if collection already contains this memeber -
                //just means we lost a race condition but the content data will be the same so who cares.
                try 
                {
                    //m_DynamicCache.Add(String.Format("{0}_{1}", sKey, m_sConnectionString), objValue); 
                    CacheCommonFunctions.AddValue2Cache<object>(String.Format("{0}_m_LDynamicCache_{1}", sKey, m_sConnectionString), m_iClientId, objValue);
                }
                catch { };
            }
            //Rahul - mits 10300 (17 sept 2007) the related codes if changed from table maintanance 
            // were not getting updated into the  cache, so added a provision to remove the entry from cache.
            public void Remove(object sKey)
            {
                try 
                {
                    //m_DynamicCache.Remove(String.Format("{0}_{1}", sKey, m_sConnectionString)); 
                    CacheCommonFunctions.RemoveValueFromCache(String.Format("{0}_m_LDynamicCache_{1}", sKey, m_sConnectionString), m_iClientId);
                }
                catch { };
            }
        }//class DynCacheTable


        public string GetPolicyInterfaceConfigSettings(string p_sSectionName, string p_sKey)
        {
            string sDisplayfields = string.Empty;
            
            try
            {
                string sKey = String.Format("PPCID_{0}{1}", p_sSectionName, p_sKey);
                if (m_Cache.ContainsKey(sKey))
                    return (string)m_Cache[sKey];
                else
                {
                    if ((p_sSectionName.CompareTo("PolicyHolderDupCheckFields") == 0 || p_sSectionName.CompareTo("AdditionalInterestDupCheckFields") == 0 || p_sSectionName.CompareTo("DriverDupCheckFields") == 0) && p_sKey==string.Empty)
                    {
                        NameValueCollection strName = RMConfigurationManager.GetNameValueSectionSettings(p_sSectionName,this.DbConnectionString, m_iClientId);
                        foreach (string sName in strName)
                        {
                            string sValue = RMConfigurationManager.GetNameValueSectionSettings(p_sSectionName, this.DbConnectionString, m_iClientId)[sName];
                            if (string.Equals(sValue,"TRUE",StringComparison.InvariantCultureIgnoreCase))
                            {
                                if (sDisplayfields != string.Empty)
                                    sDisplayfields = sDisplayfields + "," + sName;
                                else
                                    sDisplayfields = sName;

                            }

                        }
                    }
                    else
                        sDisplayfields = RMConfigurationManager.GetNameValueSectionSettings(p_sSectionName, this.DbConnectionString, m_iClientId)[p_sKey];

                    return sDisplayfields;
                }


            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("LocalCache.GetPolicyInterfaceConfigSettings.DataError", m_iClientId), p_objException);
            }
            finally { }
           
        }

        //avipinsrivas Start : Worked for Jira-340
        /// <summary>
        ///Return True if Table_Id is from Org Hierarchy
        /// </summary>
        /// <param name="p_iTableID">TABLE_ID to check if it exist in Org Hierarchy or not</param>
        /// <returns>True if Tableid is from Org Hierarchy</returns>
        public bool FromOrgHierarchy(int p_iTableID)
        {
            bool bFromOrgHierarchy = false;
            string sTableName = "";

            if (p_iTableID != 0)
            {
                sTableName = GetTableName(p_iTableID);
                bFromOrgHierarchy = FromOrgHierarchy(sTableName);                
            }

            return bFromOrgHierarchy;
        }
        /// <summary>
        ///Return True if System Table Name is from Org Hierarchy
        /// </summary>
        /// <param name="p_sSystemTableName">System Table Name to check if it exist in Org Hierarchy or not</param>
        /// <returns>True if System Table Name is from Org Hierarchy</returns>
        public bool FromOrgHierarchy(string p_sSystemTableName)
        {
            bool bFromOrgHierarchy = false;

            if (!string.IsNullOrEmpty(p_sSystemTableName))
            {
                p_sSystemTableName = p_sSystemTableName.ToUpper();
                switch (p_sSystemTableName)
                {
                    case "CLIENT":          //1005
                    case "COMPANY":         //1006
                    case "OPERATION":       //1007
                    case "REGION":          //1008
                    case "DIVISION":        //1009
                    case "LOCATION":        //1010
                    case "FACILITY":        //1011
                    case "DEPARTMENT":      //1012
                        bFromOrgHierarchy = true;
                        break;
                }
            }

            return bFromOrgHierarchy;
        }
        //avipinsrivas End

        //start JIRA 7810 Snehal
        /// <summary>
        /// Retrive all the code ids corresponding to a Parent
        /// </summary>
        /// <param name="RelatedCodeID">The Parent Code Id</param>
        /// <returns>Comma Separated code ids for the child</returns>
        //public string GetChildCodeIds(int RelatedCodeID)
        //{

        //    string sKey = String.Format("CHLDCOD_{0}", RelatedCodeID);
        //    StringBuilder sList = null;
        //    try
        //    {
        //        if (m_Cache.ContainsKey(sKey))
        //            return (string)m_Cache[sKey];
        //        else
        //        {
        //            sList = new StringBuilder();
        //            string sSQL = String.Format(@"SELECT CODE_ID FROM CODES WHERE RELATED_CODE_ID={0}", RelatedCodeID);

        //            using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, sSQL))
        //            {
        //                while (objReader.Read())
        //                {
        //                    if (sList.ToString() == "")
        //                        sList.Append(objReader.GetValue("CODE_ID"));
        //                    else
        //                    {
        //                        sList.Append(",");
        //                        sList.Append(objReader.GetValue("CODE_ID"));
        //                    }
        //                }


        //                m_Cache.Add(sKey, sList.ToString());
        //                return sList.ToString();
        //            }
        //        }
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new DataModelException(Globalization.GetString("LocalCache.GetChildCodeIds.Exception", m_iClientId), p_objEx);
        //    }
        //}
        //End JIRA 7810 
        //sachin 6385
        /// <summary>
        /// function to get all child from Parent base on related code id.
        /// </summary>
        /// <param name="iTableId">iTableId is Like Context.LocalCache.GetTableId("RESERVE_STATUS")</param>
        /// <param name="iCodeId">iCodeId is Like  Context.LocalCache.GetCodeId("H", "RESERVE_STATUS_PARENT")</param>
        /// <returns></returns>
        public string GetChildCodeIds(int iTableId, int iCodeId)
        {
            string sKey = String.Format("CHLDCOD_{0}{1}", iTableId, iCodeId);
            StringBuilder sList = null;
            if (m_Cache.ContainsKey(sKey))
                return (string)m_Cache[sKey];
            else
            {
                string sbSql = string.Empty;
                sList = new StringBuilder();
                sbSql= string.Format("SELECT CODE_ID FROM CODES WHERE TABLE_ID = {0} AND RELATED_CODE_ID= {1}  AND DELETED_FLAG=0", iTableId, iCodeId);
                //objRdr = DbFactory.GetDbReader(this.DbConnectionString, sbSql.ToString());
                //if (objRdr.Read())
                //{
                //    if (objRdr.GetValue("CODE_ID") != DBNull.Value)
                //        iReturnValue = objRdr.GetInt32("CODE_ID");
                //    else
                //        throw new RMAppException(Globalization.GetString("ReserveCurrent.GetChildIdforHold.ChildCodeNotFoundForHold", m_iClientId));
                //}
                //else
                //{
                //    throw new RMAppException(Globalization.GetString("ReserveCurrent.GetChildIdforHold.ChildCodeNotFoundForHold", m_iClientId));
                //}
                //if (objRdr != null)
                //    objRdr.Close();
                using (DbReader objReader = DbFactory.ExecuteReader(this.DbConnectionString, sbSql.ToString()))
                {
                    while (objReader.Read())
                    {
                        if (sList.ToString() == "")
                            sList.Append(objReader.GetValue("CODE_ID"));
                        else
                        {
                            sList.Append(",");
                            sList.Append(objReader.GetValue("CODE_ID"));
                        }
                    }


                    m_Cache.Add(sKey, sList.ToString());
                    return sList.ToString();
                }
            }
            //m_Cache.Add(sKey, iReturnValue);
            //return iReturnValue;
        }
        //sachin 6385
    }
}
    #endregion
