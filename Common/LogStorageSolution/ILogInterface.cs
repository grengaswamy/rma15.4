﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Riskmaster.Common   
{
    /// <summary>
    /// 
    /// </summary>
    public interface ILogInterface
    {
        //Function for logging errors.
        void Add(LogData objLogData);
    }
    /// <summary>
    /// 
    /// </summary>
    public class LogStorageTypeEnumeration
    {
        //Storage type for error log.
        public enum LogStorageType
        {
            Database = 1,
            FileSystem = 2,
            AmazonS3 = 3
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class LogStorageOutPut
    {
        public bool Success
        {
            get;
            set;
        }
        public int RowId
        {
            get;
            set;
        }
    }
}
