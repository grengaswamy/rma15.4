﻿using Amazon.S3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3.Model;
using System.IO;
using System.Web.Configuration;
namespace Riskmaster.Common
{
    /// <summary>
    /// Function for store log file on Amazon S3
    /// </summary>
    /// Add by kuladeep for Multi-tan.
    class LogCloudStorage : ILogInterface
    {
        public void Add(LogData objLogData)
        {
            byte[] file = null;
            string sRegion = string.Empty;
            const string LOG_CATEGORY_PRESENTATIONLAYER = "PresentationLayer";
            const string LOG_CATEGORY_BUSINESSLAYER = "BusinessLayer";
            string BUCKET_NAME = "rmas3bucket" + objLogData.ClientId;
            string sErrorlogKey = string.Empty;
            sRegion = WebConfigurationManager.AppSettings["Region"];
            using (IAmazonS3 client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.GetBySystemName(sRegion)))
            {
                try
                {
                    //Read log data prepare for file.
                    StringBuilder sbMessage = new StringBuilder();
                    sbMessage.Append("Timestamp: " + DateTime.Now);
                    sbMessage.AppendLine();
                    sbMessage.Append("Message: " + objLogData.Message);
                    sbMessage.AppendLine();
                    sbMessage.Append("Category: " + objLogData.Category);
                    sbMessage.AppendLine();
                    sbMessage.Append("Priority: " + objLogData.Priority);
                    sbMessage.AppendLine();
                    sbMessage.Append("EventId: " + objLogData.EventId);
                    sbMessage.AppendLine();
                    if (!objLogData.RMParamList.Equals(null))
                    {
                        sbMessage.Append("Extended Properties:");
                        sbMessage.AppendLine();
                        if(objLogData.RMParamList.Count()>0)
                        {
                            Dictionary<string, object> ExtendedLogs = (Dictionary<string, object>)(objLogData.RMParamList);
                            foreach (KeyValuePair<string, object> pair in ExtendedLogs)
                            {
                                sbMessage.Append(pair.Key+":");
                                sbMessage.Append(pair.Value);
                                sbMessage.AppendLine();
                            }
                        }
                    }

                    //TODO -Anything required in log can be append with this string.
                    //Convert log data to byte.
                    file = Encoding.ASCII.GetBytes(Convert.ToString(sbMessage));
                    if(string.IsNullOrEmpty(objLogData.Category))
                      {
                           sErrorlogKey = "rmAErrorlog" + DateTime.Now.ToString("yyyyMMddhhmmss");
                      }
                    else
                      {
                           sErrorlogKey = "rmAErrorlog_" + objLogData.Category + DateTime.Now.ToString("yyyyMMddhhmmss");
                      }

                   PutObjectRequest request = new PutObjectRequest()
                    {
                         BucketName = BUCKET_NAME,
                         Key = @"Logs/ "+ (objLogData.Category == LOG_CATEGORY_PRESENTATIONLAYER ? LOG_CATEGORY_PRESENTATIONLAYER : LOG_CATEGORY_BUSINESSLAYER) + @"/" + sErrorlogKey + ".log",
                         ContentType = "application/octet-stream",
                         InputStream = new MemoryStream(file),
                    };
                   PutObjectResponse responseOut = client.PutObject(request);
                }
                catch (AmazonS3Exception amazonS3Exception)
                {
                }
            }
        }
    }
}
