﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.Common.Financials
{
    public class PolicyLimit
    {
        private string m_sConnStr = string.Empty;
        private int m_iClientId = 0;
        private bool m_bPolicyLimitExceeded = false;
        //private bool m_bCovOccLimitExceeded = false;
        //private bool m_bPolAggLimitExceeded = false;
        //private bool m_bPolOccLimitExceeded = false;
        public bool PolicyLimitExceeded
        {
            get
            {
                return m_bPolicyLimitExceeded;
            }
            set
            {
                m_bPolicyLimitExceeded = value;
            }
        }

        private bool m_bUseDedTracking = false;
        public bool UseDedTracking
        {
            get
            {
                return m_bUseDedTracking;
            }
            set
            {
                m_bUseDedTracking = value;
            }
        }

        private int m_iPolicyLOB = 0;
        public int PolicyLOB
        {
            get
            {
                return m_iPolicyLOB;
            }
            set
            {
                m_iPolicyLOB = value;
            }
        }

        

        private double m_dblFundsAmount = 0;
        public double FundsAmount
        {
            get
            {
                return m_dblFundsAmount;
            }
            set
            {
                m_dblFundsAmount = value;
            }
        }

        //public bool CovOccLimitExceeded
        //{
        //    get
        //    {
        //        return m_bCovOccLimitExceeded;
        //    }
        //    set
        //    {
        //        m_bCovOccLimitExceeded = value;
        //    }
        //}



        //public bool PolAggLimitExceeded
        //{
        //    get
        //    {
        //        return m_bPolAggLimitExceeded;
        //    }
        //    set
        //    {
        //        m_bPolAggLimitExceeded = value;
        //    }
        //}



        //public bool PolOccLimitExceeded
        //{
        //    get
        //    {
        //        return m_bPolOccLimitExceeded;
        //    }
        //    set
        //    {
        //        m_bPolOccLimitExceeded = value;
        //    }
        //}

        public PolicyLimit(int iClientid, string sConnStr)
        {
            m_sConnStr= sConnStr;
            m_iClientId = iClientid;
        }

        private void GetDataForPolicyLimit(int iCovId, ref string sCovIdsFromPolicy, ref  string sCovIdsFromCoverage, ref  string sExternalPolicyKey, ref  string sCovKey,ref string sCovIdsonPolicy)
        {
            string sSQL = string.Empty;
            int iPolicyId = 0;
            long iPolicyUnitRowId = 0;
            int iCovType = 0;
            string sCovSeqNum = string.Empty;
            string sTransSeqNum = string.Empty;

            try
            {

                GetPolicyDetails(iCovId, out iPolicyId, out sCovKey);

                GetPolicyLob(iPolicyId);
                sExternalPolicyKey = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sConnStr, "SELECT EXTERNAL_POLICY_KEY FROM POLICY WHERE POLICY_ID=" + iPolicyId));

                sCovIdsFromPolicy = GetCoverageIdsFromPolicyKey(sExternalPolicyKey);
                sCovIdsFromCoverage = GetCoverageIdsFromCoverageKey(sExternalPolicyKey, sCovKey);
                sCovIdsonPolicy = GetCoverageIdsFromPolicyId(iPolicyId);
            }
            catch (Exception e)
            {
                throw e;
            }


        }
        private string GetCoverageIdsFromPolicyId(int iPolicyId)
        {
            string sCovids = string.Empty;

            string sSQL = string.Empty;

            try
            {
                sSQL = "SELECT POLICY_X_CVG_TYPE.POLCVG_ROW_ID FROM POLICY_X_UNIT, POLICY_X_CVG_TYPE WHERE POLICY_X_UNIT.POLICY_ID=" + iPolicyId + " AND  POLICY_X_UNIT.POLICY_UNIT_ROW_ID= POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID";

                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL))
                {
                    while (objRdr.Read())
                    {
                        if (string.IsNullOrEmpty(sCovids))
                        {
                            sCovids = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                        }
                        else
                        {
                            sCovids = sCovids + "," + Conversion.ConvertObjToStr(objRdr.GetValue(0));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new RMAppException(Globalization.GetString("PolicyLimit.GetCoverageIdfromPolicy", m_iClientId));
            }
            return sCovids;
        }
        private string GetCoverageIdsFromPolicyKey(string sPolicyKey)
        {
            string sCovids = string.Empty;

            string sSQL = string.Empty;

            try
            {
                sSQL = "SELECT POLICY_X_CVG_TYPE.POLCVG_ROW_ID FROM POLICY,POLICY_X_UNIT, POLICY_X_CVG_TYPE WHERE POLICY.EXTERNAL_POLICY_KEY='" + sPolicyKey + "' AND POLICY.POLICY_ID=POLICY_X_UNIT.POLICY_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID= POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID";

                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL))
                {
                    while (objRdr.Read())
                    {
                        if (string.IsNullOrEmpty(sCovids))
                        {
                            sCovids = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                        }
                        else
                        {
                            sCovids = sCovids + "," + Conversion.ConvertObjToStr(objRdr.GetValue(0));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new RMAppException(Globalization.GetString("PolicyLimit.GetCoverageIdfromPolicy", m_iClientId));
            }
            return sCovids;
        }


        private string GetCoverageIdsFromCoverageKey(string sPolicyKey, string sCoverageKey)
        {
            string sCovids = string.Empty;

            string sSQL = string.Empty;

            try
            {
                sSQL = "SELECT POLICY_X_CVG_TYPE.POLCVG_ROW_ID FROM POLICY,POLICY_X_UNIT, POLICY_X_CVG_TYPE WHERE POLICY.EXTERNAL_POLICY_KEY='" + sPolicyKey + "' AND  POLICY.POLICY_ID=POLICY_X_UNIT.POLICY_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID= POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID AND POLICY_X_CVG_TYPE.COVERAGE_KEY='" + sCoverageKey + "'";

                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL))
                {
                    while (objRdr.Read())
                    {
                        if (string.IsNullOrEmpty(sCovids))
                        {
                            sCovids = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                        }
                        else
                        {
                            sCovids = sCovids + "," + Conversion.ConvertObjToStr(objRdr.GetValue(0));
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw new RMAppException(Globalization.GetString("PolicyLimit.GetCoverageIdsFromCoverageKey", m_iClientId));
            }
            return sCovids;
        }
        private double GetCollectiononDeductibleReserve(string sCovIds, bool UseDedLimitTracking, int iLOB)
        {
            string sSQL = string.Empty;
            string sDedResvTypeCode = string.Empty;
            try
            {
                if (UseDedLimitTracking)
                {


                    sSQL = "SELECT DISTINCT DED_REC_RESERVE_TYPE FROM SYS_PARMS_LOB  WHERE LINE_OF_BUS_CODE=" + iLOB;
                    using (DbReader objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL))
                    {
                        while (objRdr.Read())
                        {
                            if (string.IsNullOrEmpty(sDedResvTypeCode))
                            {
                                sDedResvTypeCode = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                            }
                            else
                            {
                                sDedResvTypeCode = sDedResvTypeCode + "," + Conversion.ConvertObjToStr(objRdr.GetValue(0));
                            }

                        }
                    }

                    sSQL = "SELECT SUM(COLLECTION_TOTAL) FROM RESERVE_CURRENT,COVERAGE_X_LOSS  WHERE COVERAGE_X_LOSS.POLCVG_ROW_ID IN (" + sCovIds + " ) AND COVERAGE_X_LOSS.CVG_LOSS_ROW_ID= RESERVE_CURRENT.POLCVG_LOSS_ROW_ID AND RESERVE_TYPE_CODE IN (" + sDedResvTypeCode + ")";



                    return Conversion.ConvertObjToDouble(DbFactory.ExecuteScalar(m_sConnStr, sSQL), m_iClientId);
                }

                else
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        private double GetPaidAmountonCoverage(string sCovIds, string sRsvTypeCode)
        {
            string sSQL = string.Empty;
            double dblPaidAmount = 0;
            try
            {
                if (!string.IsNullOrEmpty(sRsvTypeCode))
                {
                    sSQL = "SELECT SUM(PAID_TOTAL) FROM RESERVE_CURRENT,COVERAGE_X_LOSS  WHERE RESERVE_TYPE_CODE IN (" + sRsvTypeCode + ") AND    COVERAGE_X_LOSS.POLCVG_ROW_ID IN (" + sCovIds + " ) AND COVERAGE_X_LOSS.CVG_LOSS_ROW_ID= RESERVE_CURRENT.POLCVG_LOSS_ROW_ID";



                    dblPaidAmount = Conversion.ConvertObjToDouble(DbFactory.ExecuteScalar(m_sConnStr, sSQL), m_iClientId);
                }

            }
            catch (Exception e)
            {
                throw e;
            }

            return dblPaidAmount;

        }
        private double GetCollectionAmountonCoverage(string sCovIds, string sRsvTypeCode)
        {
            string sSQL = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(sRsvTypeCode))
                {
                    sSQL = "SELECT SUM(COLLECTION_TOTAL) FROM RESERVE_CURRENT,COVERAGE_X_LOSS  WHERE RESERVE_TYPE_CODE IN (" + sRsvTypeCode + ") AND   COVERAGE_X_LOSS.POLCVG_ROW_ID IN (" + sCovIds + " ) AND COVERAGE_X_LOSS.CVG_LOSS_ROW_ID= RESERVE_CURRENT.POLCVG_LOSS_ROW_ID";
                    return Conversion.ConvertObjToDouble(DbFactory.ExecuteScalar(m_sConnStr, sSQL), m_iClientId);
                }
                return 0;
            }
            catch (Exception e)
            {
                throw e;
            }


        }
        
        private void GetPolicyCoverageLimits(string sPolicyKey, string sCoverageKey, string sCovIdsFromPolicyKey, string sCovIdsfromCoverageKey, int iCurrentCoverageId, string sCovIdsonPolicy,string sMappedPolicyLimitTypes)
        {

            //double dblOccurenceLimit = 0;
            //double dblAggregateLimit = 0;

            double dblAggregateAmountonPolicy = 0;
            double dblAggregateAmountonCoverage = 0;

            double dblOccurenceAmountonPolicy = 0;
            double dblOccurenceAmountonCoverage = 0;
            LocalCache objCache = null;
            string sRsvTypeCode = string.Empty;
            try
            {

               

                objCache = new LocalCache(m_sConnStr, m_iClientId);
                string sSQL = "SELECT * FROM POL_COV_LIMIT WHERE LIMIT_TYPE_CODE IN ("+sMappedPolicyLimitTypes+") AND  POLICY_KEY = '" + sPolicyKey + "' AND (COVERAGE_KEY IN ('','" + sCoverageKey + "') OR COVERAGE_KEY IS NULL )";

                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL))
                {
                    while (objRdr.Read())
                    {

                        using (DbReader objrdr1 = DbFactory.GetDbReader(m_sConnStr, "SELECT CODE1 FROM CODE_X_CODE WHERE DELETED_FLAG=0 AND CODE2=" + Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_TYPE_CODE"), m_iClientId) + " AND REL_TYPE_CODE=" + objCache.GetCodeId("RTTOLT", "CODE_REL_TYPE")))
                        {
                            while (objrdr1.Read())
                            {
                                if (string.IsNullOrEmpty(sRsvTypeCode))
                                {
                                    sRsvTypeCode = Conversion.ConvertObjToStr(objrdr1.GetValue(0));
                                }
                                else
                                {
                                    sRsvTypeCode = sRsvTypeCode + "," + Conversion.ConvertObjToStr(objrdr1.GetValue(0));
                                }
                            }
                        }

                        if (UseDedTracking)
                        {
                            dblAggregateAmountonPolicy = GetPaidAmountonCoverage(sCovIdsFromPolicyKey, sRsvTypeCode) + FundsAmount - GetCollectiononDeductibleReserve(sCovIdsFromPolicyKey);
                            dblAggregateAmountonCoverage = GetPaidAmountonCoverage(sCovIdsfromCoverageKey, sRsvTypeCode) + FundsAmount - GetCollectiononDeductibleReserve(sCovIdsfromCoverageKey);


                            dblOccurenceAmountonPolicy = GetPaidAmountonCoverage(sCovIdsonPolicy, sRsvTypeCode) + FundsAmount - GetCollectiononDeductibleReserve(iCurrentCoverageId.ToString());
                            dblOccurenceAmountonCoverage = GetPaidAmountonCoverage(iCurrentCoverageId.ToString(), sRsvTypeCode) + FundsAmount - GetCollectiononDeductibleReserve(iCurrentCoverageId.ToString());
                        }
                        else
                        {
                            dblAggregateAmountonPolicy = GetPaidAmountonCoverage(sCovIdsFromPolicyKey, sRsvTypeCode) + FundsAmount;
                            dblAggregateAmountonCoverage = GetPaidAmountonCoverage(sCovIdsfromCoverageKey, sRsvTypeCode) + FundsAmount;


                            dblOccurenceAmountonPolicy = GetPaidAmountonCoverage(sCovIdsonPolicy, sRsvTypeCode) + FundsAmount;
                            dblOccurenceAmountonCoverage = GetPaidAmountonCoverage(iCurrentCoverageId.ToString(), sRsvTypeCode) + FundsAmount;
                        }


                        switch (objCache.GetRelatedShortCode(Conversion.ConvertObjToInt(objRdr.GetValue("LIMIT_TYPE_CODE"), m_iClientId)))
                        {
                            case "OCC":

                                //if (bPolAggLimitMapped)
                                //{
                                if (string.IsNullOrEmpty(Conversion.ConvertObjToStr(objRdr.GetValue("COVERAGE_KEY"))))
                                {
                                    if (Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) < (dblOccurenceAmountonPolicy))
                                    {

                                        PolicyLimitExceeded = true;
                                        return;

                                    }
                                }
                                else
                                {
                                    if (Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) < (dblOccurenceAmountonCoverage))
                                    {

                                        PolicyLimitExceeded = true;
                                        return;

                                    }
                                }
                                //}

                                break;
                            case "AGG":
                                //if (bPolOccLimitMapped)
                                //{
                                if (string.IsNullOrEmpty(Conversion.ConvertObjToStr(objRdr.GetValue("COVERAGE_KEY"))))
                                {
                                    if (Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) < (dblAggregateAmountonPolicy))
                                    {

                                        PolicyLimitExceeded = true;
                                        return;

                                    }
                                }
                                else
                                {
                                    if (Conversion.ConvertObjToDouble(objRdr.GetValue("LIMIT_AMOUNT"), m_iClientId) < (dblAggregateAmountonCoverage))
                                    {

                                        PolicyLimitExceeded = true;
                                        return;

                                    }
                                }

                                  
                                //}
                                break;
                           
                        }
                    }
                }

            }
            catch (RMAppException rmexp)
            {
                throw rmexp;
            }
            catch (Exception e)
            {
                throw new RMAppException(Globalization.GetString("PolicyLimit.GetPolicyCoverageLimits", m_iClientId));
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                }
            }


        }
        private double GetCollectiononDeductibleReserve(string sCovIds)
        {
            string sSQL = string.Empty;
            string sDedResvTypeCode = string.Empty;

            try
            {
                sSQL = "SELECT DISTINCT DED_REC_RESERVE_TYPE FROM SYS_PARMS_LOB  WHERE LINE_OF_BUS_CODE=" + PolicyLOB;
                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL))
                {
                    while (objRdr.Read())
                    {
                        if (string.IsNullOrEmpty(sDedResvTypeCode))
                        {
                            sDedResvTypeCode = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                        }
                        else
                        {
                            sDedResvTypeCode = sDedResvTypeCode + "," + Conversion.ConvertObjToStr(objRdr.GetValue(0));
                        }

                    }
                }

                sSQL = "SELECT SUM(COLLECTION_TOTAL) FROM RESERVE_CURRENT,COVERAGE_X_LOSS  WHERE COVERAGE_X_LOSS.POLCVG_ROW_ID IN (" + sCovIds + " ) AND COVERAGE_X_LOSS.CVG_LOSS_ROW_ID= RESERVE_CURRENT.POLCVG_LOSS_ROW_ID AND RESERVE_TYPE_CODE IN (" + sDedResvTypeCode + ")";



                return Conversion.ConvertObjToDouble(DbFactory.ExecuteScalar(m_sConnStr, sSQL), m_iClientId);
            }
            catch (Exception)
            {
                throw new RMAppException(Globalization.GetString("PolicyLimit.GetCollectiononDeductibleReserve", m_iClientId));
            }

        }
        private double GetPaidAmountonCoverage(string sCovIds)
        {
            string sSQL = string.Empty;
            double dblFundsAmount = 0;

            try
            {
                sSQL = "SELECT SUM(PAID_TOTAL) FROM RESERVE_CURRENT,COVERAGE_X_LOSS  WHERE COVERAGE_X_LOSS.POLCVG_ROW_ID IN (" + sCovIds + " ) AND COVERAGE_X_LOSS.CVG_LOSS_ROW_ID= RESERVE_CURRENT.POLCVG_LOSS_ROW_ID";



                return Conversion.ConvertObjToDouble(DbFactory.ExecuteScalar(m_sConnStr, sSQL), m_iClientId);
            }
            catch (Exception e)
            {
                throw new RMAppException(Globalization.GetString("PolicyLimit.GetPaidAmountonCoverage", m_iClientId));
            }

        }

        //private double GetPaidAmountonAllClaims(string sCovIds)
        //{
        //    return GetPaidAmountonCoverage(sCovIds);
        //}
        //private double GetPaidAmountonCurrentClaim(string sCovIds)
        //{
        //    return GetPaidAmountonCoverage(sCovIds);
        //}

        private void GetPolicyLob(int ipolicyId)
        {
            int iPolicyLOB = 0;
            try
            {
                string sSQL = "SELECT POLICY_LOB_CODE FROM POLICY WHERE POLICY_ID=" + ipolicyId;
                iPolicyLOB = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnStr, sSQL), m_iClientId);

                sSQL = "SELECT LINE_OF_BUS_CODE FROM CODES WHERE CODE_ID=" + iPolicyLOB;

                int iLOB = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnStr, sSQL), m_iClientId);

                this.PolicyLOB = iLOB;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void CheckPolicyLimits(int iCoverageId, int iReserveTypeCode)
        {
            string sCovIdsFromPolicyKey = string.Empty;
            string sCovIdsfromCoverageKey = string.Empty;
            string sExternalPolicyKey = string.Empty;
            string sCoverageKey = string.Empty;
            //bool bCovOccLimitMapped = false;
            //bool bCovAggLimitMapped = false;
            //bool bPolOccLimitMapped = false;
            //bool bPolAggLimitMapped = false;
            string sMappedLimitTypes = string.Empty;
            LocalCache objCache = null;
            string sCovIdsonPolicy = string.Empty;
            try
            {
                objCache = new LocalCache(m_sConnStr, m_iClientId);

                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnStr, "SELECT CODE2 FROM CODE_X_CODE WHERE DELETED_FLAG=0 AND CODE1=" + iReserveTypeCode + " AND REL_TYPE_CODE=" + objCache.GetCodeId("RTTOLT", "CODE_REL_TYPE")))
                {
                    while (objRdr.Read())
                    {
                       
                           if(string.IsNullOrEmpty( sMappedLimitTypes))
                           {
                               sMappedLimitTypes =Conversion.ConvertObjToStr( objRdr.GetValue(0));
                           }
                           else
                           {
                               sMappedLimitTypes =sMappedLimitTypes+ ","+ Conversion.ConvertObjToStr(objRdr.GetValue(0));
                           }
                        
                    }
                }

                if (!string.IsNullOrEmpty(sMappedLimitTypes))
                {
                    GetDataForPolicyLimit(iCoverageId, ref sCovIdsFromPolicyKey, ref sCovIdsfromCoverageKey, ref sExternalPolicyKey, ref sCoverageKey,ref sCovIdsonPolicy);


                    GetPolicyCoverageLimits(sExternalPolicyKey, sCoverageKey, sCovIdsFromPolicyKey, sCovIdsfromCoverageKey, iCoverageId,sCovIdsonPolicy,sMappedLimitTypes);

                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                }
            }
        }
        private int GetPolicyDetails(int iCoverageId,  out int iPolicyID, out string sCoverageKey)       //Ankit Start : Worked on MITS - 34297
        {
            int iResult = 0;
            DbReader objReader = null;
            string sSql = string.Empty;
            iPolicyID = 0;
            sCoverageKey = string.Empty;        //Ankit Start : Worked on MITS - 34297

            try
            {
                //rupal:start, policy system interface
                //sSql = string.Format("SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID,COVERAGE_TYPE_CODE , POLICY_X_UNIT.POLICY_ID FROM POLICY_X_CVG_TYPE , POLICY_X_UNIT WHERE POLCVG_ROW_ID = {0} AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID", iCoverageId);
                sSql = string.Format("SELECT  POLICY_X_UNIT.POLICY_ID, POLICY_X_CVG_TYPE.COVERAGE_KEY FROM POLICY_X_CVG_TYPE , POLICY_X_UNIT WHERE POLCVG_ROW_ID = {0} AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID", iCoverageId);
                //rupal:end

                if (iCoverageId != 0)
                {
                    objReader = DbFactory.GetDbReader(m_sConnStr, sSql);
                    if (objReader.Read())
                    {

                        iPolicyID = objReader.GetInt32(0);

                        sCoverageKey = Conversion.ConvertObjToStr(objReader.GetValue(1));       //Ankit Start : Worked on MITS - 34297
                    }
                    objReader.Close();
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PolicyLimit.GetPolicyDetails", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                
            }
            return iResult;
        }


        
    }
}
