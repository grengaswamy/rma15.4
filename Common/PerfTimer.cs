using System;
using System.Diagnostics;



namespace Riskmaster.Common
{
	    /// <summary>
	    /// Provides Diagnostic information for measuring timings
        /// of operations within the application
	    /// </summary>
        /// <remarks>This class encapsulates the System.Diagnostics StopWatch Class</remarks>
		public class PerfTimer
		{
            private Stopwatch m_StopWatch;

            /// <summary>
            /// Gets whether or not the StopWatch timer is currently running
            /// </summary>
            public bool IsTimerRunning
            {
                get
                {
                    return m_StopWatch.IsRunning;
                }//get
            }//property

            /// <summary>
            /// Class constructor
            /// </summary>
            public PerfTimer()
            {
                m_StopWatch = new Stopwatch();
            }//constructor

            /// <summary>
            /// Starts the StopWatch
            /// </summary>
            public void StartTimer()
            {
                m_StopWatch.Start();
            }//method: StartTimer

            /// <summary>
            /// Stops the StopWatch
            /// </summary>
            public void StopTimer()
            {
                m_StopWatch.Stop();
            }//method: StopTimer

            /// <summary>
            /// Resets the StopWatch
            /// </summary>
            public void ResetTimer()
            {
                m_StopWatch.Reset();
            }//method: ResetTimer()

            /// <summary>
            /// Gets the Total Elapsed Time for the StopWatch
            /// </summary>
            /// <returns>the currently elapsed time between when the StopWatch
            /// was started and stopped</returns>
            public TimeSpan GetElapsedTime()
            {
                return m_StopWatch.Elapsed;
            }//method: GetElapsedTime();

		    
		}
		
}
