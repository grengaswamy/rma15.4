﻿using System;
using System.Collections.Generic;
using Riskmaster.Db;
using Riskmaster.Models;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Data.OracleClient;

namespace Riskmaster.Common
{
    public class TempDBStoragecs : ITempFileInterface
    {

        public bool Remove(Dictionary<string, string> parms)
        {
            bool bSuccess = false;
            string ssqlFilter = "";
            string sSQL1 = string.Empty;
            DbConnection objConn1 = null;
            string sTableName = "TEMP_DOCUMENT_STORAGE";
            string sStorageType = string.Empty;
            string sConnString = string.Empty;
            try
            {
                parms.TryGetValue("sqlFilter", out ssqlFilter);
                parms.TryGetValue("ConnString", out sConnString);
                // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
                // Delete the Files from DB Table PMT_DOCUMENT_STORAGE or TEMP_DOCUMENT_STORAGE based on the StorageType passed in the Parameter
                // and also based on the Filter passed. If filter is not sent then FileName would be used as the filter
                parms.TryGetValue("StorageType", out sStorageType);
                if (!string.IsNullOrEmpty(sStorageType))
                {
                    switch (sStorageType.ToUpper())
                    {
                        case "PERMANENT":
                            sTableName = "PMT_DOCUMENT_STORAGE";
                            break;
                        default:
                            sTableName = "TEMP_DOCUMENT_STORAGE";
                            break;
                    }
                }

                if (ssqlFilter != string.Empty)
                    sSQL1 = string.Format("DELETE FROM {0} WHERE  {1}", sTableName, ssqlFilter);
                else
                    sSQL1 = string.Format("DELETE FROM {0}", sTableName);
                objConn1 = DbFactory.GetDbConnection(sConnString);
                objConn1.Open();
                objConn1.ExecuteNonQuery(sSQL1);
                bSuccess = true;

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objConn1 != null)
                {
                    objConn1.Close();
                    objConn1.Dispose();
                    objConn1 = null;

                }
            }
            return bSuccess;
        }

        public FileStorageOutPut Add(Dictionary<string, string> parms, byte[] file)
        {
            string sTableName = "TEMP_DOCUMENT_STORAGE";
            DbWriter objWriter = null;
            string sFileName = string.Empty;
            int iRowId = 0;
            string sConnString = string.Empty;
            FileStorageOutPut objOutput = null;
            bool bSuccess = false;
            string sStorageType = "Permanent"; //mbahl3 jira RMACLOUD-2383
            string sModuleName = "";
            string sDocumentType = "";
            string sRelatedId = "";
            try
            {
                parms.TryGetValue("filename", out sFileName);
                parms.TryGetValue("ConnString", out sConnString);
                //mbahl3
                parms.TryGetValue("StorageType", out sStorageType);

                // npadhy RMACLOUD-2384 Fixed the error while printing the check
                if (!string.IsNullOrEmpty(sStorageType))
                {
                    switch (sStorageType.ToUpper())
                    {
                        case "PERMANENT":
                            sTableName = "PMT_DOCUMENT_STORAGE";
                            break;
                        case "TEMPORARY":
                            sTableName = "TEMP_DOCUMENT_STORAGE";
                            break;
                    }
                }
                //mbahl3 jira RMACLOUD-2383


                //  File.WriteAllBytes(Path.Combine(sFilePath, sFilename), file);

                // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
                // DA requires ModuleName, DocumentType and RelatedId as well to be saved in DB
                parms.TryGetValue("ModuleName", out sModuleName);
                parms.TryGetValue("DocumentType", out sDocumentType);
                parms.TryGetValue("RelatedId", out sRelatedId);
                //Change by kuladeep Start-Jira 527
                //iRowId = Utilities.GetNextUID(sConnString, sTableName);
                objWriter = DbFactory.GetDbWriter(sConnString);
                objWriter.Tables.Add(sTableName);
                objWriter.Fields.Add("FILENAME", sFileName);
                //objWriter.Fields.Add("ROW_ID", iRowId);
                objWriter.Fields.Add("FILE_CONTENT", file);

                // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
                // Check If the Parameters are present. If present pass it in Query
                if (!string.IsNullOrEmpty(sModuleName))
                    objWriter.Fields.Add("MODULE_NAME", sModuleName);
                if (!string.IsNullOrEmpty(sDocumentType) && sDocumentType != "None")
                    objWriter.Fields.Add("DOCUMENT_TYPE", sDocumentType);
                if (!string.IsNullOrEmpty(sRelatedId) && sRelatedId != "0")
                    objWriter.Fields.Add("RELATED_ID", Convert.ToInt32(sRelatedId));

                if (DbFactory.GetDatabaseType(sConnString) == eDatabaseType.DBMS_IS_ORACLE)
                {
                    iRowId = Convert.ToInt32(DbFactory.ExecuteScalar(sConnString, "SELECT SEQ_DOCUMENT_STORAGE_ROW_ID.NEXTVAL FROM DUAL"));
                    objWriter.Fields.Add("ROW_ID", iRowId);
                    objWriter.Execute();
                }
                else
                {
                    objWriter.Fields.Add("ROW_ID", 0, System.Data.ParameterDirection.Output);
                    objWriter.JoinSQL = "SELECT @ROW_ID = SCOPE_IDENTITY();";
                    Dictionary<string, object> retValues = objWriter.Execute(DbWriter.ExecuteMode.OutParam);
                    iRowId = Convert.ToInt32(retValues["ROW_ID"]);
                }
                //Change by kuladeep End -Jira 527

                bSuccess = true;

                objOutput = new FileStorageOutPut();
                objOutput.RowId = iRowId;
                objOutput.Success = bSuccess;

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objWriter = null;
            }
            return objOutput;
        }

        public FileStorageOutPut Add(Dictionary<string, string> parms, FileStream file)
        {
            string sTableName = "TEMP_DOCUMENT_STORAGE";
            DbWriter objWriter = null;
            string sFileName = string.Empty;
            int iRowId = 0;
            string sConnString = string.Empty;
            FileStorageOutPut objOutput = null;
            bool bSuccess = false;
            string sStorageType = "Permanent"; //mbahl3 jira RMACLOUD-2383
            string sModuleName = "";
            string sDocumentType = "";
            string sRelatedId = "";
            try
            {
                parms.TryGetValue("filename", out sFileName);
                parms.TryGetValue("ConnString", out sConnString);
                //mbahl3
                parms.TryGetValue("StorageType", out sStorageType);

                // npadhy RMACLOUD-2384 Fixed the error while printing the check
                if (!string.IsNullOrEmpty(sStorageType))
                {
                    switch (sStorageType.ToUpper())
                    {
                        case "PERMANENT":
                            sTableName = "PMT_DOCUMENT_STORAGE";
                            break;
                        case "TEMPORARY":
                            sTableName = "TEMP_DOCUMENT_STORAGE";
                            break;
                    }
                }
                
                parms.TryGetValue("ModuleName", out sModuleName);
                parms.TryGetValue("DocumentType", out sDocumentType);
                parms.TryGetValue("RelatedId", out sRelatedId);
                
                objWriter = DbFactory.GetDbWriter(sConnString);
                objWriter.Tables.Add(sTableName);
                objWriter.Fields.Add("FILENAME", sFileName);
                
                if (sModuleName != string.Empty)
                    objWriter.Fields.Add("MODULE_NAME", sModuleName);
                if (sDocumentType != "None")
                    objWriter.Fields.Add("DOCUMENT_TYPE", sDocumentType);
                if (sRelatedId != string.Empty && sRelatedId != "0")
                    objWriter.Fields.Add("RELATED_ID", Convert.ToInt32(sRelatedId));
                

                if (DbFactory.GetDatabaseType(sConnString) == eDatabaseType.DBMS_IS_ORACLE)
                {
                    iRowId = Convert.ToInt32(DbFactory.ExecuteScalar(sConnString, "SELECT SEQ_DOCUMENT_STORAGE_ROW_ID.NEXTVAL FROM DUAL"));
                    objWriter.Fields.Add("ROW_ID", iRowId);
                    objWriter.Execute();
                    DBStorageforOracle(iRowId, file, sConnString);
                }
                else
                {
                    objWriter.Fields.Add("ROW_ID", 0, System.Data.ParameterDirection.Output);
                    objWriter.JoinSQL = "SELECT @ROW_ID = SCOPE_IDENTITY();";
                    Dictionary<string, object> retValues = objWriter.Execute(DbWriter.ExecuteMode.OutParam);
                    iRowId = Convert.ToInt32(retValues["ROW_ID"]);
                    DBStorageforSQL(iRowId, file, sConnString);
                }


                //Byte[] bArray = null;
                //bArray = new Byte[file.Length];
                //Stream sTest = null;
                //BinaryReader br = new System.IO.BinaryReader(file);
                //int BUFFER_LENGTH = 12768, Offset = 0; // Chunk size.
                //Offset = 0;
                //Byte[] Buffer = null;
                //Buffer = br.ReadBytes(BUFFER_LENGTH);
                //SqlConnection sqlCon = null;

                //string sNewConnString = sConnString.Substring(sConnString.IndexOf(';') + 1);
                //sqlCon = (SqlConnection)ADODbFactory.CreateDatabase(sNewConnString, "System.Data.SqlClient").CreateConnection();
                //SqlCommand sqlCmd = null;
                //sqlCmd = new SqlCommand("SET NOCOUNT ON;UPDATE PMT_DOCUMENT_STORAGE SET FILE_CONTENT = 0x0 WHERE ROW_ID =" + iRowId, sqlCon);
                //sqlCon.Open();
                //sqlCmd.ExecuteNonQuery();
                //SqlCommand cmdUploadBinary = new SqlCommand();
                //cmdUploadBinary.Connection = sqlCon;
                //cmdUploadBinary.CommandTimeout = 6000;
                //cmdUploadBinary.CommandText = "UPDATE PMT_DOCUMENT_STORAGE SET FILE_CONTENT.write(@Bytes,@Offset," + Buffer.Length + ") WHERE ROW_ID =" + iRowId;

                //SqlParameter OffsetParam, BytesParam;
                //OffsetParam = cmdUploadBinary.Parameters.Add("@Offset", SqlDbType.Int);
                //OffsetParam.Value = Offset;
                //BytesParam = cmdUploadBinary.Parameters.Add("@Bytes", SqlDbType.Binary, BUFFER_LENGTH);
                //BytesParam.Value = Buffer;

                //while (Buffer.Length > 0)
                //{
                //    cmdUploadBinary.ExecuteNonQuery();
                //    Offset += Buffer.Length;
                //    OffsetParam.Value = Offset;
                //    Buffer = br.ReadBytes(BUFFER_LENGTH);
                //    BytesParam.Value = Buffer;
                //}


                bSuccess = true;

                objOutput = new FileStorageOutPut();
                objOutput.RowId = iRowId;
                objOutput.Success = bSuccess;

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objWriter = null;
            }
            return objOutput;
        }
        public void DBStorageforSQL(int iRowId, FileStream fs, string sConnenctionString)
        {
            BinaryReader br = null  ;
            SqlConnection sqlCon = null;
            SqlCommand sqlCmd = null, cmdUploadBinary= null; 
            int BUFFER_LENGTH = 12768, Offset = 0;
            string sNewConnString;
            Byte[] Buffer = null;
            SqlParameter OffsetParam, BytesParam;
            try
            {
                br = new System.IO.BinaryReader(fs);
                Offset = 0;

                Buffer = br.ReadBytes(BUFFER_LENGTH);


                sNewConnString = sConnenctionString.Substring(sConnenctionString.IndexOf(';') + 1);
                sqlCon = (SqlConnection)ADODbFactory.CreateDatabase(sNewConnString, "System.Data.SqlClient").CreateConnection();

                sqlCmd = new SqlCommand("SET NOCOUNT ON;UPDATE PMT_DOCUMENT_STORAGE SET FILE_CONTENT = 0x0 WHERE ROW_ID =" + iRowId, sqlCon);
                sqlCmd.CommandTimeout = 6000;
                sqlCon.Open();
                sqlCmd.ExecuteNonQuery();
                cmdUploadBinary = new SqlCommand();
                cmdUploadBinary.Connection = sqlCon;
                cmdUploadBinary.CommandTimeout = 6000;
                cmdUploadBinary.CommandText = "UPDATE PMT_DOCUMENT_STORAGE SET FILE_CONTENT.write(@Bytes,@Offset," + Buffer.Length + ") WHERE ROW_ID =" + iRowId;


                OffsetParam = cmdUploadBinary.Parameters.Add("@Offset", SqlDbType.Int);
                OffsetParam.Value = Offset;
                BytesParam = cmdUploadBinary.Parameters.Add("@Bytes", SqlDbType.Binary, BUFFER_LENGTH);
                BytesParam.Value = Buffer;

                while (Buffer.Length > 0)
                {
                    cmdUploadBinary.ExecuteNonQuery();
                    Offset += Buffer.Length;
                    OffsetParam.Value = Offset;
                    Buffer = br.ReadBytes(BUFFER_LENGTH);
                    BytesParam.Value = Buffer;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                sqlCon.Close();
                br.Close();
                cmdUploadBinary.Dispose();
                sqlCmd.Dispose();
                fs.Close();

            }
        }
        public void DBStorageforOracle(int iRowId, FileStream fs, string sConnectionString)
        {
            
            OracleConnection orclCon = null;
            OracleCommand orclCmd = null;
            OracleLob myLob = null;
            OracleTransaction orclTrans = null;
            String strSQL = string.Empty;
            int offset = 0;
            byte[] b_Buffer = null;
            int BUFFER_LENGTH = 32768; // Chunk size.
            BinaryReader b_Reader = null;
            
            string connectionString = string.Empty;
            string[] arrConnectionString;
            try
            {
                b_Reader = new System.IO.BinaryReader(fs);

                //since driver and DBQ doesnot supported by ADODBFactory , so remvoing them from connection string
                arrConnectionString = sConnectionString.Split(';');
                for (int item = 0; item < arrConnectionString.GetUpperBound(0); item++)
                {
                    if (arrConnectionString[item] != string.Empty)
                    {
                        if ((arrConnectionString[item].IndexOf("Driver=") == -1) && (arrConnectionString[item].IndexOf("DBQ=") == -1))
                        {
                            connectionString += arrConnectionString[item] + ";";
                        }
                    }
                }

                orclCon = (OracleConnection)ADODbFactory.CreateDatabase(connectionString, "System.Data.OracleClient").CreateConnection();

                using (orclCon)
                {
                    orclCon.Open();
                    orclTrans = orclCon.BeginTransaction();

                    orclCmd = orclCon.CreateCommand();
                    orclCmd.Transaction = orclTrans;

                    strSQL = "DECLARE xx BLOB; BEGIN DBMS_LOB.CREATETEMPORARY(xx, false, 0); :TempBLOB := xx; END;";

                    OracleParameter parmData = new OracleParameter();
                    parmData.Direction = ParameterDirection.Output;
                    parmData.OracleType = OracleType.Blob;
                    parmData.ParameterName = "TempBLOB";
                    orclCmd.Parameters.Add(parmData);

                    orclCmd.CommandText = strSQL;
                    orclCmd.ExecuteNonQuery();

                    myLob = (OracleLob)orclCmd.Parameters[0].Value;
                    b_Buffer = b_Reader.ReadBytes(BUFFER_LENGTH);
                    //buffer = br.ReadBytes((int)fs.Length);

                    //while (buffer.Length > 0)
                    while (b_Buffer.Length > 0)
                    {
                        myLob.Write(b_Buffer, offset, b_Buffer.Length);
                        //offset += buffer.Length;
                        b_Buffer = b_Reader.ReadBytes(BUFFER_LENGTH);
                    }
                    myLob.Write(b_Buffer, offset, b_Buffer.Length);

                    strSQL = "UPDATE DOCUMENT_STORAGE SET DOC_BLOB=:TEXT_Lob WHERE STORAGE_ID=" + iRowId;

                    orclCmd = orclCon.CreateCommand();
                    orclCmd.Transaction = orclTrans;
                    parmData = new OracleParameter();
                    parmData.Direction = ParameterDirection.Input;
                    parmData.OracleType = OracleType.Blob;
                    parmData.ParameterName = "TEXT_Lob";
                    parmData.Value = myLob;
                    orclCmd.Parameters.Add(parmData);

                    orclCmd.CommandText = strSQL;
                    orclCmd.ExecuteNonQuery();

                    orclTrans.Commit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally {
                
                orclCmd.Dispose();
                orclCon.Close();
                b_Reader.Close();
                fs.Close();
            }
        }
        //public byte[] Get(Dictionary<string, string> parms)
        //{
        //    string sFileName = string.Empty;
        //    string ssqlFilter = "";
        //    byte[] file = null;
        //    string sConnString = string.Empty;
        //    //mbahl3
        //    string sStorageType = "Permanent"; //mbahl3
        //    string sSQL = string.Empty;

        //    try
        //    {
        //        parms.TryGetValue("filename", out sFileName);
        //        parms.TryGetValue("sqlFilter", out ssqlFilter);
        //        parms.TryGetValue("ConnString", out sConnString);
        //        //mbahl3
        //        parms.TryGetValue("StorageType", out sStorageType);

        //        if (sStorageType == "Permanent")
        //        {
        //            sSQL = "SELECT FILE_CONTENT FROM PMT_DOCUMENT_STORAGE WHERE " + ssqlFilter;
        //        }
        //        else
        //        {
        //            sSQL = "SELECT FILE_CONTENT FROM TEMP_DOCUMENT_STORAGE WHERE " + ssqlFilter;
        //        }



        //       // using (DbReader objRdr = DbFactory.GetDbReader(sConnString, "SELECT FILE_CONTENT FROM TEMP_DOCUMENT_STORAGE   WHERE  " + ssqlFilter))
        //        using (DbReader objRdr = DbFactory.GetDbReader(sConnString, sSQL))
        //        {
        //            if (objRdr.Read())
        //            {
        //               file= objRdr.GetAllBytes("FILE_CONTENT");
        //            }
        //        }
        //      //  file = File.ReadAllBytes(Path.Combine(sFilePath, sFilename));

        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //    return file;

        //}

        public byte[] Get(Dictionary<string, string> parms)
        {
            string sConnString = string.Empty;
            byte[] file = null;
            string sSql = string.Empty;
            try
            {
                parms.TryGetValue("ConnString", out sConnString);
                // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
                // Retrieve file base on File Name
                sSql = RetrieveFiles(parms);
                using (DbReader objRdr = DbFactory.GetDbReader(sConnString, sSql))
                {
                    if (objRdr.Read())
                    {
                        file = objRdr.GetAllBytes("FILE_CONTENT");
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return file;
        }

        /// <summary>
        /// Retrieve multiple files matching the Criteria
        /// </summary>
        /// <param name="parms">Parameter storing ConnectionString, StorageType, FileName and Filtercriteria</param>
        /// <returns>List of StreamedUploadDocumentType containing filePath, FileContents, FileName, ModuleName, DocumentType and RelatedId</returns>
        public List<StreamedUploadDocumentType> GetMultiple(Dictionary<string, string> parms)
        {
            string sConnString = string.Empty;
            List<StreamedUploadDocumentType> fileDetails = null;
            string sSql = string.Empty;
            StreamedUploadDocumentType objImport = null;
            try
            {
                parms.TryGetValue("ConnString", out sConnString);
                sSql = RetrieveFiles(parms);
                fileDetails = new List<StreamedUploadDocumentType>();
                using (DbReader objRdr = DbFactory.GetDbReader(sConnString, sSql))
                {
                    while (objRdr.Read())
                    {
                        objImport = new StreamedUploadDocumentType();
                        objImport.FileName = objRdr["FILENAME"].ToString();
                        objImport.ModuleName = objRdr["MODULE_NAME"].ToString();
                        objImport.DocumentType = objRdr["DOCUMENT_TYPE"].ToString();
                        objImport.FileContents = objRdr.GetAllBytes("FILE_CONTENT");
                        fileDetails.Add(objImport);
                    }
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            return fileDetails;

        }

        /// <summary>
        /// Creates the Query based on parameters provided
        /// </summary>
        /// <param name="parms">Parms which need to be passed based on which the Files will be retrieved.</param>
        /// <returns>Query that needs to be executed to fetch the files from DB</returns>
        private string RetrieveFiles(Dictionary<string, string> parms)
        {
            string sFileName = string.Empty;
            string ssqlFilter = "";
            string sConnString = string.Empty;

            //mbahl3
            string sStorageType = "Permanent"; //mbahl3
            string sSQL = string.Empty;

            try
            {
                parms.TryGetValue("filename", out sFileName);
                parms.TryGetValue("sqlFilter", out ssqlFilter);
                parms.TryGetValue("ConnString", out sConnString);
                //mbahl3
                parms.TryGetValue("StorageType", out sStorageType);

                if (sStorageType == "Permanent")
                {
                    sSQL = "SELECT FILENAME, MODULE_NAME, DOCUMENT_TYPE, RELATED_ID, FILE_CONTENT FROM PMT_DOCUMENT_STORAGE WHERE " + ssqlFilter;
                }
                else
                {
                    sSQL = "SELECT FILE_CONTENT FROM TEMP_DOCUMENT_STORAGE WHERE " + ssqlFilter;
                }

                return sSQL;
            }
            catch (Exception e)
            {
                throw e;
            }
            return sFileName;

        }
    }
}
