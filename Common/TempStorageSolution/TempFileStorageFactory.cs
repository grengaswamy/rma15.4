﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riskmaster.Common  
{
    public class TempFileStorageFactory
    {
        public static ITempFileInterface CreateInstance(StorageTypeEnumeration.TempStorageType enumStoageType)
        {
            ITempFileInterface objTempFileStorage = null;

            switch (enumStoageType)
            {
                case StorageTypeEnumeration.TempStorageType.Database:
                    objTempFileStorage = new TempDBStoragecs();
                    break;
                case StorageTypeEnumeration.TempStorageType.FileSystem:
                    objTempFileStorage = new TempFileStorageFS();
                    break;
                default:
                    break;
            }
            return objTempFileStorage;
        }
      
    }
}
