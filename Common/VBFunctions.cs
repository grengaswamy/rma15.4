﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Riskmaster.Common
{
    /// <summary>
    /// Class of functions specifically used to emulate VB6 behavior
    /// since VB6 and C#/.Net behavior differ in various areas
    /// which may affect calculations, display etc.
    /// </summary>
    public static class VBFunctions
    {
        /// <summary>
        /// This constant is required for any functions/operations which require offsets
        /// to accommodate VB6 since indexing began with 1 in VB6 instead of 0
        /// </summary>
        const int vbOffSet = 1;

        [Obsolete("Please use the native .Net/C# equivalents of this method instead.")]
        /// <summary>
        /// Conversion function to retrieve the Day of the Week
        /// given a specific DateTime
        /// </summary>
        /// <param name="dtCurrent">DateTime instance</param>
        /// <returns>integer indicating the day of the week as used by VB6 applications</returns>
        /// <remarks>A VB Offset is required since C#/.Net starts Sunday as day 0 while VB6 considered this 
        /// as day 1</remarks>
        public static int VBDayOfWeek(DateTime dtCurrent)
        {
            

            return (int)(dtCurrent.DayOfWeek + vbOffSet);
        }//method: VBDayOfWeek()

        [Obsolete("Please use the native .Net/C# equivalents of this method instead.")]
        /// <summary>
        /// Conversion function to emulate the VB6 Mid function
        /// given a specific string
        /// </summary>
        /// <param name="strUnparsed">string instance</param>
        /// <param name="intStartIndex">integer indicating the start index to begin parsing the string</param>
        /// <param name="intLength">integer indicating the length of the string to extract</param>
        /// <returns>string indicating the Mid value as used by VB6 applications</returns>
        public static string VBMid(string strUnparsed, int intStartIndex, int intLength)
        {
            return strUnparsed.Substring(intStartIndex - vbOffSet, intLength);
        }//method: VBMid()

        [Obsolete("Please use the native .Net/C# equivalents of this method instead.")]
        /// <summary>
        /// Conversion function to emulate the VB6 Mid function
        /// given a specific string
        /// </summary>
        /// <param name="strUnparsed">string instance</param>
        /// <param name="intStartIndex">integer indicating the start index to begin parsing the string</param>
        /// <returns>string indicating the Mid value as used by VB6 applications</returns>
        public static string VBMid(string strUnparsed, int intStartIndex)
        {            
            return strUnparsed.Substring(intStartIndex - vbOffSet);
        }//method: VBMid()

        [Obsolete("Please use the native .Net/C# equivalents of this method instead.")]
        /// <summary>
        /// Conversion function to emulate the VB6 InStr function
        /// given a specific string
        /// </summary>
        /// <param name="strUnparsed">string instance</param>
        /// <param name="strSearchValue">string containing the value to find within the specified string instance</param>
        /// <returns>integer containing the InStr value as used by VB6 applications</returns>
        public static int VBInStr(string strUnparsed, string strSearchValue)
        {
            int intFoundIndex = strUnparsed.IndexOf(strSearchValue, StringComparison.OrdinalIgnoreCase);

            return intFoundIndex + vbOffSet;
        }//method: VBInStr()

        [Obsolete("Please use the native .Net/C# equivalents of this method instead.")]
        /// <summary>
        /// Conversion function to emulate the VB6 InStr function
        /// given a specific string
        /// </summary>
        /// <param name="strUnparsed">string instance</param>
        /// <param name="intStringLength">integer indicating the string length to extract from the specified string instance</param>
        /// <returns>string which is functionally equivalent to the Right function in VB6</returns>
        public static string VBRight(string strUnparsed, int intStringLength)
        {
            int intStartIndex = (strUnparsed.Length) - intStringLength;
            return strUnparsed.Substring(intStartIndex, intStringLength);
        }//method: VBRight()

        [Obsolete("Please use the native .Net/C# equivalents of this method instead.")]
        /// <summary>
        /// Conversion function to emulate the VB6 Left function
        /// given a specific string
        /// </summary>
        /// <param name="strUnparsed">string instance</param>
        /// <param name="intStringLength">integer indicating the string length to extract from the specified string instance</param>
        /// <returns>string which is functionally equivalent to the Left function in VB6</returns>
        public static string VBLeft(string strUnparsed, int intStringLength)
        {            
            return strUnparsed.Substring(0, intStringLength);
        }//method: VBLeft()

        [Obsolete("Please use the native .Net/C# equivalents of this method instead.")]
        /// <summary>
        /// Conversion function to emulate the VB6 DateValue function
        /// given a specific formatted DateTime string
        /// </summary>
        /// <param name="strDateTime">string instance of the formatted DateTime</param>
        /// <returns>DateTime which is functionally equivalent to the DateValue function in VB6</returns>
        public static DateTime VBDateValue(string strDateTime)
        {
            DateTime dtParsedDateTime = DateTime.Parse(strDateTime);
            // akaushik5 Changed for MITS 36908 Starts
            //DateTime dtDateValueDateTime = DateTime.Parse(dtParsedDateTime.ToString("MM/dd/yyyy"));
            DateTime dtDateValueDateTime = DateTime.Parse(dtParsedDateTime.ToString("MM/dd/yyyy"), CultureInfo.InvariantCulture);
            // akaushik5 Changed for MITS 36908 Ends
            return dtDateValueDateTime;

        }//method: VBDateValue()
    }
}
