﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using Riskmaster.Db;
using System.Transactions;


namespace Riskmaster.Common
{
    /// <summary>
    /// Manages Information stored in the RISKMASTER X Session Database
    /// </summary>
    public class RMSessionManager
    {
        const string USER_OBJECT = "OBJ_USER";
        const string USER_LIMITS_OBJECT = "OBJ_RMUSERLIMITS";

        /// <summary>
        /// Gets a DbReader containing the specified customized content
        /// stored in the Session Database
        /// </summary>
        /// <param name="strContentField">string containing the customization file
        /// to query</param>
        /// <returns>DbReader containing the results of the customize query</returns>
        private static DbReader GetCustomizedContent(string strContentField,int iClientId)
        {

            string strSessionConnStr = RMConfigurationSettings.GetSessionDSN(iClientId);
            StringBuilder strBldSQL = new StringBuilder();

            strBldSQL.Append(string.Format("SELECT CONTENT FROM CUSTOMIZE WHERE FILENAME='{0}'", strContentField));

            return DbFactory.ExecuteReader(strSessionConnStr, strBldSQL.ToString());
        } // method: GetCustomizedContent
        /// <summary>
        /// Gets the string of customized content from the database
        /// </summary>
        /// <param name="strContentField">string containing the customization file
        /// to query</param>
        /// <returns>string containing the customized content</returns>
        public static string GetCustomString(string strContentField,int iClientId)
        {
            string strCustomContent = string.Empty;

            strCustomContent = HttpRuntime.Cache[strContentField] as string;

            if (string.IsNullOrEmpty(strCustomContent))
            {
                using (DbReader dbCustomContentRdr = GetCustomizedContent(strContentField,iClientId))
                {
                    while (dbCustomContentRdr.Read())
                    {
                        strCustomContent = dbCustomContentRdr.GetString("CONTENT");

                        //Insert the item into Cache
                        HttpRuntime.Cache.Insert(strContentField, strCustomContent);
                    }//while
                }//using
            }//if

            return strCustomContent;
        } // method: GetCustomString
        /// <summary>
        /// Gets customized content from the Session database
        /// </summary>
        /// <returns>string containing the customized content</returns>
        public static string GetCustomContent(int iClientId)
        {
            return GetCustomString("customize_custom",iClientId);
        } // method: GetCustomContent

        /// <summary>
        /// Gets custom settings from the Session database
        /// </summary>
        /// <returns>string containing the custom settings</returns>
        public static string GetCustomSettings(int iClientId)
        {
            return GetCustomString("customize_settings",iClientId);
        } // method: GetCustomSettings
        #region SessionManager operations
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strConnString"></param>
        /// <returns></returns>
        public static int GetBinaryRowID(string strConnString, int p_iClientId)
        {
            int intBinRowID = 0;

            if (DbFactory.GetDatabaseType(strConnString) == eDatabaseType.DBMS_IS_ORACLE)
            {
                intBinRowID = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(strConnString, "SELECT SEQ_SESSION_BINARY_ROW_ID.NEXTVAL FROM DUAL"), p_iClientId);
            }
            else
            {
                intBinRowID = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(strConnString, "SELECT MAX(NEXT_UNIQUE_ID) FROM SESSION_IDS si WHERE SYSTEM_TABLE_NAME='SESSIONS_X_BINARY'"), p_iClientId);
            }
            
            return intBinRowID;
        }//method: GetBinaryRowID()

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strConnString"></param>
        /// <returns></returns>
        public static List<int> GetBinaryRowIDCollection(string strConnString, int p_iClientId)
        {
            List<int> arrBinaryRowIDs = new List<int>();
            arrBinaryRowIDs.Add(GetBinaryRowID(strConnString, p_iClientId));

            //Oracle uses SEQUENCE, so we have to retrieve the value again from the database
            if (DbFactory.GetDatabaseType(strConnString) == eDatabaseType.DBMS_IS_ORACLE)
            {
                arrBinaryRowIDs.Add(GetBinaryRowID(strConnString, p_iClientId));
            }
            else
            {
                arrBinaryRowIDs.Add(arrBinaryRowIDs[0] + 1);
                
            }

            return arrBinaryRowIDs;
        }//method: GetBinaryRowIDCollection

        /// <summary>
        /// Creates a User Session
        /// </summary>
        /// <param name="strConnString">string containing the session database connection string</param>
        /// <param name="strUserName">string containing the user name</param>
        /// <param name="arrBinaryItems">generic List array containing the binary items to be committed to the database</param>
        /// <returns>string containing the User Session ID</returns>
        public static string CreateSession(string strConnString, string strUserName, List<byte[]> arrBinaryItems, int p_iClientId)
        {
            UserSession objUserSession = null;
            List<int> arrBinaryRowIDs = new List<int>();

            //Create the UserSession object
            objUserSession = CreateUserSession(strConnString, strUserName, p_iClientId);

            //PSARIN2:  FIX Session violation issues  -- Commented code not needed
            //Set up the Binary Row IDs
           // arrBinaryRowIDs = GetBinaryRowIDCollection(strConnString);

            //Update the Session IDS lookup table with latest Binary Row ID //PSARIN2:NO NEED FOR THIS NOW
            //SetBinaryRowID(strConnString, arrBinaryRowIDs[1] + 1);

            //Create the binary items
            objUserSession.BinaryItems = CreateSessionBinaryItemCollection(objUserSession, arrBinaryItems, arrBinaryRowIDs);

            SaveUserSession(strConnString, objUserSession, p_iClientId);

            return objUserSession.SessionID;
        } // method: CreateSession

        /// <summary>
        /// Creates a custom binary object for non-standard Session operations
        /// </summary>
        /// <param name="strConnString"></param>
        /// <param name="strUserName"></param>
        /// <param name="strBinaryName"></param>
        /// <param name="bytBinaryItemValue"></param>
        /// <returns></returns>
        public static string CreateCustomSessionBinary(string strConnString, string strUserName, string strBinaryName, byte[] bytBinaryItemValue, int p_iClientId)
        {
            UserSession objUserSession = null;
            List<int> arrBinaryRowIDs = new List<int>();
            Dictionary<string, UserSessionBinaryItem> dictUserSessionBinaryItems = new Dictionary<string, UserSessionBinaryItem>();
            //PSARIN2: FIX Session violation issues  -- Commented code not needed
            //Set up the Binary Row IDs
            //arrBinaryRowIDs.Add(GetBinaryRowID(strConnString));

            //SetBinaryRowID(strConnString, arrBinaryRowIDs[0] + 1);

            //Create the UserSession object
            objUserSession = CreateUserSession(strConnString, strUserName, p_iClientId);
            //PSARIN2: FIX Session violation issues - Commented code & readded again
            //dictUserSessionBinaryItems.Add(strBinaryName, CreateSessionBinaryItem(strBinaryName, bytBinaryItemValue, objUserSession.SessionRowID, arrBinaryRowIDs[0].ToString(), objUserSession.LastChanged));
            dictUserSessionBinaryItems.Add(strBinaryName, CreateSessionBinaryItem(strBinaryName, bytBinaryItemValue, objUserSession.SessionRowID,"-1", objUserSession.LastChanged));

            //Create the binary items
            objUserSession.BinaryItems = dictUserSessionBinaryItems;

            SaveUserSession(strConnString, objUserSession, p_iClientId);

            return objUserSession.SessionID;
        } // method: CreateCustomSessionBinary

        /// <summary>
        /// Creates a new user session for initializing all other session operations
        /// </summary>
        /// <param name="strConnString">string containing the session database connection string</param>
        /// <param name="strUserName">string containing the user's login name</param>
        /// <returns>populated UserSession object</returns>
        private static UserSession CreateUserSession(string strConnString, string strUserName, int p_iClientId)
        {
            UserSession objUserSession = new UserSession();
            StringBuilder strSessionIDSQL = new StringBuilder();

            objUserSession.SessionID = Utilities.GenerateGuid();

            //Get next SESSION_ROW_ID from SEQUENCE SEQ_SESSION_ROW_ID
            if (DbFactory.GetDatabaseType(strConnString) == eDatabaseType.DBMS_IS_ORACLE)
            {
                objUserSession.SessionRowID = DbFactory.ExecuteScalar(strConnString, "SELECT SEQ_SESSION_ROW_ID.NEXTVAL FROM DUAL").ToString();
            }
            else
            {
                objUserSession.SessionRowID = DbFactory.ExecuteScalar(strConnString, "SELECT MAX(NEXT_UNIQUE_ID) FROM SESSION_IDS si WHERE SYSTEM_TABLE_NAME='SESSIONS'").ToString();
            }
            objUserSession.LoginName = strUserName;
            objUserSession.LastChanged = Conversion.ToDbDateTime(DateTime.Now);

            //Update the Session ID in the Lookup table
            strSessionIDSQL.Append(string.Format("UPDATE SESSION_IDS SET NEXT_UNIQUE_ID = {0} WHERE SYSTEM_TABLE_NAME = 'SESSIONS'", Conversion.ConvertObjToInt(objUserSession.SessionRowID, p_iClientId) + 1));

            DbFactory.ExecuteNonQuery(strConnString, strSessionIDSQL.ToString(), new Dictionary<string, string>());

            return objUserSession;
        } // method: CreateUserSession

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strBinaryName"></param>
        /// <param name="arrBinaryValue"></param>
        /// <param name="strSessionRowID"></param>
        /// <param name="strBinaryRowID"></param>
        /// <param name="strLastChanged"></param>
        /// <returns></returns>
        private static UserSessionBinaryItem CreateSessionBinaryItem(string strBinaryName, byte[] arrBinaryValue, string strSessionRowID, string strBinaryRowID,
            string strLastChanged)
        {
            UserSessionBinaryItem objUserBinaryItem = new UserSessionBinaryItem() { SessionRowID = strSessionRowID, BinaryName = strBinaryName, BinaryValue = arrBinaryValue, SessionBinaryID = strBinaryRowID, LastChanged = strLastChanged };

            return objUserBinaryItem;
        } // property CreateSessionBinaryItem

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objUserSession"></param>
        /// <param name="arrBinaryItems"></param>
        /// <param name="arrBinaryRowIDs"></param>
        /// <returns></returns>
        private static Dictionary<string, UserSessionBinaryItem> CreateSessionBinaryItemCollection(UserSession objUserSession, List<byte[]> arrBinaryItems, List<int> arrBinaryRowIDs)
        {
            //PSARIN2: FIX Session violation issues - Commented code & readded again
           // UserSessionBinaryItem objUserBinaryItem = CreateSessionBinaryItem(USER_OBJECT, arrBinaryItems[0], objUserSession.SessionRowID, arrBinaryRowIDs[0].ToString(), objUserSession.LastChanged);
           // UserSessionBinaryItem objUserBinaryItem2 = CreateSessionBinaryItem(USER_LIMITS_OBJECT, arrBinaryItems[1], objUserSession.SessionRowID, arrBinaryRowIDs[1].ToString(), objUserSession.LastChanged);
            UserSessionBinaryItem objUserBinaryItem = CreateSessionBinaryItem(USER_OBJECT, arrBinaryItems[0], objUserSession.SessionRowID, "-1", objUserSession.LastChanged);
            UserSessionBinaryItem objUserBinaryItem2 = CreateSessionBinaryItem(USER_LIMITS_OBJECT, arrBinaryItems[1], objUserSession.SessionRowID, "-2", objUserSession.LastChanged);
            Dictionary<string, UserSessionBinaryItem> dictUserBinarySessions = new Dictionary<string, UserSessionBinaryItem>();
            dictUserBinarySessions.Add(USER_OBJECT, objUserBinaryItem);
            dictUserBinarySessions.Add(USER_LIMITS_OBJECT, objUserBinaryItem2);

            return dictUserBinarySessions;
        }

        /// <summary>
        /// Saves a User Session to the database
        /// </summary>
        /// <param name="strConnString">string containing the session database connection string</param>
        /// <param name="objUserSession">object instance to the UserSession object</param>
        private static void SaveUserSession(string strConnString, UserSession objUserSession, int p_iClientId)
        {
            StringBuilder strSessionSQL = new StringBuilder();
            StringBuilder strSessionBinarySQL = new StringBuilder();
            string strSessionBinaryKey = string.Empty;
            Dictionary<string, object> dictParams = new Dictionary<string, object>();

            //using (TransactionScope transScope = new TransactionScope(TransactionScopeOption.Required))
            //{
            strSessionSQL.Append("INSERT INTO SESSIONS (SID, SESSION_ROW_ID, LASTCHANGE)");
            strSessionSQL.Append(string.Format("VALUES ('{0}', {1}, '{2}')", objUserSession.SessionID, objUserSession.SessionRowID, objUserSession.LastChanged));

            DbFactory.ExecuteNonQuery(strConnString, strSessionSQL.ToString(), new Dictionary<string, string>());
            //PSARIN2: FIX Session violation issues
            if (DbFactory.GetDatabaseType(strConnString) == eDatabaseType.DBMS_IS_ORACLE)
            {
                foreach (KeyValuePair<string, UserSessionBinaryItem> kvp in objUserSession.BinaryItems)
                {
                    int intBinRowID = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(strConnString, "SELECT SEQ_SESSION_BINARY_ROW_ID.NEXTVAL FROM DUAL"), p_iClientId);
                    strSessionBinarySQL.Append("INSERT INTO SESSIONS_X_BINARY (SESSION_X_BIN_ROW_ID, SESSION_ROW_ID, BINARY_NAME, BINARY_VALUE)");
                    strSessionBinarySQL.Append(string.Format("VALUES ({0}, {1}, {2}, {3})", "~SESSION_BINARY_ID~", "~SESSION_ROW_ID~", "~BINARY_NAME~", "~BINARY_VALUE~"));

                    //Set the Session Binary Key that is in the Dictionary collection
                    strSessionBinaryKey = kvp.Key;
                    //PSARIN2: FIX Session violation issues 
                    dictParams.Add("SESSION_BINARY_ID", intBinRowID);
                    dictParams.Add("SESSION_ROW_ID", objUserSession.SessionRowID);
                    dictParams.Add("BINARY_NAME", objUserSession.BinaryItems[strSessionBinaryKey].BinaryName);
                    dictParams.Add("BINARY_VALUE", objUserSession.BinaryItems[strSessionBinaryKey].BinaryValue);

                    DbFactory.ExecuteNonQuery(strConnString, strSessionBinarySQL.ToString(), dictParams);

                    strSessionBinarySQL.Length = 0;
                    dictParams.Clear();
                }//foreach
            }
            else
            {
                foreach (KeyValuePair<string, UserSessionBinaryItem> kvp in objUserSession.BinaryItems)
                {
                    strSessionBinarySQL.Append("INSERT INTO SESSIONS_X_BINARY (SESSION_ROW_ID, BINARY_NAME, BINARY_VALUE)");
                    strSessionBinarySQL.Append(string.Format("VALUES ({0}, {1}, {2})", "~SESSION_ROW_ID~", "~BINARY_NAME~", "~BINARY_VALUE~"));

                    //Set the Session Binary Key that is in the Dictionary collection
                    strSessionBinaryKey = kvp.Key;
                    //PSARIN2: FIX Session violation issues 
                    //dictParams.Add("SESSION_BINARY_ID", objUserSession.BinaryItems[strSessionBinaryKey].SessionBinaryID);
                    dictParams.Add("SESSION_ROW_ID", objUserSession.SessionRowID);
                    dictParams.Add("BINARY_NAME", objUserSession.BinaryItems[strSessionBinaryKey].BinaryName);
                    dictParams.Add("BINARY_VALUE", objUserSession.BinaryItems[strSessionBinaryKey].BinaryValue);

                    DbFactory.ExecuteNonQuery(strConnString, strSessionBinarySQL.ToString(), dictParams);

                    strSessionBinarySQL.Length = 0;
                    dictParams.Clear();
                }//foreach
            }
            //Complete the transaction as a whole
            //    transScope.Complete();
            //}//using
        } // method: SaveUserSession


        /// <summary>
        /// Gets the existing User Session and populates the UserSession object
        /// </summary>
        /// <param name="strConnString">string containing the session database connection string</param>
        /// <param name="strSessionID">string containing the current user session ID</param>
        /// <param name="strUserName">string containing the user's login name</param>
        /// <returns>UserSession object containing all of the populated properties</returns>
        public static UserSession GetSession(string strConnString, string strSessionID, string strUserName)
        {
            UserSession objUserSession = GetSessionNoTimestampUpdate(strConnString, strSessionID, strUserName);

            //Update the session timestamp if it's not from ViewLoggedInUsers
            if (objUserSession != null)
            {
                string sLastChanged = Conversion.ToDbDateTime(DateTime.Now);
                DbFactory.ExecuteNonQuery(strConnString, String.Format("UPDATE SESSIONS SET LASTCHANGE = '{0}' WHERE SESSION_ROW_ID={1}", sLastChanged, objUserSession.SessionRowID));
            }

            return objUserSession;
        } // method: GetSession


        /// <summary>
        /// Gets the existing User Session and populates the UserSession object
        /// </summary>
        /// <param name="strConnString">string containing the session database connection string</param>
        /// <param name="strSessionID">string containing the current user session ID</param>
        /// <param name="strUserName">string containing the user's login name</param>
        /// <returns>UserSession object containing all of the populated properties</returns>
        public static UserSession GetSessionNoTimestampUpdate(string strConnString, string strSessionID, string strUserName)
        {
            StringBuilder strSessionSQL = new StringBuilder();

            strSessionSQL.Append("SELECT * FROM SESSIONS s ");
            strSessionSQL.Append("INNER JOIN SESSIONS_X_BINARY sxb ON ");
            strSessionSQL.Append("s.SESSION_ROW_ID = sxb.SESSION_ROW_ID ");
            strSessionSQL.Append(string.Format("WHERE SID = {0}", "~SESSION_ID~"));

            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            dictParams.Add("SESSION_ID", strSessionID);

            UserSession objUserSession = null;
            Dictionary<string, UserSessionBinaryItem> dictSessionBinaryItems = new Dictionary<string, UserSessionBinaryItem>();

            using (DbReader dbReader = DbFactory.ExecuteReader(strConnString, strSessionSQL.ToString(), dictParams))
            {
                while (dbReader.Read())
                {
                    if (objUserSession == null)
                    {
                        objUserSession = new UserSession();
                    }

                    UserSessionBinaryItem objUserBinaryItem = new UserSessionBinaryItem();

                    objUserSession.SessionID = dbReader["SID"].ToString();
                    objUserSession.LastChanged = dbReader["LASTCHANGE"].ToString();
                    objUserSession.SessionRowID = dbReader["SESSION_ROW_ID"].ToString();

                    objUserBinaryItem.BinaryName = dbReader["BINARY_NAME"].ToString();
                    objUserBinaryItem.BinaryValue = dbReader.GetAllBytes("BINARY_VALUE");
                    objUserBinaryItem.SessionBinaryID = dbReader["SESSION_X_BIN_ROW_ID"].ToString();
                    objUserBinaryItem.SessionRowID = dbReader["SESSION_ROW_ID"].ToString();

                    AddBinaryItemsToDictionary(ref dictSessionBinaryItems, objUserBinaryItem.BinaryName, objUserBinaryItem);

                }//while
            }//using

            //Add the binary items to the Dictionary collection
            if (objUserSession != null)
            {
                objUserSession.BinaryItems = dictSessionBinaryItems;
            }

            return objUserSession;
        } // method: GetSession

        /// <summary>
        /// Determines if the current User Session Exists
        /// </summary>
        /// <param name="strConnString">string containing the session database connection string</param>
        /// <param name="strSessionID">string containing the current session ID</param>
        /// <returns>boolean indicating whether or not the session exists in the database</returns>
        public static bool SessionExists(string strConnString, string strSessionID, int p_iClientId)
        {
            bool blnSessionExists = false;

            int intSessionRowID = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(strConnString, string.Format("SELECT SESSION_ROW_ID FROM SESSIONS s WHERE SID='{0}'", strSessionID)), p_iClientId);

            if (intSessionRowID != 0)
            {
                blnSessionExists = true;
            } // if

            return blnSessionExists;
        } // method: SessionExists


        /// <summary>
        /// Updates the Session database with the current value of a
        /// Serialized Binary object
        /// </summary>
        /// <param name="strConnString">string containing the session database connection string</param>
        /// <param name="strSessionID">string containing the current session ID</param>
        /// <param name="objBinaryItem">an object instance to the Session Binary Item to be updated</param>
        public static void UpdateSession(string strConnString, string strSessionID, UserSessionBinaryItem objBinaryItem)
        {
            StringBuilder strUpdateSessionBinarySQL = new StringBuilder();
            Dictionary<string, object> dictParams = new Dictionary<string, object>();

            UserSession objUserSession = GetSession(strConnString, strSessionID, string.Empty);

            //using (TransactionScope transScope = new TransactionScope(TransactionScopeOption.Required))
            //{

            strUpdateSessionBinarySQL.Append(string.Format("UPDATE SESSIONS_X_BINARY SET BINARY_VALUE = {0} WHERE SESSION_ROW_ID = {1} AND BINARY_NAME  = {2}", "~BINARY_VALUE~", "~SESSION_ROW_ID~", "~BINARY_NAME~"));

            dictParams.Add("BINARY_VALUE", objBinaryItem.BinaryValue);
            dictParams.Add("SESSION_ROW_ID", objUserSession.SessionRowID);
            dictParams.Add("BINARY_NAME", objBinaryItem.BinaryName);

            //Updates the Last Changed Date in the Session database
            UpdateSessionTimestamp(strConnString, strSessionID);

            DbFactory.ExecuteNonQuery(strConnString, strUpdateSessionBinarySQL.ToString(), dictParams);

            //    transScope.Complete();
            //}//using

        } // method: UpdateSession

        /// <summary>
        /// Updates the Last Changed date on the user's session
        /// </summary>
        /// <param name="strConnString">string containing the session database connection string</param>
        /// <param name="strSessionID">string containing the current session ID</param>
        /// <returns>string indicating the last changed date</returns>
        public static string UpdateSessionTimestamp(string strConnString, string strSessionID)
        {
            StringBuilder strUpdateSessionSQL = new StringBuilder();
            string strLastChangedDate = Conversion.ToDbDateTime(DateTime.Now);

            strUpdateSessionSQL.Append(string.Format("UPDATE SESSIONS SET LASTCHANGE = '{0}' WHERE SID='{1}'", strLastChangedDate, strSessionID));

            DbFactory.ExecuteNonQuery(strConnString, strUpdateSessionSQL.ToString(), new Dictionary<string, string>());

            return strLastChangedDate;
        }//method: UpdateSessionTimestamp


        /// <summary>
        /// Removes the user's session information from the database
        /// </summary>
        /// <param name="strConnString">string containing the session database connection string</param>
        /// <param name="strSessionID">string containing the current session ID</param>
        /// <param name="strUserName">string containing the user's login name</param>
        public static void RemoveSession(string strConnString, string strSessionID, string strUserName)
        {
            //Mits id:32160 -Performance issue - Start
            //int rowId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(strConnString, String.Format("SELECT SESSION_ROW_ID FROM SESSIONS WHERE SID='{0}'", strSessionID)));

            //using (TransactionScope transScope = new TransactionScope(TransactionScopeOption.Required))
            //{
            //smishra25:Removed new Dictionary as it is not being used and another overload exists
            //DbFactory.ExecuteNonQuery(strConnString, String.Format("DELETE FROM SESSIONS WHERE SID='{0}'", strSessionID), new Dictionary<string, string>());
            //DbFactory.ExecuteNonQuery(strConnString, String.Format("DELETE FROM SESSIONS_X_BINARY WHERE SESSION_ROW_ID='{0}'", rowId), new Dictionary<string, string>());
            
            //DbFactory.ExecuteNonQuery(strConnString, String.Format("DELETE FROM SESSIONS WHERE SID='{0}'", strSessionID));
            
            //DbFactory.ExecuteNonQuery(strConnString, String.Format("DELETE FROM SESSIONS_X_BINARY WHERE SESSION_ROW_ID='{0}'", rowId));

            DbFactory.ExecuteNonQuery (strConnString,String.Format("DELETE FROM SESSIONS_X_BINARY WHERE SESSION_ROW_ID =(SELECT SESSION_ROW_ID FROM SESSIONS WHERE SID='{0}')", strSessionID));

            DbFactory.ExecuteNonQuery(strConnString, String.Format("DELETE FROM SESSIONS WHERE SID='{0}'", strSessionID));

            // akaushik5 Added for MITS 37481 Starts
            DbFactory.ExecuteNonQuery(strConnString, String.Format("DELETE FROM SM_USER_DATA WHERE SID='{0}'", strSessionID));
            // akaushik5 Added for MITS 37481 Ends

            //Mits id:32160 -Performance issue - End
            //    transScope.Complete();
            //}//using

   


        } // method: RemoveSession

        /// <summary>
        /// Returns all the Sessions with their related information.Used by the 
        /// administrator to get the information for all the users that are logged in.
        /// </summary>
        /// <returns>a generic List collection that contains the information about all the sessions.
        /// </returns>
        public static List<UserSession> GetAllSessions(string strConnString)
        {

            List<UserSession> arrUserSessions = new List<UserSession>();
            // added by csingh7  MITS 18646 : Start
            //using (DbReader objReader = DbFactory.ExecuteReader(strConnString, "SELECT SID FROM SESSIONS")) 
            string sSQL = string.Empty;
            sSQL = "SELECT DISTINCT(S.SID) FROM SESSIONS S , SESSIONS_X_BINARY SXB WHERE S.SESSION_ROW_ID = SXB.SESSION_ROW_ID";
            using (DbReader objReader = DbFactory.ExecuteReader(strConnString, sSQL))   // added by csingh7  MITS 18646 : End
            {
                while (objReader.Read())
                {
                    UserSession objUserSession = GetSessionNoTimestampUpdate(strConnString, objReader["SID"].ToString(), string.Empty);

                    if (objUserSession != null)
                    {
                        arrUserSessions.Add(objUserSession);
                    }
                }//while
            }//using

            return arrUserSessions;
        }

        /// <summary>
        /// Adds selected Binary Items to the Dictionary collection
        /// </summary>
        /// <param name="dictUserBinaryItems"></param>
        /// <param name="objUserSessionBin"></param>
        private static void AddBinaryItemsToDictionary(ref Dictionary<string, UserSessionBinaryItem> dictUserBinaryItems, string strBinaryNameKey, UserSessionBinaryItem objUserSessionBin)
        {
            if (!dictUserBinaryItems.ContainsKey(strBinaryNameKey))
            {
                dictUserBinaryItems.Add(strBinaryNameKey, objUserSessionBin);
            } // if
        }//method: AddBinaryItemsToDictionary()

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strConnString">string containing the session database connection string</param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static byte[] GetBinaryItem(string strConnString, string name)
        {
            byte[] bytBinaryItem = null;

            bytBinaryItem = DbFactory.ExecuteScalar(strConnString, string.Format("SELECT BINARY_VALUE FROM SESSIONS_X_BINARY sxb WHERE sxb.BINARY_NAME = '{0}'", name)) as byte[];

            return bytBinaryItem;
        } // method: GetBinaryItem

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strConnString">string containing the session database connection string</param>
        /// <param name="name"></param>
        /// <param name="bytes"></param>
        public static void SetBinaryItem(string strConnString, string name, byte[] bytes, int p_iClientId)
        {
            CreateCustomSessionBinary(strConnString, string.Empty, name, bytes, p_iClientId);
        } // method: SetBinaryItem

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strConnString">string containing the session database connection string</param>
        /// <param name="intBinaryRowID"></param>
        public static void SetBinaryRowID(string strConnString, int intBinaryRowID)
        {
            StringBuilder strSessionIDSQL = new StringBuilder();

            strSessionIDSQL.Append(string.Format("UPDATE SESSION_IDS SET NEXT_UNIQUE_ID = {0} WHERE SYSTEM_TABLE_NAME = 'SESSIONS_X_BINARY'", intBinaryRowID));

            DbFactory.ExecuteNonQuery(strConnString, strSessionIDSQL.ToString(), new Dictionary<string, string>());
        }//method: SetBinaryRowID() 
        #endregion


    }
}
