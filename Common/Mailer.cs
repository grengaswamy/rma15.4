using System;
using Riskmaster.ExceptionTypes;
using System.Net.Mail;
using System.Configuration;
using System.Collections.Generic;
using System.Net.Configuration;
using System.Data;
using System.Net;

namespace Riskmaster.Common
{
	/// <summary>
	/// Performs all e-mail operations using the System.Net.Mail library
	/// </summary>
	public class Mailer:IDisposable
	{
		#region Member Variables

		/// <summary>
		/// Comprises the Mail Message for the e-mail message
		/// </summary>
		private MailMessage m_objMailMessage;
		#endregion

		#region Properties
		/// <summary>
		/// To address
		/// </summary>
        public string To { get; set; }
		/// <summary>
		/// CC address
		/// </summary>
        public string Cc { get; set; }
        /// <summary>
        /// BCC address
        /// </summary>
        public string Bcc { get; set; }
		/// <summary>
		/// From address
		/// </summary>
        public string From { get; set; }
 
		/// <summary>
		/// Subject of the mail
		/// </summary>
        public string Subject { get; set; }

		/// <summary>
		/// Body of the mail
		/// </summary>
        public string Body { get; set; }

        /// <summary>
        /// ClientId 
        /// </summary>
        public int ClientId { get; set; }

        /// <summary>
        /// Set true to display Body as an HTML page.
        /// </summary>
        public bool IsBodyHtml
        {
            get;
            set;
        }

        /// <summary>
        /// Gets and sets the Smtp Server
        /// used for sending e-mail messages
        /// </summary>
        public string SmtpServer { get; set; }

        //rsushilaggar Added Alternate domain & replyto domain field in the settings screen MITS 28280 
        public string SenderAlternateDomain { get; set; }
        public string SenderReplytoDomain { get; set; }
		#endregion

		#region Constructor


         //mbahl3
        /// <summary>
        /// Constructor for scripting class
        /// </summary>
        public Mailer()
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                this.From = string.Empty;
                this.To = string.Empty;
                this.Cc = string.Empty;
                this.Bcc = string.Empty;
                this.Subject = string.Empty;
                this.Body = string.Empty;
                this.IsBodyHtml = false;
                this.ClientId = 0;
                m_objMailMessage = new MailMessage();
                //rsushilaggar Added Alternate domain & replyto domain field in the settings screen MITS 28280 
                //this.SmtpServer = RMConfigurationSettings.GetSMTPServerSettings();
                DataTable dtSettings = RMConfigurationSettings.GetSMTPServerSettings(this.ClientId);
                if (dtSettings.Rows.Count > 0)
                {
                    //this.SMTPServer = dtSettings.Rows[0]["SMTP_SERVER"].ToString();
                    this.SmtpServer = dtSettings.Rows[0]["SMTP_SERVER"].ToString();
                    this.SenderAlternateDomain = dtSettings.Rows[0]["ALTERNATE_DOMAIN"].ToString();
                    this.SenderReplytoDomain = dtSettings.Rows[0]["REPLYTO_DOMAIN"].ToString();

                }
            }
            else
                throw new Exception("Mailer" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
            // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        
        }

		/// <summary>
		/// Default constructor
        /// which initializes all default values
		/// </summary>
        public Mailer(int p_iClientId)
		{
            this.From = string.Empty;
			this.To = string.Empty; 
			this.Cc = string.Empty;
            this.Bcc = string.Empty;
            this.Subject = string.Empty; 
			this.Body = string.Empty;
            this.IsBodyHtml = false;
            this.ClientId = p_iClientId;
			m_objMailMessage = new MailMessage();
            //rsushilaggar Added Alternate domain & replyto domain field in the settings screen MITS 28280 
            //this.SmtpServer = RMConfigurationSettings.GetSMTPServerSettings();
            DataTable dtSettings = RMConfigurationSettings.GetSMTPServerSettings(p_iClientId);
            if (dtSettings.Rows.Count > 0)
            {
                //this.SMTPServer = dtSettings.Rows[0]["SMTP_SERVER"].ToString();
                this.SmtpServer = dtSettings.Rows[0]["SMTP_SERVER"].ToString();
                this.SenderAlternateDomain = dtSettings.Rows[0]["ALTERNATE_DOMAIN"].ToString();
                this.SenderReplytoDomain = dtSettings.Rows[0]["REPLYTO_DOMAIN"].ToString();

            }
            //end rsushilaggar
		}//constructor

		#endregion

		#region Send mail
		/// <summary>
		/// Send an e-mail
		/// </summary>
		public void SendMail ()
		{
            const string RECIPIENT_DELIMITER = ","; 
            //PJS MITS 10896 - separated email list
            List<string> sTolist = GetRecipients(this.To, RECIPIENT_DELIMITER);
            List<string> sCclist = GetRecipients(this.Cc, RECIPIENT_DELIMITER);
            List<string> sBcclist = GetRecipients(this.Bcc, RECIPIENT_DELIMITER);
            try
            {
                //Create mail message

                //PJS MITS 10896 - added each email add. to To 
                foreach (string strTo in sTolist)
                {
                    m_objMailMessage.To.Add(new MailAddress(strTo));
                } // foreach

                foreach (string strCC in sCclist)
                {
                    m_objMailMessage.CC.Add(new MailAddress(strCC));
                }//foreach

                foreach (string strBCC in sBcclist)
                {
                    m_objMailMessage.Bcc.Add(new MailAddress(strBCC));
                }//foreach

                m_objMailMessage.Subject = this.Subject;
                m_objMailMessage.Body = this.Body;
                m_objMailMessage.IsBodyHtml = this.IsBodyHtml;

                //rsushilaggar Added Alternate domain & replyto domain field in the settings screen MITS 28280 
                if (!string.IsNullOrEmpty(this.SenderAlternateDomain))
                {
                    string sFromAddress = this.From;
                    sFromAddress = sFromAddress.Substring(0, sFromAddress.IndexOf('@') + 1);
                    sFromAddress = sFromAddress + this.SenderAlternateDomain;
                    m_objMailMessage.From = new MailAddress(sFromAddress);
                }
                else
                {
                    m_objMailMessage.From = new MailAddress(this.From);
                }

                if (!string.IsNullOrEmpty(this.SenderReplytoDomain))
                {
                    string sFromAddress = this.From;
                    sFromAddress = sFromAddress.Substring(0, sFromAddress.IndexOf('@') + 1);
                    sFromAddress = sFromAddress + this.SenderReplytoDomain;
                    m_objMailMessage.ReplyTo = new MailAddress(sFromAddress);
                }
                //End rsushilaggar

                //Deb : for cloud email
                if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() == "true")
                {
                    this.SmtpServer = RMConfigurationManager.GetAppSetting("SMTPServer");
                    SmtpClient client = new SmtpClient(this.SmtpServer);
                    client.Credentials = new NetworkCredential(RMConfigurationManager.GetAppSetting("Username"), RMConfigurationManager.GetAppSetting("Password"));
                    client.Send(m_objMailMessage);
                    client = null;
                }
                else
                {

                    SmtpClient client = new SmtpClient(this.SmtpServer);
                    client.Send(m_objMailMessage);
                    client = null;
                }
            }
              
            catch (SmtpException p_objException)
            {
                //throw new RMAppException(Globalization.GetString("Mailer.SendMail.Error"), p_objException);
                //throw new RMAppException(Globalization.GetString("Mailer.SMTPServer.Error", this.ClientId), p_objException);//Changed by Amitosh for mits 24446 (03/29/2011)
                //caggarwal4 Merged for MITS 24446,MITS 36562
                throw new SmtpException(Globalization.GetString("Mailer.SMTPServer.Error", this.ClientId), p_objException);//Changed by Amitosh for mits 24446 (03/29/2011)
            } // catch
                
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Mailer.SendMail.Error", this.ClientId), p_objException);
            }
			finally
			{
                if(m_objMailMessage != null)
                {
                    m_objMailMessage.Dispose();
                } // if
			}
		}


        //mbahl3 jira RMA-8486
        /// <summary>
        /// Scripting assembly overload for SendMail
        /// </summary>     
        public static void SendMail(MailMessage mailMsg)
        {
            if (CommonFunctions.CheckCallingStack("Riskmaster.Scripting"))
            {
                 SendMail(mailMsg,0);
            }
            else
                throw new Exception("SendMail" + " - " + Globalization.GetString("CommonFunctions.ScriptingError", 0));
             // this function overload is available to Scripting assembly (or RMXCustomScripts) only
        }
        //mbahl3 jira RMA-8486


        /// <summary>
        /// Sends an e-mail with the specified MailMessage
        /// </summary>
        /// <param name="mailMsg"></param>
        /// <param name="iClientId"></param>
        public static void SendMail(MailMessage mailMsg, int p_iClientId)
        {
            try
            {
                if (RMConfigurationManager.GetAppSetting("CloudDeployed").ToLower() == "true")
                {
                    SmtpClient client = new SmtpClient(RMConfigurationManager.GetAppSetting("SMTPServer"));
                    client.Credentials = new NetworkCredential(RMConfigurationManager.GetAppSetting("Username"), RMConfigurationManager.GetAppSetting("Password"));
                    client.Send(mailMsg);
                    client = null;
                }
                else
                {
                    SmtpClient client = new SmtpClient(RMConfigurationSettings.GetSMTPServer(p_iClientId));
                    client.Send(mailMsg);
                    client = null;
                }
	        }//try
            catch (SmtpException p_objException)
            {
                throw new RMAppException(Globalization.GetString("Mailer.SendMail.Error", p_iClientId), p_objException);
            } // catch
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Mailer.SendMail.Error", p_iClientId), p_objException);
            }
        }//method: SendMail

        /// <summary>
        /// Get a list of all the specified recipients
        /// </summary>
        /// <param name="strRecipients">string specifying all available recipients for a mail message</param>
        /// <param name="strDelimiter">string specifying a separation delimiter</param>
        /// <returns>generic string list which contains all available recipients</returns>
        private List<string> GetRecipients(string strRecipients, string strDelimiter)
        {
            string[] arrRecipients = strRecipients.Split((strDelimiter).ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            List<string> arrParsedRecipients = new List<string>();

            foreach (string strRecipient in arrRecipients)
            {
                if (! string.IsNullOrEmpty(strRecipient))
                {
                    arrParsedRecipients.Add(strRecipient);
                } // if

            } // foreach

            return arrParsedRecipients;
        }
		#endregion

		#region Add Attachment
		
		/// Name			: AddAttachment
		/// Author			: Aditya Babbar
		/// Date Created	: 22-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment       *           Author
		///	2007.07.06		*	System.Net.Mail			*   Tom Regan
		///					*				    *	
		/// ************************************************************		
		/// <summary>
		/// Adds an attachment to the mail message.
		/// </summary>
		/// <param name="p_sAttachmentFile">
		///		Fully Qualified Attachment Filename
		/// </param>
		public void AddAttachment(string p_sAttachmentFile)
		{
			Attachment objMailAttachment = null;
			
			try
			{
				if(p_sAttachmentFile.Length>0)
				{
					objMailAttachment = new Attachment(p_sAttachmentFile);
                    m_objMailMessage.Attachments.Add(objMailAttachment);
				}
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Mailer.AddAttachment.Error", this.ClientId),p_objException);
			}
			finally
			{
				objMailAttachment = null;
			}
		}
		#endregion


		#region IDisposable Members

		/// Name			: Dispose
		/// Author			: Aditya Babbar
		/// Date Created	: 22-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************		
		/// <summary>
		/// Release all the resources.
		/// </summary>
		public void Dispose()
		{
			// This method has been implemented for future
			// needs where this object may need to release
			// the resources used by it. All clients of this object
			// are advised to call this method.
			m_objMailMessage = null;
		}

		#endregion
	}
}