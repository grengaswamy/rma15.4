﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riskmaster.Common
{
    public class GridPreference
    {
        public List<GridColumnDefHelper> colDef { get; set; }
        public string PageZise { get; set; }
        /// <summary>
        /// Sort Direction values : asc or desc. for multiple sort columns, provide comma separated values for each column
        /// </summary>
        public string[] SortDirection { get; set; }
        /// <summary>
        /// Sort Column : for multiple sort columns, provide comma separated values 
        /// </summary>
        public string[] SortColumn { get; set; }

        /// <summary>
        /// store additional user pref which may be custom for each page
        /// </summary>
        public Dictionary<string,object> AdditionalUserPref { get; set; }
    }
}
