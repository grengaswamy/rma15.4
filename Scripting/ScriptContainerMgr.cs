using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Riskmaster.Db;


//Singleton should always be able to answer "How do I get the current ScriptContainer?"
//Singleton should also be able to answer "Do I need to recompile or can I take the current Container?"

namespace Riskmaster.Scripting
{
    class ScriptContainerMgr
    {
        class AssemblyCacheItem 
        {
            internal byte[] PE;
            internal ScriptEngine.CompileKey Key;
            internal Assembly AssemblyInstance = null;
            internal Type ContainerType = null;
            internal AssemblyCacheItem(){}
        }
        
        private static ScriptContainerMgr m_objSingleton = null;
        static public ScriptContainerMgr Manager { get { return m_objSingleton;} }
        
        static ScriptContainerMgr()
        {
            m_objSingleton = new ScriptContainerMgr();
        }

        //Enforce Singleton
        private ScriptContainerMgr() 
        {
            AssemblyCache = new Dictionary<string, AssemblyCacheItem>();
        }

        private System.Collections.Generic.Dictionary<string, AssemblyCacheItem> AssemblyCache;

        //If the Assembly is accepted then you get a CompileKey in return.
        internal ScriptEngine.CompileKey CompiledAssemblyReady(byte[] RawAssembly, string sConnectionString)
        {
            //Create the Key
            AssemblyCacheItem objItem = new AssemblyCacheItem();
            objItem.Key = new ScriptEngine.CompileKey(sConnectionString);
            objItem.PE  = RawAssembly;
            
            //Remove any existing Assembly Bytes
            if (AssemblyCache.ContainsKey(objItem.Key.DataSourceKey))
                AssemblyCache.Remove(objItem.Key.DataSourceKey);

            //Stash the Assembly bytes and key for later use in determining whether to have an engine recompile
            AssemblyCache.Add(objItem.Key.DataSourceKey, objItem);

            return objItem.Key;
        }
        
        //Return null if there is no compiled Assembly to work with.
        internal object GetCurrentScriptContainer(IRMXScriptEngineHost host, ScriptEngine.CompileKey key)
        {

            AssemblyCacheItem objItem;

            //What to do if we don't have a good copy of the assembly for this datasource?
            if (!AssemblyCache.ContainsKey(key.DataSourceKey))
                return null;

            objItem = AssemblyCache[key.DataSourceKey];
            
            
            //If needed - load assembly into this AppDomain
            if (objItem.AssemblyInstance == null)
                objItem.AssemblyInstance = AppDomain.CurrentDomain.Load(objItem.PE);

            //Get Instance of Container Class
            if(objItem.ContainerType==null)
                objItem.ContainerType = objItem.AssemblyInstance.GetType("ScriptContainer", true);

            //Initialize the Container class instance with a reference back to us - used for global value lookups.
            Object objContainer = System.Activator.CreateInstance(objItem.ContainerType);
            MethodInfo InitMethod = objItem.ContainerType.GetMethod("SetScriptEngineHost");
            InitMethod.Invoke(objContainer, new object[] { host as IRMXScriptEngineHost });

            //Return Fully Primed Script Container
            return objContainer;
        }

        internal ScriptEngine.CompileKey GetCurrentCompileKey(string sConnectionString)
        {
            String sCanonKey = ScriptEngine.CompileKey.Canonicalize(sConnectionString);
            AssemblyCacheItem objItem;
            if (AssemblyCache.ContainsKey(sCanonKey))
            {
                objItem = AssemblyCache[sCanonKey];
                return objItem.Key;
            }
            else
                return null;
        }
        internal bool AssemblyBytesExistFor(string sConnectionString)
        {
            String sCanonKey = ScriptEngine.CompileKey.Canonicalize(sConnectionString);
            return AssemblyCache.ContainsKey(sCanonKey);
        }
        internal byte[] LoadBinary(string sConnectionString)
        {
            String sCanonKey = ScriptEngine.CompileKey.Canonicalize(sConnectionString);
            AssemblyCacheItem objItem;
            if (AssemblyCache.ContainsKey(sCanonKey))
            {
                objItem = AssemblyCache[sCanonKey];
                return objItem.PE;
            }
            byte[] pe;
            //There is a newer version of the Assembly available from this AppDomain.

            //Otherwise, go get it from the DB and add these bytes to the cache.
            DbReader reader = DbFactory.GetDbReader(sConnectionString, "SELECT VBS_SCRIPTS.BINARY FROM VBS_SCRIPTS WHERE VBS_SCRIPTS.ROW_ID=0");
            try
            {
                if (reader.Read())
                {
                    pe = reader.GetAllBytes("BINARY");
                    reader.Close();
                    reader.Dispose();

                    reader = DbFactory.GetDbReader(sConnectionString, "SELECT COUNT(*) FROM VBS_SCRIPTS WHERE VBS_SCRIPTS.ROW_ID<>0");
                    if (reader.Read())
                    {
                        if (reader.GetInt32(0) > 0)
                        {
                            objItem = new AssemblyCacheItem();
                            objItem.PE = pe;
                            objItem.Key = new ScriptEngine.CompileKey(sConnectionString);
                            return objItem.PE;
                        }
                    }
                }
                //No Pre-Compiled Bytes available.
                return null;

            }
            finally
            {
                reader.Close();
                reader.Dispose();
            }
        }

        //TODO... May help with debugging to be able to dump all "Dynamic Assemblies" and PDB's to disk...
        //public bool DumpBinaryToDisk()
        //{

        //}

    }
}
