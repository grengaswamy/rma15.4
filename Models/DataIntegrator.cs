﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
//Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
using System.IO;
using System.ServiceModel;
//using Riskmaster.Db;
using System.Data;

namespace Riskmaster.Models
{

    [DataContract]
    public class DataIntegratorModel : RMServiceType
    {   
        [DataMember]
        public Dictionary<string, string> Parms
        {
            get;

            set;
        }
        [DataMember]
        public int OptionSetID
        {
            get;

            set;
        }
        [DataMember]
        public string ModuleName
        {
            get;

            set;
        }
        [DataMember]
        public string OptionSetName
        {
            get;

            set;
        }
        [DataMember]
        public bool ImportFlage
        {
            get;

            set;
        }
        [DataMember]
        public string FilePath
        {
            get;

            set;
        }
        [DataMember]
        public string TaskManagerXml
        {
            get;

            set;
        }
        [DataMember]
        public string ClaimNumber
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsClaimTypeGC
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsClaimTypeVA
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsClaimTypeWC
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsClaimTypePC
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsRMClaimType
        {
            get;

            set;
        }
        [DataMember]
        public int LOB
        {
            get;

            set;
        }
        [DataMember]
        public int RMClaimTypeID
        {
            get;

            set;
        }
       

        //Developer – abharti5 |MITS 36676 | start
        [DataMember]
        public DataSet dsPolicySystemNames
        {
            get;

            set;
        }
        //Developer – abharti5 |MITS 36676 | end

        [DataMember]
        public DataSet dsISOLossType
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsISOCoverageType
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsISOPolicyType
        {
            get;

            set;
        }
        [DataMember]
        public string sISOPolicyCode
        {
            get;

            set;
        }
        [DataMember]
        public string sISOCoverageCode
        {
            get;

            set;
        }
        [DataMember]
        public string sISOLossCode
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsMedicalReserve
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsOtherReserve
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsJurisStates
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsTransTypes
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsReserveTypes
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsLOB
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsAccount
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsEntity
        {
            get;

            set;
        }

        [DataMember]
        public DataSet dsClaimantType
        {
            get;

            set;
        }
        [DataMember]
        public int iClmtTypeID
        {
            get;

            set;
        }
        [DataMember]
        public int iClaimantMapping
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsISOJuris
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsRREId
        {
            get;

            set;
        }
        [DataMember] //mrishi2 - JIRA 16461 
        public string sRREIdSV
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsAdminTrackingTables
        {
            get;

            set;
        }
        [DataMember]
        public string sAdminTrackTableName
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsAdminTrackTableFields
        {
            get;

            set;
        }
        [DataMember]
        public DataSet dsBankAccounts
        {
            get;

            set;
        }
        [DataMember]
        public bool bUpdateFlag
        {
            get;

            set;
        }
        [DataMember]
        public string sDAStagingDBImportFile
        {
            get;

            set;
        }

        [DataMember]
        public DataSet dsReserveMappings
        {
            get;

            set;
        }

        //Developer - Shubhanjali | Template - DA ISO | Safeway | Start
        [DataMember]
        public DataSet dsDocumentType
        {
            get;

            set;
        }

        [DataMember]
        public DataSet dsEnhancedNoteType
        {
            get;

            set;
        }

        [DataMember]
        public DataSet dsDiaryType
        {
            get;

            set;
        }

        [DataMember]
        public DataSet dsLossInjury
        {
            get;

            set;
        }

        //sagarwal54 MITS 37006 09/02/2014 start
        [DataMember]
        public bool bPolicySystemInterface
        {
            get;

            set;
        }
        //sagarwal54 MITS 37006 09/02/2014 end

        //Developer - Subhendu | Template - DA CLAIM EXPORT CSSTARS | Start
        [DataMember]
        public DataSet dsPolicySupp
        {
            get;

            set;
        }


        //Developer - mihtesham |  | Start
        //Developer - Subhendu | Template - DA ISO | MITS 30839 |Start
        [DataMember]
        public string sISORecordType
        {
            get;

            set;
        }
        [DataMember]
        public bool bIsCarrier
        {
            get;

            set;
        }
        //Developer - Subhendu | Template - DA ISO | MITS 30839 | End
        [DataMember]
        public DataSet dsPolicyLOB
        {
            get;

            set;
        }
        [DataMember]

        public DataSet dsLossCode
        {
            get;

            set;
        }
        [DataMember]

        public DataSet dsDisablityCode
        {
            get;

            set;
        }
        [DataMember]

        public DataSet dsDisablityCategory
        {
            get;

            set;
        }
        [DataMember]

        public DataSet dsPolicyClmtTypeMapping
        {
            get;

            set;
        }

        [DataMember]

        public DataSet dsWCPolicyType
        {
            get;

            set;
        }
      

        [DataMember]

        public DataSet dsPolicyClaimLOB
        {
            get;

            set;
        }
        //Developer – abharti5 |MITS 36676 | start
        [DataMember]

        public int iPolicySystemId
        {
            get;

            set;
        }
        //Developer – abharti5 |MITS 36676 | end

        [DataMember]

        public int iPolicyType
        {
            get;

            set;
        }

        [DataMember]

        public int iDisabilityCode
        {
            get;

            set;
        }

        [DataMember]

        public int iLossTypeCode
        {
            get;

            set;
        }

        [DataMember]

        public DataSet dsPolicyCoverageMapping
        {
            get;

            set;
        }
        //Developer - Subhendu | Template - DA ISO | MITS 30839 |Start
        [DataMember]
        public int iCovCode
        {
            get;

            set;
        }
        //Developer - Subhendu | Template - DA ISO | MITS 30839 | End
       
        //mihtesham End

        //ipuri Mits:30917-start
        
        //public string sISOType
        //{
        //    get;

        //    set;
        //}

        //[DataMember]
        //public string sISOImportFileName
        //{
        //    get;

        //    set;
        //}
        [DataMember]
        //public string[] sUserid
        public string sUserid
        {
            get;
            set;
        }
        [DataMember]
        public DataSet dsUserid
        {
            get;
            set;
        }
        [DataMember]
        public string sSubmissorUserid
        {
            get;
            set;
        }
        [DataMember]
        public DataSet dsSubmissoUserid
        {
            get;
            set;
        }

        //ipuri Mits:30917 end


        [DataMember]
        public string sClaimTypeGC
        {
            get;

            set;
        }
        [DataMember]
        public string sReserveTypeGC
        {
            get;

            set;
        }
        [DataMember]
        public string sEntityID
        {
            get;

            set;
        }
        //sagarwal54 MITS 35386 start
        [DataMember]
        public DataSet dsrmAPropCodes
        {
            get;

            set;
        }
      

        [DataMember]
        public string srmAPropCodes
        {
            get;
            set;
        }
      
        [DataMember]
        public string sISOPropCodes
        {
            get;
            set;
        }
        //sagarwal54 MITS 35386 end

        //sagarwal54 MITS 35704 start
        [DataMember]
        public DataSet dsrmASPRoleCodes
        {
            get;

            set;
        }


        [DataMember]
        public string srmASPRoleCodes
        {
            get;
            set;
        }

        [DataMember]
        public string sISOSPRoleCodes
        {
            get;
            set;
        }
        //mihtesham MITS 35711 End

        //sagarwal54 MITS 35704 start
        [DataMember]
        public DataSet dsAdditionalClaimantCodes
        {
            get;

            set;
        }
        //sagarwal54 MITS 35704 end

        //Developer - Subhendu | Template - DA CLAIM EXPORT CSSTARS | End
//Manika : Start : Vsoni5 : 07/13/2011 : MITS 24924
        [DataMember]
        public DataSet dsEntityCode
        {
            get;

            set;
        }

        [DataMember]
        public DataSet dsPeopleCode
        {
            get;

            set;
        }
//manika : End  -  MITS 24924
   
        //Vsoni5 : MITS 24981
        //This property will hold the last run time for the job and it will be 
        //populated from the DTTL_LAST_RUN column of DATA_INTEGRATOR table
        [DataMember]
        public string sLastRuntime
        {
            get;
            set;
        }
        //Vsoni5 : MITS 24981

        // npadhy Converting the WCF service to WCF Rest for Cloud
        [DataMember]
        public string EntityList
        {
            get;
            set;
        }
        //Start - MITS# 36997 GAP 06 - agupta298 - RMA 5497 
        [DataMember]
        public DataTable dtClaimTypes 
        { 
            get; set; 
        }

        [DataMember]
        public DataTable dtCoverageTypes
        {
            get;
            set;
        }

        [DataMember]
        public DataTable dtLossTypes
        {
            get;
            set;
        }

        [DataMember]
        public DataTable dtTPInterfaceType
        {
            get;
            set;
        }

        [DataMember]
        public DataTable dtJurisStates
        {
            get;

            set;
        }

        [DataMember]
        public DataTable dtPolicyLOB
        {
            get;

            set;
        }
        //End - MITS# 36997 GAP 06 - agupta298 - RMA 5497 

        //----sgupta320: for 2GB Enhancement 
        [DataMember]
        public int BatchID
        {
            get;

            set;
        }

        //----sgupta320: for 2GB Enhancement 
        [DataMember]
        public string AdminTrackTable
        {
            get;

            set;
        }
    }

    [DataContract]
    public class PPAccountList : RMServiceType
    {
        private List<string> objAccountlist = new List<string>();
        private List<string> objAccountID = new List<string>();
        [DataMember]
        public List<string> AccountNameNumber
        {
            get
            {
                return objAccountlist;
            }
            set
            {
                objAccountlist = value;
            }
        }
        [DataMember]
        public List<string> AccountID
        {
            get
            {
                return objAccountID;
            }
            set
            {
                objAccountID = value;
            }
        }
    }

    [DataContract]
    public class JobFile : RMServiceType
    {
        [DataMember]
        public int JobId
        {
            get;
            set;
        }

        [DataMember]
        public string FileName
        {
            get;
            set;
        }

        [DataMember]
        [XmlIgnore]
        public byte[] FileContents
        {
            get;
            set;
        }
    }
    //Vsoni5 : MITS 22699 : Import file transport form UI to Business Layer
    // New message contract written for transporting import file
    [DataContract]
    public class DAImportFile : RMStreamedServiceType, IDisposable
    {
        // npadhy - Stream can not be serialized using DataContract in REST. so commented this property and introduced the new property FileContents with byte array.

        //[DataMember]
        //[XmlIgnore]
        //public Stream fileStream;

        [DataMember]
        public string fileName
        {
            get;
            set;
        }

        [DataMember]
        public string filePath
        {
            get;
            set;
        }

        [DataMember]
        [XmlIgnore]
        public byte[] FileContents
        {
            get;
            set;
        }

        // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
        // We need ModuleName, OptionsetId and DocumentType while uploading the Import Files or attachments to DB
        [DataMember]
        public string ModuleName
        {
            get;
            set;
        }


        [DataMember]
        public int OptionsetId
        {
            get;
            set;
        }

        [DataMember]
        public string DocumentType
        {
            get;
            set;
        }

        public void Dispose()
        {
            //if (fileStream != null)
            //{
            //    fileStream.Dispose();
            //    fileStream = null;
            //}
            if (FileContents != null)
            {
                FileContents = null;
            }
        }
    }


}


