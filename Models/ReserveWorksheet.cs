﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Collections;

namespace Riskmaster.Models
{
    [DataContract]
    public class ReserveWorksheetRequest: RMServiceType 
    {
        [DataMember]
        public string InputXmlString
        {
            get;
            set;
        }

        [DataMember]
        public int RSWId
        {
            get;
            set;
        }

        [DataMember]
        public int ClaimId
        {
            get;
            set;
        }

        [DataMember]
        public int ClaimantEId
        {
            get;
            set;
        }

        [DataMember]
        public int UnitId
        {
            get;
            set;
        }

        [DataMember]
        public int ExistApproved
        {
            get;
            set;
        }

        [DataMember]
        public int ExistPending
        {
            get;
            set;
        }

        [DataMember]
        public int ExistPendingApproval
        {
            get;
            set;
        }

        [DataMember]
        public int ExistRejected
        {
            get;
            set;
        }

        [DataMember]
        public bool EditRejected
        {
            get;
            set;
        }

        [DataMember]
        public string FromApproved
        {
            get;
            set;
        }

        [DataMember]
        public bool IsSMSPermissionToUpdate
        {
            get;
            set;
        }

        [DataMember]
        public bool IsClaimAllowedToAccess
        {
            get;
            set;
        }

        [DataMember]
        public bool Status
        {
            get;
            set;
        }
        [DataMember]
        public string RSWType
        {
            get;
            set;
        }
        [DataMember]
        public string RSWReservesXML
        {
            get;
            set;
        }

        //rsushilaggar MITS 26332 Date 12/28/2011
        [DataMember]
        public string RequestName
        {
            get;
            set;
        }
    }

    [DataContract]
    public class ReserveWorksheetResponse : RMServiceType
    {
        [DataMember]
        public string OutputXmlString
        {
            get;
            set;
        }

        [DataMember]
        public int RSWId
        {
            get;
            set;
        }

        [DataMember]
        public int ClaimId
        {
            get;
            set;
        }

        [DataMember]
        public int ExistApproved
        {
            get;
            set;
        }

        [DataMember]
        public int ExistPending
        {
            get;
            set;
        }

        [DataMember]
        public int ExistPendingApproval
        {
            get;
            set;
        }

        [DataMember]
        public int ExistRejected
        {
            get;
            set;
        }

        [DataMember]
        public bool EditRejected
        {
            get;
            set;
        }

        [DataMember]
        public string FromApproved
        {
            get;
            set;
        }

        [DataMember]
        public bool IsSMSPermissionToUpdate
        {
            get;
            set;
        }

        [DataMember]
        public bool IsClaimAllowedToAccess
        {
            get;
            set;
        }

        [DataMember]
        public bool Status
        {
            get;
            set;
        }
        [DataMember]
        public string RSWType
        {
            get;
            set;
        }
        [DataMember]
        public string RSWReservesXML
        {
            get;
            set;
        }
    }
}
