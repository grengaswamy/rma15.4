﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace Riskmaster.Models
{
    [DataContract]
    public class ProgressNotesType : RMServiceType
    {
        public ProgressNotesType()
        {
            this.objFilter = new Filter();
            this.objProgressNoteList = new List<ProgressNote>();
            
        }
        //Out Parameters
        #region Out parameters
        [DataMember]
        public string Claimant
        {
            get;
            set;
        }
        [DataMember]
        public bool FreezeText
        {
            get;
            set;
        }
        //Changed by Gagan for mits 14626 : start
        [DataMember]
        public bool ShowDateStamp
        {
            get;
            set;
        }
        //Changed by Gagan for mits 14626 : end


        //Changed by Gagan for mits 15503 : start

        [DataMember]
        public bool bCreatePermission
        {
            get;
            set;
        }


        [DataMember]
        public bool bEditPermission
        {
            get;
            set;
        }

        [DataMember]
        public bool bDeletePermission
        {
            get;
            set;
        }

        [DataMember]
        public bool bPrintPermission
        {
            get;
            set;
        }


        [DataMember]
        public bool bViewAllNotesPermission
        {
            get;
            set;
        }

        //Changed by Gagan for mits 15503 : end

        //Start rsushilaggar MITS 21119 06/18/2010
        [DataMember]
        public bool bTemplatesPermission
        {
            get;
            set;
        }
        //End rsushilaggar MITS 21119 06/18/2010
        [DataMember]
        public string UserName
        {
            get;
            set;
        }
        //Start by Shivendu for MITS 18098
        [DataMember]
        public int PageNumber
        {
            get;
            set;
        }
        [DataMember]
        public int TotalNumberOfPages
        {
            get;
            set;
        }
        [DataMember]
        public string SortColumn
        {
            get;
            set;
        }
        [DataMember]
        public string Direction
        {
            get;
            set;
        }
        //End by Shivendu for MITS 18098
        [DataMember]
        public System.Collections.Generic.List<ProgressNote> objProgressNoteList
        {
            get;
            set;
        }
#endregion
        //Parijat:Post Editable Enhanced Notes 
        # region Post Editable Enhanced Notes
        [DataMember]
        public string EnhancedTimeLimit
        {
            get;
            set;
        }
        [DataMember]
        public bool UserEditingRights
        {
            get;
            set;
        }
        #endregion

        //rsolanki2 : mits 21294
        [DataMember]
        public bool PrintMultiplePages
        {
            get;
            set;
        }       

        [DataMember]
        public Filter objFilter
        {
            get;
            set;
        }
        [DataMember]
        public int EventID
        {
            get;
            set;
        }
        [DataMember]
        public int ClaimID
        {
            get;
            set;
        }
        [DataMember]
        public string LOB
        {
            get;
            set;
        }
        [DataMember]
        public string SortCol
        {
            get;
            set;
        }
        [DataMember]
        public string DataType
        {
            get;
            set;
        }
        [DataMember]
        public string Ascending
        {
            get;
            set;
        }
        [DataMember]
        public string Temp
        {
            get;
            set;
        }
        [DataMember]
        public string FormName
        {
            get;
            set;
        }
        [DataMember]
        public string EventNumber
        {
            get;
            set;
        }
        [DataMember]
        public string FunctionToCall
        {
            get;
            set;
        }
        [DataMember]
        public string ClaimProgressNoteId
        {
            get;
            set;
        }
        [DataMember]
        public bool ActivateFilter
        {
            get;
            set;
        }
        [DataMember]
        public string ApplySort
        {
            get;
            set;
        }
        [DataMember]
        public string ProgressNoteReportPdf
        {
            get;
            set;
        }
        [DataMember]
        public string PrintOrder1
        {
            get;
            set;
        }
        [DataMember]
        public string PrintOrder2
        {
            get;
            set;
        }
        [DataMember]
        public string PrintOrder3
        {
            get;
            set;
        }
        //Added Rakhi for R6-Print Note By NoteType
        [DataMember]
        public string NoteTypeCodeId
        {
            get;
            set;
        }
        //Added Rakhi for R6-To save Template ID of Templates
        [DataMember]
        public string TemplateId
        {
            get;
            set;
        }
        [DataMember]
        public string TemplateName
        {
            get;
            set;
        }
        //Added Rakhi for R6-To save Template ID of Templates
        //Added nashokkumarg for williams policy enhanced notes--start: MITS 21704
        [DataMember]
        public int PolicyID
        {
            get;
            set;
        }
        [DataMember]
        public string PolicyName
        {
            get;
            set;
        }
        [DataMember]
        public string PolicyNumber
        {
            get;
            set;
        }
        [DataMember]
        public string CodeId
        {
            get;
            set;
        }
        [DataMember]
        public bool NotesPolicyClaimView
        {
            get;
            set;
        }
        [DataMember]
        public bool bPolicyTrackingCreatePermission
        {
            get;
            set;
        }


        [DataMember]
        public bool bPolicyTrackingEditPermission
        {
            get;
            set;
        }

        [DataMember]
        public bool bPolicyTrackingDeletePermission
        {
            get;
            set;
        }

        [DataMember]
        public bool bPolicyTrackingPrintPermission
        {
            get;
            set;
        }


        [DataMember]
        public bool bPolicyTrackingViewAllNotesPermission
        {
            get;
            set;
        }

        [DataMember]
        public int iGenerateReportThresholdExceeded
        {
            get;
            set;
        }
		//Added by Amitosh for mits 23473(05/11/2011)
        [DataMember]
        public string SortPref
        {
            get;
            set;
        }

        //Added nashokkumarg for williams policy enhanced notes--end:MITS 21704
        //skhare7 27285
        [DataMember]
        public string PIName
        {
            get;
            set;
        }
		//akaur9 R8 Mobile Adjuster
        [DataMember]
        public bool bIsMobileAdjuster
        {
            get;
            set;
        }
        //mbahl3 for mobilty 
        [DataMember]
        public bool bIsMobilityAdjuster
        {
            get;
            set;
        }

        [DataMember]
        public string ClaimNumber
        {
            get;
            set;

        }
        //MITS 28450:Opening Claim Enhanced Notes from children of Case Management
        [DataMember]
        public int CaseMgtRowID
        {
            get;
            set;
        }
 // mkaran2 - MITS 27038 - start 
        [DataMember]
        public string ViewNotesOption
        {
            get;
            set;
        }
        [DataMember]
        public string ViewNotesTypeList
        {
            get;
            set;
        }
        // mkaran2 - MITS 27038 - end
        [DataMember]
        public string CaseMgrParent
        {
            get;
            set;
        }

        [DataMember]
        public string DemanOfferParent
        {
            get;
            set;
        }
        [DataMember]
        public int DemanOfferParentId
        {
            get;
            set;
        }
        // Added by akaushik5 for MITS 30789 Starts
        /// <summary>
        /// Gets or sets the order pref.
        /// </summary>
        /// <value>
        /// The order pref.
        /// </value>
        [DataMember]
        public string OrderPref { get; set; }
        // Added by akaushik5 for MITS 30789 Ends
        /*added by gbindra mits#34104 02112014 wwig gap15 start*/
        [DataMember]
        public int ClaimantId
        {
            get;
            set;
        }
        /*added by gbindra mits#34104 02112014 wwig gap15 end*/
    }
   
    [DataContract]
     public class Filter : RMServiceType
    {
        public Filter()
        {
            ClaimIDList = "";
            UserTypeList = "";
            NoteTypeList = "";
            NoteTypeList = "";
            EnteredByList = "";
            ActivityFromDate = "";
            ActivityToDate = "";
            NotesTextContains = "";
            SortBy = "";
            SubjectList = "";
        }
        [DataMember]
        public string ClaimIDList
        {
            get;
            set;
        }
        [DataMember]
        public string UserTypeList
        {
            get;
            set;
        }
        [DataMember]
        public string NoteTypeList
        {
            get;
            set;
        }
        [DataMember]
        public string EnteredByList
        {
            get;
            set;
        }

        [DataMember]
        public string SubjectList
        {
            get;
            set;
        }

        [DataMember]
        public string ActivityFromDate
        {
            get;
            set;
        }
        [DataMember]
        public string ActivityToDate
        {
            get;
            set;
        }
        [DataMember]
        public string NotesTextContains
        {
            get;
            set;
        }
        [DataMember]
        public string SortBy
        {
            get;
            set;
        }
        // Added by akaushik5 for MITS 30789 Starts
        /// <summary>
        /// Gets or sets the order by.
        /// </summary>
        /// <value>
        /// The order by.
        /// </value>
        [DataMember]
        public string OrderBy { get; set; }
        // Added by akaushik5 for MITS 30789 Ends
        /*added by GBINDRA MITS#34104 02122014 WWIG GAP15 START*/
        [DataMember]
        public int ClaimantIDList
        {
            get;
            set;
        }
        /*added by GBINDRA MITS#34104 02122014 WWIG GAP15 END*/
    }

    [Serializable]
    [DataContract]
     public class ProgressNote : RMServiceType
    {
  
      [DataMember]
        public int ClaimProgressNoteId
        {
            get;
            set;
        }
        [DataMember]
        public string ClaimNumber
        {
            get;
            set;
        }
        [DataMember]
        public string EventNumber
        {
            get;
            set;
        }
        [DataMember]
        public string AttachedTo
        {
            get;
            set;
        }
        [DataMember]
        public string EnteredBy
        {
            get;
            set;
        }

        [DataMember]
        public string Subject
        {
            get;
            set;
        }

        [DataMember]
        public string EnteredByName
        {
            get;
            set;
        }
        [DataMember]
        public string DateEntered
        {
            get;
            set;
        }
        [DataMember]
        public string DateEnteredForSort
        {
            get;
            set;
        } 

      [DataMember]
        public string DateCreated
        {
            get;
            set;
        }
        [DataMember]
        public string TimeCreated
        {
            get;
            set;
        }
        [DataMember]
        public string TimeCreatedForSort
        {
            get;
            set;
        }
        [DataMember]
        public string NoteTypeCode
        {
            get;
            set;
        } 
        
       [DataMember]
        public string NoteTypeCodeId
        {
            get;
            set;
        } 
         [DataMember]
        public string UserTypeCodeId
        {
            get;
            set;
        } 
         [DataMember]
        public string UserTypeCode
        {
            get;
            set;
        } 
        [DataMember]
        public string NoteMemo
        {
            get;
            set;
        }
        [DataMember]
        public string NoteMemoTrimmed
        {
            get;
            set;
        }
        //Ashish Ahuja - Mits 33124
        [DataMember]
        public string SubjectTrimmed
        {
            get;
            set;
        }
        [DataMember]
        public string NoteMemoCareTech
        {
            get;
            set;
        } 
          [DataMember]
        public string AdjusterName
        {
            get;
            set;
        }
        //For Save Notes
          [DataMember]
          public string NewRecord
          {
              get;
              set;
          }
         [DataMember]
          public string EventID
         {
             get;
             set;
         }
         [DataMember]
         public string ClaimID
         {
             get;
             set;
         }
        //Added Rakhi for R6-To save Template ID of Templates
         [DataMember]
         public int TemplateID
         {
             get;
             set;
         }
         [DataMember]
         public string TemplateName
         {
             get;
             set;
         }
        //Added Rakhi for R6-To save Template ID of Templates
         //Added nashokkumarg for williams policy enhanced notes--start:MITS 21704
         [DataMember]
         public string PolicyID
         {
             get;
             set;
         }
         [DataMember]
         public string PolicyName
         {
             get;
             set;
         }
         [DataMember]
         public string PolicyNumber
         {
             get;
             set;
         }
         [DataMember]
        public string CodeId
         {
             get;
             set;
         }
         //Added nashokkumarg for williams policy enhanced notes--end:MITS 21704
         [DataMember]
         public string IsNoteEditable
         {
             get;
             set;
         }
         //skhare7 27285
         [DataMember]
         public string PIName
         {
             get;
             set;
         }
         //akaur9 R8 Mobile Adjuster
         [DataMember]
         public bool bIsMobileAdjuster
         {
             get;
             set;
         }
        /*ADDED BY GBINDRA ON 02112014 MITS#34104 WWIG GAP15 START*/
         [DataMember]
         public string AttachedTable
         {
             get;
             set;
         }
         [DataMember]
         public int AttachedRecordId
         {
             get;
             set;
         }
         /*ADDED BY GBINDRA ON 02112014 MITS#34104 WWIG GAP15 END*/
         //mbahl3  Mobility Adjuster
         [DataMember]
         public bool bIsMobilityAdjuster
         {
             get;
             set;
         }
     }

    [DataContract]
    public class ClaimInfo : RMServiceType
    {
         [DataMember]
        public string ClaimID
         {
             get;
             set;
         }
         [DataMember]
        public string ClaimNumber
         {
             get;
             set;
         }
         [DataMember]
        public string ClaimDate
         {
             get;
             set;
         }
        [DataMember]
        public string ClaimStatusDesc
         {
             get;
             set;
         }
        [DataMember]
        public string ClaimTypeCode
         {
             get;
             set;
         }
        [DataMember]
        public string LOB
         {
             get;
             set;
         }
        [DataMember]
        public string ClaimantName
         {
             get;
             set;
         }
        
    }

    [DataContract]
    public class SelectClaimObject : RMServiceType
    {
        public SelectClaimObject()
        {
            this.objClaimList = new List<ClaimInfo>();
        }
        [DataMember]
       public string EventID
        {
            get;
            set;
        }
        [DataMember]
       public string NewRecord
        {
            get;
            set;
        }
        [DataMember]
        public List<ClaimInfo> objClaimList
        {
            get;
            set;
        }
       //mbahl3 mits 30513
        [DataMember]
        public bool NotesLevelDropdown
        {
            get;
            set;
        }
        //Start rsushilaggar MITS 21119 06/18/2010
        [DataMember]
        public bool bViewTemplates
        {
            get;
            set;
        }
        //End rsushilaggar MITS 21119 06/18/2010
        //pmittal5 Mits 21514 
        [DataMember]
        public string ClaimID
        {
            get;
            set;
        }
        [DataMember]
        public string LOB
        {
            get;
            set;
        }
        [DataMember]
        public bool SelectEvent
        {
            get;
            set;
        }
        //End - pmittal5
        //willimas:neha goel:start
        [DataMember]
        public string PolicyID
        {
            get;
            set;
        }
        //willimas:neha goel:end
        
    }

    [DataContract]
    public class GetNoteDetailsObject : RMServiceType
    {
        [DataMember]
        public string ClaimProgressNoteId
        {
            get;
            set;
        }
        [DataMember]
        public string HTML
        {
            get;
            set;
        }
        [DataMember]
        public string Text
        {
            get;
            set;
        }
        [DataMember]
        public string NoteTypeCode
        {
            get;
            set;
        }
        [DataMember]
        public string NoteTypeDesc
        {
            get;
            set;
        }
        
        [DataMember]
        public string TemplateId
        {
            get;
            set;
        }
        [DataMember]
        public string TemplateName
        {
            get;
            set;
        }
        [DataMember]
        public string ActivityDate
        {
            get;
            set;
        }
        [DataMember]
        public bool DataChangedByInitScript
        {
            get;
            set;
        }
        //Ashish Ahuja Mits 33124
        [DataMember]
        public string Subject
        {
            get;
            set;
        }
    }
    //Added Rakhi R6-Enhanced Notes Templates
    [Serializable]
    [DataContract]
    public class ProgressNoteTemplates : RMServiceType
    {
        public ProgressNoteTemplates()
        {
            this.objProgressNoteTemplateList = new List<ProgressNoteTemplates>();
        }
        [DataMember]
        public string TemplateId
        {
            get;
            set;
        }
        [DataMember]
        public string TemplateName
        {
            get;
            set;
        }
        [DataMember]
        public string TemplateMemo
        {
            get;
            set;
        }
        [DataMember]
        public string AddedBy
        {
            get;
            set;
        }
        [DataMember]
        public string UpdatedBy
        {
            get;
            set;
        }
        [DataMember]
        public string DateAdded
        {
            get;
            set;
        }
        [DataMember]
        public string DateUpdated
        {
            get;
            set;
        }
        [DataMember]
        public string NewRecord //For Save Templates
        {
            get;
            set;
        }
        [DataMember]
        public System.Collections.Generic.List<ProgressNoteTemplates> objProgressNoteTemplateList //For Template List
        {
            get;
            set;
        }
        //Start rsushilaggar MITS 21119 06/18/2010
        [DataMember]
        public bool bCreatePermission
        {
            get;
            set;
        }


        [DataMember]
        public bool bEditPermission
        {
            get;
            set;
        }

        [DataMember]
        public bool bDeletePermission
        {
            get;
            set;
        }

        [DataMember]
        public bool bViewPermission
        {
            get;
            set;
        }
        //pmittal5 Mits 21514
        [DataMember]
        public int EventID
        {
            get;
            set;
        }
        [DataMember]
        public int ClaimID
        {
            get;
            set;
        }
        [DataMember]
        public string LOB
        {
            get;
            set;
        }
        //End - pmittal5
    }
    //Added Rakhi R6-Enhanced Notes Templates

    //Added Rakhi R6-Enhanced Notes Settings
    [Serializable]
    [DataContract]
    public class ProgressNoteSettings : RMServiceType
    {

        [DataMember]
        public string Header1
        {
            get;
            set;
        }
        [DataMember]
        public string Header2
        {
            get;
            set;
        }
        [DataMember]
        public string Header3
        {
            get;
            set;
        }
        [DataMember]
        public string Header4
        {
            get;
            set;
        }
        [DataMember]
        public string Header5
        {
            get;
            set;
        }

        //Added by Amitosh for mits 23691
        [DataMember]
        public string Header6
        {
            get;
            set;
        }

        [DataMember]
        public string Header7
        {
            get;
            set;
        }

        [DataMember]
        public string Header8
        {
            get;
            set;
        }
        [DataMember]
        public string Header9
        {
            get;
            set;
        }
        //end Amitosh
        //Added by Amitosh for mits 23473 (05/11/2011)
        [DataMember]
        public string sortby
        {
            get;
            set;
        }
        // Added by akaushik5 for MITS 30789 Starts
        /// <summary>
        /// Gets or sets the order by.
        /// </summary>
        /// <value>
        /// The order by.
        /// </value>
        [DataMember]
        public string OrderBy { get; set; }
        // Added by akaushik5 for MITS 30789 Ends
        //Deb MITS 30185
        [DataMember]
        public string Header1Text
        {
            get;
            set;
        }
        [DataMember]
        public string Header2Text
        {
            get;
            set;
        }
        [DataMember]
        public string Header3Text
        {
            get;
            set;
        }
        [DataMember]
        public string Header4Text
        {
            get;
            set;
        }
        [DataMember]
        public string Header5Text
        {
            get;
            set;
        }
        [DataMember]
        public string Header6Text
        {
            get;
            set;
        }
        [DataMember]
        public string Header7Text
        {
            get;
            set;
        }

        [DataMember]
        public string Header8Text
        {
            get;
            set;
        }

        [DataMember]
        public string Header9Text
        {
            get;
            set;
        }

        [DataMember]
        public string HeaderKeyShowList
        {
            get;
            set;
        }
        //Deb MITS 30185
    }
    //Added Rakhi R6-Enhanced Notes Templates
// mbahl3 Added for MITS 30513 Starts
    [DataContract]
    public class ProgressNotesTypeChange : RMServiceType
    {
              
        [DataMember]
        public string RecordType
        {
            get;
            set;
        }

        [DataMember]
        public string RecordId
        {
            get;
            set;
        }
        [DataMember]
        public string CaptionText
        {
            get;
            set;
        }
        [DataMember]
        public string PIEId
        {
            get;
            set;
        }
    }
// mbahl3 Added for MITS 30513 Starts
    /*GBINDRA MITS#34104 02042014 WWIG GAP15 START*/
    [DataContract]
    public class ClaimantInfo : RMServiceType
    {
        [DataMember]
        public string ClaimID
        {
            get;
            set;
        }
        [DataMember]
        public int ClaimantRowId
        {
            get;
            set;
        }
        [DataMember]
        public string ClaimNumber
        {
            get;
            set;
        }
        [DataMember]
        public string ClaimantFirstName
        {
            get;
            set;
        }
        [DataMember]
        public string ClaimantLastName
        {
            get;
            set;
        }
        [DataMember]
        public string LOB
        {
            get;
            set;
        }
    }
    [DataContract]
    public class SelectClaimantObject : RMServiceType
    {
        public SelectClaimantObject()
        {
            this.objClaimantList = new List<ClaimantInfo>();
        }
        [DataMember]
        public string EventID
        {
            get;
            set;
        }
        [DataMember]
        public string NewRecord
        {
            get;
            set;
        }
        [DataMember]
        public List<ClaimantInfo> objClaimantList
        {
            get;
            set;
        }
        [DataMember]
        public bool NotesLevelDropdown
        {
            get;
            set;
        }
        [DataMember]
        public bool bViewTemplates
        {
            get;
            set;
        }
        [DataMember]
        public string ClaimID
        {
            get;
            set;
        }
        [DataMember]
        public bool SelectEvent
        {
            get;
            set;
        }
        [DataMember]
        public string PolicyID
        {
            get;
            set;
        }
        [DataMember]
        public string LOB
        {
            get;
            set;
        }
    }
    /*GBINDRA MITS#34104 02042014 WWIG GAP15 END*/
}
