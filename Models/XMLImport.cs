﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Collections;
using System.Xml.Linq;
namespace Riskmaster.Models
{
    [DataContract]
    public class XMLInput : RMServiceType
    {
        [DataMember]
        public string GetXMLInput
        {
            get;
            set;
        }
        //mbahl3 
        [DataMember]
        public bool bVal
        {
            get;
            set;
        }
        //mbahl3 
       
    }

    [DataContract]
    public class XMLOutPut : RMServiceType
    {
        [DataMember]
        public string FileContent
        {
            get;
            set;
        }

        
        [DataMember]
        public bool bIsEvent
        {
            get;
            set;
        }
        //mbahl3
         [DataMember]
        public bool bResult
        {
            get;
            set;
        }
        //mbahl3
    }

    [DataContract]
    public class XMLTemplate : RMServiceType
    {
        [DataMember]
        public string NodeName
        {
            get;
            set;
        }

        [DataMember]
        public XElement SuppXml
        {
            get;
            set;
        }
    }
}