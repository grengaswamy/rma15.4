﻿/***************************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            
 ***************************************************************************************************
 * 05/05/2014 | 33574   | pgupta93   | Changes for Continuous Trigger and Policy Search functionality.
 * 06/04/2014 | 33371   | ajohari2   | Changes for TPA in Policy System Search - added new search filter
 ****************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Collections;
using System.Xml.Linq;
using System.Data;
using System.Xml.Serialization;

namespace Riskmaster.Models
{
    [DataContract]
    public class LossCodeMappingFileContent : RMServiceType
    {

        [DataMember]
        public string FileContent
        {
            get;
            set;
        }
        [DataMember]
        public string PolicySystemIds
        {
            get;
            set;
        }

    }
    [DataContract]
    public class ReplicateLossCodeMapping : RMServiceType
    {
        [DataMember]
        public string ReplicateFromPolicySystemId
        {
            get;
            set;
        }
        [DataMember]
        public string ReplicateToPolicySystemIds
        {
            get;
            set;
        }

    }
    [DataContract]
    public class ExportLossCodeMapping : RMServiceType
    {

        [DataMember]
        public string PolicySystemId
        {
            get;
            set;
        }

    }

    [DataContract]
    public class LossCodeMappingOutput : RMServiceType
    {
        [DataMember]
        public string FileContent
        {
            get;
            set;
        }

        [DataMember]
        public bool HasError
        {
            get;
            set;
        }

        [DataMember]
        public bool TaskScheduled
        {
            get;
            set;
        }
    }

    [DataContract]
    public class PolicySystemList : RMServiceType
    {
        [DataMember]
        public string ResponseXML
        {
            get;
            set;
        }
    }

    [DataContract]
    public class PolicySearch : RMServiceType
    {
        public PolicySearch()
        {
            this.objSearchFilters = new SearchFilters();
            this.SearchRowList = new List<SearchRow>(); //MITS 35932
            this.ResponseXML = string.Empty;
            this.ResponseError = string.Empty;
            this.IntegralClaimEventSetting = string.Empty;
        }

        [DataMember]
        public SearchFilters objSearchFilters
        {
            get;
            set;
        }

        [DataMember]
        public string Ticket
        {
            get;
            set;
        }

        #region OutputParameters
        [DataMember]
        public string ClientFile
        {
            get;
            set;
        }
        [DataMember]
        public string ResponseXML
        {
            get;
            set;
        }
        [DataMember]
        public List<SearchRow> SearchRowList
        {
            get;
            set;
        }
        [DataMember]
        public string ResponseError
        {
            get;
            set;
        }       
        [DataMember]
        public string IntegralClaimEventSetting
        {
            get;
            set;
        }
        #endregion
    }

    [DataContract]
    public class PolicyObjects : RMServiceType
    {
        [DataMember]
        public string PolicyIdentfier
        {
            get;
            set;
        }

        #region OutputParameters
        [DataMember]
        public string ResponseXML
        {
            get;
            set;
        }
        #endregion
    }

    [DataContract]
    public class PolicyEnquiry : RMServiceType
    {
        [DataMember]
        public int PolicySystemId
        {
            get;
            set;
        }
        [DataMember]
        public int PolicyId
        {
            get;
            set;
        }
        [DataMember]
        public string PolicyIdentfier
        {
            get;
            set;
        }
        [DataMember]
        public string PolicySymbol
        {
            get;
            set;
        }
        [DataMember]
        public string AutoVIN
        {
            get;
            set;
        }
        [DataMember]
        public string Location
        {
            get;
            set;
        }
        [DataMember]
        public string LOB
        {
            get;
            set;
        }
        [DataMember]
        public string MasterCompany
        {
            get;
            set;
        }
        [DataMember]
        public string PolicyNumber
        {
            get;
            set;
        }
        [DataMember]
        public string Module
        {
            get;
            set;
        }
        [DataMember]
        public string EffDate
        {
            get;
            set;
        }
        [DataMember]
        public string IssueCode
        {
            get;
            set;
        }
        [DataMember]
        public string BaseLOBLine
        {
            get;
            set;
        }
        [DataMember]
        public bool IsDriverRequest
        {
            get
             {
                 if (string.Equals(BaseLOBLine, "AL") || string.Equals(BaseLOBLine, "CL"))
                 {
                     return true;
                 }
                 else
                     return false;
              }
            set
            {
            }
        }
        [DataMember]
        public string PolCompany
        {
            get;
            set;
        }
        [DataMember]
        public string State
        {
            get;
            set;
        }
        [DataMember]
        public string InsLine
        {
            get;
            set;
        }
        [DataMember]
        public string UnitNumber
        {
            get;
            set;
        }
        [DataMember]
        public string UnitRiskLocation
        {
            get;
            set;
        }
        [DataMember]
        public string UnitSubRiskLocation
        {
            get;
            set;
        }
        [DataMember]
        public string UnitState
        {
            get;
            set;
        }
        [DataMember]
        public string Product
        {
            get;
            set;
        }
        [DataMember]
        public string UnitStatus
        {
            get;
            set;
        }
        [DataMember]
        public string InterestTypeCode
        {
            get;
            set;
        }
        [DataMember]
        public string CovCode
        {
            get;
            set;
        }
        [DataMember]
        public string CovSeqNo
        {
            get;
            set;
        }
        [DataMember]
        public string TransSeq
        {
            get;
            set;
        }
        [DataMember]
        public string CovStatus
        {
            get;
            set;
        }
        //skhare7 Unit intereset
        [DataMember]
        public int UnitRowId
        {
            get;
            set;
        }
        [DataMember]
        public string StatUnitFlag
        {
            get;
            set;
        }

        [DataMember]
        public string PolLossDt
        {
            get;
            set;
        }
        //Ashish Ahuja
        [DataMember]
        public string ClaimDateReported
        {
            get;
            set;
        }

        [DataMember]
        public string UnitTypeInd
        {
            get;
            set;
        }
        
        #region OutputParameters
        [DataMember]
        public string PolicyDataAcordXML
        {
            get;
            set;
        }
        [DataMember]
        public string UnitListAcordXML
        {
            get;
            set;
        }
        //[DataMember]
        //public string WorkLossAcordXML
        //{
        //    get;
        //    set;
        //}
        [DataMember]
        public string PolicyInterestListAcordXML
        {
            get;
            set;
        }
        [DataMember]
        public string DriverListAcordXML
        {
            get;
            set;
        }
        [DataMember]
        public string ResponseAcordXML
        {
            get;
            set;
        }
        #endregion
    }

    [DataContract]
    public class SearchFilters : RMServiceType
    {
        public SearchFilters()
        {
            PolicySystemId = 0;
            State =0;
            LOB=0;
            BaseLOBLine = 0;
            PolicySymbol = string.Empty;
            CustomerNo = string.Empty;
            MasterCompany =string.Empty;
            LocationCompany =string.Empty;
            //MITS:33371 ajohari2:Start
            CombinedPolicyFilter = string.Empty;
            //MITS:33371 ajohari2:End
            Module = string.Empty;
            InsuredLastName = string.Empty;
            InsuredFirstName = string.Empty;
            PolicyNumber = string.Empty;
            //PolicyName = string.Empty;
            //PolicyStatus = 0;
            Agent = string.Empty;
            //UnitAddress = string.Empty;
            City = string.Empty;
            Zip = string.Empty;
            LossDate = string.Empty;
            //DateReported = DateTime.MinValue;
            GroupNo = string.Empty;
            InsuredSSN = string.Empty;
            AgentLastName = string.Empty;
            AgentFirstName = string.Empty;
            AgentNumber = string.Empty;
            VehicleRegistrationNumber = string.Empty;
            VehicleChasisNumber = string.Empty;
            VehicleEngineNumber = string.Empty;
        }
        [DataMember]
        public int PolicySystemId
        {
            get;
            set;
        }
        [DataMember]
        public int LOB
        {
            get;
            set;
        }
           [DataMember]
        public string InsuredSSN
        {
            get;
            set;
        }
        [DataMember]
        public string GroupNo
        {
            get;
            set;
        }
         [DataMember]
        public string CustomerNo
        {
            get;
            set;
        }
         [DataMember]
        public string MasterCompany
        {
            get;
            set;
        }
         [DataMember]
        public string LocationCompany
        {
            get;
            set;
        }
         //MITS:33371 ajohari2:Start
         [DataMember]
         public string CombinedPolicyFilter
         {
             get;
             set;
         }
         //MITS:33371 ajohari2:End
         [DataMember]
        public string PolicySymbol
        {
            get;
            set;
        }
        [DataMember]
        public int State
        {
            get;
            set;
        }
         [DataMember]
        public string InsuredLastName
        {
            get;
            set;
        }
         [DataMember]
        public string InsuredFirstName
        {
            get;
            set;
        }
         [DataMember]
        public string Module
        {
            get;
            set;
        }
        [DataMember]
        public string PolicyNumber
        {
            get;
            set;
        }
        //[DataMember]
        //public string PolicyName
        //{
        //    get;
        //    set;
        //}
        //[DataMember]
        //public int PolicyStatus
        //{
        //    get;
        //    set;
        //}
        [DataMember]
        public string Agent
        {
            get;
            set;
        }
        [DataMember]
        public string UnitAddress
        {
            get;
            set;
        }
        [DataMember]
        public string City
        {
            get;
            set;
        }
        [DataMember]
        public string Zip
        {
            get;
            set;
        }
        [DataMember]
        public string LossDate
        {
            get;
            set;
        }
        //[DataMember]
        //public DateTime DateReported
        //{
        //    get;
        //    set;
        //}
        [DataMember]
        public int BaseLOBLine
        {
            get;
            set;
        }
        [DataMember]
        public string Name
        {
            get;
            set;
        }
        [DataMember]
        public string Address
        {
            get;
            set;
        }
        [DataMember]
        public string AgentLastName
        {
            get;
            set;
        }
        [DataMember]
        public string AgentFirstName
        {
            get;
            set;
        }
        [DataMember]
        public string AgentNumber
        {
            get;
            set;
        }
        [DataMember]
        public string VehicleRegistrationNumber
        {
            get;
            set;
        }
        [DataMember]
        public string VehicleChasisNumber
        {
            get;
            set;
        }
        [DataMember]
        public string VehicleEngineNumber
        {
            get;
            set;
        }
    }

    [DataContract]
    public class DisplayFileData : RMServiceType
    {
        [DataMember]
        public string ResponseXML
        {
            get;
            set;
        }
        [DataMember]
        public string Mode
        {
            get;
            set;
        }
        [DataMember]
        public string EntityTypelist
        {
            get;
            set;
        }
        [DataMember]
        public bool ShowUnitSearch
        {
            get;
            set;
        }
    }

    [DataContract]
    public class DisplayMode : RMServiceType
    {
        [DataMember]
        public string Mode
        {
            get;
            set;
        }
    }
    [DataContract]
    public class SaveDownloadOptions : RMServiceType
    {
        [DataMember]
        public int PolicySystemId
        {
            get;
            set;
        }
        [DataMember]
        public string Mode
        {
            get;
            set;
        }
        [DataMember]
        public string SelectedValue
        {
            get;
            set;
        }
        [DataMember]
        public int PolicyId
        {
            get;
            set;
        }
       
        [DataMember]
        public string AddEntityAs
        {
            get;
            set;
        }

        [DataMember]
        public string InsuranceLine
        {
            get;
            set;
        }


        [DataMember]
        public string EntityRole
        {
            get;
            set;
        }
        [DataMember]
        public string UnitNumber
        {
            get;
            set;
        }

        [DataMember]
        public string RiskLocation
        {
            get;
            set;
        }

        [DataMember]
        public string RiskSubLocation
        {
            get;
            set;
        }
        [DataMember]
        public string Product
        {
            get;
            set;
        }

        [DataMember]
        public string UnitState
        {
            get;
            set;
        }
        [DataMember]
        public string CovCode
        {
            get;
            set;
        }
        [DataMember]
        public string PolCompany
        {
            get;
            set;
        }
        [DataMember]
        public string LossDate
        {
            get;
            set;
        }
        ////Ashish Ahuja: Claims Made Jira 1342
        [DataMember]
        public string ClaimDateReported
        {
            get;
            set;
        }
        [DataMember]
        public string StatUnitFlag
        {
            get;
            set;
        }

        [DataMember]
        public string StatUnitNumber
        {
            get;
            set;
        }

        [DataMember]
        public string LOBCd
        {
            get;
            set;
        }

        

        //#region OutputParameters
        //[DataMember]
        //public string UpdatedIds
        //{
        //    get;
        //    set;
        //}
        //#endregion
    }
    [DataContract]
    public class SavedRecords : RMServiceType
    {
        [DataMember]
        public string Mode
        {
            get;
            set;
        }
        [DataMember]
        public string RecordIds
        {
            get;
            set;
        }
        [DataMember]
        public int PolicyId
        {
            get;
            set;
        }
        [DataMember]
        public string UnitNumbers
        {
            get;
            set;
        }
        [DataMember]
        public string PolicyExternalId
        {
            get;
            set;
        }
        [DataMember]
        public string PolicySymbol
        {
            get;
            set;
        }
    }
     [DataContract]    
    public class PolicySaveRequest : RMServiceType
        {
       
         public bool IsDriverRequest
         {
             get
             {
                 if (string.Equals(BaseLOBLine, "AL") || string.Equals(BaseLOBLine, "CL"))
                 {
                     return true;
                 }
                 //if (string.Equals(BaseLOBLine, "APV") || string.Equals(LOB, "CPP") || string.Equals(LOB, "ACV") || string.Equals(LOB, "BOP"))
                 //{
                 //    return true;
                 //}
                 else
                     return false;
             }
             
         }
            [DataMember]
            public int PolicySystemId
            {
                get;
                set;
            }

            [DataMember]
            public string NameType
            {
                get;
                set;
            }

            [DataMember]
            public string PolicyIdentfier
            {
                get;
                set;
            }

            [DataMember]
            public string PolicySymbol
            {
                get;
                set;
            }
            [DataMember]
            public string State
            {
                get;
                set;
            }
            [DataMember]
            public string PolicyNumber
            {
                get;
                set;
            }

            [DataMember]
            public string ClaimFormName
            {
                get;
                set;
            }

            [DataMember]
            public int ClaimId
            {
                get;
                set;
            }
            [DataMember]
            public string Location
            {
                get;
                set;
            }
            
            [DataMember]
            public string MasterCompany
            {
                get;
                set;
            }
            [DataMember]
            public string LOB
            {
                get;
                set;
            }

            [DataMember]
            public string Module
            {
                get;
                set;
            }

          [DataMember]
            public string IssueCode
            {
                get;
                set;
            }

          [DataMember]
          public string BaseLOBLine
          {
              get;
              set;
          }

         //skhare7 Policy 
        [DataMember]
        public string UnitNumber
        {
            get;
            set;
        }

          [DataMember]
          public string InsurerNm
          {
              get;
              set;
          }

          [DataMember]
          public string InsurerAddr1
          {
              get;
              set;
          }

          [DataMember]
          public string InsurerCity
          {
              get;
              set;
          }

          [DataMember]
          public string InsurerPostalCd
          {
              get;
              set;
          }
          [DataMember]
          public string TaxId
          {
              get;
              set;
          }
          [DataMember]
          public string ClientSeqNo
          {
              get;
              set;
          }
         [DataMember]
          public string InceptionDate
          {
              get;
              set;
          }
         //Ashish Ahuja: Claims Made Jira 1342
         [DataMember]
         public string ClaimBasedFlag
         {
             get;
             set;
         }
          [DataMember]
          public string AddressSeqNo
          {
              get;
              set;
          }
          [DataMember]
          public string BirthDt
          {
              get;
              set;
          }
          [DataMember]
          public string PolLossDt
          {
              get;
              set;
          }
          [DataMember] 
          public string ResponseSoapXML
          {
              get;
              set;
          }
         [DataMember] 
          public string PolicyName
          {
              get;
              set;
          }
         
         //JIRA 1342 ajohari2: Start
         [DataMember]
         public int PolicySearchByDate
         {
             get;
             set;
         }
         //JIRA 1342 ajohari2: End
         
            #region OutputParameters
            [DataMember]
            public bool Result
            {
                get;
                set;
            }
            [DataMember]
            public int AddedPolicyId
            {
                get;
                set;
            }
            public string ResponseAcordXML
            {
                get;
                set;
            }
            #endregion
        }


     [DataContract]
     public class PolicyValidateInput : RMServiceType
     {
         [DataMember]
         public string DateOfClaim
         {
             get;
             set;
         }

         [DataMember]
         public string DateOfEvent
         {
             get;
             set;
         }

         [DataMember]
         public string CurrencyCode
         {
             get;
             set;
         }
         [DataMember]
         public int PolicyId
         {
             get;
             set;
         }
         //nsachdeva2 - 7/26/2012
         [DataMember]
         public int PolicyLobCode
         {
             get;
             set;
         }

         [DataMember]
         public int BaseLobLineCode
         {
             get;
             set;
         }
         //MITS:33574 START
         [DataMember]
         public string PolicyLobShortcode
         {
             get;
             set;
         }
         //MITS:33574 END
     }

     [DataContract]
     public class PolicyValidateResponse : RMServiceType
     {
         [DataMember]
         public string PolicyNumber
         {
             get;
             set;
         }

         [DataMember]
         public bool IsPolicyValid
         {
             get;
             set;
         }

         [DataMember]
         public bool IsMultiCurrencyEnable
         {
             get;
             set;
         }

         [DataMember]
         public bool TriggerClaimFlag
         {
             get;
             set;
         }

         [DataMember]
         public int PolicySystemId
         {
             get;
             set;
         }
         //MITS:33574 START
         [DataMember]
         public int LoBId
         {
             get;
             set;
         }
         [DataMember]
         public int ClaimStatusID
         {
             get;
             set;
         }
         [DataMember]
         public string ParentLobCode
         {
             get;
             set;
         }
         //MITS:33574 END
     }
     [DataContract]
     public class PolicyDownload : RMServiceType
     {
         [DataMember]
         public string TableName
         {
             get;
             set;
         }

         [DataMember]
         public int TableRowID
         {
             get;
             set;
         }

         [DataMember]
         public int PolicyID
         {
             get;
             set;
         }

         [DataMember]
         public string UnitTypeCode
         {
             get;
             set;
         }

         [DataMember]
         public int PolicySystemId
         {
             get;
             set;
         }

         #region Output Parameters
         [DataMember]
         public string AcordResponse
         {
             get;
             set;
         }

         [DataMember]
         public string SoapResponse
         {
             get;
             set;
         }
         [DataMember]
         public string XSLTPath
         {
             get;
             set;
         }

         [DataMember]
         public DataTable EndorsementData
         {
             get;
             set;
         }
         [DataMember]
         public string PolicySystemType
         {
             get;
             set;
         }
         #endregion

     }
    [DataContract]
    public class UnitSearch : RMServiceType
    {
        [DataMember]
        public string UnitNumber
        {
            get;
            set;
        }
        [DataMember]
        public int PolicyId
        {
            get;
            set;
        }

        [DataMember]
        public string UnitDescription
        {
            get;
            set;
        }

        [DataMember]
        public string UnitType
        {
            get;
            set;
        }

        [DataMember]
        public string Status
        {
            get;
            set;
        }

        [DataMember]
        public string VehicleID
        {
            get;
            set;
        }
        [DataMember]
        public string City
        {
            get;
            set;
        }
        [DataMember]
        public string State
        {
            get;
            set;

        }
          [DataMember]
        public string SelectedValue
        {
            get;
            set;

        }

        
        
    }

    [DataContract]
    public class UnitListing : RMServiceType
    {
        [DataMember]
        public string ResponseXML
        {
            get;
            set;
        }

        [DataMember]
        public bool ShowUnitSearch
        {
            get;
            set;
        }

        [DataMember]
        public string Mode
        {
            get;
            set;
        }

        [DataMember]
        public IEnumerable<XElement> UnitList
        {
            get;
            set;
        }

     
    }

    [DataContract]
    public class FetchPolicyDetails: RMServiceType
    {
        [DataMember] 
        public int PolicyID
        {
            get;
            set;
        }
        [DataMember]
        public string PolicyNumber
        {
            get;
            set;
        }
        [DataMember]
        public string Module
        {
            get;
            set;
        }
        [DataMember]
         public string Symbol
        {
            get;
            set;
        }
        [DataMember]
        public string MasterCompany
        {
            get;
            set;
        }
        [DataMember]
        public string LocationCompany
        {
            get;
            set;
        }
        [DataMember]
        public string SrcLocation
        {
            get;
            set;
        }
        [DataMember]
        public string PolSysUserName
        {
            get;
            set;
        }
        [DataMember]
        public string PolSysPassword
        {
            get;
            set;
        }
        [DataMember]
        public string PolicyAction
        {
            get;
            set;
        }
        [DataMember]
        public string PointURL
        {
            get;
            set;
        }
    }
    [DataContract]
    public class SearchRow : RMServiceType
    {
        public SearchRow()
        {
            PolicySystemId = 0;
            PolicyType = string.Empty;
            PolicyLOB = string.Empty;
            PolicyStatus = string.Empty;
            PolicyName = string.Empty;
            PolicyNumber = string.Empty;
            Module = string.Empty;
            PolicySymbol = string.Empty;
            InsuredFirstName = string.Empty;
            InsuredMiddleName = string.Empty;
            InsuredLastName = string.Empty;
            InsuredFullName = string.Empty;
            SortName = string.Empty;
            InsuredTaxID = string.Empty;
            AgentName = string.Empty;
            AgentNumber = string.Empty;
            City = string.Empty;
            State = string.Empty;
            EffectiveDate = string.Empty;
            ExpiryDate = string.Empty;
            InceptionDate = string.Empty;
            MasterCompany = string.Empty;
            LocationCompany = string.Empty;
        }
        [DataMember]
        public int PolicySystemId
        {
            get;
            set;
        }
        [DataMember]
        public string PolicyType
        {
            get;
            set;
        }
        [DataMember]
        public string PolicyLOB
        {
            get;
            set;
        }
        [DataMember]
        public string PolicyNumber
        {
            get;
            set;
        }
        [DataMember]
        public string PolicyName
        {
            get;
            set;
        }
        [DataMember]
        public string PolicyStatus
        {
            get;
            set;
        }
        [DataMember]
        public string PolicySymbol
        {
            get;
            set;
        }
        [DataMember]
        public string Module
        {
            get;
            set;
        }
        [DataMember]
        public string InsuredLastName
        {
            get;
            set;
        }
        [DataMember]
        public string InsuredFirstName
        {
            get;
            set;
        }
        [DataMember]
        public string InsuredMiddleName
        {
            get;
            set;
        }
        [DataMember]
        public string InsuredFullName
        {
            get;
            set;
        }
        [DataMember]
        public string SortName
        {
            get;
            set;
        }
        [DataMember]
        public string InsuredTaxID
        {
            get;
            set;
        }
        [DataMember]
        public string AgentNumber
        {
            get;
            set;
        }
        [DataMember]
        public string AgentName
        {
            get;
            set;
        }
        [DataMember]
        public string InsuredSSN
        {
            get;
            set;
        }
        [DataMember]
        public string City
        {
            get;
            set;
        }
        [DataMember]
        public string State
        {
            get;
            set;
        }
        [DataMember]
        public string MasterCompany
        {
            get;
            set;
        }
        [DataMember]
        public string LocationCompany
        {
            get;
            set;
        }
        [DataMember]
        public string EffectiveDate
        {
            get;
            set;
        }
        [DataMember]
        public string ExpiryDate
        {
            get;
            set;
        }
        [DataMember]
        public string InceptionDate
        {
            get;
            set;
        }
    }
    [DataContract]
    public class ExternalPolicyData : RMServiceType
    {
        public ExternalPolicyData()
        {
            this.PolicyData = new PolicyData();            
        }

        [DataMember]
        public string InterestListRoles { get; set; }

        [DataMember]
        public PolicyData PolicyData;
		
		[DataMember]
        public int ClaimID;

        [DataMember]
        public int PolicySystemID; 

        [DataMember]
        public string ResponseError
        {
            get;
            set;
    }
    }

    [DataContract]
    public class PolicyData
    {
        public PolicyData()
        { 
            this.Units = new List<UnitData>();
            this.Entities = new List<EntityData>();
            this.PrimiumData = new PremiumData();
            this.Endorsements = new List<EndorsementData>();
        }

        [DataMember]
        public string PolicyNumber { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string Symbol { get; set; }

        [DataMember]
        public string LOB { get; set; }

        [DataMember]
        public string StatusCode { get; set; }

        [DataMember]
        public string ServiceBranch { get; set; }

        [DataMember]
        public string EffectiveDate { get; set; }

        [DataMember]
        public string ExpiryDate { get; set; }

        [DataMember]
        public string RenewalType { get; set; }

        [DataMember]
        public string RenewalNumber { get; set; }

        [DataMember]
        public string PaymentPlan { get; set; }

        [DataMember]
        public string PaymentMode { get; set; }

        [DataMember]
        public string NextBillDate { get; set; }

        [DataMember]
        public string Currency { get; set; }

        [DataMember]
        public string CurrencyRate { get; set; }

        [DataMember]
        public string MasterCompany { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string StateCode { get; set; }
        
        [DataMember]
        public string PolicySoapXML { get; set; }

        [DataMember]
        public PremiumData PrimiumData { get; set; }

        [DataMember]
        public List<UnitData> Units;

        [DataMember]
        public List<EntityData> Entities;

        [DataMember]
        public List<EndorsementData> Endorsements;
    }

    [DataContract]
    public class UnitData
    {
        public UnitData()
        { 
            this.Coverages = new List<CoverageData>();
            this.Entities = new List<EntityData>();
            this.PrimiumData = new PremiumData();
            this.RiskClauses = new List<RiskClause>(); //Payal,RMA:7893 
        }

        [DataMember]
        public string UnitTypeInd { get; set; }
        
        [DataMember]
        public string Selected { get; set; }

        [DataMember]
        public string UnitNumber { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string TypeCode { get; set; }

        [DataMember]
        public string AttachedDate { get; set; }

        [DataMember]
        public string EffectiveDate { get; set; }

        [DataMember]
        public string ExpiryDate { get; set; }

        [DataMember]
        public string RatingFlag { get; set; }

        [DataMember]
        public string Currency { get; set; }

        [DataMember]
        public string CurrencyRate { get; set; }

        [DataMember]
        public string VehicleMake { get; set; }

        [DataMember]
        public string Model { get; set; }

        [DataMember]
        public string Year { get; set; }

        [DataMember]
        public string PurchaseValue { get; set; }

        [DataMember]
        public string PurchaseDate { get; set; }

        [DataMember]
        public string Capacity { get; set; }

        [DataMember]
        public string Seats { get; set; }

        [DataMember]
        public string BodyType { get; set; }

        [DataMember]
        public string RegistrationNumber { get; set; }

        [DataMember]
        public string ChasisNumber { get; set; }

        [DataMember]
        public string EngineNumber { get; set; }

        [DataMember]
        public string VehicleClass { get; set; }

        [DataMember]
        public string CoverType { get; set; }

        [DataMember]
        public string VehicleSumInsured { get; set; }

        [DataMember]
        public string UseFor { get; set; }

        [DataMember]
        public string Region { get; set; }

        [DataMember]
        public string GeoLocation { get; set; }

        [DataMember]
        public string Garage { get; set; }

        [DataMember]
        public string CIType { get; set; }

        [DataMember]
        public string Situation { get; set; }
        
        [DataMember]
        public string Business { get; set; }

        [DataMember]
        public string ContractTitle { get; set; }

        [DataMember]
        public string Premises { get; set; }

        [DataMember]
        public string TerritorialLimit { get; set; }

        [DataMember]
        public string Retroactive { get; set; }

        [DataMember]
        public string RetroactiveDate { get; set; }

        [DataMember]
        public string MaintenancePeriod { get; set; }

        [DataMember]
        public string DeclaredPeriod { get; set; }

        [DataMember]
        public string PostalCode { get; set; }

        [DataMember]
        public string RiskAccumulationState { get; set; }

        [DataMember]
        public string RiskAccumulationLocality { get; set; }

        [DataMember]
        public string Register { get; set; }

        [DataMember]
        public string ProtectedBy { get; set; }

        [DataMember]
        public string BuildingStorey { get; set; }

        [DataMember]
        public string Attached { get; set; }

        [DataMember]
        public string OccupiedAs { get; set; }

        [DataMember]
        public string FloorArea { get; set; }

        [DataMember]
        public string RiskRating { get; set; }

        [DataMember]
        public string HazardGrade { get; set; }

        [DataMember]
        public string ConstructionType { get; set; }

        [DataMember]
        public string ConstructionYear { get; set; }

        [DataMember]
        public string UnitSoapXML { get; set; }

        [DataMember]
        public List<RiskClause> RiskClauses { get; set; } //Payal,RMA:7893
        [DataMember]
        public PremiumData PrimiumData { get; set; }

        [DataMember]
        public List<EntityData> Entities;

        [DataMember]
        public List<CoverageData> Coverages;
    }

    public class CoverageData
    {
        public CoverageData()
        {
            this.PrimiumData = new PremiumData();
        }

        [XmlAttribute]
        public string Number { get; set; }

        [XmlAttribute]
        public string Selected { get; set; }

        [DataMember]
        public string AOAIndemnityLimits { get; set; }

        [DataMember]
        public string AOPIndemnityLimits { get; set; }

        [DataMember]
        public string BaseLimit { get; set; }

        [DataMember]
        public string VehicleInterestInsured { get; set; }

        [DataMember]
        public string LimitSumInsured { get; set; }

        [DataMember]
        public string CoverageType { get; set; }

        [DataMember]
        public string CoverageDesc { get; set; }
		
        [DataMember]
        public string Limit { get; set; }

        [DataMember]
        public string CoverageText { get; set; }

        [DataMember]
        public string CoverageSoapXML { get; set; }

        [DataMember]
        public PremiumData PrimiumData { get; set; }
    }

    public class EntityData
    {
        public EntityData()
        {
            this.Addresses = new List<AddressData>();
        }

        [XmlAttribute]
        public string Selected { get; set; }

        [DataMember]
        public string TableID { get; set; }

        [DataMember]
        public string AddAs { get; set; }

        [DataMember]
        public string Role { get; set; }

        [DataMember]
        public string RoleCode { get; set; }
        
        [DataMember]
        public string ClientNumber { get; set; }

        [DataMember]
        public string AgentNumber { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string BusinessName { get; set; }

        [DataMember]
        public string TaxID { get; set; }

        [DataMember]
        public string MobileNumber { get; set; }

        [DataMember]
        public string OfficeNumber { get; set; }

        [DataMember]
        public string ResidenceNumber { get; set; }

        [DataMember]
        public string BirthDate { get; set; }

        [DataMember]
        public string Sex { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string TypeCode { get; set; }

        [DataMember]
        public string TypeDesc { get; set; }

        [DataMember]
        public string PartySoapXML { get; set; }

        [DataMember]
        public List<AddressData> Addresses { get; set; }

    }

    public class AddressData
    {
        //[DataMember]
        //public string AddressNumber { get; set; }

        [DataMember]
        public string Type { get; set; }
        
        [DataMember]
        public string AddressLine1 { get; set; }

        [DataMember]
        public string AddressLine2 { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string ZipCode { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string Telephone { get; set; }

        [DataMember]
        public string Country { get; set; }
    }

    public class PremiumData
    {
        [DataMember]
        public string GrossPremium { get; set; }

        [DataMember]
        public string NetPremium { get; set; }

        [DataMember]
        public string CalculationMethod { get; set; }

        [DataMember]
        public string GrossRetentionOrNetRetention { get; set; }

        [DataMember]
        public string PremiumClass { get; set; }

        [DataMember]
        public string SubAccountCode { get; set; }

        [DataMember]
        public string SubAccountType { get; set; }

        [DataMember]
        public string ExchangeRate { get; set; }

        [DataMember]
        public string OriginalCurrency { get; set; }

        [DataMember]
        public string TotalSumInsured { get; set; }

        [DataMember]
        public string TotalCommissionAnnual { get; set; }

        [DataMember]
        public string TotalCommissionExtra { get; set; }

        [DataMember]
        public string AgentCommission { get; set; }

        [DataMember]
        public string TemporaryOrPermanentChange { get; set; }
    }

    public class EndorsementData
    {
        [DataMember]
        public string EndorsementID { get; set; }

        [DataMember]
        public string EndorsementReasonCode { get; set; }

        [DataMember]
        public string EndorsementReasonDesc { get; set; }

        [DataMember]
        public string EffectiveDate { get; set; }
    }
    //MITS 35932 - End
    //Payal starts
    [DataContract]
    public class UploadClaimData : RMServiceType
    {
        
        public UploadClaimData()
        {
            this.PolicySystemResponses = new List<PolicySystemResponse>();
        }
     
        
        [DataMember]
        public int PolicySystemID { get; set; }

        [DataMember]
        public string UploadXML { get; set; }

        [DataMember]
        public List<PolicySystemResponse> PolicySystemResponses { get; set; }

       

       
    }
    [DataContract]
    public  class PolicySystemResponse
    {
        [DataMember]
        public bool Response { get; set; }

        [DataMember]
        public string ResponseMsg { get; set; }

        [DataMember]
        public string ActivityID { get; set; }
        
    }
    //Payal ends
    //Payal,RMA:7893 --Starts
    [DataContract]
    public class RiskClause
    {
        [DataMember]
        public string RiskCode { get; set; }
        [DataMember]
        public string RiskDesc { get; set; }
    }
    //Payal,RMA:7893 --Ends

    ////RMA-12047
    [DataContract]
    public class CheckDuplicateReq : RMServiceType
    {
        [DataMember]
        public string PolicyNumber
        {
            get;
            set;
        }
        [DataMember]
        public string PolicySymbol
        {
            get;
            set;
        }
        [DataMember]
        public string MasterCompany
        {
            get;
            set;
        }
        [DataMember]
        public string LOBCodeId
        {
            get;
            set;
        }
        [DataMember]
        public string LocationCompany
        {
            get;
            set;
        }
        [DataMember]
        public string Module
        {
            get;
            set;
        }
        [DataMember]
        public string InsuredName
        {
            get;
            set;
        }
        [DataMember]
        public string LossDate
        {
            get;
            set;
        }
        [DataMember]
        public int SearchByDateType
        {
            get;
            set;
        }
        [DataMember]
        public bool OverRideFlag
        {
            get;
            set;
        }
        [DataMember]
        public string PolicySystemType
        {
            get;
            set;
        }
        [DataMember]
        public string Lob
        {
            get;
            set;
        }
        [DataMember]
        public string PolicySystemId
        {
            get;
            set;
        }
    }
    [DataContract]
    public class CheckDuplicateRes : RMServiceType
    {
        [DataMember]
        public string DuplicateClaimIds
        {
            get;
            set;
        }
    }
}
