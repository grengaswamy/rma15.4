﻿using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;


namespace Riskmaster.Models
{
    /// <summary>
    /// RiskmasterMembershipProvider
    /// </summary>
    [DataContract]
    public class RiskmasterMembershipProvider :RMServiceType  
    {
        [DataMember]
        public string AdminPwd
        {
            get;
            set;
        }
        [DataMember]
        public string AdminUserName
        {
            get;
            set;
        }
        [DataMember]
        public string ConnectionString
        {
            get;
            set;
        }
        [DataMember]
        public string ConnectionType
        {
            get;
            set;
        }
        [DataMember]
        public string HostName
        {
            get;
            set;
        }
        [DataMember]
        public bool IsEnabled
        {
            get;
            set;
        }
        [DataMember]
        public int ProviderID
        {
            get;
            set;
        }
        [DataMember]
        public Dictionary<string, string> LDAPServerType
        {
            get;
            set;
        }
        [DataMember]
        public string BaseDN
        {
            get;
            set;
        }
        [DataMember]
        public string UserAttribute
        {
            get;
            set;
        }

    }
    
}
