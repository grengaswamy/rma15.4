﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Riskmaster.Models
{
    [DataContract]
    public class PortalData:RMServiceType
    {
        /// <summary>
        /// sql
        /// </summary>
        [DataMember]
        public string Sql { get; set; }
        /// <summary>
        /// FileName
        /// </summary>
        [DataMember]
        public string FileName { get; set; }
        /// <summary>
        /// Content
        /// </summary>
        [DataMember]
        public string Content { get; set; }
        /// <summary>
        /// FileContent
        /// </summary>
        [DataMember] 
        public byte[] FileContent { get; set; }
    }
}
