﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Collections;

namespace Riskmaster.Models
{
    [DataContract]
    public class WCPowerViewUpgrade : RMServiceType
    {
        [DataMember]
        public bool breadonly
        {
            get;
            set;
        }
        [DataMember]
        public bool bTopDown
        {
            get;
            set;
        }
        [DataMember]
        public string viewXml
        {
            get;
            set;
        }
        [DataMember]
        public string divName
        {
            get;
            set;
        }
        [DataMember]
        public string convertedAspx
        {
            get;
            set;
        }
        [DataMember]
        public int stateID
        {
            get;
            set;
        }
        [DataMember]
        public string jurisdictionalAspx
        {
            get;
            set;
        }

    }
}
