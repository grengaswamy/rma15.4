﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.ServiceModel;
using System.IO;

namespace Riskmaster.Models
{

    [DataContract]
    public class Folder : RMServiceType
    {
        [DataMember]
        public string FolderID
        {
            get;
            set;
        }
        [DataMember]
        public string FolderName
        {
            get;
            set;
        }
    }
    [DataContract]
    public class EmailDocumentsRequest : RMServiceType
    {
        [DataMember]
        public string UserEmailIds
        {
            get;
            set;
        }
        [DataMember]
        public string OtherEmailIds
        {
            get;
            set;
        }
        [DataMember]
        public string Documents
        {
            get;
            set;
        }
        [DataMember]
        public string Subject
        {
            get;
            set;
        }
        [DataMember]
        public string Message
        {
            get;
            set;
        }
        [DataMember]
        public string Psid
        {
            get;
            set;
        }
        [DataMember]
        public string TableName
        {
            get;
            set;
        }
    }
    [DataContract]
    public class DocumentsDeleteRequest : RMServiceType
    {
        [DataMember]
        public string FolderIds
        {
            get;
            set;
        }
        [DataMember]
        public string DocumentIds
        {
            get;
            set;
        }
        [DataMember]
        public string Psid
        {
            get;
            set;
        }
        [DataMember]
        public string TableName
        {
            get;
            set;
        }
        [DataMember]
        public string ScreenFlag
        {
            get;
            set;
        }

    }
    [DataContract]
    public class DocumentUserRequest : RMServiceType
    {
        [DataMember]
        public string Psid
        {
            get;
            set;
        }
    }
    [DataContract]
    public class TransferDocumentsRequest : RMServiceType
    {
        [DataMember]
        public string Psid
        {
            get;
            set;
        }
        [DataMember]
        public string DocumentIds
        {
            get;
            set;
        }
        [DataMember]
        public string UserLoginNames
        {
            get;
            set;
        }
        [DataMember]
        public string TableName
        {
            get;
            set;
        }
        [DataMember]
        public string ScreenFlag
        {
            get;
            set;
        }
    }
    [DataContract]
    public class User
    {
        [DataMember]
        public string Id
        {
            get;
            set;
        }
        [DataMember]
        public string UserName
        {
            get;
            set;
        }
        [DataMember]
        public string FirstName
        {
            get;
            set;
        }
        [DataMember]
        public string LastName
        {
            get;
            set;
        }
        [DataMember]
        public string Email
        {
            get;
            set;
        }
    }
    [DataContract]
    public class UsersList : RMServiceType
    {
        private List<User> objUsers = new List<User>();
        [DataMember]
        public List<User> Users
        {
            get
            {
                return objUsers;
            }
            set
            {
                objUsers = value;
            }
        }
        [DataMember]
        public string LoginUser
        {
            get;
            set;
        }
        [DataMember]
        public string CurrentDate
        {
            get;
            set;
        }
        [DataMember]
        public string CurrentTime
        {
            get;
            set;
        }
    }
    [DataContract]
    public class MoveDocumentsRequest : RMServiceType
    {
        [DataMember]
        public string NewFolderId
        {
            get;
            set;
        }
        [DataMember]
        public string Documents
        {
            get;
            set;
        }
    }
    [DataContract]
    public class FolderTypeRequest : Folder
    {
        [DataMember]
        public string ParentId
        {
            get;
            set;
        }
    }
    [DataContract]
    public class DocumentListRequest : RMServiceType
    {

        [DataMember]
        public string SortOrder
        {
            get;
            set;
        }
        [DataMember]
        public string RecordId
        {
            get;
            set;
        }
        [DataMember]
        public string TableName
        {
            get;
            set;
        }
        [DataMember]
        public string FormName
        {
            get;
            set;
        }

        [DataMember]
        public string FolderId
        {
            get;
            set;
        }
        [DataMember]
        public string ParentFolderId
        {
            get;
            set;
        }
        [DataMember]
        public string FolderName
        {
            get;
            set;
        }
        [DataMember]
        public string ParentFolderName
        {
            get;
            set;
        }
        [DataMember]
        public string PageNumber
        {
            get;
            set;
        }
        [DataMember]
        public string Psid
        {
            get;
            set;
        }
        [DataMember]
        public string Regarding
        {
            get;
            set;

        }
        [DataMember]
        public string ScreenFlag
        {
            get;
            set;

        }
        //Merge: 13502
        [DataMember]
        public string NonMCMFormName
        {
            get;
            set;
        }
       

    }

    [DataContract]
    public class DocumentList : RMServiceType
    {
        private List<DocumentType> objDocs = new List<DocumentType>();
        private List<Folder> objFolders = new List<Folder>();
        [DataMember]
        public List<DocumentType> Documents
        {
            get
            {
                return objDocs;
            }
            set
            {
                objDocs = value;
            }
        }
        [DataMember]
        public Folder TopFolder
        {
            get;
            set;

        }

        [DataMember]
        public string AttachTable
        {
            get;
            set;
        }
        [DataMember]
        public string AttachRecordId
        {
            get;
            set;
        }
        [DataMember]
        public string DocInternalType
        {
            get;
            set;
        }

        [DataMember]
        public string ScreenFlag
        {
            get;
            set;
        }
        [DataMember]
        public string FormName
        {
            get;
            set;
        }

        [DataMember]
        public List<Folder> Folders
        {
            get
            {
                return objFolders;
            }
            set
            {
                objFolders = value;
            }
        }
        [DataMember]
        public string ParentFolder
        {
            get;
            set;
        }

        [DataMember]
        public string AcrosoftLoginName
        {
            get;
            set;
        }
        [DataMember]
        public string AcrosoftSession
        {
            get;
            set;
        }
        [DataMember]
        public string JumpToAcrosoft
        {
            get;
            set;
        }

        [DataMember]
        public string AcrosoftAttachmentsTypeKey
        {
            get;
            set;
        }
        [DataMember]
        public string AcrosoftPolicyTypeKey
        {
            get;
            set;
        }
        //skhare7
        [DataMember]
        public string SearchFolderBeforeCreate
        {
            get;
            set;
        }
       
        [DataMember]
        public string EventFolderFriendlyName
        {
            get;
            set;
        }
        [DataMember]
        public string ClaimFolderFriendlyName
        {
            get;
            set;
        }


        //sachin :For mits 30809
        [DataMember]
        public string ShowMCMForFunds
        {
            get;
            set;
        }



        [DataMember]
        public string PolicyFolderFriendlyName
        {
            get;
            set;
        }
        [DataMember]
        public string AcrosoftUsersTypeKey
        {
            get;
            set;
        }
        [DataMember]
        public string JumpDestination
        {
            get;
            set;
        }
        [DataMember]
        public string AsAnywhereLink
        {
            get;
            set;
        }
        [DataMember]
        public string AcrosoftSkin
        {
            get;
            set;

        }
        [DataMember]
        public string ClaimNumber
        {
            get;
            set;

        }
        [DataMember]
        public string EventNumber
        {
            get;
            set;

        }
        [DataMember]
        public string UsersFolderFriendlyName
        {
            get;
            set;
        }

        [DataMember]
        public string UserId
        {
            get;
            set;
        }
        [DataMember]
        public string Pid
        {
            get;
            set;
        }
        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [DataMember]
        public string Sid
        {
            get;
            set;
        }
        [DataMember]
        public string Psid
        {
            get;
            set;
        }
        [DataMember]
        public string Create_allowed
        {
            get;
            set;
        }
        [DataMember]
        public string Createfolder_allowed
        {
            get;
            set;
        }
        [DataMember]
        public string Att_create_allowed
        {
            get;
            set;
        }
        [DataMember]
        public string Delete_allowed
        {
            get;
            set;
        }
        [DataMember]
        public string Att_delete_allowed
        {
            get;
            set;
        }
        [DataMember]
        public string Transfer_allowed
        {
            get;
            set;
        }
        [DataMember]
        public string Att_transfer_allowed
        {
            get;
            set;
        }
        [DataMember]
        public string Copy_allowed
        {
            get;
            set;
        }
        [DataMember]
        public string Att_copy_allowed
        {
            get;
            set;
        }
        [DataMember]
        public string Email_allowed
        {
            get;
            set;
        }
       
        [DataMember]
        public string Pagenumber
        {
            get;
            set;
        }
        [DataMember]
        public string Pagecount
        {
            get;
            set;
        }
        [DataMember]
        public string Orderby
        {
            get;
            set;
        }
        [DataMember]
        public string Previouspage
        {
            get;
            set;
        }
        [DataMember]
        public string Firstpage
        {
            get;
            set;
        }

        [DataMember]
        public string Nextpage
        {
            get;
            set;
        }

        [DataMember]
        public string Lastpage
        {
            get;
            set;
        }

        [DataMember]
        public string Move_allowed
        {
            get;
            set;
        }
        //Merge: 13013
        [DataMember]
        public string ViewAttachment
        {
            get;
            set;
        }
        //smishra25:Flag which should be set when Use Outlook to email is 
        //turned on in General System Parameter Setup
        [DataMember]
        public bool UseOutlookToEmail
        {
            get;
            set;
        }
        //smishra25:Outlook File Size limit in MB
        [DataMember]
        public int SizeLimitOutlook
        {
            get;
            set;
        }
        //Mona:PaperVisionMerge : Animesh Inserted MITS 18345
        [DataMember]
        public string PaperVisionActive
        {
            get;
            set;
        }

        //tanwar2 - ImageRight - start
        [DataMember]
        public bool UseImageRight { get; set; }

        [DataMember]
        public bool UseImageRightWS { get; set; }
        //tanwar2 - ImageRight - end


    }

    //rbhatia4:R8: Use Media View Setting : July 08 2011
    [DataContract]
    public class DocumentVendorDetails : RMServiceType
    {
        private MediaViewDetails mediaViewDetails;
        private AssociatedRecordInfo associatedRecordInfo;

        public DocumentVendorDetails()
        {
            mediaViewDetails = new MediaViewDetails();
            associatedRecordInfo = new AssociatedRecordInfo();
        }


        [DataMember]
        public string DocumentManagementVendorName
        {
            get;
            set;
        }

        [DataMember]
        public MediaViewDetails MediaViewDetails
        {
            get
            {
                return mediaViewDetails;
            }
            set
            {
                mediaViewDetails = value;
            }
           
        }
        [DataMember]
        public AssociatedRecordInfo AssociatedRecordInfo
        {
            get
            {
                return associatedRecordInfo;
            }
            set
            {
                associatedRecordInfo = value;
            }

        }
        [DataMember]
        public bool OpenMediaView
        {
            get;
            set;
        }
    }

    [DataContract]
    public class MediaViewDetails
    {
        [DataMember]
        public string MediaViewUrl
        {
            get;
            set;
        }
        [DataMember]
        public string MediaViewService
        {
            get;
            set;
        }
        [DataMember]
        public string Source
        {
            get;
            set;
        }
        [DataMember]
        public string Enterprise
        {
            get;
            set;
        }
        [DataMember]
        public string Level
        {
            get;
            set;
        }
        [DataMember]
        public string UserType
        {
            get;
            set;
        }
        [DataMember]
        public string MMAgent
        {
            get;
            set;
        }
        [DataMember]
        public string MMAgency
        {
            get;
            set;
        }
        [DataMember]
        public string CommonUser
        {
            get;
            set;
        }
        [DataMember]
        public string AppConfigList
        {
            get;
            set;
        }

    }

    [DataContract]
    public class AssociatedRecordInfo
    {
        [DataMember]
        public string TableName
        {
            get;
            set;
        }

        [DataMember]
        public string FormName
        {
            get;
            set;
        }

        [DataMember]
        public string RecordId
        {
            get;
            set;
        }

        [DataMember]
        public string AssociatedRecordType
        {
            get;
            set;
        }

        [DataMember]
        public string AssociatedRecordNumber
        {
            get;
            set;
        }
    }

    [DataContract]
    public class DocumentType : RMServiceType
    {
        public DocumentType()
        {
            this.Class = new CodeType();
            this.Category = new CodeType();
            this.Type = new CodeType();
           
        }

        [DataMember]
        public string UserId
        {
            get;
            set;
        }
        [DataMember]
        public int Readonly
        {
            get;
            set;
        }
        [DataMember]
        public string Pid
        {
            get;
            set;
        }
        [DataMember]
        public int View
        {
            get;
            set;
        }
        [DataMember]
        public int Create
        {
            get;
            set;
        }
        [DataMember]
        public int Edit
        {
            get;
            set;
        }
        [DataMember]
        public int Delete
        {
            get;
            set;
        }
        [DataMember]
        public int Transfer
        {
            get;
            set;
        }
        [DataMember]
        public int Copy
        {
            get;
            set;
        }
        [DataMember]
        public int Download
        {
            get;
            set;
        }
        [DataMember]
        public int Move
        {
            get;
            set;
        }
        [DataMember]
        public int Email
        {
            get;
            set;
        }
        [DataMember]
        public string FolderId
        {
            get;
            set;
        }
        [DataMember]
        public string FolderName
        {
            get;
            set;
        }
        [DataMember]
        public string CreateDate
        {
            get;
            set;
        }
        [DataMember]
        public string UserName
        {
            get;
            set;
        }
        [DataMember]
        public string FilePath
        {
            get;
            set;
        }
        [DataMember]
        public string AttachTable
        {
            get;
            set;
        }
        [DataMember]
        public string DocInternalType
        {
            get;
            set;
        }
        [DataMember]
        public int AttachRecordId
        {
            get;
            set;
        }
        [DataMember]
        public int DocumentId
        {
            get;
            set;
        }
        [DataMember]
        public string ScreenFlag
        {
            get;
            set;
        }
        [DataMember]
        public string FormName
        {
            get;
            set;
        }
        [DataMember]
        public int PsId
        {
            get;
            set;
        }
        [DataMember]
        public string Title
        {
            get;
            set;
        }
        [DataMember]
        public string Subject
        {
            get;
            set;
        }
        [DataMember]
        public CodeType Type
        {
            get;
            set;
        }
        [DataMember]
        public string DocumentsType
        {
            get;
            set;
        }
        [DataMember]
        public string DocumentClass
        {
            get;
            set;
        }
        [DataMember]
        public CodeType Class
        {
            get;
            set;
        }
        [DataMember]
        public string DocumentCategory
        {
            get;
            set;
        }
        [DataMember]
        public CodeType Category
        {
            get;
            set;
        }
        [DataMember]
        public string Keywords
        {
            get;
            set;
        }
        [DataMember]
        public string Notes
        {
            get;
            set;
        }
        [DataMember]
        public string FileName
        {
            get;
            set;
        }
        [DataMember]
        public int Upload_Status
        {
            get;
            set;
        }

        [DataMember]
        public long FileSize
        {
            get;
            set;
        }

        [DataMember]
        public double FileSizeInMB
        {
            get;
            set;
        }

        [DataMember]
        [XmlIgnore]
        public byte[] FileContents
        {
            get;
            set;
        }
        //smishra25:Flag which should be set when Use Outlook to email is 
        //turned on in General System Parameter Setup
        [DataMember]
        public bool UseOutlookToEmail
        {
            get;
            set;
        }
        //smishra25:Outlook File Size limit in MB
        [DataMember]
        public int SizeLimitOutlook
        {
            get;
            set;
        }
        //Mona:PaperVisionMerge : Animesh Inserted 
        [DataMember]
        public string IsAtPaperVision
        {
            get;
            set;
        }
        [DataMember]
        public string PaperVisionUrl
        {
            get;
            set;
        }
        //Animesh Insertion Ends 
        [DataMember]
        public string Claimnumber
        {
            get;
            set;
        }
        [DataMember]
        public string Eventnumber
        {
            get;
            set;
        }
        [DataMember]
        public bool bMobileAdj
        {
            get;
            set;
        }
        [DataMember]
        public bool bMobilityAdj
        {
            get;
            set;
        }
        //rsharma220: Added for mobile changes
        [DataMember]
        public int ClaimId
        {
            get;
            set;
        }
        [DataMember]
        public int EventId
        {
            get;
            set;
        }
        //tanwar2 - ImageRight - start
        [DataMember]
        public bool bIRUploadStatus { get; set; }
        //tanwar2 - ImageRight - end

        //nsharma202 -start
        //JSON object returning fileContent as encoded string. This will be converted to byteArray and mapped to fileContents
        [DataMember]
        public string base64EncodedFileContents { get; set; }
        //JSON object returning typeId as string. This will be converted to int TypeId
         [DataMember]
        public string typeId { get; set; }
        //ends

         //Mudabbir added mits 36022
         [DataMember]
         public bool UseSilverlight
         {
             get;
             set;
         }
    }

    [DataContract]
    public class StreamedCodeType
    {
        [DataMember]
        public string ShortCode
        {
            get;
            set;
        }
        [DataMember]
        public string Desc
        {
            get;
            set;
        }
        [DataMember]
        public int Id
        {
            get;
            set;
        }
        [DataMember]
        public string ParentCode
        {
            get;
            set;
        }
        [DataMember]
        public string parentDesc
        {
            get;
            set;
        }
        [DataMember]
        public string parentText
        {
            get;
            set;
        }
    }

    [DataContract]
    public class StreamedBRSDocumentType : RMStreamedServiceType, IDisposable
    {
        [DataMember]
        [XmlIgnore]
        //public Stream fileStream;
        public byte[] FileContents
        {
            get;
            set;
        }

        [DataMember]
        public string FileName
        {
            get;
            set;
        }

        public void Dispose()
        {
            if (FileContents != null)
            {
                FileContents = null;
            }
            //if (fileStream != null)
            //{
            //    fileStream.Dispose();
            //    fileStream = null;
            //}
        }
    }
    //mbahl3 jira RMACLOUD-2383
    [DataContract]
    public class StreamedUploadDocumentType : RMStreamedServiceType, IDisposable
    {
        [DataMember]
        [XmlIgnore]
        
        public byte[] FileContents
        {
            get;
            set;
        }

        [DataMember]
        public string FileName
        {
            get;
            set;
        }

        // npadhy RMACLOUD-2418 - ISO Compatible with Cloud
        // We need filePath, ModuleName, RelatedId (optionsetid) and DocumentType while uploading the Import Files or attachments to DB
        [DataMember]
        public string filePath
        {
            get;
            set;
        }

        [DataMember]
        public string ModuleName
        {
            get;
            set;
        }

        [DataMember]
        public string DocumentType
        {
            get;
            set;
        }


        [DataMember]
        public int RelatedId
        {
            get;
            set;
        }


        public void Dispose()
        {
            if (FileContents != null)
            {
                FileContents = null;
            }
            
        }
    }

    //mbahl3 jira RMACLOUD-2383

    [DataContract]
    public class StreamedDocumentType : RMStreamedServiceType, IDisposable
    {
        public StreamedDocumentType()
        {
            this.Class = new StreamedCodeType();
            this.Category = new StreamedCodeType();
            this.Type = new StreamedCodeType();
        }
        public void Dispose()
        {

            //if (fileStream != null)
            //{
            //    fileStream.Dispose();
            //    fileStream = null;
            //}
            if(FileContents != null)
            {
                FileContents = null;
            }
        }

        //[MessageBodyMember]
        //[XmlIgnore]
        //public Stream fileStream;

       
        [DataMember]
        public string UserId
        {
            get;
            set;
        }
        [DataMember]
        public int Readonly
        {
            get;
            set;
        }
        [DataMember]
        public string Pid
        {
            get;
            set;
        }
        [DataMember]
        public int View
        {
            get;
            set;
        }
        [DataMember]
        public int Create
        {
            get;
            set;
        }
        [DataMember]
        public int Edit
        {
            get;
            set;
        }
        [DataMember]
        public int Delete
        {
            get;
            set;
        }
        [DataMember]
        public int Transfer
        {
            get;
            set;
        }
        [DataMember]
        public int Copy
        {
            get;
            set;
        }
        [DataMember]
        public int Download
        {
            get;
            set;
        }
        [DataMember]
        public int Move
        {
            get;
            set;
        }
        [DataMember]
        public int Email
        {
            get;
            set;
        }
        [DataMember]
        public string FolderId
        {
            get;
            set;
        }
        [DataMember]
        public string FolderName
        {
            get;
            set;
        }
        [DataMember]
        public string CreateDate
        {
            get;
            set;
        }
        [DataMember]
        public string UserName
        {
            get;
            set;
        }
        [DataMember]
        public string FilePath
        {
            get;
            set;
        }
        [DataMember]
        public string AttachTable
        {
            get;
            set;
        }
        [DataMember]
        public string DocInternalType
        {
            get;
            set;
        }
        [DataMember]
        public int AttachRecordId
        {
            get;
            set;
        }
        [DataMember]
        public int DocumentId
        {
            get;
            set;
        }
        [DataMember]
        public string ScreenFlag
        {
            get;
            set;
        }
        [DataMember]
        public string FormName
        {
            get;
            set;
        }
        [DataMember]
        public int PsId
        {
            get;
            set;
        }
        [DataMember]
        public string Title
        {
            get;
            set;
        }
        [DataMember]
        public string Subject
        {
            get;
            set;
        }
        [DataMember]
        public StreamedCodeType Type
        {
            get;
            set;
        }
        [DataMember]
        public string DocumentsType
        {
            get;
            set;
        }
        [DataMember]
        public string DocumentClass
        {
            get;
            set;
        }
        [DataMember]
        public StreamedCodeType Class
        {
            get;
            set;
        }
        [DataMember]
        public string DocumentCategory
        {
            get;
            set;
        }
        [DataMember]
        public StreamedCodeType Category
        {
            get;
            set;
        }
        [DataMember]
        public string FileSize
        {
            get;
            set;
        }
        [DataMember]
        public string Keywords
        {
            get;
            set;
        }
        [DataMember]
        public string Notes
        {
            get;
            set;
        }
        [DataMember]
        public string FileName
        {
            get;
            set;
        }
        [DataMember]
        [XmlIgnore]
        public byte[] FileContents
        {
            get;
            set;
        }
        //Amandeep Mobile Adjuster
        [DataMember]
        public string Claimnumber
        {
            get;
            set;
        }
        [DataMember]
        public bool bMobileAdj
        {
            get;
            set;
        }
        //mbahl3 
        [DataMember]
        public bool bMobilityAdj
        {
            get;
            set;
        }
        [DataMember]
        public string Eventnumber
        {
            get;
            set;
        }
    }

    [MessageContract]
    public class StreamCodeType
    {
        [MessageHeader]
        public string ShortCode
        {
            get;
            set;
        }
        [MessageHeader]
        public string Desc
        {
            get;
            set;
        }
        [MessageHeader]
        public int Id
        {
            get;
            set;
        }
        [MessageHeader]
        public string ParentCode
        {
            get;
            set;
        }
        [MessageHeader]
        public string parentDesc
        {
            get;
            set;
        }
        [MessageHeader]
        public string parentText
        {
            get;
            set;
        }
    }

    [MessageContract]
    public class StreamBRSDocType : RMStreamServiceType, IDisposable
    {
        [MessageBodyMember]
        [XmlIgnore]
        public Stream fileStream;

        [MessageHeader]
        public string FileName
        {
            get;
            set;
        }


        public void Dispose()
        {

            if (fileStream != null)
            {
                fileStream.Dispose();
                fileStream = null;
            }
        }
    }

    [MessageContract]
    public class StreamDocType : RMStreamServiceType, IDisposable
    {
        public StreamDocType()
        {
            this.Class = new StreamedCodeType();
            this.Category = new StreamedCodeType();
            this.Type = new StreamedCodeType();
        }
        public void Dispose()
        {

            if (fileStream != null)
            {
                fileStream.Dispose();
                fileStream = null;
            }
        }

        [MessageBodyMember]
        [XmlIgnore]
        public Stream fileStream;

        [MessageHeader]
        public string UserId
        {
            get;
            set;
        }
        [MessageHeader]
        public int Readonly
        {
            get;
            set;
        }
        [MessageHeader]
        public string Pid
        {
            get;
            set;
        }
        [MessageHeader]
        public int View
        {
            get;
            set;
        }
        [MessageHeader]
        public int Create
        {
            get;
            set;
        }
        [MessageHeader]
        public int Edit
        {
            get;
            set;
        }
        [MessageHeader]
        public int Delete
        {
            get;
            set;
        }
        [MessageHeader]
        public int Transfer
        {
            get;
            set;
        }
        [MessageHeader]
        public int Copy
        {
            get;
            set;
        }
        [MessageHeader]
        public int Download
        {
            get;
            set;
        }
        [MessageHeader]
        public int Move
        {
            get;
            set;
        }
        [MessageHeader]
        public int Email
        {
            get;
            set;
        }
        [MessageHeader]
        public string FolderId
        {
            get;
            set;
        }
        [MessageHeader]
        public string FolderName
        {
            get;
            set;
        }
        [MessageHeader]
        public string CreateDate
        {
            get;
            set;
        }
        [MessageHeader]
        public string UserName
        {
            get;
            set;
        }
        [MessageHeader]
        public string FilePath
        {
            get;
            set;
        }
        [MessageHeader]
        public string AttachTable
        {
            get;
            set;
        }
        [MessageHeader]
        public string DocInternalType
        {
            get;
            set;
        }
        [MessageHeader]
        public int AttachRecordId
        {
            get;
            set;
        }
        [MessageHeader]
        public int DocumentId
        {
            get;
            set;
        }
        [MessageHeader]
        public string ScreenFlag
        {
            get;
            set;
        }
        [MessageHeader]
        public string FormName
        {
            get;
            set;
        }
        [MessageHeader]
        public int PsId
        {
            get;
            set;
        }
        [MessageHeader]
        public string Title
        {
            get;
            set;
        }
        [MessageHeader]
        public string Subject
        {
            get;
            set;
        }
        [MessageHeader]
        public StreamedCodeType Type
        {
            get;
            set;
        }
        [MessageHeader]
        public string DocumentsType
        {
            get;
            set;
        }
        [MessageHeader]
        public string DocumentClass
        {
            get;
            set;
        }
        [MessageHeader]
        public StreamedCodeType Class
        {
            get;
            set;
        }
        [MessageHeader]
        public string DocumentCategory
        {
            get;
            set;
        }
        [MessageHeader]
        public StreamedCodeType Category
        {
            get;
            set;
        }
        [MessageHeader]
        public string FileSize
        {
            get;
            set;
        }
        [MessageHeader]
        public string Keywords
        {
            get;
            set;
        }
        [MessageHeader]
        public string Notes
        {
            get;
            set;
        }
        [MessageHeader]
        public string FileName
        {
            get;
            set;
        }
        [MessageHeader]
        [XmlIgnore]
        public byte[] FileContents
        {
            get;
            set;
        }
        //Amandeep Mobile Adjuster
        [MessageHeader]
        public string Claimnumber
        {
            get;
            set;
        }
        [DataMember]
        public bool bMobileAdj
        {
            get;
            set;
        }
        //mbahl3 
        [DataMember]
        public bool bMobilityAdj
        {
            get;
            set;
        }
        [DataMember]
        public string Eventnumber
        {
            get;
            set;
        }

    }

    /// <summary>
    /// StreamedUploadDocumentType
    /// </summary>
    [MessageContract]
    public class StreamUploadDocType : RMStreamServiceType, IDisposable
    {
        [MessageBodyMember]
        [XmlIgnore]
        public Stream fileStream;


        [MessageHeader]
        public string FileName
        {
            get;
            set;
        }

        [MessageHeader]
        public string filePath
        {
            get;
            set;
        }

        [MessageHeader]
        public string ModuleName
        {
            get;
            set;
        }

        [MessageHeader]
        public string DocumentType
        {
            get;
            set;
        }


        [MessageHeader]
        public int RelatedId
        {
            get;
            set;
        }


        public void Dispose()
        {
            if (fileStream != null)
            {
                fileStream = null;
            }

        }

    }
}
