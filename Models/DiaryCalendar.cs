﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Collections;

namespace Riskmaster.Models
{
    [DataContract]
    public class DiaryInput : RMServiceType
    {
        [DataMember]
        public string GetDiaryInput
        {
            get;
            set;
        }
    }

    [DataContract]
    public class DiaryOutput : RMServiceType
    {
        [DataMember]
        public String UserName
        {
            get;
            set;
        }

        [DataMember]
        public string GetDiaryOutput
        {
            get;
            set;
        }

        [DataMember]
        public string DiaryCalendarErrors
        {
            get;
            set;
        }

    }
}
