﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Collections;
using System.Xml.Linq;
namespace Riskmaster.Models
{
    [DataContract]
    public class MDINavigationTreeNodeProfileRequest : RMServiceType
    {
        [DataMember]
        public int NodeId
        {
            get;
            set;
        }

        [DataMember]
        public string ObjectName
        {
            get;
            set;
        }

        [DataMember]
        public int ViewId
        {
            get;
            set;
        }
    }


    [DataContract]
    public class MDINavigationTreeNodeProfileRequestByParent : RMServiceType
    {
        [DataMember]
        public int NodeId
        {
            get;
            set;
        }

        [DataMember]
        public string ObjectName
        {
            get;
            set;
        }

        [DataMember]
        public int ParentId
        {
            get;
            set;
        }

        [DataMember]
        public string ParentObjectName
        {
            get;
            set;
        }

        [DataMember]
        public int ViewId
        {
            get;
            set;
        }
    }


    [DataContract]
    public class MDINavigationTreeNodeProfileResponse : RMServiceType
    {
        [DataMember]
        public List<string[]> NodeDetail
        {
            get;
            set;
        }

        [DataMember]
        public List<string[]> NodeParent
        {
            get;
            set;
        }

        [DataMember]
        public List<string[]> NodeChildern
        {
            get;
            set;
        }

        [DataMember]
        public List<string[]> NodeSiblings
        {
            get;
            set;
        }
    }


    [DataContract]
    public class InitMDIRequest : RMServiceType
    {
        [DataMember]
        public string SerializedMDIMenu
        {
            get;
            set;
        }

        [DataMember]
        public int ViewId
        {
            get;
            set;
        }
    }

    [DataContract]
    public class InitMDIResponse : RMServiceType
    {
        [DataMember]
        public XElement SerializedMDIMenu
        {
            get;
            set;
        }
    }
    //nkaranam2 - 34408
    [DataContract]
    public class GetSysSettingsRequest : RMServiceType
    { 
        [DataMember]
        public List<String> oSysSettingsReqList
        {
            get;
            set;
        }
    }

    [DataContract]
    public class GetSysSettingsResponse : RMServiceType
    {
        [DataMember]
        public Dictionary<String,Object> oSysSettingsList
        {
            get;
            set;
        }

    }
    //nkaranam2 - 34408
    [DataContract]
    public class MDIClaimantFinancialReq : RMServiceType
    {
        [DataMember]
        public string ClaimantRowId
        {
            get;
            set;
        }

        
    }
    [DataContract]
    public class MDIClaimantFinancialRes : RMServiceType
    {
        [DataMember]
        public int ClaimantEID
        {
            get;
            set;
        }

        [DataMember]
        public int ClaimID
        {
            get;
            set;
        }
        [DataMember]
        public bool  IsGCLOB
        {
            get;
            set;
        }
    }
}
