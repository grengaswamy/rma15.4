using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;


    /// <summary>
    /// Class for handling all Database access methods
    /// using Microsoft's Enterprise Library Data Application Block
    /// for .Net 2.0 (Jan 2006)
    /// </summary>
    public class DataAccess
    {
        /// <summary>
        /// Class constructor
        /// </summary>
	    public DataAccess()
	    {
		    //
		    // TODO: Add constructor logic here
		    //
	    }

        /// <summary>
        /// Executes a SQL stored procedure against the database
        /// and does not return a value
        /// </summary>
        /// <param name="strSQL">the name of the stored procedure</param>
        public static void ExecuteNonQuery(string strSQL)
        {
            Database db = DatabaseFactory.CreateDatabase();

            DbCommand dbCommand = db.GetStoredProcCommand(strSQL);

            //Execute the stored procedure against the database
            //using the specified parameter values        
            db.ExecuteNonQuery(dbCommand);

            //Clean up
            dbCommand = null;
            db = null;

        }//method: ExecuteNonQuery()

        /// <summary>
        /// Executes a SQL stored procedure against the database
        /// that does not return a value
        /// </summary>
        /// <param name="strSQL">the name of the stored procedure</param>
        /// <param name="arrParamValues">an ArrayList containing the parameter values
        /// to the stored procedure</param>
        /// <remarks>This function is specially used for processing
        /// empty data entries (such as optional fields) in order
        /// to appropriately convert the empty string values into 
        /// database NULL values instead</remarks>
        public static void ExecuteNonQuery(string strSQL, ArrayList arrParams)
        {
            Database db = DatabaseFactory.CreateDatabase();

            //Process the ArrayList for empty strings
            //so as to convert them to their appropriate DBNull.Value equivalents
            ProcessArrayList(ref arrParams); 

            //Execute the stored procedure against the database
            //using the specified parameter values        
            db.ExecuteNonQuery(strSQL, arrParams.ToArray());

            //Clean up
            db=null;
        }//method: ExecuteNonQuery()

        /// <summary>
        /// Executes a query against the database that does not return a result set
        /// </summary>
        /// <param name="strStorProcName"></param>
        /// <param name="arrParams"></param>
        public static void ExecuteNonQuery(string strStorProcName, Object[] arrParams)
        {
            Database db = DatabaseFactory.CreateDatabase();

            db.ExecuteNonQuery(strStorProcName, arrParams);

            //Clean up
            db = null;
        }//method: ExecuteNonQuery()

        /// <summary>
        /// Executes a SQL stored procedure against the database
        /// and returns a scalar string value
        /// </summary>
        /// <param name="strSQL">the name of the stored procedure</param>
        /// <param name="strParamName">the name of the stored procedure parameter</param>
        /// <param name="strParamValue">the value of the stored procedure parameter</param>
        /// <returns>a string containing the return value</returns>
        public static string ExecuteScalar(string strSQL, string strParamName, string strParamValue)
        {
            string strRetValue = string.Empty;

            Database db = DatabaseFactory.CreateDatabase();

            DbCommand dbCommand = db.GetStoredProcCommand(strSQL);

            db.AddInParameter(dbCommand, strParamName, DbType.String, strParamValue);

            strRetValue = Convert.ToString(db.ExecuteScalar(dbCommand));

            //Clean up
            dbCommand = null;
            db = null;

            //return the string value from the stored procedure
            return strRetValue;
        }//method: ExecuteScalar

        /// <summary>
        /// Executes a SQL stored procedure against the database
        /// and returns a scalar string value
        /// </summary>
        /// <param name="strSQL">the name of the stored procedure</param>
        /// <param name="arrParamNames">ArrayList of stored procedure parameter names</param>
        /// <param name="arrParamValues">ArrayList of stored procedure parameter values</param>
        /// <returns>a string containing the return value</returns>
        public static string ExecuteScalar(string strSQL, ArrayList arrParamNames, ArrayList arrParamValues)
        {
            string strRetValue = string.Empty;

            Database db = DatabaseFactory.CreateDatabase();

            DbCommand dbCommand = db.GetStoredProcCommand(strSQL);

            //Loop through all of the Parameters
            for (int i = 0; i < arrParamNames.Count; i++)
            {
                db.AddInParameter(dbCommand, arrParamNames[i].ToString(), DbType.String, arrParamValues[i].ToString());
            }//for

            strRetValue = Convert.ToString(db.ExecuteScalar(dbCommand));

            //Clean up
            dbCommand = null;
            db = null;

            //return the string value from the stored procedure
            return strRetValue;
        }//method: ExecuteScalar


        /// <summary>
        /// Executes the stored procedure and returns an integer value
        /// </summary>
        /// <param name="strSQL"></param>
        /// <param name="arrParams"></param>
        /// <returns></returns>
        public static int ExecuteScalar(string strSQL, ArrayList arrParams)
        {
            int intRetValue = 0;

            Database db = DatabaseFactory.CreateDatabase();

            //Process the ArrayList for empty strings
            //so as to convert them to their appropriate DBNull.Value equivalents
            ProcessArrayList(ref arrParams);

            intRetValue = Convert.ToInt32(db.ExecuteScalar(strSQL, arrParams.ToArray()));

            //Clean up
            db = null;

            //return the integer value from the stored procedure
            return intRetValue;
        }//method: ExecuteScalar()

        /// <summary>
        /// Execute a SQL stored procedure against the database
        /// and return a DataReader
        /// </summary>
        /// <param name="strSQL"></param>
        /// <param name="arrParamNames"></param>
        /// <param name="arrParamValues"></param>
        /// <returns></returns>
        public static IDataReader ExecuteReader(string strSQL, List<string> arrParamNames, List<string> arrParamValues)
        {
            IDataReader idr;

            Database db = DatabaseFactory.CreateDatabase();

            DbCommand dbCommand = db.GetStoredProcCommand(strSQL);

            //Loop through all of the Parameters
            for (int i = 0; i < arrParamNames.Count; i++)
            {
                db.AddInParameter(dbCommand, arrParamNames[i], DbType.String, arrParamValues[i]);
            }//for


            idr = db.ExecuteReader(dbCommand);

            //Clean up
            dbCommand = null;
            db = null;

            return idr;
        }//method: ExecuteReader()

        /// <summary>
        /// OVERLOADED: Executes a SQL stored procedure against the database
        /// and returns a DataReader
        /// </summary>
        /// <param name="strStorProcName"></param>
        /// <param name="arrParams"></param>
        /// <returns></returns>
        public static IDataReader ExecuteReader(string strStorProcName, object[] arrParams)
        {
            IDataReader idr;

            Database db = DatabaseFactory.CreateDatabase();

            //Execute the stored procedure against the database 
            //and return the DataReader
            idr = db.ExecuteReader(strStorProcName, arrParams);

            return idr;
        }//method: ExecuteReader()

        /// <summary>
        /// OVERLOADED: Executes a SQL stored procedure against the database
        /// and returns a DataReader
        /// </summary>
        /// <param name="strStorProcName"></param>
        /// <returns></returns>
        public static IDataReader ExecuteReader(string strStorProcName)
        {
            IDataReader idr;

            Database db = DatabaseFactory.CreateDatabase();

            DbCommand dbCommand = db.GetStoredProcCommand(strStorProcName);

            //Execute the stored procedure against the database 
            //and return the DataReader
            idr = db.ExecuteReader(dbCommand);

            return idr;
        }//method: ExecuteReader()

        /// <summary>
        /// Executes a SQL stored procedure against the database
        /// and returns a DataSet
        /// </summary>
        /// <param name="strStorProcName"></param>
        /// <param name="arrParams"></param>
        /// <returns></returns>
        public static DataSet ExecuteDataSet(string strStorProcName, object[] arrParams)
        {
            DataSet dstResults;

            Database db = DatabaseFactory.CreateDatabase();

            //Execute the stored procedure against the database
            //and return the DataSet
            dstResults = db.ExecuteDataSet(strStorProcName, arrParams);

            //Clean up
            db = null;

            //return the DataSet as the result of this method
            return dstResults;
        }//method: ExecuteDataSet()

        /// <summary>
        /// Executes a SQL stored procedure against the database
        /// and returns a DataSet
        /// </summary>
        /// <param name="strStorProcName"></param>
        /// <param name="arrParams"></param>
        /// <returns></returns>
        public static DataSet ExecuteDataSet(string strConnStr, string strStorProcName, object[] arrParams)
        {
            DataSet dstResults;

            Database db = DatabaseFactory.CreateDatabase(strConnStr);

            //Execute the stored procedure against the database
            //and return the DataSet
            dstResults = db.ExecuteDataSet(strStorProcName, arrParams);

            //return the DataSet as the result of this method
            return dstResults;
        }//method: ExecuteDataSet()

        
        /// <summary>
        /// Executes a SQL stored procedure against the database
        /// and returns a DataTable
        /// </summary>
        /// <param name="strStorProcName"></param>
        /// <param name="arrParams"></param>
        /// <param name="strTableName">string to be assigned as the name of the DataTable </param>
        /// <returns></returns>
        /// <remarks>returning a DataTable is particularly useful
        /// when constructing your own custom DataSets that require
        /// relationships to be constructed amongst the tables in the DataSet</remarks>
        public static DataTable ExecuteDataTable(string strStorProcName, object[] arrParams,
            string strTableName)
        {
            DataSet dstResults;
            DataTable dtbResults;

            Database db = DatabaseFactory.CreateDatabase();

            dstResults = db.ExecuteDataSet(strStorProcName, arrParams);

            //Assign the DataSet 
            dtbResults = dstResults.Tables[0];

            //Assign the name of the DataTable
            dtbResults.TableName = strTableName;

            //Clean up
            db = null;

            //return the DataTable as the value of the function
            return dtbResults.Copy();
        } // method: ExecuteDataTable

        /// <summary>
        /// Process an ArrayList of database parameters
        /// </summary>
        /// <param name="arrParams"></param>
        private static void ProcessArrayList(ref ArrayList arrParams)
        {
            //Loop through all of the Parameters
            for (int i = 0; i < arrParams.Count; i++)
            {
                //Check for empty string in the Parameter list
                if (string.IsNullOrEmpty(arrParams[i].ToString()))
                {
                    //Set any empty strings to database null values
                    arrParams[i] = DBNull.Value;
                } // if
            }//for
        } // method: ProcessArrayList
}//class
