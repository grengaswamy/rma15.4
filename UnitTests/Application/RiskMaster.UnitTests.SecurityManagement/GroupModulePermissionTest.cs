﻿using Riskmaster.Application.SecurityManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using Riskmaster.Security;
using System.Configuration;

namespace RiskMaster.UnitTests.SecurityManagement
{
    
    
    /// <summary>
    ///This is a test class for GroupModulePermissionTest and is intended
    ///to contain all GroupModulePermissionTest Unit Tests
    ///</summary>
    [TestClass()]
    public class GroupModulePermissionTest
    {

        ///// <summary>
        ///// Gets the Security Database Connection String
        ///// </summary>
        ///// <returns>RMX Security Database connection string</returns>
        //static string SecurityConnectionString()
        //{
        //    return ConfigurationManager.ConnectionStrings["RMXSecurity"].ConnectionString;
        //} // method: SecurityConnectionString
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///This test case should pass with whole function in code coverage except Catch block
        ///</summary>
        [TestMethod()]
        public void CopyClonePermissionsTest()
        {
            GroupModulePermission target = new GroupModulePermission(); // TODO: Initialize to an appropriate value
            XmlDocument p_objDocIn = new XmlDocument();
            p_objDocIn.LoadXml("<form postback='' iserror='' closeonpostbackifnoerr='' message=''><group><displaycolumn><control name='entitytype' type=''>Datasource</control><control name='selectedentityid' type=''>563</control><control name='groupname' type=''>tedgs</control><control name='groups' type='' value='3' /><control name='hdDBId' type=''>563</control><control name='hdNewDBId' type='' /><control name='hdUserLoginName' type=''>csc</control><control name='hdCloneModuleId' type=''>3</control></displaycolumn></group></form>");
            int p_iGroupId = 3; // Parent group id from which permissions to be copied to clone one
            int c_iGroupId = 12; // Clone Group Id to which permissions to be copied from oroginal one
            bool expected = true; // expected result.
            bool actual;
            actual = target.CopyClonePermissions(p_objDocIn, p_iGroupId, c_iGroupId);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///This test case should fail as we are trying to copy permissions again and code coverage with Catch block
        ///</summary>
        [TestMethod()]
        public void CopyClonePermissionsTest2()
        {
            GroupModulePermission target = new GroupModulePermission(); // TODO: Initialize to an appropriate value
            XmlDocument p_objDocIn = new XmlDocument();
            p_objDocIn.LoadXml("<form postback='' iserror='' closeonpostbackifnoerr='' message=''><group><displaycolumn><control name='entitytype' type=''>Datasource</control><control name='selectedentityid' type=''>563</control><control name='groupname' type=''>tedgs</control><control name='groups' type='' value='3' /><control name='hdDBId' type=''>563</control><control name='hdNewDBId' type='' /><control name='hdUserLoginName' type=''>csc</control><control name='hdCloneModuleId' type=''>3</control></displaycolumn></group></form>");
            int p_iGroupId = 3; // Parent group id from which permissions to be copied to clone one
            int c_iGroupId = 12; // Clone Group Id to which permissions to be copied from oroginal one
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.CopyClonePermissions(p_objDocIn, p_iGroupId, c_iGroupId);
            Assert.AreEqual(expected, actual);
        }
    }
}
