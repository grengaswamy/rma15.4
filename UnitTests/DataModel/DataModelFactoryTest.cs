﻿using Riskmaster.DataModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Riskmaster.Security;
using System.Collections;
using Riskmaster.Common;

namespace Riskmaster.DataModel
{
    
    
    /// <summary>
    ///This is a test class for DataModelFactoryTest and is intended
    ///to contain all DataModelFactoryTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DataModelFactoryTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion




        /// <summary>
        ///A test for CreateDataModelFactory
        ///</summary>
        [TestMethod()]
        public void CreateDataModelFactoryTest()
        {
            string strDbConnString = string.Empty; // TODO: Initialize to an appropriate value
            DataModelFactory target = DataModelFactory.CreateDataModelFactory(strDbConnString);
            Assert.IsNotNull(target);
        }

        /// <summary>
        ///A test for DataModelFactory Constructor
        ///</summary>
        [TestMethod()]
        public void DataModelFactoryConstructorTest()
        {
            string databaseName = "RMXTraining";
            string username = "csc";
            string password = "csc";
            DataModelFactory target = new DataModelFactory(databaseName, username, password);
            Assert.IsNotNull(target);

            //m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword);
            //m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
            //m_objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
            //m_objFunds.FiringScriptFlag = 2;
            ////Rahul mits 10339 - 17 sept 2007
            //m_objAccounts = (Account)m_objDataModelFactory.GetDataModelObject("Account", false);


        }

        /// <summary>
        ///A test for DataModelFactory Constructor
        ///</summary>
        [TestMethod()]
        public void DataModelFactoryConstructorTest2()
        {
            UserLogin objUserLogin = new UserLogin("csc", "RMXTraining");
            DataModelFactory target = new DataModelFactory(objUserLogin);
            Assert.IsNotNull(target);
        }

        /// <summary>
        ///A test for GetDataModelObject
        ///</summary>
        [TestMethod()]
        public void GetDataModelObjectTest()
        {
            string databaseName = "RMXTraining";
            string username = "csc";
            string password = "csc";
            DataModelFactory target = new DataModelFactory(databaseName, username, password);
            string objectType = "Funds";
            bool isLocked = false; // TODO: Initialize to an appropriate value
            DataRoot expected = null; // TODO: Initialize to an appropriate value
            DataRoot actual;
            actual = target.GetDataModelObject(objectType, isLocked);
            Assert.IsNotNull(actual);
            Assert.IsInstanceOfType(actual, typeof(Funds));
        }

        [TestMethod()]
        public void LoadScriptFlagsTest()
        {
            Hashtable expected = new Hashtable();
            Hashtable actual = null;
            
            expected.Add("ValidationEnabled", true);
            expected.Add("InitializationEnabled", true);
            expected.Add("CalculationEnabled", true);
            expected.Add("BeforeSaveEnabled", true);
            expected.Add("AfterSaveEnabled", true);

            try
            {
                actual = RMConfigurationManager.GetDictionarySectionSettings("ScriptingEventsMissing");

            }//try
            catch (System.Configuration.ConfigurationErrorsException)
            {
                //If the specified configuration section does not exist, default all values to true
                actual = expected;
            }//catch

            CollectionAssert.AreEquivalent(expected, actual);

        }
            

    }
}
