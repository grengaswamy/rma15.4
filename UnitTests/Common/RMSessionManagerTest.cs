﻿using Riskmaster.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System;

namespace Riskmaster.Common
{
    
    
    /// <summary>
    ///This is a test class for RMSessionManagerTest and is intended
    ///to contain all RMSessionManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RMSessionManagerTest
    {


        private TestContext testContextInstance;
        private string  strConnString = ConfigurationManager.ConnectionStrings["SessionDataSource"].ConnectionString;
        private const string FUNDS_SESSION_XML = "InsufAmntXml";

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetCustomString
        ///</summary>
        [TestMethod()]
        public void GetCustomStringTest()
        {
            string strContentField = "customize_custom";
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = RMSessionManager.GetCustomString(strContentField);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetCustomString
        ///</summary>
        [TestMethod()]
        public void GetCustomStringSettingsTest()
        {
            string strContentField = "customize_settings";
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = RMSessionManager.GetCustomString(strContentField);
            Assert.AreNotEqual(expected, actual);
        }

        /// <summary>
        ///A test for CreateSession
        ///</summary>
        [TestMethod()]
        public void CreateSessionTest()
        {
            string actual = CreateRMXSession();
            Assert.IsFalse(string.IsNullOrEmpty(actual));
        }

        private string CreateRMXSession()
        {
            string strUserName = "csc";
            byte[] bytUserObj1 = Encoding.ASCII.GetBytes("this is the 1st user object.");
            byte[] bytUserObj2 = Encoding.ASCII.GetBytes("this is the 2nd user object.");

            List<byte[]> arrBinaryItems = new List<byte[]>();

            arrBinaryItems.Add(bytUserObj1);
            arrBinaryItems.Add(bytUserObj2);

            string actual;
            actual = RMSessionManager.CreateSession(strConnString, strUserName, arrBinaryItems);
            return actual;
        }

        [TestMethod()]
        public void LoadSessionTest()
        {
            string strSessionID = "d800cd66-b3be-4d01-a16e-b41acca31f03";
            SessionManager objSessionMgr = SessionManager.LoadSession(strSessionID);

            Assert.IsNotNull(objSessionMgr);
        }//method: LoadSessionTest()

        /// <summary>
        ///A test for GetSession
        ///</summary>
        [TestMethod()]
        public void GetSessionTest()
        {
            
            string strSessionID = "30e82757-ce77-415a-a16c-08f663611d79";
            string strUserName = string.Empty; // TODO: Initialize to an appropriate value

            UserSession actual;
            actual = RMSessionManager.GetSession(strConnString, strSessionID, strUserName);
            Assert.AreEqual(strSessionID, actual.SessionID);
        }

        /// <summary>
        ///A test for SessionExists
        ///</summary>
        [TestMethod()]
        public void SessionExistsTest()
        {
            string strSessionID = "7633e62f-8aff-46d4-86dd-50663e18087f";
            bool expected = true;
            bool actual;
            actual = RMSessionManager.SessionExists(strConnString, strSessionID);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for SessionExists
        ///</summary>
        [TestMethod()]
        public void SessionDoesNotExistTest()
        {
            string strSessionID = "7633e62f-8aff-46d4-86dd-50663e18087g";
            bool expected = false;
            bool actual;
            actual = RMSessionManager.SessionExists(strConnString, strSessionID);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod()]
        public void SetBinaryItemTestUpdate()
        {
            byte[] bytUserObj2 = Encoding.ASCII.GetBytes("This is some updated data.");
            //string strSessionID = CreateRMXSession(); //For testing with a new Session
            string strSessionID = "67315e66-e068-4566-a89b-8db85e0a0f92"; //for testing with an existing Session record

            SessionManager objSessionManager = SessionManager.LoadSession(strSessionID);
            objSessionManager.SetBinaryItem(FUNDS_SESSION_XML, bytUserObj2);
        } // method: SetBinaryItemTest

        /// <summary>
        /// 
        /// </summary>
        [TestMethod()]
        public void SetBinaryItemTestInsert()
        {
            byte[] bytUserObj2 = Encoding.ASCII.GetBytes("this is the 2nd user object.");
            string strBinaryItemName = Utilities.GenerateGuid();
            string strSessionID = CreateRMXSession();

            SessionManager objSessionManager = SessionManager.LoadSession(strSessionID);
            objSessionManager.SetBinaryItem(strBinaryItemName, bytUserObj2);
            objSessionManager.SetBinaryItem(FUNDS_SESSION_XML, bytUserObj2);
        } // method: SetBinaryItemTest


        /// <summary>
        ///A test for UpdateSession
        ///</summary>
        [TestMethod()]
        public void UpdateSessionTest()
        {
            string strSessionID = Utilities.GenerateGuid();
            byte[] bytUserObj2 = Encoding.ASCII.GetBytes("This is insufficient Reserve XML");
            UserSessionBinaryItem objBinaryItem = new UserSessionBinaryItem();
            objBinaryItem.BinaryName = "InsufAmntXml";
            objBinaryItem.BinaryValue = bytUserObj2;
            RMSessionManager.UpdateSession(strConnString, strSessionID, objBinaryItem);
        }

        ///// <summary>
        /////A test for GetBinaryItem
        /////</summary>
        //[TestMethod()]
        public void GetBinaryItemTest()
        {
            string name = "Enhanced Policy";
            byte[] expected = Encoding.ASCII.GetBytes("This is Enhanced Policy storage");
            byte[] actual;
            actual = RMSessionManager.GetBinaryItem(strConnString, name);
            Assert.AreEqual(expected.Length, actual.Length);
        }

        ///// <summary>
        /////A test for SetBinaryItem
        /////</summary>
        [TestMethod()]
        public void SetBinaryItemTestNew()
        {
            string name = "Enhanced Policy";
            byte[] bytes = Encoding.ASCII.GetBytes("This is Enhanced Policy storage");
            RMSessionManager.SetBinaryItem(strConnString, name, bytes);

            Assert.IsTrue(true);
        }

        /// <summary>
        ///A test for UpdateSessionTimestamp
        ///</summary>
        [TestMethod()]
        public void UpdateSessionTimestampTest()
        {
            string strSessionID = "ea530eac-666b-4cdd-92f3-b9ecdd33cdfc";
            string expected = Conversion.ToDbDateTime(DateTime.Now);
            string actual = RMSessionManager.UpdateSessionTimestamp(strConnString, strSessionID);
            Assert.AreEqual(expected, actual);
            
        }
    }
}
