﻿using Riskmaster.Db;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.OracleClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Oracle;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;


namespace Riskmaster.Db
{


    public struct sourceDataTable
    {
        static sourceDataTable()
        {
            
        }
    }
    /// <summary>
    ///This is a test class for ADODbFactoryTest and is intended
    ///to contain all ADODbFactoryTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ADODbFactoryTest
    {

        string strProviderConnString = string.Empty;


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///A test for GetParameterMarkerFormat
        ///</summary>
        [TestMethod()]
        public void GetParameterMarkerFormatOleDbTest()
        {
            string strProviderName = DbProviderTypes.OleDb;
            string expected = "?";
            string actual;
            actual = ADODbFactory.GetParameterMarkerFormat(strProviderName);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetParameterMarkerFormat
        ///</summary>
        [TestMethod()]
        public void GetParameterMarkerFormatOdbcTest()
        {
            string strProviderName = DbProviderTypes.Odbc;
            string expected = "?";
            string actual;
            actual = ADODbFactory.GetParameterMarkerFormat(strProviderName);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetParameterMarkerFormat
        ///</summary>
        [TestMethod()]
        public void GetParameterMarkerFormatSQLServerTest()
        {
            string strProviderName = DbProviderTypes.SqlClient;
            string expected = "@";
            string actual;
            actual = ADODbFactory.GetParameterMarkerFormat(strProviderName);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetParameterMarkerFormat
        ///</summary>
        [TestMethod()]
        public void GetParameterMarkerFormatOracleTest()
        {
            string strProviderName = DbProviderTypes.OracleClient;
            string expected = ":";
            string actual;
            actual = ADODbFactory.GetParameterMarkerFormat(strProviderName);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for CreateDatabase
        ///</summary>
        [TestMethod()]
        public void CreateOdbcDatabaseTest()
        {
            string strConnString = "sample conn string";
            string strFactoryType = DbProviderTypes.Odbc;
            Database actual = null;
            actual = ADODbFactory.CreateDatabase(strConnString, strFactoryType);
            Assert.IsInstanceOfType(actual, typeof(GenericDatabase));
        }


        /// <summary>
        ///A test for CreateDatabase
        ///</summary>
        [TestMethod()]
        public void CreateOleDbDatabaseTest()
        {
            string strConnString = "sample conn string";
            string strFactoryType = DbProviderTypes.OleDb;
            Database actual = null;
            actual = ADODbFactory.CreateDatabase(strConnString, strFactoryType);
            Assert.IsInstanceOfType(actual, typeof(GenericDatabase));
        }

        /// <summary>
        ///A test for CreateDatabase
        ///</summary>
        [TestMethod()]
        public void CreateOracleDatabaseTest()
        {
            string strConnString = "Data Source=TORCL;User Id=myUsername;Password=myPassword;";
            string strFactoryType = DbProviderTypes.MSOracleClient;
            Database actual = null;
            actual = ADODbFactory.CreateDatabase(strConnString, strFactoryType);
            Assert.IsInstanceOfType(actual, typeof(OracleDatabase));
        }

        /// <summary>
        ///A test for CreateDatabase
        ///</summary>
        [TestMethod()]
        public void CreateSqlDatabaseTest()
        {
            string strConnString = "Data Source=myServerAddress;Initial Catalog=myDataBase;User Id=myUsername;Password=myPassword;";
            string strFactoryType = DbProviderTypes.SqlClient;
            Database actual = null;
            actual = ADODbFactory.CreateDatabase(strConnString, strFactoryType);
            Assert.IsInstanceOfType(actual, typeof(SqlDatabase));
        }

        #region ADONetProvider Tests
        /// <summary>
        ///A test for GetADONetProvider
        ///</summary>
        [TestMethod()]
        public void GetADONetProviderSQLServerTest()
        {
            string strConnectionString = ConnectionStringInfoTest.SQLServerConnectionString;
            string expected = DbProviderTypes.SqlClient;
            string actual;
            actual = ADODbFactory.GetADONetProvider(strConnectionString, out strProviderConnString);
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(ConnectionStringInfoTest.ADONetSQLServerConnectionString, strProviderConnString);
        }

        /// <summary>
        ///A test for GetADONetProvider
        ///</summary>
        [TestMethod()]
        public void GetADONetProviderSQLServerNativeTest()
        {
            string strConnectionString = ConnectionStringInfoTest.SQLServerNativeConnectionString;
            string expected = DbProviderTypes.SqlClient;
            string actual;
            actual = ADODbFactory.GetADONetProvider(strConnectionString, out strProviderConnString);
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(ConnectionStringInfoTest.ADONetSQLServerConnectionString, strProviderConnString);
        }


        /// <summary>
        ///A test for GetADONetProvider
        ///</summary>
        [TestMethod()]
        public void GetADONetProviderSQLNativeTest()
        {
            string strConnectionString = ConnectionStringInfoTest.SQLNativeConnectionString;
            string expected = DbProviderTypes.SqlClient;
            string actual;
            actual = ADODbFactory.GetADONetProvider(strConnectionString, out strProviderConnString);
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(ConnectionStringInfoTest.ADONetSQLServerConnectionString, strProviderConnString);
        }


        /// <summary>
        ///A test for GetADONetProvider
        ///</summary>
        [TestMethod()]
        public void GetADONetProviderOracleTest()
        {
            string strConnectionString = ConnectionStringInfoTest.OracleConnectionString;
            string expected = DbProviderTypes.OracleClient;
            string actual;
            actual = ADODbFactory.GetADONetProvider(strConnectionString, out strProviderConnString);
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(ConnectionStringInfoTest.ADONetOracleConnectionString.ToUpper(), strProviderConnString.ToUpper());
        }

        /// <summary>
        ///A test for GetADONetProvider
        ///</summary>
        [TestMethod()]
        public void GetADONetProviderDSNTest()
        {
            string strConnectionString = ConnectionStringInfoTest.OdbcConnectionString;
            string expected = DbProviderTypes.Odbc;
            string actual;
            actual = ADODbFactory.GetADONetProvider(strConnectionString, out strProviderConnString);
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(strConnectionString, strProviderConnString);
        }

        /// <summary>
        ///A test for GetADONetProvider
        ///</summary>
        [TestMethod()]
        public void GetADONetProviderOleDbTest()
        {
            string strConnectionString = ConnectionStringInfoTest.OleDbConnectionString;
            string expected = DbProviderTypes.OleDb;
            string actual;
            actual = ADODbFactory.GetADONetProvider(strConnectionString, out strProviderConnString);
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(strConnectionString, strProviderConnString);
        }
        #endregion

        /// <summary>
        ///A test for BuildRMConnectionString for SQL Server
        ///</summary>
        [TestMethod()]
        public void BuildRMConnectionStringSQLServerTest()
        {
            List<string> arrConnElements = ConnectionStringInfoTest.GetConnectionStringElements(ConnectionStringInfoTest.SQLServerConnectionString);
            string strDriverPrefix = arrConnElements[0].Replace("Driver={", string.Empty);
            string strDriverName = strDriverPrefix.Replace("}", string.Empty);
            string strServerName = arrConnElements[1].Replace("Server=", string.Empty);
            string strDatabaseName = arrConnElements[2].Replace("Database=", string.Empty);
            string strUserName = arrConnElements[3].Replace("UID=", string.Empty);
            string strUserPassword = arrConnElements[4].Replace("PWD=", string.Empty);
            string actual;
            actual = ADODbFactory.BuildRMConnectionString(strDriverName, strServerName, strDatabaseName, strUserName, strUserPassword);
            Assert.AreEqual(ConnectionStringInfoTest.SQLServerConnectionString, actual);
        }

        /// <summary>
        ///A test for BuildRMConnectionString for Oracle
        ///</summary>
        [TestMethod()]
        public void BuildRMConnectionStringOracleTest()
        {

            List<string> arrConnElements = ConnectionStringInfoTest.GetConnectionStringElements(ConnectionStringInfoTest.OracleConnectionString);
            string strDriverPrefix = arrConnElements[0].Replace("Driver={", string.Empty);
            string strDriverName = strDriverPrefix.Replace("}", string.Empty);
            string strServerName = arrConnElements[1].Replace("Server=", string.Empty);
            string strDatabaseName = arrConnElements[2].Replace("DBQ=", string.Empty);
            string strUserName = arrConnElements[3].Replace("UID=", string.Empty);
            string strUserPassword = arrConnElements[4].Replace("PWD=", string.Empty);
            string actual;
            actual = ADODbFactory.BuildRMConnectionString(strDriverName, strServerName, strDatabaseName, strUserName, strUserPassword);
            Assert.AreEqual(ConnectionStringInfoTest.OracleConnectionString, actual);
            
        }

        /// <summary>
        ///A test for SQLBatchCopy
        ///</summary>
        [TestMethod()]
        public void SQLBatchCopyTest()
        {
            string srcConnectionString = ConnectionStringInfoTest.SQLSecurityConnectionString;
            string targetConnectionString = ConnectionStringInfoTest.SQLServerConnectionString;
            string strSQL = "SELECT * FROM DATA_SOURCE_TABLE";
            string strDestTableName = "DATA_SOURCE_TABLE";
            ADODbFactory.SQLBatchCopy(srcConnectionString, targetConnectionString, strSQL, strDestTableName);
            int actual = Convert.ToInt32(DbFactory.ExecuteScalar(targetConnectionString, "SELECT COUNT(*) FROM DATA_SOURCE_TABLE"));
            int expected = 5;
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]
        public void FillDataSet()
        {
          
            Database secDB = ADODbFactory.CreateDatabase(ConnectionStringInfoTest.SQLSecurityConnectionString);

            int iRecordCount = 0, expected = 5;

            using (System.Data.DataSet sourceDataTable = new System.Data.DataSet())
            {
                //Fetch Data from CDC change tables
                System.Data.Common.DbDataAdapter objSourceAd = secDB.GetDataAdapter();
                //Specify the Select Command
                objSourceAd.SelectCommand = secDB.CreateConnection().CreateCommand();
                objSourceAd.SelectCommand.CommandText = "SELECT * FROM DATA_SOURCE_TABLE";

                //Initialize auto command builder. This command builder will be used to
                //create delete commands to delete data form change table
                System.Data.Common.DbCommandBuilder SqlSourceCmdBdr = secDB.DbProviderFactory.CreateCommandBuilder();
                SqlSourceCmdBdr.DataAdapter = objSourceAd;

                //Fill data into DataTable
                objSourceAd.Fill(sourceDataTable);
               iRecordCount  = sourceDataTable.Tables[0].Rows.Count;
                
            }

            Assert.AreEqual(expected, iRecordCount);
        }//method: FillDataSet
    }
}
