﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Riskmaster.Security.Encryption
{
    
    
    /// <summary>
    ///This is a test class for RMCryptographyTest and is intended
    ///to contain all RMCryptographyTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RMCryptographyTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for EncryptSymmetric
        ///</summary>
        [TestMethod()]
        public void EncryptSymmetricTest()
        {
            string strPlainText = "riskmaster";
            string expected = strPlainText;
            string actual;
            actual = RMCryptography.EncryptSymmetric(strPlainText);
            Assert.AreNotEqual(expected, actual);
        }

        /// <summary>
        ///A test for CompareHash
        ///</summary>
        [TestMethod()]
        public void CompareHashTest()
        {
            string strPlainText = string.Empty; // TODO: Initialize to an appropriate value
            string strHashedText = string.Empty; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = RMCryptography.CompareHash(strPlainText, strHashedText);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CreateHash
        ///</summary>
        [TestMethod()]
        public void CreateHashTest()
        {
            string strPlainText = string.Empty; // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = RMCryptography.CreateHash(strPlainText);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for DecryptSymmetric
        ///</summary>
        [TestMethod()]
        public void DecryptSymmetricTest()
        {
            string strEncrypted = string.Empty; // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = RMCryptography.DecryptSymmetric(strEncrypted);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
