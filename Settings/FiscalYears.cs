using System;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Text;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Settings
{
	/// <summary>SYS_PARMS_LOB settings.</summary>
	/// <remarks>Collection contains one object of sys parms setting for each line of business</remarks>
	public class ColFYSettings: IEnumerable 
	{
		//key for each entry in m_col is LOB
		protected Hashtable m_col=null;
		/// <summary>The connection string used by the database layer for connection with the database.</summary>
		private string m_sConnectString;
		/// <summary>The Fiscal year value.</summary>
		private string m_sColFiscalYear="";
		/// <summary>The organization ID.</summary>
		private int m_iColOrgEid=0;
		/// <summary>The Line of Business Code.</summary>
		private int m_iColLineOfBusCode=0;

        private int m_iClientId = 0;

		/// <summary>Constructor creates collection of all FY settings for desired fy,LOB,OrgEid</summary>
		/// <param name="p_sConnectString"></param>
		/// <param name="p_sFY">Pass a FY arg if desire to restrict collection to a fiscal year</param>
		/// <param name="p_iLob">Pass zero to get settings for all Lines of Business</param>
		/// <param name="p_iOrgEid">Pass zero to get settings for all Organizations</param>
		///<remarks>To get all Fiscal Years do not pass a FY arg</remarks>
		///<example>ColFYSettings oCol = new ColFYSettings(connectionString,FY,LOB,OrgEid);</example>
		public ColFYSettings(string p_sConnectString,string p_sFY,int p_iLob,int p_iOrgEid, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			if(p_sFY.Length!=4 && p_sFY.Length!=0)
                throw new InvalidLengthException(Globalization.GetString("ColFYSettings.ColFYSettings.InvalidFYLen", m_iClientId));

			m_sColFiscalYear=p_sFY;
			m_iColLineOfBusCode=p_iLob;
			m_iColOrgEid=p_iOrgEid;
			m_sConnectString=p_sConnectString;
			if(m_col==null)
				LoadSettings(p_sFY,p_iLob,p_iOrgEid);

		}

		///<remarks>This constructor overload lacks FY arg, creates collection with all Fiscal Years for the LOB and OrgEid</remarks>
        public ColFYSettings(string p_sConnectString, int p_iLob, int p_iOrgEid, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_iColLineOfBusCode=p_iLob;
			m_iColOrgEid=p_iOrgEid;
			m_sConnectString=p_sConnectString;
			if(m_col==null)
				LoadSettings("",p_iLob,p_iOrgEid);

		}

		/// <summary>Read-only.  The collection is restricted to this fiscal year passed in constructor.</summary>
		public string FiscalYear
		{
			get{return m_sColFiscalYear;}
		}
		/// <summary>Read-only.  The collection is restricted to this LOB passed in constructor.</summary>
		public int LineOfBusCode
		{
			get{return m_iColLineOfBusCode;}
		}
		/// <summary>Read-only.  The collection is restricted to this OrgEid passed in constructor.</summary>
		public int OrgEid
		{
			get{return m_iColOrgEid;}
		}

		///<summary>Returns a single FYSettings object, containing the settings for a FY of desired FY|LOB|OrgEid</summary>
		///<example>FYSettings oFY = (FYSettings)oCol[FY,LOB,OrgEid];</example>
		public object this[string p_sFY,int p_iLob,int p_iOrgEid]
		{
			
			get
			{
				string sKey=p_sFY+"|"+p_iLob.ToString()+"|"+p_iOrgEid.ToString();				
				return m_col[sKey];				
			}
			set
			{
				string sKey=p_sFY+"|"+p_iLob.ToString()+"|"+p_iOrgEid.ToString();
				if(!m_col.ContainsKey(sKey))
					m_col.Add(sKey,value);
				else
					m_col[sKey] = value;
			}
		}
		IEnumerator IEnumerable.GetEnumerator() 
		{
			if(m_col!=null)
				return ((IEnumerable)m_col.Values).GetEnumerator();
			return null;
		}

		/// <summary>Add a new FYSettings object to the collection of FY settings</summary>
		public void Add(FYSettings p_oFY, string p_sFY,int p_iLob,int p_iOrgEid)
		{
			if(p_sFY.Length!=4)
                throw new InvalidLengthException(Globalization.GetString("ColFYSettings.Add.InvalidFYLen", m_iClientId));
			
			//this will not catch all duplicate violations, but will catch some
			string sKey=p_sFY+"|"+p_iLob.ToString()+"|"+p_iOrgEid.ToString();
			if(m_col.ContainsKey(sKey))
                throw new CollectionException(Globalization.GetString("ColFYSettings.Add.DuplicateColKey", m_iClientId));
			
			m_col.Add(sKey,p_oFY);
		}

		/// <summary>Save changes in FY settings collection to FISCAL_YEAR and FISCAL_YEAR_PERIOD</summary>
		/// <returns>True if saving is successful.</returns>
		public bool SaveSettings()
		{
			DbConnection objConn =null;
			DbTransaction objTrans =null;
			DbWriter writer=null;
			//DbWriter writer = DbFactory.GetDbWriter(this.m_sConnectString);
			foreach (FYSettings oFY in this)
			{
				if(	oFY.DataChanged)
				{
					try
					{
						if(objConn==null)
						{
							objConn = DbFactory.GetDbConnection(this.m_sConnectString);
							objConn.Open();
							objTrans = objConn.BeginTransaction();
							writer=DbFactory.GetDbWriter(objConn);
						}
						else
						{
							objTrans = objConn.BeginTransaction();
							writer.Reset(true);   //keep connection open
						}
						writer.Tables.Add("FISCAL_YEAR");
						if(!oFY.IsNew)
						{
							writer.Where.Add("FISCAL_YEAR="+oFY.FiscalYear);
							writer.Where.Add("LINE_OF_BUS_CODE="+oFY.LineOfBusCode);
							writer.Where.Add("ORG_EID="+oFY.OrgEid);
						}
						writer.Fields.Add("FISCAL_YEAR",oFY.FiscalYear);
						writer.Fields.Add("LINE_OF_BUS_CODE",oFY.LineOfBusCode);
						writer.Fields.Add("ORG_EID",oFY.OrgEid);
						writer.Fields.Add("FY_START_DATE",oFY.FyStartDate);
						writer.Fields.Add("FY_END_DATE",oFY.FyEndDate);
						writer.Fields.Add("QTR1_START_DATE",oFY.Qtr1StartDate);
						writer.Fields.Add("QTR1_END_DATE",oFY.Qtr1EndDate);
						writer.Fields.Add("QTR2_START_DATE",oFY.Qtr2StartDate);
						writer.Fields.Add("QTR2_END_DATE",oFY.Qtr2EndDate);
						writer.Fields.Add("QTR3_START_DATE",oFY.Qtr3StartDate);
						writer.Fields.Add("QTR3_END_DATE",oFY.Qtr3EndDate);
						writer.Fields.Add("QTR4_START_DATE",oFY.Qtr4StartDate);
						writer.Fields.Add("QTR4_END_DATE",oFY.Qtr4EndDate);
						writer.Fields.Add("NUM_PERIODS",oFY.NumPeriods);
						writer.Fields.Add("WEEK_END_DAY",oFY.WeekEndDay);
						writer.Execute(objTrans);						
						foreach (FYSettings.CPeriod oPeriod in (FYSettings.CColPeriods)oFY.Periods)
						{
							if(oPeriod.DataChanged)
							{
								writer.Tables.Add("FISCAL_YEAR_PERIOD");
								if(!oPeriod.IsNew)
								{
									writer.Where.Add("FISCAL_YEAR="+oPeriod.FiscalYear);
									writer.Where.Add("LINE_OF_BUS_CODE="+oPeriod.LineOfBusCode);
									writer.Where.Add("ORG_EID="+oPeriod.OrgEid);
								}
								writer.Fields.Add("FISCAL_YEAR",oPeriod.FiscalYear);
								writer.Fields.Add("LINE_OF_BUS_CODE",oPeriod.LineOfBusCode);
								writer.Fields.Add("ORG_EID",oPeriod.OrgEid);
								writer.Fields.Add("PERIOD_NUMBER",oPeriod.PeriodNumber);
								writer.Fields.Add("PERIOD_START_DATE",oPeriod.PeriodStartDate);
								writer.Fields.Add("PERIOD_END_DATE",oPeriod.PeriodEndDate);						
								writer.Execute(objTrans);
								oPeriod.DataChanged=false;
							}
						}
						objTrans.Commit();
						oFY.DataChanged=false;
					}					
					catch(Exception e)
					{
                        throw new DataModelException(Globalization.GetString("ColFYSettings.SaveSettings.DataError", m_iClientId), e);
					}					
				}			
			}			
			return true;
		}

		/// <summary>
		/// Load all fiscal years that meet desired combination of FY,LOB,Org.
		/// </summary>
		/// <returns>True if success</returns>
		internal bool LoadSettings(string sFY,int iLob,int iOrgEid)
		{
			DbReader rdr=null;
			m_col = new Hashtable();
			string sKey="";
			StringBuilder sbSQL = new StringBuilder("SELECT * FROM FISCAL_YEAR");
			
			//if user does not pass fy arg he wants all of them, but always restrict by LOB and OrgEid
			if(sFY.Length==4)
				sbSQL.Append(" WHERE FISCAL_YEAR='"+sFY+"' AND");
			else
				sbSQL.Append(" WHERE");

			if(iLob==0)
				sbSQL.Append(" (LINE_OF_BUS_CODE="+iLob+" OR LINE_OF_BUS_CODE IS NULL)");
			else
				sbSQL.Append(" LINE_OF_BUS_CODE="+iLob);

			if(iOrgEid==0)
				sbSQL.Append(" AND (ORG_EID="+iOrgEid+" OR ORG_EID IS NULL)");
			else
				sbSQL.Append(" AND ORG_EID="+iOrgEid);	
			
			try
			{
				rdr=DbFactory.GetDbReader(m_sConnectString,sbSQL.ToString());

				if (rdr != null)
				{	
					while(rdr.Read())
					{
						FYSettings oFY = new FYSettings();
						oFY.FyStartDate=rdr.GetString("FY_START_DATE");
						oFY.FyEndDate=rdr.GetString("FY_END_DATE");
						oFY.Qtr1StartDate=rdr.GetString("QTR1_START_DATE");
						oFY.Qtr1EndDate=rdr.GetString("QTR1_END_DATE");
						oFY.Qtr2StartDate=rdr.GetString("QTR2_START_DATE");
						oFY.Qtr2EndDate=rdr.GetString("QTR2_END_DATE");
						oFY.Qtr3StartDate=rdr.GetString("QTR3_START_DATE");
						oFY.Qtr3EndDate=rdr.GetString("QTR3_END_DATE");
						oFY.Qtr4StartDate=rdr.GetString("QTR4_START_DATE");
						oFY.Qtr4EndDate=rdr.GetString("QTR4_END_DATE");
						oFY.NumPeriods=rdr.GetInt32("NUM_PERIODS");
						oFY.WeekEndDay=rdr.GetInt32("WEEK_END_DAY");
						oFY.FiscalYear=rdr.GetString("FISCAL_YEAR");
						oFY.LineOfBusCode=rdr.GetInt32("LINE_OF_BUS_CODE");
						oFY.OrgEid=rdr.GetInt32("ORG_EID");
						oFY.DataChanged=false;
						sKey=oFY.FiscalYear+"|"+oFY.LineOfBusCode.ToString()+"|"+oFY.OrgEid.ToString();
						m_col.Add(sKey, oFY);
					}

				}
				rdr.Close();
			}
			catch(Exception e)
			{
                throw new DataModelException(Globalization.GetString("ColFYSettings.LoadSettings.DataError", m_iClientId), e);
			}

			//for each fy object grab related records in fiscal_year_period
			foreach(object obj in m_col)
			{
				FYSettings oFY=(FYSettings)((System.Collections.DictionaryEntry)obj).Value;
				sbSQL=new StringBuilder("SELECT * FROM FISCAL_YEAR_PERIOD");
				if(oFY.FiscalYear.Length==4)
					sbSQL.Append(" WHERE FISCAL_YEAR='"+oFY.FiscalYear+"' AND");
				else
					sbSQL.Append(" WHERE");

				if(oFY.LineOfBusCode==0)
					sbSQL.Append(" (LINE_OF_BUS_CODE=0 OR LINE_OF_BUS_CODE IS NULL)");
				else
					sbSQL.Append(" LINE_OF_BUS_CODE="+oFY.LineOfBusCode);

				if(oFY.OrgEid==0)
					sbSQL.Append(" AND (ORG_EID=0 OR ORG_EID IS NULL)");
				else
					sbSQL.Append(" AND ORG_EID="+oFY.OrgEid);

				try
				{
					rdr=DbFactory.GetDbReader(m_sConnectString,sbSQL.ToString());

					if (rdr != null)
					{	
						//create collection of period objects for this fiscal year|lob|org combination
						FYSettings.CColPeriods oColPeriods = (FYSettings.CColPeriods)oFY.Periods;
						while(rdr.Read())
						{
							FYSettings.CPeriod oPeriod = new FYSettings.CPeriod();
							oPeriod.FiscalYear=rdr.GetString("FISCAL_YEAR");
							oPeriod.LineOfBusCode=rdr.GetInt32("LINE_OF_BUS_CODE");
							oPeriod.OrgEid=rdr.GetInt32("ORG_EID");
							oPeriod.PeriodEndDate=rdr.GetString("PERIOD_END_DATE");
							oPeriod.PeriodNumber=rdr.GetInt32("PERIOD_NUMBER");
							oPeriod.PeriodStartDate=rdr.GetString("PERIOD_START_DATE");
							oColPeriods.AddExisting(oPeriod);
						}
					}
					oFY.DataChanged=false;
					rdr.Close();
				}
				catch(Exception e)
				{
                    throw new DataModelException(Globalization.GetString("ColFYSettings.LoadSettings.DataError", m_iClientId), e);
				}
			}

			return true;
		}

	}

	/// <summary></summary>
	public class FYSettings
	{

		public FYSettings()
		{
			//L
			// constructor
			//
		}

		//each fiscal year has a collection of fiscal_year_periods
		private CColPeriods m_ColPeriods=null;
		/// <summary>
		/// Collection of Periods for a Fiscal Year
		/// </summary>
		public CColPeriods Periods
		{
			get
			{
				if(m_ColPeriods==null)
					m_ColPeriods=new CColPeriods();
				return m_ColPeriods;
			}
		}
		public class CColPeriods: IEnumerable
		{
			protected Hashtable col=new Hashtable(); 
			public object this[string sKey]
			{
				get{return col[sKey];}
				set
				{
					if(!col.ContainsKey(sKey))
						col.Add(sKey,value);
					else
						col[sKey] = value;
				}
			}
			/// <summary>
			/// This method sets the .IsNew property "true".
			/// </summary>
			public void AddNew(CPeriod p_oPeriod)
			{
				p_oPeriod.IsNew=true;
				string sKey=p_oPeriod.PeriodNumber.ToString()+"|"+p_oPeriod.FiscalYear+"|"+p_oPeriod.LineOfBusCode.ToString()+"|"+p_oPeriod.OrgEid.ToString();
				col.Add(sKey,p_oPeriod);
			}
			/// <summary>Public method marks as new.  This is for filling in LoadSettings()</summary>
			public void AddExisting(CPeriod p_oPeriod)
			{
				string sKey=p_oPeriod.PeriodNumber.ToString()+"|"+p_oPeriod.FiscalYear+"|"+p_oPeriod.LineOfBusCode.ToString()+"|"+p_oPeriod.OrgEid.ToString();
				col.Add(p_oPeriod.PeriodNumber.ToString(),p_oPeriod);
			}			
			IEnumerator IEnumerable.GetEnumerator() 
			{
				if(col!=null)
					return ((IEnumerable)col.Values).GetEnumerator();
				return null;
			}
		}

		/// <summary>Each period setting.</summary>
		public class CPeriod
		{
			/// <summary>Fiscal year value.</summary>
			private string m_sFiscalYear;
			/// <summary>Line of Business Code.</summary>
			private int m_iLineOfBusCode;
			/// <summary>Organization ID.</summary>
			private int m_iOrgEid;
			/// <summary>Period end date.</summary>
			private string m_sPeriodEndDate;
			/// <summary>Period number.</summary>
			private int m_iPeriodNumber;
			/// <summary>Period start date.</summary>
			private string m_sPeriodStartDate;
			/// <summary>Stores whether the data has been modified or not.</summary>
			private bool m_bDataChanged;
			/// <summary>Stores whether a new row is to be entered or an existing one modified.</summary>
			private bool m_IsNew;

			/// <summary>
			///Property to access m_sFiscalYear.
			/// </summary>
			public string FiscalYear
			{
				get{return m_sFiscalYear;}
				set
				{
					if(m_sFiscalYear!=value)
					{
						m_bDataChanged=true; 
						m_sFiscalYear=value;
					}
				}
			}

			/// <summary>
			///Property to access m_iLineOfBusCode.
			/// </summary>			
			public int LineOfBusCode
			{
				get{return m_iLineOfBusCode;}
				set
				{
					if(m_iLineOfBusCode!=value)
					{
						m_bDataChanged=true;
						m_iLineOfBusCode=value;
					}
				}
			}
			
			/// <summary>
			///Property to access m_iOrgEid.
			/// </summary>	
			public int OrgEid
			{
				get{return m_iOrgEid;}
				set
				{
					if(m_iOrgEid!=value)
					{
						m_bDataChanged=true; 
						m_iOrgEid=value;
					}
				}
			}
			
			/// <summary>
			///Property to access m_sPeriodEndDate.
			/// </summary>	
			public string PeriodEndDate
			{
				get{return m_sPeriodEndDate;}
				set
				{
					if(m_sPeriodEndDate!=value)
					{
						m_bDataChanged=true; 
						m_sPeriodEndDate=value;
					}
				}
			}
			
			/// <summary>
			///Property to access m_iPeriodNumber.
			/// </summary>
			public int PeriodNumber
			{
				get{return m_iPeriodNumber;}
				set
				{
					if(m_iPeriodNumber!=value)
					{
						m_bDataChanged=true; 
						m_iPeriodNumber=value;
					}
				}
			}
			
			/// <summary>
			///Property to access m_sPeriodStartDate.
			/// </summary>
			public string PeriodStartDate
			{
				get{return m_sPeriodStartDate;}
				set
				{
					if(m_sPeriodStartDate!=value)
					{
						m_bDataChanged=true; 
						m_sPeriodStartDate=value;
					}
				}
			}
			
			/// <summary>
			///Property to access m_bDataChanged.
			/// </summary>
			public bool DataChanged
			{
				get{return m_bDataChanged;}
				set{m_bDataChanged=value;}
			}
			
			/// <summary>
			///Property to access m_IsNew.
			/// </summary>
			internal bool IsNew
			{
				get{return m_IsNew;}
				set
				{
					if(m_IsNew!=value)
					{
						m_bDataChanged=true; 
						m_IsNew=value;
					}
				}
			}
		}

		/// <summary>Fiscal year start date value.</summary>
		private string m_sFyStartDate;
		/// <summary>Fiscal year end date value.</summary>
		private string m_sFyEndDate;
		/// <summary>1st quarter start date value.</summary>
		private string m_sQtr1StartDate;
		/// <summary>1st quarter end date value.</summary>
		private string m_sQtr1EndDate;
		/// <summary>2nd quarter start date value.</summary>
		private string m_sQtr2StartDate;
		/// <summary>2nd quarter end date value.</summary>
		private string m_sQtr2EndDate;
		/// <summary>3rd quarter start date value.</summary>
		private string m_sQtr3StartDate;
		/// <summary>3rd quarter end date value.</summary>
		private string m_sQtr3EndDate;
		/// <summary>4th quarter start date value.</summary>
		private string m_sQtr4StartDate;
		/// <summary>4th quarter end date value.</summary>
		private string m_sQtr4EndDate;
		/// <summary>Number of periods.</summary>
		private int m_iNumPeriods;
		/// <summary>Weekend day.</summary>
		private int m_iWeekEndDay;
		/// <summary>Fiscal year.</summary>
		private string m_sFiscalYear;
		/// <summary>Line of business code.</summary>
		private int m_iLineOfBusCode;
		/// <summary>Organization ID.</summary>
		private int m_iOrgEid;
		/// <summary>Stores whether the data has been modified or not.</summary>
		private bool m_bDataChanged;
		/// <summary>Stores whether a new row is to be entered or an existing one modified.</summary>
		private bool m_IsNew;
		
		/// <summary>
		///Property to access m_sFyStartDate.
		/// </summary>
		public string FyStartDate
		{
			get{return m_sFyStartDate;}
			set
			{
				if(m_sFyStartDate!=value)
				{
					m_bDataChanged=true; 
					m_sFyStartDate=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_sFyEndDate.
		/// </summary>
		public string FyEndDate
		{
			get{return m_sFyEndDate;}
			set
			{
				if(m_sFyEndDate!=value)
				{
					m_bDataChanged=true; 
					m_sFyEndDate=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_sQtr1StartDate.
		/// </summary>
		public string Qtr1StartDate
		{
			get{return m_sQtr1StartDate;}
			set
			{
				if(m_sQtr1StartDate!=value)
				{
					m_bDataChanged=true; 
					m_sQtr1StartDate=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_sQtr1EndDate.
		/// </summary>
		public string Qtr1EndDate
		{
			get{return m_sQtr1EndDate;}
			set
			{
				if(m_sQtr1EndDate!=value)
				{
					m_bDataChanged=true; 
					m_sQtr1EndDate=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_sQtr2StartDate.
		/// </summary>
		public string Qtr2StartDate
		{
			get{return m_sQtr2StartDate;}
			set
			{
				if(m_sQtr2StartDate!=value)
				{
					m_bDataChanged=true; 
					m_sQtr2StartDate=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_sQtr2EndDate.
		/// </summary>
		public string Qtr2EndDate
		{
			get{return m_sQtr2EndDate;}
			set
			{
				if(m_sQtr2EndDate!=value)
				{
					m_bDataChanged=true; 
					m_sQtr2EndDate=value;
				}
			}
		}

		/// <summary>
		///Property to access m_sQtr3StartDate.
		/// </summary>		
		public string Qtr3StartDate
		{
			get{return m_sQtr3StartDate;}
			set
			{
				if(m_sQtr3StartDate!=value)
				{
					m_bDataChanged=true; 
					m_sQtr3StartDate=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_sQtr3EndDate.
		/// </summary>		
		public string Qtr3EndDate
		{
			get{return m_sQtr3EndDate;}
			set
			{
				if(m_sQtr3EndDate!=value)
				{
					m_bDataChanged=true; 
					m_sQtr3EndDate=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_sQtr4StartDate.
		/// </summary>		
		public string Qtr4StartDate
		{
			get{return m_sQtr4StartDate;}
			set
			{
				if(m_sQtr4StartDate!=value)
				{
					m_bDataChanged=true; 
					m_sQtr4StartDate=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_sQtr4EndDate.
		/// </summary>	
		public string Qtr4EndDate
		{
			get{return m_sQtr4EndDate;}
			set
			{
				if(m_sQtr4EndDate!=value)
				{
					m_bDataChanged=true; 
					m_sQtr4EndDate=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_iNumPeriods.
		/// </summary>	
		public int NumPeriods
		{
			get{return m_iNumPeriods;}
			set
			{
				if(m_iNumPeriods!=value)
				{
					m_bDataChanged=true; 
					m_iNumPeriods=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_iWeekEndDay.
		/// </summary>	
		public int WeekEndDay
		{
			get{return m_iWeekEndDay;}
			set
			{
				if(m_iWeekEndDay!=value)
				{
					m_bDataChanged=true; 
					m_iWeekEndDay=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_sFiscalYear.
		/// </summary>	
		public string FiscalYear
		{
			get{return m_sFiscalYear;}
			set
			{
				if(m_sFiscalYear!=value)
				{
					m_bDataChanged=true; 
					m_sFiscalYear=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_iLineOfBusCode.
		/// </summary>	
		public int LineOfBusCode
		{
			get{return m_iLineOfBusCode;}
			set
			{
				if(m_iLineOfBusCode!=value)
				{
					m_bDataChanged=true; 
					m_iLineOfBusCode=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_iOrgEid.
		/// </summary>
		public int OrgEid
		{
			get{return m_iOrgEid;}
			set
			{
				if(m_iOrgEid!=value)
				{
					m_bDataChanged=true; 
					m_iOrgEid=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_IsNew.
		/// </summary>
		public bool IsNew
		{
			get{return m_IsNew;}
			set
			{
				if(m_IsNew!=value)
				{
					m_IsNew=true; 
					m_IsNew=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_bDataChanged.
		/// </summary>
		public bool DataChanged
		{
			get{return m_bDataChanged;}
			set{m_bDataChanged=true; m_bDataChanged=value;}
		}


	}
}
