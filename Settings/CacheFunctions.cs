using System;
using System.Collections;
using System.Data;
using System.Text;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Text.RegularExpressions;

namespace Riskmaster.Settings
{
	/// <summary>
	/// Riskmaster.Settings.SysParms.CCacheFunctions class is used to retrieve system 
	/// setting parameters for gloassary , codes and tables
	/// </summary>
	public class CCacheFunctions
	{
		/// <summary>The connection string used by the database layer for connection with the database.</summary>
		private string m_sConnectString;
        private int m_iClientId = 0;
		/// <summary>
		///Riskmaster.Settings.SysParms.CCacheFunctions overload sets the connection string.
		/// </summary>
		/// <param name="sConnectString">Connection string</param>
        public CCacheFunctions(string p_sConnectString, int p_iClientId)
		{
			m_sConnectString=p_sConnectString;
            m_iClientId = p_iClientId;
		}

		/// <summary>
		///Riskmaster.Settings.SysParms.GetRelatedCodeID returns the RELATED_CODE_ID
		///for a particular CODE_ID in the CODES table.
		/// </summary>
		/// <param name="p_iCodeID">CODE_ID for which the RELATED_CODE_ID is required</param>
		/// <returns>Related Code ID</returns>
		public int GetRelatedCodeID(int p_iCodeID)
		{
			int iRelatedCodeID=0;
			if(p_iCodeID==0)
				return 0;
			try
			{	
				using(DbReader rdr=DbFactory.GetDbReader(m_sConnectString,"SELECT RELATED_CODE_ID FROM CODES WHERE CODE_ID = " + p_iCodeID))
				{
					if (rdr != null)
						if(rdr.Read())
							iRelatedCodeID=rdr.GetInt32("RELATED_CODE_ID");
				}
			}
			catch(Exception e)
			{
				throw new DataModelException(Globalization.GetString("CCacheFunctions.GetRelatedCodeID.DataError", m_iClientId),e);
			}
			return iRelatedCodeID;
		}

		/// <summary>
		///Riskmaster.Settings.SysParms.GetOrgAbbreviation returns the ABBREVIATION
		///for a particular ENTITY_ID in the ENTITY table.
		/// </summary>
		/// <param name="p_iEntityID">ENTITY_ID for which the ABBREVIATION is required</param>
		/// <returns>Abbreviation</returns>
		public string GetOrgAbbreviation(int p_iEntityID)
		{
			string sOrgAbbreviation="";
			if(p_iEntityID==0)
				return "";		
			try
			{
				using(DbReader rdr=DbFactory.GetDbReader(m_sConnectString,"SELECT ABBREVIATION FROM ENTITY WHERE ENTITY_ID = " + p_iEntityID))
				{
					if (rdr != null)
						if(rdr.Read())
							sOrgAbbreviation=rdr.GetString("ABBREVIATION");
				}
			}
			catch(Exception e)
			{
				throw new DataModelException(Globalization.GetString("CCacheFunctions.GetOrgAbbreviation.DataError", m_iClientId),e);
			}
			return sOrgAbbreviation;
		}

		/// <summary>
		///Riskmaster.Settings.SysParms.GetShortCode returns the SHORT_CODE
		///for a particular CODE_ID in the CODES table.
		/// </summary>
		/// <param name="iCode">CODE_ID for which the SHORT_CODE is required</param>
		/// <returns>Short code</returns>
		public string GetShortCode(int p_iCode)
		{
			string sShortCode="";
			if(p_iCode==0)
				return "";
			try
			{
				using(DbReader rdr=DbFactory.GetDbReader(m_sConnectString,"SELECT SHORT_CODE FROM CODES WHERE CODE_ID = " + p_iCode))
				{
					if (rdr != null)
						if(rdr.Read())
							sShortCode=rdr.GetString("SHORT_CODE");
				}
			}
			catch(Exception e)
			{
				throw new DataModelException(Globalization.GetString("CCacheFunctions.GetShortCode.DataError", m_iClientId),e);
			}
			return sShortCode;
		}

		/// <summary>
		///Riskmaster.Settings.SysParms.GetCodeIDWithShort returns the CODE_ID
		///for a particular SHORT_CODE and SYSTEM_TABLE_NAME in the CODES,GLOSSARY table.
		/// </summary>
		/// <param name="p_ShortCode">SHORT_CODE for which the CODE_ID is required</param>
		/// <param name="p_sSysTableName">SYSTEM_TABLE_NAME for which the CODE_ID is required</param>
		/// <returns>Code ID</returns>
		public int GetCodeIDWithShort(string p_ShortCode, string p_sSysTableName)
		{
			int iCodeIDWithShort=0;			
			p_ShortCode=Regex.Replace(p_ShortCode,"'","''");
			p_sSysTableName=Regex.Replace(p_sSysTableName,"'","''");
			try
			{
				using(DbReader rdr=DbFactory.GetDbReader(m_sConnectString,"SELECT CODES.CODE_ID FROM CODES,GLOSSARY WHERE CODES.SHORT_CODE = '" + p_ShortCode +
						  "' AND GLOSSARY.SYSTEM_TABLE_NAME = '" +p_sSysTableName +
						  "' AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.DELETED_FLAG = 0"))
				{
					
					if (rdr != null)
						if(rdr.Read())
							iCodeIDWithShort=rdr.GetInt32("CODE_ID");
				}
			}
			catch(Exception e)
			{
				throw new DataModelException(Globalization.GetString("CCacheFunctions.GetCodeIDWithShort.DataError", m_iClientId),e);
			}
			return iCodeIDWithShort;
		}

		/// <summary>
		///Riskmaster.Settings.SysParms.GetStateCode returns the STATE_ID
		///for a particular STATE_ROW_ID in the STATES table.
		/// </summary>
		/// <param name="p_iState">STATE_ROW_ID for which the STATE_ID is required</param>
		/// <returns>State ID</returns>
		public string GetStateCode(int p_iState)
		{
			string sStateCode="";
			if(p_iState==0)
				return "";	
			try
			{
				using(DbReader rdr=DbFactory.GetDbReader(m_sConnectString,"SELECT STATE_ID FROM STATES WHERE STATE_ROW_ID = " + p_iState))
				{
					if (rdr != null)
						if(rdr.Read())
							sStateCode=rdr.GetString("STATE_ID");
				}
			}
			catch(Exception e)
			{
				throw new DataModelException(Globalization.GetString("CCacheFunctions.GetStateCode.DataError", m_iClientId),e);
			}
			return sStateCode;
		}	
		
		/// <summary>
		///Riskmaster.Settings.SysParms.GetOrgInfo returns the ABBREVIATION, LAST_NAME
		///for a particular ENTITY_ID in the ENTITY table.
		/// </summary>
		/// <param name="p_iEntityID">ENTITY_ID for which the ABBREVIATION, LAST_NAME is required</param>
		/// <param name="p_sAbbreviation">ABBREVIATION, reference parameter to store the value</param>
		/// <param name="p_sName">LAST_NAME, reference parameter to store the value</param>
		public void GetOrgInfo(int p_iEntityID, ref string p_sAbbreviation, ref string p_sName)
		{
			if(p_iEntityID==0)
			{
				p_sAbbreviation="";
				p_sName="";
				return;
			}
			try
			{
				using(DbReader rdr=DbFactory.GetDbReader(m_sConnectString,"SELECT ABBREVIATION,LAST_NAME FROM ENTITY WHERE ENTITY_ID = " + p_iEntityID))
				{
					if (rdr != null)
						if(rdr.Read())
						{
							p_sAbbreviation=rdr.GetString("ABBREVIATION");
							p_sName=rdr.GetString("LAST_NAME");
						}
				}
			}
			catch(Exception e)
			{
				throw new DataModelException(Globalization.GetString("CCacheFunctions.GetOrgInfo.DataError", m_iClientId),e);
			}
		}	

		/// <summary>        
		///Riskmaster.Settings.SysParms.GetCodeInfo returns the SHORT_CODE, CODE_DESC
		///for a particular CODE_ID in the CODES,CODES_TEXT table based on the langauge code.
		/// </summary>
        /// Created By: Amandeep Kaur 12/10/2012
		/// <param name="p_iCode">CODE_ID for which the SHORT_CODE, CODE_DESC is required</param>
		/// <param name="p_ShortCode">SHORT_CODE, reference parameter to store the value</param>
		/// <param name="p_sDesc">CODE_DESC, reference parameter to store the value</param>
        /// <param name="p_sLangCode">Language Code for which the codes desc is to be returned</param>
        public void GetCodeInfo(int p_iCode, ref string p_ShortCode, ref string p_sDesc, int p_iLangCode)
		{
            int iLangCode = 0;
			if(p_iCode==0)
			{
				p_ShortCode="";
				p_sDesc="";
				return;
			}
			try
			{
                if (p_iLangCode == 0)
                {
                    p_iLangCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]); 
                }
                iLangCode = Conversion.ConvertStrToInteger(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]); 
                using (DbReader rdr = DbFactory.GetDbReader(m_sConnectString, "SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT WHERE CODES.CODE_ID = " + p_iCode +
                            " AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = " + p_iLangCode))
                {
                    if (rdr != null)
                        if (rdr.Read())
                        {
                            p_ShortCode = rdr.GetString("SHORT_CODE");
                            p_sDesc = rdr.GetString("CODE_DESC");
                        }
                }
                if (string.IsNullOrEmpty(p_ShortCode) || string.IsNullOrEmpty(p_sDesc))
                {
                    using (DbReader rdr = DbFactory.GetDbReader(m_sConnectString, "SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT WHERE CODES.CODE_ID = " + p_iCode +
                                " AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = " + iLangCode))
                    {
                        if (rdr != null)
                            if (rdr.Read())
                            {
                                p_ShortCode = rdr.GetString("SHORT_CODE");
                                p_sDesc = rdr.GetString("CODE_DESC");
                            }
                    }
                }
			}
			catch(Exception e)
			{
				throw new DataModelException(Globalization.GetString("CCacheFunctions.GetCodeInfo.DataError", m_iClientId),e);
			}
		}

        /// <summary>
        ///Riskmaster.Settings.SysParms.GetCodeInfo returns the SHORT_CODE, CODE_DESC
        ///for a particular CODE_ID in the CODES,CODES_TEXT table.
        /// </summary>
        /// <param name="p_iCode">CODE_ID for which the SHORT_CODE, CODE_DESC is required</param>
        /// <param name="p_ShortCode">SHORT_CODE, reference parameter to store the value</param>
        /// <param name="p_sDesc">CODE_DESC, reference parameter to store the value</param>      
        public void GetCodeInfo(int p_iCode, ref string p_ShortCode, ref string p_sDesc)
        {
            if (p_iCode == 0)
            {
                p_ShortCode = "";
                p_sDesc = "";
                return;
            }
            try
            {
                using (DbReader rdr = DbFactory.GetDbReader(m_sConnectString, "SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT WHERE CODES.CODE_ID = " + p_iCode +
                          " AND CODES.CODE_ID = CODES_TEXT.CODE_ID"))
                {
                    if (rdr != null)
                        if (rdr.Read())
                        {
                            p_ShortCode = rdr.GetString("SHORT_CODE");
                            p_sDesc = rdr.GetString("CODE_DESC");
                        }
                }
            }
            catch (Exception e)
            {
                throw new DataModelException(Globalization.GetString("CCacheFunctions.GetCodeInfo.DataError", m_iClientId), e);
            }
        }	
		/// <summary>
		///Riskmaster.Settings.SysParms.GetStateInfo returns the STATE_ID,STATE_NAME
		///for a particular STATE_ROW_ID in the STATES table.
		/// </summary>
		/// <param name="p_iState">STATE_ROW_ID for which the SHORT_CODE, CODE_DESC is required</param>
		/// <param name="p_sAbbreviation">STATE_ID, reference parameter to store the value</param>
		/// <param name="p_sName">STATE_NAME, reference parameter to store the value</param>
		public void GetStateInfo(int p_iState, ref string p_sAbbreviation, ref string p_sName)
		{			
			if(p_iState==0)
			{
				p_sAbbreviation="";
				p_sName="";
				return;
			}		
			try
			{
				using(DbReader rdr=DbFactory.GetDbReader(m_sConnectString,"SELECT STATE_ID,STATE_NAME FROM STATES WHERE STATE_ROW_ID = " + p_iState))
				{
					if (rdr != null)
						if(rdr.Read())
						{
							p_sAbbreviation=rdr.GetString("STATE_ID");
							p_sName=rdr.GetString("STATE_NAME");
						}
				}
			}
			catch(Exception e)
			{
				throw new DataModelException(Globalization.GetString("CCacheFunctions.GetStateInfo.DataError", m_iClientId),e);
			}
		}	

		/// <summary>
		///Riskmaster.Settings.SysParms.GetTableName returns the SYSTEM_TABLE_NAME
		///for a particular TABLE_ID in the GLOSSARY table.
		/// </summary>
		/// <param name="p_iTableID">TABLE_ID for which the SYSTEM_TABLE_NAME is required</param>
		/// <returns>System table name</returns>
		public string GetTableName(int p_iTableID)
		{
			string sTableName="";
			if(p_iTableID==0)
				return "";
			try
			{
				using(DbReader rdr=DbFactory.GetDbReader(m_sConnectString,"SELECT SYSTEM_TABLE_NAME FROM GLOSSARY WHERE TABLE_ID = " + p_iTableID))
				{
					if (rdr != null)
						if(rdr.Read())				
							sTableName=rdr.GetString("SYSTEM_TABLE_NAME");	
				}
			}
			catch(Exception e)
			{
				throw new DataModelException(Globalization.GetString("CCacheFunctions.GetTableName.DataError", m_iClientId),e);
			}
			return sTableName;
				
		}

		/// <summary>
		///Riskmaster.Settings.SysParms.GetOrgParent returns the ENTITY_ID,LAST_NAME
		///in the ENTITY table.
		/// </summary>
		/// <param name="p_iDeptEID"></param>
		/// <param name="p_iTableIDLevel"></param>
		/// <param name="p_iLevel"></param>
		/// <param name="p_iReturnEID">LAST_NAME, reference parameter to store the value</param>
		/// <returns>Last name</returns>
		public string GetOrgParent(int p_iDeptEID,int p_iTableIDLevel,int p_iLevel,ref int p_iReturnEID)
		{
			StringBuilder sFrom=new StringBuilder("");
			StringBuilder sWhere=new StringBuilder("");
			StringBuilder sSQL = new StringBuilder("");
			string sOrgParent="";
			if(p_iTableIDLevel!=0)
			{
				sFrom.Append("ENTITY  E1012");
				sWhere.Append("E1012.ENTITY_ID = "+p_iDeptEID);				
				for(int i=1011;i>=p_iTableIDLevel;i--)
				{
					sFrom.Append(",ENTITY   E" + i);					
					sWhere.Append(" AND E" + i +".ENTITY_ID = E" +(i+1)+".PARENT_EID");					
				}
				
				sSQL.Append("SELECT E"+p_iTableIDLevel+".ENTITY_ID,E"+p_iTableIDLevel+".LAST_NAME FROM ");
				sSQL.Append(sFrom + " WHERE ");
				sSQL.Append(sWhere);						
			}
			else
			{
				sFrom.Append("ENTITY   E1012");
				sWhere.Append("E1012.ENTITY_ID = "+p_iDeptEID);				
				for(int i=1011;i>=(1012 - p_iLevel);i--)
				{
					sFrom.Append(",ENTITY   E" + i);					
					sWhere.Append(" AND E" + i +".ENTITY_ID = E" +(i+1)+".PARENT_EID");					
				}
				
				sSQL.Append("SELECT E"+(1012 - p_iLevel)+".ENTITY_ID,E"+(1012 - p_iLevel)+".LAST_NAME FROM ");
				sSQL.Append(sFrom + " WHERE ");
				sSQL.Append(sWhere);				
			}
			try
			{
				using(DbReader rdr=DbFactory.GetDbReader(m_sConnectString,sSQL.ToString()))
				{
					if (rdr != null)
					 if(rdr.Read())	
					 {
						 p_iReturnEID=rdr.GetInt32("ENTITY_ID");
						 sOrgParent=rdr.GetString("LAST_NAME");
					 }	
				}
			}
			catch(Exception e)
			{
				throw new DataModelException(Globalization.GetString("CCacheFunctions.GetOrgParent.DataError", m_iClientId),e);
			}
			return 	sOrgParent;	
		}

		/// <summary>
		///Riskmaster.Settings.SysParms.GetTables returns an array list containing 
		///TABLE_ID, SYSTEM_TABLE_NAME and TABLE_NAME for particular GLOSSARY_TYPE_CODE 
		///in the TABLES table.
		/// </summary>
		/// <param name="p_sGlossaryTypes">A comma separated list of GLOSSARY_TYPE_CODE values</param>
		/// <returns>Array list of an array list containing table information</returns>
		public ArrayList[] GetTables(string p_sGlossaryTypes)
		{
			ArrayList[] arrTables = new ArrayList[3]; 
			for(int i=0; i<arrTables.Length; i++) 			
				arrTables[i] = new ArrayList(); 
			try
			{
				using(DbReader rdr=DbFactory.GetDbReader(m_sConnectString,"SELECT GLOSSARY.TABLE_ID,GLOSSARY.SYSTEM_TABLE_NAME,GLOSSARY_TEXT.TABLE_NAME FROM GLOSSARY,GLOSSARY_TEXT WHERE GLOSSARY.GLOSSARY_TYPE_CODE IN (" + p_sGlossaryTypes + ") AND GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID ORDER BY GLOSSARY_TEXT.TABLE_NAME"))
				{
					if (rdr != null)
						if(rdr.Read())	
						{
							arrTables[0].Add(rdr.GetInt32("TABLE_ID"));
							arrTables[1].Add(rdr.GetString("SYSTEM_TABLE_NAME"));
							arrTables[2].Add(rdr.GetString("TABLE_NAME"));
						}
				}
			}
			catch(Exception e)
			{
				throw new DataModelException(Globalization.GetString("CCacheFunctions.GetTables.DataError", m_iClientId),e);
			}
			return arrTables;
		}		
	}
}
