/**********************************************************************************************
 *   Date     |  jira   | Programmer | Description                                            *
 **********************************************************************************************
 * 09/20/2014 | rma-857 | ngupta36  | Incurred Handling - Collection Allowed or Not Allowed
 **********************************************************************************************/
using System;
using System.Data;
using System.ComponentModel;
using System.Collections;
using System.Text;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Settings
{
	/// <summary>SYS_PARMS_LOB settings</summary>
	///<remarks>Collection consists of one LobSettings object for each line of business in SYS_PARMS_LOB.</remarks>
	public class ColLobSettings: IEnumerable 
	{
		private DataTable m_DataTable = null;  //fetch schema to check if field exists in table before reading from/writing to
		//key for each entry in m_col is LOB
		protected Hashtable m_col=null;
		/// <summary>The connection string used by the database layer for connection with the database.</summary>
		private string m_sConnectString;
        private int m_iClientId = 0;

		/// <summary>This constructor creates enumerable collection of all LOB settings in SYS_PARMS_LOB</summary>
		/// <value></value>
		/// <remarks>Key for each entry in collection is the LOB.</remarks>
		/// <example>ColLobSettings oCol = new ColLobSettings(connectionString);</example>
        public ColLobSettings(string p_sConnectString, int p_iClientId)
		{
			m_sConnectString=p_sConnectString;
            m_iClientId = p_iClientId;
			if(m_col==null)
				LoadSettings();

		}

		/// <summary>Returns a single LobSettings object.  The object contains the settings for a line of business.</summary>
		/// <value>LobSettings object</value>
		/// <remarks>Key is LOB</remarks>
		/// <example>LobSettings oLob = (LobSettings)oCol[241];</example>
		public LobSettings this[int Lob]
		{
			get{return m_col[Lob] as LobSettings;}
			set
			{
				if(!m_col.ContainsKey(Lob))
					m_col.Add(Lob,value);
				else
					m_col[Lob] = value;
			}
		}
		IEnumerator IEnumerable.GetEnumerator() 
		{
			if(m_col!=null)
				return ((IEnumerable)m_col.Values).GetEnumerator();
			return null;
		}

		/// <summary>Add a new LobSettings object to the collection of LOB settings</summary>
		/// <value></value>
		/// <remarks></remarks>
		/// <example></example>
		public void Add(LobSettings p_oLob)
		{
			//mark as new
			p_oLob.IsNew=true;

			//temporary key, user might not have set LineOfBusCode yet
			int iKey=0;
			while(m_col.ContainsKey(iKey))
			{
				iKey++;
			}
			m_col.Add(iKey,p_oLob);
		}

		/// <summary>Save changes in LOB settings to SYS_PARMS_LOB</summary>
		/// <returns>True if saving is successful.</returns>
		public bool SaveSettings()
		{
			DbWriter writer=null;
			DataRow[] rows=null;
			DbConnection objConn =null;
			DbTransaction objTrans =null;

			foreach (LobSettings oLob in this)
			{
				if(	oLob.DataChanged)
				{
					if(oLob.LineOfBusCode==0)
						throw new DataModelException(Globalization.GetString("ColLobSettings.SaveSettings.LOBMissing", m_iClientId));
					try
					{
						if(objConn==null)
						{
							objConn = DbFactory.GetDbConnection(this.m_sConnectString);
							objConn.Open();
							objTrans = objConn.BeginTransaction();
							writer=DbFactory.GetDbWriter(objConn);
						}
						else
						{
							objTrans = objConn.BeginTransaction();
							writer.Reset(true);   //keep connection open
						}

						writer.Tables.Add("SYS_PARMS_LOB");
						if(!oLob.IsNew)
							writer.Where.Add("LINE_OF_BUS_CODE="+oLob.LineOfBusCode);

						writer.Fields.Add("LINE_OF_BUS_CODE",oLob.LineOfBusCode);
						writer.Fields.Add("CLM_ACC_LMT_FLAG",Convert.ToInt16(oLob.ClmAccLmtFlag));
						writer.Fields.Add("DUPLICATE_FLAG",Convert.ToInt16(oLob.DuplicateFlag));
						writer.Fields.Add("INC_YEAR_FLAG",Convert.ToInt16(oLob.IncYearFlag));
						writer.Fields.Add("INSUF_FUNDS_FLAG",Convert.ToInt16(oLob.InsufFundsFlag));
                        //Shruti for EMI changes
                        writer.Fields.Add("USE_AUTOCHK_RESERVE_FLAG", Convert.ToInt16(oLob.InsufFundsAutoFlag));
						writer.Fields.Add("PAY_DET_LMT_FLAG",Convert.ToInt16(oLob.PayDetLmtFlag));
						writer.Fields.Add("PAY_LMT_FLAG",Convert.ToInt16(oLob.PayLmtFlag));
						writer.Fields.Add("PRT_CHK_LMT_FLAG",Convert.ToInt16(oLob.PrtChkLmtFlag));
						writer.Fields.Add("RES_LMT_FLAG",Convert.ToInt16(oLob.ResLmtFlag));
                        writer.Fields.Add("CLM_INC_LMT_FLAG", Convert.ToInt16(oLob.ClaimIncLmtFlag));//Jira 6385
						writer.Fields.Add("RESERVE_TRACKING",oLob.ReserveTracking);
                        writer.Fields.Add("USE_LIMIT_TRACKING", oLob.UseLimitTracking);
						writer.Fields.Add("ADJ_RSV",Convert.ToInt16(oLob.AdjRsv));
						writer.Fields.Add("COLL_IN_RSV_BAL",Convert.ToInt16(oLob.CollInRsvBal));
                        writer.Fields.Add("PER_RSV_COLL_IN_RSV_BAL", Convert.ToInt16(oLob.PerRsvCollInRsvBal));//asharma326 JIRA 870
                        writer.Fields.Add("PER_RSV_COLL_IN_INCR_BAL", Convert.ToInt16(oLob.PerRsvCollInIncurred));//asharma326 JIRA 870
                        //Shruti for MITS 8551
                        writer.Fields.Add("COLL_IN_INCUR_BAL", Convert.ToInt16(oLob.CollInIncurred));
						writer.Fields.Add("BAL_TO_ZERO",Convert.ToInt16(oLob.BalToZero));
						writer.Fields.Add("NEG_BAL_TO_ZERO",Convert.ToInt16(oLob.NegBalToZero));
						writer.Fields.Add("SET_TO_ZERO",Convert.ToInt16(oLob.SetToZero));
						writer.Fields.Add("EVENT_TRIGGER",Convert.ToInt16(oLob.EventTrigger));
						writer.Fields.Add("INC_CLM_TYPE_FLAG",Convert.ToInt16(oLob.IncClmTypeFlag));
						writer.Fields.Add("INC_LOB_CODE_FLAG",Convert.ToInt16(oLob.IncLobCodeFlag));
						writer.Fields.Add("INC_ORG_FLAG",Convert.ToInt16(oLob.IncOrgFlag));
						writer.Fields.Add("ORG_TABLE_ID",oLob.OrgTableId);
						writer.Fields.Add("DURATION_CODE",oLob.DurationCode);
						writer.Fields.Add("EST_NUM_OF_CLAIMS",oLob.EstNumOfClaims);

						// Mihika 6-Jan-2006 Defect no. 1189
						writer.Fields.Add("DUP_PAY_CRITERIA", oLob.DupPayCriteria);
						writer.Fields.Add("DUP_NUM_DAYS", oLob.DupNumDays);
						// End - Mihika
						//Start Parag 17-Jan-2006 
						writer.Fields.Add("PER_CL_LMT_FLAG", oLob.PerClaimLmtFlag);
						//End Parag 17-Jan-2006 
						rows = m_DataTable.Select("ColumnName='USE_FISCAL_YEAR'");
						if(rows.GetLength(0)==1)
							writer.Fields.Add("USE_FISCAL_YEAR",Convert.ToInt16(oLob.UseFiscalYear));

						writer.Fields.Add("ADJUST_FUNDS_FLAG",Convert.ToInt16(oLob.AdjFundsFlag));
						writer.Fields.Add("CAPTION_LEVEL",oLob.CaptionLevel);
						writer.Fields.Add("NONEG_RES_ADJ_FLAG",oLob.NoNegResAdjFlag);						
						writer.Fields.Add("RESTRICT_COLL_PER_RSV_TYPE", Convert.ToInt32(oLob.PreventCollectionPerReserve)); // JIRA 857
                        writer.Fields.Add("RESTRICT_COLL_ALL_RSV_TYPE", Convert.ToInt32(oLob.PreventCollectionAllReserve)); // JIRA 857
                        writer.Fields.Add("ADD_RCV_RSV_TO_BAL_AMT", Convert.ToInt16(oLob.AddRecoveryReservetoTotalBalanceAmount)); //asharma326 JIRA 871
                        writer.Fields.Add("ADD_RCV_RSV_TO_INC_AMT", Convert.ToInt16(oLob.AddRecoveryReservetoTotalIncurredAmount)); //asharma326 JIRA 872
						writer.Fields.Add("RES_BY_CLM_TYPE",Convert.ToInt16(oLob.ResByClmType));
						rows = m_DataTable.Select("ColumnName='ADJ_RSV_CLAIM'");
						if(rows.GetLength(0)==1)
							writer.Fields.Add("ADJ_RSV_CLAIM",Convert.ToInt16(oLob.AdjRsvPerClaim));
						writer.Fields.Add("USE_ADJTEXT",Convert.ToInt16(oLob.UseAdjText));
						writer.Execute(objTrans);

						foreach(LobSettings.CReserve oReserve in (LobSettings.CColReserves)oLob.Reserves)
						{
							if(oReserve.DataChanged)
							{
								string sTable="";
								//In case new reserve info is being added and table name has not
								//been provided, then specify the default tables
								if(oReserve.TableName.Length<=0 && oReserve.ClaimTypeCode==0)
									sTable="SYS_LOB_RESERVES";
								else if(oReserve.TableName.Length<=0 && oReserve.ClaimTypeCode!=0)
									sTable="SYS_CLM_TYPE_RES";
								else
                                    sTable = oReserve.TableName;
								writer.Reset(true);    //keep connection open
								writer.Tables.Add(sTable);
								if(!oReserve.IsNew)
								{
									writer.Where.Add("LINE_OF_BUS_CODE="+oLob.LineOfBusCode);
									writer.Where.Add("RESERVE_TYPE_CODE="+oReserve.ReserveTypeCode);
									if(sTable=="SYS_CLM_TYPE_RES")
										writer.Where.Add("CLAIM_TYPE_CODE="+oReserve.ClaimTypeCode);
								}									
								writer.Fields.Add("LINE_OF_BUS_CODE",Convert.ToInt32(oLob.LineOfBusCode));
								writer.Fields.Add("RESERVE_TYPE_CODE",Convert.ToInt32(oReserve.ReserveTypeCode));
								if(sTable=="SYS_CLM_TYPE_RES")
									writer.Fields.Add("CLAIM_TYPE_CODE",Convert.ToInt32(oReserve.ClaimTypeCode));
								writer.Execute(objTrans);
							}
						}

						if (oLob.AdjRsvPerClaim)
						{
							foreach(LobSettings.CReserve oReserve in (LobSettings.CColReserves)oLob.AdjNegResTypesPerRsv)
							{
								if(oReserve.DataChanged)
								{
									string sTable="";
									writer.Reset(true);    //keep connection open
									writer.Tables.Add("SYS_RES_AUTOADJ");
									if(!oReserve.IsNew)
									{
										writer.Where.Add("LINE_OF_BUS_CODE="+oLob.LineOfBusCode);
										writer.Where.Add("RES_TYPE_CODE="+oReserve.ReserveTypeCode);
									}									
									writer.Fields.Add("LINE_OF_BUS_CODE",Convert.ToInt32(oLob.LineOfBusCode));
									writer.Fields.Add("RES_TYPE_CODE",Convert.ToInt32(oReserve.ReserveTypeCode));
									writer.Execute(objTrans);
								}
							}
						}

						objTrans.Commit();
						oLob.DataChanged=false;
					}
					catch(Exception e)
					{
						throw new DataModelException(Globalization.GetString("ColLobSettings.SaveSettings.DataError", m_iClientId),e);
					}
				}
			}

			return true;
		}

		/// <summary>Load LOB settings from SYS_PARMS_LOB</summary>
		/// <returns>True if loading is successful.</returns>
		internal bool LoadSettings()
		{
			m_col = new Hashtable();
            string sLOB = string.Empty;
            StringBuilder sbLOBs = new StringBuilder();
            StringBuilder sbResByClmTypeLOBs = new StringBuilder();
            StringBuilder sbAdjRsvPerClaimLOBs = new StringBuilder();

			try
			{
				using(DbReader rdr=DbFactory.GetDbReader(m_sConnectString,"SELECT * FROM SYS_PARMS_LOB"))
                {
				    m_DataTable=rdr.GetSchemaTable();
				    DataRow[] rows=null;

					while(rdr.Read())
					{
						LobSettings oLob = new LobSettings();
                        sLOB = rdr.GetInt32("LINE_OF_BUS_CODE").ToString();
                        if (sbLOBs.Length > 0)
                            sbLOBs.Append(",");
                        sbLOBs.Append(sLOB);

						oLob.LineOfBusCode=rdr.GetInt32("LINE_OF_BUS_CODE");
						oLob.ClmAccLmtFlag=Convert.ToBoolean(rdr.GetInt16("CLM_ACC_LMT_FLAG"));
                        //Asharma326 MITS 30874 Starts
                        oLob.ClmAccLmtUsrFlag = Convert.ToBoolean(rdr.GetInt16("CLM_ACC_LMT_USR_FLAG"));
                        //Asharma326 MITS 30874 Ends
						oLob.DuplicateFlag=Convert.ToBoolean(rdr.GetInt16("DUPLICATE_FLAG"));
						oLob.IncYearFlag=Convert.ToBoolean(rdr.GetInt16("INC_YEAR_FLAG"));
						oLob.InsufFundsFlag=Convert.ToBoolean(rdr.GetInt16("INSUF_FUNDS_FLAG"));
                        //Shruti for EMI changes
                        oLob.InsufFundsAutoFlag = Convert.ToBoolean(rdr.GetInt16("USE_AUTOCHK_RESERVE_FLAG"));
						oLob.PayDetLmtFlag=Convert.ToBoolean(rdr.GetInt16("PAY_DET_LMT_FLAG"));
						oLob.PayLmtFlag=Convert.ToBoolean(rdr.GetInt16("PAY_LMT_FLAG"));
						oLob.PrtChkLmtFlag=Convert.ToBoolean(rdr.GetInt16("PRT_CHK_LMT_FLAG"));
						oLob.ResLmtFlag=Convert.ToBoolean(rdr.GetInt16("RES_LMT_FLAG"));
                        oLob.ClaimIncLmtFlag = Convert.ToBoolean(rdr.GetInt16("CLM_INC_LMT_FLAG"));//Jira 6385
						oLob.ReserveTracking=rdr.GetInt32("RESERVE_TRACKING");
						oLob.AdjRsv=Convert.ToBoolean(rdr.GetInt16("ADJ_RSV"));
						oLob.CollInRsvBal=Convert.ToBoolean(rdr.GetInt16("COLL_IN_RSV_BAL"));
                        //Shruti for MITS 8551
                        oLob.CollInIncurred = Convert.ToBoolean(rdr.GetInt16("COLL_IN_INCUR_BAL"));
                        oLob.PerRsvCollInRsvBal = Convert.ToBoolean(rdr.GetInt16("PER_RSV_COLL_IN_RSV_BAL"));//asharma326 JIRA 870
                        oLob.PerRsvCollInIncurred = Convert.ToBoolean(rdr.GetInt16("PER_RSV_COLL_IN_INCR_BAL"));//asharma326 JIRA 870
                        oLob.UseLimitTracking = Convert.ToBoolean(rdr.GetInt16("USE_LIMIT_TRACKING"));
						oLob.BalToZero=Convert.ToBoolean(rdr.GetInt16("BAL_TO_ZERO"));
						oLob.NegBalToZero=Convert.ToBoolean(rdr.GetInt16("NEG_BAL_TO_ZERO"));
						oLob.SetToZero=Convert.ToBoolean(rdr.GetInt16("SET_TO_ZERO"));
						oLob.EventTrigger=Convert.ToBoolean(rdr.GetInt16("EVENT_TRIGGER"));
						oLob.IncClmTypeFlag=Convert.ToBoolean(rdr.GetInt16("INC_CLM_TYPE_FLAG"));
						oLob.IncLobCodeFlag=Convert.ToBoolean(rdr.GetInt16("INC_LOB_CODE_FLAG"));
						oLob.IncOrgFlag=Convert.ToBoolean(rdr.GetInt16("INC_ORG_FLAG"));
						oLob.OrgTableId=rdr.GetInt32("ORG_TABLE_ID");
						oLob.DurationCode=rdr.GetInt32("DURATION_CODE");
						oLob.EstNumOfClaims=rdr.GetInt32("EST_NUM_OF_CLAIMS");
						//Start Parag 17-Jan-2006 
						oLob.PerClaimLmtFlag=Convert.ToBoolean(rdr.GetInt16("PER_CL_LMT_FLAG"));
						//End Parag 17-Jan-2006 
						// Mihika 6-Jan-2006 Defect no. 1189
						oLob.DupPayCriteria = rdr.GetInt32("DUP_PAY_CRITERIA");
						oLob.DupNumDays = rdr.GetInt32("DUP_NUM_DAYS");
						// End - Mihika

						rows = m_DataTable.Select("ColumnName='USE_FISCAL_YEAR'");
						if(rows.GetLength(0)==1)
							oLob.UseFiscalYear=Convert.ToBoolean(rdr.GetInt16("USE_FISCAL_YEAR"));
					
						rows = m_DataTable.Select("ColumnName='ADJUST_FUNDS_FLAG'");
						if(rows.GetLength(0)==1)
							oLob.AdjFundsFlag=Convert.ToBoolean(rdr.GetInt32("ADJUST_FUNDS_FLAG"));
						
						oLob.CaptionLevel=rdr.GetInt32("CAPTION_LEVEL");
						oLob.NoNegResAdjFlag=rdr.GetInt32("NONEG_RES_ADJ_FLAG");						
						oLob.PreventCollectionPerReserve = rdr.GetInt32("RESTRICT_COLL_PER_RSV_TYPE");//JIRA 857
                        oLob.PreventCollectionAllReserve = rdr.GetInt32("RESTRICT_COLL_ALL_RSV_TYPE");//JIRA 857
                        oLob.AddRecoveryReservetoTotalBalanceAmount = rdr.GetInt16("ADD_RCV_RSV_TO_BAL_AMT");//asharma326 JIRA 871
                        oLob.AddRecoveryReservetoTotalIncurredAmount = rdr.GetInt16("ADD_RCV_RSV_TO_INC_AMT");//asharma326 JIRA 872
						
                        oLob.ResByClmType=Convert.ToBoolean(rdr.GetInt16("RES_BY_CLM_TYPE"));
                        //Get list of LOB which has Reserve By Claim Type flag is set. 
                        //The list will be use to retrive reserver types
                        if (oLob.ResByClmType)
                        {
                            if (sbResByClmTypeLOBs.Length > 0)
                                sbResByClmTypeLOBs.Append(",");
                            sbResByClmTypeLOBs.Append(sLOB);
                        }

						oLob.UseAdjText=Convert.ToBoolean(rdr.GetInt16("USE_ADJTEXT"));
                        //rsushilaggar : ISO Claim Search
                        oLob.ClmProgressNotesFlag = rdr.GetInt32("USE_CLAIM_PROGRESS_NOTES");
                        oLob.ClmCommentsFlag = rdr.GetInt32("USE_CLAIM_COMMENTS");
                        oLob.ClmISOSubmissionFlag = rdr.GetInt32("USE_ISO_SUBMISSION");
                        //end - rsushilaggar : ISO Claim Search
                        //start:added by nitin goel MITS 30910,01/23/2013
                        oLob.ApplyDedToPaymentsFlag = rdr.GetInt32("ALLOW_TO_APPLY_DED");
                        oLob.DedRecTransType = rdr.GetInt32("DED_REC_TRANS_TYPE");
                        oLob.DedRecReserveType = rdr.GetInt32("DED_REC_RESERVE_TYPE");
                        //tanwar2 - mits 30910 - start
                        oLob.DimPercentNum = rdr.GetDouble("DIM_PERCENT_PERYR_NUM");
                        oLob.PrvntPrntZeroChecks = rdr.GetBoolean("PREVENT_ZEROCHECK_FLAG");
                        //tanwar2 - mits 30910 - end
                        //start:ddhiman ,10/16/2014      
                        oLob.SharedAggDedFlag = rdr.GetInt32("SHARED_AGG_DED_TP_FLAG");
                        oLob.DimnishingFlag = rdr.GetInt32("DIM_DED_FP_FLAG");
                        //End ddhiman

                        //start:Nikhil ,10/16/2014      
                        oLob.EnableDistributionDedAmtOnVoid = rdr.GetInt32("DIS_DED_AMT_SUB_PMT_TP_FLAG");
                        oLob.DedRecoveryIdentifierChar = rdr.GetString("DED_RECOVERY_IDENTIFIER_CHAR");
                        oLob.ManualDedRecoveryIdentifierChar = rdr.GetString("MAN_DED_RECOVERY_IDENTIFIER");
                        //End ddhiman
						rows = m_DataTable.Select("ColumnName='ADJ_RSV_CLAIM'");
                        if (rows.GetLength(0) == 1)
                        {
                            oLob.AdjRsvPerClaim = Convert.ToBoolean(rdr.GetInt16("ADJ_RSV_CLAIM"));
                            if (oLob.AdjRsvPerClaim)
                            {
                                if (sbAdjRsvPerClaimLOBs.Length > 0)
                                    sbAdjRsvPerClaimLOBs.Append(",");
                                sbAdjRsvPerClaimLOBs.Append(sLOB);
                            }
                        }

						//smahajan6 - MITS #18230 - 12/14/2009 - Start
                        oLob.UseEnhPolFlag = rdr.GetInt32("USE_ENH_POL_FLAG");
                        //MGaba2: R8: SuperVisory Approval
                        oLob.UseReserveWorkSheet =  Convert.ToBoolean(rdr.GetInt32("USE_RSV_WK"));
                        oLob.IsPolicyFilter = Convert.ToBoolean(rdr.GetInt16("IS_POLICY_FILTER"));
						//smahajan6 - MITS #18230 - 12/14/2009 - End
						oLob.DataChanged=false;

						m_col.Add(rdr.GetInt32("LINE_OF_BUS_CODE"), oLob);
					}
				}

                using (DbReader rdr = DbFactory.GetDbReader(m_sConnectString, "SELECT LINE_OF_BUS_CODE, RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE IN (" + sbLOBs.ToString() + ")"))
                {
                    while (rdr.Read())
                    {
                        LobSettings.CReserve oReserve = new LobSettings.CReserve();
                        oReserve.ReserveTypeCode = rdr.GetInt32("RESERVE_TYPE_CODE");
                        oReserve.ClaimTypeCode = 0;
                        oReserve.TableName = "RESERVE_TYPE_CODE";
                        oReserve.DataChanged = false;

                        int iLOB = rdr.GetInt32("LINE_OF_BUS_CODE");
                        this[iLOB].Reserves.AddExisting(oReserve);
                    }
                }

                if (sbResByClmTypeLOBs.Length > 0)
				{
                    //rsushilaggar MITS 24370 Date 03/16/2011
                    using (DbReader rdr = DbFactory.GetDbReader(m_sConnectString, "SELECT RESERVE_TYPE_CODE, CLAIM_TYPE_CODE,LINE_OF_BUS_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE IN (" + sbResByClmTypeLOBs.ToString() + ")"))
                    {
						while(rdr.Read())
						{
							LobSettings.CReserve oReserve = new LobSettings.CReserve();
							oReserve.ReserveTypeCode=rdr.GetInt32("RESERVE_TYPE_CODE");
							oReserve.ClaimTypeCode=rdr.GetInt32("CLAIM_TYPE_CODE");															
							oReserve.DataChanged=false;
							oReserve.TableName="SYS_CLM_TYPE_RES";

                            int iLOB = rdr.GetInt32("LINE_OF_BUS_CODE");
							this[iLOB].Reserves.AddExisting(oReserve);
						}
					}
				}

                if (sbAdjRsvPerClaimLOBs.Length > 0)
				{
                    //rsushilaggar MITS 24370 Date 03/16/2011
                    using (DbReader rdr = DbFactory.GetDbReader(m_sConnectString, "SELECT RES_TYPE_CODE,LINE_OF_BUS_CODE FROM SYS_RES_AUTOADJ WHERE LINE_OF_BUS_CODE IN (" + sbAdjRsvPerClaimLOBs.ToString() + ")"))
                    {
						while(rdr.Read())
						{
							LobSettings.CReserve oReserve = new LobSettings.CReserve();
							oReserve.ReserveTypeCode=rdr.GetInt32("RES_TYPE_CODE");
							oReserve.ClaimTypeCode=0;							
							oReserve.DataChanged=false;
							oReserve.TableName="SYS_RES_AUTOADJ";

                            int iLOB = rdr.GetInt32("LINE_OF_BUS_CODE");
                            this[iLOB].AdjNegResTypesPerRsv.AddExisting(oReserve);
						}
					}						
				}

                //Set DataChnaged flag to false
                foreach (LobSettings oLob in this)
                {
                    oLob.DataChanged = false;
                }
			}
			catch(Exception ex)
			{
                throw new DataModelException(Globalization.GetString("ColLobSettings.LoadSettings.DataError", m_iClientId), ex);
			}
			return true;
		}

	}

	/// <summary>Contains Line of Business settings for a single Line of Business</summary>
	public class LobSettings
	{
		public LobSettings()
		{
			//
			// constructor
			//
		}

		/// <summary>Collection of reserve settings for a Line of Business, returns an enumerable hashtable.</summary>
		private CColReserves m_ColReserves=null;

		/// <summary>Collection of reserves for which auto adjust should occur. Only used if AdjRsvPerClaim is true.</summary>
		private CColReserves m_ColAdjNegResTypesPerRsv=null;

		/// <summary>
		///Property to access m_ColReserves.
		/// </summary>
		public CColReserves Reserves
		{
			get
			{
				if(m_ColReserves==null)
					m_ColReserves=new CColReserves();
				return m_ColReserves;
			}
		}
		/// <summary>
		///Collection of reserves for which auto adjust should occur. Only used if AdjRsvPerClaim is true.
		/// </summary>
		public CColReserves AdjNegResTypesPerRsv
		{
			get
			{
				if(m_ColAdjNegResTypesPerRsv==null)
					m_ColAdjNegResTypesPerRsv=new CColReserves();
				return m_ColAdjNegResTypesPerRsv;
			}
		}


		public class CColReserves: IEnumerable
		{
			protected Hashtable col=new Hashtable(); 
			
			public bool ContainsKey(string sKey)
			{
				return col.ContainsKey(sKey);
			}

			public object this[string sKey]
			{
				get{return col[sKey];}
				set
				{
					if(!col.ContainsKey(sKey))
						col.Add(sKey,value);
					else
						col[sKey] = value;
				}
			}
			/// <summary>
			/// This method sets the .IsNew property "true".
			/// </summary>
			public void AddNew(CReserve p_oReserve)
			{
				//mark as new
				p_oReserve.IsNew=true;
				string sKey=p_oReserve.ReserveTypeCode.ToString()+p_oReserve.ClaimTypeCode.ToString();
				col.Add(sKey,p_oReserve);
			}
			/// <summary>Public method marks as new.  this is for filling in LoadSettings()</summary>
			internal void AddExisting(CReserve p_oReserve)
			{
				string sKey=p_oReserve.ReserveTypeCode.ToString()+p_oReserve.ClaimTypeCode.ToString();
				col.Add(sKey,p_oReserve);
			}
			IEnumerator IEnumerable.GetEnumerator() 
			{
				if(col!=null)
					return ((IEnumerable)col.Values).GetEnumerator();
				return null;
			}
		}

		/// <summary>
		/// Each reserve setting is a line of business + reserve type code + claim type code (claim type code only if Reserve By Claim Type is true).
		/// </summary>
		public class CReserve
		{
			/// <summary>Reserve type code.</summary>
			private int m_iReserveTypeCode;
			/// <summary>Claim type code.</summary>
			private int m_iClaimTypeCode;
			/// <summary>Stores whether the data has been modified or not.</summary>
			private bool m_bDataChanged;
			/// <summary>Stores the table name from which the data is extracted.</summary>
			private string m_sTableName="";
			/// <summary>Stores whether a new row is to be entered or an existing one modified.</summary>
			private bool m_IsNew;

			/// <summary>
			///Property to access m_iReserveTypeCode.
			/// </summary>
			public int ReserveTypeCode
			{
				get{return m_iReserveTypeCode;}
				set
				{
					if(m_iReserveTypeCode!=value)
					{
						m_bDataChanged=true; 
						m_iReserveTypeCode=value;
					}
				}
			}

			/// <summary>
			///Property to access m_sTableName.
			/// </summary>
			public string TableName
			{
				get{return m_sTableName;}
				set
				{
					if(m_sTableName!=value)
					{
						m_bDataChanged=true; 
						m_sTableName=value;
					}
				}
			}
			
			/// <summary>
			///Property to access m_iClaimTypeCode.
			/// </summary>
			public int ClaimTypeCode
			{
				get{return m_iClaimTypeCode;}
				set
				{
					if(m_iClaimTypeCode!=value)
					{
						m_bDataChanged=true; 
						m_iClaimTypeCode=value;
					}
				}
			}
			
			/// <summary>
			///Property to access m_bDataChanged.
			/// </summary>
			public bool DataChanged
			{
				get{return m_bDataChanged;}
				set{m_bDataChanged=value;}
			}
			
			/// <summary>
			///Property to access m_IsNew.
			/// </summary>
			internal bool IsNew
			{
				get{return m_IsNew;}
				set
				{
					if(m_IsNew!=value)
					{
						m_bDataChanged=true; 
						m_IsNew=value;
					}
				}
			}
		}

		//rest are simple properties of the Line of Business object
		private bool m_bClmAccLmtFlag;
        //Asharma326 MITS 30874 Starts
        private bool m_bClmAccLmtUsrFlag;
        //Asharma326 MITS 30874 Ends
        
		private bool m_bDuplicateFlag;
		private bool m_bIncYearFlag;
		private bool m_bInsufFundsFlag;
        //Shruti for EMI changes
        private bool m_bInsufFundsAutoFlag;
		private bool m_bPayDetLmtFlag;
		//Start Parag 17-Jan-2006 
		private bool m_bPerClaimLmtFlag;
		//End Parag 17-Jan-2006 
		private bool m_bPayLmtFlag;
		private bool m_bPrtChkLmtFlag;
		private bool m_bResLmtFlag;
        private bool m_bClaimIncLmtFlag;//Jira 6385
		private int m_iReserveTracking;
		private int m_iLineOfBusCode;
		private bool m_bAdjRsv;
		private bool m_bCollInRsvBal;
        //Shruti for MITS 8551
        private bool m_bCollInIncurred;
		private bool m_bBalToZero;
		private bool m_bNegBalToZero;
		private bool m_bSetToZero;
		private bool m_bEventTrigger;
		private bool m_bIncClmTypeFlag;
		private bool m_bIncLobCodeFlag;
		private bool m_bIncOrgFlag;
		private int m_iOrgTableId;
		private int m_iDurationCode;
		private int m_iEstNumOfClaims;
		private bool m_bUseFiscalYear;
		private bool m_bAdjFundsFlag;
		private int m_iCaptionLevel;
		private int m_iPreventCollectionPerReserve; // JIRA 857
        private int m_iPreventCollectionAllReserve; // JIRA 857
        private int m_iAddRecoveryReservetoTotalBalanceAmount;//asharma326 JIRA 871
        private int m_iAddRecoveryReservetoTotalIncurredAmount;//asharma326 JIRA 872
		private int m_iNoNegResAdjFlag;
		private bool m_bResByClmType;
		private bool m_bAdjRsvPerClaim;
		private bool m_bUseAdjText;
		// Mihika 6-Jan-2006 Defect no. 1189
		private int m_iDupPayCriteria;
		private int m_iDupNumDays;
		// End- Mihika

		/// <summary>Stores whether the data has been modified or not.</summary>
		private bool m_bDataChanged;
		/// <summary>Stores whether a new row is to be entered or an existing one modified.</summary>
		private bool m_IsNew;
        //rsushilaggar : ISO Claim Search
        private int m_iClmProgressNotesFlag;
        private int m_iClmCommentsFlag;
        private int m_iClmISOSubmissionFlag;
        //End: rsushilaggar : ISO Claim Search

		//smahajan6 - MITS #18230 - 12/14/2009 : Start
        private int m_iUseEnhPolFlag;
        /// <summary>
        ///Property to access m_iUseEnhPolFlag.
        /// </summary>
        //nitin goel- mits 30910 - NIS - start
        private int m_iApplyDedToPayments;
        private int m_iDedRecTransType;
        private int m_iDedRecReserveType;
        //tanwar2 - mits 30910 - start
        private double m_dDimPercentNum;
        private bool m_bPrvntPrntZeroChecks;
        //tanwar2 - mits 30910 - end
        //ddhiman 10/16/2014
        private int m_iSharedAggDedFlag;
        private int m_iDimnishingFlag;
        //End ddhiman
        //Start - Nikhil
        private int m_iEnableDistributionDedAmtOnVoid;
        private string m_sDedRecoveryIdentifierChar;
        private string m_sManualDedRecoveryIdentifierChar;
        //End - Nikhil
        public int UseEnhPolFlag
        {
            get { return m_iUseEnhPolFlag; }
            set
            {
                if (m_iUseEnhPolFlag != value)
                {
                    m_bDataChanged = true;
                    m_iUseEnhPolFlag = value;
                }
            }
        }
        
        private bool m_bIsPolicyFilter;
        /// <summary>
        ///Property to access m_bIsPolicyFilter.
        /// </summary>
        public bool IsPolicyFilter
        {
            get { return m_bIsPolicyFilter; }
            set
            {
                if (m_bIsPolicyFilter != value)
                {
                    m_bDataChanged = true;
                    m_bIsPolicyFilter = value;
                }
            }
        }
        //smahajan6 - MITS #18230 - 12/14/2009 : End


        /// <summary>
        /// MGaba2: R8: SuperVisory Approval 
        /// </summary>
        private bool m_bUseReserveWorkSheet;
        /// <summary>
        ///Property to check whether Reserve Worksheet is ON for LOB or not
        /// </summary>
        public bool UseReserveWorkSheet
        {
            get { return m_bUseReserveWorkSheet; }
            set
            {
                if (m_bUseReserveWorkSheet != value)
                {
                    m_bDataChanged = true;
                    m_bUseReserveWorkSheet = value;
                }
            }
        }


		/// <summary>
		///Property to access m_bClmAccLmtFlag.
		/// </summary>
		public bool ClmAccLmtFlag
		{
			get{return m_bClmAccLmtFlag;}
			set
			{
				if(m_bClmAccLmtFlag!=value)
				{
					m_bDataChanged=true; 
					m_bClmAccLmtFlag=value;
				}
			}
		}
        /// <summary>
        ///Property to access m_bClmAccLmtUsrFlag.
        /// </summary>
        //Asharma326 MITS 30874 Starts
        public bool ClmAccLmtUsrFlag
        {
            get { return m_bClmAccLmtUsrFlag; }
            set
            {
                if (m_bClmAccLmtUsrFlag != value)
                {
                    m_bDataChanged = true;
                    m_bClmAccLmtUsrFlag = value;
                }
            }
        }
        
        //Asharma326 MITS 30874 Ends
		
		/// <summary>
		///Property to access m_bDuplicateFlag.
		/// </summary>
		public bool DuplicateFlag
		{
			get{return m_bDuplicateFlag;}
			set
			{
				if(m_bDuplicateFlag!=value)
				{
					m_bDataChanged=true; 
					m_bDuplicateFlag=value;
				}
			}
		}
        private bool m_bUseLimitTracking = false;
        public bool UseLimitTracking
        {
            get { return m_bUseLimitTracking; }
            set
            {
                if (m_bUseLimitTracking != value)
                {
                    m_bDataChanged = true;
                    m_bUseLimitTracking = value;
                }
            }
        }

       
		
		/// <summary>
		///Property to access m_bIncYearFlag.
		/// </summary>
		public bool IncYearFlag
		{
			get{return m_bIncYearFlag;}
			set
			{
				if(m_bIncYearFlag!=value)
				{
					m_bDataChanged=true; 
					m_bIncYearFlag=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_bInsufFundsFlag.
		/// </summary>
		public bool InsufFundsFlag
		{
			get{return m_bInsufFundsFlag;}
			set
			{
				if(m_bInsufFundsFlag!=value)
				{
					m_bDataChanged=true; 
					m_bInsufFundsFlag=value;
				}
			}
		}

        /// <summary>
        ///Property to access m_bInsufFundsAutoFlag.
        /// </summary>
        public bool InsufFundsAutoFlag
        {
            get { return m_bInsufFundsAutoFlag; }
            set
            {
                if (m_bInsufFundsAutoFlag != value)
                {
                    m_bDataChanged = true;
                    m_bInsufFundsAutoFlag = value;
                }
            }
        }
		
		/// <summary>
		///Property to access m_bPayDetLmtFlag.
		/// </summary>
		public bool PayDetLmtFlag
		{
			get{return m_bPayDetLmtFlag;}
			set
			{
				if(m_bPayDetLmtFlag!=value)
				{
					m_bDataChanged=true; 
					m_bPayDetLmtFlag=value;
				}
			}
		}
		//Start Parag 17-Jan-2006 
		/// <summary>
		///Property to access m_bPayDetLmtFlag.
		/// </summary>
		public bool PerClaimLmtFlag
		{
			get{return m_bPerClaimLmtFlag;}
			set
			{
				if(m_bPerClaimLmtFlag!=value)
				{
					m_bDataChanged=true; 
					m_bPerClaimLmtFlag=value;
				}
			}
		}
		//End Parag 17-Jan-2006 
		/// <summary>
		///Property to access m_bPayLmtFlag.
		/// </summary>
		public bool PayLmtFlag
		{
			get{return m_bPayLmtFlag;}
			set
			{
				if(m_bPayLmtFlag!=value)
				{
					m_bDataChanged=true; 
					m_bPayLmtFlag=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_bPrtChkLmtFlag.
		/// </summary>
		public bool PrtChkLmtFlag
		{
			get{return m_bPrtChkLmtFlag;}
			set
			{
				if(m_bPrtChkLmtFlag!=value)
				{
					m_bDataChanged=true; 
					m_bPrtChkLmtFlag=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_bResLmtFlag.
		/// </summary>
		public bool ResLmtFlag
		{
			get{return m_bResLmtFlag;}
			set
			{
				if(m_bResLmtFlag!=value)
				{
					m_bDataChanged=true; 
					m_bResLmtFlag=value;
				}
			}
		}

        //Jira 6385 starts
        public bool ClaimIncLmtFlag
        {
            get { return m_bClaimIncLmtFlag; }
            set
            {
                if (m_bClaimIncLmtFlag != value)
                {
                    m_bDataChanged = true;
                    m_bClaimIncLmtFlag = value;
                }
            }
        }
        //Jira 6385 ends
		
		/// <summary>
		///Property to access m_iReserveTracking.
		/// </summary>
		public int ReserveTracking
		{
			get{return m_iReserveTracking;}
			set
			{
				if(m_iReserveTracking!=value)
				{
					m_bDataChanged=true; 
					m_iReserveTracking=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_iLineOfBusCode.
		/// </summary>
		public int LineOfBusCode
		{
			get{return m_iLineOfBusCode;}
			set
			{
				if(m_iLineOfBusCode!=value)
				{
					m_bDataChanged=true; 
					m_iLineOfBusCode=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_bAdjRsv.
		/// </summary>
		public bool AdjRsv
		{
			get{return m_bAdjRsv;}
			set
			{
				if(m_bAdjRsv!=value)
				{
					m_bDataChanged=true; 
					m_bAdjRsv=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_bCollInRsvBal.
		/// </summary>
		public bool CollInRsvBal
		{
			get{return m_bCollInRsvBal;}
			set
			{
				if(m_bCollInRsvBal!=value)
				{
					m_bDataChanged=true; 
					m_bCollInRsvBal=value;
				}
			}
		}

        /// <summary>
        ///Property to access m_bCollInIncurred.
        /// </summary>
        public bool CollInIncurred
        {
            get { return m_bCollInIncurred; }
            set
            {
                if (m_bCollInIncurred != value)
                {
                    m_bDataChanged = true;
                    m_bCollInIncurred = value;
                }
            }
        }
		
		/// <summary>
		///Property to access m_bBalToZero.
		/// </summary>
		public bool BalToZero
		{
			get{return m_bBalToZero;}
			set
			{
				if(m_bBalToZero!=value)
				{
					m_bDataChanged=true; 
					m_bBalToZero=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_bNegBalToZero.
		/// </summary>
		public bool NegBalToZero
		{
			get{return m_bNegBalToZero;}
			set
			{
				if(m_bNegBalToZero!=value)
				{
					m_bDataChanged=true; 
					m_bNegBalToZero=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_bSetToZero.
		/// </summary>
		public bool SetToZero
		{
			get{return m_bSetToZero;}
			set
			{
				if(m_bSetToZero!=value)
				{
					m_bDataChanged=true; 
					m_bSetToZero=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_bEventTrigger.
		/// </summary>
		public bool EventTrigger
		{
			get{return m_bEventTrigger;}
			set
			{
				if(m_bEventTrigger!=value)
				{
					m_bDataChanged=true; 
					m_bEventTrigger=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_bIncClmTypeFlag.
		/// </summary>
		public bool IncClmTypeFlag
		{
			get{return m_bIncClmTypeFlag;}
			set
			{
				if(m_bIncClmTypeFlag!=value)
				{
					m_bDataChanged=true; 
					m_bIncClmTypeFlag=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_bIncLobCodeFlag.
		/// </summary>
		public bool IncLobCodeFlag
		{
			get{return m_bIncLobCodeFlag;}
			set
			{
				if(m_bIncLobCodeFlag!=value)
				{
					m_bDataChanged=true; 
					m_bIncLobCodeFlag=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_bIncOrgFlag.
		/// </summary>
		public bool IncOrgFlag
		{
			get{return m_bIncOrgFlag;}
			set
			{
				if(m_bIncOrgFlag!=value)
				{
					m_bDataChanged=true; 
					m_bIncOrgFlag=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_iOrgTableId.
		/// </summary>
		public int OrgTableId
		{
			get{return m_iOrgTableId;}
			set
			{
				if(m_iOrgTableId!=value)
				{
					m_bDataChanged=true; 
					m_iOrgTableId=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_iDurationCode.
		/// </summary>
		public int DurationCode
		{
			get{return m_iDurationCode;}
			set
			{
				if(m_iDurationCode!=value)
				{
					m_bDataChanged=true; 
					m_iDurationCode=value;
				}
			}
		}
		/// <summary>
		///Property to access m_iDupPayCriteria.
		/// </summary>
		public int DupPayCriteria
		{
			get{return m_iDupPayCriteria;}
			set
			{
				if(m_iDupPayCriteria!=value)
				{
					m_bDataChanged=true; 
					m_iDupPayCriteria=value;
				}
			}
		}
		/// <summary>
		///Property to access m_iDupNumDays.
		/// </summary>
		public int DupNumDays
		{
			get{return m_iDupNumDays;}
			set
			{
				if(m_iDupNumDays!=value)
				{
					m_bDataChanged=true; 
					m_iDupNumDays=value;
				}
			}
		}
		/// <summary>
		///Property to access m_iEstNumOfClaims.
		/// </summary>
		public int EstNumOfClaims
		{
			get{return m_iEstNumOfClaims;}
			set
			{
				if(m_iEstNumOfClaims!=value)
				{
					m_bDataChanged=true; 
					m_iEstNumOfClaims=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_bUseFiscalYear.
		/// </summary>
		public bool UseFiscalYear
		{
			get{return m_bUseFiscalYear;}
			set
			{
				if(m_bUseFiscalYear!=value)
				{
					m_bDataChanged=true; 
					m_bUseFiscalYear=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_bAdjFundsFlag.
		/// </summary>
		public bool AdjFundsFlag
		{
			get{return m_bAdjFundsFlag;}
			set
			{
				if(m_bAdjFundsFlag!=value)
				{
					m_bDataChanged=true; 
					m_bAdjFundsFlag=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_iCaptionLevel.
		/// </summary>
		public int CaptionLevel
		{
			get{return m_iCaptionLevel;}
			set
			{
				if(m_iCaptionLevel!=value)
				{
					m_bDataChanged=true; 
					m_iCaptionLevel=value;
				}
			}
		}
        /// <summary>
        /// Property For AddRecoveryReservetoTotalIncurredAmount.
        /// </summary>
        public int AddRecoveryReservetoTotalIncurredAmount
        {
            get { return m_iAddRecoveryReservetoTotalIncurredAmount; }
            set
            {
                if (m_iAddRecoveryReservetoTotalIncurredAmount != value)
                {
                    m_bDataChanged = true;
                    m_iAddRecoveryReservetoTotalIncurredAmount = value;
                }
            }
        }
        /// <summary>
        /// Property For AddRecoveryReservetoTotalBalanceAmount.
        /// </summary>
        public int AddRecoveryReservetoTotalBalanceAmount
        {
            get { return m_iAddRecoveryReservetoTotalBalanceAmount; }
            set
            {
                if (m_iAddRecoveryReservetoTotalBalanceAmount != value)
                {
                    m_bDataChanged = true;
                    m_iAddRecoveryReservetoTotalBalanceAmount = value;
                }
            }
        }
        /// Property For Prevent Collection Per Reserve. JIRA 857
        /// </summary>
        public int PreventCollectionPerReserve
        {
            get { return m_iPreventCollectionPerReserve; }
            set
            {
                if (m_iPreventCollectionPerReserve != value)
                {
                    m_bDataChanged = true;
                    m_iPreventCollectionPerReserve = value;
                }
            }
        }
        /// </summary>
        public int PreventCollectionAllReserve
        {
            get { return m_iPreventCollectionAllReserve; }
            set
            {
                if (m_iPreventCollectionAllReserve != value)
                {
                    m_bDataChanged = true;
                    m_iPreventCollectionAllReserve = value;
                }
            }
        }

		/// <summary>
		///Property to access m_iNoNegResAdjFlag.
		/// </summary>
		public int NoNegResAdjFlag
		{
			get{return m_iNoNegResAdjFlag;}
			set
			{
				if(m_iNoNegResAdjFlag!=value)
				{
					m_bDataChanged=true; 
					m_iNoNegResAdjFlag=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_bResByClmType.
		/// </summary>
		public bool ResByClmType
		{
			get{return m_bResByClmType;}
			set
			{
				if(m_bResByClmType!=value)
				{
					m_bDataChanged=true; 
					m_bResByClmType=value;
				}
			}
		}

		/// <summary>
		///Property to access m_bResByClmType.
		/// </summary>
		public bool AdjRsvPerClaim
		{
			get{return m_bAdjRsvPerClaim;}
			set
			{
				if(m_bAdjRsvPerClaim!=value)
				{
					m_bDataChanged=true; 
					m_bAdjRsvPerClaim=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_bUseAdjText.
		/// </summary>
		public bool UseAdjText
		{
			get{return m_bUseAdjText;}
			set
			{
				if(m_bUseAdjText!=value)
				{
					m_bDataChanged=true; 
					m_bUseAdjText=value;
				}
			}
		}
		
		/// <summary>
		///Property to access m_bDataChanged.
		/// </summary>
		public bool DataChanged
		{
			get{return m_bDataChanged;}
			set{m_bDataChanged=value;}
		}
		
		/// <summary>
		///Property to access m_IsNew.
		/// </summary>
		internal bool IsNew
		{
			get{return m_IsNew;}
			set
			{
				if(m_IsNew!=value)
				{
					m_bDataChanged=true; 
					m_IsNew=value;
				}
			}
		}

        //rsushilaggar : ISO Claim Search
        /// <summary>
        /// Property to access m_iClmProgressNotesFlag.
        /// </summary>
        public int ClmProgressNotesFlag
        {
            get { return m_iClmProgressNotesFlag; }
            set
            {
                if (m_iClmProgressNotesFlag != value)
                {
                    m_bDataChanged = true;
                    m_iClmProgressNotesFlag = value;
                }
            }
        }

        /// <summary>
        /// Property to access m_iClmCommentsFlag.
        /// </summary>
        public int ClmCommentsFlag
        {
            get { return m_iClmCommentsFlag; }
            set
            {
                if (m_iClmCommentsFlag != value)
                {
                    m_bDataChanged = true;
                    m_iClmCommentsFlag = value;
                }
            }
        }

        /// <summary>
        /// Property to access m_iClmISOSubmissionFlag.
        /// </summary>
        public int ClmISOSubmissionFlag
        {
            get { return m_iClmISOSubmissionFlag; }
            set
            {
                if (m_iClmISOSubmissionFlag != value)
                {
                    m_bDataChanged = true;
                    m_iClmISOSubmissionFlag = value;
                }
            }
        }
        //End: rsushilaggar : ISO Claim Search
        /// <summary>
        /// nitin goel - mits 30910 - NIS
        /// </summary>
        public int DedRecTransType
        {
             get { return m_iDedRecTransType; }
            set
            {
                if (m_iDedRecTransType != value)
                {
                    m_bDataChanged = true;
                    m_iDedRecTransType = value;
                }
            }            
        }
        // <summary>
        /// nitin goel - mits 30910 - NIS
        /// </summary>
        public int DedRecReserveType
        {
            get { return m_iDedRecReserveType; }
            set
            {
                if (m_iDedRecReserveType != value)
                {
                    m_bDataChanged = true;
                    m_iDedRecReserveType = value;
                }
            }
        }
        /// <summary>
        /// nitin goel - mits 30910 - NIS
        /// </summary>
        public int ApplyDedToPaymentsFlag
        {
            get { return m_iApplyDedToPayments; }
            set
            {
                if (m_iApplyDedToPayments != value)
                {
                    m_bDataChanged = true;
                    m_iApplyDedToPayments = value;
                }
            }
        }
        //tanwar2 - mits 30910
        /// <summary>
        ///  Gets/Sets the Diminishing Dedeductible Percentage
        /// </summary>
         public double DimPercentNum
         {
             get { return m_dDimPercentNum; }
             set
             {
                 if (m_dDimPercentNum!=value)
                 {
                     m_bDataChanged = true;
                     m_dDimPercentNum = value;
                 }
             }
         }

         public bool PrvntPrntZeroChecks
         {
             get { return m_bPrvntPrntZeroChecks; }
             set
             {
                 if (m_bPrvntPrntZeroChecks!=value)
                 {
                     m_bDataChanged = true;
                     m_bPrvntPrntZeroChecks = value;
                 }
             }
         }
        /// <summary>
        /// ddhiman 10/16/2014
        /// </summary>
        public int SharedAggDedFlag
        {
            get { return m_iSharedAggDedFlag; }
            set
            {
                if (m_iSharedAggDedFlag != value)
                {
                    m_bDataChanged = true;
                    m_iSharedAggDedFlag = value;
                }
            }
        }

        /// <summary>
        /// Nikhil 10/16/2014
        /// </summary>
        public int DimnishingFlag
        {
            get { return m_iDimnishingFlag; }
            set
            {
                if (m_iDimnishingFlag != value)
                {
                    m_bDataChanged = true;
                    m_iDimnishingFlag = value;
                }
            }
        }

        /// <summary>
        /// Nikhil 10/16/2014
        /// </summary>
        public int EnableDistributionDedAmtOnVoid
        {
            get { return m_iEnableDistributionDedAmtOnVoid; }
            set
            {
                if (m_iEnableDistributionDedAmtOnVoid != value)
                {
                    m_bDataChanged = true;
                    m_iEnableDistributionDedAmtOnVoid = value;
                }
            }
        }

        /// <summary>
        /// ddhiman 10/16/2014
        /// </summary>
        public string DedRecoveryIdentifierChar
        {
            get { return m_sDedRecoveryIdentifierChar; }
            set
            {
                if (m_sDedRecoveryIdentifierChar != value)
                {
                    m_bDataChanged = true;
                    m_sDedRecoveryIdentifierChar = value;
                }
            }
        }
        public string ManualDedRecoveryIdentifierChar
        {
            get { return m_sManualDedRecoveryIdentifierChar; }
            set
            {
                if (m_sManualDedRecoveryIdentifierChar != value)
                {
                    m_bDataChanged = true;
                    m_sManualDedRecoveryIdentifierChar = value;
                }
            }
        }
        public bool PerRsvCollInRsvBal { get; set; }//asharma326 JIRA 870 

        public bool PerRsvCollInIncurred { get; set; }//asharma326 JIRA 870 
    }
}
