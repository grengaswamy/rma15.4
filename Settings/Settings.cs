using System;

namespace Riskmaster.Settings
{
	/// <summary>
	/// Riskmaster.Settings.SysParms.CSettings class is a wrapper to initialize all
	/// other classes in the project
	/// </summary> 
	public class SysParms
	{
        /// <summary>
        /// Client Id. Will store valid value for Cloud and 0 for Local instances
        /// </summary>
        private int m_iClientId;
		/// <summary>The connection string used by the database layer for connection with the database.</summary>
		private string m_sConnectString;
		/// <summary>Flag to store whether SysSettings class has been loaded or not.</summary>
		private bool m_bSysSettingsLoaded;
		/// <summary>Flag to store whether CColLOBSettings class has been loaded or not.</summary>
		private bool m_bColLOBSettingsLoaded;
		/// <summary>Flag to store whether CColFiscalYear class has been loaded or not.</summary>
		private bool m_bColFiscalYearLoaded;
		/// <summary>Flag to store whether BrsSettings class has been loaded or not.</summary>
		private bool m_bBrsSettingsLoaded;
		/// <summary>Object of SysSettings</summary>
		private SysSettings m_oSysSettings;
		/// <summary>Object of ColLobSettings</summary>
		private ColLobSettings m_oColLOBSettings;
		/// <summary>Object of ColFYSettings</summary>
		private ColFYSettings m_oColFiscalYear;
		/// <summary>Object of BrsSettings</summary>
		private BrsSettings m_oBrsSettings;
		/// <summary>Object of CCacheFunctions</summary>
		private CCacheFunctions m_oCacheFunctions;
		
		/// <summary>
		///Riskmaster.Settings.SysParms.CSettings.CSettings overload sets the connection string.
		/// </summary>
		/// <param name="p_sConnectString">Connection string</param>
        /// <param name="p_iClientId">Client Id</param>
		public SysParms(string p_sConnectString, int p_iClientId)
		{
			m_sConnectString=p_sConnectString;
            m_iClientId = p_iClientId;
			
		}

		
		/// <summary>
		///Riskmaster.Settings.SysParms.CSettings.Initialize initializes all classes in the project.
		/// </summary>	
		public void Initialize()
		{
			m_oSysSettings = null;
			m_oColLOBSettings = null;
			m_oColFiscalYear = null;
			m_oBrsSettings = null;
			m_bSysSettingsLoaded = false;
			m_bColLOBSettingsLoaded = false;
			m_bColFiscalYearLoaded = false;
			m_bBrsSettingsLoaded = false;
			m_oSysSettings = new SysSettings(m_sConnectString,m_iClientId);
            m_oColLOBSettings = new ColLobSettings(m_sConnectString, m_iClientId);
            m_oColFiscalYear = new ColFYSettings(m_sConnectString, "", 0, 0, m_iClientId);
			m_oBrsSettings = new BrsSettings(m_sConnectString, m_iClientId);
            m_oCacheFunctions = new CCacheFunctions(m_sConnectString, m_iClientId);
			m_bSysSettingsLoaded = true;
			m_bColLOBSettingsLoaded = true;
			m_bColFiscalYearLoaded = true;
			m_bBrsSettingsLoaded = true;
		}

		/// <summary>
		///Property to access m_oSysSettings. Also initializes the object, if not initialized.
		/// </summary>
		public SysSettings SysSettings
		{
			get
			{
				if(!m_bSysSettingsLoaded)
				{
					//to create instance for syssetting object 
					m_oSysSettings = new SysSettings(m_sConnectString,m_iClientId);
					//m_oSysSettings.LoadSettings();
					m_bSysSettingsLoaded=true;
				}
				return m_oSysSettings;
			}			
		}		
		
		/// <summary>
		///Property to access m_oColLOBSettings. Also initializes the object, if not initialized.
		/// </summary>
		public ColLobSettings ColLobSettings
		{
			get
			{
				if(!m_bColLOBSettingsLoaded)
				{
                    m_oColLOBSettings = new ColLobSettings(m_sConnectString, m_iClientId);
					//m_oColLOBSettings.LoadSettings();
					m_bColLOBSettingsLoaded=true;
				}
				return m_oColLOBSettings;
			}	
		}	
		
		/// <summary>
		///Property to access m_oColFiscalYear. Also initializes the object, if not initialized.
		/// </summary>
		public ColFYSettings ColFiscalYear
		{
			get
			{
				if(!m_bColFiscalYearLoaded)
				{
                    m_oColFiscalYear = new ColFYSettings(m_sConnectString, 0, 0, m_iClientId);
					//m_oColFiscalYear.LoadSettings("",0,0);
					m_bColFiscalYearLoaded=true;
				}
				return m_oColFiscalYear;
			}	
		}	
		
		/// <summary>
		///Property to access m_oBrsSettings. Also initializes the object, if not initialized.
		/// </summary>		
		public BrsSettings BrsSettings
		{
			get
			{
				if(!m_bBrsSettingsLoaded)
				{
                    m_oBrsSettings = new BrsSettings(m_sConnectString, m_iClientId);

					//m_oBrsSettings.LoadSettings();
					m_bBrsSettingsLoaded=true;
				}
				return m_oBrsSettings;
			}	
		}
		
		/// <summary>
		///Property to access m_oCacheFunctions. 
		/// </summary>
		public CCacheFunctions CacheFunctions
		{
			get
			{
				if(m_oCacheFunctions==null)
                    m_oCacheFunctions = new CCacheFunctions(m_sConnectString, m_iClientId);
				return m_oCacheFunctions;
			}			
		}	
	}
}
