/**********************************************************************************************
 *   Date     |  JIRA/MITS   | Programmer | Description                                        *
 **********************************************************************************************
 * 03/14/2014 | 35039        | pgupta93   | Changes req for search code description on lookup
 * 7/24/2014  | RMA-718      | ajohari2   | Changes for TPA Access ON/OFF
 * 11/20/2014 | JIRA-4362    | ngupta73   | Claim Policy Expansion
 * 03/07/2015 | RMA-8753     | nshah28    | Address Master
 **********************************************************************************************/
using System;
using System.Data;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Text;

namespace Riskmaster.Settings
{
    /// <summary>
    /// SYS_PARMS settings.
    /// </summary>
    public class SysSettings
    {
        /// <summary>The connection string used by the database layer for connection with the database.</summary>
        private string m_sConnectString;

        /// <summary>
        /// ClientID for mulit-tenancy, cloud
        /// </summary>
        private int m_iClientId = 0;

        private DataTable m_DataTable = null;  //fetch schema to check if field exists in table before reading from/writing to

        // Implementation to Fetch the Settings from SYS_SETTINGS
        // akaushik5 Changed for MITS 37242 Starts
        //protected static Hashtable m_Settings = new Hashtable();

        //protected static Hashtable m_DBForSettings = new Hashtable();

        
        private static Hashtable m_Settings = new Hashtable();

        private static Hashtable m_DBForSettings = new Hashtable();

        // akaushik5 Changed for MITS 37242 Ends

        // npadhy The Datatypes for which the Param Name Value is implemented
        private const string INT = "INT";
        private const string BOOL = "BOOL";
        private const string CHAR = "CHAR";
        private const string STRING = "STRING";
        private const string FLOAT = "FLOAT";

        private bool blnSuccess = false;
        /// <summary>
        ///Riskmaster.Settings.SysParms.SysSettings overload sets the connection string.
        /// </summary>
        /// <param name="p_sConnectString">Connection string</param>
        public SysSettings(string p_sConnectString,int p_iClientId)
        {
            m_sConnectString = p_sConnectString;

            //akaushik5 Commented for MITS 37242 Starts
            //m_sStaticConnectString = p_sConnectString;
            //akaushik5 Commented for MITS 37242 Ends
            m_iClientId = p_iClientId;

            if (!m_DBForSettings.ContainsKey(p_sConnectString))
            {
                LoadSettings();
                LoadSystemSettings();
               
            }

            // npadhy Start R7 Enhancement Fetching the Settings from new table instead of Sys Parms
            //if (m_Settings == null || m_Settings.Count==0)

            // npadhy End R7 Enhancement Fetching the Settings from new table instead of Sys Parms

        }

        /// <summary>
        /// Author  :   Naresh Chandra Padhy
        /// Dated   :   9th,Apr 2010
        /// Purpose :   Loads the Settings from the SYS_SETTINGS to the hashtable
        /// </summary>
        /// <returns>True if the loading of settings from Sys_settings is 
        /// successful otherwise returns false and throws exception</returns>
        // akaushik5 Changed for MITS 37242 Starts
        //internal bool LoadSystemSettings()
        internal bool LoadSystemSettings(string paramName = "")
        // akaushik5 Changed for MITS 37242 Ends
        {
            SystemSettings oSysSettings;
            bool blnSuccess = false;
            DbReader rdr = null;
            // akaushik5 Added for MITS 37242 Starts
            string key = string.Empty;
            StringBuilder sql = new StringBuilder();
            // akaushik5 Added for MITS 37242 Ends
            try
            {
                // akaushik5 Changed for MITS 37242 Starts
                //rdr = DbFactory.GetDbReader(m_sConnectString, "SELECT * FROM PARMS_NAME_VALUE WHERE PARM_DATA_TYPE IS NOT NULL");
                sql.Append("SELECT * FROM PARMS_NAME_VALUE WHERE PARM_DATA_TYPE IS NOT NULL");
                if (!string.IsNullOrEmpty(paramName))
                {
                    sql.AppendFormat(" AND PARM_NAME = '{0}'", paramName);
                }

                rdr = DbFactory.GetDbReader(m_sConnectString, sql.ToString());
                // akaushik5 Changed for MITS 37242 Ends
                m_DataTable = rdr.GetSchemaTable();
                DataRow[] rows = null;

                if (rdr != null)
                {
                    while (rdr.Read())
                    {
                        // akaushik5 Added for MITS 37242 Starts
                        key = string.Format("{0}_{1}", rdr.GetString("PARM_NAME"), this.m_sConnectString);
                        if (!SysSettings.m_Settings.ContainsKey(key))
                        {
                            //akaushik5 Added for MITS 37242 Ends
                        oSysSettings = new SystemSettings();

                        rows = m_DataTable.Select("ColumnName='ROW_ID'");
                        if (rows.GetLength(0) == 1)
                            oSysSettings.RowId = rdr.GetInt32("ROW_ID");

                        rows = m_DataTable.Select("ColumnName='PARM_NAME'");
                        if (rows.GetLength(0) == 1)
                            oSysSettings.ParamKey = rdr.GetString("PARM_NAME");

                        rows = m_DataTable.Select("ColumnName='STR_PARM_VALUE'");
                        if (rows.GetLength(0) == 1)
                            oSysSettings.ParamValue = rdr.GetString("STR_PARM_VALUE");

                        rows = m_DataTable.Select("ColumnName='PARM_DATA_TYPE'");
                        if (rows.GetLength(0) == 1)
                            oSysSettings.ParamDataType = rdr.GetString("PARM_DATA_TYPE");

                        if (!string.IsNullOrEmpty(oSysSettings.ParamDataType))
                        {
                            switch (oSysSettings.ParamDataType.ToUpper())
                            {
                                case INT:
                                    Conversion.CastToType<Int32>(oSysSettings.ParamValue, out blnSuccess);
                                    if (!blnSuccess)
                                            throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                                    break;
                                case BOOL:
                                    Conversion.CastToType<Boolean>(oSysSettings.ParamValue, out blnSuccess);
                                    if (!blnSuccess)
                                            throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                                    break;
                                case CHAR:
                                    Conversion.CastToType<Boolean>(oSysSettings.ParamValue, out blnSuccess);
                                    if (!blnSuccess)
                                            throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                                    break;
                                case FLOAT:
                                    Conversion.CastToType<float>(oSysSettings.ParamValue, out blnSuccess);
                                    if (!blnSuccess)
                                            throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                                    break;
                                //Ankit Start : Worked on MITS - 32386 - FAS Integration
                                case STRING:
                                    try
                                    {
                                        Convert.ToString(oSysSettings.ParamValue);
                                    }
                                    catch (Exception ee)
                                    {
                                            throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                                    }
                                    break;
                                //Ankit End
                                default:
                                        throw new RMAppException(Globalization.GetString("SysSettings.InvalidDataType", m_iClientId));
                                    break;
                            }
                        }

                        oSysSettings.DataChanged = false;
                            // akaushik5 Changed for MITS 37242 Starts
                            //m_Settings.Add((oSysSettings.ParamKey + "_" + m_sConnectString), oSysSettings);
                            m_Settings.Add(key, oSysSettings);
                    }
                        // akaushik5 Changed for MITS 37242 Ends
                }
                }
                rdr.Close();
            }
            catch (Exception e)
            {
                m_DBForSettings.Clear();
                m_Settings.Clear();
                throw new DataModelException(Globalization.GetString("SysSettings.LoadSystemSettings.DataError", m_iClientId), e);
            }
            return true;
        }

        

        /// <summary>
        /// Saves System Parameter settings
        /// </summary>
        /// <returns>boolean indicating whether or not the settings were saved successfully</returns>
        public bool SaveSettings()
        {

            if (!m_bDataChanged)
                return true;
            DataRow[] rows = null;
            bool blnDataSaved = false;
            DbReader objReader = null;
            try
            {
                // rrachev RMA-2653 load table structure first. Not all columns exists in all databases
                objReader = DbFactory.ExecuteReader(m_sConnectString, "SELECT * FROM SYS_PARMS WHERE ROW_ID=" + this.RowId.ToString());

                using (DbWriter writer = DbFactory.GetDbWriter(objReader, objReader.Read()))
                {
                    objReader.Dispose();
                    objReader = null;
                    SetValue(writer, "CLIENT_NAME", this.ClientName);
                    SetValue(writer, "OC_FAC_ADDR1", this.OCFacAddr1);
                    SetValue(writer, "OC_FAC_ADDR2", this.OCFacAddr2);
                    SetValue(writer, "OC_FAC_ADDR3", this.OCFacAddr3);
                    SetValue(writer, "OC_FAC_ADDR4", this.OCFacAddr4);
                    SetValue(writer, "OC_FAC_CONTACT", this.OCFacContact);
                    SetValue(writer, "OC_FAC_HCFA_NO", this.OCFacHCFANo);
                    SetValue(writer, "OC_FAC_NAME", this.OCFacName);
                    SetValue(writer, "OC_FAC_PHONE", this.OCFacPhone);
                    SetValue(writer, "WORK_MON", Convert.ToInt16(this.WorkMon));
                    SetValue(writer, "WORK_TUE", Convert.ToInt16(this.WorkTue));
                    SetValue(writer, "WORK_WED", Convert.ToInt16(this.WorkWed));
                    SetValue(writer, "WORK_THU", Convert.ToInt16(this.WorkThu));
                    SetValue(writer, "WORK_FRI", Convert.ToInt16(this.WorkFri));
                    SetValue(writer, "WORK_SAT", Convert.ToInt16(this.WorkSat));
                    SetValue(writer, "WORK_SUN", Convert.ToInt16(this.WorkSun));
                    SetValue(writer, "DATE_STAMP_FLAG", Convert.ToInt16(this.DateStampFlag));
                    SetValue(writer, "FREEZE_TEXT_FLAG", Convert.ToInt16(this.FreezeTextFlag));
                    SetValue(writer, "SHIFT_1_NAME", this.Shift1Name);
                    SetValue(writer, "SHIFT_2_NAME", this.Shift2Name);
                    SetValue(writer, "SHIFT_3_NAME", this.Shift3Name);
                    SetValue(writer, "SHIFT_1_START", this.Shift1Start);
                    SetValue(writer, "SHIFT_2_START", this.Shift2Start);
                    SetValue(writer, "SHIFT_3_START", this.Shift3Start);
                    SetValue(writer, "SHIFT_1_END", this.Shift1End);
                    SetValue(writer, "SHIFT_2_END", this.Shift2End);
                    SetValue(writer, "SHIFT_3_END", this.Shift3End);
                    SetValue(writer, "ESSP", this.ESSP);
                    SetValue(writer, "CLIENT_STATUS_CODE", this.ClientStatusCode);
                    SetValue(writer, "DATE_INSTALLED", this.DateInstalled);
                    SetValue(writer, "ADDR1", this.Addr1);
                    SetValue(writer, "ADDR2", this.Addr2);
                    //SetValue(writer, "ADDR3", this.Addr3);
                    //SetValue(writer, "ADDR4", this.Addr4);
                    SetValue(writer, "CITY", this.City);
                    SetValue(writer, "STATE", this.State);
                    SetValue(writer, "ZIP_CODE", this.ZipCode);
                    SetValue(writer, "PHONE_NUMBER", this.PhoneNumber);
                    SetValue(writer, "FAX_NUMBER", this.FaxNumber);
                    SetValue(writer, "PRIMARY_CONTACT", this.PrimaryContact);
                    SetValue(writer, "PRIMARY_TITLE", this.PrimaryTitle);
                    SetValue(writer, "SECONDARY_CONTACT", this.SecondaryContact);
                    SetValue(writer, "SECONDARY_TITLE", this.SecondaryTitle);
                    SetValue(writer, "DATA_ENTRY_CONTACT", this.DataEntryContact);
                    SetValue(writer, "DATA_ENTRY_TITLE", this.DataEntryTitle);
                    SetValue(writer, "REPORTING_CONTACT", this.ReportingContact);
                    SetValue(writer, "REPORTING_TITLE", this.ReportingTitle);
                    SetValue(writer, "DEPT_MGR_CONTACT", this.DeptMgrContact);
                    SetValue(writer, "DEPT_MGR_TITLE", this.DeptMgrTitle);
                    SetValue(writer, "ACCOUNTING_CONTACT", this.AccountingContact);
                    SetValue(writer, "ACCOUNTING_TITLE", this.AccountingTitle);
                    SetValue(writer, "DTG_PRODUCT", this.DTGProduct);
                    SetValue(writer, "DTG_VERSION", this.DTGVersion);
                    SetValue(writer, "HARDWARE", this.Hardware);
                    SetValue(writer, "PRINTER", this.Printer);
                    SetValue(writer, "DISTRIBUTION_MEDIA", this.DistributionMedia);
                    SetValue(writer, "MODEM_PHONE", this.ModemPhone);
                    SetValue(writer, "DTG_SUPPORT_PHONE", this.DTGSupportPhone);
                    SetValue(writer, "EV_PREFIX", this.EvPrefix);
                    SetValue(writer, "EV_INC_YEAR_FLAG", Convert.ToInt16(this.EvIncYearFlag));
                    SetValue(writer, "RELEASE_NUMBER", this.DBReleaseNumber);
                    SetValue(writer, "ACC_LINK_FLAG", Convert.ToInt16(this.FundsAccountLink));
                    SetValue(writer, "USE_SUB_ACCOUNT", Convert.ToInt16(this.UseFundsSubAcc));
                    SetValue(writer, "EV_TYPE_ACC_FLAG", Convert.ToInt16(this.EVTypeAccFlag));
                    SetValue(writer, "SM_LIMIT_DIRECT", Convert.ToInt16(this.SMLimitDirect));
                    SetValue(writer, "RES_DO_NOT_RECALC", Convert.ToInt16(this.ResDoNotRecalc));
                    SetValue(writer, "EXCL_HOLIDAYS", Convert.ToInt16(this.ExclHolidays));
                    SetValue(writer, "USE_DEFAULT_CARRIER", Convert.ToInt16(this.UseDefaultCarrier));
                    SetValue(writer, "ATTACH_DIARY_VIS", Convert.ToInt16(this.AttachDiaryVis));
                    SetValue(writer, "USE_DEF_CARRIER", this.UseDefCarrier);
                    SetValue(writer, "USETANDE", Convert.ToInt16(this.UseTAndE));
                    SetValue(writer, "RATE_LEVEL", this.RateLevel);
                    SetValue(writer, "TANDEACCT_ID", this.TandeAcctId);
                    SetValue(writer, "FROI_PREPARER", Convert.ToInt16(this.FroiPreparer));
                    SetValue(writer, "FREEZE_DATE_FLAG", this.FreezeDateFlag);
                    SetValue(writer, "DATE_ADJUSTER_FLAG", this.DateAdjusterFlag);
                //nadim for Enhance Notes Print Order for MITS 12820
                    SetValue(writer, "ENH_PRINT_ORDER1", this.EnhPrintOrder1);
                    SetValue(writer, "ENH_PRINT_ORDER2", this.EnhPrintOrder2);
                    SetValue(writer, "ENH_PRINT_ORDER3", this.EnhPrintOrder3);
                //nadim for Enhance Notes Print Order for MITS 12820
                //Changed by Gagan for MITS 12334 : Start
                    SetValue(writer, "FRZ_EXISTING_NOTES", this.FreezeExistingNotes);
                //Changed by Gagan for MITS 12334 : End
                    SetValue(writer, "FROI_BAT_HIGH_LVL", this.FroiBatHighLvl);
                    SetValue(writer, "FROI_BAT_LOW_LVL", this.FroiBatLowLvl);
                    SetValue(writer, "FROI_BAT_CLD_CLM", this.FroiBatCldClm);
                    SetValue(writer, "AUTO_NUM_WC_EMP", Convert.ToInt16(this.AutoNumWCEmp));
                    SetValue(writer, "WC_EMP_PREFIX", this.WCEmpPrefix);
                    SetValue(writer, "EDIT_WC_EMP_NUM", Convert.ToInt16(this.EditWCEmpNum));
                    SetValue(writer, "AUTO_NUM_VIN", Convert.ToInt16(this.AutoNumVin));
                    SetValue(writer, "AUTO_SELECT_POLICY", Convert.ToInt16(this.AutoSelectPolicy));
                    SetValue(writer, "PREV_RES_BACKDATING", Convert.ToInt16(this.PrevResBackdating));
                    SetValue(writer, "PREV_RES_MODIFYZERO", Convert.ToInt16(this.PrevResModifyzero));
                    SetValue(writer, "DIARY_ORG_LEVEL", this.DiaryOrgLevel);
                    SetValue(writer, "ALLOW_GLOBAL_PEEK", this.AllowGlobalPeek);
                    //SetValue(writer, "ALLOW_VOID_CLRD_PMTS", this.AllowVoidOfClrdPmnts);   //pmittal5 Mits 21865 - This setting is in PARMS_NAME_VALUE, not in SYS_PARMS
                    SetValue(writer, "RES_SHOW_ORPHANS", this.ResShowOrphans);
                    SetValue(writer, "NO_SUB_DEP_CHECK", this.NoSubDepCheck);
                    SetValue(writer, "USE_ACROSOFT_INTERFACE", this.UseAcrosoftInterface);
                //Shruti for Fl WC Max Rate
                    SetValue(writer, "USE_FL_WC_MAX_FLAG", this.UseFLMaxRate);
                //End
                    SetValue(writer, "MULTIPLE_INSURER", this.MultipleInsurer); // Mihika 01/12/2007 MITS 8656
                    SetValue(writer, "MULTIPLE_REINSURER", this.MultipleReInsurer); // Navdeep

                    SetValue(writer, "CLM_LTR_FLAG", this.ClaimLetterFlag);  //added by Navdeep for Chubb
                    SetValue(writer, "AUTO_FROIACORD_FLAG", this.AutoFROIACORDFlag);  //added by Navdeep for Chubb Auto FROI ACORD
                    SetValue(writer, "HOSP_PHYS_ANY_ENT", this.PhysicianSearchFlag);
                    SetValue(writer, "POLICY_DROP_DOWN", this.PolicyDropDown); // Mihika 01/17/2007 MITS 8657
                    SetValue(writer, "DELETE_ALL_CLAIM_DIARIES", this.DeleteAllClaimDiaries);// Pankaj 01/17/2007 MITS 8658
                //rkaur7 MITS 16668 : START
                    SetValue(writer, "REVIEWED_DIARIES_DEN", this.UseDiariesDen);// Add by kuladeep for mits:25083
                //Gagan Safeway Retrofit Policy Jursidiction : START
                    SetValue(writer, "SHOW_JURISDICTION", this.ShowJurisdiction);
                //rkaur7 MITS 16668 : END

                /* gbindra WWIG MITS#34104 01312014 START*/
                    SetValue(writer, "ENH_NOTE_CLMNT", this.AllowNotesAtClaimant);
                /* gbindra WWIG MITS#34104 01312014 END*/
                    SetValue(writer, "PRINT_OSHA_DESC", Convert.ToInt16(this.PrintOSHADesc));

                    SetValue(writer, "HIERARCHY_LEVEL", this.HierarchyLevel);

                    SetValue(writer, "PRINT_CLAIMS_ADMIN", this.PrintClaimsAdmin);

                    SetValue(writer, "BLANK_FIELD_FLAG", this.BlankFieldFlag);
                    SetValue(writer, "FIN_HIST_EVAL", this.FinHostEval);

                //added by rkaur7 for utility changes of policy
                    SetValue(writer, "STATE_EVAL", this.StateEval);
                //rkaur7 end

                    SetValue(writer, "WC_PRINTER_SETUP", this.WCPrinterSetup);
                    SetValue(writer, "X_AXIS", Convert.ToDouble(this.XAxis));
                    SetValue(writer, "Y_AXIS", Convert.ToDouble(this.YAxis));
                    SetValue(writer, "SECURITY_FLAG", this.SecurityFlag);
                    SetValue(writer, "DEFAULT_TIME_FLAG", this.DefaultTimeFlag);
                    SetValue(writer, "DEFAULT_DEPT_FLAG", this.DefaultDeptFlag);
                    SetValue(writer, "FILTER_OTH_PEOPLE", this.FilterOthPeople);
                    SetValue(writer, "FREEZE_EVENT_DESC", this.FreezeEventDesc);

                    SetValue(writer, "FREEZE_LOC_DESC", this.FreezeLocDesc);

                // Return type changed from int to bool. 
                    SetValue(writer, "USE_MAST_BANK_ACCT", Convert.ToInt16(this.UseMastBankAcct));
                //added by stara Mits 16667--05/15/09
                    SetValue(writer, "USE_RES_FILTER", Convert.ToInt16(this.UseResFilter));
                //ends here
                    SetValue(writer, "ASSIGN_SUB_ACCOUNT", Convert.ToInt16(this.AssignCheckStockToSubAcc));
                    SetValue(writer, "FBBAL_DT_OPT", this.FundsAcountBalanceDateCriteria);

                    SetValue(writer, "P2_LOGIN", this.P2Login);
                    SetValue(writer, "P2_PASS", this.P2Pass);

                    SetValue(writer, "P2_URL", this.P2Url);
				//mbahl3 Strataware enhancement
                    SetValue(writer, "CONSUMERURL", this.StratawareConsumerUrl);
                    SetValue(writer, "SERVICEURL", this.StratwareServiceUrl);
					//mbahl3 Strataware enhancement
                     SetValue(writer, "DATE_EVENT_DESC", this.DateEventDesc);
                    SetValue(writer, "ORDER_BANK_ACC", this.OrderBankAcc);
                    SetValue(writer, "SUPP_FLD_SEC_FLAG", this.SuppFldSecFlag);
                    SetValue(writer, "ENH_HIERARCHY_LIST", this.EnhHierarchyList);
                    SetValue(writer, "EV_CAPTION_LEVEL", this.EvCaptionLevel);

                //yatharth START
                //rows = m_DataTable.Select("ColumnName='ORG_TIMEZONE_LEVEL'");
                //if (rows.GetLength(0) == 1)
                    //    SetValue(writer, "ORG_TIMEZONE_LEVEL", this.OrgTimeZoneLevel);
                //yatharth END
                    SetValue(writer, "USE_ENH_POL_FLAG", this.UseEnhPolFlag);
                //Geeta: Adding flag for Enhance Policy starts
                    SetValue(writer, "USE_BILLING_FLAG", this.UseBillingFlag);
                //Adding flag for Enhance Policy ends
                    SetValue(writer, "ORACLE_CASE_INS", this.OracleCaseIns);
                //Animesh Inserted for Bill Review Fee RMSC   //skhare7 RMSC Merge 28397
                    SetValue(writer, "USE_BILL_REV_FEE", this.UseBillReview);
                    SetValue(writer, "ORG_BILL_REV_FEE", this.OrgBillLevel);
                    SetValue(writer, "ORG_BILL_REV_FEE_EID", this.OrgBillLevelEntityId);//skhare7 RMSC merge changes to make it generic 
				//Animesh Insertion Ends //skhare7 RSC merge
                //Raman 03/08/2009 : R6 Work Loss / Restriction
                    SetValue(writer, "CREATE_WL_RES_RCDS", this.AutoCrtWrkLossRest);
                    SetValue(writer, "USE_LEGACY_COMMENTS", this.UseLegacyComments);
                //Added Rakhi for R7:Add Emp Data Elements
                //rows = m_DataTable.Select("ColumnName='USE_MUL_ADDRESSES'");
                //if (rows.GetLength(0) == 1)
                    //    SetValue(writer, "USE_MUL_ADDRESSES", this.UseMultipleAddresses);
                //Added Rakhi for R7:Add Emp Data Elements
                //Start by Shivendu for Outlook email Utility Setting
                //rows = m_DataTable.Select("ColumnName='USE_OUTLOOK'");
                //if (rows.GetLength(0) == 1)
                    //    SetValue(writer, "USE_OUTLOOK", this.UseOutlook);
                //End by Shivendu for Outlook email Utility Setting
                //Geeta 04/15/10 : R7 Task Manager Diary Starts
                //rows = m_DataTable.Select("ColumnName='TASK_MGR_DIARY'");
                //if (rows.GetLength(0) == 1)
                    //    SetValue(writer, "TASK_MGR_DIARY", this.TaskMgrDiary);
                //Geeta : R7 Task Manager Diary Ends
                writer.Execute();
                }
                //Store the SMTP Server settings in the Sett`ings table
                //rsushilaggar Added Alternate domain & replyto domain field in the settings screen MITS 28280 
                RMConfigurationSettings.SetSMTPServer(this.SMTPServer, string.Empty, this.SenderAlternateDomain, this.SenderReplytoDomain, m_iClientId);

                blnDataSaved = true;
                this.DataChanged = false;
            }
            catch (Exception e)
            {
                throw new DataException(Globalization.GetString("SysSettings.SaveSettings.DataError", m_iClientId), e);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
            }
            return blnDataSaved;

        }

        private void SetValue(DbWriter writer, string fldName, object value)
        {
            if (HasField(writer, fldName))
            {
                writer.Fields[fldName].Value = value;
            }
        }

        private static bool HasField(DbWriter writer, string fieldName)
        {
            Boolean bResult = false;
            if (writer.Fields.Count > 0)
            {
                for (int idx = 0; idx < writer.Fields.Count; idx++)
                {
                    DbField fld = writer.Fields[idx];
                    bResult = (fld.Name == fieldName);
                    if (bResult) 
                        break;
                }
            }
            return bResult;
        }

        /// <summary>
        /// Author  :   Naresh Chandra Padhy
        /// Dated   :   9th,Apr 2010
        /// Purpose :   Saves the Settings from the hashtable to SYS_SETTINGS
        /// </summary>
        /// <returns>boolean indicating whether or not the settings were saved successfully</returns>
        public bool SaveSystemSettings()
        {

            bool blnDataSaved = false;

            foreach (string sKey in m_Settings.Keys)
            {
                blnDataSaved = SaveSystemSetting(sKey);
            }
            m_DBForSettings.Clear();
            m_Settings.Clear();
            
            using (LocalCache objCache = new LocalCache(m_sConnectString, m_iClientId))
            {
                objCache.RemoveAnyKeyFromCache("SCRIPTEDITOR");
                objCache.RemoveAnyKeyFromCache("CASEMGMT");
            }
            return blnDataSaved;
        }

        private bool SaveSystemSetting(string p_sKey)
        {
            //rupal: start
            //commented as it was declared but never used
            //DataRow[] rows = null; 
            //it was declared inside try bolck, declared it outside try block so that it can be disposed in finallly block
            DbWriter writer = null;
            //rupal:end

            bool blnSuccess = false;
            SystemSettings oSysSettings;
            bool blnDataSaved = false;
            oSysSettings = (SystemSettings)m_Settings[p_sKey];
            
            if (oSysSettings.DataChanged)
            {
                try
                {
                    // Save the values in the DB
                    writer = DbFactory.GetDbWriter(this.m_sConnectString);
                    writer.Tables.Add("PARMS_NAME_VALUE");
                    writer.Where.Add(String.Format("ROW_ID={0}", oSysSettings.RowId));


                    //rows = m_DataTable.Select("ColumnName='PARAM_KEY'");
                    //if (rows.GetLength(0) == 1)
                    //    writer.Fields.Add("PARAM_KEY", oSettings.ParamKey);
                    switch (oSysSettings.ParamDataType.ToUpper())
                    {
                        case INT:
                            Conversion.CastToType<Int32>(oSysSettings.ParamValue, out blnSuccess);
                            if (!blnSuccess)
                                throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                            break;
                        case BOOL:
                            Conversion.CastToType<Boolean>(oSysSettings.ParamValue, out blnSuccess);
                            if (!blnSuccess)
                                throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                            break;
                        case CHAR:
                            Conversion.CastToType<Boolean>(oSysSettings.ParamValue, out blnSuccess);
                            if (!blnSuccess)
                                throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                            break;
                        case FLOAT:
                            Conversion.CastToType<float>(oSysSettings.ParamValue, out blnSuccess);
                            if (!blnSuccess)
                                throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                            break;
                        //Ankit Start : Worked on MITS - 32386 - FAS Integration
                        case STRING:
                            try
                            {
                                Convert.ToString(oSysSettings.ParamValue);
                            }
                            catch (Exception ee)
                            {
                                throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                            }
                            break;
                        //Ankit End
                        default:
                            throw new RMAppException(Globalization.GetString("SysSettings.InvalidDataType", m_iClientId));
                            break;
                    }
                    //No need for it now
                    //rows = m_DataTable.Select("ColumnName='STR_PARM_VALUE'");
                    //if (rows.GetLength(0) == 1)
                    writer.Fields.Add("STR_PARM_VALUE", oSysSettings.ParamValue);

                    writer.Execute();

                    oSysSettings.DataChanged = false;


                    using (LocalCache objCache = new LocalCache(m_sConnectString, m_iClientId))
                    {
                        objCache.UpdateCacheforSystemSettings(p_sKey, oSysSettings.ParamValue);
                        blnDataSaved = true;

                        //rupal:start, r8 first & final payment enh
                        #region if carrier claims is set to on, insert an entry in codes table for 'first & final' reserve status

                        
                        if (p_sKey.Contains("MULTI_COVG_CLM") && blnDataSaved == true && oSysSettings.ParamValue == "-1")
                        {
                            int iTableID = objCache.GetTableId("RESERVE_STATUS");
                            //check if entry for "first & final" reserve status already exists
                            string sSQL = "SELECT CODES_TEXT.CODE_ID FROM CODES, CODES_TEXT WHERE CODES.CODE_ID=CODES_TEXT.CODE_ID and CODES.TABLE_ID = " + iTableID + " AND CODES_TEXT.SHORT_CODE = 'F'";
                            using (DbReader objReader = DbFactory.ExecuteReader(this.m_sConnectString, sSQL))
                            {
                                if (objReader != null)
                                {
                                    if (!objReader.Read())
                                    {
                                        //insert an entry for first & final reserve when carrier claim is set on
                                        int iCodeID = Utilities.GetNextUID(m_sConnectString, "CODES", m_iClientId);
                                        sSQL = String.Format("INSERT INTO CODES(LINE_OF_BUS_CODE,CODE_ID,TABLE_ID,SHORT_CODE,RELATED_CODE_ID,DELETED_FLAG)" +
                                            " VALUES(0,{0},{1},'F',0,0)", iCodeID, iTableID);
                                        DbFactory.ExecuteNonQuery(m_sConnectString, sSQL);

                                        sSQL = string.Format("INSERT INTO CODES_TEXT(CODE_ID,LANGUAGE_CODE,SHORT_CODE,CODE_DESC)" +
                                            " VALUES({0},1033,'F','First & Final')", iCodeID);
                                        DbFactory.ExecuteNonQuery(m_sConnectString, sSQL);
                                    }
                                }
                            }
                        }
                        //rupal:end
                        #endregion
                    }
                }
                catch (RMAppException p_objEx)
                {
                    throw p_objEx;
                }
                catch (Exception p_objEx)
                {
                    throw new RMAppException(Globalization.GetString("SystemParameter.Save.SaveErr", m_iClientId), p_objEx);
                }
                finally
                {
                    writer = null;
                    oSysSettings = null;
                }

            }
            return blnDataSaved;
        }

        /// <summary>
        /// Author  :   Naresh Chandra Padhy
        /// Dated   :   9th,Apr 2010
        /// Purpose :   Saves the Settings from the hashtable to SYS_SETTINGS
        /// </summary>
        /// <returns>boolean indicating whether or not the settings were saved successfully</returns>
        public bool SaveSystemSettings(string p_sParamKey)
        {
            bool blnDataSaved = false;

            blnDataSaved = SaveSystemSetting(p_sParamKey + "_" + m_sConnectString);
            return blnDataSaved;
        }

        /// <summary>
        /// Author  :   Naresh Chandra Padhy
        /// Dated   :   9th,Apr 2010
        /// Purpose :   Retrieves the Setting value from the hashtable corresponding to a Key
        /// </summary>
        /// <param name="sParam_Key">The Key for which the Value is to be returned</param>
        /// <returns>The Returned value</returns>
        public string RetrieveSystemSettings(string sParam_Key)
        {
            // akaushik5 Added for MITS 37242 Starts
            if (!m_Settings.ContainsKey(sParam_Key + "_" + m_sConnectString))
            {
                this.LoadSystemSettings(sParam_Key);
            }
            // akaushik5 Added for MITS 37242 Ends

            if (m_Settings.ContainsKey(sParam_Key + "_" + m_sConnectString))
                return (((SystemSettings)m_Settings[sParam_Key + "_" + m_sConnectString]).ParamValue);
            else
                throw new Exception(Globalization.GetString("SysSettings.RetrieveSystemSettings", m_iClientId));
        }

        /// <summary>
        /// Author  :   Naresh Chandra Padhy
        /// Dated   :   9th,Apr 2010
        /// Purpose :   Assign the Setting value into the hashtable corresponding to a Key
        /// </summary>
        /// <param name="sParam_key">The Key for which value needs to be assigned</param>
        /// <param name="sParam_Value">The Value to be assigned</param>
        public void AssignSystemSettings(string sParam_key, string sParam_Value)
        {
            SystemSettings oSysSettings;
            bool blnSuccess = false;
            sParam_key = sParam_key + "_" + m_sConnectString;
            if (m_Settings.ContainsKey(sParam_key))
            {
                oSysSettings = (SystemSettings)m_Settings[sParam_key];

                // We do not want to override the values, if there is no change in the value of the Utility Setting
                if (oSysSettings.ParamValue != sParam_Value)
                {
                    switch (oSysSettings.ParamDataType.ToUpper())
                    {
                        case INT:
                            Conversion.CastToType<Int32>(oSysSettings.ParamValue, out blnSuccess);
                            if (!blnSuccess)
                                throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                            break;
                        case BOOL:
                            Conversion.CastToType<Boolean>(oSysSettings.ParamValue, out blnSuccess);
                            if (!blnSuccess)
                                throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                            break;
                        case CHAR:
                            Conversion.CastToType<Boolean>(oSysSettings.ParamValue, out blnSuccess);
                            if (!blnSuccess)
                                throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                            break;
                        case FLOAT:
                            Conversion.CastToType<float>(oSysSettings.ParamValue, out blnSuccess);
                            if (!blnSuccess)
                                throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                            break;
                        //Ankit Start : Worked on MITS - 32386 - FAS Integration
                        case STRING:
                            try
                            {
                                Convert.ToString(oSysSettings.ParamValue);
                            }
                            catch (Exception ee)
                            {
                                throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                            }
                            break;
                        //Ankit End
                        default:
                            throw new RMAppException(Globalization.GetString("SysSettings.InvalidDataType", m_iClientId));
                            break;
                    }

                    oSysSettings.ParamValue = sParam_Value;
                    oSysSettings.DataChanged = true;
                }
            }
            else
                throw new Exception(Globalization.GetString("SysSettings.AssignSystemSettings", m_iClientId));
        }

        /// <summary>
        /// Author  :   Naresh Chandra Padhy
        /// Dated   :   9th,Apr 2010
        /// Purpose :   Assign the Setting value into the hashtable corresponding to a Key and save that value 
        ///             to the Database
        /// </summary>
        /// <param name="sParam_key">The Key for which value needs to be assigned</param>
        /// <param name="bParam_Value">The Value to be assigned</param>
        /// <param name="bSave">If the parameter is true then all the values are saved. If it is false then only this Setting will be saved</param>
        public void AssignSystemSettings(string sParam_key, string sParam_Value, bool bSave)
        {
            AssignSystemSettings(sParam_key, sParam_Value);

            if (bSave)
                SaveSystemSettings();
            else
                SaveSystemSettings(sParam_key);
        }


        /// <summary>
        /// Loads all of the available System Parameters
        /// </summary>
        /// <returns>boolean indicating whether or not the System Parameters were loaded successfully</returns>
        internal bool LoadSettings()
        {
            bool blnDataLoaded = false;
            Hashtable htProp = GetPropertyHT();

            using (DbReader rdr = DbFactory.ExecuteReader(m_sConnectString, "SELECT * FROM SYS_PARMS"))
            {
                m_DataTable = rdr.GetSchemaTable();
                DataRow[] rows = null;

                while (rdr.Read())
                {
                    //this.ClientName=rdr.GetString("CLIENT_NAME");
                    htProp["ClientName"] = rdr.GetString("CLIENT_NAME");
                    //this.OCFacAddr1=rdr.GetString("OC_FAC_ADDR1");
                    htProp["OCFacAddr1"] = rdr.GetString("OC_FAC_ADDR1");
                    //this.OCFacAddr2=rdr.GetString("OC_FAC_ADDR2");
                    htProp["OCFacAddr2"] = rdr.GetString("OC_FAC_ADDR2");
                    //this.OCFacAddr3=rdr.GetString("OC_FAC_ADDR3");
                    htProp["OCFacAddr3"] = rdr.GetString("OC_FAC_ADDR3");
                    //this.OCFacAddr4=rdr.GetString("OC_FAC_ADDR4");
                    htProp["OCFacAddr4"] = rdr.GetString("OC_FAC_ADDR4");
                    //this.OCFacContact=rdr.GetString("OC_FAC_CONTACT");
                    htProp["OCFacContact"] = rdr.GetString("OC_FAC_CONTACT");
                    //this.OCFacHCFANo=rdr.GetString("OC_FAC_HCFA_NO");
                    htProp["OCFacHCFANo"] = rdr.GetString("OC_FAC_HCFA_NO");
                    //this.OCFacName=rdr.GetString("OC_FAC_NAME");
                    htProp["OCFacName"] = rdr.GetString("OC_FAC_NAME");
                    //this.OCFacPhone=rdr.GetString("OC_FAC_PHONE");
                    htProp["OCFacPhone"] = rdr.GetString("OC_FAC_PHONE");
                    //this.WorkMon=Convert.ToBoolean(rdr.GetInt16("WORK_MON"));
                    htProp["WorkMon"] = Convert.ToBoolean(rdr.GetInt16("WORK_MON"));
                    //this.WorkTue=Convert.ToBoolean(rdr.GetInt16("WORK_TUE"));
                    htProp["WorkTue"] = Convert.ToBoolean(rdr.GetInt16("WORK_TUE"));
                    //this.WorkWed=Convert.ToBoolean(rdr.GetInt16("WORK_WED"));
                    htProp["WorkWed"] = Convert.ToBoolean(rdr.GetInt16("WORK_WED"));
                    //this.WorkThu=Convert.ToBoolean(rdr.GetInt16("WORK_THU"));
                    htProp["WorkThu"] = Convert.ToBoolean(rdr.GetInt16("WORK_THU"));
                    //this.WorkFri=Convert.ToBoolean(rdr.GetInt16("WORK_FRI"));
                    htProp["WorkFri"] = Convert.ToBoolean(rdr.GetInt16("WORK_FRI"));
                    //this.WorkSat=Convert.ToBoolean(rdr.GetInt16("WORK_SAT"));
                    htProp["WorkSat"] = Convert.ToBoolean(rdr.GetInt16("WORK_SAT"));
                    //this.WorkSun=Convert.ToBoolean(rdr.GetInt16("WORK_SUN"));
                    htProp["WorkSun"] = Convert.ToBoolean(rdr.GetInt16("WORK_SUN"));
                    //this.DateStampFlag=Convert.ToBoolean(rdr.GetInt16("DATE_STAMP_FLAG"));
                    htProp["DateStampFlag"] = Convert.ToBoolean(rdr.GetInt16("DATE_STAMP_FLAG"));
                    //this.FreezeTextFlag=Convert.ToBoolean(rdr.GetInt16("FREEZE_TEXT_FLAG"));
                    htProp["FreezeTextFlag"] = Convert.ToBoolean(rdr.GetInt16("FREEZE_TEXT_FLAG"));
                    //this.Shift1Name=rdr.GetString("SHIFT_1_NAME");
                    htProp["Shift1Name"] = rdr.GetString("SHIFT_1_NAME");
                    //this.Shift2Name=rdr.GetString("SHIFT_2_NAME");
                    htProp["Shift2Name"] = rdr.GetString("SHIFT_2_NAME");
                    //this.Shift3Name=rdr.GetString("SHIFT_3_NAME");
                    htProp["Shift3Name"] = rdr.GetString("SHIFT_3_NAME");
                    //this.Shift1Start=rdr.GetString("SHIFT_1_START");
                    htProp["Shift1Start"] = rdr.GetString("SHIFT_1_START");
                    //this.Shift2Start=rdr.GetString("SHIFT_2_START");
                    htProp["Shift2Start"] = rdr.GetString("SHIFT_2_START");
                    //this.Shift3Start=rdr.GetString("SHIFT_3_START");
                    htProp["Shift3Start"] = rdr.GetString("SHIFT_3_START");
                    //this.Shift1End=rdr.GetString("SHIFT_1_END");
                    htProp["Shift1End"] = rdr.GetString("SHIFT_1_END");
                    //this.Shift2End=rdr.GetString("SHIFT_2_END");
                    htProp["Shift2End"] = rdr.GetString("SHIFT_2_END");
                    //this.Shift3End=rdr.GetString("SHIFT_3_END");
                    htProp["Shift3End"] = rdr.GetString("SHIFT_3_END");
                    //this.ESSP=rdr.GetInt32("ESSP");
                    htProp["ESSP"] = rdr.GetInt32("ESSP");
                    //this.ClientStatusCode=rdr.GetInt32("CLIENT_STATUS_CODE");
                    //Mits 29090 Changed the data type of Client Status Code.So changing the method to GetString
                    htProp["ClientStatusCode"] = rdr.GetString("CLIENT_STATUS_CODE");
                    //this.DateInstalled=rdr.GetString("DATE_INSTALLED");
                    htProp["DateInstalled"] = rdr.GetString("DATE_INSTALLED");
                    //this.Addr1=rdr.GetString("ADDR1");
                    htProp["Addr1"] = rdr.GetString("ADDR1");
                    //this.Addr2=rdr.GetString("ADDR2");
                    htProp["Addr2"] = rdr.GetString("ADDR2");
                    ////this.Addr3=rdr.GetString("ADDR3");
                    //htProp["Addr3"] = rdr.GetString("ADDR3");
                    ////this.Addr4=rdr.GetString("ADDR4");
                    //htProp["Addr4"] = rdr.GetString("ADDR4");
                    //this.City=rdr.GetString("CITY");
                    htProp["City"] = rdr.GetString("CITY");
                    //this.State=rdr.GetString("STATE");
                    htProp["State"] = rdr.GetString("STATE");
                    //this.ZipCode=rdr.GetString("ZIP_CODE");
                    htProp["ZipCode"] = rdr.GetString("ZIP_CODE");
                    //this.PhoneNumber=rdr.GetString("PHONE_NUMBER");
                    htProp["PhoneNumber"] = rdr.GetString("PHONE_NUMBER");
                    //this.FaxNumber=rdr.GetString("FAX_NUMBER");
                    htProp["FaxNumber"] = rdr.GetString("FAX_NUMBER");
                    //this.PrimaryContact=rdr.GetString("PRIMARY_CONTACT");
                    htProp["PrimaryContact"] = rdr.GetString("PRIMARY_CONTACT");
                    //this.PrimaryTitle=rdr.GetString("PRIMARY_TITLE");
                    htProp["PrimaryTitle"] = rdr.GetString("PRIMARY_TITLE");
                    //this.SecondaryContact=rdr.GetString("SECONDARY_CONTACT");
                    htProp["SecondaryContact"] = rdr.GetString("SECONDARY_CONTACT");
                    //this.SecondaryTitle=rdr.GetString("SECONDARY_TITLE");
                    htProp["SecondaryTitle"] = rdr.GetString("SECONDARY_TITLE");
                    //this.DataEntryContact=rdr.GetString("DATA_ENTRY_CONTACT");
                    htProp["DataEntryContact"] = rdr.GetString("DATA_ENTRY_CONTACT");
                    //this.DataEntryTitle=rdr.GetString("DATA_ENTRY_TITLE");
                    htProp["DataEntryTitle"] = rdr.GetString("DATA_ENTRY_TITLE");
                    //this.ReportingContact=rdr.GetString("REPORTING_CONTACT");
                    htProp["ReportingContact"] = rdr.GetString("REPORTING_CONTACT");
                    //this.ReportingTitle=rdr.GetString("REPORTING_TITLE");
                    htProp["ReportingTitle"] = rdr.GetString("REPORTING_TITLE");
                    //this.DeptMgrContact=rdr.GetString("DEPT_MGR_CONTACT");
                    htProp["DeptMgrContact"] = rdr.GetString("DEPT_MGR_CONTACT");
                    //this.DeptMgrTitle=rdr.GetString("DEPT_MGR_TITLE");
                    htProp["DeptMgrTitle"] = rdr.GetString("DEPT_MGR_TITLE");
                    //this.AccountingContact=rdr.GetString("ACCOUNTING_CONTACT");
                    htProp["AccountingContact"] = rdr.GetString("ACCOUNTING_CONTACT");
                    //this.AccountingTitle=rdr.GetString("ACCOUNTING_TITLE");
                    htProp["AccountingTitle"] = rdr.GetString("ACCOUNTING_TITLE");
                    //this.DTGProduct=rdr.GetString("DTG_PRODUCT");
                    htProp["DTGProduct"] = rdr.GetString("DTG_PRODUCT");
                    //this.DTGVersion=rdr.GetString("DTG_VERSION");
                    htProp["DTGVersion"] = rdr.GetString("DTG_VERSION");
                    //this.Hardware=rdr.GetString("HARDWARE");
                    htProp["Hardware"] = rdr.GetString("HARDWARE");
                    //this.Printer=rdr.GetString("PRINTER");
                    htProp["Printer"] = rdr.GetString("PRINTER");
                    //this.DistributionMedia=rdr.GetString("DISTRIBUTION_MEDIA");
                    htProp["DistributionMedia"] = rdr.GetString("DISTRIBUTION_MEDIA");
                    //this.ModemPhone=rdr.GetString("MODEM_PHONE");
                    htProp["ModemPhone"] = rdr.GetString("MODEM_PHONE");
                    //this.DTGSupportPhone=rdr.GetString("DTG_SUPPORT_PHONE");
                    htProp["DTGSupportPhone"] = rdr.GetString("DTG_SUPPORT_PHONE");
                    //this.EvPrefix=rdr.GetString("EV_PREFIX");
                    htProp["EvPrefix"] = rdr.GetString("EV_PREFIX");
                    //this.EvIncYearFlag=Convert.ToBoolean(rdr.GetInt16("EV_INC_YEAR_FLAG"));
                    htProp["EvIncYearFlag"] = Convert.ToBoolean(rdr.GetInt16("EV_INC_YEAR_FLAG"));
                    //this.DBReleaseNumber=rdr.GetString("RELEASE_NUMBER");
                    htProp["DBReleaseNumber"] = rdr.GetString("RELEASE_NUMBER");
                    //this.FundsAccountLink=Convert.ToBoolean(rdr.GetInt16("ACC_LINK_FLAG"));
                    htProp["FundsAccountLink"] = Convert.ToBoolean(rdr.GetInt16("ACC_LINK_FLAG"));
                    //this.UseFundsSubAcc=Convert.ToBoolean(rdr.GetInt16("USE_SUB_ACCOUNT"));
                    htProp["UseFundsSubAcc"] = Convert.ToBoolean(rdr.GetInt16("USE_SUB_ACCOUNT"));
                    //this.EVTypeAccFlag=Convert.ToBoolean(rdr.GetInt16("EV_TYPE_ACC_FLAG"));
                    htProp["EVTypeAccFlag"] = Convert.ToBoolean(rdr.GetInt16("EV_TYPE_ACC_FLAG"));
                    //this.SMLimitDirect=Convert.ToBoolean(rdr.GetInt16("SM_LIMIT_DIRECT"));
                    htProp["SMLimitDirect"] = Convert.ToBoolean(rdr.GetInt16("SM_LIMIT_DIRECT"));
                    //this.ResDoNotRecalc=Convert.ToBoolean(rdr.GetInt16("RES_DO_NOT_RECALC"));
                    htProp["ResDoNotRecalc"] = Convert.ToBoolean(rdr.GetInt16("RES_DO_NOT_RECALC"));
                    
                    //smishra25:Added by Shivendu,Performance related changes
                    htProp["AutoLaunchDiary"] = rdr.GetInt32("AUTO_LAUNCH_DIARY");

                    //If there is not record in the security DB's Settings table, user can not login
                    DataTable dtSettings = RMConfigurationSettings.GetSMTPServerSettings(m_iClientId);
                    if (dtSettings.Rows.Count > 0)
                    {
                        //this.SMTPServer = dtSettings.Rows[0]["SMTP_SERVER"].ToString();
                        htProp["SMTPServer"] = dtSettings.Rows[0]["SMTP_SERVER"].ToString();
                        htProp["SenderAlternateDomain"] = dtSettings.Rows[0]["ALTERNATE_DOMAIN"].ToString();
                        htProp["SenderReplytoDomain"] = dtSettings.Rows[0]["REPLYTO_DOMAIN"].ToString();
                    }
                    else
                    {
                        //this.SMTPServer = string.Empty;
                        htProp["SMTPServer"] = string.Empty;
                        htProp["SenderAlternateDomain"] = string.Empty;
                        htProp["SenderReplytoDomain"] = string.Empty;
                    }

                    //this.ExclHolidays=Convert.ToBoolean(rdr.GetInt16("EXCL_HOLIDAYS"));
                    htProp["ExclHolidays"] = Convert.ToBoolean(rdr.GetInt16("EXCL_HOLIDAYS"));
                    // JP 11.07.2005   Obsolete. Moved to FROI_OPTIONS.     this.UseDefaultCarrier=Convert.ToBoolean(rdr.GetInt16("USE_DEFAULT_CARRIER"));
                    //this.AttachDiaryVis=Convert.ToBoolean(rdr.GetInt16("ATTACH_DIARY_VIS"));
                    htProp["AttachDiaryVis"] = Convert.ToBoolean(rdr.GetInt16("ATTACH_DIARY_VIS"));
                    // JP 11.07.2005   Obsolete. Moved to FROI_OPTIONS.     this.UseDefCarrier=rdr.GetInt32("USE_DEF_CARRIER");
                    if (rdr.IsDBNull("USETANDE"))    // JP 11.07.2005  Check for null. USETANDE field can be smallint or int so we need to do this instead of a Getxxxx function.
                        //this.UseTAndE = false;
                        htProp["UseTAndE"] = false;// JP 11.07.2005
                    else                             // JP 11.07.2005
                        //this.UseTAndE=Convert.ToBoolean(rdr.GetValue("USETANDE"));
                        htProp["UseTAndE"] = Convert.ToBoolean(rdr.GetValue("USETANDE"));

                    //this.RateLevel=rdr.GetInt32("RATE_LEVEL");
                    htProp["RateLevel"] = rdr.GetInt32("RATE_LEVEL");
                    //this.TandeAcctId=rdr.GetInt32("TANDEACCT_ID");
                    htProp["TandeAcctId"] = rdr.GetInt32("TANDEACCT_ID");
                    //this.FroiPreparer=Convert.ToBoolean(rdr.GetInt16("FROI_PREPARER"));
                    htProp["FroiPreparer"] = Convert.ToBoolean(rdr.GetInt16("FROI_PREPARER"));
                    //this.FreezeDateFlag=rdr.GetInt32("FREEZE_DATE_FLAG");
                    htProp["FreezeDateFlag"] = rdr.GetInt32("FREEZE_DATE_FLAG");
                    //this.DateAdjusterFlag=rdr.GetInt32("DATE_ADJUSTER_FLAG");
                    htProp["DateAdjusterFlag"] = rdr.GetInt32("DATE_ADJUSTER_FLAG");
                    //this.FroiBatHighLvl=rdr.GetInt32("FROI_BAT_HIGH_LVL");
                    htProp["FroiBatHighLvl"] = rdr.GetInt32("FROI_BAT_HIGH_LVL");
                    //this.FroiBatLowLvl=rdr.GetInt32("FROI_BAT_LOW_LVL");
                    htProp["FroiBatLowLvl"] = rdr.GetInt32("FROI_BAT_LOW_LVL");
                    //this.FroiBatCldClm=rdr.GetInt32("FROI_BAT_CLD_CLM");
                    htProp["FroiBatCldClm"] = rdr.GetInt32("FROI_BAT_CLD_CLM");
                    //this.AutoNumWCEmp=Convert.ToBoolean(rdr.GetInt16("AUTO_NUM_WC_EMP"));
                    htProp["AutoNumWCEmp"] = Convert.ToBoolean(rdr.GetInt16("AUTO_NUM_WC_EMP"));
                    //this.WCEmpPrefix=rdr.GetString("WC_EMP_PREFIX");
                    htProp["WCEmpPrefix"] = rdr.GetString("WC_EMP_PREFIX");
                    //this.EditWCEmpNum=Convert.ToBoolean(rdr.GetInt16("EDIT_WC_EMP_NUM"));
                    htProp["EditWCEmpNum"] = Convert.ToBoolean(rdr.GetInt16("EDIT_WC_EMP_NUM"));
                    //this.AutoNumVin=Convert.ToBoolean(rdr.GetInt16("AUTO_NUM_VIN"));
                    htProp["AutoNumVin"] = Convert.ToBoolean(rdr.GetInt16("AUTO_NUM_VIN"));
                    //this.AutoSelectPolicy=Convert.ToBoolean(rdr.GetInt16("AUTO_SELECT_POLICY"));
                    htProp["AutoSelectPolicy"] = Convert.ToBoolean(rdr.GetInt16("AUTO_SELECT_POLICY"));
                    //this.PrevResBackdating=Convert.ToBoolean(rdr.GetInt16("PREV_RES_BACKDATING"));
                    htProp["PrevResBackdating"] = Convert.ToBoolean(rdr.GetInt16("PREV_RES_BACKDATING"));
                    //this.PrevResModifyzero=Convert.ToBoolean(rdr.GetInt16("PREV_RES_MODIFYZERO"));
                    htProp["PrevResModifyzero"] = Convert.ToBoolean(rdr.GetInt16("PREV_RES_MODIFYZERO"));
                    //this.DiaryOrgLevel=rdr.GetInt32("DIARY_ORG_LEVEL");
                    htProp["DiaryOrgLevel"] = rdr.GetInt32("DIARY_ORG_LEVEL");
                    //this.AllowGlobalPeek=rdr.GetInt32("ALLOW_GLOBAL_PEEK");
                    htProp["AllowGlobalPeek"] = rdr.GetInt32("ALLOW_GLOBAL_PEEK");
                    //this.AllowTDFilter = rdr.GetInt32("ALLOW_TD_FILTER");
                    htProp["AllowTDFilter"] = rdr.GetInt32("ALLOW_TD_FILTER");
                    //this.ResShowOrphans=rdr.GetInt32("RES_SHOW_ORPHANS");
                    htProp["ResShowOrphans"] = rdr.GetInt32("RES_SHOW_ORPHANS");
                    //this.NoSubDepCheck=rdr.GetInt32("NO_SUB_DEP_CHECK");
                    htProp["NoSubDepCheck"] = rdr.GetInt32("NO_SUB_DEP_CHECK");
                    //this.m_iRowId=rdr.GetInt32("ROW_ID");
                    htProp["m_iRowId"] = rdr.GetInt32("ROW_ID");
                    //this.DeleteAllClaimDiaries = rdr.GetInt16("DELETE_ALL_CLAIM_DIARIES");
                    htProp["DeleteAllClaimDiaries"] = rdr.GetInt16("DELETE_ALL_CLAIM_DIARIES");

					htProp["UseDiariesDen"] = Convert.ToBoolean(rdr.GetInt16("REVIEWED_DIARIES_DEN"));//Add by kuladeep for mits:25083

                    //for newer fields, check if they exist before reading from/writing to them
                    rows = m_DataTable.Select("ColumnName='PRINT_OSHA_DESC'");
                    if (rows.GetLength(0) == 1)
                        //this.PrintOSHADesc=Convert.ToBoolean(rdr.GetInt32("PRINT_OSHA_DESC"));
                        htProp["PrintOSHADesc"] = Convert.ToBoolean(rdr.GetInt32("PRINT_OSHA_DESC"));

                    rows = m_DataTable.Select("ColumnName='HIERARCHY_LEVEL'");
                    if (rows.GetLength(0) == 1)
                        //this.HierarchyLevel=rdr.GetString("HIERARCHY_LEVEL");
                        htProp["HierarchyLevel"] = rdr.GetString("HIERARCHY_LEVEL");

                    rows = m_DataTable.Select("ColumnName='PRINT_CLAIMS_ADMIN'");
                    if (rows.GetLength(0) == 1)
                        //this.PrintClaimsAdmin=rdr.GetInt32("PRINT_CLAIMS_ADMIN");
                        htProp["PrintClaimsAdmin"] = rdr.GetInt32("PRINT_CLAIMS_ADMIN");

                    rows = m_DataTable.Select("ColumnName='BLANK_FIELD_FLAG'");
                    if (rows.GetLength(0) == 1)
                        //this.BlankFieldFlag=rdr.GetString("BLANK_FIELD_FLAG");
                        htProp["BlankFieldFlag"] = rdr.GetString("BLANK_FIELD_FLAG");
                    rows = m_DataTable.Select("ColumnName='FIN_HIST_EVAL'");
                    if (rows.GetLength(0) == 1)
                        //this.FinHostEval=rdr.GetInt16("FIN_HIST_EVAL");
                        htProp["FinHostEval"] = rdr.GetInt16("FIN_HIST_EVAL");

                    //added by rkaur7 for utility changes of policy
                    rows = m_DataTable.Select("ColumnName='STATE_EVAL'");
                    if (rows.GetLength(0) == 1)
                        //this.StateEval = rdr.GetInt16("STATE_EVAL");
                        htProp["StateEval"] = rdr.GetInt16("STATE_EVAL");
                    //end rkaur7


                    //parag change start
                    rows = m_DataTable.Select("ColumnName='WC_PRINTER_SETUP'");
                    if (rows.GetLength(0) == 1)
                        //this.WCPrinterSetup=rdr.GetString("WC_PRINTER_SETUP");
                        htProp["WCPrinterSetup"] = rdr.GetString("WC_PRINTER_SETUP");
                    rows = m_DataTable.Select("ColumnName='X_AXIS'");
                    if (rows.GetLength(0) == 1)
                        //this.XAxis=rdr.GetDouble("X_AXIS");
                        htProp["XAxis"] = rdr.GetDouble("X_AXIS");
                    rows = m_DataTable.Select("ColumnName='Y_AXIS'");
                    if (rows.GetLength(0) == 1)
                        //this.YAxis=rdr.GetDouble("Y_AXIS");
                        htProp["YAxis"] = rdr.GetDouble("Y_AXIS");
                    rows = m_DataTable.Select("ColumnName='SECURITY_FLAG'");
                    if (rows.GetLength(0) == 1)
                        //this.SecurityFlag=rdr.GetInt16("SECURITY_FLAG");
                        htProp["SecurityFlag"] = rdr.GetInt16("SECURITY_FLAG");
                    rows = m_DataTable.Select("ColumnName='DEFAULT_TIME_FLAG'");
                    if (rows.GetLength(0) == 1)
                        //this.DefaultTimeFlag=rdr.GetInt16("DEFAULT_TIME_FLAG");
                        htProp["DefaultTimeFlag"] = rdr.GetInt16("DEFAULT_TIME_FLAG");
                    rows = m_DataTable.Select("ColumnName='DEFAULT_DEPT_FLAG'");
                    if (rows.GetLength(0) == 1)
                        //this.DefaultDeptFlag=rdr.GetInt16("DEFAULT_DEPT_FLAG");
                        htProp["DefaultDeptFlag"] = rdr.GetInt16("DEFAULT_DEPT_FLAG");
                    rows = m_DataTable.Select("ColumnName='FILTER_OTH_PEOPLE'");
                    if (rows.GetLength(0) == 1)
                        //this.FilterOthPeople=rdr.GetInt32("FILTER_OTH_PEOPLE");
                        htProp["FilterOthPeople"] = rdr.GetInt32("FILTER_OTH_PEOPLE");
                    rows = m_DataTable.Select("ColumnName='FREEZE_EVENT_DESC'");
                    if (rows.GetLength(0) == 1)
                        //this.FreezeEventDesc=rdr.GetInt16("FREEZE_EVENT_DESC");
                        htProp["FreezeEventDesc"] = rdr.GetInt16("FREEZE_EVENT_DESC");
                    rows = m_DataTable.Select("ColumnName='FREEZE_LOC_DESC'");
                    if (rows.GetLength(0) == 1)
                        //this.FreezeLocDesc=rdr.GetInt16("FREEZE_LOC_DESC");
                        htProp["FreezeLocDesc"] = rdr.GetInt16("FREEZE_LOC_DESC");
                    // Return type changed from int to bool.
                    rows = m_DataTable.Select("ColumnName='USE_MAST_BANK_ACCT'");
                    if (rows.GetLength(0) == 1)
                        //this.UseMastBankAcct=Convert.ToBoolean(rdr.GetInt16("USE_MAST_BANK_ACCT"));
                        htProp["UseMastBankAcct"] = Convert.ToBoolean(rdr.GetInt16("USE_MAST_BANK_ACCT"));
                    //added by stara Mits 16667 05/14/09
                    rows = m_DataTable.Select("ColumnName='USE_RES_FILTER'");
                    if (rows.GetLength(0) == 1)
                        //this.UseResFilter = Convert.ToBoolean(rdr.GetInt16("USE_RES_FILTER"));
                        htProp["UseResFilter"] = Convert.ToBoolean(rdr.GetInt16("USE_RES_FILTER"));
                    //ends here

                    rows = m_DataTable.Select("ColumnName='ASSIGN_SUB_ACCOUNT'");
                    if (rows.GetLength(0) == 1)
                        //this.AssignCheckStockToSubAcc=Convert.ToBoolean(rdr.GetInt16("ASSIGN_SUB_ACCOUNT"));
                        htProp["AssignCheckStockToSubAcc"] = Convert.ToBoolean(rdr.GetInt16("ASSIGN_SUB_ACCOUNT"));
                    rows = m_DataTable.Select("ColumnName='FBBAL_DT_OPT'");
                    if (rows.GetLength(0) == 1)
                        //	this.FundsAcountBalanceDateCriteria=rdr.GetInt16("FBBAL_DT_OPT");
                        htProp["FundsAcountBalanceDateCriteria"] = rdr.GetInt16("FBBAL_DT_OPT");

                    rows = m_DataTable.Select("ColumnName='P2_LOGIN'");
                    if (rows.GetLength(0) == 1)
                        //this.P2Login=rdr.GetString("P2_LOGIN");
                        htProp["P2Login"] = rdr.GetString("P2_LOGIN");
                    rows = m_DataTable.Select("ColumnName='P2_PASS'");
                    if (rows.GetLength(0) == 1)
                        //this.P2Pass=rdr.GetString("P2_PASS");
                        htProp["P2Pass"] = rdr.GetString("P2_PASS");
                    rows = m_DataTable.Select("ColumnName='P2_URL'");
                    if (rows.GetLength(0) == 1)
                        //this.P2Url=rdr.GetString("P2_URL");
                        htProp["P2Url"] = rdr.GetString("P2_URL");
				//mbahl3 Strataware enhancement
                    rows = m_DataTable.Select("ColumnName='SERVICEURL'");
                    if (rows.GetLength(0) == 1)
                        htProp["StratwareServiceUrl"] = rdr.GetString("SERVICEURL");
                    rows = m_DataTable.Select("ColumnName='CONSUMERURL'");
                    if (rows.GetLength(0) == 1)
                        htProp["StratawareConsumerUrl"] = rdr.GetString("CONSUMERURL");
				//mbahl3 Strataware enhancement
                    rows = m_DataTable.Select("ColumnName='DATE_EVENT_DESC'");
                    if (rows.GetLength(0) == 1)
                        //this.DateEventDesc=rdr.GetInt16("DATE_EVENT_DESC");
                        htProp["DateEventDesc"] = rdr.GetInt16("DATE_EVENT_DESC");
                    rows = m_DataTable.Select("ColumnName='ORDER_BANK_ACC'");
                    if (rows.GetLength(0) == 1)
                        //this.OrderBankAcc=rdr.GetInt32("ORDER_BANK_ACC");
                        htProp["OrderBankAcc"] = rdr.GetInt32("ORDER_BANK_ACC");
                    rows = m_DataTable.Select("ColumnName='SUPP_FLD_SEC_FLAG'");
                    if (rows.GetLength(0) == 1)
                        //	this.SuppFldSecFlag=rdr.GetInt16("SUPP_FLD_SEC_FLAG");
                        htProp["SuppFldSecFlag"] = rdr.GetInt16("SUPP_FLD_SEC_FLAG");
                    rows = m_DataTable.Select("ColumnName='ENH_HIERARCHY_LIST'");
                    if (rows.GetLength(0) == 1)
                        //this.EnhHierarchyList=rdr.GetInt32("ENH_HIERARCHY_LIST");
                        htProp["EnhHierarchyList"] = rdr.GetInt32("ENH_HIERARCHY_LIST");
                    rows = m_DataTable.Select("ColumnName='EV_CAPTION_LEVEL'");
                    if (rows.GetLength(0) == 1)
                        //this.EvCaptionLevel=rdr.GetInt32("EV_CAPTION_LEVEL");
                        htProp["EvCaptionLevel"] = rdr.GetInt32("EV_CAPTION_LEVEL");
                    ////yatharth START
                    //if (rows.GetLength(0)==1)
                    //    this.OrgTimeZoneLevel=rdr.GetInt32("ORG_TIMEZONE_LEVEL");
                    ////yatharth END
                    rows = m_DataTable.Select("ColumnName='USE_ENH_POL_FLAG'");
                    if (rows.GetLength(0) == 1)
                        //this.UseEnhPolFlag=rdr.GetInt32("USE_ENH_POL_FLAG");
                        htProp["UseEnhPolFlag"] = rdr.GetInt32("USE_ENH_POL_FLAG");
                    //Geeta: Adding flag for Enhance Policy starts
                    rows = m_DataTable.Select("ColumnName='USE_BILLING_FLAG'");
                    if (rows.GetLength(0) == 1)
                        //this.UseBillingFlag  = rdr.GetInt32("USE_BILLING_FLAG");
                        htProp["UseBillingFlag"] = rdr.GetInt32("USE_BILLING_FLAG");
                    //Adding flag for Enhance Policy ends
                    rows = m_DataTable.Select("ColumnName='ORACLE_CASE_INS'");
                    if (rows.GetLength(0) == 1)
                        //this.OracleCaseIns=rdr.GetInt16("ORACLE_CASE_INS");
                        htProp["OracleCaseIns"] = rdr.GetInt16("ORACLE_CASE_INS");
                    rows = m_DataTable.Select("ColumnName='USE_ACROSOFT_INTERFACE'");
                    if (rows.GetLength(0) == 1)
                        //	this.UseAcrosoftInterface=Convert.ToBoolean(rdr.GetInt32("USE_ACROSOFT_INTERFACE"));
                        htProp["UseAcrosoftInterface"] = Convert.ToBoolean(rdr.GetInt32("USE_ACROSOFT_INTERFACE"));
                    rows = m_DataTable.Select("ColumnName='MULTIPLE_INSURER'");// Mihika 01/12/2007 MITS 8656
                    if (rows.GetLength(0) == 1)
                        //this.MultipleInsurer = Convert.ToBoolean(rdr.GetInt16("MULTIPLE_INSURER"));
                        htProp["MultipleInsurer"] = Convert.ToBoolean(rdr.GetInt16("MULTIPLE_INSURER"));
                    rows = m_DataTable.Select("ColumnName='MULTIPLE_REINSURER'"); //added by Navdeep for Boeing Changes in Base
                    if (rows.GetLength(0) == 1)
                        //this.MultipleReInsurer = Convert.ToBoolean(rdr.GetInt16("MULTIPLE_REINSURER"));
                        htProp["MultipleReInsurer"] = Convert.ToBoolean(rdr.GetInt16("MULTIPLE_REINSURER"));

                    rows = m_DataTable.Select("ColumnName='POLICY_DROP_DOWN'");// Mihika 01/17/2007 MITS 8657
                    if (rows.GetLength(0) == 1)
                        //this.PolicyDropDown = Convert.ToBoolean(rdr.GetInt16("POLICY_DROP_DOWN"));
                        htProp["PolicyDropDown"] = Convert.ToBoolean(rdr.GetInt16("POLICY_DROP_DOWN"));
                    //Shruti for FL Max Rate
                    rows = m_DataTable.Select("ColumnName='USE_FL_WC_MAX_FLAG'");
                    if (rows.GetLength(0) == 1)
                        //this.UseFLMaxRate = rdr.GetInt16("USE_FL_WC_MAX_FLAG");
                        htProp["UseFLMaxRate"] = rdr.GetInt16("USE_FL_WC_MAX_FLAG");
                    //Nadim 12820 for Enhance Notes Print Order
                    rows = m_DataTable.Select("ColumnName='ENH_PRINT_ORDER1'");
                    if (rows.GetLength(0) == 1)
                        //this.EnhPrintOrder1 = rdr.GetInt16("ENH_PRINT_ORDER1");
                        htProp["EnhPrintOrder1"] = rdr.GetInt16("ENH_PRINT_ORDER1");
                    rows = m_DataTable.Select("ColumnName='ENH_PRINT_ORDER2'");
                    if (rows.GetLength(0) == 1)
                        //  this.EnhPrintOrder2 = rdr.GetInt16("ENH_PRINT_ORDER2");
                        htProp["EnhPrintOrder2"] = rdr.GetInt16("ENH_PRINT_ORDER2");
                    rows = m_DataTable.Select("ColumnName='ENH_PRINT_ORDER3'");
                    if (rows.GetLength(0) == 1)
                        // this.EnhPrintOrder3 = rdr.GetInt16("EnhPrintOrder3");
                        htProp["EnhPrintOrder3"] = rdr.GetInt16("ENH_PRINT_ORDER3");
                    //Nadim 12820 for Enhance Notes Print Order
                    //Changed by Gagan for MITS 12334 : Start
                    rows = m_DataTable.Select("ColumnName='FRZ_EXISTING_NOTES'");
                    if (rows.GetLength(0) == 1)
                        //this.FreezeExistingNotes = rdr.GetInt16("FRZ_EXISTING_NOTES");
                        htProp["FreezeExistingNotes"] = rdr.GetInt16("FRZ_EXISTING_NOTES");
                    //Changed by Gagan for MITS 12334 : End
                    //Animesh Inserted RMSC Bill Review  //skhare7 RMSC Merge 28397
                        rows = m_DataTable.Select("ColumnName='USE_BILL_REV_FEE'");
                        if (rows.GetLength(0) == 1)
                        htProp["UseBillReview"] = Convert.ToBoolean(rdr.GetInt16("USE_BILL_REV_FEE"));
                          //  this.UseBillReview = Convert.ToBoolean(rdr.GetInt16("USE_BILL_REV_FEE"));
                        rows = m_DataTable.Select("ColumnName='ORG_BILL_REV_FEE'");
                        if (rows.GetLength(0) == 1)
                        htProp["OrgBillLevel"] = rdr.GetInt16("ORG_BILL_REV_FEE");
                          //  this.OrgBillLevel = rdr.GetInt16("ORG_BILL_REV_FEE");
                        rows = m_DataTable.Select("ColumnName='ORG_BILL_REV_FEE_EID'");
                        if (rows.GetLength(0) == 1)
                            htProp["OrgBillLevelEntityId"] = rdr.GetInt16("ORG_BILL_REV_FEE_EID");
                        //Animesh insertion Ends //skhare7 RMSC merge //skhare7 RMSC Merge 28397
                    rows = m_DataTable.Select("ColumnName='CLM_LTR_FLAG'"); //added by Navdeep for Chubb
                    if (rows.GetLength(0) == 1)
                        //this.ClaimLetterFlag  = Convert.ToBoolean(rdr.GetInt16("CLM_LTR_FLAG"));
                        htProp["ClaimLetterFlag"] = Convert.ToBoolean(rdr.GetInt16("CLM_LTR_FLAG"));
                    //added by ClaimLetterFlag for Chubb - Auto FROI ACORD 
                    rows = m_DataTable.Select("ColumnName='AUTO_FROIACORD_FLAG'"); //added by Navdeep for Chubb
                    if (rows.GetLength(0) == 1)
                        //this.AutoFROIACORDFlag = Convert.ToBoolean(rdr.GetInt16("AUTO_FROIACORD_FLAG"));
                        htProp["AutoFROIACORDFlag"] = Convert.ToBoolean(rdr.GetInt16("AUTO_FROIACORD_FLAG"));
                    rows = m_DataTable.Select("ColumnName='HOSP_PHYS_ANY_ENT'");
                    if (rows.GetLength(0) == 1)
                        //this.PhysicianSearchFlag = Convert.ToBoolean(rdr.GetInt16("HOSP_PHYS_ANY_ENT"));
                        htProp["PhysicianSearchFlag"] = Convert.ToBoolean(rdr.GetInt16("HOSP_PHYS_ANY_ENT"));
                    //Gagan Safeway Retrofit Policy Jursidiction : START
                    rows = m_DataTable.Select("ColumnName='SHOW_JURISDICTION'");
                    if (rows.GetLength(0) == 1)
                        //this.ShowJurisdiction = rdr.GetInt16("SHOW_JURISDICTION");
                        htProp["ShowJurisdiction"] = rdr.GetInt16("SHOW_JURISDICTION");
                    //Gagan Safeway Retrofit Policy Jursidiction : END
                    //Raman 03/08/2009 : R6 Work Loss / Restriction
                    rows = m_DataTable.Select("ColumnName='CREATE_WL_RES_RCDS'");
                    if (rows.GetLength(0) == 1)
                        //this.AutoCrtWrkLossRest = Convert.ToBoolean(rdr.GetInt32("CREATE_WL_RES_RCDS"));
                        htProp["AutoCrtWrkLossRest"] = Convert.ToBoolean(rdr.GetInt32("CREATE_WL_RES_RCDS"));

                    rows = m_DataTable.Select("ColumnName='USE_LEGACY_COMMENTS'");
                    if (rows.GetLength(0) == 1)
                        //this.UseLegacyComments = Convert.ToBoolean(rdr.GetInt32("USE_LEGACY_COMMENTS"));
                        htProp["UseLegacyComments"] = Convert.ToBoolean(rdr.GetInt32("USE_LEGACY_COMMENTS"));
                    //Added Rakhi for R7:Add Emp Data Elements
                    //rows = m_DataTable.Select("ColumnName='USE_MUL_ADDRESSES'");
                    //if (rows.GetLength(0) == 1)
                    //    this.UseMultipleAddresses = Convert.ToBoolean(rdr.GetInt32("USE_MUL_ADDRESSES"));
                    //Added Rakhi for R7:Add Emp Data Elements
                    //Start by Shivendu for Outlook email Utility Setting
                    //rows = m_DataTable.Select("ColumnName='USE_OUTLOOK'");
                    //if (rows.GetLength(0) == 1)
                    //    this.UseOutlook = Convert.ToBoolean(rdr.GetInt32("USE_OUTLOOK"));
                    //End by Shivendu for Outlook email Utility Setting
                    //Geeta 04/15/10 : R7 Task Manager Diary Starts
                    //rows = m_DataTable.Select("ColumnName='TASK_MGR_DIARY'");
                    //if (rows.GetLength(0) == 1)
                    //    this.TaskMgrDiary = Convert.ToBoolean(rdr.GetInt32("TASK_MGR_DIARY"));                   
                    //Geeta : R7 Task Manager Diary Ends
                    //rsushilaggar 06/02/2010 MITS 20606
                    rows = m_DataTable.Select("ColumnName='RMX_LSS_ENABLE'");
                    if (rows.GetLength(0) == 1)
                        //this.RMXLSSEnable = Convert.ToBoolean(rdr.GetInt16("RMX_LSS_ENABLE"));
                        htProp["RMXLSSEnable"] = Convert.ToBoolean(rdr.GetInt32("RMX_LSS_ENABLE"));
                    //end rsushilaggar
                    //parag change end
                }//while

                this.DataChanged = false;
                blnDataLoaded = true;
                //smishra25:Checked in for MITS 24087
                if (m_DBForSettings.ContainsKey(m_sConnectString))
                {
                    m_DBForSettings.Remove(m_sConnectString);
                }
                m_DBForSettings.Add(m_sConnectString, htProp);
                
            } // using

            return blnDataLoaded;
        }//method: LoadSettings()

        /// <summary>Client name.</summary>
        private string m_sClientName;
        /// <summary>OC Fac Address 1.</summary>
        private string m_sOCFacAddr1;
        /// <summary>OC Fac Address 2.</summary>
        private string m_sOCFacAddr2;
        /// <summary>OC Fac Address 3.</summary>
        private string m_sOCFacAddr3;
        /// <summary>OC Fac Address 4.</summary>
        private string m_sOCFacAddr4;
        /// <summary>OC Fac Contact.</summary>
        private string m_sOCFacContact;
        private string m_sOCFacHCFANo;
        /// <summary>OC Fac Name.</summary>
        private string m_sOCFacName;
        /// <summary>OC Fac Phone.</summary>
        private string m_sOCFacPhone;
        /// <summary>Work Monday.</summary>
        private bool m_bWorkMon;
        /// <summary>Work Tuesday.</summary>
        private bool m_bWorkTue;
        /// <summary>Work Wednesday.</summary>
        private bool m_bWorkWed;
        /// <summary>Work Thursday.</summary>
        private bool m_bWorkThu;
        /// <summary>Work Friday.</summary>
        private bool m_bWorkFri;
        /// <summary>Work Saturday.</summary>
        private bool m_bWorkSat;
        /// <summary>Work Sunday.</summary>
        private bool m_bWorkSun;
        /// <summary>Date stamp flag.</summary>
        private bool m_bDateStampFlag;
        /// <summary>Freeze text flag.</summary>
        private bool m_bFreezeTextFlag;
        /// <summary>Shift 1 name.</summary>
        private string m_sShift1Name;
        /// <summary>Shift 2 name.</summary>
        private string m_sShift2Name;
        /// <summary>Shift 3 name.</summary>
        private string m_sShift3Name;
        /// <summary>Shift 1 start.</summary>
        private string m_sShift1Start;
        /// <summary>Shift 2 start.</summary>
        private string m_sShift2Start;
        /// <summary>Shift 3 start.</summary>
        private string m_sShift3Start;
        /// <summary>Shift 1 end.</summary>
        private string m_sShift1End;
        /// <summary>Shift 2 end.</summary>
        private string m_sShift2End;
        /// <summary>Shift 3 end.</summary>
        private string m_sShift3End;
        private int m_iESSP;
        /// <summary>Client status code.</summary>
        private int m_iClientStatusCode;
        /// <summary>Install date.</summary>
        private string m_sDateInstalled;
        /// <summary>Address 1.</summary>
        private string m_sAddr1;
        /// <summary>Address 2.</summary>
        private string m_sAddr2;
        /// <summary>Address 3.</summary>
        //private string m_sAddr3;
        ///// <summary>Address 4.</summary>
        //private string m_sAddr4;
        /// <summary>City.</summary>
        private string m_sCity;
        /// <summary>State.</summary>
        private string m_sState;
        /// <summary>Zipcode.</summary>
        private string m_sZipCode;
        /// <summary>Phone number.</summary>
        private string m_sPhoneNumber;
        /// <summary>Fax number.</summary>
        private string m_sFaxNumber;
        /// <summary>Primary contact.</summary>
        private string m_sPrimaryContact;
        /// <summary>Primary title.</summary>
        private string m_sPrimaryTitle;
        /// <summary>Secondary contact.</summary>
        private string m_sSecondaryContact;
        /// <summary>Secondary title.</summary>
        private string m_sSecondaryTitle;
        /// <summary>Data entry contact.</summary>
        private string m_sDataEntryContact;
        /// <summary>Data entry title.</summary>
        private string m_sDataEntryTitle;
        /// <summary>Reporting contact.</summary>
        private string m_sReportingContact;
        /// <summary>Reporting title.</summary>
        private string m_sReportingTitle;
        /// <summary>Department manager contact.</summary>
        private string m_sDeptMgrContact;
        /// <summary>Department manager title.</summary>
        private string m_sDeptMgrTitle;
        /// <summary>Accounting contact.</summary>
        private string m_sAccountingContact;
        /// <summary>Accounting title.</summary>
        private string m_sAccountingTitle;
        /// <summary>DTG product.</summary>
        private string m_sDTGProduct;
        /// <summary>DTG version.</summary>
        private string m_sDTGVersion;
        /// <summary>Hardware.</summary>
        private string m_sHardware;
        /// <summary>Printer.</summary>
        private string m_sPrinter;
        /// <summary>Distribution media.</summary>
        private string m_sDistributionMedia;
        /// <summary>Modem phone.</summary>
        private string m_sModemPhone;
        /// <summary>DTG support phone.</summary>
        private string m_sDTGSupportPhone;
        /// <summary>Ev prefix.</summary>
        private string m_sEvPrefix;
        private bool m_bEvIncYearFlag;
        /// <summary>DB release number.</summary>
        private string m_sDBReleaseNumber;
        /// <summary>Funds account link.</summary>
        private bool m_bFundsAccountLink;
        /// <summary>Use funds sub account flag.</summary>
        private bool m_bUseFundsSubAcc;
        /// <summary>Print OSHA description flag.</summary>
        private bool m_bPrintOSHADesc;
        /// <summary>Heirarchy level.</summary>
        private string m_sHierarchyLevel;
        private int m_iPrintClaimsAdmin;
        private bool m_bEVTypeAccFlag;
        private bool m_bSMLimitDirect;
        private bool m_bResDoNotRecalc;
        /// <summary>SMTP server.</summary>
        private string m_sSMTPServer;
        private string m_sSenderAlternateDomain;
        private string m_sSenderReplytoDomain;
        private bool m_bExclHolidays;
        /// <summary>Use default carrier flag.</summary>
        private bool m_bUseDefaultCarrier;
        private bool m_bAttachDiaryVis;
        private int m_iUseDefCarrier;
        private bool m_bUseTAndE;
        /// <summary>Rate level.</summary>
        private int m_iRateLevel;
        private int m_iTandeAcctId;
        private bool m_bFroiPreparer;
        /// <summary>Freeze date flag.</summary>
        private int m_iFreezeDateFlag;
        /// <summary>Date adjuster flag.</summary>
        private int m_iDateAdjusterFlag;
        private int m_iFroiBatHighLvl;
        private int m_iFroiBatLowLvl;
        private int m_iFroiBatCldClm;
        private bool m_bAutoNumWCEmp;
        private string m_sWCEmpPrefix;
        private bool m_bMaskSSN;
        //RUPAL:R8
        private bool m_bPersonInvolvedPayeeType;
        private bool m_bEditWCEmpNum;
        //Debabrata Biswas Entity Payment Approval R6 retrofit MITS 19715 03/02/2010
        private bool m_bUseEntityApproval;
        //Debabrata Biswas 06/02/2010 MITS 20606
        private bool m_bShowLSSInvoice;
        private bool m_bRMXLSSEnable;
        private bool m_bAutoNumVin;
        private bool m_bAutoNumPin; //Mridul 10/26/09 MITS#18230
        private bool m_bAutoSelectPolicy;
        private bool m_bPrevResBackdating;
        private bool m_bPrevResModifyzero;
        private int m_iDiaryOrgLevel;
        private int m_iAllowGlobalPeek;
        private int m_iAllowTDFilter;
        private int m_iResShowOrphans;
        private int m_iNoSubDepCheck;
        private string m_sBlankFieldFlag;
        private int m_iFinHostEval;
        //added by rkaur7 for utility changes of policy
        private int m_iStateEval;
        //rkaur7 end
        private string m_sWCPrinterSetup;
        private double m_dXAxis;
        private double m_dYAxis;
        private int m_iSecurityFlag;
        private int m_iDefaultTimeFlag;
        private int m_iDefaultDeptFlag;
        private int m_iFilterOthPeople;
        private int m_iFreezeEventDesc;
        private int m_iFreezeLocDesc;
        private int m_iDeleteAllClaimDiaries;
        // Return type changed from int to bool.
        private bool m_bUseMastBankAcct;
        private bool m_bUseResFilter; //added by stara Mits 16667--05/14/09
        private bool m_bAssignCheckStockToSubAcc;
        private int m_iFundsAcountBalanceDateCriteria;

        private string m_sP2Login;
        private string m_sP2Pass;
        private string m_sP2Url;
        private int m_iDateEventDesc;
        private int m_iOrderBankAcc;
        private int m_iSuppFldSecFlag;
        private int m_iEnhHierarchyList;
        private int m_iEvCaptionLevel;
        //yatharth START
        private int m_iOrgTimeZoneLevel;
        //yatharth END
        private int m_iUseEnhPolFlag;
        //Geeta : Added for Enhance Policy
        private int m_iUseBillingFlag;
        private int m_iOracleCaseIns;
        private bool m_bUseAcrosoftInterface;

        //Shruti for FL Max Rate
        private int m_bUseFLMaxRate;
        //End
        //Nadim 12820 for Enhance Note Print order
        private int m_iEnhPrintOrder1;
        private int m_iEnhPrintOrder2;
        private int m_iEnhPrintOrder3;
        //nadim

        //Changed by Gagan for MITS 12334 : Start
        private int m_iFreezeExistingNotes;
        //Changed by Gagan for MITS 12334 : End
        //rkaur7 MITS 16668 : End        
        //Gagan Safeway Retrofit Policy Jursidiction : START
        private int m_iShowJurisdiction;
        //rkaur7 MITS 16668 : END

        //BOB Enhancement : ukusvaha
        private int m_iMultiCovgPerClm;
        private int m_iUnitStat;
        private int m_iAutoPopulateDpt;
        private int m_iAutoFillDpteid;

		// Mihika 01/12/2007 MITS 8656
		private bool m_bMultipleInsurer;
		// Mihika 01/17/2007 MITS 8657
		private bool m_bMultipleReInsurer; //added by Navdeep
		private bool m_bPolicyDropDown;
		//Raman 03/08/2009 : R6 Work Loss / Restriction
		private bool m_bAutoCrtWrkLossRest;
		private bool m_bClmLtrFlag;  //added by Navdeep for Chubb
		private bool m_bAutoFROIACORDFlag;  //added by Navdeep for Chubb - Auto FROI ACORD
		private bool m_bUseLegacyComments;
		private bool m_bUseMultipleAddresses;//Added Rakhi for R7:Add Emp Data Elements
		private bool m_bUseOutlook;//Added by Shivendu for Outlook email Utility Setting
        // Mihika 01/12/2007 MITS 8656
        //rbhatia4:R8: Use Media View Setting : July 05 2011
        private bool m_bUseMediaView;
        private int m_iFileSizeLimitForOutlook;//Added by Shivendu for Outlook email Utility Setting
        private bool m_bHistTrackEnable; //Geeta 04/15/10 : R7 Task Manager Diary
        private bool m_bFreezeSuppText;
        private bool m_bFreezePayOrderText; //jramkumar for MITS 32095
        private bool m_bHistTrackStatus; //Umesh for CDC
        private bool m_bTaskMgrDiary; //Umesh for CDC
        private bool m_bTaskMgrEmail; //tmalhotra3 JIRA 4613
        private bool m_bUsePaperVisionInterface; //Mona:PaperVisionMerge
        private bool m_bPhysicianSearchFlag;
        private bool m_bEnhNotesPolicyInClaim;  //added by neha goel-williams policy notes in claim
        private int m_iResultLookUp;
        //START (06/29/2010): Sumit -Filter Policy based on LOB
        private bool m_bFilterPolicyBasedOnLOB;
        //End: Sumit
        private bool m_bDoOfacCheck;//Added by Yatharth to do Background OFAC check
        private int m_iCodeUserLimit;
        private int m_iQualityMgtFlag;
        private int m_iAutoLaunchDiary;
        private int m_iResultPayeeCheckReview; //Records per Page for Payee Check Review Screen

        //START (07/02/2010): Sumit - Variables used for activation of BRS
        // akaushik5 Commented for MITS 37242 Starts
        //private static bool m_blnIsBRSEnabled;
        //private static string m_sStaticConnectString;
        // akaushik5 Commented for MITS 37242 Ends
        //End: Sumit

        //START (07/02/2010): Sumit -LOB Activation variables.
        private bool m_bUseGCLOB;
        private bool m_bUseDILOB;
        private bool m_bUsePCLOB;
        private bool m_bUseVALOB;
        private bool m_bUseWCLOB;
        private bool m_bAdjAutoAssign;
        //End: Sumit

        //Start 08/04/2010 mcapps2 - Add the ability to allow voids of cleared payments
        private bool m_bAllowVoidOfClrdPmnts;
        //End 08/04/2010 mcapps2 - Add the ability to allow voids of cleared payments

        //rsushilaggar MITS 21970 Date 10/01/2010
        private bool m_bLssChecksOnHold;

        private int iBaseCurrCode;
        private bool m_bUseScriptEditor;

        // npadhy Start MITS 21557 10/05/2010 Added Settings for Maximum Tiers and Selected Discounts
        private int m_iMaxDiscTier;
        private int m_iMaxDiscounts;
        // npadhy End MITS 21557 10/05/2010 Added Settings for Maximum Tiers and Selected Discounts

        private bool m_bUsePSOReportingSYS; //spahariya MITS 25646 08/05/11 use PSO reporting system setting
		
        //rsushilaggar Added this flag in SysSettings
        private int m_iCaseMgmtFlag;
        private int m_iUseMultiCurrency;

        private int m_iEnableVSS; //averma62 rmA-VSS integration
        private int m_iClaimRptType; //Ashish Ahuja: Claims Made Jira 1342
        private int m_iAdjAssignmentAutoDiary;   //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
        //Ankit Start : Worked on MITS - 32386 - FAS Integration
        private int m_iEnableFAS;
        private string m_sFileLocationSelection;
        private string m_sSharedLocation;
        private string m_sFASServer;
        private string m_sFASUserId;
        private string m_sFASPassword;
        private string m_sFASFolder;
        private string m_sFASSharedLocation;
        private string m_sFASFilelocation;
        //Ankit End
        private bool m_iNotifyAssigner;//igupta3 Mits:32846
        private bool m_iCurrentDateDiary;//igupta3 Mits:32846
        //Added by amitosh for 23476 (05/11/2011)
            private int m_iUseClaimantName;
        //nsachdeva2: mits:26418 - claim activity log
        private bool m_bClaimActLog;
        private bool m_bEditClaimEvtDate;//sharishkumar jira 12444
		    private bool m_bAddModifyAddress; //RMA-8753 nshah28
            private bool m_bAllowSummaryForBookedReserve;    //RMA-16584 nshah28
        //Added by amitosh for R8 enhancment of Policy interface
            private bool m_bUsePolicyInterface;
            private bool m_bUploadSuppToPolicy;
            private bool m_bPolicyLimitExceeded;
            private bool m_bUseCodeMapping;
        // Added by Nikhil on 07/31/14. New setting added to upload check total to point instead of funds amoount
        private bool m_bUploadCheckTotal;
            private bool m_bEnableTPAAccess;//JIRA RMA-718 ajohari2
            private bool m_bShowMediaViewButton;
            //nsachdeva2:  MITS 25163- Policy Interface Implementation - 1/6/2012 - Commented not required.
            //private int m_iMaxPolUnits;
            //private int m_iMaxPolEntities;
            //End MITS 25163
            private int m_iPolicyCvgType;
            private int m_iPayHistLimit; //mini performance mits 
            private int m_iPolicySystemUpdate;
            private bool m_bAllowPolicySearch;  //averma62 MITS 25163- Policy Interface Implementation

        //end Amitosh

            //mbahl3 mits 30224
            private bool m_bStatusActive;
            //mbahl3 mits 30224
            //mbahl3 strataware enhancement
            private int m_iStrataWare;
            private int m_iEnableSingleuser;
            private int m_StratawareUserID;
            private string m_StratwareServiceUrl;
            private string m_StratawareConsumerUrl;
        //mbahl3 strataware enhancement
            //Animesh Inserted RMSC Bill Review  //skhare7 RMSC Merge 28397
        private bool m_bUseBillReview;
        private int m_iOrgBillLevel;
        private int m_iOrgBillLevelEntityId;
        private bool m_bAddAdjAsPI; //spahariya MITS 28867 
        
        //Animesh insertion Ends//skhare7 RMSC merge
        private bool m_bUseDiariesDen; //Add by kuladeep for mits:25083
        //skhare7 Multilingual R8 
        //private int m_iCountry;
     
        //private int m_sPhoneFormat;
        //private int m_sZipcodeFormat;
        //private int m_sDateformat;
        //private int m_sTimeformat;
        //skhare7 Multilingual R8 End
        //smishra54: Point Policy Interface
        private int m_PolicyInterfaceSearchCount;
        private bool m_bOpenPointPolicy;		
		//Nitika Starts for FNOL Reserve
        private bool m_bFNOLReserve;
		//Nitika Ends for FNOL Reserve
		private bool m_bNoReqFieldsForPolicySearch; //neha goel 11212013 MITS#33414 for WWIG
        private bool m_bUseStagingPolicySystem; //neha goel 11212013 MITS#33414 for WWIG
        private bool m_bAllowNotesAtClaimant; //gbindra MITS#34104 for WWIG 01312014
      
        private int m_iClaimantLength; //Mits 36708
        
		private int m_SearchCodeDescription;//MITS:35039
        private bool m_bPolicyAutoExpansion;// ngupta73 MITS- 34260/RMA-4362
        private bool m_bUseEntityRole;//Jira 340 entity role
        //Added by nitin goel, MITS 33588,07/29/2014
        private int m_iUseInsuredClaimDept; //ddhiman updated var name MITS 33588
        //added by swati
        private bool m_bUseTPA;
        private bool m_bUseDCIFields;
        private bool m_bUseCLUEFields;
        //chanfe end here by swati
        private bool m_bUseNMVTISReqFields;//Added by agupta298 - PMC GAP08 - RMA-4694

        //tanwar2 - ImageRight - start
        private bool m_bIR;
        //tanwar2 - ImageRight end
        private bool m_bAllowAutoLogin; //JIRA 5504
        private bool m_bPowerViewReadOnly;//asharma326 JIRA 6411
        private bool m_bPayeeAddressSelect;//pkandhari JIRA 6421
        private bool m_bUseSilverlight;//Added by mmudabbir 36022
        /// <summary>
        ///Gets whether or not BRS has been activated/installed. Make this one to be static function
        ///because it does not need access to the database.
        /// </summary>
        /// <remarks>Removed dependency on the following Registry Key
        ///  (Registry.ClassesRoot.OpenSubKey("BRSWorld3.BRS")!=null);
        /// </remarks>
        // akaushik5 Changed for MITS 37242 Starts
        //public static bool IsBRSInstalled()
        public bool IsBRSInstalled()
        // akaushik5 Changed for MITS 37242 Ends
        {
            //START (07/02/2010): Sumit - This will now be activated based on the General System parameter Utility settings.

            //const string BRS_ACTIVATION_CODE = "RMX-BRS-10-ACT";
            bool blnSuccess = false;
            //bool blnIsBRSEnabled = false;
            //string strBRSActivationCode = RMConfigurationManager.GetNameValueSectionSettings("BillingReviewSystem")["BRSActivationCode"];

            ////Determine if the activation code matches what is required
            //if (string.Equals(strBRSActivationCode, BRS_ACTIVATION_CODE))
            //{
            //    blnIsBRSEnabled = true;
            //} // if
            // akaushik5 Changed for MITS 37242 Starts
            //SysSettings objSys = null;
            //objSys = new SysSettings(m_sStaticConnectString);

            //m_blnIsBRSEnabled = (Conversion.CastToType<Int32>(objSys.RetrieveSystemSettings("USE_BRS"), out blnSuccess)) == 0 ? false : true;
            //return m_blnIsBRSEnabled;
            return (Conversion.CastToType<Int32>(this.RetrieveSystemSettings("USE_BRS"), out blnSuccess)) == 0 ? false : true;
            // akaushik5 Changed for MITS 37242 Ends
            //End : Sumit
        }
        private string GetPropertyString(string prop)
        {
            Hashtable ht = (Hashtable)m_DBForSettings[m_sConnectString];
            if (!ht.ContainsKey(prop))
            {
                LoadSettings();
            }
            //smishra25: By pass  cases where the column does not exist MITS 24087
            if (ht[prop] != null)
            {
                return ht[prop].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        private bool GetPropertyBool(string prop)
        {
            bool bSuccess = false;
            bool bValue = false;
            Hashtable ht = (Hashtable)m_DBForSettings[m_sConnectString];
            if (!ht.ContainsKey(prop))
            {
                LoadSettings();
            }
            //smishra25: By pass  cases where the column does not exist MITS 24087
            if (ht[prop] != null)
            {
                bValue = Conversion.CastToType<bool>(ht[prop].ToString(), out bSuccess);
            }
            if (bSuccess)
                return bValue;
            else
                return false;
        }
        private int GetPropertyInt(string prop)
        {
            bool bSuccess = false;
            int iValue = 0;
            Hashtable ht = (Hashtable)m_DBForSettings[m_sConnectString];
            if (!ht.ContainsKey(prop))
            {
                LoadSettings();
            }
            //smishra25: By pass  cases where the column does not exist MITS 24087
            if (ht[prop] != null)
            {
                iValue = Conversion.CastToType<int>(ht[prop].ToString(), out bSuccess);
            }
            if (bSuccess)
                return iValue;
            else
                return 0;
        }
        private double GetPropertyDouble(string prop)
        {
            bool bSuccess = false;
            double dblValue = 0;
            Hashtable ht = (Hashtable)m_DBForSettings[m_sConnectString];
            if (!ht.ContainsKey(prop))
            {
                LoadSettings();
            }
            //smishra25: By pass  cases where the column does not exist MITS 24087
            if (ht[prop] != null)
            {
                dblValue = Conversion.CastToType<double>(ht[prop].ToString(), out bSuccess);
            }
            if (bSuccess)
                return dblValue;
            else
                return 0.0;
        }
        private Hashtable GetPropertyHT()
        {
            Hashtable ht = (Hashtable)m_DBForSettings[m_sConnectString];
            if (ht == null)
            {
                ht = new Hashtable();
            }
            return ht;
        }
        /// <summary>Stores whether the data has been modified or not.</summary>
        private bool m_bDataChanged;
        /// <summary>ID of the row of data.</summary>
        private int m_iRowId;

        /// <summary>
        ///Property to access m_sClientName.
        /// </summary>
        public string ClientName
        {
            get { return GetPropertyString("ClientName"); }
            set
            {
                if (m_sClientName != value)
                {
                    m_bDataChanged = true;
                    //Start - averma62 MITS 27436
                    //m_sClientName = value;
                    Hashtable htProp = GetPropertyHT();
                    htProp["ClientName"] = value;
                    //End - averma62 MITS 27436
                }
            }
        }

        /// <summary>
        ///Property to access m_sOCFacAddr1.
        /// </summary>
        public string OCFacAddr1
        {
            get { return GetPropertyString("OCFacAddr1"); }
            set
            {
                if (m_sOCFacAddr1 != value)
                {
                    m_bDataChanged = true;
                    //m_sOCFacAddr1 = value;
                    Hashtable htProp = GetPropertyHT();
                    htProp["OCFacAddr1"] = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sOCFacAddr2.
        /// </summary>
        public string OCFacAddr2
        {
            get { return GetPropertyString("OCFacAddr2"); }
            set
            {
                if (m_sOCFacAddr2 != value)
                {
                    m_bDataChanged = true;
                    //Start - averma62 MITS 27436
                    //m_sOCFacAddr2 = value;
                    Hashtable htProp = GetPropertyHT();
                    htProp["OCFacAddr2"] = value;
                    //End - averma62 MITS 27436
                }
            }
        }

        /// <summary>
        ///Property to access OCFacAddr3.
        /// </summary>
        public string OCFacAddr3
        {
            get { return GetPropertyString("OCFacAddr3"); }
            set
            {
                if (m_sOCFacAddr3 != value)
                {
                    m_bDataChanged = true;
                    //Start - averma62 MITS 27436
                    //m_sOCFacAddr3 = value;
                    Hashtable htProp = GetPropertyHT();
                    htProp["OCFacAddr3"] = value;
                    //End - averma62 MITS 27436
                }
            }
        }

        /// <summary>
        ///Property to access m_sOCFacAddr4.
        /// </summary>
        public string OCFacAddr4
        {
            get { return GetPropertyString("OCFacAddr4"); }
            set
            {
                if (m_sOCFacAddr4 != value)
                {
                    m_bDataChanged = true;
                    //Start - averma62 MITS 27436
                    //m_sOCFacAddr4 = value;
                    Hashtable htProp = GetPropertyHT();
                    htProp["OCFacAddr4"] = value;
                    //End - averma62 MITS 27436
                }
            }
        }

        /// <summary>
        ///Property to access m_sOCFacContact.
        /// </summary>
        public string OCFacContact
        {
            get { return GetPropertyString("OCFacContact"); }
            set
            {
                if (m_sOCFacContact != value)
                {
                    m_bDataChanged = true;
                    //Start - averma62 MITS 27436
                    //m_sOCFacContact = value;
                    Hashtable htProp = GetPropertyHT();
                    htProp["OCFacContact"] = value;
                    //End - averma62 MITS 27436
                }
            }
        }

        /// <summary>
        ///Property to access m_sOCFacHCFANo.
        /// </summary>
        public string OCFacHCFANo
        {
            get { return GetPropertyString("OCFacHCFANo"); }
            set
            {
                if (m_sOCFacHCFANo != value)
                {
                    m_bDataChanged = true;
                    //Start - averma62 MITS 27436
                    //m_sOCFacHCFANo = value;
                    Hashtable htProp = GetPropertyHT();
                    htProp["OCFacHCFANo"] = value;
                    //End - averma62 MITS 27436
                }
            }
        }

        /// <summary>
        ///Property to access m_sOCFacName.
        /// </summary>
        public string OCFacName
        {
            get { return GetPropertyString("OCFacName"); }
            set
            {
                if (m_sOCFacName != value)
                {
                    m_bDataChanged = true;
                    //Start - averma62 MITS 27436
                    //m_sOCFacName = value;
                    Hashtable htProp = GetPropertyHT();
                    htProp["OCFacName"] = value;
                    //End - averma62 MITS 27436
                }
            }
        }

        /// <summary>
        ///Property to access m_sOCFacPhone.
        /// </summary>
        public string OCFacPhone
        {
            get { return GetPropertyString("OCFacPhone"); }
            set
            {
                if (m_sOCFacPhone != value)
                {
                    m_bDataChanged = true;
                    //Start - averma62 MITS 27436
                    //m_sOCFacPhone = value;
                    Hashtable htProp = GetPropertyHT();
                    htProp["OCFacPhone"] = value;
                    //End - averma62 MITS 27436
                }
            }
        }
        //Start - averma62 MITS 27436
        public int RowId
        {
            get { return int.Parse(GetPropertyString("m_iRowId")); }
        }
        //End - averma62 MITS 27436
        /// <summary>
        ///Property to access m_bWorkMon.
        /// </summary>
        public bool WorkMon
        {
            get { return GetPropertyBool("WorkMon"); }
            set
            {
                if (m_bWorkMon != value)
                {
                    m_bDataChanged = true;
                    m_bWorkMon = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_bWorkTue.
        /// </summary>
        public bool WorkTue
        {
            get { return GetPropertyBool("WorkTue"); }
            set
            {
                if (m_bWorkTue != value)
                {
                    m_bDataChanged = true;
                    m_bWorkTue = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bWorkWed.
        /// </summary>
        public bool WorkWed
        {
            get { return GetPropertyBool("WorkWed"); }
            set
            {
                if (m_bWorkWed != value)
                {
                    m_bDataChanged = true;
                    m_bWorkWed = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bWorkThu.
        /// </summary>
        public bool WorkThu
        {
            get { return GetPropertyBool("WorkThu"); }
            set
            {
                if (m_bWorkThu != value)
                {
                    m_bDataChanged = true;
                    m_bWorkThu = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bWorkFri.
        /// </summary>
        public bool WorkFri
        {
            get { return GetPropertyBool("WorkFri"); }
            set
            {
                if (m_bWorkFri != value)
                {
                    m_bDataChanged = true;
                    m_bWorkFri = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bWorkSat.
        /// </summary>
        public bool WorkSat
        {
            get { return GetPropertyBool("WorkSat"); }
            set
            {
                if (m_bWorkSat != value)
                {
                    m_bDataChanged = true;
                    m_bWorkSat = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bWorkSun.
        /// </summary>
        public bool WorkSun
        {
            get { return GetPropertyBool("WorkSun"); }
            set
            {
                if (m_bWorkSun != value)
                {
                    m_bDataChanged = true;
                    m_bWorkSun = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bDateStampFlag.
        /// </summary>
        public bool DateStampFlag
        {
            get { return GetPropertyBool("DateStampFlag"); }
            set
            {
                if (m_bDateStampFlag != value)
                {
                    m_bDataChanged = true;
                    m_bDateStampFlag = value;
                }
            }

        }
        /// <summary>
        ///Property to access m_bFreezeTextFlag.
        /// </summary>
        public bool FreezeTextFlag
        {
            get { return GetPropertyBool("FreezeTextFlag"); }
            set
            {
                if (m_bFreezeTextFlag != value)
                {
                    m_bDataChanged = true;
                    m_bFreezeTextFlag = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_sShift1Name.
        /// </summary>
        public string Shift1Name
        {
            get { return GetPropertyString("Shift1Name"); }
            set
            {
                if (m_sShift1Name != value)
                {
                    m_bDataChanged = true;
                    m_sShift1Name = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_sShift2Name.
        /// </summary>
        public string Shift2Name
        {
            get { return GetPropertyString("Shift2Name"); }
            set
            {
                if (m_sShift2Name != value)
                {
                    m_bDataChanged = true;
                    m_sShift2Name = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_sShift3Name.
        /// </summary>
        public string Shift3Name
        {
            get { return GetPropertyString("Shift3Name"); }
            set
            {
                if (m_sShift3Name != value)
                {
                    m_bDataChanged = true;
                    m_sShift3Name = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_sShift1Start.
        /// </summary>
        public string Shift1Start
        {
            get { return GetPropertyString("Shift1Start"); }
            set
            {
                if (m_sShift1Start != value)
                {
                    m_bDataChanged = true;
                    m_sShift1Start = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sShift2Start.
        /// </summary>
        public string Shift2Start
        {
            get { return GetPropertyString("Shift2Start"); }
            set
            {
                if (m_sShift2Start != value)
                {
                    m_bDataChanged = true;
                    m_sShift2Start = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sShift3Start.
        /// </summary>
        public string Shift3Start
        {
            get { return GetPropertyString("Shift3Start"); }
            set
            {
                if (m_sShift3Start != value)
                {
                    m_bDataChanged = true;
                    m_sShift3Start = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sShift1End.
        /// </summary>
        public string Shift1End
        {
            get { return GetPropertyString("Shift1End"); }
            set
            {
                if (m_sShift1End != value)
                {
                    m_bDataChanged = true;
                    m_sShift1End = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sShift2End.
        /// </summary>
        public string Shift2End
        {
            get { return GetPropertyString("Shift2End"); }
            set
            {
                if (m_sShift2End != value)
                {
                    m_bDataChanged = true;
                    m_sShift2End = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sShift3End.
        /// </summary>
        public string Shift3End
        {
            get { return GetPropertyString("Shift3End"); }
            set
            {
                if (m_sShift3End != value)
                {
                    m_bDataChanged = true;
                    m_sShift3End = value;
                }
            }
        }
        /// <summary>
        ///Property to access m_iESSP.
        /// </summary>
        public int ESSP
        {
            get { return GetPropertyInt("ESSP"); }
            set
            {
                if (m_iESSP != value)
                {
                    m_bDataChanged = true;
                    m_iESSP = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iClientStatusCode.
        /// </summary>
        public int ClientStatusCode
        {
            get { return GetPropertyInt("ClientStatusCode"); }
            set
            {
                if (m_iClientStatusCode != value)
                {
                    m_bDataChanged = true;
                    m_iClientStatusCode = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_sDateInstalled.
        /// </summary>
        public string DateInstalled
        {
            get { return GetPropertyString("DateInstalled"); }
            set
            {
                if (m_sDateInstalled != value)
                {
                    m_bDataChanged = true;
                    m_sDateInstalled = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sAddr1.
        /// </summary>
        public string Addr1
        {
            get { return GetPropertyString("Addr1"); }
            set
            {
                if (m_sAddr1 != value)
                {
                    m_bDataChanged = true;
                    m_sAddr1 = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sAddr2.
        /// </summary>
        public string Addr2
        {
            get { return GetPropertyString("Addr2"); }
            set
            {
                if (m_sAddr2 != value)
                {
                    m_bDataChanged = true;
                    m_sAddr2 = value;
                }
            }
        }
        /// <summary>
        ///Property to access m_sAddr3.
        ///// </summary>
        //public string Addr3
        //{
        //    get { return GetPropertyString("Addr3"); }
        //    set
        //    {
        //        if (m_sAddr3 != value)
        //        {
        //            m_bDataChanged = true;
        //            m_sAddr3 = value;
        //        }
        //    }
        //}
        ///// <summary>
        /////Property to access m_sAddr4.
        ///// </summary>
        //public string Addr4
        //{
        //    get { return GetPropertyString("Addr4"); }
        //    set
        //    {
        //        if (m_sAddr4 != value)
        //        {
        //            m_bDataChanged = true;
        //            m_sAddr4 = value;
        //        }
        //    }
        //}

        /// <summary>
        ///Property to access m_sCity.
        /// </summary>
        public string City
        {
            get { return GetPropertyString("City"); }
            set
            {
                if (m_sCity != value)
                {
                    m_bDataChanged = true;
                    m_sCity = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sState.
        /// </summary>
        public string State
        {
            get { return GetPropertyString("State"); }
            set
            {
                if (m_sState != value)
                {
                    m_bDataChanged = true;
                    m_sState = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sZipCode.
        /// </summary>
        public string ZipCode
        {
            get { return GetPropertyString("ZipCode"); }
            set
            {
                if (m_sZipCode != value)
                {
                    m_bDataChanged = true;
                    m_sZipCode = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sPhoneNumber.
        /// </summary>
        public string PhoneNumber
        {
            get { return GetPropertyString("PhoneNumber"); }
            set
            {
                if (m_sPhoneNumber != value)
                {
                    m_bDataChanged = true;
                    m_sPhoneNumber = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sFaxNumber.
        /// </summary>
        public string FaxNumber
        {
            get { return GetPropertyString("FaxNumber"); }
            set
            {
                if (m_sFaxNumber != value)
                {
                    m_bDataChanged = true;
                    m_sFaxNumber = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sPrimaryContact.
        /// </summary>
        public string PrimaryContact
        {
            get { return GetPropertyString("PrimaryContact"); }
            set
            {
                if (m_sPrimaryContact != value)
                {
                    m_bDataChanged = true;
                    m_sPrimaryContact = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sPrimaryTitle.
        /// </summary>
        public string PrimaryTitle
        {
            get { return GetPropertyString("PrimaryTitle"); }
            set
            {
                if (m_sPrimaryTitle != value)
                {
                    m_bDataChanged = true;
                    m_sPrimaryTitle = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sSecondaryContact.
        /// </summary>
        public string SecondaryContact
        {
            get { return GetPropertyString("SecondaryContact"); }
            set
            {
                if (m_sSecondaryContact != value)
                {
                    m_bDataChanged = true;
                    m_sSecondaryContact = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sSecondaryTitle.
        /// </summary>
        public string SecondaryTitle
        {
            get { return GetPropertyString("SecondaryTitle"); }
            set
            {
                if (m_sSecondaryTitle != value)
                {
                    m_bDataChanged = true;
                    m_sSecondaryTitle = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sDataEntryContact.
        /// </summary>
        public string DataEntryContact
        {
            get { return GetPropertyString("DataEntryContact"); }
            set
            {
                if (m_sDataEntryContact != value)
                {
                    m_bDataChanged = true;
                    m_sDataEntryContact = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sDataEntryTitle.
        /// </summary>
        public string DataEntryTitle
        {
            get { return GetPropertyString("DataEntryTitle"); }
            set
            {
                if (m_sDataEntryTitle != value)
                {
                    m_bDataChanged = true;
                    m_sDataEntryTitle = value;
                }
            }
        }
        /// <summary>
        ///Property to access m_sReportingContact.
        /// </summary>
        public string ReportingContact
        {
            get { return GetPropertyString("ReportingContact"); }
            set
            {
                if (m_sReportingContact != value)
                {
                    m_bDataChanged = true;
                    m_sReportingContact = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sReportingTitle.
        /// </summary>
        public string ReportingTitle
        {
            get { return GetPropertyString("ReportingTitle"); }
            set
            {
                if (m_sReportingTitle != value)
                {
                    m_bDataChanged = true;
                    m_sReportingTitle = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sDeptMgrContact.
        /// </summary>
        public string DeptMgrContact
        {
            get { return GetPropertyString("DeptMgrContact"); }
            set
            {
                if (m_sDeptMgrContact != value)
                {
                    m_bDataChanged = true;
                    m_sDeptMgrContact = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sDeptMgrTitle.
        /// </summary>
        public string DeptMgrTitle
        {
            get { return GetPropertyString("DeptMgrTitle"); }
            set
            {
                if (m_sDeptMgrTitle != value)
                {
                    m_bDataChanged = true;
                    m_sDeptMgrTitle = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sAccountingContact.
        /// </summary>
        public string AccountingContact
        {
            get { return GetPropertyString("AccountingContact"); }
            set
            {
                if (m_sAccountingContact != value)
                {
                    m_bDataChanged = true;
                    m_sAccountingContact = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sAccountingTitle.
        /// </summary>
        public string AccountingTitle
        {
            get { return GetPropertyString("AccountingTitle"); }
            set
            {
                if (m_sAccountingTitle != value)
                {
                    m_bDataChanged = true;
                    m_sAccountingTitle = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sDTGProduct.
        /// </summary>
        public string DTGProduct
        {
            get { return GetPropertyString("DTGProduct"); }
            set
            {
                if (m_sDTGProduct != value)
                {
                    m_bDataChanged = true;
                    m_sDTGProduct = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sDTGVersion.
        /// </summary>
        public string DTGVersion
        {
            get { return GetPropertyString("DTGVersion"); }
            set
            {
                if (m_sDTGVersion != value)
                {
                    m_bDataChanged = true;
                    m_sDTGVersion = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sHardware.
        /// </summary>
        public string Hardware
        {
            get { return GetPropertyString("Hardware"); }
            set
            {
                if (m_sHardware != value)
                {
                    m_bDataChanged = true;
                    m_sHardware = value;
                }
            }
        }
        /// <summary>
        ///Property to access m_sPrinter.
        /// </summary>
        public string Printer
        {
            get { return GetPropertyString("Printer"); }
            set
            {
                if (m_sPrinter != value)
                {
                    m_bDataChanged = true;
                    m_sPrinter = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sDistributionMedia.
        /// </summary>
        public string DistributionMedia
        {
            get { return GetPropertyString("DistributionMedia"); }
            set
            {
                if (m_sDistributionMedia != value)
                {
                    m_bDataChanged = true;
                    m_sDistributionMedia = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sModemPhone.
        /// </summary>
        public string ModemPhone
        {
            get { return GetPropertyString("ModemPhone"); }
            set
            {
                if (m_sModemPhone != value)
                {
                    m_bDataChanged = true;
                    m_sModemPhone = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sDTGSupportPhone.
        /// </summary>
        public string DTGSupportPhone
        {
            get { return GetPropertyString("DTGSupportPhone"); }
            set
            {
                if (m_sDTGSupportPhone != value)
                {
                    m_bDataChanged = true;
                    m_sDTGSupportPhone = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sEvPrefix.
        /// </summary>
        public string EvPrefix
        {
            get { return GetPropertyString("EvPrefix"); }
            set
            {
                if (m_sEvPrefix != value)
                {
                    m_bDataChanged = true;
                    m_sEvPrefix = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_bEvIncYearFlag.
        /// </summary>
        public bool EvIncYearFlag
        {
            get { return GetPropertyBool("EvIncYearFlag"); }
            set
            {
                if (m_bEvIncYearFlag != value)
                {
                    m_bDataChanged = true;
                    m_bEvIncYearFlag = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_sDBReleaseNumber.
        /// </summary>
        public string DBReleaseNumber
        {
            get { return GetPropertyString("DBReleaseNumber"); }
            set
            {
                if (m_sDBReleaseNumber != value)
                {
                    m_bDataChanged = true;
                    m_sDBReleaseNumber = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_bFundsAccountLink.
        /// </summary>
        public bool FundsAccountLink
        {
            get { return GetPropertyBool("FundsAccountLink"); }
            set
            {
                if (m_bFundsAccountLink != value)
                {
                    m_bDataChanged = true;
                    m_bFundsAccountLink = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bUseFundsSubAcc.
        /// </summary>
        public bool UseFundsSubAcc
        {
            get { return GetPropertyBool("UseFundsSubAcc"); }
            set
            {
                if (m_bUseFundsSubAcc != value)
                {
                    m_bDataChanged = true;
                    m_bUseFundsSubAcc = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bPrintOSHADesc.
        /// </summary>
        public bool PrintOSHADesc
        {
            get { return GetPropertyBool("PrintOSHADesc"); }
            set
            {
                if (m_bPrintOSHADesc != value)
                {
                    m_bDataChanged = true;
                    m_bPrintOSHADesc = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_sHierarchyLevel.
        /// </summary>
        public string HierarchyLevel
        {
            get { return GetPropertyString("HierarchyLevel"); }
            set
            {
                if (m_sHierarchyLevel != value)
                {
                    m_bDataChanged = true;
                    m_sHierarchyLevel = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_iPrintClaimsAdmin.
        /// </summary>
        public int PrintClaimsAdmin
        {
            get { return GetPropertyInt("PrintClaimsAdmin"); }
            set
            {
                if (m_iPrintClaimsAdmin != value)
                {
                    m_bDataChanged = true;
                    m_iPrintClaimsAdmin = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bEVTypeAccFlag.
        /// </summary>
        public bool EVTypeAccFlag
        {
            get { return GetPropertyBool("EVTypeAccFlag"); }
            set
            {
                if (m_bEVTypeAccFlag != value)
                {
                    m_bDataChanged = true;
                    m_bEVTypeAccFlag = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bSMLimitDirect.
        /// </summary>
        public bool SMLimitDirect
        {
            get { return GetPropertyBool("SMLimitDirect"); }
            set
            {
                if (m_bSMLimitDirect != value)
                {
                    m_bDataChanged = true;
                    m_bSMLimitDirect = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bResDoNotRecalc.
        /// </summary>
        public bool ResDoNotRecalc
        {
            get { return GetPropertyBool("ResDoNotRecalc"); }
            set
            {
                if (m_bResDoNotRecalc != value)
                {
                    m_bDataChanged = true;
                    m_bResDoNotRecalc = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_sSMPTServer.
        /// </summary>
        public string SMTPServer
        {
            get { return GetPropertyString("SMTPServer"); }
            set
            {
                if (m_sSMTPServer != value)
                {
                    m_bDataChanged = true;
                    m_sSMTPServer = value;
                }
            }
        }

        //rsushilaggar Added Alternate domain & replyto domain field in the settings screen MITS 28280 
        /// <summary>
        ///Property to access m_sSMPTServer.
        /// </summary>
        public string SenderAlternateDomain
        {
            get { return GetPropertyString("SenderAlternateDomain"); }
            set
            {
                if (m_sSenderAlternateDomain != value)
                {
                    m_bDataChanged = true;
                    m_sSenderAlternateDomain = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sSMPTServer.
        /// </summary>
        public string SenderReplytoDomain
        {
            get { return GetPropertyString("SenderReplytoDomain"); }
            set
            {
                if (m_sSenderReplytoDomain != value)
                {
                    m_bDataChanged = true;
                    m_sSenderReplytoDomain = value;
                }
            }
        }
        //End rsushilaggar
        /// <summary>
        ///Property to access m_bExclHolidays.
        /// </summary>
        public bool ExclHolidays
        {
            get { return GetPropertyBool("ExclHolidays"); }
            set
            {
                if (m_bExclHolidays != value)
                {
                    m_bDataChanged = true;
                    m_bExclHolidays = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bUseDefaultCarrier.
        /// </summary>
        public bool UseDefaultCarrier
        {
            get { return GetPropertyBool("UseDefaultCarrier"); }
            set
            {
                if (m_bUseDefaultCarrier != value)
                {
                    m_bDataChanged = true;
                    m_bUseDefaultCarrier = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bAttachDiaryVis.
        /// </summary>
        public bool AttachDiaryVis
        {
            get { return GetPropertyBool("AttachDiaryVis"); }
            set
            {
                if (m_bAttachDiaryVis != value)
                {
                    m_bDataChanged = true;
                    m_bAttachDiaryVis = value;
                }
            }

        }


        /// <summary>
        ///Property to access m_iUseDefCarrier.
        /// </summary>
        public int UseDefCarrier
        {
            get { return GetPropertyInt("UseDefCarrier"); }
            set
            {
                if (m_iUseDefCarrier != value)
                {
                    m_bDataChanged = true;
                    m_iUseDefCarrier = value;
                }
            }

        }
        /// <summary>
        ///Property to access m_bUseAcrosoftInterface
        /// </summary>
        public bool UseAcrosoftInterface
        {
            get { return GetPropertyBool("UseAcrosoftInterface"); }
            set
            {
                if (m_bUseAcrosoftInterface != value)
                {
                    m_bDataChanged = true;
                    m_bUseAcrosoftInterface = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bUseFLMaxRate
        /// </summary>
        public int UseFLMaxRate
        {
            get { return GetPropertyInt("UseFLMaxRate"); }
            set
            {
                if (m_bUseFLMaxRate != value)
                {
                    m_bDataChanged = true;
                    m_bUseFLMaxRate = value;
                }
            }

        }
        //nadim MITS 12820 

        public int EnhPrintOrder1
        {
            get { return GetPropertyInt("EnhPrintOrder1"); }
            set
            {
                if (m_iEnhPrintOrder1 != value)
                {
                    m_bDataChanged = true;
                    m_iEnhPrintOrder1 = value;
                }
            }

        }
        public int EnhPrintOrder2
        {
            get { return GetPropertyInt("EnhPrintOrder2"); }
            set
            {
                if (m_iEnhPrintOrder2 != value)
                {
                    m_bDataChanged = true;
                    m_iEnhPrintOrder2 = value;
                }
            }

        }

        public int EnhPrintOrder3
        {
            get { return GetPropertyInt("EnhPrintOrder3"); }
            set
            {
                if (m_iEnhPrintOrder3 != value)
                {
                    m_bDataChanged = true;
                    m_iEnhPrintOrder3 = value;
                }
            }

        }
        //rkaur7 MITS 16668 : START        


        //Gagan Safeway Retrofit Policy Jursidiction : START        
        public int ShowJurisdiction
        {
            get { return GetPropertyInt("ShowJurisdiction"); }
            set
            {
                if (m_iShowJurisdiction != value)
                {
                    m_bDataChanged = true;
                    m_iShowJurisdiction = value;
                }
            }

        }


        //rkaur7 MITS 16668: END




        //nadim MITS 12820 
        //Changed by Gagan for MITS 12334 : Start
        /// <summary>
        /// Property to access m_iFreezeExistingNotes
        /// </summary>
        public int FreezeExistingNotes
        {
            get { return GetPropertyInt("FreezeExistingNotes"); }
            set
            {
                if (m_iFreezeExistingNotes != value)
                {
                    m_bDataChanged = true;
                    m_iFreezeExistingNotes = value;
                }
            }
        }

        //smishra25:Added for Performance
       
        public int AutoLaunchDiary
        {
            get { return GetPropertyInt("AutoLaunchDiary"); }
            
        }
        //Changed by Gagan for MITS 12334 : End

        /// <summary>
        ///Property to access m_bUseTAndE.
        /// </summary>
        public bool UseTAndE
        {
            get { return GetPropertyBool("UseTAndE"); }
            set
            {
                if (m_bUseTAndE != value)
                {
                    m_bDataChanged = true;
                    m_bUseTAndE = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iRateLevel.
        /// </summary>
        public int RateLevel
        {
            get { return GetPropertyInt("RateLevel"); }
            set
            {
                if (m_iRateLevel != value)
                {
                    m_bDataChanged = true;
                    m_iRateLevel = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iTandeAcctId.
        /// </summary>
        public int TandeAcctId
        {
            get { return GetPropertyInt("TandeAcctId"); }
            set
            {
                if (m_iTandeAcctId != value)
                {
                    m_bDataChanged = true;
                    m_iTandeAcctId = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bFroiPreparer.
        /// </summary>
        public bool FroiPreparer
        {
            get { return GetPropertyBool("FroiPreparer"); }
            set
            {
                if (m_bFroiPreparer != value)
                {
                    m_bDataChanged = true;
                    m_bFroiPreparer = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iFreezeDateFlag.
        /// </summary>
        public int FreezeDateFlag
        {
            get { return GetPropertyInt("FreezeDateFlag"); }
            set
            {
                if (m_iFreezeDateFlag != value)
                {
                    m_bDataChanged = true;
                    m_iFreezeDateFlag = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iDateAdjusterFlag.
        /// </summary>
        public int DateAdjusterFlag
        {
            get { return GetPropertyInt("DateAdjusterFlag"); }
            set
            {
                if (m_iDateAdjusterFlag != value)
                {
                    m_bDataChanged = true;
                    m_iDateAdjusterFlag = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iFroiBatHighLvl.
        /// </summary>
        public int FroiBatHighLvl
        {
            get { return GetPropertyInt("FroiBatHighLvl"); }
            set
            {
                if (m_iFroiBatHighLvl != value)
                {
                    m_bDataChanged = true;
                    m_iFroiBatHighLvl = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iFroiBatLowLvl.
        /// </summary>
        public int FroiBatLowLvl
        {
            get { return GetPropertyInt("FroiBatLowLvl"); }
            set
            {
                if (m_iFroiBatLowLvl != value)
                {
                    m_bDataChanged = true;
                    m_iFroiBatLowLvl = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iFroiBatCldClm.
        /// </summary>
        public int FroiBatCldClm
        {
            get { return GetPropertyInt("FroiBatCldClm"); }
            set
            {
                if (m_iFroiBatCldClm != value)
                {
                    m_bDataChanged = true;
                    m_iFroiBatCldClm = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bAutoNumWCEmp.
        /// </summary>
        public bool AutoNumWCEmp
        {
            get { return GetPropertyBool("AutoNumWCEmp"); }
            set
            {
                if (m_bAutoNumWCEmp != value)
                {
                    m_bDataChanged = true;
                    m_bAutoNumWCEmp = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_sWCEmpPrefix.
        /// </summary>
        public string WCEmpPrefix
        {
            get { return GetPropertyString("WCEmpPrefix"); }
            set
            {
                if (m_sWCEmpPrefix != value)
                {
                    m_bDataChanged = true;
                    m_sWCEmpPrefix = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_bEditWCEmpNum.
        /// </summary>
        public bool EditWCEmpNum
        {
            get { return GetPropertyBool("EditWCEmpNum"); }
            set
            {
                if (m_bEditWCEmpNum != value)
                {
                    m_bDataChanged = true;
                    m_bEditWCEmpNum = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bAutoNumVin.
        /// </summary>
        public bool AutoNumVin
        {
            get { return GetPropertyBool("AutoNumVin"); }
            set
            {
                if (m_bAutoNumVin != value)
                {
                    m_bDataChanged = true;
                    m_bAutoNumVin = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bAutoSelectPolicy.
        /// </summary>
        public bool AutoSelectPolicy
        {
            get { return GetPropertyBool("AutoSelectPolicy"); }
            set
            {
                if (m_bAutoSelectPolicy != value)
                {
                    m_bDataChanged = true;
                    m_bAutoSelectPolicy = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bPrevResBackdating.
        /// </summary>
        public bool PrevResBackdating
        {
            get { return GetPropertyBool("PrevResBackdating"); }
            set
            {
                if (m_bPrevResBackdating != value)
                {
                    m_bDataChanged = true;
                    m_bPrevResBackdating = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bPrevResModifyzero.
        /// </summary>
        public bool PrevResModifyzero
        {
            get { return GetPropertyBool("PrevResModifyzero"); }
            set
            {
                if (m_bPrevResModifyzero != value)
                {
                    m_bDataChanged = true;
                    m_bPrevResModifyzero = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iDiaryOrgLevel.
        /// </summary>
        public int DiaryOrgLevel
        {
            get { return GetPropertyInt("DiaryOrgLevel"); }
            set
            {
                if (m_iDiaryOrgLevel != value)
                {
                    m_bDataChanged = true;
                    m_iDiaryOrgLevel = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iAllowGlobalPeek.
        /// </summary>
        public int AllowGlobalPeek
        {
            get { return GetPropertyInt("AllowGlobalPeek"); }
            set
            {
                if (m_iAllowGlobalPeek != value)
                {
                    m_bDataChanged = true;
                    m_iAllowGlobalPeek = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iAllowTDFilter.
        /// </summary>
        public int AllowTDFilter
        {
            get { return GetPropertyInt("AllowTDFilter"); }
            set
            {
                if (m_iAllowTDFilter != value)
                {
                    m_bDataChanged = true;
                    m_iAllowTDFilter = value;
                }
            }

        }
        /// <summary>
        ///Property to access m_iResShowOrphans.
        /// </summary>
        public int ResShowOrphans
        {
            get { return GetPropertyInt("ResShowOrphans"); }
            set
            {
                if (m_iResShowOrphans != value)
                {
                    m_bDataChanged = true;
                    m_iResShowOrphans = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iNoSubDepCheck.
        /// </summary>
        public int NoSubDepCheck
        {
            get { return GetPropertyInt("NoSubDepCheck"); }
            set
            {
                if (m_iNoSubDepCheck != value)
                {
                    m_bDataChanged = true;
                    m_iNoSubDepCheck = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_sBlankFieldFlag.
        /// </summary>
        public string BlankFieldFlag
        {
            get { return GetPropertyString("BlankFieldFlag"); }
            set
            {
                if (m_sBlankFieldFlag != value)
                {
                    m_bDataChanged = true;
                    m_sBlankFieldFlag = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_iFinHostEval.
        /// </summary>
        public int FinHostEval
        {
            get { return GetPropertyInt("FinHostEval"); }
            set
            {
                if (m_iFinHostEval != value)
                {
                    m_bDataChanged = true;
                    m_iFinHostEval = value;
                }
            }

        }


        /// <summary>
        /// Added by rkaur7 for Utility changes of Policy
        ///Property to access m_StateEval.
        /// </summary>
        public int StateEval
        {
            get { return GetPropertyInt("StateEval"); }
            set
            {
                if (m_iStateEval != value)
                {
                    m_bDataChanged = true;
                    m_iStateEval = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_sWCPrinterSetup.
        /// </summary>
        public string WCPrinterSetup
        {
            get { return GetPropertyString("WCPrinterSetup"); }
            set
            {
                if (m_sWCPrinterSetup != value)
                {
                    m_bDataChanged = true;
                    m_sWCPrinterSetup = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_dXAxis.
        /// </summary>
        public double XAxis
        {
            get { return GetPropertyDouble("XAxis"); }
            set
            {
                if (m_dXAxis != value)
                {
                    m_bDataChanged = true;
                    m_dXAxis = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_dYAxis.
        /// </summary>
        public double YAxis
        {
            get { return GetPropertyDouble("YAxis"); }
            set
            {
                if (m_dYAxis != value)
                {
                    m_bDataChanged = true;
                    m_dYAxis = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iSecurityFlag.
        /// </summary>
        public int SecurityFlag
        {
            get { return GetPropertyInt("SecurityFlag"); }
            set
            {
                if (m_iSecurityFlag != value)
                {
                    m_bDataChanged = true;
                    m_iSecurityFlag = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iDefaultTimeFlag.
        /// </summary>
        public int DefaultTimeFlag
        {
            get { return GetPropertyInt("DefaultTimeFlag"); }
            set
            {
                if (m_iDefaultTimeFlag != value)
                {
                    m_bDataChanged = true;
                    m_iDefaultTimeFlag = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iDefaultDeptFlag.
        /// </summary>
        public int DefaultDeptFlag
        {
            get { return GetPropertyInt("DefaultDeptFlag"); }
            set
            {
                if (m_iDefaultDeptFlag != value)
                {
                    m_bDataChanged = true;
                    m_iDefaultDeptFlag = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iFilterOthPeople.
        /// </summary>
        public int FilterOthPeople
        {
            get { return GetPropertyInt("FilterOthPeople"); }
            set
            {
                if (m_iFilterOthPeople != value)
                {
                    m_bDataChanged = true;
                    m_iFilterOthPeople = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iFreezeEventDesc.
        /// </summary>
        public int FreezeEventDesc
        {
            get { return GetPropertyInt("FreezeEventDesc"); }
            set
            {
                if (m_iFreezeEventDesc != value)
                {
                    m_bDataChanged = true;
                    m_iFreezeEventDesc = value;
                }
            }

        }
        /// <summary>
        ///Property to access m_iFreezeLocDesc.
        /// </summary>
        public int FreezeLocDesc
        {
            get { return GetPropertyInt("FreezeLocDesc"); }
            set
            {
                if (m_iFreezeLocDesc != value)
                {
                    m_bDataChanged = true;
                    m_iFreezeLocDesc = value;
                }
            }

        }
        /// <summary>
        ///Property to access m_iUseMastBankAcct.
        /// </summary>		
        public bool UseMastBankAcct
        {
            get { return GetPropertyBool("UseMastBankAcct"); }
            set
            {
                if (m_bUseMastBankAcct != value)
                {
                    m_bDataChanged = true;
                    m_bUseMastBankAcct = value;
                }
            }

        }

        // created by stara Mits 16667 05/14/09      
        ///<summary>
        ///Property to access m_bUseResFilter
        /// </summary>
        public bool UseResFilter
        {
            get { return GetPropertyBool("UseResFilter"); }
            set
            {
                if (m_bUseResFilter != value)
                {
                    m_bDataChanged = true;
                    m_bUseResFilter = value;
                }
            }

        }

        //ends here --stara 05/14/09
        /// <summary>
        ///Property to access m_bAssignCheckStockToSubAcc.
        /// </summary>
        public bool AssignCheckStockToSubAcc
        {
            get { return GetPropertyBool("AssignCheckStockToSubAcc"); }
            set
            {
                m_bAssignCheckStockToSubAcc = value;
            }
        }

        /// <summary>
        ///Property to access m_iFundsAcountBalanceDateCriteria.
        /// </summary>
        public int FundsAcountBalanceDateCriteria
        {
            get { return GetPropertyInt("FundsAcountBalanceDateCriteria"); }
            set
            {
                m_iFundsAcountBalanceDateCriteria = value;
            }
        }

        /// <summary>
        ///Property to access m_sP2Login.
        /// </summary>
        public string P2Login
        {
            get { return GetPropertyString("P2Login"); }
            set
            {
                if (m_sP2Login != value)
                {
                    m_bDataChanged = true;
                    m_sP2Login = value;
                }
            }
        }

        /// <summary>
        ///Property to access m_sP2Pass.
        /// </summary>
        public string P2Pass
        {
            get { return GetPropertyString("P2Pass"); }
            set
            {
                if (m_sP2Pass != value)
                {
                    m_bDataChanged = true;
                    m_sP2Pass = value;
                }
            }
        }
        //asharma326 JIRA 6411
        public bool PowerViewReadOnly
        {
            get
            {
                try
                {
                    bool blnSuccess = false;
                    string powerViewReadonly = RetrieveSystemSettings("POWERVIEWREADONLY");
                    m_bPowerViewReadOnly = powerViewReadonly != string.Empty ? (Conversion.CastToType<Int32>(powerViewReadonly, out blnSuccess) == -1 ? true : false) : false;
                }
                catch (Exception ee)
                {
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure",m_iClientId));
                }
                return m_bPowerViewReadOnly;
            }
        }
        /// <summary>
        ///Property to access m_sP2URL.
        /// </summary>
        public string P2Url
        {
            get { return GetPropertyString("P2Url"); }
            set
            {
                if (m_sP2Url != value)
                {
                    m_bDataChanged = true;
                    m_sP2Url = value;
                }
            }
        }
        //mbahl3 Strataware enhancement
        public string StratwareServiceUrl
        {
            get { return GetPropertyString("StratwareServiceUrl"); }
            set
            {
                if (m_StratawareConsumerUrl != value)
                {
                    m_bDataChanged = true;
                    m_StratawareConsumerUrl = value;
                }
            }
        }
        public string StratawareConsumerUrl
        {
            get { return GetPropertyString("StratawareConsumerUrl"); }
            set
            {
                if (m_StratawareConsumerUrl != value)
                {
                    m_bDataChanged = true;
                    m_StratawareConsumerUrl = value;
                }
            }
        }
		//mbahl3 Strataware enhancement
        /// <summary>
        ///Property to access m_iDateEventDesc.
        /// </summary>
        public int DateEventDesc
        {
            get { return GetPropertyInt("DateEventDesc"); }
            set
            {
                if (m_iDateEventDesc != value)
                {
                    m_bDataChanged = true;
                    m_iDateEventDesc = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iOrderBankAcc.
        /// </summary>
        public int OrderBankAcc
        {
            get { return GetPropertyInt("OrderBankAcc"); }
            set
            {
                if (m_iOrderBankAcc != value)
                {
                    m_bDataChanged = true;
                    m_iOrderBankAcc = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iSuppFldSecFlag.
        /// </summary>
        public int SuppFldSecFlag
        {
            get { return GetPropertyInt("SuppFldSecFlag"); }
            set
            {
                if (m_iSuppFldSecFlag != value)
                {
                    m_bDataChanged = true;
                    m_iSuppFldSecFlag = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iEnhHierarchyList.
        /// </summary>
        public int EnhHierarchyList
        {
            get { return GetPropertyInt("EnhHierarchyList"); }
            set
            {
                if (m_iEnhHierarchyList != value)
                {
                    m_bDataChanged = true;
                    m_iEnhHierarchyList = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iEvCaptionLevel.
        /// </summary>
        public int EvCaptionLevel
        {
            get { return GetPropertyInt("EvCaptionLevel"); }
            set
            {
                if (m_iEvCaptionLevel != value)
                {
                    m_bDataChanged = true;
                    m_iEvCaptionLevel = value;
                }
            }

        }
        //yatharth START

        /// <summary>
        ///Property to access m_iOrgTimeZoneLevel.
        /// </summary>
        public int OrgTimeZoneLevel
        {
            //get { 
            //    return m_iOrgTimeZoneLevel; }
            //set
            //{
            //    if (m_iOrgTimeZoneLevel != value)
            //    {
            //        m_bDataChanged = true;
            //        m_iOrgTimeZoneLevel = value;
            //    }
            //}
            get
            {

                m_iOrgTimeZoneLevel = (Conversion.CastToType<Int32>(RetrieveSystemSettings("ORG_TIMEZONE_LEVEL"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("ORG_TIMEZONE_LEVEL")) : 0;
                if (blnSuccess)
                    return m_iOrgTimeZoneLevel;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }


        //yatharth END
        /// <summary>
        ///Property to access m_iUseEnhPolFlag.
        /// </summary>
        public int UseEnhPolFlag
        {
            get { return GetPropertyInt("UseEnhPolFlag"); }
            set
            {
                if (m_iUseEnhPolFlag != value)
                {
                    m_bDataChanged = true;
                    m_iUseEnhPolFlag = value;
                }
            }
        }

		//yatharth END

        //BOB Enhancement :ukusvaha
        public int MultiCovgPerClm
        {
           
            get
            {

                m_iMultiCovgPerClm = (Conversion.CastToType<Int32>(RetrieveSystemSettings("MULTI_COVG_CLM"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("MULTI_COVG_CLM")) : 0;
                if (blnSuccess)
                    return m_iMultiCovgPerClm;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        //Added by Amitosh for mits 23476 (05/11/2011)
        public int UseClaimantName
        {

            get
            {

                m_iUseClaimantName = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_CLAIMANT_NAME"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("USE_CLAIMANT_NAME")) : 0;
                if (blnSuccess)
                    return m_iUseClaimantName;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //Added by Amitsoh for R8 enhancment of policy interface
        public bool UsePolicyInterface
        {

            get
            {
                m_bUsePolicyInterface = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_POLICY_INTERFACE"), out blnSuccess)) == 0 ? false : true;
                
                if (blnSuccess)
                    return m_bUsePolicyInterface;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        public bool PolicyLimitExceeded
        {

            get
            {
                m_bPolicyLimitExceeded = (Conversion.CastToType<Int32>(RetrieveSystemSettings("CHECK_POLICY_LIMIT"), out blnSuccess)) == 0 ? false : true;

                if (blnSuccess)
                    return m_bPolicyLimitExceeded;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //mbahl3 mits 30224
        public bool StatusActive
        {

            get
            {
                m_bStatusActive = (Conversion.CastToType<Int32>(RetrieveSystemSettings("STATUS_ACTIVE"), out blnSuccess)) == 0 ? false : true;

                if (blnSuccess)
                    return m_bStatusActive;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        //mbahl3 mits 30224


        //sharishkumar Jira 12444
        public bool EditclaimEvtDate
        {

            get
            {
                m_bEditClaimEvtDate = (Conversion.CastToType<Int32>(RetrieveSystemSettings("EDIT_CLAIM_EVT_DATE"), out blnSuccess)) == 0 ? false : true;

                if (blnSuccess)
                    return m_bEditClaimEvtDate;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //sharishkumar 12444 ends

        //mbahl3 Strataware enhancement
        public int StrataWare
        {
            get
            {
                m_iStrataWare = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_STRATAWARE"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("USE_STRATAWARE")) : 0;
                if (blnSuccess)
                    return m_iStrataWare;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }


        public int EnableSingleuser
        {
            get
            {
                m_iEnableSingleuser = (Conversion.CastToType<Int32>(RetrieveSystemSettings("ENABLESINGLEUSER"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("ENABLESINGLEUSER")) : 0;
                if (blnSuccess)
                    return m_iEnableSingleuser;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        
              //mbahl3 Strataware enhancement
        //mmudabbir for MITS 36022
        public bool UseSilverlight
        {
            get
            {
                m_bUseSilverlight = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_SILVERLIGHT"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUseSilverlight;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure"));
            }
            set
            {
                AssignSystemSettings("USE_SILVERLIGHT", value.ToString().ToUpper() == "TRUE" ? "-1" : "0", false);
            }
        }
        public bool UploadSuppToPolicy
        {

            get
            {
                //m_bUploadSuppToPolicy = (Conversion.CastToType<Int32>(RetrieveSystemSettings("UPLOAD_SUPPTO_POLICY"), out blnSuccess)) == 0 ? false : true;

                //if (blnSuccess)
                //    return m_bUploadSuppToPolicy;
                //else
                //    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure"));
                return false;
            }
        }

        public bool UseCodeMapping
        {

            get
            {
                m_bUseCodeMapping = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_CODE_MAPPING"), out blnSuccess)) == 0 ? false : true;

                if (blnSuccess)
                    return m_bUseCodeMapping;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        //JIRA RMA-718 ajohari2: Start
        public bool EnableTPAAccess
        {
            get
            {
                m_bEnableTPAAccess = (Conversion.CastToType<Int32>(RetrieveSystemSettings("Enable_TPA_Access"), out blnSuccess)) == 0 ? false : true;

                if (blnSuccess)
                    return m_bEnableTPAAccess;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //JIRA RMA-718 ajohari2: End
        // Start - Added by Nikhil on 07/31/14. New setting added to upload check total to point instead of funds amoount
        public bool UploadCheckTotal
        {

            get
            {
                m_bUploadCheckTotal = (Conversion.CastToType<Int32>(RetrieveSystemSettings("UPLOAD_CHECK_TOTAL"), out blnSuccess)) == 0 ? false : true;

                if (blnSuccess)
                    return m_bUploadCheckTotal;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //end - Added by Nikhil on 07/31/14. New setting added to upload check total to point instead of funds amoount
        /// <summary>
        /// Policy system Interface : Will be used to open  Media View in GC and WC when checked.
        /// </summary>
        public bool ShowMediaViewButton
        {

            get
            {
                m_bShowMediaViewButton = (Conversion.CastToType<Int32>(RetrieveSystemSettings("SHOW_MEDIA_VIEW"), out blnSuccess)) == 0 ? false : true;

                if (blnSuccess)
                    return m_bShowMediaViewButton;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        //Start - averma62 MITS 25163- Policy Interface Implementation
        public bool AllowPolicySearch
        {
            get
            {
                m_bAllowPolicySearch = (Conversion.CastToType<Int32>(RetrieveSystemSettings("ALLOW_POLICY_SEARCH"), out blnSuccess)) == 0 ? false : true;

                if (blnSuccess)
                    return m_bAllowPolicySearch;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //End - averma62 MITS 25163- Policy Interface Implementation
        //mbahl3 Performance mits
        public int PayHistLimit
        {
            get
            {
                m_iPayHistLimit = (Conversion.CastToType<Int32>(RetrieveSystemSettings("PAY_HISTORY_LIMIT"), out blnSuccess));

                if (blnSuccess)
                    return m_iPayHistLimit;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //mbahl3 end
        public int PolicySystemUpdate
        {

            get
            {
                return 1;
                //m_iPolicySystemUpdate = (Conversion.CastToType<Int32>(RetrieveSystemSettings("POLICY_SYSTEM_UPDATE"), out blnSuccess));

                //if (blnSuccess)
                //    return m_iPolicySystemUpdate;
                //else
                //    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure"));
            }
        }
        public int PolicyCvgType
        {

            get
            {
                m_iPolicyCvgType = (Conversion.CastToType<Int32>(RetrieveSystemSettings("POLICY_CVG_TYPE"), out blnSuccess));

                if (blnSuccess)
                    return m_iPolicyCvgType;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        
        //nsachdeva2:  MITS 25163- Policy Interface Implementation - 1/6/2012 - Commented not required.
        //public int MaxPolEntities
        //{
        //    get
        //    {
        //        m_iMaxPolEntities = (Conversion.CastToType<Int32>(RetrieveSystemSettings("MAX_POL_ENTITIES"), out blnSuccess));
        //        if (blnSuccess)
        //            return m_iMaxPolEntities;
        //        else
        //            throw new RMAppException(Globalization.GetString("SysSettings.CastFailure"));
        //    }
        //}
        //public int MaxPolUnits
        //{
        //    get
        //    {
        //        m_iMaxPolUnits = (Conversion.CastToType<Int32>(RetrieveSystemSettings("MAX_POL_UNITS"), out blnSuccess));
        //        if (blnSuccess)
        //            return m_iMaxPolUnits;
        //        else
        //            throw new RMAppException(Globalization.GetString("SysSettings.CastFailure"));
        //    }
        //}
        //End  MITS 25163
        //End amitosh
        //Added by nitin goel, MITS 33588,07/29/2014
        public int UseInsuredClaimDept
        {

            get
            {
                m_iUseInsuredClaimDept = (Conversion.CastToType<Int32>(RetrieveSystemSettings("INS_CLM_DEPT_FLAG"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("INS_CLM_DEPT_FLAG")) : 0;
                if (blnSuccess)
                    return m_iUseInsuredClaimDept; //ddhiman update variable name MITS 33588
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }


        public int UnitStat
        {

            get
            {

                m_iUnitStat = (Conversion.CastToType<Int32>(RetrieveSystemSettings("UNIT_STAT"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("UNIT_STAT")) : 0;
                if (blnSuccess)
                    return m_iUnitStat;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        public int AutoPopulateDpt
        {

            get
            {

                m_iAutoPopulateDpt = (Conversion.CastToType<Int32>(RetrieveSystemSettings("AUTO_POPU_DPT"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("AUTO_POPU_DPT")) : 0;
                if (blnSuccess)
                    return m_iAutoPopulateDpt;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        public int AutoFillDpteid
        {

            get
            {

                m_iAutoFillDpteid = (Conversion.CastToType<Int32>(RetrieveSystemSettings("AUTO_POPU_DPT_ID"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("AUTO_POPU_DPT_ID")) : 0;
                if (blnSuccess)
                    return m_iAutoFillDpteid;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

		/// <summary>
		///Property to access m_iUseEnhPolFlag.
		/// </summary>
		
        /// <summary>
        ///Property to access m_iUseBillingFlag.
        /// </summary>
        public int UseBillingFlag
        {
            get { return GetPropertyInt("UseBillingFlag"); }
            set
            {
                if (m_iUseBillingFlag != value)
                {
                    m_bDataChanged = true;
                    m_iUseBillingFlag = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_iOracleCaseIns.
        /// </summary>
        public int OracleCaseIns
        {
            get { return GetPropertyInt("OracleCaseIns"); }
            set
            {
                if (m_iOracleCaseIns != value)
                {
                    m_bDataChanged = true;
                    m_iOracleCaseIns = value;
                }
            }

        }
        /// <summary>
        ///Property to access m_bMultipleInsurer // Mihika 01/12/2007 MITS 8656
        /// </summary>
        public bool MultipleInsurer
        {
            get { return GetPropertyBool("MultipleInsurer"); }
            set
            {
                if (m_bMultipleInsurer != value)
                {
                    m_bDataChanged = true;
                    m_bMultipleInsurer = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bMultipleReInsurer // added by Navdeep
        /// </summary>
        public bool MultipleReInsurer
        {
            get { return GetPropertyBool("MultipleReInsurer"); }
            set
            {
                if (m_bMultipleReInsurer != value)
                {
                    m_bDataChanged = true;
                    m_bMultipleReInsurer = value;
                }
            }

        }

        /// <summary>
        ///Property to access m_bPolicyDropDown // Mihika 01/17/2007 MITS 8657
        /// </summary>
        public bool PolicyDropDown
        {
            get { return GetPropertyBool("PolicyDropDown"); }
            set
            {
                if (m_bPolicyDropDown != value)
                {
                    m_bDataChanged = true;
                    m_bPolicyDropDown = value;
                }
            }

        }
        /// <summary>
        ///Property to access m_bDataChanged.
        /// </summary>
        public bool DataChanged
        {
            get { return m_bDataChanged; }
            set { m_bDataChanged = true; m_bDataChanged = value; }
        }

        ///Property to access m_iDeleteAllClaimDiaries.
        /// </summary>
        public int DeleteAllClaimDiaries
        {
            get { return GetPropertyInt("DeleteAllClaimDiaries"); }
            set
            {
                if (m_iDeleteAllClaimDiaries != value)
                {
                    m_bDataChanged = true;
                    m_iDeleteAllClaimDiaries = value;
                }
            }

        }
        //Animesh Inserted RMSC Bill Review //skhare7 RMSC Merge 28397
        /// <summary>
        ///Property to access m_bUseBillReview.
        /// </summary>
        public bool UseBillReview
        {
            get { return GetPropertyBool("UseBillReview"); }
            set
            {
                if (m_bUseBillReview != value)
                {
                    m_bDataChanged = true;
                    m_bUseBillReview = value;
                }
            }

        }
        public int OrgBillLevel
        {
            get { return GetPropertyInt("OrgBillLevel"); }
            set
            {
                if (m_iOrgBillLevel != value)
                {
                    m_bDataChanged = true;
                    m_iOrgBillLevel = value;
                }
            }
        }
        //skhare7 RMSC changes to make it generic 
         public int OrgBillLevelEntityId
        {
            get { return GetPropertyInt("OrgBillLevelEntityId"); }
            set
            {
                if (m_iOrgBillLevelEntityId != value)
                {
                    m_bDataChanged = true;
                    m_iOrgBillLevelEntityId = value;
                }
            }
        }
         
    //Animesh insertion Ends //skhare7 RMSC merge //skhare7 RMSC Merge 28397

        //Animesh Insertion ends//skhare7 RMSC merge  //skhare7 RMSC Merge 28397
        //Raman 03/08/2009 : R6 Work Loss / Restriction
        /// <summary>
        ///Property to access m_EnableWorkLossRestriction.
        /// </summary>
        public bool AutoCrtWrkLossRest
        {
            get { return GetPropertyBool("AutoCrtWrkLossRest"); }
            set
            {
                if (m_bAutoCrtWrkLossRest != value)
                {
                    m_bDataChanged = true;
                    m_bAutoCrtWrkLossRest = value;
                }
            }

        }

        //added by Navdeep for Chubb : Ack/Closed Claim Letter
        /// <summary>
        /// Property to access m_bClmLtrFlag
        /// </summary>
        public bool ClaimLetterFlag
        {
            get { return GetPropertyBool("ClaimLetterFlag"); }
            set
            {
                if (m_bClmLtrFlag != value)
                {
                    m_bDataChanged = true;
                    m_bClmLtrFlag = value;
                }
            }

        }

        /// <summary>
        /// Added mcapps2 for R7: Added To allow user to void cleared payments.
        /// </summary>
        public bool AllowVoidOfClrdPmnts
        {
            get
            {
                m_bAllowVoidOfClrdPmnts = (Conversion.CastToType<Int32>(RetrieveSystemSettings("ALLOW_VOID_CLRD_PMTS"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                {
                    return m_bAllowVoidOfClrdPmnts;
                }
                else
                {
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                }
            }
        }

        //added by Navdeep for Chubb - Auto FROI ACORD
        /// <summary>
        /// Property to access m_bAutoFROIACORDFlag
        /// </summary>
        public bool AutoFROIACORDFlag
        {
            get { return GetPropertyBool("AutoFROIACORDFlag"); }
            set
            {
                if (m_bAutoFROIACORDFlag != value)
                {
                    m_bDataChanged = true;
                    m_bAutoFROIACORDFlag = value;
                }
            }
        }

        /// <summary>
        /// If comments stays in the table CLAIM/EVENT instead of table COMMENTS_TEXT
        /// </summary>
        public bool UseLegacyComments
        {
            get { return GetPropertyBool("UseLegacyComments"); }
            set
            {
                if (m_bUseLegacyComments != value)
                {
                    m_bDataChanged = true;
                    m_bUseLegacyComments = value;
                }
            }
        }
        //added by neha goel-williams: Start:MITS 21704
        /// <summary>
        /// Property to access m_iEnhNotesPolicyInClaim
        /// </summary>
        public bool EnhNotesPolicyClaimView
        {
            get
            {
                m_bEnhNotesPolicyInClaim = (Conversion.CastToType<Int32>(RetrieveSystemSettings("ENH_NOTE_POL_CLAIM"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bEnhNotesPolicyInClaim;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        /// <summary>
        /// Added Rakhi for R7:Add Emp Data Elements. To allow user to use Multiple Addresses.
        /// </summary>
        public bool UseMultipleAddresses
        {
            get
            {

                m_bUseMultipleAddresses = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_MUL_ADDRESSES"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUseMultipleAddresses;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //Added Rakhi for R7:Add Emp Data Elements
        /// <summary>
        ///smishra25:Outlook email Utility Setting
        /// </summary>
        public bool UseOutlook
        {
            get
            {
                m_bUseOutlook = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_OUTLOOK"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUseOutlook;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        /// <summary>
        ///rbhatia4:R8: Use Media View Setting : July 05 2011
        /// </summary>
        public bool UseMediaViewInterface
        {
            get
            {
                m_bUseMediaView = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_MEDIA_VIEW"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUseMediaView;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        

        /// <summary>
        ///smishra25:Outlook File size limit 
        /// </summary>
        public int FileSizeLimitForOutlook
        {
            get
            {
                m_iFileSizeLimitForOutlook = Conversion.CastToType<Int32>(RetrieveSystemSettings("SIZE_LIMIT_OUTLOOK"), out blnSuccess);
                if (blnSuccess)
                    return m_iFileSizeLimitForOutlook;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }

        }
        /// <summary>
        /// Geeta 04/15/10 : R7 Task Manager Diary
        /// </summary>
        public bool TaskMgrDiary
        {
            //get { return m_bTaskMgrDiary; }
            //set
            //{
            //    if (m_bTaskMgrDiary != value)
            //    {
            //        m_bDataChanged = true;
            //        m_bTaskMgrDiary = value;
            //    }
            //}
            get
            {
                m_bTaskMgrDiary = (Conversion.CastToType<Int32>(RetrieveSystemSettings("TASK_MGR_DIARY"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bTaskMgrDiary;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        public bool TaskMgrEmail //tmalhotra3: JIRA 4613
        {

            get
            {
                m_bTaskMgrEmail = (Conversion.CastToType<Int32>(RetrieveSystemSettings("TASK_MGR_EMAIL"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bTaskMgrEmail;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        /// <summary>
        /// Flag if set will include physician med in the search results for physician.
        /// </summary>

        public bool PhysicianSearchFlag
        {
            get { return GetPropertyBool("PhysicianSearchFlag"); }
            set
            {
                if (m_bPhysicianSearchFlag != value)
                {
                    m_bDataChanged = true;
                    m_bPhysicianSearchFlag = value;
                }
            }
        }

        public bool AutoNumPin
        {
            get
            {
                m_bAutoNumPin = (Conversion.CastToType<Int32>(RetrieveSystemSettings("AUTO_NUM_PIN"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bAutoNumPin;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        public bool HistTrackEnable
        {
            get
            {
                m_bHistTrackEnable = (Conversion.CastToType<Int32>(RetrieveSystemSettings("HIST_TRACK_ENABLE"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bHistTrackEnable;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
            set
            {
                AssignSystemSettings("HIST_TRACK_ENABLE", value.ToString().ToUpper() == "TRUE" ? "-1" : "0", false);
            }
        }

        public bool FreezeSuppText
        {
            get
            {
                m_bFreezeSuppText = (Conversion.CastToType<Int32>(RetrieveSystemSettings("FREEZE_SUP_TEXT_FLAG"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bFreezeSuppText;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
            set
            {
                AssignSystemSettings("FREEZE_SUP_TEXT_FLAG", value.ToString().ToUpper() == "TRUE" ? "-1" : "0", false);
            }
        }

        //jramkumar for MITS 32095
        public bool FreezePayOrderText
        {
            get
            {
                m_bFreezePayOrderText = (Conversion.CastToType<Int32>(RetrieveSystemSettings("FREEZE_PAY_TEXT_FLAG"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bFreezePayOrderText;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
            set
            {
                AssignSystemSettings("FREEZE_PAY_TEXT_FLAG", value.ToString().ToUpper() == "TRUE" ? "-1" : "0", false);
            }
        }

        public bool HistTrackStatus
        {
            get
            {
                m_bHistTrackStatus = (Conversion.CastToType<Int32>(RetrieveSystemSettings("HIST_TRACK_STATUS"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bHistTrackStatus;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
            set
            {
                AssignSystemSettings("HIST_TRACK_STATUS", value.ToString().ToUpper() == "TRUE" ? "-1" : "0", false);
            }
        }

        /// <summary>
        /// //Mona:PaperVisionMerge
        /// </summary>
        public bool UsePaperVisionInterface
        {
            get
            {
                m_bUsePaperVisionInterface = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_PAPERVISION"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUsePaperVisionInterface;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
            set
            {
                AssignSystemSettings("USE_PAPERVISION", value.ToString().ToUpper() == "TRUE" ? "-1" : "0", false);
            }
        }


        ///<summary>
        ///Property to access m_bUseEntityApproval
        ///Debabrata Biswas Entity Payment Approval R6 Rerofit 03/02/2010
        ///</summary>
        public bool UseEntityApproval
        {
            get
            {
                m_bUseEntityApproval = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_ENTITY_APPROVAL"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUseEntityApproval;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        ///<summary>
        ///Property to access m_bShowLSSInvoice
        ///Debabrata Biswas MITS 20606 05/05/2010
        ///</summary>
        public bool ShowLSSInvoice
        {
            get
            {
                m_bShowLSSInvoice = (Conversion.CastToType<Int32>(RetrieveSystemSettings("SHOW_LSS_INVOICE"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bShowLSSInvoice;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        /// <summary>
        ///Property to access m_bRMXLSSEnable
        ///Debabrata Biswas MITS 20606 05/05/2010
        /// </summary>
        public bool RMXLSSEnable
        {
            get { return GetPropertyBool("RMXLSSEnable"); }
            set
            {
                if (m_bRMXLSSEnable != value)
                {
                    m_bDataChanged = true;
                    m_bRMXLSSEnable = value;
                }
            }
        }

        ///<summary>
        ///Date : 06/29/2010
        ///Author : Sumit Kumar
        ///Description : Property to access m_bFilterPolicyBasedOnLOB
        ///</summary>
        public bool FilterPolicyBasedOnLOB
        {
            get
            {
                m_bFilterPolicyBasedOnLOB = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_POLICY_LOB"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bFilterPolicyBasedOnLOB;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        ///<summary>
        ///Date : 07/02/2010
        ///Author : Sumit Kumar
        ///Description : Property to access m_bUseGCLOB
        ///</summary>
        public bool UseGCLOB
        {
            get
            {
                m_bUseGCLOB = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_GC_LOB"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUseGCLOB;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        ///<summary>
        ///Date : 07/02/2010
        ///Author : Sumit Kumar
        ///Description : Property to access m_bUseDILOB
        ///</summary>
        public bool UseDILOB
        {
            get
            {
                m_bUseDILOB = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_DI_LOB"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUseDILOB;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        ///<summary>
        ///Date : 07/02/2010
        ///Author : Sumit Kumar
        ///Description : Property to access m_bUsePCLOB
        ///</summary>
        public bool UsePCLOB
        {
            get
            {
                m_bUsePCLOB = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_PC_LOB"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUsePCLOB;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        ///<summary>
        ///Date : 07/02/2010
        ///Author : Sumit Kumar
        ///Description : Property to access m_bUseVALOB
        ///</summary>
        public bool UseVALOB
        {
            get
            {
                m_bUseVALOB = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_VA_LOB"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUseVALOB;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        ///<summary>
        ///Date : 07/02/2010
        ///Author : Sumit Kumar
        ///Description : Property to access m_bUseWCLOB
        ///</summary>
        public bool UseWCLOB
        {
            get
            {
                m_bUseWCLOB = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_WC_LOB"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUseWCLOB;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        /// <summary>
        /// R7 PRF IMP:Adjuster Auto Assign
        /// </summary>
        public bool AdjAutoAssign
        {
            get
            {
                m_bAdjAutoAssign = (Conversion.CastToType<Int32>(RetrieveSystemSettings("ADJ_AUTOASSIGN_FLAG"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bAdjAutoAssign;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        /// <summary>
        /// This will decide the number of records that are shown on quick lookup screen
        /// </summary>

        public int ResultQuickLkpUp
        {
            get
            {
                m_iResultLookUp = Conversion.CastToType<Int32>(RetrieveSystemSettings("QCKLKPUP_USER_LIMIT"), out blnSuccess);
                if (blnSuccess)
                    return m_iResultLookUp;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        
            /// <summary>
        ///smishra25:CQuality Management Flag
        /// </summary>
        public int QualityMgtFlag
        {
            get
            {
                m_iQualityMgtFlag = Conversion.CastToType<Int32>(RetrieveSystemSettings("QUALITY_MGT_FLAG"), out blnSuccess);
                if (blnSuccess)
                    return m_iQualityMgtFlag;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        /// <summary>
        ///smishra25:Codes User Limit,Maximum number of records displayed
        /// </summary>
        public int CodeUserLimit
        {
            get
            {
                m_iCodeUserLimit = Conversion.CastToType<Int32>(RetrieveSystemSettings("CODES_USER_LIMIT"), out blnSuccess);
                if (blnSuccess)
                    return m_iCodeUserLimit;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        
        /// <summary>
        ///ybhaskar:OFAC Check Utility Setting
        /// </summary>
        public bool DoOfacCheck
        {
            get
            {
                m_bDoOfacCheck = (Conversion.CastToType<Int32>(RetrieveSystemSettings("DO_OFAC_CHECK"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bDoOfacCheck;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        ///<summary>
        ///ybhaskar: Records per Page for the Payee Check Review Screen
        /// </summary>
        public int ResultPayeeCheckReview
        {
            get
            {
                m_iResultPayeeCheckReview = Conversion.CastToType<Int32>(RetrieveSystemSettings("RECORDS_PAYEE_REVIEW"), out blnSuccess);
                if (blnSuccess)
                    return m_iResultPayeeCheckReview;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        /// <summary>
        /// rsushilaggar : MITS 21970 date 10/01/2010
        /// </summary>
        public bool LssChecksOnHold
        {
            get
            {
                m_bLssChecksOnHold = (Conversion.CastToType<Int32>(RetrieveSystemSettings("LSS_CHECKS_ON_HOLD"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bLssChecksOnHold;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }


 
        //pkandhari JIRA 6421
        public bool IsPayeeAddressSelect
        {
            get
            {
                try
                {
                    bool blnSuccess = false;
                    string PayeeAddressSelect = RetrieveSystemSettings("PAYEE_ADDRESS_SELECT");
                    m_bPayeeAddressSelect = PayeeAddressSelect != string.Empty ? (Conversion.CastToType<Int32>(PayeeAddressSelect, out blnSuccess) == -1 ? true : false) : false;
                }
                catch (Exception ee)
                {
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                }
                return m_bPayeeAddressSelect;
            }
        }

        //pkandhari JIRA 6421

        // npadhy Start MITS 21557 10/05/2010 Added Settings for Maximum Tiers and Selected Discounts
        public int MaxDiscTier
        {
            get
            {
                m_iMaxDiscTier = (Conversion.CastToType<Int32>(RetrieveSystemSettings("MAX_DISC_TIER"), out blnSuccess));
                if (blnSuccess)
                    return m_iMaxDiscTier;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        public int MaxDiscounts
        {
            get
            {
                m_iMaxDiscounts = (Conversion.CastToType<Int32>(RetrieveSystemSettings("MAX_DISCOUNTS"), out blnSuccess));
                if (blnSuccess)
                    return m_iMaxDiscounts;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        public bool UseScriptEditor
        {
            get
            {
                m_bUseScriptEditor = (Conversion.CastToType<Int32>(RetrieveSystemSettings("SCRPT_EDI_FLAG"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUseScriptEditor;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        // npadhy End MITS 21557 10/05/2010 Added Settings for Maximum Tiers and Selected Discounts

         //spahariya MITS 25646
        public bool UsePSOReportingSys
        {
            get
            {
                m_bUsePSOReportingSYS = (Conversion.CastToType<Int32>(RetrieveSystemSettings("PSO_REP_FLAG"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUsePSOReportingSYS;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
		
		//rsushilaggar Added this flag in SysSettings for Performance Improvement
        public int CaseMgmtFlag
        {
            get
            {
                m_iCaseMgmtFlag = (Conversion.CastToType<Int32>(RetrieveSystemSettings("CASE_MGT_FLAG"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("CASE_MGT_FLAG")) : 0;
                if (blnSuccess)
                    return m_iCaseMgmtFlag;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }		 
        //Deb MITS 25775 
        ///</summary>
        public bool MaskSSN
        {
            get
            {
                m_bMaskSSN = (Conversion.CastToType<Int32>(RetrieveSystemSettings("MASK_SSN"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bMaskSSN;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }      
        //Rupal:start, r8
        ///</summary>
        public bool AddPersonInvolvedAsPayeeType
        {
            get
            {
                m_bPersonInvolvedPayeeType = (Conversion.CastToType<Int32>(RetrieveSystemSettings("PI_AS_PAYEE_TYPE"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bPersonInvolvedPayeeType;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }  
        //rupal:end
	  /// <summary>
        ///Bkuzhanthaim : MITS-36026/RMA-338: OrgHierarchy as Payee Type (-1 True ; 0 False) from GeneralSystemParameterSetup : Start
        ///</summary>
        public bool AddOrgHierarchyAsPayeeType
        {
            get
            {
                m_bPersonInvolvedPayeeType = (Conversion.CastToType<Int32>(RetrieveSystemSettings("ORG_HIER_PAYEE_TYPE"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bPersonInvolvedPayeeType;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //Bkuzhanthaim : MITS-36026/RMA-338: End

        /// <summary>
        ///Bkuzhanthaim : MITS- 36027/RMA-342: Customization for  Navigation Tree Settings .Here Adjuster AUTO EXPANSTION (-1 True ; 0 False) : Start
        ///</summary>
        public bool AdjusterAutoExpanstioFlag
        {
            get
            {
                m_bPersonInvolvedPayeeType = (Conversion.CastToType<Int32>(RetrieveSystemSettings("ADJ_AUTO_EXPANSION"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bPersonInvolvedPayeeType;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //Bkuzhanthaim : MITS- 36027/RMA-342 : End

        /// <summary>
        ///ngupta73 : MITS- 34260/RMA-4362: Customization for  Navigation Tree Settings .Here Policy AUTO EXPANSTION (-1 True ; 0 False) : Start
        ///</summary>
        public bool PolicyAutoExpansionFlag
        {
            get
            {
                m_bPolicyAutoExpansion = (Conversion.CastToType<Int32>(RetrieveSystemSettings("POL_AUTO_EXPANSION"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bPolicyAutoExpansion;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //ngupta73 : MITS- 34260/RMA-4362: End

        /// <summary>
        /// nsachdeva2: mits:26418 - claim activity log flag
        /// </summary>
        public bool ClaimActivityLog
        {
            get
            {
                m_bClaimActLog = (Conversion.CastToType<Int32>(RetrieveSystemSettings("CLAIM_ACT_LOG"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bClaimActLog;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //RMA-8753 nshah28 Start(Adding a property)
        public bool AddModifyAddress
        {
            get
            {
                m_bAddModifyAddress = (Conversion.CastToType<Int32>(RetrieveSystemSettings("ADD_ADDRESS_FLAG"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bAddModifyAddress;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //RMA-8753 nshah28 End(Adding a property)
        //RMA-16584 nshah28 Start(Adding a property)
        public bool AllowSummaryForBookedReserve
        {
            get
            {
                m_bAllowSummaryForBookedReserve = (Conversion.CastToType<Int32>(RetrieveSystemSettings("ALW_SMR_BOOKED_RES"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bAllowSummaryForBookedReserve;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
            
        }
        //RMA-16584 nshah28 End(Adding a property)
        //Start Igupta3 "For Multi Diary Selection"  Mits:32846
        public bool NotifyAssigner
        {
            get
            {
                m_iNotifyAssigner = (Conversion.CastToType<Int32>(RetrieveSystemSettings("NOTIFY_ASSIGNER"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_iNotifyAssigner;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        public bool CurrentDateForDiary
        {
            get
            {
                m_iCurrentDateDiary = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_CUR_DATE_DIARY"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_iCurrentDateDiary;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //End Igupta3
        // Mits 36708
        public int ClaimantLength
        {
            get
            {
                m_iClaimantLength = (Conversion.CastToType<Int32>(RetrieveSystemSettings("CLAIMANT_LENGTH"), out blnSuccess)) == 0 ? 0 : Convert.ToInt32(RetrieveSystemSettings("CLAIMANT_LENGTH"));
                if (blnSuccess)
                    return m_iClaimantLength;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
            set
            {
                AssignSystemSettings("CLAIMANT_LENGTH", value.ToString(), false);
            }
        }
        //Deb MultiCurr
        public int BaseCurrencyType
        {
            get
            {
                iBaseCurrCode = (Conversion.CastToType<Int32>(RetrieveSystemSettings("BASE_CURRENCY_CODE"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("BASE_CURRENCY_CODE")) : 0;
                if (blnSuccess)
                    return iBaseCurrCode;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        public int UseMultiCurrency
        {
            get
            {
                m_iUseMultiCurrency = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_MULTI_CURRENCY"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("USE_MULTI_CURRENCY")) : 0;
                if (blnSuccess)
                    return m_iUseMultiCurrency;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        //Start averma62 
        public int EnableVSS
        {
            get
            {
                m_iEnableVSS = (Conversion.CastToType<Int32>(RetrieveSystemSettings("ENABLE_VSS"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("ENABLE_VSS")) : 0;
                if (blnSuccess)
                    return m_iEnableVSS;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //End averma62

        //Ashish Ahuja: Claims Made Jira 1342 Start
        public int ClaimDateRptType
        {
            get
            {
                m_iClaimRptType = (Conversion.CastToType<Int32>(RetrieveSystemSettings("RPT_DATE_AUTO_FLAG"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("RPT_DATE_AUTO_FLAG")) : 0;
                if (blnSuccess)
                    return m_iClaimRptType;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //Ashish Ahuja: Claims Made Jira 1342

        /// <summary>
        ///spahariya:Add adjuster as Person Involved
        /// </summary>
        public bool AddAdjAsPI
        {
            get
            {
                m_bAddAdjAsPI = (Conversion.CastToType<Int32>(RetrieveSystemSettings("ADD_ADJ_AS_PI"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bAddAdjAsPI;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
			//Nitika
        /// <summary>
        /// Add FNOL setting for FNOL reserve
        /// </summary>
        public bool FNOLReserve
        {
            get
            {
                m_bFNOLReserve = (Conversion.CastToType<Int32>(RetrieveSystemSettings("FNOL_RESERVE"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bFNOLReserve;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement 
        /// <summary>
        ///avipinsrivas:Add adjuster setting for Auto Diary
        /// </summary>
        public int AdjAssignmentAutoDiary
        {
            get
            {
                m_iAdjAssignmentAutoDiary = (Conversion.CastToType<Int32>(RetrieveSystemSettings("ADJ_ASIGN_AUTO_DIARY"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("ADJ_ASIGN_AUTO_DIARY")) : 0;
                if (blnSuccess)
                    return m_iAdjAssignmentAutoDiary;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //Ankit End
        //Ankit Start : Worked on MITS - 32386 - FAS Integration
        /// <summary>
        ///Property to access m_iEnableFAS.
        /// </summary>
        public int EnableFAS
        {
            get
            {
                try
                {
                    m_iEnableFAS = (Convert.ToInt32(RetrieveSystemSettings("ENABLE_FAS"))) != 0 ? Convert.ToInt32(RetrieveSystemSettings("ENABLE_FAS")) : 0;
                }
                catch (Exception ee)
                {
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                }

                return m_iEnableFAS;
            }
        }
        /// <summary>
        ///Property to access m_sFileLocationSelection.
        /// </summary>
        public string FileLocationSelection
        {
            get
            {
                try
                {
                    m_sFileLocationSelection = (Convert.ToString(RetrieveSystemSettings("FILE_LOCATION"))) != string.Empty ? Convert.ToString(RetrieveSystemSettings("FILE_LOCATION")) : string.Empty;
                }
                catch (Exception ee)
                {
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                }
                return m_sFileLocationSelection;
            }
        }

        /// <summary>
        ///Property to access m_sSharedLocation.
        /// </summary>
        public string SharedLocation
        {
            get
            {
                try
                {
                    m_sSharedLocation = (Convert.ToString(RetrieveSystemSettings("SHARED_LOCATION"))) != string.Empty ? Convert.ToString(RetrieveSystemSettings("SHARED_LOCATION")) : string.Empty;
                }
                catch (Exception ee)
                {
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                }
                return m_sSharedLocation;
            }
        }

        /// <summary>
        ///Property to access m_sFASServer.
        /// </summary>
        public string FASServer
        {
            get
            {
                try
                {
                    m_sFASServer = (Convert.ToString(RetrieveSystemSettings("FAS_SERVER"))) != string.Empty ? Convert.ToString(RetrieveSystemSettings("FAS_SERVER")) : string.Empty;
                }
                catch (Exception ee)
                {
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                }
                return m_sFASServer;
            }
        }

        /// <summary>
        ///Property to access m_sFASUserId.
        /// </summary>
        public string FASUserId
        {
            get
            {
                try
                {
                    m_sFASUserId = (Convert.ToString(RetrieveSystemSettings("FAS_USER_ID"))) != string.Empty ? Convert.ToString(RetrieveSystemSettings("FAS_USER_ID")) : string.Empty;
                }
                catch (Exception ee)
                {
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                }
                return m_sFASUserId;
            }
        }

        /// <summary>
        ///Property to access m_sFASPassword.
        /// </summary>
        public string FASPassword
        {
            get
            {
                try
                {
                    m_sFASPassword = (Convert.ToString(RetrieveSystemSettings("FAS_PASSWORD"))) != string.Empty ? Convert.ToString(RetrieveSystemSettings("FAS_PASSWORD")) : string.Empty;
                    if (!string.IsNullOrEmpty(m_sFASPassword))
                    { m_sFASPassword = Riskmaster.Security.Encryption.RMCryptography.DecryptString(m_sFASPassword); }
                }
                catch (Exception ee)
                {
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                }
                return m_sFASPassword;
            }
        }
        //Asharma326 MITS 32386 Starts
        public string FASFileLocation
        {
            get
            {
                try
                {
                    m_sFASFilelocation = (Convert.ToString(RetrieveSystemSettings("FILE_LOCATION"))) != string.Empty ? Convert.ToString(RetrieveSystemSettings("FILE_LOCATION")) : string.Empty;
                }
                catch (Exception ee)
                {
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                }
                return m_sFASFilelocation;
            }
        }
        public string FASSharedLocation
        {
            get
            {
                try
                {
                    m_sFASSharedLocation = (Convert.ToString(RetrieveSystemSettings("SHARED_LOCATION"))) != string.Empty ? Convert.ToString(RetrieveSystemSettings("SHARED_LOCATION")) : string.Empty;
                }
                catch (Exception ee)
                {
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                }
                return m_sFASSharedLocation;
            }
        }
        //Asharma326 MITS 32386 Ends
        /// <summary>
        ///Property to access m_sFASFolder.
        /// </summary>
        public string FASFolder
        {
            get
            {
                try
                {
                    m_sFASFolder = (Convert.ToString(RetrieveSystemSettings("FAS_FOLDER"))) != string.Empty ? Convert.ToString(RetrieveSystemSettings("FAS_FOLDER")) : string.Empty;
                }
                catch (Exception ee)
                {
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                }
                return m_sFASFolder;
            }
        }
        //Ankit End
		/// <summary>
        ///Add by kuladeep for Unable to turn off Check Denied Diaries mits:25083
        /// </summary>
        public bool UseDiariesDen
        {
            get { return GetPropertyBool("UseDiariesDen"); }
            set
            {
                if (m_bUseDiariesDen != value)
                {
                    m_bDataChanged = true;
                    m_bUseDiariesDen = value;
                }
            }
        }
        /// <summary>
        ///Add by skhare7 for country code R8 Multilingual to set the format 
        /// </summary>
        //public int Country
        //{
        //    get
        //    {
        //        m_iCountry = (Conversion.CastToType<Int32>(RetrieveSystemSettings("COUNTRY_ID"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("COUNTRY_ID")) : 0;
        //        if (blnSuccess)
        //            return m_iCountry;
        //        else
        //            throw new RMAppException(Globalization.GetString("SysSettings.CastFailure"));
        //    }
        //}
        ///// <summary>
        /////Add by skhare7 for country code R8 Multilingual to set the format 
        ///// </summary>
        //public int PhoneFormat
        //{
        //    get
        //    {
        //        m_sPhoneFormat = (Conversion.CastToType<Int32>(RetrieveSystemSettings("PHONENUMBER_FORMAT"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("PHONENUMBER_FORMAT")) : 0;
        //        if (blnSuccess)
        //            return m_sPhoneFormat;
        //        else
        //            throw new RMAppException(Globalization.GetString("SysSettings.CastFailure"));
        //    }
        //}
        ///// <summary>
        /////Add by skhare7 for country code R8 Multilingual to set the format 
        ///// </summary>
        //public int ZipcodeFormat
        //{
        //    get
        //    {
        //        m_sZipcodeFormat = (Conversion.CastToType<Int32>(RetrieveSystemSettings("ZIPCODE_FORMAT"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("ZIPCODE_FORMAT")) : 0;
        //        if (blnSuccess)
        //            return m_sZipcodeFormat;
        //        else
        //            throw new RMAppException(Globalization.GetString("SysSettings.CastFailure"));
        //    }
        //}
        ///// <summary>
        /////Add by skhare7 for country code R8 Multilingual to set the format 
        ///// </summary>
        //public int Dateformat
        //{
        //    get
        //    {
        //        m_sDateformat = (Conversion.CastToType<Int32>(RetrieveSystemSettings("DATE_FORMAT"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("DATE_FORMAT")) : 0;
        //        if (blnSuccess)
        //            return m_sDateformat;
        //        else
        //            throw new RMAppException(Globalization.GetString("SysSettings.CastFailure"));
        //    }
        //}
        ///// <summary>
        /////Add by skhare7 for country code R8 Multilingual to set the format 
        ///// </summary>
        //public int Timeformat
        //{
        //    get
        //    {
        //        m_sTimeformat = (Conversion.CastToType<Int32>(RetrieveSystemSettings("TIME_FORMAT"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("TIME_FORMAT")) : 0;
        //        if (blnSuccess)
        //            return m_sTimeformat;
        //        else
        //            throw new RMAppException(Globalization.GetString("SysSettings.CastFailure"));
        //    }
        //}
        //smishra54 R8.1 Point Policy Interface
        /// <summary>
        ///R8.1 Point Policy Interface 
        /// </summary>
        public int PolicySearchCount
        {
            get
            {
                m_PolicyInterfaceSearchCount = (Conversion.CastToType<Int32>(RetrieveSystemSettings("POLICY_SEARCH_COUNT"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("POLICY_SEARCH_COUNT")) : 0;
                if (blnSuccess)
                    return m_PolicyInterfaceSearchCount;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //skhare7 R8 Multilingual

    //mbahl3 Strataware enhancement      
        public int StratawareUserID
        {
            get
            {
                m_StratawareUserID = (Conversion.CastToType<Int32>(RetrieveSystemSettings("STRATAWARE_USER_ID"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("STRATAWARE_USER_ID")) : 0;
                if (blnSuccess)
                    return m_StratawareUserID;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
		   //mbahl3 Strataware enhancement 
        /// <summary>
        /// Policy system Interface : Will be used to open  External policy in the Point's application in GC and WC when checked.
        /// </summary>
        public bool OpenPointPolicy
        {

            get
            {
                m_bOpenPointPolicy = (Conversion.CastToType<Int32>(RetrieveSystemSettings("OPEN_POINT_POLICY"), out blnSuccess)) == 0 ? false : true;

                if (blnSuccess)
                    return m_bOpenPointPolicy;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //ygoyal3 JIRA 5504
        public bool AllowAutoLogin
        {
            get
            {
                m_bAllowAutoLogin = (Conversion.CastToType<Int32>(RetrieveSystemSettings("ALLOW_AUTO_LOGIN"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bAllowAutoLogin;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure",m_iClientId));
            }
        }
        //Ended:Yukti

        //neha goel 11212013 MITS#33414 for WWIG        
        public bool NoReqFieldsForPolicySearch
        {

            get
            {
                m_bNoReqFieldsForPolicySearch = (Conversion.CastToType<Int32>(RetrieveSystemSettings("NO_REQ_FLD_POLSEARCH"), out blnSuccess)) == 0 ? false : true;

                if (blnSuccess)
                    return m_bNoReqFieldsForPolicySearch;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        public bool UseStagingPolicySystem
        {            
            get
            {

                m_bUseStagingPolicySystem = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_STAGING_POL_SYS"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUseStagingPolicySystem;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //neha goel end for WWIG

        /*gbindra WWIG MITS#34104 01312014 START*/
        /// <summary>
        /// Property to access m_bAllowNotesAtClaimant
        /// </summary>
        public bool AllowNotesAtClaimant
        {
            get
            {
                m_bAllowNotesAtClaimant = (Conversion.CastToType<Int32>(RetrieveSystemSettings("ENH_NOTE_CLMNT"), out blnSuccess)) == 0 ? false : true;
                
                if (blnSuccess)
                    return m_bAllowNotesAtClaimant;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        /*gbindra WWIG MITS#34104 01312014 END*/

        
		//MITS:35039
        /// <summary>
        /// Add a new property for search code description
        /// </summary>
        public int SearchCodeDescription
        {

            get
            {
                m_SearchCodeDescription = (Conversion.CastToType<Int32>(RetrieveSystemSettings("SEARCH_CODE_DESC"), out blnSuccess)) != 0 ? Convert.ToInt32(RetrieveSystemSettings("SEARCH_CODE_DESC")) : 0;
                if (blnSuccess)
                    return m_SearchCodeDescription;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        /// <summary>
        ///Bkuzhanthaim : MITS-36026/RMA-338: OrgHierarchy as Payee Type (-1 True ; 0 False) from GeneralSystemParameterSetup : Start
        ///</summary>
        public bool UseEntityRole
        {
            get
            {
                m_bUseEntityRole = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_ENTITY_ROLE"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUseEntityRole;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        ///<summary>
        ///Author :Nitika Gupta
        ///Description : Property to access m_bUseTPA
        ///</summary>
        public bool UseTPA
        {
            get
            {
                m_bUseTPA = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_TPA"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUseTPA;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        //added by swati agarwal for PMC
        public bool UseDCIReportingFields
        {
            get
            {
                m_bUseDCIFields = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_DCI_FIELDS"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUseDCIFields;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        public bool UseCLUEReportingFields
        {
            get
            {
                m_bUseCLUEFields = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_CLUE_FIELDS"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUseCLUEFields;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //change end here by swati

        //agupta298 - PMC GAP08 - start - RMA-4694
        public bool UseNMVTISReqFields
        {
            get
            {
                m_bUseNMVTISReqFields = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_NMVTIS_FIELDS"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bUseNMVTISReqFields;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //agupta298 - PMC GAP08 - End - RMA-4694

        //tanwar2 - ImageRight - start
        public bool UseImgRight
        {
            get
            {
                m_bIR = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_IMAGE_RIGHT"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bIR;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }

        public bool UseImgRightWebService
        {
            get
            {
                m_bIR = (Conversion.CastToType<Int32>(RetrieveSystemSettings("USE_IMAGE_RIGHT_WS"), out blnSuccess)) == 0 ? false : true;
                if (blnSuccess)
                    return m_bIR;
                else
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
            }
        }
        //tanwar2 - ImageRight - end
        
        //asharma326 JIRA# 6422 Starts 
        private bool m_bDateStampHTMLText = false;
        public bool DateStampHTMLText
        {

            get
            {
                try
                {
                    bool blnSuccess = false;
                    string DateStampHTMLText = RetrieveSystemSettings("DATE_STAMP_HTML");
                    m_bDateStampHTMLText = DateStampHTMLText != string.Empty ? (Conversion.CastToType<Int32>(DateStampHTMLText, out blnSuccess) == -1 ? true : false) : false;
                }
                catch (Exception ee)
                {
                    throw new RMAppException(Globalization.GetString("SysSettings.CastFailure", m_iClientId));
                }
                return m_bDateStampHTMLText;
            }
        }
    } 
        //asharma326 JIRA# 6422 Ends
        
        

    /// <summary>
    /// The Structure to store the Settings from the Sys_settings
    /// </summary>
    internal class SystemSettings
    {
        internal SystemSettings()
        {
        }
        // Create Public Properties to access the variables
        public int RowId
        {
            get;
            set;
        }
        public string ParamKey
        {
            get;
            set;
        }
        public string ParamValue
        {
            get;
            set;
        }


        public string ParamDataType
        {
            get;
            set;
        }

        public bool DataChanged
        {
            get;
            set;
        }


    }

}

