using System;
using System.Threading;
using Riskmaster.LSSExportService;
using System.Diagnostics;

	/**************************************************************
	 * $File		: LSSExportService.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 09/12/2006
	 * $Author		: Manoj Agrawal
	 * $Comment		: ExportService class has functions to start/stop Export Service
	 * $Source		: 
	 ************************************************************
	 * Amendment History
	 * ************************************************************
	 * Date Amended		*	Author		*	Amendment   
	 **************************************************************/

namespace Riskmaster.LSSExportService
{
    /// <summary>
    /// 
    /// </summary>
	public class ExportService : System.ServiceProcess.ServiceBase
	{
		private ExportProcess objExportProcess = null;		//Manoj
		private Thread listenerThread;						//Manoj
        private int m_iClientId = 0;

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

        /// <summary>
        /// Default class constructor
        /// </summary>
		public ExportService()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitComponent call
		}

		// The main entry point for the process
		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
	
			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = new System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
			//
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new ExportService() };

			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = "LSSExportService";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// Name		: OnStart
		/// Author		: Manoj Agrawal
		/// Date Created: 09/12/2006
		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		protected override void OnStart(string[] args)
		{
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(WorkerThreadHandler);

			objExportProcess = new ExportProcess(m_iClientId);
			objExportProcess.Run = true;

			listenerThread = new Thread(new ThreadStart(objExportProcess.Export));
			listenerThread.Start();

			//objExportProcess.Export();
			//this.OnStop();
		}

		/// Name		: OnStop
		/// Author		: Manoj Agrawal
		/// Date Created: 09/12/2006
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
			if(objExportProcess != null)
			{
				objExportProcess.Run = false;
				//if(listenerThread.IsAlive == false)
				//	objExportProcess = null;
			}
		}

        void WorkerThreadHandler(object sender, UnhandledExceptionEventArgs args)
        {
            if (!(args.ExceptionObject is ThreadAbortException))
            {
                Exception exc = args.ExceptionObject as Exception;
                // log error
                EventLog DemoLog = new EventLog("Application");
                DemoLog.Source = "LSSExportService";
                DemoLog.WriteEntry(exc.Message + exc.StackTrace, EventLogEntryType.Error);
            }
        }
	}
}
