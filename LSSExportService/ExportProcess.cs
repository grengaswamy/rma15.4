using System;
using System.Data;
using System.Xml;
using System.Configuration;
using Riskmaster.Application.LSSInterface;
using Riskmaster.Common;
using Riskmaster.Db;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using LSSExportService.LSSDataExchangeWS35;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Specialized;
using System.ServiceModel;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Net;
using System.Net.Security;
using Riskmaster.Security.Encryption; //spahariya -12/15/2011 MITS 26834

namespace Riskmaster.LSSExportService
{
    /// <summary>
    /// Provides the ability to export Claims, Admin, Code, Payment Status from RMX to LitigationAdvisor
    /// </summary>
	internal class ExportProcess
	{
		private bool bRun = false;
        //Populate all configuration settings
        private NameValueCollection nvColl = null; 
        const int SEC = 1000;		//as time is specified in miliseconds
        const int MIN = 60 * SEC;		//as time is specified in miliseconds
        const int HOUR = 60 * MIN;	//as time is specified in miliseconds
        private int iSleepTime = 30 * MIN;	//default value -> 30 min//
        private string m_sUser = string.Empty, m_sPwd = string.Empty, m_sDsnName = string.Empty;
        private string m_strLoggingEnabled = string.Empty;
        private int m_iClientId = 0;
       

        /// <summary>
        /// Default class constructor
        /// which initializes all member variables
        /// </summary>
        internal ExportProcess(int p_iClientId)
        {
            //spahariya start -12/15/2011 MITS 26834 - To decrypt the encrypted silent LSS user and password.
            //m_sUser = nvColl["User"]
            //m_sPwd = nvColl["Pwd"];
            //m_sDsnName = nvColl["DsnName"];
            // LSSInterface setting will be same for all multi-tenant clients
            nvColl = RMConfigurationManager.GetNameValueSectionSettings("LSSInterface", string.Empty, 0);

            m_sUser = RMCryptography.DecryptString(nvColl["User"]);
            m_sPwd = RMCryptography.DecryptString(nvColl["Pwd"]);
            m_sDsnName = RMCryptography.DecryptString(nvColl["DsnName"]);
            //spahariya end -12/15/2011 MITS 26834 
            m_strLoggingEnabled = nvColl["Logging"];
            m_iClientId = p_iClientId;            
        } // method: ExportProcess

        

		#region Property Declaration
		/// <summary>
		/// Run propert is user to stop the execution
		/// </summary>
		public bool Run
		{
			get{return bRun;}
			set{bRun = value;}
		}
		#endregion

		#region Public Execute Method
		/// <summary>		
		/// This method is used to export all type of records to LSS one by one
		/// </summary>
		public void Export()
		{
			

			int iMaxRetryCount = 5;
			int iRetryCount = 1;	//from 1 to iMaxRetryCount

			
			int iTemp = 30*MIN;
			string sTo = "";
			string sCc = "";
			string sFrom = "";
			string sSubjectFail = "";
			string sBodyFail = "";
			string sSubjectSuccess = "";
			string sBodySuccess = "";
            try
            {			
	   		    iTemp = Convert.ToInt32(nvColl["SleepTime"])*MIN;
			    //sleep time should be between 5min to 24hours. defualt value is 30min.
			    if (iTemp < 5*MIN)
				    iTemp = 5*MIN;
			    else if(iTemp > 24*HOUR)
				    iTemp = 24*HOUR;
			    else
				    iTemp = 30*MIN;
			    iSleepTime = iTemp;

			    //mail is sent in case of exception
			    sTo = nvColl["MailTo"];
                sCc = nvColl["MailCc"];
			    sFrom = nvColl["MailFrom"];
                sSubjectFail = nvColl["MailSubjectFail"];
                sBodyFail = nvColl["MailBodyFail"];
                sSubjectSuccess = nvColl["MailSubjectSuccess"];
                sBodySuccess = nvColl["MailBodySuccess"];
                //this outer while loop is for normal processing. this loop keeps iterating.
                //inner while loop iterates for 5 times in case of exception. otherwise iterates only once.
                while (bRun)		//bRun is set or reset from Win Service
                {	//start conditional loop here
                    if (bRun)		//OnStop function might be called in between so checking before checking bRun each export.
                    {
                        iRetryCount = 1;
                        while (iRetryCount <= iMaxRetryCount && bRun)
                        {
                            try
                            {
                                ExportRecord("Code");
                                ExportRecord("Admin");
                                ExportRecord("Claim");
                                ExportRecord("Reserve");//Added by sharishkumar for Mits 35472
                                ExportRecord("PaymentStatus");
                                ExportRecord("Payee");//Sumit - Added for LSS Export/Import of Payee for MCIC 12/10/2008
                                ExportRecord("Check");//Sumit - Added for LSS Export/Import of Checks for MCIC 12/29/2008
                                //processign has reached here means it was successful transaction.
                                if (iRetryCount > 1)		//this means there was some exception and now it has processed it successully. so send successful e-mail.
                                {
                                    try		//this has been put in try catch because I dont want to catch e-mail errors.
                                    {
                                        //send success e-mail here
                                        SendMailMessage(iMaxRetryCount, iRetryCount, sTo, sCc, sFrom, sSubjectSuccess, sBodySuccess, true, string.Empty);
                                    }
                                    catch { }
                                }
                                break;	//break inner while loop. because processing was successful.
                            }//try
                            catch (Exception ex)
                            {
                                Riskmaster.Application.LSSInterface.CommonFunctions.logExportError(String.Format("Retry count {0} of total {1}. Exception: {2}", iRetryCount, iMaxRetryCount, ex.Message), m_iClientId);
                                bool rethrow = ExceptionPolicy.HandleException(ex, "Logging Policy");
                                try		//this has been put in try catch because I dont want to catch e-mail errors.
                                {
                                    //send exception e-mail here
                                    SendMailMessage(iMaxRetryCount, iRetryCount, sTo, sCc, sFrom, sSubjectFail, sBodyFail, false, ex.Message);
                                }
                                catch { }
                                iRetryCount++;
                                System.Threading.Thread.Sleep(iSleepTime);	//sleep for specified time before retry
                                if (iRetryCount > iMaxRetryCount)
                                {
                                    throw ex;
                                }
                            }//catch
                        }//while
                    }//if

                    System.Threading.Thread.Sleep(5000);		//add a delay of 5 seconds here so there will be gap to start loop again
                }//while

				
			}
			catch (Exception p_objException)
			{
                Riskmaster.Application.LSSInterface.CommonFunctions.logExportError(String.Format("Export process stopped because of exception: {0}", p_objException.Message), m_iClientId);
			}
			finally
			{
				bRun = false;
			}
                
        }//method: Export



        /// <summary>
        /// Sends e-mail message notifications for successful or failed 
        /// LSS export and import jobs
        /// </summary>
        /// <param name="iMaxRetryCount"></param>
        /// <param name="iRetryCount"></param>
        /// <param name="sTo"></param>
        /// <param name="sCc"></param>
        /// <param name="sFrom"></param>
        /// <param name="strSubject"></param>
        /// <param name="strBody"></param>
        /// <param name="blnSuccess"></param>
        /// <param name="strExceptionDetails"></param>
        private void SendMailMessage(int iMaxRetryCount, int iRetryCount, string sTo, string sCc, string sFrom, string strSubject, string strBody,
            bool blnSuccess, string strExceptionDetails)
        {
            using (Mailer objMail = new Mailer(m_iClientId))
            {
                try
                {
                    if (blnSuccess)
                    {
                        //send success e-mail here
                        objMail.To = sTo;
                        objMail.Cc = sCc;
                        objMail.From = sFrom;
                        objMail.Subject = strSubject.Replace("#RetryCount#", iRetryCount.ToString()).Replace("#MaxCount#", iMaxRetryCount.ToString());
                        objMail.Body = strBody.Replace("#RetryCount#", iRetryCount.ToString()).Replace("#MaxCount#", iMaxRetryCount.ToString());
                        objMail.SendMail();
                    } // if
                    else
                    {
                        //send exception e-mail here
                        objMail.To = sTo;
                        objMail.Cc = sCc;
                        objMail.From = sFrom;
                        objMail.Subject = strSubject.Replace("#RetryCount#", iRetryCount.ToString()).Replace("#MaxCount#", iMaxRetryCount.ToString()).Replace("#Exception#", strExceptionDetails);
                        objMail.Body = "Exception has occured during RMX-LSS interface data transfer. " +
                            "\n\n" + "This is retry count " + iRetryCount.ToString() + " of total " + iMaxRetryCount.ToString() + "." +
                            "\n\n" + "Sleep time is " + iSleepTime / MIN + " Min." +
                            "\n\n" + "Exception message: " + strExceptionDetails +
                            "\n\n" + strBody.Replace("#RetryCount#", iRetryCount.ToString()).Replace("#MaxCount#", iMaxRetryCount.ToString()).Replace("#Exception#", strExceptionDetails);
                        objMail.SendMail();
                    } // else
                }
                catch (System.Net.Mail.SmtpException ex)
                {
                    //Handle any Mailer exceptions here
                }
                finally
                {
                    //Perform any additional cleanup here
                }
            }//using
            
        }//method: SendMail

            
		#endregion

		#region Private ExportRecord method
		/// <summary>		
		/// This method is used to export individual type of records to LSS
		/// </summary>
        /// <param name="sExportType"></param>
        private void ExportRecord(string sExportType)
		{
            string sExpIDs = string.Empty;
            string sDSN = string.Empty;
            string sExportXml = string.Empty, sResponse=string.Empty, sReqId = string.Empty;
            string sCurrentTime = string.Empty, sStatusCd = string.Empty, sErrorCd = string.Empty;
            string sStatusDesc = string.Empty, sStatusNode = string.Empty, sTableName = string.Empty;
            string sPackType = string.Empty;

			XmlNode objReqIdNode = null;
			XmlNode objStatusCdNode = null;
			XmlNode objErrorCdNode = null;
			XmlNode objStatusDescNode = null;

			XmlDocument objDoc = null;
			XmlDocument objResponseDoc = null;

            //logging1
            int iReqId = 0;
            int iEventId = 0;
            bool bSuccess = false;
           

            //gagnihotri for Test, will be removed finally
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(IgnoreCertificateErrorHandler);

            //Instantiate the LSS WCF Service
            //DataExchangeClient objExportWS = null;
            DataExchangeClient objExportWS = null;

            ExportProcessFactory objExportFactory = new ExportProcessFactory(m_sUser, m_sPwd, m_sDsnName, m_iClientId);
            IExportProcess objExportProcess = objExportFactory.CreateExportProcess();
            sDSN = objExportProcess.RiskmasterConnectionString;

			
			try
			{
				sCurrentTime = Conversion.GetDateTime(DateTime.Now.ToString());

				switch(sExportType)
				{
					case "Code":
						sStatusNode = "SPExtensionsSyncRs";
						sTableName = "RM_LSS_CODE_EXP";
						sPackType = "INCD";
						objExportProcess.CodeExport(ref sExportXml, ref sExpIDs, sCurrentTime);
						break;

					case "Admin":
						sStatusNode = "ClaimsSubsequentRptSubmitRs";
						sTableName = "RM_LSS_ADMIN_EXP";
						sPackType = "ADMN";
						objExportProcess.AdminExport(ref sExportXml, ref sExpIDs, sCurrentTime);
						break;

					case "Claim":
						sStatusNode = "ClaimsNotificationAddRs";
						sTableName = "RM_LSS_CLAIM_EXP";
						sPackType = "CLAM";
						objExportProcess.ClaimExport(ref sExportXml, ref sExpIDs, sCurrentTime);
						break;

					case "PaymentStatus":
						sStatusNode = "SPExtensionsSyncRs";
						sTableName = "RM_LSS_PAYMNT_EXP";
						sPackType = "PAYM";
						objExportProcess.PaymentStatusExport(ref sExportXml, ref sExpIDs, sCurrentTime);
						break;
                    //Sumit - Added for LSS Export/Import of Payee for MCIC 12/10/2008
                    case "Payee":
                        sStatusNode = "ClaimsSubsequentRptSubmitRs";
                        sTableName = "RM_LSS_PAYEE_EXP";
                        sPackType = "PDEX";
                        objExportProcess.PayeeExport(ref sExportXml, ref sExpIDs, sCurrentTime);
                        break;
                    //Sumit - Added for LSS Export/Import of Check for MCIC 12/29/2008
                    case "Check":
                        sStatusNode = "ClaimsSubsequentRptSubmitRs";
                        sTableName = "RM_LSS_CHECK_EXP";
                        sPackType = "CKEX";
                        objExportProcess.CheckExport(ref sExportXml, ref sExpIDs, sCurrentTime);
                        break;
                    //Added by sharishkumar for Mits 35472
                    case "Reserve":
                        sStatusNode = "ClaimsNotificationAddRs";
                        sTableName = "RM_LSS_RESERVE_EXP";
                        sPackType = "RESV";
                        objExportProcess.ReserveExport(ref sExportXml, ref sExpIDs, sCurrentTime);
                        break;
                        //End Mits 35472
					default:
						break;
				}

				if (! string.IsNullOrEmpty(sExportXml.Trim()))
				{
					if(m_strLoggingEnabled.ToUpper().Equals("YES"))
					{
						iReqId = Utilities.GetNextUID(sDSN, "DX_REQUEST", m_iClientId);
                        iEventId = Utilities.GetNextUID(sDSN, "DX_REQUEST_EVENT", m_iClientId);
					}
                    Riskmaster.Application.LSSInterface.CommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "RMX", "STRT", sExportXml.Trim(), sDSN);
                    Riskmaster.Application.LSSInterface.CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), sDSN);
                    Riskmaster.Application.LSSInterface.CommonFunctions.iLogDXEventHist(iEventId, "STRT", sDSN, m_iClientId);

					//logging3 - begin getdata
                    Riskmaster.Application.LSSInterface.CommonFunctions.iLogDXEventHist(iEventId, "BGET", sDSN, m_iClientId);
					
					objDoc = new XmlDocument();
					objDoc.LoadXml(sExportXml);

					//logging4 - end getdata
                    Riskmaster.Application.LSSInterface.CommonFunctions.iLogDXEventHist(iEventId, "EGET", sDSN, m_iClientId);

					if (objDoc!=null)
					{
                        //Instantiate the service only if document information exists
                        objExportWS = new DataExchangeClient();

                        ////Pass the user name and password credentials to the service
                        //spahariya start -12/15/2011 MITS 26834 - To decrypt the encrypted LSS user and password.
                        //objExportWS.ClientCredentials.UserName.UserName = nvColl["LSSUser"];
                        //objExportWS.ClientCredentials.UserName.Password = nvColl["LSSPwd"];
                        objExportWS.ClientCredentials.UserName.UserName = RMCryptography.DecryptString(nvColl["LSSUser"]);
                        objExportWS.ClientCredentials.UserName.Password = RMCryptography.DecryptString(nvColl["LSSPwd"]);
                        //spahariya end -12/15/2011 MITS 26834

                        //Do not need to explicitly open the WCF connection call
                        //objExportWS.Open();
		
						//logging7 - begin webservice call
                        Riskmaster.Application.LSSInterface.CommonFunctions.iLogDXEventHist(iEventId, "BWEB", sDSN, m_iClientId);

						sResponse = objExportWS.Execute(objDoc.OuterXml);

                        //Close the opened service
                        objExportWS.Close();

						//logging8 - end webservice call
                        Riskmaster.Application.LSSInterface.CommonFunctions.iLogDXEventHist(iEventId, "EWEB", sDSN, m_iClientId);

						if(! string.IsNullOrEmpty(sResponse.Trim()))
						{
							objResponseDoc = new XmlDocument();
							objResponseDoc.LoadXml(sResponse);

							foreach (XmlNode objResponseNode in objResponseDoc.GetElementsByTagName(sStatusNode))
							{
								objStatusCdNode = objResponseNode.SelectSingleNode("MsgStatus/MsgStatusCd");
								objErrorCdNode = objResponseNode.SelectSingleNode("MsgStatus/MsgErrorCd");
								objStatusDescNode = objResponseNode.SelectSingleNode("MsgStatus/MsgStatusDesc");
								objReqIdNode = objResponseNode.SelectSingleNode("RqUID");

								if(objStatusCdNode != null)
									sStatusCd = objStatusCdNode.InnerText;

								if(objErrorCdNode != null)
									sErrorCd = objErrorCdNode.InnerText;

								if(objStatusDescNode != null)
									sStatusDesc = objStatusDescNode.InnerText;

								if(objReqIdNode != null)
									sReqId = objReqIdNode.InnerText;


								//update database table only when call is successful
								if(objStatusCdNode != null && objStatusCdNode.InnerText.Equals("Success"))
								{

                                    DbFactory.ExecuteNonQuery(sDSN, String.Format("UPDATE {0} SET SENT_FLAG = -1, DTTM_SENT = '{1}' WHERE REQ_ID = '{2}' AND DTTM_CHANGED <= '{1}' AND (SENT_FLAG IS NULL OR SENT_FLAG = 0)", sTableName, sCurrentTime, sReqId), new System.Collections.Generic.Dictionary<string, string>());
									bSuccess = true;
								}
								else
								{
									//don't mark record as 'sent' if not successful.
									//we are making SENT_FLAG = -2 for such records so that records are not picked in next run.
									//This is as per request from LSS. They don't want to resent any record from RMX if that was failed in first try.
                                    
                                    DbFactory.ExecuteNonQuery(sDSN, String.Format("UPDATE {0} SET SENT_FLAG = -2 WHERE REQ_ID = '{1}' AND DTTM_CHANGED <= '{2}' AND (SENT_FLAG IS NULL OR SENT_FLAG = 0)", sTableName, sReqId, sCurrentTime), new System.Collections.Generic.Dictionary<string, string>());
									bSuccess = false;
                                    //Added by sharishkumar for Mits 35472
                                    if (sExportType == "Reserve")
                                    {
                                        string sRcRowID = Convert.ToString(DbFactory.ExecuteScalar(sDSN, String.Format("SELECT RESERVE_ID FROM {0} WHERE REQ_ID = '{1}' AND DTTM_CHANGED <= '{2}' AND (SENT_FLAG IS NULL OR SENT_FLAG = -2)", "RM_LSS_RESERVE_EXP", sReqId, sCurrentTime), new System.Collections.Generic.Dictionary<string, string>()));
                                        if (!string.IsNullOrEmpty(sRcRowID))
                                            DbFactory.ExecuteNonQuery(sDSN, "UPDATE RESERVE_CURRENT SET LSS_RES_EXP_FLAG = 0 where RC_ROW_ID=" + sRcRowID);
                                    }
                                    //End Mits 35472
								}

								objReqIdNode = null;
								objStatusCdNode = null;
								objErrorCdNode = null;
								objStatusDescNode = null;
							}	//foreach
						
							objResponseDoc = null;
						}						
					}

					objDoc = null;

					//logging9 - final entry
					if(bSuccess)
					{
                        Riskmaster.Application.LSSInterface.CommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "RMX", "CMPD", sExportXml.Trim(), sDSN);
                        Riskmaster.Application.LSSInterface.CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", sResponse.Trim(), Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), sDSN);
                        Riskmaster.Application.LSSInterface.CommonFunctions.iLogDXEventHist(iEventId, "CMPD", sDSN, m_iClientId);
					}
					else
					{
                        Riskmaster.Application.LSSInterface.CommonFunctions.iLogDXRequest(iReqId, sPackType, "FRRM", "RMX", "FAIL", sExportXml.Trim(), sDSN);
                        Riskmaster.Application.LSSInterface.CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", sResponse.Trim(), Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), sDSN);
                        Riskmaster.Application.LSSInterface.CommonFunctions.iLogDXEventHist(iEventId, "FAIL", sDSN, m_iClientId);
					}

				} //if block end
			}	 //try
            catch (FaultException p_objException)
            {
                Riskmaster.Application.LSSInterface.CommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, sDSN, m_iClientId);
                bool rethrow = ExceptionPolicy.HandleException(p_objException, "Logging Policy");
                throw (p_objException);
            }
			catch (Exception p_objException)
			{
				//logging10 - log exception
                Riskmaster.Application.LSSInterface.CommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, sDSN, m_iClientId);
                bool rethrow = ExceptionPolicy.HandleException(p_objException, "Logging Policy");
                throw (p_objException);
			}
			finally
			{
				objResponseDoc = null;
				objDoc = null;

                //Clean up
                objExportFactory = null;
                objExportProcess = null;
                objExportWS = null;

			}
		}//ExportRecord method end

        //gagnihotri for Test, will be removed finally
        public static bool IgnoreCertificateErrorHandler(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

		#endregion

        //private void LogErrorsCustom(string sMsg)
        //{
        //    using (FileStream fs = new FileStream(@"D:\Source\R7 Source\LogsLss\Error.log", FileMode.OpenOrCreate, FileAccess.ReadWrite))
        //    {
        //        using (StreamWriter sw = new StreamWriter(fs))
        //        {
        //            sw.WriteLine("Start Log");
        //            sw.WriteLine(DateTime.Now.ToString("yyyyMMddhhmmss"));
        //            sw.WriteLine(sMsg);
        //            sw.WriteLine("End Log");
        //        }
        //    }
        //}
	}

}
