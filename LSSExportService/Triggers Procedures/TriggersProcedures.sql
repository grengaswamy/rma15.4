-- =============================================
-- ssingh47: Trigger for Codes table only work for Claim Type and Injury type codes
-- =============================================
IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = 'trCodes_IU' 
	   AND 	  type = 'TR')
    DROP TRIGGER trCodes_IU
GO

CREATE TRIGGER trCodes_IU
ON Codes
FOR INSERT, UPDATE 
AS 
Declare @sSysTabName varchar(50)
Declare @UpdatedByUser varchar(50)


select @sSysTabName = g.system_table_name from glossary g inner join inserted i on g.table_id=i.table_id where g.system_table_name in ('CLAIM_TYPE','INJURY_TYPE')
select @UpdatedByUser = UPDATED_BY_USER from inserted

If (UPDATE(short_code) OR UPDATE(deleted_flag)) AND @UpdatedByUser <> 'lssinter'  AND @sSysTabName is not null
BEGIN

If (select count(*) from deleted) > 0
BEGIN
	insert into RM_LSS_CODE_EXP (CODE_ID, OLD_SHORT_CODE,DTTM_CHANGED,
        	DTTM_SENT,TYPE_TEXT,SENT_FLAG,REQ_ID)
	select i.code_id, d.short_code, CONVERT(char(8),getdate(),112) + REPLACE(CONVERT(char(8),getdate(),8),':',''), null, Case @sSysTabName WHEN 'CLAIM_TYPE' THEN 'LINEOFBUSINESS' WHEN 'INJURY_TYPE' THEN 'NATUREOFINJURY' END,0,null
      		from deleted d left outer join inserted i
      		on d.code_id = i.code_id
END
ELSE
BEGIN
	insert into RM_LSS_CODE_EXP (CODE_ID, OLD_SHORT_CODE,DTTM_CHANGED,
        	DTTM_SENT,TYPE_TEXT,SENT_FLAG,REQ_ID)
	select i.code_id, i.short_code, CONVERT(char(8),getdate(),112) + REPLACE(CONVERT(char(8),getdate(),8),':',''), null, Case @sSysTabName WHEN 'CLAIM_TYPE' THEN 'LINEOFBUSINESS' WHEN 'INJURY_TYPE' THEN 'NATUREOFINJURY' END,0,null
      		from inserted i

END
END
If @@error <> 0
BEGIN
	RAISERROR ('Unexpected error in trigger trCodes_IU', 16, 1)
	ROLLBACK TRANSACTION
END
GO




-- =============================================
-- ssingh47: Trigger for Codes_Text table only work for Claim Type and Injury type codes
-- =============================================
IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = 'trCodes_Text_U' 
	   AND 	  type = 'TR')
    DROP TRIGGER trCodes_Text_U
GO

CREATE TRIGGER trCodes_Text_U
ON Codes_Text
FOR UPDATE 
AS 
Declare @sSysTabName varchar(50)

select @sSysTabName = g.system_table_name from codes c, inserted i, glossary g where c.code_id=i.code_id and c.table_id=g.table_id and g.system_table_name in ('CLAIM_TYPE','INJURY_TYPE')

If UPDATE(CODE_DESC) AND @sSysTabName is not null
AND (select min(cast(CONVERT(char(8),getdate(),112) + REPLACE(CONVERT(char(8),getdate(),8),':','') as bigint) - cast(a.DTTM_CHANGED as bigint))
from RM_LSS_CODE_EXP a, inserted i where a.code_id=i.code_id) > 5
BEGIN
	insert into RM_LSS_CODE_EXP (CODE_ID, OLD_SHORT_CODE,DTTM_CHANGED,
        	DTTM_SENT,TYPE_TEXT,SENT_FLAG,REQ_ID)
	select i.code_id, d.short_code, CONVERT(char(8),getdate(),112) + REPLACE(CONVERT(char(8),getdate(),8),':',''), null, Case @sSysTabName WHEN 'CLAIM_TYPE' THEN 'LINEOFBUSINESS' WHEN 'INJURY_TYPE' THEN 'NATUREOFINJURY' END,0,null
      		from deleted d left outer join inserted i
      		on d.code_id = i.code_id
END
If @@error <> 0
BEGIN
	RAISERROR ('Unexpected error in trigger trCodes_IU', 16, 1)
	ROLLBACK TRANSACTION
END
GO

-- =============================================
-- Debabrata Biswas : Trigger for Entity Table only work for Entity(People/Entity) Deletion Only
-- =============================================
IF EXISTS (SELECT name 
	   FROM   sysobjects 
	   WHERE  name = 'trEntity_U' 
	   AND 	  type = 'TR')
    DROP TRIGGER trEntity_U
GO

CREATE TRIGGER trEntity_U
ON ENTITY
FOR UPDATE
AS
DECLARE @sEntityType varchar(50)
DECLARE @sUpdatedByUser varchar(16)
DECLARE @sLSSRMXUser varchar(16)
DECLARE @sDeletedFlag varchar(2)

SELECT @sEntityType = UPPER(G.SYSTEM_TABLE_NAME) FROM GLOSSARY G INNER JOIN INSERTED E
ON E.ENTITY_TABLE_ID = G.TABLE_ID
WHERE G.TABLE_ID = E.ENTITY_TABLE_ID

IF @sEntityType = 'ADJUSTERS'
BEGIN
	SET @sEntityType = 'ADJUSTER'
END
SELECT @sUpdatedByUser = E.UPDATED_BY_USER, @sDeletedFlag = E.DELETED_FLAG FROM INSERTED E

SELECT @sLSSRMXUser = RMX_LSS_USER FROM SYS_PARMS

IF (@sDeletedFlag = '-1')
BEGIN
	IF @sUpdatedByUser <> @sLSSRMXUser
	BEGIN
		IF @sEntityType IS NOT NULL AND @sEntityType <> ''
		BEGIN
			IF @sEntityType = 'ADJUSTER'
			BEGIN
				INSERT INTO RM_LSS_ADMIN_EXP (ENTITY_ID, DTTM_CHANGED, TYPE_TEXT, SENT_FLAG) 
				SELECT E.ENTITY_ID, CONVERT(char(8),getdate(),112) + REPLACE(CONVERT(char(8),getdate(),8),':',''), @sEntityType , 0 FROM INSERTED E
			END
			ELSE
			BEGIN
				INSERT INTO RM_LSS_PAYEE_EXP (ENTITY_ID, DTTM_CHANGED, TYPE_TEXT, SENT_FLAG) 
				SELECT E.ENTITY_ID, CONVERT(char(8),getdate(),112) + REPLACE(CONVERT(char(8),getdate(),8),':',''), @sEntityType , 0 FROM INSERTED E
			END	
		END
	END
END

If @@error <> 0
BEGIN
	RAISERROR ('Unexpected error in trigger trEntity_U', 16, 1)
	ROLLBACK TRANSACTION
END
			ELSE
			BEGIN
				INSERT INTO RM_LSS_PAYEE_EXP (ENTITY_ID, DTTM_CHANGED, TYPE_TEXT, SENT_FLAG) 
				SELECT EXR.ENTITY_ID, CONVERT(char(8),getdate(),112) + REPLACE(CONVERT(char(8),getdate(),8),':',''), @sEntityType , 0 FROM ENTITY_X_ROLES EXR
			END	
		END
	END
END

If @@error <> 0
BEGIN
	RAISERROR ('Unexpected error in trigger trEntity_U', 16, 1)
	ROLLBACK TRANSACTION
END
END
GO
