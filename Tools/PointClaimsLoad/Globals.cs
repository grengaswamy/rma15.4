﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

using CCP.LossBalance;
using CCP.Common;
using System.Data;
using Riskmaster.Db;

using Riskmaster.Security;

namespace CCP.PointClaimsLoad
{
    public class Globals
    {
        public static string sXMLRec;
        public static string ErrString;
        public static string sErrMsg;
        public static string sModuleName;
        public static string sProcName;
        public static string sActivitiesUploaded;
        //start : bsharma33 Code reverted RMA-9738
        //public static string UseDBWriter;// RMA-9738:aaggarwal29 
        //ends  : bsharma33 Code reverted RMA-9738
        //Processing Switch Constants
        public const string SWStartClaim = "START";
        public const string SWDataDSN = "DataDSN";
        public const string SWDataUser = "DataUser";
        public const string SWDataPswd = "DataPswd";
        public const string SWVendorDSN = "VendorDSN";
        public const string SWVendorUser = "VendorUser";
        public const string SWVendorPswd = "VendorPswd";
        public const string SWSupportDSN = "SupportDSN";
        public const string SWSupportUser = "SupportUser";
        public const string SWSupportPswd = "SupportPswd";
        public const string SWTranslateDSN = "TranslateDSN";
        public const string SWTranslateUser = "TranslateUser";
        public const string SWTranslatePswd = "TranslatePswd";
        public const string SWClientDSN = "ClientDSN";
        public const string SWClientUser = "ClientUser";
        public const string SWClientPswd = "ClientPswd";
        public const string SWRelease = "Release";
        public const string conDiscreteLogging = "DISCRETELOGGING";
        public const string conTransEnabled = "TRANSACTIONSENABLED";
        public const string conDummyClaim = "XXX";
        public const string conSWAdjusterNumber = "Adjuster";
        public const string conAdjusterNumber = "000145";
        // UPDATE | [ADD] | NO
        public const string conSWUpdateVendor = "UPDATEVENDOR";
        public const string conSWEntityUpdateFile = "ENTITYUPDATES.XML";
        public const string conSWVendorType = "VENDORTYPE";
        public const string conSWVendorTypeBusType = "BUSTYPE";
        public const string conSWVendorTypeEntCatg = "ENTCATEGORY";
        public const string conLUTable = "UCT_LOOKUP_TRANSLATE";
        public const string conSNATable = "STATE_NAME_ABBREV";
        public const string conContactTypeSW = "ContactType";
        public const string conContactType = "BS:CP:DL:FX:IH:ML:PG:TF:TP";
        public const string conFormatSW = "FORMAT";
        public const string conTaxIDSW = ".TAXID";
        public const string conPhoneSW = ".PHONE";
        public const string conADJAssignLevel = "ADJASSIGN";
        public const string conADJAssignLevelClaim = "CLAIM";
        public const string conADJAssignLevelClaimant = "CLAIMANT";
        public const string conADJAssignExaminer = "EXAMINER";
        public const string conADJ = "ADJ";
        public const string conADJDesc = "Adjustor";
        public const string conEXM = "EXM";
        public const string conEXMDesc = "Examiner";
        public const string conPHLD = "PHLD";
        public const string conSWPOINTVendor = "POINTNUM";
        public const string conExmrLit = "POINTEXMR";
        public const string conClientID = "CLIENTIDTYPE";
       // public const string conClientLit = "POINTCLIENT"; commented as no nore required, since the value is coming from POLICY_X_WEB now
        public const string conVendorLit = "POINTNUM";
        //public const string conPOINTClient = "POINTCLIENT"; commented as no nore required, since the value is coming from POLICY_X_WEB now
        //public const conNoStateCode As String = "ZZ"
        public const string conNoStateCode = "99";
        public const string conNoAddress = "ZZ";
        public const string conNoCity = "ZZ";
        public const string conNoPostalCode = "ZZZZZZ";
        public const string conNoCounty = "ZZ";
        public const string conNoCountry = "USA";
        public const string conVendorCategorySW = "VENDORCAT";
        public const string conVendorCategories = ":VENDOR:PAYEE:";
        public const string conWPAEntryNameSW = "WPAENTRYNAME";
        public const string conWPAEntryNotesSW = "WPAENTRYNOTES";
        public const string conWPAEntryName = "Error During Upload";
        public const string conWPAAssigningUserSW = "WPAASSIGNINGUSER";
        public const string conWPAAssigningUser = "SYSTEM";
        public const string conWPAPrioritySW = "WPAPRIORITY";
        public const int conWPAPriority = 3;
        public const string conWPARoleSW = "WPAROLE";
        public const string conWPAAssignedToSW = "WPAASSIGNEDTO";
        public const string conWPASendToSW = "WPASEND";

        //Lookup Variables
        // Server name for field lookups & name parsing
        public const string sLookUpDBConnStrSW = "LOOKUPDBCONNSTR";
        // Server name for field lookups & name parsing
        public const string sLookUpDBServerSW = "LOOKUPDBSERVER";
        // DSN name for field lookups & name parsing
        public const string sLookUpDBDSNSW = "LOOKUPDBDSN";
        // DB name for field lookups & name parsing
        public const string sLookUpDBDBSW = "LOOKUPDBDATABASE";
        // DB name for field lookups & name parsing
        public const string sLookUpDBProviderSW = "LOOKUPDBPROVIDER";
        // DB name for field lookups & name parsing
        public const string sLookUpDBDriverSW = "LOOKUPDBDRIVER";
        // Look up DB user ID
        public const string sLookUpDBUserSW = "LOOKUPDBUSER";
        // Look up DB user Password
        public const string sLookUpDBPswdSW = "LOOKUPDBPSWD";


        public static bool bIsNewClaimUpload = false;
        public static bool bContinue;
        public static bool gNewVendor;
        public static System.Collections.ArrayList collEntityUpdates;
        public static System.Collections.ArrayList collClaimantUpdates;
        public static bool bRet;
        public static bool bNewClaim;
        public static bool bNewClaimant;
        public static bool bupdateDefaultDOB;
        public static bool bNewReserve;
        public static bool bStatusChangeTrans;
        public static bool bWCompDataProcessed;
        public static bool bWCompInsLine;
        public static bool bUpdateAIA;
        public static bool bProcessingReserve;
        public static bool bProcessingPayment;
        public static bool bProcessCheckDraft;
        public static string sClaimNumber;
        //Executing an offset/onset process
        public static bool bProcessOffsetOnset;
        public static string sHistSeq; // Change on 12/27
        public static string sNewClaimNumber;
        //
        public static long lNewLossDate;

        public static string sNewCatCode;
        //Indicates Onset/Offset processing
        public static bool bOffsetOnsetLossDetail;
        //This is an Offset transaction
        public static bool bOffsetLossDetail;
        //This is an Onset transaction
        public static bool bOnsetLossDetail;
        //This is an Offset transaction
        public static bool bPaymentOffset;
        public static bool bOffsetOnsetPaymentOffset;
        public static int sCLMTSEQ;
        public static int sPolCovSeq;
        public static string sResvno;
        public static string sResvType;
        public static string sADJRole;
        public static string sEXMRole;
        public static string sPHLDRole;

        public static string strRelationshipTypeCode;
        public static string strEntityType;
        public static string strFullName;
        public static string strDBA;
        public static string strNamePrefix;
        public static string strNameSuffix;
        public static string strLongName;
        public static string strFirstName;
        public static string strMiddleName;
        public static string strLastName;
        public static string[] strAddress = new string[4];
        public static string strCityState;
        public static string strCity;
        public static string strStateCode;
        public static string strPostalCd;
        public static string strCounty;
        public static string strCountry;
        public static string strGender;
        public static string strUnformatedTaxId;
        public static string strTaxId;
        public static string strLongTaxId;
        public static string strFein;
        public static string strBirthDay;
        public static string strMaritalStatus;
        public static string strSex;
        public static string strSICCode;
        public static long lEntityID;
        public static string strVenNumber;
        //SI05603
        public static long lClientNumber;
        public static long lAddrSeqNumber;// Changed 12/27
        public static string strCategory;
        public static string strTaxIdType;
        public static string[] strPhone = new string[3];
        //SI05421
        public static string[] strContact = new string[3];
        public static string sPayCode;
        public static int iResvNo;
        public static string sOrigClaimNumber;
        public static string sBodyPart;
        public static string sNatureOfInjury;
        public static string sCauseOfInjury;
        public static string[] str1099Address = new string[4];
        public static string str1099CityState;
        public static string str1099City;
        public static string str1099StateCode;
        public static string str1099PostalCd;
        public static string str1099County;
        public static string str1099Country;
        public static bool b1099Address;
        public static bool bUploadClaimantStatus;
        public static string sClaimantStatus;

        public static string ExaminerCd;
        // Programming Global Variables

        public static string strFunction;
        public static CCP.XmlComponents.XMLClaim XMLClaim;

        // Database Variables
        public static double cPointRelease;
        public const float POINT_RELEASE9 = 9;
        public const float POINT_INJ_GLBL = 15;
        public const float POINT_INJ_SAMSUNG = 1500;//  'SI07731 /Changed on 12/27
        public static bool bNewFileGUID;
        public static bool bNewFileProcess;
        public static CCP.LossBalance.LossBalance objLossBal;
        public static float cClmPaidAmount;
        public static float cClmReserveAmount;
        public static float cClmReserveChange;
        public static long lClmTransactionCount;
        public static long lClmOffsetOnsetCount;
        public static long lClmUnitStatCount;
        public static Debug dbg;
        public static string strDatabaseDat;
        public static string sCurrentEntryDate;
        public static string sYYYYMMDDNow;

        public static string sHHNNSS000000Now;
        
        public const int INDEMNITY_RESERVE = 9;
        /**************************************************************
        NOTE:  These pointers must never be assigned new values and must remain
        constant through out this entire application*/
        public static PMSPCL20ClaimsMasterFile objPCL20 = new PMSPCL20ClaimsMasterFile();
        public static PMSPCL14WCompClaimsInfoFile objPCL14 = new PMSPCL14WCompClaimsInfoFile();
        public static PMSPCL30ClaimantMasterFile objPCL30 = new PMSPCL30ClaimantMasterFile();
        public static PMSPCT30ClientMasterFile objPCT30 = new PMSPCT30ClientMasterFile();
        public static PMSPCL42ClaimCoverageFile objPCL42 = new PMSPCL42ClaimCoverageFile();
        public static PMSPCL50ClaimsResPayTrans objPCL50 = new PMSPCL50ClaimsResPayTrans();
        public static DRFTClaimsDraftFile objDRFT = new DRFTClaimsDraftFile();
        public static PMSPAP00CheckFile objAP00 = new PMSPAP00CheckFile();
        public static PMSPAD00AdjustorMasterFile objAD00 = new PMSPAD00AdjustorMasterFile();
        public static PMSPCL92AutomaticIndemnityFile objPCL92 = new PMSPCL92AutomaticIndemnityFile();
        public static BASCLT0100 objBASCLT0100 = new BASCLT0100();
        public static BASCLT0300 objBASCLT0300 = new BASCLT0300();
        public static BASCLT1700 objBASCLT1700 = new BASCLT1700();
        public static Utils objUtils = new Utils();
        public static PMSPTX00TaxIdFile objPTX00 = new PMSPTX00TaxIdFile();
        public static PMSP0000PointALUpload objPMSP0000 = new PMSP0000PointALUpload();
        /**************************************************************************************/

        public static string PolicySystemName;
        public static UploadConfiguration objConfig;
        public static DbWriter objWriter;
        public static Riskmaster.Db.DbConnection dbConnection;
        public static Riskmaster.Db.DbTransaction dbTrans;
        public static StreamWriter objLogWriter = null;
        public static UserLogin objRMUser = null;
        public static Dictionary<int, string> drDateTimeStmap; //JIRA 4729: aaggarwal29
        public static void InitForClaim(PMSPCL20ClaimsMasterFile InitRec)
        {
            Globals.dbg.PushProc("InitForClaim");
            InitRec.Clear();

            InitRec.PMSPCL20RecStatus = "O";
            InitRec.PMSPCL20ClmOffice = "000";
            InitRec.PMSPCL20ExaminerCd = "00";
            InitRec.PMSPCL20LossTime = "12:00:00";
            InitRec.PMSPCL20RptTime = "12:00:00";
            InitRec.PMSPCL20ClmMadeDte = 0;
            InitRec.PMSPCL20RptByNme = "INSURED";
            InitRec.PMSPCL20NxtRvwDte = 0;
            InitRec.PMSPCL20Examiner = " ";
            InitRec.PMSPCL20ClmDesc = " ";
            InitRec.PMSPCL20CatCode = " ";
            InitRec.PMSPCL20DescCode = "0";

            Globals.dbg.PopProc();
        }
        public static void InitForUnitStat(PMSPCL14WCompClaimsInfoFile InitRec)
        {
            Globals.dbg.PushProc("InitForUnitStat");
            InitRec.Clear();
            InitRec.PMSPCL14LsMarStat = "U";
            InitRec.PMSPCL14LsNbrDeps = 0;
            InitRec.PMSPCL14LsDob = "00000000";
            InitRec.PMSPCL14LsDod = "00000000";
            InitRec.PMSPCL14LsCurrAww = 0;
            InitRec.PMSPCL14CurrCompRt = 0;
            InitRec.PMSPCL14HistDate = "0000000";
            InitRec.PMSPCL14HistSeq = 1;
            InitRec.PMSPCL14InjuryZip = "0000000000";
            InitRec.PMSPCL14ClassNum = "000000";
            InitRec.PMSPCL14Fein = "000000000";
            InitRec.PMSPCL14EmplrSic = Convert.ToString(InitRec.PMSPCL14EmplrSicFieldLength);
            InitRec.PMSPCL14EmpPayInd = "0";
            InitRec.PMSPCL14OthrWkPymt = 0;
            InitRec.PMSPCL14DateRepEmp = "0000000";
            InitRec.PMSPCL14PostInjWge = 0;
            InitRec.PMSPCL14PctImpair = 0;
            InitRec.PMSPCL14DteOfDisab = "0000000";
            InitRec.PMSPCL14DteMaxMed = "0000000";

            Globals.dbg.PopProc();

        }
        public static void InitForClaimant(PMSPCL30ClaimantMasterFile InitRec)
        {
            Globals.dbg.PushProc("InitForClaimant");
            InitRec.Clear();
            InitRec.PMSPCL30RecStatus = "O";

            Globals.dbg.PopProc();
        }
        public static void InitForClientFile(PMSPCT30ClientMasterFile InitRec)
        {
            Globals.dbg.PushProc("InitForClientFile");
            InitRec.Clear();
            InitRec.PMSPCT30Ct30type = "CL";
            InitRec.PMSPCT30ClientType = "C";
            Globals.dbg.PopProc();
        }
        public static void InitAddressWork()
        {
            Globals.dbg.PushProc("InitAddressWork");
            strRelationshipTypeCode = "";
            strEntityType = "";
            strNamePrefix = "";
            strFirstName = "";
            strMiddleName = "";
            strLastName = "";
            strLongName = "";
            strDBA = "";
            strNameSuffix = "";
            strAddress[0] = "";
            strAddress[1] = "";
            strAddress[2] = "";
            strAddress[3] = "";
            strCity = "";
            strStateCode = "";
            strPostalCd = "";
            strCounty = "";
            strCountry = "";
            strGender = "";
            strSex = "";
            strTaxId = "";
            strLongTaxId = "";
            strFein = "";
            lEntityID = 0;
            //strPhone = ""           'SI05421
            strPhone[0] = "";
            //SI05421
            strPhone[1] = "";
            //SI05421
            strPhone[2] = "";
            //SI05421
            strBirthDay = "";
            strMaritalStatus = "";
            strSICCode = "";
            lClientNumber = 0;
            lAddrSeqNumber = 0; // Changed on 12/27
            //SI05603
            Globals.dbg.PopProc();
        }
        public static void InitForClaimCoverageFile(PMSPCL42ClaimCoverageFile InitRec)
        {
            Globals.dbg.PushProc("InitForClaimCoverageFile");
            Parameters objParams = (Parameters)Globals.GetParams();
            InitRec.Clear();
            InitRec.PMSPCL42RecStatus = "O";
            //InitRec.PMSPCL42Adjustor = objParams.Parameter(Globals.conSWAdjusterNumber); commented by bsharma33

            InitRec.PMSPCL42Examiner = "00";
            InitRec.PMSPCL42ClaimOff = "000";

            if(Globals.objConfig.ConfigElement.AddDefaultExaminerCD == "YES")
            {
                if(!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.ExaminerCD))
                {
                    InitRec.PMSPCL42Examiner = Globals.objConfig.ConfigElement.ExaminerCD;
                }
            }

            if (Globals.objConfig.ConfigElement.AddDefaultClmOffice == "YES")
            {
                if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.ClmOffice))
                {
                    InitRec.PMSPCL42ClaimOff = Globals.objConfig.ConfigElement.ClmOffice;
                }
            }


            if (Globals.objConfig.ConfigElement.AddDefaultAdjustor == "YES")
            {
                if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.Adjustor))
                {
                    InitRec.PMSPCL42Adjustor = Globals.objConfig.ConfigElement.Adjustor;
                }
            }

            Globals.dbg.PopProc();
        }

        public static void InitForPointALUploadFile(PMSP0000PointALUpload InitRec)
        {
            dbg.PushProc("InitForPointALUploadFile");
            InitRec.Clear();
            //InitRec.PMSP0000OPER0ID = GetParams().Parameter(SWDataUser);
            InitRec.PMSP0000OPER0ID = "rmA";

            dbg.PopProc();
        }
        public static void InitForReservePaymentTransaction(PMSPCL50ClaimsResPayTrans InitRec)
        {
            Globals.dbg.PushProc("InitForReservePaymentTransaction");
            Parameters objParams = (Parameters)Globals.GetParams();
            InitRec.Clear();
            InitRec.PMSPCL50RecStatus = "N";
            InitRec.PMSPCL50TranEntTm = "12000001";
            InitRec.PMSPCL50ClmOffice = "000";
            InitRec.PMSPCL50ExaminerCd = "00";
            InitRec.PMSPCL50ExaminerCd = ExaminerCd;

            if (Globals.objConfig.ConfigElement.AddDefaultExaminerCD == "YES")
            {
                if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.ExaminerCD))
                {
                    InitRec.PMSPCL50ExaminerCd = Globals.objConfig.ConfigElement.ExaminerCD;
                }
            }

            if (Globals.objConfig.ConfigElement.AddDefaultClmOffice == "YES")
            {
                if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.ClmOffice))
                {

                    InitRec.PMSPCL50ClmOffice = Globals.objConfig.ConfigElement.ClmOffice;
                }
            }

            if (Globals.objConfig.ConfigElement.AddDefaultAdjustor == "YES")
            {
                if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.Adjustor))
                {
                    InitRec.PMSPCL50Adjustor = Globals.objConfig.ConfigElement.Adjustor;
                }
            }

            Globals.dbg.PopProc();
        }

        public static void InitForDraftFile(DRFTClaimsDraftFile InitRec)
        {
            Globals.dbg.PushProc("InitForDraftFile");

            InitRec.Clear();
            InitRec.DRFTAPHistory = "P";
            InitRec.DRFTPayType = "2";
            InitRec.DRFTPayStatus = "P";

            Globals.dbg.PopProc();
        }

        public static void InitForCheckFile(PMSPAP00CheckFile InitRec)
        {
            Globals.dbg.PushProc("InitForCheckFile");

            InitRec.Clear();
            InitRec.PMSPAP00PayType = "2";
            InitRec.PMSPAP00PayStatus = "P";

            Globals.dbg.PopProc();
        }

        public static void InitForAdjustorMasterFile(PMSPAD00AdjustorMasterFile InitRec)
        {
            Globals.dbg.PushProc("InitForAdjustorMasterFile");

            InitRec.Clear();
            InitRec.PMSPAD00TypeAdjustor = "V";

            Globals.dbg.PopProc();
        }

        public static void InitForTaxIdFile(PMSPTX00TaxIdFile InitRec)
        {
            Globals.dbg.PushProc("InitForTaxIdFile");

            InitRec.Clear();

            Globals.dbg.PopProc();
        }

        public static void InitForAutomaticIndemnityFile(PMSPCL92AutomaticIndemnityFile InitRec)
        {
            Globals.dbg.PushProc("InitForAutomaticIndemnityFile");

            InitRec.Clear();
            InitRec.PMSPCL92PayeeAge = " 0";
            InitRec.PMSPCL92MonthsEmpl = " 0";
            InitRec.PMSPCL92YearsEmpl = " 0";
            InitRec.PMSPCL92PaymntFreq = "0";

            Globals.dbg.PopProc();
        }

        public static DateTime GetDateTime(string dt)
        {
          if(string.IsNullOrEmpty(dt))
              {
                return Convert.ToDateTime("1/1/1900");
              }
          else
              {
                 return Convert.ToDateTime(dt);
              }
        }
        public static Parameters GetParams()
        {
            return Globals.dbg.Parameters;

        }

        public static CCP.Common.CommonFunctions CFunc()
        {
            return Globals.dbg.CommonFunctions;
        }

        public static FileFunctions CFleFnc()
        {
            return Globals.dbg.FileFunctions;
        }

        public static SystemInfo SysSettings()
        {
            return Globals.dbg.SystemInfo;
        }

        public object CDBFunc()
        {

            return null;

        }

        public static CCP.XmlFormatting.XML XMLObj()
        {
           return CCP.LossBalance.LossBalance.Instance.cXMLSControls.XML;//.cXML;//.cXMLSControls.XML; 
        }















    }

   
}
