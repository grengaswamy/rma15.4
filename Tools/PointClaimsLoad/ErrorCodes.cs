﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.PointClaimsLoad
{
    public class ErrorCodes
    {
        public const int ERR_SEV_Undefined = -1;
        public const int ERR_SEV_Informational = 0;
        public const int ERR_SEV_Warning = 1;
        public const int ERR_SEV_Severe = 2;

        public const int ERR_SEV_Critical = 3;
        public const string ERR_SSEV_Undefined = "UNDEFINED";
        public const string ERR_SSEV_Information = "INFORMATION";
        public const string ERR_SSEV_Warning = "WARNING";
        public const string ERR_SSEV_Severe = "SEVERE";

        public const string ERR_SSEV_Critical = "CRITICAL";
        public const int ERR_TYPE_Undefined = -1;
        public const int ERR_TYPE_General = 0;
        public const int ERR_TYPE_Application = 1;
        public const int ERR_TYPE_Validation = 2;

        public const int ERR_TYPE_Operation = 3;
        public const string ERR_STYPE_Undefined = "UNDEFINED";
        public const string ERR_STYPE_General = "GENERAL";
        public const string ERR_STYPE_Application = "APPLICATION";
        public const string ERR_STYPE_Validation = "VALIDATION";
        public const string ERR_STYPE_Operation = "OPERATION";

        //Be Sure to put new error codes in the Error_Codes.TXT file
        public const string sErrCodesFile = "Error_Codes";
        public const string sErrCodesFileExt = ".txt";
        public const string ERRD_INVALID_ERROR_CODE = "Invalid Error Code";
        public const int  ERRC_USER_CODES = 300;
        public const int  ERRC_NO_DESCRIPTION = 0;
        public const int  ERRC_ERROR = 1;
        public const int  ERRC_ABORTED = 2;
        public const int ERRC_PERMISSION = 3;    //<%1>=Application <%2>=Function ID  SI05120
        public const int ERRC_COMPLETE = 4;     //<%1>=Function Name
        public const int ERRC_UCS_INITIALIZATION = 5;   //<%1>=App Name <%2>=DLL Name

        //Additional Messages and Text
        public const int ERRC_OK_CANCEL = 10;
        public const int ERRC_SKIP_ITEM = 11;
        public const int ERRC_CONFIRM_DELETE = 12;//<%1>=Item  <%2>=Item Name    'SI03696
        public const int ERRC_CONFIRM_PHYS_DELETE = 13;//Confirm physical delete of record
        public const int ERRC_CONFIRM_PHYS_DELETE_2 = 14;//Confirm physical delete of record
        public const int ERRC_REQUEST_DELETE = 15;//Request Type of Delete

        //Termination Reasons
        public const int  ERRC_TERMINATE_OUT_OF_BALANCE = 50;

        //General Data Errors
        public const int ERRC_DATA_REQUIRED = 60;//<%1>=Field Name
        public const int  ERRC_VERIFY_DELETE = 61;
        public const int  ERRC_VERIFY_SAVE = 62;
        public const int ERRC_CODE_LOOKUP = 63;//<%1>=Code Table, <%2>=Short Code   'SI05495
        public const int ERRC_STATE_LOOKUP = 64;//<%1>=State Code                    'SI05495
        public const int ERRC_TABLE_LOOKUP = 65;//<%1>=Table Name                    'SI05495
        public const int ERRC_EXCEEDS_MAX_VALUE = 66;//<%1>=Field Name <%2>=Max Value     'SI06008
        public const int ERRC_EXCEEDS_MIN_VALUE = 67;//<%1>=Field Name <%2>=Min Value     'SI06008
        public const int ERRC_VERIFY_SAVE_NEW = 68;//Verify save on new record
        public const int  ERRC_VERIFY_OPERATION = 69;//Verify <%1> operation to perform   'SI06230

        //XML Processing Errors
        public const int  ERRC_XML_ERRORS = 100;
        public const int  ERRC_NO_DOCUMENT = 101;
        public const int  ERRC_NODE_NOT_FOUND = 102;
        public const int  ERRC_NO_NODE = 103;
        public const int  ERRC_ATTRIBUTE_NOT_FOUND = 104;
        public const int  ERRC_INVALID_NODE = 105;
        public const int  ERRC_INVALID_VALUE = 106;
        public const int  ERRC_NO_NODE_NAME = 107;
        public const int  ERRC_DOCUMENT_LOAD_FAILED = 108;
        public const int  ERRC_DOCUMENT_SAVE_FAILED = 109;
        public const int  ERRC_INVALID_OPERATION = 110;
        public const int  ERRC_NODE_COUNT_DIFFERENCE = 111;
        public const int  ERRC_DATA_VALIDATION_ERROR = 112;
        public const int  ERRC_INVALID_TIME_VALUE = 113;
        public const int  ERRC_INVALID_DATE_VALUE = 114;
        public const int ERRC_NEED_PARENT_COMP = 115;//1=component name
        public const int  ERRC_MISSING_ELEMENT_REF = 116;//'<%1> = target element        'SI05495
                                                         //'<%2> = reference id
                                                         //'<%3> = source element
                                                         //'<%4> = source element id



        public const int  ERRC_DIARY_NOT_ROLLABLE = 117;
        public const int  ERRC_DIARY_NOT_ROUTABLE = 118;
        public const int  ERRC_DIARY_NOT_VOIDABLE = 119;
        public const int  ERRC_DIARY_NOT_COMPLETED = 120;
        public const int  ERRC_DIARY_NOT_REJECTED = 121;

        //Parameter File Errors
        public const int  ERRC_PARAM_FILE = 1000;
        public const int  ERRC_PARAM_FILE_READ_GENERAL = 1001;
        public const int  ERRC_PARAM_FILE_WRITE_GENERAL = 1002;
        public const int  ERRC_PARAM_FILE_READ_NOTEXISTS = 1004;
        public const int  ERRC_PARAM_FILE_WRITE_EXISTS = 1005;

        //File Processing Errors
        public const int  ERRC_FILE = 1020;
        public const int  ERRC_FILE_READ_GENERAL = 1021;
        public const int  ERRC_FILE_WRITE_GENERAL = 1022;
        public const int  ERRC_FILE_NOTEXISTS = 1023;
        public const int  ERRC_FILE_EXISTS = 1024;
        public const int  ERRC_FILE_NONAME = 1025;
        public const int  ERRC_FILE_NO_FOLDER = 1026;
        public const int  ERRC_FILE_BLANK_REC = 1027;
        public const int  ERRC_FILE_READ_ONLY = 1028;
        public const int  ERRC_FILE_SAVE_SUCCESSFUL = 1029;
        public const int  ERRC_FILE_SAVE_UNSUCCESSFUL = 1030;

        //Parameter Errors
        public const int ERRC_PARAMETER_INVALID = 1010;// 1=Parameter Value

        //Merge Form Errors
        public const int  ERRC_MF = 1200;
        public const int  ERRC_MF_FIELD_DB = 1201;
        public const int  ERRC_MF_FILE_COLLISION = 1202;
        public const int  ERRC_MF_NO_RECORDS_FOUND = 1203;
        public const int  ERRC_MF_DATA_MISSING = 1204;
        public const int ERRC_MF_SUPP_FIELD_REF = 1205;//Form=<%1>: Field=<%2>: SrcTble=<%3>: SrcFld=<%4>. 'SI06879

        //Import Processing Errors
        public const int  ERRC_IMP_ERRORS = 1250;
        public const int  ERRC_IMP_CSV_FLD_CNT = 1251;
        public const int  ERRC_IMP_NO_CLAIM = 1252;
        public const int  ERRC_IMP_NO_COMPONENT = 1253;
        public const int  ERRC_IMP_NO_SUPP_DEFINED = 1254;
        public const int  ERRC_IMP_INCOMPAT_DATA_TYPES = 1255;
        public const int  ERRC_IMP_FF_RECLEN = 1256;
        public const int  ERRC_IMP_NO_CLAIMANT = 1257;
        public const int  ERRC_IMP_UNK_INV_IMPORT = 1258;
        public const int ERRC_IMP_NO_IMPORT_SWITCH = 1259;// <%1>=Import Function <%2>=App.Path
        public const int ERRC_IMP_MISSING_REFERENCE = 1260;//<%1>=Target Component <%2>=Missing Reference
        public const int ERRC_IMP_OH_PARENT_EID = 1261;//<%1>=Target Entity <%2> Parent ID <%3> Expected Parent ID
        public const int ERRC_IMP_OH_TABLE_ID = 1262;//<%1>=Target Entity <%2> Table ID <%3> Expected Table ID
        public const int ERRC_IMP_BANK_CODE = 1263;// <%1>=Value Submitted <%2> Value Using
        public const int ERRC_IMP_VCH_MISMATCH = 1264;//<%1>=Field Name <%2>=Submitted Value <%3>=Found Value
        public const int ERRC_IMP_RECORD_NOT_FOUND = 1265;//<%1>=Type of Record

        //Database Errors
        public const int  ERRC_DB_ERROR = 1270;
        public const int  ERRC_DB_NO_DB = 1271;
        public const int  ERRC_DB_NO_USER = 1272;
        public const int  ERRC_DB_LOOKUP_ERROR = 1273;
        public const int ERRC_DB_LOGIN = 1274;//Login Error: <%1>=DB <%2>=UID
        public const int ERRC_DB_NO_DB_OPEN = 1275;
        public const int ERRC_DB_LOGOFF_CONFIRM = 1276;//<%1>=DBname

        //Conversion Errors
        public const int  ERRC_CNV_NO_CLAIM = 1280;
        public const int  ERRC_CNV_RQRD_DATA = 1281;
        public const int  ERRC_CNV_INVALID_CLASS = 1282;
        public const int ERRC_CNV_INVALID_PAY_STATUS = 1283;//Status, Claim, Transid
        public const int ERRC_CNV_NO_DRFTFILE_REC = 1284;//Claim, ClmtSeq, CovSeq, Resvno, Transid

        //Claim Query / Extract Errors
        public const int  ERRC_CQ_ERROR = 1300;
        public const int ERRC_CQ_NO_CLAIM = 1301;// ClaimNum=<%1> ClaimID=<%2>
        public const int ERRC_CQ_NO_EVENT = 1302;// EventNum=<%1> EventID=<%2>
        public const int ERRC_CQ_NO_DIARY = 1303;//DiaryID=<%1> UserID=<%2> AttTable=<%3> AttRecID=<%4> SI06023
        public const int  ERRC_CQ_NO_PARAMS = 1304;

        //Processing Errors
        public const int ERRC_PROCESS_ABORTED = 1500;//1=Reason
        public const int  ERRC_OUT_OF_BALANCE = 1501;
        public const int  ERRC_GUID_NOTEXISTS = 1502;
        public const int  ERRC_GUID_EXISTS = 1503;
        public const int  ERRC_GUID_DIFFERENCE = 1504;
        public const int  ERRC_RESTART_NOTEXISTS = 1505;
        public const int ERRC_CRC_MISMATCH = 1506;//Table=<%1>: Record=<%2>

        //Scripting execution errors
        public const int  ERRC_SCRIPT_ERROR = 1540;
        public const int  ERRC_SCRIPT_FILE_NOTFOUND = 1541;
        public const int  ERRC_SCRIPT_PROCEDURE_NOTFOUND = 1542;
        public const int  ERRC_SCRIPT_ENGINE_LOAD = 1543;

        public const int  ERRC_SCRIPT_EXECUTION = 1544;

        //Collection errors
        public const int  ERRC_KEY_IS_NOT_UNIQUE = 35602;

        //RX Processing Errors
        public const int ERRC_RX_INTEGER_RQRD = 1601;//%1=Value Name

        //Custom Report Errors
        public const int  ERRC_CUSTOM_REPORTS = 1650;

        //Object Processing Errors
        public const int  ERRC_OBJ_ERRORS = 1700;
        public const int  ERRC_OBJ_SAVE = 1701;
        public const int  ERRC_OBJ_READ = 1702;

        //Application Errors
        public const int  ERRC_INITIALIZATION = 3000;

        //Security Management Errors
        public const int  ERRC_SECURITY_ERRORS = 1800;
        public const int  ERRC_SEC_NO_DOCUMENT_PATH = 1801;
        public const int  ERRC_SEC_INVALID_LICENSE_CODE = 1802;
        public const int  ERRC_SEC_SPACESIN_LOGINNAME = 1803;
        public const int  ERRC_SEC_DATABASE_ERROR = 1804;
        public const int  ERRC_SEC_PASSWORD_MISMATCH = 1805;
        public const int  ERRC_SEC_DSN_ALREADY_DEFINED = 1806;
        public const int  ERRC_SEC_LOGIN_ALREADY_DEFINED = 1807;
        public const int  ERRC_SEC_USER_ALREADY_DEFINED = 1808;
        public const int  ERRC_SEC_GROUP_ALREADY_DEFINED = 1809;
        public const int  ERRC_SEC_CHANGING_PASSWORD = 1810;
        public const int  ERRC_SEC_PASSWORD_CHANGED = 1811;
        public const int  ERRC_SEC_LOGIN_INVALID = 1812;
        public const int  ERRC_SEC_LICENSE_CORRUPT = 1813;
        public const int  ERRC_SEC_WARNING_DAYS = 1814;
        public const int  ERRC_SEC_CLOSE_WARNING = 1815;
        public const int  ERRC_SEC_MIN_LENGTH = 1816;
        public const int  ERRC_SEC_VERIFY_LOGINNAME = 1817;
        public const int  ERRC_SEC_VERIFY_ADDUSER = 1818;

        //Policy Download Errors
        public const int  ERRC_POLDNL_START = 2100;
        public const int  ERRC_POLDNL_END = 2125;

       //User Interface Errors
       //These need to be added to the ERROR_TYPE in UniversalConstants.bas
       //General Errors
        public const int  ERRC_FIELD_REQUIRED = 1;
        public const int  ERRC_CODE_NOT_EFFECTIVE = 2;
        public const int ERRC_DATE_LESS_THAN = 3;//Date Must Be >= <%1> (<%2>)
        public const int ERRC_DATE_GREATER_THAN = 4;//Date Must Be <= <%1> (<%2>)
        public const int ERRC_GREATER_THAN_MAX = 5;//Value Must Be <= <%1>
        public const int ERRC_LESS_THAN_MIN = 6;//Value Must Be >= <%1>
        public const int ERRC_TIME_LESS_THAN = 7;//Time Must Be >= <%1> (<%2>)
        public const int ERRC_TIME_GREATER_THAN = 8;//Time Must Be <= <%1> (<%2>)
        public const int ERRC_CODE_NOT_VALID = 9;//Code entered not valid (<%1>)
        public const int ERRC_DUPLICATE_RECORD = 10;//Record already exists with the following keys <%1>...
        public const int ERRC_START_SCREEN_ERRORS = 20;//Starting code for specific screen errors

        //Operational Errors
        public const int  ERRC_OPERATION_NOT_ALLOWED = 1;

        //Distribution Errors
        public const int  ERRC_DIST_NO_ENTITY_EMAIL = 1900;

        /*The following error codes referr to errors pulled from the CErrorCodes class.
          The numbering convention is as follows:
          From 1 to 15 positions:  Error code group;  This is either a form name, object name, or
          some other convienient grouping
          Followed by a '.'
          Followed by 1 to 4 position sequence number.*/
        public const string CERRC_MUST_SAVE = "COMMON.0001";
        public const string CERRC_FLD_REQUIRED = "COMMON.0002";
        public const string CERRC_CONTACT_SYSADMIN = "COMMON.0003";
        public const string CERRC_SPELL_CHECK_COMPLETE = "COMMON.0004";
        public const string ERR_FINANCIALS_ATTACHED = "COMMON.0005";
        public const string CERR_NO_PARENT = "COMMON.0006";
        public const string CERR_VALIDATION_ERR = "COMMON.0007";
        public const string CERR_DATANOTFOUND_ERR = "COMMON.0008";
        public const string CERR_DELETENOTALLOWED_ERR = "COMMON.0009";
        public const string CERR_RESERVELOCK_ERR = "COMMON.0010";
        public const string CERR_MULTIRECSREAD_ERR = "COMMON.0011";
        public const string CERR_UNKNOWN_ERR = "COMMON.0012";
        public const string CERR_UNKNOWN_ERR_CODE = "COMMON.0013";

        public const string CERR_EVENTACCESS_DENIED = "EVENT.0001";
        public const string CERR_EVENTCREATE_DENIED = "EVENT.0002";
        public const string CERR_CLAIMACCESS_DENIED = "CLAIM.0001";
        public const string CERR_CLAIMCREATE_DENIED = "CLAIM.0002";

        public const string SERR_INVPARENT_STR = "$" + CERR_NO_PARENT + "|Invalid Parent";
        public const string SERR_VALERROR_STR = "$" + CERR_VALIDATION_ERR + "|Validation Error.|";
        public const string SERR_DNFERROR_STR = "$" + CERR_DATANOTFOUND_ERR + "|Data Not Found.|";
        public const string SERR_DLTNAERROR_STR = "$" + CERR_DELETENOTALLOWED_ERR + "|Delete Not Allowed.|";
        public const string SERR_MULTIRECS_STR = "$" + CERR_MULTIRECSREAD_ERR + "|Multiple Records Retrieved.|";
        public const string SERR_RESERVELOCK_STR = "$" + CERR_RESERVELOCK_ERR + "|Can not lock reserves.|";

        public const string SERR_UNKNOWN_STR = "$" + CERR_UNKNOWN_ERR + "|Unknown Program Error.|";

        //MDI Parent Errors
        public const string CERRC_INITIALIZATION = "INITIALIZE.0001";//App Initialization Error

        //Policy Download Error Codes
        public const string CERRC_POLDOWNLOAD_SETUP = "POLDOWNLOAD.0001";//Can not determine download type
        public const string CERRC_POLDOWNLOAD_POLVALID = "POLDOWNLOAD.0002";//POLVALID did not load
        public const string CERRC_PAYEE_CONCAT_VERBIAGE = "CPMFUNDS.0001";//Join verbiage for multiple payees
        public const string CERRC_DM_NO_SYSTEM = "DM.0001";//DocManagement errors
        public const string MOVE_FIN_FOUND_HOLD = "MoveFinancials_Validation";
        public const string MOVE_CLAIAMNT_FOUND_FINANCIALS = "FinancialClaimant_Validation";
        public const string QUICK_DIARY_CREATE_ACCESS = "QuickDiary_Create_Validation";

}
}
