﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.PointClaimsLoad
{
    public class PMSPCL14WCompClaimsInfoFile
    {
        public string PMSPCL14Claim;
        public int PMSPCL14ClmtSeq;
        public string PMSPCL14LsMarStat;
        public int PMSPCL14LsNbrDeps;
        public string PMSPCL14LsDob;
        public string PMSPCL14LsDod;
        public string PMSPCL14LsOccCode;
        public string PMSPCL14LsLstWrkDa;
        public string PMSPCL14LsDteRetd;
        public string PMSPCL14LsRepIndr;
        public string PMSPCL14LsLegRep;
        public float PMSPCL14LsCurrAww;
        public float PMSPCL14CurrCompRt;
        public string PMSPCL14LsNote1;
        public string PMSPCL14LsNote2;
        public string PMSPCL14LsNote3;
        public string PMSPCL14LossEntDteYr;
        public string PMSPCL14LossEntDteMo;
        public string PMSPCL14LossEntDteDy;
        public string PMSPCL14HistDate;
        public int PMSPCL14HistSeq;
        public string PMSPCL14Act;
        public string PMSPCL14TypeLoss;
        public string PMSPCL14TypeRecv;
        public string PMSPCL14TypeCovg;
        public string PMSPCL14TypeSetmt;
        public string PMSPCL14InjCode;
        public long PMSPCL14Client;
        public string PMSPCL14LsStJuris;
        public string PMSPCL14InjuryZip;
        public string PMSPCL14HireDteYr;
        public string PMSPCL14HireDteMo;
        public string PMSPCL14HireDteDy;
        public string PMSPCL14EmplyStat;
        public string PMSPCL14ClassNum;
        public string PMSPCL14Fein;
        public string PMSPCL14EmplrSic;
        public string PMSPCL14EmpPayInd;
        public string PMSPCL14WageMeth;
        public float PMSPCL14OthrWkPymt;
        public string PMSPCL14DateRepEmp;
        public string PMSPCL14SurgInd;
        public int PMSPCL14PostInjWge;
        public int PMSPCL14PctImpair;
        public string PMSPCL14DteOfDisab;
        public string PMSPCL14DteMaxMed;
        public string PMSPCL14Deductible;
        public string PMSPCL14Controvert;
        public string PMSPCL14BoSocSec;
        public string PMSPCL14BoUnempl;
        public string PMSPCL14BoPension;
        public string PMSPCL14BoSpecial;
        public string PMSPCL14BoOther;
        public string PMSPCL14LsOccuptn;
        public string PMSPCL14DeductCode;
        public string PMSPCL14McoInd;

        public string PMSPCL14FraudClaim;
        //SI06135
        public int PMSPCL14EmplrSicFieldLength;

        public PMSPCL14WCompClaimsInfoFile()
        {
            Clear();
        }

        public void Clear()
        {
            PMSPCL14Claim = "";
            PMSPCL14ClmtSeq = 0;
            PMSPCL14LsMarStat = "";
            PMSPCL14LsNbrDeps = 0;
            PMSPCL14LsDob = "";
            PMSPCL14LsDod = "";
            PMSPCL14LsOccCode = "";
            PMSPCL14LsLstWrkDa = "";
            PMSPCL14LsDteRetd = "";
            PMSPCL14LsRepIndr = "";
            PMSPCL14LsLegRep = "";
            PMSPCL14LsCurrAww = 0;
            PMSPCL14CurrCompRt = 0;
            PMSPCL14LsNote1 = "";
            PMSPCL14LsNote2 = "";
            PMSPCL14LsNote3 = "";
            PMSPCL14LossEntDteYr = "";
            PMSPCL14LossEntDteMo = "";
            PMSPCL14LossEntDteDy = "";
            PMSPCL14HistDate = "";
            PMSPCL14HistSeq = 0;
            PMSPCL14Act = "";
            PMSPCL14TypeLoss = "";
            PMSPCL14TypeRecv = "";
            PMSPCL14TypeCovg = "";
            PMSPCL14TypeSetmt = "";
            PMSPCL14InjCode = "";
            PMSPCL14Client = 0;
            PMSPCL14LsStJuris = "";
            PMSPCL14InjuryZip = "";
            PMSPCL14HireDteYr = "0000";
            PMSPCL14HireDteMo = "00";
            PMSPCL14HireDteDy = "00";
            PMSPCL14EmplyStat = "";
            PMSPCL14ClassNum = "";
            PMSPCL14Fein = "";
            PMSPCL14EmplrSic = "";
            PMSPCL14EmpPayInd = "";
            PMSPCL14WageMeth = "";
            PMSPCL14OthrWkPymt = 0;
            PMSPCL14DateRepEmp = "";
            PMSPCL14SurgInd = "";
            PMSPCL14PostInjWge = 0;
            PMSPCL14PctImpair = 0;
            PMSPCL14DteOfDisab = "";
            PMSPCL14DteMaxMed = "";
            PMSPCL14Deductible = "";
            PMSPCL14Controvert = "";
            PMSPCL14BoSocSec = "N";
            //SI05268
            PMSPCL14BoUnempl = "N";
            //SI05268
            PMSPCL14BoPension = "N";
            //SI05268
            PMSPCL14BoSpecial = "N";
            //SI05268
            PMSPCL14BoOther = "N";
            //SI05268
            PMSPCL14LsOccuptn = "";
            PMSPCL14DeductCode = "";
            PMSPCL14McoInd = "";
            PMSPCL14FraudClaim = "";

            ////Start SI06135
            if (Globals.cPointRelease >= 13)
                {
                PMSPCL14EmplrSicFieldLength = 6;
                }
            else
                {
                PMSPCL14EmplrSicFieldLength = 4;
                }
            ////End SI06135
        }
    }
}
