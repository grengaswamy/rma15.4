﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCP.Common;
using System.IO;
using System.Configuration;

namespace CCP.PointClaimsLoad
{
    public class MainModule
    {
        static FileFunctions objFileFunc;
        static Parameters objParams;
        public static int iArgumentLength;
        public static int rc = 0;

       // public static void Main(string[] args)
        //{
            //    args = new string[6];
            //    args[0] = "1"; // Policy Sytem ID
            //    args[1] = "RMA141SP2POLICY"; // RM DSN
            //    args[2] = "POLICYUPLOAD_abk_20140519120722.XML"; // Upload file POLICYUPLOAD_abk_20130715192331.XML
            //    args[3] = "D:\\RMX\\RiskmasterCDR\\UI\\riskmaster\\appfiles\\Policy Interface"; // Upload path
            //    args[4] = "D:\\RMX\\RiskmasterCDR\\UI\\riskmaster\\userdata\\PolicyInterface\\PolicyUpload\\"; // log path
            //    args[5] = "csc";
            //iArgumentLength = args.Length;
            //LoadXML(args[0], args[1], args[2], args[3], args[4], args[5], "false");
         //}
        //start : bsharma33 Code reverted RMA-9738
        //public static string LoadXML(string iPolicySystemID,string sDSNName,string sXMLFile,string sUploadPath,string sLogPath,string sUserName, string sUseWriter)	
        public static string LoadXML(string iPolicySystemID,string sDSNName,string sXMLFile,string sUploadPath,string sLogPath,string sUserName)
        //ends  : bsharma33 Code reverted RMA-9738
        {

            string sFileName = string.Empty;
            StreamWriter sw = null;
            Globals.dbg = new CCP.Common.Debug();
            objFileFunc = new FileFunctions();
            Globals.objConfig = new UploadConfiguration(Convert.ToInt32(iPolicySystemID), sUserName, sDSNName,out Globals.objRMUser);
            Globals.cPointRelease = Conversion.ConvertStrToDouble(Globals.objConfig.ConfigElement.ReleaseVersion);
            //start : bsharma33 Code reverted RMA-9738
            //Globals.UseDBWriter = sUseWriter;// RMA-9738:aaggarwal29
            //ends  : bsharma33 Code reverted RMA-9738
            objParams = Globals.GetParams();
            objParams.Database = sDSNName;
            objParams.InputPath = sUploadPath;
            objParams.XMLFile = sXMLFile;
            objParams.LogPath = sLogPath;
            objParams.DataSource = Globals.objConfig.ConfigElement.DataDSN;
            objParams.Parameter(Globals.SWDataDSN, Globals.objConfig.ConfigElement.DataDSN);
            objParams.Parameter(Globals.SWDataUser, Globals.objConfig.ConfigElement.DataDSNUser);
            objParams.Parameter(Globals.SWDataPswd, Globals.objConfig.ConfigElement.DataDSNPwd);
            Globals.objLogWriter = new System.IO.StreamWriter(Globals.GetParams().LogPath + Globals.objConfig.ConfigElement.Name + "\\PointClaimsLoad_" + Globals.GetParams().XMLFile + ".log", true);
            Globals.objLogWriter.Write(Environment.NewLine + "---------------------------------------------------------------------------------------------------" + Environment.NewLine);
            Globals.objLogWriter.Write("Policy Upload Log");
            Globals.objLogWriter.Write(Environment.NewLine + "---------------------------------------------------------------------------------------------------" + Environment.NewLine);

            try
            {
                Globals.dbg.PushProc("Main");
                Initialize();
                if (Globals.bContinue)
                    GetXMLFileInfo();
                //changes for JIRA 7504 start
                if (!ScriptingFunctions.ScriptFileExist(out sFileName))
                {
                    Globals.objLogWriter.WriteLine("Scripting Warning : Script file  " + sFileName + "does not exist.");
                }
                //changes for JIRA 7504 end
               
                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlApp, ScriptingFunctions.conScpEvtStart);
                Globals.dbg.LogEntry("UploadParser");
                if (Globals.bContinue)
                    Parser.UploadParser();

                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlLoad, ScriptingFunctions.conScpEvtStart);

                if (Globals.bContinue)
                {
                    LoadDatabase.GetCurrentEntryDate();
                }
                Globals.dbg.LogEntry("ParseClaimDocXML");
                Globals.drDateTimeStmap = new Dictionary<int, string>();//JIRA 4729
                if (Globals.bContinue)
                {
                    Parser.ParseClaimDocXML();
                    rc = -2;
                }

                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlLoad, ScriptingFunctions.conScpEvtEnd);
                rc = 0;
            }
            catch(Exception ex)
            {
                //tanwar2 - Writing complete error log
                //Forcing log write with flush - though not the ideal way but seems the best option here.
                //Globals.objLogWriter.WriteLine("Error : " + ex.Message);
                Globals.objLogWriter.WriteLine("Error : " + ex.ToString());
                Globals.objLogWriter.Flush();
                //tanwar2 - end
            }
            finally
            {

                ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlApp, ScriptingFunctions.conScpEvtEnd);
                UpdateActivityLog();
                if (Globals.bContinue)
                    {
                       File.Move(objParams.InputPath + "\\" + objParams.XMLFile, objParams.InputPath + "\\" + "BKUP" + objParams.XMLFile);
                    }
                Terminate();
                Globals.dbg.PopProc();
                Globals.dbg = null;
                Globals.drDateTimeStmap = null; //JIRA 4729

            }
            return Globals.sActivitiesUploaded;


        }

        private static string UpdateActivityLog()
        {
            string sbSQL = null;
            string sActivityIds = Globals.sActivitiesUploaded;

            try
            {
                if (!string.IsNullOrEmpty(sActivityIds))
                {
                    sbSQL = " UPDATE ACTIVITY_TRACK SET UPLOAD_FLAG = 0 WHERE ACTIVITY_ROW_ID IN ( " + sActivityIds + ")";
                    Riskmaster.Db.DbFactory.ExecuteNonQuery(Globals.objRMUser.objRiskmasterDatabase.ConnectionString, sbSQL);
                }
            }
            catch(Exception ex)
            {
                //return string.Empty;
                Globals.objLogWriter.WriteLine("Activity track update failed. Activity IDs : " + sActivityIds);
                Globals.objLogWriter.WriteLine("Error : " + ex.Message);

            }

            return sActivityIds;
        }
        public static void GetXMLFileInfo()
        {
            string sTmp = string.Empty,sHost = string.Empty, sInputPath = string.Empty, sInputFile = string.Empty;

            SystemInfo objSytemInfo = Globals.SysSettings();

            Globals.dbg.PushProc("GetXMLFileInfo", objParams.Database, objParams.DatabaseSW, "", "");//objParams.DataSource

            if (!string.IsNullOrEmpty(objParams.Database))
            {
                Globals.strDatabaseDat = objParams.Database + ".dat";
            }
            else
            {
                Globals.strDatabaseDat = "";
            }
            Globals.dbg.DebugTrace(Globals.strDatabaseDat, "", "", "", "");

            if (objParams.XMLFile.Contains("\\"))
            {
                objFileFunc.SplitFileName(objParams.XMLFile, out sInputPath, out sInputFile);
                objParams.InputPath = sInputPath;
                objParams.InputFile = sInputFile;
                objParams.XMLFile = objParams.InputFile;
            }
            Globals.dbg.LogEntry("Reading:" + objParams.InputPath + "\\" + objParams.XMLFile);
            objFileFunc.FileDelete(objParams.InputPath, "BKUP" + objParams.XMLFile);
            Globals.bContinue = true;
            Globals.dbg.PopProc(objParams.InputPath, objParams.XMLFile, "", "");

        }

        private static void Initialize()
        {
            rc = -1;
            Globals.bContinue = true;
            Globals.dbg.PushProc("Initialize");

            if (iArgumentLength == 0) //Command 
            {
                Globals.dbg.Parameters.NoRead = true;
            }
            if(objParams.IsSwitch(Globals.SWStartClaim))
            {
                objParams.Parameter(Globals.SWStartClaim, StringUtils.Right(string.Format("000000000000", objParams.Parameter(Globals.SWStartClaim)), 12));
                Globals.bNewFileProcess = false;
            }

            if (objParams.IsSwitch(objParams.RerunSW))
            {
                Globals.bNewFileProcess = false;
            }
            else
            {
                Globals.bNewFileProcess = true;
            }

            Globals.collEntityUpdates = new System.Collections.ArrayList();
            Globals.collClaimantUpdates = new System.Collections.ArrayList();
            Globals.dbg.PopProc();
        }

        private static void Terminate()
        {
            Globals.dbg.PushProc("Terminate");

            //ScriptingFunctions.subEndScripting();
             objFileFunc = null;
             objParams = null;
            Globals.XMLClaim = null;
            Globals.collEntityUpdates = null;
            Globals.collClaimantUpdates = null;
            Globals.objBASCLT0300 = null;
            Globals.objBASCLT1700 = null;
            Globals.objBASCLT0100 = null;
            Globals.objPCL20 = null;
            Globals.objPCL14 = null;
            Globals.objPCL30 = null;
            Globals.objPCT30 = null;
            Globals.objPCL42 = null;
            Globals.objPCL50 = null;
            Globals.objDRFT = null;
            Globals.objAP00 = null;
            Globals.objAD00 = null;
            Globals.objPCL92 = null;
            Globals.objUtils = null;
            Globals.objConfig = null;
            Globals.objLossBal = null;
            Globals.objPTX00 = null;
            Globals.objUtils = null;

            if (Globals.objWriter != null)
                {
                if (Globals.objWriter.Connection == null)
                    {
                       Globals.objWriter = null;
                    }
                else
                    {
                       Globals.objWriter.Dispose();
                    }
                }
            if (Globals.dbConnection != null)
              {
              if (Globals.dbConnection.State == System.Data.ConnectionState.Open)
                  {
                    Globals.dbConnection.Close();
                  }
                Globals.dbConnection.Dispose();
              }
           
            if (Globals.dbTrans !=null)
                Globals.dbTrans.Dispose();
            if (Globals.objLogWriter != null)
                {
                  Globals.objLogWriter.Close();
                  Globals.objLogWriter.Dispose();
                }

            Globals.dbg.PopProc();
        }
        
        //private void GenEntityUpdateList()
        //{
        //    CCP.XmlComponents.XMLEntity xmlEntity = default(CCP.XmlComponents.XMLEntity);
        //    CCP.XmlFormatting.XML xmlObj = Globals.XMLObj();
        //    CCP.Constants.entType lEntityType;
        //    System.DateTime dtmCurrDate = default(System.DateTime);
        //    dtmCurrDate = System.DateTime.Today;
        //    System.TimeSpan dtmCurrTime = default(System.TimeSpan);
        //    dtmCurrTime = System.DateTime.Now.TimeOfDay;
        //    try
        //    {
        //        Globals.dbg.PushProc("GenEntityUpdateList", objParams.Parameter(Globals.conSWEntityUpdateFile), "", "", "");

        //        if (!objParams.IsTrue(Globals.conSWEntityUpdateFile))
        //        {
        //            return;
        //        }

        //        objFileFunc.FileDelete(objParams.OutPath, "BKUP" + objParams.Parameter(Globals.conSWEntityUpdateFile));

        //        if (objFileFunc.FileExists(objParams.OutPath, objParams.Parameter(Globals.conSWEntityUpdateFile)))
        //        {
        //            objFileFunc.FileRename(objParams.OutPath, objParams.Parameter(Globals.conSWEntityUpdateFile), objParams.OutPath, "BKUP" + objParams.Parameter(Globals.conSWEntityUpdateFile));
        //        }
        //        string sFileName = null;
        //        sFileName = "ENTITYUPDATES" + dtmCurrDate + dtmCurrTime;
        //        sFileName = sFileName.Replace(" ", "");
        //        sFileName = sFileName.Replace("/", "");
        //        sFileName = sFileName.Replace(":", "");
        //        Globals.XMLObj().XMLNewDoc(sFileName);
        //        Globals.XMLObj().Header.Name = "POINTENTITYUPDATE";
        //        Globals.XMLObj().Header.AppVersion = Globals.dbg.AppVersion;
        //        Globals.XMLObj().Header.SourceApplication = Globals.dbg.AppTitle;
        //        Globals.XMLObj().Header.SourceDB = objParams.DatabaseSW;
        //        Globals.XMLObj().Header.TargetDB = objParams.Database;
        //        Globals.XMLObj().Header.UserID = objParams.Parameter(Globals.SWDataUser);
        //        Globals.XMLObj().Header.Password = objParams.Parameter(Globals.SWDataPswd);
        //        Globals.XMLObj().Header.Description = "Entity ID Number Updates";
        //        Globals.XMLObj().Message.Create(null, CCP.Constants.pmType.pmmtRequest, "ENTITYUPDATE");
        //        Globals.XMLObj().Message.AddParameter("MATCHENTITYID", CCP.Constants.cxmlDataType.dtBoolean, "true", "");

        //        xmlEntity = new CCP.XmlComponents.XMLEntity();
        //        foreach (PMSPAD00AdjustorMasterFile objAD in Globals.collEntityUpdates)
        //        {
        //            if (xmlEntity.getEntitybyID(Convert.ToString(objAD.PMSPAD00EntityID)) == null)
        //            {
        //                if (objAD.EntityType == "BUSINESS")
        //                {
        //                    xmlEntity.Create(Convert.ToString(objAD.PMSPAD00EntityID), CCP.Constants.entType.entTypeBus, objAD.PMSPAD00LongTaxID, CCP.Constants.entTaxIdType.entTaxIdFEIN, "", "", "", "", "");
        //                }
        //                else
        //                {
        //                    if (objAD.EntityType == "INDIVIDUAL")
        //                    {
        //                        xmlEntity.Create(Convert.ToString(objAD.PMSPAD00EntityID), CCP.Constants.entType.entTypeInd, objAD.PMSPAD00LongTaxID, CCP.Constants.entTaxIdType.entTaxIdSSN, "", "", "", "", "");
        //                    }
        //                    else
        //                    {
        //                        xmlEntity.Create(Convert.ToString(objAD.PMSPAD00EntityID), CCP.Constants.entType.entTypeHou, objAD.PMSPAD00LongTaxID, CCP.Constants.entTaxIdType.entTaxIdUnd, "", "", "", "", "");
        //                    }
        //                }
        //            }
        //            xmlEntity.addIDNumber(objAD.PMSPAD00AdjustorNbr, objAD.NumberType);
        //        }

        //        if(Conversion.ConvertObjToBool(Globals.objConfig.ConfigElement.PointClient))
        //        {
        //            foreach (BASCLT0100 objCLT in Globals.collClaimantUpdates)
        //            {
        //                if (xmlEntity.getEntitybyID(Convert.ToString(objCLT.EntityID)) == null)
        //                {
        //                    //SI06356 Start
        //                    switch (objCLT.NAMETYPE)
        //                    {
        //                        case "I":
        //                            lEntityType = CCP.Constants.entType.entTypeInd;
        //                            break;
        //                        case "B":
        //                        case "G":
        //                            lEntityType = CCP.Constants.entType.entTypeBus;
        //                            break;
        //                        case "C":
        //                            lEntityType = CCP.Constants.entType.entTypeInd;
        //                            break;
        //                        default:
        //                            lEntityType = CCP.Constants.entType.entTypeBus;
        //                            break;
        //                    }
        //                    xmlEntity.Create(Convert.ToString(objCLT.EntityID), lEntityType, "", CCP.Constants.entTaxIdType.entTaxIDNA, "", "", "", "", "");

        //                }
        //                xmlEntity.addIDNumber(Convert.ToString(objCLT.CLTSEQNUM), objParams.Parameter(Globals.conClientID));
        //            }
        //        }
        //        else
        //        {
        //            foreach (PMSPCT30ClientMasterFile objCLMT in Globals.collClaimantUpdates)
        //            {
        //                if (xmlEntity.getEntitybyID(Convert.ToString(objCLMT.EntityID)) == null)
        //                {
        //                    switch (objCLMT.PMSPCT30ClientType)
        //                    {
        //                        case "I":
        //                            lEntityType = Constants.entType.entTypeInd;
        //                            break;
        //                        case "B":
        //                        case "G":
        //                        case "C":
        //                            lEntityType = Constants.entType.entTypeBus;
        //                            break;
        //                        default:
        //                            lEntityType = Constants.entType.entTypeInd;
        //                            break;
        //                    }
        //                    xmlEntity.Create(Convert.ToString(objCLMT.EntityID), lEntityType, "", Constants.entTaxIdType.entTaxIDNA, "", "", "", "", "");
        //                }
        //                xmlEntity.addIDNumber(Convert.ToString(objCLMT.PMSPCT30Client), objParams.Parameter(Globals.conClientID));
        //            }
        //        }
        //        sFileName = "";
        //        sFileName = objParams.Parameter(Globals.conSWEntityUpdateFile);
        //        sFileName = sFileName.Replace(".xml", "");
        //        sFileName = sFileName + dtmCurrDate + dtmCurrTime;
        //        sFileName = sFileName.Replace(" ", "");
        //        sFileName = sFileName.Replace("/", "");
        //        sFileName = sFileName.Replace(":", "");
        //        sFileName = sFileName + ".xml";
        //        Globals.XMLObj().XMLSaveDocument(null, objFileFunc.JoinFileName(objParams.OutPath, sFileName), CCP.Constants.cxmlFileOptions.cxmlFOOverwrite);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        Globals.dbg.PopProc();
        //    }


        //}

        //public void subSendWPADiary(long ErrNum, string ErrDesc)
        //{
        //    string sUserToNotify = null;
        //    string[] aUser = new string[11];
        //    object oShell = null;
        //    string sParms = null;
        //    try
        //    {
        //        Globals.dbg.PushProc("subSendWPADiary", ErrDesc, "", "", "");
        //        if (!objParams.IsTrue(Globals.conWPASendToSW))
        //        {
        //            return;
        //        }

        //        if (!string.IsNullOrEmpty(objParams.Database))
        //        {
        //            sParms = "/D=" + objParams.Database;
        //        }
        //        if (!string.IsNullOrEmpty(objParams.UserID) & !string.IsNullOrEmpty(objParams.Password))
        //        {
        //            sParms = sParms + " /U=" + objParams.UserID + " /P=" + objParams.Password;
        //        }
        //        sParms = sParms + " /CLAIM=" + Globals.sClaimNumber;
        //        sParms = sParms + " /" + Globals.conWPAEntryNameSW + "=" + Convert.ToChar(34) + objParams.Parameter(Globals.conWPAEntryNameSW) + Convert.ToChar(34);
        //        sParms = sParms + " /" + Globals.conWPAAssigningUserSW + "=" + objParams.Parameter(Globals.conWPAAssigningUserSW);
        //        sParms = sParms + " /" + Globals.conWPAPrioritySW + "=" + objParams.Parameter(Globals.conWPAPrioritySW);
        //        if (!string.IsNullOrEmpty(objParams.Parameter(Globals.conWPARoleSW)))
        //        {
        //            sParms = sParms + " /" + Globals.conWPARoleSW + "=" + objParams.Parameter(Globals.conWPARoleSW);
        //        }
        //        if (!string.IsNullOrEmpty(objParams.Parameter(Globals.conWPAAssignedToSW)))
        //        {
        //            sParms = sParms + " /" + Globals.conWPAAssignedToSW + "=" + objParams.Parameter(Globals.conWPAAssignedToSW);
        //        }
        //        sParms = sParms + " /" + Globals.conWPAEntryNotesSW + "=" + Convert.ToChar(34) + ErrNum + " - " + ErrDesc + Convert.ToChar(34);

        //    }
        //    catch (Exception ex)
        //    {
        //        Globals.dbg.LogEntry("Call to CCPAP failed. No Diary sent to " + objParams.Parameter(Globals.conWPAAssignedToSW) + "-" + objParams.Parameter(Globals.conWPARoleSW));
        //    }
        //    finally
        //    {
        //        oShell = null;
        //        Globals.dbg.PopProc();
        //    }

        //}

    }
}
