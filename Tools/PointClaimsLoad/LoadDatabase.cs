﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCP.Common;
using RC = Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using System.IO;
using Riskmaster.Application.PolicySystemInterface;

namespace CCP.PointClaimsLoad
    {
    public class LoadDatabase
        {

        #region private variables
        private static string sYYYYMMDD;
        private static string sOverRideCYYMMDD;
        private static string sVendorActivity;
        private static float TotPayment;
        private static float CurrResv;
        private static Dictionary<string, object> dictParams = null;
        private static StringBuilder strSQL = null;
        private static int m_iClientId = 0;
        #endregion

        // rrachev JIRA RMA 7701
        #region helpers
        private static DateTime ToValidSQLDate(DateTime value)
        {
            DateTime dApplictionDefaultDateTime = Globals.GetDateTime(null);
            if (value > dApplictionDefaultDateTime)
                return value;
            else
                return dApplictionDefaultDateTime;
        }
        #endregion

        

        public enum UpdateMode : int
        {
            UPDATE_CLIENTSEQNUM = 0, // Update clientseqbum in entity table
            UPDATE_ADDRSEQNUM = 1, // Update addressseqnum in entity table
        }

        public static void ClearDictionaryObject()
        {
        if (dictParams != null)
            {
            dictParams.Clear();
            }
        else
        {
        dictParams = new Dictionary<string, object>();
            }
        if (strSQL != null)
            {
            strSQL.Clear();
            }
        else
        {
        strSQL = new StringBuilder();
            }
        }

        public static void Execute(DbWriter obWirter)
            {
            try
                {
                    //start : bsharma33 Code reverted RMA-9738
                    //string sQuery;
                    //int iVal;
                    //if (Globals.UseDBWriter.ToLower() == "false")
                    //{
                    //    sQuery = GenerateSQL(obWirter);
                    //    iVal = DbFactory.ExecuteNonQuery(Globals.dbConnection.ConnectionString, sQuery);
                    //}
                    //else
                    //{
					    LogSql(obWirter);
                        obWirter.Execute(Globals.dbTrans);
                    //}
                    //ends  : bsharma33 Code reverted RMA-9738
                }
            catch (Exception e)
                {
                Globals.objLogWriter.Write("*** Above Query Failed, node couldn't be loaded. ***"+ Environment.NewLine);
                Globals.objLogWriter.Write(Environment.NewLine + "----------------------------------------" + Environment.NewLine);
                throw e;
                }

            }

        //start : bsharma33 Code reverted RMA-9738
        //public static string GenerateSQL(DbWriter writer)
        //{
        //    string query = writer.Sql, value, temp;

        //    for (int i = 0; i < writer.Fields.Count; i++)
        //    {
        //        switch (writer.Fields[i].Value.GetType().ToString())
        //        {
        //            case "System.String": value = "'" + RC.Conversion.ConvertObjToStr(writer.Fields[i].Value).Replace("'", "''") + "'";
        //                break;
        //            case "System.DateTime":
        //                temp = RC.Conversion.ConvertObjToStr(writer.Fields[i].Value);
                        
        //                if (writer.Fields[i].ParameterName.ToLower().Contains("time")) //to handle cases where point has time type fields
        //                {
        //                    temp = Convert.ToString(string.Format("{0:hh:mm:ss}", Convert.ToDateTime(temp)));
        //                }
        //                else // to handle cases where point table has date type fileds
        //                {
        //                    //bsharma33: Start changes for RMA-9738 
        //                    //temp = Globals.objUtils.ConvertDateToPoint(string.Format("{0:yyyyMMdd}", Convert.ToDateTime(temp)));
        //                    //if (temp == "0000101")
        //                    //    temp = "0010101"; // last date issue.
        //                    temp = string.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(temp));
        //                    //bsharma33: End changes for RMA-9738 
        //                }

        //                value = "'" + temp + "'";
        //                break;
        //            default: value = RC.Conversion.ConvertObjToStr(writer.Fields[i].Value).Replace("'", "''");
        //                break;
        //        }
        //        query = query.Replace(writer.Fields[i].ParameterName, value);
        //    }

        //    Globals.objLogWriter.Write(query);
        //    Globals.objLogWriter.Write(Environment.NewLine + "----------------------------------------" + Environment.NewLine);
        //    return query;
        //}
        //ends : bsharma33 Code reverted RMA-9738
        public static void LogSql(DbWriter writer)
            {
            string query = writer.Sql, value;

            for (int i = 0; i < writer.Fields.Count; i++)
                {
                switch (writer.Fields[i].Value.GetType().ToString())
                    {
                    case "System.String": value = "'" + RC.Conversion.ConvertObjToStr(writer.Fields[i].Value) + "'";
                        break;
                    case "System.DateTime": value = "'" + RC.Conversion.ConvertObjToStr(writer.Fields[i].Value) + "'";
                        break;
                    default: value = RC.Conversion.ConvertObjToStr(writer.Fields[i].Value);
                        break;
                    }
                query = query.Replace(writer.Fields[i].ParameterName, value);
                }
            
            Globals.objLogWriter.Write(query);
            Globals.objLogWriter.Write(Environment.NewLine + "----------------------------------------" + Environment.NewLine);
            }
                
        //public static void UpdateClaimantPMSPCL30(string sClaimNumber, string sClaimStatus)
        //{
        //    string sSql = string.Empty;

        //    try
        //    {

        //        Globals.objWriter.Tables.Add("PMSPCL30");
        //        Globals.objWriter.Fields.Add("RECSTATUS", sClaimStatus);
        //        Globals.objWriter.Where.Add("CLAIM='" + sClaimNumber + "' ");
        //        Execute(Globals.objWriter);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        Globals.objWriter.Reset(true);
        //    }
        //    return;
        //}

        public static void LocateClaim()
            {
            Globals.dbg.PushProc("LocateClaim");

            DbReader dbReader = null;
            try
                {
                ClearDictionaryObject();
                strSQL.AppendFormat("SELECT * FROM PMSPCL20 WHERE CLAIM={0}", "~CLAIM~");
                dictParams.Add("CLAIM", Globals.objPCL20.PMSPCL20Claim);

                dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr,strSQL.ToString(),dictParams);
                //dbReader = ExecuteReader("SELECT * FROM PMSPCL20 WHERE CLAIM='" + Globals.objPCL20.PMSPCL20Claim + "'");
                Globals.objWriter.Tables.Add("PMSPCL20");
                if (!dbReader.Read())
                    {
                    Globals.bNewClaim = true;
                    Globals.bNewClaimant = true;
                    Globals.bNewReserve = true;
                    Globals.objWriter.Fields.Add("CLAIM", StringUtils.Left(Globals.objPCL20.PMSPCL20Claim, 12));
                    }
                else
                    {
                    Globals.bNewClaim = false;
                    Globals.dbg.LogEntry("old claim");
                    if (StringUtils.Trim(Globals.objPCL20.PMSPCL20CatCode) == "")
                        {
                        Globals.objPCL20.PMSPCL20CatCode = Convert.ToString(dbReader.GetValue("CATCODE"));
                        }
                    Globals.objWriter.Where.Add("CLAIM = '" + Globals.objPCL20.PMSPCL20Claim + "'");

                    }
                UpdateClaimRow();
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                if (dbReader != null)
                    dbReader.Dispose();
                Globals.dbg.PopProc();
                }
            }

        public static void UpdateClaimRow()
            {
            string sSql = string.Empty;
            Globals.dbg.PushProc("UpdateClaimRow");
            Globals.dbg.LogEntry("UpdateClaimRow");
            if (Globals.objPCL20.PMSPCL20RecStatus != "")
                {
                Globals.objWriter.Fields.Add("RECSTATUS", StringUtils.Left(Globals.objPCL20.PMSPCL20RecStatus + " ", 1));
                }

            if (Globals.bIsNewClaimUpload)
                {
                if (Globals.objPCL20.PMSPCL20ClmOffice != "")
                    {
                    Globals.objWriter.Fields.Add("CLMOFFICE", StringUtils.Left(Globals.objPCL20.PMSPCL20ClmOffice + " ", 3));
                    }
                }
            if (Globals.bIsNewClaimUpload)
                {
                if (Globals.objPCL20.PMSPCL20Examiner != "")
                    {
                    Globals.objWriter.Fields.Add("EXAMINER", StringUtils.Left(Globals.objPCL20.PMSPCL20Examiner + " ", 10));
                    }


                }
            if (Globals.bIsNewClaimUpload)
                {
                if (Globals.objPCL20.PMSPCL20ExaminerCd != "")
                    {
                    Globals.objWriter.Fields.Add("EXAMINERCD", StringUtils.Left(Globals.objPCL20.PMSPCL20ExaminerCd + " ", 2));
                    }

                }
            if (Globals.bIsNewClaimUpload)
                {
                Globals.objWriter.Fields.Add("LOSSDTE", Globals.objPCL20.PMSPCL20LossDte);
                }
            if (Globals.objPCL20.PMSPCL20LossTime != "")
                {
                Globals.objWriter.Fields.Add("LOSSTIME", StringUtils.Left(Globals.objPCL20.PMSPCL20LossTime + " ", 8));
                }
            Globals.objWriter.Fields.Add("RPTDTE", Globals.objPCL20.PMSPCL20RptDte);
            if (Globals.objPCL20.PMSPCL20RptTime != "")
                {
                Globals.objWriter.Fields.Add("RPTTIME", StringUtils.Left(Globals.objPCL20.PMSPCL20RptTime + " ", 8));
                }
            Globals.objWriter.Fields.Add("CLMMADEDTE", Globals.objPCL20.PMSPCL20ClmMadeDte);
            if (Globals.bIsNewClaimUpload)
                {
                if (Globals.objPCL20.PMSPCL20RptByNme != "")
                    {
                    Globals.objWriter.Fields.Add("RPTBYNME", StringUtils.Left(Globals.objPCL20.PMSPCL20RptByNme + " ", 30));
                    }

                }
            if (Globals.bIsNewClaimUpload)
                {
                if (Globals.objPCL20.PMSPCL20ClmDesc != "")
                    {
                    Globals.objWriter.Fields.Add("CLMDESC", StringUtils.Left(Globals.objPCL20.PMSPCL20ClmDesc + " ", 30));
                    }
                }

            if (Globals.objPCL20.PMSPCL20ClaimType != "")
                {
                    {
                    Globals.objWriter.Fields.Add("CLAIMTYPE", StringUtils.Left(Globals.objPCL20.PMSPCL20ClaimType + " ", 2));
                    }
                }
            if (Globals.objPCL20.PMSPCL20ClaimState != "")
                {
                Globals.objWriter.Fields.Add("CLAIMSTATE", StringUtils.Left(Globals.objPCL20.PMSPCL20ClaimState + " ", 2));
                }
            if (Globals.objPCL20.PMSPCL20CatCode != "")
                {
                Globals.objWriter.Fields.Add("CATCODE", StringUtils.Left(Globals.objPCL20.PMSPCL20CatCode + " ", 8));
                }

            Globals.objWriter.Fields.Add("NXTRVWDTE", Globals.objPCL20.PMSPCL20NxtRvwDte);

            if (Globals.objPCL20.PMSPCL20DescCode != "")
                {
                Globals.objWriter.Fields.Add("DESCODE", StringUtils.Left(Globals.objPCL20.PMSPCL20DescCode + " ", 4));
                }
            else
                {
                Globals.objWriter.Fields.Add("DESCODE", "    ");
                }
            try
                {
                Execute(Globals.objWriter);
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.objWriter.Reset(true);
                }

            }

        public static string PointClaimDEUnitStat(string sClaimNumber, string sCLMTSEQ)
            {

            string sResult = string.Empty;

            Globals.sHistSeq = "0";
            ClearDictionaryObject();
            strSQL.AppendFormat("SELECT HISTSEQ FROM PMSPCL14 WHERE CLAIM={0} AND CLMTSEQ= {1} ORDER BY HISTSEQ DESC", "~CLAIM~", "~CLMTSEQ~");
            dictParams.Add("CLAIM", sClaimNumber);
            dictParams.Add("CLMTSEQ", sCLMTSEQ);
            string sHistSeq = Convert.ToString(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams));
            if (!string.IsNullOrEmpty(sHistSeq))
                {
                Globals.sHistSeq = sHistSeq;
                }

            ClearDictionaryObject();
            strSQL.AppendFormat("SELECT HISTSEQ FROM PMSPCL14 WHERE CLAIM={0} AND CLMTSEQ= {1} AND HISTSEQ = {2}", "~CLAIM~", "~CLMTSEQ~","~HISTSEQ~");
            dictParams.Add("CLAIM", sClaimNumber);
            dictParams.Add("CLMTSEQ", sCLMTSEQ);
            dictParams.Add("HISTSEQ", Convert.ToInt32(Globals.sHistSeq) + 1);

            sResult = Convert.ToString(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams));
            //sResult = Convert.ToString(ExecuteScalar("SELECT HISTSEQ FROM PMSPCL14 WHERE CLAIM='" + sClaimNumber + "' AND CLMTSEQ= " + sCLMTSEQ + " AND HISTSEQ = " + Convert.ToInt32(Globals.sHistSeq) + 1));
            
            return sResult;
           
            }

        public static void LocateUnitStat()
            {
            string sSql = string.Empty;
            Globals.dbg.PushProc("LocateUnitStat");
            DataSet dbDataSet = null;
            DbWriter obWriter = null;

            string sResult = PointClaimDEUnitStat(Globals.sClaimNumber, Convert.ToString(Globals.sCLMTSEQ));
            //string sResult = Convert.ToString(ExecuteScalar("SELECT CLAIM FROM PMSPCL14 WHERE CLAIM='" + Globals.sClaimNumber + "' AND CLMTSEQ= " + Convert.ToString(Globals.sCLMTSEQ)));

            if (string.IsNullOrEmpty(sResult))
                {
                Globals.dbg.LogEntry("NEW RECORD");
                Globals.objPCL14.PMSPCL14HistSeq = Convert.ToInt32(Globals.sHistSeq) + 1;
                if (Globals.objPCL14.PMSPCL14HistSeq <= 99)
                    {
                    Globals.objWriter.Tables.Add("PMSPCL14");
                    Globals.objWriter.Fields.Add("CLAIM", StringUtils.Left(Globals.objPCL14.PMSPCL14Claim + " ", 12));
                    Globals.objWriter.Fields.Add("CLMTSEQ", Globals.objPCL14.PMSPCL14ClmtSeq);
                    Globals.objWriter.Fields.Add("CLASSNUM", StringUtils.Left(Globals.objPCL14.PMSPCL14ClassNum + " ", 6));
                    UpdateUnitStatRow();
                    }
                }
            //else
            //{
            //    if (Globals.objPCL14.PMSPCL14HistSeq <= 99)
            //        {
            //    UpdateUnitStatRowQuery(Globals.sClaimNumber, Convert.ToString(Globals.sCLMTSEQ)); // inline query
            //}
            //}
            if (Globals.objPCL14.PMSPCL14LsCurrAww != 0)
                {
                obWriter = DbFactory.GetDbWriter(Globals.dbConnection);
                obWriter.Tables.Add("PMSPCL92");
                obWriter.Fields.Add("WEEKLYWAGE", Convert.ToString(Globals.objPCL14.PMSPCL14LsCurrAww));
                obWriter.Where.Add("CLAIM ='" + Globals.objPCL14.PMSPCL14Claim + "'");
                try
                    {
                    Execute(obWriter);
                    }
                catch (Exception ex)
                    {
                    throw ex;

                    }
                finally
                    {
                    if (obWriter != null)
                        {
                            obWriter = null;
                        }
                    Globals.dbg.PopProc();
                    }

                }
            }

        private static void UpdateUnitStatRow()
            {
            Globals.dbg.PushProc("UpdateUnitStatRow");
            //ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlWC, ScriptingFunctions.conScpEvtBSave, Globals.PointClaimDErsUnitStat);

            Globals.objWriter.Fields.Add("LSMARSTAT", StringUtils.Left(Globals.objPCL14.PMSPCL14LsMarStat + " ", 1));
            Globals.objWriter.Fields.Add("LSNBRDEPS", Globals.objPCL14.PMSPCL14LsNbrDeps);
            Globals.objWriter.Fields.Add("LSDOB", StringUtils.Left(Globals.objPCL14.PMSPCL14LsDob + " ", 8));
            Globals.objWriter.Fields.Add("LSDOD", StringUtils.Left(Globals.objPCL14.PMSPCL14LsDod + " ", 8));
            Globals.objWriter.Fields.Add("LSOCCCODE", StringUtils.Left(Globals.objPCL14.PMSPCL14LsOccCode + " ", 2));
            //if (Globals.objPCL14.PMSPCL14LsLstWrkDa != string.Empty)
            Globals.objWriter.Fields.Add("LSLSTWRKDA", StringUtils.Left(Globals.objPCL14.PMSPCL14LsLstWrkDa + " ", 7));
            //if (Globals.objPCL14.PMSPCL14LsDteRetd != string.Empty)
            Globals.objWriter.Fields.Add("LSDTERETD", StringUtils.Left(Globals.objPCL14.PMSPCL14LsDteRetd + " ", 7));
            //if (Globals.objPCL14.PMSPCL14LsRepIndr != string.Empty)
            Globals.objWriter.Fields.Add("LSREPINDR", StringUtils.Left(Globals.objPCL14.PMSPCL14LsRepIndr + " ", 1));
            //if (Globals.objPCL14.PMSPCL14LsLegRep != string.Empty)
            Globals.objWriter.Fields.Add("LSLEGREP", StringUtils.Left(Globals.objPCL14.PMSPCL14LsLegRep + " ", 30));
            Globals.objWriter.Fields.Add("LSCURRAWW", Globals.objPCL14.PMSPCL14LsCurrAww);
            Globals.objWriter.Fields.Add("CURRCOMPRT", Globals.objPCL14.PMSPCL14CurrCompRt);
            //if (Globals.objPCL14.PMSPCL14LsNote1 != string.Empty)
            Globals.objWriter.Fields.Add("LSNOTE1", StringUtils.Left(Globals.objPCL14.PMSPCL14LsNote1 + " ", 64));
            //if (Globals.objPCL14.PMSPCL14LsNote2 != string.Empty)
            Globals.objWriter.Fields.Add("LSNOTE2", StringUtils.Left(Globals.objPCL14.PMSPCL14LsNote2 + " ", 78));
            //if (Globals.objPCL14.PMSPCL14LsNote3 != string.Empty)
            Globals.objWriter.Fields.Add("LSNOTE3", StringUtils.Left(Globals.objPCL14.PMSPCL14LsNote3 + " ", 78));
            //if (Globals.objPCL14.PMSPCL14LossEntDteYr != string.Empty)
            Globals.objWriter.Fields.Add("LOSSENTYR", StringUtils.Left(Globals.objPCL14.PMSPCL14LossEntDteYr + " ", 3));
            //if (Globals.objPCL14.PMSPCL14LossEntDteMo != string.Empty)
            Globals.objWriter.Fields.Add("LOSSENTMO", StringUtils.Left(Globals.objPCL14.PMSPCL14LossEntDteMo + " ", 2));
            //if (Globals.objPCL14.PMSPCL14LossEntDteDy != string.Empty)
            Globals.objWriter.Fields.Add("LOSSENTDY", StringUtils.Left(Globals.objPCL14.PMSPCL14LossEntDteDy + " ", 2));
            //if (Globals.objPCL14.PMSPCL14HistDate != string.Empty)
            Globals.objWriter.Fields.Add("HISTDATE", StringUtils.Left(Globals.objPCL14.PMSPCL14HistDate + " ", 7));
            Globals.objWriter.Fields.Add("HISTSEQ", Globals.objPCL14.PMSPCL14HistSeq);
            //if (Globals.objPCL14.PMSPCL14Act != string.Empty)
            Globals.objWriter.Fields.Add("ACT", StringUtils.Left(Globals.objPCL14.PMSPCL14Act + " ", 2));
            //if (Globals.objPCL14.PMSPCL14TypeLoss != string.Empty)
            Globals.objWriter.Fields.Add("TYPELOSS", StringUtils.Left(Globals.objPCL14.PMSPCL14TypeLoss + " ", 2));
            //if (Globals.objPCL14.PMSPCL14TypeRecv != string.Empty)
            Globals.objWriter.Fields.Add("TYPERECV", StringUtils.Left(Globals.objPCL14.PMSPCL14TypeRecv + " ", 2));
            //if (Globals.objPCL14.PMSPCL14TypeCovg != string.Empty)
            Globals.objWriter.Fields.Add("TYPECOVG", StringUtils.Left(Globals.objPCL14.PMSPCL14TypeCovg + " ", 2));
            //if (Globals.objPCL14.PMSPCL14TypeSetmt != string.Empty)
            Globals.objWriter.Fields.Add("TYPESETMT", StringUtils.Left(Globals.objPCL14.PMSPCL14TypeSetmt + " ", 2));
            //if (Globals.objPCL14.PMSPCL14InjCode != string.Empty)
            Globals.objWriter.Fields.Add("INJCODE", StringUtils.Left(Globals.objPCL14.PMSPCL14InjCode + " ", 2));
            Globals.objWriter.Fields.Add("CLIENT", Globals.objPCL14.PMSPCL14Client);
            //if (Globals.objPCL14.PMSPCL14LsStJuris != string.Empty)
            Globals.objWriter.Fields.Add("LSSTJURIS", StringUtils.Left(Globals.objPCL14.PMSPCL14LsStJuris + " ", 2));
            //if (Globals.objPCL14.PMSPCL14InjuryZip != string.Empty)
            Globals.objWriter.Fields.Add("INJURYZIP", StringUtils.Left(Globals.objPCL14.PMSPCL14InjuryZip + " ", 10));
            //if (Globals.objPCL14.PMSPCL14HireDteYr != string.Empty)
            Globals.objWriter.Fields.Add("HIREDTEYR", StringUtils.Left(Globals.objPCL14.PMSPCL14HireDteYr + " ", 4));
            //if (Globals.objPCL14.PMSPCL14HireDteMo != string.Empty)
            Globals.objWriter.Fields.Add("HIREDTEMO", StringUtils.Left(Globals.objPCL14.PMSPCL14HireDteMo + " ", 2));
            //if (Globals.objPCL14.PMSPCL14HireDteDy != string.Empty)
            Globals.objWriter.Fields.Add("HIREDTEDY", StringUtils.Left(Globals.objPCL14.PMSPCL14HireDteDy + " ", 2));
            //if (Globals.objPCL14.PMSPCL14EmplyStat != string.Empty)
            Globals.objWriter.Fields.Add("EMPLYSTAT", StringUtils.Left(Globals.objPCL14.PMSPCL14EmplyStat + " ", 1));
            //if (Globals.objPCL14.PMSPCL14Fein != string.Empty)
            Globals.objWriter.Fields.Add("FEIN", StringUtils.Left(Globals.objPCL14.PMSPCL14Fein + " ", 9));
            //if (Globals.objPCL14.PMSPCL14EmplrSic != string.Empty)
            Globals.objWriter.Fields.Add("EMPLYRSIC", StringUtils.Left(Globals.objPCL14.PMSPCL14EmplrSic + " ", Globals.objPCL14.PMSPCL14EmplrSicFieldLength));//    'SI06135
            //'SI06135Globals.objWriter.Fields.Add("EMPLYRSIC", StringUtils.Left(Globals.objPCL14.PMSPCL14EmplrSic + " ", 4)
            //if (Globals.objPCL14.PMSPCL14EmpPayInd != string.Empty)
            Globals.objWriter.Fields.Add("EMPPAYIND", StringUtils.Left(Globals.objPCL14.PMSPCL14EmpPayInd + " ", 1));
            //if (Globals.objPCL14.PMSPCL14WageMeth != string.Empty)
            Globals.objWriter.Fields.Add("WAGEMETH", StringUtils.Left(Globals.objPCL14.PMSPCL14WageMeth + " ", 1));
            Globals.objWriter.Fields.Add("OTHRWKPYMT", Globals.objPCL14.PMSPCL14OthrWkPymt);
            //if (Globals.objPCL14.PMSPCL14DateRepEmp != string.Empty)
            Globals.objWriter.Fields.Add("DATEREPEMP", StringUtils.Left(Globals.objPCL14.PMSPCL14DateRepEmp + " ", 7));
            //if (Globals.objPCL14.PMSPCL14SurgInd != string.Empty)
            Globals.objWriter.Fields.Add("SURGIND", StringUtils.Left(Globals.objPCL14.PMSPCL14SurgInd + " ", 1));
            Globals.objWriter.Fields.Add("POSTINJWGE", Globals.objPCL14.PMSPCL14PostInjWge);
            Globals.objWriter.Fields.Add("PCTIMPAIR", Globals.objPCL14.PMSPCL14PctImpair);
            //if (Globals.objPCL14.PMSPCL14DteOfDisab != string.Empty)
            Globals.objWriter.Fields.Add("DTEOFDISAB", StringUtils.Left(Globals.objPCL14.PMSPCL14DteOfDisab + " ", 7));
            //if (Globals.objPCL14.PMSPCL14DteMaxMed != string.Empty)
            Globals.objWriter.Fields.Add("DTEMAXMED", StringUtils.Left(Globals.objPCL14.PMSPCL14DteMaxMed + " ", 7));
            //if (Globals.objPCL14.PMSPCL14Deductible != string.Empty)
            Globals.objWriter.Fields.Add("DEDUCTIBLE", StringUtils.Left(Globals.objPCL14.PMSPCL14Deductible + " ", 1));
            //if (Globals.objPCL14.PMSPCL14Controvert != string.Empty)
            Globals.objWriter.Fields.Add("CONTROVERT", StringUtils.Left(Globals.objPCL14.PMSPCL14Controvert + " ", 1));
            //if (Globals.objPCL14.PMSPCL14BoSocSec != string.Empty)
            Globals.objWriter.Fields.Add("BOSOCSEC", StringUtils.Left(Globals.objPCL14.PMSPCL14BoSocSec + " ", 1));
            //if (Globals.objPCL14.PMSPCL14BoUnempl != string.Empty)
            Globals.objWriter.Fields.Add("BOUNEMPL", StringUtils.Left(Globals.objPCL14.PMSPCL14BoUnempl + " ", 1));
            //if (Globals.objPCL14.PMSPCL14BoPension != string.Empty)
            Globals.objWriter.Fields.Add("BOPENSION", StringUtils.Left(Globals.objPCL14.PMSPCL14BoPension + " ", 1));
            //if (Globals.objPCL14.PMSPCL14BoSpecial != string.Empty)
            Globals.objWriter.Fields.Add("BOSPECIAL", StringUtils.Left(Globals.objPCL14.PMSPCL14BoSpecial + " ", 1));
            //if (Globals.objPCL14.PMSPCL14BoOther != string.Empty)
            Globals.objWriter.Fields.Add("BOOTHER", StringUtils.Left(Globals.objPCL14.PMSPCL14BoOther + " ", 1));
            //if (Globals.objPCL14.PMSPCL14LsOccuptn != string.Empty)
            Globals.objWriter.Fields.Add("LSOCCUPTN", StringUtils.Left(Globals.objPCL14.PMSPCL14LsOccuptn + " ", 30));
            //if (Globals.objPCL14.PMSPCL14DeductCode != string.Empty)
            Globals.objWriter.Fields.Add("DEDUCTCODE", StringUtils.Left(Globals.objPCL14.PMSPCL14DeductCode + " ", 2));
            //if (Globals.objPCL14.PMSPCL14McoInd != string.Empty)
            Globals.objWriter.Fields.Add("MCOIND", StringUtils.Left(Globals.objPCL14.PMSPCL14McoInd + " ", 2));
            //if (Globals.objPCL14.PMSPCL14FraudClaim != string.Empty)
            Globals.objWriter.Fields.Add("FRAUDCLAIM", StringUtils.Left(Globals.objPCL14.PMSPCL14FraudClaim + " ", 1));
            try
                {
                Execute(Globals.objWriter);
                }
            catch (Exception ex)
                {
                throw ex;
                }

            finally
                {
                Globals.objWriter.Reset(true);
                Globals.dbg.PopProc();
                }



            }

        public static void UpdateUnitStatRowQuery(string sClaimNumber, string sCLMTSEQ)
            {

            Globals.objWriter.Tables.Add("PMSPCL14");
            Globals.objWriter.Fields.Add("LSMARSTAT", StringUtils.Left(Globals.objPCL14.PMSPCL14LsMarStat + " ", 1));
            Globals.objWriter.Fields.Add("LSNBRDEPS", Globals.objPCL14.PMSPCL14LsNbrDeps);
            Globals.objWriter.Fields.Add("LSDOB", StringUtils.Left(Globals.objPCL14.PMSPCL14LsDob + " ", 8));
            Globals.objWriter.Fields.Add("LSDOD", StringUtils.Left(Globals.objPCL14.PMSPCL14LsDod + " ", 8));
            Globals.objWriter.Fields.Add("LSOCCCODE", StringUtils.Left(Globals.objPCL14.PMSPCL14LsOccCode + " ", 2));
            Globals.objWriter.Fields.Add("LSLSTWRKDA", StringUtils.Left(Globals.objPCL14.PMSPCL14LsLstWrkDa + " ", 7));
            Globals.objWriter.Fields.Add("LSDTERETD", StringUtils.Left(Globals.objPCL14.PMSPCL14LsDteRetd + " ", 7));
            Globals.objWriter.Fields.Add("LSREPINDR", StringUtils.Left(Globals.objPCL14.PMSPCL14LsRepIndr + " ", 1));
            Globals.objWriter.Fields.Add("LSLEGREP", StringUtils.Left(Globals.objPCL14.PMSPCL14LsLegRep + " ", 30));
            Globals.objWriter.Fields.Add("LSCURRAWW", Globals.objPCL14.PMSPCL14LsCurrAww);
            Globals.objWriter.Fields.Add("CURRCOMPRT", Globals.objPCL14.PMSPCL14CurrCompRt);
            Globals.objWriter.Fields.Add("LSNOTE1", StringUtils.Left(Globals.objPCL14.PMSPCL14LsNote1 + " ", 64));
            Globals.objWriter.Fields.Add("LSNOTE2", StringUtils.Left(Globals.objPCL14.PMSPCL14LsNote2 + " ", 78));
            Globals.objWriter.Fields.Add("LSNOTE3", StringUtils.Left(Globals.objPCL14.PMSPCL14LsNote3 + " ", 78));
            Globals.objWriter.Fields.Add("LOSSENTYR", StringUtils.Left(Globals.objPCL14.PMSPCL14LossEntDteYr + " ", 3));
            Globals.objWriter.Fields.Add("LOSSENTMO", StringUtils.Left(Globals.objPCL14.PMSPCL14LossEntDteMo + " ", 2));
            Globals.objWriter.Fields.Add("LOSSENTDY", StringUtils.Left(Globals.objPCL14.PMSPCL14LossEntDteDy + " ", 2));
            Globals.objWriter.Fields.Add("HISTDATE", StringUtils.Left(Globals.objPCL14.PMSPCL14HistDate + " ", 7));
            Globals.objWriter.Fields.Add("HISTSEQ", Globals.objPCL14.PMSPCL14HistSeq);
            Globals.objWriter.Fields.Add("ACT", StringUtils.Left(Globals.objPCL14.PMSPCL14Act + " ", 2));
            Globals.objWriter.Fields.Add("TYPELOSS", StringUtils.Left(Globals.objPCL14.PMSPCL14TypeLoss + " ", 2));
            Globals.objWriter.Fields.Add("TYPERECV", StringUtils.Left(Globals.objPCL14.PMSPCL14TypeRecv + " ", 2));
            Globals.objWriter.Fields.Add("TYPECOVG", StringUtils.Left(Globals.objPCL14.PMSPCL14TypeCovg + " ", 2));
            Globals.objWriter.Fields.Add("TYPESETMT", StringUtils.Left(Globals.objPCL14.PMSPCL14TypeSetmt + " ", 2));
            Globals.objWriter.Fields.Add("INJCODE", StringUtils.Left(Globals.objPCL14.PMSPCL14InjCode + " ", 2));
            Globals.objWriter.Fields.Add("CLIENT", Globals.objPCL14.PMSPCL14Client);
            Globals.objWriter.Fields.Add("LSSTJURIS", StringUtils.Left(Globals.objPCL14.PMSPCL14LsStJuris + " ", 2));
            Globals.objWriter.Fields.Add("INJURYZIP", StringUtils.Left(Globals.objPCL14.PMSPCL14InjuryZip + " ", 10));
            Globals.objWriter.Fields.Add("HIREDTEYR", StringUtils.Left(Globals.objPCL14.PMSPCL14HireDteYr + " ", 4));
            Globals.objWriter.Fields.Add("HIREDTEMO", StringUtils.Left(Globals.objPCL14.PMSPCL14HireDteMo + " ", 2));
            Globals.objWriter.Fields.Add("HIREDTEDY", StringUtils.Left(Globals.objPCL14.PMSPCL14HireDteDy + " ", 2));
            Globals.objWriter.Fields.Add("EMPLYSTAT", StringUtils.Left(Globals.objPCL14.PMSPCL14EmplyStat + " ", 1));
            Globals.objWriter.Fields.Add("FEIN", StringUtils.Left(Globals.objPCL14.PMSPCL14Fein + " ", 9));
            Globals.objWriter.Fields.Add("EMPLYRSIC", StringUtils.Left(Globals.objPCL14.PMSPCL14EmplrSic + " ", Globals.objPCL14.PMSPCL14EmplrSicFieldLength));
            Globals.objWriter.Fields.Add("EMPPAYIND",StringUtils.Left(Globals.objPCL14.PMSPCL14EmpPayInd + " ", 1));
            Globals.objWriter.Fields.Add("WAGEMETH",StringUtils.Left(Globals.objPCL14.PMSPCL14WageMeth + " ", 1));
            Globals.objWriter.Fields.Add("OTHRWKPYMT",Globals.objPCL14.PMSPCL14OthrWkPymt);
            Globals.objWriter.Fields.Add("DATEREPEMP", StringUtils.Left(Globals.objPCL14.PMSPCL14DateRepEmp + " ", 7));
            Globals.objWriter.Fields.Add("SURGIND", StringUtils.Left(Globals.objPCL14.PMSPCL14SurgInd + " ", 1));
            Globals.objWriter.Fields.Add("POSTINJWGE",Globals.objPCL14.PMSPCL14PostInjWge);
            Globals.objWriter.Fields.Add("PCTIMPAIR",Globals.objPCL14.PMSPCL14PctImpair);
            Globals.objWriter.Fields.Add("DTEOFDISAB",StringUtils.Left(Globals.objPCL14.PMSPCL14DteOfDisab + " ", 7));
            Globals.objWriter.Fields.Add("DTEMAXMED",StringUtils.Left(Globals.objPCL14.PMSPCL14DteMaxMed + " ", 7));
            Globals.objWriter.Fields.Add("DEDUCTIBLE",StringUtils.Left(Globals.objPCL14.PMSPCL14Deductible + " ", 1));
            Globals.objWriter.Fields.Add("CONTROVERT",StringUtils.Left(Globals.objPCL14.PMSPCL14Controvert + " ", 1));
            Globals.objWriter.Fields.Add("BOSOCSEC",StringUtils.Left(Globals.objPCL14.PMSPCL14BoSocSec + " ", 1));
            Globals.objWriter.Fields.Add("BOUNEMP",StringUtils.Left(Globals.objPCL14.PMSPCL14BoUnempl + " ", 1));
            Globals.objWriter.Fields.Add("BOPENSION",StringUtils.Left(Globals.objPCL14.PMSPCL14BoPension + " ", 1));
            Globals.objWriter.Fields.Add("BOSPECIAL",StringUtils.Left(Globals.objPCL14.PMSPCL14BoSpecial + " ", 1));
            Globals.objWriter.Fields.Add("BOOTHER",StringUtils.Left(Globals.objPCL14.PMSPCL14BoOther + " ", 1));
            Globals.objWriter.Fields.Add("LSOCCUPTN",StringUtils.Left(Globals.objPCL14.PMSPCL14LsOccuptn + " ", 30));
            Globals.objWriter.Fields.Add("DEDUCTCODE",StringUtils.Left(Globals.objPCL14.PMSPCL14DeductCode + " ", 2));
            Globals.objWriter.Fields.Add("MCOIND",StringUtils.Left(Globals.objPCL14.PMSPCL14McoInd + " ", 2));
            Globals.objWriter.Fields.Add("FRAUDCLAIM",StringUtils.Left(Globals.objPCL14.PMSPCL14FraudClaim + " ", 1));
            Globals.objWriter.Where.Add(" WHERE CLAIM='" + sClaimNumber + "' AND CLMTSEQ= " + sCLMTSEQ);


            try
                {

                  Execute(Globals.objWriter);
                
                }


            catch (Exception ex)
                {
                  throw ex;
                }
            finally
                {
                Globals.objWriter.Reset(true);
                }


            }

        public static void LocateClaimant()
            {
            Globals.dbg.PushProc("LocateClaimant");

            //if (Globals.mParams().IsTrue("POINTCLIENT"))
            if (RC.Conversion.ConvertObjToBool(Globals.objConfig.ConfigElement.PointClient, m_iClientId)) // CLIENTFILE_FLAG in database
                {
                  LocateBASCLT1700();
                }

            LocatePMSPCT30();
            LocatePMSPCL32();
            Globals.dbg.PopProc();
            }
        
        public static void LocateBASCLT1700()
            {
            DbReader dbReader = null;
            Globals.dbg.PushProc("LocateBASCLT1700");
            try
                {
                ClearDictionaryObject();
                strSQL.AppendFormat("SELECT * FROM BASCLT1700  WHERE CLAIM = {0} AND CLMTSEQ ={1}", "~CLAIM~", "~CLMTSEQ~");
                dictParams.Add("CLAIM", Globals.objBASCLT1700.CLAIM);
                dictParams.Add("CLMTSEQ", Globals.objBASCLT1700.CLMTSEQ);
                dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr,strSQL.ToString(),dictParams);
                
                //dbReader = ExecuteReader("SELECT * FROM BASCLT1700  WHERE CLAIM = '" + Globals.objBASCLT1700.CLAIM + "' AND CLMTSEQ ='" + Globals.objBASCLT1700.CLMTSEQ + "'");
                Globals.dbg.LogEntry("BASCLT1700");
                Globals.dbg.LogEntry("objBASCLT1700.CLMTSEQ");
                Globals.dbg.LogEntry(Convert.ToString(Globals.objBASCLT1700.CLMTSEQ));
                if (!dbReader.Read())
                    {
                    Globals.bNewClaimant = true;
                    Globals.dbg.LogEntry("NEW");
                    Globals.bNewReserve = true;
                    Globals.objBASCLT1700.RECDATE = System.DateTime.Now.Date;
                    Globals.objBASCLT1700.RECTIME = System.DateTime.Now;

                    }
                else
                    {
                    Globals.bNewClaimant = false;
                    Globals.dbg.LogEntry("UPDATE");
                    if (Globals.objBASCLT1700.CLTSEQNUM != Globals.objBASCLT0100.CLTSEQNUM && Globals.objBASCLT0100.CLTSEQNUM != 0)
                        {
                        Globals.dbg.LogEntry("Mismatch in Client Sequence Numbers: AdvClaims=" + Globals.objBASCLT0100.CLTSEQNUM + "  BASCLT1700=" + Globals.objBASCLT1700.CLTSEQNUM);
                        Globals.objBASCLT1700.CLTSEQNUM = Globals.objBASCLT0100.CLTSEQNUM;
                        Globals.objBASCLT0300.CLTSEQNUM = Globals.objBASCLT0100.CLTSEQNUM;
                        Globals.objBASCLT1700.ADDRSEQNUM = 0;
                        Globals.objBASCLT1700.RECDATE = System.DateTime.Now.Date;
                        Globals.objBASCLT1700.RECTIME = System.DateTime.Now;

                        }
                    else
                        {
                        Globals.objBASCLT1700.CLTSEQNUM = Convert.ToInt64(dbReader.GetValue("CLTSEQNUM"));
                        Globals.objBASCLT1700.ADDRSEQNUM = Convert.ToInt64(dbReader.GetValue("ADDRSEQNUM"));
                        }

                    }

                LocateBASCLT0100();
                LocateBASCLT0300();
                UpdateBASCLT1700();
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                if (dbReader != null)
                    dbReader.Dispose();
                }
            Globals.dbg.PopProc();

            }
        
        public static void LocatePMSPCT30()
            {
            Globals.dbg.PushProc("LocatePMSPCT30");
            Parameters objParams = (Parameters)Globals.GetParams();
            DbReader dbReader = null;
            try
                {
                ClearDictionaryObject();
                strSQL.AppendFormat("SELECT * FROM PMSPCL30 WHERE CLAIM= {0} AND CLMTSEQ={1}", "~CLAIM~", "~CLMTSEQ~");
                dictParams.Add("CLAIM", Globals.sClaimNumber);
                dictParams.Add("CLMTSEQ", Globals.sCLMTSEQ);

                dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams);
                //dbReader = ExecuteReader("SELECT * FROM PMSPCL30 WHERE CLAIM= '" + Globals.sClaimNumber + "' AND CLMTSEQ=" + Globals.sCLMTSEQ);
                if (!dbReader.Read())
                    {
                    Globals.bNewClaimant = true;
                    Globals.bNewReserve = true;
                    Globals.objPCL30.PMSPCL30Client = 0;
                    if (!RC.Conversion.ConvertObjToBool(Globals.objConfig.ConfigElement.PointClient, m_iClientId))
                        {
                        LocateClient();
                        }
                    UpdateClaimantRow();
                    }
                else
                    {
                    Globals.bNewClaimant = false;
                    Globals.objPCL30.PMSPCL30Client = Convert.ToInt64(dbReader.GetValue("CLIENT"));
                    Globals.objPCL30.PMSPCL30RecStatus = Convert.ToString(dbReader.GetValue("RECSTATUS"));

                    if (Globals.bUpdateAIA == false)
                        {
                        Globals.objPCL30.PMSPCL30AiaCode1 = Convert.ToString(dbReader.GetValue("AIACODE1"));
                        Globals.objPCL30.PMSPCL30AiaCode2 = Convert.ToString(dbReader.GetValue("AIACODE2"));
                        Globals.objPCL30.PMSPCL30AiaCode3 = Convert.ToString(dbReader.GetValue("AIACODE3"));
                        }
                    UpdateClaimantRow();

                    if (!RC.Conversion.ConvertObjToBool(Globals.objConfig.ConfigElement.PointClient, m_iClientId))
                        {
                        LocateClient();
                        }
                    }
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                if (dbReader != null)
                    dbReader.Dispose();
                Globals.dbg.PopProc();
                }
            }

        public static void LocatePMSPCL32()
        {
            Globals.dbg.PushProc("LocatePMSPCL32");
            string sSql = string.Empty;
            string sResult = string.Empty;

            try
            {
                ClearDictionaryObject();
                strSQL.AppendFormat("SELECT CLAIM FROM PMSPCL32 WHERE CLAIM= {0} AND CLMTSEQ={1}", "~CLAIM~", "~CLMTSEQ~");
                dictParams.Add("CLAIM", Globals.sClaimNumber);
                dictParams.Add("CLMTSEQ", Globals.sCLMTSEQ);

                sResult = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams));
                //sResult = Convert.ToString(ExecuteScalar("SELECT CLAIM FROM PMSPCL32 WHERE CLAIM= '" + Globals.sClaimNumber + "' AND CLMTSEQ=" + Globals.sCLMTSEQ));
                if (Globals.cPointRelease >= Globals.POINT_INJ_GLBL)
                {
                    Globals.objWriter.Tables.Add("PMSPCL32");
                    if (string.IsNullOrEmpty(sResult))
                    {
                        Globals.objWriter.Fields.Add("CLAIM", Globals.sClaimNumber);
                        Globals.objWriter.Fields.Add("CLMTSEQ", Globals.sCLMTSEQ);
                        Globals.objWriter.Fields.Add("CLAIMANT_DATE_BIRTH", (string.IsNullOrEmpty(Globals.strBirthDay) ? "00001900" : string.Format("{0:MMddyyyy}", Globals.GetDateTime(Globals.strBirthDay))));
                    }
                    else
                    {
                        Globals.objWriter.Fields.Add("CLAIMANT_DATE_BIRTH", (string.IsNullOrEmpty(Globals.strBirthDay) ? "00001900" : string.Format("{0:MMddyyyy}", Globals.GetDateTime(Globals.strBirthDay))));
                        Globals.objWriter.Where.Add("CLAIM='" + Globals.sClaimNumber + "' AND CLMTSEQ ='" + Globals.sCLMTSEQ + "'");
                    }
                    Execute(Globals.objWriter);
                }
                else
                {
                    Globals.objWriter.Tables.Add("PMSPCL32");
                    if (string.IsNullOrEmpty(sResult))
                    {
                        Globals.objWriter.Fields.Add("CLAIM", Globals.sClaimNumber);
                        Globals.objWriter.Fields.Add("CLMTSEQ", Globals.sCLMTSEQ);
                        Globals.objWriter.Fields.Add("DOB", (string.IsNullOrEmpty(Globals.strBirthDay) ? "00001900" : string.Format("{0:MMddyyyy}", Globals.GetDateTime(Globals.strBirthDay))));
                    }
                    else
                    {
                        Globals.objWriter.Fields.Add("DOB", (string.IsNullOrEmpty(Globals.strBirthDay) ? "00001900" : string.Format("{0:MMddyyyy}", Globals.GetDateTime(Globals.strBirthDay))));
                        Globals.objWriter.Where.Add("CLAIM='" + Globals.sClaimNumber + "' AND CLMTSEQ ='" + Globals.sCLMTSEQ + "'");
                    }
                    Execute(Globals.objWriter);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Globals.objWriter.Reset(true);
                Globals.dbg.PopProc();
            }
        }

        private static void UpdateClaimantRow()
            {
            Globals.dbg.PushProc("UpdateClaimantRow");
            Globals.objWriter.Tables.Add("PMSPCL30");
            if (Globals.bNewClaimant)
                {

                Globals.objWriter.Fields.Add("CLAIM", StringUtils.Left(Globals.objPCL30.PMSPCL30Claim + " ", 12));
                Globals.objWriter.Fields.Add("CLMTSEQ", Globals.objPCL30.PMSPCL30ClmtSeq);
                if (!RC.Conversion.ConvertObjToBool(Globals.objConfig.ConfigElement.PointClient, m_iClientId))
                    {
                    Globals.objWriter.Fields.Add("CLIENT", Globals.objPCT30.PMSPCT30Client);
                    Globals.objPCL30.PMSPCL30Client = Globals.objPCT30.PMSPCT30Client;
                    }
                else
                    {
                    Globals.objWriter.Fields.Add("CLIENT", Globals.objBASCLT1700.CLTSEQNUM);
                    Globals.objPCL30.PMSPCL30Client = Globals.objBASCLT1700.CLTSEQNUM;
                    }
                }
            else
                {
                Globals.objWriter.Where.Add("CLAIM= '" + Globals.sClaimNumber + "' AND CLMTSEQ=" + Globals.sCLMTSEQ);
                if (!string.IsNullOrEmpty(Globals.objPCL30.PMSPCL30Claim))
                    Globals.objWriter.Fields.Add("CLIENT", Globals.objPCL30.PMSPCL30Client);
                }
            if (Globals.bUploadClaimantStatus)
            {
                Globals.objWriter.Fields.Add("RECSTATUS", StringUtils.Left(Globals.sClaimantStatus + " ", 1));
            }
            else
            {
                if (!string.IsNullOrEmpty(Globals.objPCL30.PMSPCL30RecStatus))
                    Globals.objWriter.Fields.Add("RECSTATUS", StringUtils.Left(Globals.objPCL30.PMSPCL30RecStatus + " ", 1));
            }
            Globals.objWriter.Fields.Add("SUIT", Globals.objPCL30.PMSPCL30Suit);
            if (!string.IsNullOrEmpty(Globals.objPCL30.PMSPCL30AiaCode1))
                Globals.objWriter.Fields.Add("AIACODE1", StringUtils.Left(Globals.objPCL30.PMSPCL30AiaCode1 + " ", 2));
            if (!string.IsNullOrEmpty(Globals.objPCL30.PMSPCL30AiaCode2))
                Globals.objWriter.Fields.Add("AIACODE2", StringUtils.Left(Globals.objPCL30.PMSPCL30AiaCode2 + " ", 2));
            if (!string.IsNullOrEmpty(Globals.objPCL30.PMSPCL30AiaCode3))
                Globals.objWriter.Fields.Add("AIACODE3", StringUtils.Left(Globals.objPCL30.PMSPCL30AiaCode3 + " ", 2));

            try
                {
                Execute(Globals.objWriter);
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.objWriter.Reset(true);
                Globals.dbg.PopProc();
                }

            }

        private static void AddNewClient()
            {
            Globals.dbg.PushProc("AddNewClient");

            long lNextValue = Convert.ToInt64(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr,"SELECT MAX(CLIENT)  From PMSPCT30"));

            if (lNextValue == 0)
                {
                Globals.objPCT30.PMSPCT30Client = 1;
                }
            else
                {
                Globals.objPCT30.PMSPCT30Client = lNextValue + 1;
                }
            Globals.dbg.PopProc();
            }



        private static void UpdateRMAEntity(UpdateMode enUpdateMode)
        {
            //SetUpPolicySystem objPolInt = new SetUpPolicySystem(Globals.objRMUser.LoginName, Globals.objRMUser.objRiskmasterDatabase.ConnectionString);
            PolicySystemInterface objPolSysIntrfc = new PolicySystemInterface(Globals.objRMUser, m_iClientId);
            switch (enUpdateMode)
            {
                    
                case UpdateMode.UPDATE_CLIENTSEQNUM: 
                                                   {
                                                       objPolSysIntrfc.UpdateClientSequenceNumber(Globals.objBASCLT1700.RMAENTITYID, Globals.objBASCLT1700.CLTSEQNUM);
                                                   } 
                                                   break;

                case UpdateMode.UPDATE_ADDRSEQNUM: 
                                                   {
                                                       objPolSysIntrfc.UpdateAddressSequenceNumber(Globals.objBASCLT1700.RMAADDRESSID, Globals.objBASCLT1700.ADDRSEQNUM);
                                                   } 
                                                   break;
            }
        }
        private static void GetNextClientSequence()
            {
            DbReader dbReader = null;
            Globals.dbg.PushProc("GetNextClientSequence");

            Parameters objParams = (Parameters)Globals.GetParams();
            try
                {
                dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr,"SELECT * FROM BASCLT0200");
                //NOTE:  THIS ASSUMES THAT THIS ROW IS ALWAYS PRESENT
                dbReader.Read();
                Globals.objWriter.Tables.Add("BASCLT0200");
                Globals.objWriter.Where.Add("BASSTATIC1  = " + dbReader.GetValue("BASSTATIC1")); // only one row in the table BASCLT0200
                if (Convert.ToInt32(dbReader.GetValue("CLTSEQNUM")) == 0)
                    {
                    Globals.objWriter.Fields.Add("CLTSEQNUM", 1);
                    }
                else
                    {
                    Globals.objWriter.Fields.Add("CLTSEQNUM", Convert.ToInt32(dbReader.GetValue("CLTSEQNUM")) + 1);
                    }

                Globals.objBASCLT1700.CLTSEQNUM = Convert.ToInt64(dbReader.GetValue("CLTSEQNUM")) + 1;
                Globals.objWriter.Fields.Add("RECPROG", "CCPPC");
                Globals.objWriter.Fields.Add("RECUSER", Globals.objConfig.ConfigElement.DataDSNUser);
                Globals.objWriter.Fields.Add("RECDATE", System.DateTime.Now.Date);
                Globals.objWriter.Fields.Add("RECTIME", System.DateTime.Now);

                Execute(Globals.objWriter);
                UpdateRMAEntity(UpdateMode.UPDATE_CLIENTSEQNUM);
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.objWriter.Reset(true);
                if (dbReader != null)
                    dbReader.Dispose();
                Globals.dbg.PopProc();

                }


            }

        private static void GetNextAddressSequence()
            {
            Parameters objParams = (Parameters)Globals.GetParams();
            DbReader dbReader = null;
            long iAddressSeqNum = 0;
            try
                {
                Globals.objWriter.Tables.Add("BASCLT0400");
                ClearDictionaryObject();
                strSQL.AppendFormat("SELECT * FROM BASCLT0400  WHERE CLTSEQNUM = {0}", "~CLTSEQNUM~");
                dictParams.Add("CLTSEQNUM", Globals.objBASCLT1700.CLTSEQNUM);

                dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams);
                //dbReader = ExecuteReader("SELECT * FROM BASCLT0400  WHERE CLTSEQNUM = '" + Convert.ToString(Globals.objBASCLT1700.CLTSEQNUM) + "'");
                if (!dbReader.Read())
                    {
                    //Globals.objWriter.Fields.Add("CLTSEQNUM", Globals.objBASCLT1700.CLTSEQNUM);// JIRA -7504: unit test fix, moved outside of If block
                    iAddressSeqNum = 1;
                        //Globals.objWriter.Fields.Add("RECDATE", System.DateTime.Now.Date);// JIRA -7504: unit test fix, moved outside of If block
                    }
                else
                    {
                    if (Convert.ToInt64(dbReader.GetValue("ADDRSEQNUM")) == 0)
                        {
                        iAddressSeqNum = 1;
                        }
                    else
                        {
                        iAddressSeqNum = Convert.ToInt64(dbReader.GetValue("ADDRSEQNUM")) + 1;
                        }
                    Globals.objWriter.Where.Add("CLTSEQNUM = " + Globals.objBASCLT1700.CLTSEQNUM.ToString());// JIRA-7504:unit test fix, BASCLT0400 should be updated instead of insert
                    }

                Globals.objWriter.Fields.Add("CLTSEQNUM", Globals.objBASCLT1700.CLTSEQNUM);//JIRA-7504:unit test fix, BASCLT0400 should have default values for these variables as well
                //iAddressSeqNum = 1;
                Globals.objWriter.Fields.Add("RECDATE", System.DateTime.Now.Date);//JIRA-7504: unit test fix, BASCLT0400 should have default values for these variables as well
                Globals.objWriter.Fields.Add("ADDRSEQNUM", iAddressSeqNum);
                Globals.objBASCLT1700.ADDRSEQNUM = iAddressSeqNum;

                Globals.objWriter.Fields.Add("RECPROG", "CCPPC");
                Globals.objWriter.Fields.Add("RECUSER", Globals.objConfig.ConfigElement.DataDSNUser);//objParams.Parameter(Globals.SWDataUser));
                Globals.objWriter.Fields.Add("RECTIME", System.DateTime.Now);

                Execute(Globals.objWriter);
                UpdateRMAEntity(UpdateMode.UPDATE_ADDRSEQNUM);
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.objWriter.Reset(true);
                if (dbReader != null)
                    dbReader.Dispose();
                Globals.dbg.PopProc();

                }




            }

        public static void LocateClient()
            {
            PMSPCT30ClientMasterFile objCLMT = default(PMSPCT30ClientMasterFile);
            Parameters objParams = (Parameters)Globals.GetParams();
            bool bNewClient = false;
            Globals.dbg.PushProc("LocateClient");

            ClearDictionaryObject();
            strSQL.AppendFormat("SELECT CLIENT FROM PMSPCT30 WHERE CLIENT={0}", "~CLIENT~");
            dictParams.Add("CLIENT", Globals.objPCL30.PMSPCL30Client);
            string sResult = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams));
            if (string.IsNullOrEmpty(sResult))
                {
                bNewClient = true;
                AddNewClient();
                }
            else
                {
                bNewClient = false;
                Globals.bNewClaimant = false;
                Globals.objPCT30.PMSPCT30Client = Convert.ToInt64(sResult);
                }
            UpdateClientRow(bNewClient);

            if (bNewClient && !RC.Conversion.ConvertObjToBool(Globals.objConfig.ConfigElement.PointClient, m_iClientId))
                {
                objCLMT = Globals.objPCT30.Clone();
                Globals.collClaimantUpdates.Add(objCLMT);
                }

            Globals.dbg.PopProc();
            }
        
        public static void LocateBASCLT0100()
            {
            BASCLT0100 objCLT = default(BASCLT0100);
            bool bNewClient = false;

            Globals.dbg.PushProc("LocateBASCLT0100");
            ClearDictionaryObject();
            strSQL.AppendFormat("SELECT RECPROG FROM BASCLT0100 WHERE CLTSEQNUM = {0}", "~CLTSEQNUM~");
            dictParams.Add("CLTSEQNUM", Globals.objBASCLT1700.CLTSEQNUM);
            string sResult = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams));

            //string sResult = Convert.ToString(ExecuteScalar("SELECT RECPROG FROM BASCLT0100 WHERE CLTSEQNUM = '" + Convert.ToString(Globals.objBASCLT1700.CLTSEQNUM) + "'"));
            if (string.IsNullOrEmpty(sResult) || Globals.objBASCLT1700.CLTSEQNUM == 0)
                {
                GetNextClientSequence();
                bNewClient = true;
                Globals.objBASCLT0100.EFFDATE = System.DateTime.Now.Date;
                //Start SISamsung - Only update RM clients and skip update of POINT clients
                Globals.objBASCLT0100.CLTSEQNUM = Globals.objBASCLT1700.CLTSEQNUM;
                UpdateBASCLT0100(bNewClient);
                }
            else
                {
                bNewClient = false;
                //  Start SISamsung - Only update RM clients and skip update of POINT clients
                if ((string.Equals(Globals.objConfig.ConfigElement.UploadPointEntity.Trim(), "-1")))// dbisht6 mits 36536 
                {
                    Globals.objBASCLT0100.CLTSEQNUM = Globals.objBASCLT1700.CLTSEQNUM;
                    UpdateBASCLT0100(bNewClient);
                }
               
                }
            Globals.dbg.LogEntry("objBASCLT0100.CLTSEQNUM");
            Globals.dbg.LogEntry(Convert.ToString(Globals.objBASCLT0100.CLTSEQNUM));
               if(Globals.bupdateDefaultDOB)
               {
                   UpdateDateinBASCLT0100(Globals.objBASCLT0100.CLTSEQNUM);
               }

            if (bNewClient)
                {
                objCLT = Globals.objBASCLT0100.Clone();
                Globals.collClaimantUpdates.Add(objCLT);
                }
            Globals.dbg.PopProc();
            }


        public static void UpdateDateinBASCLT0100(long iCLTSEQ)
        {
            string sSql = string.Empty;
            ClearDictionaryObject();
            strSQL.AppendFormat("SELECT RECPROG FROM BASCLT0100 WHERE CLTSEQNUM = {0}", "~CLTSEQNUM~");
            dictParams.Add("CLTSEQNUM", iCLTSEQ);
            string sResult = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams));
            try
            {
                if (!string.IsNullOrEmpty(sResult))
                {
                    DbFactory.ExecuteNonQuery(Globals.objConfig.ConfigElement.DataDSNConnStr, "UPDATE BASCLT0100 SET  BIRTHDATE='1/1/0001'  WHERE  CLTSEQNUM = " + iCLTSEQ.ToString());
                    //Globals.objWriter.Tables.Add("BASCLT0100");
                    //Globals.objWriter.Fields.Add("BIRTHDATE", "1/1/0001");
                    //Globals.objWriter.Where.Add(" CLTSEQNUM = " + iCLTSEQ.ToString());
                    //Globals.objWriter.Execute();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //Globals.objWriter.Reset(true);
            }


        }


        public static void LocateBASCLT0300()
            {
            Globals.dbg.PushProc("LocateBASCLT0300");
            DbReader dbReader = null;
            bool bNew = false;
            try
                {
                ClearDictionaryObject();
                strSQL.AppendFormat("SELECT RECPROG FROM BASCLT0300  WHERE CLTSEQNUM = {0} AND ADDRSEQNUM = {1}", "~CLTSEQNUM~", "~ADDRSEQNUM~");
                dictParams.Add("CLTSEQNUM", Globals.objBASCLT1700.CLTSEQNUM);
                dictParams.Add("ADDRSEQNUM", Globals.objBASCLT1700.ADDRSEQNUM);
                string sResult = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams));
                //string sResult = Convert.ToString(ExecuteScalar("SELECT RECPROG FROM BASCLT0300  WHERE CLTSEQNUM = '" + Convert.ToString(Globals.objBASCLT1700.CLTSEQNUM) + "' AND ADDRSEQNUM = '" + Convert.ToString(Globals.objBASCLT1700.ADDRSEQNUM) + "'"));
                if (string.IsNullOrEmpty(sResult))
                    {

                    Globals.dbg.DebugTrace("A", Globals.objBASCLT0300.ADDRLN1, Globals.objBASCLT0300.ADDRLN2, Globals.objBASCLT0300.ADDRLN3, Globals.objBASCLT0300.ADDRLN4);
                    Globals.dbg.DebugTrace("B", Globals.objBASCLT0300.CITY, Globals.objBASCLT0300.STATE, Globals.objBASCLT0300.ZIPCODE, Globals.objBASCLT0300.COUNTRY);
                    
                    ClearDictionaryObject();
                    strSQL.AppendFormat("SELECT * FROM BASCLT0300 WHERE CLTSEQNUM = {0}   AND ADDRLN1 = {1}   AND ADDRLN2 = {2}  AND ADDRLN3 = {3}  AND ADDRLN4 = {4}  AND CITY = {5}   AND STATE = {6}   AND ZIPCODE =  {7}      AND COUNTRY = {8}", "~CLTSEQNUM~", "~ADDRLN1~", "~ADDRLN2~", "~ADDRLN3~", "~ADDRLN4~", "~CITY~", "~STATE~", "~ZIPCODE~", "~COUNTRY~");
                    dictParams.Add("CLTSEQNUM", Globals.objBASCLT1700.CLTSEQNUM);
                    dictParams.Add("ADDRLN1", Globals.objBASCLT0300.ADDRLN1);
                    dictParams.Add("ADDRLN2", Globals.objBASCLT0300.ADDRLN2);
                    dictParams.Add("ADDRLN3", Globals.objBASCLT0300.ADDRLN3);
                    dictParams.Add("ADDRLN4", Globals.objBASCLT0300.ADDRLN4);
                    dictParams.Add("CITY", Globals.objBASCLT0300.CITY);
                    dictParams.Add("STATE", Globals.objBASCLT0300.STATE);
                    dictParams.Add("ZIPCODE", Globals.objBASCLT0300.ZIPCODE);
                    dictParams.Add("COUNTRY", Globals.objBASCLT0300.COUNTRY);
                    dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams);
                    //dbReader = ExecuteReader("SELECT * FROM BASCLT0300 WHERE CLTSEQNUM = '" + Convert.ToString(Globals.objBASCLT1700.CLTSEQNUM) + "'   AND ADDRLN1 = '" + Globals.objBASCLT0300.ADDRLN1 + "'   AND ADDRLN2 = '" + Globals.objBASCLT0300.ADDRLN2 + "'  AND ADDRLN3 = '" + Globals.objBASCLT0300.ADDRLN3 + "'  AND ADDRLN4 = '" + Globals.objBASCLT0300.ADDRLN4 + "'  AND CITY = '" + Globals.objBASCLT0300.CITY + "'   AND STATE = '" + Globals.objBASCLT0300.STATE + "'   AND ZIPCODE =  '" + Globals.objBASCLT0300.ZIPCODE + "'      AND COUNTRY = '" + Globals.objBASCLT0300.COUNTRY + "'");
                    if (!dbReader.Read() || Globals.objBASCLT1700.ADDRSEQNUM == 0)
                        {
                        bNew = true;
                        GetNextAddressSequence();
                        Globals.objBASCLT0300.EFFDATE = System.DateTime.Now;
                        Globals.objBASCLT0300.RECDATE = System.DateTime.Now.Date;
                        Globals.objBASCLT0300.RECTIME = System.DateTime.Now;
                        Globals.objBASCLT0300.CLTSEQNUM = Globals.objBASCLT1700.CLTSEQNUM;
                        Globals.objBASCLT0300.ADDRSEQNUM = Globals.objBASCLT1700.ADDRSEQNUM;
                        UpdateBASCLT0300(bNew);
                        }
                    else
                    {
                        Globals.objBASCLT1700.CLTSEQNUM = Convert.ToInt64(dbReader.GetValue("CLTSEQNUM"));
                        Globals.objBASCLT1700.ADDRSEQNUM = Convert.ToInt64(dbReader.GetValue("ADDRSEQNUM"));
                        ClearDictionaryObject();
                        strSQL.AppendFormat("SELECT RECPROG FROM BASCLT0300  WHERE CLTSEQNUM = {0} AND ADDRSEQNUM = {1}", "~CLTSEQNUM~", "~ADDRSEQNUM~");
                        dictParams.Add("CLTSEQNUM", Globals.objBASCLT1700.CLTSEQNUM);
                        dictParams.Add("ADDRSEQNUM", Globals.objBASCLT1700.ADDRSEQNUM);
                        sResult = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams));
                        //sResult = Convert.ToString(ExecuteScalar("SELECT RECPROG FROM BASCLT0300  WHERE CLTSEQNUM = '" + Convert.ToString(Globals.objBASCLT1700.CLTSEQNUM) + "' AND ADDRSEQNUM = '" + Convert.ToString(Globals.objBASCLT1700.ADDRSEQNUM) + "'"));
                        //Start SISamsung - Only update RM clients addresses and skip update of POINT clients
                        if ((string.Equals(Globals.objConfig.ConfigElement.UploadPointEntity.Trim(), "-1"))) // dbisht6 mits 36536
                        {
                            Globals.objBASCLT0300.CLTSEQNUM = Globals.objBASCLT1700.CLTSEQNUM;
                            Globals.objBASCLT0300.ADDRSEQNUM = Globals.objBASCLT1700.ADDRSEQNUM;
                            UpdateBASCLT0300(bNew);
                        }
                        //End SISamsung
                        
                    }
                    }
                else
                    {
                    //Start SISamsung - Only update RM clients addresses and skip update of POINT clients
                        if ((string.Equals(Globals.objConfig.ConfigElement.UploadPointEntity.Trim(), "-1"))) // dbisht6 mits 36536
                        {
                        Globals.objBASCLT0300.CLTSEQNUM = Globals.objBASCLT1700.CLTSEQNUM;
                        Globals.objBASCLT0300.ADDRSEQNUM = Globals.objBASCLT1700.ADDRSEQNUM;
                        UpdateBASCLT0300(bNew);
                        }
                    //End SISamsung
                  
                    }

                //UpdateBASCLT0300(bNew);

                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                if (dbReader != null)
                    dbReader.Dispose();
                Globals.dbg.PopProc();
                }
            }

        private static void UpdateClientRow(bool bNewClient)
            {
            Globals.dbg.PushProc("UpdateClientRow");
            DbReader dbReader = null;
            try
                {
                Globals.objWriter.Tables.Add("PMSPCT30");
                if (!bNewClient)
                    {
                    Globals.objWriter.Where.Add(" CLIENT='" + Convert.ToString(Globals.objPCT30.PMSPCT30Client) + "'");
                    }

                if (!string.IsNullOrEmpty(Convert.ToString(Globals.objPCT30.PMSPCT30Client)))
                    Globals.objWriter.Fields.Add("CLIENT", Globals.objPCT30.PMSPCT30Client);
                if (!string.IsNullOrEmpty(Globals.objPCT30.PMSPCT30Ct30type))
                    Globals.objWriter.Fields.Add("CT30TYPE", StringUtils.Left(Globals.objPCT30.PMSPCT30Ct30type + " ", 2));
                if (!string.IsNullOrEmpty(Globals.objPCT30.PMSPCT30ClientType))
                    Globals.objWriter.Fields.Add("CLIENTTYPE", StringUtils.Left(Globals.objPCT30.PMSPCT30ClientType + " ", 1));
                if (!string.IsNullOrEmpty(Globals.objPCT30.PMSPCT30LName))
                    Globals.objWriter.Fields.Add("LNAME", StringUtils.Left(Globals.objPCT30.PMSPCT30LName + " ", 15));
                if (!string.IsNullOrEmpty(Globals.objPCT30.PMSPCT30FName))
                    Globals.objWriter.Fields.Add("FNAME", StringUtils.Left(Globals.objPCT30.PMSPCT30FName + " ", 11));
                if (!string.IsNullOrEmpty(Globals.objPCT30.PMSPCT30MName))
                    Globals.objWriter.Fields.Add("MNAME", StringUtils.Left(Globals.objPCT30.PMSPCT30MName + " ", 1));
                if (!string.IsNullOrEmpty(Globals.objPCT30.PMSPCT30SirName))
                    Globals.objWriter.Fields.Add("SIRNAME", StringUtils.Left(Globals.objPCT30.PMSPCT30SirName + " ", 3));
                if (!string.IsNullOrEmpty(Globals.objPCT30.PMSPCT30Address1))
                    Globals.objWriter.Fields.Add("ADDRESS1", StringUtils.Left(Globals.objPCT30.PMSPCT30Address1 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objPCT30.PMSPCT30Address2))
                    Globals.objWriter.Fields.Add("ADDRESS2", StringUtils.Left(Globals.objPCT30.PMSPCT30Address2 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objPCT30.PMSPCT30City))
                    Globals.objWriter.Fields.Add("CITY", StringUtils.Left(Globals.objPCT30.PMSPCT30City + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objPCT30.PMSPCT30State))
                    Globals.objWriter.Fields.Add("STATE", StringUtils.Left(Globals.objPCT30.PMSPCT30State + " ", 2));
                if (!string.IsNullOrEmpty(Globals.objPCT30.PMSPCT30ZipCode))
                    Globals.objWriter.Fields.Add("ZIPCODE", StringUtils.Left(Globals.objPCT30.PMSPCT30ZipCode + " ", 10));
                if (!string.IsNullOrEmpty(Globals.objPCT30.PMSPCT30Sex))
                    Globals.objWriter.Fields.Add("SEX", StringUtils.Left(Globals.objPCT30.PMSPCT30Sex + " ", 1));
                if (Globals.objPCT30.PMSPCT30Age != null) //RMA-4939
                    Globals.objWriter.Fields.Add("AGE", StringUtils.Left(Globals.objPCT30.PMSPCT30Age + " ", 3));
                if (!string.IsNullOrEmpty(Globals.objPCT30.PMSPCT30SSN))
                    Globals.objWriter.Fields.Add("SSN", StringUtils.Left(Globals.objPCT30.PMSPCT30SSN + " ", 9));
                if (!string.IsNullOrEmpty(Globals.objPCT30.PMSPCT30Contact))
                    Globals.objWriter.Fields.Add("CONTACT", StringUtils.Left(Globals.objPCT30.PMSPCT30Contact + " ", 30));
                //if (!string.IsNullOrEmpty(Globals.objPCT30.PMSPCT30Phone))
                if (Globals.objPCT30.PMSPCT30Phone != null) //aaggarwal29 : JIRA 7031, let empty string pass to POINT so that if phone number is changed to empty in rmA later, it gets updated properly
                    Globals.objWriter.Fields.Add("PHONE", StringUtils.Left(Globals.objPCT30.PMSPCT30Phone + " ", 10));
                if (!string.IsNullOrEmpty(Globals.objPCT30.PMSPCT30TaxId))
                    Globals.objWriter.Fields.Add("TAXID", StringUtils.Left(Globals.objPCT30.PMSPCT30TaxId + " ", 15));

                Execute(Globals.objWriter);

                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.objWriter.Reset(true);
                if (dbReader != null)
                    dbReader.Dispose();
                Globals.dbg.PopProc();
                }


            }
        private static string limitFieldReplaceEmptyWithSpace(string sParam, int iLen) {
            if (!string.IsNullOrEmpty(sParam))
                return StringUtils.Left(sParam, iLen);
            else
                return " ";
        } 

        private static void UpdateBASCLT0100(bool bNewClient)
            {
            Globals.dbg.PushProc("UpdateBASCLT0100");
            Parameters objParams = (Parameters)Globals.GetParams();
            Globals.objWriter.Tables.Add("BASCLT0100");
            if (!bNewClient)
                {
                Globals.objWriter.Where.Add(" CLTSEQNUM = '" + Convert.ToString(Globals.objBASCLT0100.CLTSEQNUM) + "'");
                }
            if (Globals.objBASCLT0100.CLTSEQNUM != 0)
                Globals.objWriter.Fields.Add("CLTSEQNUM", Globals.objBASCLT0100.CLTSEQNUM);
            //SISamsung: Group no is not updated by RMA
            //if (!string.IsNullOrEmpty(Globals.objBASCLT0100.GROUPNO))
            //    Globals.objWriter.Fields.Add("GROUPNO", StringUtils.Left(Globals.objBASCLT0100.GROUPNO, 10));
            //End SISamsung
            if (!string.IsNullOrEmpty(Globals.objBASCLT0100.NAMETYPE))
                Globals.objWriter.Fields.Add("NAMETYPE", StringUtils.Left(Globals.objBASCLT0100.NAMETYPE, 1));
            if (!string.IsNullOrEmpty(Globals.objBASCLT0100.NAMESTATUS))
                Globals.objWriter.Fields.Add("NAMESTATUS", StringUtils.Left(Globals.objBASCLT0100.NAMESTATUS, 1));
            //Start 'SI07731

            if (Globals.cPointRelease == Globals.POINT_INJ_SAMSUNG)
                {
                Globals.objWriter.Fields.Add("CLIENTNAME", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.CLIENTNAME, 60));
                Globals.objWriter.Fields.Add("FIRSTNAME", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.FIRSTNAME, 40));
                Globals.objWriter.Fields.Add("MIDNAME", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.MIDNAME, 40));
                Globals.objWriter.Fields.Add("PREFIXNM", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.PREFIXNM, 8));
                Globals.objWriter.Fields.Add("SUFFIXNM", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.SUFFIXNM, 40));
                Globals.objWriter.Fields.Add("LONGNAME", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.LONGNAME, 60));
                Globals.objWriter.Fields.Add("DBA", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.DBA, 60));

                }
            else if (Globals.cPointRelease >= Globals.POINT_INJ_GLBL)
                {
                Globals.objWriter.Fields.Add("CLIENTNAME", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.CLIENTNAME, 256));
                Globals.objWriter.Fields.Add("FIRSTNAME", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.FIRSTNAME, 64));
                Globals.objWriter.Fields.Add("MIDNAME", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.MIDNAME, 64));
                Globals.objWriter.Fields.Add("PREFIXNM", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.PREFIXNM, 64));
                Globals.objWriter.Fields.Add("SUFFIXNM", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.SUFFIXNM, 64));
                Globals.objWriter.Fields.Add("LONGNAME", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.LONGNAME, 512));
                Globals.objWriter.Fields.Add("DBA", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.DBA, 256));
                }
            else
                {
                Globals.objWriter.Fields.Add("CLIENTNAME", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.CLIENTNAME, 60));
                Globals.objWriter.Fields.Add("FIRSTNAME", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.FIRSTNAME, 40));
                Globals.objWriter.Fields.Add("MIDNAME", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.MIDNAME, 40));
                Globals.objWriter.Fields.Add("PREFIXNM", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.PREFIXNM, 8));
                Globals.objWriter.Fields.Add("SUFFIXNM", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.SUFFIXNM, 40));
                Globals.objWriter.Fields.Add("LONGNAME", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.LONGNAME, 60));
                Globals.objWriter.Fields.Add("DBA", limitFieldReplaceEmptyWithSpace(Globals.objBASCLT0100.DBA, 60));
                } 
            if (!string.IsNullOrEmpty(Globals.objBASCLT0100.CONTACT))
                Globals.objWriter.Fields.Add("CONTACT", StringUtils.Left(Globals.objBASCLT0100.CONTACT, 30));
            if (!string.IsNullOrEmpty(Globals.objBASCLT0100.PHONE1))
                Globals.objWriter.Fields.Add("PHONE1", StringUtils.Left(Globals.objBASCLT0100.PHONE1, 32));
            else
                Globals.objWriter.Fields.Add("PHONE1", " "); // JIRA 7031: added empty string for phone update
            if (!string.IsNullOrEmpty(Globals.objBASCLT0100.PHONE2))
                Globals.objWriter.Fields.Add("PHONE2", StringUtils.Left(Globals.objBASCLT0100.PHONE2, 32));
            else
                Globals.objWriter.Fields.Add("PHONE2", " ");// JIRA 7031: added empty string for phone update
            if (!string.IsNullOrEmpty(Globals.objBASCLT0100.FEDTAXID))
                Globals.objWriter.Fields.Add("FEDTAXID", StringUtils.Left(Globals.objBASCLT0100.FEDTAXID, 12));
            if (!string.IsNullOrEmpty(Globals.objBASCLT0100.SSN))
                Globals.objWriter.Fields.Add("SSN", StringUtils.Left(Globals.objBASCLT0100.SSN, 11));
            if (!string.IsNullOrEmpty(Globals.objBASCLT0100.SEX))
                Globals.objWriter.Fields.Add("SEX", StringUtils.Left(Globals.objBASCLT0100.SEX, 1));
            // rrachev JIRA RMA-7701
            //if (Convert.ToString(Globals.objBASCLT0100.BIRTHDATE) == "1/1/0001 12:00:00 AM") // "1/1/1900 12:00:00 AM"
            //    Globals.objWriter.Fields.Add("BIRTHDATE", Globals.GetDateTime("1/1/1900"));
            //else
            //    Globals.objWriter.Fields.Add("BIRTHDATE", Globals.objBASCLT0100.BIRTHDATE);
            Globals.objWriter.Fields.Add("BIRTHDATE", ToValidSQLDate(Globals.objBASCLT0100.BIRTHDATE));

            // rrachev JIRA RMA-7701
            //if (Convert.ToString(Globals.objBASCLT0100.EFFDATE) == "1/1/0001 12:00:00 AM") // "1/1/1900 12:00:00 AM"
            //    Globals.objWriter.Fields.Add("EFFDATE", Globals.GetDateTime("1/1/1900"));
            //else
            //    Globals.objWriter.Fields.Add("EFFDATE", Globals.objBASCLT0100.EFFDATE);
            Globals.objWriter.Fields.Add("EFFDATE", ToValidSQLDate(Globals.objBASCLT0100.EFFDATE));

            if(bNewClient)
            Globals.objWriter.Fields.Add("RECPROG", "CCPPC");
            
            Globals.objWriter.Fields.Add("RECUSER", Globals.objConfig.ConfigElement.DataDSNUser);//objParams.Parameter(Globals.SWDataUser));

            // rrachev JIRA RMA-7701
            //if (Convert.ToString(Globals.objBASCLT0100.RECDATE) == "1/1/0001 12:00:00 AM")
            //    Globals.objWriter.Fields.Add("RECDATE", Globals.GetDateTime("1/1/1900"));
            //else
            //    Globals.objWriter.Fields.Add("RECDATE", Globals.objBASCLT0100.RECDATE);
            Globals.objWriter.Fields.Add("RECDATE", ToValidSQLDate(Globals.objBASCLT0100.RECDATE));

            // rrachev JIRA RMA-7701
            //if (Convert.ToString(Globals.objBASCLT0100.RECTIME) == "1/1/0001 12:00:00 AM")
            //    Globals.objWriter.Fields.Add("RECTIME", Globals.GetDateTime("1/1/1900"));
            //else
            //    Globals.objWriter.Fields.Add("RECTIME", Globals.objBASCLT0100.RECTIME);
            Globals.objWriter.Fields.Add("RECTIME", ToValidSQLDate(Globals.objBASCLT0100.RECTIME));

            try
                {
                Execute(Globals.objWriter);
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.objWriter.Reset(true);
                Globals.dbg.PopProc();
                }

            }

        private static void UpdateBASCLT0300(bool bNewRecord)
            {
            string sPN = null;
            sPN = "UpdateBASCLT0300";
            Globals.dbg.PushProc(sPN);
            Parameters objParams = (Parameters)Globals.GetParams();

            Globals.objWriter.Tables.Add("BASCLT0300");
            if (!bNewRecord)
                {
                Globals.objWriter.Where.Add("CLTSEQNUM = '" + Convert.ToString(Globals.objBASCLT0300.CLTSEQNUM) + "' AND ADDRSEQNUM = '" + Convert.ToString(Globals.objBASCLT0300.ADDRSEQNUM) + "'");
                }
            if (Globals.objBASCLT0300.CLTSEQNUM != 0)
                Globals.objWriter.Fields.Add("CLTSEQNUM", Globals.objBASCLT0300.CLTSEQNUM);
            if (Globals.objBASCLT0300.ADDRSEQNUM != 0)
                Globals.objWriter.Fields.Add("ADDRSEQNUM", Globals.objBASCLT0300.ADDRSEQNUM);
            if (!string.IsNullOrEmpty(Globals.objBASCLT0300.ADDRTYPE))
                Globals.objWriter.Fields.Add("ADDRTYPE", StringUtils.Left(Globals.objBASCLT0300.ADDRTYPE, 10));
            if (!string.IsNullOrEmpty(Globals.objBASCLT0300.ADDRSTATUS))
                Globals.objWriter.Fields.Add("ADDRSTATUS", StringUtils.Left(Globals.objBASCLT0300.ADDRSTATUS, 1));
            if (!string.IsNullOrEmpty(Globals.objBASCLT0300.ADDRLN1))
                Globals.objWriter.Fields.Add("ADDRLN1", StringUtils.Left(Globals.objBASCLT0300.ADDRLN1, 64));
            if (!string.IsNullOrEmpty(Globals.objBASCLT0300.ADDRLN2))
                Globals.objWriter.Fields.Add("ADDRLN2", StringUtils.Left(Globals.objBASCLT0300.ADDRLN2, 64));
            if (!string.IsNullOrEmpty(Globals.objBASCLT0300.ADDRLN3))
                Globals.objWriter.Fields.Add("ADDRLN3", StringUtils.Left(Globals.objBASCLT0300.ADDRLN3, 64));
            if (!string.IsNullOrEmpty(Globals.objBASCLT0300.ADDRLN4))
                Globals.objWriter.Fields.Add("ADDRLN4", StringUtils.Left(Globals.objBASCLT0300.ADDRLN4, 64));
            if (!string.IsNullOrEmpty(Globals.objBASCLT0300.CITY))
                Globals.objWriter.Fields.Add("CITY", StringUtils.Left(Globals.objBASCLT0300.CITY, 32));
            if (!string.IsNullOrEmpty(Globals.objBASCLT0300.STATE))
                Globals.objWriter.Fields.Add("STATE", StringUtils.Left(Globals.objBASCLT0300.STATE, 2));
            if (!string.IsNullOrEmpty(Globals.objBASCLT0300.ZIPCODE))
                Globals.objWriter.Fields.Add("ZIPCODE", StringUtils.Left(Globals.objBASCLT0300.ZIPCODE, 11));
            if (!string.IsNullOrEmpty(Globals.objBASCLT0300.COUNTRY))
                Globals.objWriter.Fields.Add("COUNTRY", StringUtils.Left(Globals.objBASCLT0300.COUNTRY, 3));
            if (!string.IsNullOrEmpty(Globals.objBASCLT0300.COUNTY))
                Globals.objWriter.Fields.Add("COUNTY", StringUtils.Left(Globals.objBASCLT0300.COUNTY, 3));
            // rrachev JIRA RMA-7701
            //if (Convert.ToString(Globals.objBASCLT0300.EFFDATE) == "1/1/0001 12:00:00 AM")
            //    Globals.objWriter.Fields.Add("EFFDATE", Globals.GetDateTime("1/1/1900"));
            //else
            //    Globals.objWriter.Fields.Add("EFFDATE", Globals.objBASCLT0300.EFFDATE);
            Globals.objWriter.Fields.Add("EFFDATE", ToValidSQLDate(Globals.objBASCLT0300.EFFDATE));

            Globals.objWriter.Fields.Add("RECPROG", "CCPPC");
            Globals.objWriter.Fields.Add("RECUSER", Globals.objConfig.ConfigElement.DataDSNUser);

            // rrachev JIRA RMA-7701
            //if (Convert.ToString(Globals.objBASCLT0300.RECDATE) == "1/1/0001 12:00:00 AM")
            //    Globals.objWriter.Fields.Add("RECDATE", Globals.GetDateTime("1/1/1900"));
            //else
            //    Globals.objWriter.Fields.Add("RECDATE", Globals.objBASCLT0300.RECDATE);
            Globals.objWriter.Fields.Add("RECDATE", ToValidSQLDate(Globals.objBASCLT0300.RECDATE));

            // rrachev JIRA RMA-7701
            //if (Convert.ToString(Globals.objBASCLT0300.RECTIME) == "1/1/0001 12:00:00 AM")
            //    Globals.objWriter.Fields.Add("RECTIME", Globals.GetDateTime("1/1/1900"));
            //else
            //    Globals.objWriter.Fields.Add("RECTIME", Globals.objBASCLT0300.RECTIME);
            Globals.objWriter.Fields.Add("RECTIME", ToValidSQLDate(Globals.objBASCLT0300.RECTIME));

            try
                {
                Execute(Globals.objWriter);
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.objWriter.Reset(true);
                Globals.dbg.PopProc();
                }

            }
        
        private static void UpdateBASCLT1700()
            {
            Globals.dbg.PushProc("UpdateBASCLT1700");
            Parameters objParams = (Parameters)Globals.GetParams();
            Globals.objWriter.Tables.Add("BASCLT1700");
            if (!Globals.bNewClaimant)
                {
                Globals.objWriter.Where.Add("CLAIM = '" + Globals.objBASCLT1700.CLAIM + "' AND CLMTSEQ ='" + Globals.objBASCLT1700.CLMTSEQ + "'");
                }


            if (!string.IsNullOrEmpty(Globals.objBASCLT1700.CLAIM))
                Globals.objWriter.Fields.Add("CLAIM", StringUtils.Left(Globals.objBASCLT1700.CLAIM, 12));
            if (Globals.objBASCLT1700.CLMTSEQ != 0)
                Globals.objWriter.Fields.Add("CLMTSEQ", Globals.objBASCLT1700.CLMTSEQ);
            if (Globals.objBASCLT1700.CLTSEQNUM != 0)
                Globals.objWriter.Fields.Add("CLTSEQNUM", Globals.objBASCLT1700.CLTSEQNUM);
            if (Globals.objBASCLT1700.ADDRSEQNUM != 0)
                Globals.objWriter.Fields.Add("ADDRSEQNUM", Globals.objBASCLT1700.ADDRSEQNUM);
            if (!string.IsNullOrEmpty(Globals.objBASCLT1700.ROLE))
                Globals.objWriter.Fields.Add("ROLE", StringUtils.Left(Globals.objBASCLT1700.ROLE, 12));
            Globals.objWriter.Fields.Add("RECPROG", "CCPPC");
            Globals.objWriter.Fields.Add("RECUSER", Globals.objConfig.ConfigElement.DataDSNUser);

            // rrachev JIRA RMA-7701
            //if (Convert.ToString(Globals.objBASCLT1700.RECDATE) == "1/1/0001 12:00:00 AM")
            //    Globals.objWriter.Fields.Add("RECDATE", Globals.GetDateTime("1/1/1900"));
            //else
            //    Globals.objWriter.Fields.Add("RECDATE", Globals.objBASCLT1700.RECDATE);
            Globals.objWriter.Fields.Add("RECDATE", ToValidSQLDate(Globals.objBASCLT1700.RECDATE));

            // rrachev JIRA RMA-7701
            //if (Convert.ToString(Globals.objBASCLT1700.RECTIME) == "1/1/0001 12:00:00 AM")
            //    Globals.objWriter.Fields.Add("RECTIME", Globals.GetDateTime("1/1/1900"));
            //else
            //    Globals.objWriter.Fields.Add("RECTIME", Globals.objBASCLT1700.RECTIME);
            Globals.objWriter.Fields.Add("RECTIME", ToValidSQLDate(Globals.objBASCLT1700.RECTIME));

            try
                {
                Execute(Globals.objWriter);
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.objWriter.Reset(true);
                //if (dbReader != null)
                //    dbReader.Dispose();
                }
            }

        public static void LocateClaimCoverage()
            {
            Globals.dbg.PushProc("LocateClaimCoverage");
            ClearDictionaryObject();
            strSQL.AppendFormat("SELECT RECSTATUS FROM PMSPCL42 WHERE CLAIM={0} AND CLMTSEQ={1} AND POLCOVSEQ={2}","~CLAIM~","~CLMTSEQ~","~POLCOVSEQ~");
            dictParams.Add("CLAIM", Globals.sClaimNumber);
            dictParams.Add("CLMTSEQ", Globals.sCLMTSEQ);
            dictParams.Add("POLCOVSEQ", Globals.sPolCovSeq);

            string sResult = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams));
            //string sResult = Convert.ToString(ExecuteScalar("SELECT RECSTATUS FROM PMSPCL42 WHERE CLAIM='" + Globals.sClaimNumber + "' AND CLMTSEQ=" + Globals.sCLMTSEQ + " AND POLCOVSEQ=" + Globals.sPolCovSeq));
            if (string.IsNullOrEmpty(sResult))
                {
                Globals.bNewReserve = true;

                }
            else
                {
                Globals.bNewReserve = false;
                Globals.bNewClaim = false;
                Globals.objPCL42.PMSPCL42RecStatus = sResult;
                }
            UpdateClaimCoverageRow();
            if (Globals.bWCompInsLine == true)
                {
                UpdateWCompClassCode();
                }
            Globals.dbg.PopProc();
            }

        private static void UpdateClaimCoverageRow()
            {

            string sAdjustor = string.Empty, sExaminerCD = string.Empty, sClaimOff = string.Empty, sExaminer = string.Empty;

            Globals.dbg.PushProc("UpdateClaimCoverageRow");
            Parameters objParams = (Parameters)Globals.GetParams();
            Globals.objWriter.Tables.Add("PMSPCL42");

            if (Globals.bNewReserve)
                {
                Globals.objWriter.Fields.Add("CLAIM", StringUtils.Left(Globals.sClaimNumber, 12));
                Globals.objWriter.Fields.Add("CLMTSEQ", Globals.sCLMTSEQ);
                Globals.objWriter.Fields.Add("POLCOVSEQ", Globals.sPolCovSeq);
                }
            else
                {
                Globals.objWriter.Where.Add("CLAIM='" + Globals.sClaimNumber + "' AND CLMTSEQ=" + Globals.sCLMTSEQ + " AND POLCOVSEQ=" + Globals.sPolCovSeq);
                }

            if (!string.IsNullOrEmpty(Globals.objPCL42.PMSPCL42RecStatus))
                Globals.objWriter.Fields.Add("RECSTATUS", StringUtils.Left(Globals.objPCL42.PMSPCL42RecStatus + " ", 1));
            if (!string.IsNullOrEmpty(Globals.objPCL42.PMSPCL42Symbol))
                {
                if (Globals.cPointRelease >= Globals.POINT_INJ_GLBL)
                    {
                    Globals.objWriter.Fields.Add("SYMBOL", StringUtils.Left(Globals.objPCL42.PMSPCL42Symbol, 3));
                    }
                else
                    {
                    Globals.objWriter.Fields.Add("SYMBOL", StringUtils.Left(Globals.objPCL42.PMSPCL42Symbol + " ", 3));
                    }
                }
            if (!string.IsNullOrEmpty(Globals.objPCL42.PMSPCL42PolicyNo))
                Globals.objWriter.Fields.Add("POLICYNO", StringUtils.Left(Globals.objPCL42.PMSPCL42PolicyNo + " ", 7));
            if (!string.IsNullOrEmpty(Globals.objPCL42.PMSPCL42Module))
                Globals.objWriter.Fields.Add("MODULE", StringUtils.Left(Globals.objPCL42.PMSPCL42Module + " ", 2));
            if (!string.IsNullOrEmpty(Globals.objPCL42.PMSPCL42MasterCo))
                Globals.objWriter.Fields.Add("MASTERCO", StringUtils.Left(Globals.objPCL42.PMSPCL42MasterCo + " ", 2));
            if (!string.IsNullOrEmpty(Globals.objPCL42.PMSPCL42Location))
                Globals.objWriter.Fields.Add("LOCATION", StringUtils.Left(Globals.objPCL42.PMSPCL42Location + " ", 2));
            if (!string.IsNullOrEmpty(Globals.objPCL42.PMSPCL42UnitNo))
                Globals.objWriter.Fields.Add("UNITNO", StringUtils.Left(Globals.objPCL42.PMSPCL42UnitNo + " ", 5));
            Globals.objWriter.Fields.Add("COVSEQ", Globals.objPCL42.PMSPCL42CovSeq);
            Globals.objWriter.Fields.Add("TRANSSEQ", Globals.objPCL42.PMSPCL42TransSeq);

            if (!string.IsNullOrEmpty(Globals.objPCL42.PMSPCL42Adjustor))
                {
                sAdjustor = StringUtils.Left(Globals.objPCL42.PMSPCL42Adjustor + " ", 6);
                }
            if (!string.IsNullOrEmpty(Globals.objPCL42.PMSPCL42LossCause))
                Globals.objWriter.Fields.Add("LOSSCAUSE", StringUtils.Left(Globals.objPCL42.PMSPCL42LossCause + " ", 2));
            if (Globals.bIsNewClaimUpload)
                {
                if (Globals.cPointRelease < Globals.POINT_RELEASE9)
                    {

                    if (!string.IsNullOrEmpty(Globals.objPCL42.PMSPCL42Examiner))
                        sExaminer = StringUtils.Left(Globals.objPCL42.PMSPCL42Examiner + " ", 2);
                    if (Globals.objConfig.ConfigElement.AddDefaultExaminerCD == "YES")
                        {
                        if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.ExaminerCD))
                            {
                            sExaminer = Globals.objConfig.ConfigElement.ExaminerCD;
                            }
                        }

                    }
                else
                    {
                    if (!string.IsNullOrEmpty(Globals.objPCL42.PMSPCL42Examiner))
                        sExaminerCD = StringUtils.Left(Globals.objPCL42.PMSPCL42Examiner + " ", 2);
                    if (Globals.objConfig.ConfigElement.AddDefaultExaminerCD == "YES")
                        {
                        if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.ExaminerCD))
                            {
                            sExaminerCD = Globals.objConfig.ConfigElement.ExaminerCD;
                            }
                        }
                    }


                if (!string.IsNullOrEmpty(Globals.objPCL42.PMSPCL42ClaimOff))
                    sClaimOff = StringUtils.Left(Globals.objPCL42.PMSPCL42ClaimOff + " ", 3);
                }
            if (Globals.objConfig.ConfigElement.AddDefaultClmOffice == "YES")
                {
                if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.ClmOffice))
                    {
                    sClaimOff = Globals.objConfig.ConfigElement.ClmOffice;
                    }
                }

            if (Globals.objConfig.ConfigElement.AddDefaultAdjustor == "YES")
                {
                if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.Adjustor))
                    {
                      sAdjustor = Globals.objConfig.ConfigElement.Adjustor;
                    }
                }
            if (!string.IsNullOrEmpty(sAdjustor))
                Globals.objWriter.Fields.Add("ADJUSTOR", sAdjustor);
            if (!string.IsNullOrEmpty(sExaminer))
                Globals.objWriter.Fields.Add("EXAMINER", sExaminer);
            if (!string.IsNullOrEmpty(sClaimOff))
                Globals.objWriter.Fields.Add("CLAIMOFF", sClaimOff);
            if (!string.IsNullOrEmpty(sExaminerCD))
                Globals.objWriter.Fields.Add("EXAMINERCD", sExaminerCD);
            try
                {
                Execute(Globals.objWriter);
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.objWriter.Reset(true);
                Globals.dbg.PopProc();
                }
            }

        public static DbReader PointClaimDEReservePayment(string sClaimNumber, int sCLMTSEQ, int sPolCovSeq, string sPMSPCL50ResvNo)
            {
            string sTrnSeq = string.Empty, sSql2 = string.Empty;
            DbReader dbReader = null;
            ClearDictionaryObject();
            TotPayment = 0;
            CurrResv = 0;
            if (Globals.cPointRelease >= Globals.POINT_INJ_GLBL)
                {
                strSQL.AppendFormat("SELECT TRANSSEQ,TOTPAYMENT,CURRRESV FROM PMSPCL50 WHERE CLAIM = {0} AND CLMTSEQ = {1} AND POLCOVSEQ ={2} AND RESVNO = {3} ORDER BY TRANSSEQ DESC", "~CLAIM~", "~CLMTSEQ~", "~POLCOVSEQ~", "~RESVNO~");
                //sSql2 = "SELECT TRANSSEQ,TOTPAYMENT,CURRRESV FROM PMSPCL50 WHERE CLAIM = '" + sClaimNumber + "' AND CLMTSEQ = " + sCLMTSEQ + " AND POLCOVSEQ =" + sPolCovSeq + " AND RESVNO = '" + sPMSPCL50ResvNo + "' ORDER BY TRANSSEQ DESC";
                }
            else
                {
                strSQL.AppendFormat("SELECT TRANSSEQ,CHAR(TOTPAYMENT) TOTPAYMENT ,CHAR(CURRRESV) CURRRESV   FROM PMSPCL50 WHERE CLAIM = {0} AND CLMTSEQ = {1} AND POLCOVSEQ ={2} AND RESVNO = {3} ORDER BY TRANSSEQ DESC", "~CLAIM~", "~CLMTSEQ~", "~POLCOVSEQ~", "~RESVNO~");
                //sSql2 = "SELECT TRANSSEQ,CHAR(TOTPAYMENT) TOTPAYMENT ,CHAR(CURRRESV) CURRRESV   FROM PMSPCL50    WHERE CLAIM = '" + sClaimNumber + "' AND CLMTSEQ = " + sCLMTSEQ + " AND POLCOVSEQ =" + sPolCovSeq + " AND RESVNO = '" + sPMSPCL50ResvNo + "' ORDER BY TRANSSEQ DESC";
                }
            dictParams.Add("CLAIM", sClaimNumber);
            dictParams.Add("CLMTSEQ", sCLMTSEQ);
            dictParams.Add("POLCOVSEQ", sPolCovSeq);
            dictParams.Add("RESVNO", sPMSPCL50ResvNo);

            dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams);
            //dbReader = ExecuteReader(sSql2);

            if (!dbReader.Read())
                {
                sTrnSeq = "0";
                TotPayment = 0;
                CurrResv = 0;
                }
            else
                {
                sTrnSeq = Conversion.ConvertObjToStr(dbReader.GetValue("TRANSSEQ"));
                TotPayment = Convert.ToSingle(dbReader.GetValue("TOTPAYMENT"));
                CurrResv = Convert.ToSingle(dbReader.GetValue("CURRRESV"));
                }

            ClearDictionaryObject();
            strSQL.AppendFormat("SELECT * FROM PMSPCL50    WHERE CLAIM = {0} AND CLMTSEQ = {1} AND POLCOVSEQ ={2} AND RESVNO = {3}   AND TRANSSEQ = {4} ORDER BY TRANSSEQ DESC ","~CLAIM~","~CLMTSEQ~","~POLCOVSEQ~","~RESVNO~","~TRANSSEQ~");
            dictParams.Add("CLAIM", sClaimNumber);
            dictParams.Add("CLMTSEQ", sCLMTSEQ);
            dictParams.Add("POLCOVSEQ", sPolCovSeq);
            dictParams.Add("RESVNO", sPMSPCL50ResvNo);
            dictParams.Add("TRANSSEQ", sTrnSeq);
            return DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams);
            //return ExecuteReader("SELECT * FROM PMSPCL50    WHERE CLAIM = '" + sClaimNumber + "' AND CLMTSEQ = " + sCLMTSEQ + " AND POLCOVSEQ =" + sPolCovSeq + " AND RESVNO = '" + sPMSPCL50ResvNo + "'   AND TRANSSEQ = '" + sTrnSeq + "' ORDER BY TRANSSEQ DESC ");


            }

        public static void LocateReservePayment()
            {
            Globals.dbg.PushProc("LocateReservePayment");
            DbReader dbReader = null;
            bool bNewResPay = true;

            dbReader = PointClaimDEReservePayment(Globals.sClaimNumber, Globals.sCLMTSEQ, Globals.sPolCovSeq, Convert.ToString(Globals.objPCL50.PMSPCL50ResvNo));

            Globals.objPCL50.PMSPCL50TransSeq = 1;
            if (dbReader.Read())
                {
                Globals.bNewClaim = false;
                bNewResPay = true;
                UpdatePriorReservePaymentRow(dbReader);
                }

            UpdateReservePaymentRow(bNewResPay);
            Globals.dbg.PopProc();
            }

        private static void UpdateReservePaymentRow(bool bNewResPay)
            {


            string sAdjustor = string.Empty, sExaminerCD = string.Empty, sClaimOff = string.Empty;
            Globals.dbg.PushProc("UpdateReservePaymentRow");
            Parameters objParams = (Parameters)Globals.GetParams();

            Globals.objWriter.Tables.Add("PMSPCL50");
            if (!bNewResPay)
                {
                Globals.objWriter.Where.Add(" CLAIM = '" + StringUtils.Left(Globals.sClaimNumber, 12) + "' AND CLMTSEQ = " + Globals.sCLMTSEQ + " AND POLCOVSEQ =" + Globals.sPolCovSeq + " AND RESVNO = '" + Globals.objPCL50.PMSPCL50ResvNo + "'   AND TRANSSEQ = '" + Globals.objPCL50.PMSPCL50TransSeq + "'");
                }

            Globals.objWriter.Fields.Add("CLAIM", StringUtils.Left(Globals.sClaimNumber, 12));
            Globals.objWriter.Fields.Add("CLMTSEQ", Globals.sCLMTSEQ);
            Globals.objWriter.Fields.Add("POLCOVSEQ", Globals.sPolCovSeq);
            Globals.objWriter.Fields.Add("RESVNO", Globals.objPCL50.PMSPCL50ResvNo);
            Globals.objWriter.Fields.Add("TRANSSEQ", Globals.objPCL50.PMSPCL50TransSeq);


            if (!string.IsNullOrEmpty(Globals.objPCL50.PMSPCL50RecStatus))
                Globals.objWriter.Fields.Add("RECSTATUS", StringUtils.Left(Globals.objPCL50.PMSPCL50RecStatus + " ", 1));
            if (!string.IsNullOrEmpty(Globals.objPCL50.PMSPCL50ClaimTrans))
                Globals.objWriter.Fields.Add("CLAIMTRANS", StringUtils.Left(Globals.objPCL50.PMSPCL50ClaimTrans + " ", 2));
            Globals.objWriter.Fields.Add("RESVPAYAMT", Globals.objPCL50.PMSPCL50ResvPayAmt);
            Globals.objWriter.Fields.Add("CURRRESV", Globals.objPCL50.PMSPCL50CurrResv);
            Globals.objWriter.Fields.Add("TOTPAYMENT", Globals.objPCL50.PMSPCL50TotPayment);
            Globals.objWriter.Fields.Add("TRANENTDT", Globals.objPCL50.PMSPCL50TranEntDt);
            if (!string.IsNullOrEmpty(Globals.objPCL50.PMSPCL50TranEntTm))
                Globals.objWriter.Fields.Add("TRANENTTM", StringUtils.Left(Globals.objPCL50.PMSPCL50TranEntTm + " ", 8));
            Globals.objWriter.Fields.Add("ACCTDTE", Globals.objPCL50.PMSPCL50AcctDte);
            if (!string.IsNullOrEmpty(Globals.objPCL50.PMSPCL50PaymentTyp))
                Globals.objWriter.Fields.Add("PAYMENTTYP", StringUtils.Left(Globals.objPCL50.PMSPCL50PaymentTyp + " ", 2));
            if (!string.IsNullOrEmpty(Globals.objPCL50.PMSPCL50ReinsInd))
                Globals.objWriter.Fields.Add("REINSIND", StringUtils.Left(Globals.objPCL50.PMSPCL50ReinsInd + " ", 1));
            if (!string.IsNullOrEmpty(Globals.objPCL50.PMSPCL50Vendor))
                Globals.objWriter.Fields.Add("VENDOR", StringUtils.Left(Globals.objPCL50.PMSPCL50Vendor + " ", 6));
            if (!string.IsNullOrEmpty(Globals.objPCL50.PMSPCL50OffsetSw))
                Globals.objWriter.Fields.Add("OFFSETSW", StringUtils.Left(Globals.objPCL50.PMSPCL50OffsetSw + " ", 1));


            if (!string.IsNullOrEmpty(Globals.objPCL50.PMSPCL50Adjustor))
                sAdjustor = StringUtils.Left(Globals.objPCL50.PMSPCL50Adjustor + " ", 6);

            if (Globals.objConfig.ConfigElement.AddDefaultAdjustor == "YES")
                {
                if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.Adjustor))
                    {
                    sAdjustor = Globals.objConfig.ConfigElement.Adjustor;
                    }
                }

            if (Globals.bIsNewClaimUpload)
                {
                if (!string.IsNullOrEmpty(Globals.objPCL50.PMSPCL50ClmOffice))
                    {
                    sClaimOff = StringUtils.Left(Globals.objPCL50.PMSPCL50ClmOffice + " ", 3);
                    }
                if (!string.IsNullOrEmpty(Globals.objPCL50.PMSPCL50ExaminerCd))
                    {
                    sExaminerCD = StringUtils.Left(Globals.objPCL50.PMSPCL50ExaminerCd + " ", 2);
                    }
                }

            if (Globals.objConfig.ConfigElement.AddDefaultExaminerCD == "YES")
                {
                if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.ExaminerCD))
                    {
                    sExaminerCD = Globals.objConfig.ConfigElement.ExaminerCD;
                    }
                }
            if (Globals.objConfig.ConfigElement.AddDefaultClmOffice == "YES")
                {
                if (!string.IsNullOrEmpty(Globals.objConfig.ConfigElement.ClmOffice))
                    {
                    sClaimOff = Globals.objConfig.ConfigElement.ClmOffice;
                    }
                }

            if (Globals.cPointRelease >= Globals.POINT_RELEASE9)
                Globals.objWriter.Fields.Add("MGAIND", " ");



            if (!string.IsNullOrEmpty(sAdjustor))
                Globals.objWriter.Fields.Add("ADJUSTOR", sAdjustor);
            if (!string.IsNullOrEmpty(sClaimOff))
                Globals.objWriter.Fields.Add("CLMOFFICE", sClaimOff);
            if (!string.IsNullOrEmpty(sExaminerCD))
                Globals.objWriter.Fields.Add("EXAMINERCD", sExaminerCD);
            try
                {
                Execute(Globals.objWriter);
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                //urrr_exit:
                Globals.objWriter.Reset(true);
                Globals.dbg.PopProc();
                //Exit Sub
                }
            }

        private static void UpdatePriorReservePaymentRow(DbReader dbReader)
            {
            Globals.dbg.PushProc("UpdatePriorReservePaymentRow");
            Globals.objPCL50.PMSPCL50TransSeq = Convert.ToInt32(dbReader.GetValue("TRANSSEQ")) + 1;

            if (!Globals.bStatusChangeTrans)
                {
                Globals.objPCL50.PMSPCL50RecStatus = Convert.ToString(dbReader.GetValue("RECSTATUS"));
                }

            if (Globals.bProcessingReserve)
                {
                UpdateForReserveTypes(dbReader);
                }
            else
                {
                UpdateForPaymentTypes(dbReader);
                }
            Globals.dbg.PopProc();
            }

        private static void UpdateForReserveTypes(DbReader dbReader)
            {
            Globals.dbg.PushProc("UpdateForReserveTypes");
            //Globals.objPCL50.PMSPCL50TotPayment = Convert.ToSingle(dbReader.GetValue("TOTPAYMENT"));
            Globals.objPCL50.PMSPCL50TotPayment = TotPayment;
            Globals.dbg.PopProc();
            }

        private static void UpdateForPaymentTypes(DbReader dbReader)
            {
            Globals.dbg.PushProc("UpdateForPaymentTypes");

            if (Globals.sResvType == "R")
                {
                //Globals.objPCL50.PMSPCL50TotPayment = Convert.ToSingle(dbReader.GetValue("TOTPAYMENT")) + Globals.objPCL50.PMSPCL50ResvPayAmt;
                Globals.objPCL50.PMSPCL50TotPayment = TotPayment + Globals.objPCL50.PMSPCL50ResvPayAmt;
                }
            else
                {
                //Globals.objPCL50.PMSPCL50TotPayment = Convert.ToSingle(dbReader.GetValue("TOTPAYMENT")) - Globals.objPCL50.PMSPCL50ResvPayAmt;
                Globals.objPCL50.PMSPCL50TotPayment = TotPayment - Globals.objPCL50.PMSPCL50ResvPayAmt;
                }

            switch (Globals.objPCL50.PMSPCL50ClaimTrans)
                {
                case "FF":
                case "FP":
                case "SP":
                    Globals.objPCL50.PMSPCL50CurrResv = 0;
                    break;
                case "PO":
                    //Globals.objPCL50.PMSPCL50CurrResv = Convert.ToSingle(dbReader.GetValue("CURRRESV"));
                    Globals.objPCL50.PMSPCL50CurrResv = CurrResv;
                    break;
                default:
                    //Globals.objPCL50.PMSPCL50CurrResv = Convert.ToSingle(dbReader.GetValue("CURRRESV")) + Globals.objPCL50.PMSPCL50ResvPayAmt;
                    Globals.objPCL50.PMSPCL50CurrResv = CurrResv + Globals.objPCL50.PMSPCL50ResvPayAmt;
                    break;
                }

            if (Globals.sResvType != "R")
                {
                if (Globals.objPCL50.PMSPCL50CurrResv < 0)
                    Globals.objPCL50.PMSPCL50CurrResv = 0;
                }
            else
                {
                if (Globals.objPCL50.PMSPCL50CurrResv > 0)
                    Globals.objPCL50.PMSPCL50CurrResv = 0;
                }

            Globals.dbg.PopProc();
            }

        public static void AddNewDraft()
            {
            object recordsAffected;
            string sSql = null;
            long hTransSeq = 0;
            DbWriter obWriter = null;
            Globals.dbg.PushProc("AddNewDraft");
            try
                {

                obWriter = DbFactory.GetDbWriter(Globals.dbConnection);
                if (Globals.bPaymentOffset == true)
                    {
                    obWriter.Tables.Add("DRFTFILE");
                    obWriter.Fields.Add("PAYSTATUS", "V");
                    obWriter.Where.Add("ITEMNO = '" + Globals.objDRFT.DRFTItemNo + "' " + "   AND BANKCODE = '" + StringUtils.Left(Globals.objDRFT.DRFTBankCode + " ", 4) + "'" + "   AND CLAIM = '" + Globals.objDRFT.DRFTClaim + "'" + "   AND CLMTSEQ = " + Globals.objDRFT.DRFTClmtSeq + "   AND POLCOVSEQ = " + Globals.objDRFT.DRFTPolCovSeq + "   AND RESVNO = " + Globals.objDRFT.DRFTResvNo + "   AND TRANSSEQ IN (" + " SELECT TRANSSEQ FROM PMSPAP00" + " WHERE MST_ITEM_NO ='" + Globals.objDRFT.DRFTItemNo + "' " + "  AND CLAIMNO = '" + Globals.objDRFT.DRFTClaim + "' " + "  AND CLMTSEQ = " + Globals.objDRFT.DRFTClmtSeq + "  AND POLCOVSEQ = " + Globals.objDRFT.DRFTPolCovSeq + " AND RESVNO = " + Globals.objDRFT.DRFTResvNo + "  AND MST_PAY_STATUS <> 'V')");
                    try
                        {
                        Execute(obWriter);
                        obWriter.Reset(true);
                        }
                    catch (Exception ex)
                        {

                        throw ex;
                        }

                    obWriter.Tables.Add("PMSPCL50");
                    obWriter.Fields.Add("OFFSETSW", "Y");
                    obWriter.Where.Add("CLAIM = '" + Globals.objDRFT.DRFTClaim + "' " + " AND CLMTSEQ = " + Globals.objDRFT.DRFTClmtSeq + " AND POLCOVSEQ = " + Globals.objDRFT.DRFTPolCovSeq + " AND RESVNO = " + Globals.objDRFT.DRFTResvNo + " AND TRANSSEQ IN (" + " SELECT TRANSSEQ FROM PMSPAP00" + "  WHERE MST_ITEM_NO ='" + Globals.objDRFT.DRFTItemNo + "' " + " AND CLAIMNO = '" + Globals.objDRFT.DRFTClaim + "' " + " AND CLMTSEQ = " + Globals.objDRFT.DRFTClmtSeq + " AND POLCOVSEQ = " + Globals.objDRFT.DRFTPolCovSeq + " AND RESVNO = " + Globals.objDRFT.DRFTResvNo + " AND MST_PAY_STATUS <> 'V')");

                    try
                        {
                        Execute(obWriter);
                        obWriter.Reset(true);
                        }
                    catch (Exception ex)
                        {

                        throw ex;

                        }
                    Globals.objAP00.PMSPAP00LastChangedDate = Globals.objUtils.ConvertDateToPoint(Globals.sYYYYMMDDNow);
                    Globals.objAP00.PMSPAP00LastChangedTime = StringUtils.Left(Globals.sHHNNSS000000Now, 8);
                    Globals.objAP00.PMSPAP00CashEntryDate = Globals.objAP00.PMSPAP00LastChangedDate;
                    Globals.objAP00.PMSPAP00CashEntryTime = Globals.objAP00.PMSPAP00LastChangedTime;
                    obWriter.Tables.Add("PMSPAP00");
                    obWriter.Fields.Add("MST_CHECK_RECONCILED", "2");
                    obWriter.Fields.Add("MST_PAY_STATUS", "V");
                    obWriter.Fields.Add("MST_LAST_CHANGED_DATE", StringUtils.Left(Globals.objAP00.PMSPAP00LastChangedDate, 7));
                    obWriter.Fields.Add("MST_LAST_CHANGED_TIME", StringUtils.Left(Globals.objAP00.PMSPAP00LastChangedTime, 8));
                    obWriter.Fields.Add("CASH_ENTRY_DATE", StringUtils.Left(Globals.objAP00.PMSPAP00LastChangedDate, 7));
                    obWriter.Fields.Add("CASH_ENTRY_TIME", StringUtils.Left(Globals.objAP00.PMSPAP00LastChangedTime, 8));
                    obWriter.Where.Add("MST_ITEM_NO ='" + Globals.objDRFT.DRFTItemNo + "' " + "   AND CLAIMNO = '" + Globals.objDRFT.DRFTClaim + "' " + "   AND CLMTSEQ = " + Globals.objDRFT.DRFTClmtSeq + "   AND POLCOVSEQ = " + Globals.objDRFT.DRFTPolCovSeq + "   AND RESVNO = " + Globals.objDRFT.DRFTResvNo + "   AND MST_PAY_STATUS <> 'V'");

                    try
                        {
                        Execute(obWriter);
                        obWriter.Reset(true);
                        }
                    catch (Exception ex)
                        {

                        throw ex;

                        }

                    if (Globals.cPointRelease >= 9)
                        {

                        Globals.objDRFT.DRFTCRTDate = Globals.objUtils.ConvertDateToPoint(Globals.sYYYYMMDDNow);
                        Globals.objDRFT.DRFTCRTTime = Globals.sHHNNSS000000Now;
                        //Globals.objDRFT.DRFTPayStatus = "";
                        UpdateDraftFileRow(true);
                        }
                    return;
                    }

                ClearDictionaryObject();
                strSQL.AppendFormat("SELECT TRANSSEQ FROM DRFTFILE WHERE CLAIM = {0}  AND CLMTSEQ = {1}   AND POLCOVSEQ= {2} AND RESVNO= {3}  AND TRANSSEQ = {4} AND NOT PAYSTATUS IN ('R', 'H')", "~CLAIM~", "~CLMTSEQ~", "~POLCOVSEQ~", "~RESVNO~", "~TRANSSEQ~");
                dictParams.Add("CLAIM", Globals.objDRFT.DRFTClaim);
                dictParams.Add("CLMTSEQ", Globals.objDRFT.DRFTClmtSeq);
                dictParams.Add("POLCOVSEQ", Globals.objDRFT.DRFTPolCovSeq);
                dictParams.Add("RESVNO", Globals.objDRFT.DRFTResvNo);
                dictParams.Add("TRANSSEQ", Globals.objDRFT.DRFTTransSeq);

                string sResult = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr,strSQL.ToString(),dictParams));
                //string sResult = RC.Conversion.ConvertObjToStr(ExecuteScalar("SELECT TRANSSEQ FROM DRFTFILE WHERE CLAIM = '" + Globals.objDRFT.DRFTClaim + "'  AND CLMTSEQ=" + Globals.objDRFT.DRFTClmtSeq + "   AND POLCOVSEQ= " + Globals.objDRFT.DRFTPolCovSeq + " AND RESVNO= " + Globals.objDRFT.DRFTResvNo + "  AND TRANSSEQ = " + Globals.objDRFT.DRFTTransSeq + " AND NOT PAYSTATUS IN ('R', 'H')"));

                if (string.IsNullOrEmpty(sResult))
                    {

                    Globals.objDRFT.DRFTCRTDate = Globals.objUtils.ConvertDateToPoint(Globals.sYYYYMMDDNow);
                    Globals.objDRFT.DRFTCRTTime = Globals.sHHNNSS000000Now;
                    UpdateDraftFileRow(true);

                    }
                else if (Globals.bPaymentOffset == true)
                    {
                    hTransSeq = Convert.ToInt64(sResult);
                    sSql = "UPDATE DRFTFILE SET PAYSTATUS = 'V' " + " WHERE ITEMNO='" + StringUtils.Left(Globals.objDRFT.DRFTItemNo + " ", 4) + "' " + "   AND BANKCODE='" + Globals.objDRFT.DRFTBankCode + "'" + "   AND CLAIM='" + Globals.objDRFT.DRFTClaim + "'" + "   AND CLMTSEQ=" + Globals.objDRFT.DRFTClmtSeq + "   AND POLCOVSEQ=" + Globals.objDRFT.DRFTPolCovSeq + "   AND RESVNO=" + Globals.objDRFT.DRFTResvNo;
                    obWriter.Tables.Add("DRFTFILE");
                    obWriter.Fields.Add("PAYSTATUS", "V");
                    obWriter.Where.Add("ITEMNO='" + Globals.objDRFT.DRFTItemNo + "' " + "   AND BANKCODE='" + Globals.objDRFT.DRFTBankCode + "'" + "   AND CLAIM='" + Globals.objDRFT.DRFTClaim + "'" + "   AND CLMTSEQ=" + Globals.objDRFT.DRFTClmtSeq + "   AND POLCOVSEQ=" + Globals.objDRFT.DRFTPolCovSeq + "   AND RESVNO=" + Globals.objDRFT.DRFTResvNo);

                    try
                        {
                        Execute(obWriter);
                        obWriter.Reset(true);
                        }
                    catch (Exception ex)
                        {
                        throw ex;

                        }
                    obWriter.Tables.Add("PMSPCL50");
                    obWriter.Fields.Add("OFFSETSW", "Y");
                    obWriter.Where.Add("CLAIM='" + Globals.objDRFT.DRFTClaim + "' " + "   AND CLMTSEQ=" + Globals.objDRFT.DRFTClmtSeq + "   AND POLCOVSEQ=" + Globals.objDRFT.DRFTPolCovSeq + "   AND RESVNO=" + Globals.objDRFT.DRFTResvNo + "   AND TRANSSEQ=" + hTransSeq);
                    try
                        {
                        Execute(obWriter);
                        obWriter.Reset(true);
                        }
                    catch (Exception ex)
                        {
                        throw ex;
                        }

                    if (Globals.cPointRelease >= 9)
                        {

                        Globals.objDRFT.DRFTCRTDate = Globals.objUtils.ConvertDateToPoint(Globals.sYYYYMMDDNow);
                        Globals.objDRFT.DRFTCRTTime = Globals.sHHNNSS000000Now;
                        Globals.objDRFT.DRFTPayStatus = "";
                        UpdateDraftFileRow(true);
                        }

                    }
                else
                    {

                    try
                        {
                        UpdateDraftFileRow(false);
                        }
                    catch (Exception ex)
                        {
                        throw ex;
                        }
                    }

                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                if (obWriter != null)
                    {
                        obWriter = null;
                    }
                Globals.dbg.PopProc();
                }
            }

        private static void UpdateDraftFileRow(bool bIsNew)
            {
            Globals.dbg.PushProc("UpdateDraftFileRow");
            Globals.objWriter.Tables.Add("DRFTFILE");

            if (bIsNew)
                {
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTSymbol))
                    {
                    if (Globals.cPointRelease >= Globals.POINT_INJ_GLBL)
                        {
                        Globals.objWriter.Fields.Add("SYMBOL", StringUtils.Left(Globals.objDRFT.DRFTSymbol, 3));
                        }
                    else
                        {
                        Globals.objWriter.Fields.Add("SYMBOL", StringUtils.Left(Globals.objDRFT.DRFTSymbol + " ", 3));
                        }

                    }
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPolicyNo))
                    Globals.objWriter.Fields.Add("POLICYNO", StringUtils.Left(Globals.objDRFT.DRFTPolicyNo + " ", 7));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTModule))
                    Globals.objWriter.Fields.Add("MODULE", StringUtils.Left(Globals.objDRFT.DRFTModule + " ", 2));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTMasterCo))
                    Globals.objWriter.Fields.Add("MASTERCO", StringUtils.Left(Globals.objDRFT.DRFTMasterCo + " ", 2));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTLocation))
                    Globals.objWriter.Fields.Add("LOCATION", StringUtils.Left(Globals.objDRFT.DRFTLocation + " ", 2));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTClaim))
                    Globals.objWriter.Fields.Add("CLAIM", StringUtils.Left(Globals.objDRFT.DRFTClaim + " ", 12));
                Globals.objWriter.Fields.Add("CLMTSEQ", Globals.objDRFT.DRFTClmtSeq);
                Globals.objWriter.Fields.Add("POLCOVSEQ", Globals.objDRFT.DRFTPolCovSeq);
                Globals.objWriter.Fields.Add("RESVNO", Globals.objDRFT.DRFTResvNo);
                Globals.objWriter.Fields.Add("TRANSSEQ", Globals.objDRFT.DRFTTransSeq);
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTTransCode))
                    Globals.objWriter.Fields.Add("TRANSCODE", StringUtils.Left(Globals.objDRFT.DRFTTransCode + " ", 2));
                Globals.objWriter.Fields.Add("DRFTPRTDTE", Globals.objDRFT.DRFTDrftPrtDte);

                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTSERDTEFROM))
                    Globals.objWriter.Fields.Add("SERDTEFROM", Globals.objDRFT.DRFTSERDTEFROM);
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTSERDTETO))
                    Globals.objWriter.Fields.Add("SERDTETO", Globals.objDRFT.DRFTSERDTETO);
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTInvoiceNo1))
                    Globals.objWriter.Fields.Add("INVOICENO1", Globals.objDRFT.DRFTInvoiceNo1);

                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTBankCode))
                    Globals.objWriter.Fields.Add("BANKCODE", StringUtils.Left(Globals.objDRFT.DRFTBankCode + " ", 4));
                Globals.objWriter.Fields.Add("DRFTAMT", Globals.objDRFT.DRFTDrftAmt);
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTDrftNumber))
                    Globals.objWriter.Fields.Add("DRFTNUMBER", StringUtils.Right(" " + Globals.objDRFT.DRFTDrftNumber, 7));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPPCode))
                    Globals.objWriter.Fields.Add("PPCODE", StringUtils.Left(Globals.objDRFT.DRFTPPCode + " ", 2));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayePhrase))
                    Globals.objWriter.Fields.Add("PAYEPHRASE", StringUtils.Left(Globals.objDRFT.DRFTPayePhrase + " ", 75));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayToCode1))
                    Globals.objWriter.Fields.Add("PAYTOCODE1", StringUtils.Left(Globals.objDRFT.DRFTPayToCode1 + " ", 1));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeNo1))
                    Globals.objWriter.Fields.Add("PAYEENO1", StringUtils.Left(Globals.objDRFT.DRFTPayeeNo1 + " ", 5));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeName1))
                    Globals.objWriter.Fields.Add("PAYEENAME1", StringUtils.Left(Globals.objDRFT.DRFTPayeeName1 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeAdd11))
                    Globals.objWriter.Fields.Add("PAYEEADD11", StringUtils.Left(Globals.objDRFT.DRFTPayeeAdd11 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeAdd12))
                    Globals.objWriter.Fields.Add("PAYEEADD12", StringUtils.Left(Globals.objDRFT.DRFTPayeeAdd12 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeAdd13))
                    Globals.objWriter.Fields.Add("PAYEEADD13", StringUtils.Left(Globals.objDRFT.DRFTPayeeAdd13 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeZip1))
                    Globals.objWriter.Fields.Add("PAYEEZIP1", StringUtils.Left(Globals.objDRFT.DRFTPayeeZip1 + " ", 10));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayToCode2))
                    Globals.objWriter.Fields.Add("PAYTOCODE2", StringUtils.Left(Globals.objDRFT.DRFTPayToCode2 + " ", 1));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeNo2))
                    Globals.objWriter.Fields.Add("PAYEENO2", StringUtils.Left(Globals.objDRFT.DRFTPayeeNo2 + " ", 5));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeName2))
                    Globals.objWriter.Fields.Add("PAYEENAME2", StringUtils.Left(Globals.objDRFT.DRFTPayeeName2 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeAdd21))
                    Globals.objWriter.Fields.Add("PAYEEADD21", StringUtils.Left(Globals.objDRFT.DRFTPayeeAdd21 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeAdd22))
                    Globals.objWriter.Fields.Add("PAYEEADD22", StringUtils.Left(Globals.objDRFT.DRFTPayeeAdd22 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeAdd23))
                    Globals.objWriter.Fields.Add("PAYEEADD23", StringUtils.Left(Globals.objDRFT.DRFTPayeeAdd23 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeZip2))
                    Globals.objWriter.Fields.Add("PAYEEZIP2", StringUtils.Left(Globals.objDRFT.DRFTPayeeZip2 + " ", 10));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayToCode3))
                    Globals.objWriter.Fields.Add("PAYTOCODE3", StringUtils.Left(Globals.objDRFT.DRFTPayToCode3 + " ", 1));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeNo3))
                    Globals.objWriter.Fields.Add("PAYEENO3", StringUtils.Left(Globals.objDRFT.DRFTPayeeNo3 + " ", 5));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeName3))
                    Globals.objWriter.Fields.Add("PAYEENAME3", StringUtils.Left(Globals.objDRFT.DRFTPayeeName3 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeAdd31))
                    Globals.objWriter.Fields.Add("PAYEEADD31", StringUtils.Left(Globals.objDRFT.DRFTPayeeAdd31 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeAdd32))
                    Globals.objWriter.Fields.Add("PAYEEADD32", StringUtils.Left(Globals.objDRFT.DRFTPayeeAdd32 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeAdd33))
                    Globals.objWriter.Fields.Add("PAYEEADD33", StringUtils.Left(Globals.objDRFT.DRFTPayeeAdd33 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayeeZip3))
                    Globals.objWriter.Fields.Add("PAYEEZIP3", StringUtils.Left(Globals.objDRFT.DRFTPayeeZip3 + " ", 10));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTMailToCode))
                    Globals.objWriter.Fields.Add("MAILTOCODE", StringUtils.Left(Globals.objDRFT.DRFTMailToCode + " ", 1));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTMailToNo))
                    Globals.objWriter.Fields.Add("MAILTONO", StringUtils.Left(Globals.objDRFT.DRFTMailToNo + " ", 5));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTMailToName))
                    Globals.objWriter.Fields.Add("MAILTONAME", StringUtils.Left(Globals.objDRFT.DRFTMailToName + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTMailToAdd1))
                    Globals.objWriter.Fields.Add("MAILTOADD1", StringUtils.Left(Globals.objDRFT.DRFTMailToAdd1 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTMailToAdd2))
                    Globals.objWriter.Fields.Add("MAILTOADD2", StringUtils.Left(Globals.objDRFT.DRFTMailToAdd2 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTMailToAdd3))
                    Globals.objWriter.Fields.Add("MAILTOADD3", StringUtils.Left(Globals.objDRFT.DRFTMailToAdd3 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTMailToZip))
                    Globals.objWriter.Fields.Add("MAILTOZIP", StringUtils.Left(Globals.objDRFT.DRFTMailToZip + " ", 10));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTMemoCode))
                    Globals.objWriter.Fields.Add("MEMOCODE", StringUtils.Left(Globals.objDRFT.DRFTMemoCode + " ", 2));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTMemo1))
                    Globals.objWriter.Fields.Add("MEMO1", StringUtils.Left(Globals.objDRFT.DRFTMemo1 + " ", 72));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTMemo2))
                    Globals.objWriter.Fields.Add("MEMO2", StringUtils.Left(Globals.objDRFT.DRFTMemo2 + " ", 72));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTNote1))
                    Globals.objWriter.Fields.Add("NOTE1", StringUtils.Left(Globals.objDRFT.DRFTNote1 + " ", 72));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTNote2))
                    Globals.objWriter.Fields.Add("NOTE2", StringUtils.Left(Globals.objDRFT.DRFTNote2 + " ", 72));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTAPHistory))
                    Globals.objWriter.Fields.Add("APHISTORY", StringUtils.Left(Globals.objDRFT.DRFTAPHistory + " ", 1));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayType))
                    Globals.objWriter.Fields.Add("PAYTYPE", StringUtils.Left(Globals.objDRFT.DRFTPayType + " ", 1));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTPayStatus))
                    Globals.objWriter.Fields.Add("PAYSTATUS", StringUtils.Left(Globals.objDRFT.DRFTPayStatus + " ", 1));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTItemNo))
                    Globals.objWriter.Fields.Add("ITEMNO", StringUtils.Right(" " + Globals.objDRFT.DRFTItemNo, 7));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTCheckNo))
                    Globals.objWriter.Fields.Add("CHECKNO", StringUtils.Right(" " + Globals.objDRFT.DRFTCheckNo, 7));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTCRTDate))
                    Globals.objWriter.Fields.Add("CRTDATE", StringUtils.Left(Globals.objDRFT.DRFTCRTDate + " ", 7));
                if (!string.IsNullOrEmpty(Globals.objDRFT.DRFTCRTTime))
                    Globals.objWriter.Fields.Add("CRTTIME", StringUtils.Left(Globals.objDRFT.DRFTCRTTime + " ", 12));
                }
            else
                {
                Globals.objWriter.Where.Add(" CLAIM = '" + Globals.objDRFT.DRFTClaim + "'  AND CLMTSEQ=" + Globals.objDRFT.DRFTClmtSeq + "   AND POLCOVSEQ= " + Globals.objDRFT.DRFTPolCovSeq + " AND RESVNO= " + Globals.objDRFT.DRFTResvNo + "  AND TRANSSEQ = " + Globals.objDRFT.DRFTTransSeq + " AND NOT PAYSTATUS IN ('R', 'H')");
                Globals.objWriter.Fields.Add("CLAIM", Globals.objDRFT.DRFTClaim);
                Globals.objWriter.Fields.Add("CLMTSEQ", Globals.objDRFT.DRFTClmtSeq);
                Globals.objWriter.Fields.Add("POLCOVSEQ", Globals.objDRFT.DRFTPolCovSeq);
                Globals.objWriter.Fields.Add("RESVNO", Globals.objDRFT.DRFTResvNo);
                Globals.objWriter.Fields.Add("TRANSSEQ", Globals.objDRFT.DRFTTransSeq);
                Globals.objWriter.Fields.Add("SYMBOL", Globals.objDRFT.DRFTSymbol);
                Globals.objWriter.Fields.Add("POLICYNO", Globals.objDRFT.DRFTPolicyNo);
                Globals.objWriter.Fields.Add("MODULE", Globals.objDRFT.DRFTModule);
                Globals.objWriter.Fields.Add("MASTERCO", Globals.objDRFT.DRFTMasterCo);
                Globals.objWriter.Fields.Add("LOCATION", Globals.objDRFT.DRFTLocation);
                Globals.objWriter.Fields.Add("BANKCODE", StringUtils.Left(Globals.objDRFT.DRFTBankCode + " ", 4));
                Globals.objWriter.Fields.Add("ITEMNO", StringUtils.Left(Globals.objDRFT.DRFTItemNo + " ", 7));
                }

            try
                {
                Execute(Globals.objWriter);

                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.objWriter.Reset(true);
                Globals.dbg.PopProc();
                }
            }

        public static void LocateCheckFile()
            {
            Globals.dbg.PushProc("LocateCheckFile");
            //Globals.objLogWriter.Write(" LocateCheckFile method start " + Environment.NewLine); //commented the log statement, not required now.
            try
                {
                    if (Globals.bPaymentOffset == true)
                    {
                        //Globals.objLogWriter.Write(" Return from LocateCheckFile method " + Environment.NewLine); //commented the log statement, not required now.
                        return;
                    }

                ClearDictionaryObject();
                strSQL.AppendFormat("SELECT CLAIMNO FROM PMSPAP00 WHERE MST_PAYTYPE= '2' AND CLAIMNO = {0} AND CLMTSEQ = {1} AND POLCOVSEQ = {2} AND RESVNO = {3} AND TRANSSEQ = {4}", "~CLAIMNO~", "~CLMTSEQ~", "~POLCOVSEQ~", "~RESVNO~", "~TRANSSEQ~");
                dictParams.Add("CLAIMNO", StringUtils.Left(Globals.objAP00.PMSPAP00Claim, 12));
                dictParams.Add("CLMTSEQ", Globals.objAP00.PMSPAP00ClmtSeq);
                dictParams.Add("POLCOVSEQ", Globals.objAP00.PMSPAP00PolCovSeq);
                dictParams.Add("RESVNO", Globals.objAP00.PMSPAP00ResvNo);
                dictParams.Add("TRANSSEQ", Globals.objAP00.PMSPAP00TransSeq);

                string sResult = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(),dictParams));
                //string sResult = RC.Conversion.ConvertObjToStr(ExecuteScalar("SELECT CLAIMNO FROM PMSPAP00 WHERE MST_PAYTYPE= '2' AND CLAIMNO = '" + StringUtils.Left(Globals.objAP00.PMSPAP00Claim, 12) + "' AND CLMTSEQ = '" + Convert.ToString(Globals.objAP00.PMSPAP00ClmtSeq) + "' AND POLCOVSEQ = '" + Convert.ToString(Globals.objAP00.PMSPAP00PolCovSeq) + "'AND RESVNO = '" + Convert.ToString(Globals.objAP00.PMSPAP00ResvNo) + "' AND TRANSSEQ = '" + Convert.ToString(Globals.objAP00.PMSPAP00TransSeq) + "'"));

                if (string.IsNullOrEmpty(sResult))
                    {

                    Globals.objAP00.PMSPAP00CreateDate = Globals.objUtils.ConvertDateToPoint(Globals.sYYYYMMDDNow);
                    Globals.objAP00.PMSPAP00CreateTime = Globals.sHHNNSS000000Now;
                    Globals.objAP00.PMSPAP00LastChangedDate = Globals.objUtils.ConvertDateToPoint(Globals.sYYYYMMDDNow);
                    Globals.objAP00.PMSPAP00LastChangedTime = StringUtils.Left(Globals.sHHNNSS000000Now, 8);
                    Globals.objAP00.PMSPAP00CashEntryDate = Globals.objAP00.PMSPAP00LastChangedDate;
                    Globals.objAP00.PMSPAP00CashEntryTime = Globals.objAP00.PMSPAP00LastChangedTime;
                    //Globals.objLogWriter.Write("No existing record in PMSPAP00, Calling UpdateCheckFileRow method for insert " + Environment.NewLine); //commented the log statement, not required now.
                    UpdateCheckFileRow(true);

                    }
                else
                    {
                        //Globals.objLogWriter.Write("Existing record found in PMSPAP00, Calling UpdateCheckFileRow method for update " + Environment.NewLine); //commented the log statement , not required now
                    UpdateCheckFileRow(false);
                    }
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.dbg.PopProc();
                }
            }

        private static void UpdateCheckFileRow(bool bIsNew)
            {
            Globals.dbg.PushProc("UpdateCheckFileRow");
            //Globals.objLogWriter.Write(" UpdateCheckFileRow method start " + Environment.NewLine); //commented the log statement, not required now
            Globals.objWriter.Tables.Add("PMSPAP00");
            if (bIsNew)
                {
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00PayType))
                    Globals.objWriter.Fields.Add("MST_PAYTYPE", StringUtils.Left(Globals.objAP00.PMSPAP00PayType + " ", 1));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00ItemNo))
                    Globals.objWriter.Fields.Add("MST_ITEM_NO", StringUtils.Right(" " + Globals.objAP00.PMSPAP00ItemNo, 7));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00PayStatus))
                    Globals.objWriter.Fields.Add("MST_PAY_STATUS", StringUtils.Left(Globals.objAP00.PMSPAP00PayStatus + " ", 1));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00Loc))
                    Globals.objWriter.Fields.Add("MST_LOC", StringUtils.Left(Globals.objAP00.PMSPAP00Loc + " ", 2));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00Mco))
                    Globals.objWriter.Fields.Add("MST_MCO", StringUtils.Left(Globals.objAP00.PMSPAP00Mco + " ", 2));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00Sym))
                    Globals.objWriter.Fields.Add("MST_SYM", StringUtils.Left(Globals.objAP00.PMSPAP00Sym + " ", 3));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00PolNo))
                    Globals.objWriter.Fields.Add("MST_POLNO", StringUtils.Left(Globals.objAP00.PMSPAP00PolNo + " ", 7));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00Mod))
                    Globals.objWriter.Fields.Add("MST_MOD", StringUtils.Left(Globals.objAP00.PMSPAP00Mod + " ", 2));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00Agency))
                    Globals.objWriter.Fields.Add("MST_AGENCY", StringUtils.Left(Globals.objAP00.PMSPAP00Agency + " ", 7));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00BankCode))
                    Globals.objWriter.Fields.Add("MST_BANK_CODE", StringUtils.Left(Globals.objAP00.PMSPAP00BankCode + " ", 4));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00CheckNo))
                    Globals.objWriter.Fields.Add("MST_CHECK_NO", StringUtils.Right(" " + Globals.objAP00.PMSPAP00CheckNo, 7));
                Globals.objWriter.Fields.Add("MST_CHECK_AMT", Globals.objAP00.PMSPAP00CheckAmt);
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00PayeeName))
                    Globals.objWriter.Fields.Add("MST_PAYEE_NAME", StringUtils.Left(Globals.objAP00.PMSPAP00PayeeName + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00Payee2))
                    Globals.objWriter.Fields.Add("MST_PAYEE2", StringUtils.Left(Globals.objAP00.PMSPAP00Payee2 + " ", 30));
                //Globals.objLogWriter.Write(" Value of MST_PAYEE2 = " + Globals.objAP00.PMSPAP00Payee2 + Environment.NewLine); //commented the log statement, not required now
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00MailLine3))
                    Globals.objWriter.Fields.Add("MST_MAIL_LINE3", StringUtils.Left(Globals.objAP00.PMSPAP00MailLine3 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00MailLine4))
                    Globals.objWriter.Fields.Add("MST_MAIL_LINE4", StringUtils.Left(Globals.objAP00.PMSPAP00MailLine4 + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00ZipCode))
                    Globals.objWriter.Fields.Add("MST_ZIP_CODE", StringUtils.Left(Globals.objAP00.PMSPAP00ZipCode + " ", 10));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00CheckDate))
                    Globals.objWriter.Fields.Add("MST_CHECK_DATE", StringUtils.Left(Globals.objAP00.PMSPAP00CheckDate + " ", 7));
                Globals.objWriter.Fields.Add("MST_CHECK_RECONCILED", Globals.objAP00.PMSPAP00CheckReconciled);
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00CreateDate))
                    Globals.objWriter.Fields.Add("MST_CREATE_DATE", StringUtils.Left(Globals.objAP00.PMSPAP00CreateDate + " ", 7));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00CreateTime))
                    Globals.objWriter.Fields.Add("MST_CREATE_TIME", StringUtils.Left(Globals.objAP00.PMSPAP00CreateTime + " ", 12));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00LastChangedDate))
                    Globals.objWriter.Fields.Add("MST_LAST_CHANGED_DATE", StringUtils.Left(Globals.objAP00.PMSPAP00LastChangedDate + " ", 7));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00LastChangedTime))
                    Globals.objWriter.Fields.Add("MST_LAST_CHANGED_TIME", StringUtils.Left(Globals.objAP00.PMSPAP00LastChangedTime + " ", 8));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00EntrySequence))
                    Globals.objWriter.Fields.Add("MST_ENTRY_SEQUENCE", StringUtils.Left(Globals.objAP00.PMSPAP00EntrySequence + " ", 2));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00CashEntryDate))
                    Globals.objWriter.Fields.Add("CASH_ENTRY_DATE", StringUtils.Left(Globals.objAP00.PMSPAP00CashEntryDate + " ", 7));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00CashEntryTime))
                    Globals.objWriter.Fields.Add("CASH_ENTRY_TIME", StringUtils.Left(Globals.objAP00.PMSPAP00CashEntryTime + " ", 8));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00CashEntrySequence))
                    Globals.objWriter.Fields.Add("CASH_ENTRY_SEQUENCE", StringUtils.Left(Globals.objAP00.PMSPAP00CashEntrySequence + " ", 2));
                if (!string.IsNullOrEmpty(Globals.objAP00.PMSPAP00Claim))

                    Globals.objWriter.Fields.Add("CLAIMNO", StringUtils.Left(Globals.objAP00.PMSPAP00Claim + " ", 12));
                Globals.objWriter.Fields.Add("CLMTSEQ", Globals.objAP00.PMSPAP00ClmtSeq);
                Globals.objWriter.Fields.Add("POLCOVSEQ", Globals.objAP00.PMSPAP00PolCovSeq);
                Globals.objWriter.Fields.Add("RESVNO", Globals.objAP00.PMSPAP00ResvNo);
                Globals.objWriter.Fields.Add("TRANSSEQ", Globals.objAP00.PMSPAP00TransSeq);
                }
            else
                {
                Globals.objWriter.Where.Add(" MST_PAYTYPE= '2' AND CLAIMNO = '" + StringUtils.Left(Globals.objAP00.PMSPAP00Claim, 12) + "' AND CLMTSEQ = '" + Convert.ToString(Globals.objAP00.PMSPAP00ClmtSeq) + "' AND POLCOVSEQ = '" + Convert.ToString(Globals.objAP00.PMSPAP00PolCovSeq) + "'AND RESVNO = '" + Convert.ToString(Globals.objAP00.PMSPAP00ResvNo) + "' AND TRANSSEQ = '" + Convert.ToString(Globals.objAP00.PMSPAP00TransSeq) + "'");
                Globals.objWriter.Fields.Add("MST_LOC", StringUtils.Left(Globals.objAP00.PMSPAP00Loc + " ", 2));
                Globals.objWriter.Fields.Add("MST_MCO", StringUtils.Left(Globals.objAP00.PMSPAP00Mco + " ", 2));
                Globals.objWriter.Fields.Add("MST_SYM", StringUtils.Left(Globals.objAP00.PMSPAP00Sym + " ", 3));
                Globals.objWriter.Fields.Add("MST_POLNO", StringUtils.Left(Globals.objAP00.PMSPAP00PolNo + " ", 7));
                Globals.objWriter.Fields.Add("MST_MOD", StringUtils.Left(Globals.objAP00.PMSPAP00Mod + " ", 2));
                Globals.objWriter.Fields.Add("MST_ITEM_NO", StringUtils.Left(Globals.objAP00.PMSPAP00ItemNo + " ", 7));
                Globals.objWriter.Fields.Add("MST_BANK_CODE", StringUtils.Left(Globals.objAP00.PMSPAP00BankCode + " ", 4));
                }
            try
                {
                Execute(Globals.objWriter);
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.objWriter.Reset(true);
                Globals.dbg.PopProc();
                //Globals.objLogWriter.Write(" UpdateCheckFileRow method end " + Environment.NewLine); //commented the log statemnet, not required now
                }


            }

        public static void UpdateVendorFile(int iPayeeNo)
            {
            Globals.dbg.PushProc("UpdateVendorFile");
            ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlVndr, ScriptingFunctions.conScpEvtStart);
            Parameters objParams = (Parameters)Globals.GetParams();
            switch (iPayeeNo)
                {
                case 1:
                        {
                        Globals.objAD00.PMSPAD00Name = Globals.objDRFT.DRFTPayeeName1;
                        Globals.objAD00.PMSPAD00Address = StringUtils.Trim(Globals.objDRFT.DRFTPayeeAdd11 + " " + Globals.objDRFT.DRFTPayeeAdd12);
                        Globals.objAD00.PMSPAD00CityState = Globals.objDRFT.DRFTPayeeAdd13;
                        Globals.objAD00.PMSPAD00ZipCode = Globals.objDRFT.DRFTPayeeZip1;
                        }

                    break;
                case 2:
                        {
                        Globals.objAD00.PMSPAD00Name = Globals.objDRFT.DRFTPayeeName2;
                        Globals.objAD00.PMSPAD00Address = StringUtils.Trim(Globals.objDRFT.DRFTPayeeAdd21 + " " + Globals.objDRFT.DRFTPayeeAdd22);
                        Globals.objAD00.PMSPAD00CityState = Globals.objDRFT.DRFTPayeeAdd23;
                        Globals.objAD00.PMSPAD00ZipCode = Globals.objDRFT.DRFTPayeeZip2;
                        }

                    break;
                case 3:
                        {
                        Globals.objAD00.PMSPAD00Name = Globals.objDRFT.DRFTPayeeName3;
                        Globals.objAD00.PMSPAD00Address = StringUtils.Trim(Globals.objDRFT.DRFTPayeeAdd31 + " " + Globals.objDRFT.DRFTPayeeAdd32);
                        Globals.objAD00.PMSPAD00CityState = Globals.objDRFT.DRFTPayeeAdd33;
                        Globals.objAD00.PMSPAD00ZipCode = Globals.objDRFT.DRFTPayeeZip3;
                        }

                    break;
                }

                {
                Globals.objAD00.PMSPAD00EntityID = Globals.lEntityID;
                Globals.objAD00.PMSPAD00AdjustorNbr = Globals.strVenNumber;
                Globals.objAD00.PMSPAD00TaxIdSSN = Globals.strTaxId;
                Globals.objAD00.PMSPAD00LongTaxID = Globals.strLongTaxId;
                Globals.objAD00.PMSPAD00SSN = Globals.strTaxIdType;
                Globals.objAD00.PMSPAD00TypeAdjustor = Globals.strCategory;
                Globals.objAD00.PMSPAD00PhoneNumber = Globals.strPhone[0];
                Globals.objAD00.NumberType = objParams.Parameter(Globals.conVendorLit);
                Globals.objAD00.EntityType = Globals.strEntityType;
                }

                    {
                    if (Globals.b1099Address)
                        {
                        Globals.objPTX00.PMSPTX00Name = Globals.objDRFT.DRFTPayeeName1;
                        Globals.objPTX00.PMSPTX00Address = StringUtils.Trim(Globals.str1099Address[0] + " " + Globals.str1099Address[1]);
                        Globals.objPTX00.PMSPTX00CityState = Globals.str1099City + ", " + Globals.str1099StateCode;
                        Globals.objPTX00.PMSPTX00ZipCode = Globals.str1099PostalCd;
                        }
                    else
                        {
                        Globals.objPTX00.PMSPTX00Name = Globals.objDRFT.DRFTPayeeName1;
                        Globals.objPTX00.PMSPTX00Address = StringUtils.Trim(Globals.objDRFT.DRFTPayeeAdd11 + " " + Globals.objDRFT.DRFTPayeeAdd12);
                        Globals.objPTX00.PMSPTX00CityState = Globals.objDRFT.DRFTPayeeAdd13;
                        Globals.objPTX00.PMSPTX00ZipCode = Globals.objDRFT.DRFTPayeeZip1;
                        }
                    Globals.objPTX00.PMSPTX00TaxIdSSN = Globals.strTaxId;
                    Globals.objPTX00.PMSPTX00LongTaxID = Globals.strLongTaxId;
                    Globals.objPTX00.PMSPTX00SSN = Globals.strTaxIdType;
                    Globals.objPTX00.PMSPTX00PhoneNumber = Globals.strPhone[0];
                    }

                    LocateVendor();

                    Globals.strVenNumber = Globals.objAD00.PMSPAD00AdjustorNbr;

                    ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlVndr, ScriptingFunctions.conScpEvtEnd);
                    Globals.dbg.PopProc();
            }
        
        private static void AddNewVendor()
            {
            string sTemp = null;
            long lTemp = 0;
            try
                {
                Globals.dbg.PushProc("AddNewVendor");
                Globals.objWriter.Tables.Add("PMSPAD00");
                string sNextVal = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr,"SELECT MAX(ADNM_ADJUSTOR_NBR) From PMSPAD00"));
                if (string.IsNullOrEmpty(Globals.objAD00.PMSPAD00AdjustorNbr))
                    {
                    if (string.IsNullOrEmpty(sNextVal))
                        {
                        Globals.objAD00.PMSPAD00AdjustorNbr = "00000A";
                        }
                    }

                Globals.strVenNumber = Globals.objAD00.PMSPAD00AdjustorNbr;
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.dbg.PopProc();
                }

            }
        
        public static void LocateVendor()
            {
            bool bUpdateVenNum = false;
            PMSPAD00AdjustorMasterFile objAD = new PMSPAD00AdjustorMasterFile();
            DbReader dbReader = null;
            Globals.dbg.PushProc("LocateVendor");
            Globals.gNewVendor = false;
            bUpdateVenNum = false;
            ClearDictionaryObject();
            strSQL.AppendFormat("SELECT ADNM_ADJUSTOR_NBR  FROM PMSPAD00 WHERE ADNM_ADJUSTOR_NBR = {0}", "~ADNMADJUSTORNBR~");
            dictParams.Add("ADNMADJUSTORNBR", StringUtils.Right(" " + Globals.objAD00.PMSPAD00AdjustorNbr, 6));
            string sResult = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr,strSQL.ToString(),dictParams));
            //string sResult = Convert.ToString(ExecuteScalar("SELECT ADNM_ADJUSTOR_NBR  FROM PMSPAD00 WHERE ADNM_ADJUSTOR_NBR = '" + StringUtils.Right(" " + Globals.objAD00.PMSPAD00AdjustorNbr, 6) + "'"));

            if (string.IsNullOrEmpty(sResult))
                {
                Globals.gNewVendor = true;
                }
            else
                {
                Globals.objAD00.PMSPAD00AdjustorNbr = sResult;
                }

            if (Globals.gNewVendor && !string.IsNullOrEmpty(Globals.objAD00.PMSPAD00TaxIdSSN))
                {
                ClearDictionaryObject();
                strSQL.AppendFormat("SELECT * FROM PMSPAD00 WHERE ADNM_TANID_SSN = {0}", "~ADNMTANIDSSN~");
                dictParams.Add("ADNMTANIDSSN", Globals.objAD00.PMSPAD00TaxIdSSN);
                dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams);
                //dbReader = ExecuteReader("SELECT * FROM PMSPAD00 WHERE ADNM_TANID_SSN = '" + Globals.objAD00.PMSPAD00TaxIdSSN + "'");
                if (!dbReader.Read())
                    {
                    Globals.gNewVendor = true;
                    }
                else
                    {
                    Globals.gNewVendor = false;
                    Globals.objAD00.PMSPAD00AdjustorNbr = Convert.ToString(dbReader.GetValue("ADNM_ADJUSTOR_NBR"));
                    if (Globals.cPointRelease > 7)
                        Globals.objAD00.PMSPAD00SSN = Convert.ToString(dbReader.GetValue("ADNM_SSN"));
                    bUpdateVenNum = true;
                    }
                }

            if (Globals.gNewVendor && string.IsNullOrEmpty(Globals.objAD00.PMSPAD00TaxIdSSN))
                {

                if (dbReader != null)
                    dbReader.Dispose();

                ClearDictionaryObject();
                strSQL.AppendFormat("SELECT * FROM PMSPAD00 WHERE ADNM_NAME ={0}  AND ADNM_ADDRESS = {1} AND ADNM_CITY_STATE = {2} AND ADNM_ZIP_CODE = {3}", "~ADNMNAME~", "~ADNMADDRESS~", "~ADNMCITYSTATE~", "~ADNMZIPCODE~");
                dictParams.Add("ADNMNAME", StringUtils.Left(Globals.objAD00.PMSPAD00Name, 30));
                dictParams.Add("ADNMADDRESS", StringUtils.Left(Globals.objAD00.PMSPAD00Address, 50));
                dictParams.Add("ADNMCITYSTATE", StringUtils.Left(Globals.objAD00.PMSPAD00CityState, 49));
                dictParams.Add("ADNMZIPCODE", StringUtils.Left(Globals.objAD00.PMSPAD00ZipCode, 10));

                dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams);
                //dbReader = ExecuteReader("SELECT * FROM PMSPAD00 WHERE ADNM_NAME ='" + StringUtils.Left(Globals.objAD00.PMSPAD00Name, 30) + "'  AND ADNM_ADDRESS = '" + StringUtils.Left(Globals.objAD00.PMSPAD00Address, 50) + "' AND ADNM_CITY_STATE = '" + StringUtils.Left(Globals.objAD00.PMSPAD00CityState, 49) + "' AND ADNM_ZIP_CODE = '" + StringUtils.Left(Globals.objAD00.PMSPAD00ZipCode, 10) + "'");

                if (!dbReader.Read())
                    {
                    Globals.gNewVendor = true;
                    }
                else
                    {
                    Globals.gNewVendor = false;
                    Globals.objAD00.PMSPAD00AdjustorNbr = Convert.ToString(dbReader.GetValue("ADNM_ADJUSTOR_NBR"));
                    Globals.objAD00.PMSPAD00TaxIdSSN = Convert.ToString(dbReader.GetValue("ADNM_TANID_SSN"));
                    if (Globals.cPointRelease > 7)
                        Globals.objAD00.PMSPAD00SSN = Convert.ToString(dbReader.GetValue("ADNM_SSN"));
                    bUpdateVenNum = true;
                    }
                }

            if (Globals.gNewVendor)
                {
                AddNewVendor();
                bUpdateVenNum = true;
                sVendorActivity = "Added";
                }
            else
                {
                Globals.bNewClaimant = false;

                sVendorActivity = "Updated";
                }
            UpdateVendorRow();

            if (bUpdateVenNum)
                {
                objAD = Globals.objAD00.Clone();
                Globals.collEntityUpdates.Add(objAD);
                }

            Globals.dbg.PopProc();

            }

        private static void UpdateVendorRow()
            {
            Parameters objParams = (Parameters)Globals.GetParams();
            int i = 0;
            Globals.dbg.PushProc("UpdateVendorRow");
            try
                {
                if (objParams.Parameter(Globals.conSWUpdateVendor) == "UPDATE" || (objParams.Parameter(Globals.conSWUpdateVendor) == "ADD" && Globals.gNewVendor))
                    {
                    if (!Globals.gNewVendor)
                        {
                        Globals.objWriter.Where.Add(" ADNM_ADJUSTOR_NBR = '" + Globals.objAD00.PMSPAD00AdjustorNbr + "'");
                        }

                    if (!string.IsNullOrEmpty(Globals.objAD00.PMSPAD00AdjustorNbr))
                        Globals.objWriter.Fields.Add("ADNM_ADJUSTOR_NBR", StringUtils.Left(Globals.objAD00.PMSPAD00AdjustorNbr + " ", 6));

                    if (!string.IsNullOrEmpty(Globals.objAD00.PMSPAD00TaxIdSSN))
                        Globals.objWriter.Fields.Add("ADNM_TANID_SSN", StringUtils.Left(Globals.objAD00.PMSPAD00TaxIdSSN + " ", 11));

                    if (!string.IsNullOrEmpty(Globals.objAD00.PMSPAD00Name))
                        Globals.objWriter.Fields.Add("ADNM_NAME", StringUtils.Left(Globals.objAD00.PMSPAD00Name + " ", 30));

                    if (!string.IsNullOrEmpty(Globals.objAD00.PMSPAD00Address))
                        Globals.objWriter.Fields.Add("ADNM_ADDRESS", StringUtils.Left(Globals.objAD00.PMSPAD00Address + " ", 50));

                    if (!string.IsNullOrEmpty(Globals.objAD00.PMSPAD00CityState))
                        Globals.objWriter.Fields.Add("ADNM_CITY_STATE", StringUtils.Left(Globals.objAD00.PMSPAD00CityState + " ", 49));

                    if (!string.IsNullOrEmpty(Globals.objAD00.PMSPAD00ZipCode))
                        Globals.objWriter.Fields.Add("ADNM_ZIP_CODE", StringUtils.Left(Globals.objAD00.PMSPAD00ZipCode + " ", 10));

                    if (!string.IsNullOrEmpty(Globals.objAD00.PMSPAD00PhoneNumber))
                        Globals.objWriter.Fields.Add("ADNM_PHONE_NUMBER", StringUtils.Left(Globals.objAD00.PMSPAD00PhoneNumber + " ", 12));

                    if (!string.IsNullOrEmpty(Globals.objAD00.PMSPAD00TypeAdjustor))
                        Globals.objWriter.Fields.Add("ADNM_TYPE_ADJUSTOR", StringUtils.Left(Globals.objAD00.PMSPAD00TypeAdjustor + " ", 1));

                    if (!string.IsNullOrEmpty(Globals.objAD00.PMSPAD00ClaimsOfficeNumber))
                        Globals.objWriter.Fields.Add("ADNM_CLAIMS_OFFICE_NUMBER", StringUtils.Left(Globals.objAD00.PMSPAD00ClaimsOfficeNumber + " ", 3));

                    if (!string.IsNullOrEmpty(Globals.objAD00.PMSPAD00ClaimsOfficeNumber))
                        Globals.objWriter.Fields.Add("ADNM_CLAIMS_OFFICE", StringUtils.Left(Globals.objAD00.PMSPAD00ClaimsOfficeNumber + " ", 3));

                    if (!string.IsNullOrEmpty(Globals.objAD00.PMSPAD00StatusCode))
                        Globals.objWriter.Fields.Add("ADNM_STATUS_CODE", StringUtils.Left(Globals.objAD00.PMSPAD00StatusCode + " ", 1));

                    if (!string.IsNullOrEmpty(Globals.objAD00.PMSPAD00UserProfileCode))
                        Globals.objWriter.Fields.Add("USER_PROFIL_CODE", StringUtils.Left(Globals.objAD00.PMSPAD00UserProfileCode + " ", 10));

                    if (Globals.objAD00.PMSPAD00DateStamp != 0)
                        Globals.objWriter.Fields.Add("DATE_STAMP", Globals.objAD00.PMSPAD00DateStamp);

                    if (!string.IsNullOrEmpty(Globals.objAD00.PMSPAD00SSN))
                        Globals.objWriter.Fields.Add("ADNM_SSN", StringUtils.Left(Globals.objAD00.PMSPAD00SSN + " ", 1));

                    Execute(Globals.objWriter);


                    }


                if (Globals.cPointRelease > 7)
                    {
                    ClearDictionaryObject();
                    strSQL.AppendFormat("SELECT ADNM_TAXID_SSN FROM PMSPTX00 WHERE ADNM_TAXID_SSN= {0}", "~ADNMTAXIDSSN~");
                    dictParams.Add("ADNMTAXIDSSN", Globals.objPTX00.PMSPTX00TaxIdSSN);

                    string sResult = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(),dictParams));
                    //string sResult = Convert.ToString(ExecuteScalar("SELECT ADNM_TAXID_SSN FROM PMSPTX00 WHERE ADNM_TAXID_SSN= '" + Globals.objPTX00.PMSPTX00TaxIdSSN + "'"));

                    UpdateTaxIdFileRow(sResult);
                    }

                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.objWriter.Reset(true);
                Globals.dbg.PopProc();
                }

            }

        private static void UpdateTaxIdFileRow(string sTaxIdSSN)
            {
            Globals.dbg.PushProc("UpdateTaxIdFileRow");
            DbWriter obWriter = Riskmaster.Db.DbFactory.GetDbWriter(Globals.dbConnection);
            try
                {
                obWriter.Tables.Add("PMSPTX00");
                if (!string.IsNullOrEmpty(sTaxIdSSN))
                    {
                    obWriter.Where.Add(" ADNM_TAXID_SSN= '" + sTaxIdSSN + "'");
                    }
                if (!string.IsNullOrEmpty(Globals.objPTX00.PMSPTX00TaxIdSSN))
                    obWriter.Fields.Add("ADNM_TAXID_SSN", StringUtils.Left(Globals.objPTX00.PMSPTX00TaxIdSSN + " ", 11));
                if (!string.IsNullOrEmpty(Globals.objPTX00.PMSPTX00Name))
                    obWriter.Fields.Add("ADNM_NAME", StringUtils.Left(Globals.objPTX00.PMSPTX00Name + " ", 30));
                if (!string.IsNullOrEmpty(Globals.objPTX00.PMSPTX00Address))
                    obWriter.Fields.Add("ADNM_ADDRESS", StringUtils.Left(Globals.objPTX00.PMSPTX00Address + " ", 50));
                if (!string.IsNullOrEmpty(Globals.objPTX00.PMSPTX00CityState))
                    obWriter.Fields.Add("ADNM_CITY_STATE", StringUtils.Left(Globals.objPTX00.PMSPTX00CityState + " ", 49));
                if (!string.IsNullOrEmpty(Globals.objPTX00.PMSPTX00ZipCode))
                    obWriter.Fields.Add("ADNM_ZIP_CODE", StringUtils.Left(Globals.objPTX00.PMSPTX00ZipCode + " ", 10));
                if (!string.IsNullOrEmpty(Globals.objPTX00.PMSPTX00PhoneNumber))
                    obWriter.Fields.Add("ADNM_PHONE_NUMBER", StringUtils.Left(Globals.objPTX00.PMSPTX00PhoneNumber + " ", 12));
                if (!string.IsNullOrEmpty(Globals.objPTX00.PMSPTX00SSN))
                    obWriter.Fields.Add("SSNIND", StringUtils.Left(Globals.objPTX00.PMSPTX00SSN + " ", 1));

                Execute(obWriter);
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                if (obWriter != null)
                    {
                    obWriter.Dispose();
                    }
                Globals.dbg.PopProc();
                }

            }

        public static void LocateIndemnity()
            {
            Globals.dbg.PushProc("LocateIndemnity");
            bool bNewRec = false;
            ClearDictionaryObject();
            strSQL.AppendFormat("SELECT CLAIM FROM PMSPCL92 WHERE CLAIM ={0} AND CLMTSEQ = {1} AND POLCOVSEQ = {2} AND RESVNO = {3}", "~CLAIM~", "~CLMTSEQ~", "~POLCOVSEQ~", "~RESVNO~");
            dictParams.Add("CLAIM", Globals.sClaimNumber);
            dictParams.Add("CLMTSEQ", Globals.sCLMTSEQ);
            dictParams.Add("POLCOVSEQ", Globals.sPolCovSeq);
            dictParams.Add("RESVNO", Globals.objPCL92.PMSPCL92Resvno);

            string sResult = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr,strSQL.ToString(),dictParams));
            //string sResult = RC.Conversion.ConvertObjToStr(ExecuteScalar("SELECT CLAIM FROM PMSPCL92 WHERE CLAIM='" + Globals.sClaimNumber + "' AND CLMTSEQ= " + Globals.sCLMTSEQ + " AND POLCOVSEQ= " + Globals.sPolCovSeq + " AND RESVNO='" + Convert.ToString(Globals.objPCL92.PMSPCL92Resvno) + "'"));
            if (string.IsNullOrEmpty(sResult))
                {
                bNewRec = true;
                }
            UpdateIndemnityRow(bNewRec);
            Globals.dbg.PopProc();
            }

        private static void UpdateIndemnityRow(bool bNewRec)
            {
            string strTemp = null;
            string sSql = null;

            Globals.dbg.PushProc("UpdateIndemnityRow");

            if (Globals.objPCL92.PMSPCL92WeeklyWage == 0)
                {
                ClearDictionaryObject();
                strSQL.AppendFormat("SELECT LSCURRAWW FROM PMSPCL14 WHERE CLAIM={0} AND CLMTSEQ = {1}","~CLAIM~","~CLMTSEQ~");
                dictParams.Add("CLAIM", Globals.sClaimNumber);
                dictParams.Add("CLMTSEQ", Globals.sCLMTSEQ);

                string sResult = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr,strSQL.ToString(),dictParams));
                //string sResult = RC.Conversion.ConvertObjToStr(ExecuteScalar("SELECT LSCURRAWW FROM PMSPCL14 WHERE CLAIM='" + Globals.sClaimNumber + "' AND CLMTSEQ=" + Globals.sCLMTSEQ + ""));
                if (!string.IsNullOrEmpty(sResult))
                    {
                    Globals.objPCL92.PMSPCL92WeeklyWage = Convert.ToSingle(sResult);
                    }
                }

            Globals.objWriter.Tables.Add("PMSPCL92");
            if (!bNewRec)
                {
                Globals.objWriter.Where.Add(" CLAIM='" + Globals.sClaimNumber + "' AND CLMTSEQ= " + Globals.sCLMTSEQ + " AND POLCOVSEQ= " + Globals.sPolCovSeq + " AND RESVNO='" + Convert.ToString(Globals.objPCL92.PMSPCL92Resvno) + "'");
                }

            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92Symbol))
                {
                if (Globals.cPointRelease >= Globals.POINT_INJ_GLBL)
                    {
                    Globals.objWriter.Fields.Add("SYMBOL", StringUtils.Left(Globals.objPCL92.PMSPCL92Symbol, 3));
                    }
                else
                    {
                    Globals.objWriter.Fields.Add("SYMBOL", StringUtils.Left(Globals.objPCL92.PMSPCL92Symbol + " ", 3));
                    }
                }
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PolicyNo))
                Globals.objWriter.Fields.Add("POLICYNO", StringUtils.Left(Globals.objPCL92.PMSPCL92PolicyNo + " ", 7));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92Module))
                Globals.objWriter.Fields.Add("MODULE", StringUtils.Left(Globals.objPCL92.PMSPCL92Module + " ", 2));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92MasterCo))
                Globals.objWriter.Fields.Add("MASTERCO", StringUtils.Left(Globals.objPCL92.PMSPCL92MasterCo + " ", 2));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92Location))
                Globals.objWriter.Fields.Add("LOCATION", StringUtils.Left(Globals.objPCL92.PMSPCL92Location + " ", 2));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92Claim))
                Globals.objWriter.Fields.Add("CLAIM", StringUtils.Left(Globals.objPCL92.PMSPCL92Claim + " ", 12));
            Globals.objWriter.Fields.Add("CLMTSEQ", Globals.objPCL92.PMSPCL92ClmtSeq);
            Globals.objWriter.Fields.Add("POLCOVSEQ", Globals.objPCL92.PMSPCL92PolCovSeq);
            Globals.objWriter.Fields.Add("RESVNO", Globals.objPCL92.PMSPCL92Resvno);
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PaymentTyp))
                Globals.objWriter.Fields.Add("PAYMENTTYP", StringUtils.Left(Globals.objPCL92.PMSPCL92PaymentTyp + " ", 2));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeAge))
                Globals.objWriter.Fields.Add("PAYEEAGE", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeAge + " ", 2));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92MonthsEmpl))
                Globals.objWriter.Fields.Add("MONTHSEMPL", StringUtils.Left(Globals.objPCL92.PMSPCL92MonthsEmpl + " ", 2));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92YearsEmpl))
                Globals.objWriter.Fields.Add("YEARSEMPL", StringUtils.Left(Globals.objPCL92.PMSPCL92YearsEmpl + " ", 2));
            Globals.objWriter.Fields.Add("PAYMENTS", Globals.objPCL92.PMSPCL92Payments);
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PaymntFreq))
                Globals.objWriter.Fields.Add("PAYMNTFREQ", StringUtils.Left(Globals.objPCL92.PMSPCL92PaymntFreq + " ", 1));
            Globals.objWriter.Fields.Add("PAYMNTRATE", Globals.objPCL92.PMSPCL92PaymntRate);
            Globals.objWriter.Fields.Add("WEEKLYWAGE", Globals.objPCL92.PMSPCL92WeeklyWage);
            Globals.objWriter.Fields.Add("STARTDATE", Globals.objPCL92.PMSPCL92StartDate);
            Globals.objWriter.Fields.Add("NXTPAYDTE", Globals.objPCL92.PMSPCL92NxtPayDte);
            Globals.objWriter.Fields.Add("STOPDATE", Globals.objPCL92.PMSPCL92StopDate);
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92Reason))
                Globals.objWriter.Fields.Add("REASON", StringUtils.Left(Globals.objPCL92.PMSPCL92Reason + " ", 50));
            Globals.objWriter.Fields.Add("DRFTPRTDTE", Globals.objPCL92.PMSPCL92DrftPrtDte);
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92BankCode))
                Globals.objWriter.Fields.Add("BANKCODE", StringUtils.Left(Globals.objPCL92.PMSPCL92BankCode + " ", 4));
            Globals.objWriter.Fields.Add("DRFTAMT", Globals.objPCL92.PMSPCL92DrftAmt);
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92DrftNumber))
                Globals.objWriter.Fields.Add("DRFTNUMBER", StringUtils.Left(Globals.objPCL92.PMSPCL92DrftNumber + " ", 7));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PPCode))
                Globals.objWriter.Fields.Add("PPCODE", StringUtils.Left(Globals.objPCL92.PMSPCL92PPCode + " ", 2));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayePhrase))
                Globals.objWriter.Fields.Add("PAYEPHRASE", StringUtils.Left(Globals.objPCL92.PMSPCL92PayePhrase + " ", 75));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayToCode1))
                Globals.objWriter.Fields.Add("PAYTOCODE1", StringUtils.Left(Globals.objPCL92.PMSPCL92PayToCode1 + " ", 1));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeNo1))
                Globals.objWriter.Fields.Add("PAYEENO1", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeNo1 + " ", 5));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeName1))
                Globals.objWriter.Fields.Add("PAYEENAME1", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeName1 + " ", 30));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeAdd11))
                Globals.objWriter.Fields.Add("PAYEEADD11", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeAdd11 + " ", 30));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeAdd12))
                Globals.objWriter.Fields.Add("PAYEEADD12", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeAdd12 + " ", 30));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeAdd13))
                Globals.objWriter.Fields.Add("PAYEEADD13", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeAdd13 + " ", 30));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeZip1))
                Globals.objWriter.Fields.Add("PAYEEZIP1", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeZip1 + " ", 10));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayToCode2))
                Globals.objWriter.Fields.Add("PAYTOCODE2", StringUtils.Left(Globals.objPCL92.PMSPCL92PayToCode2 + " ", 1));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeNo2))
                Globals.objWriter.Fields.Add("PAYEENO2", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeNo2 + " ", 5));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeName2))
                Globals.objWriter.Fields.Add("PAYEENAME2", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeName2 + " ", 30));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeAdd21))
                Globals.objWriter.Fields.Add("PAYEEADD21", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeAdd21 + " ", 30));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeAdd22))
                Globals.objWriter.Fields.Add("PAYEEADD22", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeAdd22 + " ", 30));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeAdd23))
                Globals.objWriter.Fields.Add("PAYEEADD23", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeAdd23 + " ", 30));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeZip2))
                Globals.objWriter.Fields.Add("PAYEEZIP2", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeZip2 + " ", 10));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayToCode3))
                Globals.objWriter.Fields.Add("PAYTOCODE3", StringUtils.Left(Globals.objPCL92.PMSPCL92PayToCode3 + " ", 1));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeNo3))
                Globals.objWriter.Fields.Add("PAYEENO3", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeNo3 + " ", 5));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeName3))
                Globals.objWriter.Fields.Add("PAYEENAME3", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeName3 + " ", 30));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeAdd31))
                Globals.objWriter.Fields.Add("PAYEEADD31", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeAdd31 + " ", 30));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeAdd32))
                Globals.objWriter.Fields.Add("PAYEEADD32", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeAdd32 + " ", 30));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeAdd33))
                Globals.objWriter.Fields.Add("PAYEEADD33", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeAdd33 + " ", 30));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92PayeeZip3))
                Globals.objWriter.Fields.Add("PAYEEZIP3", StringUtils.Left(Globals.objPCL92.PMSPCL92PayeeZip3 + " ", 10));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92MailToCode))
                Globals.objWriter.Fields.Add("MAILTOCODE", StringUtils.Left(Globals.objPCL92.PMSPCL92MailToCode + " ", 1));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92MailToNo))
                Globals.objWriter.Fields.Add("MAILTONO", StringUtils.Left(Globals.objPCL92.PMSPCL92MailToNo + " ", 2));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92MailToName))
                Globals.objWriter.Fields.Add("MAILTONAME", StringUtils.Left(Globals.objPCL92.PMSPCL92MailToName + " ", 30));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92MailToAdd1))
                Globals.objWriter.Fields.Add("MAILTOADD1", StringUtils.Left(Globals.objPCL92.PMSPCL92MailToAdd1 + " ", 30));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92MailToAdd2))
                Globals.objWriter.Fields.Add("MAILTOADD2", StringUtils.Left(Globals.objPCL92.PMSPCL92MailToAdd2 + " ", 30));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92MailToAdd3))
                Globals.objWriter.Fields.Add("MAILTOADD3", StringUtils.Left(Globals.objPCL92.PMSPCL92MailToAdd3 + " ", 30));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92MailToZip))
                Globals.objWriter.Fields.Add("MAILTOZIP", StringUtils.Left(Globals.objPCL92.PMSPCL92MailToZip + " ", 10));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92MemoCode))
                Globals.objWriter.Fields.Add("MEMOCODE", StringUtils.Left(Globals.objPCL92.PMSPCL92MemoCode + " ", 2));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92Memo1))
                Globals.objWriter.Fields.Add("MEMO1", StringUtils.Left(Globals.objPCL92.PMSPCL92Memo1 + " ", 72));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92Memo2))
                Globals.objWriter.Fields.Add("MEMO2", StringUtils.Left(Globals.objPCL92.PMSPCL92Memo2 + " ", 72));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92Note1))
                Globals.objWriter.Fields.Add("NOTE1", StringUtils.Left(Globals.objPCL92.PMSPCL92Note1 + " ", 72));
            if (!string.IsNullOrEmpty(Globals.objPCL92.PMSPCL92Note2))
                Globals.objWriter.Fields.Add("NOTE2", StringUtils.Left(Globals.objPCL92.PMSPCL92Note2 + " ", 72));
            try
                {
                Execute(Globals.objWriter);
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.objWriter.Reset(true);
                Globals.dbg.PopProc();

                }

            }

        public static void UpdateAccountDates()
            {
            string sSql = null;
            string sAcctDte = null;
            object recordsAffected;
            DbReader dbReader = null;

            Globals.dbg.PushProc("UpdateAccountDates");
            try
                {

                sSql = "SELECT * FROM PMSPDATE" + " WHERE KEYFIELD = ' '";
                dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, sSql);
                if (!dbReader.Read())
                    {
                    sOverRideCYYMMDD = "0000000";
                    }
                else
                    {
                    sOverRideCYYMMDD = Convert.ToString(dbReader.GetValue("SYSOVRDTE"));
                    if (string.IsNullOrEmpty(StringUtils.Trim(sOverRideCYYMMDD)))
                        {
                        sOverRideCYYMMDD = "0000000";
                        }
                    }

                GetAccountDate(ref sAcctDte);
                Globals.objWriter.Tables.Add("PMSPCL50");
                Globals.objWriter.Fields.Add("ACCTDTE", sAcctDte);
                Globals.objWriter.Where.Add("ACCTDTE = 0");
                Execute(Globals.objWriter);
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.objWriter.Reset(true);
                if (dbReader != null)
                    dbReader.Dispose();
                Globals.dbg.PopProc();
                }
            }

        public static void GetAccountDate(ref string sAcctDte)
            {
            string sSql = null;
            object[][] vArray = null;
            string sCYYMMDD = null;
            long lYYYY = 0;
            int iFields = 1;
            object rowsAffected;
            DataSet dbDataSet = null;
            DbReader dbReader = null;
            DataTable dtDataTable = null;
            try
                {
                Globals.dbg.PushProc("GetAccountDate");

                sYYYYMMDD = string.Format("{0:yyyyMMdd}", System.DateTime.Now);


                lYYYY = Convert.ToInt64(StringUtils.Left(sYYYYMMDD, 4));
                sCYYMMDD = Globals.objUtils.ConvertDateToPoint(sYYYYMMDD);
                if (Convert.ToInt64(sOverRideCYYMMDD) > Convert.ToInt64(sCYYMMDD))
                    {
                    sCYYMMDD = sOverRideCYYMMDD;
                    lYYYY = Convert.ToInt64(Globals.objUtils.ConvertPointYear(StringUtils.Left(sCYYMMDD, 3)));
                    }

                ClearDictionaryObject();
                strSQL.AppendFormat("SELECT * FROM PMSPACCDT" + " WHERE ACCDTYEAR = {0}", "~ACCDTYEAR~");
                dictParams.Add("ACCDTYEAR", lYYYY);

                dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams);
                //dbReader = ExecuteReader("SELECT * FROM PMSPACCDT" + " WHERE ACCDTYEAR = " + lYYYY);

                dtDataTable = new DataTable();
                dtDataTable.Load(dbReader);
                if (dtDataTable.Rows.Count == 0)
                    {
                    sAcctDte = "00000";
                    return;
                    }

                while (iFields < dtDataTable.Columns.Count)
                    {
                    if (RC.Conversion.ConvertStrToLong(sCYYMMDD) >= RC.Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(dtDataTable.Rows[0][iFields]).Replace(" ", "")) && RC.Conversion.ConvertStrToLong(sCYYMMDD) <= RC.Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(dtDataTable.Rows[0][iFields + 1]).Replace(" ", "")))
                        {
                        sAcctDte = StringUtils.Left(Convert.ToString(dtDataTable.Rows[0][iFields + 1]), 5);
                        break;
                        }
                    iFields = iFields + 2;
                    }
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.dbg.PopProc();
                }
            }

        private static void UpdateWCompClassCode()
            {

            Globals.dbg.PushProc("UpdateWCompClassCode");
            DbReader dbReader = null, dbReader1 = null;
            try
                {
                ClearDictionaryObject();
                strSQL.AppendFormat("SELECT SYMBOL, SARCLASS FROM PMSPSA15 WHERE SYMBOL ={0} AND POLICY0NUM = {1} AND MODULE = {2} AND MASTER0CO ={3}  AND LOCATION ={4}  AND SARUNIT ={5}  AND SARSEQNO = {6}  AND SASEQNO = {7}", "~SYMBOL~", "~POLICY0NUM~", "~MODULE~", "~MASTER0CO~", "~LOCATION~", "~SARUNIT~", "~SARSEQNO~", "~SASEQNO~");
                dictParams.Add("SYMBOL", Globals.objPCL42.PMSPCL42Symbol);
                dictParams.Add("POLICY0NUM", Globals.objPCL42.PMSPCL42PolicyNo);
                dictParams.Add("MODULE", Globals.objPCL42.PMSPCL42Module);
                dictParams.Add("MASTER0CO", Globals.objPCL42.PMSPCL42MasterCo);
                dictParams.Add("LOCATION", Globals.objPCL42.PMSPCL42Location);
                dictParams.Add("SARUNIT", Globals.objPCL42.PMSPCL42UnitNo);
                dictParams.Add("SARSEQNO", Globals.objPCL42.PMSPCL42CovSeq);
                dictParams.Add("SASEQNO", Globals.objPCL42.PMSPCL42TransSeq);

                dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams);
                //dbReader = ExecuteReader("SELECT SYMBOL FROM PMSPSA15 WHERE SYMBOL ='" + Globals.objPCL42.PMSPCL42Symbol + "' AND POLICY0NUM = '" + Globals.objPCL42.PMSPCL42PolicyNo + "' AND MODULE = '" + Globals.objPCL42.PMSPCL42Module + "' AND MASTER0CO ='" + Globals.objPCL42.PMSPCL42MasterCo + "'  AND LOCATION ='" + Globals.objPCL42.PMSPCL42Location + "'  AND SARUNIT ='" + Globals.objPCL42.PMSPCL42UnitNo + "'  AND SARSEQNO = '" + Globals.objPCL42.PMSPCL42CovSeq + "'  AND SASEQNO = '" + Globals.objPCL42.PMSPCL42TransSeq + "'");
                if (!dbReader.Read())
                    {
                    return;
                    }

                string sResult = PointClaimDEUnitStat(Globals.sClaimNumber, Convert.ToString(Globals.sCLMTSEQ));
                Globals.objWriter.Tables.Add("PMSPCL14");
                if (!string.IsNullOrEmpty(sResult))
                    {
                    Globals.InitForUnitStat(Globals.objPCL14);
                    Globals.objPCL14.PMSPCL14ClassNum = Convert.ToString(dbReader.GetValue("SARCLASS"));

                    Globals.objWriter.Fields.Add("CLAIM", StringUtils.Left(Globals.sClaimNumber + " ", 12));
                    Globals.objWriter.Fields.Add("CLMTSEQ", Globals.sCLMTSEQ);
                    Globals.objWriter.Fields.Add("CLASSNUM", StringUtils.Left(Globals.objPCL14.PMSPCL14ClassNum + " ", 6));
                    Globals.objWriter.Fields.Add("LSMARSTAT", StringUtils.Left(Globals.objPCL14.PMSPCL14LsMarStat + " ", 1));
                    Globals.objWriter.Fields.Add("LSNBRDEPS", Globals.objPCL14.PMSPCL14LsNbrDeps);
                    Globals.objWriter.Fields.Add("LSDOB", StringUtils.Left(Globals.objPCL14.PMSPCL14LsDob + " ", 8));
                    Globals.objWriter.Fields.Add("LSDOD", StringUtils.Left(Globals.objPCL14.PMSPCL14LsDod + " ", 8));
                    Globals.objWriter.Fields.Add("LSOCCCODE", StringUtils.Left(Globals.objPCL14.PMSPCL14LsOccCode + " ", 2));
                    Globals.objWriter.Fields.Add("LSLSTWRKDA", StringUtils.Left(Globals.objPCL14.PMSPCL14LsLstWrkDa + " ", 7));
                    Globals.objWriter.Fields.Add("LSDTERETD", StringUtils.Left(Globals.objPCL14.PMSPCL14LsDteRetd + " ", 7));
                    Globals.objWriter.Fields.Add("LSREPINDR", StringUtils.Left(Globals.objPCL14.PMSPCL14LsRepIndr + " ", 1));
                    Globals.objWriter.Fields.Add("LSLEGREP", StringUtils.Left(Globals.objPCL14.PMSPCL14LsLegRep + " ", 30));
                    Globals.objWriter.Fields.Add("LSCURRAWW", Globals.objPCL14.PMSPCL14LsCurrAww);
                    Globals.objWriter.Fields.Add("CURRCOMPRT", Globals.objPCL14.PMSPCL14CurrCompRt);
                    Globals.objWriter.Fields.Add("LSNOTE1", StringUtils.Left(Globals.objPCL14.PMSPCL14LsNote1 + " ", 64));
                    Globals.objWriter.Fields.Add("LSNOTE2", StringUtils.Left(Globals.objPCL14.PMSPCL14LsNote2 + " ", 78));
                    Globals.objWriter.Fields.Add("LSNOTE3", StringUtils.Left(Globals.objPCL14.PMSPCL14LsNote3 + " ", 78));
                    Globals.objWriter.Fields.Add("LOSSENTYR", StringUtils.Left(Globals.objPCL14.PMSPCL14LossEntDteYr + " ", 3));
                    Globals.objWriter.Fields.Add("LOSSENTMO", StringUtils.Left(Globals.objPCL14.PMSPCL14LossEntDteMo + " ", 2));
                    Globals.objWriter.Fields.Add("LOSSENTDY", StringUtils.Left(Globals.objPCL14.PMSPCL14LossEntDteDy + " ", 2));
                    Globals.objWriter.Fields.Add("HISTDATE", StringUtils.Left(Globals.objPCL14.PMSPCL14HistDate + " ", 7));
                    Globals.objWriter.Fields.Add("HISTSEQ", Globals.objPCL14.PMSPCL14HistSeq);
                    Globals.objWriter.Fields.Add("ACT", StringUtils.Left(Globals.objPCL14.PMSPCL14Act + " ", 2));
                    Globals.objWriter.Fields.Add("TYPELOSS", StringUtils.Left(Globals.objPCL14.PMSPCL14TypeLoss + " ", 2));
                    Globals.objWriter.Fields.Add("TYPERECV", StringUtils.Left(Globals.objPCL14.PMSPCL14TypeRecv + " ", 2));
                    Globals.objWriter.Fields.Add("TYPECOVG", StringUtils.Left(Globals.objPCL14.PMSPCL14TypeCovg + " ", 2));
                    Globals.objWriter.Fields.Add("TYPESETMT", StringUtils.Left(Globals.objPCL14.PMSPCL14TypeSetmt + " ", 2));
                    Globals.objWriter.Fields.Add("INJCODE", StringUtils.Left(Globals.objPCL14.PMSPCL14InjCode + " ", 2));
                    Globals.objWriter.Fields.Add("CLIENT", Globals.objPCL14.PMSPCL14Client);
                    Globals.objWriter.Fields.Add("LSSTJURIS", StringUtils.Left(Globals.objPCL14.PMSPCL14LsStJuris + " ", 2));
                    Globals.objWriter.Fields.Add("INJURYZIP", StringUtils.Left(Globals.objPCL14.PMSPCL14InjuryZip + " ", 10));
                    Globals.objWriter.Fields.Add("HIREDTEYR", StringUtils.Left(Globals.objPCL14.PMSPCL14HireDteYr + " ", 4));
                    Globals.objWriter.Fields.Add("HIREDTEMO", StringUtils.Left(Globals.objPCL14.PMSPCL14HireDteMo + " ", 2));
                    Globals.objWriter.Fields.Add("HIREDTEDY", StringUtils.Left(Globals.objPCL14.PMSPCL14HireDteDy + " ", 2));
                    Globals.objWriter.Fields.Add("EMPLYSTAT", StringUtils.Left(Globals.objPCL14.PMSPCL14EmplyStat + " ", 1));
                    Globals.objWriter.Fields.Add("FEIN", StringUtils.Left(Globals.objPCL14.PMSPCL14Fein + " ", 9));
                    Globals.objWriter.Fields.Add("EMPLYRSIC", StringUtils.Left(Globals.objPCL14.PMSPCL14EmplrSic + " ", Globals.objPCL14.PMSPCL14EmplrSicFieldLength));
                    Globals.objWriter.Fields.Add("EMPPAYIND", StringUtils.Left(Globals.objPCL14.PMSPCL14EmpPayInd + " ", 1));
                    Globals.objWriter.Fields.Add("WAGEMETH", StringUtils.Left(Globals.objPCL14.PMSPCL14WageMeth + " ", 1));
                    Globals.objWriter.Fields.Add("OTHRWKPYMT", Globals.objPCL14.PMSPCL14OthrWkPymt);
                    Globals.objWriter.Fields.Add("DATEREPEMP", StringUtils.Left(Globals.objPCL14.PMSPCL14DateRepEmp + " ", 7));
                    Globals.objWriter.Fields.Add("SURGIND", StringUtils.Left(Globals.objPCL14.PMSPCL14SurgInd + " ", 1));
                    Globals.objWriter.Fields.Add("POSTINJWGE", Globals.objPCL14.PMSPCL14PostInjWge);
                    Globals.objWriter.Fields.Add("PCTIMPAIR", Globals.objPCL14.PMSPCL14PctImpair);
                    Globals.objWriter.Fields.Add("DTEOFDISAB", StringUtils.Left(Globals.objPCL14.PMSPCL14DteOfDisab + " ", 7));
                    Globals.objWriter.Fields.Add("DTEMAXMED", StringUtils.Left(Globals.objPCL14.PMSPCL14DteMaxMed + " ", 7));
                    Globals.objWriter.Fields.Add("DEDUCTIBLE", StringUtils.Left(Globals.objPCL14.PMSPCL14Deductible + " ", 1));
                    Globals.objWriter.Fields.Add("CONTROVERT", StringUtils.Left(Globals.objPCL14.PMSPCL14Controvert + " ", 1));
                    Globals.objWriter.Fields.Add("BOSOCSEC", StringUtils.Left(Globals.objPCL14.PMSPCL14BoSocSec + " ", 1));
                    Globals.objWriter.Fields.Add("BOUNEMPL", StringUtils.Left(Globals.objPCL14.PMSPCL14BoUnempl + " ", 1));
                    Globals.objWriter.Fields.Add("BOPENSION", StringUtils.Left(Globals.objPCL14.PMSPCL14BoPension + " ", 1));
                    Globals.objWriter.Fields.Add("BOSPECIAL", StringUtils.Left(Globals.objPCL14.PMSPCL14BoSpecial + " ", 1));
                    Globals.objWriter.Fields.Add("BOOTHER", StringUtils.Left(Globals.objPCL14.PMSPCL14BoOther + " ", 1));
                    Globals.objWriter.Fields.Add("LSOCCUPTN", StringUtils.Left(Globals.objPCL14.PMSPCL14LsOccuptn + " ", 30));
                    Globals.objWriter.Fields.Add("DEDUCTCODE", StringUtils.Left(Globals.objPCL14.PMSPCL14DeductCode + " ", 2));
                    Globals.objWriter.Fields.Add("MCOIND", StringUtils.Left(Globals.objPCL14.PMSPCL14McoInd + " ", 2));
                    Globals.objWriter.Fields.Add("FRAUDCLAIM", StringUtils.Left(Globals.objPCL14.PMSPCL14FraudClaim + " ", 1));
                    try
                        {
                        Execute(Globals.objWriter);

                        }
                    catch (Exception ex)
                        {
                        throw ex;
                        }
                    }
                else
                    {
                    Globals.objPCL14.PMSPCL14ClassNum = Convert.ToString(dbReader.GetValue("SARCLASS"));
                        {
                        Globals.objWriter.Fields.Add("CLASSNUM", Globals.objPCL14.PMSPCL14ClassNum);
                        Globals.objWriter.Where.Add("CLAIM='" + Globals.sClaimNumber + "' AND CLMTSEQ= " + Globals.sCLMTSEQ);
                        try
                            {
                            Execute(Globals.objWriter);

                            }
                        catch (Exception ex)
                            {
                            throw ex;
                            }
                        }
                    }
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.objWriter.Reset(true);
                if (dbReader != null)
                    dbReader.Dispose();
                Globals.dbg.PopProc();
                }
            }

        public static void UpdateClaimCoverageStatus()
            {
            string sSql = null;
            try
                {
                Globals.dbg.PushProc("UpdateClaimCoverageStatus");

                ClearDictionaryObject();
                strSQL.AppendFormat("SELECT CLAIM FROM PMSPCL42 WHERE CLAIM={0} AND CLMTSEQ={1} AND POLCOVSEQ={2}", "~CLAIM~", "~CLMTSEQ~", "~POLCOVSEQ~");
                dictParams.Add("CLAIM",Globals.sClaimNumber);
                dictParams.Add("CLMTSEQ",Globals.sCLMTSEQ);
                dictParams.Add("POLCOVSEQ", Globals.sPolCovSeq);

                string sResult = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr,strSQL.ToString(),dictParams));
                //string sResult = Convert.ToString(ExecuteScalar("SELECT CLAIM FROM PMSPCL42 WHERE CLAIM='" + Globals.sClaimNumber + "' AND CLMTSEQ=" + Globals.sCLMTSEQ + " AND POLCOVSEQ=" + Globals.sPolCovSeq));

                if (string.IsNullOrEmpty(sResult))
                    {
                    return;
                    }
                else
                    {
                    Globals.objWriter.Tables.Add("PMSPCL42");
                    Globals.objWriter.Where.Add(" CLAIM='" + Globals.sClaimNumber + "' AND CLMTSEQ=" + Globals.sCLMTSEQ + " AND POLCOVSEQ=" + Globals.sPolCovSeq);
                    Globals.objWriter.Fields.Add("RECSTATUS", StringUtils.Left(Globals.objPCL42.PMSPCL42RecStatus, 1));

                    Execute(Globals.objWriter);
                    }

                if (Globals.objPCL30.PMSPCL30RecStatus == Globals.objPCL42.PMSPCL42RecStatus)
                    {
                    return;
                    }
                else if (Globals.objPCL30.PMSPCL30RecStatus == "C")
                    {
                    if (Globals.objPCL42.PMSPCL42RecStatus == "O" || Globals.objPCL42.PMSPCL42RecStatus == "R")
                        {
                        Globals.objPCL30.PMSPCL30RecStatus = "R";
                        UpdateClaimantStatus();
                        }
                    else
                        {
                        CheckClaimantStatus();
                        }
                    }
                else
                    {
                    CheckClaimantStatus();
                    }
                }
            catch (Exception ex)
                {
                throw ex;

                }
            finally
                {
                Globals.objWriter.Reset(true);
                Globals.dbg.PopProc();
                }
            }

        public static void CheckClaimantStatus()
            {
            string sSql = null;
            long lOpenCount = 0;
            try
                {
                Globals.dbg.PushProc("CheckClaimantStatus");
                ClearDictionaryObject();
                strSQL.AppendFormat("SELECT POLCOVSEQ FROM PMSPCL42 WHERE CLAIM={0} AND CLMTSEQ= {1} AND RECSTATUS='O' OR RECSTATUS='R'","~CLAIM~","~CLMTSEQ~");
                dictParams.Add("CLAIM",Globals.sClaimNumber);
                dictParams.Add("CLMTSEQ", Globals.sCLMTSEQ);

                string sResult = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(),dictParams));
                //string sResult = Convert.ToString(ExecuteScalar("SELECT POLCOVSEQ FROM PMSPCL42 WHERE CLAIM='" + Globals.sClaimNumber + "' AND CLMTSEQ= '" + Globals.sCLMTSEQ + "' AND RECSTATUS='O' OR RECSTATUS='R'"));

                if (string.IsNullOrEmpty(sResult))
                    {
                    lOpenCount = 0;
                    }
                else
                    {
                    lOpenCount = 1;
                    }

                if (lOpenCount == 0)
                    {
                    if (Globals.objPCL30.PMSPCL30RecStatus == "C")
                        {
                        return;
                        }
                    else
                        {
                        Globals.objPCL30.PMSPCL30RecStatus = "C";
                        }
                    }
                else
                    {
                    if (Globals.objPCL30.PMSPCL30RecStatus == "C")
                        {
                        Globals.objPCL30.PMSPCL30RecStatus = "R";
                        }
                    else
                        {
                        return;
                        }
                    }

                UpdateClaimantStatus();
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                Globals.dbg.PopProc();
                }


            }

        public static void UpdateClaimantStatus()
            {
            string sSql = null;
            long lOpenCount = 0;
            DbReader dbReader = null;
            DbWriter obWriter = Riskmaster.Db.DbFactory.GetDbWriter(Globals.dbConnection);//"Driver={iSeries Access ODBC Driver}; System = 20.15.82.16;   Naming=SQL; DefaultLibraries=BNRDP2DAT,BNRDP2OBJ, BNRR00OBJ, BNRS01OBJ;UserID=lab07; Password=sangeet#4;"
            try
                {
                Globals.dbg.PushProc("UpdateClaimantStatus");
                ClearDictionaryObject();
                strSQL.AppendFormat("SELECT * FROM PMSPCL30 WHERE CLAIM= {0} AND CLMTSEQ={1}", "~CLAIM~", "~CLMTSEQ~");
                dictParams.Add("CLAIM", Globals.sClaimNumber);
                dictParams.Add("CLMTSEQ", Globals.sCLMTSEQ);
                dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams);
                
                //dbReader = ExecuteReader("SELECT * FROM PMSPCL30 WHERE CLAIM= '" + Globals.sClaimNumber + "' AND CLMTSEQ=" + Globals.sCLMTSEQ);

                if (!dbReader.Read())
                    {
                    return;
                    }
                else
                    {
                    obWriter.Tables.Add("PMSPCL30");
                    obWriter.Where.Add(String.Format(" CLAIM='{0}' AND CLMTSEQ='{1}'", Globals.sClaimNumber, Globals.sCLMTSEQ));
                    if (Globals.bUploadClaimantStatus)
                    {
                        obWriter.Fields.Add("RECSTATUS", StringUtils.Left(Globals.sClaimantStatus, 1));
                    }
                    else
                    {
                        obWriter.Fields.Add("RECSTATUS", StringUtils.Left(Globals.objPCL30.PMSPCL30RecStatus, 1));
                    }
                    Execute(obWriter);
                    }

                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
               obWriter=null;

                if (dbReader != null)
                    dbReader.Dispose();
                Globals.dbg.PopProc();

                }
            }

        public static void GetCurrentEntryDate()
            {
            string sSql = string.Empty;

            DbReader dbReader = null;
            Globals.dbg.PushProc("GetCurrentEntryDate");
            try
                {
                sSql = "SELECT * FROM PMSPDATE" + " WHERE KEYFIELD = ' '";
                dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr,"SELECT * FROM PMSPDATE" + " WHERE KEYFIELD = ' '");

                if (!dbReader.Read())
                    {
                    sOverRideCYYMMDD = "0000000";
                    }
                else
                    {
                    sOverRideCYYMMDD = Convert.ToString(dbReader.GetValue("SYSOVRDTE"));

                    if (string.IsNullOrEmpty(StringUtils.Trim(sOverRideCYYMMDD)))
                        {
                        sOverRideCYYMMDD = "0000000";
                        }
                    }

                sYYYYMMDD = string.Format("{0:yyyyMMdd}", System.DateTime.Now);
                Globals.sCurrentEntryDate = Globals.objUtils.ConvertDateToPoint(sYYYYMMDD);

                if (Convert.ToInt64(sOverRideCYYMMDD) > Convert.ToInt64(Globals.sCurrentEntryDate))
                    {
                    Globals.sCurrentEntryDate = sOverRideCYYMMDD;
                    }
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                if (dbReader != null)
                    dbReader.Dispose();
                    Globals.dbg.PopProc();
                }
            }




        public static void UpdatePointALUploadFile()
        {
            string sSql = string.Empty;
            //dbg.PushProc("UpdatePointALUploadFile");

            try
            {
                 Globals.objWriter.Tables.Add("PMSP0000");

                if (!string.IsNullOrEmpty(Globals.objPMSP0000.PMSP0000Symbol))
                {
                    Globals.objWriter.Fields["SYMBOL"].Value = StringUtils.Left(Globals.objPMSP0000.PMSP0000Symbol, 3);
                }

                if (!string.IsNullOrEmpty(Globals.objPMSP0000.PMSP0000POLICY0NUM))
                    Globals.objWriter.Fields["POLICY0NUM"].Value = StringUtils.Left(Globals.objPMSP0000.PMSP0000POLICY0NUM + " ", 7);

                if (!string.IsNullOrEmpty(Globals.objPMSP0000.PMSP0000Module))
                    Globals.objWriter.Fields["MODULE"].Value = StringUtils.Left(Globals.objPMSP0000.PMSP0000Module + " ", 2);

                if (!string.IsNullOrEmpty(Globals.objPMSP0000.PMSP0000MASTER0CO))
                    Globals.objWriter.Fields["MASTER0CO"].Value = StringUtils.Left(Globals.objPMSP0000.PMSP0000MASTER0CO + " ", 2);

                if (!string.IsNullOrEmpty(Globals.objPMSP0000.PMSP0000Location))
                    Globals.objWriter.Fields["LOCATION"].Value = StringUtils.Left(Globals.objPMSP0000.PMSP0000Location + " ", 2);

                Globals.objWriter.Fields["TYPE0ACT"].Value = Globals.objPMSP0000.PMSP0000TYPE0ACT;
                //Globals.objWriter.Fields["ACT0YEAR"].Value = Globals.objPMSP0000.PMSP0000ACT0YEAR;
                //Globals.objWriter.Fields["ACT0MONTH"].Value = Globals.objPMSP0000.PMSP0000ACT0MONTH;
                //Globals.objWriter.Fields["ACT0DAY"].Value = Globals.objPMSP0000.PMSP0000ACT0DAY;
                
                if (!string.IsNullOrEmpty(Globals.objPMSP0000.PMSP0000ACT0YEAR))
	                Globals.objWriter.Fields["ACT0YEAR"].Value = StringUtils.Left(Globals.objPMSP0000.PMSP0000ACT0YEAR + " ", 3);

                if (!string.IsNullOrEmpty(Globals.objPMSP0000.PMSP0000ACT0MONTH))
	                Globals.objWriter.Fields["ACT0MONTH"].Value = StringUtils.Left(Globals.objPMSP0000.PMSP0000ACT0MONTH + " ", 2);

                if (!string.IsNullOrEmpty(Globals.objPMSP0000.PMSP0000ACT0DAY))
	                Globals.objWriter.Fields["ACT0DAY"].Value = StringUtils.Left(Globals.objPMSP0000.PMSP0000ACT0DAY + " ", 2);

                Globals.objWriter.Fields["OPER0ID"].Value = Globals.objPMSP0000.PMSP0000OPER0ID;
                Globals.objWriter.Fields["COMPANY0NO"].Value = Globals.objPMSP0000.PMSP0000COMPANY0NO;
                Globals.objWriter.Fields["DELETE0IND"].Value = Globals.objPMSP0000.PMSP0000DELETE0IND;
                Globals.objWriter.Fields["TRANS0STAT"].Value = Globals.objPMSP0000.PMSP0000TRANS0STAT;
                Globals.objWriter.Fields["ETIME"].Value = Globals.objPMSP0000.PMSP0000ETIME;
                //Globals.objWriter.Fields["ENT0YEAR"].Value = Globals.objPMSP0000.PMSP0000ENT0YEAR;
                //Globals.objWriter.Fields["ENT0MONTH"].Value = Globals.objPMSP0000.PMSP0000ENT0MONTH;
                //Globals.objWriter.Fields["ENT0DAY"].Value = Globals.objPMSP0000.PMSP0000ENT0DAY;

                if (!string.IsNullOrEmpty(Globals.objPMSP0000.PMSP0000ENT0YEAR))
	                Globals.objWriter.Fields["ENT0YEAR"].Value = StringUtils.Left(Globals.objPMSP0000.PMSP0000ENT0YEAR + " ", 3);

                if (!string.IsNullOrEmpty(Globals.objPMSP0000.PMSP0000ENT0MONTH))
	                Globals.objWriter.Fields["ENT0MONTH"].Value = StringUtils.Left(Globals.objPMSP0000.PMSP0000ENT0MONTH + " ", 2);

                if (!string.IsNullOrEmpty(Globals.objPMSP0000.PMSP0000ENT0DAY))
	                Globals.objWriter.Fields["ENT0DAY"].Value = StringUtils.Left(Globals.objPMSP0000.PMSP0000ENT0DAY + " ", 2);

                Globals.objWriter.Fields["ERROR001"].Value = Globals.objPMSP0000.PMSP0000ERROR001;
                Globals.objWriter.Fields["ERROR002"].Value = Globals.objPMSP0000.PMSP0000ERROR002;
                Globals.objWriter.Fields["ERROR002"].Value = Globals.objPMSP0000.PMSP0000ERROR003;
                Globals.objWriter.Fields["ERROR004"].Value = Globals.objPMSP0000.PMSP0000ERROR004;
                Globals.objWriter.Fields["ERROR005"].Value = Globals.objPMSP0000.PMSP0000ERROR005;
                Globals.objWriter.Fields["ERROR006"].Value = Globals.objPMSP0000.PMSP0000ERROR006;
                Globals.objWriter.Fields["ERROR007"].Value = Globals.objPMSP0000.PMSP0000ERROR007;
                Globals.objWriter.Fields["ERROR008"].Value = Globals.objPMSP0000.PMSP0000ERROR008;
                Globals.objWriter.Fields["ERROR009"].Value = Globals.objPMSP0000.PMSP0000ERROR009;
                Globals.objWriter.Fields["ERROR010"].Value = Globals.objPMSP0000.PMSP0000ERROR010;
                Globals.objWriter.Fields["ERROR011"].Value = Globals.objPMSP0000.PMSP0000ERROR011;
                Globals.objWriter.Fields["ERROR012"].Value = Globals.objPMSP0000.PMSP0000ERROR012;
                Globals.objWriter.Fields["ERROR013"].Value = Globals.objPMSP0000.PMSP0000ERROR013;
                Globals.objWriter.Fields["ERROR014"].Value = Globals.objPMSP0000.PMSP0000ERROR014;
                Globals.objWriter.Fields["ERROR015"].Value = Globals.objPMSP0000.PMSP0000ERROR015;
                Globals.objWriter.Fields["ERROR016"].Value = Globals.objPMSP0000.PMSP0000ERROR016;
                Globals.objWriter.Fields["ERROR017"].Value = Globals.objPMSP0000.PMSP0000ERROR017;
                Globals.objWriter.Fields["ERROR018"].Value = Globals.objPMSP0000.PMSP0000ERROR018;
                Globals.objWriter.Fields["ERROR019"].Value = Globals.objPMSP0000.PMSP0000ERROR019;
                Globals.objWriter.Fields["ERROR020"].Value = Globals.objPMSP0000.PMSP0000ERROR020;
                Globals.objWriter.Fields["REC0CNT"].Value = Globals.objPMSP0000.PMSP0000REC0CNT;
                Globals.objWriter.Fields["COMPRATEIN"].Value = Globals.objPMSP0000.PMSP0000COMPRATEIN;
                Globals.objWriter.Execute();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Globals.objWriter.Reset(true);
            }

            //ScriptingFunctions.ExecutePolicyUploadCustomScript(ScriptingFunctions.conScptLvlClm, ScriptingFunctions.conScpEvtASave, PointClaimDErsPointALUpload);


        }
        public static void LocatePointALUploadFile()
        {
            DbReader dbReader = null;
            //PointClaimDEPointALUpload(objPMSP0000.PMSP0000Symbol, objPMSP0000.PMSP0000POLICY0NUM, objPMSP0000.PMSP0000Module, objPMSP0000.PMSP0000MASTER0CO, objPMSP0000.PMSP0000Location);

            ClearDictionaryObject();
            //strSQL.AppendFormat("SELECT * FROM PMSPCL20 WHERE CLAIM={0}", "~CLAIM~");
            //dictParams.Add("CLAIM", Globals.objPCL20.PMSPCL20Claim);
            strSQL.AppendFormat("SELECT * FROM PMSP0000 WHERE  SYMBOL = {0} AND POLICY0NUM = {1} AND MODULE = {2} AND MASTER0CO = {3} AND LOCATION = {4} ", "~SYMBOL~", "~POLICY0NUM~", "MODULE", "MASTER0CO", "LOCATION");
            dictParams.Add("SYMBOL", Globals.objPMSP0000.PMSP0000Symbol);
            dictParams.Add("POLICY0NUM", Globals.objPMSP0000.PMSP0000POLICY0NUM);
            dictParams.Add("MODULE", Globals.objPMSP0000.PMSP0000Module);
            dictParams.Add("MASTER0CO", Globals.objPMSP0000.PMSP0000MASTER0CO);
            dictParams.Add("LOCATION", Globals.objPMSP0000.PMSP0000Location);

            dbReader = DbFactory.ExecuteReader(Globals.objConfig.ConfigElement.DataDSNConnStr, strSQL.ToString(), dictParams);
            //if ((PointClaimDErsPointALUpload.BOF == true) | (PointClaimDErsPointALUpload.EOF == true))
            if (!dbReader.Read())
            {
                UpdatePointALUploadFile();
            }
        }
        
       }
    }
