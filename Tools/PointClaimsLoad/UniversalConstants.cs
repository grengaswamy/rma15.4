﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.PointClaimsLoad
{
    public class UniversalConstants
    {
        // System Constants
        public const string SYS_CCP_REG_KEY = "SOFTWARE\\CSC\\CCP";

        public const string SYS_CCP_REG_VAL_LANGUAGE = "LANGUAGE";
        public const string SYS_DFLT_LANGUAGE_CODE = "1033";
        public const string SYS_DFLT_LANGUAGE = "English";
        public const string SYS_BATCH_PARMS_SW = "BatchParms";
        public const string SYS_BATCH_PARMS_DB = "WSDatabase";
        public const string SYS_BATCH_PARMS_UID = "WSUserid";
        public const string SYS_BATCH_PARMS_PWD = "WSPassword";
        public const string NO_DATE = "99991231";
        public const string HI_DATE = "99991231";
        public const string LOW_DATE = "19000101";
        public const string HI_TIME = "235959";
        public const string LOW_TIME = "000000";
        public const string BLANK_STRING = "<BLANK>";
        public const string BLANK_STRING_2 = "<B>";
        public const string SYS_HOSTS = "Hosts";
        public const string SYS_HOST_DSN = "HostDSN";
        public const string SYS_DFLT_MIN_DATE_VALUE = "18000101";
        public const string SYS_DFLT_MAX_DATE_VALUE = "20991231";

        //Universal Code Table Names
        public const string ctLanguageCodes = "LANGUAGE_CODES";

        //Trapped System Errors
        public const int ERR_UNKNOWN = 1;
        public const int ERR_VB_RUN_TIME = 2;
        public const int ERR_PRINTER = 3;
        public const int ERR_GRID_CONTROL = 4;
        public const int ERR_OLE = 5;
        public const int ERR_COMMON_DIALOG = 6;
        public const int ERR_ODBC = 7;

        public const int ERR_DATA_ACCESS = 8;
        public const int iDCANCEL = 2;
        public const int iDRETRY = 4;

        //Install Registry Entries
        public const string INSTALL_REG_KEY = "INSTALL";
        public const string INSTALL_REG_COMPANY = "Company";
        public const string INSTALL_REG_DIR = "Directory";
        public const string INSTALL_REG_FNOL = "FNOLPath";
        public const string INSTALL_REG_GROUP = "Group";
        public const string INSTALL_REG_NAME = "Name";
        public const string INSTALL_REG_SYS = "SystemFiles";

        //Cache Registry Options
        public const string CACHE_OPTIONS = "Options\\Caching";
        public const string CACHE_ALWAYS = "Always";
        public const string CACHE_HIDE = "Hide";
        public const string CACHE_MINIMIZE = "Minimize";

        public const string CACHE_WARNING = "Warning";

        //Application Execution Options
        public const string APP_REG_OPTIONS = "Options";
        public const string APP_REG_OPTION_DIS_SPLASH = "DisableSplashScreens";
        //End SI05481

        //Application Caching Options
        public const string CACHING_SWITCH = "Caching";
        public const string CACHING_OPTIONS = "Options\\Caching";
        public const string CACHING_ALWAYS = "Always";
        public const string CACHING_WARNING = "Warning";
        public const string CACHING_MINIMIZE = "Minimize";
        public const string CACHING_HIDE = "Hide";
        public const string CACHING_GLOSS_MINIMIZE = "GLOSSARYMinimize";
        public const string CACHING_GLOSS_HIDE = "GLOSSARYHide";
        public const string CACHING_CODES_MINIMIZE = "CODESMinimize";
        public const string CACHING_CODES_HIDE = "CODESHide";
        public const string CACHING_ENTITY_MINIMIZE = "ENTITYMinimize";
        public const string CACHING_ENTITY_HIDE = "ENTITYHide";
        public const string CACHING_STATES_MINIMIZE = "STATESMinimize";
        public const string CACHING_STATES_HIDE = "STATESHide";
        public const string CACHING_USERS = ".Users";
        public const string REG_SCRIPTING = "Scripting";
        public const string REG_SCRIPT_COMMON_KEY = "CommonDisabled";
        public const string REG_SCRIPT_VAL_KEY = "ValidationDisabled";
        public const string REG_SCRIPT_INIT_KEY = "InitializationDisabled";
        public const string REG_SCRIPT_CALC_KEY = "CalculationDisabled";
        public const string REG_SCRIPT_BSAVE_KEY = "BeforeSaveDisabled";
        public const string REG_SCRIPT_ASAVE_KEY = "AfterSaveDisabled";
        public const string REG_SCRIPT_ALOAD_KEY = "AfterLoadDisabled";
        public const string REG_SCRIPT_ADELETE_KEY = "AfterDeleteDisabled";
        public const string REG_SCRIPT_BDELETE_KEY = "BeforeDeleteDisabled";
        public const string REG_SCRIPT_ACOPY_KEY = "AfterCopyDisabled";
        public const string REG_SCRIPT_BCOPY_KEY = "BeforeCopyDisabled";
        public const string REG_SCRIPTREFRESHALWAYS = "RefreshScriptAlways";
        public const string REG_SCRIPTABORTONERRORS = "AbortOnErrors";
        public const string REG_SCRIPTDEBUGGING = "Debug";
        public const string REG_SYS_TRANSACTION = "TransactionsDisabled";
        public const string REG_SYS_ORGSECURITY = "OrganizationSecurity";

        //WPA Registry Options
        public const string WPA_OPTIONS = "Options\\WPA";
        public const string WPA_LOAD_TO_DO = "ToDoListLoading";
        public const string WPA_MAIL_CHECKING = "NewMailChecking";
        public const string WPA_AUTO_DIARY = "AutoDiary";
        public const string WPA_EXPORT_SCHEDULE = "SchedulePlusExport";
        public const string WPA_MAIL = "MailIntegration";
        public const string WPA_SORT_SEQ = "DefaultSortSeq";
        public const string WPA_ACTIVE_RECORD = "ActiveRecordDiaries";
        public const string WPA_SHOW_ALL_DATE = "ShowAllDates";
        public const string WPA_CLAIMANT_LEVEL = "ClaimantLevelGeneration";
        public const string WPA_DFLT_ROLLABLE = "Rollable";
        public const string WPA_DFLT_ROUTABLE = "Routable";
        public const string WPA_DFLT_RPRTCMPLT = "NotifyComplete";
        public const string WPA_DFLT_RPRTOVRDU = "NotifyOverdue";
        public const string WPA_ITEM_COUNT = "ItemCount";
        public const int WPA_DEF_ITEM_COUNT = 200;
        public const string WPI_MAIL_CLIENT = "MailClient";

        //WPA Runtime Parameters
        //Format = /WPA.POSTy.x=
        public const string WPAPARAM = "WPA";
        //y=blank | SUCCESSFUL | ERROR
        //x=action or value
        //Unconditional Post
        public const string WPAPARAMPOST = ".POST";
        //Post on Success
        public const string WPAPARAMPOSTGOOD = ".POSTSUCCESSFUL";
        //Post on Error
        public const string WPAPARAMPOSTERROR = ".POSTERROR";
        //=Task Short Code
        public const string WPAPARAMPOSTTASK = ".TASK";
        //=Task Description
        public const string WPAPARAMPOSTTASKDESC = ".TASKDESC";
        //=Activity ShortCode
        public const string WPAPARAMPOSTACTIVITY = ".ACTIVITY";
        //=Activity Description
        public const string WPAPARAMPOSTACTIVITYDESC = ".ACTIVITYDESC";
        //=UserID
        public const string WPAPARAMPOSTUSER = ".USER";
        //=Message
        public const string WPAPARAMPOSTMESSAGE = ".MESSAGE";
        //=AutoTasking Code
        public const string WPAPARAMPOSTAUTOTASK = ".AUTOTASK";
        public const string WPAPARAMPOSTTABLE = ".TABLE";
        public const string WPAPARAMPOSTASSGNBY = ".ASSIGNEDBY";
        public const string WPAPARAMPOSTASSGNTO = ".ASSIGNEDTO";
        public const string COLOR_OPTIONS = "Options\\Colors";
        public const string COLOR_BACKGRND = "Background";
        public const string COLOR_FOREGRND = "Foreground";
        public const string COLOR_APP_BACKGRND = "ApplicationBackground";
        public const string COLOR_TBAR_BACKGRND = "ToolBarBackground";
        public const string COLOR_TBAR_FOREGRND = "ToolBarForeground";
        public const string COLOR_REQFIELDS_ITL = "ReqFieldsIalicize";
        public const string COLOR_REQFIELDS_AST = "ReqFieldsAsterisk";
        public const string COLOR_REQFIELDS_UL = "ReqFieldsUnderline";
        public const string COLOR_REQFIELDS_FOR = "ReqFieldsForeGround";
        public const string COLOR_REQFIELDS_BACK = "ReqFieldsBackGround";
        public const string COLOR_CUSTLABEL = "CustLabel";
        public const string COLOR_BUTTONBACK = "ButtonsBackground";
        public const string COLOR_BUTTONFORE = "ButtonsForeground";
        public const string COLOR_ENTRYFORE = "EntryFieldsForeground";
        public const string COLOR_ENTRYBACK = "EntryFieldsBackground";
        public const string COLOR_OPTIONBACK = "OptionButtonsBackground";
        public const string COLOR_LABELBACK = "LabelBackground";
        public const string COLOR_LABELFORE = "LabelForeground";
        public const string COLOR_LISTBACK = "ListBackground";
        public const string COLOR_LISTFORE = "ListForeground";

        // Error Code Constants
        // Error Code = cstr(Error_Type + Error_Code) & cstr(Table ID)
        public const int ERROR_TYPE_VALIDATE = 1000;
        public const int ERROR_TYPE_OPERATION = 2000;
        public const int ERROR_TYPE_DATAENTRY = 3000;
        public const int ERROR_TYPE_LABEL = 4000;
        public const int ERROR_TYPE_LOADING = 5000;
        public const long ERROR_TYPE_FIELD_CAPTION = 40000;
        public const long ERROR_TYPE_FIELD_HELP = 40001;
        public const long ERROR_TYPE_FIELD_TAG = 40002;
        public const long ERROR_TYPE_FIELD_TOOLTIP = 40003;
        public const long ERROR_TYPE_MNU_ITEM = 40004;
        public const long ERROR_TYPE_COMP_NAME = 40005;

        public const int ERROR_TYPE_SCRIPT = 9000;
        public const long SCRIPT_EXECUTION_ERROR = 29899;

        public const string SCRIPT_PROC_NOTFOUND = "80020006";
        // Application constants
        public const int iUndefined = -1;

        public const string sUndefined = "UNDEFINED";
        public const char cXCFIRST = '0';
        public const char cXCLAST = '3';
        public const char cXCNEXT = '1';
        public const char cXCPREV = '2';

        public const char cXCCURR = '4';
        public const string sUCPayTypeUnspecified = "UNSPECIFIED";
        public const string sUCPayTypeSalary = "SALARY";
        public const string sUCPayTypeHourly = "HOURLY";
        public const int iUCPayTypeUnspecified = 0;
        public const int iUCPayTypeSalary = 1;

        public const int iUCPayTypeHourly = 2;
        public const string sUCPayPeriodUnspecified = "UNSPECIFIED";
        public const string sUCPayPeriodHourly = "HOURLY";
        public const string sUCPayPeriodWeekly = "WEEKLY";
        public const string sUCPayPeriodBiWeekly = "BIWEEKLY";
        public const string sUCPayPeriodSemiMonthly = "SEMIMONTHLY";
        public const string sUCPayPeriodMonthly = "MONTHLY";
        public const string sUCPayPeriodQuarterly = "QUARTERLY";
        public const string sUCPayPeriodAnnual = "ANNUAL";
        public const string sUCPayPeriodDaily = "DAILY";
        public const int iUCPayPeriodUnspecified = 0;
        public const int iUCPayPeriodHourly = 1;
        public const int iUCPayPeriodWeekly = 2;
        public const int iUCPayPeriodBiWeekly = 3;
        public const int iUCPayPeriodSemiMonthly = 4;
        public const int iUCPayPeriodMonthly = 5;
        public const int iUCPayPeriodQuarterly = 6;
        public const int iUCPayPeriodAnnual = 7;
        public const int iUCPayPeriodDaily = 8;
        public const string conCTVerbiageNLSSupport = "NLS_TRANSLATE";

        // Application Defined Role Codes
        public const string ROLE_X_COMPONENT = "RXC";
        public const string ADR_ROLECOMP_CODE = "ADDRXC";
        public const string CLAIMANT_CODE = "CLT";
        public const string ADJUSTER_CODE = "ADJ";
        public const string PAYEE_CODE = "PAYEE";
        public const string MAILTO_CODE = "MAILTO";
        public const string PAYOR_CODE = "PR";
        public const string WC_RELATIONSHIP_CDE = "WCEMPLYE";
        public const string AGENT_ROLE = "AGT";
        public const string POLICYHOLDER_ROLE = "PHLD";
        public const string SPOUSE_ROLE = "SPOUSE";
        public const string DEPENDENT_ROLE = "DP";
        public const string BENEFICIARY_ROLE = "BEN";
        public const string LEGAL_GUARDIAN_ROLE = "GRD";
        public const string EMPLOYER_ROLE = "EMR";
        public const string SUPERVISOR_ROLE = "SUP";
        public const string MANAGER_ROLE = "MGR";
        public const string CHILD_ROLE = "CHILD";
        public const string AUTO_MAILTO_RELATION = "AUTOMAILTO";
        public const string AUTO_PAYEE_RELATION = "AUTOPAYEE";
        public const string COMB_PAY_ADDR_RelCode = "COMB_PAY_ADDR";
        public const string ROLE_TYPE = "ROLE";
        public const string ADR_REL_TYPE = "ADDR";

        // Table to get code for Rel. Type
        public const string RELATIONSHIP_TYPE_TABLE = "RELATIONSHIP_TYPE";

        // Table to get code for Rel.
        public const string RELATIONSHIP_TABLE = "RELATIONSHIP";

        // Application defined table name
        public const string CLAIM_TABLE = "CLAIM";
        public const string EVENT_TABLE = "EVENT";
        public const string MANUAL_POLICY_TABLE = "MAN_POLICY";
        public const string ENTITY_TABLE = "ENTITY";

        public const string ADDRESS_TABLE = "ENTITY_ADDRESS";

        //System Settings options
        public const string SYSSET_DEFAULT = "DEFAULT";
        public const string SYSSET_CAPTIONS = "Captions";
        public const string SYSSET_SPLASH = "SplashScreens";
        public const string SYSSET_GRAPHICS = "Graphics";
        public const string SYSSET_PICTURE = "Picture";
        public const string SYSSET_SUPPORT = "Support";
        public const string SYSSET_LOGGING = "LOGGING";
        public const string SYSSET_LOGGINGRETRY = "LOGGINGRETRY";
        public const string SYSSET_GUIDLOGFILE = "GUIDLOGFILE";
        public const string SYSSET_LOGFILE = "LOGFILE";
        public const string SYSSET_LOGPATH = "LOGPATH";
        public const string SYSSET_DEBUGGING = "DEBUGGING";
        public const string SYSSET_DSGNMODE = "DSGNMODE";
        public const string SYSSET_DISCRETELOG = "DISCRETELOGGING";
        public const string SYSSET_HELPFILE = "HelpFile";
        public const string SYSSET_HELPPATH = "HelpPath";
        public const string SYSSET_HELPTOPIC = "HelpTopics";
        public const string SYSSET_COMPANY = "Company";
        public const string SYSSET_PRODUCTID = "ProductID";
        public const string SYSSET_AUTOENCRYPT = "AutoEncrypt";
        public const string conPRODUCTID = "AC";
        public const string SYSSET_PGMNAMES = "Programs";
        public const string SYSSET_USERID = "WSUserid";
        public const string SYSSET_PASSWORD = "WSPassword";
        public const string SYSSET_INPUTFILE = "InputFile";
        public const string SYSSET_INPUTPATH = "InputPath";
        public const string SYSSET_OUTPUTFILE = "OutputFile";
        public const string SYSSET_OUTPUTPATH = "OutputPath";
        public const string SYSSET_MAILPROVIDER = "MailProvider";
        public const string SYSSET_TYPE_DEFAULT = "Default";
        public const string SYSSET_TYPE_OPTION = "Options";
        public const string SYSSET_ITEM_WPA_PROCESS = "WPAProcessing";
        public const string SYSSET_COMP_SPELLCHK = "SpellCheck";
        public const string SYSSET_ITEM_SPELLCHK_DCT = "Dictionary";
        public const string SYSSET_DFLT_SPELLCHK_DCT = "dtgspell.dct";

        //Universal Class Name Component
        public const string SYSSET_CLASSNAME = "ClassName";
        public const string SYSSET_EVENT_OPTIONS = "Event Options";
        public const string SYSSET_EVENT_NUMBER = "Next Event Number";
        public const string SYSSET_EVENT_USECLMNUM = "Use Claim Number";
        public const string SYSSET_CLAIM_OPTIONS = "Claim Options";
        public const string SYSSET_CLAIM_NUMBER = "Next Claim Number";
        public const string SYSSET_CLAIM_AUTOGEN = "Disable AutoGen";
        public const string SYSSET_CLAIM_NUMMASK = "Format Mask";
        public const string SYSSET_CLAIM_NUMMASKONLY = "Mask Only";
        public const string SYSSET_CLAIM_NUMKEY = "Key Format";

        //The following constants are used to define standard document management properties
        public const string DM_SYSSET_TYPE_CLASSNAME = "DocManagement";
        public const string DM_CLASSNAME = "CCPDM.CDocManagement";
        public const string SYSSET_SEARCH = "Search";
        public const string SYSSET_SORT1 = "Sort1";
        public const string SYSSET_SORT2 = "Sort2";
        public const string SYSSET_SORT3 = "Sort3";

        //Mail Merge Constants
        public const string MM_SYSINFO = "MailMerge";

        public const string MM_FILE8BYTES = "FileName8Bytes";
        //These must be kept in synch with the definitions in CCPUF.CFunctions
        public const int UC_ufdtString = 0;
        public const int UC_ufdtInteger = 1;
        public const int UC_ufdtLong = 2;
        public const int UC_ufdtDecimal = 3;
        public const int UC_ufdtDouble = 4;
        public const int UC_ufdtCurrency = 5;
        public const int UC_ufdtDate = 6;
        public const int UC_ufdtBool = 7;
        public const int UC_ufdtByte = 8;
        public const int UC_ufdtSingle = 9;
        public const int UC_ufdtTime = 10;
        public const int UC_ufdtDTTM = 11;

        //Start SI05503
        public const string sYSSET_MEASUREMENT = "Measurements";
        public const string sYSSET_MEASUREMENT_SAE = "SAE";
        public const string sYSSET_MEASUREMENT_METRIC = "METRIC";
        public const string WC_INSLINE_LIST = ":WC:WCC:WCV:";

        //Standard Weight Units
        public const string POUNDS = "LBS";
        public const string KILOGRAMS = "KGS";
        public const string GRAMS = "G";
        public const string TONS = "TONS";
        public const string METRICTONS = "MTONS";

        public const string OUNCES = "OZS";

        //Standard Length Units
        public const string FOOT = "FT";
        public const string YARD = "YD";
        public const string INCH = "IN";
        public const string MILE = "MILE";
        public const string METER = "M";
        public const string KILOMETER = "KM";
        public const string CENTIMETER = "CM";
        public const string MILIMETER = "MM";

        //Activity Log Trigger Options
        public const string AL_TRIGGER_TYPE = "ActivityLog";
        //Table Name for Activity Log Entry
        public const string AL_TRIGGER_COMPONENT = "";
        //Trigger Short Code
        public const string AL_TRIGGER_ITEM = "";
        //{Yes = Write Trigger}, No = Ignore Trigger
        public const string AL_TRIGGER_VALUE = "";
        public const string SYSSET_HIERARCHY = "Hierarchy";
        public const string SYSSET_ORGSEC = "OrgSecurity";
        public const string SYSSET_ORGSEC_USE = "UseOrgSecurity";
        public const string SYSSET_ORGSEC_ViewUnAssign = "ViewUnAssignedClaims";
        public const string SYSSET_ORGSEC_OHSLevel = "BottomTableLevel";
        public const string SYSSET_ORGHIER_Customer = "Customer";
        public const string SYSSET_ORGHIER_IDNumberType = "IDNumberType";
        public const string SYSSET_ORGHIER_OHLevel = "OHLevel";
        public const string SYSSET_ORGHIER_UseComboBox = "UseComboBoxOnPolicy";
        public const string IX_IMPORTFILE_SW = "/IX";
        public const string AC_WEB_PARAM = "AC_WEB_IMPORT";



    }
}
