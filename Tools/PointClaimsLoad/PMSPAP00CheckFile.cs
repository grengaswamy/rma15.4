﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.PointClaimsLoad
{
    public class PMSPAP00CheckFile
    {
        public string PMSPAP00PayType;
        public string PMSPAP00ItemNo;
        public string PMSPAP00PayStatus;
        public string PMSPAP00Loc;
        public string PMSPAP00Mco;
        public string PMSPAP00Sym;
        public string PMSPAP00PolNo;
        public string PMSPAP00Mod;
        public string PMSPAP00Agency;
        public string PMSPAP00BankCode;
        public string PMSPAP00CheckNo;
        public float PMSPAP00CheckAmt;
        public string PMSPAP00PayeeName;
        public string PMSPAP00Payee2;
        public string PMSPAP00MailLine3;
        public string PMSPAP00MailLine4;
        public string PMSPAP00ZipCode;
        public string PMSPAP00CheckDate;
        public int PMSPAP00CheckReconciled;
        public string PMSPAP00CreateDate;
        public string PMSPAP00CreateTime;
        public string PMSPAP00LastChangedDate;
        public string PMSPAP00LastChangedTime;
        public string PMSPAP00EntrySequence;
        public string PMSPAP00CashEntryDate;
        public string PMSPAP00CashEntryTime;
        public string PMSPAP00CashEntrySequence;
        public string PMSPAP00Claim;
        public int PMSPAP00ClmtSeq;
        public int PMSPAP00PolCovSeq;
        public int PMSPAP00ResvNo;
        public int PMSPAP00TransSeq;

        public PMSPAP00CheckFile()
        {
            Clear();

        }

        public void Clear()
        {
            PMSPAP00PayType = "";
            PMSPAP00ItemNo = "";
            PMSPAP00PayStatus = "";
            PMSPAP00Loc = "";
            PMSPAP00Mco = "";
            PMSPAP00Sym = "";
            PMSPAP00PolNo = "";
            PMSPAP00Mod = "";
            PMSPAP00Agency = "";
            PMSPAP00BankCode = "";
            PMSPAP00CheckNo = "";
            PMSPAP00CheckAmt = 0;
            PMSPAP00PayeeName = "";
            PMSPAP00Payee2 = "";
            PMSPAP00MailLine3 = "";
            PMSPAP00MailLine4 = "";
            PMSPAP00ZipCode = "";
            PMSPAP00CheckDate = "";
            PMSPAP00CheckReconciled = 0;
            PMSPAP00CreateDate = "";
            PMSPAP00CreateTime = "";
            PMSPAP00LastChangedDate = "";
            PMSPAP00LastChangedTime = "";
            PMSPAP00EntrySequence = "00";
            //SI04357
            PMSPAP00CashEntryDate = "";
            PMSPAP00CashEntryTime = "";
            PMSPAP00CashEntrySequence = "";
            PMSPAP00Claim = "";
            PMSPAP00ClmtSeq = 0;
            PMSPAP00PolCovSeq = 0;
            PMSPAP00ResvNo = 0;
            PMSPAP00TransSeq = 0;
        }
    }
}
