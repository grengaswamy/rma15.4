﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.PointClaimsLoad
{
    public class PMSPCL50ClaimsResPayTrans
    {
        public string PMSPCL50Claim;
        public int PMSPCL50ClmtSeq;
        public int PMSPCL50PolCovSeq;
        public int PMSPCL50ResvNo;
        public int PMSPCL50TransSeq;
        public string PMSPCL50RecStatus;
        public string PMSPCL50ClaimTrans;
        public float PMSPCL50ResvPayAmt;
        public float PMSPCL50CurrResv;
        public float PMSPCL50TotPayment;
        //SI04558
        public long PMSPCL50TranEntDt;
        public string PMSPCL50TranEntTm;
        public long PMSPCL50AcctDte;
        public string PMSPCL50PaymentTyp;
        public string PMSPCL50ReinsInd;
        public string PMSPCL50Vendor;
        public string PMSPCL50OffsetSw;
        public string PMSPCL50Adjustor;
        public string PMSPCL50ClmOffice;
        public string PMSPCL50ExaminerCd;

        public PMSPCL50ClaimsResPayTrans()
        {
            Clear();
        }

        public void Clear()
        {
            PMSPCL50Claim = "";
            PMSPCL50ClmtSeq = 0;
            PMSPCL50PolCovSeq = 0;
            PMSPCL50ResvNo = 0;
            PMSPCL50TransSeq = 0;
            PMSPCL50RecStatus = "";
            PMSPCL50ClaimTrans = "";
            PMSPCL50ResvPayAmt = 0;
            PMSPCL50CurrResv = 0;
            PMSPCL50TotPayment = 0;
            PMSPCL50TranEntDt = 0;
            //SI04558
            PMSPCL50TranEntTm = "";
            PMSPCL50AcctDte = 0;
            PMSPCL50PaymentTyp = "";
            PMSPCL50ReinsInd = "";
            PMSPCL50Vendor = "";
            PMSPCL50OffsetSw = "";
            PMSPCL50Adjustor = "";
            PMSPCL50ClmOffice = "";
            PMSPCL50ExaminerCd = "";
        }
    }
}
