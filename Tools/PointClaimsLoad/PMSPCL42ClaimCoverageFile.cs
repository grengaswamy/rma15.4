﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.PointClaimsLoad
{
    public class PMSPCL42ClaimCoverageFile
    {
        public string PMSPCL42Claim;
        public int PMSPCL42ClmtSeq;
        public int PMSPCL42PolCovSeq;
        public string PMSPCL42RecStatus;
        public string PMSPCL42Symbol;
        public string PMSPCL42PolicyNo;
        public string PMSPCL42Module;
        public string PMSPCL42MasterCo;
        public string PMSPCL42Location;
        public string PMSPCL42UnitNo;
        //SI04558
        public int PMSPCL42CovSeq;
        //SI04558
        public int PMSPCL42TransSeq;
        public string PMSPCL42Adjustor;
        public string PMSPCL42LossCause;
        public string PMSPCL42Examiner;
        public string PMSPCL42ClaimOff;

        public PMSPCL42ClaimCoverageFile()
        {
            Clear();
        }
        public void Clear()
        {
            PMSPCL42Claim = "";
            PMSPCL42ClmtSeq = 0;
            PMSPCL42PolCovSeq = 0;
            PMSPCL42RecStatus = "";
            PMSPCL42Symbol = "";
            PMSPCL42PolicyNo = "";
            PMSPCL42Module = "";
            PMSPCL42MasterCo = "";
            PMSPCL42Location = "";
            PMSPCL42UnitNo = "";
            PMSPCL42CovSeq = 0;
            //SI04558
            PMSPCL42TransSeq = 0;
            //SI04558
            PMSPCL42Adjustor = "";
            PMSPCL42LossCause = "";
            PMSPCL42Examiner = "";
            PMSPCL42ClaimOff = "";
        }
    }
}
