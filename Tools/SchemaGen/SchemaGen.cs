using System;
using System.Xml;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.DataModel;
using System.IO;
using System.Diagnostics;
using System.Reflection;

	/// <summary>
	/// Summary description for Class1.
	/// </summary>
class App
{	
	static string m_SchemaPath="";
	static UserLogin m_objUserLogin=null;

	/// <summary>
	/// The main entry point for the application.
	/// </summary>
	[STAThread]
	static void Main(string[] args)
	{
		GenerateSchemaImp objGen = null;

		if(!CheckParams(args))
			return;
			
		//Always Get UserLogin to use for subsequent DMF creation 
		Riskmaster.Security.Login objLogin = new  Login(SecurityDatabase.Dsn);
		try
		{
			objLogin.RegisterApplication(0,0,ref m_objUserLogin);
		}
		catch(Exception e)
		{
			Console.WriteLine("Login Exception:");
			Console.WriteLine(e.InnerException.Message);
		}
		if(m_objUserLogin==null)
		{
			Console.WriteLine("Authentication Failed");
			return;
		}
		
		objGen = new GenerateSchemaImp(m_objUserLogin,m_SchemaPath);
		objGen.GenerateSchema();
		objGen = null;

		//Give Results
		Console.WriteLine(String.Format("Schema generated successfully at: {0}",m_SchemaPath));
	}
	static bool CheckParams(string[] args)
	{

		if(args.Length >1)
		{
			Console.WriteLine("[ERROR] Incorrect usage, please specify the full path to the folder where the schema file is to be stored.");
			Console.WriteLine("Example: SchemaGen c:\\home\\schema");
			return false;
		}
		if (args.Length<1)
			m_SchemaPath = System.AppDomain.CurrentDomain.BaseDirectory;
		else
			m_SchemaPath = args[0];

		if(!System.IO.Directory.Exists(m_SchemaPath))
		{
			Console.WriteLine("[ERROR] Invalid Path specified, please specify the full path to the folder where the schema file is to be stored.");
			Console.WriteLine("Example: SchemaGen c:\\home\\schema");
			return false;
		}
			
		if(args.Length<=1)
			return true;
			
		return true;
	}
}
	/// <summary>
	/// </summary>
	class GenerateSchemaImp
	{
		const string SCHEMA_FILE= "schema-1.xsd";
		private DataModelFactory m_dmf = null;
		private string m_SchemaPath; 
		private string m_SchemaFile;
		public string SchemaPath{get{return m_SchemaPath;}set{m_SchemaPath = value;}}
		public string SchemaFile{get{return m_SchemaFile;}set{m_SchemaFile = value;}}

		public GenerateSchemaImp(UserLogin objLogin,string schemaPath)
		{
			m_dmf = new DataModelFactory(objLogin);
			m_SchemaPath = schemaPath;
			m_SchemaFile = SCHEMA_FILE;
		}


	public void GenerateSchema()
	{
		System.Reflection.Assembly a = System.Reflection.Assembly.GetAssembly(typeof(Riskmaster.DataModel.Event));
		Type[] types = a.GetTypes();
		using (StreamWriter w = File.CreateText(m_SchemaPath + m_SchemaFile))
		{
			w.Write(@"<?xml version=""1.0"" encoding=""utf-8""?>
				<xs:schema 
				elementFormDefault=""qualified"" 
				attributeFormDefault=""unqualified"" 
				xmlns:xs=""http://www.w3.org/2001/XMLSchema"" 
				xmlns:rm=""http://www.riskmaster.com/wizard""> 
				<xs:include schemaLocation=""RMInstanceTypes.xsd""  />");
			/*<xs:include schemaLocation=""RMControlTypes.xsd"" />*/
				
			foreach(Type t in types)
			{
				if(!t.IsClass)
					continue;
					
				if(t.BaseType.Name == "DataCollection")
					w.Write(GenerateListTypeSchema(t));
				else if(t.BaseType.Name != "DataObject" && t.BaseType.Name!="PersonInvolved" && t.BaseType.Name!="PersonInvolved" )
					continue;
				else
					w.Write(GenerateObjectSchema(t));
			}
			w.WriteLine("</xs:schema>");
				
		}//End Using Log File
		if(m_dmf!=null)
			m_dmf.UnInitialize();
		m_dmf =null;
	}

	#region Internal Functions to Implement Schema Generation.
	private static int Depth = 0;
	private string GenerateObjectSchema(Type t){return GenerateObjectSchema(t,"");}
	private string GenerateObjectSchema(Type t, string sProp)
	{
		Depth++;
		Trace.Write(t.Name);
		if(t.Name == "ClaimAdjuster")
			Depth=Depth;
			
		if(sProp=="")
			sProp = t.Name;

		string result = "";
		string stmp="";
		Riskmaster.DataModel.ExtendedTypeAttribute ext = null;
		object o = m_dmf.GetDataModelObject(t.Name,false);
		DataObject objData =    (o as DataObject);
		SupplementalObject objSuppBase =(o as SupplementalObject); 
		if(objSuppBase == null && objData ==null)
		{
			Depth--;
			return "";
		}
		if(objSuppBase!=null && Depth<=1)
		{
			Depth--;
			return "";
		}
		if(Depth>1)
		{
			result += String.Format("<xs:element name=\"{0}\" type=\"{1}-type\" minOccurs=\"0\" maxOccurs=\"1\"/>",sProp,t.Name);
			Depth--;
			return result;
		}
		else
		{
			result += String.Format("<xs:element name=\"{0}\" type=\"{1}-type\" />",sProp,t.Name);
			result += String.Format("<xs:complexType name=\"{0}-type\">",t.Name);
		}
		result += "<xs:sequence>";
			
		string s;
		//			foreach(string s in objData.Properties)
		foreach(DataObject.PropertyMetaInfo objInfo in objData.SerializationSequence)
		{
			s = objInfo.PropertyName;
			if(objData.OnTrimSerialization(s))
				continue;
			if(s =="Parent")
			{
				result += String.Format("<xs:element  name=\"Parent\" type=\"xs:string\" />");
				if(t.GetProperty("Supplementals") !=null)
					result +="<xs:element name=\"Supplementals\" type=\"Supplementals-Type\" minOccurs=\"0\" maxOccurs=\"1\"/>";

				continue;
			}
			if(s == "CurrentAdjuster")
				Depth=Depth;

			ext =GetExtendedTypeAttribute(objData,t,s);
				
			if(objData.HasChildProperty(s,true) || s =="Parent") //It's a sub-object
			{
				DataObject objChild = objData.GetProperty(s) as DataObject;
				if(objChild != null) //Check that child is a valid singleton
				{
					if(!(objChild.GetType().Name=="FundsAuto" && t.Name=="FundsAutoSplit")) //Cyclical Object Ref
						if(!(objChild.GetType().Name=="FundsAuto" && t.Name=="FundsAutoBatch")) //Cyclical Object Ref
							if(!(objChild.GetType().Name=="FundsAutoBatch" && t.Name=="FundsAuto")) //Cyclical Object Ref
								result +=GenerateObjectSchema(objChild.GetType(),s);
				}
				else //Property Returned Null but in this case we want all possibilities for the schema but this one must be 
					// Optional in instance data
				{
					Type objType = GetReturnType(objData,s);
					result +=GenerateObjectSchema(objType,s);
				}

				DataCollection objList = objData.GetProperty(s) as DataCollection;
				if(objList != null) //Check that child is a valid list
				{
					result += String.Format("<xs:element name=\"{0}\" minOccurs=\"0\" maxOccurs=\"1\"><xs:complexType>",s); // BSB Internal types must be anonymous ...
					//result += String.Format("<xs:element name=\"{0}\"><xs:complexType name=\"{1}-type\">",s,objList.GetType().Name);
					result += "<xs:sequence minOccurs=\"0\" maxOccurs=\"unbounded\">";
					//if(objList.GetTypeName()!=objData.GetType().Name)
					//	result += GenerateObjectSchema(dmf.GetDataModelObject(objList.GetTypeName(),false).GetType()) ;
					//else
					result += String.Format("<xs:element  name=\"{0}\" type=\"{1}-type\" />",objList.GetTypeName(),objList.GetTypeName()) ;
					result += "</xs:sequence>";
					result += "<xs:attribute name=\"count\" type=\"xs:int\"/>";
					result += "<xs:attribute name=\"committedcount\" type=\"xs:int\"/>";
					result += "</xs:complexType>";
					result += "</xs:element>";
				}
				DataSimpleList objSimpleList = objData.GetProperty(s) as DataSimpleList;
				if(objSimpleList != null)
				{
					result += "<xs:element name=\"" + s + "\" type=\"simpleList-type\">";//TODO - IMPLEMENT SIMPLELIST";
					result += "</xs:element>";
				}
			}
			else 
			{
				if(ext==null)
					stmp = "unspecified";
				else if(ext.FieldType == RMExtType.Code)
					stmp = "code";
				else if(ext.FieldType == RMExtType.CodeList)
					stmp = "codelist";
				else if(ext.FieldType==RMExtType.Entity)
					stmp = "entity";
				else if(ext.FieldType==RMExtType.EntityList)
					stmp = "entitylist";
				else if(ext.FieldType==RMExtType.OrgH)
					stmp = "orgh";
				else if(ext.FieldType==RMExtType.ChildLink)
					stmp = "childlink";
				
				else
					stmp="missing-RMExtType";

				result += "<xs:element name=\"" + s + "\" type=\"" + stmp + "\" />";
			}	
			
			//Suggest This Item be tagged with extended info...
			//				RecordSuggestion(objData, t,s);
		}// End For Each
			

		result += "</xs:sequence>";
		if(Depth>1)
			result += String.Format("</xs:element>");
		else
		{
			result += "<xs:attribute name=\"remove\" type=\"xs:string\" use=\"optional\"/>";
			result += String.Format("</xs:complexType>");
		}
		Depth--;
		return  result;
	}
	private string GenerateListTypeSchema(Type t)
	{
		string result = "";
		DataCollection objList = m_dmf.GetDataModelObject(t.Name,false) as DataCollection;
		if(objList != null) //Check that child is a valid list
		{
			result += String.Format("<xs:element name=\"{0}\" type=\"{1}-type\" />",t.Name,t.Name);
			result += String.Format("<xs:complexType name=\"{0}-type\">",t.Name);
			//	result += String.Format("<xs:element name=\"{0}\" minOccurs=\"0\" maxOccurs=\"1\"><xs:complexType>",s); // BSB Internal types must be anonymous ...
			//result += String.Format("<xs:element name=\"{0}\"><xs:complexType name=\"{1}-type\">",s,objList.GetType().Name);
			result += "<xs:sequence minOccurs=\"0\" maxOccurs=\"unbounded\">";
			result += String.Format("<xs:element  name=\"{0}\" type=\"{1}-type\" />",objList.GetTypeName(),objList.GetTypeName()) ;
			result += "</xs:sequence>";
			result += "<xs:attribute name=\"count\" type=\"xs:int\"/>";
			result += "<xs:attribute name=\"committedcount\" type=\"xs:int\"/>";
			result += "</xs:complexType>";
			//result += "</xs:element>";
		}
		return result;
	}
	private static ExtendedTypeAttribute GetExtendedTypeAttribute(DataObject objData, Type t, string s)
	{
		ExtendedTypeAttribute ext = null;
		MemberInfo inf=null;
		try{inf = (t.GetMember(s)[0]);}
		catch
		{ext = null;}
		try{ext =inf.GetCustomAttributes(typeof(Riskmaster.DataModel.ExtendedTypeAttribute),false)[0] as Riskmaster.DataModel.ExtendedTypeAttribute;}
		catch(IndexOutOfRangeException){}
		return ext;
	}
	private static Type GetReturnType(DataObject objData, string sProp)
	{
		try
		{
			return ((objData.GetType().GetMember(sProp)[0]) as PropertyInfo).GetGetMethod().ReturnType;
		}
		catch(IndexOutOfRangeException){return null;}
	}
}
	#endregion


