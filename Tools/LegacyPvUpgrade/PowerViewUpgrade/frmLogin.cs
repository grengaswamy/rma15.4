using System;
using System.Configuration;
using System.Windows.Forms;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;

namespace RiskmasterTools.PowerViewUpgrade
{
	/// <summary>
	/// Summary description for frmLogin.
	/// </summary>
	public class frmLogin : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtUserName;
		private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnOK;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private Login m_objLogin = null;
		private UserLogin m_objUser = null;
		private string m_sDSN = string.Empty;
		private string m_sUserID = string.Empty;
		private string m_sPassword = string.Empty;
		private System.Windows.Forms.Button btnExit;
		private bool m_bLogin = false;

		public frmLogin()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(24, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "User Name:";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(112, 56);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(160, 20);
            this.txtUserName.TabIndex = 1;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(112, 96);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(160, 20);
            this.txtPassword.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(24, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password:";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(38, 156);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(88, 32);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(160, 156);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 32);
            this.btnExit.TabIndex = 7;
            this.btnExit.Text = "Exit";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmLogin
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(304, 213);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label1);
            this.Name = "frmLogin";
            this.Text = "Login to Riskmaster";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void frmLogin_Load(object sender, System.EventArgs e)
		{
		}

        public string Dsn
        {
            get { return m_sDSN; }
            set { m_sDSN = value; }
        }

        private string m_ConnectionString;

        public string ConnectionString
        {
            get { return m_ConnectionString; }
            set { m_ConnectionString = value; }
        }

		private void btnOK_Click(object sender, System.EventArgs e)
		{
            try
            {
                //Check for entered values
                m_sUserID = txtUserName.Text.Trim();
                m_sPassword = txtPassword.Text.Trim();
                if (m_sUserID.Length == 0)
                {
                    MessageBox.Show("Please enter a valid user name.");
                    txtUserName.Focus();
                    return;
                }
                if (m_sPassword.Length == 0)
                {
                    MessageBox.Show("Please enter a valid password.");
                    txtPassword.Focus();
                    return;
                }

                //for R5.. security DSN would be picked from App Config and we would no longer depend on legacy ODBC dtgSecurity32 DSN
                //m_objLogin = new Login();

                string sSecurityConnectionString = ConfigurationManager.ConnectionStrings["RMXSecurity"].ConnectionString;
                m_objLogin = new Login(sSecurityConnectionString);
                m_bLogin = m_objLogin.AuthenticateUser(m_sDSN, m_sUserID, m_sPassword, out m_objUser);
                if (m_bLogin)
                {
                    m_ConnectionString = m_objUser.objRiskmasterDatabase.ConnectionString;
                } // if

                this.Close();
            }
            catch (OrgSecAccessViolationException)
            {
                MessageBox.Show("BES is enabled. Please use a BES admin group member account to upgrade PowerViews", "BES login error");
            }
            catch (Exception ex)
            {
                string sErrorMessage = ex.Message;
                Exception oInnerEx = ex.InnerException;
                while (oInnerEx != null)
                {
                    sErrorMessage += " InnerException: " + oInnerEx.Message;
                    oInnerEx = oInnerEx.InnerException;
                }
                sErrorMessage += ex.StackTrace;
                MessageBox.Show(sErrorMessage);
            }
		}

		private void btnExit_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		public string UserID
		{
			get{ return m_sUserID; }
		}

		public string Password
		{
			get{ return m_sPassword; }
		}

		public bool LoginSuccess
		{
			get{ return m_bLogin; }
		}
	}
}
