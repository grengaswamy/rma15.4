﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.IO;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.DataModel;
using System.Web.Security;
using Riskmaster.ExceptionTypes;
using System.Collections;
//using CCP.Common;
using CCP.Constants;
using CCP.XmlFormatting;
using CCP.XmlSupport;
using CCP.XmlComponents;
using Riskmaster.Security.RMApp;
using System.Diagnostics;
using System.Threading;
using Riskmaster.Common.Win32;
using Riskmaster.Security.Encryption;
using Riskmaster.Application.PolicySystemInterface;
using System.Configuration;
using C1.C1Zip;
using System.Linq;
using Riskmaster.Application.Reserves;
using Riskmaster.Application.IntegralSystemInterface;
//Payal: RMA-6471 --Starts
using System.Xml.Linq;
using System.Xml.XPath;
using PolicySystemUpate.IntegralInterfaceService;
//Payal: RMA-6471 --Ends
using System.Web;
using System.Text.RegularExpressions;
//vkumar258 RMA-7484 


namespace PolicySystemUpate
{
    public class Program
    {
        private  static UserLogin m_objUserLogin = null;
        private static DataModelFactory m_objFactory = null;
        private static string  RqId = string.Empty;
        private static string sSessionId = string.Empty;
        private static Dictionary<string, string> objEntityList = null;
        //private static Dictionary<string, string> objEventIdList = null;
        private static ArrayList objSplitList = null;
        private static int iPrevClaimantEID = 0;
        private static int iPrevReserveTypeCode=0;
        private static int iPrevCoverageId = 0;
        private static Thread MainThread;
        static string m_JobId = string.Empty;
        private static string sActivityRowId = string.Empty;
        private static int iPolicySystemId = 0;
        private static int iTransPolicyId = 0;
        private static int iTransClaimantId = 0;
        //private static int iReserveAdjusterId = 0;
        private static string sPolicyUploadPath = string.Empty;
        private static StringBuilder sbLogFile =null;
        private static string sDSNName = string.Empty;
        private static ArrayList objFundsList = null;
        private static ArrayList objRcRowIds = null;
        private static ArrayList objNoAddrInEntity = null;
        private static string sClientFileSetting = string.Empty;
        private static string sActivityRowIds = string.Empty;
        private static int m_iClientId = 0;
        //start : bsharma33 Code reverted RMA-9738
		//private static string sUseWriter = string.Empty;// RMA-9738:aaggarwal29//	
        //ends : bsharma33 Code reverted RMA-9738
        private static Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE? PolicySystemTypeIndicator;//vkumar258
        private static string sTotalReserveBalance = string.Empty;//vkumar258
        private static int iLlength = 0;//JIRA 6419 pkandhari
        private static int iFlength = 0;//JIRA 6419 pkandhari
        private static int iMlength = 0;//JIRA 6419 pkandhari
		 //syadav55 Jira 6419,6420 Starts
        private static int iAdd1length = 0;
        private static int iAdd2length = 0;
        private static int iAdd3length = 0;
        private static int iAdd4length = 0;
        private static ArrayList claimWithCorruptActivity = null;
        //syadav55 Jira 6419,6420 ends
        private static string sVoucherID = string.Empty; //Payal RMA:7484

        public struct RsvMoveHistKey
        {
            public int ClaimantEid;
            public int CvgLossRowId;
            public int PolCovSeq;
            public int LossCode;
            public int RsvTypeCode;
            public int PolCvgRowId;
        }
        private static void DisposeGlobalVariables()
        {

            m_objUserLogin = null;
            if (m_objFactory != null)
                m_objFactory.Dispose();
            objEntityList = null;
           // objEventIdList = null;
            objSplitList = null;
            objFundsList = null;
            sbLogFile = null;
            objRcRowIds = null;
            claimWithCorruptActivity = null;
                    }
        private static RsvMoveHistKey GetRsvMoveHistDetails(int iRcRowId, int iRsvMoveHistID)
        {
            RsvMoveHistKey objRsvMoveHist;
            try
            {
                objRsvMoveHist = new RsvMoveHistKey();
                using (DbReader objRdr = DbFactory.GetDbReader(m_objFactory.Context.DbConn.ConnectionString, "SELECT * FROM RESERVE_MOVE_HIST WHERE RESERVE_MOVE_HIST_ROW_ID = " + iRsvMoveHistID + " AND RC_ROW_ID=" + iRcRowId))
                {
                    if (objRdr.Read())
                    {
                        objRsvMoveHist.ClaimantEid = Conversion.ConvertObjToInt(objRdr.GetValue("CLAIMANT_EID"),m_iClientId);
                        objRsvMoveHist.CvgLossRowId = Conversion.ConvertObjToInt(objRdr.GetValue("POLCVG_LOSS_ROW_ID"),m_iClientId);
                        objRsvMoveHist.PolCovSeq = Conversion.ConvertObjToInt(objRdr.GetValue("POLCOVSEQ"),m_iClientId);
                        objRsvMoveHist.LossCode = Conversion.ConvertObjToInt(objRdr.GetValue("LOSS_CODE"),m_iClientId);
                        objRsvMoveHist.RsvTypeCode = Conversion.ConvertObjToInt(objRdr.GetValue("RESERVE_TYPE_CODE"),m_iClientId);
                        objRsvMoveHist.PolCvgRowId = Conversion.ConvertObjToInt(objRdr.GetValue("POLCVG_ROW_ID"),m_iClientId);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return objRsvMoveHist;
        }

        private static void GetSplitMoveHistData(int iSplitMoveHistKey,ref int splitRowId,ref int iRsvHistRowId)
        {
            try
            {
                using (DbReader objRdr = DbFactory.GetDbReader(m_objFactory.Context.DbConn.ConnectionString, "SELECT RESERVE_MOVE_HIST_ROW_ID,SPLIT_ROW_ID FROM SPLIT_MOVE_HIST WHERE SPLIT_MOVE_HIST_ROW_ID = " + iSplitMoveHistKey))
                {
                    if (objRdr.Read())
                    {
                        splitRowId = Conversion.ConvertObjToInt(objRdr.GetValue("SPLIT_ROW_ID"),m_iClientId);
                        iRsvHistRowId = Conversion.ConvertObjToInt(objRdr.GetValue("RESERVE_MOVE_HIST_ROW_ID"),m_iClientId);
                       
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static void Main(string[] args)
        {
            string sSQL = string.Empty;
            XmlDocument objDocument = null;
            XmlElement objRoot = null;

            List<byte[]> arrSessionBinary = null;
            string sSessionConnStr = string.Empty;
            UserLoginLimits objLimits = null;
            LocalCache objCache = null;
            StreamWriter objStream = null;
            string sPolicySystemName = string.Empty;
            string sFileName = string.Empty;
            bool bError = false;
            string sDbType = string.Empty;
            string sActivityFileName = string.Empty;
            string sCollectionStatusCode = string.Empty;
            StringBuilder sLogHeader = null;
            StreamReader streamUploadLog = null;
            string sPolicyUploadLog = string.Empty;
            string sClaimNumber = string.Empty; //Payal RMA-6471
            string sError = string.Empty;
            try
            {
                //args = new string[7];
                //args[0] = "RMA154_ACONPOLICY";
                //args[1] = "csc";
                //args[2] = "1";
                //args[3] = "0";
                //args[4] = "false";
                //args[5] = "0";
                //args[6] = "";   //mm/dd/yyyy

                m_objUserLogin = new UserLogin(args[1], args[0], m_iClientId);

                if (RMConfigurationManager.GetDictionarySectionSettings("FilePath", m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId) != null)
                {
                    sPolicyUploadPath = RMConfigurator.UserDataPath + "\\PolicyInterface\\PolicyUpload\\";
                }

                //m_objUserLogin = new UserLogin(args[1], args[0], m_iClientId);
                iPolicySystemId = Conversion.ConvertStrToInteger(args[2]);
                sPolicySystemName = GetPolicySystemName(args[2]);
                DataTable dt = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString,m_iClientId)).GetPolicySystemInfoById(iPolicySystemId);//vkumar258
                if (dt != null)
                {
                    PolicySystemTypeIndicator = CommonFunctions.GetPolicySystemTypeIndicator(dt.Rows[0]["POLICY_SYSTEM_TYPE"].ToString());//vkumar258
                    sPolicySystemName = dt.Rows[0]["POLICY_SYSTEM_NAME"].ToString();//vkumar258
                }
                if (!Directory.Exists(sPolicyUploadPath + "\\" + sPolicySystemName + "\\"))
                {
                    Directory.CreateDirectory(sPolicyUploadPath + "\\" + sPolicySystemName);
                }
                sbLogFile = new StringBuilder();
                Console.WriteLine("0 ^*^*^ Policy Update started ");
                sbLogFile.Append(" Policy Update started " + Environment.NewLine);
                sDSNName = args[0];
                if (m_objUserLogin.DatabaseId <= 0)
                {
                    Console.WriteLine("0 ^*^*^ Authentication Failed");
                    sbLogFile.Append(" Authentication Failed " + Environment.NewLine);
                    throw new Exception("Authentication failure.");
                }
                arrSessionBinary = new List<byte[]>();
                sSessionConnStr = RMConfigurationManager.GetConnectionString("SessionDataSource", m_iClientId);

                arrSessionBinary.Add(Riskmaster.Common.Utilities.BinarySerialize(m_objUserLogin));

                m_JobId = args[5];

                objLimits = new UserLoginLimits(m_objUserLogin, m_iClientId);

                arrSessionBinary.Add(Riskmaster.Common.Utilities.BinarySerialize(objLimits));
                
                sSessionId = Riskmaster.Common.RMSessionManager.CreateSession(sSessionConnStr, args[1], arrSessionBinary, m_iClientId);
       
                if(string.IsNullOrEmpty(sPolicySystemName))
                {
                    Console.WriteLine("0 ^*^*^ Policy System not found");
                    sbLogFile.Append(" Policy Sytem  not found " + Environment.NewLine);
                    throw new Exception("Policy System not found.");
                }

                Console.WriteLine("0 ^*^*^ Get Activity Track Data");
                sbLogFile.Append(" Get Activity Track Data " + Environment.NewLine);

                objDocument = new XmlDocument();
                objRoot = objDocument.CreateElement("CLAIMDOC");
                objDocument.AppendChild(objRoot);

                objDocument = SetHeader(objDocument,args[2]);
                objRcRowIds = new ArrayList();

                m_objFactory = new DataModelFactory(m_objUserLogin.objRiskmasterDatabase.DataSourceName, m_objUserLogin.LoginName, m_objUserLogin.Password, m_iClientId);
                objEntityList = new Dictionary<string, string>();

                //objEventIdList = new Dictionary<string, = new Dictionary<string, string>();string>(); 
                objSplitList = new ArrayList();
                objFundsList = new ArrayList();
                claimWithCorruptActivity = new ArrayList();
                objNoAddrInEntity = new ArrayList();
                objCache = new LocalCache(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);
                sClientFileSetting = GetClientFileSetting(iPolicySystemId.ToString());
                sDbType = m_objFactory.Context.DbConn.DatabaseType.ToString();
                //start : bsharma33 Code reverted RMA-9738
                //sUseWriter = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("PolicyUploadSettings", m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)["UseWriter"]);// RMA-9738:aaggarwal29 // 
                //ends : bsharma33 Code reverted RMA-9738
                if (string.Equals(Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("PolicyUploadSettings", m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)["UploadClaimsWithNonPrintedTransactions"]), "true", StringComparison.InvariantCultureIgnoreCase))
                {

                    sCollectionStatusCode = m_objFactory.Context.LocalCache.GetCodeId("R", "CHECK_STATUS") + "," + m_objFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS");

                    sSQL = @"SELECT DISTINCT ACTIVITY_TRACK.* FROM ACTIVITY_TRACK,POLICY,CLAIM,CLAIM_X_POLICY
                        WHERE ACTIVITY_TRACK.CLAIM_ID >0 AND ACTIVITY_TRACK.CLAIM_ID = CLAIM.CLAIM_ID AND 
                        CLAIM.CLAIM_ID = CLAIM_X_POLICY.CLAIM_ID AND CLAIM_X_POLICY.POLICY_ID = POLICY.POLICY_ID 
                        AND ACTIVITY_TRACK.UPLOAD_FLAG = -1  AND ACTIVITY_TRACK.POLICY_SYSTEM_ID=" + iPolicySystemId + " AND POLICY.POLICY_SYSTEM_ID = " + iPolicySystemId;
                       //  + " AND ((CHECK_STATUS in (0," + m_objFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS") + ") AND ((ACTIVITY_TRACK.IS_COLLECTION <> 1 ) OR  (ACTIVITY_TRACK.IS_COLLECTION IS NULL))) OR ((ACTIVITY_TRACK.CHECK_STATUS  IN ("+sCollectionStatusCode+") AND ACTIVITY_TRACK.IS_COLLECTION = 1)))";

                    if ((args.Length==7) &&  !string.IsNullOrEmpty(args[6]))
                        sSQL = sSQL + " AND ACTIVITY_TRACK.DTTM_RCD_ADDED <= " + Conversion.GetDateTime(args[6]);

                         sSQL = sSQL +" ORDER BY ACTIVITY_TRACK.ACTIVITY_ROW_ID";

                         using (DbReader objReader = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL))
                         {
                             while (objReader.Read())
                             {
                                 if (claimWithCorruptActivity.Contains(objReader.GetValue("CLAIM_ID")))
                                     continue;
                                 else
                                 {
                                     if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.POINT) //Payal RMA:7484 
                                     {
                                         if (VerifyActivity(Conversion.ConvertObjToInt(objReader.GetValue("FOREIGN_TABLE_KEY"), m_iClientId), Conversion.ConvertObjToInt(objReader.GetValue("FOREIGN_TABLE_ID"), m_iClientId), Conversion.ConvertObjToInt(objReader.GetValue("CHECK_STATUS"), m_iClientId), Conversion.ConvertObjToBool(objReader.GetValue("IS_COLLECTION"), m_iClientId), objCache, Riskmaster.Common.Conversion.ConvertObjToBool(objReader.GetValue("VOID_FLAG"), m_iClientId)))
                                         {
                                             if (Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), m_iClientId) == 0)
                                                 objDocument = CreateClaimNodeForRealTimeSystem(objDocument, Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("FCLAIM"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("FTRANS"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("ACTIVITY_ROW_ID"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("ACTIVITY_TYPE"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("RESERVE_AMOUNT"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_STATUS"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("CHANGE_AMOUNT"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToBool(objReader.GetValue("VOID_FLAG"), m_iClientId), Conversion.ConvertObjToInt(objReader.GetValue("MOVE_HIST_TABLE_ID"), m_iClientId), Conversion.ConvertObjToInt(objReader.GetValue("MOVE_HIST_TABLE_KEY"), m_iClientId));
                                             else
                                                 objDocument = CreateClaimNodeForBatch(objDocument, Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("FOREIGN_TABLE_KEY"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("FOREIGN_TABLE_ID"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("ACTIVITY_ROW_ID"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("ACTIVITY_TYPE"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("RESERVE_AMOUNT"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_STATUS"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("CHANGE_AMOUNT"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToBool(objReader.GetValue("VOID_FLAG"), m_iClientId), sTotalReserveBalance, Conversion.ConvertObjToInt(objReader.GetValue("MOVE_HIST_TABLE_ID"), m_iClientId), Conversion.ConvertObjToInt(objReader.GetValue("MOVE_HIST_TABLE_KEY"), m_iClientId));
                                         }
                                     }
                                     //Payal RMA:7484 starts 
                                     else
                                     {
                                         if (Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), m_iClientId) == 0)
                                             objDocument = CreateClaimNodeForRealTimeSystem(objDocument, Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("FCLAIM"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("FTRANS"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("ACTIVITY_ROW_ID"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("ACTIVITY_TYPE"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("RESERVE_AMOUNT"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_STATUS"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("CHANGE_AMOUNT"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToBool(objReader.GetValue("VOID_FLAG"), m_iClientId), Conversion.ConvertObjToInt(objReader.GetValue("MOVE_HIST_TABLE_ID"), m_iClientId), Conversion.ConvertObjToInt(objReader.GetValue("MOVE_HIST_TABLE_KEY"), m_iClientId));
                                         else
                                             objDocument = CreateClaimNodeForBatch(objDocument, Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("FOREIGN_TABLE_KEY"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("FOREIGN_TABLE_ID"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("ACTIVITY_ROW_ID"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("ACTIVITY_TYPE"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("RESERVE_AMOUNT"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_STATUS"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("CHANGE_AMOUNT"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToBool(objReader.GetValue("VOID_FLAG"), m_iClientId), sTotalReserveBalance, Conversion.ConvertObjToInt(objReader.GetValue("MOVE_HIST_TABLE_ID"), m_iClientId), Conversion.ConvertObjToInt(objReader.GetValue("MOVE_HIST_TABLE_KEY"), m_iClientId));
                                     }
                                     //Payal RMA:7484 ends
                                 }
                             }
                         }
                    objDocument = CreateVouchersNode(objDocument);
                    objDocument = CreateEntitiesNode(objDocument, sClientFileSetting);

                    objDocument = SetMessage(objDocument);

                }
                else
                {
                    if (RMConfigurationManager.GetDictionarySectionSettings("PolicyUploadSettings", m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId) != null)
                    {
                        if (string.Equals(Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("PolicyUploadSettings", m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)["UploadNonPrintedCollections"]), "true", StringComparison.InvariantCultureIgnoreCase))
                        {
                            sCollectionStatusCode = m_objFactory.Context.LocalCache.GetCodeId("R", "CHECK_STATUS") + "," + m_objFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS");
                        }
                        else
                        {
                            sCollectionStatusCode = m_objFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS").ToString();
                        }
                    }
                    else
                    {
                        sCollectionStatusCode = m_objFactory.Context.LocalCache.GetCodeId("R", "CHECK_STATUS").ToString();
                    }

                sSQL = @"SELECT DISTINCT ACTIVITY_TRACK.* FROM ACTIVITY_TRACK,POLICY,CLAIM,CLAIM_X_POLICY
                        WHERE ACTIVITY_TRACK.CLAIM_ID >0 AND ACTIVITY_TRACK.CLAIM_ID = CLAIM.CLAIM_ID AND 
                        CLAIM.CLAIM_ID = CLAIM_X_POLICY.CLAIM_ID AND CLAIM_X_POLICY.POLICY_ID = POLICY.POLICY_ID 
                        AND ACTIVITY_TRACK.UPLOAD_FLAG = -1  AND ACTIVITY_TRACK.POLICY_SYSTEM_ID="+iPolicySystemId+" AND POLICY.POLICY_SYSTEM_ID = "+ iPolicySystemId
                        +" AND ACTIVITY_TRACK.CLAIM_ID NOT IN ("
                        + "SELECT CLAIM_ID FROM ACTIVITY_TRACK WHERE UPLOAD_FLAG=-1 AND POLICY_SYSTEM_ID = " + iPolicySystemId + " AND ((ACTIVITY_TRACK.CHECK_STATUS NOT IN (0," + m_objFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS") + ") AND ACTIVITY_TRACK.VOID_FLAG=0 AND ((ACTIVITY_TRACK.IS_COLLECTION <> 1 ) OR (ACTIVITY_TRACK.IS_COLLECTION IS NULL))) OR ((ACTIVITY_TRACK.CHECK_STATUS NOT IN (" + sCollectionStatusCode + ") AND ACTIVITY_TRACK.IS_COLLECTION = 1)))"
                    + ")";


                if ((args.Length == 7) && !string.IsNullOrEmpty(args[6]))
                        sSQL = sSQL + " AND ACTIVITY_TRACK.DTTM_RCD_ADDED <= " + Conversion.GetDateTime(args[6]);
                    
                    
                    sSQL = sSQL + " ORDER BY ACTIVITY_TRACK.ACTIVITY_ROW_ID";

                using (DbReader objReader = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL))
                       {
                           while (objReader.Read())
                           {
                               if (claimWithCorruptActivity.Contains(objReader.GetValue("CLAIM_ID")))
                                   continue;
                               else
                               {
                               if (!(Conversion.ConvertObjToInt(objReader.GetValue("CHECK_STATUS"), m_iClientId) == m_objFactory.Context.LocalCache.GetCodeId("R", "CHECK_STATUS")))
                               {
                                   if (Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), m_iClientId) == 0)
                                       objDocument = CreateClaimNodeForRealTimeSystem(objDocument, Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("FCLAIM"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("FTRANS"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("ACTIVITY_ROW_ID"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("ACTIVITY_TYPE"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("RESERVE_AMOUNT"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_STATUS"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("CHANGE_AMOUNT"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToBool(objReader.GetValue("VOID_FLAG"), m_iClientId), Conversion.ConvertObjToInt(objReader.GetValue("MOVE_HIST_TABLE_ID")), Conversion.ConvertObjToInt(objReader.GetValue("MOVE_HIST_TABLE_KEY")));
                                   else
                                       objDocument = CreateClaimNodeForBatch(objDocument, Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("FOREIGN_TABLE_KEY"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("FOREIGN_TABLE_ID"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("ACTIVITY_ROW_ID"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("ACTIVITY_TYPE"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("RESERVE_AMOUNT"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_STATUS"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToDouble(objReader.GetValue("CHANGE_AMOUNT"), m_iClientId), Riskmaster.Common.Conversion.ConvertObjToBool(objReader.GetValue("VOID_FLAG"), m_iClientId), sTotalReserveBalance, Conversion.ConvertObjToInt(objReader.GetValue("MOVE_HIST_TABLE_ID"),m_iClientId), Conversion.ConvertObjToInt(objReader.GetValue("MOVE_HIST_TABLE_KEY"),m_iClientId));
                                   }
                               }
                           }
                    }
                    //  objDocument = CreateEventNode(objDocument);
                    objDocument = CreateVouchersNode(objDocument);
                    objDocument = CreateEntitiesNode(objDocument, sClientFileSetting);

                    objDocument = SetMessage(objDocument);
                    sSQL = @"SELECT CLAIM_NUMBER FROM ACTIVITY_TRACK,CLAIM WHERE UPLOAD_FLAG=-1 AND POLICY_SYSTEM_ID = " + iPolicySystemId + " AND ACTIVITY_TRACK.CHECK_STATUS NOT IN (0," + m_objFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS") + ") AND ACTIVITY_TRACK.VOID_FLAG=0 AND CLAIM.CLAIM_ID = ACTIVITY_TRACK.CLAIM_ID AND ((ACTIVITY_TRACK.IS_COLLECTION <> 1 ) OR (ACTIVITY_TRACK.IS_COLLECTION IS NULL))";

                    using (DbReader objReader = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL))
                {
                    while (objReader.Read())
                    {
                        Console.WriteLine("0 ^*^*^ Claim Number " + Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER")) + " could not be uploaded due to its transactions are in non printed state ");
                        sbLogFile.Append(" Claim number " + Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER")) + " could not be uploaded due to its transactions are in non printed state " + Environment.NewLine);  
                    }
                }


                    sSQL = @"SELECT CLAIM_NUMBER FROM ACTIVITY_TRACK,CLAIM WHERE UPLOAD_FLAG=-1 AND POLICY_SYSTEM_ID = " + iPolicySystemId + " AND ACTIVITY_TRACK.CHECK_STATUS NOT IN (" + m_objFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS") + ") AND ACTIVITY_TRACK.VOID_FLAG=0 AND CLAIM.CLAIM_ID = ACTIVITY_TRACK.CLAIM_ID AND ((ACTIVITY_TRACK.IS_COLLECTION = 1 ))";

                using (DbReader objReader = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL))
                {
                    while (objReader.Read())
                    {
                        Console.WriteLine("0 ^*^*^ Claim Number " + Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER")) + " could not be uploaded due to its transactions are in non printed state ");
                        sbLogFile.Append(" Claim number " + Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER")) + " could not be uploaded due to its transactions are in non printed state " + Environment.NewLine);  
                    }
                }
                }
                sFileName = "POLICYUPLOAD_" + m_objUserLogin.LoginName + "_"+System.DateTime.Now.ToString("yyyyMMddHHmmss")+".XML";
                if (File.Exists(RMConfigurator.AppFilesPath + "\\Policy Interface\\" + sFileName))
                    File.Delete(RMConfigurator.AppFilesPath + "\\Policy Interface\\" + sFileName);

                sActivityFileName = RMConfigurator.AppFilesPath + "\\Policy Interface\\ActivityIds_" + sFileName.Replace(".XML", ".txt");
                if (File.Exists(sActivityFileName))
                    File.Delete(sActivityFileName);
                objStream = new StreamWriter(RMConfigurator.AppFilesPath + "\\Policy Interface\\" + sFileName);
                Console.WriteLine("0 ^*^*^ XML File Created");
                sbLogFile.Append(" XML file created " + Environment.NewLine);
                objStream.Write(objDocument.InnerXml);
                objStream.Close();
                Console.WriteLine("0 ^*^*^ Upload data to " + sPolicySystemName);
                sbLogFile.Append(" upload data to " + sPolicySystemName + Environment.NewLine);

                //Header for PolicyUploadLog
                sLogHeader = new StringBuilder();
                sLogHeader.AppendLine("Timestamp : " + DateTime.Now.ToString());
                sLogHeader.AppendLine("Point policy system : " + sPolicySystemName);
                sLogHeader.AppendLine("RM database : " + args[0]);
                sLogHeader.AppendLine("Upload XML File : " + sFileName);
                sLogHeader.AppendLine("------------------------------------------------------------------------------------------------");
                //Payal: RMA-6471 --Starts
                /*
                if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.POINT)//vkumar258
                {
                    CallCCPCPolicyUpload(args[2], sFileName);
                }
                */
               
                switch (PolicySystemTypeIndicator)
                {
                    case Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.POINT:
                        CallCCPCPolicyUpload(args[2], sFileName);
                        break;
                    case Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.INTEGRAL:
                        string allactivityids = string.Empty;
                        //Payal starts --RMA:7484
                        if (File.Exists(RMConfigurator.AppFilesPath + "\\Policy Interface\\" + sFileName))
                            File.Move(RMConfigurator.AppFilesPath + "\\Policy Interface\\" + sFileName, RMConfigurator.AppFilesPath + "\\Policy Interface\\BKUP" + sFileName);
                        //Payal Ends --RMA:7484
                        IntegralInterfaceServiceClient rmservice = new IntegralInterfaceServiceClient();
                        UploadClaimData oclaimData = new UploadClaimData();
                        oclaimData.Token = sSessionId;
                        oclaimData.PolicySystemID = iPolicySystemId;
                        oclaimData.UploadXML = objDocument.InnerXml;
                        rmservice.UploadClaimData(ref oclaimData);
                        foreach (PolicySystemResponse oRes in oclaimData.PolicySystemResponses)
                        {
                            if (oRes.Response)
                            {
                                if (string.IsNullOrEmpty(sActivityRowIds))
                                    sActivityRowIds = oRes.ActivityID;
                                else
                                    sActivityRowIds = sActivityRowIds + ',' + oRes.ActivityID;
                            }
                            else if (!oRes.Response)
                            {
                               
                                    sSQL = @"SELECT CLAIM_NUMBER FROM ACTIVITY_TRACK,CLAIM WHERE  CLAIM.CLAIM_ID = ACTIVITY_TRACK.CLAIM_ID AND ACTIVITY_TRACK.ACTIVITY_ROW_ID= " + oRes.ActivityID + " ";
                                    using (DbReader objReader = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL))
                                    {
                                        while (objReader.Read())
                                        {
                                            sClaimNumber = Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER"));
                                        }
                                    }
                                    Console.WriteLine("Claim Number:" + sClaimNumber + " Activity Row Id:" + oRes.ActivityID + " could not be uploaded due to the " + oRes.ResponseMsg + Environment.NewLine);  
                                sbLogFile.Append("Claim Number:" + sClaimNumber + " Activity Row Id:" + oRes.ActivityID + " could not be uploaded due to the " + oRes.ResponseMsg + Environment.NewLine);
                                    sError = "Error";
                                
                            }
                        }
                        UpdateActivityIDS();
                        if (sError == "Error")
                        {
                            Console.WriteLine("0 ^*^*^ Error while uploading");
                            sbLogFile.Append(" Error while uploading " + Environment.NewLine);
                            throw new Exception("Error while Uploading");
                        }
                        break;
                    default:
                            break;
                      
                   
                }
               

                //Payal: RMA-6471 --Ends
                    Console.WriteLine("0 ^*^*^ Data uploaded to " + sPolicySystemName);
                    sbLogFile.Append(" Data uploaded to " + sPolicySystemName + " (Refer upload log below) "  +Environment.NewLine);
                if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.POINT) //Payal: RMA-6471
                {
                    if (File.Exists(sPolicyUploadPath + sPolicySystemName + "\\PointClaimsLoad_" + sFileName + ".log"))
                {
                    string sLogFileContent = string.Empty;
                    using (StreamReader objStrReader = new StreamReader(sPolicyUploadPath + sPolicySystemName + "\\PointClaimsLoad_" + sFileName + ".log"))
                    {

                         sLogFileContent = objStrReader.ReadToEnd();

                        }
                    if (string.IsNullOrEmpty(sActivityRowIds))
                    {
                        sActivityRowIds = " : None";
                    }
                        sbLogFile.Append(" Uploaded Activity Row Ids are " + sActivityRowIds + Environment.NewLine);
                        if (sLogFileContent.Contains("Error"))
                        {
                            Console.WriteLine("0 ^*^*^ Error while uploading");
                            sbLogFile.Append(" Error while uploading " + Environment.NewLine);
                            throw new Exception("Error while Uploading");
                        }
                    }
                } 
                //Payal: RMA-6471 --Starts
                if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.INTEGRAL)
                {
                    if (string.IsNullOrEmpty(sActivityRowIds))
                    {
                        sActivityRowIds = " : None";
                    }
                    sbLogFile.Append("Successful Uploaded Activity Row Ids are " + sActivityRowIds + Environment.NewLine);



                } 
                //Payal: RMA-6471 --Ends

                objStream = null;
            }
            catch (Exception exc)
            {
                Console.WriteLine("1001 ^*^*^ {0} ", exc.Message + " " + exc.InnerException);
                bool rethrow = ExceptionPolicy.HandleException(exc, "Logging Policy");
            }
            finally
            {
                if (File.Exists(sPolicyUploadPath + sPolicySystemName + "\\PolicySystemUpdate.log"))
                    File.Delete(sPolicyUploadPath + sPolicySystemName + "\\PolicySystemUpdate.log");
                  objStream = new StreamWriter(sPolicyUploadPath + sPolicySystemName + "\\PolicySystemUpdate.log");

                  if (File.Exists(sPolicyUploadPath + sPolicySystemName + "\\PointClaimsLoad_" + sFileName + ".log"))
                  {
                      streamUploadLog = new StreamReader(sPolicyUploadPath + sPolicySystemName + "\\PointClaimsLoad_" + sFileName + ".log");
                      sPolicyUploadLog = streamUploadLog.ReadToEnd();
                      streamUploadLog.Close();
                      File.Delete(sPolicyUploadPath + sPolicySystemName + "\\PointClaimsLoad_" + sFileName + ".log");
                  }

                  objStream.Write(sLogHeader + sbLogFile.ToString() + Environment.NewLine + sPolicyUploadLog);
                objStream.Close();

                if (File.Exists(sPolicyUploadPath + sPolicySystemName + "\\PolicySystemUpdate.log"))
                {
                    FileToDatabase(RMConfigurator.AppFilesPath + "\\Policy Interface\\BKUP" + sFileName, sPolicyUploadPath + sPolicySystemName + "\\PolicySystemUpdate.log", iPolicySystemId.ToString());   
                }

                DisposeGlobalVariables();
                arrSessionBinary = null;
                       objDocument = null;
             objRoot = null;
             objNoAddrInEntity = null;
             arrSessionBinary = null;
             objLimits = null;
             if(objCache!=null)
                objCache.Dispose();
             if (objStream != null)
             {
                 objStream.Close();
                 objStream = null;
             }
             streamUploadLog = null;
            }
        }
        
        public static bool VerifyActivity(int iForeignTableKey,int iForeignTableId,int iCheckStatus,bool bIsCollection, LocalCache objCache,bool bVoid)
        {
            bool bReturnValue = true;
            
            if (iForeignTableKey > 0)
            {
                if (objRcRowIds == null)
                    objRcRowIds = new ArrayList();

               switch (objCache.GetTableName(iForeignTableId))
                {
                    case "RESERVE_CURRENT":
                        if (objRcRowIds.Contains(iForeignTableKey))
                        {
                            bReturnValue = false;
                        }
                        break;
                    case "FUNDS":
                        Funds objFunds = (Funds)m_objFactory.GetDataModelObject("Funds", false);
                                objFunds.MoveTo(iForeignTableKey);
                        foreach (FundsTransSplit objSplit in objFunds.TransSplitList)
                        {
                            if (objRcRowIds.Contains(objSplit.RCRowId))
                            {
                                bReturnValue = false;
                                break;
                            }
                            if ((iCheckStatus != objCache.GetCodeId("P", "CHECK_STATUS") && !bIsCollection && bVoid))
                            {
                                bReturnValue = false;
                                break;
                            }

                            if ((iCheckStatus != objCache.GetCodeId("P", "CHECK_STATUS") && !bIsCollection) || (((iCheckStatus != objCache.GetCodeId("R", "CHECK_STATUS")) && (iCheckStatus != objCache.GetCodeId("P", "CHECK_STATUS")) && bIsCollection)))
                            {
                                bReturnValue = false;
                                if (!objRcRowIds.Contains(objSplit.RCRowId))
                                {
                                    objRcRowIds.Add(objSplit.RCRowId);
                                }
                            }

                        }
                        break;

                }
               

            }
            return bReturnValue;
        }

        public static string GetReportedDate(int iClaimId,int iEventId)
        {
            string sReportedDate = string.Empty;
            string sSQL=string.Empty;
            string sColumnName=string.Empty;
            string sTableName = string.Empty;
            
            try
            {
                sTableName = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("IncidentReportedDate", m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)["table"]);
                sColumnName = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("IncidentReportedDate", m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)["column"]);
                switch (sTableName.ToUpper())
                {
                    case "CLAIM":
                    case "CLAIM_SUPP":
                        sSQL = " SELECT " + sColumnName + " FROM "+ sTableName+ " WHERE CLAIM_ID = " + iClaimId;
                        break;
                    case "EVENT":
                    case "EVENT_SUPP":
                        sSQL = " SELECT " + sColumnName + " FROM " + sTableName + " WHERE EVENT_ID =  " + iEventId;
                        break;
                }

                using (DbReader objrdr = DbFactory.GetDbReader(m_objFactory.Context.DbConn.ConnectionString, sSQL))
                {
                    if (objrdr.Read())
                    {
                        sReportedDate = Riskmaster.Common.Conversion.GetDate(Conversion.ConvertObjToStr(objrdr.GetValue(0)));
                    }

                }

            }
            catch(Exception e)
            {
                Console.WriteLine("0 ^*^*^ Error occured while getting reported date.");
                sbLogFile.Append(e.Message);

                sReportedDate = string.Empty;
            }

            return sReportedDate;
        }

        public static string GetReportedTime(int iClaimId, int iEventId)
        {
            string sReportedTime = "120000";    //Ankit Start : Worked for MITS - 34509
            string sSQL = string.Empty;
            string sColumnName = string.Empty;
            string sTableName = string.Empty;

            try
            {
                sTableName = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("IncidentReportedTime", m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)["table"]);
                sColumnName = Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("IncidentReportedTime", m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)["column"]);
                switch (sTableName.ToUpper())
                {
                    case "CLAIM":
                    case "CLAIM_SUPP":
                        sSQL = " SELECT " + sColumnName + " FROM " + sTableName + " WHERE CLAIM_ID = " + iClaimId;
                        break;
                    case "EVENT":
                    case "EVENT_SUPP":
                        sSQL = " SELECT " + sColumnName + " FROM " + sTableName + " WHERE EVENT_ID =  " + iEventId;
                        break;
                }

                using (DbReader objrdr = DbFactory.GetDbReader(m_objFactory.Context.DbConn.ConnectionString, sSQL))
                {
                    if (objrdr.Read())
                    {
                        if (!string.IsNullOrEmpty(Riskmaster.Common.Conversion.GetTime(Conversion.ConvertObjToStr(objrdr.GetValue(0)))) && !string.Equals(Riskmaster.Common.Conversion.GetTime(Conversion.ConvertObjToStr(objrdr.GetValue(0))), "000000"))      //Ankit Start : Worked for MITS - 34509
                            sReportedTime = Riskmaster.Common.Conversion.GetTime(Conversion.ConvertObjToStr(objrdr.GetValue(0)));
                    }

                }

            }
            catch (Exception e)
            {
                Console.WriteLine("0 ^*^*^ Error occured while getting reported date.");
                sbLogFile.Append(e.Message);

                sReportedTime = string.Empty;
            }

            return sReportedTime;
        }

        public static void FileToDatabase(string sXMLfilePath, string sCsharplogfilePath, string sPolicySystemID)
        {
            FileStream stream = null;
            byte[] fileData = null;
            C1ZipFile ZipFile = null;
            string sZipFilePath = string.Empty;
            string sZipFileName = string.Empty;
            try
            {
                sZipFilePath = RMConfigurator.BasePath + "//temp//PolicyUpload";
                sZipFileName = Conversion.ToDbDateTime(DateTime.Now) + "_" + sPolicySystemID + ".zip"; 
                ZipFile = new C1ZipFile(sZipFilePath+ "//" + sZipFileName);

                if (File.Exists(sCsharplogfilePath))
                {
                    ZipFile.Entries.Add(sCsharplogfilePath);
                }
                if (File.Exists(sXMLfilePath))
                {
                    ZipFile.Entries.Add(sXMLfilePath);
                }
                ZipFile.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("0 ^*^*^ Error occured in reading zip file.");
                sbLogFile.Append(" Error occured in reading zip file " + Environment.NewLine);
                throw e;
            }
            finally
            {
                if (ZipFile != null)
                {
                    ZipFile.Close();
                    ZipFile = null;
                }
            }
                try
                {
                    stream = new FileStream(sZipFilePath + "//" + sZipFileName, FileMode.Open, FileAccess.Read);
                    fileData = new byte[stream.Length];
                    stream.Read(fileData, 0, (int)stream.Length);
                    stream.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine("0 ^*^*^ Error occured in reading zip file.");
                    throw e;
                }
                finally
                {
                    stream.Close();
                    stream.Dispose();
                }
                try
                {
                    Console.WriteLine("0 ^*^*^ Attach file to database.");
                    using (DbConnection dbConnection = DbFactory.GetDbConnection(RMConfigurationManager.GetConnectionString("TaskManagerDataSource", m_iClientId)))
                    {
                        dbConnection.Open();
                        DbCommand cmdInsert = dbConnection.CreateCommand();
                        DbParameter paramFileId = cmdInsert.CreateParameter();
                        DbParameter paramJobId = cmdInsert.CreateParameter();
                        DbParameter paramFileName = cmdInsert.CreateParameter();
                        DbParameter paramFileData = cmdInsert.CreateParameter();
                        DbParameter paramContentType = cmdInsert.CreateParameter();
                        DbParameter paramLength = cmdInsert.CreateParameter();

                        cmdInsert.CommandText = "INSERT INTO TM_JOBS_DOCUMENT " +
                            "(TM_FILE_ID, JOB_ID, FILE_NAME, FILE_DATA, CONTENT_TYPE, CONTENT_LENGTH) VALUES " +
                            "(~FileId~, ~JobId~, ~FileName~, ~FileData~, ~ContentType~, ~ContentLength~)";

                        paramFileId.Direction = ParameterDirection.Input;
                        paramFileId.Value = int.Parse(m_JobId);
                        //paramFileId.Value = JobId;
                        paramFileId.ParameterName = "FileId";
                        paramFileId.SourceColumn = "TM_FILE_ID";
                        cmdInsert.Parameters.Add(paramFileId);

                        paramJobId.Direction = ParameterDirection.Input;
                        paramJobId.Value = int.Parse(m_JobId);
                        //paramJobId.Value = JobId;
                        paramJobId.ParameterName = "JobId";
                        paramJobId.SourceColumn = "JOB_ID";
                        cmdInsert.Parameters.Add(paramJobId);

                        paramFileName.Direction = ParameterDirection.Input;
                        paramFileName.Value = sZipFileName;
                        paramFileName.ParameterName = "FileName";
                        paramFileName.SourceColumn = "FILE_NAME";
                        cmdInsert.Parameters.Add(paramFileName);

                        paramFileData.Direction = ParameterDirection.Input;
                        paramFileData.Value = fileData;
                        paramFileData.ParameterName = "FileData";
                        paramFileData.SourceColumn = "FILE_DATA";
                        cmdInsert.Parameters.Add(paramFileData);

                        paramContentType.Direction = ParameterDirection.Input;
                        paramContentType.Value = "";
                        paramContentType.ParameterName = "ContentType";
                        paramContentType.SourceColumn = "CONTENT_TYPE";
                        cmdInsert.Parameters.Add(paramContentType);

                        paramLength.Direction = ParameterDirection.Input;
                        paramLength.Value = fileData.Length;
                        paramLength.ParameterName = "ContentLength";
                        paramLength.SourceColumn = "CONTENT_LENGTH";
                        cmdInsert.Parameters.Add(paramLength);

                        cmdInsert.ExecuteNonQuery();

                    }
                    Console.WriteLine("0 ^*^*^ File attached to database successfully.");
                    if (File.Exists(sXMLfilePath))
                        File.Delete(sXMLfilePath);

                    if (File.Exists(sZipFilePath + "//" + sZipFileName))
                        File.Delete(sZipFilePath + "//" + sZipFileName);
                }
                catch (System.Exception e)
                {
                    Console.WriteLine("0 ^*^*^ Error occured in saving zip file to database.");
                    throw e;
                }

                finally
                {
                    if (fileData != null)
                    {
                        fileData = null;
                    }
                }
            
        }
        private static XmlDocument CreateClaimNodeForRealTimeSystem(XmlDocument objDoc, int iClaimId, int iTransId, int iActivityRowId, int iActivityType, double iReserveAmount, int iReserveStatus, double iChangeAmount, bool bVoidFlag,int iMoveHistTableId,int iMoveHistTableKey)
        {
            XMLClaim objXMLCLaim = null;
            string sLOBType = string.Empty;
            string sLOBShortCode = string.Empty;
            string sClaimType = string.Empty;
            string sClaimTypeCode = string.Empty;

            string sClaimStatus = string.Empty;
            string sClaimStatusCode = string.Empty;

            string sEventType = string.Empty;
            string sEventTypeCode = string.Empty;
            XmlNode objtemp = null;
            Claim objClaim = null;
            int iCurrClaimId = 0;
            int iCurrTransId = 0;
           string sDateofLoss= string.Empty;
           int iClaimTypeCodeId = 0;
           int iLobCodeId = 0;
           int iClaimStatusCodeId = 0;
           int iEventStateCode = 0;
           int iEventTypeCodeId = 0;
            PolicySystemInterface objPolSystemInterface = new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString,m_iClientId);
            IntegralSystemInterface objIntgSystemInterface = new IntegralSystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString,m_iClientId);
            //Payal RMA:7484 Starts
            string sActivityType = string.Empty;
            string sDateOfTransaction = string.Empty;
            string sTimeofTransaction = string.Empty;
            //Payal RMA:7484 Ends

            try
            {
                objXMLCLaim = new XMLClaim();
                    iCurrClaimId = iClaimId;
                    iCurrTransId = iTransId;
                    objClaim = (Claim)m_objFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(iClaimId);
                    if (objClaim.ClaimNumber.Length > 12)
                    {
                        Console.WriteLine("0 ^*^*^ Claim Number " + objClaim.ClaimNumber + " length cannot be greater than 12");
                        sbLogFile.Append(" Claim number " + objClaim.ClaimNumber + "length cannot be greater than 12 characters   " + Environment.NewLine);
                        throw new Exception("Claim Number length cannot be greater than 12 characters.");
                    }
                    if (string.IsNullOrEmpty(sActivityRowId))
                        sActivityRowId = iActivityRowId.ToString();
                    else
                        sActivityRowId = sActivityRowId + "," + iActivityRowId.ToString();


                    string sStateShortCode = string.Empty;
                    string sState = string.Empty;
                //iClaimStatusCodeId = objClaim.Context.LocalCache.GetRelatedCodeId(objClaim.ClaimStatusCode);
                iClaimStatusCodeId = objIntgSystemInterface.GetPSMappedCodeIDFromRMXCodeId(objClaim.ClaimStatusCode, "CLAIM_STATUS", "Claim Status Code on Create Claim Node", iPolicySystemId.ToString());
                iClaimTypeCodeId = objPolSystemInterface.GetPSMappedCode(objClaim.ClaimTypeCode, "CLAIM_TYPE", "ClaimType on CreateClaimNodeForRealTimeSystem", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode); //MITS 32000 : aaggarwal29
        
                    if (objClaim.ClaimDescriptionCode > 0)
                    {
                    iEventTypeCodeId = objPolSystemInterface.GetPSMappedCode(objClaim.ClaimDescriptionCode, "CLAIM_DESCRIPTION", "ClaimDescriptionCode on CreateClaimNodeForBatch", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode); //MITS 32000 : aaggarwal29
                        objClaim.Context.LocalCache.GetCodeInfo(iEventTypeCodeId, ref sEventType, ref sEventTypeCode);


                    }
                    else
                    {
                        sEventTypeCode = objClaim.LossDescription;

                    }

                iEventStateCode = objPolSystemInterface.GetPSMappedCode(objClaim.FilingStateId, "STATES", "State on CreateClaimNodeForBatch", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode); //MITS 32000 : aaggarwal29

                        if (iEventStateCode > 0)
                            objClaim.Context.LocalCache.GetCodeInfo(iEventStateCode, ref sStateShortCode, ref sState);
                        else
                            objClaim.Context.LocalCache.GetStateInfo(objClaim.FilingStateId, ref sStateShortCode, ref sState);
                    objClaim.Context.LocalCache.GetCodeInfo(iClaimTypeCodeId, ref sClaimType, ref sClaimTypeCode);
                    objClaim.Context.LocalCache.GetCodeInfo(iClaimStatusCodeId, ref sClaimStatus, ref sClaimStatusCode);
                    if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.POINT) //Payal (Dry Run Issue)
                    {
                        if (IsClaimReopened(objClaim))
                        {
                        sClaimStatus = "R";
                        sClaimStatusCode = "Reopened";
                        }
                    }
                objXMLCLaim.ActivityId = iActivityRowId.ToString();

                    objXMLCLaim.EventID = objClaim.EventId.ToString();
                int iCatCodeId = objPolSystemInterface.GetPSMappedCode(objClaim.CatastropheCode, "CATASTROPHE_TYPES", "Catastrophe on CreateClaimNodeForBatch", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29
                   objXMLCLaim.ClaimCatastrophe = objClaim.Context.LocalCache.GetShortCode(iCatCodeId);
                 
                    using(DbReader objRdr = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString,"SELECT SPLIT_ROW_ID FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID = "+ iCurrTransId)) 
                    {
                        while(objRdr.Read())
                        {
                            int iSplitRowId=0;
                            int iRsvHistMoveId=0;
                            if (iMoveHistTableId == objClaim.Context.LocalCache.GetTableId("SPLIT_MOVE_HIST"))
                            {
                                GetSplitMoveHistData(iMoveHistTableKey, ref iSplitRowId, ref iRsvHistMoveId);

                                if (iSplitRowId != Conversion.ConvertObjToInt(objRdr.GetValue(0), m_iClientId))
                                {
                                    continue;
                                }
                            }
                            XmlNode objClaimNode = null;
                            //Ankit Start : Worked on MITS - 34509
                            string sLossTime = "120000";
                            if (objClaim.Context.InternalSettings.SysSettings.PolicyCvgType.ToString() == "0")
                            {
                                if (!string.Equals(Riskmaster.Common.Conversion.GetTime(objClaim.TimeOfClaim), "000000") && !string.IsNullOrEmpty(Riskmaster.Common.Conversion.GetTime(objClaim.TimeOfClaim)))
                                    sLossTime = Riskmaster.Common.Conversion.GetTime(objClaim.TimeOfClaim);
                                //Ankit End
                                sDateofLoss =Riskmaster.Common.Conversion.GetDate(objClaim.DateOfClaim);
                                objClaimNode = objXMLCLaim.Create(objClaim.ClaimId.ToString(), objClaim.ClaimNumber.ToUpper(), sClaimTypeCode, sClaimType, sClaimStatusCode, sClaimStatus, Riskmaster.Common.Conversion.GetDate(objClaim.DateOfClaim), sLossTime, GetReportedDate(objClaim.ClaimId, objClaim.EventId), GetReportedTime(objClaim.ClaimId, objClaim.EventId), sEventType, sEventTypeCode, sStateShortCode, sState, sTotalReserveBalance,string.Empty, string.Empty); //Payal RMA:7484
                                

                            }
                            else
                            {
                                if (!string.Equals(Riskmaster.Common.Conversion.GetTime((objClaim.Parent as Event).TimeOfEvent), "000000") && !string.IsNullOrEmpty(Riskmaster.Common.Conversion.GetTime((objClaim.Parent as Event).TimeOfEvent)))
                                    sLossTime = Riskmaster.Common.Conversion.GetTime((objClaim.Parent as Event).TimeOfEvent);
                                //Ankit End
                                sDateofLoss=Riskmaster.Common.Conversion.GetDate((objClaim.Parent as Event).DateOfEvent);
                                objClaimNode = objXMLCLaim.Create(objClaim.ClaimId.ToString(), objClaim.ClaimNumber.ToUpper(), sClaimTypeCode, sClaimType, sClaimStatusCode, sClaimStatus, Riskmaster.Common.Conversion.GetDate((objClaim.Parent as Event).DateOfEvent), sLossTime, GetReportedDate(objClaim.ClaimId, objClaim.EventId), GetReportedTime(objClaim.ClaimId, objClaim.EventId), sEventType, sEventTypeCode, sStateShortCode, sState, sTotalReserveBalance, string.Empty, string.Empty); //Payal RMA:7484
                            }
                        iLobCodeId = objPolSystemInterface.GetPSMappedCode(objClaim.PolicyLOBCode, "POLICY_CLAIM_LOB", "PolicyClaimLOB on CreateClaimNodeForBatch", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29

                    objXMLCLaim.AccidentState = sStateShortCode;
                    objXMLCLaim.AccidentState_Code = sState;
                    objXMLCLaim.AccidentDescription = sEventTypeCode;
                    objXMLCLaim.AccidentDescription_Code = sEventType;

                   //Payal RMA:7484 Starts
                   if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.INTEGRAL) 
                   {
                       CreateClaimModificationNode(iClaimId, iActivityRowId, ref sDateOfTransaction, ref sTimeofTransaction);
                       if (!string.IsNullOrEmpty(sDateOfTransaction))
                        {
                            objXMLCLaim.ActivityType_Code = "Claim Modification";
                            objXMLCLaim.DateOfTransaction = sDateOfTransaction;
                            objXMLCLaim.TimeOfTransaction = sTimeofTransaction;
                        }
                   }
                   //Payal RMA:7484 Ends

                    
                    objClaim.Context.LocalCache.GetCodeInfo(iLobCodeId, ref sLOBType, ref sLOBShortCode);

                    objXMLCLaim.LOB_Code = sLOBType;
                    objXMLCLaim.LOB = sLOBShortCode;
                    objXMLCLaim.EventID = objClaim.EventId.ToString();
                    
                    if (objClaim.SelfInsuredFlag)
                        objXMLCLaim.SelfInsured = "Yes";
                    else
                        objXMLCLaim.SelfInsured = "No";

                    if (objClaim.SpHandlingFlag)
                        objXMLCLaim.SplHandling = "Yes";
                    else
                        objXMLCLaim.SplHandling = "No";


                    if (!CreatePaymentNode(Riskmaster.Common.Conversion.ConvertObjToInt(objRdr.GetValue(0), m_iClientId), objXMLCLaim.LossDetail, iActivityRowId, iActivityType, objClaim.ClaimTypeCode, objClaim.LineOfBusCode, iReserveStatus, bVoidFlag, objClaim.ClaimNumber.ToUpper(),iRsvHistMoveId)) //MITS 32000 : aaggarwal29
                    {
                        return objDoc;
                    }

                    if (iTransPolicyId > 0)
                    {
                        foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
                        {
                            if (iTransPolicyId == objClaimXPolicy.PolicyId)
                                CreatePolicyNode(objXMLCLaim, objClaimXPolicy.PolicyId, objClaim.ClaimTypeCode, objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29
                        }
                    }
                    else
                    {
                        foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
                        {
                            if (iTransPolicyId == objClaimXPolicy.PolicyId)
                                CreatePolicyNode(objXMLCLaim, objClaimXPolicy.PolicyId, objClaim.ClaimTypeCode, objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29
                        }
                    }
                    iTransPolicyId = 0;
                    
                    if (objClaim.Context.LocalCache.GetShortCode(objClaim.LineOfBusCode) == "WC")
                    {
                        if (!CreateUnitStatsNode(objClaim, objXMLCLaim.UnitStats))
                        {
                            return objDoc;
                        }
                        CreateInjuryNode(objClaim, objXMLCLaim.Injury);
                    }
                        foreach (ClaimAdjuster objAdjuster in objClaim.AdjusterList)
                        {
                            CreateAdjusterNode(objAdjuster.AdjRowId, objXMLCLaim.PartyInvolved);

                            if (objAdjuster.CurrentAdjFlag)
                            {
                                //objXMLCLaim.ExaminerCd = objAdjuster.ReferenceNumber; // MITS 32432 .ExaminerCD;
                                objXMLCLaim.ExaminerCd = objAdjuster.AdjusterEntity.ReferenceNumber; //MITS 35680: aaggarwal29 - Deleting EXAMINERCD column from CLAIM_ADJUSTER

                                objXMLCLaim.Examiner = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(SecurityDatabase.Dsn, string.Format("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.USER_ID={0}", objAdjuster.AdjusterEntity.RMUserId)));
                                break;
                            }
                            else
                            {
                                //objXMLCLaim.ExaminerCd = objAdjuster.ReferenceNumber; // MITS 32432 .ExaminerCD;
                                objXMLCLaim.ExaminerCd = objAdjuster.AdjusterEntity.ReferenceNumber; //MITS 35680: aaggarwal29 - Deleting EXAMINERCD column from CLAIM_ADJUSTER

                                objXMLCLaim.Examiner = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(SecurityDatabase.Dsn, string.Format("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.USER_ID={0}", objAdjuster.AdjusterEntity.RMUserId)));
                            }
                        }

                //    }
                    if (iTransClaimantId > 0)
                    {
                        CreateClaimantNode(iTransClaimantId, objXMLCLaim.PartyInvolved, objClaim.Context.LocalCache.GetShortCode(objClaim.LineOfBusCode), iTransPolicyId, sDateofLoss);
                    }
                    else
                    {
                        foreach (Claimant objClaimant in objClaim.ClaimantList)
                        {
                            CreateClaimantNode(objClaimant.ClaimantRowId, objXMLCLaim.PartyInvolved, objClaim.Context.LocalCache.GetShortCode(objClaim.LineOfBusCode), iTransPolicyId, sDateofLoss);
                        }
                    }

                    
                 //   iReserveAdjusterId = 0;
                    iTransClaimantId = 0;
                    objtemp = objDoc.SelectSingleNode("/CLAIMDOC");
                    objtemp.AppendChild(objDoc.ImportNode(objClaimNode, true));
                    objClaimNode = null;
                    }
                    Console.WriteLine("0 ^*^*^ Claim Number " + objClaim.ClaimNumber + " added");
                    sbLogFile.Append(" Claim number " + objClaim.ClaimNumber + " added    " + Environment.NewLine);
                       
                }
            }
            catch (Exception E) 
            {
                claimWithCorruptActivity.Add(objClaim.ClaimId);
                Console.WriteLine("An Error occured during Adding claim number :" + objClaim.ClaimNumber);
                sbLogFile.Append(" Claim number " + objClaim.ClaimNumber + " added    " + Environment.NewLine);
            }
            finally
            {
                objtemp = null;
                if (objClaim != null)
                    objClaim.Dispose();
                if (objClaim != null)
                    objClaim.Dispose();
                objXMLCLaim = null;
                if (objPolSystemInterface != null)
                    objPolSystemInterface = null;
                if (objIntgSystemInterface != null)
                    objIntgSystemInterface = null;
            }
            return objDoc;
        }
        private static string GetPolicySystemName(string sPolicySystemId)
        {
            string sPolicySystemName = string.Empty;
            string sSelectSQL = string.Empty;

            try
            {
                sSelectSQL = "SELECT POLICY_SYSTEM_NAME FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID=" + sPolicySystemId;
                sPolicySystemName = Riskmaster.Common.Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSelectSQL));
            }
            catch (Exception e)
            {
                Console.WriteLine("0 ^*^*^ Policy System not found ");
                sbLogFile.Append(" Policy System not found " + Environment.NewLine);
                throw new Exception("Policy System not found");
            }

            return sPolicySystemName;
            
        }


        private static string GetClientFileSetting(string sPolicySystemId)
        {
            string sClientFileValue = string.Empty;
            string sSelectSQL = string.Empty;

            try
            {
                sSelectSQL = "SELECT CLIENTFILE_FLAG FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID=" + sPolicySystemId;
                sClientFileValue = Riskmaster.Common.Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSelectSQL));
            }
            catch (Exception e)
            {
                Console.WriteLine("0 ^*^*^ Policy System not found in method GetClientFileSetting ");
                sbLogFile.Append(" Policy System not found in method GetClientFileSetting " + Environment.NewLine);
                throw new Exception("Policy System not found in method GetClientFileSetting");
            }

            return sClientFileValue;

        }

        private static void CallCCPCPolicyUpload(string sPolicySystemId,string sFileName)
        {
            try
            {
                //start : bsharma33 Code reverted RMA-9738
                //sActivityRowIds = CCP.PointClaimsLoad.MainModule.LoadXML(sPolicySystemId, m_objUserLogin.objRiskmasterDatabase.DataSourceName, sFileName, RMConfigurator.AppFilesPath + "\\Policy Interface", sPolicyUploadPath, m_objUserLogin.LoginName, sUseWriter); // RMA-9738:aaggarwal29
                sActivityRowIds = CCP.PointClaimsLoad.MainModule.LoadXML(sPolicySystemId, m_objUserLogin.objRiskmasterDatabase.DataSourceName, sFileName, RMConfigurator.AppFilesPath + "\\Policy Interface", sPolicyUploadPath, m_objUserLogin.LoginName);
                //ends  : bsharma33 Code reverted RMA-9738

            }
            catch(Exception e)
            {
                Console.WriteLine("0 ^*^*^ Could not load data to Policy System ");
                sbLogFile.Append(" Could not load data to Policy System " + Environment.NewLine);
                throw new Exception("Could not load data to Policy System");
            }
        }


        private static XmlDocument SetHeader(XmlDocument objDoc,string sPolicySystemId)
        {
            XMLHeader objHeader = null;
            try
            {
                objHeader = new XMLHeader();
                objHeader.CreateDate = System.DateTime.Now.ToString("yyyy/MM/dd");
                objHeader.CreateTime = System.DateTime.Now.ToString("hhmmss");
                objHeader.CreateTimeZone = "EST";
                objHeader.Create(objDoc, "CLAIMSUPLOAD", "CCPTXTreeDataExtract", "Extracts Claim Transactions for Upload to Issuing System", string.Empty, m_objUserLogin.objRiskmasterDatabase.DataSourceName, "4.0.0.0", string.Empty, string.Empty);
                objHeader.CreateDate = System.DateTime.Now.ToString("yyyy/MM/dd");
                objHeader.CreateTime = System.DateTime.Now.ToString("hhmmss");
                objHeader.UserID = m_objUserLogin.LoginName;
                objHeader.Password = m_objUserLogin.Password;
                objHeader.SessionID = sSessionId;
                RqId = objHeader.RqUID;
            }
            finally
            {
                objHeader = null;
            }
            return objDoc;
        }

        private static XmlDocument SetMessage(XmlDocument objDoc)
        {
            XMLMessage objMessage = null;
            try
            {
                objMessage = new XMLMessage();
                objMessage.RequestingDocTimeZone = "EST";
                objMessage.Create(objDoc, pmType.pmmtResponse, "GETCLAIM", "URL", "100", pmRespType.pmrtText, "SUCCESSFUL", "CCPBXBusinessDataExtract", m_objUserLogin.objRiskmasterDatabase.DataSourceName, "CLAIMEXTRACT", System.DateTime.Now.ToString("dd/MM/yyyy"), System.DateTime.Now.ToString("hhmmss"));
                objMessage.RequestingDocRqUID = RqId;
                objMessage.RequestingDocTimeZone = "EST";
                objMessage.ResponseType = pmRespType.pmrtText;
            }
            finally
            {
                objMessage = null;
            }

            return objDoc;
        }

        private static bool IsClaimReopened(Claim objClaim)
        {
            string sSQL = string.Empty;
                 if (objClaim.Context.LocalCache.GetShortCode(objClaim.Context.LocalCache.GetRelatedCodeId(objClaim.ClaimStatusCode)) == "O")
                 {
                         sSQL = "SELECT CL_STATUS_ROW_ID FROM CLAIM_STATUS_HIST,CLAIM,CODES WHERE CLAIM.CLAIM_ID = " + objClaim.ClaimId + " AND  CLAIM.CLAIM_ID = CLAIM_STATUS_HIST.CLAIM_ID   AND CODES.CODE_ID = CLAIM_STATUS_HIST.STATUS_CODE   AND CODES.RELATED_CODE_ID = " + objClaim.Context.LocalCache.GetCodeId("C", "STATUS");
                         using (DbReader objRdr = DbFactory.GetDbReader(objClaim.Context.DbConn.ConnectionString, sSQL))
                         {
                             if (objRdr.Read())
                             {
                                 return true;
                             }
                     }
                 }
             return false;
        }
        private static XmlDocument CreateClaimNodeForBatch(XmlDocument objDoc, int iClaimId, int iTableKey, int iTableId, int iActivityRowId, int iActivityType, double iReserveAmount, int iReserveStatus, double iChangeAmount, bool bVoidFlag, string sTotalReserveAmount,int iMoveHistTableId,int iMoveHistTableKey)
        {
            XMLClaim objXMLCLaim = null;
            string sLOBType = string.Empty;
            string sLOBShortCode = string.Empty;
            int iLobCodeId = 0;
            string sClaimType = string.Empty;
            string sClaimTypeCode = string.Empty;
            int iClaimTypeCodeId = 0;
            string sClaimStatus = string.Empty;
            string sClaimStatusCode = string.Empty;
            int iClaimStatusCodeId = 0;
            string sDateofLoss = string.Empty;
            string sEventType = string.Empty;
            string sEventTypeCode = string.Empty;
            int iEventTypeCodeId = 0;
            XmlNode objtemp = null;
            Claim objClaim = null;
             int iCurrClaimId=0;
             int iCurrForeignKey=0;
             int iCurrForeignTableID=0;
             int iEventStateCode = 0;
            int iCatCode=0;
            XMLEvent objEvent = null;
            PolicySystemInterface objPolSystemInterface = new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString,m_iClientId);
            IntegralSystemInterface objIntgSystemInterface = new IntegralSystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString,m_iClientId);
            //Payal RMA:7484 Starts
            string sActivityType = string.Empty;
            string sDateOfTransaction = string.Empty;
            string sTimeofTransaction = string.Empty;
            //Payal RMA:7484 Ends
            try
             {
                     objXMLCLaim = new XMLClaim();
                     iCurrClaimId = iClaimId;
                     iCurrForeignKey = iTableKey;
                     iCurrForeignTableID = iTableId;
                     objClaim = (Claim)m_objFactory.GetDataModelObject("Claim", false);
                     objClaim.MoveTo(iClaimId);
                     if (objClaim.ClaimNumber.Length > 12)
                     {
                         Console.WriteLine("0 ^*^*^ Claim Number " + objClaim.ClaimNumber + " length cannot be greater than 12");
                         sbLogFile.Append(" Claim number " + objClaim.ClaimNumber + "length cannot be greater than 12 characters    " + Environment.NewLine);
                         throw new Exception("Claim Number length cannot be greater than 12 characters.");
                     }
                     if (string.IsNullOrEmpty(sActivityRowId))
                         sActivityRowId = iActivityRowId.ToString();
                     else
                         sActivityRowId = sActivityRowId + "," + iActivityRowId.ToString();


                     string sStateShortCode = string.Empty;
                     string sState = string.Empty;
                //Added:Yukti, Get CLAIM_STATUS_CODE for INTEGRAL
                //iClaimStatusCodeId = objClaim.Context.LocalCache.GetRelatedCodeId(objClaim.ClaimStatusCode);                     
                iClaimStatusCodeId = objIntgSystemInterface.GetPSMappedCodeIDFromRMXCodeId(objClaim.ClaimStatusCode, "CLAIM_STATUS", "Claim Status Code on Create Claim Node", iPolicySystemId.ToString());
                iClaimTypeCodeId = objPolSystemInterface.GetPSMappedCode(objClaim.ClaimTypeCode, "CLAIM_TYPE", "ClaimType on CreateClaimNodeForBatch", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode); //MITS 32000 : aaggarwal29
                iEventStateCode = objPolSystemInterface.GetPSMappedCode(objClaim.FilingStateId, "STATES", "State on CreateClaimNodeForBatch", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29

                         if (iEventStateCode > 0)
                             objClaim.Context.LocalCache.GetCodeInfo(iEventStateCode, ref sStateShortCode, ref sState);
                         else
                             objClaim.Context.LocalCache.GetStateInfo(objClaim.FilingStateId, ref sStateShortCode, ref sState);
                     objClaim.Context.LocalCache.GetCodeInfo(iClaimTypeCodeId, ref sClaimType, ref sClaimTypeCode);
                     objClaim.Context.LocalCache.GetCodeInfo(iClaimStatusCodeId, ref sClaimStatus, ref sClaimStatusCode);
                     if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.POINT) //Payal (Dry Run Issue)
                    {
                        if (IsClaimReopened(objClaim))
                        {
                        sClaimStatus = "R";
                        sClaimStatusCode = "Reopened";
                        }
                    }
                     if (objClaim.ClaimDescriptionCode > 0)
                     {
                    iEventTypeCodeId = objPolSystemInterface.GetPSMappedCode(objClaim.ClaimDescriptionCode, "CLAIM_DESCRIPTION", "ClaimDescriptionCode on CreateClaimNodeForBatch", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29
                         objClaim.Context.LocalCache.GetCodeInfo(iEventTypeCodeId, ref sEventType, ref sEventTypeCode);

                      
                     }
                     else
                     {
                         sEventTypeCode  = objClaim.LossDescription;

                     }

                     XmlNode objClaimNode = null;
                     //Ankit Start : Worked on MITS - 34509
                     string sLossTime = "120000";
                     if (objClaim.Context.InternalSettings.SysSettings.PolicyCvgType.ToString() == "0")
                     {
                         if (!string.Equals(Riskmaster.Common.Conversion.GetTime(objClaim.TimeOfClaim), "000000") && !string.IsNullOrEmpty(Riskmaster.Common.Conversion.GetTime(objClaim.TimeOfClaim)))
                             sLossTime = Riskmaster.Common.Conversion.GetTime(objClaim.TimeOfClaim);
                         //Ankit End
                         sDateofLoss = Riskmaster.Common.Conversion.GetDate(objClaim.DateOfClaim);
                         objClaimNode = objXMLCLaim.Create(objClaim.ClaimId.ToString(), objClaim.ClaimNumber.ToUpper(), sClaimTypeCode, sClaimType, sClaimStatusCode, sClaimStatus, Riskmaster.Common.Conversion.GetDate(objClaim.DateOfClaim), sLossTime, GetReportedDate(objClaim.ClaimId, objClaim.EventId), GetReportedTime(objClaim.ClaimId, objClaim.EventId), sEventType, sEventTypeCode, sStateShortCode, sState, sTotalReserveAmount, string.Empty, string.Empty); //Payal RMA:7484
                     }
                     else
                     {
                         if (!string.Equals(Riskmaster.Common.Conversion.GetTime((objClaim.Parent as Event).TimeOfEvent), "000000") && !string.IsNullOrEmpty(Riskmaster.Common.Conversion.GetTime((objClaim.Parent as Event).TimeOfEvent)))
                             sLossTime = Riskmaster.Common.Conversion.GetTime((objClaim.Parent as Event).TimeOfEvent);
                         //Ankit End
                         sDateofLoss = Riskmaster.Common.Conversion.GetDate((objClaim.Parent as Event).DateOfEvent);
                         objClaimNode = objXMLCLaim.Create(objClaim.ClaimId.ToString(), objClaim.ClaimNumber.ToUpper(), sClaimTypeCode, sClaimType, sClaimStatusCode, sClaimStatus, Riskmaster.Common.Conversion.GetDate((objClaim.Parent as Event).DateOfEvent), sLossTime, GetReportedDate(objClaim.ClaimId, objClaim.EventId), GetReportedTime(objClaim.ClaimId, objClaim.EventId), sEventType, sEventTypeCode, sStateShortCode, sState, sTotalReserveAmount, string.Empty, string.Empty); //Payal RMA:7484
                     }
                iLobCodeId = objPolSystemInterface.GetPSMappedCode(objClaim.PolicyLOBCode, "POLICY_CLAIM_LOB", "PolicyClaimLOB on CreateClaimNodeForBatch", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29
                     objClaim.Context.LocalCache.GetCodeInfo(iLobCodeId, ref sLOBType, ref sLOBShortCode);
                     objXMLCLaim.AccidentDescription = sEventTypeCode;
                     objXMLCLaim.AccidentDescription_Code = sEventType;
                     objXMLCLaim.AccidentState = sStateShortCode;
                     objXMLCLaim.AccidentState_Code = sState;

                //Payal RMA:7484 Starts
                   if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.INTEGRAL) 
                   {
                       CreateClaimModificationNode(iClaimId, iActivityRowId, ref sDateOfTransaction, ref sTimeofTransaction);
                       if (!string.IsNullOrEmpty(sDateOfTransaction))
                        {
                            objXMLCLaim.ActivityType_Code = "Claim Modification";
                            objXMLCLaim.DateOfTransaction = sDateOfTransaction;
                            objXMLCLaim.TimeOfTransaction = sTimeofTransaction;
                        }
                   }
                   //Payal RMA:7484 Ends

                     objXMLCLaim.LOB_Code = sLOBType;
                     objXMLCLaim.LOB = sLOBShortCode;
                       if (objClaim.SelfInsuredFlag)
                         objXMLCLaim.SelfInsured = "Yes";
                     else
                         objXMLCLaim.SelfInsured = "No";

                     if (objClaim.SpHandlingFlag)
                         objXMLCLaim.SplHandling = "Yes";
                     else
                         objXMLCLaim.SplHandling = "No";


                     objXMLCLaim.EventID = objClaim.EventId.ToString();
                objXMLCLaim.ActivityId = iActivityRowId.ToString();
                 objXMLCLaim.EventID = objClaim.EventId.ToString();
                int iCatCodeId = objPolSystemInterface.GetPSMappedCode(objClaim.CatastropheCode, "CATASTROPHE_TYPES", "Catastrophe on CreateClaimNodeForBatch", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29
                objXMLCLaim.ClaimCatastrophe = objClaim.Context.LocalCache.GetShortCode(iCatCodeId);
                //Added:Yukti, Dt:12/31/2014       
                if (objClaim.ClaimantList.Count != 0)
                {
                    ReserveFunds objResFunds = new ReserveFunds(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_objUserLogin, m_iClientId);
                    //Payal RMA:12744 Starts
                    if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.INTEGRAL)
                       sTotalReserveBalance = objResFunds.GetTotalReserveBalance(iCurrClaimId,true); //True to exlculde Recovery balance calcuation for Integral Policy System
                    else
                       sTotalReserveBalance = objResFunds.GetTotalReserveBalance(iCurrClaimId, false); 
                    //Payal RMA:12744 Ends
               }
                //Ended:Yukti                 
                objXMLCLaim.TotalClaimReserveBalance = sTotalReserveBalance;//vkumar258

                     if (objClaim.Context.LocalCache.GetShortCode(objClaim.LineOfBusCode) == "WC")
                     {
                         if (!CreateUnitStatsNode(objClaim, objXMLCLaim.UnitStats))
                         {
                             return objDoc;
                         }
                         CreateInjuryNode(objClaim, objXMLCLaim.Injury);
                     }
                     if ((iCurrForeignTableID > 0) && (iCurrForeignKey > 0))
                     {
                         if (!MapFinancialData(iCurrForeignTableID, iCurrForeignKey, objXMLCLaim.LossDetail, iActivityRowId, iActivityType, iReserveAmount, iReserveStatus, iChangeAmount, objClaim.ClaimTypeCode, objClaim.LineOfBusCode, bVoidFlag, objClaim.ClaimNumber.ToUpper(),iMoveHistTableId,iMoveHistTableKey))//MITS 32000 : aaggarwal29
                         {

                             return objDoc;
                         }
                     }
                     if (iTransPolicyId > 0)
                     {
                         foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
                         {
                             if (iTransPolicyId == objClaimXPolicy.PolicyId)
                                 CreatePolicyNode(objXMLCLaim, objClaimXPolicy.PolicyId, objClaim.ClaimTypeCode,objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29
                         }
                     }
                     else
                     {
                         foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
                         {
                             CreatePolicyNode(objXMLCLaim, objClaimXPolicy.PolicyId, objClaim.ClaimTypeCode, objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29
                         }
                     }
                         foreach (ClaimAdjuster objAdjuster in objClaim.AdjusterList)
                         {
                             CreateAdjusterNode(objAdjuster.AdjRowId, objXMLCLaim.PartyInvolved);

                             if (objAdjuster.CurrentAdjFlag)
                             {
                                 //objXMLCLaim.ExaminerCd = objAdjuster.ReferenceNumber; // MITS 32432 .ExaminerCD;
                                 objXMLCLaim.ExaminerCd = objAdjuster.AdjusterEntity.ReferenceNumber; //MITS 35680: aaggarwal29 - Deleting EXAMINERCD column from CLAIM_ADJUSTER

                                 objXMLCLaim.Examiner = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(SecurityDatabase.Dsn, string.Format("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.USER_ID={0}", objAdjuster.AdjusterEntity.RMUserId)));
                                 break;
                             }
                             else
                             {
                                 //objXMLCLaim.ExaminerCd = objAdjuster.ReferenceNumber; // MITS 32432 .ExaminerCD;
                                 objXMLCLaim.ExaminerCd = objAdjuster.AdjusterEntity.ReferenceNumber; //MITS 35680: aaggarwal29 - Deleting EXAMINERCD column from CLAIM_ADJUSTER

                                 objXMLCLaim.Examiner = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(SecurityDatabase.Dsn, string.Format("SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_DETAILS_TABLE.USER_ID={0}", objAdjuster.AdjusterEntity.RMUserId)));
                             }

                         }

                   //  }
                     if (iTransClaimantId > 0)
                     {
                         CreateClaimantNode(iTransClaimantId, objXMLCLaim.PartyInvolved, objClaim.Context.LocalCache.GetShortCode(objClaim.LineOfBusCode), iTransPolicyId, sDateofLoss);
                     }
                     else
                     {
                         foreach (Claimant objClaimant in objClaim.ClaimantList)
                         {
                             CreateClaimantNode(objClaimant.ClaimantRowId, objXMLCLaim.PartyInvolved, objClaim.Context.LocalCache.GetShortCode(objClaim.LineOfBusCode), iTransPolicyId, sDateofLoss);
                         }
                     }

                    
                     iTransPolicyId = 0;
                   //  iReserveAdjusterId = 0;
                     iTransClaimantId = 0;
                     objtemp = objDoc.SelectSingleNode("/CLAIMDOC");
                     objtemp.AppendChild(objDoc.ImportNode(objClaimNode, true));

                     

                Console.WriteLine("0 ^*^*^ Claim Number " + objClaim.ClaimNumber + " added");
                sbLogFile.Append(" Claim number " + objClaim.ClaimNumber + " added " + Environment.NewLine);
            }
            catch (Exception e)
            {
                claimWithCorruptActivity.Add(objClaim.ClaimId);
                Console.WriteLine("An Error occured during Adding claim number :" + objClaim.ClaimNumber);
                sbLogFile.Append(" Claim number " + objClaim.ClaimNumber + " added    " + Environment.NewLine);
            
            
            }
             finally
             {
                 objtemp = null;
                 if (objClaim != null)
                     objClaim.Dispose();
                 objXMLCLaim = null;
                if (objPolSystemInterface != null)
                    objPolSystemInterface = null;
                if (objIntgSystemInterface != null)
                    objIntgSystemInterface = null;
             }
            return objDoc;
        }
        private static void CreateInjuryNode(Claim objClaim,XMLInjury objInjury)
        {
            objInjury.Create();
            int iAIA12CodeId = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(objClaim.AIACode12, "AIA_CODE_12", "AIACode12 on CreateInjuryNode", iPolicySystemId.ToString(), objClaim.ClaimTypeCode,objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29
            if (iAIA12CodeId > 0)
            {
                objInjury.BodyPart_Code = objClaim.Context.LocalCache.GetShortCode(iAIA12CodeId);
                objInjury.BodyPart = objClaim.Context.LocalCache.GetCodeDesc(iAIA12CodeId);
            }
            int iAIA34CodeId = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(objClaim.AIACode34, "AIA_CODE_34", "AIACode34 on CreateInjuryNode", iPolicySystemId.ToString(),objClaim.ClaimTypeCode,objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29
            if (iAIA34CodeId > 0)
            {
                objInjury.NatureOfInjury_Code = objClaim.Context.LocalCache.GetShortCode(iAIA34CodeId);
                objInjury.NatureOfInjury = objClaim.Context.LocalCache.GetCodeDesc(iAIA34CodeId);
            }
            int iAIA56CodeId = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(objClaim.AIACode56, "AIA_CODE_56", "AIACode56 on CreateInjuryNode", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29
            if (iAIA56CodeId > 0)
            {
                objInjury.CauseOfInjury_Code = objClaim.Context.LocalCache.GetShortCode(iAIA56CodeId);
                objInjury.CauseOfInjury = objClaim.Context.LocalCache.GetCodeDesc(iAIA56CodeId);
            }
            //dbisht6 JIRA-10309
            //int iPercentCode = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(objClaim.UnitStat.ImpairmentPercentCode, "NCCI_IMPAIRMENT", "ImpairmentPercentCode on CreateInjuryNode", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29
            //objInjury.PercentDisability =objClaim.Context.LocalCache.GetShortCode(iPercentCode);
            int iMgndCareOrgTypeCod = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(objClaim.UnitStat.MgndCareOrgTypeCode, "MGND_CARE_ORG_TYPE", "MgndCareOrgTypeCode on CreateInjuryNode", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode);
            objInjury.ManagedCareOrgType_Code = objClaim.Context.LocalCache.GetShortCode(iMgndCareOrgTypeCod);
            //dbisht6 end
           
        }

        private static bool CreateUnitStatsNode(Claim objClaim, XMLUnitStats objXMLUnitStats)
        {
            objXMLUnitStats.Create();
            int iClaimantNumber = 0;
            foreach (Claimant objclaimant in objClaim.ClaimantList)
            {
                iClaimantNumber = objclaimant.ClaimantNumber;

            }
            if (iClaimantNumber == 0)
                iClaimantNumber = 1;
            if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.POINT && HasBlankAddressDetails(objClaim.PrimaryPiEmployee.PiEid, 0, "EMPLOYEE", objClaim.ClaimNumber, 0))
            {
                return false;
            }
            objXMLUnitStats.Employee.Create(objClaim.PrimaryPiEmployee.PiEid.ToString());
            objXMLUnitStats.Employee.ClaimantNumber = iClaimantNumber.ToString();
            objXMLUnitStats.Employee.HiredDate = Riskmaster.Common.Conversion.GetDate(objClaim.PrimaryPiEmployee.DateHired.ToString());

            int iStateCode = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(objClaim.FilingStateId, "STATES", "State on CreateUnitStatsNode", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29
            string sStateShortCode = string.Empty;
            string sState = string.Empty;
            if (iStateCode > 0)
          objXMLUnitStats.JurisdictionState_Code=  objClaim.Context.LocalCache.GetShortCode(iStateCode);
            else
                objXMLUnitStats.JurisdictionState_Code = objClaim.Context.LocalCache.GetStateCode(objClaim.FilingStateId);

            int iCatCodeId = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(objClaim.CatastropheCode, "CATASTROPHE_TYPES", "Catastrophe on CreateClaimNodeForBatch", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29
            objXMLUnitStats.CatastropheNumber = objClaim.Context.LocalCache.GetShortCode(iCatCodeId);

            //MITS 35420: aaggarwal29 ; query was not returning any data , corrected table name as CLAIM_CATEGORY 
            int iClaimCategory = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(objClaim.ClaimCategoryCode, "CLAIM_CATEGORY", "ClaimCategoryCode on CreateUnitStatsNode", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29
            objXMLUnitStats.BenefitType_Code = objClaim.Context.LocalCache.GetShortCode(iClaimCategory);
            objXMLUnitStats.EmployerNotifiedDate =Riskmaster.Common.Conversion.GetDate((objClaim.Parent as Event).DateReported);
            return true;
            //dbisht6 RMA-10309
            objXMLUnitStats.Employee.AvgWklyWage = objClaim.PrimaryPiEmployee.WeeklyRate.ToString();
            //int NcciLossTypeLossCode = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(objClaim.UnitStat.NcciLossTypeLossCode, "NCCI_LOSS_TYPE_LOSS_CODE", "NcciLossTypeLossCode on CreateUnitStatsNode", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode);//MITS 32000 : aaggarwal29
            //objXMLUnitStats.LossAct_Code =objClaim.Context.LocalCache.GetShortCode( NcciLossTypeLossCode);
            int NcciLossTypeLossCode = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(objClaim.UnitStat.NcciLossTypeLossCode, "NCCI_CLAIM_STATUS", "NcciLossTypeLossCode on CreateUnitStatsNode", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode);
            objXMLUnitStats.TypeLoss_Code = objClaim.Context.LocalCache.GetShortCode(NcciLossTypeLossCode);

            int iTypeRecovery_Code = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(objClaim.UnitStat.NcciLossTypeRecovCode, "NCCI_RECOV_CODE", "NcciLossTypeRecovCode on CreateUnitStatsNode", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode);
            objXMLUnitStats.TypeRecovery_Code = objClaim.Context.LocalCache.GetShortCode(iTypeRecovery_Code);

            //objXMLUnitStats.JurisdictionState_Code = objClaim.Jurisdictionals.;

            objXMLUnitStats.Employee.EmpFein = objClaim.PrimaryPiEmployee.FEIN;
            //objXMLUnitStats. objClaim.UnitStat.ImpairmentPercentCode;
            int iPositionCode = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(objClaim.PrimaryPiEmployee.PositionCode, "POSITIONS", "PositionCode on CreateUnitStatsNode", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode);
            objXMLUnitStats.Employee.Occupation_Code = objClaim.Context.LocalCache.GetShortCode(iPositionCode);
            objXMLUnitStats.Employee.Occupation = objClaim.Context.LocalCache.GetCodeDesc(iPositionCode);

            int iFraudInd = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(objClaim.ClueFaultInd, "CLUE_FAULT_IND", "FraudIndicator on CreateUnitStatsNode", iPolicySystemId.ToString(), objClaim.ClaimTypeCode, objClaim.LineOfBusCode);
            objXMLUnitStats.FraudulentClaimInd_Code = objClaim.Context.LocalCache.GetShortCode(iFraudInd);
            //dbisht6 end
        }

        private static bool MapFinancialData(int iForeignTableId, int iForeignKey, XMLLossDetail objXML, int iActivityRowId, int iActivityType, double iReserveAmount, int iReserveStatus, double iChangeAmount, int iClaimTypeCd, int iLineOfBusCd, bool bVoidFlag, string sClaimNumber,int iMoveHistTableId,int iMoveHistTablekey) //MITS 32000 : aaggarwal29
        {
            LocalCache objCache =  null;
            bool bReturnValue = true; 
            int iSplitRowId = 0;
            int iRsvMoveHistRowId = 0;
            try
            {
                objCache = new LocalCache(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);
                if (m_objFactory.Context.LocalCache.GetTableName(m_objFactory.Context.LocalCache.GetCodeTableId(m_objFactory.Context.LocalCache.GetRelatedCodeId(iReserveStatus))) == "RESERVE_STATUS_PARENT")
                {
                    iReserveStatus = m_objFactory.Context.LocalCache.GetRelatedCodeId(iReserveStatus);
                }

                switch (objCache.GetTableName(iForeignTableId))
                {
                    case "RESERVE_CURRENT":
                       
                   bReturnValue=     CreateReserveNode(iForeignKey, objXML, iActivityRowId, iActivityType,iReserveAmount,iReserveStatus,iChangeAmount, iClaimTypeCd, iLineOfBusCd,sClaimNumber,iMoveHistTablekey); //MITS 32000 : aaggarwal29
                        break;
                    case "FUNDS":
                        Funds objFunds = (Funds)m_objFactory.GetDataModelObject("Funds", false);
                        objFunds.MoveTo(iForeignKey);
                        if (iMoveHistTablekey > 0)
                        {
                            GetSplitMoveHistData(iMoveHistTablekey, ref iSplitRowId, ref iRsvMoveHistRowId);
                        }
                        foreach (FundsTransSplit objTransSplit in objFunds.TransSplitList)
                        {
                            if ((iSplitRowId==0) || (iSplitRowId == objTransSplit.SplitRowId))
                            {
                                if (bReturnValue)
                                {

                                    bReturnValue = CreatePaymentNode(objTransSplit.SplitRowId, objXML, iActivityRowId, iActivityType, iClaimTypeCd, iLineOfBusCd, iReserveStatus, bVoidFlag, sClaimNumber, iRsvMoveHistRowId); //MITS 32000 : aaggarwal29
                                }
                                //else
                                //    break;


                                //break;
                            }
                            
                        }
                        
                        break;

                }

            }
            finally
            {
                if (objCache != null)
                    objCache.Dispose();
            }
            return bReturnValue;
        }
        private static void CreatePolicyNode(XMLClaim objXMLClaim, int iPolicyID, int iClaimTypeCd, int iLineOfBusCd)//MITS 32000 : aaggarwal29
        {
            Policy objPolicy = null;
            string sLOBCode =string.Empty;
            string sLOB =string.Empty;
            PolicySystemInterface objManager = null;
            string sSQL = string.Empty;
            try
            {
                objPolicy = (Policy)m_objFactory.GetDataModelObject("Policy", false);

                objPolicy.MoveTo(iPolicyID);

                if (objPolicy.PolicySystemId == iPolicySystemId)
                {
                    objXMLClaim.Policy.PolicyNumber = objPolicy.PolicyNumber;
                    objXMLClaim.Policy.PolicyID = iPolicyID.ToString();

                    objXMLClaim.Policy.EffectiveDate = Riskmaster.Common.Conversion.GetDate(objPolicy.EffectiveDate);
                    objXMLClaim.Policy.ExpirationDate = Riskmaster.Common.Conversion.GetDate(objPolicy.ExpirationDate);
                    objXMLClaim.Policy.Symbol = objPolicy.PolicySymbol;
                    objXMLClaim.Policy.Module = objPolicy.Module.ToString();

                    objPolicy.Context.LocalCache.GetCodeInfo((new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(objPolicy.PolicyLOB, "POLICY_CLAIM_LOB", "POLICY_CLAIM_LOB on CreatePolicyNode", iPolicySystemId.ToString(), iClaimTypeCd, iLineOfBusCd), ref sLOBCode, ref sLOB);//MITS 32000 : aaggarwal29
                    objXMLClaim.Policy.LOB_Code = sLOBCode;
                    objXMLClaim.Policy.LOB = sLOB;

                    objManager = new PolicySystemInterface(m_objUserLogin, m_iClientId);
                    //objXMLClaim.Policy.Location=  objManager.GetDownLoadXMLNodeSpecificData("POLICY", objPolicy.PolicyId, objPolicy.PolicyId, "com.csc_LocCompany", true, string.Empty);
                    //objXMLClaim.Policy.MasterCompany_Code = objManager.GetDownLoadXMLNodeSpecificData("POLICY", objPolicy.PolicyId, objPolicy.PolicyId, "com.csc_MasterCompany", true, string.Empty);
                    //objXMLClaim.Policy.Branch_Code = objManager.GetDownLoadXMLNodeSpecificData("POLICY", objPolicy.PolicyId, objPolicy.PolicyId, "com.csc_BranchCd", false, string.Empty);
                    //objXMLClaim.Policy.AgentNumber = objManager.GetDownLoadXMLNodeSpecificData("POLICY", objPolicy.PolicyId, objPolicy.PolicyId, "ContractNumber", false, string.Empty);


                    objXMLClaim.Policy.Location = objPolicy.LocationCompany;
                    objXMLClaim.Policy.MasterCompany_Code = objPolicy.MasterCompany;
                    if (string.IsNullOrEmpty(objPolicy.BranchCode))
                    {
                        if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.POINT)//vkumar258
                        {
                        objXMLClaim.Policy.Branch_Code = objManager.GetDownLoadXMLNodeSpecificData("POLICY", objPolicy.PolicyId, objPolicy.PolicyId, "com.csc_BranchCd", false, string.Empty);
                        }
                    }
                    else
                    {
                        objXMLClaim.Policy.Branch_Code = objPolicy.BranchCode;
                    }
                    //objXMLClaim.Policy.AgentNumber =  
                        
                    //    objManager.GetDownLoadXMLNodeSpecificData("POLICY", objPolicy.PolicyId, objPolicy.PolicyId, "ContractNumber", false, string.Empty);

                    /*vkumar258 This code need to be change as per new approach of entity role, by the time commenting it as build is breaking */
                    //Payal RMA: 9839 Starts
                    //if (objPolicy.Context.InternalSettings.SysSettings.UseEntityRole)
                    //{
                    //    sSQL = "SELECT REFERENCE_NUMBER FROM ENTITY,POLICY_X_ENTITY,ENTITY_X_ROLES WHERE POLICY_ID =" + objPolicy.PolicyId + "  AND POLICY_X_ENTITY.ENTITY_ID= ENTITY.ENTITY_ID AND ENTITY.ENTITY_ID = ENTITY_X_ROLES.ENTITY_ID AND ENTITY_X_ROLES.ENTITY_TABLE_ID=  " + objPolicy.Context.LocalCache.GetTableId("AGENTS") + "  AND TYPE_CODE =ENTITY_X_ROLES.ENTITY_TABLE_ID ";
                    //}
                    ////Payal RMA: 9839 Ends
                    //else
                    //{
                       // sSQL = "SELECT REFERENCE_NUMBER FROM ENTITY,POLICY_X_ENTITY WHERE POLICY_ID =" + objPolicy.PolicyId + "  AND POLICY_X_ENTITY.ENTITY_ID= ENTITY.ENTITY_ID AND ENTITY.ENTITY_TABLE_ID=  " + objPolicy.Context.LocalCache.GetTableId("AGENTS") + "  AND TYPE_CODE =ENTITY_TABLE_ID";
                    //sSQL = "SELECT REFERENCE_NUMBER FROM ENTITY,POLICY_X_ENTITY WHERE POLICY_X_ENTITY.ENTITY_ID= ENTITY.ENTITY_ID AND ENTITY.ENTITY_TABLE_ID= " + objPolicy.Context.LocalCache.GetTableId("AGENTS") + "  AND POLICY_ID in (select  policy_id from policy where policy_system_id =" + iPolicySystemId+ ")order by entity.entity_id desc "; //vkumar258                    
                    sSQL = string.Format(" SELECT  REFERENCE_NUMBER  FROM ENTITY,POLICY_X_ENTITY WHERE POLICY_ID = {0}  AND POLICY_X_ENTITY.ENTITY_ID= ENTITY.ENTITY_ID  AND TYPE_CODE = {1}", objPolicy.PolicyId, objPolicy.Context.LocalCache.GetTableId("AGENTS"));

                    //}
                    //vkumar258 End
                    
                    objXMLClaim.Policy.AgentNumber =Conversion.ConvertObjToStr(  DbFactory.ExecuteScalar(objPolicy.Context.DbConn.ConnectionString, sSQL));

                    
                }
            }
            finally
            {
                if (objPolicy != null)
                    objPolicy.Dispose();
            }
        }
        
        private static void CreateAdjusterNode(int iAdjRowID, XMLPartyInvolved objXmlPI)
        {
            ClaimAdjuster objClaimAdjuster = null;
            try
            {
             objClaimAdjuster=  (ClaimAdjuster)m_objFactory.GetDataModelObject("ClaimAdjuster", false);
                objClaimAdjuster.MoveTo(iAdjRowID);


                objXmlPI.putPartyInvolved(iAdjRowID.ToString(), objClaimAdjuster.AdjusterEid.ToString(), "", "ADJ");

                if(!objEntityList.ContainsKey(objClaimAdjuster.AdjusterEid.ToString()))
                objEntityList.Add(objClaimAdjuster.AdjusterEid.ToString(),"");

            }
            finally
            {
                if (objClaimAdjuster != null)
                    objClaimAdjuster.Dispose();
            }

        }
        //private static XmlDocument CreateEventNode(XmlDocument objDoc)
        //{

        //    XMLEvent objXMLEvent = null;
        //    XmlNode objNode = null;
        //    Event objEvent = null;
        //    int iEventTypeCodeId = 0;
        //    XmlNode objtemp = null;
        //    try
        //    {
        //        if (objEventIdList.Count > 0)
        //        {
        //              foreach (string skey in objEventIdList.Keys)
        //            {
        //            objXMLEvent = new XMLEvent();

        //            objEvent = (Event)m_objFactory.GetDataModelObject("Event", false);
        //            objEvent.MoveTo(Conversion.ConvertStrToInteger(skey));

        //            iEventTypeCodeId =(new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCodeIDFromRMXCodeId(objEvent.EventTypeCode, "EVENT_TYPE", "EventType on CreateClaimNodeForBatch", iPolicySystemId.ToString());
        //          objNode=  objXMLEvent.Create(objEvent.EventId.ToString(), objEvent.EventNumber, objEvent.Context.LocalCache.GetCodeDesc(iEventTypeCodeId), objEvent.Context.LocalCache.GetShortCode(iEventTypeCodeId), Riskmaster.Common.Conversion.GetDate(objEvent.DateReported), Riskmaster.Common.Conversion.GetDate(objEvent.TimeOfEvent), Riskmaster.Common.Conversion.GetDate(objEvent.EventDescription));
                    
        //                //  objEvent.Catastrophe_ReportingCode = objClaim.Context.LocalCache.GetShortCode(iCatCode);
        //            objtemp = objDoc.SelectSingleNode("/CLAIMDOC");
        //            objtemp.AppendChild(objDoc.ImportNode(objNode, true));
        //                  objXMLEvent = null;
        //            }
        //        }
        //    }
        //    finally
        //    {
        //        objXMLEvent = null;
        //    }
        //    return objDoc;


        //}

        private static XmlDocument CreateEntitiesNode(XmlDocument objDoc, string pClientFileSetting)
        {

            XMLEntities objEntities = null;
            try
            {
                if (objEntityList.Count > 0)
                {
                    objEntities = new XMLEntities();
                    XmlNode objNode = objEntities.Create(objDoc, "");
                    SetEntityNameLength();//JIRA 6419 pkandhari
                    foreach (string skey in objEntityList.Keys)
                    {   
                        CreateEntityNode(Conversion.ConvertStrToInteger(skey), objEntities.Entity, objEntityList[skey], pClientFileSetting);
                    }


                    if (objFundsList.Count > 0)
                    {
                        foreach (int iTransId in objFundsList)
                        {
                            CreateFundsMailTo(iTransId, objEntities.Entity);
                            //Funds objFunds = null;
                            //objFunds = (Funds)m_objFactory.GetDataModelObject("Funds", false);
                            //objFunds.MoveTo(iTransId);
                            //CreateEntityNode(objFunds.MailToEid, objEntities.Entity, objEntityList[skey], pClientFileSetting);

                        }

                    }


                }
            }
            finally
            {
                objEntities = null;
            }
            return objDoc;


        }
        private static void CreateFundsMailTo(int iTransId, XMLEntity objXmlEntity)
        {
            Funds objFunds = null;
            Entity objEntity = null;
            Address objAddress = null;

            try
            {
                
                objFunds = (Funds)m_objFactory.GetDataModelObject("Funds", false);
                objFunds.MoveTo(iTransId);

                objEntity = (Entity)m_objFactory.GetDataModelObject("Entity", false);
                objEntity.MoveTo(objFunds.MailToEid);

                objAddress = (Address)m_objFactory.GetDataModelObject("Address", false);
                objAddress.MoveTo(objFunds.MailToAddressId);

                objXmlEntity.Create("MT"+objEntity.EntityId.ToString(), entType.entTypeInd, string.Empty, entTaxIdType.entTaxIdSSN, objEntity.FirstName, objEntity.MiddleName, objEntity.LastName, "", "");


                objXmlEntity.LastName = objEntity.LastName;
                objXmlEntity.FirstName = objEntity.FirstName;
                objXmlEntity.MiddleName = objEntity.MiddleName;
                
                objXmlEntity.AddressReference.putAddressReference("MT"+objEntity.EntityId.ToString(), string.Empty, string.Empty, string.Empty, string.Empty);

                CreateAddressNodeForFunds(objAddress, objXmlEntity.AddressReference.Address,objFunds.MailToEid);
            }
            finally
            {
                if (objFunds != null)
                {
                    objFunds.Dispose();
                    objFunds = null;
                }
            }

        }

        
        private static XmlDocument CreateVouchersNode(XmlDocument objDoc)
        {
              XMLVouchers objVouchers  = null;
              try
              {
                  if (objSplitList.Count > 0)
                  {
                      objVouchers = new XMLVouchers();
                      XmlNode objNode = objVouchers.Create(objDoc, "");
                      foreach (int iSplitId in objSplitList)
                      {
                          CreateVoucherNode(iSplitId, objVouchers.Voucher);
                      }

                  }
              }
              finally
              {
                  objVouchers = null;
              }

            return objDoc;


        }
        private static void CreateEntityNode(int iEntityID,XMLEntity objXmlEntity,string sValue, string pClientFileSetting)
        {
            Entity objEntity=null;
            string sGender = string.Empty;
            string sGenderCode = string.Empty;
            string[] sEntityDate= null;
            int iEntityXAddrId = 0;
            string sSSN = string.Empty;
            string sAddrSeqNum=string.Empty;
            EntityXAddresses objEntityXAddresses = null;
            try
            {
                objEntity = (Entity)m_objFactory.GetDataModelObject("Entity", false);
                objEntity.MoveTo(iEntityID);

                sSSN = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(objEntity.Context.DbConn.ConnectionString, "SELECT TAX_ID FROM ENTITY WHERE ENTITY_ID =" + iEntityID));
                if (objEntity.NameType == objEntity.Context.LocalCache.GetCodeId("IND", "ENTITY_NAME_TYPE") || CheckAdditionalConditions(objEntity))            //Ankit Start : Work for StoneTrust Issue
                {                    
                    objXmlEntity.Create(iEntityID.ToString(), entType.entTypeInd, sSSN, entTaxIdType.entTaxIdSSN, objEntity.FirstName, objEntity.MiddleName, objEntity.LastName, "", "");
                    if (string.IsNullOrEmpty(objEntity.FirstName) && objEntity.LastName.Length > 15)
                    {
                        objXmlEntity.LastName = objEntity.LastName.Substring(0, 15);
                        //syadav55 Jira 6419,6420 starts  
                        // objXmlEntity.FirstName = objEntity.LastName.Substring(15);
                        objXmlEntity.FirstName = (!string.IsNullOrEmpty(objEntity.LastName) && objEntity.LastName.Length - 15 > iLlength && iLlength > 0) ? objEntity.LastName.Substring(15, iLlength) : objEntity.LastName.Substring(15);
                                               
                        objXmlEntity.MiddleName = (!string.IsNullOrEmpty(objEntity.MiddleName) && objEntity.MiddleName.Length > iMlength && iMlength > 0) ? objEntity.MiddleName.Substring(0, iMlength) : objEntity.MiddleName;
                        //syadav55 Jira 6419,6420 ends
                    }
                    else
                    {
                        //syadav55 Jira 6419,6420 starts                       
                        objXmlEntity.LastName = (!string.IsNullOrEmpty(objEntity.LastName) && objEntity.LastName.Length > iLlength && iLlength > 0) ? objEntity.LastName.Substring(0, iLlength) : objEntity.LastName;
                        objXmlEntity.FirstName = (!string.IsNullOrEmpty(objEntity.FirstName) && objEntity.FirstName.Length > iFlength && iFlength > 0) ? objEntity.FirstName.Substring(0, iFlength) : objEntity.FirstName;
                        objXmlEntity.MiddleName = (!string.IsNullOrEmpty(objEntity.MiddleName) && objEntity.MiddleName.Length > iMlength && iMlength > 0) ? objEntity.MiddleName.Substring(0, iMlength) : objEntity.MiddleName;
                        //syadav55 Jira 6419,6420 ends
                    }
					objXmlEntity.ContactInfo = objEntity.Phone2; //JIRA 7031: Merged changes
                }
                // bpaskova Jira 6585 start
                //else
                //{
                //    objXmlEntity.Create(iEntityID.ToString(), entType.entTypeBus, sSSN, entTaxIdType.entTaxIdSSN, objEntity.FirstName, objEntity.MiddleName, objEntity.LastName, "", "");
                //    objXmlEntity.ContactInfo = objEntity.Phone1;
                //    objXmlEntity.LegalName = objEntity.LastName;
                //}
                else if (objEntity.NameType == objEntity.Context.LocalCache.GetCodeId("BUS", "ENTITY_NAME_TYPE"))
                {
                    objXmlEntity.Create(iEntityID.ToString(), entType.entTypeBus, sSSN, entTaxIdType.entTaxIdSSN, objEntity.FirstName, objEntity.MiddleName, objEntity.LastName, "", "");
                    objXmlEntity.ContactInfo = objEntity.Phone1;
                    objXmlEntity.LegalName = objEntity.LastName;
                }    
                else
                {                    
                    objXmlEntity.Create(iEntityID.ToString(), entType.entTypeBus, sSSN, entTaxIdType.entTaxIdSSN, objEntity.FirstName, objEntity.MiddleName, objEntity.LastName, "", "");
                    objXmlEntity.ContactInfo = objEntity.Phone1;
                    objXmlEntity.LegalName = concatPersonName(objEntity);   
                }
            
                objEntity.Context.LocalCache.GetCodeInfo(objEntity.SexCode, ref sGenderCode, ref  sGender);
                objXmlEntity.Gender = sGender;
                objXmlEntity.Gender_Code = sGenderCode;

                if (!string.IsNullOrEmpty(sValue))
                {
                    sEntityDate = sValue.Split(',');
                    //iEntityXAddrId = GetEntityAddrSeqNum(Conversion.ConvertStrToInteger(sEntityDate[1]), iEntityID);// Moved into null check.
                }

                objXmlEntity.IDNumber = objEntity.ClientSequenceNumber;
                objXmlEntity.AddressSeqNum = objEntity.AddressSequenceNumber.ToString();


               
              
                objXmlEntity.DateOfBirth = Riskmaster.Common.Conversion.GetDate(objEntity.BirthDate);
                //if (sEntityDate != null) //psarin2 added null check
                //{
                //    if (string.IsNullOrEmpty(objEntity.BirthDate) && (sEntityDate.Length > 1) && string.Equals(sEntityDate[0], "true", StringComparison.InvariantCultureIgnoreCase) && pClientFileSetting.Trim()=="1") // CLIENTFILE_FLAG ==1 means CF on; CLIENTFILE_FLAG  == 0 means CF off
                //    {
                //        objXmlEntity.DateOfBirth = Riskmaster.Common.Conversion.GetDate(sEntityDate[2]);
                //    }
                //}
                if (iEntityXAddrId == 0)
                {
                    foreach (EntityXAddresses objAdd in objEntity.EntityXAddressesList)
                    {
                        objEntityXAddresses = objAdd;
                        break;
                    }
                }
                else
                {
                    objEntityXAddresses = (EntityXAddresses)m_objFactory.GetDataModelObject("EntityXAddresses", false);
                    objEntityXAddresses.MoveTo(iEntityXAddrId);
                }

                if (objEntityXAddresses != null)
                {
                    objXmlEntity.AddressReference.putAddressReference(objEntityXAddresses.AddressId.ToString(), string.Empty, string.Empty, string.Empty, string.Empty);
                    // CreateAddressNode( objEntityXAddresses, objXmlEntity.AddressReference.Address, ref sAddrSeqNum);
                }
                else
                {
                    objXmlEntity.AddressReference.putAddressReference(objEntity.EntityId.ToString(), string.Empty, string.Empty, string.Empty, string.Empty);
                }
                CreateAddressNode(objEntity, objEntityXAddresses, objXmlEntity.AddressReference.Address, ref sAddrSeqNum);

                objXmlEntity.AddressSeqNum = sAddrSeqNum;
               
            }
            finally
            {
                if (objEntity != null)
                    objEntity.Dispose();
            }


        }
        
        // bpaskova Jira 6585 start
        private static string concatPersonName(Entity objEntity)
        {
            string sPersonName = string.Empty;
            if (!string.IsNullOrWhiteSpace(objEntity.FirstName))
            {
                sPersonName = objEntity.FirstName.Trim() + " ";
            }
            if (!string.IsNullOrWhiteSpace(objEntity.MiddleName))
            {
                sPersonName += objEntity.MiddleName.Trim() + " ";
            }
            return sPersonName + objEntity.LastName;
        }
        // bpaskova Jira 6585 end  

        private static void CreateClaimantNode(int iClaimantRowID,XMLPartyInvolved objXmlPI, string ClaimLOB,int iPolicyId,string dateofloss)
        {
            Claimant objClaimant = null;
            try
            {
                objClaimant = (Claimant)m_objFactory.GetDataModelObject("Claimant", false);
                objClaimant.MoveTo(iClaimantRowID);

                if(!objEntityList.ContainsKey(objClaimant.ClaimantEid.ToString()))
                    objEntityList.Add(objClaimant.ClaimantEid.ToString(), "true," + iPolicyId + "," + dateofloss + "," + ClaimLOB);        //Ankit Start : Work for StoneTrust Issue

               if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.POINT)  //Payal (SIT Issue)
                   objXmlPI.putPartyInvolved(iClaimantRowID.ToString(), objClaimant.ClaimantEid.ToString(), "", "CLT");
               else
                   objXmlPI.putPartyInvolved(iClaimantRowID.ToString(), objClaimant.ClaimantEid.ToString(), "", "CL"); //Payal (SIT Issue)

                
            }
            finally
            {
                if (objClaimant != null)
                    objClaimant.Dispose();
            }

        }

        private static int GetEntityAddrSeqNum(int iPolicyId, int iTransEntityId)
        {
            int iEntityId = 0;
            int iAddrSeqNum = 0;
            int iAddrId = 0;
            if (iPolicyId > 0)
            {
                // If entity is downloaded as insured for corresponding policy
                iAddrId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, " SELECT ADDRESS_ID FROM POLICY_X_INSURED WHERE POLICY_ID=" + iPolicyId), m_iClientId);
                using (DbReader objRdr = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT  ADDRESS_SEQ_NUM,ENTITY_ID FROM  ENTITY_X_ADDRESSES WHERE ADDRESS_ID=" + iAddrId))
                {
                    if (objRdr.Read())
                    {
                        iEntityId = Conversion.ConvertObjToInt(objRdr.GetValue("ENTITY_ID"), m_iClientId);
                        iAddrSeqNum = Conversion.ConvertObjToInt(objRdr.GetValue("ADDRESS_SEQ_NUM"), m_iClientId);
                    }
                }

                //If entity is not found in POLICY_X_INSURED.This means entity was downloaded as additional interest list
                if (iAddrId ==0)
                {
                    
                    iAddrId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, " SELECT ADDRESS_ID FROM POLICY_X_ENTITY WHERE POLICY_ID=" + iPolicyId), m_iClientId);
                    using (DbReader objRdr = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT  ADDRESS_SEQ_NUM,ENTITY_ID FROM  ENTITY_X_ADDRESSES WHERE ADDRESS_ID=" + iAddrId))
                    {
                        if (objRdr.Read())
                        {
                            iEntityId = Conversion.ConvertObjToInt(objRdr.GetValue("ENTITY_ID"), m_iClientId);
                            iAddrSeqNum = Conversion.ConvertObjToInt(objRdr.GetValue("ADDRESS_SEQ_NUM"), m_iClientId);
                        }
                    }
                }
            }

            if (iEntityId == iTransEntityId)
            {
                return iAddrId;
            }
            else
            {
                return 0;
            }




        }

        //syadav55 Jira 6419,6420 starts
        public static void SetEntityNameLength()
        {
            XmlDocument xml = new XmlDocument();
            string sSQL = string.Empty;
            string sEntityLengthXML = string.Empty;
            string sModuleName = string.Empty;
            int iPointType; // Payal;RMA-9513
            XmlNode xmlNode;
            try
            {
                sSQL = @"SELECT RELEASE_VERSION FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID = " + iPolicySystemId;
                // int iPointType = Convert.ToInt32(DbFactory.ExecuteScalar(m_objFactory.Context.DbConn.ConnectionString, sSQL)); // Commented for Payal; RMA-9513
                iPointType = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objFactory.Context.DbConn.ConnectionString, sSQL), m_iClientId); // Payal; RMA-9513

                sEntityLengthXML = RMConfigurator.UserDataPath + "\\PolicyInterface\\Entity_Length_Specifications.xml";                
                
                if (!File.Exists(sEntityLengthXML))
                {
                    Console.WriteLine("0 ^*^*^ Entity_Length_Specifications.xml is missing");
                    throw new Exception("Upload stopped Entity_Length_Specifications.xml is missing");
                }
                xml.Load(RMConfigurator.UserDataPath + "\\PolicyInterface\\Entity_Length_Specifications.xml"); //load xml

                if (iPointType == 15) //PIJ
                {
                    sModuleName = "PIJ";
                }
                else if (iPointType < 15) //Point Classic
                {
                    sModuleName = "PointClassic";
                }

                xmlNode = xml.SelectSingleNode("/instance/" + sModuleName + "/LastName"); 
                if (xmlNode != null && !string.IsNullOrEmpty(xmlNode.InnerText))
                   iLlength = Convert.ToInt32(xmlNode.InnerText);

                xmlNode = xml.SelectSingleNode("/instance/" + sModuleName + "/FirstName");
                if (xmlNode != null && !string.IsNullOrEmpty(xmlNode.InnerText))
                    iFlength = Convert.ToInt32(xmlNode.InnerText);

                xmlNode = xml.SelectSingleNode("/instance/" + sModuleName + "/MiddleName");
                if (xmlNode != null && !string.IsNullOrEmpty(xmlNode.InnerText))
                    iMlength = Convert.ToInt32(xmlNode.InnerText);

                xmlNode = xml.SelectSingleNode("/instance/" + sModuleName + "/ADDRLN1");
                if (xmlNode != null && !string.IsNullOrEmpty(xmlNode.InnerText))
                    iAdd1length = Convert.ToInt32(xmlNode.InnerText);

                xmlNode = xml.SelectSingleNode("/instance/" + sModuleName + "/ADDRLN2");
                if (xmlNode != null && !string.IsNullOrEmpty(xmlNode.InnerText))
                    iAdd2length = Convert.ToInt32(xmlNode.InnerText);

                xmlNode = xml.SelectSingleNode("/instance/" + sModuleName + "/ADDRLN3");
                if (xmlNode != null && !string.IsNullOrEmpty(xmlNode.InnerText))
                    iAdd3length = Convert.ToInt32(xmlNode.InnerText);

                xmlNode = xml.SelectSingleNode("/instance/" + sModuleName + "/ADDRLN4");
                if (xmlNode != null && !string.IsNullOrEmpty(xmlNode.InnerText))
                    iAdd4length = Convert.ToInt32(xmlNode.InnerText);               
            }
            catch (Exception exe)
            {
                throw exe;
            }
        }
        //syadav55 Jira 6419,6420 ends

        private static void CreateAddressNode(Entity objEntity,EntityXAddresses objEntityXAddresses, XMLAddress objXmlAddr,ref string sAddrSeqNum)
        {
            //EntityXAddresses objEntityXAddresses = null;
            // Entity objEntity = null;
            string sState = string.Empty;
            string sStateCode = string.Empty;
            string sCountry = string.Empty;
            string sCountryCode = string.Empty;
            int iStateCode = 0;
            try
            {
                //if (iAddrRowID == 0)
                //{
                //    objEntity = (Entity)m_objFactory.GetDataModelObject("Entity", false);
                //    objEntity.MoveTo(iEntityId);
                //    foreach (EntityXAddresses objAdd in objEntity.EntityXAddressesList)
                //    {
                //        objXmlAddr.AddressID = objAdd.AddressId.ToString();
                //        objXmlAddr.City = objAdd.City;
                //        objXmlAddr.PostalCode = objAdd.ZipCode;

                //        objXmlAddr.addAddressLine(objAdd.Addr1);
                //        objXmlAddr.addAddressLine(objAdd.Addr2);

                //        objAdd.Context.LocalCache.GetCodeInfo(objAdd.Country, ref sCountryCode, ref sCountry);
                //        iStateCode = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCodeIDFromRMXCodeId(objAdd.State, "STATES", "State on CreateAddressNode", iPolicySystemId.ToString());
                //        if (iStateCode > 0)
                //            objAdd.Context.LocalCache.GetCodeInfo(iStateCode, ref sStateCode, ref sState);
                //        else
                //            objAdd.Context.LocalCache.GetStateInfo(iStateCode, ref sStateCode, ref sState);
                //        objXmlAddr.County = sCountry;
                //        objXmlAddr.County_Code = sCountryCode;

                //        objXmlAddr.State = sState;
                //        objXmlAddr.State_Code = sStateCode;
                //        sAddrSeqNum = objAdd.AddressSeqNum.ToString();
                //        break;
                //    }
                //    //objXmlAddr.AddressID = iAddrRowID.ToString();
                //    //objXmlAddr.City = objEntity.City;
                //    //objXmlAddr.PostalCode = objEntity.ZipCode;

                //    //objXmlAddr.addAddressLine(objEntity.Addr1);
                //    //objXmlAddr.addAddressLine(objEntity.Addr2);

                //    //objEntity.Context.LocalCache.GetCodeInfo(objEntity.CountryCode, ref sCountryCode, ref sCountry);
                //    //iStateCode = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCodeIDFromRMXCodeId(objEntity.StateId, "STATES", "State on CreateAddressNode", iPolicySystemId.ToString());
                //    //if (iStateCode > 0)
                //    //    objEntity.Context.LocalCache.GetCodeInfo(iStateCode, ref sStateCode, ref sState);
                //    //else
                //    //    objEntity.Context.LocalCache.GetStateInfo(iStateCode, ref sStateCode, ref sState);
                //    //objXmlAddr.County = sCountry;
                //    //objXmlAddr.County_Code = sCountryCode;

                //    //objXmlAddr.State = sState;
                //    //objXmlAddr.State_Code = sStateCode;



                //}
                //else
                //{
                // objEntityXAddresses = (EntityXAddresses)m_objFactory.GetDataModelObject("Entity", false);
                //objEntityXAddresses.MoveTo(iAddrRowID);
                if (objEntityXAddresses != null)
                {
					//RMA-8753 nshah28(Added by ashish) START
                    objXmlAddr.AddressID = objEntityXAddresses.AddressId.ToString();
                    objXmlAddr.City = objEntityXAddresses.Address.City;
                    objXmlAddr.PostalCode = objEntityXAddresses.Address.ZipCode;

                    //syadav55 Jira 6419,6420 starts
                    objEntityXAddresses.Address.Addr1 = (!string.IsNullOrEmpty(objEntityXAddresses.Address.Addr1) && objEntityXAddresses.Address.Addr1.Length > iAdd1length && iAdd1length > 0) ? objEntityXAddresses.Address.Addr1.Substring(0, iAdd1length) : objEntityXAddresses.Address.Addr1;
                    objEntityXAddresses.Address.Addr2 = (!string.IsNullOrEmpty(objEntityXAddresses.Address.Addr2) && objEntityXAddresses.Address.Addr2.Length > iAdd2length && iAdd2length > 0) ? objEntityXAddresses.Address.Addr2.Substring(0, iAdd2length) : objEntityXAddresses.Address.Addr2;
                    objEntityXAddresses.Address.Addr3 = (!string.IsNullOrEmpty(objEntityXAddresses.Address.Addr3) && objEntityXAddresses.Address.Addr3.Length > iAdd3length && iAdd3length > 0) ? objEntityXAddresses.Address.Addr3.Substring(0, iAdd3length) : objEntityXAddresses.Address.Addr3;
                    objEntityXAddresses.Address.Addr4 = (!string.IsNullOrEmpty(objEntityXAddresses.Address.Addr4) && objEntityXAddresses.Address.Addr4.Length > iAdd4length && iAdd4length > 0) ? objEntityXAddresses.Address.Addr4.Substring(0, iAdd4length) : objEntityXAddresses.Address.Addr4;
                    
                    objXmlAddr.addAddressLine(objEntityXAddresses.Address.Addr1);
                    objXmlAddr.addAddressLine(objEntityXAddresses.Address.Addr2);
                    objXmlAddr.addAddressLine(objEntityXAddresses.Address.Addr3);
                    objXmlAddr.addAddressLine(objEntityXAddresses.Address.Addr4);
                    //syadav55 Jira 6419,6420 ends

                    objEntityXAddresses.Context.LocalCache.GetCodeInfo(objEntityXAddresses.Address.Country, ref sCountryCode, ref sCountry);
                    iStateCode = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCodeIDFromRMXCodeId(objEntityXAddresses.Address.State, "STATES", "State on CreateAddressNode", iPolicySystemId.ToString());
                    if (iStateCode > 0)
                        objEntityXAddresses.Context.LocalCache.GetCodeInfo(iStateCode, ref sStateCode, ref sState);
                    else
                        objEntityXAddresses.Context.LocalCache.GetStateInfo(iStateCode, ref sStateCode, ref sState);
                    objXmlAddr.County = sCountry;
                    objXmlAddr.County_Code = sCountryCode;

                    objXmlAddr.State = sState;
                    objXmlAddr.State_Code = sStateCode;
                    sAddrSeqNum = objEntityXAddresses.Address.AddressSeqNum.ToString();
					//RMA-8753 nshah28(Added by ashish) END
                }
                else
                {
                    objXmlAddr.AddressID = objEntity.EntityId.ToString();
                    objXmlAddr.City = objEntity.City;
                    objXmlAddr.PostalCode = objEntity.ZipCode;

                    //syadav55 Jira 6419,6420 starts
                    objEntity.Addr1 = (!string.IsNullOrEmpty(objEntity.Addr1) && objEntity.Addr1.Length > iAdd1length && iAdd1length > 0) ? objEntity.Addr1.Substring(0, iAdd1length) : objEntity.Addr1;
                    objEntity.Addr2 = (!string.IsNullOrEmpty(objEntity.Addr2) && objEntity.Addr2.Length > iAdd2length && iAdd2length > 0) ? objEntity.Addr2.Substring(0, iAdd2length) : objEntity.Addr2;
                    objEntity.Addr3 = (!string.IsNullOrEmpty(objEntity.Addr3) && objEntity.Addr3.Length > iAdd3length && iAdd3length > 0) ? objEntity.Addr3.Substring(0, iAdd3length) : objEntity.Addr3;
                    objEntity.Addr4 = (!string.IsNullOrEmpty(objEntity.Addr4) && objEntity.Addr4.Length > iAdd4length && iAdd4length > 0) ? objEntity.Addr4.Substring(0, iAdd4length) : objEntity.Addr4;
                    //syadav55 Jira 6419,6420 ends
                    objXmlAddr.addAddressLine(objEntity.Addr1);
                    objXmlAddr.addAddressLine(objEntity.Addr2);
                    objXmlAddr.addAddressLine(objEntity.Addr3);
                    objXmlAddr.addAddressLine(objEntity.Addr4);

                    objEntity.Context.LocalCache.GetCodeInfo(objEntity.CountryCode, ref sCountryCode, ref sCountry);
                    iStateCode = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCodeIDFromRMXCodeId(objEntity.StateId, "STATES", "State on CreateAddressNode", iPolicySystemId.ToString());
                    if (iStateCode > 0)
                        objEntity.Context.LocalCache.GetCodeInfo(iStateCode, ref sStateCode, ref sState);
                    else
                        objEntity.Context.LocalCache.GetStateInfo(iStateCode, ref sStateCode, ref sState);
                    objXmlAddr.County = sCountry;
                    objXmlAddr.County_Code = sCountryCode;

                    objXmlAddr.State = sState;
                    objXmlAddr.State_Code = sStateCode;
                    //   sAddrSeqNum = objEntity.AddressSequenceNumber.ToString();
                    // }
                }
            }
            finally
            {
                //if (objEntityXAddresses != null)
                //    objEntityXAddresses.Dispose();

                //if (objEntity != null)
                //    objEntity.Dispose();
            }

        }

        private static void CreateAddressNodeForFunds(Address objAddress, XMLAddress objXmlAddr, int iMailToAddressId)
        {
            string sState = string.Empty;
            string sStateCode = string.Empty;
            string sCountry = string.Empty;
            string sCountryCode = string.Empty;
            int iStateCode = 0;
            try
            {

                objXmlAddr.AddressID = "MT" + iMailToAddressId;
                objXmlAddr.City = objAddress.City;
                objXmlAddr.PostalCode = objAddress.ZipCode;

                //syadav55 Jira 6419,6420 starts
                objAddress.Addr1 = (!string.IsNullOrEmpty(objAddress.Addr1) && objAddress.Addr1.Length > iAdd1length && iAdd1length > 0) ? objAddress.Addr1.Substring(0, iAdd1length) : objAddress.Addr1;
                objAddress.Addr2 = (!string.IsNullOrEmpty(objAddress.Addr2) && objAddress.Addr2.Length > iAdd2length && iAdd2length > 0) ? objAddress.Addr2.Substring(0, iAdd2length) : objAddress.Addr2;
                objAddress.Addr3 = (!string.IsNullOrEmpty(objAddress.Addr3) && objAddress.Addr3.Length > iAdd3length && iAdd3length > 0) ? objAddress.Addr3.Substring(0, iAdd3length) : objAddress.Addr3;
                objAddress.Addr4 = (!string.IsNullOrEmpty(objAddress.Addr4) && objAddress.Addr4.Length > iAdd4length && iAdd4length > 0) ? objAddress.Addr4.Substring(0, iAdd4length) : objAddress.Addr4;
                
                objXmlAddr.addAddressLine(objAddress.Addr1);
                objXmlAddr.addAddressLine(objAddress.Addr2);
                objXmlAddr.addAddressLine(objAddress.Addr3);
                objXmlAddr.addAddressLine(objAddress.Addr4);
                //syadav55 Jira 6419,6420 ends

                objAddress.Context.LocalCache.GetCodeInfo(objAddress.Country, ref sCountryCode, ref sCountry);
                iStateCode = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCodeIDFromRMXCodeId(objAddress.State, "STATES", "State on CreateAddressNode", iPolicySystemId.ToString());
                if (iStateCode > 0)
                    objAddress.Context.LocalCache.GetCodeInfo(iStateCode, ref sStateCode, ref sState);
                else
                    objAddress.Context.LocalCache.GetStateInfo(iStateCode, ref sStateCode, ref sState);
                objXmlAddr.County = sCountry;
                objXmlAddr.County_Code = sCountryCode;

                objXmlAddr.State = sState;
                objXmlAddr.State_Code = sStateCode;

            }
            finally
            {

            }

        }

        private static void GetPolicyUnitData(int iUnitId, string sUnitType, ref string sRiskLoc, ref string sSubLoc, ref string sInsLine, ref string sUnitNo) //Payal: RMA-6471  added sUnitNo
        {
            if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.POINT) //Payal: RMA-6471 
            {
                using (DbReader objRdr = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT UNIT_RISK_LOC,UNIT_RISK_SUB_LOC,INS_LINE,UNIT_NUMBER FROM POINT_UNIT_DATA WHERE UNIT_ID = " + iUnitId + " AND UNIT_TYPE='" + sUnitType + "'")) //Payal: RMA-6471 --added UNIT_NUMBER
            {
                if (objRdr.Read())
                {
                    sRiskLoc = Riskmaster.Common.Conversion.ConvertObjToStr(objRdr.GetValue(0));
                    sSubLoc = Riskmaster.Common.Conversion.ConvertObjToStr(objRdr.GetValue(1));
                    sInsLine= Riskmaster.Common.Conversion.ConvertObjToStr(objRdr.GetValue(2));
                        sUnitNo = Riskmaster.Common.Conversion.ConvertObjToStr(objRdr.GetValue(3)); //Payal: RMA-6471 
                    }
                }
            }
            else
            {
                using (DbReader objRdr = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT UNIT_NUMBER FROM INTEGRAL_UNIT_DATA WHERE UNIT_ID = " + iUnitId + " AND UNIT_TYPE='" + sUnitType + "'"))
                    if (objRdr.Read())
                    {
                        sUnitNo = Riskmaster.Common.Conversion.ConvertObjToStr(objRdr.GetValue(0));

        }
            }

        }
        //Payal: RMA-6471 --Starts
        private static void UpdateActivityIDS()
        {
            string sSQL = null;
            try
            {
                if (!string.IsNullOrEmpty(sActivityRowIds))
                {
                    sSQL = "UPDATE ACTIVITY_TRACK SET UPLOAD_FLAG = 0 WHERE ACTIVITY_ROW_ID IN ( " + sActivityRowIds + ")";
                    Riskmaster.Db.DbFactory.ExecuteNonQuery(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Ankit Start : Work for StoneTrust Issue
        private static bool CheckAdditionalConditions(Entity oEntity)
        {
            bool blnReturn = false;
            //int intLOB = 0;
            //bool success;
            //Dictionary<string, string> dictParams = null;

            string strEValues;
            string[] arrValues;
            string strLOB = string.Empty;

            if (objEntityList.ContainsKey(oEntity.EntityId.ToString()))
                strEValues = objEntityList[oEntity.EntityId.ToString()];
            else
                strEValues = string.Empty;

            if (!string.IsNullOrEmpty(strEValues))
            {
                arrValues = strEValues.Split(',');
                if (arrValues.Length > 3)
                    strLOB = arrValues[3];
            }
            //dictParams = new Dictionary<string, string>();
            //dictParams.Add("CLAIMID", oEntity.ClaimId.ToString());
            //intLOB = Conversion.CastToType<int>(DbFactory.ExecuteScalar(oEntity.Context.DbConn.ConnectionString, string.Format("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID={0}", "~CLAIMID~"), dictParams).ToString(), out success);
            //commented for entity roles
            //if (string.Equals(oEntity.Context.LocalCache.GetTableName(oEntity.EntityTableId).ToUpper(), "EMPLOYEES", StringComparison.OrdinalIgnoreCase)
            //    && string.Equals(strLOB, "WC", StringComparison.OrdinalIgnoreCase))
           
            /*smishra54 - This code need to be change as per new approach of entity role, by the time commenting it as build is breaking */
            //if (oEntity.Context.InternalSettings.SysSettings.UseEntityRole)
            //{
            //    if ((oEntity.IsEntityRoleExists(oEntity.EntityXRoleList, oEntity.Context.LocalCache.GetTableId("EMPLOYEES")) > 0) && string.Equals(strLOB, "WC", StringComparison.OrdinalIgnoreCase))
            //    {
            //        blnReturn = true;
            //    }
            //}
            //else
            //{
            if (string.Equals(oEntity.Context.LocalCache.GetTableName(oEntity.EntityTableId).ToUpper(), "EMPLOYEES", StringComparison.OrdinalIgnoreCase)
                && string.Equals(strLOB, "WC", StringComparison.OrdinalIgnoreCase))
            {
                blnReturn = true;
                }
            /*}
            smishra54 - End */
            return blnReturn;
        }
        //Ankit End
        private static void CreateLossDetailsNode(XMLLossDetail objXmlLossDetails, int iClaimantEID, int iClaimantNumber, int iReserveTypeCode, int iPolCvgRowId, int iRsrvRowId, int iClaimId, int iReserveClmntSeqNo, int iClaimTypeCd, int iLineOfBusCd) //MITS 32000:aaggarwal29
        {

            LocalCache objCache = null;
            int iPolRsrvCodeId = 0;
            PolicyXCvgType objPolicyXCvg =null;
              int iUnitId=0;
             int iLocation=0; 
             int iEffDate=0;
             int iPolicyId = 0;
             string sUnitType = string.Empty;
            int iCvgCode = 0;
            string sRiskLoc = string.Empty;
            string sSubLoc = string.Empty;
           string sInsLine=string.Empty;//skhare7 Changed for Recovery reserve
            //CvgXLoss objCvgXLoss = null;
            //Payal: RMA-6471 --Starts
            string sUnitNo = string.Empty;
            string sRiskType = string.Empty;
            XElement objElement = null;
            XElement oElements = null;
            string sCode = string.Empty;
            string sDescription = string.Empty;
            //Payal: RMA-6471 --Ends
            ReserveCurrent objReserveCurrent = null;

            try
            {
                objCache = new LocalCache(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);
                objReserveCurrent = (ReserveCurrent)m_objFactory.GetDataModelObject("ReserveCurrent", false);//vkumar258 
                objReserveCurrent.MoveTo(iRsrvRowId);

                // Payal --Starts RMA:7484 
                if (!IsExistingLossDetailNode(objXmlLossDetails, iRsrvRowId))
                {
                    objXmlLossDetails.Create();
                }
                // Payal --Ends RMA:7484 

                if (iClaimantNumber == 0)
                    iClaimantNumber = 1;
                
                objXmlLossDetails.ClaimantNumber = iClaimantNumber.ToString();
                objXmlLossDetails.LossDetailID = iRsrvRowId.ToString(); // Payal RMA:7484 
                objXmlLossDetails.ClaimantSeq = iReserveClmntSeqNo.ToString();
                objXmlLossDetails.ClaimantEntityID = iClaimantEID.ToString();
                iPolRsrvCodeId = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(iReserveTypeCode, "RESERVE_TYPE", "ReserveType on CreateLossDetailsNode", iPolicySystemId.ToString(),iClaimTypeCd,iLineOfBusCd); //Mits32000 : aaggarwal29
                
                objXmlLossDetails.ReserveType= objCache.GetCodeDesc(iPolRsrvCodeId);
                if(PolicySystemTypeIndicator==Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.POINT) //RMA: 14810
                   objXmlLossDetails.ReserveType_Code = objCache.GetShortCode(iPolRsrvCodeId);
               //RMA: 14810 Payal starts
                else
                {
                    if ((objCache.GetShortCode(objCache.GetRelatedCodeId(objReserveCurrent.ReserveTypeCode))) == "R")
                        objXmlLossDetails.ReserveType_Code = objCache.GetShortCode(iPolRsrvCodeId);
                    else
                        objXmlLossDetails.ReserveType_Code = objCache.GetShortCode(iPolRsrvCodeId).Substring(3, 2);
                }
                //RMA: 14810 Payal Ends



                //objCvgXLoss = (CvgXLoss)m_objFactory.GetDataModelObject("CvgXLoss", false);
                //objCvgXLoss.MoveTo(icoveragelossId);

                   objPolicyXCvg = (PolicyXCvgType)m_objFactory.GetDataModelObject("PolicyXCvgType", false);
                   objPolicyXCvg.MoveTo(iPolCvgRowId);

                   GetCoverageDetailsFromPolxCvg(iPolCvgRowId, ref iUnitId, ref iLocation, ref iEffDate, ref iPolicyId, ref sUnitType);
                objXmlLossDetails.PolicyID = iPolicyId.ToString();
                objXmlLossDetails.CoverageEffDate = Riskmaster.Common.Conversion.GetDate(objPolicyXCvg.EffectiveDate);
                objXmlLossDetails.CoverageSeq = objPolicyXCvg.CvgSequenceNo;
                iCvgCode = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(objPolicyXCvg.CoverageTypeCode, "COVERAGE_TYPE", "CoverageType on CreateLossDetailsNode", iPolicySystemId.ToString(), iClaimTypeCd, iLineOfBusCd); //MITS 32000 : aaggarwal29

               if (iUnitId > 0)
               {
                    GetPolicyUnitData(iUnitId, sUnitType, ref sRiskLoc, ref sSubLoc, ref sInsLine, ref sUnitNo);//skhare7 Changed for recovery reserve sUnitNo added by //Payal: RMA-6471 
                   objXmlLossDetails.CoverageLocation = sRiskLoc;
                   objXmlLossDetails.CoverageSubLocation = sSubLoc;
                  // string sIssueCode = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetDownLoadXMLNodeSpecificData("POLICY", iPolicyId, iPolicyId, "com.csc_IssueCd", false, string.Empty);
                   objXmlLossDetails.CoverageInsLine_Code = sInsLine;//skhare7 Recovery Reserve
                    objXmlLossDetails.TotalUnitNumber = sUnitNo; //Payal: RMA-6471 
               }
              if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.POINT) //Payal (SIT Issue) (RMA:7484)
              {
                   objXmlLossDetails.CoveragePeril_Code = objCache.GetShortCode(iCvgCode);
                   objXmlLossDetails.CoveragePeril = objCache.GetCodeDesc(iCvgCode);
              }
             else
             {
                    objXmlLossDetails.CoveragePeril_Code = objPolicyXCvg.CvgClassCode; // Done by Payal : SIT Issue (RMA:7484)
                    objXmlLossDetails.CoveragePeril = objPolicyXCvg.CvgClassCode; // Done by Payal : SIT Issue (RMA:7484)
             }

                //Payal: RMA-6471 --Starts
                if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.INTEGRAL)
                {
                    sRiskType = new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString,m_iClientId).GetDownLoadXMLData("POLICY_X_UNIT", objPolicyXCvg.PolicyUnitRowId, iPolicyId);
                    objElement = XElement.Parse(sRiskType);
                    oElements = objElement.XPathSelectElement("//RiskType");
                    sCode = oElements.Attribute("code").Value.Trim();
                    sDescription = oElements.Attribute("description").Value.Trim();
                    objXmlLossDetails.RiskType = sDescription;
                    objXmlLossDetails.RiskType_Code = sCode;
                }
                //Payal: RMA-6471 --Ends  
               //vkumar258 RMA-7484 Starts
                objXmlLossDetails.TotalPaid = objReserveCurrent.PaidTotal.ToString();
                objXmlLossDetails.Incurred = objReserveCurrent.IncurredAmount.ToString();
                //vkumar258 RMA-7484 End

            }
            finally
            {
                if(objCache!=null)
                  objCache.Dispose();
                if (objReserveCurrent != null)
                  objReserveCurrent.Dispose();

            }
        }

        private static void GetCoverageDetailsFromPolxCvg(int iPolCvgRowId, ref int iUnitId, ref int iLocation, ref int iEffDate, ref int iPolicyId, ref string sUnitType)
        {
            PolicyXCvgType objPolicyXCvg = null;

            PolicyXUnit objPolicyUnit = null;

            try
            {
                objPolicyXCvg = (PolicyXCvgType)m_objFactory.GetDataModelObject("PolicyXCvgType", false);
                objPolicyXCvg.MoveTo(iPolCvgRowId);

                objPolicyUnit = (PolicyXUnit)m_objFactory.GetDataModelObject("PolicyXUnit", false);
                objPolicyUnit.MoveTo(objPolicyXCvg.PolicyUnitRowId);

                iPolicyId = objPolicyUnit.PolicyId;
                sUnitType = objPolicyUnit.UnitType;
                iUnitId = objPolicyUnit.UnitId;

            }
            finally
            {
                if (objPolicyXCvg != null)
                    objPolicyXCvg.Dispose();
                if (objPolicyUnit != null)
                    objPolicyUnit.Dispose();
            }
        }
        private static void GetCoverageDetailsFromCvgLoss(int iCoverageId, ref int iUnitId, ref int iLocation, ref int iEffDate,ref int iPolicyId,ref string sUnitType)
        {
            PolicyXCvgType objPolicyXCvg = null;
            
            PolicyXUnit objPolicyUnit=null;
            
            CvgXLoss objCvgXLoss = null;
            try
            {
                objCvgXLoss = (CvgXLoss)m_objFactory.GetDataModelObject("CvgXLoss", false);
                objCvgXLoss.MoveTo(iCoverageId);

            objPolicyXCvg = (PolicyXCvgType)m_objFactory.GetDataModelObject("PolicyXCvgType", false);
            objPolicyXCvg.MoveTo(objCvgXLoss.PolCvgRowId);

             objPolicyUnit = (PolicyXUnit)m_objFactory.GetDataModelObject("PolicyXUnit", false);
             objPolicyUnit.MoveTo(objPolicyXCvg.PolicyUnitRowId);

            iPolicyId = objPolicyUnit.PolicyId;
            sUnitType = objPolicyUnit.UnitType;
            iUnitId = objPolicyUnit.UnitId;
            
        }
            finally
            {
                 if(objPolicyXCvg != null)
                 objPolicyXCvg.Dispose();
                 if(objPolicyUnit != null)
                     objPolicyUnit.Dispose();
                 if (objCvgXLoss != null)
                     objCvgXLoss.Dispose();
            }
        }
        private static string GetUnitNumber(int iUnitId,string sUnitType)
        {
            string sUnitNumber = string.Empty;
            //Added:Yukti, UNIT_NUMBER from INTEGRAL data
            if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.POINT)
            {
            using (DbReader objRdr = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT STAT_UNIT_NUMBER FROM POINT_UNIT_DATA WHERE UNIT_ID = " + iUnitId + " AND UNIT_TYPE='" + sUnitType + "'"))
            {
                if (objRdr.Read())
                    {
                        sUnitNumber = Riskmaster.Common.Conversion.ConvertObjToStr(objRdr.GetValue(0));
                    }
                }
            }
            else
            {
                using (DbReader objRdr = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT UNIT_NUMBER FROM INTEGRAL_UNIT_DATA WHERE UNIT_ID = " + iUnitId + " AND UNIT_TYPE='" + sUnitType + "'"))
                {
                    if (objRdr.Read())
                    {
                    sUnitNumber = Riskmaster.Common.Conversion.ConvertObjToStr(objRdr.GetValue(0));
                    }
                }
            }

            return sUnitNumber;

        }
        private static void GetCoverageSeqNo(int iPolCvgRowId, ref string sCoverageSeqNo,ref string sTransSeqNo)
        {
            PolicyXCvgType objPolicyXCvg = null;
           // CvgXLoss objCvgXLoss = null;
            try
            {
                //objCvgXLoss = (CvgXLoss)m_objFactory.GetDataModelObject("CvgXLoss", false);
                //objCvgXLoss.MoveTo(iCvgLossId);

                objPolicyXCvg = (PolicyXCvgType)m_objFactory.GetDataModelObject("PolicyXCvgType", false);
                objPolicyXCvg.MoveTo(iPolCvgRowId);
                sCoverageSeqNo = objPolicyXCvg.CvgSequenceNo;
                sTransSeqNo = objPolicyXCvg.TransSequenceNo;
            }
            finally
            {
                if(objPolicyXCvg!=null)
                objPolicyXCvg.Dispose();

            }
            

        }
        private static bool ClaimantHasOpenReserves(int iClaimId, int iClaimantEid)
        {
            int iopenReserveCount = 0;
            try
            {
              iopenReserveCount=  Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT COUNT(1) FROM RESERVE_CURRENT WHERE CLAIM_ID=" + iClaimId + " AND CLAIMANT_EID=" + iClaimantEid + " AND RES_STATUS_CODE IN ( SELECT CODE_ID FROM CODES WHERE RELATED_CODE_ID =" + m_objFactory.Context.LocalCache.GetCodeId("O", "RESERVE_STATUS_PARENT") + " AND TABLE_ID=" + m_objFactory.Context.LocalCache.GetTableId("RESERVE_STATUS") + ")"), m_iClientId);
                if (iopenReserveCount > 0)
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                //throw e;
            }

            return false;
        }
        private static bool CreatePaymentNode(int iTransSplitId, XMLLossDetail objXMLLossDetail, int iActvityRowId, int iActivityType, int iClaimTypeCd, int iLineOfBusCd, int iRsvStatusCode, bool bIsVoid, string sClaimNumber,int iMoveRsrvHistKey) //MITS 32000 : aaggarwal29
        {
            FundsTransSplit objSplit = null;
            Funds objFunds = null;
            Claimant objClaimant = null;
            ReserveCurrent objReserveCurrent = null;
            int iPolTransType = 0;
            int iPolRsrvCodeId = 0;
            int iUnitId=0;
            int iLocation=0;
            int iEffDate =0;
            int iPolicyId=0;
            Policy objPolicy = null;
            string sUnitType = string.Empty;
            ReserveHistory objReserveHistory = null;
           CvgXLoss objCvgXLoss=null;
           string sTransNo = string.Empty;
           string sCvgSeqNo = string.Empty;
           int iClaimantEid = 0;
           int iCvgLossRowId = 0;
           int iRsvTypeCode = 0;
           int iPolCovSeq = 0;
           int iLossCode = 0;
           int iPolCvgRowId = 0;
           RsvMoveHistKey objRsvMoveHist;
           bool bIntegralRecoveryCollection = false; //Payal for RMA-12744

            try
            {
                objSplit = (FundsTransSplit)m_objFactory.GetDataModelObject("FundsTransSplit", false);
                objSplit.MoveTo(iTransSplitId);

                objFunds = (Funds)m_objFactory.GetDataModelObject("Funds", false);
                objFunds.MoveTo(objSplit.TransId);

              
                int iRsrvId = objSplit.RCRowId;


                objReserveCurrent = (ReserveCurrent)m_objFactory.GetDataModelObject("ReserveCurrent", false);
                objReserveCurrent.MoveTo(iRsrvId);

                if (iMoveRsrvHistKey > 0)
                {
                    objRsvMoveHist = GetRsvMoveHistDetails(iRsrvId, iMoveRsrvHistKey);
                    iClaimantEid = objRsvMoveHist.ClaimantEid;
                    iCvgLossRowId = objRsvMoveHist.CvgLossRowId;
                    iPolCovSeq = objRsvMoveHist.PolCovSeq;
                    iLossCode = objRsvMoveHist.LossCode;
                    iRsvTypeCode = objRsvMoveHist.RsvTypeCode;
                    iPolCvgRowId = objRsvMoveHist.PolCvgRowId;

                    GetCoverageDetailsFromPolxCvg(iPolCvgRowId, ref iUnitId, ref iLocation, ref iEffDate, ref iPolicyId, ref sUnitType);


                }
                else
                {
                    GetCoverageDetailsFromCvgLoss(objReserveCurrent.CoverageLossId, ref iUnitId, ref iLocation, ref iEffDate, ref iPolicyId, ref sUnitType);

                    objCvgXLoss = (CvgXLoss)m_objFactory.GetDataModelObject("CvgXLoss", false);
                    objCvgXLoss.MoveTo(objReserveCurrent.CoverageLossId);

                    iClaimantEid = objReserveCurrent.ClaimantEid;
                    iCvgLossRowId = objReserveCurrent.CoverageLossId;
                    iPolCovSeq = objReserveCurrent.PolicyCvgSeqNo;
                    iLossCode = objCvgXLoss.LossCode;
                    iRsvTypeCode = objReserveCurrent.ReserveTypeCode;
                    iPolCvgRowId = objCvgXLoss.PolCvgRowId;
                }

                objClaimant = (Claimant)m_objFactory.GetDataModelObject("Claimant", false);
                objClaimant.MoveTo(GetClaimantRowdId(iClaimantEid, objFunds.ClaimId));
              //  GetCoverageDetails(objReserveCurrent.CoverageLossId, ref iUnitId, ref iLocation, ref iEffDate, ref iPolicyId, ref sUnitType);
                if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.POINT && HasBlankAddressDetails(iClaimantEid, iPolicyId, "PAYMENT", sClaimNumber, iRsrvId))
                {
                    return false;
                }

                objPolicy = (Policy)m_objFactory.GetDataModelObject("Policy", false);
                objPolicy.MoveTo(iPolicyId);
                iTransClaimantId = objClaimant.ClaimantRowId;
                iTransPolicyId = iPolicyId;
                //Commented by Payal --Starts RMA:7484
                /*
                if (objReserveCurrent.AssignAdjusterEid > 0)
                {
                    objXMLLossDetail.ReserveAdjusterExaminerCd = GetAdjusterExaminerCd(objReserveCurrent.AssignAdjusterEid, objReserveCurrent.ClaimId);
                }
                 *  * */
                //Commented by Payal --Ends RMA:7484
                if(iPolicySystemId == objPolicy.PolicySystemId)
                {
                    CreateLossDetailsNode(objXMLLossDetail, iClaimantEid, objClaimant.ClaimantNumber, iRsvTypeCode, iPolCvgRowId, iRsrvId, objFunds.ClaimId, iPolCovSeq, iClaimTypeCd, iLineOfBusCd);
                    // Payal --Starts RMA:7484
                    if (objReserveCurrent.AssignAdjusterEid > 0)
                    {
                        objXMLLossDetail.ReserveAdjusterExaminerCd = GetAdjusterExaminerCd(objReserveCurrent.AssignAdjusterEid, objReserveCurrent.ClaimId);
                    }
                    // Payal --Starts RMA:7484
                    GetCoverageSeqNo(iPolCvgRowId, ref sCvgSeqNo, ref sTransNo);

                    objXMLLossDetail.PolicyCoverageLegacyKey = GetUnitNumber(iUnitId, sUnitType) + "," + sCvgSeqNo + "," + sTransNo;

                //     objCvgXLoss = (CvgXLoss)m_objFactory.GetDataModelObject("CvgXLoss", false);
                //objCvgXLoss.MoveTo(objReserveCurrent.CoverageLossId);

                if (objXMLLossDetail.CoverageInsLine_Code == "WC")
                {
                    //MITS 32000 : aaggarwal29 start
                    objXMLLossDetail.LossDisability_Code = objReserveCurrent.Context.LocalCache.GetShortCode((new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(iLossCode, "DISABILITY_TYPE", "DisabilityTypeCode on CreatePaymentNode", iPolicySystemId.ToString(),iClaimTypeCd,iLineOfBusCd)); 
                    objXMLLossDetail.LossDisability = objReserveCurrent.Context.LocalCache.GetCodeDesc((new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(iLossCode, "DISABILITY_TYPE", "DisabilityTypeCode on CreatePaymentNode", iPolicySystemId.ToString(),iClaimTypeCd,iLineOfBusCd));
                    //MITS 32000 : aaggarwal29 end
                }
                else
                {
                    //MITS 32000 : aaggarwal29 start
                    objXMLLossDetail.CauseOfLoss_Code = objReserveCurrent.Context.LocalCache.GetShortCode((new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(iLossCode, "LOSS_CODES", "LossCodes on CreatePaymentNode", iPolicySystemId.ToString(),iClaimTypeCd,iLineOfBusCd)); 
                    objXMLLossDetail.CauseOfLoss = objReserveCurrent.Context.LocalCache.GetCodeDesc((new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(iLossCode, "LOSS_CODES", "LossCodes on CreatePaymentNode", iPolicySystemId.ToString(),iClaimTypeCd,iLineOfBusCd));
                    //MITS 32000 : aaggarwal29 end
                }

                iPolTransType = (new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(objSplit.TransTypeCode, "TRANS_TYPES", "TransTypes on CreatePaymentNode", iPolicySystemId.ToString(),iClaimTypeCd,iLineOfBusCd); //MITS 32000 : aaggarwal29
               // iPolRsrvCodeId = objReserveCurrent.Context.LocalCache.GetRelatedCodeId(objReserveCurrent.ResStatusCode);
              
                XmlNode PaymentNode = objXMLLossDetail.addFinTrans(iActvityRowId.ToString(), string.Empty, string.Empty, objSplit.AddedByUser, string.Empty, objSplit.Context.LocalCache.GetCodeDesc(iPolTransType), objSplit.Context.LocalCache.GetShortCode(iPolTransType), objSplit.Context.LocalCache.GetCodeDesc(iRsvStatusCode), objSplit.Context.LocalCache.GetShortCode(iRsvStatusCode), objReserveCurrent.BalanceAmount.ToString()); //Payal for RMA-12744(objReserveCurrent.BalanceAmount.ToString())
                //using (DbReader objRdr = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT DTTM_RCD_ADDED FROM ACTIVITY_TRACK WHERE ACTIVITY_ROW_ID = " + iActvityRowId))
                //{
                //    if (objRdr.Read())
                //    {
                //        string sDttmRcdAdded = Riskmaster.Common.Conversion.ConvertObjToStr(objRdr.GetValue(0));
                //        objXMLLossDetail.TransactionDate = Riskmaster.Common.Conversion.GetDate(sDttmRcdAdded.Length >= 8 ? sDttmRcdAdded.Substring(0, 8) : string.Empty);
                //        objXMLLossDetail.TransactionTime = Riskmaster.Common.Conversion.GetTime(sDttmRcdAdded);
                //    }
                //}

                if (objClaimant.ClaimantStatusCode == 0)
                {
                    if (ClaimantHasOpenReserves(objFunds.ClaimId, objFunds.ClaimantEid))
                    {
                        objXMLLossDetail.ClaimantStatus = "Open";
                        objXMLLossDetail.ClaimantStatus_Code = "O";
                    }
                }
                else
                {
                    objXMLLossDetail.ClaimantStatus = objClaimant.Context.LocalCache.GetCodeDesc(objClaimant.Context.LocalCache.GetRelatedCodeId(objClaimant.ClaimantStatusCode));
                    objXMLLossDetail.ClaimantStatus_Code = objClaimant.Context.LocalCache.GetRelatedShortCode(objClaimant.ClaimantStatusCode);

                }


                        string sDttmRcdUpdated = Riskmaster.Common.Conversion.ConvertObjToStr(objFunds.DttmRcdLastUpd);
                        if (!string.IsNullOrEmpty(sDttmRcdUpdated))
                        {
                            objXMLLossDetail.TransactionDate = Riskmaster.Common.Conversion.GetDate(sDttmRcdUpdated.Length >= 8 ? sDttmRcdUpdated.Substring(0, 8) : string.Empty);
                            objXMLLossDetail.TransactionTime = Riskmaster.Common.Conversion.GetTime(sDttmRcdUpdated);
                        }
                        if (string.Equals(Conversion.ConvertObjToStr(RMConfigurationManager.GetDictionarySectionSettings("PolicyUploadSettings", m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)["UploadSameTransandCheckDate"]), "true", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (!string.IsNullOrEmpty(objFunds.DateOfCheck))
                    {
                        objXMLLossDetail.TransactionDate = Riskmaster.Common.Conversion.GetDate(objFunds.DateOfCheck);
                    }

                }
 
                if (objFunds.CollectionFlag)
                {
                    if (objReserveCurrent.Context.LocalCache.GetRelatedCodeId(iRsvTypeCode) == objReserveCurrent.Context.LocalCache.GetCodeId("R", "MASTER_RESERVE"))
                        objXMLLossDetail.PayAmount = objSplit.Amount.ToString();
                    else
                    objXMLLossDetail.PayAmount = (objSplit.Amount * -1).ToString();
                }
                else
                    objXMLLossDetail.PayAmount = objSplit.Amount.ToString();

                    objXMLLossDetail.InvoiceAmount = objSplit.InvoiceAmount.ToString();


                    objXMLLossDetail.InvoiceNumber = objSplit.InvoiceNumber;

                    objXMLLossDetail.InvoiceBy = objSplit.InvoicedBy;
                    objXMLLossDetail.InvoiceDate = Riskmaster.Common.Conversion.GetDate(objSplit.InvoiceDate);

                    objXMLLossDetail.ActivityType_Code = objSplit.Context.LocalCache.GetShortCode(iActivityType);
                    objXMLLossDetail.ActivityType = objSplit.Context.LocalCache.GetCodeDesc(iActivityType);
                    //Payal --Starts for RMA-12744
                    if ((objFunds.CollectionFlag) && (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.INTEGRAL))
                    {
                        bIntegralRecoveryCollection = true;
                    }
                    //Payal --Ends for RMA-12744


                    if (iMoveRsrvHistKey == 0)
                    {
                        if (objFunds.FinalPaymentFlag)
                        {
                            if (bIsVoid && ((objReserveCurrent.Context.LocalCache.GetShortCode(iRsvStatusCode) == "C") || (objReserveCurrent.Context.LocalCache.GetShortCode(iRsvStatusCode) == "RO")))
                        {
                            if (bIntegralRecoveryCollection)
                            {
                                    objXMLLossDetail.ActivityType_Code = "OO"; //Payal for RMA-12744
                                    objXMLLossDetail.ActivityType = "Offset without recovery  reserve adj";
                            }
                            else
                            {
                                objXMLLossDetail.ActivityType_Code = "PO";
                                objXMLLossDetail.ActivityType = "Offset Without Reserve Adjustment";
                            }
                        }
                        //Ankit Start : Work for MITS - 34355
                        else if (bIsVoid)
                        {
                            if (bIntegralRecoveryCollection)
                            {
                                    objXMLLossDetail.ActivityType_Code = "OR"; //Payal for RMA-12744
                                    objXMLLossDetail.ActivityType = "Offset with recovery reserve adj";
                            }
                            else
                            {
                                    objXMLLossDetail.ActivityType_Code = "OA";
                                    objXMLLossDetail.ActivityType = "Offset Reserve Adjustment";
                            }
                        }
                        //Ankit End
                        else
                        {
                            if (bIntegralRecoveryCollection)
                            {
                                    objXMLLossDetail.ActivityType_Code = "FR"; //Payal for RMA-12744
                                    objXMLLossDetail.ActivityType = "Final Recovery";
                            }
                            else
                            {
                                    objXMLLossDetail.ActivityType_Code = "FP";
                                    objXMLLossDetail.ActivityType = "Final Payment";
                           }
                        }
                    }
                    else if (objSplit.IsFirstFinal)
                    {
                            if (bIsVoid && ((objReserveCurrent.Context.LocalCache.GetShortCode(iRsvStatusCode) == "C") || (objReserveCurrent.Context.LocalCache.GetShortCode(iRsvStatusCode) == "RO")))
                        {
                            objXMLLossDetail.ActivityType_Code = "PO";
                            objXMLLossDetail.ActivityType = "Offset Without Reserve Adjustment";
                        }
                        //Ankit Start : Work for MITS - 34355
                        else if (bIsVoid)
                        {
                            objXMLLossDetail.ActivityType_Code = "OA";
                            objXMLLossDetail.ActivityType = "Offset Reserve Adjustment";
                        }
                        //Ankit End
                        else
                        {
                            objXMLLossDetail.ActivityType_Code = "FF";
                            objXMLLossDetail.ActivityType = "First Final";
                        }
                    }
                    else if (objFunds.IsSupplementalPay)
                    {
                            if (bIsVoid && ((objReserveCurrent.Context.LocalCache.GetShortCode(iRsvStatusCode) == "C")))
                        {
                            objXMLLossDetail.ActivityType_Code = "PO";
                            objXMLLossDetail.ActivityType = "Offset Without Reserve Adjustment";
                        }
                        //Ankit Start : Work for MITS - 34355
                        else if (bIsVoid)
                        {
                            objXMLLossDetail.ActivityType_Code = "OA";
                            objXMLLossDetail.ActivityType = "Offset Reserve Adjustment";
                        }
                        //Ankit End
                        else
                        {
                            objXMLLossDetail.ActivityType_Code = "SP";
                            objXMLLossDetail.ActivityType = "Supplemental Payment";
                        }
                    }
                   else if (objFunds.VoidFlag && ((objReserveCurrent.Context.LocalCache.GetShortCode(iRsvStatusCode) == "C")))
                    {
                        if (bIntegralRecoveryCollection)
                        {
                                objXMLLossDetail.ActivityType_Code = "OO"; //Payal for RMA-12744
                                objXMLLossDetail.ActivityType = "Offset without recovery  reserve adj";
                        }
                        else
                        {
                                objXMLLossDetail.ActivityType_Code = "PO";
                                objXMLLossDetail.ActivityType = "Offset Without Reserve Adjustment";
                        }
                    }
                    }
                    //Payal : RMA-7484 Starts
                     if (sVoucherID != objSplit.TransId.ToString())
                         objSplitList.Add(objSplit.SplitRowId);
                    //Payal : RMA-7484 Ends
                        objXMLLossDetail.VoucherReference = objSplit.TransId.ToString();
                    //Payal : RMA-7484 Starts
                        sVoucherID = objXMLLossDetail.VoucherReference.ToString();
                    //  objSplitList.Add(objSplit.SplitRowId);
                    //Payal : RMA-7484 Ends
                  
                }
            }
            finally
            {
                 if(objSplit != null)
                     objSplit.Dispose();
                 if(objFunds != null)
                     objFunds.Dispose();
                 if(objClaimant != null)
                     objClaimant.Dispose();

                 if (objReserveCurrent != null)
                     objReserveCurrent.Dispose();
                 if (objPolicy != null)
                     objPolicy.Dispose();
            }
            return true;
        }
     

        private static void CreateVoucherNode(int iSplitId, XMLVoucher objXmlVoucher)
        {
            FundsTransSplit objSplit = null;
             Funds objFunds = null;
            //Added by Nikhil on 08/07/14, Send check total instead or funds amount based on utility setting
             string sAmount = string.Empty;
            //Added by vkumar258 RMA-7484 Starts
             string sPrintTime = string.Empty;
             string sVoidTime = string.Empty;
             int iVoidFlag = 0;
             string sPaymentStatus = string.Empty;
             string sPaymentStatusCode = string.Empty;
             Claim objClaim = null;
             IntegralSystemInterface objIntgSystemInterface = new IntegralSystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);
             //vkumar258 RMA-7484 End
             //Payal RMA: 7484 Starts
             string sPaymentReason = string.Empty;
             string sPaymentReasonCode = string.Empty;
             LocalCache objCache = null;
             //Payal RMA: 7484 Ends
             string sBankName = null; //Payal for RMA-12744
             try
             {
                 objCache = new LocalCache(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId); //Payal RMA: 7484
                 objSplit = (FundsTransSplit)m_objFactory.GetDataModelObject("FundsTransSplit", false);
                 objSplit.MoveTo(iSplitId);

                 objFunds = (Funds)m_objFactory.GetDataModelObject("Funds", false);
                 objFunds.MoveTo(objSplit.TransId);
                 //Added by Sravan on 4/29/2015, send Payment status details
                 objClaim = (Claim)m_objFactory.GetDataModelObject("Claim", false);
                 objClaim.MoveTo(objFunds.ClaimId);
                 int iPaymentStatusCodeId = objIntgSystemInterface.GetPSMappedCodeIDFromRMXCodeId(objFunds.StatusCode, "CHECK_STATUS", "PaymentStatusCode on CreateVoucherNode", iPolicySystemId.ToString());
                 objFunds.Context.LocalCache.GetCodeInfo(iPaymentStatusCodeId, ref sPaymentStatus, ref sPaymentStatusCode);

                 string sNotes = HttpUtility.HtmlEncode(objFunds.Notes);
                 string sVoucherTime = objFunds.DttmRcdAdded;
                 sVoucherTime = sVoucherTime.Substring(8);

                 if (objFunds.StatusCode == m_objFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS"))
                 {
                     sPrintTime = objFunds.DttmRcdAdded;
                     sPrintTime = sPrintTime.Substring(8);
                 }

                 if (objFunds.VoidFlag)
                 {
                     sVoidTime = objFunds.DttmRcdAdded;
                     sVoidTime = sVoidTime.Substring(8);
                 }
                //End - Added by sravan
                 Account objAccount = (Account)m_objFactory.GetDataModelObject("Account", false);
                 objAccount.MoveTo(objFunds.AccountId);
                 //Start -  Added by Nikhil on 08/07/14, Send check total instead or funds amount based on utility setting
                 if (objFunds.Context.InternalSettings.SysSettings.UploadCheckTotal && objFunds.PaymentFlag)
                 {
                     sAmount = objFunds.CheckTotal.ToString();
                 }
                 else
                 { 
                      sAmount = objFunds.Amount.ToString();
                 }
                 //Payal: RMA-7484 Starts
                 sPaymentReason = objCache.GetShortCode(objFunds.TransactionReason);
                 sPaymentReasonCode = objCache.GetCodeDesc(objFunds.TransactionReason);

                 if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.INTEGRAL)
                 {
                     objXmlVoucher.Create(objFunds.TransId.ToString(), enVCHType.enVCHCheck, "Yes", objFunds.TransNumber.ToString(), objFunds.CtlNumber, objAccount.AccountName, objAccount.BankCode, sAmount, Riskmaster.Common.Conversion.GetDate(objFunds.DateOfCheck), sPaymentStatusCode, sPaymentStatus, Riskmaster.Common.Conversion.GetDate(objFunds.DateOfCheck), string.Empty, string.Empty, objFunds.BatchNumber.ToString(), string.Empty, sNotes, sVoucherTime, sPrintTime, sVoidTime, sPaymentReasonCode, sPaymentReason, string.Empty, string.Empty); ///Added by Sravan for RMA-17466
                 }
                 else
                 {
                     //objXmlVoucher.Create(objFunds.TransId.ToString(), enVCHType.enVCHCheck, "Yes", objFunds.TransNumber.ToString(), objFunds.CtlNumber, objAccount.AccountName,objAccount.BankCode, objFunds.Amount.ToString(), Riskmaster.Common.Conversion.GetDate(objFunds.DateOfCheck), string.Empty, string.Empty, Riskmaster.Common.Conversion.GetDate(objFunds.DateOfCheck), string.Empty, string.Empty, objFunds.BatchNumber.ToString(), string.Empty);
                     objXmlVoucher.Create(objFunds.TransId.ToString(), enVCHType.enVCHCheck, "Yes", objFunds.TransNumber.ToString(), objFunds.CtlNumber, objAccount.AccountName, objAccount.BankCode, sAmount, Riskmaster.Common.Conversion.GetDate(objFunds.DateOfCheck), string.Empty, string.Empty, Riskmaster.Common.Conversion.GetDate(objFunds.DateOfCheck), string.Empty, string.Empty, objFunds.BatchNumber.ToString(), string.Empty, sNotes, sVoucherTime, sPrintTime, sVoidTime, sPaymentReasonCode, sPaymentReason, string.Empty, string.Empty); ///Payal for RMA-12744
                     //end -  Added by Nikhil on 08/07/14, Send check total instead or funds amount based on utility setting
                 }
                 objXmlVoucher.InvoiceAmount = objSplit.InvoiceAmount.ToString();
                 objXmlVoucher.InvoiceDate = Riskmaster.Common.Conversion.GetDate(objSplit.InvoiceDate);
                 objXmlVoucher.InvoicedBy = objSplit.InvoicedBy;
                objXmlVoucher.InvoiceNumber = objSplit.InvoiceNumber;
                 //Payal --Starts for RMA-12744
                if ((objFunds.CollectionFlag) && (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.INTEGRAL))
                {
                    objXmlVoucher.ChequeType = "L";
                    objXmlVoucher.OriginatingBankName = "Axis Bank";  //Bank Key
                }

                //Payal --Ends for RMA-12744

                try
                {
                    string sSQLTemp = "SELECT PAYEE_ROW_ID,PAYEE_EID FROM FUNDS_X_PAYEE WHERE FUNDS_TRANS_ID=" + objFunds.TransId + " ORDER BY PAYEE_ROW_ID ";

                    using (DbReader objRdr = DbFactory.GetDbReader(objSplit.Context.DbConn.ConnectionString, sSQLTemp))
                    {
                        while (objRdr.Read())
                        {
                            //Payal --Starts for RMA-12744
                            if ((objFunds.CollectionFlag) && (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.INTEGRAL))
                                objXmlVoucher.PayeeCode = "CF";
                            //Payal --Ends for RMA-12744
                            else
                            objXmlVoucher.PayeeCode = "PAYEE";
                            objXmlVoucher.addPayee(Conversion.ConvertObjToStr(objRdr.GetValue(0)), Conversion.ConvertObjToStr(objRdr.GetValue(1)));


                            if (!objEntityList.ContainsKey(Conversion.ConvertObjToStr(objRdr.GetValue(1))))
                                objEntityList.Add(Conversion.ConvertObjToStr(objRdr.GetValue(1)), "true," + 0 + "," + string.Empty);
                        }

                    }
                }

                catch (Exception e)
                {
                }
                 //Payal: RMA-7484 Starts
                if (objFunds.VoidFlag)
                   objXmlVoucher.VoidDate = Riskmaster.Common.Conversion.GetDate(objFunds.VoidDate);
                if ((objFunds.VoidFlag) || (objFunds.StatusCode == m_objFactory.Context.LocalCache.GetCodeId("R", "CHECK_STATUS")))
                {
                   objXmlVoucher.PrintDate = "";
                 }
                //Payal: RMA-7484 Ends
                if (!objFundsList.Contains(objFunds.TransId))
                     objFundsList.Add(objFunds.TransId);
                 objXmlVoucher.PayeeCode = "MAILTO";
                 objXmlVoucher.addPayee(objFunds.TransId.ToString(), "MT"+objFunds.MailToEid.ToString());


                 
                 if(objFunds.ManualCheckFlag)
                     objXmlVoucher.ManualInd = "YES";
                    else
                     objXmlVoucher.ManualInd = "NO";
                 objXmlVoucher.SERDTEFROM = Riskmaster.Common.Conversion.GetDate(objSplit.FromDate);
                 objXmlVoucher.SERDTETO = Riskmaster.Common.Conversion.GetDate(objSplit.ToDate);
                 
             }
             finally
             {
                 if (objSplit != null)
                     objSplit.Dispose();
                 if (objFunds != null)
                     objFunds.Dispose();
                 if (objClaim != null)
                     objClaim.Dispose();
                 if (objIntgSystemInterface != null)
                     objIntgSystemInterface = null;
                 if (objCache != null)
                     objCache = null;
             }

        }
        private static void GetEntityName(int iEntityID, ref string sLastName, ref string sFirstName)
        {
            try
            {
                using (DbReader objRdr = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT FIRST_NAME,LAST_NAME FROM ENTITY WHERE ENTITY_ID=" + iEntityID))
                {
                    if (objRdr.Read())
                    {
                        sLastName =Conversion.ConvertObjToStr(objRdr.GetValue("LAST_NAME"));
                        sFirstName =Conversion.ConvertObjToStr(objRdr.GetValue("FIRST_NAME"));
                    }
                }
 
                    
            }
            catch (Exception e)
            {
            }

        }

        private static bool HasBlankAddressDetails(int iEntityId,int iPolicyId,string sUploadType,string sClaimNumber,int iRcRowId)
        {
            Entity objEntity = null;
            EntityXAddresses objEntityXAddresses = null;
            int iEntityAddrId=0;
            bool bBlankAddrDetails = false;
            string sLastName=string.Empty;
            string sFirstName=string.Empty;
            string sEntityName = string.Empty;
            try
            {


                if (objNoAddrInEntity.Contains(sUploadType + "|" + iRcRowId) || objNoAddrInEntity.Contains(sUploadType + "|" + sClaimNumber))//bsharma33 RMA-12757 
                {
                    bBlankAddrDetails = true;
                    return bBlankAddrDetails;
                }

                if (objRcRowIds.Contains(iRcRowId) && iRcRowId != 0) //bsharma33 RMA-12757
                {
                    bBlankAddrDetails = true;
                }
                else
                {

                    using (DbReader objRdr = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT * FROM POLICY_X_INSURED WHERE POLICY_ID=" + iPolicyId + " AND INSURED_EID="+iEntityId))
                    {
                        if (objRdr.Read())
                        {
                            //If Address Id is null then client is on rmA version <  14.2
                            // Address Id is not null then client is on rmA version >=  14.2
                            var fieldNames = Enumerable.Range(0, objRdr.FieldCount).Select(i => objRdr.GetName(i)).ToArray();
                            if (fieldNames.Contains("ADDRESS_ID"))
                            {
                                //}
                                //if (objRdr["ADDRESS_ID"] != null)
                                //{
                                iEntityAddrId = GetEntityAddrSeqNum(iPolicyId, iEntityId);
                                if (iEntityAddrId > 0)
                                {
                                    objEntityXAddresses = (EntityXAddresses)m_objFactory.GetDataModelObject("EntityXAddresses", false);
                                    objEntityXAddresses.MoveTo(iEntityAddrId);

                                    // npadhy - Implementing the Review comments from Amitosh
									//RMA-8753 nshah28(Added by ashish)
                                    if ((objEntityXAddresses.Address.State == 0) || string.IsNullOrEmpty(objEntityXAddresses.Address.State.ToString()) ||
                                        string.IsNullOrEmpty(objEntityXAddresses.Address.Addr1 + objEntityXAddresses.Address.Addr2 + objEntityXAddresses.Address.Addr3 + objEntityXAddresses.Address.Addr4) || string.IsNullOrEmpty(objEntityXAddresses.Address.ZipCode) || string.IsNullOrEmpty(objEntityXAddresses.Address.City))
                                    {
                                        bBlankAddrDetails = true;
                                    }
                                }
                                else
                                {

                                    objEntity = (Entity)m_objFactory.GetDataModelObject("Entity", false);
                                    objEntity.MoveTo(iEntityId);
                                    if (objEntity.EntityXAddressesList.Count > 0)
                                    {
                                        foreach (EntityXAddresses objEntityAddr in objEntity.EntityXAddressesList)
                                        {
										//RMA-8753 nshah28(Added by ashish)
                                            if ((objEntityAddr.Address.State == 0) || string.IsNullOrEmpty(objEntityAddr.Address.State.ToString()) ||
                                         string.IsNullOrEmpty(objEntityAddr.Address.Addr1 + objEntityAddr.Address.Addr2 + objEntityAddr.Address.Addr3 + objEntityAddr.Address.Addr4) || string.IsNullOrEmpty(objEntityAddr.Address.ZipCode) || string.IsNullOrEmpty(objEntityAddr.Address.City))
                                            {
                                                bBlankAddrDetails = true;

                                            }
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if ((objEntity.StateId == 0) || string.IsNullOrEmpty(objEntity.StateId.ToString()) ||
                                   string.IsNullOrEmpty(objEntity.Addr1 + objEntity.Addr2 + objEntity.Addr3 + objEntity.Addr4) || string.IsNullOrEmpty(objEntity.ZipCode) || string.IsNullOrEmpty(objEntity.City))
                                        {

                                            bBlankAddrDetails = true;
                                        }
                                    }

                                }
                            }
                            else
                            {

                                objEntity = (Entity)m_objFactory.GetDataModelObject("Entity", false);
                                objEntity.MoveTo(iEntityId);

                                if ((objEntity.StateId == 0) || string.IsNullOrEmpty(objEntity.StateId.ToString()) ||
                                    string.IsNullOrEmpty(objEntity.Addr1 + objEntity.Addr2 + objEntity.Addr3 + objEntity.Addr4) || string.IsNullOrEmpty(objEntity.ZipCode) || string.IsNullOrEmpty(objEntity.City))
                                {

                                    bBlankAddrDetails = true;
                                }

                            }

                        }
                        else
                        {
                            using (DbReader objRdr1 = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT * FROM POLICY_X_ENTITY WHERE POLICY_ID=" + iPolicyId + " AND ENTITY_ID=" + iEntityId))
                            {
                                if (objRdr1.Read())
                                {
                                    //If Address Id is null then client is on rmA version <  14.2
                                    // Address Id is not null then client is on rmA version >=  14.2
                                    var fieldNames = Enumerable.Range(0, objRdr.FieldCount).Select(i => objRdr.GetName(i)).ToArray();
                                    if (fieldNames.Contains("ADDRESS_ID"))
                                    {
                                        //}
                                        //if (objRdr["ADDRESS_ID"] != null)
                                        //{
                                        iEntityAddrId = GetEntityAddrSeqNum(iPolicyId, iEntityId);
                                        if (iEntityAddrId > 0)
                                        {
                                            objEntityXAddresses = (EntityXAddresses)m_objFactory.GetDataModelObject("EntityXAddresses", false);
                                            objEntityXAddresses.MoveTo(iEntityAddrId);
											//RMA-8753 nshah28(Added by ashish)
                                            if ((objEntityXAddresses.Address.State == 0) || string.IsNullOrEmpty(objEntityXAddresses.Address.State.ToString()) ||
                                                string.IsNullOrEmpty(objEntityXAddresses.Address.Addr1 + objEntityXAddresses.Address.Addr2 + objEntityXAddresses.Address.Addr3 + objEntityXAddresses.Address.Addr4) || string.IsNullOrEmpty(objEntityXAddresses.Address.ZipCode) || string.IsNullOrEmpty(objEntityXAddresses.Address.City))
                                            {
                                                bBlankAddrDetails = true;
                                            }
                                        }
                                        else
                                        {

                                            objEntity = (Entity)m_objFactory.GetDataModelObject("Entity", false);
                                            objEntity.MoveTo(iEntityId);
                                            if (objEntity.EntityXAddressesList.Count > 0)
                                            {
                                                foreach (EntityXAddresses objEntityAddr in objEntity.EntityXAddressesList)
                                                {
													//RMA-8753 nshah28(Added by ashish)
                                                    if ((objEntityAddr.Address.State == 0) || string.IsNullOrEmpty(objEntityAddr.Address.State.ToString()) ||
                                                 string.IsNullOrEmpty(objEntityAddr.Address.Addr1 + objEntityAddr.Address.Addr2 + objEntityAddr.Address.Addr3 + objEntityAddr.Address.Addr4) || string.IsNullOrEmpty(objEntityAddr.Address.ZipCode) || string.IsNullOrEmpty(objEntityAddr.Address.City))
                                                    {
                                                        bBlankAddrDetails = true;

                                                    }
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if ((objEntity.StateId == 0) || string.IsNullOrEmpty(objEntity.StateId.ToString()) ||
                                           string.IsNullOrEmpty(objEntity.Addr1 + objEntity.Addr2 + objEntity.Addr3 + objEntity.Addr4) || string.IsNullOrEmpty(objEntity.ZipCode) || string.IsNullOrEmpty(objEntity.City))
                                                {

                                                    bBlankAddrDetails = true;
                                                }
                                            }

                                        }
                                    }
                                    else
                                    {

                                        objEntity = (Entity)m_objFactory.GetDataModelObject("Entity", false);
                                        objEntity.MoveTo(iEntityId);

                                        if ((objEntity.StateId == 0) || string.IsNullOrEmpty(objEntity.StateId.ToString()) ||
                                            string.IsNullOrEmpty(objEntity.Addr1 + objEntity.Addr2 + objEntity.Addr3 + objEntity.Addr4) || string.IsNullOrEmpty(objEntity.ZipCode) || string.IsNullOrEmpty(objEntity.City))
                                        {

                                            bBlankAddrDetails = true;
                                        }

                                    }
                                }
                                else
                                {
                                    // rma Entity
                                    //if (iEntityAddrId > 0)
                                    //{
                                    //    objEntityXAddresses = (EntityXAddresses)m_objFactory.GetDataModelObject("EntityXAddresses", false);
                                    //    objEntityXAddresses.MoveTo(iEntityAddrId);

                                    //    if ((objEntityXAddresses.State == 0) || string.IsNullOrEmpty(objEntityXAddresses.State.ToString()) ||
                                    //        string.IsNullOrEmpty(objEntityXAddresses.Addr1) || string.IsNullOrEmpty(objEntityXAddresses.ZipCode) || string.IsNullOrEmpty(objEntityXAddresses.City))
                                    //    {
                                    //        bBlankAddrDetails = true;
                                    //    }
                                    //}
                                    //else
                                    //{

                                    objEntity = (Entity)m_objFactory.GetDataModelObject("Entity", false);
                                    objEntity.MoveTo(iEntityId);

                                    if (objEntity.EntityXAddressesList.Count > 0)
                                    {
                                        foreach (EntityXAddresses objEntityAddr in objEntity.EntityXAddressesList)
                                        {
											//RMA-8753 nshah28(Added by ashish)
                                            if ((objEntityAddr.Address.State == 0) || string.IsNullOrEmpty(objEntityAddr.Address.State.ToString()) ||
                                         string.IsNullOrEmpty(objEntityAddr.Address.Addr1 + objEntityAddr.Address.Addr2 + objEntityAddr.Address.Addr3 + objEntityAddr.Address.Addr4) || string.IsNullOrEmpty(objEntityAddr.Address.ZipCode) || string.IsNullOrEmpty(objEntityAddr.Address.City))
                                            {
                                                bBlankAddrDetails = true;

                                            }
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if ((objEntity.StateId == 0) || string.IsNullOrEmpty(objEntity.StateId.ToString()) ||
                                        string.IsNullOrEmpty(objEntity.Addr1 + objEntity.Addr2 + objEntity.Addr3 + objEntity.Addr4) || string.IsNullOrEmpty(objEntity.ZipCode) || string.IsNullOrEmpty(objEntity.City))
                                        {

                                            bBlankAddrDetails = true;
                                        }
                                    }

                                    //}
                                }
                            }
                        }

                    }


                }

                    if (bBlankAddrDetails)
                    {
                        if (!objRcRowIds.Contains(iRcRowId) && iRcRowId != 0)//bsharma33 RMA-12757
                        {
                            objRcRowIds.Add(iRcRowId);
                        }

                        if (objNoAddrInEntity.Contains(sUploadType + "|" + iRcRowId) || objNoAddrInEntity.Contains(sUploadType + "|" + sClaimNumber))//bsharma33 RMA-12757
                        {
                            return bBlankAddrDetails;
                        }
                        else
                        {

                            if (sUploadType == "EMPLOYEE")
                                objNoAddrInEntity.Add(sUploadType + "|" + sClaimNumber);
                            else
                            objNoAddrInEntity.Add(sUploadType + "|" + iRcRowId);

                            GetEntityName(iEntityId, ref  sLastName, ref  sFirstName);

                            if (string.IsNullOrEmpty(sFirstName))
                            {
                                sEntityName = sLastName;
                            }
                            else
                            {
                                sEntityName = sFirstName + " " + sLastName;
                            }

                            switch (sUploadType)
                            {
                                case "PAYMENT":

                                    Console.WriteLine("0 ^*^*^ Payment can not be uploaded for claim: " + sClaimNumber + " since claimant: " + sEntityName + " has missing address details");
                                    sbLogFile.Append(" Payment can not be uploaded for claim: " + sClaimNumber + " since claimant: " + sEntityName + " has missing address details" + Environment.NewLine);
                                    break;
                                case "RESERVE":
                                    Console.WriteLine("0 ^*^*^ Reserve can not be uploaded for claim: " + sClaimNumber + " since claimant: " + sEntityName + " has missing address details");
                                    sbLogFile.Append(" Reserve can not be uploaded for claim: " + sClaimNumber + " since claimant: " + sEntityName + " has missing address details" + Environment.NewLine);
                                    break;
                                case "EMPLOYEE":
                                    Console.WriteLine("0 ^*^*^ Claim can not be uploaded for claim number : " + sClaimNumber + " since employee: " + sEntityName + " has missing address details");
                                    sbLogFile.Append(" Claim can not be uploaded for claim number : " + sClaimNumber + " since employee: " + sEntityName + " has missing address details" + Environment.NewLine);
                                    break;
                            }
                        }
                        // return bBlankAddrDetails;
                    }
                }
            
            catch (Exception e)
            {
                return bBlankAddrDetails;
            }
            finally
            {
                if (objEntity != null)
                {
                    objEntity.Dispose();
                    objEntity = null;
                }
                if (objEntityXAddresses != null)
                {
                    objEntityXAddresses.Dispose();
                    objEntityXAddresses = null;
                }
                 
            }

            return bBlankAddrDetails;
        }
        private static bool CreateReserveNode(int iRsrvRowId, XMLLossDetail objXMLLossDetail, int iActvityRowId, int iActivityType, double iReserveAmount, int iReserveStatus, double iChangeAmount, int iClaimTypeCd, int iLineOfBusCd,string sClaimNumber,int iMoveHistTableKey) //MITS 32000 : aaggarwal29
        {
            ReserveCurrent objReserveCurrent = null;
            Claimant objClaimant=null;
              ReserveHistory objReserveHistory = null;
              int iPolRsrvCode = 0;
              int iUnitId = 0;
              int iLocation = 0;
              int iEffDate = 0;
              int iPolicyId = 0;
              string sUnitType = string.Empty;
              Policy objPolicy = null;
            string sResStatusCode=string.Empty;
            string sResStatus=string.Empty;
            string sTransNo = string.Empty;
            string sCvgSeqNo = string.Empty;
            CvgXLoss objCvgXLoss = null;
            int iClaimantEid = 0;
            int iCvgLossRowId = 0;
            int iRsvTypeCode = 0;
            int iPolCovSeq = 0;
            int iLossCode = 0;
            int iPolCvgRowId = 0;
            RsvMoveHistKey objRsvMoveHist;
              try
              {
                  objReserveCurrent = (ReserveCurrent)m_objFactory.GetDataModelObject("ReserveCurrent", false);
                  objReserveCurrent.MoveTo(iRsrvRowId);
                 
                  if (iMoveHistTableKey > 0)
                  {
                      objRsvMoveHist = GetRsvMoveHistDetails(iRsrvRowId, iMoveHistTableKey);
                      iClaimantEid = objRsvMoveHist.ClaimantEid;
                      iCvgLossRowId = objRsvMoveHist.CvgLossRowId;
                      iPolCovSeq = objRsvMoveHist.PolCovSeq;
                      iLossCode = objRsvMoveHist.LossCode;
                      iRsvTypeCode = objRsvMoveHist.RsvTypeCode;
                      iPolCvgRowId = objRsvMoveHist.PolCvgRowId;

                      GetCoverageDetailsFromPolxCvg(iPolCvgRowId, ref iUnitId, ref iLocation, ref iEffDate, ref iPolicyId, ref sUnitType);

                      
                  }
                  else
                  {
                      GetCoverageDetailsFromCvgLoss(objReserveCurrent.CoverageLossId, ref iUnitId, ref iLocation, ref iEffDate, ref iPolicyId, ref sUnitType);
                      objCvgXLoss = (CvgXLoss)m_objFactory.GetDataModelObject("CvgXLoss", false);
                      objCvgXLoss.MoveTo(objReserveCurrent.CoverageLossId);


                      iClaimantEid = objReserveCurrent.ClaimantEid;
                      iCvgLossRowId = objReserveCurrent.CoverageLossId;
                      iPolCovSeq = objReserveCurrent.PolicyCvgSeqNo;
                      iLossCode = objCvgXLoss.LossCode;
                      iRsvTypeCode = objReserveCurrent.ReserveTypeCode;
                      iPolCvgRowId = objCvgXLoss.PolCvgRowId;
                  }

                  //objXMLLossDetail.ClaimantEntityID = iClaimantEid.ToString();

                  objClaimant = (Claimant)m_objFactory.GetDataModelObject("Claimant", false);
                  objClaimant.MoveTo(GetClaimantRowdId(iClaimantEid, objReserveCurrent.ClaimId));


               


                  
                if (PolicySystemTypeIndicator == Riskmaster.Common.Constants.POLICY_SYSTEM_TYPE.POINT && HasBlankAddressDetails(iClaimantEid, iPolicyId, "RESERVE", sClaimNumber, iRsrvRowId))
                  {
                      return false;
                  }
                  objPolicy = (Policy)m_objFactory.GetDataModelObject("Policy", false);
                  objPolicy.MoveTo(iPolicyId);
                  iTransPolicyId = iPolicyId;
                  iTransClaimantId = objClaimant.ClaimantRowId;
                  //Commented by Payal --Starts RMA:7484
                 /*
                  if (objReserveCurrent.AssignAdjusterEid > 0)
                  {
                      objXMLLossDetail.ReserveAdjusterExaminerCd = GetAdjusterExaminerCd(objReserveCurrent.AssignAdjusterEid, objReserveCurrent.ClaimId);
                  }
                  *  * */
                  if (iPolicySystemId == objPolicy.PolicySystemId)
                  {
                   //   iPolRsrvCode = iReserveStatus;
                     

                      CreateLossDetailsNode(objXMLLossDetail, iClaimantEid, objClaimant.ClaimantNumber, iRsvTypeCode, iPolCvgRowId, iRsrvRowId, objReserveCurrent.ClaimId,iPolCovSeq, iClaimTypeCd, iLineOfBusCd); //MITS 32000 : aaggarwal29
                      // Payal --Starts RMA:7484 
                      if (objReserveCurrent.AssignAdjusterEid > 0)
                      {
                          objXMLLossDetail.ReserveAdjusterExaminerCd = GetAdjusterExaminerCd(objReserveCurrent.AssignAdjusterEid, objReserveCurrent.ClaimId);
                      }
                      // Payal --Ends RMA:7484 
                      objXMLLossDetail.ClaimantEntityID = iClaimantEid.ToString();
                      GetCoverageSeqNo(iPolCvgRowId, ref sCvgSeqNo, ref sTransNo);
                      objXMLLossDetail.PolicyCoverageLegacyKey = GetUnitNumber(iUnitId, sUnitType) + "," + sCvgSeqNo+ "," + sTransNo;
                   
                      objXMLLossDetail.addFinTrans(iActvityRowId.ToString(), string.Empty, string.Empty, objReserveCurrent.AddedByUser, string.Empty, string.Empty, string.Empty, objReserveCurrent.Context.LocalCache.GetCodeDesc(iReserveStatus), objReserveCurrent.Context.LocalCache.GetShortCode(iReserveStatus), string.Empty); //Payal for RMA-12744

                      //using (DbReader objRdr = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT DTTM_RCD_ADDED FROM ACTIVITY_TRACK WHERE ACTIVITY_ROW_ID = " + iActvityRowId))
                      //{
                      //    if (objRdr.Read())
                      //    {
                      //        string sDttmRcdAdded = Riskmaster.Common.Conversion.ConvertObjToStr(objRdr.GetValue(0));
                      //        objXMLLossDetail.TransactionDate = Riskmaster.Common.Conversion.GetDate(sDttmRcdAdded.Length >= 8 ? sDttmRcdAdded.Substring(0, 8) : string.Empty);
                      //        objXMLLossDetail.TransactionTime = Riskmaster.Common.Conversion.GetTime(sDttmRcdAdded);
                      //    }
                      //}

                      string sDttmRcdUpdated = Riskmaster.Common.Conversion.ConvertObjToStr(objReserveCurrent.DttmRcdLastUpd);
                      if (!string.IsNullOrEmpty(sDttmRcdUpdated))
                      {
                          objXMLLossDetail.TransactionDate = Riskmaster.Common.Conversion.GetDate(sDttmRcdUpdated.Length >= 8 ? sDttmRcdUpdated.Substring(0, 8) : string.Empty);
                          objXMLLossDetail.TransactionTime = Riskmaster.Common.Conversion.GetTime(sDttmRcdUpdated);
                      }

                      if (objClaimant.ClaimantStatusCode == 0)
                      {
                          if (ClaimantHasOpenReserves(objReserveCurrent.ClaimId, objReserveCurrent.ClaimantEid))
                          {
                              objXMLLossDetail.ClaimantStatus = "Open";
                              objXMLLossDetail.ClaimantStatus_Code = "O";
                          }
                      }
                      else
                      {
                          objXMLLossDetail.ClaimantStatus = objClaimant.Context.LocalCache.GetCodeDesc(objClaimant.Context.LocalCache.GetRelatedCodeId(objClaimant.ClaimantStatusCode));
                          objXMLLossDetail.ClaimantStatus_Code = objClaimant.Context.LocalCache.GetRelatedShortCode(objClaimant.ClaimantStatusCode);

                      }    

                    objXMLLossDetail.ReserveAmount = objReserveCurrent.ReserveAmount.ToString();
                      //objReserveCurrent.Context.LocalCache.GetCodeInfo(objReserveCurrent.Context.LocalCache.GetRelatedCodeId(iReserveStatus), ref sResStatusCode, ref sResStatus);
                       

                      objXMLLossDetail.ReserveStatus_Code = objReserveCurrent.Context.LocalCache.GetShortCode(iReserveStatus);
                      objXMLLossDetail.ReserveStatus = objReserveCurrent.Context.LocalCache.GetCodeDesc(iReserveStatus);
                      objXMLLossDetail.ReserveBalance = iReserveAmount.ToString();
                      objXMLLossDetail.ReserveChange = iChangeAmount.ToString();

                      //objCvgXLoss = (CvgXLoss)m_objFactory.GetDataModelObject("CvgXLoss", false);
                      //objCvgXLoss.MoveTo(objReserveCurrent.CoverageLossId);

                      if (objXMLLossDetail.CoverageInsLine_Code == "WC")
                      {
                          //MITS 32000 : aaggarwal29 start
                          objXMLLossDetail.LossDisability_Code = objReserveCurrent.Context.LocalCache.GetShortCode((new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(iLossCode, "DISABILITY_TYPE", "DisabilityTypeCode on CreatePaymentNode", iPolicySystemId.ToString(),iClaimTypeCd,iLineOfBusCd));
                          objXMLLossDetail.LossDisability = objReserveCurrent.Context.LocalCache.GetCodeDesc((new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(iLossCode, "DISABILITY_TYPE", "DisabilityTypeCode on CreatePaymentNode", iPolicySystemId.ToString(),iClaimTypeCd,iLineOfBusCd));
                          //MITS 32000 : aaggarwal29 end
                      }
                      else
                      {
                          //MITS 32000 : aaggarwal29 start
                          objXMLLossDetail.CauseOfLoss_Code = objReserveCurrent.Context.LocalCache.GetShortCode((new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(iLossCode, "LOSS_CODES", "LossCodes on CreatePaymentNode", iPolicySystemId.ToString(),iClaimTypeCd,iLineOfBusCd));
                          objXMLLossDetail.CauseOfLoss = objReserveCurrent.Context.LocalCache.GetCodeDesc((new PolicySystemInterface(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId)).GetPSMappedCode(iLossCode, "LOSS_CODES", "LossCodes on CreatePaymentNode", iPolicySystemId.ToString(),iClaimTypeCd,iLineOfBusCd));
                          //MITS 32000 : aaggarwal29 end
                      }

                      objXMLLossDetail.ActivityType_Code = objReserveCurrent.Context.LocalCache.GetShortCode(iActivityType);
                      objXMLLossDetail.ActivityType = objReserveCurrent.Context.LocalCache.GetCodeDesc(iActivityType);
                  }
              }
              finally
              {
                  if (objClaimant != null)
                      objClaimant.Dispose();
                  if (objReserveHistory != null)
                      objReserveHistory.Dispose();
                  if (objReserveCurrent != null)
                      objReserveCurrent.Dispose();
              }
            return true;
        }

        private static int GetReserveCurrentId(int iPolXCvgId, int iClaimantId,int iClaimId,int iReserveType)
        {
            int iRsrvId = 0;

            try
            {
                using (DbReader objReader = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT RC_ROW_ID FROM RESERVE_CURRENT WHERE CLAIM_ID=" + iClaimId + " AND CLAIMANT_EID=" + iClaimantId + " AND RESERVE_TYPE_CODE =" + iReserveType + " AND POLCVG_ROW_ID=" + iPolXCvgId))
                {
                    if (objReader.Read())
                    {
                        iRsrvId = Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                    }
                }

            }
            catch(Exception e)
            {
                Console.WriteLine("0 ^*^*^ Reserve not found");
                sbLogFile.Append(" reserve not found " + Environment.NewLine);
                throw new Exception("Reserve not found");
            }

            return iRsrvId;
        }

        private static int GetReserveHistoryId(int iCvgLossId, int iClaimantId, int iClaimId, int iReserveType)
        {
            int iRsrvId = 0;

            try
            {
                using (DbReader objReader = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT MAX(RSV_ROW_ID) FROM RESERVE_HISTORY WHERE CLAIM_ID =" + iClaimId + " AND CLAIMANT_EID=" + iClaimantId + " AND RESERVE_TYPE_CODE =" + iReserveType + " AND POLCVG_LOSS_ROW_ID=" + iCvgLossId))
                {
                    if (objReader.Read())
                    {
                        iRsrvId = Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                    }
                }

            }
            catch(Exception e)
            {
                Console.WriteLine("0 ^*^*^ Reserve history not found");
                sbLogFile.Append(" reserve history not found " + Environment.NewLine);
                throw new Exception("Reserve history not found");
            }

            return iRsrvId;
        }
        private static int GetClaimantRowdId(int iClaimantEID, int iClaimId)
        {
            int iRsrvId = 0;

            try
            {
                using (DbReader objReader = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT CLAIMANT_ROW_ID FROM CLAIMANT WHERE CLAIM_ID=" + iClaimId + " AND CLAIMANT_EID=" + iClaimantEID))
                {
                    if (objReader.Read())
                    {
                        iRsrvId = Riskmaster.Common.Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                    }
                }

            }
            catch(Exception e)
            {
                Console.WriteLine("0 ^*^*^ claimant not found");
                sbLogFile.Append(" Claimant not found " + Environment.NewLine);
                throw new Exception("Claimant not Found");
            }

            return iRsrvId;
        }

        private static string GetAdjusterExaminerCd(int iAdjEID, int iClaimId)
        {
            string sAdjExaminerCd = string.Empty;

            try
            {
                using (DbReader objReader = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT REFERENCE_NUMBER FROM ENTITY WHERE ENTITY_ID = " + iAdjEID)) //MITS 35680: aaggarwal29 - Deleting EXAMINERCD column from CLAIM_ADJUSTER
                {
                    if (objReader.Read())
                    {
                        sAdjExaminerCd = Riskmaster.Common.Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("0 ^*^*^ claimant not found");
                sbLogFile.Append(" Claimant not found " + Environment.NewLine);
                throw new Exception("Claimant not Found");
            }

            return sAdjExaminerCd;
        }
                // Payal --Starts RMA:7484 
        private static bool IsExistingLossDetailNode(XMLLossDetail obXmlLossDetail, int iReserverowID)
        {
            if (obXmlLossDetail.LossDetailID == iReserverowID.ToString())
                return true;
            else
                return false;
        }
        
        private static void CreateClaimModificationNode(int iClaimID,int iActivityRowId,ref string sDateOfTransaction,ref string sTimeofTransaction)
        {
            string sDateTime = string.Empty;

           using (DbReader objRdr = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT DTTM_RCD_ADDED FROM ACTIVITY_TRACK WHERE CLAIM_ID = " + iClaimID + " AND FOREIGN_TABLE_ID= 0 AND FOREIGN_TABLE_KEY=0 AND UPLOAD_FLAG=-1 AND ACTIVITY_ROW_ID= "+ iActivityRowId +" ")) 
                {
                    if (objRdr.Read())
                    {
                        sDateTime = Riskmaster.Common.Conversion.ConvertObjToStr(objRdr.GetValue(0));
                        sDateOfTransaction = Riskmaster.Common.Conversion.GetDate(sDateTime.Length >= 8 ? sDateTime.Substring(0, 8) : string.Empty);
                        sTimeofTransaction = Riskmaster.Common.Conversion.GetTime(sDateTime);
                    }
                }
            }
            
        
        // Payal --Ends RMA:7484 

        
    }

}

