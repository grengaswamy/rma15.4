using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Riskmaster.DataModel;
using Riskmaster.Db;
using System.Xml;  
using Riskmaster.Common; 
namespace DBTest
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private string sConStr;
		private string sFilePath;
		private string user;
		private string pwd;
		private string dsn;


		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.Button button10;
		private System.Windows.Forms.Button button11;
		private System.Windows.Forms.Button button12;
		private System.Windows.Forms.Button button13;
		private System.Windows.Forms.Button button14;
		private System.Windows.Forms.Button button15;
		private System.Windows.Forms.Button button16;
		private System.Windows.Forms.Button button17;
		private System.Windows.Forms.Button button18;
		private System.Windows.Forms.Button button19;
		private System.Windows.Forms.Button button20;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button8 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.button10 = new System.Windows.Forms.Button();
			this.button11 = new System.Windows.Forms.Button();
			this.button12 = new System.Windows.Forms.Button();
			this.button13 = new System.Windows.Forms.Button();
			this.button14 = new System.Windows.Forms.Button();
			this.button15 = new System.Windows.Forms.Button();
			this.button16 = new System.Windows.Forms.Button();
			this.button17 = new System.Windows.Forms.Button();
			this.button18 = new System.Windows.Forms.Button();
			this.button19 = new System.Windows.Forms.Button();
			this.button20 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(16, 16);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(96, 32);
			this.button1.TabIndex = 0;
			this.button1.Text = "Bad Event";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(160, 16);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(104, 32);
			this.button2.TabIndex = 1;
			this.button2.Text = "Delete Event";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(16, 56);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(96, 32);
			this.button3.TabIndex = 2;
			this.button3.Text = "Bad Claim";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(160, 56);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(104, 32);
			this.button4.TabIndex = 3;
			this.button4.Text = "Delete Claim";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(16, 96);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(96, 32);
			this.button5.TabIndex = 4;
			this.button5.Text = "Bad Patient";
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(160, 96);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(104, 32);
			this.button6.TabIndex = 5;
			this.button6.Text = "Delete Patient";
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// button7
			// 
			this.button7.Location = new System.Drawing.Point(16, 136);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(96, 32);
			this.button7.TabIndex = 6;
			this.button7.Text = "Bad Employee";
			this.button7.Click += new System.EventHandler(this.button7_Click);
			// 
			// button8
			// 
			this.button8.Location = new System.Drawing.Point(160, 136);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(104, 32);
			this.button8.TabIndex = 7;
			this.button8.Text = "Delete Employee";
			this.button8.Click += new System.EventHandler(this.button8_Click);
			// 
			// button9
			// 
			this.button9.Location = new System.Drawing.Point(16, 176);
			this.button9.Name = "button9";
			this.button9.Size = new System.Drawing.Size(96, 32);
			this.button9.TabIndex = 8;
			this.button9.Text = "Bad Entity";
			this.button9.Click += new System.EventHandler(this.button9_Click);
			// 
			// button10
			// 
			this.button10.Location = new System.Drawing.Point(160, 176);
			this.button10.Name = "button10";
			this.button10.Size = new System.Drawing.Size(104, 32);
			this.button10.TabIndex = 9;
			this.button10.Text = "Delete Entity";
			this.button10.Click += new System.EventHandler(this.button10_Click);
			// 
			// button11
			// 
			this.button11.Location = new System.Drawing.Point(16, 216);
			this.button11.Name = "button11";
			this.button11.Size = new System.Drawing.Size(104, 32);
			this.button11.TabIndex = 10;
			this.button11.Text = "Disability Plan";
			this.button11.Click += new System.EventHandler(this.button11_Click);
			// 
			// button12
			// 
			this.button12.Location = new System.Drawing.Point(160, 216);
			this.button12.Name = "button12";
			this.button12.Size = new System.Drawing.Size(96, 32);
			this.button12.TabIndex = 11;
			this.button12.Text = "Delete Plan";
			this.button12.Click += new System.EventHandler(this.button12_Click);
			// 
			// button13
			// 
			this.button13.Location = new System.Drawing.Point(16, 256);
			this.button13.Name = "button13";
			this.button13.Size = new System.Drawing.Size(96, 32);
			this.button13.TabIndex = 12;
			this.button13.Text = "Bad Policy";
			this.button13.Click += new System.EventHandler(this.button13_Click);
			// 
			// button14
			// 
			this.button14.Location = new System.Drawing.Point(160, 256);
			this.button14.Name = "button14";
			this.button14.Size = new System.Drawing.Size(88, 32);
			this.button14.TabIndex = 13;
			this.button14.Text = "Delete Policy";
			this.button14.Click += new System.EventHandler(this.button14_Click);
			// 
			// button15
			// 
			this.button15.Location = new System.Drawing.Point(16, 296);
			this.button15.Name = "button15";
			this.button15.Size = new System.Drawing.Size(96, 32);
			this.button15.TabIndex = 14;
			this.button15.Text = "Physician";
			this.button15.Click += new System.EventHandler(this.button15_Click);
			// 
			// button16
			// 
			this.button16.Location = new System.Drawing.Point(160, 296);
			this.button16.Name = "button16";
			this.button16.Size = new System.Drawing.Size(112, 32);
			this.button16.TabIndex = 15;
			this.button16.Text = "Delete Physician";
			this.button16.Click += new System.EventHandler(this.button16_Click);
			// 
			// button17
			// 
			this.button17.Location = new System.Drawing.Point(16, 336);
			this.button17.Name = "button17";
			this.button17.Size = new System.Drawing.Size(104, 32);
			this.button17.TabIndex = 16;
			this.button17.Text = "Bad Medical Staff";
			this.button17.Click += new System.EventHandler(this.button17_Click);
			// 
			// button18
			// 
			this.button18.Location = new System.Drawing.Point(160, 336);
			this.button18.Name = "button18";
			this.button18.Size = new System.Drawing.Size(112, 32);
			this.button18.TabIndex = 17;
			this.button18.Text = "Delete Staff";
			this.button18.Click += new System.EventHandler(this.button18_Click);
			// 
			// button19
			// 
			this.button19.Location = new System.Drawing.Point(16, 376);
			this.button19.Name = "button19";
			this.button19.Size = new System.Drawing.Size(88, 24);
			this.button19.TabIndex = 18;
			this.button19.Text = "Bad Vehicle";
			this.button19.Click += new System.EventHandler(this.button19_Click);
			// 
			// button20
			// 
			this.button20.Location = new System.Drawing.Point(160, 376);
			this.button20.Name = "button20";
			this.button20.Size = new System.Drawing.Size(96, 24);
			this.button20.TabIndex = 19;
			this.button20.Text = "Delete Vehicle";
			this.button20.Click += new System.EventHandler(this.button20_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(384, 438);
			this.Controls.Add(this.button20);
			this.Controls.Add(this.button19);
			this.Controls.Add(this.button18);
			this.Controls.Add(this.button17);
			this.Controls.Add(this.button16);
			this.Controls.Add(this.button15);
			this.Controls.Add(this.button14);
			this.Controls.Add(this.button13);
			this.Controls.Add(this.button12);
			this.Controls.Add(this.button11);
			this.Controls.Add(this.button10);
			this.Controls.Add(this.button9);
			this.Controls.Add(this.button8);
			this.Controls.Add(this.button7);
			this.Controls.Add(this.button6);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			string sXml="";
			XmlDocument objErr = new XmlDocument();
			XmlElement Rt  = objErr.CreateElement("Events"); 
			objErr.AppendChild(Rt ); 
			XmlDocument dom = new XmlDocument();
			DataModelFactory dmf = new DataModelFactory("demosvr","csc","csc"); 
			Event o = dmf.GetDataModelObject("Event",false) as Event ;
			DataSet ds = DbFactory.GetDataSet(sConStr
				,"SELECT EVENT_ID FROM EVENT WHERE EVENT_ID>0 ORDER BY EVENT_ID");
			foreach(DataRow row in ds.Tables[0].Rows)
			{
				try
				{
					o.MoveTo(Conversion.ConvertObjToInt(row[0]));
					sXml = o.SerializeObject(dom);
					XmlElement ev = objErr.CreateElement("Event");
					ev.SetAttribute("EventId",o.EventId.ToString());
					ev.SetAttribute("Status", "OK");
					Rt.AppendChild(ev); 
				}
				catch(Exception ex)
				{
					XmlElement ev = objErr.CreateElement("Event");
					ev.SetAttribute("EventId",o.EventId.ToString());
					ev.SetAttribute("Status", "FAIL");
					ev.SetAttribute("Message", ex.Message);
					ev.SetAttribute("StackTrace",ex.StackTrace);
					Rt.AppendChild(ev);  
				}
			}
			objErr.Save(sFilePath + o.GetType().Name + ".xml");  			
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			string IdLst="";
			XmlDocument objErr = new XmlDocument();
			objErr.Load(@"C:\tmp\Event.xml");
			XmlNodeList lst = objErr.SelectNodes("//Event[@Status='FAIL']");
			foreach(XmlNode nod in lst)
			{
				if(IdLst=="")
					IdLst = ((XmlElement)nod).GetAttribute("EventId");
				else
					IdLst = IdLst + "," + ((XmlElement)nod).GetAttribute("EventId");
			}  

			DbConnection con = DbFactory.GetDbConnection(sConStr);
			con.Open(); 
			con.ExecuteNonQuery("DELETE FROM EVENT WHERE EVENT_ID IN (" + IdLst + ")");
		}


		private void button3_Click(object sender, System.EventArgs e)
		{
			string sXml="";
			XmlDocument objErr = new XmlDocument();
			XmlElement Rt  = objErr.CreateElement("Claims"); 
			objErr.AppendChild(Rt ); 
			XmlDocument dom = new XmlDocument();
			DataModelFactory dmf = new DataModelFactory("demosvr","csc","csc"); 
			Claim o = dmf.GetDataModelObject("Claim",false) as Claim;
			DataSet ds = DbFactory.GetDataSet(sConStr
				,"SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_ID>0 ORDER BY CLAIM_ID");
			foreach(DataRow row in ds.Tables[0].Rows)
			{
				try
				{
					o.MoveTo(Conversion.ConvertObjToInt(row[0]));
					sXml = o.SerializeObject(dom);
					XmlElement ev = objErr.CreateElement("Claim");
					ev.SetAttribute("ClaimId",o.ClaimId.ToString());
					ev.SetAttribute("Status", "OK");
					Rt.AppendChild(ev); 
				}
				catch(Exception ex)
				{
					XmlElement ev = objErr.CreateElement("Claim");
					ev.SetAttribute("ClaimId",o.ClaimId.ToString());
					ev.SetAttribute("Status", "FAIL");
					ev.SetAttribute("Message", ex.Message);
					ev.SetAttribute("StackTrace",ex.StackTrace);
					Rt.AppendChild(ev);  
				}
			}
			objErr.Save(sFilePath + o.GetType().Name + ".xml");  			
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			string IdLst="";
			XmlDocument objErr = new XmlDocument();
			objErr.Load(@"C:\tmp\Claim.xml");
			XmlNodeList lst = objErr.SelectNodes("//Claim[@Status='FAIL']");
			foreach(XmlNode nod in lst)
			{
				if(IdLst=="")
					IdLst = ((XmlElement)nod).GetAttribute("ClaimId");
				else
					IdLst = IdLst + "," + ((XmlElement)nod).GetAttribute("ClaimId");
			}  

			DbConnection con = DbFactory.GetDbConnection(sConStr);
			con.Open(); 
			con.ExecuteNonQuery("DELETE FROM CLAIM WHERE CLAIM_ID IN (" + IdLst + ")");
		}

		private void button5_Click(object sender, System.EventArgs e)
		{
			string sXml="";
			XmlDocument objErr = new XmlDocument();
			XmlElement Rt  = objErr.CreateElement("Patients"); 
			objErr.AppendChild(Rt ); 
			XmlDocument dom = new XmlDocument();
			DataModelFactory dmf = new DataModelFactory("demosvr","csc","csc"); 
			Patient o = dmf.GetDataModelObject("Patient",false) as Patient;
			DataSet ds = DbFactory.GetDataSet(sConStr
				,"SELECT PATIENT_ID FROM PATIENT WHERE PATIENT_ID>0 ORDER BY PATIENT_ID");
			foreach(DataRow row in ds.Tables[0].Rows)
			{
				try
				{
					o.MoveTo(Conversion.ConvertObjToInt(row[0]));
					sXml = o.SerializeObject(dom);
					XmlElement ev = objErr.CreateElement("Patient");
					ev.SetAttribute("PatientId",Conversion.ConvertObjToStr(row[0]));
					ev.SetAttribute("Status", "OK");
					Rt.AppendChild(ev); 
				}
				catch(Exception ex)
				{
					XmlElement ev = objErr.CreateElement("Patient");
					ev.SetAttribute("PatientId",Conversion.ConvertObjToStr(row[0]));
					ev.SetAttribute("Status", "FAIL");
					ev.SetAttribute("Message", ex.Message);
					ev.SetAttribute("StackTrace",ex.StackTrace);
					Rt.AppendChild(ev);  
				}
			}
			objErr.Save(sFilePath + o.GetType().Name + ".xml");  	
		}


		private void button6_Click(object sender, System.EventArgs e)
		{
			string IdLst="";
			XmlDocument objErr = new XmlDocument();
			objErr.Load(@"C:\tmp\Patient.xml");
			XmlNodeList lst = objErr.SelectNodes("//Patient[@Status='FAIL']");
			foreach(XmlNode nod in lst)
			{
				if(IdLst=="")
					IdLst = ((XmlElement)nod).GetAttribute("PatientId");
				else
					IdLst = IdLst + "," + ((XmlElement)nod).GetAttribute("PatientId");
			}  

			DbConnection con = DbFactory.GetDbConnection(sConStr);
			con.Open(); 
			con.ExecuteNonQuery("DELETE FROM PATIENT WHERE PATIENT_ID IN (" + IdLst + ")");
		}
		private void button7_Click(object sender, System.EventArgs e)
		{
			string sXml="";
			XmlDocument objErr = new XmlDocument();
			XmlElement Rt  = objErr.CreateElement("Employees"); 
			objErr.AppendChild(Rt ); 
			XmlDocument dom = new XmlDocument();
			DataModelFactory dmf = new DataModelFactory("demosvr","csc","csc"); 
			Employee o = dmf.GetDataModelObject("Employee",false) as Employee ;
			DataSet ds = DbFactory.GetDataSet(sConStr
				,"SELECT EMPLOYEE_EID FROM EMPLOYEE WHERE EMPLOYEE_EID>0 ORDER BY EMPLOYEE_EID");
			foreach(DataRow row in ds.Tables[0].Rows)
			{
				try
				{
					o.MoveTo(Conversion.ConvertObjToInt(row[0]));
					sXml = o.SerializeObject(dom);
					XmlElement ev = objErr.CreateElement("Employee");
					ev.SetAttribute("EmployeeId",Conversion.ConvertObjToStr(row[0]));
					ev.SetAttribute("Status", "OK");
					Rt.AppendChild(ev); 
				}
				catch(Exception ex)
				{
					XmlElement ev = objErr.CreateElement("Employee");
					ev.SetAttribute("EmployeeId",Conversion.ConvertObjToStr(row[0]));
					ev.SetAttribute("Status", "FAIL");
					ev.SetAttribute("Message", ex.Message);
					ev.SetAttribute("StackTrace",ex.StackTrace);
					Rt.AppendChild(ev);  
				}
			}
			objErr.Save(sFilePath + o.GetType().Name + ".xml");  	
		}

		private void button8_Click(object sender, System.EventArgs e)
		{
			string IdLst="";
			XmlDocument objErr = new XmlDocument();
			objErr.Load(@"C:\tmp\Employee.xml");
			XmlNodeList lst = objErr.SelectNodes("//Employee[@Status='FAIL']");
			foreach(XmlNode nod in lst)
			{
				if(IdLst=="")
					IdLst = ((XmlElement)nod).GetAttribute("EmployeeId");
				else
					IdLst = IdLst + "," + ((XmlElement)nod).GetAttribute("EmployeeId");
			}  

			DbConnection con = DbFactory.GetDbConnection(sConStr);
			con.Open(); 
			con.ExecuteNonQuery("DELETE FROM EMPLOYEE WHERE EMPLOYEE_EID IN (" + IdLst + ")");
		}

		private void button9_Click(object sender, System.EventArgs e)
		{
			string sXml="";
			XmlDocument objErr = new XmlDocument();
			XmlElement Rt  = objErr.CreateElement("Entities"); 
			objErr.AppendChild(Rt ); 
			XmlDocument dom = new XmlDocument();
			DataModelFactory dmf = new DataModelFactory("demosvr","csc","csc"); 
			Entity o = dmf.GetDataModelObject("Entity",false) as Entity ;
			DataSet ds = DbFactory.GetDataSet(sConStr
				,"SELECT ENTITY_ID FROM ENTITY WHERE ENTITY_ID>0 ORDER BY ENTITY_ID");
			foreach(DataRow row in ds.Tables[0].Rows)
			{
				try
				{
					o.MoveTo(Conversion.ConvertObjToInt(row[0]));
					sXml = o.SerializeObject(dom);
					XmlElement ev = objErr.CreateElement("Entity");
					ev.SetAttribute("EntityId",Conversion.ConvertObjToStr(row[0]));
					ev.SetAttribute("Status", "OK");
					Rt.AppendChild(ev); 
				}
				catch(Exception ex)
				{
					XmlElement ev = objErr.CreateElement("Entity");
					ev.SetAttribute("EntityId",Conversion.ConvertObjToStr(row[0]));
					ev.SetAttribute("Status", "FAIL");
					ev.SetAttribute("Message", ex.Message);
					ev.SetAttribute("StackTrace",ex.StackTrace);
					Rt.AppendChild(ev);  
				}
			}
			objErr.Save(sFilePath + o.GetType().Name + ".xml");  		
		}

		private void button10_Click(object sender, System.EventArgs e)
		{
			string IdLst="";
			XmlDocument objErr = new XmlDocument();
			objErr.Load(@"C:\tmp\Entity.xml");
			XmlNodeList lst = objErr.SelectNodes("//Entity[@Status='FAIL']");
			foreach(XmlNode nod in lst)
			{
				if(IdLst=="")
					IdLst = ((XmlElement)nod).GetAttribute("EntityId");
				else
					IdLst = IdLst + "," + ((XmlElement)nod).GetAttribute("EntityId");
			}  

			DbConnection con = DbFactory.GetDbConnection(sConStr);
			con.Open(); 
			con.ExecuteNonQuery("DELETE FROM ENTITY WHERE ENTITY_ID IN (" + IdLst + ")");	
		}


		private void button11_Click(object sender, System.EventArgs e)
		{
			string sXml="";
			XmlDocument objErr = new XmlDocument();
			XmlElement Rt  = objErr.CreateElement("DisabilityPlans"); 
			objErr.AppendChild(Rt ); 
			XmlDocument dom = new XmlDocument();
			DataModelFactory dmf = new DataModelFactory("demosvr","csc","csc"); 
			DisabilityPlan o = dmf.GetDataModelObject("DisabilityPlan",false) as DisabilityPlan ;
			DataSet ds = DbFactory.GetDataSet(sConStr
				,"SELECT PLAN_ID FROM DISABILITY_PLAN WHERE PLAN_ID>0 ORDER BY PLAN_ID");
			foreach(DataRow row in ds.Tables[0].Rows)
			{
				try
				{
					o.MoveTo(Conversion.ConvertObjToInt(row[0]));
					sXml = o.SerializeObject(dom);
					XmlElement ev = objErr.CreateElement("DisabilityPlan");
					ev.SetAttribute("PlanId",Conversion.ConvertObjToStr(row[0]));
					ev.SetAttribute("Status", "OK");
					Rt.AppendChild(ev); 
				}
				catch(Exception ex)
				{
					XmlElement ev = objErr.CreateElement("DisabilityPlan");
					ev.SetAttribute("PlanId",Conversion.ConvertObjToStr(row[0]));
					ev.SetAttribute("Status", "FAIL");
					ev.SetAttribute("Message", ex.Message);
					ev.SetAttribute("StackTrace",ex.StackTrace);
					Rt.AppendChild(ev);  
				}
			}
			objErr.Save(sFilePath + o.GetType().Name + ".xml");  		
		}

		private void button12_Click(object sender, System.EventArgs e)
		{
			string IdLst="";
			XmlDocument objErr = new XmlDocument();
			objErr.Load(@"C:\tmp\DisabilityPlan.xml");
			XmlNodeList lst = objErr.SelectNodes("//DisabilityPlan[@Status='FAIL']");
			foreach(XmlNode nod in lst)
			{
				if(IdLst=="")
					IdLst = ((XmlElement)nod).GetAttribute("PlanId");
				else
					IdLst = IdLst + "," + ((XmlElement)nod).GetAttribute("PlanId");
			}  

			DbConnection con = DbFactory.GetDbConnection(sConStr);
			con.Open(); 
			con.ExecuteNonQuery("DELETE FROM DISABILITY_PLAN WHERE PLAN_ID IN (" + IdLst + ")");			
		}

		private void button13_Click(object sender, System.EventArgs e)
		{
			string sXml="";
			XmlDocument objErr = new XmlDocument();
			XmlElement Rt  = objErr.CreateElement("Policies"); 
			objErr.AppendChild(Rt ); 
			XmlDocument dom = new XmlDocument();
			DataModelFactory dmf = new DataModelFactory("demosvr","csc","csc"); 
			Policy o = dmf.GetDataModelObject("Policy",false) as Policy ;
			DataSet ds = DbFactory.GetDataSet(sConStr
				,"SELECT POLICY_ID FROM POLICY WHERE POLICY_ID>0 ORDER BY POLICY_ID");
			foreach(DataRow row in ds.Tables[0].Rows)
			{
				try
				{
					o.MoveTo(Conversion.ConvertObjToInt(row[0]));
					sXml = o.SerializeObject(dom);
					XmlElement ev = objErr.CreateElement("Policy");
					ev.SetAttribute("PolicyId",Conversion.ConvertObjToStr(row[0]));
					ev.SetAttribute("Status", "OK");
					Rt.AppendChild(ev); 
				}
				catch(Exception ex)
				{
					XmlElement ev = objErr.CreateElement("Policy");
					ev.SetAttribute("PolicyId",Conversion.ConvertObjToStr(row[0]));
					ev.SetAttribute("Status", "FAIL");
					ev.SetAttribute("Message", ex.Message);
					ev.SetAttribute("StackTrace",ex.StackTrace);
					Rt.AppendChild(ev);  
				}
			}
			objErr.Save(sFilePath + o.GetType().Name + ".xml");  	
		}

		private void button14_Click(object sender, System.EventArgs e)
		{
			string IdLst="";
			XmlDocument objErr = new XmlDocument();
			objErr.Load(@"C:\tmp\Policy.xml");
			XmlNodeList lst = objErr.SelectNodes("//Policy[@Status='FAIL']");
			foreach(XmlNode nod in lst)
			{
				if(IdLst=="")
					IdLst = ((XmlElement)nod).GetAttribute("PolicyId");
				else
					IdLst = IdLst + "," + ((XmlElement)nod).GetAttribute("PolicyId");
			}  

			DbConnection con = DbFactory.GetDbConnection(sConStr);
			con.Open(); 
			con.ExecuteNonQuery("DELETE FROM POLICY WHERE POLICY_ID IN (" + IdLst + ")");
		}

		private void button15_Click(object sender, System.EventArgs e)
		{
			string sXml="";
			XmlDocument objErr = new XmlDocument();
			XmlElement Rt  = objErr.CreateElement("Physicians"); 
			objErr.AppendChild(Rt ); 
			XmlDocument dom = new XmlDocument();
			DataModelFactory dmf = new DataModelFactory("demosvr","csc","csc"); 
			Physician o = dmf.GetDataModelObject("Physician",false) as Physician ;
			DataSet ds = DbFactory.GetDataSet(sConStr
				,"SELECT PHYS_EID FROM PHYSICIAN WHERE PHYS_EID>0 ORDER BY PHYS_EID");
			foreach(DataRow row in ds.Tables[0].Rows)
			{
				try
				{
					o.MoveTo(Conversion.ConvertObjToInt(row[0]));
					sXml = o.SerializeObject(dom);
					XmlElement ev = objErr.CreateElement("Physician");
					ev.SetAttribute("PhysicianId",Conversion.ConvertObjToStr(row[0]));
					ev.SetAttribute("Status", "OK");
					Rt.AppendChild(ev); 
				}
				catch(Exception ex)
				{
					XmlElement ev = objErr.CreateElement("Physician");
					ev.SetAttribute("PhysicianId",Conversion.ConvertObjToStr(row[0]));
					ev.SetAttribute("Status", "FAIL");
					ev.SetAttribute("Message", ex.Message);
					ev.SetAttribute("StackTrace",ex.StackTrace);
					Rt.AppendChild(ev);  
				}
			}
			objErr.Save(sFilePath + o.GetType().Name + ".xml");  			
		}

		private void button16_Click(object sender, System.EventArgs e)
		{
			string IdLst="";
			XmlDocument objErr = new XmlDocument();
			objErr.Load(@"C:\tmp\Physician.xml");
			XmlNodeList lst = objErr.SelectNodes("//Physician[@Status='FAIL']");
			foreach(XmlNode nod in lst)
			{
				if(IdLst=="")
					IdLst = ((XmlElement)nod).GetAttribute("PhysicianId");
				else
					IdLst = IdLst + "," + ((XmlElement)nod).GetAttribute("PhysicianId");
			}  

			DbConnection con = DbFactory.GetDbConnection(sConStr);
			con.Open(); 
			con.ExecuteNonQuery("DELETE FROM PHYSICIAN WHERE PHYS_EID IN (" + IdLst + ")");
		}

		private void button17_Click(object sender, System.EventArgs e)
		{
			string sXml="";
			XmlDocument objErr = new XmlDocument();
			XmlElement Rt  = objErr.CreateElement("MedicalStaffs"); 
			objErr.AppendChild(Rt ); 
			XmlDocument dom = new XmlDocument();
			DataModelFactory dmf = new DataModelFactory("demosvr","csc","csc"); 
			MedicalStaff o = dmf.GetDataModelObject("MedicalStaff",false) as MedicalStaff ;
			DataSet ds = DbFactory.GetDataSet(sConStr
				,"SELECT STAFF_EID FROM MED_STAFF WHERE STAFF_EID>0 ORDER BY STAFF_EID");
			foreach(DataRow row in ds.Tables[0].Rows)
			{
				try
				{
					o.MoveTo(Conversion.ConvertObjToInt(row[0]));
					sXml = o.SerializeObject(dom);
					XmlElement ev = objErr.CreateElement("MedicalStaff");
					ev.SetAttribute("StaffId",Conversion.ConvertObjToStr(row[0]));
					ev.SetAttribute("Status", "OK");
					Rt.AppendChild(ev); 
				}
				catch(Exception ex)
				{
					XmlElement ev = objErr.CreateElement("MedicalStaff");
					ev.SetAttribute("StaffId",Conversion.ConvertObjToStr(row[0]));
					ev.SetAttribute("Status", "FAIL");
					ev.SetAttribute("Message", ex.Message);
					ev.SetAttribute("StackTrace",ex.StackTrace);
					Rt.AppendChild(ev);  
				}
			}
			objErr.Save(sFilePath + o.GetType().Name + ".xml");  	
		}

		private void button18_Click(object sender, System.EventArgs e)
		{
			string IdLst="";
			XmlDocument objErr = new XmlDocument();
			objErr.Load(sFilePath + "MedicalStaff.xml");
			XmlNodeList lst = objErr.SelectNodes("//MedicalStaff[@Status='FAIL']");
			foreach(XmlNode nod in lst)
			{
				if(IdLst=="")
					IdLst = ((XmlElement)nod).GetAttribute("StaffId");
				else
					IdLst = IdLst + "," + ((XmlElement)nod).GetAttribute("StaffId");
			}  

			DbConnection con = DbFactory.GetDbConnection(sConStr);
			con.Open(); 
			con.ExecuteNonQuery("DELETE FROM MED_STAFF WHERE STAFF_EID IN (" + IdLst + ")");
		}

		private void button19_Click(object sender, System.EventArgs e)
		{
			string sXml="";
			XmlDocument objErr = new XmlDocument();
			XmlElement Rt  = objErr.CreateElement("Vehicles"); 
			objErr.AppendChild(Rt ); 
			XmlDocument dom = new XmlDocument();
			DataModelFactory dmf = new DataModelFactory(dsn,user,pwd); 
			Vehicle o = dmf.GetDataModelObject("Vehicle",false) as Vehicle ;
			DataSet ds = DbFactory.GetDataSet(dmf.Context.DbConn.ConnectionString   
				,"SELECT UNIT_ID FROM VEHICLE WHERE UNIT_ID>0 ORDER BY UNIT_ID");
			foreach(DataRow row in ds.Tables[0].Rows)
			{
				try
				{
					o.MoveTo(Conversion.ConvertObjToInt(row[0]));
					sXml = o.SerializeObject(dom);
					XmlElement ev = objErr.CreateElement("Vehicle");
					ev.SetAttribute("VehicleId",Conversion.ConvertObjToStr(row[0]));
					ev.SetAttribute("Status", "OK");
					Rt.AppendChild(ev); 
				}
				catch(Exception ex)
				{
					XmlElement ev = objErr.CreateElement("Vehicle");
					ev.SetAttribute("VehicleId",Conversion.ConvertObjToStr(row[0]));
					ev.SetAttribute("Status", "FAIL");
					ev.SetAttribute("Message", ex.Message);
					ev.SetAttribute("StackTrace",ex.StackTrace);
					Rt.AppendChild(ev);  
				}
			}
			objErr.Save(sFilePath + o.GetType().Name + ".xml");
		}

		private void button20_Click(object sender, System.EventArgs e)
		{
			string IdLst="";
			XmlDocument objErr = new XmlDocument();
			objErr.Load(sFilePath + "Vehicle.xml");
			XmlNodeList lst = objErr.SelectNodes("//Vehicle[@Status='FAIL']");
			foreach(XmlNode nod in lst)
			{
				if(IdLst=="")
					IdLst = ((XmlElement)nod).GetAttribute("VehicleId");
				else
					IdLst = IdLst + "," + ((XmlElement)nod).GetAttribute("VehicleId");
			}  

			DbConnection con = DbFactory.GetDbConnection(sConStr);
			con.Open(); 
			con.ExecuteNonQuery("DELETE FROM VEHICLE WHERE UNIT_ID IN (" + IdLst + ")");
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			sConStr =System.Configuration.ConfigurationSettings.AppSettings["ConnStr"];
			user  = System.Configuration.ConfigurationSettings.AppSettings["User"];
			pwd  = System.Configuration.ConfigurationSettings.AppSettings["Pwd"];
			dsn  = System.Configuration.ConfigurationSettings.AppSettings["Dsn"];
			sFilePath =System.Configuration.ConfigurationSettings.AppSettings["FilePath"];
		}
	}
}