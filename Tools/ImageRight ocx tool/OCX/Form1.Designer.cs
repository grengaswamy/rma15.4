﻿namespace OCX
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            this.openFileBtn = new System.Windows.Forms.Button();
            this.locationEditor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.drawerEditor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.fileTypeNameEditor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.fileNumberEditor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.label = new Infragistics.Win.Misc.UltraLabel();
            this.documentIdEditor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.fileLabel = new Infragistics.Win.Misc.UltraLabel();
            this.documentLabel = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.attributeValueEditor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.attributeNameEditor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.targetFileNumberEditor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.targetFileTypeEditor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.mergeLbl = new Infragistics.Win.Misc.UltraLabel();
            this.mergeFilesBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.locationEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.drawerEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileTypeNameEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileNumberEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentIdEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.attributeValueEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.attributeNameEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.targetFileNumberEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.targetFileTypeEditor)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileBtn
            // 
            this.openFileBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.openFileBtn.Location = new System.Drawing.Point(166, 360);
            this.openFileBtn.Name = "openFileBtn";
            this.openFileBtn.Size = new System.Drawing.Size(75, 23);
            this.openFileBtn.TabIndex = 9;
            this.openFileBtn.Text = "Click Me";
            this.openFileBtn.UseVisualStyleBackColor = true;
            this.openFileBtn.Click += new System.EventHandler(this.OpenFileBtnClick);
            // 
            // locationEditor
            // 
            this.locationEditor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.locationEditor.Location = new System.Drawing.Point(15, 89);
            this.locationEditor.Name = "locationEditor";
            this.locationEditor.NullText = "Location ID (-1 if none)";
            appearance1.FontData.ItalicAsString = "True";
            this.locationEditor.NullTextAppearance = appearance1;
            this.locationEditor.Size = new System.Drawing.Size(386, 21);
            this.locationEditor.TabIndex = 2;
            // 
            // drawerEditor
            // 
            this.drawerEditor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.drawerEditor.Location = new System.Drawing.Point(15, 116);
            this.drawerEditor.Name = "drawerEditor";
            this.drawerEditor.NullText = "Drawer Name";
            appearance2.FontData.ItalicAsString = "True";
            this.drawerEditor.NullTextAppearance = appearance2;
            this.drawerEditor.Size = new System.Drawing.Size(386, 21);
            this.drawerEditor.TabIndex = 3;
            // 
            // fileTypeNameEditor
            // 
            this.fileTypeNameEditor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fileTypeNameEditor.Location = new System.Drawing.Point(15, 143);
            this.fileTypeNameEditor.Name = "fileTypeNameEditor";
            this.fileTypeNameEditor.NullText = "File Type Name";
            appearance6.FontData.ItalicAsString = "True";
            this.fileTypeNameEditor.NullTextAppearance = appearance6;
            this.fileTypeNameEditor.Size = new System.Drawing.Size(386, 21);
            this.fileTypeNameEditor.TabIndex = 4;
            // 
            // fileNumberEditor
            // 
            this.fileNumberEditor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fileNumberEditor.Location = new System.Drawing.Point(15, 170);
            this.fileNumberEditor.Name = "fileNumberEditor";
            this.fileNumberEditor.NullText = "File Number";
            appearance7.FontData.ItalicAsString = "True";
            this.fileNumberEditor.NullTextAppearance = appearance7;
            this.fileNumberEditor.Size = new System.Drawing.Size(386, 21);
            this.fileNumberEditor.TabIndex = 5;
            // 
            // label
            // 
            this.label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label.Location = new System.Drawing.Point(15, 13);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(386, 41);
            this.label.TabIndex = 6;
            this.label.Text = "Make sure IR Desktop is running, then provide the following:";
            // 
            // documentIdEditor
            // 
            this.documentIdEditor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.documentIdEditor.Location = new System.Drawing.Point(15, 333);
            this.documentIdEditor.Name = "documentIdEditor";
            this.documentIdEditor.NullText = "Document ID";
            appearance5.FontData.ItalicAsString = "True";
            this.documentIdEditor.NullTextAppearance = appearance5;
            this.documentIdEditor.Size = new System.Drawing.Size(386, 21);
            this.documentIdEditor.TabIndex = 8;
            // 
            // fileLabel
            // 
            this.fileLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fileLabel.Location = new System.Drawing.Point(15, 50);
            this.fileLabel.Name = "fileLabel";
            this.fileLabel.Size = new System.Drawing.Size(140, 33);
            this.fileLabel.TabIndex = 8;
            this.fileLabel.Text = "To open file:";
            // 
            // documentLabel
            // 
            this.documentLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.documentLabel.Location = new System.Drawing.Point(15, 304);
            this.documentLabel.Name = "documentLabel";
            this.documentLabel.Size = new System.Drawing.Size(195, 23);
            this.documentLabel.TabIndex = 9;
            this.documentLabel.Text = "To select document:";
            // 
            // ultraLabel1
            // 
            this.ultraLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ultraLabel1.Location = new System.Drawing.Point(12, 208);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(140, 23);
            this.ultraLabel1.TabIndex = 10;
            this.ultraLabel1.Text = "or:";
            // 
            // attributeValueEditor
            // 
            this.attributeValueEditor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.attributeValueEditor.Location = new System.Drawing.Point(12, 264);
            this.attributeValueEditor.Name = "attributeValueEditor";
            this.attributeValueEditor.NullText = "Attribute Value (demo - only type String is supported)";
            appearance8.FontData.ItalicAsString = "True";
            this.attributeValueEditor.NullTextAppearance = appearance8;
            this.attributeValueEditor.Size = new System.Drawing.Size(386, 21);
            this.attributeValueEditor.TabIndex = 7;
            // 
            // attributeNameEditor
            // 
            this.attributeNameEditor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.attributeNameEditor.Location = new System.Drawing.Point(12, 237);
            this.attributeNameEditor.Name = "attributeNameEditor";
            this.attributeNameEditor.NullText = "File-Level Attribute Name (demo - only type String is supported)";
            appearance9.FontData.ItalicAsString = "True";
            this.attributeNameEditor.NullTextAppearance = appearance9;
            this.attributeNameEditor.Size = new System.Drawing.Size(386, 21);
            this.attributeNameEditor.TabIndex = 6;
            // 
            // targetFileNumberEditor
            // 
            this.targetFileNumberEditor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.targetFileNumberEditor.Location = new System.Drawing.Point(12, 463);
            this.targetFileNumberEditor.Name = "targetFileNumberEditor";
            this.targetFileNumberEditor.NullText = "Target File Number";
            appearance4.FontData.ItalicAsString = "True";
            this.targetFileNumberEditor.NullTextAppearance = appearance4;
            this.targetFileNumberEditor.Size = new System.Drawing.Size(386, 21);
            this.targetFileNumberEditor.TabIndex = 11;
            // 
            // targetFileTypeEditor
            // 
            this.targetFileTypeEditor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.targetFileTypeEditor.Location = new System.Drawing.Point(12, 436);
            this.targetFileTypeEditor.Name = "targetFileTypeEditor";
            this.targetFileTypeEditor.NullText = "Programmatic Name of Target File Type";
            appearance3.FontData.ItalicAsString = "True";
            this.targetFileTypeEditor.NullTextAppearance = appearance3;
            this.targetFileTypeEditor.Size = new System.Drawing.Size(386, 21);
            this.targetFileTypeEditor.TabIndex = 10;
            // 
            // mergeLbl
            // 
            this.mergeLbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mergeLbl.Location = new System.Drawing.Point(12, 407);
            this.mergeLbl.Name = "mergeLbl";
            this.mergeLbl.Size = new System.Drawing.Size(140, 23);
            this.mergeLbl.TabIndex = 13;
            this.mergeLbl.Text = "To merge files:";
            // 
            // mergeFilesBtn
            // 
            this.mergeFilesBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.mergeFilesBtn.Location = new System.Drawing.Point(166, 492);
            this.mergeFilesBtn.Name = "mergeFilesBtn";
            this.mergeFilesBtn.Size = new System.Drawing.Size(75, 23);
            this.mergeFilesBtn.TabIndex = 12;
            this.mergeFilesBtn.Text = "Click Me";
            this.mergeFilesBtn.UseVisualStyleBackColor = true;
            this.mergeFilesBtn.Click += new System.EventHandler(this.MergeFilesBtnClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 527);
            this.Controls.Add(this.mergeFilesBtn);
            this.Controls.Add(this.targetFileNumberEditor);
            this.Controls.Add(this.targetFileTypeEditor);
            this.Controls.Add(this.mergeLbl);
            this.Controls.Add(this.attributeValueEditor);
            this.Controls.Add(this.attributeNameEditor);
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.documentLabel);
            this.Controls.Add(this.fileLabel);
            this.Controls.Add(this.documentIdEditor);
            this.Controls.Add(this.label);
            this.Controls.Add(this.fileNumberEditor);
            this.Controls.Add(this.fileTypeNameEditor);
            this.Controls.Add(this.drawerEditor);
            this.Controls.Add(this.locationEditor);
            this.Controls.Add(this.openFileBtn);
            this.Name = "Form1";
            this.Text = "IR Desktop OCX Sample";
            ((System.ComponentModel.ISupportInitialize)(this.locationEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.drawerEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileTypeNameEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileNumberEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentIdEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.attributeValueEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.attributeNameEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.targetFileNumberEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.targetFileTypeEditor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button openFileBtn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor locationEditor;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor drawerEditor;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor fileTypeNameEditor;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor fileNumberEditor;
        private Infragistics.Win.Misc.UltraLabel label;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor documentIdEditor;
        private Infragistics.Win.Misc.UltraLabel fileLabel;
        private Infragistics.Win.Misc.UltraLabel documentLabel;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor attributeValueEditor;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor attributeNameEditor;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor targetFileNumberEditor;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor targetFileTypeEditor;
        private Infragistics.Win.Misc.UltraLabel mergeLbl;
        private System.Windows.Forms.Button mergeFilesBtn;
    }
}

