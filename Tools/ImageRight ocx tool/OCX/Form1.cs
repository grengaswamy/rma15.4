﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using ImageRight.Desktop.Interop;
using imageright.tracer.interfaces;

namespace OCX
{
    public partial class Form1 : Form
    {
        private static Desktop _desktopOcx;

        private class Input
        {
            public long LocationId;
            public string DrawerName;
            public string FileTypeName;
            public string FileNumber;

            public long DocumentId;

            public string FileAttributeName;
            public string FileAttributeValue;

            public string TargetFileNumber;
            public string TargetFileType;
        }

        public Form1()
        {
            InitializeComponent();
            _desktopOcx = new ImageRight.Desktop.Interop.Desktop();
        }

        private void OpenFileBtnClick(object sender, EventArgs e)
        {
            try
            {
                AssertDesktopIsRunning();

                var userInput = ReadUserInput();

                OpenFile(userInput);

                AssertFileIsOpen(userInput.FileNumber);

                SetFocusToDocument(userInput.DocumentId);

                AssertDocumentIsSelected(userInput.DocumentId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void OpenFileOnUrlProtocol(string p_sDrawer, string p_sFileType, string p_sFileNumber, long p_lDocumentID)
        {
            try
            {
                Input userInput = new Input();
                userInput.LocationId = -1L;
                userInput.DrawerName = p_sDrawer;
                userInput.FileTypeName = p_sFileType;
                userInput.FileNumber = p_sFileNumber;
                userInput.DocumentId = p_lDocumentID;
                AssertDesktopIsRunning();
                OpenFile(userInput);
                AssertFileIsOpen(userInput.FileNumber);

                if (p_lDocumentID > 0L)
                {
                    SetFocusToDocument(userInput.DocumentId);
                }
                

                //AssertDocumentIsSelected(userInput.DocumentId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MergeFilesBtnClick(object sender, EventArgs e)
        {
            try
            {
                var userInput = ReadUserInput();

                AssertDesktopIsRunning();

                AssertFileIsOpen(userInput.FileNumber);

                MergeFile(userInput);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private static void AssertDesktopIsRunning()
        {
            try
            {
                if (_desktopOcx.Application == null)
                    throw new ApplicationException("IR Desktop appears to be not running");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        private Input ReadUserInput()
        {
            return new Input
            {
                LocationId = !string.IsNullOrEmpty(locationEditor.Text) ? Int64.Parse(locationEditor.Text) : -1,
                DrawerName = drawerEditor.Text.Trim(),
                FileTypeName = fileTypeNameEditor.Text.Trim(),
                FileNumber = fileNumberEditor.Text.Trim(),
                DocumentId = !string.IsNullOrEmpty(documentIdEditor.Text) ? Int64.Parse(documentIdEditor.Text) : -1,
                FileAttributeName = attributeNameEditor.Text.Trim(),
                FileAttributeValue = attributeValueEditor.Text.Trim(),
                TargetFileType = targetFileTypeEditor.Text.Trim(),
                TargetFileNumber = targetFileNumberEditor.Text.Trim()
            };
        }

        private static void OpenFile(Input userInput)
        {
            if (CanOpenByFileNumber(userInput))
            {
                _desktopOcx.Application.FileManager.OpenFile(userInput.LocationId, userInput.DrawerName, userInput.FileTypeName, userInput.FileNumber);
            }
            else if (CanOpenByDocumentAttribute(userInput))
            {
                _desktopOcx.Application.FileManager.OpenFileByFileAttribute(userInput.FileAttributeName, userInput.FileAttributeValue);
            }
            else
            {
                throw new Exception("Please provide input to open file.");
            }
        }

        private static bool CanOpenByDocumentAttribute(Input userInput)
        {
            if (string.IsNullOrEmpty(userInput.FileAttributeName))
                return false;
            if (string.IsNullOrEmpty(userInput.FileAttributeValue))
                return false;
            return true;
        }

        private static bool CanOpenByFileNumber(Input userInput)
        {
            if (string.IsNullOrEmpty(userInput.DrawerName))
                return false;
            if (string.IsNullOrEmpty(userInput.FileTypeName))
                return false;
            if (string.IsNullOrEmpty(userInput.FileNumber))
                return false;
            return true;
        }

        private static void AssertFileIsOpen(string fileNumber)
        {
            if (!string.IsNullOrEmpty(fileNumber))
            {
                if (!string.Equals(fileNumber, _desktopOcx.Application.FileManager.FileNumber, StringComparison.InvariantCultureIgnoreCase))
                    throw new ApplicationException("File could not be opened.");
            }
            if (!_desktopOcx.Application.FileManager.HasActiveFile)
                throw new ApplicationException("No open files detected.");
        }

        private static void SetFocusToDocument(long documentId)
        {
            _desktopOcx.Application.TreeManager.FocusOnDocument(documentId);
        }

        private void AssertDocumentIsSelected(long documentId)
        {
            if (documentId != -1)
            {
                if (_desktopOcx.Application.FileManager.DocumentId != documentId)
                    throw new Exception(string.Format("Document with id {0} was not selected.", documentId));
                
                label.Text = string.Format("Document with Id {0} was selected in the IR Desktop.", _desktopOcx.Application.FileManager.DocumentId.ToString());
            }
            else
            {
                label.Text = "No documents were selected in the IR Desktop.";
            }
        }

        private static void MergeFile(Input userInput)
        {
            if (CanMergeFiles(userInput))
            {
                var locationId = _desktopOcx.Application.FileManager.LocationId;

                var currentDrawer = _desktopOcx.Application.FileManager.Drawer;
                var currentFileType = _desktopOcx.Application.FileManager.FileType;
                var currentFileNumber = _desktopOcx.Application.FileManager.FileNumber;

                var newDrawer = currentDrawer;
                var newFileType = GetTargetFileType(userInput.TargetFileType, GetAllFileTypesInDrawer(GetDrawerId(_desktopOcx.Application.FileManager.LocationId, newDrawer)));
                var newFileNumber = CreateNewMultipartFileNumber(userInput.TargetFileNumber);

                if (string.Equals(currentFileType, newFileType, StringComparison.InvariantCultureIgnoreCase) && string.Equals(currentFileNumber, userInput.TargetFileNumber, StringComparison.InvariantCultureIgnoreCase))
                    NotifySourceAndTargetAreSame();
                else if (IsMergeActionConfirmed())
                    _desktopOcx.Application.FileManager.ChangeDrawerAndFileNumber(locationId, currentDrawer, currentFileType, currentFileNumber, newDrawer, newFileType, newFileNumber);
            }
            else
            {
                throw new Exception("Please ensure that a file is open and target file type and target file number are specified.");
            }
        }

        private static void NotifySourceAndTargetAreSame()
        {
            throw new Exception("Source and Target files are the same.");
        }

        private static bool IsMergeActionConfirmed()
        {
            return DialogResult.Yes ==
                   MessageBox.Show(
                       "This application will attempt to merge currently active file according to the specified parameters. This action cannot be undone. Continue?",
                       "Confirmation", MessageBoxButtons.YesNo);
        }

        private static bool CanMergeFiles(Input userInput)
        {
            if (string.IsNullOrEmpty(userInput.TargetFileType))
                return false;
            if (string.IsNullOrEmpty(userInput.TargetFileNumber))
                return false;
            return true;
        }

        private static long GetDrawerId(long locationId, string drawerName)
        {
            long toReturn = -1;

            var allDrawers = _desktopOcx.Application.FileManager.GetDrawers(locationId) as object[];
            if (allDrawers != null)
            {
                // 0 - drawer id, 1 - drawer name, 2 -drawer description
                foreach (var drawerInfo in allDrawers.Cast<string>().Select(drawerData => drawerData.Split(';'))
                                  .Where(drawerInfo => string.Equals(drawerInfo[1], drawerName, StringComparison.InvariantCultureIgnoreCase)))
                {
                    toReturn = Int64.Parse(drawerInfo[0]);
                    break;
                }
            }

            if (toReturn == -1)
                throw new Exception("Specified Drawer was not found.");

            return toReturn;
        }

        private static IEnumerable<string> GetAllFileTypesInDrawer(long drawerId)
        {
            var fileTypes = _desktopOcx.Application.FileManager.GetFileTypes(drawerId) as object[];
            var toReturn = fileTypes  != null  ? fileTypes.Cast<String>() : new string[0];
            return toReturn;
        }

        private static string GetTargetFileType(string targetFileType, IEnumerable<string> allowedFileTypes)
        {
            foreach (var fileType in allowedFileTypes.Where(allowedFileType => string.Equals(targetFileType, allowedFileType, StringComparison.InvariantCultureIgnoreCase)))
                return fileType;

            throw new Exception("Target file type is not allowed in the current drawer.");
        }

        private static object CreateNewMultipartFileNumber(string targetFileNumber)
        {
            return _desktopOcx.Application.FileManager.CreateNewMultipartFileNumber(targetFileNumber, string.Empty, string.Empty);
        }
    }
}
