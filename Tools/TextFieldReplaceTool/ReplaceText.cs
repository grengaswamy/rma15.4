using System;
using System.Data;
using Riskmaster.Db;
using Riskmaster.Common;
using System.Xml;
using System.IO;

namespace TextFieldRepaceTool
{
	/// <summary>
	/// Summary description for RemoveBadAscii.
	/// </summary>
	public class ReplaceTextClass
	{
		public delegate void progressDelegate(string sMessage);
		public event progressDelegate showProgress;
		public event progressDelegate showComplete;
		public delegate void messageDelegate(string sMessage, bool bError);
		public event messageDelegate showMessage;

		private string m_sConnectionString = string.Empty;
		private eDatabaseType m_DatabaseType;
		private string m_sFilePath = string.Empty;
		private bool m_bPauseScan = false;
		private const string m_ConfigXmlFile = "ReplaceText.xml";
        private const string m_ResultXmlFile = "FoundRecords.xml";
        private string m_sOriginal = string.Empty;
        private string m_sNew = string.Empty;

		public ReplaceTextClass(string sConnectionString, eDatabaseType p_DatabaseType, string sPath, string sOriginal, string sNew)
		{
			//
			// TODO: Add constructor logic here
			//
			m_sConnectionString = sConnectionString;
			m_DatabaseType = p_DatabaseType;
            m_sFilePath = sPath;
            m_sOriginal = sOriginal;
            m_sNew = sNew;
		}


		/// <summary>
		/// scan the table.column defined in the configure file for
		/// the specified text
		/// </summary>
		public void scanTextField()
		{
			XmlDocument oConfigXml = new XmlDocument();
			XmlDocument oBadRecordsXml = new XmlDocument();
			DbReader oReader = null;
			int iBadRecords = 0;
			bool bError = false;

			try
			{
				oConfigXml.Load( m_sFilePath + m_ConfigXmlFile);

				oBadRecordsXml.LoadXml("<records/>");
				XmlNode oParentNode = oBadRecordsXml.SelectSingleNode("/records");

				//raise the scanTable event
				string sMessage = "Start Scanning for the specified text.";
				showProgress(sMessage);

				//loop through all the tables which are required to be scanned
				XmlNodeList oTableNodeList = oConfigXml.SelectNodes("//tables/table[@scan='yes']");
				foreach(XmlNode oTableNode in oTableNodeList)
				{
					string sTableName = oTableNode.Attributes["name"].Value;
					string sIDColumn = oTableNode.Attributes["idcolumn"].Value;
					string sTextColumn = oTableNode.Attributes["textcolumn"].Value;
					string sHtmlTextColumn = oTableNode.Attributes["htmltextcolumn"].Value;
					
					//raise the scanTable event
					sMessage = "Scanning table: " + sTableName + "." + sTextColumn + System.Environment.NewLine;
					showProgress(sMessage);

					string sSQL = "SELECT " + sIDColumn + " FROM " + sTableName + " WHERE ";
					
					switch( m_DatabaseType )
					{
						case eDatabaseType.DBMS_IS_SQLSRVR:
                            if( sTextColumn.Length > 0 )
							    sSQL += "PATINDEX('%" + m_sOriginal + "%', " + sTextColumn + ")>0";
                            else
                                sSQL += "PATINDEX('%" + m_sOriginal + "%', " + sHtmlTextColumn + ")>0";
							break;
						case eDatabaseType.DBMS_IS_ORACLE:
                            if( sTextColumn.Length > 0 )
                                sSQL += "INSTR(" + sTextColumn + ", '" + m_sOriginal + "')>0";
                            else
                                sSQL += "INSTR(" + sHtmlTextColumn + ", '" + m_sOriginal + "')>0";
							break;
						default:
                            if (sTextColumn.Length > 0)
                                sSQL += "PATINDEX('%" + m_sOriginal + "%', " + sTextColumn + ")>0";
                            else
                                sSQL += "PATINDEX('%" + m_sOriginal + "%', " + sHtmlTextColumn + ")>0";
							break;
					}

					oReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
					while( oReader.Read() )
					{
						int iRecordId = oReader.GetInt(sIDColumn);
						XmlElement oBadRecord = oBadRecordsXml.CreateElement("record");
						oBadRecord.SetAttribute("tablename", sTableName);
						oBadRecord.SetAttribute("idcolumn", sIDColumn);
						oBadRecord.SetAttribute("idvalue", iRecordId.ToString());
						oBadRecord.SetAttribute("textcolumn", sTextColumn);
						oBadRecord.SetAttribute("htmltextcolumn", sHtmlTextColumn);
                        oBadRecord.SetAttribute("updated", "0");
						oParentNode.AppendChild(oBadRecord);

						iBadRecords++;
						showMessage(iBadRecords.ToString() + " bad records found.", false);
					}
					oReader.Close();
				}
			}
			catch(Exception ex)
			{
				showMessage("Error during scan. " + ex.Message, true );
				bError = true;
			}
			finally
			{
				if( oReader != null )
					oReader.Close();
			}

			//Save the result to file
			oBadRecordsXml.Save(m_sFilePath + m_ResultXmlFile);

			if( !bError )
			{
				showMessage("Done scanning. Total " + iBadRecords.ToString() + " bad records found.", false);
			}
			
		}

		/// <summary>
		/// Remove the bad ascii characters found in the scan step
		/// </summary>
		public void ReplaceText()
		{
			XmlDocument oBadRecordsXml = new XmlDocument();
			XmlNodeList oRecordNodeList = null;
			string sSQL = string.Empty;
			DbReader oReader = null;
			string sText = string.Empty;
			string sHtmlText = string.Empty;
			DbWriter objDbWriter = null;
			int iRecordsNum = 0;
			bool bError = false;

			//Retrive the comments and remove the bad ascii character
			try
			{
				oBadRecordsXml.Load( m_sFilePath + m_ResultXmlFile);
				oRecordNodeList = oBadRecordsXml.SelectNodes("//records/record[@updated='0']");
				foreach(XmlNode oRecordNode in oRecordNodeList)
				{
					string sTableName = oRecordNode.Attributes["tablename"].Value;
					string sIDValue = oRecordNode.Attributes["idvalue"].Value;
					string sIDColumn = oRecordNode.Attributes["idcolumn"].Value;
					string sTextColumn = oRecordNode.Attributes["textcolumn"].Value;
					string sHtmlTextColumn = oRecordNode.Attributes["htmltextcolumn"].Value;

                    sSQL = "SELECT ";

                    string sSelectList = string.Empty;
                    if(sTextColumn.Length > 0)
                        sSelectList += sTextColumn;

                    if (sHtmlTextColumn.Length > 0)
                    {
                        if (!string.IsNullOrEmpty(sSelectList))
                            sSelectList += ",";
                        sSQL += sHtmlTextColumn;
                    }
					sSQL += " FROM " + sTableName + " WHERE " + sIDColumn + "=" + sIDValue;
					oReader = DbFactory.GetDbReader(m_sConnectionString, sSQL );
					if( oReader.Read() )
					{
                        if (sTextColumn.Length > 0)
                        {
                            sText = oReader.GetString(sTextColumn);
                            sText = sText.Replace(m_sOriginal, m_sNew);
                        }

						if( sHtmlTextColumn.Length > 0 )
						{
							sHtmlText = oReader.GetString(sHtmlTextColumn);
							sHtmlText = sHtmlText.Replace(m_sOriginal, m_sNew);
						}

						//Update the database record
						objDbWriter = DbFactory.GetDbWriter(m_sConnectionString);
						objDbWriter.Tables.Add(sTableName);
                        if (sTextColumn.Length > 0)
                        {
                            objDbWriter.Fields.Add(sTextColumn, sText);
                        }
						if( sHtmlTextColumn.Length > 0 )
						{
							objDbWriter.Fields.Add(sHtmlTextColumn, sHtmlText);
						}
						objDbWriter.Where.Add(sIDColumn + " =" + sIDValue );
						objDbWriter.Execute();
                        objDbWriter = null;
						iRecordsNum++;
						showProgress("Process for " + sTableName + "." + sIDColumn + "=" + sIDValue + System.Environment.NewLine);
                        oRecordNode.Attributes["updated"].Value = "1";
                    }
					oReader.Close();
				}
                oBadRecordsXml.Save(m_sFilePath + m_ResultXmlFile);
			}
			catch(Exception e)
			{
				showMessage("Error in removing bad ascii. " + e.Message, true);
				bError = true;
			}
			finally
			{
				if( oReader != null )
					oReader.Close();
			}

			if( !bError )
			{
				showMessage("Done replacing text. Total of " + iRecordsNum.ToString() + " records processed.", false);
			}
		}

		public bool PauseScan
		{
			set{ m_bPauseScan = value; }
		}

        public string OriginalValue
        {
            set { m_sOriginal = value; }
        }

        public string NewValue
        {
            set { m_sNew = value; }
        }
	}
}
