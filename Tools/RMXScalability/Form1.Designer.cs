﻿namespace RMXScalability
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpBoxOption = new System.Windows.Forms.GroupBox();
            this.txtEvent = new System.Windows.Forms.TextBox();
            this.txtEmp = new System.Windows.Forms.TextBox();
            this.chkEvent = new System.Windows.Forms.CheckBox();
            this.chkEmp = new System.Windows.Forms.CheckBox();
            this.txtNonOcc = new System.Windows.Forms.TextBox();
            this.txtVA = new System.Windows.Forms.TextBox();
            this.txtPC = new System.Windows.Forms.TextBox();
            this.chkNonOccc = new System.Windows.Forms.CheckBox();
            this.chkVA = new System.Windows.Forms.CheckBox();
            this.chkPC = new System.Windows.Forms.CheckBox();
            this.txtGC = new System.Windows.Forms.TextBox();
            this.chkGC = new System.Windows.Forms.CheckBox();
            this.txtWC = new System.Windows.Forms.TextBox();
            this.chkWC = new System.Windows.Forms.CheckBox();
            this.gpbDateRange = new System.Windows.Forms.GroupBox();
            this.dtpToDate = new System.Windows.Forms.DateTimePicker();
            this.lblFromDate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnViewReport = new System.Windows.Forms.Button();
            this.lblStatusBar = new System.Windows.Forms.Label();
            this.pBar = new System.Windows.Forms.ProgressBar();
            this.grpBoxOption.SuspendLayout();
            this.gpbDateRange.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpBoxOption
            // 
            this.grpBoxOption.Controls.Add(this.txtEvent);
            this.grpBoxOption.Controls.Add(this.txtEmp);
            this.grpBoxOption.Controls.Add(this.chkEvent);
            this.grpBoxOption.Controls.Add(this.chkEmp);
            this.grpBoxOption.Controls.Add(this.txtNonOcc);
            this.grpBoxOption.Controls.Add(this.txtVA);
            this.grpBoxOption.Controls.Add(this.txtPC);
            this.grpBoxOption.Controls.Add(this.chkNonOccc);
            this.grpBoxOption.Controls.Add(this.chkVA);
            this.grpBoxOption.Controls.Add(this.chkPC);
            this.grpBoxOption.Controls.Add(this.txtGC);
            this.grpBoxOption.Controls.Add(this.chkGC);
            this.grpBoxOption.Controls.Add(this.txtWC);
            this.grpBoxOption.Controls.Add(this.chkWC);
            this.grpBoxOption.Controls.Add(this.gpbDateRange);
            this.grpBoxOption.Controls.Add(this.panel1);
            this.grpBoxOption.Controls.Add(this.lblStatusBar);
            this.grpBoxOption.Controls.Add(this.pBar);
            this.grpBoxOption.Location = new System.Drawing.Point(23, 12);
            this.grpBoxOption.Name = "grpBoxOption";
            this.grpBoxOption.Size = new System.Drawing.Size(439, 643);
            this.grpBoxOption.TabIndex = 0;
            this.grpBoxOption.TabStop = false;
            this.grpBoxOption.Text = "Please select option and enter number of records to generate in the respective te" +
    "xtbox";
            // 
            // txtEvent
            // 
            this.txtEvent.Location = new System.Drawing.Point(280, 26);
            this.txtEvent.MaxLength = 9;
            this.txtEvent.Name = "txtEvent";
            this.txtEvent.Size = new System.Drawing.Size(100, 20);
            this.txtEvent.TabIndex = 30;
            this.txtEvent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEvent_KeyPress);
            // 
            // txtEmp
            // 
            this.txtEmp.Location = new System.Drawing.Point(280, 59);
            this.txtEmp.MaxLength = 9;
            this.txtEmp.Name = "txtEmp";
            this.txtEmp.Size = new System.Drawing.Size(100, 20);
            this.txtEmp.TabIndex = 29;
            this.txtEmp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmp_KeyPress);
            // 
            // chkEvent
            // 
            this.chkEvent.AutoSize = true;
            this.chkEvent.Location = new System.Drawing.Point(51, 29);
            this.chkEvent.Name = "chkEvent";
            this.chkEvent.Size = new System.Drawing.Size(101, 17);
            this.chkEvent.TabIndex = 28;
            this.chkEvent.Text = "Generate Event";
            this.chkEvent.UseVisualStyleBackColor = true;
            // 
            // chkEmp
            // 
            this.chkEmp.AutoSize = true;
            this.chkEmp.Location = new System.Drawing.Point(51, 62);
            this.chkEmp.Name = "chkEmp";
            this.chkEmp.Size = new System.Drawing.Size(119, 17);
            this.chkEmp.TabIndex = 27;
            this.chkEmp.Text = "Generate Employee";
            this.chkEmp.UseVisualStyleBackColor = true;
            // 
            // txtNonOcc
            // 
            this.txtNonOcc.Location = new System.Drawing.Point(279, 224);
            this.txtNonOcc.MaxLength = 9;
            this.txtNonOcc.Name = "txtNonOcc";
            this.txtNonOcc.Size = new System.Drawing.Size(100, 20);
            this.txtNonOcc.TabIndex = 26;
            this.txtNonOcc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNonOcc_KeyPress);
            // 
            // txtVA
            // 
            this.txtVA.Location = new System.Drawing.Point(278, 189);
            this.txtVA.MaxLength = 9;
            this.txtVA.Name = "txtVA";
            this.txtVA.Size = new System.Drawing.Size(100, 20);
            this.txtVA.TabIndex = 25;
            this.txtVA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVA_KeyPress);
            // 
            // txtPC
            // 
            this.txtPC.Location = new System.Drawing.Point(278, 153);
            this.txtPC.MaxLength = 9;
            this.txtPC.Name = "txtPC";
            this.txtPC.Size = new System.Drawing.Size(100, 20);
            this.txtPC.TabIndex = 24;
            this.txtPC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPC_KeyPress);
            // 
            // chkNonOccc
            // 
            this.chkNonOccc.AutoSize = true;
            this.chkNonOccc.Location = new System.Drawing.Point(50, 224);
            this.chkNonOccc.Name = "chkNonOccc";
            this.chkNonOccc.Size = new System.Drawing.Size(149, 17);
            this.chkNonOccc.TabIndex = 23;
            this.chkNonOccc.Text = "Generate Non-Occ Claims";
            this.chkNonOccc.UseVisualStyleBackColor = true;
            // 
            // chkVA
            // 
            this.chkVA.AutoSize = true;
            this.chkVA.Location = new System.Drawing.Point(50, 189);
            this.chkVA.Name = "chkVA";
            this.chkVA.Size = new System.Drawing.Size(186, 17);
            this.chkVA.TabIndex = 22;
            this.chkVA.Text = "Generate Vehicle Accident Claims";
            this.chkVA.UseVisualStyleBackColor = true;
            // 
            // chkPC
            // 
            this.chkPC.AutoSize = true;
            this.chkPC.Location = new System.Drawing.Point(50, 157);
            this.chkPC.Name = "chkPC";
            this.chkPC.Size = new System.Drawing.Size(145, 17);
            this.chkPC.TabIndex = 21;
            this.chkPC.Text = "Generate Property Claims";
            this.chkPC.UseVisualStyleBackColor = true;
            // 
            // txtGC
            // 
            this.txtGC.Location = new System.Drawing.Point(279, 121);
            this.txtGC.MaxLength = 9;
            this.txtGC.Name = "txtGC";
            this.txtGC.Size = new System.Drawing.Size(100, 20);
            this.txtGC.TabIndex = 20;
            this.txtGC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGC_KeyPress);
            // 
            // chkGC
            // 
            this.chkGC.AutoSize = true;
            this.chkGC.Location = new System.Drawing.Point(50, 124);
            this.chkGC.Name = "chkGC";
            this.chkGC.Size = new System.Drawing.Size(143, 17);
            this.chkGC.TabIndex = 19;
            this.chkGC.Text = "Generate General Claims";
            this.chkGC.UseVisualStyleBackColor = true;
            // 
            // txtWC
            // 
            this.txtWC.Location = new System.Drawing.Point(279, 91);
            this.txtWC.MaxLength = 9;
            this.txtWC.Name = "txtWC";
            this.txtWC.Size = new System.Drawing.Size(100, 20);
            this.txtWC.TabIndex = 18;
            this.txtWC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWC_KeyPress);
            // 
            // chkWC
            // 
            this.chkWC.AutoSize = true;
            this.chkWC.Location = new System.Drawing.Point(50, 91);
            this.chkWC.Name = "chkWC";
            this.chkWC.Size = new System.Drawing.Size(213, 17);
            this.chkWC.TabIndex = 17;
            this.chkWC.Text = "Generate Worker\'s Compensation Claim";
            this.chkWC.UseVisualStyleBackColor = true;
            // 
            // gpbDateRange
            // 
            this.gpbDateRange.Controls.Add(this.dtpToDate);
            this.gpbDateRange.Controls.Add(this.lblFromDate);
            this.gpbDateRange.Controls.Add(this.label2);
            this.gpbDateRange.Controls.Add(this.dtpFromDate);
            this.gpbDateRange.Location = new System.Drawing.Point(27, 261);
            this.gpbDateRange.Name = "gpbDateRange";
            this.gpbDateRange.Size = new System.Drawing.Size(385, 67);
            this.gpbDateRange.TabIndex = 14;
            this.gpbDateRange.TabStop = false;
            this.gpbDateRange.Text = "Select date range for claim";
            // 
            // dtpToDate
            // 
            this.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpToDate.Location = new System.Drawing.Point(266, 28);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.Size = new System.Drawing.Size(89, 20);
            this.dtpToDate.TabIndex = 3;
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            this.lblFromDate.Location = new System.Drawing.Point(19, 31);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(62, 13);
            this.lblFromDate.TabIndex = 0;
            this.lblFromDate.Text = "From Date :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(212, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "To Date :";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFromDate.Location = new System.Drawing.Point(87, 28);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(89, 20);
            this.dtpFromDate.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnGenerate);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.btnViewReport);
            this.panel1.Location = new System.Drawing.Point(27, 572);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(385, 65);
            this.panel1.TabIndex = 13;
            // 
            // btnGenerate
            // 
            this.btnGenerate.Enabled = false;
            this.btnGenerate.Location = new System.Drawing.Point(15, 21);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(75, 23);
            this.btnGenerate.TabIndex = 15;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // btnClose
            // 
            this.btnClose.Enabled = false;
            this.btnClose.Location = new System.Drawing.Point(278, 21);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(91, 23);
            this.btnClose.TabIndex = 18;
            this.btnClose.Text = "Close";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnViewReport
            // 
            this.btnViewReport.Enabled = false;
            this.btnViewReport.Location = new System.Drawing.Point(148, 21);
            this.btnViewReport.Name = "btnViewReport";
            this.btnViewReport.Size = new System.Drawing.Size(75, 23);
            this.btnViewReport.TabIndex = 16;
            this.btnViewReport.Text = "View Report";
            this.btnViewReport.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnViewReport.UseVisualStyleBackColor = true;
            this.btnViewReport.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // lblStatusBar
            // 
            this.lblStatusBar.AutoSize = true;
            this.lblStatusBar.Location = new System.Drawing.Point(24, 385);
            this.lblStatusBar.Name = "lblStatusBar";
            this.lblStatusBar.Size = new System.Drawing.Size(13, 13);
            this.lblStatusBar.TabIndex = 12;
            this.lblStatusBar.Text = "1";
            // 
            // pBar
            // 
            this.pBar.Location = new System.Drawing.Point(27, 346);
            this.pBar.Name = "pBar";
            this.pBar.Size = new System.Drawing.Size(385, 23);
            this.pBar.TabIndex = 11;
            this.pBar.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 667);
            this.Controls.Add(this.grpBoxOption);
            this.Name = "Form1";
            this.Text = "RMX Random Data Generator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpBoxOption.ResumeLayout(false);
            this.grpBoxOption.PerformLayout();
            this.gpbDateRange.ResumeLayout(false);
            this.gpbDateRange.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpBoxOption;
        private System.Windows.Forms.Label lblStatusBar;
        private System.Windows.Forms.ProgressBar pBar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnViewReport;
        private System.Windows.Forms.Label lblFromDate;
        private System.Windows.Forms.DateTimePicker dtpToDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.GroupBox gpbDateRange;
        private System.Windows.Forms.CheckBox chkNonOccc;
        private System.Windows.Forms.CheckBox chkVA;
        private System.Windows.Forms.CheckBox chkPC;
        private System.Windows.Forms.TextBox txtGC;
        private System.Windows.Forms.CheckBox chkGC;
        private System.Windows.Forms.TextBox txtWC;
        private System.Windows.Forms.CheckBox chkWC;
        private System.Windows.Forms.TextBox txtEvent;
        private System.Windows.Forms.TextBox txtEmp;
        private System.Windows.Forms.CheckBox chkEvent;
        private System.Windows.Forms.CheckBox chkEmp;
        private System.Windows.Forms.TextBox txtNonOcc;
        private System.Windows.Forms.TextBox txtVA;
        private System.Windows.Forms.TextBox txtPC;
    }
}

