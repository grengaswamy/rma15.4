
/*
CREATED BY : RUPAL KOTAK
CREATED ON : 14 APR 2011
DESC : INSERTTS A ROW IN GLOSSARY FOR PROCESSROWDETAILS TABLE
*/

use <Database Name>
GO

INSERT INTO GLOSSARY
       (   
           LINE_OF_BUS_FLAG,
           REQD_IND_TABL_FLAG
           ,TABLE_ID
           ,SYSTEM_TABLE_NAME
           ,GLOSSARY_TYPE_CODE
           ,ATTACHMENTS_FLAG        
           ,REQD_REL_TABL_FLAG
           ,NEXT_UNIQUE_ID
           ,DELETED_FLAG
        )   
     VALUES
         ( 
			 0,
			 0,
			 (SELECT MAX(TABLE_ID)+1 FROM GLOSSARY),
			 'BatchRowDetails',
			 1,
			 0,          
			 0,
			 1,
			 0
         )

GO

