/*
Created By : Rupal Kotak
Created On : 14 Apr 2011
Desc : Creates a new table named ProcessDetails in database
*/

use <Database Name>
GO

CREATE TABLE BatchDetails
(
	BatchID int NOT NULL,
	TableID int NOT NULL,
	BatchStartDate varchar(10) NULL,
	BatchStartTime varchar(10) NULL,
	BatchEndDate varchar(10) NULL,
	BatchEndTime varchar(10) NULL,
	AddedByUser nchar(10) NULL,
	ConnectionString varchar(200) NULL,
	LOB int NULL,
	Remarks varchar(300) NULL,
	NoRowsEntered int NULL,
	NoRowsCreated int NULL	
)

GO
