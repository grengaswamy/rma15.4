/*
Created By : Rupal Kotak
Created On: 14 Apr 2011
Desc : Adds a new table named [BatchRowDetails] in database

*/


--1.FIRST RUN THE QUERY


CREATE TABLE BatchRowDetails
(
	BatchRowId int NOT NULL,
	BatchId int NOT NULL,
	BatchRowNumber varchar(50) NULL,	
	EventNumber varchar(50) NULL,	
	ClaimNumber varchar(50) NULL,	
	EmpNumber varchar(50) NULL,
	ClaimantRowId int NULL,
	PiRowId int NULL
) 


--2. THEN COMMIT THE CHANGES

COMMIT