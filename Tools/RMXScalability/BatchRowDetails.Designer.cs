﻿namespace RMXScalability
{
    partial class BatchRowDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgBatchList = new System.Windows.Forms.DataGridView();
            this.BatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchStartDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchEndDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoRowsCreated = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConnString = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.User = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgBatchRowList = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBatchList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgBatchRowList)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.dgBatchRowList);
            this.groupBox1.Location = new System.Drawing.Point(29, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1083, 485);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Batch Row Details";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgBatchList);
            this.groupBox2.Location = new System.Drawing.Point(22, 30);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1041, 143);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Batch Details";
            // 
            // dgBatchList
            // 
            this.dgBatchList.AllowUserToAddRows = false;
            this.dgBatchList.AllowUserToDeleteRows = false;
            this.dgBatchList.AllowUserToOrderColumns = true;
            this.dgBatchList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBatchList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BatchID,
            this.BatchStartDate,
            this.StartTime,
            this.BatchEndDate,
            this.EndTime,
            this.dataGridViewTextBoxColumn1,
            this.LOB,
            this.dataGridViewTextBoxColumn2,
            this.NoRowsCreated,
            this.ConnString,
            this.User});
            this.dgBatchList.Location = new System.Drawing.Point(6, 19);
            this.dgBatchList.Name = "dgBatchList";
            this.dgBatchList.Size = new System.Drawing.Size(1018, 118);
            this.dgBatchList.TabIndex = 2;
            // 
            // BatchID
            // 
            this.BatchID.DataPropertyName = "BatchID";
            this.BatchID.HeaderText = "Batch Id";
            this.BatchID.Name = "BatchID";
            this.BatchID.ReadOnly = true;
            // 
            // BatchStartDate
            // 
            this.BatchStartDate.DataPropertyName = "BatchStartDate";
            this.BatchStartDate.HeaderText = "Batch Start Date";
            this.BatchStartDate.Name = "BatchStartDate";
            this.BatchStartDate.ReadOnly = true;
            // 
            // StartTime
            // 
            this.StartTime.DataPropertyName = "BatchStartTime";
            this.StartTime.HeaderText = "Start Time";
            this.StartTime.Name = "StartTime";
            this.StartTime.ReadOnly = true;
            // 
            // BatchEndDate
            // 
            this.BatchEndDate.DataPropertyName = "BatchEndDate";
            this.BatchEndDate.HeaderText = "Batch End Date";
            this.BatchEndDate.Name = "BatchEndDate";
            this.BatchEndDate.ReadOnly = true;
            // 
            // EndTime
            // 
            this.EndTime.DataPropertyName = "BatchEndTime";
            this.EndTime.HeaderText = "End Time";
            this.EndTime.Name = "EndTime";
            this.EndTime.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "SYSTEM_TABLE_NAME";
            this.dataGridViewTextBoxColumn1.HeaderText = "Module";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // LOB
            // 
            this.LOB.DataPropertyName = "LOB_DESC";
            this.LOB.HeaderText = "LOB";
            this.LOB.Name = "LOB";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "NoRowsEntered";
            this.dataGridViewTextBoxColumn2.HeaderText = "No of Rows Entered";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // NoRowsCreated
            // 
            this.NoRowsCreated.DataPropertyName = "NoRowsCreated";
            this.NoRowsCreated.HeaderText = "No of Rows Created";
            this.NoRowsCreated.Name = "NoRowsCreated";
            this.NoRowsCreated.ReadOnly = true;
            // 
            // ConnString
            // 
            this.ConnString.DataPropertyName = "ConnectionString";
            this.ConnString.HeaderText = "Conn. String";
            this.ConnString.Name = "ConnString";
            this.ConnString.ReadOnly = true;
            // 
            // User
            // 
            this.User.DataPropertyName = "AddedByUser";
            this.User.HeaderText = "User";
            this.User.Name = "User";
            this.User.ReadOnly = true;
            // 
            // dgBatchRowList
            // 
            this.dgBatchRowList.AllowUserToAddRows = false;
            this.dgBatchRowList.AllowUserToDeleteRows = false;
            this.dgBatchRowList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBatchRowList.Location = new System.Drawing.Point(22, 195);
            this.dgBatchRowList.Name = "dgBatchRowList";
            this.dgBatchRowList.Size = new System.Drawing.Size(1041, 271);
            this.dgBatchRowList.TabIndex = 1;
            // 
            // BatchRowDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1138, 509);
            this.Controls.Add(this.groupBox1);
            this.Name = "BatchRowDetails";
            this.Text = "Batch Row Details";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgBatchList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgBatchRowList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgBatchRowList;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgBatchList;
        private System.Windows.Forms.DataGridViewTextBoxColumn BatchID;
        private System.Windows.Forms.DataGridViewTextBoxColumn BatchStartDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn BatchEndDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn EndTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn LOB;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoRowsCreated;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConnString;
        private System.Windows.Forms.DataGridViewTextBoxColumn User;
    }
}