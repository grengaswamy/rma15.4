﻿namespace RMXScalability
{
    partial class BatchList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgBatchList = new System.Windows.Forms.DataGridView();
            this.BatchID = new System.Windows.Forms.DataGridViewLinkColumn();
            this.BatchStartDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BatchEndDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Module = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecordCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoRowsCreated = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConnString = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.User = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBatchList)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgBatchList);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1052, 355);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Process Batch List";
            // 
            // dgBatchList
            // 
            this.dgBatchList.AllowUserToAddRows = false;
            this.dgBatchList.AllowUserToDeleteRows = false;
            this.dgBatchList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBatchList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BatchID,
            this.BatchStartDate,
            this.StartTime,
            this.BatchEndDate,
            this.EndTime,
            this.Module,
            this.LOB,
            this.RecordCount,
            this.NoRowsCreated,
            this.ConnString,
            this.User});
            this.dgBatchList.Location = new System.Drawing.Point(21, 39);
            this.dgBatchList.Name = "dgBatchList";
            this.dgBatchList.Size = new System.Drawing.Size(1003, 289);
            this.dgBatchList.TabIndex = 1;
            this.dgBatchList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBatchList_CellContentClick);
            // 
            // BatchID
            // 
            this.BatchID.DataPropertyName = "BatchID";
            this.BatchID.HeaderText = "Batch Id";
            this.BatchID.Name = "BatchID";
            this.BatchID.ReadOnly = true;
            this.BatchID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.BatchID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // BatchStartDate
            // 
            this.BatchStartDate.DataPropertyName = "BatchStartDate";
            this.BatchStartDate.HeaderText = "Batch Start Date";
            this.BatchStartDate.Name = "BatchStartDate";
            this.BatchStartDate.ReadOnly = true;
            // 
            // StartTime
            // 
            this.StartTime.DataPropertyName = "BatchStartTime";
            this.StartTime.HeaderText = "Start Time";
            this.StartTime.Name = "StartTime";
            this.StartTime.ReadOnly = true;
            // 
            // BatchEndDate
            // 
            this.BatchEndDate.DataPropertyName = "BatchEndDate";
            this.BatchEndDate.HeaderText = "Batch End Date";
            this.BatchEndDate.Name = "BatchEndDate";
            this.BatchEndDate.ReadOnly = true;
            // 
            // EndTime
            // 
            this.EndTime.DataPropertyName = "BatchEndTime";
            this.EndTime.HeaderText = "End Time";
            this.EndTime.Name = "EndTime";
            this.EndTime.ReadOnly = true;
            this.EndTime.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Module
            // 
            this.Module.DataPropertyName = "SYSTEM_TABLE_NAME";
            this.Module.HeaderText = "Module";
            this.Module.Name = "Module";
            this.Module.ReadOnly = true;
            // 
            // LOB
            // 
            this.LOB.DataPropertyName = "LOB_DESC";
            this.LOB.HeaderText = "LOB";
            this.LOB.Name = "LOB";
            // 
            // RecordCount
            // 
            this.RecordCount.DataPropertyName = "NoRowsEntered";
            this.RecordCount.HeaderText = "No of Rows Enterd";
            this.RecordCount.Name = "RecordCount";
            this.RecordCount.ReadOnly = true;
            // 
            // NoRowsCreated
            // 
            this.NoRowsCreated.DataPropertyName = "NoRowsCreated";
            this.NoRowsCreated.HeaderText = "No of Rows Created";
            this.NoRowsCreated.Name = "NoRowsCreated";
            this.NoRowsCreated.ReadOnly = true;
            // 
            // ConnString
            // 
            this.ConnString.DataPropertyName = "ConnectionString";
            this.ConnString.HeaderText = "Conn. String";
            this.ConnString.Name = "ConnString";
            this.ConnString.ReadOnly = true;
            // 
            // User
            // 
            this.User.DataPropertyName = "AddedByUser";
            this.User.HeaderText = "User";
            this.User.Name = "User";
            this.User.ReadOnly = true;
            // 
            // BatchList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1076, 379);
            this.Controls.Add(this.groupBox1);
            this.Name = "BatchList";
            this.Text = "Process Batch List";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgBatchList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgBatchList;
        private System.Windows.Forms.DataGridViewLinkColumn BatchID;
        private System.Windows.Forms.DataGridViewTextBoxColumn BatchStartDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn BatchEndDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn EndTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Module;
        private System.Windows.Forms.DataGridViewTextBoxColumn LOB;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecordCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoRowsCreated;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConnString;
        private System.Windows.Forms.DataGridViewTextBoxColumn User;

    }
}