﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Resources;
using System.Configuration;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Diagnostics;
using System.IO;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using Riskmaster.Application.CodesList;

namespace RMXScalability
{
    class AppHelper
    {

        //DBHelper objDBHelper = DBHelper.GetDBHelperInstance();

        private string sExceptionKey = "AppHelper.Error";

        public bool ValidateAppConfigSetting()
        {
            try
            {
                bool bValid = true;

                List<string> lstErrMsg = new List<string>();
                /*
                if (ConfigurationSettings.AppSettings["UserID"] == null)
                {
                    lstErrMsg.Add(Globalization.GetString("Appsetting.UserID.Key.Missing"));
                    bValid = false;
                }
                if (ConfigurationSettings.AppSettings["Password"] == null)
                {
                    lstErrMsg.Add(Globalization.GetString("Appsetting.Password.Key.Missing"));
                    bValid = false;
                }
                if (ConfigurationSettings.AppSettings["DataSourceName"] == null)
                {
                    lstErrMsg.Add(Globalization.GetString("Appsetting.DataSourceName.Key.Missing"));
                    bValid = false;
                }
                if (ConfigurationSettings.AppSettings["DeptCount"] == null)
                {
                    lstErrMsg.Add(Globalization.GetString("Appsetting.EmpNameSeprator.Key.Missing"));
                    bValid = false;
                }
                if (ConfigurationSettings.AppSettings["EmployeeNameFilePath"] == null)
                {
                    lstErrMsg.Add(Globalization.GetString("Appsetting.EmployeeNameFilePath.Key.Missing"));
                    bValid = false;
                }
                if (ConfigurationSettings.AppSettings["EmpNameSeprator"] == null)
                {
                    lstErrMsg.Add(Globalization.GetString("Appsetting.EmpNameSeprator.Key.Missing"));
                    bValid = false;
                }*/
                if (ConfigurationSettings.AppSettings["UseSupportiveChildEntitiesForHowManyRecords"] == null)
                {
                    lstErrMsg.Add(Globalization.GetString("Appsetting.WCClaimPerEmployee.Key.Missing"));
                    bValid = false;
                }
                /*
                if (ConfigurationSettings.AppSettings["ErrorLogLocation"] == null)
                {
                    lstErrMsg.Add(Globalization.GetString("Appsetting.ErrorLogLocation.Key.Missing"));
                    bValid = false;
                }
                */
                if (bValid)
                {
                    System.Collections.Specialized.NameValueCollection obj = ConfigurationSettings.AppSettings;
                    string[] strKeyArray = obj.AllKeys;

                    string ResourceKey = "";
                    for (int i = 0; i < strKeyArray.Length; i++)
                    {
                        if (ConfigurationSettings.AppSettings[strKeyArray[i]].ToString() == "")
                        {
                            ResourceKey = "AppSetting." + strKeyArray[i] + ".Missing";
                            lstErrMsg.Add(Globalization.GetString(ResourceKey));
                            bValid = false;
                        }
                    }
                }
                if (!bValid)
                    ShowMsgBox(lstErrMsg, "Error");
                return bValid;
            }
            catch (Exception ex)
            {
                throw new RMAppException(GetErrorMessage(ex, sExceptionKey, "ValidateAppConfigSetting"), ex);
            }
        }

        public bool ValidateEmpNameSeparator()
        {
            try
            {
                string sEmpNameSeprator = ConfigurationSettings.AppSettings["EmpNameSeprator"].ToString();
                string ResourceKey = "";
                if (sEmpNameSeprator == "")
                {
                    ResourceKey = "AppSetting.EmpNameSeprator";
                    MessageBox.Show(Globalization.GetString(ResourceKey),"Error");
                    return false;
                }
                else if (sEmpNameSeprator.Length > 1)
                {
                    ResourceKey = "AppSetting.EmpNameSeprator.Length";
                    MessageBox.Show(Globalization.GetString(ResourceKey),"Error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new RMAppException(GetErrorMessage(ex, sExceptionKey, "ValidateEmpNameSeparator"), ex);
            }
        }

        public bool ValidateErrorLogLocation()
        {
            try
            {
                string ResourceKey = "";
                string sErrorLogLocation = ConfigurationSettings.AppSettings["ErrorLogLocation"].ToString();
                if (!Directory.Exists(sErrorLogLocation))
                {
                    ResourceKey = "AppSetting.ErrorLogLocation.NotFound";
                    MessageBox.Show(Globalization.GetString(ResourceKey),"Error");
                    return false;
                }
                 
                else
                {
                    try
                    {
                        string sFileName = "ErrorLog_Test.txt"; 
                        string scompletePath = sErrorLogLocation + "\\" + sFileName;                        
                        string[] lines = new string[5];
                        lines[0] = "Test";
                        File.WriteAllLines(scompletePath, lines);                        
                    }
                    catch (Exception ex)
                    {
                        ResourceKey = "AppSetting.ErrorLogLocation.WriteError";
                        MessageBox.Show(Globalization.GetString(ResourceKey),"Error");
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex1)
            {
                throw new RMAppException(GetErrorMessage(ex1, sExceptionKey, "ValidateErrorLogLocation"), ex1);
            }
           
        }

        public bool ValidateDepartmentCount()
        {
            try
            {
                string sDeptCount = ConfigurationSettings.AppSettings["DeptCount"].ToString();
                if (!IsNumeric(sDeptCount))
                {
                    string ResourceKey = "AppSetting.DeptCount.Numeric";
                    MessageBox.Show(Globalization.GetString(ResourceKey),"Error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new RMAppException(GetErrorMessage(ex, sExceptionKey, "ValidateDepartmentCount"), ex);
            }
        }

        public bool ValidateWCClaimPerEmployee()
        {
            try
            {
                string sWCClaimPerEmployee = ConfigurationSettings.AppSettings["UseSupportiveChildEntitiesForHowManyRecords"].ToString();
                string s = ConfigurationSettings.AppSettings["UseSupportiveChildEntitiesForHowManyRecords"].ToString();
                if (!IsNumeric(sWCClaimPerEmployee))
                {
                    string ResourceKey = "AppSetting.WCClaimPerEmployee.Numeric";
                    MessageBox.Show(Globalization.GetString(ResourceKey),"Error");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new RMAppException(GetErrorMessage(ex, sExceptionKey, "ValidateWCClaimPerEmployee"), ex);
            }
        }

        public bool IsNumeric(string sInput)
        {
            try
            {
                bool bNumeric = true;
                string sAlphabets = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
                for (int i = 0; i < sInput.Length; i++)
                {
                    if (sAlphabets.Contains(sInput[i]))
                    {
                        bNumeric = false;
                        break;
                    }
                }
                return bNumeric;
            }
            catch (Exception ex)
            {
                throw new RMAppException(GetErrorMessage(ex, sExceptionKey, "IsNumeric"), ex);
            }
        }

        public void ShowMsgBox(List<string> lstMsg, string sCaption)
        {
            string sErrorMessage = "";

            for (int i = 0; i < lstMsg.Count(); i++)
            {
                if (sErrorMessage == "")
                    sErrorMessage = (i + 1).ToString() + ". " + lstMsg[i];
                else
                    sErrorMessage = sErrorMessage + "\n" + (i + 1).ToString() + ". " + lstMsg[i];
            }           
            MessageBox.Show(sErrorMessage, sCaption);
        }

        public void SaveExceptionToDisk(Exception ex)
        {
            try
            {
                //string sFolderPath = ConfigurationSettings.AppSettings["ErrorLogLocation"].ToString();
                string sFolderPath = AppDomain.CurrentDomain.BaseDirectory + "\\" + "RMXScaleLog";
                if(!Directory.Exists(sFolderPath))
                    Directory.CreateDirectory(sFolderPath);
                string sFileName = "ErrorLog_" + System.DateTime.Now.Year.ToString() + "-" + System.DateTime.Now.Month.ToString() + "-" + System.DateTime.Now.Day.ToString() + ".txt";
                string scompletePath = sFolderPath + "\\" + sFileName;
                StringBuilder lines = new StringBuilder();
                StackTrace st = new StackTrace(ex, true);
                //StackFrame[] frames = st.GetFrames();
                // Iterate over the frames extracting the information you need        
                StackFrame frame = st.GetFrame(0);
                StackFrame frame1 = st.GetFrame(st.FrameCount - 1);
                lines.AppendLine( "Error Occured On : " + System.DateTime.Now.ToShortDateString() + " " + System.DateTime.Now.ToShortTimeString());
                lines.AppendLine("Error Log :");
                lines.AppendLine("File Name : " + frame.GetFileName());
                lines.AppendLine("Method Name : " + frame.GetMethod().Name);
                lines.AppendLine("Line Number : " + frame.GetFileLineNumber().ToString());
                lines.AppendLine("Detailed Error Message : " + ex.Message);
                lines.AppendLine("---------------------------------------------------------------------");
                lines.AppendLine("     ");
                File.AppendAllText(scompletePath, lines.ToString());
            }
            catch (Exception exc)
            {
                //do nothing
            }
        }

        public string GetConfigSetting(string Key)
        {
            try
            {
                return ConfigurationSettings.AppSettings[Key].ToString();
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(GetErrorMessage(ex, sExceptionKey, "GetConfigSetting"), ex);
            }
        }

        public string GetErrorMessage(Exception ex, string sExceptionKey, string sMethodName)
        {
            try
            {
                return Globalization.GetString(sExceptionKey) + sMethodName + "'. Error message - " + ex.Message;
            }
            catch (Exception ex1)
            {
            }
            //should not come here in normal conditions
            return "";
        }

        public int GetTableID(string sTableName)
        {
            int iTableID = 0;
            try
            {
                //using (Entity objEntity = (Entity)(objDBHelper.objDataModelFactory.GetDataModelObject("Entity", false)))
                using (Entity objEntity = (Entity)(DBHelper.oDataModelFactory.GetDataModelObject("Entity", false)))
                {
                    iTableID = objEntity.Context.LocalCache.GetTableId(sTableName);
                }
                if (iTableID == 0)
                {
                    string sErrMsg = Globalization.GetString("TableName.Error");
                    sErrMsg = sErrMsg + " Table Name Supplied : " + sTableName;
                    throw new RMAppException(sErrMsg);
                }
            }
            catch (RMAppException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new RMAppException(GetErrorMessage(ex, sExceptionKey, "GetTableID"), ex);
            }
            return iTableID;
        }
    }
}
