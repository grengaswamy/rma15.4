﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Security;
using Riskmaster.DataModel;
using Riskmaster.Application.CodesList;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace RMXScalability
{
    sealed class DBHelper
    {

        /// <summary>
        /// this class contains only those feilds which are intialised at application start and will remail same through out the appliation
        /// </summary>

        //below feilds are initialized in home screen

        public static UserLogin oUserLogin;
        public static string sUserName = "";
        public static string sPassword = "";
        public static string sDataSourceName = "";
        public static string sConnectionString = "";
        public static DataModelFactory oDataModelFactory;
        public static CodesListManager oCodeListManager = null;
        public static bool bSilentMode = false;       
    }
}
