﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace RMXScalability
{

    public partial class BatchRowDetails : Form
    {
        //DBHelper objDBHelper = DBHelper.GetDBHelperInstance();
        AppHelper objAppHelper = new AppHelper();
        int iProcessID = 0;

        private BatchRowDetails()
        {
        }

        public BatchRowDetails(int ProcessID)
        {
            InitializeComponent();
            //MessageBox.Show("Depending on the amount of data, this may take a while to load. Please be patient", "Information");
            iProcessID = ProcessID;
            BindBatchGrid();
            BindBatchRowGrid();
        }

        private void BindBatchGrid()
        {
            try
            {
                //string sSQL = "select g.SYSTEM_TABLE_NAME,p.* from ProcessDetails p inner join GLOSSARY g on p.TableID=g.TABLE_ID where ProcessID =" + iProcessID + " order by ProcessID desc";
                string sSQL = "select g.SYSTEM_TABLE_NAME,p.*,CODES_TEXT.CODE_DESC LOB_DESC from BatchDetails p inner join GLOSSARY g on p.TableID=g.TABLE_ID left join CODES_TEXT on p.LOB=CODES_TEXT.CODE_ID where BatchID =" + iProcessID + "  order by BatchID desc";
                DataSet ds = DbFactory.GetDataSet(DBHelper.sConnectionString, sSQL);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        dgBatchList.AutoGenerateColumns = false;
                        dgBatchList.DataSource = ds.Tables[0];
                    }
                }
            }
            catch (RMAppException ex)
            {
                MessageBox.Show(ex.Message, "error");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error");
            }
        }

        private void BindBatchRowGrid()
        {
            try
            {                
                //string sSQL = "select PRowID,PRowNumber from ProcessRowDetails where ProcessID = " + iProcessID + "  order by ProcessRowID desc";
                string sSQL = "select * from BatchRowDetails where BatchId = " + iProcessID + "  order by BatchRowId desc";
                DataSet ds = DbFactory.GetDataSet(DBHelper.sConnectionString, sSQL);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        dgBatchRowList.AutoGenerateColumns = true;
                        dgBatchRowList.DataSource = ds.Tables[0];
                    }
                }
            }
            catch (RMAppException ex)
            {
                MessageBox.Show(ex.Message, "error");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error");
            }

        }
    }
}
