﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Security;

namespace RMXScalability
{
    public partial class Home : Form
    {        
        AppHelper objHelper = new AppHelper();
        Login m_objLogin = new Login();

        public Home()
        {
            InitializeComponent();
        }

        private void Home_Load(object sender, EventArgs e)
        {
            try
            {

                Boolean bCon = false;

                if (!DBHelper.bSilentMode)
                {

                    bCon = m_objLogin.RegisterApplication(this.Handle.ToInt32(), 0, ref DBHelper.oUserLogin);
                }
                if (!DBHelper.bSilentMode && !bCon)
                {
                    // for invalid Login
                    if (m_objLogin != null) m_objLogin.Dispose();
                    MessageBox.Show("Invalid Login","Login Failed");
                    Application.Exit();
                }
                else
                {
                    if (DBHelper.oUserLogin.objRiskmasterDatabase.DataSourceName == null || DBHelper.oUserLogin.objRiskmasterDatabase.DataSourceName == "")
                    {
                        MessageBox.Show("User is not authenticated for given data source name.", "Login Failed");
                        Application.Exit();
                    }
                    else
                    {
                        DBHelper.sUserName = DBHelper.oUserLogin.LoginName;
                        DBHelper.sPassword = DBHelper.oUserLogin.Password;
                        DBHelper.sDataSourceName = DBHelper.oUserLogin.objRiskmasterDatabase.DataSourceName;
                        DBHelper.sConnectionString = DBHelper.oUserLogin.objRiskmasterDatabase.ConnectionString;
                        DBHelper.oDataModelFactory = new Riskmaster.DataModel.DataModelFactory(DBHelper.oUserLogin);
                        DBHelper.oCodeListManager = new Riskmaster.Application.CodesList.CodesListManager(DBHelper.sConnectionString);
                    }
                    //enable only if user authentication was successful 
                    btnGenerateData.Enabled = true;
                    btnViewReport.Enabled = true;
                    btnExit.Enabled = true;
                    pnlButton.Enabled = true;
                }
            }
            catch (RMAppException ex)
            {
                btnGenerateData.Enabled = false;
                btnViewReport.Enabled = false;
                btnExit.Enabled = true;
                MessageBox.Show(ex.Message, "Error");
                objHelper.SaveExceptionToDisk(ex);
            }
            catch (Exception ex)
            {
                btnGenerateData.Enabled = false;
                btnViewReport.Enabled = false;
                btnExit.Enabled = true;
                MessageBox.Show(ex.Message, "Error");
                objHelper.SaveExceptionToDisk(ex);
            }
        }       

        private void btnGenerateData_Click(object sender, EventArgs e)
        {
            //Application.Run(new Form1());
            try
            {
                Form1 objForm1 = new Form1();
                objForm1.ShowDialog();
            }
            catch (Exception ex)
            {
                btnGenerateData.Enabled = false;
                MessageBox.Show(ex.Message, "Error");
                objHelper.SaveExceptionToDisk(ex);
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            //objDBHelper.Dispose();
            Application.Exit();
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            //Application.Run(new Form1());
            try
            {                
                BatchList objBatchList = new BatchList();
                objBatchList.ShowDialog();                
            }
            catch (Exception ex)
            {
                btnGenerateData.Enabled = false;
                MessageBox.Show(ex.Message, "Error");
                objHelper.SaveExceptionToDisk(ex);
            }
        }

        private void Home_FormClosing(object sender, FormClosingEventArgs e)
        {
            //objDBHelper.Dispose();
        }

       
    }
}
