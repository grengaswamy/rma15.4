﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace RMXScalability
{
    public partial class BatchList : Form
    {
        //DBHelper objDBHelper = DBHelper.GetDBHelperInstance();
        AppHelper objAppHelper = new AppHelper();

        public BatchList()
        {
            InitializeComponent();
            //MessageBox.Show("Depending on the amount of data, this may take a while to load. Please be patient", "Information");
            BindBatchGrid();
        }

        public BatchList(List<int> lstProcessIds)
        {
            InitializeComponent();            
            
            BindBatchGrid(lstProcessIds);            
        }

        private void BindBatchGrid()
        {
            try
            {   
                //string sSQL = "select g.SYSTEM_TABLE_NAME,p.* from ProcessDetails p inner join GLOSSARY g on p.TableID=g.TABLE_ID order by ProcessID desc";
                string sSQL = "select g.SYSTEM_TABLE_NAME,p.*,CODES_TEXT.CODE_DESC LOB_DESC from BatchDetails p inner join GLOSSARY g on p.TableID=g.TABLE_ID left join CODES_TEXT on p.LOB=CODES_TEXT.CODE_ID  order by BatchID desc";
                DataSet ds = DbFactory.GetDataSet(DBHelper.sConnectionString, sSQL);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        dgBatchList.AutoGenerateColumns = false;
                        dgBatchList.DataSource = ds.Tables[0];
                    }
                    
                }
                
            }
            catch (RMAppException ex)
            {
                MessageBox.Show(ex.Message, "error");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error");
            }
             
        }

        private void BindBatchGrid(List<int> lstProcessIds)
        {
            try
            {
                //string sSQL = "select g.SYSTEM_TABLE_NAME,p.* from ProcessDetails p inner join GLOSSARY g on p.TableID=g.TABLE_ID where ProcessID =" + iProcessID + " order by ProcessID desc";
                StringBuilder sWhereInClause = new StringBuilder();
                sWhereInClause.Append("(");
                foreach (int iBatchId in lstProcessIds)
                {
                    sWhereInClause.Append(iBatchId.ToString() + ",");
                }
                sWhereInClause.Remove(sWhereInClause.Length - 1, 1);
                sWhereInClause.Append(")");
                string sSQL = "select g.SYSTEM_TABLE_NAME,p.*,CODES_TEXT.CODE_DESC LOB_DESC from BatchDetails p inner join GLOSSARY g on p.TableID=g.TABLE_ID left join CODES_TEXT on p.LOB=CODES_TEXT.CODE_ID where BatchID in" + sWhereInClause.ToString() + "  order by BatchID desc";
                DataSet ds = DbFactory.GetDataSet(DBHelper.sConnectionString, sSQL);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        dgBatchList.AutoGenerateColumns = false;
                        dgBatchList.DataSource = ds.Tables[0];
                    }
                }
            }
            catch (RMAppException ex)
            {
                MessageBox.Show(ex.Message, "error");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error");
            }
        }

        void dgBatchList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (this.dgBatchList.Columns[e.ColumnIndex] is DataGridViewLinkColumn)
                {
                    string link = this.dgBatchList[e.ColumnIndex, e.RowIndex].Value.ToString();
                    bool bconverted = false;
                    BatchRowDetails frmBatchRowDetails = new BatchRowDetails(Conversion.CastToType<int>(link, out bconverted));                    
                    frmBatchRowDetails.ShowDialog();                    
                }
            }
            catch (RMAppException ex)
            {
                MessageBox.Show(ex.Message, "error");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "error");
            }


        }


    }
}
