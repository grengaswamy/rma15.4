REM List out the Java Certificate Key Stores
%1 -list -keystore %2 -storepass %3

REM BSB 05.15.2006 Add Local Test Only CA's to the cacerts java 
keystore:
%1 -import -alias %4 -file %5 -keystore %2 -storepass %3
%1 -import -alias %6 -file %7 -keystore %2 -storepass %3
