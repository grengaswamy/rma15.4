/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 05/19/2010 | SIW360  |    AS      | Match .NET xml object with XC object
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using CCP.Common;

namespace CCP.XmlFormatting
{
    public class XMLControls
    {
        private XML m_xml;
        public XMLControls()
        {
            //SIW485 XMLGlobals.gControl = this;
            XMLGlobals.lClmId = 0;
            XMLGlobals.lPolId = 0;
            XMLGlobals.lUntId = 0;
            XMLGlobals.lInjId = 0;
            XMLGlobals.lInvId = 0;
            XMLGlobals.lLDId = 0;
            XMLGlobals.lLitId = 0;
            XMLGlobals.lSubId = 0;
            XMLGlobals.lArbId = 0;
            XMLGlobals.lTRGId = 0;
            XMLGlobals.lSLOGId = 0;
            XMLGlobals.lPIId = 0; // Start SIW360
            XMLGlobals.lLSDId = 0;
            XMLGlobals.lRSDId = 0;
            XMLGlobals.lARID = 0;
            XMLGlobals.lCRID = 0; // End SIW360
        }

		//Start SIW485
        //~XMLControls()
        //{
        //    XMLGlobals.gControl = null;
        //}
		//End SIW485

        public Debug Debug
        {
            get
            {
                return XML.Debug;
            }
            set
            {
                XML.Debug = value;   
            }
        }

        public Errors Errors
        {
            get
            {
                return Debug.Errors;
            }
        }

        public XML XML
        {
            get
            {
                if (m_xml == null)
                    m_xml = new XML();
                return m_xml;
            }
            set
            {
                if (m_xml != null && value != null && m_xml.guid == value.guid)
                    return;
                m_xml = null;
                m_xml = value;
                if (m_xml == null)
                    m_xml = new XML();
            }
        }
    }
}
