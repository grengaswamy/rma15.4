/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 01/27/2009 | SIW139  |    JTC     | Field blanking with [b]
 * 04/10/2009 | SIW183  |    NDB     | Serialize/De-Serialize/Non-Static XML Document functionality
 *                                   | (Major change)
 * 04/15/2010 | SIW371  |    AS      | Update in SerializeNode function for boolean / entity fields
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 * 11/13/2011 | SIW7537 |    AS      | Update in SerializeNode function for boolean / entity fields
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;
using CCP.Common;
using CCP.Constants;

namespace CCP.XmlFormatting
{
    public abstract class XMLXCNodeBase : XMLACNode   //SIW529
    {
        //Provides an abstract class with functionality common to most classes in XmlComponents.
        //SIW529 private XML m_xml = null;
        //SIW529 private bool m_UseSharedXML = true;
        private string m_NodeName = String.Empty;
        //Start SIW485
        protected XMLUtils m_xmlUtils = null;

        public abstract XMLUtils Utils
        {
            get;
        }

        public abstract XMLUtils XMLUtils
        {
            get;
        }
        //End SIW485

        public virtual string SerializeNode()
        {
            if (null != putNode)
            {
                if (!String.IsNullOrEmpty(putNode.OuterXml)) //SIW371
                {
                    return putNode.OuterXml;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }
        // Start SIW7537
        public virtual XmlNode DeSerializeNode(string sXML)
        {
            return DeSerializeNode(sXML, null);
        }
        public virtual XmlNode DeSerializeNode(string sXML, XmlDocument document)
        {
            if (null != Parent_Node)
            {
                XmlDocumentFragment xmlImportDocFrag;
                if (document != null)
                    xmlImportDocFrag = document.CreateDocumentFragment();
                else
                    xmlImportDocFrag = (new XmlDocument()).CreateDocumentFragment();

                xmlImportDocFrag.InnerXml = sXML;
                XmlNode xmlImportNode = xmlImportDocFrag.SelectSingleNode(NodeName);
                //if ((!String.IsNullOrEmpty(xmlImportNode.InnerXml)) && (!String.IsNullOrEmpty(xmlImportNode.Value)))
                if (!String.IsNullOrEmpty(xmlImportNode.InnerXml))
                {
                    InsertInDocument(null, xmlImportNode);
                }
                //SIW529 ResetParent();  //Jim:  Didn't think this was needed here, as we aren't actually changing the parent.
                return Node;
            }
            else
            {
                return null;
            }
        }
        // End SIW7537
        //access to the Errors object
        public override Errors Errors
        {
            get
            {
                return Utils.gControl.Errors;	//SIW485 Changed XMLGLobals to Utils
            }
        }

        public override ArrayList NodeOrder
        {
            get
            {
                return sThisNodeOrder;
            }
        }

        //SIW529 public override XmlDocument Document
        public virtual XmlDocument Document //SIW529
        {
            get
            {
                return XML.Document;
            }
            protected set
            {
                XML.Document = value;
                ResetParent();
            }
        }

        //access to the XML formatting functions
        public virtual XML XML
        {
            get
            {
                //Start SIW529
                //if (m_UseSharedXML == true)
                //{
                //    if (null == Utils.gControl)	//SIW485 Changed XMLGLobals to Utils
                //    {
                //        return null;
                //    }
                //    else
                //    {
                return Utils.gControl.XML;	//SIW485 Changed XMLGLobals to Utils
                //    }
                //}
                //else
                //{
                //    return m_xml;
                //}
                //End SIW529
            }
            //Start SIW139
            set
            {
                //Start SIW529
                //if (m_UseSharedXML == true)
                //{
                Utils.gControl.XML = value;	//SIW485 Changed XMLGLobals to Utils
                //}
                //else
                //{
                //    m_xml = value;
                //}
                //End SIW529
                ResetParent();
            }
            //End SIW139
        }

        //Start SIW529
        //public virtual bool UseSharedXMLDocument
        //{
        //    get
        //    {
        //        return m_UseSharedXML;
        //    }
        //    set
        //    {
        //        if (value != m_UseSharedXML)
        //        {
        //            m_UseSharedXML = value;
        //            if (value == false)
        //            {
        //                XML = new XML();
        //                XML.Document = new XmlDocument();
        //                XML.Document.LoadXml(Utils.gControl.XML.Document.OuterXml);	//SIW485 Changed XMLGLobals to Utils
        //            }
        //        }
        //        m_UseSharedXML = value;
        //    }
        //}
        //End SIW529

        public Debug Debug
        {
            get
            {
                return Utils.gControl.Debug;	//SIW485 Changed XMLGLobals to Utils
            }
        }

        //IMPORTANT NOTE: If this Parent property is hidden with the keywork 'new' in a subclass,
        //need to override InsertInDocument, Parent_Node, Parent_NodeOrder as well
        //(could just copy/paste from below if retaining functionality, change virtual to override)
        public override XMLACNode Parent
        {
            get
            {
                if (m_Parent == null)
                {
                    m_Parent = XML;
                    ResetParent();
                }
                return m_Parent;
            }
        }

        protected virtual void ResetParent() { }

        //returns the parent node
        public virtual XmlNode Parent_Node
        {
            get
            {
                return Parent.putNode;
            }
        }

        //returns the node order list from the parent
        public virtual ArrayList Parent_NodeOrder
        {
            get
            {
                return Parent.NodeOrder;
            }
        }

        public virtual void InsertInDocument(XmlDocument Doc, XmlNode nde)
        {
            if (Doc != null)
                Document = Doc;
            if (nde != null)
                Node = nde;

            XML.XMLInsertNodeInPlace(Parent_Node, Node, Parent_NodeOrder);
        }

        //returns the Xml associated with this node
        public string sXML
        {
            get
            {
                string spn = "XMLXCNode.sXML(Get)";
                if (xmlThisNode != null)
                    return xmlThisNode.OuterXml;
                else
                {
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn);
                    return "";
                }
            }
        }

        public override XmlNode putNode
        {
            get
            {
                if (Node == null)
                    Create();
                return Node;
            }
        }

        protected virtual XmlNode CreateNode()
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;
            Errors.Clear();

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = (XmlElement)XML.XMLNewElement(NodeName, false);
                Node = xmlElement;

                InsertInDocument(null, null);
                return Node;
            }
            else
            {
                Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, this.GetType().ToString() + ".CreateNode");
                return null;
            }

        }
        //needed in classes that don't override the putNode property from here
        //if not overridden in a class, will throw not implemented exception
        public virtual XmlNode Create()
        {
            return CreateNode();
        }

        protected abstract string DefaultNodeName
        {
            get;
        }

        public virtual string NodeName
        {
            get
            {
                if (String.IsNullOrEmpty(m_NodeName))
                {
                    ResetNodeName();
                }
                return m_NodeName;
            }
            set
            {
                if (AllowNodeNameOverride == true)
                {
                    m_NodeName = value;
                    if (String.IsNullOrEmpty(m_NodeName))
                    {
                        ResetNodeName();
                    }
                }
                else
                {
                    throw new NotImplementedException("Set method is not appropriate for the Class");
                }
            }
        }

        protected virtual bool AllowNodeNameOverride
        {
            get { return false; }
        }

        public virtual void ResetNodeName()
        {
            m_NodeName = DefaultNodeName;
        }

        public virtual void LinkXMLObjects(XMLXCNodeBase linkedObject)
        {
            //SIW529 xmlChild.UseSharedXMLDocument = this.UseSharedXMLDocument;
            //SIW529 xmlChild.XML = this.XML;
            linkedObject.XML = this.XML;
        }

    }
}
