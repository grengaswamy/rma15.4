/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using CCP.Constants;
using CCP.Common;

namespace CCP.XmlFormatting
{
    /******************************************************************************
    ' The cXMLBatch class provides the functionality to support and manage a
    ' ClaimsPro XML Document Batch Submission List
    '*****************************************************************************
    '<BATCH COUNT="Count of Documents to be Processed {integer}">
    '   <FOLDER>Batch Submission Folder{string}</FOLDER>
    '   <ITEM TYPE="Type of Input Document {string} [FROIIMPORT,othersTBD]">
    '      <FILE>Physical File Name of Document {string}</FILE>
    '      <FOLDER ADDRESS="[RELATIVE],[ABSOLUTE]">Specific File Folder, Overrides Batch Folder {string}</FOLDER>
    '      <NAME>Internal Document Name Matched to Document Name in Header {string}</NAME>
    '   </ITEM>
    '</BATCH>
    '*****************************************************************************/
    public class XMLBatch:XMLXFNode
    {
        private XmlNode m_xmlItem;      //the ITEM node
        private Dictionary<biItemType, string> sItemType;
        private Dictionary<biFolderAddress, string> sFolderAddress;

        //initialize
        public XMLBatch()
        {
            sItemType = new Dictionary<biItemType, string>();
            sFolderAddress = new Dictionary<biFolderAddress, string>();
            sItemType.Add(biItemType.biXML, Constants.sbiXML);
            sItemType.Add(biItemType.biProcedure, Constants.sbiProcedure);
            sItemType.Add(biItemType.biFile, Constants.sbiFile);
            sItemType.Add(biItemType.biText, Constants.sbiText);
            sFolderAddress.Add(biFolderAddress.biAbsolute, Constants.sbiAbsolute);
            sFolderAddress.Add(biFolderAddress.biRelative, Constants.sbiRelative);

            xf = null;
            xmlThisNode = null;
            m_xmlItem = null;
        }

        //access to the COUNT attribute of the BATCH node
        public int Count
        {
            get
            {
                int idata;
                XmlNode xmlNode;

                idata = 0;
                xmlNode = Node;
                if (xmlNode != null)
                    idata = Int32.Parse(XML.XMLGetAttributeValue(xmlNode, Constants.sBatCount));
                return idata;
            }
            set
            {
                XmlNode xmlNode;

                xmlNode = Node;
                if (xmlNode != null)
                    XML.XMLSetAttributeValue(xmlNode, Constants.sBatCount, value + "");
            }
        }

        //access to the XML functions object
        public override XML XML
        {
            get
            {
                if (xf == null)
                {
                    xf = new XML();
                    xf.Batch = this;
                }
                return xf;
            }
            set
            {
                if ((xf != null) && (value != null) && (xf.guid == value.guid))
                    return;
                xf = null;
                xf = value;
                if (xf != null)
                    xf.Batch = this;
            }
        }

        //access to the FOLDER node
        public string Folder
        {
            get
            {
                string sdata;
                string spn;

                sdata = "";
                spn = "XMLBatch.Folder(Get)";
                if (xmlThisNode != null)
                    sdata = XML.XMLExtractElement(xmlThisNode, Constants.sBatFolder);
                else
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn);

                return sdata;
            }
            set
            {
                XmlElement xmlNode;
                string spn;
                spn = "XMLBatch.Folder(Let)";

                if (xmlThisNode != null)
                {
                    xmlNode = (XmlElement)xmlThisNode.SelectSingleNode(Constants.sBatFolder);
                    if (xmlNode == null)
                    {
                        if (value != "" && value != null)
                        {
                            xmlNode = XML.XMLElementWithData_Node(Constants.sBatFolder, value);
                            xmlThisNode.AppendChild(xmlNode);
                        }
                    }
                    else
                        if (value != "" && value != null)
                            xmlNode.InnerXml = value;
                        else
                            xmlThisNode.RemoveChild(xmlNode);
                }
                else
                {
                    xmlNode = null;
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn, value);
                }
            }
        }

        //access to the ITEM node
        public XmlNode Item
        {
            get
            {
                return m_xmlItem;
            }
            set
            {
                if (value == null)
                    m_xmlItem = value;
                else
                    if (value.Name == "ITEM")
                        m_xmlItem = value;
            }
        }

        //access to the FILE node
        public string ItemFile
        {
            get
            {
                string sdata = "";
                string spn;
                spn = "XMLBatch.ItemFile(Get)";

                if (m_xmlItem != null)
                    sdata = XML.XMLExtractElement(m_xmlItem, Constants.sBatItemFile);
                else
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn);

                return sdata;
            }
            set
            {
                XmlElement xmlNameNode;
                string spn;
                spn = "XMLBatch.ItemFile(Let)";

                if (m_xmlItem != null)
                {
                    xmlNameNode = (XmlElement)m_xmlItem.SelectSingleNode(Constants.sBatItemFile);
                    if (xmlNameNode == null)
                    {
                        xmlNameNode = XML.XMLElementWithData_Node(Constants.sBatItemFile, value);
                        m_xmlItem.AppendChild(xmlNameNode);
                    }
                    else
                        xmlNameNode.InnerXml = value;
                }
                else
                {
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn, value);
                }
            }
        }

        //access to the FOLDER NODE
        public string ItemFolder
        {
            get
            {
                string sdata = "";
                string spn;
                spn = "XMLBatch.ItemFolder(Get)";

                if (m_xmlItem != null)
                    sdata = XML.XMLExtractElement(m_xmlItem, Constants.sBatItemFolder);
                else
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn);

                return sdata;
            }
            set
            {
                XmlElement xmlNode;
                string spn;
                spn = "XMLBatch.ItemFolder(Let)";

                if (m_xmlItem != null)
                {
                    xmlNode = (XmlElement)m_xmlItem.SelectSingleNode(Constants.sBatItemFolder);
                    if (xmlNode == null)
                    {
                        if (value != "" && value != null)
                        {
                            xmlNode = XML.XMLElementWithData_Node(Constants.sBatItemFolder, value);
                            m_xmlItem.AppendChild(xmlNode);
                        }
                    }
                    else
                        if (value == "" || value == null)
                            m_xmlItem.RemoveChild(xmlNode);
                        else
                            xmlNode.InnerXml = value;
                }
                else
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn, value);
            }
        }

        //access to the NAME node
        public string ItemName
        {
            get
            {
                string sdata = "";
                string spn;
                spn = "XML.Batch.ItemName(Get)";

                if (m_xmlItem != null)
                    sdata = XML.XMLExtractElement(m_xmlItem, Constants.sBatItemName);
                else
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn);

                return sdata;
            }
            set
            {
                XmlElement xmlNameNode;
                string spn;
                spn = "XMLBatch.ItemName(Let)";

                if (m_xmlItem != null)
                {
                    xmlNameNode = (XmlElement)m_xmlItem.SelectSingleNode(Constants.sBatItemName);
                    if (xmlNameNode == null)
                    {
                        xmlNameNode = XML.XMLElementWithData_Node(Constants.sBatItemName, value);
                        m_xmlItem.AppendChild(xmlNameNode);
                    }
                    else
                        xmlNameNode.InnerXml = value;
                }
                else
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn, value);
            }
        }

        //access to the TYPE attribute of the ITEM node
        public biItemType ItemType
        {
            get
            {
                string sdata;
                string spn;
                biItemType iType;
                spn = "XMLBatch.ItemType(Get)";

                sdata = "";
                iType = biItemType.biTypeUndefined;
                if (m_xmlItem != null)
                {
                    sdata = XML.XMLGetAttributeValue(m_xmlItem, Constants.sBatItemType);
                    foreach (KeyValuePair<biItemType, string> kvp in sItemType)
                    {
                        if (kvp.Value == sdata)
                        {
                            iType = kvp.Key;
                            break;
                        }
                    }
                }
                else
                {
                    XML.Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn);
                }
                return iType;
            }
            set
            {
                string spn;
                string type;
                spn = "XMLBatch.ItemType(Let)";
                sItemType.TryGetValue(value, out type);
                if (m_xmlItem != null)
                    XML.XMLSetAttributeValue(m_xmlItem, Constants.sBatItemType, type);
                else
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn, type);
            }
        }

        //access to the BATCH node
        public override XmlNode Node
        {
            get
            {
                XmlDocument xmlDocument;

                if (xmlThisNode == null)
                {
                    xmlDocument = Document;
                    xmlThisNode = XML.XMLGetNode(xmlDocument.DocumentElement, Constants.sBatchElement);
                }
                if (xmlThisNode == null)
                {
                    Create(null, null);
                }
                return xmlThisNode;
            }
        }

        public XmlNode AddItem(biItemType cItmtype, string sItmname,string sItmfile)
        {
            return AddItem(cItmtype, sItmname, sItmfile, "", biFolderAddress.biRelative);
        }

        public XmlNode AddItem(biItemType cItmtype, string sItmname, string sItmfile, string sItmfolder)
        {
            return AddItem(cItmtype, sItmname, sItmfile, sItmfolder, biFolderAddress.biRelative);
        }

        //adds an ITEM node
        public XmlNode AddItem(biItemType cItmtype, string sItmname, string sItmfile, string sItmfolder, biFolderAddress cItmfldaddress)
        {
            XmlNode xmlBatNode;
            XmlDocument xmlDocument;

            xmlBatNode = Node;

            xmlDocument = Document;
            m_xmlItem = xmlDocument.CreateElement(Constants.sBatItem);

            xmlBatNode.AppendChild(m_xmlItem);

            ItemType = cItmtype;
            ItemName = sItmname;
            ItemFile = sItmfile;
            if (sItmfolder != "" && sItmfolder != null)
            {
                ItemFolder = sItmfolder;
                ItemFolderAddress = cItmfldaddress;
            }

            Count = Count + 1;
            return m_xmlItem;
        }

        //access to the ADDRESS attribute of the FOLDER node
        public biFolderAddress ItemFolderAddress
        {
            get
            {
                string sdata;
                string spn;
                biFolderAddress iaddr;
                XmlNode xmlfldr;
                spn = "XMLBatch.ItemFolderAddress(Get)";

                sdata = "";
                iaddr = biFolderAddress.biAddressUndefined;
                if (m_xmlItem != null)
                {
                    xmlfldr = XML.XMLGetNode(m_xmlItem, Constants.sBatItemFolder);
                    if (xmlfldr != null)
                    {
                        sdata = XML.XMLGetAttributeValue(xmlfldr, Constants.sBatItemFolderAddress);
                        foreach (KeyValuePair<biFolderAddress, string> kvp in sFolderAddress)
                        {
                            if (kvp.Value == sdata)
                            {
                                iaddr = kvp.Key;
                                break;
                            }
                        }
                    }
                    else
                        Errors.PostError(ErrorGlobalConstants.ERRC_NODE_NOT_FOUND, spn, "Item Folder");
                }
                else
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn, "Batch Item");
                return iaddr;
            }
            set
            {
                XmlNode xmlfldr;
                string spn;
                string val;
                spn = "XMLBatch.ItemFolderAddress(Let)";
                sFolderAddress.TryGetValue(value, out val);
                if (m_xmlItem != null)
                {
                    xmlfldr = XML.XMLGetNode(m_xmlItem, Constants.sBatItemFolder);
                    if (xmlfldr != null)
                        XML.XMLSetAttributeValue(xmlfldr, Constants.sBatItemFolderAddress, val);
                    else
                        Errors.PostError(ErrorGlobalConstants.ERRC_NODE_NOT_FOUND, spn, "Item Folder");
                }
                else
                    Errors.PostError(ErrorGlobalConstants.ERRC_NO_NODE, spn, "Item:" + val);
            }
        }

        //Creates a new BATCH node
        public XmlNode Create(XmlDocument xDocument, string sFolder)
        {
            XmlDocument xmlDocument;
            XmlDocumentFragment xmlDocFrag;
            XmlElement xmlElement;

            xmlThisNode = null;

            if (xDocument != null)
                Document = xDocument;

            xmlDocument = Document;

            if (xmlDocument != null)
            {
                xmlDocFrag = xmlDocument.CreateDocumentFragment();

                xmlElement = xmlDocument.CreateElement(Constants.sBatchElement);
                Node = xmlElement;

                xmlDocFrag.AppendChild(xmlElement);

                Count = 0;
                Folder = sFolder;

                InsertInDocument(null, null);
                return xmlThisNode;
            }
            else
            {
                XML.Errors.PostError(ErrorGlobalConstants.ERRC_NO_DOCUMENT, "cXMLBatch.Create");
                return null;
            }
        }

        //retrieve an ITEM node
        public XmlNode GetItem(biDirection direction)
        {
            XmlNode batnode;

            batnode = Node;

            switch (direction)
            {
                case biDirection.biFirst:
                    m_xmlItem = XML.XMLGetNode(batnode, Constants.sBatItem);
                    break;
                case biDirection.biNext:
                    if (m_xmlItem != null)
                        m_xmlItem = m_xmlItem.NextSibling;
                    else
                        m_xmlItem = XML.XMLGetNode(batnode, Constants.sBatItem);
                    break;
                case biDirection.biPrev:
                    if (m_xmlItem != null)
                        m_xmlItem = m_xmlItem.PreviousSibling;
                    else
                        m_xmlItem = batnode.LastChild;
                    break;
                case biDirection.biCurr:
                    break;
            }
            return m_xmlItem;
        }

        //puts the BATCH node into the document
        public override void InsertInDocument(XmlDocument doc, XmlNode nde)
        {
            XmlNode xmlCurrNode;

            base.InsertInDocument(doc, nde);

            xmlCurrNode = XML.XMLGetNode(xmlDocument.DocumentElement, Constants.sBatchElement);

            if (xmlCurrNode != null)
                xmlDocument.DocumentElement.ReplaceChild(xmlNewNode, xmlCurrNode);
            else
            {
                xmlCurrNode = XML.XMLGetNode(xmlDocument.DocumentElement, Constants.sPM);
                if (xmlCurrNode == null)
                    xmlCurrNode = XML.XMLGetNode(xmlDocument.DocumentElement, Constants.sHdrElement);
                if (xmlCurrNode != null)
                    xmlCurrNode = xmlCurrNode.NextSibling;
                else
                    xmlCurrNode = xmlDocument.DocumentElement.FirstChild;
                if (xmlCurrNode != null)
                    xmlDocument.DocumentElement.InsertBefore(xmlNewNode, xmlCurrNode);
                else
                    xmlDocument.DocumentElement.AppendChild(xmlNewNode);
            }
        }
    } //End Class XMLBatch
}
