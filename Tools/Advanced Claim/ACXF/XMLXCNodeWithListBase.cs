/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 09/01/2010 | SIW493  |   ASM      | Correct rest logic in NodeList
 * 09/02/2010 | SIW493  |    AS      | Fix for Nodelist and GroupList(sub-lists)
 * 09/08/2010 | SIW493  |    AS      | Fix for GroupList(sub-lists)
 * 11/03/2010 | SIW529  |    JTC     | Remaining fixes for removing static functions
 *                                   | Includes removing the first paramater of all Utils put* and get* function calls
 *                                   | Not all items are marked with the SI, due to overwhelming number of lines
 *                                   | Removed some code from W485, renamed XMLXC* objects to XMLXC*Base (not all marked)
 *                                   | Set accessors on Document and Node properties changed to protected
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections;

namespace CCP.XmlFormatting
{
    //Extended abstract class for use in classes with XmlNodeLists
    //NodeList and Count may be unimplemented in such nodes due to name changes

    public abstract class XMLXCNodeWithListBase:XMLXCNodeBase   //SIW529
    {
        protected XmlNodeList xmlNodeList;

        public virtual string SerializeNodeList()
        {
            string sXML = String.Empty;
            bool bReset = true;

            while(null != getNode(bReset))
            {
                bReset = false;
                sXML += SerializeNode();
            }
            return sXML;
        }

        public virtual XmlNodeList DeSerializeNodeList(string sXML)
        {
            if (null != Parent_Node)
            {

                XmlDocumentFragment xmlImportDocFrag = (new XmlDocument()).CreateDocumentFragment();
                xmlImportDocFrag.InnerXml = sXML;
                foreach (XmlNode xmlImportNode in xmlImportDocFrag.SelectNodes(NodeName))
                {
                    InsertInDocument(null, xmlImportNode);                    
                }
                //SIW529 ResetParent();  //Jim:  Didn't think this was needed here, as we aren't actually changing the parent.
                return NodeList;
            }
            else
            {
                return null;
            }
        }

        public virtual XmlNodeList NodeList
        {
            get { throw new NotImplementedException(); }
        }        
        protected XmlNodeList getNodeList(string sConst)
        {
            //return getNodeList(sConst, ref xmlNodeList, ref xmlNodeListEnum, Parent_Node);
            return getNodeList(sConst, ref xmlNodeList, Parent_Node);
        }

        protected XmlNodeList getNodeList(string sConst, ref XmlNodeList xmlnlist)//, ref IEnumerator xmlnlenum)
        {
            return getNodeList(sConst, ref xmlnlist, Parent_Node);
        }

        //protected XmlNodeList getNodeList(string sConst, ref XmlNodeList xmlnlist, ref IEnumerator xmlnlenum, XmlNode pnode)
        protected XmlNodeList getNodeList(string sConst, ref XmlNodeList xmlnlist, XmlNode pnode)
        {
            if (xmlnlist == null)
            {
                xmlnlist = XML.XMLGetNodes(pnode, sConst);
            }
            return xmlnlist;
        }

        public int Count
        {
            get
            {
                if (NodeList != null)
                    return NodeList.Count;
                else
                    return 0;
            }
        }

        public XmlNode getNode(bool reset)
        {
            if (reset) xmlNodeList = null; //SIW493
            if (NodeList != null)
            {
                IEnumerator xmlNodeListEnum = xmlNodeList.GetEnumerator();
                if (!reset)
                {
                    xmlNodeListEnum.MoveNext();
                    while (Node != xmlNodeListEnum.Current && xmlNodeListEnum.Current != null)
                        xmlNodeListEnum.MoveNext();
                }
                if (xmlNodeListEnum.MoveNext())
                    Node = (XmlNode)xmlNodeListEnum.Current;
                else
                    Node = null;
                if (Node == null)
                    xmlNodeList = null;
            }
            else
                Node = null;
            return Node;
        }

        //protected XmlNode getNode(bool reset, ref XmlNodeList xmlnlist, ref IEnumerator xmlnlenum, ref XmlNode xnode)
        protected XmlNode getNode(bool reset, ref XmlNodeList xmlnlist, ref XmlNode xnode)
        { 
            // Start SIW493            
            //if (reset) xmlnlist = null; 
            //if (GroupList != null) 
            // Unable to set base XC update in sub-lists because 
            // unlike all xc object NodeList, 
            // these sub-lists do not have a common name, such as Grouplist / DataItemList
            if (xmlnlist != null) // End SIW493
            {
                IEnumerator xmlnlenum = xmlnlist.GetEnumerator();
                if (!reset)
                {
                    xmlnlenum.MoveNext();
                    while (xnode != xmlnlenum.Current && xmlnlenum.Current != null)
                        xmlnlenum.MoveNext();
                }
                if (xmlnlenum.MoveNext())
                    xnode = (XmlNode)xmlnlenum.Current;
                else
                    xnode = null;
                if (xnode == null)
                {
                    xmlnlist = null;
                }
            }
            else
                xnode = null;
            return xnode;
        }

        /*protected XmlNode getFirst(ref XmlNodeList xmlnlist, ref IEnumerator xmlnlenum, ref XmlNode xnode)
        {
            return getNode(true, ref xmlnlist, ref xmlnlenum, ref xnode);
        }*/

        public XmlNode getFirst()
        {
            return getNode(true);
        }

        /*public XmlNode getNext(ref XmlNodeList xmlnlist, ref IEnumerator xmlnlenum, ref XmlNode xnode)
        {
            return getNode(false, ref xmlnlist, ref xmlnlenum, ref xnode);
        }*/

        public XmlNode getNext()
        {
            return getNode(false);
        }
    }
}
