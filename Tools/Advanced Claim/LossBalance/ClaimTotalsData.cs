﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.LossBalance
{
    public class ClaimTotalsData
    {

        private float m_cPaidAmount;
        private float m_cReserveAmount;
        private float m_cReserveChange;
        private string m_sClaimNumber;
        private long m_lTransactionCount;
        private long m_lOffsetOnsetCount;
        private long m_lUnitStatCount;
        
        public float PaidAmount
        {
            get {return m_cPaidAmount;}

            set { m_cPaidAmount = value; }
        }

        public float ReserveAmount
        {
           get { return m_cReserveAmount;}

           set { m_cReserveAmount = value; }
        }

        public float ReserveChange
        {
            get { return m_cReserveChange; }

            set { m_cReserveChange =  value; }
        }

        public string ClaimNumber
        {
           get { return m_sClaimNumber;}

            set { m_sClaimNumber = value; }
        }

        public long TransactionCount
        {
            get { return m_lTransactionCount; }
       
           set { m_lTransactionCount = value; }
        }

        public long OffsetOnsetCount
        {
           get { return m_lOffsetOnsetCount;}

            set { m_lOffsetOnsetCount = value; }
        }

        public long UnitStatCount
        {
           get { return m_lUnitStatCount;}

             set { m_lUnitStatCount =  value; }
        }

    }
}
