﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCP.LossBalance
{
    public class ResvTypeTotalData
    {

        private float m_cPaidAmount;
        private float m_cReserveAmount;
        private float m_cReserveChange;
        private string m_sReserveType;
        private long m_lPaymentCount;
        private long m_lReserveCount;
        private long m_lDetailCount;
        private long m_lLossDetailCount;

        public float PaidAmount
        {
            get {return m_cPaidAmount;}

            set { m_cPaidAmount = value; }
        }

        public float ReserveAmount
        {
            get {return m_cReserveAmount;}

            set { m_cReserveAmount = value; }
        }

        public float ReserveChange
        {
            get {return m_cReserveChange;}

            set {m_cReserveChange = value;}
        }

        public string ReserveType
        {
            get { return m_sReserveType; }

            set {m_sReserveType = value;}
        }

        public long PaymentCount
        {
           get { return m_lPaymentCount;}

            set {m_lPaymentCount = value;}
        }

        public long ReserveCount
        {
            get {return m_lReserveCount;}

            set {m_lReserveCount = value;}
        }

        public long DetailCount
        {
           get { return m_lDetailCount;}

            set {m_lDetailCount = value;}
        }

        public long LossDetailCount
        {
           get { return m_lLossDetailCount;}

           set { m_lLossDetailCount = value;}
        }


    }
}
