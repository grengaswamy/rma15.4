/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 11/21/2007 | 05583   |    Lau     | Add COUNTY_OF_ORIGIN
 * 03/27/2008 | 06267   |    JTC     | Add Diary DATE_AFTER
 * 04/21/2008 | 06321   |    JTC     | Add user list to security
 * 04/29/2008 | 06333   |    SW      | Retrofit XC/XF/XS updates to Dot Net Assemblies
 * 05/08/2008 | 06345   |    SW      | Add Entity DISPLAY_NAME       
 * 05/28/2008 | 06354   |    SW      | Add DISPLAY_ADDRESS tag to Address XML. 
 * 06/06/2008 | 06320   |    NAB     | Add Support DB Connection String
 * 06/17/2008 | SI06369 |    sw      | Implemented HTML to replace RTF in RadEditors  
 * 07/14/2008 | SI06023 |    AS      | Paramter Read/Save.
 * 07/21/2008 | SI06267 |    JTC     | Update for HTML with Diary
 * 07/24/2008 | SI06321 |    JTC     | Update for user id in security
 * 07/24/2008 | SI06249 |    sw      | Add Effective start date and End date to Address/Contact XML    
 * 08/06/2008 | SI06369 |    sw      | CommentHTML Tag fix
 * 08/15/2008 | SI06420 |    CB      | Get first child 
 * 08/27/2008 | SI06453 |    AS      | Add code-id attribute for search
 * 09/10/2008 | SI06423 |    CB      | Added "Non Veh Property Damage" and "Insured"
 * 10/31/2008 | SI06467 |    ASM     | Added XMLTreatment.
 * 11/07/2008 | SI06423a|    KCB     | Update to previous field Mapping
 * 11/20/2008 | SI06472 |    JTC     | retrofit from COM - separate diary response time
 * 12/05/2008 | SI06501 |    NDB     | Adding Column attribute names for field type
 * 01/27/2009 | SIW139  |    JTC     | Field blanking with [b]
 * 03/04/2009 | SIW119  |    sw      | Add System Settings - related to SIW119
 * 03/30/2009 | SIW157  |    sw      | Temp Address Search Issue
 * 04/08/2009 | SIW11b  |   KCB      | Freeze Payments
 * 05/22/2009 | SIW180  |   JTC      | Prevent saving of stale data
 * 07/08/2009 | SIW10   |   sw       | EventDescriptionHTML and LocationDescriptionHTML fields save/load
 * 07/16/2009 | SIW189  |   SW       | Implemented HTML for fields of OshaInfo, added NO_RULES_FLAG and RECORDABLE_FLAG 
 * 09/09/2009 | SIW44   |   SW       | Field YearLastExposed fixed
 * 09/10/2009 | SIW12   |   sw       | Entity Type chnaged to Uppercase to match with VB XC
 * 09/29/2009 | SIN204  |    JTC     | Updates for ISO Interface
 * 11/10/2009 | SIW198  |    SW      | Location Address on Salvage Page.
 * 11/19/2009 | SIW44   |    ASM     | WorkerComp (Employee) field's mapping
 * 01/18/2010 | SI06650 |   NDB      | Party Involed ID changes
 * 03/11/2010 | SIW353  |    AS      | XMLComment updated with Public/Private flags
 * 03/18/2010 | SIW360  |    AS      | Reordering TechKey
 * 03/21/2010 | SIW367  |    AS      | ParentCommentID added to comment xmlobject
 * 03/23/2010 | SIW254  |    AS      | HTML fields added to litigation and case management xmlobject
 * 04/09/2010 | SIW404  |    JTC     | Restrict max number of extracted diaries
 * 04/15/2010 | SIW392  |   ASM      |  LostDays and RestrictedDays load/save
 * 04/28/2010 | SIW391  |    SW      | Time Day began field on Worker compensation
 * 05/04/2010 | SIW403  |   ASM      | Expire Party Involved Role
 * 05/04/2010 | SIW261  |   JTC      | Attachments
 * 05/18/2010 | SIW316  |    AP      | Need data entry for Entity ID and Entity Category.Values show on Entity Tree.  Need to be able to Add, Edit, Delete from tree.       
 * 05/17/2010 | SIW360  |   AP       | Techkey position reodered in .NET XML components similar to ordering in CCPXC components.
 * 05/20/2010 | SIW360  |   AS       | .NET XML objects changed as per in CCPXC components.
 * 05/24/2010 | SIW321  |   SW       | Tech Key added to XMLEvent
 * 06/11/2010 | SIW367  |   SW       | Initial, Next and Previous CommentID.  
 * 06/28/2010 | SIW452  |    AS      | Add severity attribute to error node
 * 06/29/2010 | SIW344  |    AP      | Liability Loss missing from drop down on Claim Entry screen.Adding  Liability Loss screen in AC Web.
 * 08/03/2010 | SIW485  |    JTC     | Fix for Multi-User Environment
 *  SIW489 7/15/2010 hfw - changes for WEB based Combined Pay
 *  08/13/2010 |SIW490 |    ASM     | Tree Label   
 * 08/16/2010 | SIW490  |    AS      | Update for new Tree definition
 * 11/12/2010 | SIW452  |    AS      | Add percent disability and number of weeks to loss detail object
 * 11/17/2010 | SIW493  |    AS      | ENTITYIDREF added for Entity ID of party involvedLogic, for backward compatibility as per SI06838
 * 12/30/2010 | SIW344  |   SW       | Liability Unit type corrected.
 * 03/23/2011 | SIW7036 |   SW       | Handled Incurred limit in AC Web.
 * 03/16/2011 | SIW7049  |   AS      | To assign a diary module to a module security group. 
 * 03/22/2011 | SIW7038  |   BS      | To distinguish diary items from  tasks and to add policy number,
                                     |  insured name and document number on diary screen as read only fields.
 * 04/25/2011 | SIW7076 |   VM       | Supplemental implementation.
 * 04/26/2011 | SIW7062 |   AL       | Define the enumeration for the Control Types on web page. 
 * 05/13/2011 | SIW7123 |   AV       | Policy Limits and Deductibles
 * 05/13/2011 |SIW7127 |   BS       |   AC needs to be able to set the expiry date on an Entity using the XML import
 *06/01/2011 |SIW7127 |   ASR       |   AC needs to be able to set the Effective date on an Entity using the XML import
 * 05/13/2011 | SIN7096 |   MS       | Add Modifier Code in the Procedure Node in .Net XML
 * 5/4/2011   | SIN7178 |  hfw       | add future print date for payments
 * 06/11/2011| SIN7206  |   Lau      | Add Legacy Key
*  SIN7227 hfw 6/16/2011 - add payment offsets to Web
 *  06/10/2011 | SIW7182 |   RG      | Defining Account nodes for Entity Accounts
 * 06/26/2011 |N7268  |     AM       | Added constants for Mail Merge
 * 06/30/2011 | SIN7227 |   AS       | Reserve / Payment transaction requests
 * 06/11/2011 | SIW7214 |    AV       | Medicare information for the claimant
 *  06/10/2011 | SIN7278 |   Vineet  | Admin Tracking
 * 08/04/2011 | SIW7341  |   AL      | Ability to attach Reserve/Payment to Diary.  
 * 08/08/2011 | SIW7332 |   Vineet   | Diary Peek
 *  08/12/2011 | SIW7309 | umahajan2 | Added DescriptionOfSymptoms, DescriptionOfSymptomsHTML, WhereInjuredTaken, ImpairmentFlag
 *  08/21/2011 | SIW7240 |   VM       | Comb Payment web enhancement
 *  08/19/2011 | SIW7388 | umahajan2 | Added Version Number in Claim Data
 *  09/07/2011 | SIW7473 |    RG      |  Added Account ID(CheckBook Id) on Entity Account screen 
 *  09/17/2011 | SIW7368 |    ND      |  Scheduled Activity updates
 *  08/14/2011 | SIW7419 |   RG      | Demand Offer for Property, vehicle and Injury.
 *  08/28/2011 | SIN7404 |    AM     | Added Quick Query Infrastructure
 *  09/17/2011 | SIW7531 |    AP      |  Initial Version of Out of Office.
 * 09/21/2011 | SIW7327 |  ACH       | Add claim history
 *  09/19/2011 | SIN7541 |    ASR     |  Added Disallow Payments Checkbox in Other Information tab of Entity Screen
 * 09/23/2011 | SIW7123 |    AV      | Add new nodes for limit and deductible
 *  09/28/2011 | SIN7587 |    UM     |   Added New constants for ID Number Nodes
 * 09/23/2011 | SIW7509 |   Vineet   | Load payment screen from payment history screen
 * 10/05/2011 | SIW7601 |   SW       | EntityAccountID added to combpay
 *  10/10/2011 | SIW7620 |   AL      | Added constants for Physician and MediStaff screens. 
 *  09/22/2011 | SIW7076 |   AV  | Add contact id node to the contact node.
 * 10/13/2011 | SIW7647 | Vineet     | Display payment/collection history for frozen reserve 
 * 11/03/2011 | SIN7750 | MK         | Insured Person name for Dairy List
 * 11/16/2011 | SIW7537 | ubora      | reserveless payment / collection in ACWeb
 * 11/18/2011 | SIW7076 |   AV  |     Changed the name of node from Contact_ID to ID.
 * 11/24/2011 | SIW7790 |   AV      | Add a new relation ship id in contact node in order to update contact type.
 * 12/02/2011 |SIN7884  |  SS      | Resolved - Demand/Offer 'Activity' in WEB is not displaying after a Demand / Offer is saved
 * 12/08/2011 | SIW7896 |   ubora    | Implement Missing field - Security Department in ACWeb
 * 12/11/2011 | SIW7882 |    RG      |  Group Association for Supplementals
 * 12/23/2011 | SIW7844 |  AV/BS -   |Fix to modify search headers in ACWEB for financials
 * 01/24/2012 | SIW8067 |    SAS     |ID Number is not working for Converion ID Number type
 * 12/26/2011 | SIW7983 |  Vineet    |  Adding six new fields on the Litigation screen.
 * 01/06/2012 | SIN8034 |   SS       | Added Sub Speciality field.
 * 01/31/2012 | SIW7928 |    PB      | Bank Account combo box processing on Payment Panel.
 * 02/15/2012 | SIW8084 |    ND      | Introduced constant to handle AMOUNT fields.
 * 02/29/2012|  SIW7455 |   PV       | Add claim severity code.
 * 01/30/2012 | SIW7855 |   ubora    | Add a new claim to an existing event
 * 03/14/2012 | SIN8237 |   ubora    | Added Address Reference code
 * 03/13/2012 | SIW8235 |   PV       | Time zone issue when assigning diary to a out-of-office user in a differnt time zone
 * 03/30/2012 | SIW8315 |   PS       | Entity Account Adding Primary flag
 * 04/17/2012 | SIN8386 |   MK       | ReserveAmount and ChangeAmount not getting updated on reserve change.
 * 04/30/2012 | SIN8438 |   MS       | Added UserLookUp Type
 * 05/12/2012 | SI08479 |   m.baum   | Comment author Names
 * 05/04/2012 | SIN8440 |    SAS     | Prefix suffix fix on individual entity 
 * 05/14/2012 | SIN8499 |   SW       | Entity reference OrgSec and OrgHier
 * 02/22/2012 | SIN8043 |    MS      | Added the Weekly Pay and Duration node to lost days
 * 02/02/2012 | SIN8155 |  ASR       | Added Duration to Lost Days Nod
 * 05/24/2012 | SIN8529 |    PV      | Implemented Organization Hierarchy
 * 06/12/2012 | N8584    |    SS      | Add Effective start date and End date to Entity XML  
 * ***********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace CCP.Constants
{
    public enum xcEmpPayPeriod
    {
        xcEmpPPUnspecified = Constants.iUCPayPeriodUnspecified,
        xcEmpPPHourly = Constants.iUCPayPeriodHourly,
        xcEmpPPWeekly = Constants.iUCPayPeriodWeekly,
        xcEmpPPBiWeekly = Constants.iUCPayPeriodBiWeekly,
        xcEmpPPSemiMonthly = Constants.iUCPayPeriodSemiMonthly,
        xcEmpPPMonthly = Constants.iUCPayPeriodMonthly,
        xcEmpPPQuarterly = Constants.iUCPayPeriodQuarterly,
        xcEmpPPAnnual = Constants.iUCPayPeriodAnnual
    }

    public enum xcEmpSpecified
    {
        xcEmpUnspecified = Constants.iSpecifiedUnspecified,
        xcEmpPreInjury = Constants.iSpecifiedPreInjury,
        xcEmpPostInjury = Constants.iSpecifiedPostInjury,
        xcEmpOther = Constants.iSpecifiedOther,
        xcEmpCurrent = Constants.iSpecifiedCurrent
    }

    public enum xcEmpBenefitType
    {
        xcEmpBTUnspecified = Constants.iBenefitTypeUnspecified,
        xcEmpBTUnemployment = Constants.iBenefitTypeUnemployment,
        xcEmpBTSocSec = Constants.iBenefitTypeSocSec,
        xcEmpBTPensionPlan = Constants.iBenefitTypePenPlan,
        xcEmpBTSpclFund = Constants.iBenefitTypeSpclFund,
        xcEmpBTOther = Constants.iBenefitTypeOther
    }

    public enum xcEmpPayType
    {
        xcEmpPTUnspecified = Constants.iUCPayTypeUnspecified,
        xcEmpPTSalary = Constants.iUCPayTypeSalary,
        xcEmpPTHourly = Constants.iUCPayTypeHourly
    }

    public enum entType
    {
        entTypeInd = Constants.iEntTypeInd,
        entTypeBus = Constants.iEntTypeBus,
        entTypeHou = Constants.iEntTypeHou,
        entTypeClient = Constants.iEntTypeClient,
        entTypeCompany = Constants.iEntTypeCompany,
        entTypeOperation = Constants.iEntTypeOperation,
        entTypeRegion = Constants.iEntTypeRegion,
        entTypeDivision = Constants.iEntTypeDivision,
        entTypeLocation = Constants.iEntTypeLocation,
        entTypeFacility = Constants.iEntTypeFacility,
        entTypeDepartment = Constants.iEntTypeDepartment,
        //Start SIN8499
        entTypeClientSec = Constants.iEntTypeClientSec,
        entTypeCompanySec = Constants.iEntTypeCompanySec,
        entTypeOperationSec = Constants.iEntTypeOperationSec,
        entTypeRegionSec = Constants.iEntTypeRegionSec,
        entTypeDivisionSec = Constants.iEntTypeDivisionSec,
        entTypeLocationSec = Constants.iEntTypeLocationSec,
        entTypeFacilitySec = Constants.iEntTypeFacilitySec,
        entTypeDepartmentSec = Constants.iEntTypeDepartmentSec,
        //End SIN8499
        entTypeUnd = Constants.iUndefined
    }

    public enum entTaxIdType
    {
        entTaxIDNA = Constants.iEntTaxIdTypeNA,
        entTaxIdSSN = Constants.iEntTaxIdTypeSSN,
        entTaxIdFEIN = Constants.iEntTaxIdTypeFEIN,
        entTaxIdUnd = Constants.iUndefined
    }

    public enum xcUntType
    {
        xcUTUndefined = Constants.iUndefined,
        xcUTLocation = Constants.iUntTypeLoc,
        xcUTVehicle = Constants.iUntTypeVeh,
        xcUTProperty = Constants.iUntTypeProp,    // SI06023 - Implemented in SI06333
        xcUTLiability = Constants.iUntTypeLiab    //SIW344  
    }

    public enum enVCHType
    {
        enVCHUnSpecified = Constants.iUndefined,
        enVCHCheck = Constants.iVCHTypeCheck,
        enVCHDraft = Constants.iVCHTypeDraft,
        enVCHReceipt = Constants.iVCHTypeReceipt
    }

    public enum doType
    {
        doTypeUndefined = Constants.iUndefined,
        doTypeDemand = 0,
        doTypeOffer = 1,
        doTypeAppeal = 2
    }

    public enum ahType
    {
        ahTypeUndefined = Constants.iUndefined,
        ahTypeActivity = 0,
        ahTypeStatus = 1
    }

    public enum cxmlDataType
    {
        dtDefault = Constants.idtString,
        dtUndefined = Constants.iUndefined,
        dtDate = Constants.idtDate,
        dtTime = Constants.idtTime,
        dtNumber = Constants.idtNumber,
        dtString = Constants.idtString,
        dtCode = Constants.idtCode,
        dtBoolean = Constants.idtBoolean,
        dtDTTM = Constants.idtDTTM,
        dtEntityRef = Constants.idtEntityRef,
        dtDataRecord = Constants.idtDataRecord
    }

    public enum cxmlFileOptions
    {
        cxmlFOOverwrite = 0,
        cxmlFOPrompt = 1,
        cxmlFOBackup = 2
    }

    public enum pmType
    {
        pmmtRequest = Constants.ipmmtRequest,
        pmmtResponse = Constants.ipmmtResponse,
        pmmtUndefined = Constants.iUndefined
    }

    public enum pmRespType
    {
        pmrtFile = Constants.ipmrtFile,
        pmrtURL = Constants.ipmrtURL,
        pmrtText = Constants.ipmrtText,
        pmrtUndefined = Constants.iUndefined
    }

    public enum pmParamDataDirection
    {
        pmGetData = 0,
        pmPutData = 1
    }

    public enum biItemType
    {
        biXML = Constants.ibiXML,
        biProcedure = Constants.ibiProcedure,
        biFile = Constants.ibiFile,
        biText = Constants.ibiText,
        biTypeUndefined = Constants.ibiUndefined
    }

    public enum biFolderAddress
    {
        biAbsolute = Constants.ibiAbsolute,
        biRelative = Constants.ibiRelative,
        biAddressUndefined = Constants.ibiUndefined
    }

    public enum biDirection
    {
        biFirst = Constants.ibiFirst,
        biNext = Constants.ibiNext,
        biPrev = Constants.ibiPrev,
        biCurr = Constants.ibiCurr
    }

    public enum LDTOffOnSet
    {
        LDTOOSUnspecified = 0,
        LDTOOSOnset = Constants.iLDOnSet,
        LDTOOSOffset = Constants.iLDOffSet
    }

    public enum xcRepairType
    {
        xcRTUndefined = Constants.iUndefined,
        xcRTReplace = Constants.iRepairTypeReplace,
        xcRTRepair = Constants.iRepairTypeRepair,
        xcRTNew = Constants.iRepairTypeNew,
        xcRTRebuilt = Constants.iRepairTypeRebuilt
    }

    public enum cdsTableType
    {
        cdttUnspecified = Constants.iUndefined,
        cdttSystem = Constants.iCDTableTypeSystem,
        cdttSystemCode = Constants.iCDTableTypeSystemCode,
        cdttUserCode = Constants.iCDTableTypeUserCode,
        cdttEntityCode = Constants.iCDTableTypeEntityCode,
        cdttOrgHier = Constants.iCDTableTypeOrgHier,
        cdttSupplemental = Constants.iCDTableTypeSupp,
        cdttPeople = Constants.iCDTableTypePeople,
        cdttAdminTrack = Constants.iCDTableTypeAdminTrack,
        cdttJurisdic = Constants.iCDTableTypeJurisdic,
        cdttIndStdCodes = Constants.iCDTableTypeIndStdCodes,
        cdttGlossary = Constants.iCDTableTypeGlossary
    }

    public enum diaExpPP
    {
        expUnspecified = Constants.iDiaExpPPUnspecified,
        expPost = Constants.iDiaExpPPPost,
        expPay = Constants.iDiaExpPPPay
    }

    //Start SIW7076
    public enum SupplementalFieldTypes
    {
        SuppTypeText = 0,
        SuppTypeNumber = 1,
        SuppTypeCurrency = 2,
        suppTypeDate = 3,
        suppTypeTime = 4,
        SuppTypeCodeText = 5,
        SuppTypeCode = 6,
        SuppTypePrimaryKey = 7,
        SuppTypeEntity = 8,
        SuppTypeState = 9,
        SuppTypeClaimLookup = 10,
        SuppTypeFreeText = 11,
        SuppTypeEventLookup = 12,
        suppTypeTableLookup = 13,
        suppTypeUserLookup = 14,
        SuppTypeDateTimeStamp = 15,
        suppTypeMultiCode = 16,
        suppTypeBoolean = 17,
        suppTypeList = 18,
        SuppTypeCodeCombo = 19,
        SuppTypeSecondaryKey = 20,
        SuppTypeParentKey = 21
    }
    //End SIW7076
    // Start SIW7062
    public enum ControlType
    {
        TextBox = 0,
        RadComboBox = 1,
        RadDatePicker = 2,
        RadTimePicker = 3,
        CheckBox = 4,
        RadMaskedTextBox = 5,
        RadNumericTextBox = 6,
        RadEditor = 7,
	    RadioButtonList = 8,
	 	ListBox = 9	
    }
    // end SIW7062

    //SIN7404
    public enum ScriptDisplayType
    {
        ALERT = 1,
        CONFIRMATION = 2,
        LIST_ALERT = 3,
        LIST_CONFIRMATION = 4
    }

    public enum ScriptProcessingType
    {
        SELECT_CONTINUE = 1,
        SELECT_NOSELECT_CONTINUE = 2,
        STOP_PROCESS = 3,
        CONTINUE = 4
    }
    //SIN7404
    public class Constants
    {
        public Constants() { }

        //The following are taken from CCPUF.Constants
        public const string sResponseFileExt = "_RESP.XML";    // Add to end of file, used to process XML file.

        public const string sSwitchOutFile = "/OUTFILE";       // =output file
        public const string sSwitchOutPath = "/OUTPATH";       // =output path
        public const string sSwitchInFile = "/INFILE";         // =input file      
        public const string sSwitchInPath = "/INPATH";         // =input path      
        public const string sSwitchNoUpdate = "/NOUPDATE";     // /NOUpdate  Prevents DB Updates
        public const string sSwitchRerun = "/RERUN";           // /RERUN{=RqID}
        public const string sSwitchGuidLog = "/GUIDLOG";       // /GUIDLOG=path/file
        //SIN204 public const string sSwitchXMLInput = "/XML";          // /XML=filename
        //SIN204 public const string sSwitchInputPath = "/C";           // /C=path  To Be Replaced by INPATH
        public const string sSwitchLogPath = "/LOGPATH";       // /LOGPATH=path
        public const string sSwitchLogFile = "/LOGFILE";       // /LOGFILE=file
        public const string sSwitchLogRetry = "/LOGRETRY";     // /LOGRETRY=count        
        public const string sSwitchReport = "/REPORT";         // /REPORT{=path/file}
        public const string sSwitchParamFile = "/PARAMFILE";   // /PARAMFILE=parameterfile
        public const string sSwitchParamFileOW = "/PARAMFILEOVERWRITE";  // /PARAMFILEOVERWRITE=[Yes|No]

        public const string sSwitchNameParse = "/NAMEPARSE";
        public const string sSwitchLowerCase = ".LOWERCASE";
        public const string sSwitchUpperCase = ".UPPERCASE";
        public const string sSwitchPunctation = ".PUNCTUATION";

        //Start SIN204
        public const string sSwitchXMLInput = "/XML";
        public const string sSwitchInputPath = sSwitchInPath;
        public const string sSwitchDatabase = "/DATABASE";
        public const string sSwitchLogging = "/LOGGING";
        public const string sSwitchGUI = "/GUI";
        public const string sSwitchDataSource = "/DSN";
        public const string sSwitchUserID = "/USERNAME";
        public const string sSwitchPassword = "/PASSWORD";
        public const string sSwitchServer = "/SERVER";
        public const string sSwitchSilent = "/SILENT";
        public const string sSwitchDebug = "/DEBUG";
        public const string sSwitchInputFile = sSwitchInFile;
        public const string sSwitchDataProvider = "/DATAPROVIDER";
        public const string sSwitchWPAAssigningUser = "/WPAASSIGNINGUSER";
        public const string sSwitchWPAAssignedTo = "/WPAASSIGNEDTO";
        public const string sSwitchWPASend = "/WPASEND";
        public const string sSwitchWPARole = "/WPAROLE";
        public const string sSwitchLogRetention = "/LOGRETENTION";
        public const string sSwitchLogMaxSize = "/LOGMAXSIZE";
        public const string sSwitchLogNumBUps = "/LOGNUMBUPS";
        public const string sSwitchAppTitle = "/APPTITLE";
        public const string sSwitchProdID = "/PRODID";
        public const string sSwitchProdName = "/PRODNAME";
        public const string sSwitchCompany = "/COMPANY";
        public const string sSwitchDiscreteLog = "/DISCRETELOGGING";
        //End SIN204

        //SIN204 public const string sSwitchDatabase = "/D";            // /D=databasename
        //SIN204 public const string sSwitchLogging = "/L";             // /L=[Yes|No] Turn logging off or on
        //SIN204 public const string sSwitchGUI = "/G";                 // /G{=Yes|No} Enable/Disable GUI data entry
        //SIN204 public const string sSwitchDataSource = "/N";          // /N=Datasourcename
        //SIN204 public const string sSwitchUserID = "/U";              // /U=userid
        //SIN204 public const string sSwitchPassword = "/P";            // /P=password
        //SIN204 public const string sSwitchServer = "/S";              // /S=servername
        //SIN204 public const string sSwitchSilent = "/Q";              // /Q{=Yes|No}  Disable|Enable all screen messages
        //SIN204 public const string sSwitchDebug = "/Z";               // /Z{=Yes|No}
        //SIN204 public const string sSwitchInputFile = "/I";           // /I=Input File Name    To be replaced by INPATH
        //SIN204 public const string sSwitchDataProvider = "/V";        // /V=Database Provider
        public const string sSwitchDesignMode = "/DSGNMODE";   // /DSGNMODE=NO  Set design mode
        public const string sSwitchDumpParams = "/DUMPPARAMS"; // Dump all loaded parameters
        public const string sSwitchClearLog = "/CLEARLOG";     // /CLEARLOG
        public const string sSwitchComment = "/PARAMCOMMENTDELIM";   // in-line comment delimiter   
        public const string sSwitchClear = "/PARAMCLEAR";            // Clear parameters  
        public const string sSwitchCounter = "/PARAMCOUNTER";        // Immediatly sets value of internal counter
        public const string sSwitchRplcChar = "/PARAMREPLACECHAR";   // Defines in-line replacement character
        public const string sSwitchFuncChar = "/PARAMFUNCTIONCHAR";  // Defines in-line replacement character   
        public const string sSwitchFuncSplit = "/PARAMFUNCTIONSPLIT";// Defines Function split character        
        public const string sSwitchEncryptChar = "/PARAMENCRYPTCHAR";// Defines encryption character
        public const string sSwitchSpaceChar = "/PARAMSPACECHAR";   // Defines in-line Space character         
        public const string sSwitchCntrlChar = "/PARAMCNTRLCHAR";   // Defines Control character delimiters    
        public const string sSwitchLabelQual = "/PARAMLABEL";       // Specifies override label indicator      

        public const string sSwitchExecute = "/EXECUTE";            // Function to execute in an application
        public const string sSwitchExecuteAbortonErr = ".ABORTONERROR"; //Abort processing on error
        public const string sSwitchIndex = "/INDEX";                // INDEXing for batch processing
        public const string sSwitchPrinter = "/PRINTER";            // Printer Definitions
        public const string sSwitchPrinterName = ".NAME";           // Printer Name
        public const string sSwitchPrinterFile = ".FILE";           // Printer File Name
        public const string sSwitchPrinterBin = ".BIN";             // Paper bin
        public const string sSwitchIgnoreErrors = "/IGNOREERRORS";  //=[True|{False}] Ignore Errors
        public const string sSwitchStopOnCodeErrors = "/STOPONCODEERRORS";//=[True|{False}] Fail if a code lookup fails
        public const string sSwitchSaveParams = "/SAVEPARAMS";      //Save Parameters
        public const string sSwitchReadParams = "/READPARAMS";      //Read parameter files              
        public const string sSwitchNoRead = "/NOREAD";              //Do Not Read initialization file   
        public const string sSwitchNoSave = "/NOSAVE";              //Do Not Save initialization file   
        public const string sSwitchNoMigrate = "/DONOTMIGRATE";     //Do not translate old switches to new


        // Replaceable Parameters in Data Items
        public const string sRplcDate = "D";         // Replace with Current Date
        public const string sRplcTime = "T";         // Replace with Current Time
        public const string sRplcAppName = "N";      // Replace with Application Name
        public const string sRplcAppPath = "P";      // Replace with Application Path
        public const string sRplcAppVrsn = "V";      // Replace with Application Version
        public const string sRplcDatabase = "B";     // Replace with database
        public const string sRplcGUID = "U";         // Replace with a GUID
        public const string sRplcSwitch = "S";       // #Sswitchname#  Replace with value of switch
        public const string sRplcIncrement = "+";    // Increment internal counter and replace
        public const string sRplcDecrement = "-";    // Decrement internal counter and replace
        public const string sRplcCounter = "*";      // Replace with current value of counter
        public const string sRplcQuote = "Q";        // Replace with Single Quote
        public const string sRplcIniValue = "I";     // Ini file lookup #I&Group&Value&File#
        public const string sRplcCharacter = "C";    // #Cnnn# Replace nnn with character value  
        public const string sRplcASCII = "A";        // #Ac Replace c with its ASCII value       
        public const string sRplcWSName = "W";       // Replace with workstation name            
        public const string sRplcLangCode = "L";     // Replace with current language code       


        // Function Parameters in Data Items
        public const string sFunctionCharacter = "@";
        public const string sFuncFileName = "F";       // @Fdata@  Parse for File Name with extension
        public const string sFuncShortFileName = "S";  // @Sdata@  Parse for File Name without extension
        public const string sFuncFilePath = "P";       // @Pdata@  Parse for Path Name
        public const string sFuncFileType = "T";       // @Tdata@  Parse for File Type
        public const string sFuncYear = "Y";           // Returns the year portion of a date
        public const string sFuncMonth = "M";          // Returns the numeric month portion of a date
        public const string sFuncDay = "D";            // Returns the numeric day portion of a date
        public const string sFuncHour = "H";           // Returns the hour portion of a time
        public const string sFuncMinute = "N";         // Returns the minute portion of a time
        public const string sFuncSecond = "C";         // Returns the second portion of a time
        public const string sFuncRight = "R";          // Returns the right portion of a string
        public const string sFuncLeft = "L";           // Returns the left portion of a string
        public const string sFuncSubStr = "U";         // Returns a substring within a string
        public const string sFuncReplace = "E";        // Replaces a string with another in a function string


        public const string sQuote = "'";                // Single Quote
        public const string sCommentDelimiter = "'";     // Specifies the comment indicator in parameter files
        public const string sReplacementCharacter = "#";
        public const string sEncryptionCharacter = "~";  // Specifies an encrypted item
        public const string sFunctionSplitChar = ".";    // Separator for function parameters     
        public const string sSpaceCharacter = "^";       // Replaced in the parameter string with a space  
        public const string sCCStart = "{";              // Defines Control Character Replacement    
        public const string sCCEnd = "}";                // Defines Control Character Replacement    
        public const string sCRLF = "CRLF";              // Replace with vbCrLf             
        public const string sLF = "LF";                  // Replace with vbLF               
        public const string sCR = "CR";                  // Replace with vbCR               
        public const string sTAB = "TAB";                // Replace with vbTab              
        public const string sFF = "FF";                  // Replace with vbFf               

        public const string sLabelQualifier = ".LABEL";  // Right most characters indicates a parameter label
        public const string sDoNotIndex = ".NX";         // Right most characters indicates that parameter is not to be indexed


        // Key used to access Crypt Functions
        public const string CRYPTKEY = "6378b87457a5ecac8674e9bac12e7cd9";

        public const string sProperCaseExceptions = ":OF:AND:OR:THE:BUT:WITH:";
        public const string sUpperCaseExceptions = ":C/O:PO:PO.:";
        public const string sPunctuation = ".,?;:'*[]{}()-";
        public const string sBoolTrue = ":YES:Y:TRUE:T:1:+1:-1:+:POSITIVE:";
        public const string sBoolFalse = ":NO:N:FALSE:F:0:-:NEGATIVE:";

        // Ini File Constants

        public const string sIniProfileFile = "SystemSettings.dat";

        public const string sScrnWidth = "cx";
        public const string sScrnHeight = "cy";
        public const string sScrnLeft = "x";
        public const string sScrnTop = "y";
        public const string sScrnMinWidth = "mcx";
        public const string sScrnMinHeight = "mcy";


        public const string sColWidth = "x";
        public const string sColPosition = "Position";
        public const string sColSortKey = "SortKey";
        public const string sColSortOrder = "SortOrder";
        public const string sColSorted = "Sorted";

        public const string sIndxParameters = "()";  //Used to define an execution process

        public const int lLOGFILE_SIZE = 150000;                  //Default max log file size
        public const int iLOGFILE_RETN = 5;             //SI06023 //Default days to retain log files
        public const int iLOGFILE_NUMB = 2;                 //Default max log files to have
        public const string SYSSET_LOGFILE_SIZE = "MaxLogFileSize"; //[LOGGING] maximum file size
        public const string SYSSET_LOGFILE_NUMB = "MaxNumbFiles";   //[LOGGING] maximum number of log files
        public const string SYSSET_RETENTION = "Retention"; // SI06023

        //The following are taken from CCPUF.UniversalConstants
        public const string SYS_CCP_REG_KEY = @"SOFTWARE\CSC\CCP";
        public const string SYS_CCP_REG_VAL_LANGUAGE = "LANGUAGE";

        public const string SYS_DFLT_LANGUAGE_CODE = "1033";
        public const string SYS_DFLT_LANGUAGE = "English";
        public const string SYS_BATCH_PARMS_SW = "BatchParms";
        public const string SYS_BATCH_PARMS_DB = "WSDatabase";
        public const string SYS_BATCH_PARMS_UID = "WSUserid";
        public const string SYS_BATCH_PARMS_PWD = "WSPassword";
        public const string NO_DATE = "99991231";

        public const string ctLanguageCodes = "LANGUAGE_CODES"; //(SI03956 - Implemented in SI06333)
        public const int ERR_UNKNOWN = 1;
        public const int ERR_VB_RUN_TIME = 2;
        public const int ERR_PRINTER = 3;
        public const int ERR_GRID_CONTROL = 4;
        public const int ERR_OLE = 5;
        public const int ERR_COMMON_DIALOG = 6;
        public const int ERR_ODBC = 7;
        public const int ERR_DATA_ACCESS = 8;

        public const int IDCANCEL = 2;
        public const int IDRETRY = 4;

        public const string INSTALL_REG_KEY = "INSTALL";
        public const string INSTALL_REG_COMPANY = "Company";
        public const string INSTALL_REG_DIR = "Directory";
        public const string INSTALL_REG_FNOL = "FNOLPath";
        public const string INSTALL_REG_GROUP = "Group";
        public const string INSTALL_REG_NAME = "Name";
        public const string INSTALL_REG_SYS = "SystemFiles";

        public const string CACHE_OPTIONS = @"Options\Caching";
        public const string CACHE_ALWAYS = "Always";
        public const string CACHE_HIDE = "Hide";
        public const string CACHE_MINIMIZE = "Minimize";
        public const string CACHE_WARNING = "Warning";

        public const string APP_REG_OPTIONS = "Options";
        public const string APP_REG_OPTION_DIS_SPLASH = "DisableSplashScreens";

        public const string CACHING_OPTIONS = @"Options\Caching";
        public const string CACHING_ALWAYS = "Always";
        public const string CACHING_WARNING = "Warning";
        public const string CACHING_MINIMIZE = "Minimize";
        public const string CACHING_HIDE = "Hide";
        public const string CACHING_GLOSS_MINIMIZE = "GLOSSARYMinimize";
        public const string CACHING_GLOSS_HIDE = "GLOSSARYHide";
        public const string CACHING_CODES_MINIMIZE = "CODESMinimize";
        public const string CACHING_CODES_HIDE = "CODESHide";
        public const string CACHING_ENTITY_MINIMIZE = "ENTITYMinimize";
        public const string CACHING_ENTITY_HIDE = "ENTITYHide";
        public const string CACHING_STATES_MINIMIZE = "STATESMinimize";
        public const string CACHING_STATES_HIDE = "STATESHide";

        public const string REG_SCRIPTING = "Scripting";
        public const string REG_SCRIPT_COMMON_KEY = "CommonDisabled";//(SI05721 - Implemented in SI06333)
        public const string REG_SCRIPT_VAL_KEY = "ValidationDisabled";
        public const string REG_SCRIPT_INIT_KEY = "InitializationDisabled";
        public const string REG_SCRIPT_CALC_KEY = "CalculationDisabled";
        public const string REG_SCRIPT_BSAVE_KEY = "BeforeSaveDisabled";
        public const string REG_SCRIPT_ASAVE_KEY = "AfterSaveDisabled";
        public const string REG_SCRIPT_ALOAD_KEY = "AfterLoadDisabled";
        public const string REG_SCRIPT_ADELETE_KEY = "AfterDeleteDisabled";
        public const string REG_SCRIPT_BDELETE_KEY = "BeforeDeleteDisabled";
        public const string REG_SCRIPTREFRESHALWAYS = "RefreshScriptAlways";
        public const string REG_SCRIPTABORTONERRORS = "AbortOnErrors";//(SI06168 - Implemented in SI06333)
        public const string REG_SCRIPTDEBUGGING = "Debug";//(SI06168 - Implemented in SI06333)

        public const string REG_SYS_TRANSACTION = "TransactionsDisabled";

        public const string WPA_OPTIONS = @"Options\WPA";
        public const string WPA_LOAD_TO_DO = "ToDoListLoading";
        public const string WPA_MAIL_CHECKING = "NewMailChecking";
        public const string WPA_AUTO_DIARY = "AutoDiary";
        public const string WPA_EXPORT_SCHEDULE = "SchedulePlusExport";
        public const string WPA_MAIL = "MailIntegration";
        public const string WPA_SORT_SEQ = "DefaultSortSeq";
        public const string WPA_ACTIVE_RECORD = "ActiveRecordDiaries";
        public const string WPA_SHOW_ALL_DATE = "ShowAllDates";
        public const string WPA_CLAIMANT_LEVEL = "ClaimantLevelGeneration";
        public const string WPA_DFLT_ROLLABLE = "Rollable";
        public const string WPA_DFLT_ROUTABLE = "Routable";
        public const string WPA_DFLT_RPRTCMPLT = "NotifyComplete";
        public const string WPA_DFLT_RPRTOVRDU = "NotifyOverdue";
        public const string WPA_ITEM_COUNT = "ItemCount";
        public const int WPA_DEF_ITEM_COUNT = 200;
        public const string WPAPARAM = "WPA";                             //Format = /WPA.POSTy.x=
        //y=blank | SUCCESSFUL | ERROR
        //x=action or value
        public const string WPAPARAMPOST = ".POST";                       //Unconditional Post
        public const string WPAPARAMPOSTGOOD = ".POSTSUCCESSFUL";         //Post on Success
        public const string WPAPARAMPOSTERROR = ".POSTERROR";             //Post on Error
        public const string WPAPARAMPOSTTASK = ".TASK";                   //=Task Short Code
        public const string WPAPARAMPOSTTASKDESC = ".TASKDESC";           //=Task Description
        public const string WPAPARAMPOSTACTIVITY = ".ACTIVITY";           //=Activity ShortCode
        public const string WPAPARAMPOSTACTIVITYDESC = ".ACTIVITYDESC";   //=Activity Description
        public const string WPAPARAMPOSTUSER = ".USER";                   //=UserID
        public const string WPAPARAMPOSTMESSAGE = ".MESSAGE";             //=Message
        public const string WPAPARAMPOSTAUTOTASK = ".AUTOTASK";           //=AutoTasking Code

        public const string COLOR_OPTIONS = @"Options\Colors";
        public const string COLOR_BACKGRND = "Background";
        public const string COLOR_FOREGRND = "Foreground";
        public const string COLOR_REQFIELDS = "ReqFields";
        public const string COLOR_CUSTLABEL = "CustLabel";
        public const string COLOR_BUTTONBACK = "ButtonsBackground";
        public const string COLOR_BUTTONFORE = "ButtonsForeground";
        public const string COLOR_ENTRYFORE = "EntryFieldsForeground";
        public const string COLOR_ENTRYBACK = "EntryFieldsBackground";
        public const string COLOR_OPTIONBACK = "OptionButtonsBackground";
        public const string COLOR_LABELBACK = "LabelBackground";
        public const string COLOR_LABELFORE = "LabelForeground";
        public const string COLOR_LISTBACK = "ListBackground";
        public const string COLOR_LISTFORE = "ListForeground";

        public const int ERROR_TYPE_VALIDATE = 1000;
        public const int ERROR_TYPE_OPERATION = 2000;
        public const int ERROR_TYPE_DATAENTRY = 3000;
        public const int ERROR_TYPE_FIELD_CAPTION = 40000;
        public const int ERROR_TYPE_FIELD_HELP = 40001;
        public const int ERROR_TYPE_FIELD_TAG = 40002;
        public const int ERROR_TYPE_FIELD_TOOLTIP = 40003;
        public const int ERROR_TYPE_MNU_ITEM = 40004;
        public const int ERROR_TYPE_COMP_NAME = 40005;
        public const int ERROR_TYPE_SCRIPT = 9000;

        public const int SCRIPT_EXECUTION_ERROR = 29899;
        public const string SCRIPT_PROC_NOTFOUND = "80020006";

        public const int iUndefined = -1;
        public const string sUndefined = "UNDEFINED";

        public const int cXCFIRST = 0;
        public const int cXCLAST = 3;
        public const int cXCNEXT = 1;
        public const int cXCPREV = 2;
        public const int cXCCURR = 4;

        public const string sUCPayTypeUnspecified = "UNSPECIFIED";
        public const string sUCPayTypeSalary = "SALARY";
        public const string sUCPayTypeHourly = "HOURLY";
        public const int iUCPayTypeUnspecified = 0;
        public const int iUCPayTypeSalary = 1;
        public const int iUCPayTypeHourly = 2;

        public const string sUCPayPeriodUnspecified = "UNSPECIFIED";
        public const string sUCPayPeriodHourly = "HOURLY";
        public const string sUCPayPeriodWeekly = "WEEKLY";
        public const string sUCPayPeriodBiWeekly = "BIWEEKLY";
        public const string sUCPayPeriodSemiMonthly = "SEMIMONTHLY";
        public const string sUCPayPeriodMonthly = "MONTHLY";
        public const string sUCPayPeriodQuarterly = "QUARTERLY";
        public const string sUCPayPeriodAnnual = "ANNUAL";
        public const int iUCPayPeriodUnspecified = 0;
        public const int iUCPayPeriodHourly = 1;
        public const int iUCPayPeriodWeekly = 2;
        public const int iUCPayPeriodBiWeekly = 3;
        public const int iUCPayPeriodSemiMonthly = 4;
        public const int iUCPayPeriodMonthly = 5;
        public const int iUCPayPeriodQuarterly = 6;
        public const int iUCPayPeriodAnnual = 7;

        public const string conCTVerbiageNLSSupport = "NLS_TRANSLATE";

        public const string ROLE_X_COMPONENT = "RXC";
        public const string CLAIMANT_CODE = "CLT";
        public const string ADJUSTER_CODE = "ADJ";
        public const string PAYEE_CODE = "PAYEE";
        public const string MAILTO_CODE = "MAILTO";
        public const string CLAIM_TABLE = "CLAIM";
        public const string EVENT_TABLE = "EVENT";
        public const string ENTITY_TABLE = "ENTITY";

        public const string SYSSET_DEFAULT = "DEFAULT";
        public const string SYSSET_CAPTIONS = "Captions";
        public const string SYSSET_SPLASH = "SplashScreens";
        public const string SYSSET_GRAPHICS = "Graphics";
        public const string SYSSET_PICTURE = "Picture";
        public const string SYSSET_SUPPORT = "Support";
        public const string SYSSET_LOGGING = "LOGGING";
        public const string SYSSET_LOGGINGRETRY = "LOGGINGRETRY";    //SI05869
        public const string SYSSET_GUIDLOGFILE = "GUIDLOGFILE";
        public const string SYSSET_LOGFILE = "LOGFILE";
        public const string SYSSET_LOGPATH = "LOGPATH";
        public const string SYSSET_USERLOG = "USERLOG";              //SI06335
        public const string SYSSET_DEBUGGING = "DEBUGGING";
        public const string SYSSET_DSGNMODE = "DSGNMODE";
        public const string SYSSET_DISCRETELOG = "DISCRETELOGGING";
        public const string SYSSET_HELPFILE = "HelpFile";
        public const string SYSSET_COMPANY = "Company";
        public const string SYSSET_PRODUCTID = "ProductID";
        public const string SYSSET_AUTOENCRYPT = "AutoEncrypt";
        public const string conPRODUCTID = "AC";
        public const string SYSSET_PGMNAMES = "Programs";
        public const string SYSSET_USERID = "WSUserid";
        public const string SYSSET_PASSWORD = "WSPassword";


        public const string SYSSET_TYPE_DEFAULT = "Default";
        public const string SYSSET_TYPE_OPTION = "Options";
        public const string SYSSET_ITEM_WPA_PROCESS = "WPAProcessing";

        public const string SYSSET_CLASSNAME = "ClassName";  //Universal Class Name Component

        public const string DM_SYSSET_TYPE_CLASSNAME = "DocManagement";
        public const string DM_CLASSNAME = "CCPDM.CDocManagement";

        public const string MM_SYSINFO = "MailMerge";
        public const string MM_FILE8BYTES = "FileName8Bytes";

        public const int UC_cdtString = 0;
        public const int UC_cdtInteger = 1;
        public const int UC_cdtLong = 2;
        public const int UC_cdtDecimal = 3;
        public const int UC_cdtDouble = 4;
        public const int UC_cdtCurrency = 5;
        public const int UC_cdtDate = 6;
        public const int UC_cdtBool = 7;
        public const int UC_cdtByte = 8;
        public const int UC_cdtSingle = 9;
        public const int UC_cdtTime = 10;
        public const int UC_cdtDTTM = 11;

        public const string SYSSET_MEASUREMENT = "Measurements";
        public const string SYSSET_MEASUREMENT_SAE = "SAE";
        public const string SYSSET_MEASUREMENT_METRIC = "METRIC";

        public const string POUNDS = "LBS";
        public const string KILOGRAMS = "KGS";
        public const string GRAMS = "G";
        public const string TONS = "TONS";
        public const string METRICTONS = "MTONS";
        public const string OUNCES = "OZS";

        public const string FOOT = "FT";
        public const string YARD = "YD";
        public const string INCH = "IN";
        public const string MILE = "MILE";
        public const string METER = "M";
        public const string KILOMETER = "KM";
        public const string CENTIMETER = "CM";
        public const string MILIMETER = "MM";

        //The following are XC Constants

        public const string BLANK = "[B]";  //SIW139

        public const string sSecSecurity = "SECURITY";
        public const string sSecGroups = "GROUPS";
        public const string sSecGroup = "GROUP";
        public const string sSecGroupNextID = "NEXTID";
        public const string sSecGroupCount = "COUNT";
        public const string sSecGroupID = "ID";
        public const string sSecGroupName = "NAME"; //SIW7049
        public const string sSecGroupIDPfx = "GRP";

        public const string sSecDSNs = "DSNS";
        public const string sSecDSN = "DSN";
        public const string sSecDSNCount = "COUNT";
        public const string sSecDSNName = "NAME";

        //Start SI06321
        public const string sSecUsers = "USERS";
        public const string sSecUserCount = "COUNT";
        public const string sSecUser = "USER";
        public const string sSecUserLogin = "LGIN";
        public const string sSecUserID = "ID";
        public const string sSecManagerID = "MANAGER_ID";  //SIW7332
        public const string sSecUserName = "NAME";
        public const string sSecUserNameFirst = "FIRST";
        public const string sSecUserNameLast = "LAST";
        //End SI06321
        // Start SIW7341 
        public const string sSecUserEmailAddr = "EMAIL_ADDR";
        // End SIW7341 
        //Start SI06320
        public const string sSecSuppDBConnString = "SUPP_DB_CONN_STRING";
        public const string sSecSuppDBConnStringValue = "VALUE";
        //End SI06320
        //Standard Definitions
        public const string sStdComment = "COMMENT";      //(SI06023 - Implemented in SI060333)
        public const string sStdTechKey = "TECH_KEY";      //SIW360

        public const string sStdTreeLabel = "TREE_LABEL";  //SIW490
        //Start SIW7036
        public const string sStdFinancialActivity = "FINANCIAL_ACTIVITY";
        public const string sStdIsFrozen = "ISFROZEN";
        public const string sStdIsOnHold = "ISONHOLD";
        public const string sStdIsSchedule = "ISSCHEDULE";
        public const string sStdIsPayment = "ISPAYMENT";
        public const string sStdIsCollection = "ISCOLLECTION";
        //End SIW7036
        public const string sStdViewPayment = "VIEWPAYMENT";   //SIW7647
        public const bool xcGetFirst = true;    //SI06420
        public const bool xcGetNext = false;    //SI06420
        public const String Culturetype = "en-GB";//SIW8235

        /*'<CLAIM ID="CLMxxx" EVENT="EVTxxx">
'   <CLAIM_NUMBER>00000000000065</CLAIM_NUMBER>
'   <NEW_CLAIM_NUMBER>offset_onset Claim Number</NEW_CLAIM_NUMBER>
'   <FILE_NUMBER>file number</FILE_NUMBER>
'   <LINE_OF_BUSINESS CODE="WCV">Workers Comp Voluntary</LINE_OF_BUSINESS>
'   <CLAIM_TYPE CODE="LTI">Lost Time</CLAIM_TYPE>
'   <DATE_OF_LOSS YEAR="2002" MONTH="03" DAY="01" />
'   <TIME_OF_LOSS HOUR="17" MINUTE="35" SECOND="04" />
'   <DATE_DISCOVERED YEAR="2002" MONTH="03" DAY="07" />            
'   <DATE_REPORTED YEAR="2002" MONTH="03" DAY="07" />
'   <TIME_REPORTED HOUR="17" MINUTE="35" SECOND="04" />
'   <DATE_RPTD_TO_RM YEAR="2002" MONTH="03" DAY="07" />     'SIW7855
'   <DATE_ENTERED YEAR="2002" MONTH="03" DAY="07" />
'   <TIME_ENTERED HOUR="17" MINUTE="35" SECOND="04" />
'   <DATE_CLOSED YEAR="2002" MONTH="03" DAY="07" />
'   <TIME_CLOSED HOUR="17" MINUTE="35" SECOND="04" />
'   <DATE_DESTROYED YEAR="2002" MONTH="03" DAY="07" />                
'   <DESTROYED INDICATOR="True|False"/>                               
'   <FREEZE_PAYMENTS INDICATOR="True|False"/>                   'SI06358
'   <FINANCIAL_ACTIVITY ISFROZEN='True|False'/> 'SIW7036
'   <SELF_INSURED INDICATOR="True|False"/>                               
'   <SPECIAL_HANDLING INDICATOR ="True|False"/>                          
'   <CLOSE_METHOD CODE="***">closed method description</CLOSE_METHOD>
'   <HOME_OFFICE_REVIEW_DATE YEAR="2002" MONTH="03" DAY="07" />
'   <SERVICE_CODE CODE="EST">Eastern Office</CLAIM_TYPE>
'   <ACCIDENT_DESCRIPTION CODE="**">accident description</ACCIDENT_DESCRIPTION>
'   <SEVERITY CODE="*" ID="****">Severity_type</SEVERITY>           'SIW7455
'   <ACCIDENT_TYPE CODE="**">accident type description</ACCIDENT_TYPE>
'   <AIA CODE='aiacode'>                                           
'      <AIA1 CODE='AIA1'>description</AIA1>                        
'      <AIA2 CODE='AIA2'>description</AIA2>                        
'      <AIA3 CODE='AIA3'>description</AIA3>                        
'   </AIA>                                                         
'   <CLAIM_STATUS CODE="O">Open</CLAIM_STATUS>
'   <ACCIDENT_STATE CODE="NC">North Carolina</ACCIDENT_STATE>
'   <FILING_STATE CODE="NC">North Carolina</FILING_STATE>
'   <JURISDICTION_STATE CODE="NC">North Carolina</JURISDICTION_STATE>
'   <INDEX_BUREAU_REQUEST>cibrequestflag</INDEX_BUREAU_REQUEST>
'   <SEC_DEPT_EID ENTITY_ID="ENTXXX" DISPLAY_NAME="ABC - XYZ"></SEC_DEPT_EID> '  SI7021 'SIW7896 'SIN8499
'   <AUTHORITY_CONTACTED>
'     <AUTHORITY>authority contacted</AUTHORITY>
'     <REPORT_NUMBER>report number</REPORT_NUMBER>
'     <STATE_DOT_NOTIFIED_DATE YEAR="2002" MONTH="03" DAY="07" />
'     <STATE_DOT_REPORT_NUMBER>report number</REPORT_NUMBER>
'     <FEDERAL_DOT_NOTIFIED_DATE YEAR="2002" MONTH="03" DAY="07" />
'   </AUTHORITY_CONTACTED>
'   <PREVIOUSLY_REPORTED INDICATOR="YES|NO"/>
'   <STATUS_HISTORY>                                                 'Start SIW7327
'      <STATUS_CHANGE>
'          <!-- Multiple history lines allowed -->
'          <TECH_KEY>technical key</TECH_KEY>
'          <HIST_CLAIM_STATUS CODE="O">Open</CLAIM_STATUS>
'          <CHANGE_REASON CODE="x" CODE_ID="id">reason text</CHANGE_REASON>
'          <DATE_CHANGED YEAR="2002" MONTH="03" DAY="07" />
'          <CHANGED_BY_USER USER_NAME="login name">user name</CHANGED_BY_USER>
'          <APPROVED_BY_USER USER_NAME="login name">user name</APPROVED_BY_USER>
'      </STATUS_CHANGE>
'   </STATUS_HISTORY>                                                'End SIW7327
'   <PARTY_INVOLVED ID="ENTxxx" CODE="Role">Role Description</PARTY_INVOLVED>
'        <!--Muliple Party Involved nodes allowed -->
'   <COMMENT_REFERENCE ID="CMTxx"/>
'        <!-- multiple entries -->
'   Start SIN8237
'   <ADDRESS_REFERENCE ID="ARDid  References an address in the Addresses Collection {id}"
'                      CODE="Type of Address Code (Billing Mailing, Home, etc.) {string} [ADDRESS_TYPE]">Address Type Translated {string}</ADDRESS_REFERENCE>
'   End SIN8237
'   <POLICY> .... </POLICY>
'   <UNIT_STAT_DCI> .... </UNIT_STAT_DCI>N
'   <INJURY INJURY_ID="INJxxx">  .....  </INJURY>
'   <INVOLVED_UNITS> ... </INVOLVED_UNITS>
'   <VEHICLE_LOSS> ... </VEHICLE_LOSS>
'   <PROPERTY_LOSS> ... </PROPERTY_LOSS>
'   <LITIGATION> .... </LITIGATION>
'   <SUBROGATION> .... </SUBROGATION>
'   <ARBITRATION> .... </ARBITRATION>
'   <JURISDICTIONAL_DATA> .... </JURISDICTIONAL_DATA>
'   <DEMAND_OFFER> ... </DEMAND_OFFER>
'   <LOSS_DETAIL> .... </LOSS_DETAIL>
'   <INVOICE_SUMMARY> .... </INVOICE_SUMMARY>
'   <INVOICE> .... </INVOICE>
'   <DATA_ITEM> ... </DATA_ITEM>
'   <TRIGGERS> ... </TRIGGERS>                                  
'   <MEDICARE> ... </MEDICARE>          'SIW7214
'   <VERSION_NUMBER>D</VERSION_NUMBER>  'SIW7388
'</CLAIM>*/
        public const string sClmNode = "CLAIM";
        public const string sClmID = "ID";
        public const string sClmIDPfx = "CLM";
        public const string sClmEventID = "EVENT";
        public const string sClmEventIDPfx = "EVT";
        public const string sClmNumber = "CLAIM_NUMBER";
        public const string sClmNewNumber = "NEW_CLAIM_NUMBER";
        public const string sClmFileNumber = "FILE_NUMBER";
        public const string sClmType = "CLAIM_TYPE";
        public const string sClmLOB = "LINE_OF_BUSINESS";
        //start SIW7455
        public const string sClmSeverity = "SEVERITY";
        //end SIW7455
        public const string sClmDOL = "DATE_OF_LOSS";
        public const string sClmTOL = "TIME_OF_LOSS";
        public const string sClmDRPT = "DATE_REPORTED";
        public const string sClmTRPT = "TIME_REPORTED";
        public const string sClmDRPTtoRM = "DATE_RPTD_TO_RM";               //SIW7855
        public const string sClmDateDiscovered = "DATE_DISCOVERED";
        public const string sClmDENT = "DATE_ENTERED";
        public const string sClmTENT = "TIME_ENTERED";
        public const string sClmDCLSD = "DATE_CLOSED";
        public const string sClmTCLSD = "TIME_CLOSED";
        public const string sClmDtDstryed = "DATE_DESTROYED";
        public const string sClmDstryed = "DESTROYED";
        public const string sFreezePayments = "FREEZE_PAYMENTS";            //SIW11b



        public const string sClmPreparedByUser = "PREPARED_BY_USER";
public const string sClmUpdatedByUser = "UPDATED_BY_USER";
public const string sClmDttmAdded = "DTTM_ADDED";
public const string sClmDttmUpdated = "DTTM_UPDATED";

public const string sEvePreparedByUser = "PREPARED_BY_USER";
public const string sEveUpdatedByUser = "UPDATED_BY_USER";
public const string sEveDttmAdded = "DTTM_ADDED";
public const string sEveDttmUpdated = "DTTM_UPDATED";

public const string sRsvPreparedByUser = "PREPARED_BY_USER";
public const string sRsvUpdatedByUser = "UPDATED_BY_USER";
public const string sRsvDttmAdded = "DTTM_ADDED";
public const string sRsvDttmUpdated = "DTTM_UPDATED";

public const string sClaimantStatus = "CLAIMANT_STATUS";


public const string sSubroPreparedByUser = "PREPARED_BY_USER";
public const string sSubroUpdatedByUser = "UPDATED_BY_USER";
public const string sSubroDttmAdded = "DTTM_ADDED";
public const string sSubroDttmUpdated = "DTTM_UPDATED";

public const string sLitPreparedByUser = "PREPARED_BY_USER";
public const string sLitUpdatedByUser = "UPDATED_BY_USER";
public const string sLitDttmAdded = "DTTM_ADDED";
public const string sLitDttmUpdated = "DTTM_UPDATED";

public const string sPolPreparedByUser = "PREPARED_BY_USER";
public const string sPolUpdatedByUser = "UPDATED_BY_USER";
public const string sPolDttmAdded = "DTTM_ADDED";
public const string sPolDttmUpdated = "DTTM_UPDATED";

public const string sCovPreparedByUser = "PREPARED_BY_USER";
public const string scovUpdatedByUser = "UPDATED_BY_USER";
public const string sCovDttmAdded = "DTTM_ADDED";
public const string sCovDttmUpdated = "DTTM_UPDATED";
//public const stringsEvePreparedByUser = "PREPARED_BY_USER";
public const string sfundsUpdatedByUser = "UPDATED_BY_USER";
public const string sFundsDttmAdded = "DTTM_ADDED";
public const string sFundsDttmUpdated = "DTTM_UPDATED";

public const string sFundsApproveUser = "APPROVE_USER";
public const string sFundsDttmApproval = "APPROVE_USER";

public const string sFtsPreparedByUser = "FTS_PREPARED_BY_USER";
public const string sFtsUpdatedByUser = "FTS_UPDATED_BY_USER";
public const string sFtsDttmAdded = "FTS_DTTM_ADDED";
public const string sFtsDttmUpdated = "FTS_DTTM_UPDATED";

public const string sFiled1099Flag = "FILED_1099_FLAG";


        public const string sClmSelfInsrd = "SELF_INSURED";
        public const string sClmSplHndlng = "SPECIAL_HANDLING";
        public const string sClmLossLocZip = "LOSS_LOC_ZIP";
        public const string sClmCLSMethod = "CLOSE_METHOD";
        public const string sClmDHORvw = "HOME_OFFICE_REVIEW_DATE";
        public const string sClmServiceCode = "SERVICE_CODE";
        public const string sClmAccidentDesc = "ACCIDENT_DESCRIPTION";
        public const string sClmAccidentType = "ACCIDENT_TYPE";
        public const string sClmAIA =  "AIA";
        public const string sClmAIA1 = "AIA1";
        public const string sClmAIA2 = "AIA2";
        public const string sClmAIA3 = "AIA3";
        public const string sClmStatus = "CLAIM_STATUS";
        public const string sClmAccidentState = "ACCIDENT_STATE";
        public const string sClmFilingState = "FILING_STATE";
        public const string sClmJurisdictionState = "JURISDICTION_STATE";
        public const string sClmCIBRequest = "INDEX_BUREAU_REQUEST";
        public const string sClmSecDept = "SEC_DEPT_EID";                      //SIW7896
        //SIN8499 public const string sClmSecDeptEntDisplayName = "ENT_DISPLAY_NAME";    //SIW7896
        public const string sClmAuthorityCont = "AUTHORITY_CONTACTED";
        public const string sClmACAuthority = "AUTHORITY";
        public const string sClmACReportNumber = "REPORT_NUMBER";
        public const string sClmACStDOTNotDate = "STATE_DOT_NOTIFIED_DATE";
        public const string sClmACStDOTRprtNumber = "STATE_DOT_REPORT_NUMBER";
        public const string sClmACFedDOTNotDate = "FEDERAL_DOT_NOTIFIED_DATE";
        public const string sClmPreviouslyReported = "PREVIOUSLY_REPORTED";
        public const string sClmPartyInvolved = "PARTY_INVOLVED";
        public const string sClmComment = "COMMENT_REFERENCE";
        public const string sClmPolicy = "POLICY";
        public const string sClmUnitStats = "UNIT_STAT_DCI";
        public const string sClmInjury = "INJURY";
        public const string sClmInvolvedUnits = "INVOLVED_UNITS";
        public const string sClmVehLoss = "VEHICLE_LOSS";
        public const string sClmPropLoss = "PROPERTY_LOSS";
        public const string sClmLitigation = "LITIGATION";
        public const string sClmSubrogation = "SUBROGATION";
        public const string sClmArbitration = "ARBITRATION";
        public const string sClmJurisdictionalData = "JURISDICTIONAL_DATA";
        public const string sClmInvSummary = "INVOICE_SUMMARY";
        public const string sClmInvoice = "INVOICE";
        public const string sClmDemandOffer = "DEMAND_OFFER";
        public const string sClmLossDetail = "LOSS_DETAIL";
        public const string sClmDataItem = "DATA_ITEM";
        public const string sClmDoc = "DOCUMENT";
        public const string sClmTrigger = "TRIGGERS";
        public const string sClmMedicare = "MEDICARE";        //SIW7214  
        public const string sClmLiabLoss = "LIABILITY_LOSS";  //SIW344
        public const string sClmVersionNumber = "VERSION_NUMBER"; //SIW7388
        public const string sDistributionType = "DISTRIBUTION_TYPE"; //SIW7928
        //Start SIW7327
        public const string sClmSHNode = "STATUS_HISTORY";
        public const string sClmSHHistItem = "STATUS_CHANGE";
        public const string sClmSHStatus = "HIST_CLAIM_STATUS";
        public const string sClmSHReason = "CHANGE_REASON";
        public const string sClmSHDateChanged = "DATE_CHANGED";
        public const string sClmSHChangedBy = "CHANGED_BY_USER";
        public const string sClmSHApprovedBy = "APPROVED_BY_USER";
        public const string sClmSHHistItemCount = "COUNT";
        public const string sClmTotalClaimReserveBalance = "TOTAL_CLAIM_RESERVE_BALANCE";//vkumar258
        public const string sLDUnitNumber = "UNIT_NUMBER";//Payal
        public const string sLDRiskType = "RISK_TYPE";//Payal
        //End SIW7327
        //vkumar258
        public const string sIncurredAmount = "INCURRED";
        public const string sPaidAmount = "TOTAL_PAID";
        public const string sBranch = "BRANCH";
        public const string sBranchCode = "CODE";
        public const string sEntity = "ENTITY";
        public const string sEntityCode = "ENTITY_CODE";
        public const string sEntityID = "ENTITY_ID";
        public const string sEntityType = "TYPE";
        //vkumar258 
        //Payal Starts -- RMA:7484
        public const string sClaimModificationNode = "CLAIM_MODIFICATION";
        public const string sCMActivityType = "ACTIVITY_TYPE";
        public const string sCMActivityTypeCode = "CODE";
        public const string sCMDateOfTransaction = "DATE_OF_TRANSACTION";
        public const string sCMTimeOfTransaction = "TIME_OF_TRANSACTION";
        //Payal Ends -- RMA:7484

        //public const string sClmTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<EVENT ID="EVTxx">;
        '   <!--Event is an optional level that can relate multiple claims to the same event-->
        '   <NUMBER>Specific Number or 'GENERATE' to generate an event number {string}</NUMBER>
        '   <TECH_KEY>AC Technical key for the element </TECH_KEY>     'SIW321
        '   <DESCRIPTION>Description of the Event {string}</DESCRIPTION>
        '   <DESCRIPTION_HTML>Description of the Event {string}in HTML format</DESCRIPTION_HTML>  //SI06369//SIW10
        '   <DATE YEAR="2002" MONTH="03" DAY="07" />
        '   <TIME HOUR="17" MINUTE="35" SECOND="04" />
        '   <DATE_REPORTED YEAR="2002" MONTH="03" DAY="07" />
        '   <TIME_REPORTED HOUR="17" MINUTE="35" SECOND="04" />
        '   <TYPE CODE="xx">Event Type</TYPE>
        '   <CATASTROPHE INTERNAL_CODE="xx" REPORTING_CODE="xx">Catastrophe</CATASTROPHE>
        '   <ORG_DEPT_EID ENTITY_ID="ENTXXX"></ORG_DEPT_EID> 'SIN8529
        '   <FICO CODE="xx">FICO</FICO>
        '   <STATUS CODE="O">Open</STATUS>
        '   <ON_PREMISES INDICATOR="YES|NO"/>
        '   <INDICATOR CODE="code">Indicator</INDICATOR>
        '   <NUMBER_OF_INJURIES>number</NUMBER_OF_INJURIES>
        '   <NUMBER_OF_FATALATIES>number</NUMBER_OF_FATALATIES>
        '   <LOCATION_DESCRIPTION>Area Description {string} [ADDRESS_TYPE]</LOCATION_DESCRIPTION>
        '   <LOC_DESC_HTML>Area Description {string} [ADDRESS_TYPE] in HTML format</LOC_DESC_HTML> //SI06369//SIW10
        '   <PRIMARY_LOCATION CODE="code">Primary Location</PRIMARY_LOCATION>
        '   <LOCATION_TYPE CODE="loctype">Location Type</TYPE>
        '   <ADDRESS_REFERENCE ID="ARDid  References an address in the Addresses Collection {id}"
        '                      CODE="Type of Address Code (Billing Mailing, Home, etc.) {string} [ADDRESS_TYPE]">Address Type Translated {string}</ADDRESS_REFERENCE>
        '   <PARTY_INVOLVED ID="ENTxxx" CODE="Role">Role Description</PARTY_INVOLVED>
        '        <!--Muliple Party Involved nodes allowed -->
        '   <COMMENT_REFERENCE ID="CMTxx"/>
        '        <!-- multiple entries -->
        '   <COUNTY_OF_ORIGIN CODE="xx">County of Origin</COUNTY_OF_ORIGIN>
        '</EVENT>*/
        public const string sEvtNode = "EVENT";
        public const string sEvtId = "ID";
        public const string sEvtIDPfx = "EVT";
        public const string sEvtNumber = "NUMBER";
        public const string sEvtDesc = "DESCRIPTION";
        public const string sEvtDescHTML = "DESCRIPTION_HTML";       //SI06369//SIW10
        public const string sEvtDate = "DATE";
        public const string sEvtTime = "TIME";
        public const string sEvtReportedDate = "DATE_REPORTED";
        public const string sEvtReportedTime = "TIME_REPORTED";
        public const string sEvtType = "TYPE";
        public const string sEvtCatastrophe = "CATASTROPHE";
        public const string sEvtCatInternal = "INTERNAL_CODE";
        public const string sEvtCatBureau = "BUREAU_CODE";          //SIN7077
        public const string sEvtCatReporting = "REPORTING_CODE";
        public const string sEvtFICO = "FICO";
        public const string sEvtStatus = "STATUS";
        public const string sEvtOnPremises = "ON_PREMISES";
        public const string sEvtIndicator = "INDICATOR";
        public const string sEvtNumOfInj = "NUMBER_OF_INJURIES";
        public const string sEvtNumOfFat = "NUMBER_OF_FATALATIES";
        public const string sEvtLocDesc = "LOCATION_DESCRIPTION";
        public const string sEvtLocDescHTML = "LOC_DESC_HTML";     //SI06369 //SIW10
        public const string sEvtLocPrimary = "PRIMARY_LOCATION";
        public const string sEvtLocType = "LOCATION_TYPE";
        public const string sEvtAdrRef = "ADDRESS_REFERENCE";
        public const string sEvtLocAdrRefID = "ID";
        public const string sEvtLocAdrRefIDPfx = "ADR";
        public const string sEvtPartyInvolved = "PARTY_INVOLVED";
        public const string sEvtComment = "COMMENT_REFERENCE";
        public const string sEvtCountyOfOrigin = "COUNTY_OF_ORIGIN";
        public const string sEvtOrgDept = "ORG_DEPT_EID"; //SIN8529
        //public const string sEvtTechKey = "TECH_KEY"; //(SI06023 - Implemented in SI06333)//SIW360

        /*'<CLAIM>
        '   <POLICY ID="POL143" VERIFIED="boolean" PRIMARY="boolean">
        '      <POLICY_NUMBER LOCATION="00" MODULE="01" SYMBOL="WCV">0003290</POLICY_NUMBER>
        '      <MASTER_COMPANY CODE="05">MYND</MASTER_COMPANY>
        '      <EFFECTIVE_DATE YEAR="2001" MONTH="12" DAY="01" />
        '      <EXPIRATION_DATE YEAR="2002" MONTH="08" DAY="01" />
        '      <RETRO_DATE YEAR="2001" MONTH="12" DAY="01" />                  
        '      <TAIL_DATE YEAR="2001" MONTH="12" DAY="01" />                   
        '      <CANCELLATION_DATE YEAR="2002" MONTH="08" DAY="01" />
        '      <LINE_OF_BUSINESS CODE="WCV">Workers Comp Voluntary</LINE_OF_BUSINESS>
        '   <ORG_DEPT_EID ENTITY_ID="ENTXXX"></ORG_DEPT_EID> 'SIN8529
        '      <BRANCH CODE="00"></BRANCH>
        '      <AGENT_NUMBER>1500681</AGENT_NUMBER>
        '      <SUB_AGENT>dskdj</SUB_AGENT>
        '      <REINSURANCE ASSIGNED="boolean"/>
        '      <LEGACY_KEY>Policy Key from Host system</LEGACY_KEY>  SIN7206
        '      <LIMIT> ...... </LIMIT>
        '        <!-- Multiple Limits -->
        '      <DEDUCTIBLE> ...... </DEDUCTIBLE>
        '        <!-- Multiple Deductibles -->
        '      <INSURED_UNIT> ....  </INSURED_UNIT>
        '      <ENDORSEMENT> ... <ENDORSEMENT>
        '      <PARTY_INVOLVED ID="ENTxxx" CODE="Role">Role Description</PARTY_INVOLVED>
        '        <!--Muliple Party Involved nodes allowed -->
        '      <COMMENT_REFERENCE ID="CMTxx"/>
        '        <!-- multiple entries -->
        '      <DATA_ITEM> ... </DATA_ITEM>
        '      <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333)
        '      <ISSUE_SYSTEM_ID CODE="xx">Issuing System</ISSUE_SYSTEM_ID>
        '   </POLICY>
        '</CLAIM>*/

        public const string sPolNode = "POLICY";
        public const string sPolID = "ID";
        public const string sPolIDPfx = "POL";
        public const string sPolVerified = "VERIFIED";
        public const string sPolPrimary = "PRIMARY";
        public const string sPolNumber = "POLICY_NUMBER";
        public const string sPolLocation = "LOCATION";
        public const string sPolModule = "MODULE";
        public const string sPolSymbol = "SYMBOL";
        public const string sPolMasterCo = "MASTER_COMPANY";
        public const string sPolEffDate = "EFFECTIVE_DATE";
        public const string sPolExpDate = "EXPIRATION_DATE";
        public const string sPolCanDate = "CANCELLATION_DATE";
        public const string sPolLOB = "LINE_OF_BUSINESS";
        public const string sPolBranch = "BRANCH";
        public const string sPolAgentNumber = "AGENT_NUMBER";
        public const string sPolSubAgentNumber = "SUB_AGENT";
        public const string sPolReinsurance = "REINSURANCE";
        public const string sPolLegacyKey = "LEGACY_KEY"; //SIN7206
        public const string sPolReinAssigned = "ASSIGNED";
        public const string sPolLimit = "LIMIT";
        public const string sPolDeductible = "DEDUCTIBLE";
        public const string sPolPartyInvolved = "PARTY_INVOLVED";
        public const string sPolComment = "COMMENT_REFERENCE";
        public const string sPolUnit = "INSURED_UNIT";
        public const string sPolEndorsement = "ENDORSEMENT";
        public const string sPolLimDed = "LIMIT_DEDUCTIBLE";    //SIW7123
        public const string sPolRetroDate = "RETRO_DATE";
        public const string sPolTailDate = "TAIL_DATE";
        public const string sPolDataItem = "DATA_ITEM";
        public const string sPolOrgDept = "ORG_DEPT_EID"; //SIN8529
        //public const string sPolTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360
        public const string sPolIssueSystemId = "ISSUE_SYSTEM_ID";//(SI06232 - Implemented in SI06333)

        /*'<INSURED_UNIT UNIT_NUMBER="xx">
        '   <UNIT_REFERENCE ID="UNTxxx"/>
        '   <INSURANCE_LINE CODE="WC"></INSURANCE_LINE>
        '   <EFFECTIVE_DATE YEAR="2001" MONTH="08" DAY="01" />
        '   <EXPIRATION_DATE YEAR="2002" MONTH="08" DAY="01" />
        '   <LIMIT> ...... </LIMIT>
        '     <!-- Multiple Limits -->
        '   <DEDUCTIBLE> ...... </DEDUCTIBLE>
        '     <!-- Multiple Deductibles -->
        '   <SCHEDULED_ITEM> ...   </SCHEDULED_ITEM>
        '   <COVERAGE_DATA> ... </COVERAGE_DATA>
        '   <ENDORSEMENT> ... <ENDORSEMENT>
        '   <ASSIGNED_DRIVER ID="ENTrefid" NUMBER="nn" RELATION_TO_INSURED="reltnCode">Relation to Insured</ASSIGNED_DRIVER>
        '   <PARTY_INVOLVED ID="ENTxxx" CODE="Role">Role Description</PARTY_INVOLVED>
        '     <!--Muliple Party Involved nodes allowed -->
        '   <COMMENT_REFERENCE ID="CMTxx"/>
        '     <!-- multiple entries -->
        '   <DATA_ITEM> ... </DATA_ITEM>                          
        '</INSURED_UNIT>*/

        public const string sInsUntNode = "INSURED_UNIT";
        public const string sInsUntNumber = "UNIT_NUMBER";
        public const string sInsUntReference = "UNIT_REFERENCE";
        public const string sInsUntRefID = "ID";
        public const string sInsIDPfx = "IUT"; //SIW490
        public const string sInsID = "ID"; //SIW490
        public const string sInsUntInsLine = "INSURANCE_LINE";
        public const string sInsUntEffDate = "EFFECTIVE_DATE";
        public const string sInsUntExpDate = "EXPIRATION_DATE";
        public const string sInsUntLimit = "LIMIT";
        public const string sInsUntDeductible = "DEDUCTIBLE";
        public const string sInsUntSchedItem = "SCHEDULED_ITEM";
        public const string sInsUntCoverage = "COVERAGE_DATA";
        public const string sInsUntVehAsgnDriver = "ASSIGNED_DRIVER";
        public const string sInsUntVehAsgnDrvrNumber = "NUMBER";
        public const string sInsUntVehAsgnDrvrRltnToIns = "RELATION_TO_INSURED";
        public const string sInsUntPartyInvolved = "PARTY_INVOLVED";
        public const string sInsUntComment = "COMMENT_REFERENCE";
        public const string sInsUntEndorsement = "ENDORSEMENT";
        public const string sInsUntDataItem = "DATA_ITEM";
        //public const string sInsUntTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<INSURED_UNIT>
        '   <SCHEDULED_ITEM  ID="SIxxx">
        '     <ITEM_TYPE CODE="JEW|COMP|...">Jewlery<ITEM_TYPE>
        '     <DESCRIPTION>Item Description</DESCRIPTION>
        '     <SERIAL_NUMBER>Serial Number</SERIAL_NUMBER>
        '     <DECLARED_VALUE>$$$$$$</DECLARED_VALUE>
        '     <REPLACEMENT_VALUE>$$$$</REPLACEMENT_VALUE>
        '     <EFFECTIVE_DATE YEAR="2001" MONTH="08" DAY="01" />
        '     <EXPIRATION_DATE YEAR="2002" MONTH="08" DAY="01" />
        '     <LIMIT> ...... </LIMIT>
        '        <!-- Multiple Limits -->
        '     <DEDUCTIBLE> ...... </DEDUCTIBLE>
        '        <!-- Multiple Deductibles -->
        '     <COMMENT_REFERENCE ID="CMTxx"/>
        '        <!-- multiple entries -->
        '   </SCHEDULED_ITEM>
        '     <!-- Multiple Items -->
        '</INSURED_UNIT>*/

        public const string sSItmNode = "SCHEDULED_ITEM";
        public const string sSItmID = "ID";
        public const string sSItmIDPfx = "SI";
        public const string sSItmType = "ITEM_TYPE";
        public const string sSItmDescription = "DESCRIPTION";
        public const string sSItmSerialNum = "SERIAL_NUMBER";
        public const string sSItmDeclValue = "DECLARED_VALUE";
        public const string sSItmRplcValue = "REPLACEMENT_VALUE";
        public const string sSItmEffDate = "EFFECTIVE_DATE";
        public const string sSItmExpDate = "EXPIRATION_DATE";
        public const string sSItmLimit = "LIMIT";
        public const string sSItmDeductible = "DEDUCTIBLE";
        public const string sSItmComment = "COMMENT_REFERENCE";
        //public const string sSItmTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<INSURED_UNIT>
        '   <COVERAGE_DATA ID="COVxxx" LEGACY_KEY="00001,1,1" COV_SEQ_NO="1" ITEM_SEQ_NO="1">
        '      <COVERAGE CODE="032">Workers Compensation</COVERAGE>
        '      <EFFECTIVE_DATE YEAR="2001" MONTH="08" DAY="01" />
        '      <EXPIRATION_DATE YEAR="2002" MONTH="08" DAY="01" />
        '      <LIMIT> ...... </LIMIT>
        '        <!-- Multiple Limits -->
        '      <DEDUCTIBLE> ...... </DEDUCTIBLE>
        '        <!-- Multiple Deductibles -->
        '      <RISK_GROUP CODE="011">Workers Compensation - Voluntary</RISK_GROUP>
        '      <CLASSIFICATION CODE="8017  ">Store: Retail Noc</CLASSIFICATION>
        '      <STATE CODE="TX">Texas</COV_STATE>
        '      <ENDORSEMENT> ... <ENDORSEMENT>
        '      <PARTY_INVOLVED ID="ENTxxx" CODE="Role">Role Description</PARTY_INVOLVED>
        '        <!--Muliple Party Involved nodes allowed -->
        '      <COMMENT_REFERENCE ID="CMTxx"/>
        '        <!-- multiple entries -->
        '      <DATA_ITEM> ... </DATA_ITEM>                          
        '     <FINANCIAL_ACTIVITY ISFROZEN='True|False' ISPAYMENT='True|False' ISCOLLECTION='True|False'/> 'SIW7036
        '   </COVERAGE_DATA>
        '</INSURED_UNIT>*/

        public const string sCovNode = "COVERAGE_DATA";
        public const string sCovID = "ID";
        public const string sCovIDPfx = "COV";
        public const string sCovLegacyKey = "LEGACY_KEY";
        public const string sCovSeqNo = "COV_SEQ_NO";
        public const string sCovItemSeqNo = "ITEM_SEQ_NO";
        public const string sCovCoverage = "COVERAGE";
        public const string sCovEffDate = "EFFECTIVE_DATE";
        public const string sCovExpDate = "EXPIRATION_DATE";
        public const string sCovLimit = "LIMIT";
        public const string sCovDeductible = "DEDUCTIBLE";
        public const string sCovRiskGroup = "RISK_GROUP";
        public const string sCovClassification = "CLASSIFICATION";
        public const string sCovState = "STATE";
        public const string sCovComment = "COMMENT_REFERENCE";
        public const string sCovPartyInvolved = "PARTY_INVOLVED";
        public const string sCovEndorsement = "ENDORSEMENT";
        public const string sCovDataItem = "DATA_ITEM";
        //public const string sCovTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<parent>
        '  <ENDORSEMENT>
        '     <FORM_NUMBER>formnumber</FORM_NUMBER>
        '     <FORM_DATE YEAR="2001" MONTH="08" DAY="01" />
        '     <DESCRIPTION>description</DESCRIPTION>
        '     <EFFECTIVE_DATE YEAR="2001" MONTH="08" DAY="01" />
        '     <EXPIRATION_DATE YEAR="2002" MONTH="08" DAY="01" />
        '     <SPECIAL_CONDITIONS>text</SPECIAL_CONDITIONS>
        '        <!-- Multiple Endorsements allowed -->
        '  </ENDORSEMENT>
        '    <!-- Multiple Endorsements allowed -->
        '</parent>*/

        public const string sEndNode = "ENDORSEMENT";
        public const string sEndFormNumber = "FORM_NUMBER";
        public const string sEndFormDate = "FORM_DATE";
        public const string sEndFormDescription = "DESCRIPTION";
        public const string sEndFormEffDate = "EFFECTIVE_DATE";
        public const string sEndFormExpDate = "EXPIRATION_DATE";
        public const string sEndFormSpclCond = "SPECIAL_CONDITIONS";
        //public const string sEndTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        //START SIW7123

        /*****************************************************************************
        '<POLICY>
        '   <LIMIT_DEDUCTIBLE>
        '           <TECH_KEY>AC Technical key for the element </TECH_KEY>
        '           <TYPE CODE="">Type</TYPE>
        '           <COVERAGE CODE="">Coverage</COVERAGE>
        '           <APPLICATION CODE="">Applicatioin</APPLICATION>
        '           <AMOUNT>2000</AMOUNT>
        '           <USED_AMOUNT>1000</USED_AMOUNT>
        '           <PART_OF>50</PART_OF>
        '           <AGGREGATE></AGGREGATE>
        '           <DESCRIPTION>Test</DESCRIPTION>
        '   </LIMIT_DEDUCTIBLE>
        '</POLICY>*/
        public const string sPolLimDedNode = "LIMIT_DEDUCTIBLE"; 
        public const string sPolLimDedID = "ID";
        public const string sPolLimDedType = "TYPE";
        public const string sPolLimDedCoverage = "COVERAGE";
        public const string sPolLimDedApp = "APPLICATION";
        public const string sPolLimDedAmount = "AMOUNT";
        public const string sPolLimDedUsedAmount = "USED_AMOUNT";
        public const string sPolLimDedPartOf = "PART_OF";
        public const string sPolLimDedAggregate = "AGGREGATE";
        public const string sPolLimDedDescription = "DESCRIPTION";


        //END SIW7123

        /*'<PROPERTY_LOSS>
        '   <UNIT_REFERENCE ID="UNTxxx"/>
        '   <SCHEDULED_ITEM> ...   </SCHEDULED_ITEM>
        '   <LOSS_PROP_TYPE CODE="code>description</LOSS_PROP_TYPE>
        '   <THEFT_LOCATION CODE="code>description</THEFT_LOCATION>
        '   <INCENDIARY_FIRE INDICATOR="YES|NO"/>
        '   <UNDER_CONSTRUCTION INDICATOR="YES|NO"/>
        '   <VACANT INDICATOR="YES|NO"/>
        '   <LOSS_INFORMATION> ... </LOSS_INFORMATION>
        '      <!Multiple Demand Offers Allowed>            //SI7419
        '      <DEMAND_OFFER> ... </DEMAND_OFFER>
        '</PROPERTY_LOSS>*/

        public const string sPLNode = "PROPERTY_LOSS";
        public const string sPLUnitReference = "UNIT_REFERENCE";
        public const string sPLUnitReferenceID = "ID";
        public const string sPLSchedItem = "SCHEDULED_ITEM";
        public const string sPLLossPropType = "LOSS_PROP_TYPE";
        public const string sPLTheftLocation = "THEFT_LOCATION";
        public const string sPLIncendiaryFire = "INCENDIARY_FIRE";
        public const string sPLUnderConstruction = "UNDER_CONSTRUCTION";
        public const string sPLVacant = "VACANT";
        public const string sPLLossInfo = "LOSS_INFORMATION";
        public const string sPLDemandOffer= "DEMAND_OFFER";      //SIW7419
        //public const string sPLTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<VEHICLE_LOSS>
        '   <UNIT_REFERENCE ID="UNTxxx"/>
        '   <DRIVER_RELATION_TO_OWNER CODE="rltn>relationship</DRIVER_RELATION_TO_OWNER>
        '   <DRIVEABLE INDICATOR="YES|NO"/>
        '   <NON_VEH_PROP_DAMAGE INDICATOR="YES|NO">                       'SI06423a
        '   <LOSS_INFORMATION> ... </LOSS_INFORMATION>
        '      <!Multiple Demand Offers Allowed>              // SIW7419
        '      <DEMAND_OFFER> ... </DEMAND_OFFER>
        '</VEHICLE_LOSS>*/

        public const string sVLNode = "VEHICLE_LOSS";
        public const string sVLUnitReference = "UNIT_REFERENCE";
        public const string sVLUnitReferenceID = "ID";
        public const string sVLDriverRltnToOwner = "DRIVER_RELATION_TO_OWNER";
        public const string sVLDriveable = "DRIVEABLE";
        public const string sVLNonVehPropDamage = "NON_VEH_PROP_DAMAGE";   //SI06423a
        public const string sVLLossInfo = "LOSS_INFORMATION";
        public const string sVLDemandOffer = "DEMAND_OFFER";          //SIW7419
        //public const string sVLTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<LOSS_INFORMATION>
        '    <LOSS_DESCRIPTION>description of loss</LOSS_DESCRIPTION>
        '    <USED_WITH_PERMISSION INDICATOR="YES|NO"/>
        '    <DAMAGE_DESCRIPTION>description of damage</DAMAGE_DESCRIPTION>
        '    <DAMAGE_DESC_HTML>description of damage in HTML format</DAMAGE_DESC_HTML>    'SI06369    
        '    <ESTIMATED_DAMAGE_AMOUNT>$$$$$$$<ESTIMATED_DAMAGE_AMOUNT>
        '    <ACTUAL_DAMAGE_AMOUNT>$$$$$$$<ACTUAL_DAMAGE_AMOUNT>
        '    <RENTAL>
        '       <DAILY_CHARGE>daily rental charge</DAILY_CHARGE>
        '       <START_DATE YEAR="yyyy" MONTH="mmm" DAY="dd"/>
        '       <END_DATE YEAR="yyyy" MONTH="mmm" DAY="dd"/>
        '       <VENDOR ID="ENTxxxx>vendor name</VENDOR>
        '    </RENTAL>
        '    <CAN_BE_SEEN>
        '       <WHERE ID="ADRxxx">where is property for viewing</WHERE>
        '       <DATE YEAR="yyyy" MONTH="mmm" DAY="dd"/>
        '       <AFTER_TIME HOUR="hh" MINUTE="mm" SECOND="sss"/>
        '       <BEFORE_TIME HOUR="hh" MINUTE="mm" SECOND="sss"/>
        '    </CAN_BE_SEEN>
        '    <CHARGES TYPE=[TOWING, CLEANING, REPAIRS, RENTAL, OTHER]>$$$$$</CHARGES>
        '       <!-- Multiple Charges Allowed --->
        '    <OTHER_INSURANCE>
        '      <POLICY_NUMBER>policynumber</POLICY_NUMBER
        '      <CARRIER ID="ENTxxx">carriername</CARRIER>
        '      <EFFECTIVE_DATE YEAR="year" MONTH="month" DAY="day"/>
        '      <EXPIRATION_DATE YEAR="year" MONTH="month" DAY="day"/>
        '      <COVERAGE>
        '          <COVERAGE_TYPE CODE="xx">Coverage Type</COVERAGE_TYPE>
        '          <COVERAGE_LIMIT CODE="xx" AMOUNT="amount">limit type</COVERAGE_LIMIT>
        '             <!-- Multiple Limits Allowed -->
        '          <COVERAGE_DEDUCTIBLE CODE="xx" AMOUNT="xx">deductible type</COVERAGE_DEDUCTIBLE>
        '             <!-- Multiple Deductibles Allowed -->
        '      </COVERAGE>
        '             <!-- Multiple Coverages Allowed -->
        '    </OTHER_INSURANCE>
        '    <REPAIR_ESTIMATE_DETAILS>
        '       <REPAIR_ESTIMATE>
        '          <REPAIR_ITEM CODE="xx">itemname</REPAIR_ITEM>
        '          <ITEM_UCR CODE="itemucrcode" SOURCE="ucrsourcecatalog"/>
        '          <COST>itemcost</COST>
        '          <LABOR HOURS="hours" RATE="rate">laborcost</LABOR>
        '          <TYPE_OF_REPAIR CODE="REPLACE|REPAIR|NEW|REBUILT">
        '          <VENDOR ID="ENTxxxx/>
        '       </REPAIR_ESTIMATE>
        '          <!-- Multiple Items allowed -->
        '    <REPAIR_ESTIMATE_DETAILS>
        '    <SALVAGE> ... </SALVAGE>
        '    <PARTY_INVOLVED ID="ENTxxx" CODE="Role">Role Description</PARTY_INVOLVED>
        '      <!--Muliple Party Involved nodes allowed -->
        '    <COMMENT_REFERENCE ID="CMTxx"/>
        '      <!-- multiple entries -->
        '    <DATA_ITEM> ... </DATA_ITEM>
        '      <!-- multiple data items -->
        '</LOSS_INFORMATION>*/

        public const string sLINode = "LOSS_INFORMATION";
        public const string sLIDescription = "LOSS_DESCRIPTION";
        public const string sLIUsedWithPermission = "USED_WITH_PERMISSION";
        public const string sLIDamageDescription = "DAMAGE_DESCRIPTION";
        public const string sLIDamageDescriptionHTML = "DAMAGE_DESC_HTML";    //SI06369
        public const string sLIEstDamageAmount = "ESTIMATED_DAMAGE_AMOUNT";
        public const string sLIActualDamageAmount = "ACTUAL_DAMAGE_AMOUNT";
        public const string sLIRepairEstDetails = "REPAIR_ESTIMATE_DETAILS";
        public const string sLIRepairEst = "REPAIR_ESTIMATE";
        public const string sLIRepairEstItem = "REPAIR_ITEM";
        public const string sLIRepairEstItemUCR = "ITEM_UCR";
        public const string sLIRepairEstItemUCRSource = "SOURCE";
        public const string sLIRepairEstItemCost = "COST";
        public const string sLIRepairEstItemLabor = "LABOR";
        public const string sLIRepairEstItemLaborHours = "HOURS";
        public const string sLIRepairEstItemLaborRate = "RATE";
        public const string sLIRepairEstItemTypeRepair = "TYPE_OF_REPAIR";
        public const string sLIRepairEstItemVendor = "VENDOR";
        public const string sLIRental = "RENTAL";
        public const string sLIRentalCharge = "DAILY_CHARGE";
        public const string sLIRentalStartDate = "START_DATE";
        public const string sLIRentalEndDate = "END_DATE";
        public const string sLIRentalVendor = "VENDOR";
        public const string sLISeen = "CAN_BE_SEEN";
        public const string sLISeenWhere = "WHERE";
        public const string sLISeenDate = "DATE";
        public const string sLISeenAfterTime = "AFTER_TIME";
        public const string sLISeenBeforeTime = "BEFORE_TIME";
        public const string sLICharges = "CHARGES";
        public const string sLIChargesType = "TYPE";
        public const string sLIOthrIns = "OTHER_INSURANCE";
        public const string sLIOthrInsPolicy = "POLICY_NUMBER";
        public const string sLIOthrInsCarrier = "CARRIER";
        public const string sLIOthrInsEffDate = "EFFECTIVE_DATE";
        public const string sLIOthrInsExpDate = "EXPIRATION_DATE";
        public const string sLIOthrInsCoverage = "COVERAGE";
        public const string sLIOthrInsCoverageType = "COVERAGE_TYPE";
        public const string sLIOthrInsCoverageLimit = "COVERAGE_LIMIT";
        public const string sLIOthrInsCoverageLimitAmount = "AMOUNT";
        public const string sLIOthrInsCoverageDeductible = "COVERAGE_DEDUCTIBLE";
        public const string sLIOthrInsCoverageDeductibleAmount = "AMOUNT";
        public const string sLISalvage = "SALVAGE";
        public const string sLIDataItem = "DATA_ITEM";
        public const string sLIPartyInvolved = "PARTY_INVOLVED";
        public const string sLICommentReference = "COMMENT_REFERENCE";
        public const string sRepairTypeReplace = "REPLACE";
        public const string sRepairTypeRepair = "REPAIR";
        public const string sRepairTypeNew = "NEW";
        public const string sRepairTypeRebuilt = "REBUILT";
        public const int iRepairTypeReplace = 1;
        public const int iRepairTypeRepair = 2;
        public const int iRepairTypeNew = 3;
        public const int iRepairTypeRebuilt = 4;
        //public const string sLITechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<INVOLVED_UNITS COUNT="unitcount">
        '   <UNIT UNIT_TYPE="L" ID="UNTxxx">
        '      <UNIT_NUMBER>00001</UNIT_NUMBER>
        '      <DESCRIPTION>description</DESCRIPTION>
        '      <GENERAL_CONDITION CODE="xx">General Condition</GENERAL_CONDITION>
        '      <DATE_PURCHASED YEAR="2002" MONTH="08" DAY="01" />
        '      <ORIGINAL_COST>original cost</ORIGINAL_COST>
        '      <MARKET_VALUE>market value</MARKET_VALUE>
        '      <REPLACEMENT_VALUE>replacement value</REPLACEMENT_VALUE>
        '      <NON_VEH_PROP_DAMAGE INDICATOR="YES|NO">                       'SI06423   'SI06423a - Moved to LossInfo
        '      <INSURED INDICATOR="YES|NO">                                   'SI06423
        '      <LOCATION_DATA>
        '         <ADDRESS_REFERENCE ID="ADR2"></ADDRESS_REFERENCE>
        '         <LOC_STATE CODE="TX">Texas </LOC_STATE>
        '      </LOCATION_DATA>
        '   <ORG_DEPT_EID ENTITY_ID="ENTXXX"></ORG_DEPT_EID> 'SIN8529
        '      <BUILDING>
        '         <NUMBER>1</NUMBER>
        '         <DESCRIPTION>Building Description<\DESCRIPTION>
        '         <CONSTRUCTION CODE="constcode">description<CONSTRUCTION>
        '         <YEAR_OF_CONSTRUCTION>year</YEAR_OF_CONSTRUCTION>
        '         <SQUARE_FOOTAGE>SquareFoot</SQUARE_FOOTAGE>
        '      </BUILDING>
        '        <!--Multiple Buildings Allowed -->
        '      <VEHICLE MAKE="Ford" MODEL=" Explorer" YEAR="1995">
        '         <VEHICLE_TYPE CODE="PP">PP</VEH_TYPE>
        '         <SERIAL_NO>MX183273688</SERIAL_NO>
        '         <BODY_TYPE CODE="">bodytype</BODY_TYPE>
        '         <ENGINE_SIZE UNIT="CUIN|CC" CODE="xx">size</ENGINE_SIZE>
        '         <CHASSIS CODE="xx">typeofchassis</CHASSIS>
        '         <TRANSMISSION CODE="AUTO|MANUAL">TypeofTransmission</TRANSMISSION>
        '         <COLOR CODE=code>description</COLOR>
        '         <PURPOSE_OF_USE CODE="xx">purpose of use</PURPOSE_OF_USE>
        '         <GROSS_WEIGHT>weight</GROSS_WEIGHT>
        '         <DATE_LAST_SERVICE YEAR="2002" MONTH="08" DAY="01" />
        '         <TYPE_OF_SERVICE CODE="xx">type of service</TYPE_OF_SERVICE>
        '         <GARAGE_STATE CODE="xx">state</GARAGE_STATE>
        '         <REGISTRATION>
        '            <STATE_REGISTRATION>StateRegistrationNumber</STATE_REGISTRATION>
        '            <STATE CODE="SC">South Carolina</STATE>
        '            <LICENSE>licensenumber</LICENSE>
        '            <DATE YEAR="2002" MONTH="08" DAY="01" />
        '         </REGISTRATION>
        '         <LEASE INDICATOR ="YES|NO">                                     
        '            <LEASE_NUMBER>lease number</LEASE_NUMBER>
        '            <LEASE_TERM>term</LEASE_TERM>
        '            <AMOUNT>leaseamount</AMOUNT>
        '            <DATE_EXPIRES YEAR="2002" MONTH="08" DAY="01" />
        '         </LEASE>
        '      </VEHICLE>
        '      <PARTY_INVOLVED ID="ENTxxx" CODE="Role">Role Description</PARTY_INVOLVED>
        '        <!--Muliple Party Involved nodes allowed -->
        '      <COMMENT_REFERENCE ID="CMTxx"/>
        '        <!-- multiple entries -->
        '      <POL_HLD_IS_OWNER INDICATOR="YES|NO">
        '      <OTHER_INSURANCE INDICATOR="YES|NO">
        '   </UNIT>
        '</INVOLVED_UNITS>*/

        public const string sUntsInvolved = "INVOLVED_UNITS";
        public const string sUntsCount = "COUNT";
        public const string sUntNode = "UNIT";
        public const string sUntType = "UNIT_TYPE";
        public const string sUntID = "ID";
        public const string sUntIDPfx = "UNT";
        public const string sUntNumber = "UNIT_NUMBER";
        public const string sUntDescription = "DESCRIPTION";
        public const string sUntGeneralCond = "GENERAL_CONDITION";
        public const string sUntDatePurchased = "DATE_PURCHASED";
        public const string sUntOrigCost = "ORIGINAL_COST";
        public const string sUntMarketValue = "MARKET_VALUE";
        public const string sUntReplacementValue = "REPLACEMENT_VALUE";
        public const string sUntPartyInvolved = "PARTY_INVOLVED";
        public const string sUntComment = "COMMENT_REFERENCE";
        public const string sUntDocument = "DOCUMENT_REFERENCE";
        public const string sUntLocation = "LOCATION_DATA";
        public const string sUntLocAddress = "ADDRESS_REFERENCE";
        public const string sUntLocState = "LOC_STATE";
        //public const string sUntTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360
        public const string sUntBuilding = "BUILDING";
        public const string sUntBldngNumber = "NUMBER";
        public const string sUntBldngDescription = "DESCRIPTION";
        public const string sUntBldngConstruction = "CONSTRUCTION";
        public const string sUntBldngYearOfConst = "YEAR_OF_CONSTRUCTION";
        public const string sUntBldngSqrFt = "SQUARE_FOOTAGE";
        public const string sUntVehicle = "VEHICLE";
        public const string sUntVehMake = "MAKE";
        public const string sUntVehModel = "MODEL";
        public const string sUntVehYear = "YEAR";
        public const string sUntVehBodyType = "BODY_TYPE";
        public const string sUntVehEngineSize = "ENGINE_SIZE";
        public const string sUntVehEngineSizeUnit = "UNIT";
        public const string sUntVehChassis = "CHASSIS";
        public const string sUntVehTransmission = "TRANSMISSION";
        public const string sUntVehColor = "COLOR";
        public const string sUntVehUse = "PURPOSE_OF_USE";
        public const string sUntVehType = "VEHICLE_TYPE";
        public const string sUntVehSerialNo = "SERIAL_NO";
        public const string sUntVehGrossWgt = "GROSS_WEIGHT";
        public const string sUntVehDateLastSrvc = "DATE_LAST_SERVICE";
        public const string sUntVehTypeofService = "TYPE_OF_SERVICE";
        public const string sUntVehGarageState = "GARAGE_STATE";
        public const string sUntVehRegistration = "REGISTRATION";
        public const string sUntVehStateRegNum = "STATE_REGISTRATION";
        public const string sUntVehRegState = "STATE";
        public const string sUntVehRegLicense = "LICENSE";
        public const string sUntVehRegDate = "DATE";
        public const string sUntVehLeaseNode = "LEASE";
        public const string sUntVehLeaseNumber = "LEASE_NUMBER";
        public const string sUntVehLeaseTerm = "LEASE_TERM";
        public const string sUntVehLeaseAmount = "AMOUNT";
        public const string sUntVehLeaseExpDate = "DATE_EXPIRES";
        public const string sUntPolHldIsVehOwner = "POL_HLD_IS_OWNER";
        public const string sUntOtherInsurance = "OTHER_INSURANCE";
        public const string sUntVehOrgDept = "ORG_DEPT_EID"; //SIN8529
      //public const string sUntNonVehPropDamage = "NON_VEH_PROP_DAMAGE";   //SI06423 //SI06423a - Moved to loss info
        public const string sUntInsured = "INSURED";                        //SI06423
        public const string sUntTypeLoc = "L";
        public const string sUntTypeVeh = "V";
        public const string sUntTypeProp = "P";//(SI06023 - Implemented in SI06333)
        public const string sUntTypeLiab = "I";//SIW344
        public const int iUntTypeLoc = 1;
        public const int iUntTypeVeh = 2;
        public const int iUntTypeProp = 3;//(SI06023 - Implemented in SI06333)
        public const int iUntTypeLiab = 4;//SIW344      
        //public const string sUntVehTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<CLAIM>
        '   ....
        '   <UNIT_STAT_DCI>
        '      <ANNUITY_PURCHASE_AMOUNT>0</ANNUITY_PURCHASE_AMOUNT>
        '      <TOTAL_GROSS_INCURRED>0</TOTAL_GROSS_INCURRED>
        '      <DEDUCTIBLE REIMBURSEMENT="No" CODE="pgmcode">Deductible Program<DEDUCTIBLE/>
        '      <DISPUTED_CASE INDICATOR="No" />
        '      <POSSIBLE_FRAUD Code="02">Fraudulent Claim Indicator</POSSIBLE_FRAUD>
        '      <LUMP_SUM INDICATOR="No" />
        '      <JURISDICTION_STATE CODE="NC">North Carolina</JURISDICTION_STATE>
        '      <WORKERS_COMP_CAT>0</WORKERS_COMP_CAT>
        '      <BENEFIT_TYPE CODE="05">Temporary Total or Temporary Partial Disability</BENEFIT_TYPE>
        '      <DATE_EMPLOYER_NOTIFIED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <TIME_EMPLOYER_NOTIFIED HOUR="17" MINUTE="35" SECOND="04" />
        '      <DATE_ATTY_FORM_RCVD YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <NCCI_WAGE_RANGE CODE="xxx">wage range</NCCI_WAGE_RANGE>
        '      <CATASTROPHE_NUMBER>catastrophe</CATASTROPHE_NUMBER>
        '      <LOSS_ACT CODE='lossactcode'>Loss Act</LOSS_ACT>
        '      <SETTLEMENT_METHOD CODE="code">settlement method</SETTLEMENT_METHOD>
        '      <SETTLEMENT_TYPE CODE="code">settlement Type</SETTLEMENT_TYPE>
        '      <TYPE_RECOVERY CODE="code">type recovery</TYPE_RECOVERY>
        '      <TYPE_RESERVE CODE="code">reserve typee</TYPE_RESERVE>
        '      <TYPE_COVERAGE CODE="code">coverage type</TYPE_COVERAGE>
        '      <TYPE_LOSS CODE="code">loss code</TYPE_LOSS>
        '      <LOSS_COVERAGE CODE="code">loss coverage</LOSS_COVERAGE>
        '      <EMPLOYEE>
        '          ....
        '      </EMPLOYEE>
        '      <PARTY_INVOLVED ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</PARTY_INVOLVED>
        '         <!--Multiple Parties Involved at the UnitStats level such as Employee, Employer, etc.-->
        '   </UNIT_STAT_DCI>
        '</CLAIM>*/

        public const string sUSNode = "UNIT_STAT_DCI";
        public const string sUSAnnPurAmt = "ANNUITY_PURCHASE_AMOUNT";
        public const string sUSTotGrossIncurred = "TOTAL_GROSS_INCURRED";
        public const string sUSDeductible = "DEDUCTIBLE";
        public const string sUSDeductibleReimbursement = "REIMBURSEMENT";
        public const string sUSDisputedCase = "DISPUTED_CASE";
        public const string sUSPossibleFraud = "POSSIBLE_FRAUD";
        public const string sUSLumpSum = "LUMP_SUM";
        public const string sUSJurisdictionState = "JURISDICTION_STATE";
        public const string sUSWrkCompCategory = "WORKERS_COMP_CAT";
        public const string sUSBenefitType = "BENEFIT_TYPE";
        public const string sUSEmployerNotifiedDate = "DATE_EMPLOYER_NOTIFIED";
        public const string sUSEmployerNotifiedTime = "TIME_EMPLOYER_NOTIFIED";
        public const string sUSAttyFormRcptDate = "DATE_ATTY_FORM_RCVD";
        public const string sUSEmployerWageRange = "NCCI_WAGE_RANGE";
        public const string sUSCatastrophe = "CATASTROPHE_NUMBER";
        public const string sUSLossAct = "LOSS_ACT";
        public const string sUSSettleMethod = "SETTLEMENT_METHOD";
        public const string sUSSettleType = "SETTLEMENT_TYPE";
        public const string sUSTypeRecovery = "TYPE_RECOVERY";
        public const string sUSTypeReserve = "TYPE_RESERVE";
        public const string sUSTypeCoverage = "TYPE_COVERAGE";
        public const string sUSTypeLoss = "TYPE_LOSS";
        public const string sUSLossCoverage = "LOSS_COVERAGE";
        public const string sUSClaimDenied = "CLAIM_DENIED";
        public const string sUSEmployee = "EMPLOYEE";
        public const string sUSPartyInvolved = "PARTY_INVOLVED";
        //public const string sUSTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<parent>
        '   <EMPLOYEE CLAIMANT_NUMBER="1" ID="ENT249">
        '      <EMPLOYEE_NUMBER>employee number</EMPLOYEE_NUMBER>
        '      <EMPLOYMENT_STATUS ACTIVE="YES|NO {boolean}" CODE="statuscode" FULLTIME="boolean"></EMPLOYMENT_STATUS>
        '      <RETURNED_TO_WORK NEXT_DAY="YES|NO {boolean}" DUTIES="YES|NO {boolean}"/>
        '      <OCCUPATION CODE="Standard Occupation Code {string} [OCCUPATION]">Occupation Translated {string}</OCCUPATION>
        '      <EMPLOYEE_LEGAL_REP>employee legal representative</EMPLOYEE_LEGAL_REP>
        '      <DATE_HIRED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <DATE_TERMINATED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <STATE_HIRED _OPTIONAL_="" CODE="State Abbreviation {string} [STATES]">State Name {string}</STATE_HIRED>
        '      <AVG_WKLY_WAGE _OPTIONAL_="">{decimal}</AVG_WKLY_WAGE>
        '      <ADJUSTED_WAGE _OPTIONAL_="">{decimal}</ADJUSTED_WAGE>
        '      <COMPENSATION_RATE>{decimal}</COMPENSATION_RATE>      'SIW44
        '      <WORK_PERMIT_DATE YEAR="" MONTH="" DAY=""/>
        '      <WORK_PERMIT_NUMBER>number</WORK_PERMIT_NUMBER>
        '      <EXEMPT_STATUS INDICATOR="YES|NO"/>                   'SIW44
        '      <REGULAR_JOB_STATUS INDICATOR="YES|NO"/>              'SIW44
        '      <HIRED_IN_JURISDICTION INDICATOR="YES|NO"/>           'SIW44
        '      <PAY SPECIFIED="[PREINJURY|POSTINJURY|OTHER|CURRENT]
        '           CALCULATE_METHOD="calculatemethodcode"
        '           TYPE="SALARY | HOURLY {string} [PAY_TYPE]"
        '           PERIOD="[HOURLY|WEEKLY|BIWEEKLY|SEMIMONTHLY|ANNUAL]{string}"
        '           FULL_PAY_DAY_INJURED="YES|NO {boolean}"
        '           CONTINUED="YES|NO {boolean}"
        '           OT_ELIGIBLE="YES|NO {boolean}">{decimal}</PAY>
        '           <!-- Multiple Lines of different types allowed -->
        '      <ENTITLEMENT CODE="[description|TOTAL]"
        '                   PERIOD="[HOURLY|WEEKLY|BIWEEKLY|SEMIMONTHLY|ANNUAL]"
        '                   CONTINUED="YES|NO {boolean}">
        '            amount {decimal}</ENTITLEMENT>
        '         <!-One line for each appropriate entitlement. If submitting a TOTAL, do not submit itemized items and vice versa -->
        '      <TIME_DAY_BEGAN HOUR="17" MINUTE="35" SECOND="04" />        //SIW391
        '      <WEEKLY_SCHEDULE SPECIFIED="[PREINJURY|POSTINJURY|CURRENT|OTHER]
        '                       WEEK_STARTS_ON="the day of week normally considered the start of the work week[SUN,MON,TUE,WED,THU,FRI,SAT,AVG]"
        '                       DAYS_WORKED="The number of days worked per week{integer}">
        '          <DAY CODE="Day of Week [SUN,MON,TUE,WED,THU,FRI,SAT,AVG]"
        '               SCHD_REG_HOURS="{integer}"
        '               EST_OT_HOURS="{integer}">Day of week</DAY>
        '              <SHIFT_START_TIME TIME_ZONE="Time Zone that the Time references [EST,EDT,MCT,etc.]"  HOUR="Hour (2) {Integer} [0-23]"  MINUTE="Minute (2) {Integer} [0-60]" SECOND="Second (2) {Integer} [0-60=</TIME_CREATED>
        '              <SHIFT_END_TIME TIME_ZONE="Time Zone that the Time references [EST,EDT,MCT,etc.]"  HOUR="Hour (2) {Integer} [0-23]"  MINUTE="Minute (2) {Integer} [0-60]" SECOND="Second (2) {Integer} [0-60=</TIME_CREATED>
        '          </DAY>
        '          <!-Repeat for each appropriate day. If AVG submitted, this indicates the average days activities within a weeks time-->
        '      </WEEKLY_SCHEDULE >
        '          <!-- Repeat schedule as needed with different SPECIFIED values -->
        '      <WEEKLY_HOURS> integer </WEEKLY_HOURS>
        '      <FIRST_DAY_NOT_FULL_DAY _OPTIONAL_="" YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <ASSIGNED_DEPARTMENT _OPTIONAL_="" CODE="Department Code {string} [ORG_HIERARCHY]">Deparment Assigned {string}</ASSIGNED_DEPARTMENT>
        '      <NUMBER_OF_DEPENDANTS>0</NUMBER_OF_DEPENDANTS>
        '      <EDUCATION CODE="*">Default</EDUCATION>               
        '      <PRIMARY_LANGUAGE CODE="*">Default</PRIMARY_LANGUAGE>   
        '      <PAST_WORK_EX>Describe the work ex{string}</PAST_WORK_EX>  
        '      <PRIOR_WORK_HIST>Describe the Prior Work History{string}</PRIOR_WORK_HIST>  
        '      <LOST_DAYS ID="LSDxxxxxx">                    'SIW392
        '      <TECH _KEY></TECH_KEY>                         'SIN8043
        '        <LAST_WORK_DAY YEAR="" MONTH="" DAY=""/>    'SIW392
        '        <DATE_RETURN_TO_WORK YEAR="" MONTH="04" DAY=""/>   'SIW392
        '        <WEEKLY_PAY>0</WEEKLY_PAY>                  'SIN8043
        '        <DURATION>0</DURATION>                      'SIN8043
        '      </LOST_DAYS>   'SIW392
        '           <!-- Multiple Lines of Lost Days of different ID's allowed -->   'SIW392
        '      <RESTRICTED_DAYS ID="RSDxxxx">   'SIW392
        '        <FIRST_DAY_RSTC YEAR="" MONTH="" DAY=""/>   'SIW392
        '        <LAST_DAY_RSTC YEAR="" MONTH="" DAY=""/>   'SIW392
        '        <PERCENT_DISABLED>{integer}</PERCENT_DISABLED>   'SIW392
        '        <POSITION CODE="*">Default</POSITION>   'SIW392
        '      </RESTRICTED_DAYS>   'SIW392
        '           <!-- Multiple Lines of Restricted Days of different ID's allowed -->   'SIW392
        '      <PREV_WC INDICATOR="YES|NO">                              
        '         <DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>     
        '         <DESCRIPTION>Description of the previous WC claim {string}</DESCRIPTION>  
        '      </PREV_WC>                                                
        '      <DOMINANT_HAND CODE="*">Default</DOMINANT_HAND>           
        '      <LIVING_ARRANGEMENTS>Describe the Living Arrangements{string}</LIVING_ARRANGEMENTS>  
        '      <MEDICAL_INFORMATION>                                     
        '         <ALLERGIES INDICATOR="YES|NO">list</ALLERGIES>           
        '         <MEDICATIONS>List the Medications{string}</MEDICATIONS>  
        '         <SPEC_NEEDS>Describe the Special Needs{string}</SPEC_NEEDS>  
        '         <HEIGHT>{string}</HEIGHT>                                 
        '         <WEIGHT>{string}</WEIGHT>                                 
        '         <SMOKER INDICATOR="YES|NO">                               
        '         <PPD>Number of Packs per Day</PPD>                        
        '         <ALCOHOLUSE INDICATOR="YES|NO">                           
        '         <CANCER INDICATOR="YES|NO">                               
        '         <DIABETES INDICATOR="YES|NO">                             
        '         <HEART_COND INDICATOR="YES|NO">                           
        '         <HEPATITIS INDICATOR="YES|NO">                            
        '         <HYPERTENSION INDICATOR="YES|NO">                         
        '         <LUNG_DISEASE INDICATOR="YES|NO">                         
        '         <SEIZURES INDICATOR="YES|NO">                             
        '         <BLOOD_DISORDER INDICATOR="YES|NO">                       
        '         <MENTAL_DISORDER INDICATOR="YES|NO">                      
        '         <ARTHRITIS INDICATOR="YES|NO">                            
        '         <PAST_SURG_DESCRIPTION>Describe the Past Surgeries{string}</PAST_SURG_DESCRIPTION>  
        '         <OTHER_DESCRIPTION>List the other conditions{string}</OTHER_DESCRIPTION>  
        '      </MEDICAL_INFORMATION>                                     
        '      <ATTY_AUTH_REP INDICATOR="No" />
        '      <BENEFITS_OFFSET TYPE="[UNEMPLOYMENT|SOCIALSECURITY|PENSIONPLAN|SPECIALFUND|OTHER]">Amount</BENEFITS_OFFSET>
        '         <!-- Multiple rows allowed -->
        '      <PARTY_INVOLVED ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</PARTY_INVOLVED>
        '         <!--Multiple Parties Involved at the employee level such as Dependents, Beneficiaries, etc.-->
        '      <COMMENT_REFERENCE ID="CMTxx"/>
        '         <!-- multiple entries -->
        '   </EMPLOYEE>
        '</parent>*/

        public const string sEmpNode = "EMPLOYEE";
        public const string sEmpClmntNumber = "CLAIMANT_NUMBER";
        public const string sEmpEntityID = "ID";
        public const string sEmpNumber = "EMPLOYEE_NUMBER";
        public const string sEmpStatus = "EMPLOYMENT_STATUS";
        public const string sEmpStatActive = "ACTIVE";
        public const string sEmpStatFullTime = "FULLTIME";
        public const string sEmpRTW = "RETURNED_TO_WORK";
        public const string sEmpRTWNextDay = "NEXT_DAY";
        public const string sEmpRTWFullDuties = "FULL_DUTIES";
        public const string sEmpOccupation = "OCCUPATION";
        public const string sEmpHiredDate = "DATE_HIRED";
        public const string sEmpTerminatedDate = "DATE_TERMINATED";
        public const string sEmpStateHired = "STATE_HIRED";
        public const string sEmpWorkPermitDate = "WORK_PERMIT_DATE";
        public const string sEmpWorkPermitNumber = "WORK_PERMIT_NUMBER";
        public const string sEmpExemptStatus = "EXEMPT_STATUS";                   //SIW44
        public const string sEmpRegularJobStatus = "REGULAR_JOB_STATUS";          //SIW44
        public const string sEmpHiredInJurisdiction = "HIRED_IN_JURISDICTION";    //SIW44
        public const string sEmpAdjustedW = "ADJUSTED_WAGE";
        public const string sEmpCR = "COMPENSATION_RATE";                         //SIW44
        public const string sEmpAVW = "AVG_WKLY_WAGE";
        public const string sEmpPay = "PAY";
        public const string sEmpPayCalcMethod = "CALCULATE_METHOD";
        public const string sEmpPayType = "TYPE";
        public const string sEmpSpecified = "SPECIFIED";
        public const string sEmpPeriod = "PERIOD";
        public const string sEmpPayFullPayDayInjured = "FULL_PAY_DAY_INJURED";
        public const string sEmpPayContinued = "CONTINUED";
        public const string sEmpPayOTEligible = "OT_ELIGIBLE";
        public const string sEmpEntitlement = "ENTITLEMENT";
        public const string sEmpTimeDayBegan = "TIME_DAY_BEGAN"; //SIW391
        public const string sEmpWeeklySchedule = "WEEKLY_SCHEDULE";
        public const string sEmpWSStartDay = "WEEK_STARTS_ON";
        public const string sEmpWSNumDaysWorked = "DAYS_WORKED";
        public const string sEmpWSDay = "DAY";
        public const string sEmpWSDayRegHours = "SCHD_REG_HOURS";
        public const string sEmpWSDayEstOTHours = "EST_OT_HOURS";
        public const string sEmpWSDayShiftStart = "SHIFT_START_TIME";
        public const string sEmpWSDayShiftEnd = "SHIFT_END_TIME";
        public const string sEmpRestrictedDays = "RESTRICTED_DAYS";  //SIW392
        public const string sEmpLostDays = "LOST_DAYS";              //SIW392
        public const string sEmp1stDayNotFullPay = "FIRST_DAY_NOT_FULL_DAY";
        //public const string sEmpLastDayWorked = "LAST_DAY_WORKED";  //SIW392
        //public const string sEmpRTWDate = "DATE_RETURNED_TO_WORK"; //SIW392
        public const string sEmpAssignedDept = "ASSIGNED_DEPARTMENT";
        public const string sEmpNumbOfDpndnts = "NUMBER_OF_DEPENDANTS";
        public const string sEmpEducation = "EDUCATION";
        public const string sEmpPrimaryLanguage = "PRIMARY_LANGUAGE";
        public const string sEmpPastWorkEx = "PAST_WORK_EX";
        public const string sEmpLivingArrangements = "LIVING_ARRANGEMENTS";
        public const string sEmpPriorWorkHist = "PRIOR_WORK_HIST";
        public const string sEmpPrevWC = "PREV_WC";
        public const string sEmpPrevWCDate = "DATE";
        public const string sEmpPrevWCDesc = "DESCRIPTION";


        //Lost Days
        public const string sLSDId = "ID";                           //SIW392
        public const string sLSDIDPfx = "LSD";                       //SIW392
        public const string sLSDLastDayWorked = "LAST_DAY_WORKED";   //SIW392
        public const string sLSDRTWDate = "DATE_RETURNED_TO_WORK";   //SIW392
        public const string sLSDDuration = "DURATION"; //SIN8155 
        public const string sLSDWeeklyPay = "WEEKLY_PAY";                  //SIN8043

        //Restricted Days   
        public const string sRSDId = "ID"; //SIW392
        public const string sRSDIDPfx = "RSD"; //SIW392
        public const string sRsdPercentDisabled = "PERCENT_DISABLED";    //SIW392
        public const string sRsdFirstDayRstDate = "FIRST_DAY_RSTC";   //SIW392
        public const string sRsdLastDayRstDate = "LAST_DAY_RSTC";    //SIW392
        public const string sRsdPosition = "POSITION";              //SIW392
 
        public const string sEmpMedInfoNode = "MEDICAL_INFORMATION";
        public const string sEmpAllergies = "ALLERGIES";
        public const string sEmpMedications = "MEDICATIONS";
        public const string sEmpSpecNeeds = "SPEC_NEEDS";
        public const string sEmpHeight = "HEIGHT";
        public const string sEmpWeight = "WEIGHT";
        public const string sEmpDominantHand = "DOMINANT_HAND";
        public const string sEmpSmoker = "SMOKER";
        public const string sEmpPPD = "PPD";
        public const string sEmpAlcoholUse = "ALCOHOLUSE";
        public const string sEmpCancer = "CANCER";
        public const string sEmpDiabetes = "DIABETES";
        public const string sEmpHeartCond = "HEART_COND";
        public const string sEmpHepatitis = "HEPATITIS";
        public const string sEmpHypertension = "HYPERTENSION";
        public const string sEmpLungDisease = "LUNG_DISEASE";
        public const string sEmpSeizures = "SEIZURES";
        public const string sEmpBloodDisorder = "BLOOD_DISORDER";
        public const string sEmpMentalDisorder = "MENTAL_DISORDER";
        public const string sEmpArthritis = "ARTHRITIS";
        public const string sEmpOther = "OTHER_DESCRIPTION";
        public const string sEmpPastSurgeries = "PAST_SURG_DESCRIPTION";


        public const string sEmpAttyRepresented = "ATTY_AUTH_REP";
        public const string sEmpBenOffset = "BENEFITS_OFFSET";
        public const string sEmpPartyInvolved = "PARTY_INVOLVED";
        public const string sEmpCommentReference = "COMMENT_REFERENCE";
        public const string sEmpWeeklyHours = "WEEKLY_HOURS";
        //public const string sEmpTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        public const string sEmpEmployerRelationship = "EMR";
        public const string sSpecifiedUnspecified = "UNSPECIFIED";
        public const string sSpecifiedPreInjury = "PREINJURY";
        public const string sSpecifiedPostInjury = "POSTINJURY";
        public const string sSpecifiedOther = "OTHER";
        public const string sSpecifiedCurrent = "CURRENT";
        public const int iSpecifiedUnspecified = 0;
        public const int iSpecifiedPreInjury = 1;
        public const int iSpecifiedPostInjury = 2;
        public const int iSpecifiedOther = 3;
        public const int iSpecifiedCurrent = 4;

        public const string sBenefitTypeUnspecified = "UNSPECIFIED";
        public const string sBenefitTypeUnemployment = "UNEMPLOYMENT";
        public const string sBenefitTypeSocSec = "SOCIALSECURITY";
        public const string sBenefitTypePenPlan = "PENSIONPLAN";
        public const string sBenefitTypeSpclFund = "SPECIALFUND";
        public const string sBenefitTypeOther = "OTHER";
        public const int iBenefitTypeUnspecified = 0;
        public const int iBenefitTypeUnemployment = 1;
        public const int iBenefitTypeSocSec = 2;
        public const int iBenefitTypePenPlan = 3;
        public const int iBenefitTypeSpclFund = 4;
        public const int iBenefitTypeOther = 5;

        /*'<INJURY ID="INJxxx">
        '   <INJURED_PARTY ID="ENTxxx"/>
        '   <SPECIFICS ON_PREMISE_IND="YES|NO {boolean}"
        '              SAFETY_EQP_USED="YES|NO {boolean}"
        '              SAFETY_EQP_AVAIL="YES|NO {boolean}"/>
        '   <ILLNESS INDICATOR="TRUE|FALSE"/>
        '   <INJURY INDICATOR="TRUE|FALSE"/>
        '   <NATURE_OF_INJURY CODE="80">Default</NATURE_OF_INJURY>
        '   <CAUSE_OF_INJURY CODE="99">Default</CAUSE_OF_INJURY>
        '   <ESTIMATED_LENGTH_DISABILITY>0</ESTIMATED_LENGTH_DISABILITY>
        '   <WORK_ACTIVITY>The specific work activity that the employee was engage in. {string}</WORK_ACTIVITY>
        '   <WORK_ACTIVITY_HTML>The specific work activity that the employee was engage in.HTML format {string}</WORK_ACTIVITY_HTML> 'SIW189
        '   <WORK_PROCESS>The work process that the employee was engaged in  {string}</WORK_PROCESS>
        '   <WORK_PROCESS_HTML>The work process that the employee was engaged in.HTML format {string}</WORK_PROCESS_HTML> 'SIW189
        '   <DESCRIPTION>Describe injury or accident {string}</DESCRIPTION>
        '   <DESCRIPTION_HTML>Describe injury or accident, HTML format {string}</DESCRIPTION_HTML> 'SIW189
        '   <DESCRIPTION_OF_SYMPTOMS>Describe the symptoms{string}</DESCRIPTION_OF_SYMPTOMS>
        '   <DESC_SYMPT_HTML>Describe the symptoms{string} in HTML format</DESC_SYMPT_HTML>    'SI06369
        '   <VOCATIONAL_REHAB INDICATOR="No" />
        '   <SURGERY INDICATOR="No" />
        '   <PRIOR_INJURY INDICATOR="No" />                       
        '   <PRIOR_CLAIM INDICATOR="No" />                        
        '   <RECORDED_INTERVIEW INDICATOR="No" />                 
        '   <EOB INDICATOR="No" />                                
        '   <LOST_CONSCIOUSNESS INDICATOR="No" />
        '   <INJURY_CONSISTENT_WITH_POLICE_REPORT INDICATOR="YES|NO">
        '   <CATASTROPHIC_INJURY INDICATOR="YES|NO"/>
        '   <HOSPITAL_ER_VISIT INDICATORY="YES|NO"/>
        '   <NO_RULES_FLAG INDICATOR="YES|NO" />   'SIW189
        '   <RECORDABLE_FLAG INDICATOR="YES|NO" /> 'SIW189
        '   <FILE_CLOSE_DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '   <COLLATERAL_SOURCE CODE="">collateral source</COLLATERAL_SOURCE>
        '   <MANAGED_CARE_ORG_TYPE CODE="typeoforg">Organization Type</MANAGED_CARE_ORG_TYPE>
        '   <DATE_OF_INJURY YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '   <TIME_OF_INJURY YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '   <DATE_DISABILITY_BEGAN YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '   <DATE_DISABILITY_END YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '   <DATE_MAXIMUM_MEDICAL_IMPROVEMENT YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '   <PERCENT_OF_DISABILITY>percent</PERCENT_OF_DISABILITY>
        '   <DATE_OF_DEATH YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '   <DEPARTMENT CODE="Department Code {string} [ORG_HIERARCHY]">Deparment Where Injury Occurred {string}</DEPARTMENT>
        '   <YEAR_LAST_EXPOSED YEAR="yyyy/>
        '   <ORG_DEPT_EID ENTITY_ID="ENTXXX"></ORG_DEPT_EID> 'SIN8529
        '   <MATERIAL_INVOLVED>List of materials (Chemical, Biological, Equipment) involved {string}</MATERIAL_INVOLVED>
        '       <!-- Multiple Lines Allowed -->
        '   <BODY_PART CODE="Body Part Code {string} [BODY_PART]">Translated Body Part {string}</BODY_PART>
        '       <!-- Multiple Body Parts Allowed -->
        '   <PRE_EXISTING_CONDITION CODE="code>ncci diagnosis codes</PRE_EXISTING_CONDITION>
        '   <DIAGNOSIS>
        '      <SPECIFIC CODE="ICD-9 Code {string} [ICD9]">Diagnosis Translated {string}</SPECIFIC>
        '      <DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '   </DIAGNOSIS>
        '        <!-- Multiple Diagonsis Codes Allowed -->
        '   <TREATMENT>
        '      <SPECIFIC CODE="Standard CPT Treatment Code {string} [TREATMENT_CODE]" TYPE="Code Type (CPT,HCFA, etc.) {string} [CPT,HCFA]">description</SPECIFIC>
        '      <STARTED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <ENDED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <TREATED_BY ENTITY_ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</TREATED_BY>
        '      <DATE_INITIAL_VISIT YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>       
        '      <PHYSICIAN_SPECIALITY CODE ="DOC">description</PHYSICIAN_SPECIALITY>       
        '   </TREATMENT>
        '      <!--Multiple treatments allowed-->
        '   <MEDICATION CODE="" DOSE="">description</MEDICATION>
        '      <!--Multiple Medication allowed-->
        '   <WEEKLY_OFFSET>Amount</WEEKLY_OFFSET>        
        'Start SIW7309
        '   <DESCRIPTION_OF_PROGNOSIS>Describe the prognosis{string}</DESCRIPTION_OF_PROGNOSIS>
        '   <DESCRIPTION_OF_PROGNOSIS_HTML>Describe the prognosis{string}</DESCRIPTION_OF_PROGNOSIS_HTML>
        '   <IMPAIRMENT_FLAG INDICATOR="TRUE|FALSE"/>
        '   <WHERE_INJURED_TAKEN>Address (string)</WHEN_INJURED_TAKEN>
        'End SIW7309           
        '   <EMPLOYEE>
        '      ..Employee Information
        '   </EMPLOYEE>
        '   <PARTY_INVOLVED ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</PARTY_INVOLVED>
        '        <!--Multiple Parties Involved-->
        '      <!Multiple Demand Offers Allowed>               //SIW7419
        '      <DEMAND_OFFER> ... </DEMAND_OFFER>
        '   <COMMENT_REFERENCE ID="CMTxx"/>
        '        <!-- multiple entries -->
        '</INJURY>*/
        public const string sInjNode = "INJURY";
        public const string sInjID = "ID";
        public const string sInjIDPfx = "INJ";
        public const string sInjParty = "INJURED_PARTY";
        public const string sInjSpecifics = "SPECIFICS";
        public const string sInjOnPremise = "ON_PREMISE_IND";
        public const string sInjSafetyEqpUsed = "SAFETY_EQP_USED";
        public const string sInjSafetyEqpAvailable = "SAFETY_EQP_AVAIL";
        public const string sInjIllness = "ILLNESS";
        public const string sInjInjury = "INJURY";
        public const string sInjNatureOfInjury = "NATURE_OF_INJURY";
        public const string sInjCauseOfInjury = "CAUSE_OF_INJURY";
        public const string sInjEstLenOfDisability = "ESTIMATED_LENGTH_DISABILITY";
        public const string sInjWorkActivity = "WORK_ACTIVITY";
        public const string sInjWorkActivityHTML = "WORK_ACTIVITY_HTML"; //SIW189
        public const string sInjWorkProcess = "WORK_PROCESS";
        public const string sInjWorkProcessHTML = "WORK_PROCESS_HTML"; //SIW189
        public const string sInjDescriptionOfInjury = "DESCRIPTION";
        public const string sInjDescriptionOfInjuryHTML = "DESCRIPTION_HTML"; //SIW189
        public const string sInjDescriptionOfSymptoms = "DESCRIPTION_OF_SYMPTOMS";
        public const string sInjDescriptionOfSymptomsHTML = "DESC_SYMPT_HTML";      //SI06369
        public const string sInjVocRehabilition = "VOCATIONAL_REHAB";
        public const string sInjSurgery = "SURGERY";
        public const string sInjPriorInjury = "PRIOR_INJURY";
        public const string sInjPriorClaim = "PRIOR_CLAIM";
        public const string sInjRecordedInterview = "RECORDED_INTERVIEW";
        public const string sInjEOB = "EOB";
        public const string sInjLostCons = "LOST_CONSCIOUSNESS";
        public const string sInjConWPolRpt = "INJURY_CONSISTENT_WITH_POLICE_REPORT";
        public const string sInjCatasInjury = "CATASTROPHIC_INJURY";
        public const string sInjHospER = "HOSPITAL_ER_VISIT";
        public const string sInjNoRulesFlag = "NO_RULES_FLAG";      //SIW189
        public const string sInjRecordableFlag = "RECORDABLE_FLAG"; //SIW189
        public const string sInjFileCloseDate = "FILE_CLOSE_DATE";
        public const string sInjCollSource = "COLLATERAL_SOURCE";
        public const string sInjManagedCareOrgType = "MANAGED_CARE_ORG_TYPE";
        public const string sInjDateOfInjury = "DATE_OF_INJURY";
        public const string sInjTimeOfInjury = "TIME_OF_INJURY";
        public const string sInjDateDisBegan = "DATE_DISABILITY_BEGAN";
        public const string sInjDateDisEnd = "DATE_DISABILITY_END";
        public const string sInjDateMaxMedImp = "DATE_MAXIMUM_MEDICAL_IMPROVEMENT";
        public const string sInjPrctOfDis = "PERCENT_OF_DISABILITY";
        public const string sInjDateOfDeath = "DATE_OF_DEATH";
        public const string sInjInvolvedDepartment = "DEPARTMENT";
        public const string sInjYrLastExposed = "YEAR_LAST_EXPOSED";
        public const string sInjYrLastExposedAtt = "YEAR"; //SIW44
        public const string sInjMaterialInvolved = "MATERIAL_INVOLVED";
        public const string sInjBodyPart = "BODY_PART";
        public const string sInjPreExistCond = "PRE_EXISTING_CONDITION";
        public const string sInjDiagnosis = "DIAGNOSIS";
        public const string sInjDiagSpecific = "SPECIFIC";
        public const string sInjDiagDate = "DATE";
        public const string sInjTreatment = "TREATMENT";
        public const string sInjTreatSpecific = "SPECIFIC";
        public const string sInjTreatStarted = "STARTED";
        public const string sInjTreatEnded = "ENDED";
        public const string sInjTreatedBy = "TREATED_BY";
        public const string sInjDateInitialVisit = "DATE_INITIAL_VISIT";
        public const string sInjPhysicianSpeciality = "PHYSICIAN_SPECIALITY";
        public const string sInjMedication = "MEDICATION";
        public const string sInjMedicationDose = "DOSE";
        public const string sInjWeeklyOffset = "WEEKLY_OFFSET";
        public const string sInjPartyInvolved = "PARTY_INVOLVED";
        public const string sInjDemandOffer = "DEMAND_OFFER";            //SIW7419
        public const string sInjComment = "COMMENT_REFERENCE";
        public const string sInjEmployee = "EMPLOYEE";
        public const string sInjOrgDept = "ORG_DEPT_EID"; //SIN8529
        //SIW7309 Start
        public const string sInjDescriptionOfPrognosis = "DESCRIPTION_OF_PROGNOSIS";
        public const string sInjDescriptionOfPrognosisHTML = "DESCRIPTION_OF_PROGNOSIS_HTML";
        public const string sInjImpairmentFlag = "IMPAIRMENT_FLAG";
        public const string sInjWhereInjuredTaken = "WHERE_INJURED_TAKEN";
        //SIW7309 End
        //public const string sInjTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

       /* 'SI06467 Start

        '<CLAIM>
        '   <INJURY ID="INJxxx">
        '      <TREATMENT>
        '         <TECH_KEY>AC Technical key for the element </TECH_KEY>
        '         <SPECIFIC CODE="Standard CPT Treatment Code {string} [TREATMENT_CODE]" TYPE="Code Type (CPT,HCFA, etc.) {string} [CPT,HCFA]">description</SPECIFIC>
        '         <STARTED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '         <ENDED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '         <TREATED_BY ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</TREATED_BY>
        '         <DATE_INITIAL_VISIT YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '         <PHYSICIAN_SPECIALITY CODE ="DOC">description</PHYSICIAN_SPECIALITY>
        '         <PRE_CERTIFICATION>
        '            <TECH_KEY>AC Technical key for the element </TECH_KEY>
        '            <CLASS_DESCRIPTION CODE="Class Description Code {string} [PRECERT_CLASS_TYPE]" TYPE="Code Type (DEC,NON, PRE) {string} [DEC,NON,PRE]">description</CLASS_DESCRIPTION>
        '            <DATE_RECEIVED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '            <TIME_OF_EXAM YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '            <TYPE_REQUEST CODE="Type Request Desc Code {string} [PRECERT_REQUEST_TY]" TYPE="Code Type (TES,TRE) {string} [TES,TRE]">description</TYPE_REQUEST>
        '            <PROCEDURE_DESCRIPTION>Procedure description </PROCEDURE_DESCRIPTION>
        '            <NUMBER_OF_VISITS_REQ> Number of Visits Required</NUMBER_OF_VISITS_REQ>
        '            <DECISION_DESC CODE="Decision Desc Code {string} [PRECERT_DEC_TYPE]" TYPE="Code Type (APP,DEN,MOD) {string} [APP,DEN,MOD]">description</DECISION_DESC>
        '            <DATE_OF_DECISION YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '            <REASON_DECISION_DESC CODE="Reason Decision Desc Code {string} [PRECERT_REAS_DEC_T]" TYPE="Code Type (A,B,C,D1, etc.) {string} [A,B]">description</REASON_DECISION_DESC>
        '            <NEXT_REVIEW YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '            <NUMBER_OF_VISITS> Number of Actual Visits</NUMBER_OF_VISITS>
        '            <VISIT_FROM YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '            <VISIT_TO YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '            <PERCENT_COPAY INDICATOR="Yes/No" />
        '            <APPEAL_DECISION CODE="Appeal Decision Code {string} [PRECERT_APPEAL_DEC]" TYPE="Code Type (APP,DEN,MOD) {string} [APP,DEN,MOD]">description</APPEAL_DECISION>
        '            <APPEAL_RECEIVED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '            <APPEAL_RECEIVED_DECISION YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '            <NEXT_APPEAL YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '            <PRECERT_RESULTS></PRECERT_RESULTS>
        '         </PRE_CERTIFICATION>
        '         <!--Multiple Pre-Certification allowed-->
        '         <IME>
        '             <TECH_KEY>AC Technical key for the element </TECH_KEY>
        '            <EXAM_SCHEDULED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '            <TIME_OF_EXAM YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '            <IME_REQUESTED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '            <EXAM_COMPLETED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '            <IME_RESULTS></IME_RESULTS>
        '            <IME_RESULTS_HTML></IME_RESULTS_HTML>     'SIW254
        '         </IME>
        '         <!--Multiple Indepedent Medical Exams (IME) allowed-->
        '      </TREATMENT>
        '         <!--Multiple treatments allowed-->
        '   </INJURY>
        '</CLAIM>*/

         public const string sTreatPreCertNode = "PRE_CERTIFICATION";
         public const string sTreatPCClassDesc = "CLASS_DESCRIPTION";
         public const string sTreatPCDateReceived = "DATE_RECEIVED";
         public const string sTreatPCTimeOfExam = "TIME_OF_EXAM";
         public const string sTreatPCTypeRequest = "TYPE_REQUEST";
         public const string sTreatPCProcedureDesc = "PROCEDURE_DESCRIPTION";
         public const string sTreatPCNumOfVisitReq = "NUMBER_OF_VISITS_REQ";
         public const string sTreatPCDecisionDesc = "DECISION_DESC";
         public const string sTreatPCDecisionDate = "DATE_OF_DECISION";
         public const string sTreatPCReasonDecisionDesc = "REASON_DECISION_DESC";
         public const string sTreatPCNextReview = "NEXT_REVIEW";
         public const string sTreatPCNumOfVisits = "NUMBER_OF_VISITS";
         public const string sTreatPCVisitFromYear = "VISIT_FROM";
         public const string sTreatPCVisitToYear = "VISIT_TO";
         public const string sTreatPCPercentCoPay = "PERCENT_COPAY";
         public const string sTreatPCAppealDescision = "APPEAL_RECEIVED";
         public const string sTreatPCAppealReceived = "APPEAL_RECEIVED_DECISION";
         public const string sTreatPCAppealReceivedDecision = "";
         public const string sTreatPCNextAppeal = "NEXT_APPEAL";
         public const string sTreatPCResults = "PRECERT_RESULTS";
         public const string sTreatIMENode = "IME";
         public const string sTreatIMEExamSched = "EXAM_SCHEDULED";
         public const string sTreatIMETimeOfExam = "TIME_OF_EXAM";
         public const string sTreatIMERequested = "IME_REQUESTED";
         public const string sTreatIMECompleted = "EXAM_COMPLETED";
         public const string sTreatIMEResults = "IME_RESULTS";
         public const string sTreatIMEResultsHTML = "IME_RESULTS_HTML"; //SIW254
         //public const string sTreatTechKey = "TECH_KEY";//SIW360
         //public const string sIMETechKey = "TECH_KEY";//SIW360
         //public const string sPreCertTechKey = "TECH_KEY";//SIW360
          //End SI06467
        
        /*'  <!--Multiple levels -->
        '   <PARTY_INVOLVED ID="ENTxxx" CODE="Role">
        '      <TYPE>Role Description</TYPE>
         '      <EXPIRY_DATE YEAR="year" MONTH="month" DAY="day"/>       'SIW403
        '      <DATA_ITEM/>
        '      <PARTY_INVOLVED ID="ENTxxx" CODE="Role">
        '       <FIRST_NAME></FIRST_NAME>
        '        ...
        '      </PARTY_INVOLVED>
        '      <COMMENT_REFERENCE> ... </COMMENT>
        '   </PARTY_INVOLVED>
        '     <!-- Multiple Occurrences -->*/
        public const string sPartyInvolvedNode = "PARTY_INVOLVED";
        // SI06650 public const string sPIID = "ENTITY_ID";
        public const string sPIID = "ID"; // IS06650
        public const string sPIEntId = "ENTITY_ID"; // IS06650 //SIW360
        public const string sPIEntRefId = "ENTITYIDREF"; //SIW493
        // SI06650 public const string sPIIDPfx = "ENT";
        public const string sPIIDPfx = "PI"; // SI06650
        public const string sPIType = "TYPE";
        public const string sPIDataItem = "DATA_ITEM";
        public const string sPIComment = "COMMENT_REFERENCE";
        public const string sPIExpDate = "EXPIRATION_DATE";     //SIW403
        public const string sInsuredName = "FIRST_NAME"; //SIN7750
        //public const string sPITechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<ENTITIES NEXT_ID="0" COUNT="xx">
        '   <ENTITY ID="ENTid {id}" TYPE="Business, Individual, Household {string} [ENTITY_TYPE]">
        '      <ENTITY_NAME>
        '         <INDIVIDUAL>
        '            <PREFIX ID="CodeID" CODE="ShortCode">(MR|MISS|MS|MRS|etc.) {string} [PREFIX]</PREFIX>   'SIN8440
        '            <FIRST_NAME>First Name {string}</FIRST_NAME>
        '            <MIDDLE_NAME>Middle Name {string}</MIDDLE_NAME>
        '            <LAST_NAME>Last Name {string}</LAST_NAME>
        '            <SUFFIX ID="CodeID" CODE="ShortCode">(II|III|JR|SR|etc.) {string} [SUFFIX]</SUFFIX>     'SIN8440
        '            <INITIALS>mdm</INITIALS>
        '            <AKA>alsoknownas</AKA>
        '            <USER_ID> ... </USER_ID>                     'SIW316
        '         </INDIVIDUAL>
        '         <BUSINESS>
        '            <BUSINESS_NAME>business name {string)</BUSINESS_NAME>       
        '            <LEGAL_NAME>Legal Business Name {string}</LEGAL_NAME>
        '            <ABBREVIATION>LBN {string}</ABBREVIATION>
        '            <DOING_BUSINESS_AS>Doing Business As Name {string}</DOING_BUSINESS_AS>
        '         </BUSINESS>
        '      </ENTITY_NAME>
        '         <EXPIRATION_DATE YEAR='yyyyy' MONTH='mm' DAY='dd'>displaydate</EXPIRATION_DATE>   'SIW7127
        '         <EFFECTIVE_DATE YEAR='yyyy' MONTH='mm' DAY='dd'>displaydate</EFFECTIVE_DATE> 'SIW7127
        '      <TAX_ID _OPTIONAL_="" TYPE="TIN|SSN {string} [TIN, SSN]">Tax ID Number {string}</TAX_ID>
        '      <REPORTABLE_TYPE_1099 CODE="">description</REPORTABLE_TYPE_1099>
        '      <W9_INFO VALID='True'|'False'>                                                            
        '         <DATE YEAR='yyyyy' MONTH='mm' DAY='dd'>displaydate</DATE>                              
        '         <REQUEST_DATE YEAR='yyyyy' MONTH='mm' DAY='dd'>displaydate</REQUEST_DATE>              
        '         <LAST_IRS_EXCP_DATE YEAR='yyyyy' MONTH='mm' DAY='dd'>displaydate</LAST_IRS_EXCP_DATE>  
        '      </W9_INFO>      
        '         <ENT_EFFECTIVE_START_DATE YEAR='yyyyy' MONTH='mm' DAY='dd'/>   'N8584
        '         <ENT_EFFECTIVE_END_DATE YEAR='yyyyy' MONTH='mm' DAY='dd'/>     'N8584 
        '      <BUWH ENABLED='True'|'False'>                                                             
        '         <START_DATE YEAR='yyyyy' MONTH='mm' DAY='dd'>displaydate</START_DATE>                  
        '         <END_DATE YEAR='yyyyy' MONTH='mm' DAY='dd'>displaydate</END_DATE>                      
        '         <PERCENT>percent</PERCENT>                                                             
        '      </BUWH>                                                                                   
        '      <DEMOGRAPHICS _OPTIONAL_="">
        '         <GENDER CODE="M|F|U {string} [GENDER]">Gender Translated {string}</GENDER>
        '         <DATE_OF_BIRTH YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '         <DATE_OF_DEATH YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '         <MARITAL_STATUS CODE="M|S {string} [MARITAL_STATUS]" SPOUSE="ENTid  References a specific entity {id}">Marrital Status Translated {string}</MARITAL_STATUS>
        '         <EDUCATION CODE="*">Default</EDUCATION>               
        '         <PRIMARY_LANGUAGE CODE="*">Default</PRIMARY_LANGUAGE>   
        '         <DRIVERS_LICENSE>
        '            <STATE CODE="sc">STATE</STATE>
        '            <NUMBER>dkskldk</NUMBER>
        '            <EFFECTIVE_DATE YEAR="" MONTH="" DAY=""/>
        '            <EXPIRATION_DATE YEAR="" MONTH="" DAY=""/>
        '            <QUALIFER CODE="">qualifier</QUALIFIER>
        '                <!--Multiples allowed-->
        '                <!--Restrictions and priveleges-->
        '         </DRIVERS_LICENSE>
        '         <RELATION_TO_INSD CODE="">relation <RELATION_TO_INSD/>   
        '      </DEMOGRAPHICS>
        '      <BUSINESS_DATA>
        '        <SIC>SICCode {string}</SIC>
        '        <NAIC>naic code</NAIC_CODE>
        '        <NATURE_OF_BUSINESS CODE="">Nature of business {string}</NATURE_OF_BUSINESS>
        '        <UNEMP_INS_ACCT>Unemployement Insurance Account Number {string}</UNEMP_INS_ACT>
        '        <WC_FILING_NUMBER>filingnumber</WC_FILING_NUMBER>                  
        '        <TYPE_OF_BUSINESS CODE="CORP">Corporation</TYPE_OF_BUSINESS>
        '        <CONTACT_NAME>person to contact</CONTACT_NAME>                     
        '        <BUSINESS_LICENSE>
        '            <STATE CODE="sc">STATE</STATE>
        '            <NUMBER>dkskldk</NUMBER>
        '            <EFFECTIVE_DATE YEAR="" MONTH="" DAY=""/>
        '            <EXPIRATION_DATE YEAR="" MONTH="" DAY=""/>
        '            <QUALIFER CODE="">qualifier</QUALIFIER>
        '                <!--Multiples allowed-->
        '                <!--Restrictions and priveleges-->
        '        </BUSINESS_LICENSE>
        '      <BUSINESS_DATA>
        '      <CONTACT PRIMARY="YES|NO">
        '         <!--Mulitiple contacts allowed per entity-->
        '         <WHO ENTITY_ID="Entity Contact ID {id}"/>
        '         <TYPE CODE="Type of Contact [ENT_RELATIONSHIPS]">Business Phone, Home Phone, etc. {string}</TYPE>
        '         <INFORMATION>contact data</INFORMATION>
        '         <AFTER HOUR="Hour (2) {integer} [0-23]" MINUTE="Minute (2) {integer} [0-60]" SECOND="Second (2) {integer} [0-60]"/>
        '         <BEFORE HOUR="Hour (2) {integer} [0-23]" MINUTE="Minute (2) {integer} [0-60]" SECOND="Second (2) {integer} [0-60]"/>
        '         <EFFECTIVE_START_DATE YEAR='yyyyy' MONTH='mm' DAY='dd'/>   'SI06249
        '         <EFFECTIVE_END_DATE YEAR='yyyyy' MONTH='mm' DAY='dd'/>     'SI06249  
        '      </CONTACT>
        '      <ADDRESS_REFERENCE ID="ARDid" CODE="Type of Address Code" PRIMARY="YES|NO">Address Type Translated {string}</ADDRESS_REFERENCE>
        '      <ACCOUNT> ... </ACCOUNT>		            //SIW7182
        '        <!-- multiple entries -->
        '      <RELATED_ENTITY ID="related entity id" CODE="relationshipcode">relationship</RELATED_ENTITY>
        '        <!-- multiple entries -->
        '      <CATEGORY CODE="code">category</CATEGORY>
        '        <!-- multiple entries -->
        ' SIN7587 Start
        '    //<ID_NUMBER TYPE="type of id number>idnumber</ID_NUMBER>
        '   <ID_NUMBER>
        '       <ID_NBR_ROW_ID></ID_NBR_ROW_ID> 
        '       <ID_NBR_TYPE></ID_NBR_TYPE> 
        '       <EFF_START_DATE YEAR="" MONTH="" DAY="" /> 
        '       <EFF_END_DATE YEAR="" MONTH="" DAY="" /> 
        '       <ID_NBR></ID_NBR> 
        '   </ID_NUMBER>
        'SIN7587 End
        '        <!-- multiple entries -->
        '      <COMMENT_REFERENCE ID="CMTxx"/>
        '        <!-- multiple entries -->
        '      <DOCUMENT_REFERENCE ID="DOCxx"/>              
        '        <!-- multiple entries -->                   
        '      <TRIGGERS> ... </TRIGGERS>                    
        ' Begin SIW489
        '      <COMBINED_PAY ID="CPAYxxx">
        '         <!--Mulitiple combined pay entries allowed per entity-->
        '           <TECH_KEY>AC Technical key for the element </TECH_KEY>  'SI06023
        '           <ADDRESS_REFERENCE ID="RELATIONID"
        '                      ADDRESS_ID="ADRid  References an address in the Addresses Collection {id}"
        '                      CODE="Relationship Code">Type of</ADDRESS>
        '                     <TECH_KEY>technical key value for relationship</TECH_KEY>
        '           </ADDRESS_REFERENCE >
        '           <ACCOUNT CODE="banknumber">
        '               <TECH_KEY>AC Technical key for the element </TECH_KEY>  ' SI06543a
        '               <ROUTING_NUMBER>Routing</ROUTING_NUAMBER>
        '               <ACCOUNT_NUMBER>AccountNumber</ACCOUNT_NUMBER>
        '               <FRACTIONAL_NUMBER>fractionalnumber</FRACTIONAL_NUMBER>
        '           </ACCOUNT>
        '           <NEXT_PRINT_DATE YEAR="year" MONTH="month" DAY="day"/>
        '      </COMBINED_PAY>
        ' End SIW489
        '   </ENTITY>
        '</ENTITIES>*/

        public const string sEntCollection = "ENTITIES";
        public const string sEntNextID = "NEXT_ID";
        public const string sEntCount = "COUNT";
        public const string sEntNode = "ENTITY";
        public const string sEntID = "ID";
        public const string sEntType = "TYPE";
        public const string sEntRelationship = "CODE";//vkumar258
        public const string sEntName = "ENTITY_NAME";
        public const string sEntIndName = "INDIVIDUAL";
        public const string sEntPrefix = "PREFIX";
        public const string sEntFName = "FIRST_NAME";
        public const string sEntMName = "MIDDLE_NAME";
        public const string sEntLName = "LAST_NAME";
        public const string sEntSuffix = "SUFFIX";
        public const string sEntInitials = "INITIALS";
        public const string sEntAKA = "AKA";
        public const string sEntBusName = "BUSINESS";
        public const string sEntBusinessName = "BUSINESS_NAME";
        public const string sEntLglName = "LEGAL_NAME";
        public const string sEntAbbrev = "ABBREVIATION";
        public const string sEntDBAName = "DOING_BUSINESS_AS";
        public const string sEntExpDate = "EXPIRATION_DATE"; //SIW7127
        public const string sEntEfcDate = "EFFECTIVE_DATE"; //SIW7127
        public const string sEntTaxID = "TAX_ID";
        public const string sEntTaxIdType = "TYPE";
        public const string sEntTaxIdTypeNA = " ";
        public const string sEntTaxIdTypeSSN = "SSN";
        public const string sEntTaxIdTypeFEIN = "FEIN";
        public const int iEntTaxIdTypeNA = 0;
        public const int iEntTaxIdTypeSSN = 1;
        public const int iEntTaxIdTypeFEIN = 2;
        public const string sEnt1099RptType = "REPORTABLE_TYPE_1099";
        public const string sEntW9Info = "W9_INFO";
        public const string sEntW9Valid = "VALID";
        public const string sEntW9Date = "DATE";
        public const string sEntW9ReqDate = "REQUEST_DATE";
        public const string sEntW9LastIRSExcepDate = "LAST_IRS_EXCP_DATE";
        public const string sEntEffStartDt = "ENT_EFFECTIVE_START_DATE";  //N8584
        public const string sEntEffEndDt = "ENT_EFFECTIVE_END_DATE";      //N8584
        public const string sEntBUWH = "BUWH";
        public const string sEntBUWHEnabled = "ENABLED";
        public const string sEntBUWHStartDate = "START_DATE";
        public const string sEntBUWHEndDate = "END_DATE";
        public const string sEntBUWHPercent = "PERCENT";
        public const string sEntAddress = "ADDRESS_REFERENCE";
        public const string sEntAccount = "ACCOUNT"; //SIW7182
        public const string sEntDemographics = "DEMOGRAPHICS";
        public const string sEntGender = "GENDER";
        public const string sEntDOB = "DATE_OF_BIRTH";
        public const string sEntDOD = "DATE_OF_DEATH";
        public const string sEntMarStatus = "MARITAL_STATUS";
        public const string sEntSpouseId = "SPOUSE";
        public const string sEntEducation = "EDUCATION";
        public const string sEntRelationToInsd = "RELATION_TO_INSD";
        public const string sEntPrimaryLanguage = "PRIMARY_LANGUAGE";
        public const string sEntDriverLic = "DRIVERS_LICENSE";
        public const string sEntDLState = "STATE";
        public const string sEntDLNumber = "NUMBER";
        public const string sEntDLEffDt = "EFFECTIVE_DATE";
        public const string sEntDLExpDt = "EXPIRATION_DATE";
        public const string sEntDLQualifier = "QUALIFIER";
        public const string sEntBusData = "BUSINESS_DATA";
        public const string sEntSIC = "SIC";
        public const string sEntNAIC = "NAIC";
        public const string sEntNatofBus = "NATURE_OF_BUSINESS";
        public const string sEntUnempInsAcct = "UNEMP_INS_ACCT";
        public const string sEntWCFilingNum = "WC_FILING_NUMBER";
        public const string sEntBusType = "TYPE_OF_BUSINESS";
        public const string sEntBusContactName = "CONTACT_NAME";
        public const string sEntBusLicense = "BUSINESS_LICENSE";
        public const string sEntBusLicState = "STATE";
        public const string sEntBusLicNumber = "NUMBER";
        public const string sEntBusLicEffDt = "EFFECTIVE_DATE";
        public const string sEntBusLicExpDt = "EXPIRATION_DATE";
        public const string sEntBusLicQualifier = "QUALIFER";
        public const string sEntContact = "CONTACT";
        public const string sEntContPrimary = "PRIMARY";
        public const string sEntContWho = "WHO";
        public const string sEntContType = "TYPE";
        public const string sEntContInfo = "INFORMATION";
        public const string sEntContAfter = "AFTER";
        public const string sEntContBefore = "BEFORE";
        public const string sContactRelId = "REL_ID"; //SIW7790

        // Start SIW7620
        public const string sEntPHYSICIAN = "PHYSICIAN";
        public const string sEntPHYSEID = "PHYS_EID"; 
        public const string sEntPHYSICIANNUMBER = "PHYSICIAN_NUMBER"; 
        public const string sEntMEDSTAFFNUMBER = "MED_STAFF_NUMBER"; 
        public const string sEntMEDICARENUMBER = "MEDICARE_NUMBER"; 
        public const string sEntPRIMARYSPECIALTY = "PRIMARY_SPECIALTY"; 
        //SIN8034 start
        public const string sEntSubSpecID = "ID";
        public const string sEntSubSpecIDPfx = "SUBSPEC";
        public const string sEntSubSpecPEID = "PARENT_ENTRY_ID";
        public const string sEntSubSpecEntry = "ENTRY";
        public const string sEntSubSpecCode = "SUB_SPECIALITY_CODE";
        public const string sEntSubSpecialities = "SUB_SPECIALITIES";
        public const string sEntSubSpeciality = "SUB_SPECIALITY";
        public const string sEntSubSpecNextID = "NEXTID";
        public const string sEntSubSpecCount = "COUNT";
        // SIN8034 ends
        public const string sEntSTAFFSTATUSCODE = "STAFF_STATUS_CODE"; 
        public const string sEntSTAFFTYPECODE = "STAFF_TYPE_CODE"; 
        public const string sEntSTAFFCATCODE = "STAFF_CAT_CODE"; 
        public const string sEntINTERNALNUMBER = "INTERNAL_NUMBER"; 
        public const string sEntDEPTASSIGNEDEID = "DEPT_ASSIGNED_EID"; 
        public const string sEntAPPOINTDATE = "APPOINT_DATE";
        public const string sEntREAPPOINTDATE = "REAPPOINT_DATE";
        public const string sEntLICSTATE = "LIC_STATE"; 
        public const string sEntLICNUM = "LIC_NUM"; 
        public const string sEntLICISSUEDATE = "LIC_ISSUE_DATE";
        public const string sEntLICEXPIRYDATE = "LIC_EXPIRY_DATE";
        public const string sEntLICDEANUM = "LIC_DEA_NUM"; 
        public const string sEntLICDEAEXPDATE = "LIC_DEA_EXP_DATE"; 
        public const string sEntMEMBERSHIP = "MEMBERSHIP"; 
        public const string sEntCONTEDUCATION = "CONT_EDUCATION"; 
        public const string sEntTEACHINGEXP = "TEACHING_EXP"; 
        public const string sEntINSCOMPANY = "INS_COMPANY"; 
        public const string sEntINSPOLICY = "INS_POLICY";


        public const string sEntMEDSTAFF = "MED_STAFF";
        public const string sEntSTAFFEID = "STAFF_EID";
        public const string sEntMEDISTAFFNUMBER = "MED_STAFF_NUMBER";
        public const string sEntMEDSTAFFSTATUSCODE = "STAFF_STATUS_CODE";
        public const string sEntMEDSTAFFPOSCODE = "STAFF_POS_CODE";
        public const string sEntMEDSTAFFCATCODE = "STAFF_CAT_CODE";
        public const string sEntMedDEPTASSINEDEID = "DEPT_ASSINED_EID";
        public const string sEntHIREDATE = "HIRE_DATE";
        public const string sEntMEDLICNUM = "LIC_NUM";
        public const string sEntMEDLICSTATE = "LIC_STATE";
        public const string sEntMEDLICISSUEDATE = "LIC_ISSUE_DATE";
        public const string sEntMEDLICEXPIRYDATE = "LIC_EXPIRY_DATE";
        public const string sEntMEDLICDEANUM = "LIC_DEA_NUM";
        public const string sEntMEDLICDEAEXPDATE = "LIC_DEA_EXP_DATE";


        public const string sEntPHYPRIVILEGE = "PHY_PRIVILEGE";
        public const string sEntPHYPRIVID = "PRIV_ID";
        public const string sEntPRIVPHYSEID = "PHYS_EID";
        public const string sEntPHYCATEGORY_CODE = "CATEGORY_CODE";
        public const string sEntPHYTYPE_CODE = "TYPE_CODE";
        public const string sEntPHYSTATUS_CODE = "STATUS_CODE";
        public const string sEntPHYINT_DATE = "INT_DATE";
        public const string sEntPHYEND_DATE = "END_DATE";

        public const string sEntPHYCERTS = "PHY_CERTS";
        public const string sEntPHYCERTID = "CERT_ID";
        public const string sEntPHYNAMECODE = "NAME_CODE";
        public const string sEntPHYBOARDCODE = "BOARD_CODE";

        public const string sEntPHYEDUCATION = "PHY_EDUCATION";
        public const string sEntPHYEDUCID = "EDUC_ID";
        public const string sEntPHYEDUCTYPECODE = "EDUC_TYPE_CODE";
        public const string sEntPHYINSTITUTIONEID = "INSTITUTION_EID";
        public const string sEntInstitutionID = "ID";        
        public const string sEntPHYDEGREETYPE = "DEGREE_TYPE";
        public const string sEntPHYDEGREEDATE = "DEGREE_DATE";


        public const string sPhyPrevHospital = "PHY_PREV_HOSP";
        public const string sPhyEntityID = "PHYS_EID";
        public const string sPrevHospID = "PREV_HOSP_ID";
        public const string sHospStatusCode = "STATUS_CODE";
        public const string sHospEntID = "HOSPITAL_EID";
        public const string sHospID = "ID";
        public const string sHospPrivCode = "PRIV_CODE";
        public const string sHospIntDate = "INT_DATE";
        public const string sHospEndDate = "END_DATE";

        public const string sEntMEDPRIVILEGE = "MED_STAFF_PRIVILEGE";
        public const string sEntMEDPRIVID = "PRIV_ID";
        public const string sEntMEDID = "MEDS_EID";
        public const string sEntMEDCATEGORYCODE = "CATEGORY_CODE";
        public const string sEntMEDTYPECODE = "TYPE_CODE";
        public const string sEntMEDSTATUSCODE = "STATUS_CODE";
        public const string sEntMEDINTDATE = "INT_DATE";
        public const string sEntMEDENDDATE = "END_DATE";

        public const string sEntMEDCERTS = "MED_STAFF_CERTS";
        public const string sEntMEDCERTID = "CERT_ID";
        public const string sEntMEDNAMECODE = "NAME_CODE";
        public const string sEntMEDBOARDCODE = "BOARD_CODE";
        // End SIW7620
        
        public const string sEntRelatedEnt = "RELATED_ENTITY";
        public const string sEntCategory = "CATEGORY";
        public const string sEntIDNumber = "ID_NUMBER";
        public const string sEntIDNType = "TYPE";
        public const string sEntComment = "COMMENT_REFERENCE";
        //SIW7076  public const string sEntContactID = "CONTACT_ID"; //SIW7076
        public const string sEntContactID = "ID"; //SIW7076
        public const string sEntContactIDPrefix = "CON"; //SIW7076
        public const string sEntTriggers = "TRIGGERS";
        //SIN7587 Start
        
        //Start SIW8067 - Constants updated for Entity ID Number
        //public const string sEntIDNumberRowID = "ID_NBR_ROW_ID"; 
        //SIW8067public const string sEntIDNumberType = "ID_NBR_TYPE";
        //SIW8067public const string sEntIDNumberNbr = "ID_NBR";
        //SIW8067public const string sEntIDNumberEffStartDate = "EFF_START_DATE";
        //SIW8067public const string sEntIDNumberEffEndDate = "EFF_END_DATE";
        public const string sEntIDNumberID = "ID";
        public const string sEntIDNIDPfx = "IDN";
        //public const string sEntIDNType = "TYPE";          
        public const string sEntIDNumberNbr = "NUMBER";        
        public const string sEntIDNumberEffStartDate = "EFFECTIVE_DATE";
        public const string sEntIDNumberEffEndDate = "EXPIRATION_DATE";

        //SIW8067 End
        //SIN7587 End
        //Start SIW12
        //public const string sEntTypeBus = "Business";
        //public const string sEntTypeInd = "Individual";
        //public const string sEntTypeHou = "Household";
        public const string sEntTypeBus = "BUSINESS";
        public const string sEntTypeInd = "INDIVIDUAL";
        public const string sEntTypeHou = "HOUSEHOLD";
        //End SIW12
        //Start SIN8499
        public const string sEntTypeClientSec = "CLIENT_SEC";
        public const string sEntTypeCompanySec = "COMPANY_SEC";
        public const string sEntTypeOperationSec = "OPERATION_SEC";
        public const string sEntTypeRegionSec = "REGION_SEC";
        public const string sEntTypeDivisionSec = "DIVISION_SEC";
        public const string sEntTypeLocationSec = "LOCATION_SEC";
        public const string sEntTypeFacilitySec = "FACILITY_SEC";
        public const string sEntTypeDepartmentSec = "DEPARTMENT_SEC";
        //End SIN8499
        public const string sEntTypeClient = "CLIENT";
        public const string sEntTypeCompany = "COMPANY";
        public const string sEntTypeOperation = "OPERATION";
        public const string sEntTypeRegion = "REGION";
        public const string sEntTypeDivision = "DIVISION";
        public const string sEntTypeLocation = "LOCATION";
        public const string sEntTypeFacility = "FACILITY";
        public const string sEntTypeDepartment = "DEPARTMENT";
        public const string sEntDocument = "DOCUMENT_REFERENCE";
        public const int iEntTypeBus = 1;
        public const int iEntTypeInd = 2;
        public const int iEntTypeHou = 3;
        //Start SIN8499
        public const int iEntTypeClientSec = 905;
        public const int iEntTypeCompanySec = 906;
        public const int iEntTypeOperationSec = 907;
        public const int iEntTypeRegionSec = 908;
        public const int iEntTypeDivisionSec = 909;
        public const int iEntTypeLocationSec = 910;
        public const int iEntTypeFacilitySec = 911;
        public const int iEntTypeDepartmentSec = 912;
        //End SIN8499
        public const int iEntTypeClient = 1005;
        public const int iEntTypeCompany = 1006;
        public const int iEntTypeOperation = 1007;
        public const int iEntTypeRegion = 1008;
        public const int iEntTypeDivision = 1009;
        public const int iEntTypeLocation = 1010;
        public const int iEntTypeFacility = 1011;
        public const int iEntTypeDepartment = 1012;
        //public const string sEntTechKey = "TECH_KEY";           //(SI06023 - Implemented in SI06333)//SIW360
        public const string sEntDisplayName = "DISPLAY_NAME";   //SI06345 - Implemented in SI06333)
        public const string sEntContEffStartDt = "EFFECTIVE_START_DATE";  //SI06249
        public const string sEntContEffEndDt = "EFFECTIVE_END_DATE";      //SI06249  
        public const string sEntUuserId = "USER_ID";      //SIW316
        //Begin SIW489
        public const string sECPIDPfx  = "CPAY";
        public const string sEntCombPay = "COMBINED_PAY";
        public const string sEntCombPayNextPrtDt = "NEXT_PRINT_DATE";
        public const string sEntCombPayId = "ID";
        public const string sEntCombPayAcctNode = "ACCOUNT";
        public const string sEntCombPayBankNumber = "CODE";
        public const string sEntCombPayAccountName = "NAME";
        public const string sEntCombPayRoutingNumber = "ROUTING_NUMBER";
        public const string sEntCombPayAccountNumber  = "ACCOUNT_NUMBER";
        public const string sEntCombPayFractionalNumber = "FRACTIONAL_NUMBER";
        //End SIW489
        /*'Diaries XML Node
        '<DIARIES NEXT_ID="#" COUNT="#">
        'Start SIW404
        '   <GROUP_START>#</GROUP_START>
        '   <GROUP_END>#</GROUP_END>
        '   <NAVIGATION>NEXT|PREVIOUS</NAVIGATION>
        'End SIW404
        '   <DIARY ID="DIA#">
        '      <ENTRY CODE="", CODEID="">Name (string)</ENTRY>                           'SI06472
        '      <DATE_CREATED YEAR="####" MONTH="##" DAY="##"/>
        '      <TIME_CREATED HOUR="##" MINUTE="##" SECOND="##" TIME_ZONE="EST"/>
        '      <DATE_AFTER YEAR="####" MONTH="##" DAY="##"/>          'SI06267
        '      <DATE_COMPLETE YEAR="####" MONTH="##" DAY="##"/>
        '      <TIME_COMPLETE HOUR="##" MINUTE="##" SECOND="##" TIME_ZONE="EST"/>
        '      <AUTO_ID>#</AUTO_ID>
        '      <DELETED INDICATOR="TRUE|FALSE"/>
        '      <VOID INDICATOR="TRUE|FALSE"/>
        '      <ESTIMATED_TIME>#</ESTIMATED_TIME>
        '      <NOTIFY_FLAG INDICATOR="TRUE|FALSE"/>
        '      <ROUTE_FLAG INDICATOR="TRUE|FALSE"/>
        '      <ENTRY_NOTE>Notes (string)</ENTRY_NOTE>                                   'SI06472
        '      <ENTRY_NOTES_RTF>string</ENTRY_NOTES_RTF>
        '      <ENTRY_NOTES_HTML>string</ENTRY_NOTES_HTML>      'SI06267
        '      <PRIORITY>#</PRIORITY>
        '      <STATUS_OPEN INDICATOR="TRUE|FALSE"/>
        '      <AUTO_CONFIRM INDICATOR="TRUE|FALSE"/>
        '      <RESPONSE_DATE YEAR="####" MONTH="##" DAY="##"/>                          'SI06472
        '      <RESPONSE_TIME HOUR="##" MINUTE="##" SECOND="##" TIME_ZONE="EST"/>        'SI06472
        '      <RESPONSE>Response (string)</RESPONSE>                                    'SI06472
        '      <RESPONSE_RTF>string</RESPONSE_RTF>
        '      <RESPONSE_HTML>string</RESPONSE_HTML>            'SI06267
        '      <ASSIGNED_USER>username</ASSIGNED_USER>
        '      <ASSIGNING_USER>username</ASSIGNING_USER>
        '      <ASSIGNED_GROUP>Group Name (string)</ASSIGNED_GROUP>
        '      <IS_ATTACHED INDICATOR="TRUE|FALSE"/>
        '      <ATTACH_TABLE>TableID</ATTACH_TABLE>
        '      <ATTACH_RECORD_ID>##</ATTACH_RECORD_ID>
        '      <REGARDING>string</REGARDING>
        '      <CLAIMANT>#</CLAIMANT>
        '      <CLAIM_ID>Claim ID</CLAIM_ID>
        '      <EVENT_ID>Event ID</EVENT_ID>
        '      <ROUTABLE INDICATOR="TRUE|FALSE"/>
        '      <ROLLABLE INDICATOR="TRUE|FALSE"/>
        '      <ROUTED_TO_ID>user id</ROUTED_TO_ID>
        '      <ROUTED_FROM_ID>user id</ROUTED_FROM_ID>
        '      <AUTO_LATE_NOTIFY INDICATOR="TRUE|FALSE"/>
        '      <SERIES_ID>#</SERIES_ID>
        '      <OVERRIDE_USER="user id"/>
        '      <OVERRIDE_DATE YEAR="####" MONTH="##" DAY="##"/>
        '      <OVERRIDE_TIME HOUR="##" MINUTE="##" SECOND="##" TIME_ZONE="EST"/>
        '      <RESERVE_ID>#</RESERVE_ID>
        '      <PAY_POST_FLAG>#</PAY_POST_FLAG>
        '      <TE_TRACKED INDICATOR="TRUE|FALSE"/>
        '      <ACTIVITIES NEXTID="#" COUNT="#">
        '         <ACTIVITY ID="#">
        '            <PARENT_ENTRY_ID>#</PARENT_ENTRY_ID>
        '            <ACTIVITY_CODE CODE="...">...</ACTIVITY_CODE>
        '            <TEXT>(string)</TEXT>
        '         </ACTIVITY>
        '      </ACTIVITIES>
        '      <EXPENSES NEXTID="#" COUNT="#">
        '         <EXPENSE ID="#">
        '            <ENTRY_ID>#</ENTRY_ID>
        '            <LINE_NUMBER>#</LINE_NUMBER>
        '            <START_DATE YEAR="####" MONTH="##" DAY="##"/>
        '            <START_TIME HOUR="##" MINUTE="##" SECOND="##" TIME_ZONE="EST"/>
        '            <END_DATE YEAR="####" MONTH="##" DAY="##"/>
        '            <END_TIME Hour="##" MINUTE="##" SECOND="##" TIME_ZONE="EST"/>
        '            <DESCRIPTION CODE="...">...</DESCRIPTION>
        '            <UNIT CODE="...">...</UNIT>
        '            <REASON CODE="...">...</REASON>
        '            <STATUS CODE="...">...</STATUS>
        '            <DISAPPROVAL_REASON CODE="...">...</DISAPPROVAL_REASON>
        '            <APPROVED_BY>user id</APPROVED_BY>
        '            <APPROVED_DATE YEAR="####" MONTH="##" DAY="##"/>
        '            <TRANSACTION_TYPE CODE="...">...</TRANSACTION_TYPE>
        '            <UNITS>#</UNITS>
        '            <AMOUNT_PER_UNIT>#</AMOUNT_PER_UNIT>
        '            <AMOUNT_NET>#</AMOUNT_NET>
        '            <MANUAL_NET INDICATOR="TRUE|FALSE"/>
        '            <AMOUNT_APPROVED>#</AMOUNT_APPROVED>
        '            <AMOUNT_PAID>#</AMOUNT_PAID>
        '            <DELETED INDIACATOR="TRUE|FALSE"/>
        '            <BILLABLE INDICATOR="TRUE|FALSE"/>\
        '            <CRC>#</CRC>
        '         </EXPENSE>
        '      </EXPENSES>
        '      <COMMENT_REFERENCE ID="CMTxx"/>
        '        <!-- multiple entries -->
        '      <PARTY_INVOLVED ID="ENTxxx" CODE="Role">Role Description</PARTY_INVOLVED>
        '        <!--Muliple Party Involved nodes allowed -->
        '           <LOSS_DETAIL>...</LOSS_DETAIL>
        '   </DIARY>
        '</DIARIES>*/

        public const string sDiaNode = "DIARY";
        public const string sDiaCollection = "DIARIES";
        //Start SIW404
        public const string sDiaGroupStart = "GROUP_START";
        public const string sDiaGroupEnd = "GROUP_END";
        public const string sDiaNavigation = "NAVIGATION";
        //End SIW404
        public const string sDiaNextID = "NEXT_ID";
        public const string sDiaCount = "COUNT";
        public const string sDiaID = "ID";
        public const string sDiaIDPfx = "DIA";
        public const string sDiaActIDPfx = "ACT";
        public const string sDiaExpIDPfx = "EXP";
        public const string sDiaDateCreated = "DATE_CREATED";
        public const string sDiaTimeCreated = "TIME_CREATED";
        public const string sDiaDateAfter = "DATE_AFTER";       //SI06267
        //SI06472 public const string sDiaDateComp = "DATE_COMPLETE";
        //SI06472 public const string sDiaTimeComp = "TIME_COMPLETE";
        public const string sDiaDateDue = "DATE_DUE";           //SI06472
        public const string sDiaTimeDue = "TIME_DUE";           //SI06472
        public const string sDiaAutoID = "AUTO_ID";
        public const string sDiaDeleted = "DELETED";
        public const string sDiaVoid = "VOID";
        public const string sDiaEstTime = "ESTIMATED_TIME";
        public const string sDiaNotify = "NOTIFY_FLAG";
        public const string sDiaRoute = "ROUTE_FLAG";
        public const string sDiaEntry = "ENTRY";
        public const string sDiaEntryID = "ID";
        //SI06472 public const string sDiaEntryName = "NAME";
        public const string sDiaEntryNotes = "ENTRY_NOTES";     //SI06472
        public const string sDiaEntryNotesRTF = "ENTRY_NOTES_RTF";
        public const string sDiaEntryNotesHTML = "ENTRY_NOTES_HTML";        //SI06267
        public const string sDiaPriority = "PRIORITY";
        public const string sDiaStatus = "STATUS_OPEN";
        public const string sWPAStatus = "WPA_STATUS_CODE";      //SIW7038
        public const string sDiaDocumentNumber = "DOCUMENT_NUMBER";      //SIW7038
        public const string sDiaAutoConfirm = "AUTO_CONFIRM";
        public const string sDiaRespDate = "RESPONSE_DATE";     //SI06472
        public const string sDiaRespTime = "RESPONSE_TIME";     //SI06472
        public const string sDiaResp = "RESPONSE";
        public const string sDiaRespRTF = "RESPONSE_RTF";
        public const string sDiaRespHTML = "RESPONSE_HTML";     //SI06267
        public const string sDiaAssignedUser = "ASSIGNED_USER";
        public const string sDiaAssigningUser = "ASSIGNING_USER";
        public const string sDiaAssignedGroup = "ASSIGNED_GROUP";
        public const string sDiaIsAttached = "IS_ATTACHED";
        public const string sDiaAttachTable = "ATTACH_TABLE";
        public const string sDiaAttachRecord = "ATTACH_RECORD_ID";
        public const string sDiaRegarding = "REGARDING";
        public const string sDiaClaimant = "CLAIMANT";
        public const string sDiaClaimID = "CLAIM_ID";
        public const string sDiaEventID = "EVENT_ID";
        public const string sDiaRoutable = "ROUTABLE";
        public const string sDiaRollable = "ROLLABLE";
        public const string sDiaRoutedTo = "ROUTED_TO_ID";
        public const string sDiaRoutedFrom = "ROUTED_FROM_ID";
        public const string sDiaAutoLateNotify = "AUTO_LATE_NOTIFY";
        public const string sDiaSeriesID = "SERIES_ID";
        public const string sDiaReserveID = "RESERVE_ID";
        public const string sDiaPayPost = "PAY_POST_FLAG";
        public const string sDiaTETracked = "TE_TRACKED";
        public const string sDiaActivities = "ACTIVITIES";
        public const string sDiaActivity = "ACTIVITY";
        public const string sDiaActNextID = "NEXTID";
        public const string sDiaActCount = "COUNT";
        public const string sDiaActID = "ID";
        public const string sDiaActPEID = "PARENT_ENTRY_ID";
        public const string sDiaActCode = "ACTIVITY_CODE";
        public const string sDiaActEntry = "ENTRY";
        public const string sDiaExpenses = "EXPENSES";
        public const string sDiaExpense = "EXPENSE";
        public const string sDiaExpNextID = "NEXTID";
        public const string sDiaExpCount = "COUNT";
        public const string sDiaExpID = "ID";
        public const string sDiaExpEntryID = "ENTRY_ID";
        public const string sDiaExpLineNum = "LINE_NUMBER";
        public const string sDiaExpStartDate = "START_DATE";
        public const string sDiaExpStartTime = "START_TIME";
        public const string sDiaExpEndDate = "END_DATE";
        public const string sDiaExpEndTime = "END_TIME";
        public const string sDiaExpDesc = "DESCRIPTION";
        public const string sDiaExpUnit = "UNIT";
        public const string sDiaExpReason = "REASON";
        public const string sDiaExpStatus = "STATUS";
        public const string sDiaExpDisReason = "DISAPPROVAL_REASON";
        public const string sDiaExpApprovedBy = "APPROVED_BY";
        public const string sDiaExpApprovedDate = "APPROVED_DATE";
        public const string sDiaExpTransType = "TRANSACTION_TYPE";
        public const string sDiaExpUnits = "UNITS";
        public const string sDiaExpAmtPerUnit = "AMOUNT_PER_UNIT";
        public const string sDiaExpAmtNet = "AMOUNT_NET";
        public const string sDiaExpManNet = "MANUAL_NET";
        public const string sDiaExpAmtApp = "AMOUNT_APPROVED";
        public const string sDiaExpAmtPaid = "AMOUNT_PAID";
        public const string sDiaExpDeleted = "DELETED";
        public const string sDiaExpBill = "BILLABLE";
        public const string sDiaExpCRC = "CRC";
        public const string sDiaComment = "COMMENT_REFERENCE";
        public const string sDiaPartyInvolved = "PARTY_INVOLVED";
        public const string sDiaLoss = "LOSS_DETAIL";
        public const string sDiaExpPPUnspecified = "UNSPECIFIED";
        public const string sDiaExpPPPost = "POST";
        public const string sDiaExpPPPay = "PAY";
        public const int iDiaExpPPUnspecified = 0;
        public const int iDiaExpPPPost = 1;
        public const int iDiaExpPPPay = 2;
        //public const string sDiaTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*' <!-- Multiple Levels -->
        '  <DIARY_REFERENCE ID="DIAxxx">
        '     <!-- Multiple Occurrences -->*/

        public const string sDiaReference = "DIARY_REFERENCE";

        /*'  <!--Multiple levels -->
        '   <ADDRESS_REFERENCE ID="RELATIONID"
        '                       ADDRESS_ID="ARDid  References an address in the Addresses Collection {id}"
        '                      CODE="Type of Address Code (Billing Mailing, Home, etc.) {string} [ADDRESS_TYPE]">
        '                      PRIMARY="YES|NO"
        '      <TECH_KEY>Technical Key</TECH_KEY>
        '      <TYPE>Address Type Translated {string}</TYPE>
        '      <DOING_BUSINESS_AS>dbaname</DOING_BUSINESS_AS>
        '      <DATA_ITEM/>
        '   </ADDRESS_REFERENCE>
        '     <!-- Multiple Occurrences -->*/
        public const string sAddressReferenceNode = "ADDRESS_REFERENCE";
        public const string sARID = "ADDRESS_ID"; //SIW360        
        public const string sARIDPfx = "ADR";
        public const string sADREFID = "ID"; //SIW360        
        public const string sADREFIDPFX = "AR";//SIW360
        public const string sARPrimary = "PRIMARY";
        public const string sARType = "TYPE";
        public const string sARDBA = "DOING_BUSINESS_AS";
        public const string sAREffDate = "EFFECTIVE_DATE"; //SIW360
        public const string sARExpDate = "EXPIRATION_DATE";//SIW360
        public const string sARDataItem = "DATA_ITEM";

        /*'Address XML Node
        '<ADDRESSES NEXT_ID="0" COUNT="xx">
        '  <ADDRESS ID="ADR2">
        '     <ADDRESS_LINE>AddressLine</ADDRESS_LINE>
        '     <! supports multiple address lines !>
        '     <CITY>Dallas</CITY>
        '     <COUNTY CODE="code">CountName</COUNTY>
        '     <STATE CODE="TX">Texas                         </STATE>
        '     <COUNTRY CODE="code">Country Name</COUNTRY>
        '     <POSTAL_CODE>75252</POSTAL_CODE>
        '     <COMMENT_REFERENCE ID="CMTxx"/>
        '       <!-- multiple entries -->
        '     <EFFECTIVE_START_DATE YEAR="yyyy" MONTH="mm" DAY="dd"/>     'SI06249
        '     <EFFECTIVE_END_DATE YEAR="yyyy" MONTH="mm" DAY="dd"/>       'Si06249
        '   <DISPLAY_ADDRESS>Address to be displayed in the tree<DISPLAY_ADDRESS>         'SI06345 - Implemented in SI06333)
        '  </ADDRESS>
        '</ADDRESSES>*/
        public const string sAdrCollection = "ADDRESSES";
        public const string sAdrNextID = "NEXT_ID";
        public const string sAdrID = "ID";
        public const string sAdrCount = "COUNT";
        public const string sAdrNode = "ADDRESS";
        public const string sAdrLine = "ADDRESS_LINE";
        public const string sAdrCity = "CITY";
        public const string sAdrCounty = "COUNTY";
        public const string sAdrState = "STATE";
        public const string sAdrCountry = "COUNTRY";
        public const string sAdrPostCode = "POSTAL_CODE";
        public const string sAdrComment = "COMMENT_REFERENCE";
        public const string sAdrDisplAddr = "DISPLAY_ADDRESS";//(SI06345 - Implemented in SI06354)
        //public const string sAdrTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360
        public const string sAdrEffStartDt = "EFFECTIVE_START_DATE";  //SI06249
        public const string sAdrEffEndDt = "EFFECTIVE_END_DATE";      //SI06249

        /*Start SIW7182
        'Account XML Node
        '<ENTITY>
        '  <!-- Multiple Account Blocks Allowed-->
        '   <ACCOUNT ID='ACT123'>
        '       <TECH_KEY>AC Technical key for the element </TECH_KEY>
        '       <TREE_LABEL>component label</TREE_LABEL>
        '       <CHECK_BOOK_ID>ARV1</CHECK_BOOK_ID>                  //SIW7473
        '       <ACCOUNT_NAME>Carol de costa</ACCOUNT_NAME>
        '       <ACCOUNT_CATEGORY CODE="ACH">arch</ACCOUNT_CATEGORY>
        '       <ACCOUNT_TYPE CODE="CHK">checkings</ACCOUNT_TYPE>
        '       <BANK_NAME>martha international</BANK_NAME>
        '       <BANK_ADDRESS1>Dallas</BANK_ADDRESS1>
        '       <BANK_ADDRESS2>Texas</BANK_ADDRESS2>
        '       <ACCOUNT_NUMBER></ACCOUNT_NUMBER> 
        '       <ROUTING_NUMBER></ROUTING_NUMBER> 
        '       <IBAN></IBAN>
        '       <SORT_CODE></SORT_CODE>
        '       <SWIFT_CODE></SWIFT_CODE>
        '       <ADDITIONAL_DETAILS> turn of the indicator<ADDITIONAL_DETAILS>
        '       <CURRENCY_ID  CODE=""></CURRENCY_ID>
        '       <DATE_PRE_NOTE_SENT YEAR="2011" MONTH="03" DAY="07" />
        '       <EFFECTIVE_DATE YEAR="2011" MONTH="03" DAY="07" />
        '       <EXPIRATION_DATE YEAR="2011" MONTH="03" DAY="07" />
        '       <INACTIVATE_EFT INDICATOR="True|False"/>
        '       <FINANCIAL_INFO_STATUS CODE="CRT">created</FINANCIAL_INFO_STATUS>
        '       <DISALLOW_PAYMENT_REASON CODE="PRO">vendor</DISALLOW_PAYMENT_REASON>
        '   </ACCOUNT>
        '</ENTITY> */
        public const string sAccID = "ID";
        public const string sACCIDPfx = "ACC";
        public const string sAccNode = "ACCOUNT";
        public const string sChkBookId = "CHECK_BOOK_ID";              //SIW7473
        public const string sAccNm = "ACCOUNT_NAME";
        public const string sAccCat = "ACCOUNT_CATEGORY";
        public const string sAccType = "ACCOUNT_TYPE";
        public const string sBankName = "BANK_NAME";
        public const string sBankAddress1 = "BANK_ADDRESS1";
        public const string sBankAddress2 = "BANK_ADDRESS2";
        public const string sAccNumber = "ACCOUNT_NUMBER";
        public const string sRoutNumber = "ROUTING_NUMBER";
        public const string sIban = "IBAN";
        public const string sSortCd = "SORT_CODE";
        public const string sSwiftCd = "SWIFT_CODE";
        public const string sAdditionalDetail = "ADDITIONAL_DETAILS";
        public const string sCurrencyId = "CURRENCY_ID";
        public const string sDatePreNoteSnt = "DATE_PRE_NOTE_SENT";
        public const string sEffectiveDate = "EFFECTIVE_DATE";
        public const string sExpirationDate = "EXPIRATION_DATE";
        public const string sInactivateEft = "INACTIVATE_EFT";
        public const string sPrimaryAccount = "PRIMARY_ACCOUNT"; //SIW8315
        public const string sFinancialInfoStatus = "FINANCIAL_INFO_STATUS";
        public const string sDisallowPaymentRsn = "DISALLOW_PAYMENT_REASON";
        public const string sAccComment = "COMMENT_REFERENCE";      
        //End SIW7182
        /*' <!-- Multiple Levels -->
          '  <COMMENT_REFERENCE ID="CRxxx" COMMENT_ID="CMTxxx">       'SIW360
        '     <!-- Multiple Occurrences -->*/
        public const string sCmtReference = "COMMENT_REFERENCE";
        public const string sCmtRefID = "ID"; //SIW360
        public const string sCmtRefIDPFX = "CR"; //SIW360
        public const string sCmtIDRef = "COMMENT_ID"; //SIW360
        public const string sCmtEffDate = "EFFECTIVE_START_DATE";  //SIW360
        public const string sCmtExpDate = "EFFECTIVE_END_DATE";      //SIW360


        /*
        '   <COMMENT ID="CMTxxx" PARENT_ID="CMTxxxparentcomment" NEXT_ID="CMTxxxNextCommentInList" PREV_ID="CMTxxxPreviousCommentInList"> 'SIW367
        '      <TECH_KEY>AC Technical key for the element </TECH_KEY>  '(SI06023 - Implemented in SI06333) 'SIW360
        '      <PARENT_COMMENT_ID>Comment ID of parent when appending another comment</PARENT_COMMENT_ID> 'SIW367
        '      <CATEGORY TYPE="xxxxx">Category Description</CATEGORY>
        '      <SUBJECT>Comment Subject</SUBJECT>
        '      <DATE YEAR="2002" MONTH="03" DAY="01" />
        '      <TIME HOUR="17" MINUTE="35" SECOND="04" />
        '      <AUTHOR USER_ID="userid" ENTITY_ID="ENTxxx">                 //SI08479
        '          <NAME>authorfullname</NAME>                              //SI08479
        '          <FIRST_NAME>authorfullname</FULL_NAME>                   //SI08479        
        '          <LAST_NAME>authorfullname</LAST_NAME>                    //SI08479
        '      </AUTHOR>                                                    //SI08479
//SAI08479'      <AUTHOR USER_ID="userid" ENTITY_ID="ENTxxx"/>author name</AUTHOR>
        '      <TEXT>This is the text of the comment</TEXT>
        '      <TEXT_HTML>This is the text of the comment in HTML format</TEXT_HTML>   'SI06369        
        '      <PRIVATE_FLAG INDICATOR = "True|False"/>                     'SIW353
        '      <PUBLIC_FLAG INDICATOR = "True|False"/>                      'SIW353
        '      <PUBLICVIEWONLY_FLAG INDICATOR = "True|False"/>              'SIW353
        '      <UPDATE_ALLOWED INDICATOR = "True|False"/>                   'SIW353
        '      <APPEND_ALLOWED INDICATOR = "True|False"/>                   'SIW353
        '   </COMMENT>
         */

        public const string sCmtCollection = "COMMENTS";
        public const string sCmtCount = "COUNT";
        public const string sCmtNextID = "NEXT_ID";
        public const string sCmtNode = "COMMENT";
        public const string sCmtID = "ID";
        public const string sCmtIDPfx = "CMT";
        public const string sCmtParentID = "PARENT_COMMENT_ID";//SIW367
        public const string sCmtCategory = "CATEGORY";
        public const string sCmtSubject = "SUBJECT";
        public const string sCmtDate = "DATE";
        public const string sCmtTime = "TIME";
        public const string sCmtAuthor = "AUTHOR";
        public const string sCmtAuthorName = "NAME";                        //SI08479
        public const string sCmtAuthorFirstName = "FIRST_NAME";             //SI08479
        public const string sCmtAuthorLastName = "LAST_NAME";               //SI08479
        
        public const string sCmtDateLastUpdated = "DATELASTUPDATED";
        public const string sCmtTimeLastUpdated = "TIMELASTUPDATED";
        public const string sCmtUserID = "USER_ID"; 
        public const string sCmtAuthorID = "ENTITY_ID";
        public const string sCmtText = "TEXT";
        //public const string sCmtHTML = "COMMENT_HTML";  //SI06369
        public const string sCmtHTML = "TEXT_HTML";       //SI06369
        //public const string sCmtTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360
        public const string sCmtPrivateFlag = "PRIVATE_FLAG";//SIW353
        public const string sCmtPublicFlag = "PUBLIC_FLAG";//SIW353
        public const string sCmtPublicViewOnlyFlag = "PUBLICVIEWONLY_FLAG";//SIW353
        public const string sCmtUpdateAllowed = "UPDATE_ALLOWED";//SIW353
        public const string sCmtAppendOnlyAllowed = "APPEND_ALLOWED";//SIW353
        public const string sCmtInitialID = "INITIAL_ID";                     //SIW367
        public const string sCmtPrevID = "PREV_ID";                           //SIW367
        public const string sLITClaimantLawFirm = "CLAIMANT_LAW_FIRM";
        

        /*' <!-- Multiple Levels -->
        '  <DOCUMENT_REFERENCE ID="DOCxxx">
        '     <!-- Multiple Occurrences -->*/
        public const string sDocReference = "DOCUMENT_REFERENCE";

        /*'<DOCUMENTS COUNT="xx" NEXT_ID="xx">
        '   <FOLDER>Global folder name where the documents are</FOLDER>
        '   <DOCUMENT DOCUMENT_ID="DOCxxx">
        '     <FOLDER>Override Folder name for this document</FOLDER>
        '     <ARCHIVE_LVL>123</ARCHIVE_LVL>
        '     <CREATE_DATE YEAR="2002" MONTH="03" DAY="01" />
        '     <CREATE_TIME HOUR="17" MINUTE="35" SECOND="04" /> 'SIW261
        '     <CATEGORY _OPTIONAL_="" CODE="Category Code {string} [CATEGORY]">Category Name {String}</CATEGORY>
        '     <NAME>Friendly Document Name</NAME>
        '     <CLASS _OPTIONAL_="" CODE="Class Code {string} [CLASS]">Class Name {String}</CLASS>
        '     <SUBJECT>Title for the document</SUBJECT>
        '     <TYPE _OPTIONAL_="" CODE="Document Type Code {string} []">Document Type Name {String}</TYPE>
        '     <NOTES>Notes about the document</NOTES>
        '     <SECURITY_LVL>Level of security (Long)</SECURITY_LVL>
        '     <USER_ID>User ID {String}</USER_ID>
        '     <INTERNAL_TYPE>2</INTERNAL_TYPE>
        '     <CREATE_APP>Name of application that creates the attachement</CREATE_APP>
        '     <FILENAME>Name of the file to attach</FILENAME>
        '     <FILEPATH>Path to the file to attach</FILEPATH>
        '     <TABLENAME>Name of the table to which we attach the file</TABLENAME>
        '     <BASE64>The binary data for an attachment, converted to Base64 format</BASE64>    //SIW261
        '   </DOCUMENT>
        '</DOCUMENTS>*/
        public const string sDocCollection = "DOCUMENTS";
        public const string sDocCount = "COUNT";
        public const string sDocNextID = "NEXT_ID";
        public const string sDocNode = "DOCUMENT";
        public const string sDocID = "ID";
        public const string sDocIDPfx = "DOC";
        public const string sDocSubject = "SUBJECT";
        public const string sDocFileName = "FILENAME";
        public const string sDocTableName = "TABLENAME";
        public const string sDocFolder = "FOLDER";
        public const string sDocArchLvl = "ARCHIVE_LVL";
        public const string sDocCreateDate = "CREATE_DATE";
        public const string sDocCreateTime = "CREATE_TIME"; //SIW261
        public const string sDocCategory = "CATEGORY";
        public const string sDocName = "NAME";
        public const string sDocClass = "CLASS";
        public const string sDocType = "TYPE";
        public const string sDocNotes = "NOTES";
        public const string sDocSecLevel = "SECURITY_LVL";
        public const string sDocUserID = "USER_ID";
        public const string sDocInternalType = "INTERNAL_TYPE";
        public const string sDocCreateApp = "CREATE_APP";
        public const string sDocFilePath = "FILEPATH";
        public const string sDocClaimNumber = "CLAIM_NUMBER";
        public const string sDocEventNumber = "EVENT_NUMBER";
        //public const string sDocTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360
        public const string sDocBase64 = "BASE64";  //SIW261


        /*'<JURISDICTIONAL_DATA>
        '   <JURISDICTION CODE="MA">Massachusetts</JURISDICTION>
        '   <DATA_ITEM NAME="INDUSTRY_CODE" TYPE="CODE" CODE="xx">Description</DATA_ITEM>
        '        <!-- Multiple Data Items Allowed -->
        '   <PARTY_INVOLVED ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</PARTY_INVOLVED>
        '        <!--Multiple Parties Involved-->
        '   <COMMENT_REFERENCE ID="CMTxx"/>
        '        <!-- multiple entries -->
        '</JURISDICTIONAL_DATA>*/
        public const string sJurNode = "JURISDICTION_DATA";
        public const string sJurJurisdiction = "JURISDICTION";
        public const string sJurDataItem = "DATA_ITEM";
        public const string sJurDataName = "NAME";
        public const string sJurDataType = "TYPE";
        public const string sJurPartyInvolved = "PARTY_INVOLVED";
        public const string sJurCommentReference = "COMMENT_REFERENCE";
        //public const string sJurTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<ADDITIONAL_DATA>
        '   <!-These are supplemental fields defined within a group associated with a particular data element -- >
        '   <!-Additional data groups and items may be defined.  Appropriate fields must be defined within the ClaimsPro Database. -- >
        '   <DATA_GROUP
        '         ID="[Dgyyyy] document unique ID that corresponds to a data component within the body of this document"
        '         COMPONENT_NAME="Corresponds to a group data tag name in this document"
        '         SUPP_TABLE="Corresponds to a specific supplemental table in ClaimsPro">
        '      <DATA_ITEM
        '         NAME="Data Item Name"
        '         SUPP_FIELD="Corresponds to a supplemental table field within the table above"
        '         TYPE="[BOOLEAN,DATE,TIME,CODE,STRING,NUMBER] specifies how the XML processor is to parse the data item"
        '            [data attributes corresponding to data types above as follows:
        '              BOOLEAN:  INDICATOR="[YES|NO]{Boolean}"
        '              DATE:  YEAR="year{integer}" MONTH="month{integer}" DAY="day{integer}"
        '              TIME:  HOUR="hour{integer}" MINUTE="minute{integer}" SECOND="second{integer}" TIME_ZONE="time zone{string}"
        '              CODE:  CODE="shortcode"]>
        '            [Data corresponding to the data type as follows:
        '            STRING:  data value {string}
        '            NUMBER:  data value {decimal}
        '            Date:  formatted Date
        '            TIME:  formatted time
        '      </DATA_ITEM>
        '      <PARTY_INVOLVED ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</PARTY_INVOLVED>
        '          <!--Multiple Parties Involved-->
        '      <ADDRESS_REFERENCE ID="ADRxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</ADDRESS_REFERENCE>
        '          <!--Multiple Parties Involved-->
        '      <COMMENT_REFERENCE ID="CMTxx"/>
        '          <!-- multiple entries -->
        '   </DATA_GROUP>
        '   <!-- Multiple Data Groups Allows -->
        '</ADDITIONAL_DATA>*/

        public const string sAddNode = "ADDITIONAL_DATA";
        public const string sAddGroup = "DATA_GROUP";
        public const string sAddComponentName = "COMPONENT_NAME";
        public const string sAddSuppTable = "SUPP_TABLE";
        public const string sAddComponentID = "ID";
        public const string sAddDataItem = "DATA_ITEM";
        public const string sAddDataName = "NAME";
        public const string sAddDataValue = "VALUE";
        public const string sAddSuppField = "SUPP_FIELD";
        public const string sAddPartyInvolved = "PARTY_INVOLVED";
        public const string sAddAddressReference = "ADDRESS_REFERENCE";
        public const string sAddCommentReference = "COMMENT_REFERENCE";

        //SIW7882-Start
        public const string sAddCodeFileId = "CODE_TABLE_NAME";
        public const string sAddLocked = "LOCKED";
        public const string sAddDisabled = "DISABLED";
        public const string sAddRequired = "REQUIRED";
        public const string sAddLookUp = "LOOKUP";
        public const string sAddPattern = "PATTERN";
        public const string sAddFieldSize = "FIELD_SIZE";
        public const string sAddFieldType = "FIELD_TYPE";
        

        //SIW7882-End

        //public const string sAddTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*' ' <PARENT_NODE>
            '   <LIMIT ID="LMTxxx">
            '     <TECH_KEY>AC Technical key for the element </TECH_KEY>
            '     <TYPE CODE="LIM">CodeTable = POL_LIM_DED_TYPE</TYPE>
            '     <SPECIFIED CODE="OCCUR">CodeTable = POLICY_APP_TYPE</SPECIFIED>
            '     <COVERAGE CODE="">codetable = COVERAGE_CODE</COVERAGE>
            '     <AMOUNT>2135</AMOUNT>
            '     <USED_AMOUNT>...</USED_AMOUNT>
            '     <PART_OF>...</PART_OF>
            '     <AGGREGATE>...</AGGREGATE>
            '     <DESCRIPTION>text</DESCRIPTION>
            '     <EFFECTIVE_DATE YEAR="year" MONTH="month" DAY="day"/>
            '     <EXPIRATION_DATE YEAR="year" MONTH="month" DAY="day"/>
            '   </LIMIT>
            '   <!-- Multiple Limits -->
' </PARENT_NODE>*/

        public const string sLmtNode = "LIMIT";
        public const string sLmtSpecified = "SPECIFIED";
        public const string sLmtEffDate = "EFFECTIVE_DATE";
        public const string sLmtExpDate = "EXPIRATION_DATE";

        //Start SIW7123
        public const string sLmtID = "ID";
        public const string sLmtIDPFX = "LMT";   
        public const string sLmtType = "TYPE";
        public const string sLmtAmt = "AMOUNT";
        public const string sLmtUsedAmount = "USED_AMOUNT";
        public const string sLmtCoverage = "COVERAGE";
        public const string sLmtPartOf = "PART_OF";
        public const string sLmtAggregate = "AGGREGATE";
        public const string sLmtDescription = "DESCRIPTION";
        //End SIW7123
        /*' <PARENT_NODE>
            '   <DEDUCTIBLE  ID="DEDxxx">
            '     <TECH_KEY>AC Technical key for the element </TECH_KEY>
            '     <TYPE CODE="DED">CodeTable=POL_LIM_DED_TYPE</TYPE>
            '     <SPECIFIED CODE="OCCUR">CodeTable = POLICY_APP_TYPE</SPECIFIED>
            '     <COVERAGE CODE="">codetable = COVERAGE_CODE</COVERAGE>
            '     <AMOUNT>2135</AMOUNT>
            '     <USED_AMOUNT>...</USED_AMOUNT>
            '     <PART_OF>...</PART_OF>
            '     <AGGREGATE>...</AGGREGATE>
            '     <DESCRIPTION>text</DESCRIPTION>
            '     <EFFECTIVE_DATE YEAR="year" MONTH="month" DAY="day"/>
            '     <EXPIRATION_DATE YEAR="year" MONTH="month" DAY="day"/>
            '   </DEDUCTIBLE>
            '   <!-- Multiple Deductible Lines -->
        ' </PARENT_NODE>*/

        public const string sDedNode = "DEDUCTIBLE";
        public const string sDedSpecified = "SPECIFIED";
        public const string sDedEffDate = "EFFECTIVE_DATE";
        public const string sDedExpDate = "EXPIRATION_DATE";

        //Start SIW7123
        public const string sDedID = "ID";
        public const string sDedIDPFX = "DED";
        public const string sDedType = "TYPE";
        public const string sDedAmt = "AMOUNT";
        public const string sDedUsedAmount = "USED_AMOUNT";
        public const string sDedCoverage = "COVERAGE";
        public const string sDedPartOf = "PART_OF";
        public const string sDedAggregate = "AGGREGATE";
        public const string sDedDescription = "DESCRIPTION";
        //End SIW7123
        //public const string sDedTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<CLAIM>
        '   <INVOICES INVOICE_COUNT="invoicecount">
        '      <TOTAL_BILLED>Billed Amount</TOTAL_BILLED>
        '      <TOTAL_REDUCED>Total reductions</TOTAL_REDUCED>
        '      <TOTAL_ALLOWED>Total Allowed</TOTAL_ALLOWED>
        '      <TOTAL_FEES>total fees</TOTAL_FEES>
        '   </INVOICES>
        '   <INVOICE ID="INVxx" LINE_COUNT="line count">
        '      <BILLED>Billed Amount</BILLED>
        '      <REDUCED>Total invoice reduction</REDUCED>
        '      <ALLOWED>Total invoice allowed</ALLOWED>
        '      <FEES>fees</FEES>
        '      <NUMBER>BER.6</NUMBER>
        '      <PO_NUMBER>NA</PO_NUMBER>
        '      <REFERENCE>BER.5</REFERENCE>
        '      <INVOICE_DATE YEAR="" MONTH="" DAY="">BER.53</DATE>
        '      <DUE_DATE YEAR="" MONTH="" DAY="">BER.58</DUE_DATE>
        '      <REVIEW_DATE YEAR="" MONTH="" DAY="">BER.56</REVIEW_DATE>
        '      <DATE_RECEIVED YEAR="year" MONTH="month" DAY="day">BER.53</DATE_RECEIVED>
        '      <FROM_DATE YEAR="" MONTH="" DAY="">
        '      <TO_DATE YEAR="" MONTH="" DAY="">
        '      <RELATED_DOCUMENTATION>
        '          <TYPE CODE="">documenttype</TYPE>
        '          <FILENAME>filename</FILENAME>
        '          <DESCRIPTION>description</DESCRIPTION>
        '      </RELATED_DOCUMENTATION>
        '          <! -- Multiple entries allowed -->
        '      <PAYMENT_ASSIGNMENT PAY_CODE="BER.59,61 : P=Pay, H=Hold, D=Deny, R=Review">
        '          <TYPE_OF_PAYMENT CODE="TOPCode:  Expense, Medical, etc.  Tied to receiving
        '            System ">Type of Payment</TYPE_OF_PAYMENT>"
        '          <RESERVE CODE="ReserveCode:  Defined in Setup.  Need specfic one.">Reserve to
        '            be Assigned</RESERVE>
        '          <PAY_TRANSACTION_TYPE CODE="transtypecode">transactiontype</PAY_TRANSACTION_TYPE>
        '      </PAYMENT_ASSIGNMENT>
        '      <PARTY_INVOLVED ID="EntityID: References Entity Below" CODE="Relationship Code">relationship</PARTY_INVOLVED>
        '      <ADDRESS_REFERENCE ID="addressID" CODE="Relationship Code">Type of</ADDRESS>
        '      <COMMENT_REFERENCE> ... </COMMENT>
        '      <FEE>
        '         <PAYMENT_ASSIGNMENT PAY_CODE=" P=Pay, H=Hold, D=Deny, R=Review">
        '            <TYPE_OF_PAYMENT CODE="TOPCode:  Expense, Medical, etc.  Tied to
        '             receiving system">Type of Payment</TYPE_OF_PAYMENT>
        '            <RESERVE CODE="ReserveCode:  Defined in Setup.  Need specific
        '             one.">Reserve to be Assigned</RESERVE>
        '         </PAYMENT_ASSIGNMENT>
        '         <PAYEE ID="ENTid:  Identify Payee for the fee.  References an Entity
        '          below" CODE="BER.14, 20, 25, 30, 35:  Payee Code">Type of Payee / Fee
        '          (RVWR, PPN, PPAY, HOSPRVW, UR)</PAYEE>
        '         <AMOUNT>BER.16, 22, 27, 32, 37    : Fee</AMOUNT>
        '         <CONTROL_NUMBER>BER.17, 23, 28, 33, 38</CONTROL_NUMBER>
        '      </FEE>
        '      <DIAGNOSIS>
        '         <SPECIFIC CODE="ICD-9 Code {string} [ICD9]">Diagnosis Translated {string}</SPECIFIC>
        '         <DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      </DIAGNOSIS>
        '          <!-- Multiple Diagonsis Codes Allowed -->
        '      <TREATMENT>
        '         <SPECIFIC CODE="Standard CPT Treatment Code {string} [TREATMENT_CODE]" TYPE="Code Type (CPT,HCFA, etc.) {string} [CPT,HCFA]">description</SPECIFIC>
        '         <STARTED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '         <ENDED YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '         <TREATED_BY ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</TREATED_BY>
        '      </TREATMENT>
        '        <!--Multiple treatments allowed-->
        '      <EOB CODE="BER.62,63 remark code">
        '          <SHORT_REMARK>RMCR.5</SHORT_REMARK>
        '          <LONG_REMARK>RMCR.6 + RMCR.7</LONG_REMARK>
        '      </EOB>
        '      <STATE_ADJUDICATED>
        '          <STATE CODE="">statename</STATE>
        '          <DISCLAIMER>SD.4 : All Sequences Combined for Single Disclaimer Line</DISCLAIMER>
        '      </STATE_ADJUDICATED>
        '      <PROCESS_ERROR CODE="Error Code">Error message</PROCESS_ERROR>
        '         <!-- Multiple lines allowed -->
        '      <DETAILS>
        '        <DETAIL>
        '           <PLACE_OF_SVC CODE="LIR.2">Translated</PLACE_OF_SVC>
        '           <TYPE_OF_SVC CODE="LIR.7">Translated</TYPE_OF_SVC>
        '           <SVC_START_DATE YEAR="year" MONTH="month" DAY="day">LIR.4</START_DATE>
        '           <SVC_END_DATE YEAR="year" MONTH="month" DAY="day">LIR.5</END_DATE>
        '           <UNITS TYPE="Type of Unit (Defaults to Each)">unitsbilled</UNITS>
        '           <BILLED>LIR.15 billed amount</BILLED>
        '           <DISALLOWED>LIR.16 disallowed by review</DISALLOWED>
        '           <HOSP_DISALLOWED>LIR.17 hospital disallowed</HOSP_DISALLOWED>
        '           <UR_DISALLOWED>LIR.18 UR disallowed</UR_DISALLOWED>
        '           <DISCOUNT>LIR.19 Negotiated Discount</DISCOUNT>
        '           <PPNREDUCED>LIR.20 PPN Reduction</PPNREDUCED>
        '           <PAYED>LIR.21 (Previously Paid Amounts)</PAYED>
        '           <ALLOWED>allowed amount</ALLOWED>
        '           <SAVED>saved amount</SAVED>
        '           <CONTRACT>
        '              <NUMBER>contractnumber</NUMBER>
        '              <DISCOUNT TYPE="discounttype">discountamount</DISCOUNT>
        '           </CONTRACT>
        '           <EOB CODE="LIR.22,23,24 eobcode">
        '              <!--Comments retrieved from RMCR record where PMCR.2="L" and PMCR.4 = CODE-->
        '              <!--Multiple Remarks allowed-->
        '              <SHORT_REMARK>RMCR.5</SHORT_REMARK>
        '              <LONG_REMARK>RMCR.6 + RMCR.7</LONG_REMARK>
        '           </EOB>
        '           <PROCEDURE>
        '              <USE>use</USE>
        '              <STATE CODE="statecode">state</STATE>
        '              <SPECIFIED CODE="ccc">PCR.5</SPECIFIED>
        '              <TYPE CODE="code">type</TYPE>                             
        '              <TOOTH>Number</TOOTH>
        '           </PROCEDURE>
        '           <DIAGNOIS>number 1 through 5 as sequenced above</DIAGNOIS>
        '        </DETAIL>
        '      </DETAILS>
        '   </INVOICE>
        '</CLAIM>*/

        public const string sInvoices = "INVOICE_SUMMARY";
        public const string sInvCount = "COUNT";
        public const string sInvTotalBilled = "TOTAL_BILLED";
        public const string sInvTotalReduced = "TOTAL_REDUCED";
        public const string sInvTotalAllowed = "TOTAL_ALLOWED";
        public const string sInvTotalFees = "TOTAL_FEES";
        public const string sInvoiceNode = "INVOICE";
        public const string sInvoiceID = "ID";
        public const string sInvoiceIDPFX = "INV";
        public const string sInvLineCount = "LINE_COUNT";
        public const string sInvBilled = "BILLED";
        public const string sInvReduced = "REDUCED";
        public const string sInvAllowed = "ALLOWED";
        public const string sInvFees = "FEES";
        public const string sInvNumber = "NUMBER";
        public const string sInvPONumber = "PO_NUMBER";
        public const string sInvReference = "REFERENCE";
        public const string sInvDate = "INVOICE_DATE";
        public const string sInvDueDate = "DUE_DATE";
        public const string sInvReviewDate = "REVIEW_DATE";
        public const string sInvReceivedDate = "DATE_RECEIVED";
        public const string sInvFromDate = "FROM_DATE";
        public const string sInvToDate = "TO_DATE";
        public const string sInvRltdDocument = "RELATED_DOCUMENTATION";
        public const string sInvRltdDocType = "TYPE";
        public const string sInvRltdDocFileName = "FILENAME";
        public const string sInvRltdDocDescription = "DESCRIPTION";
        public const string sInvPayAssignment = "PAYMENT_ASSIGNMENT";
        public const string sInvPayAssignmentCode = "PAY_CODE";
        public const string sInvPayAssignmentTypeofPayment = "TYPE_OF_PAYMENT";
        public const string sInvPayAssignmentTransactionType = "PAY_TRANSACTION_TYPE";
        public const string sInvPayAssignmentReserve = "RESERVE";
        public const string sInvPartyInvolved = "PARTY_INVOLVED";
        public const string sInvAddressRef = "ADDRESS_REFERENCE";
        public const string sInvCommentRef = "COMMENT_REFERENCE";
        public const string sInvFee = "FEE";
        public const string sInvFeePayee = "PAYEE";
        public const string sInvFeeAmount = "AMOUNT";
        public const string sInvFeeControlNumber = "CONTROL_NUMBER";
        public const string sInvDiagnosis = "DIAGNOSIS";
        public const string sInvDiagSpecific = "SPECIFIC";
        public const string sInvDiagDate = "DATE";
        public const string sInvEOB = "EOB";
        public const string sInvEOBShortRemark = "SHORT_REMARK";
        public const string sInvEOBLongRemark = "LONG_REMARK";
        public const string sInvStateAdjudicated = "STATE_ADJUDICATED";
        public const string sInvStateAdjudicatedState = "STATE";
        public const string sInvStateAdjudicatedDisclaimer = "DISCLAIMER";
        public const string sInvProcessError = "PROCESS_ERROR";
        public const string sInvDetails = "DETAILS";
        public const string sInvDetail = "DETAIL";
        public const string sInvDetailPlaceOfSvc = "PLACE_OF_SVC";
        public const string sInvDetailTypeOfSvc = "TYPE_OF_SVC";
        public const string sInvDetailSvcStartDate = "SVC_START_DATE";
        public const string sInvDetailSvcEndDate = "SVC_END_DATE";
        public const string sInvDetailUnits = "UNITS";
        public const string sInvDetailBilled = "BILLED";
        public const string sInvDetailDisallowed = "DISALLOWED";
        public const string sInvDetailHospDisallowed = "HOSP_DISALLOWED";
        public const string sInvDetailURDisallowed = "UR_DISALLOWED";
        public const string sInvDetailDiscount = "DISCOUNT";
        public const string sInvDetailPPNReduced = "PPNREDUCED";
        public const string sInvDetailPayed = "PAYED";
        public const string sInvDetailAllowed = "ALLOWED";
        public const string sInvDetailSaved = "SAVED";
        public const string sInvDetailContract = "CONTRACT";
        public const string sInvDetailContractNumber = "NUMBER";
        public const string sInvDetailContractDiscType = "DISCOUNT";
        public const string sInvDetailProcedure = "PROCEDURE";
        public const string sInvDetailProcedureSpecified = "SPECIFIED";
        public const string sInvDetailProcedureType = "TYPE";
        public const string sInvDetailProcedureState = "STATE";
        public const string sInvDetailProcedureUse = "USE";
        public const string sInvDetailProcedureTooth = "TOOTH";
        public const string sInvDetailProcedureModifier = "MODIFIER"; //SIN7096
        public const string sInvDetailDiagnosis = "DIAGNOSIS";
        //public const string sInvTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<PARENT_NODE>
        '   <DATA_ITEM
        '      Name = "Data Item Name"
        '      TYPE="[BOOLEAN,DATE,TIME,CODE,STRING,NUMBER] specifies how the XML processor is to parse the data item"
        '         [data attributes corresponding to data types above as follows:
        '           BOOLEAN:  INDICATOR="[YES|NO]{Boolean}"
        '           DATE:  YEAR="year{integer}" MONTH="month{integer}" DAY="day{integer}"
        '           TIME:  HOUR="hour{integer}" MINUTE="minute{integer}" SECOND="second{integer}" TIME_ZONE="time zone{string}"
        '           CODE:  CODE="shortcode"]>
        '         [Data corresponding to the data type as follows:
        '         STRING:  data value {string}
        '         NUMBER:  data value {decimal}
        '         DATE:  formatted Date
        '         TIME:  formatted TIME
        '   </DATA_ITEM>
        '</PARENT_NODE>*/

        public const string sVarDataItem = "DATA_ITEM";
        public const string sVarDataName = "NAME";

        /*'<CLAIM>
        '   <!-- Multiple Loss Details Allowed -->
        '   <LOSS_DETAIL ID="LDxxx" RESERVE_CLAIMANT_SEQ="1" OFFSET_ONSET="OFFSET">
        '     <LOSS_CLAIMANT CLAIMANT_NUMBER="1" ID="ENT249" />
        '     <RESERVE_TYPE CODE="MED">Medical</RESERVE_TYPE>
        '     <LOSS_MEMBER CODE="**" />
        '     <LOSS_DISABILITY CODE="DM" BUREAU_CODE="xx">Defense Medical Evaluation</LOSS_DISABILITY>
        '     <PERCENT_OF_DISABILITY>percent</PERCENT_OF_DISABILITY> 'SIW452
        '     <NUMBER_OF_WEEK>Number of Week</NUMBER_OF_WEEK>        'SIW452
        '     <RESERVE_CATEGORY CODE="**" />
        '     <CAUSE_OF_LOSS CODE="07" BUREAU_CODE="xx">WM</CAUSE_OF_LOSS>
        '     <CURRENT_STATUS CODE="O">Open</CURRENT_STATUS>                   
        '     <BALANCE>balanceamount</BALANCE>                                 
        '     <INCURRED>incurred</INCURRED>                                    
        '     <TOTAL_PAID>paidtotal</TOTAL_PAID>                               
        '     <TOTAL_COLLECTION>collectionTotal</TOTAL_COLLECTION>             
        '     <FINANCIAL_ACTIVITY ISFROZEN='True|False' ISONHOLD="True|False" ISPAYMENT='True|False' ISSCHEDULE='True|False' VIEWPAYMENT='True|False'/> 'SIW7036 'SIW7647
        '     <POLICY_COVERAGE POLICY_ID="POL143" LEGACY_COVERAGE_KEY="00001,1,2">
        '        <COV_EFF_DATE YEAR="2001" MONTH="12" DAY="01" />
        '        <INSURANCE_LINE CODE="WC">Workers Compensation</INSURANCE_LINE>
        '        <LOCATION>1</LOCATION>
        '        <SUB_LOCATION>0</SUB_LOCATION>
        '        <RISK_UNIT_GROUP />
        '        <RISK_UNIT>Workers Compensation - Voluntary</RISK_UNIT>
        '        <PERIL_OF_COVERAGE CODE="WC">Workers Compensation</PERIL_OF_COVERAGE>
        '        <COVERAGE_SEQUENCE>2</COVERAGE_SEQUENCE>
        '     </POLICY_COVERAGE>
        '     <FINANCIAL_TRANSACTION>
        '        <ACTIVITY_SEQUENCE>613</ACTIVITY_SEQUENCE>
        '        <ACTIVITY_TYPE CODE="PO">Offset No Reserve Adjustment</ACTIVITY_TYPE>
        '        <ADDED_BY_USER>ps</ADDED_BY_USER>
        '        <DATE_OF_TRANSACTION YEAR="2002" MONTH="03" DAY="20" />
        '        <TIME_OF_TRANSACTION HOUR="17" MINUTE="35" SECOND="04" />
        '        <TRANSACTION_TYPE CODE="**" />
        '        <RESERVE_STATUS CODE="O">Open</RESERVE_STATUS>
        '        <PAYMENT_TRANSACTION>
        '           <CONTROL_NUMBER>control number</CONTROL_NUMBER>
        '           <PAYMENT_AMOUNT>100</PAYMENT_AMOUNT>
        '           <VOUCHER_REFERENCE ID="VCHxxx"/>
        '           <REPORTABLE CODE="**" />
        '           <PAYMENT_OFFSET INDICATOR="Yes" />
        '           <EXPENSE_TYPE CODE="xx">expense type</EXPENSE_TYPE>
        '           <INVOICE_NUMBER>invoice number</INVOICE_NUMBER>
        '           <INVOICE_AMOUNT>0</INVOICE_AMOUNT>
        '           <INVOICE_DATE YEAR="2002" MONTH="03" DAY="20" />
        '           <INVOICED_BY>invoiced by</INVOICED_BY>
        '           <PO_NUMBER>PO Number</PO_NUMBER>
        '           <SERVICE_FROM_DATE YEAR="2002" MONTH="03" DAY="20" />
        '           <SERVICE_TO_DATE YEAR="2002" MONTH="03" DAY="20" />
        '        </PAYMENT_TRANSACTION>
        '        <RESERVE_TRANSACTION>
        '           <BALANCE_AMOUNT>0</BALANCE_AMOUNT>
        '           <CHANGE_AMOUNT>0</CHANGE_AMOUNT>
        '           <ENTRY_OPERATOR>0</ENTRY_OPERATOR>
        '           <REASON_FOR_CHANGE CODE="xx">Change Reason</REASON_FOR_CHANGE>
        '        </RESERVE_TRANSACTION>
        '      <!-- Payment or Reserve, not both-->
        '     </FINANCIAL_TRANSACTION>
        '     <PARTY_INVOLVED ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">Relationship Translated {string}</PARTY_INVOLVED>
        '         <!--Multiple Parties Involved-->
        '     <COMMENT_REFERENCE ID="CMTxx"/>
        '         <!-- multiple entries -->
        '   </LOSS_DETAIL>
        '</CLAIM>*/

        public const string sLDNode = "LOSS_DETAIL";
        public const string sLDID = "ID";
        public const string sLDIDPfx = "LD";
        public const string sLDRsvClmntSeq = "RESERVE_CLAIMANT_SEQ";
        public const string sLDOffOnSet = "OFFSET_ONSET";
        public const string sLDOffSet = "OFFSET";
        public const string sLDOnSet = "ONSET";
        public const int iLDOffSet = 1;
        public const int iLDOnSet = 2;
        public const string sLDClaimant = "LOSS_CLAIMANT";
        public const string sLDClaimantNumber = "CLAIMANT_NUMBER";
        public const string sLDEntityID = "ID";
        public const string sLDRsvType = "RESERVE_TYPE";
        public const string sLDLossMember = "LOSS_MEMBER";
        public const string sLDBureauCode = "BUREAU_CODE";
        public const string sLDLossDisability = "LOSS_DISABILITY";
        public const string sLDPercentDisability = "PERCENT_OF_DISABILITY";      //SIW452
        public const string sLDNumberOfWeek = "NUMBER_OF_WEEK";                  //SIW452
        public const string sLDRsvCategory = "RESERVE_CATEGORY";
        public const string sLDCauseOfLoss = "CAUSE_OF_LOSS";
        public const string sLDCurrentStatus = "CURRENT_STATUS";
        public const string sLDBalance = "BALANCE";
        public const string sLDIncurred = "INCURRED";
        public const string sLDTotalPaid = "TOTAL_PAID";
        public const string sLDTotalCollection = "TOTAL_COLLECTION";
        public const string sLDPolCoverageNode = "POLICY_COVERAGE";
        public const string sLDPolCovPolicyID = "POLICY_ID";
        public const string sLDPolCovPolIDPfx = "POL";
        public const string sLDPolCovLegacyCovKey = "LEGACY_COVERAGE_KEY";        
        public const string sLDPolCovEffDate = "COV_EFF_DATE";
        public const string sLDPolCovInsLine = "INSURANCE_LINE";
        public const string sLDPolCovLocationNbr = "LOCATION";
        public const string sLDPolCovSubLocationNbr = "SUB_LOCATION";
        public const string sLDPolCovRiskUnitGrp = "RISK_UNIT_GROUP";
        public const string sLDPolCovRiskUnit = "RISK_UNIT";
        public const string sLDPolCovPeril = "PERIL_OF_COVERAGE";
        public const string sLDPolCovSeq = "COVERAGE_SEQUENCE";
        public const string sLDFTNode = "FINANCIAL_TRANSACTION";
        public const string sLDFTActSequence = "ACTIVITY_SEQUENCE";
        public const string sLDFTActType = "ACTIVITY_TYPE";
        public const string sLDFTAddedByUser = "ADDED_BY_USER";
        public const string sLDFTTransDate = "DATE_OF_TRANSACTION";
        public const string sLDFTTransTime = "TIME_OF_TRANSACTION";
        public const string sLDFTTransType = "TRANSACTION_TYPE";
        public const string sLDFTRsvStatus = "RESERVE_STATUS";
        public const string sLDPTNode = "PAYMENT_TRANSACTION";
        public const string sLDPTControlNumber = "CONTROL_NUMBER";
        public const string sLDPTAmount = "PAYMENT_AMOUNT";
        public const string sLDPTIsFinal = "ISFINAL";    //SIW7509
        public const string sLDPTIsFirstFinal = "ISFIRSTFINAL";    //SIW7537
        public const string sLDPTIsSupplemental = "ISSUPPLEMENTAL";    //SIW7537
        public const string sLDPTInvAmount = "INVOICE_AMOUNT";
        public const string sLDPTInvNbr = "INVOICE_NUMBER";
        public const string sLDPTInvBy = "INVOICED_BY";
        public const string sLDPTInvDate = "INVOICE_DATE";
        public const string sLDPTPONbr = "PO_NUMBER";
        public const string sLDPTSvcFromDate = "SERVICE_FROM_DATE";
        public const string sLDPTSvcToDate = "SERVICE_TO_DATE";
        public const string sLDPTReportableType = "REPORTABLE";
        public const string sLDPTPaymentOffsetInd = "PAYMENT_OFFSET";
        public const string sLDPTExpType = "EXPENSE_TYPE";
        public const string sLDPTVoucherReference = "VOUCHER_REFERENCE";
        public const string sLDPTVoucherRefID = "ID";
        public const string sLDPTMailToEntityID = "ENTITY_ID";
        public const string sLDRTNode = "RESERVE_TRANSACTION";
        public const string sLDRTBalance = "BALANCE_AMOUNT";
        public const string sLDRTChange = "CHANGE_AMOUNT";
        public const string sLDRTAmount = "RESERVE_AMOUNT"; //SIN8386
        public const string sLDRTEntryOperator = "ENTRY_OPERATOR";
        public const string sLDRTChangeReason = "REASON_FOR_CHANGE";
        public const string sLDPartyInvolved = "PARTY_INVOLVED";
        public const string sLDCommentRef = "COMMENT_REFERENCE";
        //public const string sLDTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<VOUCHERS NEXT_ID="0" COUNT=count>
        '   <VOUCHER ID="VCHxxx" MANUAL_IND="TRUE" TYPE="[CHECK|DRAFT|RECEIPT]>
        '      <VOUCHER_AMOUNT>100</VOUCHER_AMOUNT>
        '      <CONTROL_NUMBER>0000311</CONTROL_NUMBER>
        '      <ORIGINATING_BANK ID="ENTxx" CODE="banknumber">
        '      <ACCOUNT_NAME>AccountName<ACCOUNT_NAME>   'SIW7928
        '         <ROUTING_NUMBER>Routing</ROUTING_NUMBER>
        '         <ACCOUNT_NUMBER>AccountNumber</ACCOUNT_NUMBER>
        '         <FRACTIONAL_NUMBER>fractionalnumber</FRACTIONAL_NUMBER>
        '      </ORIGINATING_BANK>
        '      <TARGET_BANK ID="ENTxx" CODE="banknumber">
        '       <ACCOUNT_NAME>AccountName<ACCOUNT_NAME>   'SIW7928
        '         <ROUTING_NUMBER>Routing</ROUTING_NUAMBER>
        '         <ACCOUNT_NUMBER>AccountNumber</ACCOUNT_NUMBER>
        '         <FRACTIONAL_NUMBER>fractionalnumber</FRACTIONAL_NUMBER>
        '      </TARGET_BANK>
        '      <DOCUMENT_NUMBER>5455</DOCUMENT_NUMBER>
        '      <VOUCHER_DATE YEAR="2002" MONTH="03" DAY="07" />
        '      <RECEIPT_DATE YEAR="2002" MONTH="03" DAY="07" />
        '      <POST_DATE YEAR="2002" MONTH="03" DAY="07" />
        '      <PRINT_DATE YEAR="2002" MONTH="03" DAY="07" />
        '      <HONORED_DATE YEAR="2002" MONTH="03" DAY="07" />               
        '      <VOID_DATE YEAR="2002" MONTH="03" DAY="07" />                  
        '      <VOUCHER_BATCH_NUMBER>0</VOUCHER_BATCH_NUMBER>
        '      <VOUCHER_STATUS CODE="O">Open</VOUCHER_STATUS>
        '      <MEMO>memo phrase</MEMO>
        '      <PAYEE_VERBIAGE MANUAL_IND="YES|NO">payee verbiage block</PAYEE_VERBIAGE>  
        '      <PREPARED_BY_USER>userid</PREPARED_BY_USER>
' Begin SI6842
'      <COMB_PAY CODE="CBP">Combined Payment>
'         <TECH_KEY>COMB_PAY_ENTITY_ID</TECH_KEY>
'      </COMB_PAY>
' End SI6842
        '      <!Multiple Invoices Allowed>
        '      <INVOICE>
        '         <NUMBER>invoice number</NUMBER>
        '         <DATE YEAR="2002" MONTH="03" DAY="07" />
        '         <INVOICED_BY>invoiced by</INVOICED_BY>
        '         <PO_NUMBER>PO Number</PO_NUMBER>
        '         <SERVICE_FROM_DATE YEAR="2002" MONTH="03" DAY="20" />
        '         <SERVICE_TO_DATE YEAR="2002" MONTH="03" DAY="20" />
        '         <AMOUNT>0</INVOICE_AMOUNT>
        '      </INVOICE>
        '      <!Multiple Parties Allowed>
        '      <PARTY_INVOLVED .... payees, banks, others>
        '         <TYPE>Payee</TYPE>
        '         <DATA_ITEM NAME="PayeePhrase" TYPE="String">payee phrase</DATA_ITEM>
        '      </PARTY_INVOLVED>
        '      <ADDRESS_REFERENCE ID="addressID" CODE="Relationship Code">Type of</ADDRESS>
        '      <COMMENT_REFERENCE> ... </COMMENT>
        '   </VOUCHER>
        '<VOUCHERS>*/

        public const string sVCHGroup = "VOUCHERS";
        public const string sVCHNextID = "NEXT_ID";
        public const string sVCHCount = "COUNT";
        public const string sVCHVoucher = "VOUCHER";
        public const string sVCHID = "ID";
        public const string sVCHIDPfx = "VCH";
        public const string sVCHType = "TYPE";
        public const string sVCHManualInd = "MANUAL_IND";
        public const string sVCHAmount = "VOUCHER_AMOUNT";
        public const string sVCHControlNumber = "CONTROL_NUMBER";
        public const string sVCHTargetBank = "TARGET_BANK";
        public const string sVCHOriginatingBank = "ORIGINATING_BANK";
        public const string sVCHBankNumber = "CODE";
        public const string sVCHAccountName = "ACCOUNT_NAME"; //SIW7928
        public const string sVCHRoutingNumber = "ROUTING_NUMBER";
        public const string sVCHAccountNumber = "ACCOUNT_NUMBER";
        public const string sVCHFractionalNumber = "FRACTIONAL_NUMBER";
        public const string sVCHDocumentNumber = "DOCUMENT_NUMBER";
        public const string sVCHDate = "VOUCHER_DATE";
        public const string sVCHReceiptDate = "RECEIPT_DATE";
        public const string sVCHPostDate = "POST_DATE";
        public const string sVCHPrintDate = "PRINT_DATE";
        public const string sVCHDateToPrint = "DATE_TO_PRINT"; //  SIN7178
        public const string sVCHHonoredDate = "HONORED_DATE";
        public const string sVCHVoidDate = "VOID_DATE";
        public const string sVCHBatchNumber = "VOUCHER_BATCH_NUMBER";
        public const string sVCHStatus = "PAYMENT_STATUS";//vkumar258 modified voucher_status to payment_status
        public const string sVCHMemo = "MEMO";
        public const string sVCHPayeeVerbiage = "PAYEE_VERBIAGE";
        public const string sVCHPayeeVerbiageManual = "MANUAL_IND";
        public const string sVCHPreparedByUser = "PREPARED_BY_USER";
        public const string sVCHInvoice = "INVOICE";
        public const string sVCHInvAmount = "AMOUNT";
        public const string sVCHInvNbr = "NUMBER";
        public const string sVCHInvBy = "INVOICED_BY";
        public const string sVCHInvDate = "DATE";
        public const string sVCHPONbr = "PO_NUMBER";
        public const string sVCHSvcFromDate = "SERVICE_FROM_DATE";
        public const string sVCHSvcToDate = "SERVICE_TO_DATE";
        public const string sVCHPartyInvolved = "PARTY_INVOLVED";
        public const string sVCHPayeePhrase = "PAYEE_PHRASE";
        public const string sVCHAddressReference = "ADDRESS_REFERENCE";
        public const string sVCHCommentReference = "COMMENT_REFERENCE";
// Begin SIW489
        public const string sVCHCombPay = "COMB_PAY";
        public const string sVCHCombPayCode = "CODE";
        public const string sVCHCombPayEntityId = "COMB_PAY_ENTITY_ID";
// End SIW489
        public const string sVCHTypeCheck = "CHECK";
        public const string sVCHTypeDraft = "DRAFT";
        public const string sVCHTypeReceipt = "RECEIPT";
        public const int iVCHTypeCheck = 1;
        public const int iVCHTypeDraft = 2;
        public const int iVCHTypeReceipt = 3;
        //vkumar258 Starts
        public const string sVCHOriginatingBankCode = "CODE";
        public const string sNotes = "NOTES";
        public const string sPaymentReason = "PAYMENT_REASON";
        public const string sVoucherTime = "VOUCHER_TIME";
        public const string sPrintTime = "PRINT_TIME";
        public const string sVoucherVoidTime = "VOID_TIME";
        //vkumar258 End
        //Payal --Starts for RMA-12744
        public const string sChequeType = "CHEQUE_TYPE";
        public const string sCBankName = "BANK_NAME";
        public const string sReserveAmount = "RESERVE_AMOUNT";
        //Payal --Ends for RMA-12744

        //public const string sVCHTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<CLAIM>
        '  <!-- Multiple Litigation Blocks Allowed-->
        '  <LITIGATION ID="LITxxx">
        '     <LITIGATION_TYPE CODE="xx">litigation type</LITIGATION_TYPE>
        '     <MATTER_NUMBER>sdasdad</MATTER_NUMBER>
        '     <DOCKET_NUMBER>dsdasd</DOCKET_NUMBER>
        '     <SUIT_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <COURT_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <VENUE_STATE CODE="xx">state</VENUE_STATE>
        '     <COUNTY CODE="xx">county<COUNTY>
        'Start SIW7983
        '     <MATTER_NAME>sdasdad</MATTER_NAME>
        '     <V_COMPANY INDICATOR="True|False"/>
        '     <JURISDICTION CODE="xx">jurisdiction</VENUE_STATE>
        '     <FEDERAL_DISTRICT CODE="xx">district</VENUE_STATE>
        '     <COUNTRY CODE="xx">country</VENUE_STATE>  
        'End SIW7983
        '     <LITIGATION_STATUS CODE="xx">litigation status</LITIGATION_STATUS>
        '     <SUIT_STATUS CODE="xx">Status</SUIT_STATUS>
        '     <SUIT_REASON CODE="xx">reason</SUIT_REASON>
        '     <SUIT_CITY>city</SUIT_CITY>
        '     <SERVED_BY CODE="">dasdas</SERVED_BY>
        '     <MEDIA_ARBITRATION INDICATOR="True|False"/>
        '     <SUIT_IN_DEFAULT INDICATOR="True|False"/>
        '     <RESULTS_IN_DEFAULT CODE="xx">sdasda</RESULTS_IN_DEFAULT>
        '     <COURT_VENUE CODE="xx">sdsad</COURT_VENUE>
        '     <APPEAL_REASON CODE="">xsdsdasd</APPEAL_REASON>
        '     <APPEAL_REASON_HTML>xsdsdasd</APPEAL_REASON_HTML>                         'SIW254
        '     <DEMAND_ALLEGATIONS>demands and allegations block</DEMAND_ALLEGATIONS>
        '     <DEMAND_ALLEG_HTML>demands and allegations HTML text</DEMAND_ALLEG_HTML>  'SIW254
        '     <RELATED_ARBITRATION ID="ARBxxx">
        '      <!Multiple Parties Allowed>
        '      <PARTY_INVOLVED .... Attorney, Judge, Witnesses, Others>...</PARTY_INVOLVED>
        '      <!Multiple Demand Offers Allowed>
        '      <DEMAND_OFFER> ... </DEMAND_OFFER>
        '      <!Multiple Activity_Status Allowed>
        '      <ACTIVITY_HISTORY> ... </ACTIVITY_HISTORY>
        '      <!Multiple Comments Allowed>
        '      <COMMENT_REFERENCE> ... </COMMENT>
        '      <DATA_ITEM> ... </DATA_ITEM>
        '  </LITIGATION_INFO>
        '</CLAIM>*/

        public const string sLITNode = "LITIGATION";
        public const string sLITID = "ID";
        public const string sLITIDPfx = "LIT";
        public const string sLITType = "LITIGATION_TYPE";
        public const string sLITMatter = "MATTER_NUMBER";
        public const string sLITDocket = "DOCKET_NUMBER";
        public const string sLITSuitDate = "SUIT_DATE";
        public const string sLITCourtDate = "COURT_DATE";
        public const string sLITCity = "SUIT_CITY";
        public const string sLITCounty = "COUNTY";
        public const string sLITVenueState = "VENUE_STATE";
        //Start SIW7983
        public const string sLITMatterName = "MATTER_NAME";
        public const string sLITVCompany = "V_COMPANY";
        public const string sLITJurisdiction = "JURISDICTION";
        public const string sLITFederalDistrict = "FEDERAL_DISTRICT";
        public const string sLITCountry = "COUNTRY";
        //End SIW7983       
        public const string sLITStatus = "LITIGATION_STATUS";
        public const string sLITSuitStatus = "SUIT_STATUS";
        public const string sLITSuitReason = "SUIT_REASON";
        public const string sLITServedBy = "SERVED_BY";
        public const string sLITMediaArbitration = "MEDIA_ARBITRATION";
        public const string sLITSuitInDefault = "SUIT_IN_DEFAULT";
        public const string sLITResultsInDefault = "RESULTS_IN_DEFAULT";
        public const string sLITCourtVenue = "COURT_VENUE";
        public const string sLITAppealReason = "APPEAL_REASON";
        public const string sLITAppealReasonHTML = "APPEAL_REASON_HTML"; //SIW254
        public const string sLITDemandAllegation = "DEMAND_ALLEGATIONS";        
        public const string sLITDemandAllegationHTML = "DEMAND_ALLEG_HTML"; //SIW254
        public const string sLITRelatedArb = "RELATED_ARBITRATION";
        public const string sLITPartyInvolved = "PARTY_INVOLVED";
        public const string sLITDemandOffer = "DEMAND_OFFER";
        public const string sLITActivityHistory = "ACTIVITY_HISTORY";
        public const string sLITCommentReference = "COMMENT_REFERENCE";
        public const string sLITDataItem = "DATA_ITEM";
        //public const string sLITTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<parent>
        '  Multiple nodes allowed
        '  <DEMAND_OFFER ID="DOxxx" TYPE="DEMAND | OFFER | APPEAL"/>   
        '   <ACTIVITY CODE="xx"></ACTIVITY> <SIN7884>
        '     <AMOUNT>amount</AMOUNT>
        '     <DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <TIME HOUR="17" MINUTE="35" SECOND="04" />
        '     <RESULT CODE="xx">result</RESULT>
        '     <DECISION CODE="xx">descision</DECISION>
        '     <REMARKS>remarks text</REMARKS>                                    
        '      <!Multiple Parties Allowed>
        '      <PARTY_INVOLVED .... Attorney, Judge, Witnesses, Others>...</PARTY_INVOLVED>
        '      <!Multiple Comments Allowed>
        '      <COMMENT_REFERENCE> ... </COMMENT>
        '      <!Multiple Data Items Allowed>
        '      <DATA_ITEM> ... </DATA_ITEM>
        '  </DEMAND_OFFER>
        '</parent>*/

        public const string sDONode = "DEMAND_OFFER";
        public const string sDOID = "ID";
        public const string sDOIDPfx = "DO";
        public const string sDOType = "TYPE";
        public const string sDOTypeDemand = "DEMAND";
        public const string sDOTypeOffer = "OFFER";
        public const string sDOTypeAppeal = "APPEAL";
        public const string sDOAmount = "AMOUNT";
        public const string sDODate = "DATE";
        public const string sDOTime = "TIME";
        public const string sDOResult = "RESULT";
        public const string sDODecision = "DECISION";
        public const string sDORemarks = "REMARKS";
        public const string sDOPartyInvolved = "PARTY_INVOLVED";
        public const string sDOCommentReference = "COMMENT_REFERENCE";
        public const string sDODataItem = "DATA_ITEM";
        public const string sDOActivity = "ACTIVITY"; // SIN7884
        //public const string sDOTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360


        /*'<parent>
        '  <ACTIVITY_HISTORY LINE_NUMBER="x" PREDECESSOR_LINE_NUMBER="x" ITEM_TYPE="ACTIVITY | STATUS">
        '     <ACTIVITY_TYPE CODE="xx">Activity</ACTIVITY_CODE>
        '     <DESCRIPTION>description</DESCRIPTION>
        '     <STATUS CODE="xx">status</STATUS>
        '     <DATE_CREATED YEAR="2002" MONTH="03" DAY="07" />
        '     <DATE_DUE YEAR="2002" MONTH="03" DAY="07" />
        '     <DATE_SCHEDULED_START YEAR="2002" MONTH="03" DAY="07" />
        '     <DATE_COMPLETE YEAR="2002" MONTH="03" DAY="07" />
        '     <NOTES>ActivityNotes</NOTES>                                            SI04744
        '      <!Multiple Parties Allowed>
        '      <PARTY_INVOLVED .... Attorney, Judge, Witnesses, Others>...</PARTY_INVOLVED>
        '      <!Multiple Comments Allowed>
        '      <COMMENT_REFERENCE> ... </COMMENT>
        '      <!Multiple Data Items Allowed>
        '      <DATA_ITEM> ... </DATA_ITEM>
        '  </ACTIVITY_HISTORY>
        '</parent>*/

        public const string sAHNode = "ACTIVITY_HISTORY";
        public const string sAHID = "ID"; // SIW7368
        public const string sAHIDPfx = "AH"; // SIW7368
        public const string sAHLineNbr = "LINE_NUMBER";
        public const string sAHPredLineNbr = "PREDECESSOR_LINE_NUMBER";
        public const string sAHItemType = "ITEM_TYPE";
        public const string sAHItemTypeActivity = "ACTIVITY";
        public const string sAHItemTypeStatus = "STATUS";
        public const string sAHActivityType = "ACTIVITY_TYPE";
        public const string sAHDescription = "DESCRIPTION";
        public const string sAHStatus = "STATUS";
        public const string sAHDateCreated = "DATE_CREATED";
        public const string sAHDateDue = "DATE_DUE";
        public const string sAHDateSchedStart = "DATE_SCHEDULED_START";
        public const string sAHDateComplete = "DATE_COMPLETE";
        public const string sAHNotes = "NOTES";
        public const string sAHPartyInvolved = "PARTY_INVOLVED";
        public const string sAHCommentReference = "COMMENT_REFERENCE";
        public const string sAHDataItem = "DATA_ITEM";
        public const string sAHOwner = "OWNER"; // SIW7368
        public const string sAHTechKey = "TECH_KEY"; // SIW7368
        public const string sAHDeleteFlag = "DELETED_FLAG"; // SIW7368
        //public const string sAHTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360
        /*'<CLAIM>
        '  <SUBROGATION ID="SUBxxx">
        '     <TYPE CODE="xx">type</TYPE>
        '     <ADVERSE_POLICY_NUM>policynumber</ADVERSE_POLICY_NUM>
        '     <ADVERSE_CLAIM_NUM>claimnumber</ADVERSE_CLAIM_NUM>
        '     <FIRST_NOTICE_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <NEXT_NOTICE_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <COUNTERCLAIM>counterclaim</COUNTERCLAIM>
        '     <LITIGATION INDICATOR="TRUE|FALSE"/>
        '     <AWARD_AMOUNT>amount</AWARD_AMOUNT>
        '     <ADDITIONAL_ADVERSE INDICATOR="TRUE|FALSE">       //(SI06023 - Implemented in SI06333)
        '     <PARTY CODE="xx">description</PARTY>              //(SI06023 - Implemented in SI06333) 
        '     <ADDITIONAL_ADVERSE>???</ADDITIONAL_ADVERSE>
        '     <STATUS CODE="xx">status</STATUS>
        '     <STATUS_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <STATUS_DESCRIPTION>description</STATUS_DESCRIPTION>
        '     <NUMBER_OF_YEARS>years</NUMBER_OF_YEARS>
        '     <STATUTE_LIMITATION_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <COVERAGE CODE="xx">coverage</COVERAGE>
        '     <ADV_PARTIES_COV_LIMIT>limit</ADV_PARTIES_COV_LIMIT>
        '     <AMOUNT_PAID>paid amount</AMOUNT_PAID>
        '     <COV_DED_AMOUNT>deductible</COV_DED_AMOUNT>
        '     <SETTLEMENT_PERCENT>percent</SETTLEMENT_PERCENT>
        '     <TOTAL_TO_BE_RECOVERED>totalamount</TOTAL_TO_BE_RECOVERED>
        '     <POTENTIAL_PAYOUT_AMT>amount</POTENTIAL_PAYOUT_AMT>
        '     <POTENTIAL_DED_AMT>amount</POTENTIAL_DED_AMT>
        '     <RELATED_ARBITRATION ID="ARBxxx">
        '      <!Multiple Parties Allowed>
        '      <PARTY_INVOLVED .... Attorney, Judge, Witnesses, Others>...</PARTY_INVOLVED>
        '      <!Multiple Comments Allowed>
        '      <COMMENT_REFERENCE> ... </COMMENT>
        '      <!Multiple Activity_Status Allowed>
        '      <ACTIVITY_HISTORY> ... </ACTIVITY_HISTORY>
        '      <!Multiple Data Items Allowed>
        '      <DATA_ITEM> ... </DATA_ITEM>
        '  </SUBROGATION>
        '</CLAIM>*/

        public const string sSUBNode = "SUBROGATION";
        public const string sSUBID = "ID";
        public const string sSUBIDPfx = "SUB";
        public const string sSUBType = "TYPE";
        public const string sSUBAdvPolicy = "ADVERSE_POLICY_NUM";
        public const string sSUBAdvClaim = "ADVERSE_CLAIM_NUM";
        public const string sSUBFirstNoticeDate = "FIRST_NOTICE_DATE";
        public const string sSUBNextNoticeDate = "NEXT_NOTICE_DATE";
        public const string sSUBCounterClaim = "COUNTERCLAIM";
        public const string sSUBLitigation = "LITIGATION_INVOLVED";
        public const string sSUBAwardAmount = "AWARD_AMOUNT";
        public const string sSUBCompNegligence = "COMPARATIVE_NEGLIGENCE";
        public const string sSUBAddAdverse = "ADDITIONAL_ADVERSE";
        public const string sSUBStatus = "STATUS";
        public const string sSUBStatusDate = "STATUS_DATE";
        public const string sSUBStatusDesc = "STATUS_DESCRIPTION";
        public const string sSUBNbrOfYears = "NUMBER_OF_YEARS";
        public const string sSUBStatLimDate = "STATUTE_LIMITATION_DATE";
        public const string sSUBCoverage = "COVERAGE";
        public const string sSUBAdvPartyCovLimit = "ADV_PARTIES_COV_LIMIT";
        public const string sSUBAmtPaid = "AMOUNT_PAID";
        public const string sSUBDedAmount = "COV_DED_AMOUNT";
        public const string sSUBSettlementPrc = "SETTLEMENT_PERCENT";
        public const string sSUBTotalTBRecovered = "TOTAL_TO_BE_RECOVERED";
        public const string sSUBPotPayoutAmt = "POTENTIAL_PAYOUT_AMT";
        public const string sSUBPotDedAmt = "POTENTIAL_DED_AMT";
        public const string sSUBRelatedArb = "RELATED_ARBITRATION";
        public const string sSUBActivityHistory = "ACTIVITY_HISTORY";
        public const string sSUBPartyInvolved = "PARTY_INVOLVED";
        public const string sSUBCommentReference = "COMMENT_REFERENCE";
        public const string sSUBDataItem = "DATA_ITEM";
        //public const string sSUBTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<CLAIM>
        '  <ARBITRATION ID="ARBxxx">
        '     <TYPE CODE="xx">type</TYPE>
        '     <ADVERSE_CLAIM_NUM>claimnumber</ADVERSE_CLAIM_NUM>
        '     <FILE_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <HEARING_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <COMPARATIVE_NEGLIGENCE INDICATOR="TRUE|FALSE"/>
        '     <ADDITIONAL_ADVERSE>???</ADDITIONAL_ADVERSE>
        '     <STATUS CODE="xx">status</STATUS>
        '     <STATUS_DATE YEAR="2002" MONTH="03" DAY="07" />
        '     <STATUS_DESCRIPTION>description</STATUS_DESCRIPTION>
        '     <AWARD_AMOUNT>award</AWARD_AMOUNT>
        '     <RELATED_LITIGATION ID="LITxxx">
        '     <RELATED_SUBROGATION ID="SUBxxx">
        '      <!Multiple Demand Offers Allowed>
        '      <DEMAND_OFFER> ... </DEMAND_OFFER>
        '      <!Multiple Parties Allowed>
        '      <PARTY_INVOLVED .... Attorney, Judge, Witnesses, Others>...</PARTY_INVOLVED>
        '      <!Multiple Comments Allowed>
        '      <COMMENT_REFERENCE> ... </COMMENT>
        '      <!Multiple Activity_Status Allowed>
        '      <ACTIVITY_HISTORY> ... </ACTIVITY_HISTORY>
        '      <!Multiple Data Items Allowed>
        '      <DATA_ITEM> ... </DATA_ITEM>
        '  </ARBITRATION>
        '</CLAIM>*/

        public const string sARBNode = "ARBITRATION";
        public const string sARBID = "ID";
        public const string sARBIDPfx = "ARB";
        public const string sARBType = "TYPE";
        public const string sARBAdvClaim = "ADVERSE_CLAIM_NUM";
        public const string sARBFileDate = "FILE_DATE";
        public const string sARBHearingDate = "HEARING_DATE";
        public const string sARBCompNegligence = "COMPARATIVE_NEGLIGENCE";
        public const string sARBAddAdverse = "ADDITIONAL_ADVERSE";
        public const string sARBParty = "PARTY";//(SI06023 - Implemented in SI06333)
        public const string sARBStatus = "STATUS";
        public const string sARBStatusDate = "STATUS_DATE";
        public const string sARBStatusDesc = "STATUS_DESCRIPTION";
        public const string sARBAwardAmount = "AWARD_AMOUNT";
        public const string sARBRelatedSub = "RELATED_SUBROGATION";
        public const string sARBRelatedLit = "RELATED_LITIGATION";
        public const string sARBDemandOffer = "DEMAND_OFFER";
        public const string sARBActivityHistory = "ACTIVITY_HISTORY";
        public const string sARBPartyInvolved = "PARTY_INVOLVED";
        public const string sARBCommentReference = "COMMENT_REFERENCE";
        public const string sARBDataItem = "DATA_ITEM";
        //public const string sARBTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<Parent>
        '  <SALVAGE>
        '     <CONTROL_NUMBER>control</CONTROL_NUMBER>
        '     <TYPE CODE="xx">type</TYPE>
        '     <STATUS CODE="xx">status</STATUS>
        '     <STOCK_NUMBER>stocknumber</STOCK_NUMBER>
        '     <DATE_ARRIVED YEAR="2002" MONTH="03" DAY="07" />
        '     <DATE_CUTOFF YEAR="2002" MONTH="03" DAY="07" />
        '     <DATE_APPRAISED YEAR="2002" MONTH="03" DAY="07" />
        '     <DATE_SOLD YEAR="2002" MONTH="03" DAY="07" />
        '     <DATE_CLOSED YEAR="2002" MONTH="03" DAY="07" />
        '     <CUTOFF_REASON CODE="xx">cutoff reason</CUTOFF_REASON>
        '     <DAILY_FEE>fee</DAILY_FEE>
        '     <ACTUAL_CASH_VALUE>value</ACTUAL_CASH_VALUE>
        '     <INITIAL_TOW_CHARGE>charge</INITIAL_TOW_CHARGE>
        '     <SALE_PRICE>sale price</SALE_PRICE>
        '     <RETAINED_BY_OWNER INDICATOR="True|False"/>
        '     <NET_RECOVERED>recovered amount</NET_RECOVERED>
        '     <PERCENT_RECOVERED>recovered amount</PERCENT_RECOVERED>
        '     <STORAGE_CHARGES>storage charges</STORAGE_CHARGES>
        '     <OTHER_CHARGES>other charges</OTHER_CHARGES>
        '     <TOTAL_CHARGES>total charges</TOTAL_CHARGES>
        '     <ESTIMATED_SALVAGE_VALUE>value</ESTIMATED_SALVAGE_VALUE>
        '     <COMMENT>commenttext</COMMENT>                           //SI06023
        '      <PARTY_INVOLVED .... Attorney, Judge, Witnesses, Others>...</PARTY_INVOLVED>
        '      <!Multiple Comments Allowed>
        '      <COMMENT_REFERENCE> ... </COMMENT>
        '      <!Multiple Data Items Allowed>
        '      <DATA_ITEM> ... </DATA_ITEM>
        '  </SALVAGE>
        '</Parent>*/

        public const string sSALNode = "SALVAGE";
        public const string sSALType = "TYPE";
        public const string sSALControlNumber = "CONTROL_NUMBER";
        public const string sSALStatus = "STATUS";
        public const string sSALStockNumbr = "STOCK_NUMBER";
        public const string sSALDateArrived = "DATE_ARRIVED";
        public const string sSALDateCutoff = "DATE_CUTOFF";
        public const string sSALDateAppraised = "DATE_APPRAISED";
        public const string sSALDateSold = "DATE_SOLD";
        public const string sSALDateClosed = "DATE_CLOSED";
        public const string sSALCutoff = "CUTOFF_REASON";
        public const string sSALDailyFee = "DAILY_FEE";
        public const string sSALActualCashValue = "ACTUAL_CASH_VALUE";
        public const string sSALTowCharge = "INITIAL_TOW_CHARGE";
        public const string sSALSalePrice = "SALE_PRICE";
        public const string sSALOwnerRetained = "RETAINED_BY_OWNER";
        public const string sSALNetRecovered = "NET_RECOVERED";
        public const string sSALPercentRecovered = "PERCENT_RECOVERED";
        public const string sSALStorageCharge = "STORAGE_CHARGES";
        public const string sSALOtherCharges = "OTHER_CHARGES";
        public const string sSALTotalCharges = "TOTAL_CHARGES";
        public const string sSALLocationAddress = "LOCATION_ADDRESS";       //SIW198
        public const string sSALSalvageValue = "ESTIMATED_SALVAGE_VALUE";
        public const string sSALPartyInvolved = "PARTY_INVOLVED";
        public const string sSALCommentReference = "COMMENT_REFERENCE";
        public const string sSALDataItem = "DATA_ITEM";
        //public const string sSALTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        /*'<TRIGGERS COUNT='xxxx'>
        '  <TRIGGER ID='TRGxxx'>
        '    <TRIGGER_TYPE CODE="triggercode">Description of trigger</TRIGGER_TYPE>
        '    <DATE YEAR="YYYY" MONTH="MM" DAY="DD">[displaydate</DATE>]
        '    <TIME HOUR="HH" MINUTE="MM" SECOND="SS" TIMEZONE="ttt"/>[display time</TIME>]
        '    <USER>Login name of user that issued the trigger</USER>
        '    <UPLOADED INDICATOR={Y|N})>
        '    <UPLOADED_RqUID>guid for the process that uploaded the trigger</UPLOADED_RqUID>
        '    <RELATED_TABLE TABLE_ID="tableid">tablename</RELATED_TABLE>
        '    <RELATED_RECORD_KEY>recordkey</RELATED_RECORD_KEY>
        '    <SERVICE_LOG ID="SLOGxxxx">
        '      <!-- Multiple Service Logs Allowed -->
        '      <STATUS CODE="statuscode">Current status description</STATUS>
        '      <PROCESS_DATE YEAR="YYYY" MONTH="MM" DAY="DD">[displaydate</PROCESS_DATE>]
        '      <PROCESS_TIME HOUR="HH" MINUTE="MM" SECOND="SS" TIMEZONE="ttt"/>[display time</PROCESS_TIME>]
        '      <COMPLETE_DATE YEAR="YYYY" MONTH="MM" DAY="DD">[displaydate</COMPLETE_DATE>]
        '      <COMPLETE_TIME HOUR="HH" MINUTE="MM" SECOND="SS" TIMEZONE="ttt"/>[display time</COMPLETE_TIME>]
        '      <REQUEST>
        '        <NAME>service request name</NAME>
        '        <RqUID>document ID where trigger was reported</RqUID>
        '        <DATE YEAR="YYYY" MONTH="MM" DAY="DD">[displaydate</DATE>]
        '        <TIME HOUR="HH" MINUTE="MM" SECOND="SS" TIMEZONE="ttt"/>[display time</TIME>]
        '        <DESCRIPTION>service description</DESCRIPTION>
        '      </REQUEST>
        '      <RESPONSE>
        '        <NAME>Name of the response document</NAME>
        '        <RqUID>ID of the response document</RqUID>
        '        <DATE YEAR="YYYY" MONTH="MM" DAY="DD">[displaydate</DATE>]
        '        <TIME HOUR="HH" MINUTE="MM" SECOND="SS" TIMEZONE="ttt"/>[display time</TIME>]
        '      </RESPONSE>
        '      <ERROR CODE="errorcode">errordescription</ERROR>
        '    </SERVICE_LOG>
        '    <DATA_ITEM> ... </DATA_ITEM>
        '  </TRIGGER>
        '</TRIGGERS>*/
        public const string sTrgrList = "TRIGGERS";
        public const string sTrgrCount = "COUNT";
        public const string sTrgrNode = "TRIGGER";
        public const string sTrgrID = "ID";
        public const string sTrgrIDPFX = "TRG";
        public const string sTrgrSLogIDPFX = "SLOG";
        public const string sTrgrType = "TYPE";
        public const string sTrgrDate = "DATE";
        public const string sTrgrTime = "TIME";
        public const string sTrgrUser = "USER";
        public const string sTrgrName = "NAME";
        public const string sTrgrDesc = "DESCRIPTION";
        public const string sTrgrRequest = "REQUEST";
        public const string sTrgrResponse = "RESPONSE";
        public const string sTrgrRqUID = "RqUID";
        public const string sTrgrProcDate = "PROCESS_DATE";
        public const string sTrgrProcTime = "PROCESS_TIME";
        public const string sTrgrStatus = "STATUS";
        public const string sTrgrUploaded = "UPLOADED";
        public const string sTrgrRelatedTable = "RELATED_TABLE";
        public const string sTrgrRelatedTableID = "TABLE_ID";
        public const string sTrgrRelatedRecordKey = "RELATED_RECORD_KEY";
        public const string sTrgrCompleteDate = "COMPLETE_DATE";
        public const string sTrgrCompleteTime = "COMPLETE_TIME";
        public const string sTrgrServiceLog = "SERVICE_LOG";
        public const string sTrgrError = "ERROR";
        public const string sTrgrDataItem = "DATA_ITEM";
        //public const string sTrgrTechKey = "TECH_KEY";//(SI06023 - Implemented in SI06333)//SIW360

        //The following are from XMLFormatting

        // The following constants are taken from CCPXFConstants.bas

        // Application constants
        //public const string sCCPXmlVersion = "2010.3"; //AC Web //SIW360  //SIW490
        public const string sCCPXmlVersion = "2010.3.2";  //SIW490

        public const string sCCPXmlAddressIDPrefix = "ADR";
        public const string sCCPXmlEntityIDPrefix = "ENT";

        // Universal Data Types
        public const int idtDate = 0;
        public const int idtTime = 1;
        public const int idtNumber = 2;
        public const int idtString = 3;
        public const int idtCode = 4;
        public const int idtBoolean = 5;
        public const int idtType = 6;
        public const int idtDTTM = 7;
        public const int idtCodeList = 8;
        public const int idtEntityRef = 9;
        public const int idtDataRecord = 10;

        public const string sdtDate = "DATE";
        public const string sdtTime = "TIME";
        public const string sdtNumber = "NUMBER";
        public const string sdtString = "STRING";
        public const string sdtCode = "CODE";
        public const string sdtBoolean = "BOOLEAN";
        public const string sdtType = "TYPE";
        public const string sdtDTTM = "DTTM";
        public const string sdtEntityRef = "ENTITYREF";
        public const string sdtDataRecord = "TABLE";
        public const string sdtAmount = "AMOUNT";//SIW8084
        public const string sdtUserLookUp = "USERLOOKUP";//SIN8438

        // XML Header Tag Name Constants
        // <HEADER DOCVERSION="2002.1"
        //         SOURCEAPP="vAppName"
        //         SOURCEDB="vUserDSN"
        //         TARGETDB="vTargetDatabase"
        //         APPVERSION="vAppVersion"
        //         USERID="vUserId"
		//Start SIW485
        //         PASSWORD="vPassword"
		//		   SESSIONID="vSessionID">
		//End SIW485
        //   <DOC_NAME>vDocName</DOC_NAME>
        //   <RqUID>uuid</RqUID>
        //   <DESCRIPTION>vAppDescription</DESCRIPTION>
        //   <DATE_CREATED YEAR="CurrentYear"
        //                 MONTH="CurrentMonth"
        //                 DAY="CurrentDay"/>
        //   <TIME_CREATED TIME_ZONE="CurrentTimeZone"
        //                 HOUR="CurrentHour"
        //                 MINUTE="CurrentMinute"
        //                 SECOND="CurrentSecond"/>
        //Start SIW180
        //   <DATE_EXTRACTED YEAR="CurrentYear"
        //                   MONTH="CurrentMonth"
        //                   DAY="CurrentDay"/>
        //   <TIME_EXTRACTED TIME_ZONE="CurrentTimeZone"
        //                   HOUR="CurrentHour"
        //                   MINUTE="CurrentMinute"
        //                   SECOND="CurrentSecond"/>
        //End SIW180
        // </HEADER>

        public const string sHdrElement = "HEADER";
        public const string sHdrName = "DOC_NAME";
        public const string sHdrDescription = "DESCRIPTION";
        public const string sHdrDate = "DATE_CREATED";
        public const string sHdrTime = "TIME_CREATED";
        public const string sHdrDateExtract = "DATE_EXTRACTED"; //SIW180
        public const string sHdrTimeExtract = "TIME_EXTRACTED"; //SIW180
        public const string sHdrDocVersion = "DOCVERSION";
        public const string sHdrSessionID = "SESSIONID";        //SIW485
        public const string sHdrSourceDB = "SOURCEDB";
        public const string sHdrTargetDB = "TARGETDB";
        public const string sHdrRqUID = "RqUID";
        public const string sHdrSourceApp = "SOURCEAPP";
        public const string sHdrAppVersion = "APPVERSION";
        public const string sHdrUserID = "USERID";
        public const string sHdrPassword = "PASSWORD";
        public const string sHdrUseSSO = "UseSSO";  //SIW8194

        // XML Processing Message Tag Constants
        // <PROCESSING_MESSAGE TYPE="[REQUEST,RESPONSE]">
        //   <FUNCTION>Function</FUNCTION>
        //   <RESPOND_TO>URL to send a Request Reponse to {string}</RESPOND_TO>
        //   <RESPOND_BY>Number of seconds that a response is required by {integer}</RESPOND_BY>
        //   <PARAMETERS COUNT={integer}>
        //      <PARAMETER NAME="Parameter Name" TYPE="[NUMBER,DATE,TIME,STRING,CODE]"
        //        [CODE="code"|
        //         YEAR="year" MONTH="month" DAY="day"|
        //         HOUR="hour" MINUTE="minute" SECOND="second" TIME_ZONE="timezone"]>
        //         optional value
        //      </PARAMETER>
        //      <!-- Multiple Parameters Allowed -->
        //   </PARAMETERS>
        //   <REPONSE_MESSAGE TYPE="{string} [FILE, URL, TEXT]">
        //      <ERRORS COUNT="errorcount">
        //         <ERROR CODE="errorcode"
        //                NUMBER="errornumber"
        //                MNEMONIC="text"
        //                SOURCE="errorsource">description</Error>
        //      </ERRORS>
        //      <MESSAGE>text response message or the address of a file containing the response {string}</MESSAGE>
        //      <REQUESTING_DOC>
        //         <NAME>Requesting document name. From request HEADER-&gt;NAME {string}</NAME>
        //         <RqUID>{UID of requesting documment}</RqUID>
        //         <APPLICATION>Name of application issuing the original request. From request HEADER-&gt;APPLICATION {string}</APPLICATION>
        //         <SOURCEDB>Database name of where original request originated.  From request HEADER-&gt;SOURCEDB {string}</SOURCEDB>
        //         <DATE YEAR="Year (4) {Integer} [1900 - 9999]"
        //               MONTH="Month (2) {Integer} [1-12]"
        //               DAY="Day (2) {Integer} [1-31]"/>
        //         <TIME TIME_ZONE="Time Zone that the Time references [EST,EDT,MCT,etc.]"
        //               HOUR="Hour (2) {Integer} [0-23]"
        //               MINUTE="Minute (2) {Integer} [0-59]"
        //               SECOND="Second (2) {Integer} [0-59]"/>
        //      </REQUESTING_DOC>
        //   </REPONSE_MESSAGE>
        // </PROCESSING_MESSAGE>

        public const string sPM = "PROCESSING_MESSAGE";
        public const string sPMType = "TYPE";
        public const string sPMTypeRequest = "REQUEST";
        public const string sPMTypeResponse = "RESPONSE";
        public const string sPMFunction = "FUNCTION";
        public const string sPMRespondTo = "RESPOND_TO";
        public const string sPMRespondBy = "RESPOND_BY";
        public const string sPMParameters = "PARAMETERS";
        public const string sPMParametersCount = "COUNT";
        public const string sPMParameter = "PARAMETER";
        public const string sPMParameterName = "NAME";
        public const string sPMParameterType = "TYPE";
        public const string sPMResponse = "REPONSE_MESSAGE";
        public const string sPMResponseType = "TYPE";
        public const string sPMResponseTypeFile = "FILE";
        public const string sPMResponseTypeURL = "URL";
        public const string sPMResponseTypeText = "TEXT";
        public const string sPMErrors = "ERRORS";
        public const string sPMCount = "COUNT";
        public const string sPMError = "ERROR";
        public const string sPMCode = "CODE";
        public const string sPMNumber = "NUMBER";
        public const string sPMMnemonic = "MNEMONIC";
        public const string sPMErrSource = "SOURCE";
        public const string sPMErrSeverity = "SEVERITY"; //SIW452
        public const string sPMResponseMessage = "MESSAGE";
        public const string sPMRequestDoc = "REQUESTING_DOC";
        public const string sPMRequestApp = "APPLICATION";
        public const string sPMRequestSourceDB = "SOURCEDB";
        public const string sPMRequestName = "NAME";
        public const string sPMRequestUID = "RqUID";
        public const string sPMRequestDate = "DATE";
        public const string sPMRequestTime = "TIME";

        // Message enumerators
        public const int ipmmtRequest = 0;
        public const int ipmmtResponse = 1;

        public const int ipmrtFile = 0;
        public const int ipmrtURL = 1;
        public const int ipmrtText = 2;

        // XML Date Tag Name Constants
        public const string sDateYear = "YEAR";
        public const string sDateMonth = "MONTH";
        public const string sDateDay = "DAY";

        public const string sIndicator = "INDICATOR";

        // XML Time Tag Name Constants
        public const string sTimeHour = "HOUR";
        public const string sTimeMinute = "MINUTE";
        public const string sTimeSecond = "SECOND";
        public const string sTimeTimeZone = "TIME_ZONE";

        // <BATCH COUNT="Count of Documents to be Processed {integer}">
        //   <FOLDER>Batch submision folder {string}</FOLDER>
        //   <ITEM TYPE="Type of Input Document {string} [FROIIMPORT,othersTBD]">
        //      <FILE>Physical File Name of Document {string}</FILE>
        //      <NAME>Internal Document Name Matched to Document Name in Header {string}</NAME>
        //      <FOLDER>Folder where item is located {string}</FOLDER>
        //   </ITEM>
        // </BATCH>

        public const string sBatchElement = "BATCH";
        public const string sBatCount = "COUNT";
        public const string sBatFolder = "FOLDER";
        public const string sBatItem = "ITEM";
        public const string sBatItemType = "TYPE";
        public const string sBatItemFile = "FILE";
        public const string sBatItemFolder = "FOLDER";
        public const string sBatItemFolderAddress = "ADDRESS";
        public const string sBatItemName = "NAME";

        public const string sAddressID = "ID";

        public const int ibiFirst = 0;
        public const int ibiNext = 1;
        public const int ibiCurr = 2;
        public const int ibiPrev = -1;

        public const string sbiAbsolute = "ABSOLUTE";
        public const string sbiRelative = "RELATIVE";
        public const string sbiUndefined = "UNDEFINED";

        public const int ibiUndefined = -1;
        public const int ibiAbsolute = 0;
        public const int ibiRelative = 1;

        public const int ibiXML = 0;
        public const int ibiProcedure = 1;
        public const int ibiFile = 2;
        public const int ibiText = 3;

        public const string sbiXML = "XML";
        public const string sbiProcedure = "PROCEDURE";
        public const string sbiFile = "FILE";
        public const string sbiText = "TEXT";

        //imported from XS

        /*'<GLOSSARY TABLE_COUNT="tablecount">
'   <!-- Multiple Table Entries -->
'   <GLOSSARY_TABLE ID="TBLxxxx"
'                   TYPE="type of table (SYS,USER,etc.)"
'                   LINE_OF_BUS_IND="boolean"
'                   DELETED="boolean"
'                   ATTACHMENTS="boolean"
'                   CODES_COUNT="count">
'      <TABLE_NAME>system table name</TABLE_NAME>
'      <IND_STD_TABLE REQUIRED="boolean">industry standard table name</IND_STD_TABLE_NAME>
'      <RELATED_TABLE REQUIRED="boolean">Related table name</RELATED_TABLE_NAME>
'      <INDEX_COLUMNS>index</INDEX_COLUMNS>
'      <!-- Multiple Friendly Names -->
'      <FRIENDLY_NAME>
'         <NAME>friendly name</NAME>
'         <LANGUAGE CODE="language code">language</LANGUAGE>
'         <USEAGE>table useage</USEAGE>
'      </FRIENDLY_NAME>
'      <!-- Multiple Code Entries -->
'      <CODE_ENTRY ID="CDEXXX"
'                  SHORT_CODE="short code"
'                  DELETED="boolean"
'                  DATE_TRIGGER="date trigger">
'         <EFF_DATE>Effective Date</EFF_DATE>
'         <EXP_DATE>Expiration Date</EXP_DATE>
'         <LINE_OF_BUSINESS CODE="lob short code">line of business</LINE_OF_BUSINESS>
'         <INDUSTRY_STD CODE="industry standard short code">industry standard</INDUSTRY_STD>
'         <RELATED CODE="Related code short code">related code from related table</RELATED>
'         <!-- Multiple text entries -->
'         <CODE_TEXT>
'            <TEXT>text</TEXT>
'            <LANGUAGE CODE="language code">language</LANGUAGE>
'            <USEAGE>code useage</USEAGE>
'         </CODE_TEXT>
'      </CODE_ENTRY>
'   </CODE_TABLE>
'</CODES>*/

        public const string sCDGlossary = "GLOSSARY";
        public const string sCDTableCount = "TABLE_COUNT";
        public const string sCDTable = "GLOSSARY_TABLE";
        public const string sCDTableID = "ID";
        public const string sCDTableIDPfx = "TBL";
        public const string sCDTableType = "TYPE";
        public const string sCDTableLOBInd = "LINE_OF_BUS_IND";
        public const string sCDTableCodesCount = "CODES_COUNT";
        public const string sCDTableAttachment = "ATTACHMENTS";
        public const string sCDTableName = "TABLE_NAME";
        public const string sCDTableIndStdTable = "IND_STD_TABLE";
        public const string sCDTableRelatedTable = "RELATED_TABLE";
        public const string sCDTableIndex = "INDEX_COLUMNS";
        public const string sCDRequired = "REQUIRED";
        public const string sCDTableFriendlyNameNode = "FRIENDLY_NAME";
        public const string sCDTableFriendlyName = "NAME";
        public const string sCDLanguage = "LANGUAGE";
        public const string sCDUseage = "USEAGE";
        public const string sCDCodeNode = "CODE_ENTRY";
        public const string sCDCodeID = "ID";
        public const string sCDCodeIDPfx = "CDE";
        public const string sCDShortCode = "SHORT_CODE";
        public const string sCDDeleted = "DELETED";
        public const string sCDDateTrigger = "DATE_TRIGGER";
        public const string sCDEffDate = "EFF_DATE";
        public const string sCDExpDate = "EXP_DATE";
        public const string sCDLineOfBusCode = "LINE_OF_BUSINESS";
        public const string sCDIndStdCode = "INDUSTRY_STD";
        public const string sCDRelatedCode = "RELATED";
        public const string sCDCodeTextNode = "CODE_TEXT";
        public const string sCDCodeText = "TEXT";

        public const int iCDTableTypeSystem = 1;
        public const int iCDTableTypeSystemCode = 2;
        public const int iCDTableTypeUserCode = 3;
        public const int iCDTableTypeEntityCode = 4;
        public const int iCDTableTypeOrgHier = 5;
        public const int iCDTableTypeSupp = 6;
        public const int iCDTableTypePeople = 7;
        public const int iCDTableTypeAdminTrack = 8;
        public const int iCDTableTypeJurisdic = 9;
        public const int iCDTableTypeIndStdCodes = 10;
        public const int iCDTableTypeGlossary = 11;

        public const string sCDTableTypeSystem = "SYSTEM";
        public const string sCDTableTypeSystemCode = "SYSTEM_CODE";
        public const string sCDTableTypeUserCode = "USER_CODE";
        public const string sCDTableTypeEntityCode = "ENTITY_CODE";
        public const string sCDTableTypeOrgHier = "ORGHIERARCHY";
        public const string sCDTableTypeSupp = "SUPPLEMENTAL";
        public const string sCDTableTypePeople = "PEOPLE";
        public const string sCDTableTypeAdminTrack = "ADMIN.TRACKING";
        public const string sCDTableTypeJurisdic = "JURISDICTIONAL";
        public const string sCDTableTypeIndStdCodes = "INDSTD_CODES";
        public const string sCDTableTypeGlossary = "GLOSSARY";

        /*'<Parent Node>
        '   <BALANCES>
        '     <DATA_ITEM
        '         Name = "Data Item Name"
        '         TYPE="[BOOLEAN,DATE,TIME,CODE,STRING,NUMBER] specifies how the XML processor is to parse the data item"
        '            [data attributes corresponding to data types above as follows:
        '             BOOLEAN:  INDICATOR="[YES|NO]{Boolean}"
        '             DATE:  YEAR="year{integer}" MONTH="month{integer}" DAY="day{integer}"
        '             TIME:  HOUR="hour{integer}" MINUTE="minute{integer}" SECOND="second{integer}" TIME_ZONE="time zone{string}"
        '             CODE:  CODE="shortcode"]>
        '            [Data corresponding to the data type as follows:
        '            STRING:  data value {string}
        '            NUMBER:  data value {decimal}
        '            DATE:  formatted Date
        '            TIME:  formatted TIME
        '     </DATA_ITEM>
        '   </BALANCES>
        '</Parent Node>*/

        public const string sBLNode = "BALANCES";

        /*'<ORG_HIERARCHY>
        '   <CLIENT ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '      <COMPANY ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '         <OPERATION ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '            <REGION ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '               <DIVISION ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '                  <LOCATION ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '                     <FACILITY ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '                        <DEPARTMENT ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO"/>
        '                     </FACILITY>
        '                  </LOCATION>
        '               </DIVISION>
        '            </REGION>
        '         </OPERATION>
        '      </COMPANY>
        '   </CLIENT>
        '</ORG_HIERARCHY>*/

        public const string sOrgHierNode = "ORG_HIERARCHY";
        public const string sOrgHierKey = "KEY";
        public const string sOrgHierID = "ID";
        public const string sOrgHierDelete = "DELETE";
        public const string sOrgHierClient = "CLIENT";
        public const string sOrgHierCompany = "COMPANY";
        public const string sOrgHierOperation = "OPERATION";
        public const string sOrgHierRegion = "REGION";
        public const string sOrgHierDivision = "DIVISION";
        public const string sOrgHierLocation = "LOCATION";
        public const string sOrgHierFacility = "FACILITY";
        public const string sOrgHierDepartment = "DEPARTMENT";
        public const int iOrgHierClient = 1005;
        public const int iOrgHierCompany = 1006;
        public const int iOrgHierOperation = 1007;
        public const int iOrgHierRegion = 1008;
        public const int iOrgHierDivision = 1009;
        public const int iOrgHierLocation = 1010;
        public const int iOrgHierFacility = 1011;
        public const int iOrgHierDepartment = 1012;

        // Start SIW7896
        /*'<ORG_SECURITY>
        '   <CLIENT_SEC ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '      <COMPANY_SEC ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '         <OPERATION_SEC ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '            <REGION_SEC ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '               <DIVISION_SEC ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '                  <LOCATION_SEC ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '                     <FACILITY_SEC ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO">
        '                        <DEPARTMENT_SEC ID="ENTxxx" KEY="XXXXXX" DELETE="YES|NO"/>
        '                     </FACILITY_SEC>
        '                  </LOCATION_SEC>
        '               </DIVISION_SEC>
        '            </REGION_SEC>
        '         </OPERATION_SEC>
        '      </COMPANY_SEC>
        '   </CLIENT_SEC>
        '</ORG_SECURITY>*/

        public const string sOrgSecNode = "ORG_SECURITY";
        public const string sOrgSecKey = "KEY";
        public const string sOrgSecID = "ID";
        public const string sOrgSecDelete = "DELETE";
        public const string sOrgSecClient = "CLIENT_SEC";
        public const string sOrgSecCompany = "COMPANY_SEC";
        public const string sOrgSecOperation = "OPERATION_SEC";
        public const string sOrgSecRegion = "REGION_SEC";
        public const string sOrgSecDivision = "DIVISION_SEC";
        public const string sOrgSecLocation = "LOCATION_SEC";
        public const string sOrgSecFacility = "FACILITY_SEC";
        public const string sOrgSecDepartment = "DEPARTMENT_SEC";
        public const int iOrgSecClient = 905;
        public const int iOrgSecCompany = 906;
        public const int iOrgSecOperation = 907;
        public const int iOrgSecRegion = 908;
        public const int iOrgSecDivision = 909;
        public const int iOrgSecLocation = 910;
        public const int iOrgSecFacility = 911;
        public const int iOrgSecDepartment = 912;
        // End SIW7896

        /*'<CODE_RELATIONSHIPS RELATIONSHIP_COUNT="1">
        '  <RELATIONSHIP ID="CXR1623" RELATIONSHIP_SHORT_CODE="LOBTOCT" RELATION_COUNT=1 DESC_ONLY="No" REVERSABLE="Yes" SHOW_REVERSE="No" ONE_ONLY="No" REV_ONE_ONLY="No" AUTO_REV_REL="No" EXCL_MATCH_VAL="No">
        '    <CODE_TABLE_NAME>Table_Name</CODE_TABLE_NAME>
        '    <RELATE_TABLE_NAME>Table_Name</RELATE_TABLE_NAME>
        '    <RELATION_DESC>Line of Business to Claim Type</RELATION_DESC>
        '    <RELATION_REV_DESC>Claim Type to Line of Business</RELATION_REV_DESC>
        '    <CODE_CAPTION>Line Of Business</CODE_CAPTION>
        '    <RELATED_CAPTION>Claim Types</RELATED_CAPTION>
        '    <RELATION_ENTRY ID="CXC5677" SHORT_CODE="APV" REL_SHORT_CODE="AA" DELETED="No"/>
        '     ...
        '  </RELATIONSHIP>
        '</CODE_RELATIONSHIPS>*/

        public const string sCRNode = "CODE_RELATIONSHIPS";
        public const string sCRRelationshipCount = "RELATIONSHIP_COUNT";
        public const string sCRRelationship = "RELATIONSHIP";
        public const string sCRRelationshipID = "ID";
        public const string sCRRelationshipIDPfx = "CXR";
        public const string sCRRelationshipShort = "RELATIONSHP_SHORT_CODE";
        public const string sCRRelationCount = "RELATION_COUNT";
        public const string sCRDescOnly = "DESC_ONLY";
        public const string sCRReversable = "REVERSABLE";
        public const string sCRShowReverse = "SHOW_REVERSE";
        public const string sCROneOnly = "ONE_ONLY";
        public const string sCRRevOneOnly = "REV_ONE_ONLY";
        public const string sCRAutoReverse = "AUTO_REV_REL";
        public const string sCRExcludeMatch = "EXCL_MATCH_VAL";
        public const string sCRCodeTableName = "CODE_TABLE_NAME";
        public const string sCRRelateTableName = "RELATE_TABLE_NAME";
        public const string sCRRelationDesc = "RELATION_DESC";
        public const string sCRRelationRevDesc = "RELATION_REV_DESC";
        public const string sCRCodeCaption = "CODE_CAPTION";
        public const string sCRRelatedCaption = "RELATED_CAPTION";
        public const string sCRRelation = "RELATION_ENTRY";
        public const string sCRRelationID = "ID";
        public const string sCRRelationIDPfx = "CXC";
        public const string sCRRelationShort = "SHORT_CODE";
        public const string sCRRelationRelShort = "REL_SHORT_CODE";
        public const string sCRRelationDeleted = "DELETED";

        /*'<Parent Node>
        '  <SEARCH ID=SRC1>
        '    <NAME>Friendly name stored on SEARCH_VIEW.VIEW_NAME</NAME>
        '    <TABLE>name of the primary table in the search</TABLE>
        '    <SOUNDEX INDICATOR=YesNo />
        '    <MAX_NUMBER_OF_ROWS>Maximum Number ofrows to return</MAX_NUMBER_OF_ROWS>
        '    <FIELDS COUNT=nn NEXTID=nn>
        '        <FIELD ID=FLD1>
        '           <FILTER_NAME>Field name in SEARCH_DICTIONARY</FILTER_NAME>
        '           <OPERATOR>String representing the range (GT, LT, GE, LE, BET, EQ, ...)</OPERATOR>
        '           <FILTER_VALUE>value entered on the screen</FILTER_VALUE>
        '           <FILTER_VALUE>value entered on the screen</FILTER_VALUE>
        '           <SORT>Value of sorting preferences 1,2 or 3</SORT>
        '           <DISPLAY_NAME>Resulting FIELD_DESC value in SEARCH_DICTIOANRY table</DISPLAY_NAME>
        '        </FIELD>
        '    </FIELDS>
        '    <DATA_ROWS COUNT = nn NEXTID= nn>
        '        <ROW ID=ROW1>
        '           <KEY>recordkey</KEY>
        '           <COL IDREF=FLD1>resulting value</COL>
        '           <COL IDREF=FLD3>resulting value</COL>
        '        </ROW>
        '    </DATA_ROWS>
        '  </SEARCH>
        '</Parent Node>*/

        public const string sSrchNode = "SEARCH";
        public const string sSrchID = "ID";
        public const string sSrchIDPfx = "SRC";
        public const string sSrchFldIDPfx = "FLD";
        public const string sSrchDRIDPfx = "ROW";
        public const string sSrchName = "NAME";
        public const string sSrchTable = "TABLE";
        public const string sSrchSoundex = "SOUNDEX";
        public const string sSrchStartWithSortKey = "START_WITH_SORT_KEY";
        public const string sSrchHeader = "HEADER";  //SIW7844
        public const string sSrchMaxRows = "MAX_NUMBER_OF_ROWS";
        public const string sSrchNextSortKey = "NEXT_SORT_KEY";
        public const string sSrchPreviousSortKey = "PREVIOUS_SORT_KEY";
        public const string sSrchNextTechKey = "NEXT_TECH_KEY";             //(SI06298 - Implemented in SI06333)
        public const string sSrchPreviousTechKey = "PREVIOUS_TECH_KEY";     //(SI06298 - Implemented in SI06333)
        public const string sSrchSearchDirection = "SEARCH_DIRECTION";
        public const string sSrchFields = "FIELDS";
        public const string sSrchField = "FIELD";
        public const string sSrchFldNextID = "NEXTID";
        public const string sSrchFldCount = "COUNT";
        public const string sSrchFldID = "ID";
        public const string sSrchFldFilterName = "FILTER_NAME";
        public const string sSrchFldOper = "OPERATOR";
        public const string sSrchFldFilterVal1 = "FILTER_VALUE_1";
        public const string sSrchFldFilterVal2 = "FILTER_VALUE_2";
        public const string sSrchFldSort = "SORT";
        public const string sSrchFldDispName = "DISPLAY_NAME";
        public const string sSrchFldDispMask = "DISPLAY_MASK"; // SI6501
        public const string sSrchDataRows = "DATA_ROWS";
        public const string sSrchDataRow = "ROW";
        public const string sSrchDRNextID = "NEXTID";
        public const string sSrchDRCount = "COUNT";
        public const string sSrchDRID = "ID";
        public const string sSrchDR = "ROW";
        public const string sSrchRowKey = "KEY";
        public const string sSrchRowCol = "COL";
        //Start- N7268
        public const string sMergeNode = "MERGE";
        public const string sMergeName = "NAME";
        public const string sMrgIDPfx = "MRG";
        public const string sMrgFormId = "ID";
        public const string sMrgFormName = "FORM";
        public const string sMrgFormFile = "FORMFILE";
        //End - N7268
        // Start SI6501
        public const string sSrchRowColID = "IDREF";
        public const string sSrchRowColType = "TYPE";
        public const string sSrchRowColCodeID = "CODE_ID";
        public const string sSrchRowColShortCode = "CODE";
        public const string sSrchRowColDesc = "DESC"; //SIW157
        // End SI6501
        public const string sSrchFldFltCodeId = "CODE_ID";//SI06453
        /*SIW119 Start
        '<Parent Node>
        '<SYSTEM_SETTINGS NEXT_ID="0" COUNT="xx">
        '   <SYSTEM_SETTING ID="SYS1">
        '    <USER_ID>User ID for whom the setting apply. "0" is for all users </USER_ID>
        '    <TYPE>defines the type of setting</TYPE>
        '    <COMPONENT>component for which the setting apply</COMPONENT>
        '    <ITEM>item to which the setting apply. Subset of Component</ITEM>
        '    <VALUE>Vlaue of the setting</VALUE>
        '    <COMPUTER_NAME>computer name to which the setting apply</COMPUTER_NAME>
        '    <DESCRIPTION>plain english description of the setting</DESCRIPTION>
        '    <GROUP_ID>Group ID for whom the setting apply. "0" is for all users.</GROUP_ID>
        '    <CATEGORY>Category to which the setting apply</CATEGORY>
        '   </SYSTEM_SETTING>
        '</SYSTEM_SETTINGS>
        '</Parent Node>
         */
        public const string sSysCollection = "SYSTEM_SETTINGS";
        public const string sSysNextID = "NEXT_ID";
        public const string sSysCount = "COUNT";
        public const string sSysNode = "SYSTEM_SETTING";
        public const string sSysID = "ID";
        public const string sSysIDPfx = "SYS";
        public const string sSysUserID = "USER_ID";
        public const string sSysType = "TYPE";
        public const string sSysComponent = "COMPONENT";
        public const string sSysItem = "ITEM";
        public const string sSysValue = "VALUE";
        public const string sSysComputerName = "COMPUTER_NAME";
        public const string sSysDescription = "DESCRIPTION";
        public const string sSysGroupID = "GROUP_ID";
        public const string sSysCategory = "CATEGORY";
        //SIW119 End
        //SIW344 Starts
        /*
            '<LIABILITY_LOSS ID="LLxx">
            '  <UNIT_ID ID="UNTxxx"/>
            '  <TECH_KEY>AC Technical key for the element </TECH_KEY>
            '  <LIABILITY_TYPE CODE="xx">type </LIABILITY_TYPE>
            '  <INJURY_TYPE CODE="xx">type </INJURY_TYPE>
            '  <DAMAGE_TYPE CODE ="xx" >TYPE</DAMAGE_TYPE>
            '  <AREA_OF_PRACTICE CODE="xx">TYPE</AREA_OF_PRACTICE>
            '  <PLNTF_CLMT_TYPE CODE="XX">TYPE </PLNTF_CLMT_TYPE>
            '  <COVERAGE_FORM CODE="XX">TYPE </COVERAGE_FORM>
            '  <WITHIN_BIZ_SCOPE CODE="XX">TYPE</WITHIN_BIZ_SCOPE>
            '  <TYPE_OF_PREMISES CODE="XX">type </TYPE_OF_PREMISES>
            '  <TYPE_OF_PRODUCT CODE="XX">TYPE</TYPE_OF_PRODUCT>
            '  <DAMAGE_TYPE CODE ="XX" >TYPE</DAMAGE_TYPE>
            '  <PRODUCT_IND CODE ="XX" >INDICATOR</PRODUCT_IND>
            '  <AREA_OF_PRAC CODE="XX">TYPE</AREA_OF_PRAC>
            '  <HIRE_DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
            '  <EMP_TITLE CODE ="XX" >TITLE</EMP_TITLE>
            '  <YEARS_IN_POSITION> years</YEARS_IN_POSITION>
            '  <TERM_DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
            '  <PRODUCT_MANUFACTURER>MANUFACTURER NAME</PRODUCT_MANUFACTURER>
            '  <PRODUCT_BRAND_NAME>BRAND NAME</PRODUCT_BRAND_NAME>
            '  <PRODUCT_GENERIC_NAME>PRODUCT GENERIC NAME</PRODUCT_GENERIC_NAME>
            '  <PRODUCT_ALLEGED_HARM> PRODUCT ALLEGEDHARM </PRODUCT_ALLEGED_HARM>
            '  <PREMISES_OTHER>Other</PREMISES_OTHER>
            '  <PREMISES_OTHER_HTML>Other</PREMISES_OTHER_HTML>
            '  <GENERAL_DAMAGES>GENERAL DAMAGE</GENERAL_DAMAGES>
            '  <GENERAL_DAMAGES_HTML>GENERAL DAMAGE HTML</GENERAL_DAMAGES_HTML>
            '  <SPECIAL_DAMAGE>SPECICAL DAMAGE</SPECIAL_DAMAGE>
            '  <SPECIAL_DAMAGE_HTML>SPECICAL DAMAGE</SPECIAL_DAMAGE_HTML>
            '  <WHERE_PRODUCT_SEEN>PRODUCT SEEN</WHERE_PRODUCT_SEEN>
            '  <WHERE_PRODUCT_SEEN_HTML>PRODUCT SEEN HTML</WHERE_PRODUCT_SEEN_HTML>
            '  <PRODUCT_OTHER>PRODUCT OTHER</PRODUCT_OTHER>
            '  <PRODUCT_OTHER_HTML>PRODUCT OTHER</PRODUCT_OTHER_HTML>
            '  <BG_CHECK CODE ="XX">TYPE </BG_CHECK>
            '  <PARTY_INVOLVED ID="ENTxxx" CODE="Role">Affected Party</PARTY_INVOLVED>
            '  <LOSS_INFORMATION> ... </LOSS_INFORMATION>
            '</LIABILITY_LOSS>         
         */

        public const string sLLNode = "LIABILITY_LOSS";
        public const string sLLUnitReference = "UNIT_REFERENCE";
        public const string sLLUnitReferenceID = "ID";
        public const string sLLType = "LIABILITY_TYPE";
        public const string SLLInjuryType = "INJURY_TYPE";
        public const string sLLDamageType = "DAMAGE_TYPE";
        public const string sLLPlntfClmtType = "PLNTF_CLMT_TYPE";
        public const string sLLCovergaeForm = "COVERAGE_FORM";
        public const string sLLWithInBizScope = "WITHIN_BIZ_SCOPE";
        public const string sLLTypeOfPremises = "TYPE_OF_PREMISES";
        public const string sLLTypeOfProduct = "TYPE_OF_PRODUCT";
        public const string sLLProductInd = "PRODUCT_IND";
        public const string sLLAreaOfPractice = "AREA_OF_PRACTICE";
        public const string sLLEmpTitle = "EMP_TITLE";
        public const string sLLHireDate = "HIRE_DATE";
        public const string sLLTermDate = "TERM_DATE";
        public const string sLLProductIncidentIndDt = "PRODUCT_INCIDENT_IND_DT";
        public const string sLLYearsInPosition = "YEARS_IN_POSITION";
        public const string sLLProductManufacturer = "PRODUCT_MANUFACTURER";
        public const string sLLProductBrandName = "PRODUCT_BRAND_NAME";
        public const string sLLProductGenericName = "PRODUCT_GENERIC_NAME";
        public const string sLLProductAllegedName = "PRODUCT_ALLEGED_HARM";
        public const string sLLPremisesOther = "PREMISES_OTHER";
        public const string sLLPremisesOtherHTML = "PREMISES_OTHER_HTML";
        public const string sLLGeneralDamages = "GENERAL_DAMAGES";
        public const string sLLGeneralDamagesHTML = "GENERAL_DAMAGES_HTML";
        public const string sLLSpecialDamages = "SPECIAL_DAMAGE";
        public const string sLLSpecialDamagesHTML = "SPECIAL_DAMAGE_HTML";
        public const string sLLWhereProductSeen = "WHERE_PRODUCT_SEEN";
        public const string sLLWhereProductSeenHTML = "WHERE_PRODUCT_SEEN_HTML";
        public const string sLLProductOther = "PRODUCT_OTHER";
        public const string sLLProductOtherHTML = "PRODUCT_OTHER_HTML";
        public const string sLLBGCheck = "BG_CHECK";
        public const string sLLLossInfo = "LOSS_INFORMATION";
        //SIW344 Ends
        //Start SIW7214
        /**********
        '   <MEDICARE ID="CMSxxx" ENTITY_ID="ENTxxx">
        '   <!-- Multiple Medicare entries Allowed -->
        '      <TECH_KEY>AC Technical key for the element </TECH_KEY>
        '      <TREE_LABEL>component label</TREE_LABEL>
        '      <ELIGIBLE INDICATOR="TRUE|FALSE"/>
        '      <EFFECTIVE_DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <CAUSE_OF_INJURY CODE="99">
        '          Default
        '       </CAUSE_OF_INJURY>
        '      <NF_LIMIT>Amount</NF_LIMIT>
        '      <NF_EXHAUST_DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '      <PLAN SEQ="x">
        '         <PLAN_INS_CODE CODE="Standard CPT Treatment Code {string} [CMS_INS_PLAN]" TYPE="Code Type (CPT,HCFA, etc.) {string} [CPT,HCFA]">          '                description
        '         </PLAN_INS_CODE>
        '         <ORM_IND INDICATOR="TRUE|FALSE"/>
        '         <ORM_TERM_DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '         <TPOC SEQ="x">
        '            <TPOC_DATE YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '            <TPOC_AMOUNT>Amount</NF_LIMIT>
        '            <TPOC_DELAY YEAR="Year (4) {integer} [1900 - 9999]" MONTH="Month (2) {integer} [1-12]" DAY="Day (2) {integer} [1-31]"/>
        '         </TPOC>
        '            <!--Multiple TPOC allowed-->
        '      </PLAN>
        '         <!--Multiple Plans allowed-->
        '      <PARTY_INVOLVED ID="ENTxxx References an Entity below {id}" CODE="RelationCode {string} [RELATIONSHIP]">
        '              Relationship Translated {string}
        '      </PARTY_INVOLVED>
        '           <!--Multiple Parties Involved-->
        '      <DATA_ITEM>                                
        '           <!-- multiple entries -->              
        '   </MEDICARE>
         * ************************************************/
            public const string sCMSNode = "MEDICARE";
            public const string sCMSID = "ID";
            public const string sCMSIDPfx = "CMS";
            public const string sCMSEntId = "ENTITY_ID";
            public const string sCMSEligible = "ELIGIBLE";
            public const string sCMSEffectiveDt = "EFFECTIVE_DATE";
            public const string sCMSHICN = "HICN";
            public const string sCMSCauseOfInjury = "CAUSE_OF_INJURY";
            public const string sCMSDiagnosis = "DIAGNOSIS";
            public const string sCMSDiagSpecific = "SPECIFIC";
            public const string sCMSNFLimit = "NF_LIMIT";
            public const string sCMSNFExhaustDt = "NF_EXHAUST_DATE";
            public const string sCMSPlanNode = "PLAN";
            public const string sCMSPlanSeq = "SEQ";
            public const string sCMSPlanIns = "PLAN_INS_CODE";
            public const string sCMSPlanORMInd = "ORM_IND";
            public const string sCMSPlanORMTermDt = "ORM_TERM_DATE";
            public const string sCMSPlanTPOCNode = "TPOC";
            public const string sCMSPlanTPOCSeq = "SEQ";
            public const string sCMSPlanTPOCDt = "TPOC_DATE";
            public const string sCMSPlanTPOCAmt = "TPOC_AMOUNT";
            public const string sCMSPlanTPOCDelayDt = "TPOC_DELAY";
        //End SIW7214
//' Begin SIN7227
        /*'<TRANSACTION_REQUESTS NEXT_ID="0" COUNT=xx" FUNCTION="APPROVE|REJECT|OFFSET">
        '      <TRANSACTION_REQUEST >                           
        '         <HOLD_ID>HOLD_ID</HOLD_ID>
        '         <TABLE_ID>funds table ID</TABLE_ID>
        '         <TRANS_TYPE>PAYMENT</TRANS_TYPE>
        '         <TRANS_ID>trans ID</TRANS_ID>
        '         <WITH_ADJUSTMENT_FLAG INDICATOR='YESNO"/>
        '         <COMB_PAYMENT_FLAG INDICATOR='YESNO"/>
        '         <SUCCESS_FLAG INDICATOR='YESNO"/>
        '         <REASON CODE="X">Because></REASON>
        '         <REASON_DESC="desc">
        '         <COMMENT="comment">
        '      </TRANSACTION_REQUEST>
        '</TRANSACTION_REQUESTS>*/
        public const string sTransNodeGroup = "TRANSACTION_REQUESTS";
        public const string sTransFunction = "FUNCTION";
        public const string sTransNextID = "NEXT_ID";
        public const string sTransCount = "COUNT";
        public const string sTransNodeName = "TRANSACTION_REQUEST";
        public const string sTransHoldID = "HOLD_ID";
        public const string sTransTableID = "TRANS_TABLE_ID";
        public const string sTransType = "TRANS_TYPE";
        public const string sTransID = "TRANS_ID";
        public const string sTransCombPayFlag = "COMB_PAYMENT_FLAG";
        public const string sTransSuccessFlag = "SUCCESS_FLAG";
        public const string sTransWithAdj = "WITH_ADJUSTMENT";
        public const string sOverrideFlag = "OVERRIDE_FLAG";
        public const string sOverrideAmount = "OVERRIDE_AMOUNT";        
        public const string sTransReason = "REASON";
        public const string sTransReasonDesc = "REASON_DESC";
        public const string sTransComment = "COMMENT";
        //' End  SIN7227
        //Start SIN7278

       /*  <ADMIN_TRACKING NEXT_ID="0" COUNT=count>
       '   <ADMIN_TABLE NAME="Table Name">
       '      ...
       '   </ADMIN_TABLE>
       '<ADMIN_TRACKING> */

        public const string sAdmGroup  = "ADMIN_TRACKING";
        public const string sAdmNextID = "NEXT_ID";
        public const string sAdmCount  = "COUNT";
        public const string sAdmAdminTable  = "ADMIN_TABLE";
        public const string sAdmName  = "NAME";
        public const string sAdmDataName = "NAME";
        public const string sAdmDataType = "TYPE";


        //End SIN7278

        /*Begin SIW7240
        '      <COMBINED_PAY ID="CPAYxxx">
        '         <!--Mulitiple combined pay entries allowed per entity-->
        '           <TECH_KEY>AC Technical key for the element </TECH_KEY>  'SI06023
        '           <ADDRESS_REFERENCE ID="RELATIONID"
        '                      ADDRESS_ID="ADRid  References an address in the Addresses Collection {id}"
        '                      CODE="Relationship Code">Type of</ADDRESS>
        '                     <TECH_KEY>technical key value for relationship</TECH_KEY>
        '           </ADDRESS_REFERENCE >
        '           <ACCOUNT CODE="banknumber">
        '               <TECH_KEY>AC Technical key for the element </TECH_KEY>
        '               <NAME>Acciunt name</NAME>
        '               <ROUTING_NUMBER>Routing</ROUTING_NUAMBER>
        '               <ACCOUNT_NUMBER>AccountNumber</ACCOUNT_NUMBER>
        '               <FRACTIONAL_NUMBER>fractionalnumber</FRACTIONAL_NUMBER>
        '           </ACCOUNT>
        '           <NEXT_PRINT_DATE YEAR="year" MONTH="month" DAY="day"/>
        '           <FREQUENCY CODE="XX"?weekly></FREQUENCY CODE>
        '           <WEEK_MONTH_COUNT="1"/>
        '           <STOP_DATE YEAR="year" MONTH="month" DAY="day"/>
        '           <STOP_FLAG INDICATOR='YESNO"/>
        '           <NEXT_SCHED_DATE YEAR="year" MONTH="month" DAY="day"/>
        '           <CURRENT_BATCH="1"/>
        '           <CURRENT_BATCH_NUM_PAYS="1"/>
        '           <EFT_ACCT_NUM="1"/>
        '           <EFT_ROUTE_NUM="1"/>
        '           <EFT_EFF_DATE YEAR="year" MONTH="month" DAY="day"/>
        '           <EFT_EXP_DATE YEAR="year" MONTH="month" DAY="day"/>
        '           <ENTITY_ACCOUNT_ID>ACC123</ENTITY_ACCOUNT_ID>           'SIW7601
        '      </COMBINED_PAY>
        ' */
        public const string sEntEFT  = "EFT";
        public const string sEntEFTRoutingNumber  = "ROUTING_NUMBER";
        public const string sEntEFTAccountNumber = "ACCOUNT_NUMBER";
        public const string sEntDisallowPayments = "Disallow_Payments";//SIN7541
        public const string sEntEFTStartDt = "START_DATE";
        public const string sEntEFTEndDt = "END_DATE";
        public const string stringsEntCombPayID = "ID";
        public const string sEntCombPayFrequency= "FREQUENCY";
        public const string sEntCombPayWeekMonthCount = "WEEK_MONTH_COUNT";
        public const string sEntCombPayStopDate  = "STOP_DATE";
        public const string sEntCombPayStopFlag = "STOP_FLAG";
        public const string sEntCombPayNextSchedDate  = "NEXT_SCHED_DATE";
        public const string sEntCombPayLastSchedDate = "LAST_SCHED_DATE";
        public const string sEntCombPayLastDOM = "LAST_DOM";
        public const string sEntCombPayCurrBatch  = "CURRENT_BATCH";
        public const string sEntCombPayCurrBatchNumPays  = "CURRENT_BATCH_NUM_PAYS";
        public const string sEntCombPayEFTAcct = "EFT_ACCT_NUM";
        public const string sEntCombPayEFTRoute = "EFT_ROUTE_NUM";
        public const string sEntCombPayEFTEffDate = "EFT_EFF_DATE";
        public const string sEntCombPayEFTExpDate  = "EFT_EXP_DATE";
        public const string sEntCombPayEntAccountID = "ENTITY_ACCOUNT_ID";  //SIW7601
        //End  SIW7240
        // Start SIW7531 
      //  <USERS COUNT=count>
            //<USER>
            //  <Calender_ID></Calender_ID>
            //  <Assigned_User_ID></Assigned_User_ID>
            //  <Leaving_Date></Leaving_Date>
            //  <Leaving_Time></Leaving_Time>
            //  <Returning_Date></Returning_Date>
            //  <Returning_Time></Returning_Time>
            //  <Comment></Comment>
            //</USER>
      //  </USERS>
        public const string sOutOfOffice = "OutOfOffice";
        public const string sUserId = "User_ID";
        public const string sDsnID = "DSNID";
        public const string sAssignedUser = "Assigned_User_ID";
        public const string sLeavingDate = "Leaving_Date";
        public const string sLeavingTime = "Leaving_Time";
        public const string sReturningDate = "Returning_Date";
        public const string sReturningTime = "Returning_Time";
        public const string sCalenderId = "Calender_ID";
        public const string sTaskId = "Task_ID";
        public const string sComment = "Comment";
        // End SIW7531 

        //SIN7404
        public const string sQQQuickQuery = "QUICKQUERY";
        public const string sQQQueryEscape = "ESCAPE";
        public const string sQQNotificationId = "NOTIFICATION_ID";
        public const string sQQPrevNotificationId = "PREV_NOTIFICATION_ID";
        public const string sQQDisplayMessage = "DISPLAY_MSG";
        public const string sQQProceedClick = "OK_CLICK";
        public const string sQQCancelClick = "CANCEL_CLICK";
        public const string sQQDisplayType = "DISPLAY_TYPE";
        public const string SQQAction = "USER_ACTION";
        public const string SQQQueryData = "QUERYDATA";
        public const string SQQUserReSelect = "RESELECT";
        public const string sQQAssociatedComp = "ASSOCIATED_COMP";
        public const string SQQUserExit = "USER_EXIT";
        public const string SQQScriptAction = "SCRIPTACTION";
        public const string sQQProperty = "PROPERTY";
        //SIN7404
    }
}
