/**********************************************************************************************
 *   Date     |    SI   | Programmer | Description                                            *
 **********************************************************************************************
 * 10/02/2007 |         |    JTC     | Created
 * 09/29/2009 | SIN204  |    JTC     | Updates for ISO Interface
 *********************************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using CCP.Constants;
using System.Windows.Forms;

namespace CCP.Common
{
    public class Error
    {
        private object oObject1;
        private object oObject2;
        private object oDataItem1;
        private object oDataItem2;
        private string sSource;
        private string sRemarks;
        private string sDescription;
        private int iCode;

        public object ObjectItem1
        {
            get { return oObject1; }
            set { oObject1 = value; }
        }

        public object ObjectItem2
        {
            get { return oObject2; }
            set { oObject2 = value; }
        }

        public object DataItem1
        {
            get { return oDataItem1; }
            set { oDataItem1 = value; }
        }

        public object DataItem2
        {
            get { return oDataItem2; }
            set { oDataItem2 = value; }
        }

        public string Source
        {
            get { return sSource; }
            set { sSource = value; }
        }

        public string Remarks
        {
            get { return sRemarks; }
            set { sRemarks = value; }
        }

        public string Description
        {
            get { return sDescription; }
            set { sDescription = value; }
        }

        public int Code
        {
            get { return iCode; }
            set { iCode = value; }
        }

        public Error()
        {

        }
    }

    public class Errors
    {
        private ArrayList colErrors;
        private Debug m_Dbg;

        public Debug Debug
        {
            set
            {
                if ((m_Dbg != null) && (value != null))
                {
                    if (m_Dbg.DebugGUID == value.DebugGUID)
                    {
                        return;
                    }
                }
                if (value == null)
                {
                    m_Dbg = new Debug();
                    m_Dbg.Errors = this;
                }
                else
                {
                    m_Dbg = value;
                }
            }
            get
            {
                if (m_Dbg == null)
                {
                    m_Dbg = new Debug();
                    m_Dbg.Errors = this;
                }
                return m_Dbg;
            }
        }

        public Errors()
        {
            m_Dbg = null;
            colErrors = new ArrayList();
            
            //SIN204 Utils.subLoadErrorCodes(this);
        }

        public Error this[int i]
        {
            get
            {
                object v = colErrors[i];
                if (v.GetType().ToString() == "CCP.Common.Error")
                {
                    return (Error)v;
                }
                else
                {
                    return ((KeyValuePair<Error, string>)v).Key;
                }
            }
        }

        public Error Add(int Code, string Description, string Remarks,
                         String Source, object DataItem1, object DataItem2,
                         object ObjectItem1, object ObjectItem2, string sKey)
        {
            Error objNewMember = new Error();

            objNewMember.Code = Code;
            objNewMember.Description = Description;
            objNewMember.Remarks = Remarks;
            objNewMember.Source = Source;
            objNewMember.DataItem1 = DataItem1;
            objNewMember.DataItem2 = DataItem2;
            objNewMember.ObjectItem1 = ObjectItem1;
            objNewMember.ObjectItem2 = ObjectItem2;
            if (sKey == null || sKey.Length == 0)
                colErrors.Add(objNewMember);
            else
                colErrors.Add(new KeyValuePair<Error,string>(objNewMember, sKey));

            return objNewMember;
        }

        public void Clear()
        {
            colErrors.Clear();
        }

        /*public Error Item()
        {
            throw new System.NotImplementedException();
        }*/

        public Error PostError(int code, string source)
        {
            return PostError(code, source, "", null, null, true);
        }

        public Error PostError(int code, string source, string data)
        {
            return PostError(code, source, data, null, null, true);
        }

        public Error PostError(int code, string source, string data, object doc, object node)
        {
            return PostError(code, source, data, doc, node, true);
        }

        public Error PostError(int code, string source, string data, object doc, object node, bool bLogError)
        {
            //if (code == null)
            //{
            //    code = ErrorGlobalConstants.ERRC_ERROR;
            //}
            if (source == null || source == "")
            {
                source = "PostError";
            }
            if(data == null){
                data = "";
            }
            if (bLogError)
            {
                Debug.LogEntry(code + ":" + source + ":" + data);
            }
            return Add(code, "", "", source, data, "", doc, node, null);
        }

        //Start SIN204
        public DialogResult ProcessAppError(int errc, string dcsr)
        {
            MessageBoxButtons button = MessageBoxButtons.AbortRetryIgnore;
            string othr = String.Empty;
            return ProcessAppError(errc, button, dcsr, othr);
        }
        //End SIN204

        public DialogResult ProcessAppError(int errc, MessageBoxButtons button, string dcsr, string othr)
        {
            return ProcessAppError(errc, button, MessageBoxIcon.None, dcsr, othr, null, null, null, null, null, true);
        }

        public DialogResult ProcessAppError(int errc, MessageBoxButtons button, string dcsr, string othr, object data1)
        {
            return ProcessAppError(errc, button, MessageBoxIcon.None, dcsr, othr, data1, null, null, null, null, true);
        }

        public DialogResult ProcessAppError(int errc, MessageBoxButtons button, MessageBoxIcon icon, string dcsr, string othr, object data1)
        {
            return ProcessAppError(errc, button, icon, dcsr, othr, data1, null, null, null, null, true);
        }

        public DialogResult ProcessAppError(int errc, MessageBoxButtons button, MessageBoxIcon icon, string dcsr,
                                            string othr, object data1, object data2, object data3, object data4,
                                            object data5, bool bShowStack)
        {
            string sErrorStr;
            DialogResult resp;
            string errd;

            Debug.PushProc("Errors.ProcessAppError");

            resp = 0;

            sErrorStr = "Application Error:" + errc + "\r\n";
            errd = ErrorDescription(errc, data1, data2, data3, data4, data5);

            if (errd != "" && errd != null)
            {
                sErrorStr = sErrorStr + errd + "\r\n";
            }

            if (dcsr != "" && dcsr != null)
            {
                sErrorStr = sErrorStr + dcsr + "\r\n";
            }

            sErrorStr = sErrorStr + "Procedure=" + Debug.CurrProcedure + "\r\n";

            if (bShowStack)
            {
                sErrorStr = sErrorStr + "Calling Stack=" + Debug.Stack + "\r\n";
            }

            if (othr != "" && othr != null)
            {
                sErrorStr = sErrorStr + othr + "\r\n";
            }

            sErrorStr = FillInData(sErrorStr, data1, data2, data3, data4, data5);

            if (Debug.Interactive)
            {
                resp = Utils.DisplayMsgBox(Debug, sErrorStr, button, icon, Debug.AppTitle + " Error:" + errc);
                //resp = Utils.DisplayMsgBox(Debug, sErrorStr, button, icon, Debug.AppTitle + " Error:" + errc, Debug.AppHelpFile, errc);
            }
            Debug.LogEntry(sErrorStr);

            if (((resp == DialogResult.Abort) || !Debug.Interactive) && !Debug.IgnoreErrors && !Debug.DesignMode)
            {
                Debug.CloseLog();
            }

            Add(errc, errd, dcsr, Debug.CurrProcedure, "", "", null, null, "");
            //Err.Clear();
            Debug.PopProc();
            return resp;
        }

        //Start SIN204
        public Dictionary<int, string> ErrorCodes
        {
            get
            {
                Utils.subLoadErrorCodes(this);
                return Globals.errCodes;
            }
        }
        //End SIN204

        public string ErrorDescription(int errc, object data1, object data2, object data3, object data4, object data5)
        {
            string sErrStr;
            //SIN204 if (Globals.errCodes.ContainsKey(errc))
            if (ErrorCodes.ContainsKey(errc))   //SIN204
            {
                //SIN204 Globals.errCodes.TryGetValue(errc,out sErrStr);
                ErrorCodes.TryGetValue(errc, out sErrStr);  //SIN204
                sErrStr = sErrStr.Trim();
                sErrStr = FillInData(sErrStr, data1, data2, data3, data4, data5);
                return sErrStr;
            }
            return "";
        }

        public string ErrorDescription(int errc)
        {
            return ErrorDescription(errc, "*", "*", "*", "*", "*");
        }

        public string FillInData(string str, object data1, object data2, object data3, object data4, object data5)
        {
            string strerrd;
            CommonFunctions Fnc = new CommonFunctions();
            string[] paramArray = new string[5] { (string)data1, (string)data2, (string)data3, (string)data4, (string)data5 };

            strerrd = Fnc.ResolveConstantsWParray(str, paramArray);
            return strerrd;
        }

        public DialogResult ProcessError(int errcode, string CurrProcedure, Exception e)
        {
            return ProcessError(errcode, CurrProcedure, e, MessageBoxButtons.AbortRetryIgnore, true);
        }

        public DialogResult ProcessError(int errcode, string CurrProcedure, Exception e, MessageBoxButtons buttons, bool bShowStack)
        {
            string strErrorStr;
            DialogResult response;
            string et;
            string es, ed, curproc;

            response = DialogResult.Ignore;

            Debug.PushProc("Errors.ProcessError");

            Debug.DebugTrace("1", errcode, e.GetType().ToString(), null, null);

            et = e.GetType().ToString();
            ed = e.Message;
            es = e.Source;

            if (CurrProcedure != "" && CurrProcedure != null)
                curproc = CurrProcedure;
            else
                curproc = Debug.CurrProcedure;

            strErrorStr = "App Error Code:" + errcode + "\r\n" + ErrorDescription(errcode) + "\r\n";
            strErrorStr = strErrorStr + "Error:" + et + "\r\n" + ed + "\r\n" + es + "\r\n";

            if (bShowStack)
                strErrorStr = strErrorStr + "Procedure=" + curproc + "\r\n" + Debug.Stack;

            Debug.DebugTrace("2", Debug.Interactive, strErrorStr,null,null);
            if (Debug.Interactive)
                response = Utils.DisplayMsgBox(Debug, strErrorStr, buttons, MessageBoxIcon.Information,
                                               Debug.AppTitle + " Error:" + errcode);
            Debug.DebugTrace("3", null, null, null, null);
            try
            {
                Debug.LogEntry(strErrorStr);
            }
            catch { }

            if(response == DialogResult.Abort || !Debug.Interactive)
                if(!Debug.IgnoreErrors)
                    if (!Debug.DesignMode)
                    {
                        bool berr = true;
                        while (berr)
                        {
                            try
                            {
                                Debug.CloseLog();
                                berr = false;
                            }
                            catch { }
                        }
                        Application.Exit();
                    }
            Debug.DebugTrace("4", Debug.CurrProcedure, es, null, null);
            Add(errcode, strErrorStr, Debug.CurrProcedure, es, "", "", null, null, null);
            Debug.PopProc();
            return response;
        }

        public DialogResult MessageBox(int errc, MessageBoxIcon icons, MessageBoxButtons buttons, string dcsr, string othr, object data1, object data2, object data3, object data4, object data5, string sMsgTitle)
        {
            string strErrorStr;
            DialogResult response;
            string errd;
            string sTitle;

            Debug.PushProc("Errors.MessageBox");

            response = DialogResult.Ignore;

            errd = ErrorDescription(errc, data1, data2, data3, data4, data5);

            strErrorStr = "";

            if (dcsr != "" && dcsr != null)
                strErrorStr = strErrorStr + dcsr + "\r\n";
            if (errd != "" && errd != null)
                strErrorStr = strErrorStr + errd + "\r\n";
            if (othr != "" && othr != null)
                strErrorStr = strErrorStr + othr + "\r\n";

            strErrorStr = FillInData(strErrorStr, data1, data2, data3, data4, data5);

            if (sMsgTitle != "" && sMsgTitle != null)
                sTitle = sMsgTitle;
            else
                sTitle = Debug.AppTitle + " Message:" + errc;

            if (Debug.Interactive)
                response = Utils.DisplayMsgBox(Debug, strErrorStr, buttons, icons, sTitle);
            Debug.LogEntry(strErrorStr);

            if(response == DialogResult.Abort || !Debug.Interactive)
                if(!Debug.IgnoreErrors)
                    if (!Debug.DesignMode)
                    {
                        Debug.CloseLog();
                        Application.Exit();
                    }

            Add(errc, errd, dcsr, Debug.CurrProcedure, "", "", null, null, "");
            Debug.PopProc();
            return response;
        }

        public DialogResult MessageBox(int errc, MessageBoxIcon icons, MessageBoxButtons buttons, string dcsr, string othr,object data1)
        {
            return MessageBox(errc, icons, buttons, dcsr, othr, data1, "", "", "", "", "");
        }

        public string LogFile
        {
            get
            {
                return Debug.LogFile;
            }
            set
            {
                Debug.LogFile = value;
            }
        }

        public bool Logging
        {
            get
            {
                return Debug.Logging;
            }
            set
            {
                Debug.Logging = value;
            }
        }

        public bool Interactive
        {
            get
            {
                return Debug.Interactive;
            }
            set
            {
                Debug.Interactive = value;
            }
        }

        public int Count
        {
            get
            {
                return colErrors.Count;
            }
        }
    }
}
