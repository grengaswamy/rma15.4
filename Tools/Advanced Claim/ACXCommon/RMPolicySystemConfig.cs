﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Configuration;

using Riskmaster.Common;
using Riskmaster.Security;

namespace CCP.PointClaimsLoad
{

    public class RMPolicySystemConfig
    {
        private RMPolicySystemInterfaceSection rmpolicySection;
        private RMPolicySystemElement rmPolicySysElem;

        public RMPolicySystemElement ConfigElement
        {
            get
            {
                return rmPolicySysElem;
            }
        }
        public RMPolicySystemConfig(int sPolicySystemId,string sUserName,string sRMDsnName, out UserLogin objUser)
        {
            string[] sDSNDetails = null;
            UserLogin objUserLogin = null;
            objUserLogin = new UserLogin(sUserName, sRMDsnName);

            Riskmaster.Application.PolicySystemInterface.SetUpPolicySystem objPolInt = new Riskmaster.Application.PolicySystemInterface.SetUpPolicySystem(sUserName, objUserLogin.objRiskmasterDatabase.ConnectionString);
            objPolInt.GetPolicyDSNDetails(ref sDSNDetails, sPolicySystemId);
            rmpolicySection = ConfigurationManager.GetSection("RMPolicySystemInterface") as RMPolicySystemInterfaceSection;
            CheckConfigurationSection(rmpolicySection, "RMPolicySystemInterface");
            rmPolicySysElem = rmpolicySection.RMPolicySystems[sDSNDetails[6]] as RMPolicySystemElement;
            ConfigElement.DataDSN = sDSNDetails[1];
            ConfigElement.DataDSNUser = sDSNDetails[4];
            ConfigElement.DataDSNPwd = sDSNDetails[5];
            ConfigElement.DataDSNConnStr = sDSNDetails[7];
            ConfigElement.PointClient = sDSNDetails[8];
            ConfigElement.SupportDSN = string.Empty;
            ConfigElement.SupportDSNUser = string.Empty;
            ConfigElement.SupportDSNPwd = string.Empty;
            ConfigElement.ReleaseVersion = sDSNDetails[9];
            objUser = objUserLogin;

        }

        internal static void CheckConfigurationSection(object objConfigSection, string strSectionName)
        {
            if (objConfigSection == null)
            {
                throw new ConfigurationErrorsException(string.Format("Specified configuration section does not exist or has been removed: {0}", strSectionName));
            }
        }


    }
}
